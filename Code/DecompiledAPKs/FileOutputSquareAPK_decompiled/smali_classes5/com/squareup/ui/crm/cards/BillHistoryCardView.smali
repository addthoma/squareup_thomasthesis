.class public Lcom/squareup/ui/crm/cards/BillHistoryCardView;
.super Landroid/widget/LinearLayout;
.source "BillHistoryCardView.java"


# instance fields
.field private billHistoryView:Lcom/squareup/ui/activity/billhistory/BillHistoryView;

.field presenter:Lcom/squareup/ui/crm/cards/BillHistoryScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private progressBar:Landroid/widget/ProgressBar;

.field private final shortAnimTimeMs:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 32
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    const-class p2, Lcom/squareup/ui/crm/cards/BillHistoryScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/cards/BillHistoryScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/crm/cards/BillHistoryScreen$Component;->inject(Lcom/squareup/ui/crm/cards/BillHistoryCardView;)V

    .line 35
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/BillHistoryCardView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const/high16 p2, 0x10e0000

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/ui/crm/cards/BillHistoryCardView;->shortAnimTimeMs:I

    return-void
.end method


# virtual methods
.method actionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 77
    invoke-static {p0}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 46
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 47
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/BillHistoryCardView;->presenter:Lcom/squareup/ui/crm/cards/BillHistoryScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/cards/BillHistoryScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 51
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/BillHistoryCardView;->presenter:Lcom/squareup/ui/crm/cards/BillHistoryScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/cards/BillHistoryScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 52
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 39
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 41
    sget v0, Lcom/squareup/crmviewcustomer/R$id;->crm_bill_history_view:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/activity/billhistory/BillHistoryView;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/BillHistoryCardView;->billHistoryView:Lcom/squareup/ui/activity/billhistory/BillHistoryView;

    .line 42
    sget v0, Lcom/squareup/crmviewcustomer/R$id;->crm_progress_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/BillHistoryCardView;->progressBar:Landroid/widget/ProgressBar;

    return-void
.end method

.method onPrintGiftReceiptButtonClicked()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 73
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/BillHistoryCardView;->billHistoryView:Lcom/squareup/ui/activity/billhistory/BillHistoryView;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->onPrintGiftReceiptButtonClicked()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method onReceiptButtonClicked()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/BillHistoryCardView;->billHistoryView:Lcom/squareup/ui/activity/billhistory/BillHistoryView;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->onReceiptButtonClicked()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method onRefundButtonClicked()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/BillHistoryCardView;->billHistoryView:Lcom/squareup/ui/activity/billhistory/BillHistoryView;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->onRefundButtonClicked()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method onReprintTicketButtonClicked()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/BillHistoryCardView;->billHistoryView:Lcom/squareup/ui/activity/billhistory/BillHistoryView;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->onReprintTicketButtonClicked()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .line 57
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not implemented. Set saveEnabled to false."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method show(Lcom/squareup/billhistory/model/BillHistory;)V
    .locals 1

    .line 89
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/BillHistoryCardView;->billHistoryView:Lcom/squareup/ui/activity/billhistory/BillHistoryView;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->show(Lcom/squareup/billhistory/model/BillHistory;)V

    return-void
.end method

.method showProgress(Z)V
    .locals 1

    if-eqz p1, :cond_0

    .line 94
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/BillHistoryCardView;->progressBar:Landroid/widget/ProgressBar;

    iget v0, p0, Lcom/squareup/ui/crm/cards/BillHistoryCardView;->shortAnimTimeMs:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;I)V

    goto :goto_0

    .line 96
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/BillHistoryCardView;->progressBar:Landroid/widget/ProgressBar;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method public temporarilyDisablePrintGiftReceiptButton()V
    .locals 1

    .line 85
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/BillHistoryCardView;->billHistoryView:Lcom/squareup/ui/activity/billhistory/BillHistoryView;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->temporarilyDisablePrintGiftReceiptButton()V

    return-void
.end method

.method public temporarilyDisableReprintTicketButton()V
    .locals 1

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/BillHistoryCardView;->billHistoryView:Lcom/squareup/ui/activity/billhistory/BillHistoryView;

    invoke-virtual {v0}, Lcom/squareup/ui/activity/billhistory/BillHistoryView;->temporarilyDisableReprintTicketButton()V

    return-void
.end method
