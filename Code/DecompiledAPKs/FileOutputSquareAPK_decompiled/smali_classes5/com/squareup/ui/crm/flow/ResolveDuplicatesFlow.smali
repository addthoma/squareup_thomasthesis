.class public Lcom/squareup/ui/crm/flow/ResolveDuplicatesFlow;
.super Ljava/lang/Object;
.source "ResolveDuplicatesFlow.java"

# interfaces
.implements Lmortar/bundler/Bundler;
.implements Lcom/squareup/ui/crm/cards/ResolveDuplicatesScreen$Controller;
.implements Lcom/squareup/ui/crm/cards/MergingDuplicatesScreen$Controller;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/flow/ResolveDuplicatesFlow$Component;,
        Lcom/squareup/ui/crm/flow/ResolveDuplicatesFlow$SharedScope;
    }
.end annotation


# instance fields
.field private expectedDuplicateCount:Ljava/lang/Integer;

.field private final flow:Lflow/Flow;

.field private parentKey:Lcom/squareup/ui/main/RegisterTreeKey;


# direct methods
.method constructor <init>(Lflow/Flow;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/ResolveDuplicatesFlow;->flow:Lflow/Flow;

    return-void
.end method


# virtual methods
.method public closeMergingDuplicatesScreen()V
    .locals 2

    .line 83
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ResolveDuplicatesFlow;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/crm/cards/MergingDuplicatesScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public closeResolveDuplicatesScreen()V
    .locals 2

    .line 66
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ResolveDuplicatesFlow;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/crm/cards/ResolveDuplicatesScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public getExpectedDuplicateCount()Ljava/lang/Integer;
    .locals 1

    .line 75
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ResolveDuplicatesFlow;->expectedDuplicateCount:Ljava/lang/Integer;

    return-object v0
.end method

.method public getMortarBundleKey()Ljava/lang/String;
    .locals 1

    .line 41
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 2

    if-nez p1, :cond_0

    return-void

    :cond_0
    const/4 v0, -0x1

    const-string v1, "expectedDuplicateCount"

    .line 50
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/flow/ResolveDuplicatesFlow;->expectedDuplicateCount:Ljava/lang/Integer;

    .line 51
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/ResolveDuplicatesFlow;->expectedDuplicateCount:Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    if-ne p1, v0, :cond_1

    const/4 p1, 0x0

    .line 52
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/ResolveDuplicatesFlow;->expectedDuplicateCount:Ljava/lang/Integer;

    :cond_1
    return-void
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ResolveDuplicatesFlow;->expectedDuplicateCount:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 58
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const-string v1, "expectedDuplicateCount"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_0
    return-void
.end method

.method public showFirstScreen(Lcom/squareup/ui/main/RegisterTreeKey;)V
    .locals 2

    .line 87
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/ResolveDuplicatesFlow;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    const/4 v0, 0x0

    .line 88
    iput-object v0, p0, Lcom/squareup/ui/crm/flow/ResolveDuplicatesFlow;->expectedDuplicateCount:Ljava/lang/Integer;

    .line 89
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ResolveDuplicatesFlow;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/crm/cards/ResolveDuplicatesScreen;

    invoke-direct {v1, p1}, Lcom/squareup/ui/crm/cards/ResolveDuplicatesScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public showMergingDuplicatesScreen(I)V
    .locals 2

    .line 70
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/flow/ResolveDuplicatesFlow;->expectedDuplicateCount:Ljava/lang/Integer;

    .line 71
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/ResolveDuplicatesFlow;->flow:Lflow/Flow;

    new-instance v0, Lcom/squareup/ui/crm/cards/MergingDuplicatesScreen;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/ResolveDuplicatesFlow;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-direct {v0, v1}, Lcom/squareup/ui/crm/cards/MergingDuplicatesScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    sget-object v1, Lflow/Direction;->REPLACE:Lflow/Direction;

    invoke-static {p1, v0, v1}, Lcom/squareup/container/Flows;->replaceTop(Lflow/Flow;Lcom/squareup/container/ContainerTreeKey;Lflow/Direction;)V

    return-void
.end method

.method public success()V
    .locals 0

    return-void
.end method
