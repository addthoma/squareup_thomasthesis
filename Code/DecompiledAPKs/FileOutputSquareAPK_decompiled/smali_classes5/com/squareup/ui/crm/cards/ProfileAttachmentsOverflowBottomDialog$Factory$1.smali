.class Lcom/squareup/ui/crm/cards/ProfileAttachmentsOverflowBottomDialog$Factory$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "ProfileAttachmentsOverflowBottomDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/cards/ProfileAttachmentsOverflowBottomDialog$Factory;->create(Landroid/content/Context;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/crm/cards/ProfileAttachmentsOverflowBottomDialog$Factory;

.field final synthetic val$bottomSheet:Lcom/google/android/material/bottomsheet/BottomSheetDialog;

.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$runner:Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/cards/ProfileAttachmentsOverflowBottomDialog$Factory;Lcom/google/android/material/bottomsheet/BottomSheetDialog;Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;Landroid/content/Context;)V
    .locals 0

    .line 41
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsOverflowBottomDialog$Factory$1;->this$0:Lcom/squareup/ui/crm/cards/ProfileAttachmentsOverflowBottomDialog$Factory;

    iput-object p2, p0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsOverflowBottomDialog$Factory$1;->val$bottomSheet:Lcom/google/android/material/bottomsheet/BottomSheetDialog;

    iput-object p3, p0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsOverflowBottomDialog$Factory$1;->val$runner:Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;

    iput-object p4, p0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsOverflowBottomDialog$Factory$1;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 1

    .line 43
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsOverflowBottomDialog$Factory$1;->val$bottomSheet:Lcom/google/android/material/bottomsheet/BottomSheetDialog;

    invoke-virtual {p1}, Lcom/google/android/material/bottomsheet/BottomSheetDialog;->dismiss()V

    .line 44
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsOverflowBottomDialog$Factory$1;->val$runner:Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->setBusy(Ljava/lang/Boolean;)V

    .line 45
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsOverflowBottomDialog$Factory$1;->val$runner:Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ProfileAttachmentsOverflowBottomDialog$Factory$1;->val$context:Landroid/content/Context;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->downloadFile(Landroid/content/Context;)V

    return-void
.end method
