.class Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner$CameraListener;
.super Ljava/lang/Object;
.source "ProfileAttachmentsScopeRunner.java"

# interfaces
.implements Lcom/squareup/camerahelper/CameraHelper$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CameraListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;


# direct methods
.method private constructor <init>(Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;)V
    .locals 0

    .line 496
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner$CameraListener;->this$0:Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner$1;)V
    .locals 0

    .line 496
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner$CameraListener;-><init>(Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;)V

    return-void
.end method


# virtual methods
.method public onImageCanceled()V
    .locals 2

    .line 504
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner$CameraListener;->this$0:Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;

    invoke-static {v0}, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->access$200(Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;)Lflow/Flow;

    move-result-object v0

    const-class v1, Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadBottomDialog;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public onImagePicked(Landroid/net/Uri;)V
    .locals 5

    .line 498
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner$CameraListener;->this$0:Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;

    invoke-static {v0, p1}, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->access$102(Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;Landroid/net/Uri;)Landroid/net/Uri;

    .line 499
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner$CameraListener;->this$0:Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;

    invoke-static {p1}, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->access$200(Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;)Lflow/Flow;

    move-result-object p1

    sget-object v0, Lflow/Direction;->REPLACE:Lflow/Direction;

    new-instance v1, Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadScreen;

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner$CameraListener;->this$0:Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;

    invoke-static {v2}, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;->access$300(Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;)Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadScreen;-><init>(Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope;)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const-class v3, Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadBottomDialog;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-static {p1, v0, v1, v2}, Lcom/squareup/container/Flows;->goBackPastAndAdd(Lflow/Flow;Lflow/Direction;Lcom/squareup/container/ContainerTreeKey;[Ljava/lang/Class;)V

    return-void
.end method
