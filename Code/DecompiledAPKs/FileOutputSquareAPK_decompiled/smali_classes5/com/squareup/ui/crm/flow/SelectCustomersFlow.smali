.class public Lcom/squareup/ui/crm/flow/SelectCustomersFlow;
.super Ljava/lang/Object;
.source "SelectCustomersFlow.java"

# interfaces
.implements Lmortar/bundler/Bundler;
.implements Lcom/squareup/ui/crm/cards/DeleteCustomersConfirmationScreen$Controller;
.implements Lcom/squareup/ui/crm/cards/DeletingCustomersScreen$Controller;
.implements Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen$Controller;
.implements Lcom/squareup/ui/crm/cards/AddingCustomersToGroupScreen$Controller;
.implements Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen$Controller;
.implements Lcom/squareup/ui/crm/cards/MergingCustomersScreen$Runner;
.implements Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneScreen$Runner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/flow/SelectCustomersFlow$SharedScope;
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private contactSet:Lcom/squareup/protos/client/rolodex/ContactSet;

.field private final createGroupFlow:Lcom/squareup/ui/crm/flow/CreateGroupFlow;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final flow:Lflow/Flow;

.field private groupToAddTo:Lcom/squareup/protos/client/rolodex/Group;

.field private loadedContactMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;"
        }
    .end annotation
.end field

.field private final loyalty:Lcom/squareup/loyalty/LoyaltyServiceHelper;

.field private loyaltyAccountMappings:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;",
            ">;"
        }
    .end annotation
.end field

.field private final loyaltyAccounts:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final onShowSelectLoyaltyPhoneScreen:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

.field private parentKeyForLastScreen:Lcom/squareup/ui/main/RegisterTreeKey;

.field private final res:Lcom/squareup/util/Res;

.field private final result:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final rolodex:Lcom/squareup/crm/RolodexServiceHelper;

.field private final selectLoyaltyPhoneScreenBusy:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final selectedLoyaltyAccountMapping:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lflow/Flow;Lcom/squareup/ui/crm/flow/CreateGroupFlow;Lcom/squareup/settings/server/Features;Lcom/squareup/crm/RolodexServiceHelper;Lcom/squareup/loyalty/LoyaltyServiceHelper;Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Res;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->loyaltyAccounts:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 78
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->selectedLoyaltyAccountMapping:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 80
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->onShowSelectLoyaltyPhoneScreen:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 81
    invoke-static {}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->create()Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->selectLoyaltyPhoneScreenBusy:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 83
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->result:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 98
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->flow:Lflow/Flow;

    .line 99
    iput-object p2, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->createGroupFlow:Lcom/squareup/ui/crm/flow/CreateGroupFlow;

    .line 100
    iput-object p3, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->features:Lcom/squareup/settings/server/Features;

    .line 101
    iput-object p4, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    .line 102
    iput-object p5, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->loyalty:Lcom/squareup/loyalty/LoyaltyServiceHelper;

    .line 103
    iput-object p6, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->analytics:Lcom/squareup/analytics/Analytics;

    .line 104
    iput-object p7, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->res:Lcom/squareup/util/Res;

    .line 105
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->loyaltyAccountMappings:Ljava/util/Map;

    return-void
.end method

.method public static synthetic lambda$WB8Rz62IjRd0k5QURXWZ656MV48(Lcom/squareup/ui/crm/flow/SelectCustomersFlow;Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->updateMap(Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;)V

    return-void
.end method

.method public static synthetic lambda$lhTgY6QO3k6prahkt-rNlPXriGc(Lcom/squareup/ui/crm/flow/SelectCustomersFlow;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lio/reactivex/Maybe;
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->mergeProposalToLoyalty(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lio/reactivex/Maybe;

    move-result-object p0

    return-object p0
.end method

.method private mergeProposalToLoyalty(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lio/reactivex/Maybe;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/ListManualMergeProposalResponse;",
            ">;)",
            "Lio/reactivex/Maybe<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;",
            ">;>;"
        }
    .end annotation

    .line 343
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->getOkayResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/rolodex/ListManualMergeProposalResponse;

    if-nez p1, :cond_0

    .line 346
    invoke-static {}, Lio/reactivex/Maybe;->empty()Lio/reactivex/Maybe;

    move-result-object p1

    return-object p1

    .line 350
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 351
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/ListManualMergeProposalResponse;->merge_proposal:Lcom/squareup/protos/client/rolodex/MergeProposal;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/MergeProposal;->duplicate_contacts:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/rolodex/Contact;

    .line 354
    iget-object v2, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->loadedContactMap:Ljava/util/Map;

    iget-object v1, v1, Lcom/squareup/protos/client/rolodex/Contact;->contact_token:Ljava/lang/String;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/rolodex/Contact;

    if-eqz v1, :cond_1

    .line 357
    sget-object v2, Lcom/squareup/crm/util/AppSpecificDataType;->LOYALTY_ACCOUNT_TOKEN:Lcom/squareup/crm/util/AppSpecificDataType;

    .line 358
    invoke-static {v1, v2}, Lcom/squareup/crm/util/RolodexContactHelper;->getAppSpecificData(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/crm/util/AppSpecificDataType;)Lcom/squareup/protos/client/rolodex/AppField;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 360
    iget-object v1, v1, Lcom/squareup/protos/client/rolodex/AppField;->text_value:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 365
    :cond_2
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_3

    .line 368
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->selectedLoyaltyAccountMapping:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v0, Lcom/squareup/ui/crm/flow/MergeCustomersFlow;->EMPTY_LOYALTY_ACCOUNT_MAPPING:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 370
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->flow:Lflow/Flow;

    new-instance v0, Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-direct {v0, v1}, Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    .line 371
    invoke-static {}, Lio/reactivex/Maybe;->empty()Lio/reactivex/Maybe;

    move-result-object p1

    return-object p1

    .line 374
    :cond_3
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneScreen;

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-direct {v1, v2}, Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {p1, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    .line 377
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->loyalty:Lcom/squareup/loyalty/LoyaltyServiceHelper;

    invoke-virtual {p1, v0}, Lcom/squareup/loyalty/LoyaltyServiceHelper;->batchGetLoyaltyAccounts(Ljava/util/List;)Lio/reactivex/Single;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/Maybe;->fromSingle(Lio/reactivex/SingleSource;)Lio/reactivex/Maybe;

    move-result-object p1

    return-object p1
.end method

.method private updateMap(Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;)V
    .locals 4

    if-eqz p1, :cond_1

    .line 332
    iget-object p1, p1, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;->loyaltyAccounts:Ljava/util/Map;

    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/loyalty/LoyaltyAccount;

    .line 333
    iget-object v0, v0, Lcom/squareup/protos/client/loyalty/LoyaltyAccount;->mappings:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    .line 334
    iget-object v2, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->loyaltyAccountMappings:Ljava/util/Map;

    iget-object v3, v1, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;->loyalty_account_mapping_token:Ljava/lang/String;

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method public applyMultiSelectActionAndRedirect(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/ui/crm/v2/MultiSelectMode;Lcom/squareup/protos/client/rolodex/ContactSet;Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/main/RegisterTreeKey;",
            "Lcom/squareup/ui/main/RegisterTreeKey;",
            "Lcom/squareup/ui/crm/v2/MultiSelectMode;",
            "Lcom/squareup/protos/client/rolodex/ContactSet;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;)V"
        }
    .end annotation

    .line 294
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    .line 295
    iput-object p2, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->parentKeyForLastScreen:Lcom/squareup/ui/main/RegisterTreeKey;

    .line 296
    iput-object p4, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->contactSet:Lcom/squareup/protos/client/rolodex/ContactSet;

    .line 297
    iput-object p5, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->loadedContactMap:Ljava/util/Map;

    .line 298
    sget-object p2, Lcom/squareup/ui/crm/flow/SelectCustomersFlow$1;->$SwitchMap$com$squareup$ui$crm$v2$MultiSelectMode:[I

    invoke-virtual {p3}, Lcom/squareup/ui/crm/v2/MultiSelectMode;->ordinal()I

    move-result p3

    aget p2, p2, p3

    const/4 p3, 0x1

    if-eq p2, p3, :cond_3

    const/4 p3, 0x2

    if-eq p2, p3, :cond_2

    const/4 p3, 0x3

    if-eq p2, p3, :cond_1

    const/4 p3, 0x4

    if-eq p2, p3, :cond_1

    const/4 p1, 0x5

    if-eq p2, p1, :cond_0

    goto :goto_0

    .line 321
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->showAddingCustomersToGroupScreen()V

    goto :goto_0

    :cond_1
    const/4 p2, 0x0

    .line 315
    iput-object p2, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->groupToAddTo:Lcom/squareup/protos/client/rolodex/Group;

    .line 317
    iget-object p2, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->flow:Lflow/Flow;

    new-instance p3, Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen;

    invoke-direct {p3, p1}, Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {p2, p3}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 310
    :cond_2
    iget-object p2, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->flow:Lflow/Flow;

    new-instance p3, Lcom/squareup/ui/crm/cards/DeleteCustomersConfirmationScreen;

    invoke-direct {p3, p1}, Lcom/squareup/ui/crm/cards/DeleteCustomersConfirmationScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {p2, p3}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 300
    :cond_3
    iget-object p2, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->features:Lcom/squareup/settings/server/Features;

    sget-object p3, Lcom/squareup/settings/server/Features$Feature;->CRM_MANAGE_LOYALTY_IN_DIRECTORY:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p2, p3}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p2

    if-eqz p2, :cond_4

    .line 303
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->onShowSelectLoyaltyPhoneScreen:Lcom/jakewharton/rxrelay2/PublishRelay;

    sget-object p2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    goto :goto_0

    .line 305
    :cond_4
    iget-object p2, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->flow:Lflow/Flow;

    new-instance p3, Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen;

    invoke-direct {p3, p1}, Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {p2, p3}, Lflow/Flow;->set(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method public cancelAddCustomersToGroupScreen()V
    .locals 2

    .line 142
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public cancelDeleteCustomersConfirmationScreen()V
    .locals 2

    .line 250
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/crm/cards/DeleteCustomersConfirmationScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    .line 251
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_MULTI_SELECT_DELETE_CANCEL:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    return-void
.end method

.method public cancelMergeCustomersConfirmationScreen()V
    .locals 2

    .line 224
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    .line 225
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_MULTI_SELECT_MERGE_CANCEL:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    return-void
.end method

.method public closeAddingCustomersToGroupScreen()V
    .locals 2

    const/4 v0, 0x0

    .line 117
    iput-object v0, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->contactSet:Lcom/squareup/protos/client/rolodex/ContactSet;

    .line 118
    iput-object v0, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->groupToAddTo:Lcom/squareup/protos/client/rolodex/Group;

    .line 119
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    .line 120
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->result:Lcom/jakewharton/rxrelay2/PublishRelay;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public closeDeletingCustomersScreen()V
    .locals 2

    const/4 v0, 0x0

    .line 233
    iput-object v0, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->contactSet:Lcom/squareup/protos/client/rolodex/ContactSet;

    .line 234
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/crm/cards/DeletingCustomersScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    .line 235
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->result:Lcom/jakewharton/rxrelay2/PublishRelay;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public closeMergingCustomersScreen()V
    .locals 2

    const/4 v0, 0x0

    .line 124
    iput-object v0, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->contactSet:Lcom/squareup/protos/client/rolodex/ContactSet;

    .line 125
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/crm/cards/MergingCustomersScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    .line 126
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->result:Lcom/jakewharton/rxrelay2/PublishRelay;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public closeSelectLoyaltyPhoneScreen()V
    .locals 2

    .line 282
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public deleteCustomers(Lcom/squareup/protos/client/rolodex/ContactSet;)V
    .locals 4

    .line 239
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->contactSet:Lcom/squareup/protos/client/rolodex/ContactSet;

    .line 244
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->flow:Lflow/Flow;

    new-instance v0, Lcom/squareup/ui/crm/cards/DeletingCustomersScreen;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->parentKeyForLastScreen:Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-direct {v0, v1}, Lcom/squareup/ui/crm/cards/DeletingCustomersScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const-class v2, Lcom/squareup/ui/crm/cards/DeleteCustomersConfirmationScreen;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {p1, v0, v1}, Lcom/squareup/container/Flows;->goBackPastAndAdd(Lflow/Flow;Lcom/squareup/container/ContainerTreeKey;[Ljava/lang/Class;)V

    .line 246
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_MULTI_SELECT_DELETE_DELETE:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    return-void
.end method

.method public getContactSet()Lcom/squareup/protos/client/rolodex/ContactSet;
    .locals 1

    .line 193
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->contactSet:Lcom/squareup/protos/client/rolodex/ContactSet;

    return-object v0
.end method

.method public getDeleteCustomersConfirmationText()Ljava/lang/String;
    .locals 3

    .line 255
    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->getContactSet()Lcom/squareup/protos/client/rolodex/ContactSet;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/protos/client/rolodex/ContactSet;->contact_count:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 258
    iget-object v1, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->LOYALTY_DELETE_WILL_SEND_SMS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    if-eqz v1, :cond_0

    .line 260
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/crm/applet/R$string;->crm_confirm_customer_loyalty_deletion_one_format:I

    .line 261
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/crm/applet/R$string;->crm_confirm_customer_deletion_one_format:I

    .line 262
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 264
    :cond_1
    iget-object v2, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->res:Lcom/squareup/util/Res;

    if-eqz v1, :cond_2

    sget v1, Lcom/squareup/crm/applet/R$string;->crm_confirm_customer_loyalty_deletion_many_format:I

    goto :goto_1

    :cond_2
    sget v1, Lcom/squareup/crm/applet/R$string;->crm_confirm_customer_deletion_many_format:I

    :goto_1
    invoke-interface {v2, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    const-string v2, "number"

    .line 267
    invoke-virtual {v1, v2, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 268
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getGroupToAddTo()Lcom/squareup/protos/client/rolodex/Group;
    .locals 1

    .line 113
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->groupToAddTo:Lcom/squareup/protos/client/rolodex/Group;

    return-object v0
.end method

.method public getLoyaltyAccounts()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;",
            ">;"
        }
    .end annotation

    .line 206
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->loyaltyAccounts:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object v0
.end method

.method public getMortarBundleKey()Ljava/lang/String;
    .locals 1

    .line 146
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTargetLoyaltyAccountMapping()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;",
            ">;"
        }
    .end annotation

    .line 286
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->selectedLoyaltyAccountMapping:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$null$0$SelectCustomersFlow(Lio/reactivex/disposables/Disposable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 163
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->selectLoyaltyPhoneScreenBusy:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$null$1$SelectCustomersFlow()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 164
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->selectLoyaltyPhoneScreenBusy:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$onEnterScope$2$SelectCustomersFlow(Lkotlin/Unit;)Lio/reactivex/MaybeSource;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 162
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    iget-object v0, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->contactSet:Lcom/squareup/protos/client/rolodex/ContactSet;

    invoke-interface {p1, v0}, Lcom/squareup/crm/RolodexServiceHelper;->getManualMergeProposal(Lcom/squareup/protos/client/rolodex/ContactSet;)Lio/reactivex/Single;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/flow/-$$Lambda$SelectCustomersFlow$gdXnvu4xpccOv-kfZF2hKX_xq2Y;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$SelectCustomersFlow$gdXnvu4xpccOv-kfZF2hKX_xq2Y;-><init>(Lcom/squareup/ui/crm/flow/SelectCustomersFlow;)V

    .line 163
    invoke-virtual {p1, v0}, Lio/reactivex/Single;->doOnSubscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/flow/-$$Lambda$SelectCustomersFlow$Tft0TglNE_CDLRQ1M-eP9T1JULE;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$SelectCustomersFlow$Tft0TglNE_CDLRQ1M-eP9T1JULE;-><init>(Lcom/squareup/ui/crm/flow/SelectCustomersFlow;)V

    .line 164
    invoke-virtual {p1, v0}, Lio/reactivex/Single;->doOnTerminate(Lio/reactivex/functions/Action;)Lio/reactivex/Single;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/flow/-$$Lambda$SelectCustomersFlow$lhTgY6QO3k6prahkt-rNlPXriGc;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$SelectCustomersFlow$lhTgY6QO3k6prahkt-rNlPXriGc;-><init>(Lcom/squareup/ui/crm/flow/SelectCustomersFlow;)V

    .line 165
    invoke-virtual {p1, v0}, Lio/reactivex/Single;->flatMapMaybe(Lio/reactivex/functions/Function;)Lio/reactivex/Maybe;

    move-result-object p1

    return-object p1
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 150
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CRM_MANAGE_LOYALTY_IN_DIRECTORY:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 160
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->onShowSelectLoyaltyPhoneScreen:Lcom/jakewharton/rxrelay2/PublishRelay;

    new-instance v1, Lcom/squareup/ui/crm/flow/-$$Lambda$SelectCustomersFlow$nZUZLIz5pM90R_M5ASq0JV1noWY;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$SelectCustomersFlow$nZUZLIz5pM90R_M5ASq0JV1noWY;-><init>(Lcom/squareup/ui/crm/flow/SelectCustomersFlow;)V

    .line 161
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->switchMapMaybe(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 166
    invoke-static {}, Lcom/squareup/receiving/StandardReceiver;->filterSuccess()Lio/reactivex/ObservableTransformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->loyaltyAccounts:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 167
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 160
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 171
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->loyaltyAccounts:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    new-instance v1, Lcom/squareup/ui/crm/flow/-$$Lambda$SelectCustomersFlow$WB8Rz62IjRd0k5QURXWZ656MV48;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$SelectCustomersFlow$WB8Rz62IjRd0k5QURXWZ656MV48;-><init>(Lcom/squareup/ui/crm/flow/SelectCustomersFlow;)V

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 2

    if-nez p1, :cond_0

    return-void

    :cond_0
    const-string v0, "parentKey"

    .line 179
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/main/RegisterTreeKey;

    iput-object v0, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    const-string v0, "parentKeyForLastScreen"

    .line 180
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/main/RegisterTreeKey;

    iput-object v0, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->parentKeyForLastScreen:Lcom/squareup/ui/main/RegisterTreeKey;

    .line 181
    sget-object v0, Lcom/squareup/protos/client/rolodex/ContactSet;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-string v1, "contactSet"

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Protos;->loadProto(Lcom/squareup/wire/ProtoAdapter;Landroid/os/Bundle;Ljava/lang/String;)Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/rolodex/ContactSet;

    iput-object v0, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->contactSet:Lcom/squareup/protos/client/rolodex/ContactSet;

    .line 182
    sget-object v0, Lcom/squareup/protos/client/rolodex/Group;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-string v1, "groupToAddTo"

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Protos;->loadProto(Lcom/squareup/wire/ProtoAdapter;Landroid/os/Bundle;Ljava/lang/String;)Lcom/squareup/wire/Message;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/rolodex/Group;

    iput-object p1, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->groupToAddTo:Lcom/squareup/protos/client/rolodex/Group;

    return-void
.end method

.method public onResult()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 385
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->result:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-object v0
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 186
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    const-string v1, "parentKey"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 187
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->parentKeyForLastScreen:Lcom/squareup/ui/main/RegisterTreeKey;

    const-string v1, "parentKeyForLastScreen"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 188
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->contactSet:Lcom/squareup/protos/client/rolodex/ContactSet;

    invoke-static {v0}, Lcom/squareup/util/ProtosPure;->encodeOrNull(Lcom/squareup/wire/Message;)[B

    move-result-object v0

    const-string v1, "contactSet"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 189
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->groupToAddTo:Lcom/squareup/protos/client/rolodex/Group;

    invoke-static {v0}, Lcom/squareup/util/ProtosPure;->encodeOrNull(Lcom/squareup/wire/Message;)[B

    move-result-object v0

    const-string v1, "groupToAddTo"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    return-void
.end method

.method public selectLoyaltyPhoneScreenBusy()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 210
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->selectLoyaltyPhoneScreenBusy:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-object v0
.end method

.method public setGroupToAddTo(Lcom/squareup/protos/client/rolodex/Group;)V
    .locals 0

    .line 109
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->groupToAddTo:Lcom/squareup/protos/client/rolodex/Group;

    return-void
.end method

.method public setTargetLoyaltyAccountMapping(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 274
    iget-object p2, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->selectedLoyaltyAccountMapping:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    iget-object v0, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->loyaltyAccountMappings:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public showAddingCustomersToGroupScreen()V
    .locals 5

    .line 137
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupScreen;

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->parentKeyForLastScreen:Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-direct {v1, v2}, Lcom/squareup/ui/crm/cards/AddingCustomersToGroupScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const-class v3, Lcom/squareup/ui/crm/cards/AddCustomersToGroupScreen;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Lcom/squareup/container/Flows;->goBackPastAndAdd(Lflow/Flow;Lcom/squareup/container/ContainerTreeKey;[Ljava/lang/Class;)V

    return-void
.end method

.method public showCreateGroupScreen()V
    .locals 2

    .line 229
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->createGroupFlow:Lcom/squareup/ui/crm/flow/CreateGroupFlow;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/crm/flow/CreateGroupFlow;->showFirstScreen(Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-void
.end method

.method public showMergeCustomersScreen()V
    .locals 3

    .line 278
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen;

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-direct {v1, v2}, Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public showMergingCustomersScreen()V
    .locals 5

    .line 217
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/crm/cards/MergingCustomersScreen;

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->parentKeyForLastScreen:Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-direct {v1, v2}, Lcom/squareup/ui/crm/cards/MergingCustomersScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Class;

    const-class v3, Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    const-class v3, Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneScreen;

    const/4 v4, 0x1

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Lcom/squareup/container/Flows;->goBackPastAndAdd(Lflow/Flow;Lcom/squareup/container/ContainerTreeKey;[Ljava/lang/Class;)V

    .line 220
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_V2_MULTI_SELECT_MERGE_MERGE:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    return-void
.end method

.method public success()V
    .locals 2

    .line 197
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->result:Lcom/jakewharton/rxrelay2/PublishRelay;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public successMergedContact(Lcom/squareup/protos/client/rolodex/Contact;)V
    .locals 1

    .line 202
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->result:Lcom/jakewharton/rxrelay2/PublishRelay;

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public supportsLoyaltyDirectoryIntegration()Z
    .locals 2

    .line 130
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CRM_MANAGE_LOYALTY_IN_DIRECTORY:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    return v0
.end method
