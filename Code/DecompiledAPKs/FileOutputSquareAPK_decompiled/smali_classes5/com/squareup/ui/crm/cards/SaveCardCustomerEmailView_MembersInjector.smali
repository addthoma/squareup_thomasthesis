.class public final Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView_MembersInjector;
.super Ljava/lang/Object;
.source "SaveCardCustomerEmailView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView;",
        ">;"
    }
.end annotation


# instance fields
.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen$Presenter;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen$Presenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    .line 22
    iput-object p2, p0, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView_MembersInjector;->resProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen$Presenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView;",
            ">;"
        }
    .end annotation

    .line 28
    new-instance v0, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView_MembersInjector;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectPresenter(Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView;Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen$Presenter;)V
    .locals 0

    .line 39
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView;->presenter:Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen$Presenter;

    return-void
.end method

.method public static injectRes(Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView;Lcom/squareup/util/Res;)V
    .locals 0

    .line 44
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView;->res:Lcom/squareup/util/Res;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView;)V
    .locals 1

    .line 32
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen$Presenter;

    invoke-static {p1, v0}, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView_MembersInjector;->injectPresenter(Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView;Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen$Presenter;)V

    .line 33
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView_MembersInjector;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Res;

    invoke-static {p1, v0}, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView_MembersInjector;->injectRes(Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView;Lcom/squareup/util/Res;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 9
    check-cast p1, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView_MembersInjector;->injectMembers(Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView;)V

    return-void
.end method
