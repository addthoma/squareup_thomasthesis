.class public final Lcom/squareup/ui/crm/flow/SelectCustomersFlow_Factory;
.super Ljava/lang/Object;
.source "SelectCustomersFlow_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/crm/flow/SelectCustomersFlow;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final createGroupFlowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/flow/CreateGroupFlow;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final loyaltyProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltyServiceHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final rolodexProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexServiceHelper;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/flow/CreateGroupFlow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltyServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)V"
        }
    .end annotation

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p2, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow_Factory;->createGroupFlowProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p3, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 44
    iput-object p4, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow_Factory;->rolodexProvider:Ljavax/inject/Provider;

    .line 45
    iput-object p5, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow_Factory;->loyaltyProvider:Ljavax/inject/Provider;

    .line 46
    iput-object p6, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 47
    iput-object p7, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow_Factory;->resProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/crm/flow/SelectCustomersFlow_Factory;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/flow/CreateGroupFlow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltyServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;)",
            "Lcom/squareup/ui/crm/flow/SelectCustomersFlow_Factory;"
        }
    .end annotation

    .line 60
    new-instance v8, Lcom/squareup/ui/crm/flow/SelectCustomersFlow_Factory;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/crm/flow/SelectCustomersFlow_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v8
.end method

.method public static newInstance(Lflow/Flow;Lcom/squareup/ui/crm/flow/CreateGroupFlow;Lcom/squareup/settings/server/Features;Lcom/squareup/crm/RolodexServiceHelper;Lcom/squareup/loyalty/LoyaltyServiceHelper;Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Res;)Lcom/squareup/ui/crm/flow/SelectCustomersFlow;
    .locals 9

    .line 66
    new-instance v8, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/crm/flow/SelectCustomersFlow;-><init>(Lflow/Flow;Lcom/squareup/ui/crm/flow/CreateGroupFlow;Lcom/squareup/settings/server/Features;Lcom/squareup/crm/RolodexServiceHelper;Lcom/squareup/loyalty/LoyaltyServiceHelper;Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Res;)V

    return-object v8
.end method


# virtual methods
.method public get()Lcom/squareup/ui/crm/flow/SelectCustomersFlow;
    .locals 8

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lflow/Flow;

    iget-object v0, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow_Factory;->createGroupFlowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/ui/crm/flow/CreateGroupFlow;

    iget-object v0, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow_Factory;->rolodexProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/crm/RolodexServiceHelper;

    iget-object v0, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow_Factory;->loyaltyProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/loyalty/LoyaltyServiceHelper;

    iget-object v0, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/ui/crm/flow/SelectCustomersFlow_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/util/Res;

    invoke-static/range {v1 .. v7}, Lcom/squareup/ui/crm/flow/SelectCustomersFlow_Factory;->newInstance(Lflow/Flow;Lcom/squareup/ui/crm/flow/CreateGroupFlow;Lcom/squareup/settings/server/Features;Lcom/squareup/crm/RolodexServiceHelper;Lcom/squareup/loyalty/LoyaltyServiceHelper;Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Res;)Lcom/squareup/ui/crm/flow/SelectCustomersFlow;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/SelectCustomersFlow_Factory;->get()Lcom/squareup/ui/crm/flow/SelectCustomersFlow;

    move-result-object v0

    return-object v0
.end method
