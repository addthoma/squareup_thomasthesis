.class public interface abstract Lcom/squareup/ui/crm/cards/AllFrequentItemsScreen$Runner;
.super Ljava/lang/Object;
.source "AllFrequentItemsScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/AllFrequentItemsScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation


# virtual methods
.method public abstract closeAllFrequentItemsScreen()V
.end method

.method public abstract getContactForAllFrequentItemsScreen()Lcom/squareup/protos/client/rolodex/Contact;
.end method
