.class public final Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentView_MembersInjector;
.super Ljava/lang/Object;
.source "MultiOptionFilterContentView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentView;",
        ">;"
    }
.end annotation


# instance fields
.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentPresenter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentPresenter;",
            ">;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentPresenter;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentView;",
            ">;"
        }
    .end annotation

    .line 26
    new-instance v0, Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentView_MembersInjector;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentView_MembersInjector;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectPresenter(Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentView;Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentPresenter;)V
    .locals 0

    .line 36
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentView;->presenter:Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentPresenter;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentView;)V
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentPresenter;

    invoke-static {p1, v0}, Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentView_MembersInjector;->injectPresenter(Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentView;Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentPresenter;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 8
    check-cast p1, Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentView_MembersInjector;->injectMembers(Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentView;)V

    return-void
.end method
