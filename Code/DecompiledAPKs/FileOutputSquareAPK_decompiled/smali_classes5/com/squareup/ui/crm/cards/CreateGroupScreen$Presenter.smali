.class Lcom/squareup/ui/crm/cards/CreateGroupScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "CreateGroupScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/CreateGroupScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/crm/cards/CreateGroupView;",
        ">;"
    }
.end annotation


# static fields
.field private static final KEY_UNIQUE_KEY:Ljava/lang/String; = "uniqueKey"


# instance fields
.field private final busy:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private createGroupDisposable:Lio/reactivex/disposables/Disposable;

.field private final errorBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

.field private final onSaveClicked:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field private final rolodex:Lcom/squareup/crm/RolodexServiceHelper;

.field private final runner:Lcom/squareup/ui/crm/cards/CreateGroupScreen$Runner;

.field private final threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

.field private uniqueKey:Ljava/util/UUID;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/cards/CreateGroupScreen$Runner;Lcom/squareup/ui/ErrorsBarPresenter;Lcom/squareup/crm/RolodexServiceHelper;Lcom/squareup/util/Res;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V
    .locals 1
    .param p5    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 93
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    const/4 v0, 0x0

    .line 82
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/CreateGroupScreen$Presenter;->busy:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 83
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/CreateGroupScreen$Presenter;->onSaveClicked:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 84
    invoke-static {}, Lio/reactivex/disposables/Disposables;->empty()Lio/reactivex/disposables/Disposable;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/CreateGroupScreen$Presenter;->createGroupDisposable:Lio/reactivex/disposables/Disposable;

    .line 94
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/CreateGroupScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/CreateGroupScreen$Runner;

    .line 95
    iput-object p2, p0, Lcom/squareup/ui/crm/cards/CreateGroupScreen$Presenter;->errorBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

    .line 96
    iput-object p3, p0, Lcom/squareup/ui/crm/cards/CreateGroupScreen$Presenter;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    .line 97
    iput-object p4, p0, Lcom/squareup/ui/crm/cards/CreateGroupScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 98
    iput-object p5, p0, Lcom/squareup/ui/crm/cards/CreateGroupScreen$Presenter;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    const/4 p1, 0x1

    .line 100
    invoke-virtual {p2, p1}, Lcom/squareup/ui/ErrorsBarPresenter;->setMaxMessages(I)V

    return-void
.end method

.method static synthetic lambda$null$4(Ljava/lang/Boolean;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 138
    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-nez p0, :cond_0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$null$7(Lkotlin/Unit;Lcom/squareup/protos/client/rolodex/Group;)Lcom/squareup/protos/client/rolodex/Group;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    return-object p1
.end method

.method private onSavePressed(Lcom/squareup/ui/crm/cards/CreateGroupView;Lcom/squareup/protos/client/rolodex/Group;)V
    .locals 2

    .line 167
    invoke-static {p1}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 169
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CreateGroupScreen$Presenter;->createGroupDisposable:Lio/reactivex/disposables/Disposable;

    invoke-interface {v0}, Lio/reactivex/disposables/Disposable;->dispose()V

    .line 170
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CreateGroupScreen$Presenter;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/CreateGroupScreen$Presenter;->uniqueKey:Ljava/util/UUID;

    invoke-interface {v0, p2, v1}, Lcom/squareup/crm/RolodexServiceHelper;->upsertManualGroup(Lcom/squareup/protos/client/rolodex/Group;Ljava/util/UUID;)Lio/reactivex/Single;

    move-result-object p2

    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$CreateGroupScreen$Presenter$kLpEgZsx3W5EkFwcc7f17djUVoY;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$CreateGroupScreen$Presenter$kLpEgZsx3W5EkFwcc7f17djUVoY;-><init>(Lcom/squareup/ui/crm/cards/CreateGroupScreen$Presenter;)V

    .line 171
    invoke-virtual {p2, v0}, Lio/reactivex/Single;->doOnSubscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object p2

    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$CreateGroupScreen$Presenter$sOl8WykuK7YfJdGBlJw8Gm5qZKE;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$CreateGroupScreen$Presenter$sOl8WykuK7YfJdGBlJw8Gm5qZKE;-><init>(Lcom/squareup/ui/crm/cards/CreateGroupScreen$Presenter;)V

    .line 175
    invoke-virtual {p2, v0}, Lio/reactivex/Single;->doOnTerminate(Lio/reactivex/functions/Action;)Lio/reactivex/Single;

    move-result-object p2

    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$CreateGroupScreen$Presenter$0eJLoJ6q0szXSw6fQ3SGSZU6P00;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$CreateGroupScreen$Presenter$0eJLoJ6q0szXSw6fQ3SGSZU6P00;-><init>(Lcom/squareup/ui/crm/cards/CreateGroupScreen$Presenter;Lcom/squareup/ui/crm/cards/CreateGroupView;)V

    .line 179
    invoke-virtual {p2, v0}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/CreateGroupScreen$Presenter;->createGroupDisposable:Lio/reactivex/disposables/Disposable;

    return-void
.end method


# virtual methods
.method public synthetic lambda$null$12$CreateGroupScreen$Presenter(Lcom/squareup/ui/crm/cards/CreateGroupView;Lcom/squareup/protos/client/rolodex/UpsertGroupResponse;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 181
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CreateGroupScreen$Presenter;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 182
    invoke-static {p1}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 183
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/CreateGroupScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/CreateGroupScreen$Runner;

    iget-object p2, p2, Lcom/squareup/protos/client/rolodex/UpsertGroupResponse;->group:Lcom/squareup/protos/client/rolodex/Group;

    invoke-interface {p1, p2}, Lcom/squareup/ui/crm/cards/CreateGroupScreen$Runner;->closeCreateGroupScreen(Lcom/squareup/protos/client/rolodex/Group;)V

    return-void
.end method

.method public synthetic lambda$null$13$CreateGroupScreen$Presenter(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 185
    iget-object p2, p0, Lcom/squareup/ui/crm/cards/CreateGroupScreen$Presenter;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {p2}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 186
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->getOkayResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/rolodex/UpsertGroupResponse;

    const-string p2, ""

    if-eqz p1, :cond_0

    .line 187
    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/UpsertGroupResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object v0, v0, Lcom/squareup/protos/client/Status;->localized_description:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 188
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CreateGroupScreen$Presenter;->errorBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/UpsertGroupResponse;->status:Lcom/squareup/protos/client/Status;

    iget-object p1, p1, Lcom/squareup/protos/client/Status;->localized_description:Ljava/lang/String;

    invoke-virtual {v0, p2, p1}, Lcom/squareup/ui/ErrorsBarPresenter;->addError(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 190
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/CreateGroupScreen$Presenter;->errorBarPresenter:Lcom/squareup/ui/ErrorsBarPresenter;

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CreateGroupScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/crmscreens/R$string;->crm_group_saving_error:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, p2, v0}, Lcom/squareup/ui/ErrorsBarPresenter;->addError(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method public synthetic lambda$null$2$CreateGroupScreen$Presenter(Lcom/squareup/ui/crm/cards/CreateGroupView;Ljava/lang/Boolean;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 126
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CreateGroupScreen$Presenter;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 127
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-virtual {p1, v0}, Lcom/squareup/ui/crm/cards/CreateGroupView;->setActionBarUpButtonEnabled(Z)V

    .line 128
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-virtual {p1, v0}, Lcom/squareup/ui/crm/cards/CreateGroupView;->setEnabled(Z)V

    .line 129
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/cards/CreateGroupView;->showProgress(Z)V

    return-void
.end method

.method public synthetic lambda$null$5$CreateGroupScreen$Presenter(Lcom/squareup/ui/crm/cards/CreateGroupView;Ljava/lang/Boolean;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 141
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CreateGroupScreen$Presenter;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 142
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/cards/CreateGroupView;->setActionBarPrimaryButtonEnabled(Z)V

    return-void
.end method

.method public synthetic lambda$null$8$CreateGroupScreen$Presenter(Lcom/squareup/ui/crm/cards/CreateGroupView;Lcom/squareup/protos/client/rolodex/Group;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 150
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CreateGroupScreen$Presenter;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 151
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/crm/cards/CreateGroupScreen$Presenter;->onSavePressed(Lcom/squareup/ui/crm/cards/CreateGroupView;Lcom/squareup/protos/client/rolodex/Group;)V

    return-void
.end method

.method public synthetic lambda$onLoad$0$CreateGroupScreen$Presenter(Lcom/squareup/ui/crm/cards/CreateGroupView;)V
    .locals 1

    .line 115
    invoke-static {p1}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 116
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/CreateGroupScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/CreateGroupScreen$Runner;

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Lcom/squareup/ui/crm/cards/CreateGroupScreen$Runner;->closeCreateGroupScreen(Lcom/squareup/protos/client/rolodex/Group;)V

    return-void
.end method

.method public synthetic lambda$onLoad$1$CreateGroupScreen$Presenter()V
    .locals 2

    .line 119
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CreateGroupScreen$Presenter;->onSaveClicked:Lcom/jakewharton/rxrelay2/PublishRelay;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$onLoad$3$CreateGroupScreen$Presenter(Lcom/squareup/ui/crm/cards/CreateGroupView;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 124
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CreateGroupScreen$Presenter;->busy:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$CreateGroupScreen$Presenter$PElV9KlzM6vRHFHD6CQmMHtQaVk;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$CreateGroupScreen$Presenter$PElV9KlzM6vRHFHD6CQmMHtQaVk;-><init>(Lcom/squareup/ui/crm/cards/CreateGroupScreen$Presenter;Lcom/squareup/ui/crm/cards/CreateGroupView;)V

    .line 125
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$6$CreateGroupScreen$Presenter(Lcom/squareup/ui/crm/cards/CreateGroupView;)Lio/reactivex/disposables/Disposable;
    .locals 3

    .line 134
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CreateGroupScreen$Presenter;->busy:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    .line 136
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/CreateGroupView;->group()Lio/reactivex/Observable;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/crm/cards/-$$Lambda$Q1OWHH8J2eGDNEPohIiSMbya4g4;->INSTANCE:Lcom/squareup/ui/crm/cards/-$$Lambda$Q1OWHH8J2eGDNEPohIiSMbya4g4;

    .line 137
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/crm/cards/-$$Lambda$CreateGroupScreen$Presenter$7UpOQtgNRChSyRs1BNQ6PbIMrvU;->INSTANCE:Lcom/squareup/ui/crm/cards/-$$Lambda$CreateGroupScreen$Presenter$7UpOQtgNRChSyRs1BNQ6PbIMrvU;

    .line 134
    invoke-static {v0, v1, v2}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    .line 139
    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$CreateGroupScreen$Presenter$hS7We8i8xItreUE1EhQvpY-lWrs;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$CreateGroupScreen$Presenter$hS7We8i8xItreUE1EhQvpY-lWrs;-><init>(Lcom/squareup/ui/crm/cards/CreateGroupScreen$Presenter;Lcom/squareup/ui/crm/cards/CreateGroupView;)V

    .line 140
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$9$CreateGroupScreen$Presenter(Lcom/squareup/ui/crm/cards/CreateGroupView;)Lio/reactivex/disposables/Disposable;
    .locals 3

    .line 147
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CreateGroupScreen$Presenter;->onSaveClicked:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 148
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/CreateGroupView;->group()Lio/reactivex/Observable;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/crm/cards/-$$Lambda$CreateGroupScreen$Presenter$nQ0Vx6Au5kv2SwaYSElYsx8-uVs;->INSTANCE:Lcom/squareup/ui/crm/cards/-$$Lambda$CreateGroupScreen$Presenter$nQ0Vx6Au5kv2SwaYSElYsx8-uVs;

    invoke-virtual {v0, v1, v2}, Lcom/jakewharton/rxrelay2/PublishRelay;->withLatestFrom(Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$CreateGroupScreen$Presenter$xSum3lHaz-oiRTFUCTD9LY2fKgA;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$CreateGroupScreen$Presenter$xSum3lHaz-oiRTFUCTD9LY2fKgA;-><init>(Lcom/squareup/ui/crm/cards/CreateGroupScreen$Presenter;Lcom/squareup/ui/crm/cards/CreateGroupView;)V

    .line 149
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onSavePressed$10$CreateGroupScreen$Presenter(Lio/reactivex/disposables/Disposable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 172
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/CreateGroupScreen$Presenter;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {p1}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 173
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/CreateGroupScreen$Presenter;->busy:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$onSavePressed$11$CreateGroupScreen$Presenter()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 176
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CreateGroupScreen$Presenter;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 177
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CreateGroupScreen$Presenter;->busy:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$onSavePressed$14$CreateGroupScreen$Presenter(Lcom/squareup/ui/crm/cards/CreateGroupView;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 179
    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$CreateGroupScreen$Presenter$Ed79wHQwOfTvbP7rr5Jp1nLKmTY;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$CreateGroupScreen$Presenter$Ed79wHQwOfTvbP7rr5Jp1nLKmTY;-><init>(Lcom/squareup/ui/crm/cards/CreateGroupScreen$Presenter;Lcom/squareup/ui/crm/cards/CreateGroupView;)V

    new-instance p1, Lcom/squareup/ui/crm/cards/-$$Lambda$CreateGroupScreen$Presenter$GpZ4elwruT0WbZhGCWuJp93o89k;

    invoke-direct {p1, p0, p2}, Lcom/squareup/ui/crm/cards/-$$Lambda$CreateGroupScreen$Presenter$GpZ4elwruT0WbZhGCWuJp93o89k;-><init>(Lcom/squareup/ui/crm/cards/CreateGroupScreen$Presenter;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V

    invoke-virtual {p2, v0, p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method protected onExitScope()V
    .locals 1

    .line 104
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CreateGroupScreen$Presenter;->createGroupDisposable:Lio/reactivex/disposables/Disposable;

    invoke-interface {v0}, Lio/reactivex/disposables/Disposable;->dispose()V

    .line 105
    invoke-super {p0}, Lmortar/ViewPresenter;->onExitScope()V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 5

    .line 109
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 110
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/CreateGroupScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/cards/CreateGroupView;

    .line 112
    new-instance v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v3, p0, Lcom/squareup/ui/crm/cards/CreateGroupScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/crmscreens/R$string;->crm_create_group_title:I

    .line 113
    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/crm/cards/-$$Lambda$CreateGroupScreen$Presenter$5kQ2C-M7gGaKWycciK9O3ahsVgU;

    invoke-direct {v2, p0, v0}, Lcom/squareup/ui/crm/cards/-$$Lambda$CreateGroupScreen$Presenter$5kQ2C-M7gGaKWycciK9O3ahsVgU;-><init>(Lcom/squareup/ui/crm/cards/CreateGroupScreen$Presenter;Lcom/squareup/ui/crm/cards/CreateGroupView;)V

    .line 114
    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/crm/cards/CreateGroupScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/common/strings/R$string;->save:I

    .line 118
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/crm/cards/-$$Lambda$CreateGroupScreen$Presenter$nX0b0h1S9Y0-vny24mhS2vrl1cM;

    invoke-direct {v2, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$CreateGroupScreen$Presenter$nX0b0h1S9Y0-vny24mhS2vrl1cM;-><init>(Lcom/squareup/ui/crm/cards/CreateGroupScreen$Presenter;)V

    .line 119
    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 120
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v1

    .line 112
    invoke-virtual {v0, v1}, Lcom/squareup/ui/crm/cards/CreateGroupView;->setActionBarConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 123
    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$CreateGroupScreen$Presenter$7jvhvV_RHNYfd8x6fidFmJaELHM;

    invoke-direct {v1, p0, v0}, Lcom/squareup/ui/crm/cards/-$$Lambda$CreateGroupScreen$Presenter$7jvhvV_RHNYfd8x6fidFmJaELHM;-><init>(Lcom/squareup/ui/crm/cards/CreateGroupScreen$Presenter;Lcom/squareup/ui/crm/cards/CreateGroupView;)V

    invoke-static {v0, v1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 133
    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$CreateGroupScreen$Presenter$81lWkRs267DoldOBwGz4_L3utzw;

    invoke-direct {v1, p0, v0}, Lcom/squareup/ui/crm/cards/-$$Lambda$CreateGroupScreen$Presenter$81lWkRs267DoldOBwGz4_L3utzw;-><init>(Lcom/squareup/ui/crm/cards/CreateGroupScreen$Presenter;Lcom/squareup/ui/crm/cards/CreateGroupView;)V

    invoke-static {v0, v1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 146
    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$CreateGroupScreen$Presenter$HoGC246Lhfedv9Sg6EiqwdNhH44;

    invoke-direct {v1, p0, v0}, Lcom/squareup/ui/crm/cards/-$$Lambda$CreateGroupScreen$Presenter$HoGC246Lhfedv9Sg6EiqwdNhH44;-><init>(Lcom/squareup/ui/crm/cards/CreateGroupScreen$Presenter;Lcom/squareup/ui/crm/cards/CreateGroupView;)V

    invoke-static {v0, v1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    if-nez p1, :cond_0

    .line 155
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/CreateGroupScreen$Presenter;->uniqueKey:Ljava/util/UUID;

    .line 156
    invoke-virtual {v0}, Lcom/squareup/ui/crm/cards/CreateGroupView;->setInitialFocus()V

    goto :goto_0

    :cond_0
    const-string/jumbo v0, "uniqueKey"

    .line 158
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/CreateGroupScreen$Presenter;->uniqueKey:Ljava/util/UUID;

    :goto_0
    return-void
.end method

.method protected onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 163
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CreateGroupScreen$Presenter;->uniqueKey:Ljava/util/UUID;

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "uniqueKey"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
