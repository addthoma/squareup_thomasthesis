.class public final Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner_Factory;
.super Ljava/lang/Object;
.source "ProfileAttachmentsScopeRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final arg10Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final arg11Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final arg12Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexServiceHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexAttachmentLoader;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/camerahelper/CameraHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final arg5Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadState;",
            ">;"
        }
    .end annotation
.end field

.field private final arg6Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final arg7Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/ToastFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg8Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field

.field private final arg9Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexAttachmentLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/camerahelper/CameraHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/ToastFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;)V"
        }
    .end annotation

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 58
    iput-object p2, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 59
    iput-object p3, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 60
    iput-object p4, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner_Factory;->arg3Provider:Ljavax/inject/Provider;

    .line 61
    iput-object p5, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner_Factory;->arg4Provider:Ljavax/inject/Provider;

    .line 62
    iput-object p6, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner_Factory;->arg5Provider:Ljavax/inject/Provider;

    .line 63
    iput-object p7, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner_Factory;->arg6Provider:Ljavax/inject/Provider;

    .line 64
    iput-object p8, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner_Factory;->arg7Provider:Ljavax/inject/Provider;

    .line 65
    iput-object p9, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner_Factory;->arg8Provider:Ljavax/inject/Provider;

    .line 66
    iput-object p10, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner_Factory;->arg9Provider:Ljavax/inject/Provider;

    .line 67
    iput-object p11, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner_Factory;->arg10Provider:Ljavax/inject/Provider;

    .line 68
    iput-object p12, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner_Factory;->arg11Provider:Ljavax/inject/Provider;

    .line 69
    iput-object p13, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner_Factory;->arg12Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner_Factory;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexAttachmentLoader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/camerahelper/CameraHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/ToastFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;)",
            "Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner_Factory;"
        }
    .end annotation

    .line 84
    new-instance v14, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner_Factory;

    move-object v0, v14

    move-object v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    invoke-direct/range {v0 .. v13}, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v14
.end method

.method public static newInstance(Lflow/Flow;Lcom/squareup/crm/RolodexServiceHelper;Lcom/squareup/crm/RolodexAttachmentLoader;Lcom/squareup/camerahelper/CameraHelper;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadState;Lcom/squareup/util/Res;Lcom/squareup/util/ToastFactory;Ljava/text/DateFormat;Ljava/text/DateFormat;Ljava/util/Locale;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;)Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;
    .locals 15

    .line 91
    new-instance v14, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;

    move-object v0, v14

    move-object v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    invoke-direct/range {v0 .. v13}, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;-><init>(Lflow/Flow;Lcom/squareup/crm/RolodexServiceHelper;Lcom/squareup/crm/RolodexAttachmentLoader;Lcom/squareup/camerahelper/CameraHelper;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadState;Lcom/squareup/util/Res;Lcom/squareup/util/ToastFactory;Ljava/text/DateFormat;Ljava/text/DateFormat;Ljava/util/Locale;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;)V

    return-object v14
.end method


# virtual methods
.method public get()Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;
    .locals 14

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lflow/Flow;

    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/crm/RolodexServiceHelper;

    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/crm/RolodexAttachmentLoader;

    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/camerahelper/CameraHelper;

    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner_Factory;->arg4Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner_Factory;->arg5Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadState;

    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner_Factory;->arg6Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner_Factory;->arg7Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/util/ToastFactory;

    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner_Factory;->arg8Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Ljava/text/DateFormat;

    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner_Factory;->arg9Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Ljava/text/DateFormat;

    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner_Factory;->arg10Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Ljava/util/Locale;

    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner_Factory;->arg11Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Lio/reactivex/Scheduler;

    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner_Factory;->arg12Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v13, v0

    check-cast v13, Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-static/range {v1 .. v13}, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner_Factory;->newInstance(Lflow/Flow;Lcom/squareup/crm/RolodexServiceHelper;Lcom/squareup/crm/RolodexAttachmentLoader;Lcom/squareup/camerahelper/CameraHelper;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/crm/cards/ProfileAttachmentsUploadState;Lcom/squareup/util/Res;Lcom/squareup/util/ToastFactory;Ljava/text/DateFormat;Ljava/text/DateFormat;Ljava/util/Locale;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;)Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 19
    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner_Factory;->get()Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;

    move-result-object v0

    return-object v0
.end method
