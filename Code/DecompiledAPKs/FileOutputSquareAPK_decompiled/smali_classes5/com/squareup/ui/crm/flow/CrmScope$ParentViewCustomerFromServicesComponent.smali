.class public interface abstract Lcom/squareup/ui/crm/flow/CrmScope$ParentViewCustomerFromServicesComponent;
.super Ljava/lang/Object;
.source "CrmScope.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/flow/CrmScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ParentViewCustomerFromServicesComponent"
.end annotation


# virtual methods
.method public abstract addCustomerToInvoice(Lcom/squareup/ui/crm/flow/CrmScope$AddToHoldsCustomerModule;)Lcom/squareup/ui/crm/flow/CrmScope$AddToHoldsCustomerC;
.end method

.method public abstract viewCustomer(Lcom/squareup/ui/crm/flow/CrmScope$BaseViewCustomerProfileWithRefundHelperModule;)Lcom/squareup/ui/crm/flow/CrmScope$ViewCustomerFromServicesComponent;
.end method
