.class public interface abstract Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersScreen$Runner;
.super Ljava/lang/Object;
.source "SeeAllRewardTiersScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation


# virtual methods
.method public abstract cancelSeeAllRewardTiersScreen()V
.end method

.method public abstract getHoldsCoupons()Lcom/squareup/checkout/HoldsCoupons;
.end method

.method public abstract loyaltyStatus()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;",
            ">;"
        }
    .end annotation
.end method

.method public abstract rewardTierClicked(Lcom/squareup/protos/client/loyalty/RewardTier;)V
.end method

.method public abstract unredeemRewardTier(Lcom/squareup/protos/client/loyalty/RewardTier;)V
.end method
