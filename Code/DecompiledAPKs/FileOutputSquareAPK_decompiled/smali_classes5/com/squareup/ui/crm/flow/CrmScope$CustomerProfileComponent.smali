.class public interface abstract Lcom/squareup/ui/crm/flow/CrmScope$CustomerProfileComponent;
.super Ljava/lang/Object;
.source "CrmScope.java"

# interfaces
.implements Lcom/squareup/ui/crm/flow/CrmScope$SharedCustomerProfileComponent;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/flow/CrmScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "CustomerProfileComponent"
.end annotation


# virtual methods
.method public abstract allAppointmentsCoordinatorFactory()Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator$Factory;
.end method

.method public abstract customerInvoiceCoordinator()Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;
.end method

.method public abstract dippedCardSpinnerScreen()Lcom/squareup/ui/crm/cards/DippedCardSpinnerScreen$Component;
.end method

.method public abstract saveCardCustomerEmailScreen()Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen$Component;
.end method

.method public abstract saveCardSpinnerScreen()Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Component;
.end method

.method public abstract saveCardVerifyPostalCodeScreen()Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Component;
.end method
