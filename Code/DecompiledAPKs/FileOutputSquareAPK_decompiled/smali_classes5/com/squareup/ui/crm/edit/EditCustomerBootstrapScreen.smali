.class public final Lcom/squareup/ui/crm/edit/EditCustomerBootstrapScreen;
.super Lcom/squareup/container/BootstrapTreeKey;
.source "EditCustomerBootstrapScreen.kt"

# interfaces
.implements Lcom/squareup/ui/buyer/PaymentExempt;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\u0018\u00002\u00020\u00012\u00020\u0002B\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u0010\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH\u0017J\u0008\u0010\u000c\u001a\u00020\rH\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/ui/crm/edit/EditCustomerBootstrapScreen;",
        "Lcom/squareup/container/BootstrapTreeKey;",
        "Lcom/squareup/ui/buyer/PaymentExempt;",
        "props",
        "Lcom/squareup/ui/crm/edit/EditCustomerProps;",
        "updateCustomerFlow",
        "Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;",
        "(Lcom/squareup/ui/crm/edit/EditCustomerProps;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;)V",
        "doRegister",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "getParentKey",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final props:Lcom/squareup/ui/crm/edit/EditCustomerProps;

.field private final updateCustomerFlow:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/crm/edit/EditCustomerProps;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;)V
    .locals 1

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "updateCustomerFlow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0}, Lcom/squareup/container/BootstrapTreeKey;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/crm/edit/EditCustomerBootstrapScreen;->props:Lcom/squareup/ui/crm/edit/EditCustomerProps;

    iput-object p2, p0, Lcom/squareup/ui/crm/edit/EditCustomerBootstrapScreen;->updateCustomerFlow:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;

    return-void
.end method

.method public static final synthetic access$getProps$p(Lcom/squareup/ui/crm/edit/EditCustomerBootstrapScreen;)Lcom/squareup/ui/crm/edit/EditCustomerProps;
    .locals 0

    .line 11
    iget-object p0, p0, Lcom/squareup/ui/crm/edit/EditCustomerBootstrapScreen;->props:Lcom/squareup/ui/crm/edit/EditCustomerProps;

    return-object p0
.end method


# virtual methods
.method public doRegister(Lmortar/MortarScope;)V
    .locals 3

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    sget-object v0, Lcom/squareup/ui/crm/edit/EditCustomerWorkflowRunner;->Companion:Lcom/squareup/ui/crm/edit/EditCustomerWorkflowRunner$Companion;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/crm/edit/EditCustomerWorkflowRunner$Companion;->get(Lmortar/MortarScope;)Lcom/squareup/ui/crm/edit/EditCustomerWorkflowRunner;

    move-result-object p1

    .line 21
    invoke-interface {p1}, Lcom/squareup/ui/crm/edit/EditCustomerWorkflowRunner;->onResult()Lio/reactivex/Observable;

    move-result-object v0

    .line 22
    new-instance v1, Lcom/squareup/ui/crm/edit/EditCustomerBootstrapScreen$doRegister$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/edit/EditCustomerBootstrapScreen$doRegister$1;-><init>(Lcom/squareup/ui/crm/edit/EditCustomerBootstrapScreen;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 28
    new-instance v1, Lcom/squareup/ui/crm/edit/EditCustomerBootstrapScreen$doRegister$2;

    iget-object v2, p0, Lcom/squareup/ui/crm/edit/EditCustomerBootstrapScreen;->updateCustomerFlow:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;

    invoke-direct {v1, v2}, Lcom/squareup/ui/crm/edit/EditCustomerBootstrapScreen$doRegister$2;-><init>(Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    new-instance v2, Lcom/squareup/ui/crm/edit/EditCustomerBootstrapScreen$sam$io_reactivex_functions_Consumer$0;

    invoke-direct {v2, v1}, Lcom/squareup/ui/crm/edit/EditCustomerBootstrapScreen$sam$io_reactivex_functions_Consumer$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v2, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    .line 29
    iget-object v0, p0, Lcom/squareup/ui/crm/edit/EditCustomerBootstrapScreen;->props:Lcom/squareup/ui/crm/edit/EditCustomerProps;

    invoke-interface {p1, v0}, Lcom/squareup/ui/crm/edit/EditCustomerWorkflowRunner;->start(Lcom/squareup/ui/crm/edit/EditCustomerProps;)V

    return-void
.end method

.method public getParentKey()Ljava/lang/Object;
    .locals 1

    .line 16
    sget-object v0, Lcom/squareup/ui/crm/edit/EditCustomerScope;->INSTANCE:Lcom/squareup/ui/crm/edit/EditCustomerScope;

    return-object v0
.end method
