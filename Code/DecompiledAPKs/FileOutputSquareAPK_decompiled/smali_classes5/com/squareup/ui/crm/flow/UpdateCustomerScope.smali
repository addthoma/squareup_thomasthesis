.class public Lcom/squareup/ui/crm/flow/UpdateCustomerScope;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "UpdateCustomerScope.java"

# interfaces
.implements Lcom/squareup/container/RegistersInScope;
.implements Lcom/squareup/ui/buyer/PaymentExempt;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/crm/flow/UpdateCustomerScope$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/flow/UpdateCustomerScope$Component;,
        Lcom/squareup/ui/crm/flow/UpdateCustomerScope$Module;,
        Lcom/squareup/ui/crm/flow/UpdateCustomerScope$ParentComponent;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/crm/flow/UpdateCustomerScope;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final baseContact:Lcom/squareup/protos/client/rolodex/Contact;

.field public final contactValidationType:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;

.field public final crmScopeType:Lcom/squareup/ui/crm/flow/CrmScopeType;

.field public final parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

.field public final type:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;

.field public final updateCustomerResultKey:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 129
    sget-object v0, Lcom/squareup/ui/crm/flow/-$$Lambda$UpdateCustomerScope$BbGfUuCWgClXAaJI-Q2mIiYZfqA;->INSTANCE:Lcom/squareup/ui/crm/flow/-$$Lambda$UpdateCustomerScope$BbGfUuCWgClXAaJI-Q2mIiYZfqA;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/crm/flow/UpdateCustomerScope;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;Lcom/squareup/ui/crm/flow/CrmScopeType;Lcom/squareup/protos/client/rolodex/Contact;)V
    .locals 0

    .line 53
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    .line 55
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/UpdateCustomerScope;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    .line 56
    iput-object p2, p0, Lcom/squareup/ui/crm/flow/UpdateCustomerScope;->updateCustomerResultKey:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;

    .line 57
    iput-object p3, p0, Lcom/squareup/ui/crm/flow/UpdateCustomerScope;->type:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;

    .line 58
    iput-object p4, p0, Lcom/squareup/ui/crm/flow/UpdateCustomerScope;->contactValidationType:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;

    .line 59
    iput-object p5, p0, Lcom/squareup/ui/crm/flow/UpdateCustomerScope;->crmScopeType:Lcom/squareup/ui/crm/flow/CrmScopeType;

    .line 60
    iput-object p6, p0, Lcom/squareup/ui/crm/flow/UpdateCustomerScope;->baseContact:Lcom/squareup/protos/client/rolodex/Contact;

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/crm/flow/UpdateCustomerScope;
    .locals 8

    .line 130
    const-class v0, Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/ui/main/RegisterTreeKey;

    .line 132
    invoke-static {}, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;->values()[Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;

    move-result-object v0

    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v3, v0, v1

    .line 133
    invoke-static {}, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;->values()[Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;

    move-result-object v0

    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v4, v0, v1

    .line 134
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-static {v0}, Lcom/squareup/updatecustomerapi/UpdateCustomerFlowKt;->toContactValidationType(I)Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;

    move-result-object v5

    .line 135
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result p0

    .line 137
    new-instance v0, Lcom/squareup/ui/crm/flow/UpdateCustomerScope;

    .line 142
    invoke-static {p0}, Lcom/squareup/ui/crm/flow/CrmScopeTypeKt;->toCrmScopeType(I)Lcom/squareup/ui/crm/flow/CrmScopeType;

    move-result-object v6

    new-instance p0, Lcom/squareup/protos/client/rolodex/Contact$Builder;

    invoke-direct {p0}, Lcom/squareup/protos/client/rolodex/Contact$Builder;-><init>()V

    .line 144
    invoke-virtual {p0}, Lcom/squareup/protos/client/rolodex/Contact$Builder;->build()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v7

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/ui/crm/flow/UpdateCustomerScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;Lcom/squareup/ui/crm/flow/CrmScopeType;Lcom/squareup/protos/client/rolodex/Contact;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 120
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/main/RegisterTreeKey;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 122
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/UpdateCustomerScope;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 123
    iget-object p2, p0, Lcom/squareup/ui/crm/flow/UpdateCustomerScope;->updateCustomerResultKey:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;

    invoke-virtual {p2}, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;->ordinal()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 124
    iget-object p2, p0, Lcom/squareup/ui/crm/flow/UpdateCustomerScope;->type:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;

    invoke-virtual {p2}, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;->ordinal()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 125
    iget-object p2, p0, Lcom/squareup/ui/crm/flow/UpdateCustomerScope;->contactValidationType:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;

    invoke-static {p2}, Lcom/squareup/updatecustomerapi/UpdateCustomerFlowKt;->toInt(Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;)I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 126
    iget-object p2, p0, Lcom/squareup/ui/crm/flow/UpdateCustomerScope;->crmScopeType:Lcom/squareup/ui/crm/flow/CrmScopeType;

    invoke-static {p2}, Lcom/squareup/ui/crm/flow/CrmScopeTypeKt;->toInt(Lcom/squareup/ui/crm/flow/CrmScopeType;)I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method

.method public getParentKey()Ljava/lang/Object;
    .locals 1

    .line 64
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/UpdateCustomerScope;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    return-object v0
.end method

.method public register(Lmortar/MortarScope;)V
    .locals 1

    .line 115
    const-class v0, Lcom/squareup/ui/crm/flow/UpdateCustomerScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/flow/UpdateCustomerScope$Component;

    .line 116
    invoke-static {p1}, Lmortar/bundler/BundleService;->getBundleService(Lmortar/MortarScope;)Lmortar/bundler/BundleService;

    move-result-object p1

    invoke-interface {v0}, Lcom/squareup/ui/crm/flow/UpdateCustomerScope$Component;->runner()Lcom/squareup/ui/crm/v2/flow/UpdateCustomerScopeRunner;

    move-result-object v0

    invoke-virtual {p1, v0}, Lmortar/bundler/BundleService;->register(Lmortar/bundler/Bundler;)V

    return-void
.end method
