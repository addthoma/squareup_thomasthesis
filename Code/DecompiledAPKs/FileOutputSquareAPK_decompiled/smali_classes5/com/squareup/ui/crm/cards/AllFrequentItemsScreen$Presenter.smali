.class Lcom/squareup/ui/crm/cards/AllFrequentItemsScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "AllFrequentItemsScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/AllFrequentItemsScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/crm/cards/AllFrequentItemsView;",
        ">;"
    }
.end annotation


# instance fields
.field private final formatter:Lcom/squareup/ui/crm/cards/FrequentItemSubtitleFormatter;

.field private final res:Lcom/squareup/util/Res;

.field private final runner:Lcom/squareup/ui/crm/cards/AllFrequentItemsScreen$Runner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/cards/AllFrequentItemsScreen$Runner;Lcom/squareup/util/Res;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 55
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 56
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/AllFrequentItemsScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/AllFrequentItemsScreen$Runner;

    .line 57
    iput-object p2, p0, Lcom/squareup/ui/crm/cards/AllFrequentItemsScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 58
    new-instance p1, Lcom/squareup/ui/crm/cards/FrequentItemSubtitleFormatter;

    invoke-direct {p1, p2}, Lcom/squareup/ui/crm/cards/FrequentItemSubtitleFormatter;-><init>(Lcom/squareup/util/Res;)V

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/AllFrequentItemsScreen$Presenter;->formatter:Lcom/squareup/ui/crm/cards/FrequentItemSubtitleFormatter;

    return-void
.end method

.method private bindRow(Lcom/squareup/ui/account/view/SmartLineRow;Lcom/squareup/protos/client/rolodex/FrequentItem;)V
    .locals 1

    .line 86
    iget-object v0, p2, Lcom/squareup/protos/client/rolodex/FrequentItem;->display_name:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleText(Ljava/lang/CharSequence;)V

    .line 88
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/AllFrequentItemsScreen$Presenter;->formatter:Lcom/squareup/ui/crm/cards/FrequentItemSubtitleFormatter;

    invoke-virtual {v0, p2}, Lcom/squareup/ui/crm/cards/FrequentItemSubtitleFormatter;->getFrequentItemText(Lcom/squareup/protos/client/rolodex/FrequentItem;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/account/view/SmartLineRow;->setSubtitleText(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method protected onLoad(Landroid/os/Bundle;)V
    .locals 3

    .line 62
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 63
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/AllFrequentItemsScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/cards/AllFrequentItemsView;

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/AllFrequentItemsScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/crmscreens/R$string;->crm_frequent_items_heading:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 66
    new-instance v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 67
    invoke-virtual {v1, v2, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/AllFrequentItemsScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/AllFrequentItemsScreen$Runner;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/crm/cards/-$$Lambda$GN3JML5vJHh-_H-xs3A9vJDLfN4;

    invoke-direct {v2, v1}, Lcom/squareup/ui/crm/cards/-$$Lambda$GN3JML5vJHh-_H-xs3A9vJDLfN4;-><init>(Lcom/squareup/ui/crm/cards/AllFrequentItemsScreen$Runner;)V

    .line 68
    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 69
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->hidePrimaryButton()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 70
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    .line 66
    invoke-virtual {p1, v0}, Lcom/squareup/ui/crm/cards/AllFrequentItemsView;->setActionBarConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 72
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/AllFrequentItemsScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/AllFrequentItemsScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/cards/AllFrequentItemsScreen$Runner;->getContactForAllFrequentItemsScreen()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/squareup/ui/crm/cards/AllFrequentItemsScreen$Presenter;->setContact(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/ui/crm/cards/AllFrequentItemsView;)V

    return-void
.end method

.method setContact(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/ui/crm/cards/AllFrequentItemsView;)V
    .locals 3

    .line 76
    invoke-virtual {p2}, Lcom/squareup/ui/crm/cards/AllFrequentItemsView;->clearRows()V

    .line 77
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Contact;->frequent_items:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/rolodex/FrequentItem;

    .line 78
    invoke-virtual {p2}, Lcom/squareup/ui/crm/cards/AllFrequentItemsView;->addRow()Lcom/squareup/ui/account/view/SmartLineRow;

    move-result-object v1

    .line 79
    sget-object v2, Lcom/squareup/marin/widgets/ChevronVisibility;->GONE:Lcom/squareup/marin/widgets/ChevronVisibility;

    invoke-virtual {v1, v2}, Lcom/squareup/ui/account/view/SmartLineRow;->setChevronVisibility(Lcom/squareup/marin/widgets/ChevronVisibility;)V

    const/4 v2, 0x1

    .line 80
    invoke-virtual {v1, v2}, Lcom/squareup/ui/account/view/SmartLineRow;->setSubtitleVisible(Z)V

    .line 81
    invoke-direct {p0, v1, v0}, Lcom/squareup/ui/crm/cards/AllFrequentItemsScreen$Presenter;->bindRow(Lcom/squareup/ui/account/view/SmartLineRow;Lcom/squareup/protos/client/rolodex/FrequentItem;)V

    goto :goto_0

    :cond_0
    return-void
.end method
