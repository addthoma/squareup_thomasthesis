.class public Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionView;
.super Landroid/widget/LinearLayout;
.source "ProfileAttachmentsSectionView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionView$Component;
    }
.end annotation


# instance fields
.field private final header:Lcom/squareup/ui/crm/rows/ProfileSectionHeader;

.field private final onUploadFilesClicked:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onViewAllClicked:Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field presenter:Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final rows:Landroid/widget/LinearLayout;

.field private final uploadFiles:Landroid/widget/TextView;

.field private final viewAll:Landroid/widget/Button;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 39
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    const-class p2, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionView$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionView$Component;

    invoke-interface {p2, p0}, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionView$Component;->inject(Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionView;)V

    .line 42
    sget p2, Lcom/squareup/crmviewcustomer/R$layout;->crm_v2_profile_rows_and_button_section:I

    invoke-static {p1, p2, p0}, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    const/4 p1, 0x1

    .line 43
    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionView;->setOrientation(I)V

    .line 45
    sget p1, Lcom/squareup/crmviewcustomer/R$id;->profile_section_header:I

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionView;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/rows/ProfileSectionHeader;

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionView;->header:Lcom/squareup/ui/crm/rows/ProfileSectionHeader;

    .line 46
    sget p1, Lcom/squareup/crmviewcustomer/R$id;->profile_section_rows:I

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionView;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionView;->rows:Landroid/widget/LinearLayout;

    .line 47
    sget p1, Lcom/squareup/crm/R$id;->crm_profile_section_header_action:I

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionView;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionView;->uploadFiles:Landroid/widget/TextView;

    .line 48
    sget p1, Lcom/squareup/crmviewcustomer/R$id;->view_all_button:I

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionView;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionView;->viewAll:Landroid/widget/Button;

    .line 50
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionView;->uploadFiles:Landroid/widget/TextView;

    invoke-static {p1}, Lcom/squareup/util/RxViews;->debouncedOnClicked(Landroid/view/View;)Lrx/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionView;->onUploadFilesClicked:Lrx/Observable;

    .line 51
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionView;->viewAll:Landroid/widget/Button;

    invoke-static {p1}, Lcom/squareup/util/RxViews;->debouncedOnClicked(Landroid/view/View;)Lrx/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionView;->onViewAllClicked:Lrx/Observable;

    .line 53
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionView;->header:Lcom/squareup/ui/crm/rows/ProfileSectionHeader;

    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionView;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    sget v0, Lcom/squareup/crmviewcustomer/R$string;->crm_profile_attachments_header_uppercase:I

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/rows/ProfileSectionHeader;->setTitle(Ljava/lang/String;)V

    .line 55
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionView;->header:Lcom/squareup/ui/crm/rows/ProfileSectionHeader;

    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionView;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    sget v0, Lcom/squareup/crmviewcustomer/R$string;->crm_profile_attachments_add_files:I

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    sget-object v0, Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;->ENABLED:Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;

    invoke-virtual {p1, p2, v0}, Lcom/squareup/ui/crm/rows/ProfileSectionHeader;->setActionButton(Ljava/lang/String;Lcom/squareup/ui/crm/rows/ProfileSectionHeader$ActionButtonState;)V

    return-void
.end method


# virtual methods
.method addRow()Lcom/squareup/ui/account/view/SmartLineRow;
    .locals 3

    .line 82
    sget v0, Lcom/squareup/crmviewcustomer/R$layout;->crm_v2_smartlinerow_list_row:I

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionView;->rows:Landroid/widget/LinearLayout;

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/account/view/SmartLineRow;

    .line 83
    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionView;->rows:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 84
    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionView;->header:Lcom/squareup/ui/crm/rows/ProfileSectionHeader;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/squareup/ui/crm/rows/ProfileSectionHeader;->setDividerVisible(Z)V

    return-object v0
.end method

.method public clearRows()V
    .locals 2

    .line 89
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionView;->rows:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 90
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionView;->header:Lcom/squareup/ui/crm/rows/ProfileSectionHeader;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/ui/crm/rows/ProfileSectionHeader;->setDividerVisible(Z)V

    return-void
.end method

.method public getOnUploadFilesClicked()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionView;->onUploadFilesClicked:Lrx/Observable;

    return-object v0
.end method

.method public getOnViewAllClicked()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionView;->onViewAllClicked:Lrx/Observable;

    return-object v0
.end method

.method public hideUpload()V
    .locals 2

    .line 102
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionView;->uploadFiles:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 60
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionView;->presenter:Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionView;->presenter:Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter;->dropView(Ljava/lang/Object;)V

    .line 66
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method public setContact(Lcom/squareup/protos/client/rolodex/Contact;)V
    .locals 1

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionView;->presenter:Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionPresenter;->setContact(Lcom/squareup/protos/client/rolodex/Contact;)V

    return-void
.end method

.method public setUploadEnabled(Z)V
    .locals 1

    .line 106
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionView;->uploadFiles:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    return-void
.end method

.method public setVisible(Z)V
    .locals 0

    .line 94
    invoke-static {p0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method public showViewAll()V
    .locals 2

    .line 98
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ProfileAttachmentsSectionView;->viewAll:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    return-void
.end method
