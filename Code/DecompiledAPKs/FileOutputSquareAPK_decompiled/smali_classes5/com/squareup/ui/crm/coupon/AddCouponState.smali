.class public Lcom/squareup/ui/crm/coupon/AddCouponState;
.super Ljava/lang/Object;
.source "AddCouponState.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/coupon/AddCouponState$SharedScope;
    }
.end annotation


# static fields
.field public static final NO_DISCOUNT:Lcom/squareup/api/items/Discount;


# instance fields
.field private final discount:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/api/items/Discount;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 28
    new-instance v0, Lcom/squareup/api/items/Discount$Builder;

    invoke-direct {v0}, Lcom/squareup/api/items/Discount$Builder;-><init>()V

    invoke-virtual {v0}, Lcom/squareup/api/items/Discount$Builder;->build()Lcom/squareup/api/items/Discount;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/crm/coupon/AddCouponState;->NO_DISCOUNT:Lcom/squareup/api/items/Discount;

    return-void
.end method

.method constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    sget-object v0, Lcom/squareup/ui/crm/coupon/AddCouponState;->NO_DISCOUNT:Lcom/squareup/api/items/Discount;

    invoke-static {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/coupon/AddCouponState;->discount:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-void
.end method


# virtual methods
.method public discount()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/api/items/Discount;",
            ">;"
        }
    .end annotation

    .line 40
    iget-object v0, p0, Lcom/squareup/ui/crm/coupon/AddCouponState;->discount:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object v0
.end method

.method public setDiscount(Lcom/squareup/api/items/Discount;)V
    .locals 1

    const-string v0, "Discount cannot be null. Set with NO_DISCOUNT instead."

    .line 35
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    iget-object v0, p0, Lcom/squareup/ui/crm/coupon/AddCouponState;->discount:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method
