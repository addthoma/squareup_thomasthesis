.class public interface abstract Lcom/squareup/ui/crm/MaybeContactAddressBookEnabled;
.super Ljava/lang/Object;
.source "MaybeContactAddressBookEnabled.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/MaybeContactAddressBookEnabled$NoContactAddressBookModule;,
        Lcom/squareup/ui/crm/MaybeContactAddressBookEnabled$ContactAddressBookEnabledModule;,
        Lcom/squareup/ui/crm/MaybeContactAddressBookEnabled$NoContactAddressBook;,
        Lcom/squareup/ui/crm/MaybeContactAddressBookEnabled$ContactAddressBookEnabled;
    }
.end annotation


# virtual methods
.method public abstract isEnabled()Z
.end method
