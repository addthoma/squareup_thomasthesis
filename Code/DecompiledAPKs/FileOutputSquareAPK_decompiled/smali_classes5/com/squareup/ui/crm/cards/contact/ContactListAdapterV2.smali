.class Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2;
.super Landroid/widget/BaseAdapter;
.source "ContactListAdapterV2.java"

# interfaces
.implements Lcom/squareup/stickylistheaders/StickyListHeadersAdapter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2$RowType;
    }
.end annotation


# static fields
.field private static final NO_HEADER:I = -0x1

.field static final NUMBER_OF_BOTTOM_ROWS:I = 0x1

.field static final NUMBER_OF_TOP_ROWS:I = 0x2


# instance fields
.field private bottomRowLayout:Ljava/lang/Integer;

.field private final context:Landroid/content/Context;

.field private onBottomRowInflated:Lrx/functions/Action1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/functions/Action1<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private onTop2RowInflated:Lrx/functions/Action1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/functions/Action1<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private onTopRowInflated:Lrx/functions/Action1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/functions/Action1<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private final presenter:Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;

.field private top2RowLayout:Ljava/lang/Integer;

.field private topRowLayout:Ljava/lang/Integer;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;)V
    .locals 0

    .line 41
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2;->context:Landroid/content/Context;

    .line 43
    iput-object p2, p0, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2;->presenter:Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;

    return-void
.end method

.method private getRowType(I)Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2$RowType;
    .locals 2

    if-nez p1, :cond_0

    .line 200
    sget-object p1, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2$RowType;->TOP_ROW:Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2$RowType;

    return-object p1

    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 202
    sget-object p1, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2$RowType;->TOP2_ROW:Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2$RowType;

    return-object p1

    .line 203
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2;->getCount()I

    move-result v1

    sub-int/2addr v1, v0

    if-ge p1, v1, :cond_2

    .line 204
    sget-object p1, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2$RowType;->CONTACT:Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2$RowType;

    return-object p1

    .line 206
    :cond_2
    sget-object p1, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2$RowType;->BOTTOM_ROW:Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2$RowType;

    return-object p1
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .line 83
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2;->presenter:Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->getContactCount()I

    move-result v0

    add-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public getHeaderId(I)J
    .locals 5

    .line 66
    sget-object v0, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2$1;->$SwitchMap$com$squareup$ui$crm$cards$contact$ContactListAdapterV2$RowType:[I

    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2;->getRowType(I)Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2$RowType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2$RowType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    const/4 v2, 0x2

    if-eq v0, v1, :cond_2

    const-wide/16 v3, -0x1

    if-eq v0, v2, :cond_1

    const/4 p1, 0x3

    if-eq v0, p1, :cond_1

    const/4 p1, 0x4

    if-ne v0, p1, :cond_0

    return-wide v3

    .line 78
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Unexpected row type"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    return-wide v3

    .line 72
    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2;->presenter:Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;

    sub-int/2addr p1, v2

    invoke-virtual {v0, p1}, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->getHeaderId(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public getHeaderView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .line 47
    sget-object v0, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2$1;->$SwitchMap$com$squareup$ui$crm$cards$contact$ContactListAdapterV2$RowType:[I

    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2;->getRowType(I)Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2$RowType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2$RowType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    if-eqz p2, :cond_0

    goto :goto_0

    .line 49
    :cond_0
    sget p2, Lcom/squareup/crm/R$layout;->crm_v2_list_header_row:I

    .line 51
    invoke-static {p2, p3}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 53
    :goto_0
    iget-object p3, p0, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2;->presenter:Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;

    add-int/lit8 p1, p1, -0x2

    invoke-virtual {p3, p2, p1}, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->bindHeader(Landroid/view/View;I)V

    return-object p2

    .line 57
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Unexpected row type"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 0

    .line 99
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2;->getRowType(I)Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2$RowType;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2$RowType;->ordinal()I

    move-result p1

    return p1
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    if-eqz p2, :cond_0

    .line 105
    invoke-static {p2}, Lcom/squareup/util/RxViews;->unsubscribeOnDetachNow(Landroid/view/View;)V

    .line 111
    :cond_0
    sget-object v0, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2$1;->$SwitchMap$com$squareup$ui$crm$cards$contact$ContactListAdapterV2$RowType:[I

    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2;->getRowType(I)Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2$RowType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2$RowType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    const/4 v2, 0x2

    if-eq v0, v1, :cond_a

    if-eq v0, v2, :cond_7

    const/4 p1, 0x3

    if-eq v0, p1, :cond_4

    const/4 p1, 0x4

    if-ne v0, p1, :cond_3

    .line 141
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2;->bottomRowLayout:Ljava/lang/Integer;

    if-eqz p1, :cond_2

    .line 142
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-static {p1, p3}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    .line 143
    iget-object p2, p0, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2;->onBottomRowInflated:Lrx/functions/Action1;

    if-eqz p2, :cond_1

    .line 144
    invoke-interface {p2, p1}, Lrx/functions/Action1;->call(Ljava/lang/Object;)V

    :cond_1
    return-object p1

    .line 148
    :cond_2
    new-instance p1, Landroid/view/View;

    iget-object p2, p0, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2;->context:Landroid/content/Context;

    invoke-direct {p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    return-object p1

    .line 151
    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Unexpected row type"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 123
    :cond_4
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2;->top2RowLayout:Ljava/lang/Integer;

    if-eqz p1, :cond_6

    .line 124
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-static {p1, p3}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    .line 125
    iget-object p2, p0, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2;->onTop2RowInflated:Lrx/functions/Action1;

    if-eqz p2, :cond_5

    .line 126
    invoke-interface {p2, p1}, Lrx/functions/Action1;->call(Ljava/lang/Object;)V

    :cond_5
    return-object p1

    .line 130
    :cond_6
    new-instance p1, Landroid/view/View;

    iget-object p2, p0, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2;->context:Landroid/content/Context;

    invoke-direct {p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    return-object p1

    .line 113
    :cond_7
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2;->topRowLayout:Ljava/lang/Integer;

    if-eqz p1, :cond_9

    .line 114
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-static {p1, p3}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    .line 115
    iget-object p2, p0, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2;->onTopRowInflated:Lrx/functions/Action1;

    if-eqz p2, :cond_8

    .line 116
    invoke-interface {p2, p1}, Lrx/functions/Action1;->call(Ljava/lang/Object;)V

    :cond_8
    return-object p1

    .line 120
    :cond_9
    new-instance p1, Landroid/view/View;

    iget-object p2, p0, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2;->context:Landroid/content/Context;

    invoke-direct {p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    return-object p1

    :cond_a
    if-eqz p2, :cond_b

    .line 133
    check-cast p2, Lcom/squareup/ui/crm/rows/CustomerUnitRow;

    goto :goto_0

    :cond_b
    new-instance p2, Lcom/squareup/ui/crm/rows/CustomerUnitRow;

    iget-object p3, p0, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2;->context:Landroid/content/Context;

    invoke-direct {p2, p3}, Lcom/squareup/ui/crm/rows/CustomerUnitRow;-><init>(Landroid/content/Context;)V

    .line 136
    :goto_0
    iget-object p3, p0, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2;->presenter:Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;

    sub-int/2addr p1, v2

    invoke-virtual {p3, p2, p1}, Lcom/squareup/ui/crm/cards/contact/ContactListPresenterV2;->bindContact(Lcom/squareup/ui/crm/rows/CustomerUnitRow;I)V

    return-object p2
.end method

.method public getViewTypeCount()I
    .locals 1

    .line 95
    invoke-static {}, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2$RowType;->values()[Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2$RowType;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method hideBottomRow()Z
    .locals 1

    .line 190
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2;->bottomRowLayout:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 191
    iput-object v0, p0, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2;->bottomRowLayout:Ljava/lang/Integer;

    .line 192
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2;->notifyDataSetChanged()V

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method hideTopRow()V
    .locals 1

    .line 172
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2;->topRowLayout:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 173
    iput-object v0, p0, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2;->topRowLayout:Ljava/lang/Integer;

    .line 174
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public isEnabled(I)Z
    .locals 1

    .line 62
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2;->getRowType(I)Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2$RowType;

    move-result-object p1

    sget-object v0, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2$RowType;->CONTACT:Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2$RowType;

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method showBottomRow(ILrx/functions/Action1;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lrx/functions/Action1<",
            "Landroid/view/View;",
            ">;)Z"
        }
    .end annotation

    .line 180
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2;->bottomRowLayout:Ljava/lang/Integer;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 181
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2;->bottomRowLayout:Ljava/lang/Integer;

    .line 182
    iput-object p2, p0, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2;->onBottomRowInflated:Lrx/functions/Action1;

    .line 183
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2;->notifyDataSetChanged()V

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method showTop2Row(ILrx/functions/Action1;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lrx/functions/Action1<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .line 164
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2;->top2RowLayout:Ljava/lang/Integer;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 165
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2;->top2RowLayout:Ljava/lang/Integer;

    .line 166
    iput-object p2, p0, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2;->onTop2RowInflated:Lrx/functions/Action1;

    .line 167
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method showTopRow(ILrx/functions/Action1;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lrx/functions/Action1<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .line 156
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2;->topRowLayout:Ljava/lang/Integer;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 157
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2;->topRowLayout:Ljava/lang/Integer;

    .line 158
    iput-object p2, p0, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2;->onTopRowInflated:Lrx/functions/Action1;

    .line 159
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/contact/ContactListAdapterV2;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method
