.class public Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "AllAppointmentsCoordinator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator$Adapter;,
        Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator$Factory;
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private appointments:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel$CrmAppointmentRowViewModel;",
            ">;"
        }
    .end annotation
.end field

.field private appointmentsList:Landroidx/recyclerview/widget/RecyclerView;

.field private final appointmentsListAdapter:Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator$Adapter;

.field private final res:Lcom/squareup/util/Res;

.field private final runner:Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Runner;

.field private final type:Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Type;


# direct methods
.method private constructor <init>(Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Runner;Lcom/squareup/util/Res;Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Type;)V
    .locals 1

    .line 36
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    .line 30
    new-instance v0, Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator$Adapter;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator$Adapter;-><init>(Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator;)V

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator;->appointmentsListAdapter:Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator$Adapter;

    .line 34
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator;->appointments:Ljava/util/List;

    .line 37
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator;->runner:Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Runner;

    .line 38
    iput-object p2, p0, Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator;->res:Lcom/squareup/util/Res;

    .line 39
    iput-object p3, p0, Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator;->type:Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Type;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Runner;Lcom/squareup/util/Res;Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Type;Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator$1;)V
    .locals 0

    .line 25
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator;-><init>(Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Runner;Lcom/squareup/util/Res;Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Type;)V

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator;)Ljava/util/List;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator;->appointments:Ljava/util/List;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator;)Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Runner;
    .locals 0

    .line 25
    iget-object p0, p0, Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator;->runner:Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Runner;

    return-object p0
.end method

.method private bindViews(Landroid/view/View;)V
    .locals 1

    .line 104
    sget v0, Lcom/squareup/crmviewcustomer/R$id;->crm_appointments_list:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator;->appointmentsList:Landroidx/recyclerview/widget/RecyclerView;

    .line 105
    invoke-static {p1}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    return-void
.end method

.method private getActionBarConfig(Lcom/squareup/util/Res;)Lcom/squareup/marin/widgets/MarinActionBar$Config;
    .locals 4

    .line 85
    new-instance v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 88
    sget-object v1, Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator$1;->$SwitchMap$com$squareup$ui$crm$cards$AllAppointmentsScreen$Type:[I

    iget-object v2, p0, Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator;->type:Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Type;

    invoke-virtual {v2}, Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Type;->ordinal()I

    move-result v2

    aget v1, v1, v2

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    const/4 v3, 0x2

    if-eq v1, v3, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    .line 93
    :cond_0
    sget v1, Lcom/squareup/crmviewcustomer/R$string;->crm_all_upcoming_appointments_title:I

    goto :goto_0

    .line 90
    :cond_1
    sget v1, Lcom/squareup/crmviewcustomer/R$string;->crm_all_past_appointments_title:I

    .line 97
    :goto_0
    invoke-interface {p1, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonTextBackArrow(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 98
    invoke-virtual {p1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator;->runner:Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Runner;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/crm/cards/-$$Lambda$hp3Bs1KnkQohMFirAxue5oKPOAU;

    invoke-direct {v2, v1}, Lcom/squareup/ui/crm/cards/-$$Lambda$hp3Bs1KnkQohMFirAxue5oKPOAU;-><init>(Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Runner;)V

    .line 99
    invoke-virtual {p1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 100
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    .line 57
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 58
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator;->bindViews(Landroid/view/View;)V

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator;->res:Lcom/squareup/util/Res;

    invoke-direct {p0, v1}, Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator;->getActionBarConfig(Lcom/squareup/util/Res;)Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator;->appointmentsList:Landroidx/recyclerview/widget/RecyclerView;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator;->appointmentsListAdapter:Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator$Adapter;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 63
    sget-object v0, Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator$1;->$SwitchMap$com$squareup$ui$crm$cards$AllAppointmentsScreen$Type:[I

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator;->type:Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Type;

    invoke-virtual {v1}, Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    goto :goto_0

    .line 73
    :cond_0
    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$AllAppointmentsCoordinator$kkYVNK2xlzCG7miBNi4bY5MpRLw;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$AllAppointmentsCoordinator$kkYVNK2xlzCG7miBNi4bY5MpRLw;-><init>(Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    goto :goto_0

    .line 65
    :cond_1
    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$AllAppointmentsCoordinator$-FWQYPsUxTNPyLTBVXQNqfC-FJ8;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$AllAppointmentsCoordinator$-FWQYPsUxTNPyLTBVXQNqfC-FJ8;-><init>(Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    :goto_0
    return-void
.end method

.method public synthetic lambda$attach$1$AllAppointmentsCoordinator()Lrx/Subscription;
    .locals 2

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator;->runner:Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Runner;->pastAppointments()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$AllAppointmentsCoordinator$3mse-ZF9bf3189sB9dex8vO11Ic;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$AllAppointmentsCoordinator$3mse-ZF9bf3189sB9dex8vO11Ic;-><init>(Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$attach$3$AllAppointmentsCoordinator()Lrx/Subscription;
    .locals 2

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator;->runner:Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Runner;->upcomingAppointments()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$AllAppointmentsCoordinator$N6mGiFuIUcTqNgnzFWU6Z8odUSo;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$AllAppointmentsCoordinator$N6mGiFuIUcTqNgnzFWU6Z8odUSo;-><init>(Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$null$0$AllAppointmentsCoordinator(Ljava/util/List;)V
    .locals 0

    .line 66
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 68
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator;->appointments:Ljava/util/List;

    .line 69
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator;->appointmentsListAdapter:Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator$Adapter;

    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator$Adapter;->notifyDataSetChanged()V

    return-void
.end method

.method public synthetic lambda$null$2$AllAppointmentsCoordinator(Ljava/util/List;)V
    .locals 0

    .line 75
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 77
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator;->appointments:Ljava/util/List;

    .line 78
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator;->appointmentsListAdapter:Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator$Adapter;

    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator$Adapter;->notifyDataSetChanged()V

    return-void
.end method
