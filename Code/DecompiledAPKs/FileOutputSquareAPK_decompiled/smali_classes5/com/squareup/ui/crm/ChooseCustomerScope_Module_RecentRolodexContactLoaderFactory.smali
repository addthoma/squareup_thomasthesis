.class public final Lcom/squareup/ui/crm/ChooseCustomerScope_Module_RecentRolodexContactLoaderFactory;
.super Ljava/lang/Object;
.source "ChooseCustomerScope_Module_RecentRolodexContactLoaderFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/crm/RolodexContactLoader;",
        ">;"
    }
.end annotation


# instance fields
.field private final connectivityMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final module:Lcom/squareup/ui/crm/ChooseCustomerScope$Module;

.field private final rolodexProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexServiceHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final threadEnforcerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/ui/crm/ChooseCustomerScope$Module;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/crm/ChooseCustomerScope$Module;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexServiceHelper;",
            ">;)V"
        }
    .end annotation

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/squareup/ui/crm/ChooseCustomerScope_Module_RecentRolodexContactLoaderFactory;->module:Lcom/squareup/ui/crm/ChooseCustomerScope$Module;

    .line 37
    iput-object p2, p0, Lcom/squareup/ui/crm/ChooseCustomerScope_Module_RecentRolodexContactLoaderFactory;->connectivityMonitorProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p3, p0, Lcom/squareup/ui/crm/ChooseCustomerScope_Module_RecentRolodexContactLoaderFactory;->mainSchedulerProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p4, p0, Lcom/squareup/ui/crm/ChooseCustomerScope_Module_RecentRolodexContactLoaderFactory;->threadEnforcerProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p5, p0, Lcom/squareup/ui/crm/ChooseCustomerScope_Module_RecentRolodexContactLoaderFactory;->rolodexProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Lcom/squareup/ui/crm/ChooseCustomerScope$Module;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/crm/ChooseCustomerScope_Module_RecentRolodexContactLoaderFactory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/crm/ChooseCustomerScope$Module;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lrx/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexServiceHelper;",
            ">;)",
            "Lcom/squareup/ui/crm/ChooseCustomerScope_Module_RecentRolodexContactLoaderFactory;"
        }
    .end annotation

    .line 52
    new-instance v6, Lcom/squareup/ui/crm/ChooseCustomerScope_Module_RecentRolodexContactLoaderFactory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/crm/ChooseCustomerScope_Module_RecentRolodexContactLoaderFactory;-><init>(Lcom/squareup/ui/crm/ChooseCustomerScope$Module;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static recentRolodexContactLoader(Lcom/squareup/ui/crm/ChooseCustomerScope$Module;Lcom/squareup/connectivity/ConnectivityMonitor;Lrx/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/crm/RolodexServiceHelper;)Lcom/squareup/crm/RolodexContactLoader;
    .locals 0

    .line 58
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/ui/crm/ChooseCustomerScope$Module;->recentRolodexContactLoader(Lcom/squareup/connectivity/ConnectivityMonitor;Lrx/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/crm/RolodexServiceHelper;)Lcom/squareup/crm/RolodexContactLoader;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/crm/RolodexContactLoader;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/crm/RolodexContactLoader;
    .locals 5

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerScope_Module_RecentRolodexContactLoaderFactory;->module:Lcom/squareup/ui/crm/ChooseCustomerScope$Module;

    iget-object v1, p0, Lcom/squareup/ui/crm/ChooseCustomerScope_Module_RecentRolodexContactLoaderFactory;->connectivityMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/connectivity/ConnectivityMonitor;

    iget-object v2, p0, Lcom/squareup/ui/crm/ChooseCustomerScope_Module_RecentRolodexContactLoaderFactory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lrx/Scheduler;

    iget-object v3, p0, Lcom/squareup/ui/crm/ChooseCustomerScope_Module_RecentRolodexContactLoaderFactory;->threadEnforcerProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/thread/enforcer/ThreadEnforcer;

    iget-object v4, p0, Lcom/squareup/ui/crm/ChooseCustomerScope_Module_RecentRolodexContactLoaderFactory;->rolodexProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/crm/RolodexServiceHelper;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/ui/crm/ChooseCustomerScope_Module_RecentRolodexContactLoaderFactory;->recentRolodexContactLoader(Lcom/squareup/ui/crm/ChooseCustomerScope$Module;Lcom/squareup/connectivity/ConnectivityMonitor;Lrx/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Lcom/squareup/crm/RolodexServiceHelper;)Lcom/squareup/crm/RolodexContactLoader;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/ui/crm/ChooseCustomerScope_Module_RecentRolodexContactLoaderFactory;->get()Lcom/squareup/crm/RolodexContactLoader;

    move-result-object v0

    return-object v0
.end method
