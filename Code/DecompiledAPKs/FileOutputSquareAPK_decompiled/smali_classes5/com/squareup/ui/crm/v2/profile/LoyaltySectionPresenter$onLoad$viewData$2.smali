.class final Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter$onLoad$viewData$2;
.super Ljava/lang/Object;
.source "LoyaltySectionPresenter.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;->onLoad(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\"\u0010\u0002\u001a\u001e\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00060\u00060\u0003H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData;",
        "<name for destructuring parameter 0>",
        "Lkotlin/Pair;",
        "Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$ContactLoadingState;",
        "kotlin.jvm.PlatformType",
        "Lcom/squareup/loyalty/LoyaltyStatusResponse;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter$onLoad$viewData$2;->this$0:Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lkotlin/Pair;)Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "+",
            "Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$ContactLoadingState;",
            "+",
            "Lcom/squareup/loyalty/LoyaltyStatusResponse;",
            ">;)",
            "Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData;"
        }
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$ContactLoadingState;

    invoke-virtual {p1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/loyalty/LoyaltyStatusResponse;

    .line 74
    sget-object v1, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$ContactLoadingState;->VALID:Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$ContactLoadingState;

    if-ne v0, v1, :cond_0

    .line 75
    new-instance v0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData$ViewData;

    const-string v1, "status"

    .line 76
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter$onLoad$viewData$2;->this$0:Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;

    invoke-static {v1}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;->access$getLoyaltySettings$p(Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;)Lcom/squareup/loyalty/LoyaltySettings;

    move-result-object v1

    invoke-interface {v1}, Lcom/squareup/loyalty/LoyaltySettings;->isLoyaltyProgramActive()Z

    move-result v1

    .line 78
    iget-object v2, p0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter$onLoad$viewData$2;->this$0:Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;

    invoke-static {v2}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;->access$getLoyaltySettings$p(Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter;)Lcom/squareup/loyalty/LoyaltySettings;

    move-result-object v2

    invoke-interface {v2}, Lcom/squareup/loyalty/LoyaltySettings;->canRedeemPoints()Z

    move-result v2

    .line 75
    invoke-direct {v0, p1, v1, v2}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData$ViewData;-><init>(Lcom/squareup/loyalty/LoyaltyStatusResponse;ZZ)V

    check-cast v0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData;

    goto :goto_0

    .line 81
    :cond_0
    sget-object p1, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData$Loading;->INSTANCE:Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData$Loading;

    move-object v0, p1

    check-cast v0, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData;

    :goto_0
    return-object v0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 42
    check-cast p1, Lkotlin/Pair;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionPresenter$onLoad$viewData$2;->apply(Lkotlin/Pair;)Lcom/squareup/ui/crm/v2/profile/LoyaltySectionViewData;

    move-result-object p1

    return-object p1
.end method
