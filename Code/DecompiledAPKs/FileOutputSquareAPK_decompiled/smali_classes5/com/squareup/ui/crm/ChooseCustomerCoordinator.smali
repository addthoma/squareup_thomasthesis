.class public final Lcom/squareup/ui/crm/ChooseCustomerCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "ChooseCustomerCoordinator.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000`\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0017\u0008\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u000cH\u0016J\u0010\u0010\u0018\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u000cH\u0002J\u0015\u0010\u0019\u001a\u00020\u00162\u0006\u0010\u001a\u001a\u00020\u001bH\u0000\u00a2\u0006\u0002\u0008\u001cJ\u0015\u0010\u001d\u001a\u00020\u00162\u0006\u0010\u001e\u001a\u00020\u001fH\u0000\u00a2\u0006\u0002\u0008 J\u0010\u0010!\u001a\u00020\u00162\u0006\u0010\"\u001a\u00020#H\u0002R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006$"
    }
    d2 = {
        "Lcom/squareup/ui/crm/ChooseCustomerCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "runner",
        "Lcom/squareup/ui/crm/ChooseCustomerScreen$Runner;",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lcom/squareup/ui/crm/ChooseCustomerScreen$Runner;Lcom/squareup/util/Res;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/MarinActionBar;",
        "adapter",
        "Lcom/squareup/ui/crm/SimpleCustomerListAdapter;",
        "progressView",
        "Landroid/view/View;",
        "recyclerView",
        "Landroidx/recyclerview/widget/RecyclerView;",
        "searchBox",
        "Lcom/squareup/ui/XableEditText;",
        "searchMessageView",
        "Lcom/squareup/widgets/MessageView;",
        "shortAnimTimeMs",
        "",
        "attach",
        "",
        "view",
        "bindViews",
        "showMessage",
        "text",
        "",
        "showMessage$crm_choose_customer_release",
        "showProgress",
        "visible",
        "",
        "showProgress$crm_choose_customer_release",
        "updateVisibility",
        "state",
        "Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;",
        "crm-choose-customer_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private adapter:Lcom/squareup/ui/crm/SimpleCustomerListAdapter;

.field private progressView:Landroid/view/View;

.field private recyclerView:Landroidx/recyclerview/widget/RecyclerView;

.field private final res:Lcom/squareup/util/Res;

.field private final runner:Lcom/squareup/ui/crm/ChooseCustomerScreen$Runner;

.field private searchBox:Lcom/squareup/ui/XableEditText;

.field private searchMessageView:Lcom/squareup/widgets/MessageView;

.field private final shortAnimTimeMs:I


# direct methods
.method public constructor <init>(Lcom/squareup/ui/crm/ChooseCustomerScreen$Runner;Lcom/squareup/util/Res;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "runner"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/crm/ChooseCustomerCoordinator;->runner:Lcom/squareup/ui/crm/ChooseCustomerScreen$Runner;

    iput-object p2, p0, Lcom/squareup/ui/crm/ChooseCustomerCoordinator;->res:Lcom/squareup/util/Res;

    .line 50
    iget-object p1, p0, Lcom/squareup/ui/crm/ChooseCustomerCoordinator;->res:Lcom/squareup/util/Res;

    const/high16 p2, 0x10e0000

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getInteger(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/ui/crm/ChooseCustomerCoordinator;->shortAnimTimeMs:I

    .line 51
    new-instance p1, Lcom/squareup/ui/crm/SimpleCustomerListAdapter;

    invoke-direct {p1}, Lcom/squareup/ui/crm/SimpleCustomerListAdapter;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/crm/ChooseCustomerCoordinator;->adapter:Lcom/squareup/ui/crm/SimpleCustomerListAdapter;

    return-void
.end method

.method public static final synthetic access$getAdapter$p(Lcom/squareup/ui/crm/ChooseCustomerCoordinator;)Lcom/squareup/ui/crm/SimpleCustomerListAdapter;
    .locals 0

    .line 38
    iget-object p0, p0, Lcom/squareup/ui/crm/ChooseCustomerCoordinator;->adapter:Lcom/squareup/ui/crm/SimpleCustomerListAdapter;

    return-object p0
.end method

.method public static final synthetic access$getRecyclerView$p(Lcom/squareup/ui/crm/ChooseCustomerCoordinator;)Landroidx/recyclerview/widget/RecyclerView;
    .locals 1

    .line 38
    iget-object p0, p0, Lcom/squareup/ui/crm/ChooseCustomerCoordinator;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    if-nez p0, :cond_0

    const-string v0, "recyclerView"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getRunner$p(Lcom/squareup/ui/crm/ChooseCustomerCoordinator;)Lcom/squareup/ui/crm/ChooseCustomerScreen$Runner;
    .locals 0

    .line 38
    iget-object p0, p0, Lcom/squareup/ui/crm/ChooseCustomerCoordinator;->runner:Lcom/squareup/ui/crm/ChooseCustomerScreen$Runner;

    return-object p0
.end method

.method public static final synthetic access$getSearchBox$p(Lcom/squareup/ui/crm/ChooseCustomerCoordinator;)Lcom/squareup/ui/XableEditText;
    .locals 1

    .line 38
    iget-object p0, p0, Lcom/squareup/ui/crm/ChooseCustomerCoordinator;->searchBox:Lcom/squareup/ui/XableEditText;

    if-nez p0, :cond_0

    const-string v0, "searchBox"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$setAdapter$p(Lcom/squareup/ui/crm/ChooseCustomerCoordinator;Lcom/squareup/ui/crm/SimpleCustomerListAdapter;)V
    .locals 0

    .line 38
    iput-object p1, p0, Lcom/squareup/ui/crm/ChooseCustomerCoordinator;->adapter:Lcom/squareup/ui/crm/SimpleCustomerListAdapter;

    return-void
.end method

.method public static final synthetic access$setRecyclerView$p(Lcom/squareup/ui/crm/ChooseCustomerCoordinator;Landroidx/recyclerview/widget/RecyclerView;)V
    .locals 0

    .line 38
    iput-object p1, p0, Lcom/squareup/ui/crm/ChooseCustomerCoordinator;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    return-void
.end method

.method public static final synthetic access$setSearchBox$p(Lcom/squareup/ui/crm/ChooseCustomerCoordinator;Lcom/squareup/ui/XableEditText;)V
    .locals 0

    .line 38
    iput-object p1, p0, Lcom/squareup/ui/crm/ChooseCustomerCoordinator;->searchBox:Lcom/squareup/ui/XableEditText;

    return-void
.end method

.method public static final synthetic access$updateVisibility(Lcom/squareup/ui/crm/ChooseCustomerCoordinator;Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;)V
    .locals 0

    .line 38
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/ChooseCustomerCoordinator;->updateVisibility(Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 2

    .line 159
    invoke-static {p1}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    const-string v1, "ActionBarView.findIn(view)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 161
    sget v0, Lcom/squareup/crmchoosecustomer/R$id;->crm_choose_customer_progress_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerCoordinator;->progressView:Landroid/view/View;

    .line 162
    sget v0, Lcom/squareup/crmchoosecustomer/R$id;->crm_search_box:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/XableEditText;

    iput-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerCoordinator;->searchBox:Lcom/squareup/ui/XableEditText;

    .line 163
    sget v0, Lcom/squareup/crmchoosecustomer/R$id;->crm_choose_customer_search_message:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerCoordinator;->searchMessageView:Lcom/squareup/widgets/MessageView;

    .line 164
    sget v0, Lcom/squareup/crmchoosecustomer/R$id;->crm_choose_customer_recycler_view:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView;

    iput-object p1, p0, Lcom/squareup/ui/crm/ChooseCustomerCoordinator;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    return-void
.end method

.method private final updateVisibility(Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;)V
    .locals 5

    .line 114
    sget-object v0, Lcom/squareup/ui/crm/ChooseCustomerCoordinator$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const-string v1, ""

    const/4 v2, 0x1

    const-string v3, "recyclerView"

    const/4 v4, 0x0

    packed-switch v0, :pswitch_data_0

    .line 139
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected visual state "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;->name()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 134
    :pswitch_0
    invoke-virtual {p0, v4}, Lcom/squareup/ui/crm/ChooseCustomerCoordinator;->showProgress$crm_choose_customer_release(Z)V

    .line 135
    iget-object p1, p0, Lcom/squareup/ui/crm/ChooseCustomerCoordinator;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/crm/R$string;->crm_contact_search_empty:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/ChooseCustomerCoordinator;->showMessage$crm_choose_customer_release(Ljava/lang/String;)V

    .line 136
    iget-object p1, p0, Lcom/squareup/ui/crm/ChooseCustomerCoordinator;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    if-nez p1, :cond_0

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast p1, Landroid/view/View;

    invoke-static {p1, v2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    goto :goto_0

    .line 128
    :pswitch_1
    invoke-virtual {p0, v4}, Lcom/squareup/ui/crm/ChooseCustomerCoordinator;->showProgress$crm_choose_customer_release(Z)V

    .line 129
    invoke-virtual {p0, v1}, Lcom/squareup/ui/crm/ChooseCustomerCoordinator;->showMessage$crm_choose_customer_release(Ljava/lang/String;)V

    .line 130
    iget-object p1, p0, Lcom/squareup/ui/crm/ChooseCustomerCoordinator;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    if-nez p1, :cond_1

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast p1, Landroid/view/View;

    invoke-static {p1, v2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    goto :goto_0

    .line 122
    :pswitch_2
    invoke-virtual {p0, v4}, Lcom/squareup/ui/crm/ChooseCustomerCoordinator;->showProgress$crm_choose_customer_release(Z)V

    .line 123
    iget-object p1, p0, Lcom/squareup/ui/crm/ChooseCustomerCoordinator;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/crm/R$string;->crm_failed_to_load_customers:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/ChooseCustomerCoordinator;->showMessage$crm_choose_customer_release(Ljava/lang/String;)V

    .line 124
    iget-object p1, p0, Lcom/squareup/ui/crm/ChooseCustomerCoordinator;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    if-nez p1, :cond_2

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    check-cast p1, Landroid/view/View;

    invoke-static {p1, v4}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    goto :goto_0

    .line 116
    :pswitch_3
    invoke-virtual {p0, v2}, Lcom/squareup/ui/crm/ChooseCustomerCoordinator;->showProgress$crm_choose_customer_release(Z)V

    .line 117
    invoke-virtual {p0, v1}, Lcom/squareup/ui/crm/ChooseCustomerCoordinator;->showMessage$crm_choose_customer_release(Ljava/lang/String;)V

    .line 118
    iget-object p1, p0, Lcom/squareup/ui/crm/ChooseCustomerCoordinator;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    if-nez p1, :cond_3

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    check-cast p1, Landroid/view/View;

    invoke-static {p1, v4}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 5

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/ChooseCustomerCoordinator;->bindViews(Landroid/view/View;)V

    .line 56
    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    const-string v1, "actionBar"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    iget-object v2, p0, Lcom/squareup/ui/crm/ChooseCustomerCoordinator;->runner:Lcom/squareup/ui/crm/ChooseCustomerScreen$Runner;

    invoke-interface {v2}, Lcom/squareup/ui/crm/ChooseCustomerScreen$Runner;->actionBarGlyphIsX()Ljava/lang/Boolean;

    move-result-object v2

    const-string v3, "runner.actionBarGlyphIsX()"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    goto :goto_0

    :cond_1
    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    :goto_0
    iget-object v3, p0, Lcom/squareup/ui/crm/ChooseCustomerCoordinator;->runner:Lcom/squareup/ui/crm/ChooseCustomerScreen$Runner;

    invoke-interface {v3}, Lcom/squareup/ui/crm/ChooseCustomerScreen$Runner;->getTitle()Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v0, v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    new-instance v1, Lcom/squareup/ui/crm/ChooseCustomerCoordinator$attach$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/ChooseCustomerCoordinator$attach$1;-><init>(Lcom/squareup/ui/crm/ChooseCustomerCoordinator;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->showUpButton(Ljava/lang/Runnable;)V

    .line 58
    new-instance v0, Lcom/squareup/ui/crm/ChooseCustomerCoordinator$attach$2;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/ChooseCustomerCoordinator$attach$2;-><init>(Lcom/squareup/ui/crm/ChooseCustomerCoordinator;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerCoordinator;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    const-string v1, "recyclerView"

    if-nez v0, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    iget-object v2, p0, Lcom/squareup/ui/crm/ChooseCustomerCoordinator;->adapter:Lcom/squareup/ui/crm/SimpleCustomerListAdapter;

    check-cast v2, Landroidx/recyclerview/widget/RecyclerView$Adapter;

    invoke-virtual {v0, v2}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerCoordinator;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    if-nez v0, :cond_4

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    new-instance v2, Lcom/squareup/noho/NohoEdgeDecoration;

    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const-string/jumbo v4, "view.resources"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v2, v3}, Lcom/squareup/noho/NohoEdgeDecoration;-><init>(Landroid/content/res/Resources;)V

    check-cast v2, Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;

    invoke-virtual {v0, v2}, Landroidx/recyclerview/widget/RecyclerView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;)V

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerCoordinator;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    if-nez v0, :cond_5

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    new-instance v2, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    check-cast v2, Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    invoke-virtual {v0, v2}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerCoordinator;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    if-nez v0, :cond_6

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    new-instance v1, Lcom/squareup/ui/crm/ChooseCustomerCoordinator$attach$3;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/ChooseCustomerCoordinator$attach$3;-><init>(Lcom/squareup/ui/crm/ChooseCustomerCoordinator;)V

    check-cast v1, Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->addOnScrollListener(Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;)V

    .line 80
    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerCoordinator;->runner:Lcom/squareup/ui/crm/ChooseCustomerScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/ChooseCustomerScreen$Runner;->getScrollPosition()Lrx/Observable;

    move-result-object v0

    const-string v1, "runner.scrollPosition"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v1, Lcom/squareup/ui/crm/ChooseCustomerCoordinator$attach$4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/ChooseCustomerCoordinator$attach$4;-><init>(Lcom/squareup/ui/crm/ChooseCustomerCoordinator;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/ObservablesKt;->subscribeWith(Lrx/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lrx/Subscription;

    .line 83
    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerCoordinator;->runner:Lcom/squareup/ui/crm/ChooseCustomerScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/ChooseCustomerScreen$Runner;->getCustomerListItems()Lrx/Observable;

    move-result-object v0

    const-string v1, "runner.customerListItems"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v1, Lcom/squareup/ui/crm/ChooseCustomerCoordinator$attach$5;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/ChooseCustomerCoordinator$attach$5;-><init>(Lcom/squareup/ui/crm/ChooseCustomerCoordinator;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/ObservablesKt;->subscribeWith(Lrx/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lrx/Subscription;

    .line 86
    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerCoordinator;->runner:Lcom/squareup/ui/crm/ChooseCustomerScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/ChooseCustomerScreen$Runner;->getScreenLoadingState()Lrx/Observable;

    move-result-object v0

    const-string v1, "runner.screenLoadingState"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v1, Lcom/squareup/ui/crm/ChooseCustomerCoordinator$attach$6;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/ChooseCustomerCoordinator$attach$6;-><init>(Lcom/squareup/ui/crm/ChooseCustomerCoordinator;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/ObservablesKt;->subscribeWith(Lrx/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lrx/Subscription;

    .line 89
    iget-object p1, p0, Lcom/squareup/ui/crm/ChooseCustomerCoordinator;->searchBox:Lcom/squareup/ui/XableEditText;

    const-string v0, "searchBox"

    if-nez p1, :cond_7

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    iget-object v1, p0, Lcom/squareup/ui/crm/ChooseCustomerCoordinator;->runner:Lcom/squareup/ui/crm/ChooseCustomerScreen$Runner;

    invoke-interface {v1}, Lcom/squareup/ui/crm/ChooseCustomerScreen$Runner;->getSearchTerm()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {p1, v1}, Lcom/squareup/ui/XableEditText;->setText(Ljava/lang/CharSequence;)V

    .line 90
    iget-object p1, p0, Lcom/squareup/ui/crm/ChooseCustomerCoordinator;->searchBox:Lcom/squareup/ui/XableEditText;

    if-nez p1, :cond_8

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_8
    new-instance v1, Lcom/squareup/ui/crm/ChooseCustomerCoordinator$attach$7;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/ChooseCustomerCoordinator$attach$7;-><init>(Lcom/squareup/ui/crm/ChooseCustomerCoordinator;)V

    check-cast v1, Landroid/text/TextWatcher;

    invoke-virtual {p1, v1}, Lcom/squareup/ui/XableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 97
    iget-object p1, p0, Lcom/squareup/ui/crm/ChooseCustomerCoordinator;->searchBox:Lcom/squareup/ui/XableEditText;

    if-nez p1, :cond_9

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_9
    new-instance v0, Lcom/squareup/ui/crm/ChooseCustomerCoordinator$attach$8;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/ChooseCustomerCoordinator$attach$8;-><init>(Lcom/squareup/ui/crm/ChooseCustomerCoordinator;)V

    check-cast v0, Lcom/squareup/debounce/DebouncedOnEditorActionListener;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/XableEditText;->setOnEditorActionListener(Lcom/squareup/debounce/DebouncedOnEditorActionListener;)V

    return-void
.end method

.method public final showMessage$crm_choose_customer_release(Ljava/lang/String;)V
    .locals 3

    const-string/jumbo v0, "text"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 154
    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerCoordinator;->searchMessageView:Lcom/squareup/widgets/MessageView;

    const-string v1, "searchMessageView"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Landroid/view/View;

    check-cast p1, Ljava/lang/CharSequence;

    invoke-static {p1}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v2

    xor-int/lit8 v2, v2, 0x1

    invoke-static {v0, v2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 155
    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerCoordinator;->searchMessageView:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final showProgress$crm_choose_customer_release(Z)V
    .locals 1

    const-string v0, "progressView"

    if-eqz p1, :cond_1

    .line 145
    iget-object p1, p0, Lcom/squareup/ui/crm/ChooseCustomerCoordinator;->progressView:Landroid/view/View;

    if-nez p1, :cond_0

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    iget v0, p0, Lcom/squareup/ui/crm/ChooseCustomerCoordinator;->shortAnimTimeMs:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;I)V

    goto :goto_0

    .line 147
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/crm/ChooseCustomerCoordinator;->progressView:Landroid/view/View;

    if-nez p1, :cond_2

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void
.end method
