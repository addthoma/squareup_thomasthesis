.class public Lcom/squareup/ui/crm/rows/CustomerUnitRow;
.super Landroid/widget/LinearLayout;
.source "CustomerUnitRow.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/rows/CustomerUnitRow$ViewData;
    }
.end annotation


# instance fields
.field private checkCircle:Lcom/squareup/marin/widgets/MarinCheckBox;

.field private contactInfo:Landroid/widget/TextView;

.field private initialCircleView:Lcom/squareup/ui/crm/rows/InitialCircleView;

.field private name:Landroid/widget/TextView;

.field private piiContactLineScrubber:Lcom/squareup/accessibility/AccessibilityScrubber;

.field private piiDisplayNameScrubber:Lcom/squareup/accessibility/AccessibilityScrubber;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 35
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 37
    sget v0, Lcom/squareup/crm/R$layout;->crm_v2_customer_unit_row:I

    invoke-static {p1, v0, p0}, Lcom/squareup/ui/crm/rows/CustomerUnitRow;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 39
    sget p1, Lcom/squareup/crm/R$id;->crm_customer_display_name_pii_wrapper:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/accessibility/AccessibilityScrubber;

    iput-object p1, p0, Lcom/squareup/ui/crm/rows/CustomerUnitRow;->piiDisplayNameScrubber:Lcom/squareup/accessibility/AccessibilityScrubber;

    .line 40
    sget p1, Lcom/squareup/crm/R$id;->crm_customer_contact_line_pii_wrapper:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/accessibility/AccessibilityScrubber;

    iput-object p1, p0, Lcom/squareup/ui/crm/rows/CustomerUnitRow;->piiContactLineScrubber:Lcom/squareup/accessibility/AccessibilityScrubber;

    .line 41
    sget p1, Lcom/squareup/crm/R$id;->crm_customer_display_name:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/ui/crm/rows/CustomerUnitRow;->name:Landroid/widget/TextView;

    .line 42
    sget p1, Lcom/squareup/crm/R$id;->crm_customer_contact_info:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/ui/crm/rows/CustomerUnitRow;->contactInfo:Landroid/widget/TextView;

    .line 43
    sget p1, Lcom/squareup/crm/R$id;->crm_customer_initial_circle:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/rows/InitialCircleView;

    iput-object p1, p0, Lcom/squareup/ui/crm/rows/CustomerUnitRow;->initialCircleView:Lcom/squareup/ui/crm/rows/InitialCircleView;

    .line 44
    sget p1, Lcom/squareup/crm/R$id;->crm_customer_check_circle:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marin/widgets/MarinCheckBox;

    iput-object p1, p0, Lcom/squareup/ui/crm/rows/CustomerUnitRow;->checkCircle:Lcom/squareup/marin/widgets/MarinCheckBox;

    return-void
.end method

.method private static getContactInfo(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/util/Res;Lcom/squareup/text/PhoneNumberHelper;)Ljava/lang/String;
    .locals 2

    .line 88
    iget-object v0, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 90
    :cond_0
    iget-object v0, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    iget-object v0, v0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->phone_number:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    iget-object v0, v0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->email_address:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    return-object v1

    .line 92
    :cond_1
    iget-object v0, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    iget-object v0, v0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->phone_number:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 93
    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->email_address:Ljava/lang/String;

    return-object p0

    .line 94
    :cond_2
    iget-object v0, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    iget-object v0, v0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->email_address:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 95
    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->phone_number:Ljava/lang/String;

    invoke-interface {p2, p0}, Lcom/squareup/text/PhoneNumberHelper;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 97
    :cond_3
    sget v0, Lcom/squareup/crm/R$string;->customer_unit_contact_info_format:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    iget-object v0, v0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->phone_number:Ljava/lang/String;

    .line 98
    invoke-interface {p2, v0}, Lcom/squareup/text/PhoneNumberHelper;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    const-string v0, "phone"

    invoke-virtual {p1, v0, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/CustomerProfile;->email_address:Ljava/lang/String;

    const-string p2, "email"

    .line 99
    invoke-virtual {p1, p2, p0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 100
    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method private static getInitialCircleIsItalic(Lcom/squareup/protos/client/rolodex/Contact;Ljava/util/Locale;)Z
    .locals 0

    .line 126
    invoke-static {p0, p1}, Lcom/squareup/ui/crm/rows/CustomerUnitRow;->getInitials(Lcom/squareup/protos/client/rolodex/Contact;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p0

    return p0
.end method

.method private static getInitialCircleValue(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/util/Res;Ljava/util/Locale;)Ljava/lang/String;
    .locals 0

    .line 117
    invoke-static {p0, p2}, Lcom/squareup/ui/crm/rows/CustomerUnitRow;->getInitials(Lcom/squareup/protos/client/rolodex/Contact;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p0

    .line 118
    invoke-static {p0}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p2

    if-eqz p2, :cond_0

    .line 119
    sget p0, Lcom/squareup/crm/R$string;->crm_contact_default_display_initials:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    :cond_0
    return-object p0
.end method

.method public static getInitials(Lcom/squareup/protos/client/rolodex/Contact;Ljava/util/Locale;)Ljava/lang/String;
    .locals 5

    .line 105
    invoke-static {p0}, Lcom/squareup/crm/util/RolodexContactHelper;->getFirstName(Lcom/squareup/protos/client/rolodex/Contact;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-string v3, ""

    if-eqz v0, :cond_0

    .line 106
    invoke-static {p0}, Lcom/squareup/crm/util/RolodexContactHelper;->getFirstName(Lcom/squareup/protos/client/rolodex/Contact;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v3

    .line 108
    :goto_0
    invoke-static {p0}, Lcom/squareup/crm/util/RolodexContactHelper;->getLastName(Lcom/squareup/protos/client/rolodex/Contact;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 109
    invoke-static {p0}, Lcom/squareup/crm/util/RolodexContactHelper;->getLastName(Lcom/squareup/protos/client/rolodex/Contact;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0, p1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    .line 113
    :cond_1
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static getViewDataFromContact(Lcom/squareup/protos/client/rolodex/Contact;ILcom/squareup/util/Res;Lcom/squareup/text/PhoneNumberHelper;Ljava/util/Locale;ZZ)Lcom/squareup/ui/crm/rows/CustomerUnitRow$ViewData;
    .locals 11

    move-object v0, p0

    move-object v1, p2

    move-object v2, p4

    .line 73
    new-instance v10, Lcom/squareup/ui/crm/rows/CustomerUnitRow$ViewData;

    .line 74
    invoke-static {p0, p2}, Lcom/squareup/crm/util/RolodexContactHelper;->getFullName(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v3

    move-object v4, p3

    .line 75
    invoke-static {p0, p2, p3}, Lcom/squareup/ui/crm/rows/CustomerUnitRow;->getContactInfo(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/util/Res;Lcom/squareup/text/PhoneNumberHelper;)Ljava/lang/String;

    move-result-object v4

    .line 77
    invoke-static {p0, p2, p4}, Lcom/squareup/ui/crm/rows/CustomerUnitRow;->getInitialCircleValue(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/util/Res;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v5

    .line 78
    invoke-static {p0, p4}, Lcom/squareup/ui/crm/rows/CustomerUnitRow;->getInitialCircleIsItalic(Lcom/squareup/protos/client/rolodex/Contact;Ljava/util/Locale;)Z

    move-result v6

    .line 79
    invoke-static {p0, p2}, Lcom/squareup/crm/util/CustomerColorUtilsKt;->getCircleColor(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/util/Res;)I

    move-result v7

    const/4 v8, 0x0

    move-object v0, v10

    move-object v1, v3

    move-object v2, v4

    move v3, p1

    move-object v4, v5

    move v5, v6

    move v6, v7

    move v7, v8

    move/from16 v8, p5

    move/from16 v9, p6

    invoke-direct/range {v0 .. v9}, Lcom/squareup/ui/crm/rows/CustomerUnitRow$ViewData;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;ZIZZZ)V

    return-object v10
.end method


# virtual methods
.method public setViewData(Lcom/squareup/ui/crm/rows/CustomerUnitRow$ViewData;)V
    .locals 3

    .line 48
    sget v0, Lcom/squareup/crm/R$string;->crm_customer_name_row_content_description:I

    invoke-static {p0, v0}, Lcom/squareup/phrase/Phrase;->from(Landroid/view/View;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    iget v1, p1, Lcom/squareup/ui/crm/rows/CustomerUnitRow$ViewData;->index:I

    add-int/lit8 v1, v1, 0x1

    const-string v2, "index"

    .line 49
    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 50
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 51
    iget-object v1, p0, Lcom/squareup/ui/crm/rows/CustomerUnitRow;->piiDisplayNameScrubber:Lcom/squareup/accessibility/AccessibilityScrubber;

    invoke-virtual {v1, v0}, Lcom/squareup/accessibility/AccessibilityScrubber;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 52
    iget-object v1, p0, Lcom/squareup/ui/crm/rows/CustomerUnitRow;->piiContactLineScrubber:Lcom/squareup/accessibility/AccessibilityScrubber;

    invoke-virtual {v1, v0}, Lcom/squareup/accessibility/AccessibilityScrubber;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/CustomerUnitRow;->name:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/squareup/ui/crm/rows/CustomerUnitRow$ViewData;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/CustomerUnitRow;->contactInfo:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/squareup/ui/crm/rows/CustomerUnitRow$ViewData;->contactInfo:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/CustomerUnitRow;->contactInfo:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/squareup/ui/crm/rows/CustomerUnitRow$ViewData;->contactInfo:Ljava/lang/String;

    invoke-static {v1}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 58
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/CustomerUnitRow;->piiContactLineScrubber:Lcom/squareup/accessibility/AccessibilityScrubber;

    iget-object v1, p1, Lcom/squareup/ui/crm/rows/CustomerUnitRow$ViewData;->contactInfo:Ljava/lang/String;

    invoke-static {v1}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/CustomerUnitRow;->initialCircleView:Lcom/squareup/ui/crm/rows/InitialCircleView;

    iget-boolean v1, p1, Lcom/squareup/ui/crm/rows/CustomerUnitRow$ViewData;->inMultiSelectMode:Z

    xor-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/CustomerUnitRow;->checkCircle:Lcom/squareup/marin/widgets/MarinCheckBox;

    iget-boolean v1, p1, Lcom/squareup/ui/crm/rows/CustomerUnitRow$ViewData;->inMultiSelectMode:Z

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/CustomerUnitRow;->checkCircle:Lcom/squareup/marin/widgets/MarinCheckBox;

    iget-boolean v1, p1, Lcom/squareup/ui/crm/rows/CustomerUnitRow$ViewData;->multiSelected:Z

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinCheckBox;->setChecked(Z)V

    .line 64
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/CustomerUnitRow;->initialCircleView:Lcom/squareup/ui/crm/rows/InitialCircleView;

    iget-object v1, p1, Lcom/squareup/ui/crm/rows/CustomerUnitRow$ViewData;->initialCircleString:Ljava/lang/String;

    iget-boolean v2, p1, Lcom/squareup/ui/crm/rows/CustomerUnitRow$ViewData;->initialCircleItalic:Z

    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/crm/rows/InitialCircleView;->setText(Ljava/lang/String;Z)V

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/CustomerUnitRow;->initialCircleView:Lcom/squareup/ui/crm/rows/InitialCircleView;

    iget p1, p1, Lcom/squareup/ui/crm/rows/CustomerUnitRow$ViewData;->circleColor:I

    invoke-virtual {v0, p1}, Lcom/squareup/ui/crm/rows/InitialCircleView;->setBackgroundColor(I)V

    return-void
.end method
