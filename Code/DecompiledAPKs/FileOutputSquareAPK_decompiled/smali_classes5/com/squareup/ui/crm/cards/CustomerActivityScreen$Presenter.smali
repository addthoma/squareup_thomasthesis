.class Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "CustomerActivityScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/CustomerActivityScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/crm/cards/CustomerActivityView;",
        ">;"
    }
.end annotation


# instance fields
.field private final customerActivityHelper:Lcom/squareup/ui/crm/cards/CustomerActivityHelper;

.field private final dateFormatter:Ljava/text/DateFormat;

.field private final eventLoader:Lcom/squareup/crm/RolodexEventLoader;

.field private events:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Event;",
            ">;"
        }
    .end annotation
.end field

.field private final features:Lcom/squareup/settings/server/Features;

.field private final locale:Ljava/util/Locale;

.field private final onNearEndOfLists:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;>;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field private final runner:Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Runner;

.field private final threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

.field private final timeFormatter:Ljava/text/DateFormat;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Runner;Lcom/squareup/ui/crm/cards/CustomerActivityHelper;Lcom/squareup/crm/RolodexEventLoader;Lcom/squareup/thread/enforcer/ThreadEnforcer;Ljava/text/DateFormat;Ljava/text/DateFormat;Ljava/util/Locale;Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;)V
    .locals 1
    .param p4    # Lcom/squareup/thread/enforcer/ThreadEnforcer;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 103
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 95
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Presenter;->onNearEndOfLists:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 97
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Presenter;->events:Ljava/util/List;

    .line 104
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Runner;

    .line 105
    iput-object p2, p0, Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Presenter;->customerActivityHelper:Lcom/squareup/ui/crm/cards/CustomerActivityHelper;

    .line 106
    iput-object p3, p0, Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Presenter;->eventLoader:Lcom/squareup/crm/RolodexEventLoader;

    .line 107
    iput-object p4, p0, Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Presenter;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    .line 108
    iput-object p5, p0, Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Presenter;->dateFormatter:Ljava/text/DateFormat;

    .line 109
    iput-object p6, p0, Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Presenter;->timeFormatter:Ljava/text/DateFormat;

    .line 110
    iput-object p7, p0, Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Presenter;->locale:Ljava/util/Locale;

    .line 111
    iput-object p8, p0, Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 112
    iput-object p9, p0, Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method static synthetic lambda$null$4(Lcom/squareup/datafetch/AbstractLoader$LoaderState;)Ljava/lang/Boolean;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 143
    instance-of p0, p0, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Progress;

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$onLoad$0(Lcom/squareup/datafetch/AbstractLoader$LoaderState;)Ljava/lang/Boolean;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 129
    instance-of p0, p0, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Progress;

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$onLoad$1(Ljava/lang/Boolean;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 131
    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-eqz p0, :cond_0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public getDateString(Lcom/squareup/protos/common/time/DateTime;)Ljava/lang/String;
    .locals 3

    .line 204
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Presenter;->dateFormatter:Ljava/text/DateFormat;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Presenter;->timeFormatter:Ljava/text/DateFormat;

    iget-object v2, p0, Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Presenter;->locale:Ljava/util/Locale;

    invoke-static {v0, v1, p1, v2}, Lcom/squareup/crm/DateTimeFormatHelper;->formatDateForTransaction(Ljava/text/DateFormat;Ljava/text/DateFormat;Lcom/squareup/protos/common/time/DateTime;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getEvents()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Event;",
            ">;"
        }
    .end annotation

    .line 186
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Presenter;->events:Ljava/util/List;

    return-object v0
.end method

.method isActivityClickable(Lcom/squareup/protos/client/rolodex/Event;)Z
    .locals 1

    .line 190
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Presenter;->customerActivityHelper:Lcom/squareup/ui/crm/cards/CustomerActivityHelper;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/crm/cards/CustomerActivityHelper;->isActivityClickable(Lcom/squareup/protos/client/rolodex/Event;)Z

    move-result p1

    return p1
.end method

.method public synthetic lambda$null$2$CustomerActivityScreen$Presenter(Lcom/squareup/ui/crm/cards/CustomerActivityView;Ljava/lang/Boolean;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 137
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Presenter;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 138
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/cards/CustomerActivityView;->showProgress(Z)V

    return-void
.end method

.method public synthetic lambda$null$5$CustomerActivityScreen$Presenter(Lcom/squareup/ui/crm/cards/CustomerActivityView;Ljava/lang/Boolean;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 146
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Presenter;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 147
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/cards/CustomerActivityView;->showProgress(Z)V

    return-void
.end method

.method public synthetic lambda$null$7$CustomerActivityScreen$Presenter(Lkotlin/Unit;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 157
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Presenter;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {p1}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 158
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Presenter;->eventLoader:Lcom/squareup/crm/RolodexEventLoader;

    const/16 v0, 0x32

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/crm/RolodexEventLoader;->loadMore(Ljava/lang/Integer;)V

    return-void
.end method

.method public synthetic lambda$null$9$CustomerActivityScreen$Presenter(Lcom/squareup/ui/crm/cards/CustomerActivityView;Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 164
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Presenter;->threadEnforcer:Lcom/squareup/thread/enforcer/ThreadEnforcer;

    invoke-interface {v0}, Lcom/squareup/thread/enforcer/ThreadEnforcer;->confine()V

    .line 166
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 167
    invoke-virtual {p2}, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;->getItems()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 168
    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 171
    :cond_0
    invoke-virtual {p2}, Lcom/squareup/datafetch/AbstractLoader$LoaderState$Results;->getHasMore()Z

    move-result p2

    if-eqz p2, :cond_1

    .line 174
    iget-object p2, p0, Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Presenter;->onNearEndOfLists:Lcom/jakewharton/rxrelay2/PublishRelay;

    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/CustomerActivityView;->onNearEndOfList()Lio/reactivex/Observable;

    move-result-object v1

    const-wide/16 v2, 0x1

    invoke-virtual {v1, v2, v3}, Lio/reactivex/Observable;->take(J)Lio/reactivex/Observable;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    .line 177
    :cond_1
    iput-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Presenter;->events:Ljava/util/List;

    .line 178
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/CustomerActivityView;->refresh()V

    return-void
.end method

.method public synthetic lambda$onLoad$10$CustomerActivityScreen$Presenter(Lcom/squareup/ui/crm/cards/CustomerActivityView;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 162
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Presenter;->eventLoader:Lcom/squareup/crm/RolodexEventLoader;

    invoke-virtual {v0}, Lcom/squareup/crm/RolodexEventLoader;->results()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerActivityScreen$Presenter$pTd5HTJzI5Hwk0ntVKNmYW52t5Q;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerActivityScreen$Presenter$pTd5HTJzI5Hwk0ntVKNmYW52t5Q;-><init>(Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Presenter;Lcom/squareup/ui/crm/cards/CustomerActivityView;)V

    .line 163
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$3$CustomerActivityScreen$Presenter(Lio/reactivex/Observable;Lcom/squareup/ui/crm/cards/CustomerActivityView;)Lio/reactivex/disposables/Disposable;
    .locals 1

    .line 135
    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerActivityScreen$Presenter$QEhtTnV26L81AubuiTPVz1YJgxA;

    invoke-direct {v0, p0, p2}, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerActivityScreen$Presenter$QEhtTnV26L81AubuiTPVz1YJgxA;-><init>(Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Presenter;Lcom/squareup/ui/crm/cards/CustomerActivityView;)V

    .line 136
    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$6$CustomerActivityScreen$Presenter(Lcom/squareup/ui/crm/cards/CustomerActivityView;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 142
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Presenter;->eventLoader:Lcom/squareup/crm/RolodexEventLoader;

    invoke-virtual {v0}, Lcom/squareup/crm/RolodexEventLoader;->state()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerActivityScreen$Presenter$pgFE98In1hna17-6w85e5bYKgeI;->INSTANCE:Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerActivityScreen$Presenter$pgFE98In1hna17-6w85e5bYKgeI;

    .line 143
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 144
    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerActivityScreen$Presenter$5SDqgZc7NeYvXTsquLC7-EySWBs;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerActivityScreen$Presenter$5SDqgZc7NeYvXTsquLC7-EySWBs;-><init>(Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Presenter;Lcom/squareup/ui/crm/cards/CustomerActivityView;)V

    .line 145
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$8$CustomerActivityScreen$Presenter()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 155
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Presenter;->onNearEndOfLists:Lcom/jakewharton/rxrelay2/PublishRelay;

    invoke-static {v0}, Lio/reactivex/Observable;->switchOnNext(Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerActivityScreen$Presenter$3FLu87m4whv-v8DcRdIeECavANg;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerActivityScreen$Presenter$3FLu87m4whv-v8DcRdIeECavANg;-><init>(Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Presenter;)V

    .line 156
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method onActivityClicked(Lcom/squareup/protos/client/rolodex/Event;Landroid/view/View;)V
    .locals 1

    .line 194
    iget-object p2, p0, Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v0, Lcom/squareup/settings/server/Features$Feature;->CRM_MESSAGING:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p2, v0}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p2

    if-eqz p2, :cond_0

    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/Event;->type:Lcom/squareup/protos/client/rolodex/EventType;

    sget-object v0, Lcom/squareup/protos/client/rolodex/EventType;->FEEDBACK_CONVERSATION:Lcom/squareup/protos/client/rolodex/EventType;

    if-ne p2, v0, :cond_0

    .line 195
    iget-object p2, p0, Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Runner;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Event;->event_token:Ljava/lang/String;

    invoke-interface {p2, p1}, Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Runner;->showConversationScreen(Ljava/lang/String;)V

    goto :goto_0

    .line 196
    :cond_0
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/Event;->type:Lcom/squareup/protos/client/rolodex/EventType;

    sget-object v0, Lcom/squareup/protos/client/rolodex/EventType;->PAYMENT:Lcom/squareup/protos/client/rolodex/EventType;

    if-ne p2, v0, :cond_1

    .line 197
    iget-object p2, p0, Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Runner;

    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/Event;->title:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Event;->event_token:Ljava/lang/String;

    invoke-interface {p2, v0, p1}, Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Runner;->showBillHistoryScreen(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 199
    :cond_1
    iget-object p2, p0, Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Presenter;->customerActivityHelper:Lcom/squareup/ui/crm/cards/CustomerActivityHelper;

    invoke-virtual {p2, p1}, Lcom/squareup/ui/crm/cards/CustomerActivityHelper;->onActivityClicked(Lcom/squareup/protos/client/rolodex/Event;)V

    :goto_0
    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 3

    .line 117
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 118
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/cards/CustomerActivityView;

    .line 120
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/crmscreens/R$string;->crm_activity_list_header_v2:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 122
    new-instance v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 123
    invoke-virtual {v1, v2, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Runner;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/crm/cards/-$$Lambda$GxjqPGUOQy2AP8HGc4LmuL6OwLc;

    invoke-direct {v2, v1}, Lcom/squareup/ui/crm/cards/-$$Lambda$GxjqPGUOQy2AP8HGc4LmuL6OwLc;-><init>(Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Runner;)V

    .line 124
    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 125
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    .line 122
    invoke-virtual {p1, v0}, Lcom/squareup/ui/crm/cards/CustomerActivityView;->setActionBarConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 128
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Presenter;->eventLoader:Lcom/squareup/crm/RolodexEventLoader;

    .line 129
    invoke-virtual {v0}, Lcom/squareup/crm/RolodexEventLoader;->state()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerActivityScreen$Presenter$r8MhaGtLrfFH7fdzaWJJCc0RAgs;->INSTANCE:Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerActivityScreen$Presenter$r8MhaGtLrfFH7fdzaWJJCc0RAgs;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    .line 130
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/CustomerActivityView;->atEndOfList()Lio/reactivex/Observable;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerActivityScreen$Presenter$ImeQX6b-fzZ7CpyMMyG-H4PpNRk;->INSTANCE:Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerActivityScreen$Presenter$ImeQX6b-fzZ7CpyMMyG-H4PpNRk;

    .line 128
    invoke-static {v0, v1, v2}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    .line 132
    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    .line 134
    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerActivityScreen$Presenter$a-UjTpc1KUUxGOCWLWEmc0smaKc;

    invoke-direct {v1, p0, v0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerActivityScreen$Presenter$a-UjTpc1KUUxGOCWLWEmc0smaKc;-><init>(Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Presenter;Lio/reactivex/Observable;Lcom/squareup/ui/crm/cards/CustomerActivityView;)V

    invoke-static {p1, v1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 141
    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerActivityScreen$Presenter$SJf9kA3qftM31v6IWGZ_2f-4LLI;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerActivityScreen$Presenter$SJf9kA3qftM31v6IWGZ_2f-4LLI;-><init>(Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Presenter;Lcom/squareup/ui/crm/cards/CustomerActivityView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 154
    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerActivityScreen$Presenter$JAQnVp5JSZO4vqiJssNsBU7s_ow;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerActivityScreen$Presenter$JAQnVp5JSZO4vqiJssNsBU7s_ow;-><init>(Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Presenter;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 161
    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerActivityScreen$Presenter$3WLf0cVhB4JE6dvcmyYj6JXqQiA;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$CustomerActivityScreen$Presenter$3WLf0cVhB4JE6dvcmyYj6JXqQiA;-><init>(Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Presenter;Lcom/squareup/ui/crm/cards/CustomerActivityView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
