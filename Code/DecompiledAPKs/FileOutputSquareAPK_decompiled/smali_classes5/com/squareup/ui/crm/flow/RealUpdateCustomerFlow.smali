.class public Lcom/squareup/ui/crm/flow/RealUpdateCustomerFlow;
.super Ljava/lang/Object;
.source "RealUpdateCustomerFlow.java"

# interfaces
.implements Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;


# instance fields
.field private results:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/flow/RealUpdateCustomerFlow;->results:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-void
.end method


# virtual methods
.method public emitResult(Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;)V
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/RealUpdateCustomerFlow;->results:Lcom/jakewharton/rxrelay2/PublishRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method

.method public bridge synthetic getFirstScreen(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;Lcom/squareup/ui/crm/flow/CrmScopeType;Lcom/squareup/protos/client/rolodex/Contact;)Lcom/squareup/container/ContainerTreeKey;
    .locals 0

    .line 14
    invoke-virtual/range {p0 .. p6}, Lcom/squareup/ui/crm/flow/RealUpdateCustomerFlow;->getFirstScreen(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;Lcom/squareup/ui/crm/flow/CrmScopeType;Lcom/squareup/protos/client/rolodex/Contact;)Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object p1

    return-object p1
.end method

.method public getFirstScreen(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;Lcom/squareup/ui/crm/flow/CrmScopeType;Lcom/squareup/protos/client/rolodex/Contact;)Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 9

    .line 30
    new-instance v0, Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2;

    new-instance v8, Lcom/squareup/ui/crm/flow/UpdateCustomerScope;

    move-object v1, v8

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v1 .. v7}, Lcom/squareup/ui/crm/flow/UpdateCustomerScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;Lcom/squareup/ui/crm/flow/CrmScopeType;Lcom/squareup/protos/client/rolodex/Contact;)V

    invoke-direct {v0, v8}, Lcom/squareup/ui/crm/v2/UpdateCustomerScreenV2;-><init>(Lcom/squareup/ui/crm/flow/UpdateCustomerScope;)V

    return-object v0
.end method

.method public results()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;",
            ">;"
        }
    .end annotation

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/RealUpdateCustomerFlow;->results:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-object v0
.end method
