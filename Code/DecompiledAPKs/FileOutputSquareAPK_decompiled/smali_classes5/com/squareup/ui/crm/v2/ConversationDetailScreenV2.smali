.class public final Lcom/squareup/ui/crm/v2/ConversationDetailScreenV2;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "ConversationDetailScreenV2.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/coordinators/CoordinatorProvider;
.implements Lcom/squareup/ui/crm/v2/CustomersAppletDetailScreen;
.implements Lcom/squareup/container/MaybePersistent;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/crm/v2/ConversationDetailScreenV2$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/v2/ConversationDetailScreenV2$Component;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/crm/v2/ConversationDetailScreenV2;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final conversationToken:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 65
    sget-object v0, Lcom/squareup/ui/crm/v2/-$$Lambda$ConversationDetailScreenV2$cnAfdga9RsRxJRkgbsWr4nwTjsY;->INSTANCE:Lcom/squareup/ui/crm/v2/-$$Lambda$ConversationDetailScreenV2$cnAfdga9RsRxJRkgbsWr4nwTjsY;

    .line 66
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/crm/v2/ConversationDetailScreenV2;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 31
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/ConversationDetailScreenV2;->conversationToken:Ljava/lang/String;

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/crm/v2/ConversationDetailScreenV2;
    .locals 1

    .line 67
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object p0

    .line 68
    new-instance v0, Lcom/squareup/ui/crm/v2/ConversationDetailScreenV2;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/v2/ConversationDetailScreenV2;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 61
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/main/RegisterTreeKey;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 62
    iget-object p2, p0, Lcom/squareup/ui/crm/v2/ConversationDetailScreenV2;->conversationToken:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .line 44
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Lcom/squareup/ui/main/RegisterTreeKey;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "{conversationToken="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/ConversationDetailScreenV2;->conversationToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getParentKey()Ljava/lang/Object;
    .locals 1

    .line 36
    sget-object v0, Lcom/squareup/ui/crm/applet/ConversationDetailScope;->INSTANCE:Lcom/squareup/ui/crm/applet/ConversationDetailScope;

    return-object v0
.end method

.method public isPersistent()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 1

    .line 48
    const-class v0, Lcom/squareup/ui/crm/v2/ConversationDetailScreenV2$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/v2/ConversationDetailScreenV2$Component;

    .line 49
    invoke-interface {p1}, Lcom/squareup/ui/crm/v2/ConversationDetailScreenV2$Component;->coordinator()Lcom/squareup/ui/crm/v2/ConversationDetailCoordinatorV2;

    move-result-object p1

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 72
    sget v0, Lcom/squareup/crm/applet/R$layout;->crm_v2_conversation_view:I

    return v0
.end method
