.class public Lcom/squareup/ui/crm/applet/ChooseFiltersScope;
.super Lcom/squareup/ui/crm/applet/InCustomersAppletScope;
.source "ChooseFiltersScope.java"

# interfaces
.implements Lcom/squareup/container/RegistersInScope;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/crm/applet/ChooseFiltersScope$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/applet/ChooseFiltersScope$Component;,
        Lcom/squareup/ui/crm/applet/ChooseFiltersScope$Module;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/crm/applet/ChooseFiltersScope;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/crm/applet/ChooseFiltersScope;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    new-instance v0, Lcom/squareup/ui/crm/applet/ChooseFiltersScope;

    invoke-direct {v0}, Lcom/squareup/ui/crm/applet/ChooseFiltersScope;-><init>()V

    sput-object v0, Lcom/squareup/ui/crm/applet/ChooseFiltersScope;->INSTANCE:Lcom/squareup/ui/crm/applet/ChooseFiltersScope;

    .line 66
    sget-object v0, Lcom/squareup/ui/crm/applet/ChooseFiltersScope;->INSTANCE:Lcom/squareup/ui/crm/applet/ChooseFiltersScope;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/crm/applet/ChooseFiltersScope;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 63
    invoke-direct {p0}, Lcom/squareup/ui/crm/applet/InCustomersAppletScope;-><init>()V

    return-void
.end method


# virtual methods
.method public register(Lmortar/MortarScope;)V
    .locals 2

    .line 54
    const-class v0, Lcom/squareup/ui/crm/applet/ChooseFiltersScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/applet/ChooseFiltersScope$Component;

    .line 56
    invoke-static {p1}, Lmortar/bundler/BundleService;->getBundleService(Lmortar/MortarScope;)Lmortar/bundler/BundleService;

    move-result-object p1

    .line 57
    invoke-interface {v0}, Lcom/squareup/ui/crm/applet/ChooseFiltersScope$Component;->chooseFiltersFlow()Lcom/squareup/ui/crm/flow/ChooseFiltersFlow;

    move-result-object v1

    invoke-virtual {p1, v1}, Lmortar/bundler/BundleService;->register(Lmortar/bundler/Bundler;)V

    .line 58
    invoke-interface {v0}, Lcom/squareup/ui/crm/applet/ChooseFiltersScope$Component;->createFilterFlow()Lcom/squareup/ui/crm/flow/CreateFilterFlow;

    move-result-object v1

    invoke-virtual {p1, v1}, Lmortar/bundler/BundleService;->register(Lmortar/bundler/Bundler;)V

    .line 59
    invoke-interface {v0}, Lcom/squareup/ui/crm/applet/ChooseFiltersScope$Component;->updateFilterFlow()Lcom/squareup/ui/crm/flow/UpdateFilterFlow;

    move-result-object v0

    invoke-virtual {p1, v0}, Lmortar/bundler/BundleService;->register(Lmortar/bundler/Bundler;)V

    return-void
.end method
