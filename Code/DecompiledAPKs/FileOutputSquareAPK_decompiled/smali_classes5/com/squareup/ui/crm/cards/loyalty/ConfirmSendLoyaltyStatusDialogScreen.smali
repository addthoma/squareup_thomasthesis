.class public final Lcom/squareup/ui/crm/cards/loyalty/ConfirmSendLoyaltyStatusDialogScreen;
.super Lcom/squareup/ui/crm/flow/InCrmScope;
.source "ConfirmSendLoyaltyStatusDialogScreen.java"

# interfaces
.implements Lcom/squareup/container/MaybePersistent;


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/ui/crm/cards/loyalty/ConfirmSendLoyaltyStatusDialogScreen$Factory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/cards/loyalty/ConfirmSendLoyaltyStatusDialogScreen$Component;,
        Lcom/squareup/ui/crm/cards/loyalty/ConfirmSendLoyaltyStatusDialogScreen$Factory;,
        Lcom/squareup/ui/crm/cards/loyalty/ConfirmSendLoyaltyStatusDialogScreen$Runner;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/crm/cards/loyalty/ConfirmSendLoyaltyStatusDialogScreen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 65
    sget-object v0, Lcom/squareup/ui/crm/cards/loyalty/-$$Lambda$ConfirmSendLoyaltyStatusDialogScreen$idjUcZRXA7VDm92Uar9vUMb-KUw;->INSTANCE:Lcom/squareup/ui/crm/cards/loyalty/-$$Lambda$ConfirmSendLoyaltyStatusDialogScreen$idjUcZRXA7VDm92Uar9vUMb-KUw;

    .line 66
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/crm/cards/loyalty/ConfirmSendLoyaltyStatusDialogScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/crm/flow/CrmScope;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 26
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/flow/InCrmScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/crm/cards/loyalty/ConfirmSendLoyaltyStatusDialogScreen;
    .locals 1

    .line 67
    const-class v0, Lcom/squareup/ui/crm/flow/CrmScope;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/crm/flow/CrmScope;

    .line 68
    new-instance v0, Lcom/squareup/ui/crm/cards/loyalty/ConfirmSendLoyaltyStatusDialogScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/loyalty/ConfirmSendLoyaltyStatusDialogScreen;-><init>(Lcom/squareup/ui/crm/flow/CrmScope;)V

    return-object v0
.end method


# virtual methods
.method public isPersistent()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
