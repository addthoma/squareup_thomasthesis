.class Lcom/squareup/ui/crm/v2/profile/ContactEditView$3;
.super Lcom/squareup/text/EmptyTextWatcher;
.source "ContactEditView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/v2/profile/ContactEditView;->addObservableToEditText(Lcom/squareup/ui/BaseXableEditText;Lcom/jakewharton/rxrelay/PublishRelay;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/crm/v2/profile/ContactEditView;

.field final synthetic val$observable:Lcom/jakewharton/rxrelay/PublishRelay;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/v2/profile/ContactEditView;Lcom/jakewharton/rxrelay/PublishRelay;)V
    .locals 0

    .line 483
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView$3;->this$0:Lcom/squareup/ui/crm/v2/profile/ContactEditView;

    iput-object p2, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView$3;->val$observable:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-direct {p0}, Lcom/squareup/text/EmptyTextWatcher;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 1

    .line 485
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ContactEditView$3;->val$observable:Lcom/jakewharton/rxrelay/PublishRelay;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method
