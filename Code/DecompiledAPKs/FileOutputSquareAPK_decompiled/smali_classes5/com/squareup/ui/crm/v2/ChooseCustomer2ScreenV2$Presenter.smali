.class Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;
.super Lmortar/ViewPresenter;
.source "ChooseCustomer2ScreenV2.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;",
        ">;"
    }
.end annotation


# static fields
.field static final SEARCH_DELAY_MS:J = 0xc8L


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final recentContacts:Lcom/squareup/crm/RolodexRecentContactLoader;

.field private final res:Lcom/squareup/util/Res;

.field private final runner:Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Runner;

.field private final searchContacts:Lcom/squareup/crm/RolodexContactLoader;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Runner;Lcom/squareup/crm/RolodexRecentContactLoader;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 105
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 106
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;->runner:Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Runner;

    .line 107
    invoke-interface {p1}, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Runner;->getContactLoaderForSearch()Lcom/squareup/crm/RolodexContactLoader;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;->searchContacts:Lcom/squareup/crm/RolodexContactLoader;

    .line 108
    iput-object p2, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;->recentContacts:Lcom/squareup/crm/RolodexRecentContactLoader;

    .line 109
    iput-object p3, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;->res:Lcom/squareup/util/Res;

    .line 110
    iput-object p4, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    .line 112
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;->searchContacts:Lcom/squareup/crm/RolodexContactLoader;

    const-wide/16 p2, 0xc8

    invoke-virtual {p1, p2, p3}, Lcom/squareup/crm/RolodexContactLoader;->setSearchDelayMs(J)V

    .line 113
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;->searchContacts:Lcom/squareup/crm/RolodexContactLoader;

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Lcom/squareup/crm/RolodexContactLoader;->setRestrictToSearchingOnly(Z)V

    .line 114
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;->searchContacts:Lcom/squareup/crm/RolodexContactLoader;

    sget-object p2, Lcom/squareup/protos/client/rolodex/ListContactsSortType;->DISPLAY_NAME_ASCENDING:Lcom/squareup/protos/client/rolodex/ListContactsSortType;

    invoke-virtual {p1, p2}, Lcom/squareup/crm/RolodexContactLoader;->setSortType(Lcom/squareup/protos/client/rolodex/ListContactsSortType;)V

    return-void
.end method

.method private getDetailFromRecent()Ljava/lang/String;
    .locals 2

    .line 286
    sget-object v0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$1;->$SwitchMap$com$squareup$ui$crm$flow$CrmScopeType:[I

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;->runner:Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Runner;

    invoke-interface {v1}, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Runner;->getPathType()Lcom/squareup/ui/crm/flow/CrmScopeType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/ui/crm/flow/CrmScopeType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    goto :goto_0

    .line 295
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid flow type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    :goto_0
    const-string v0, "POST_TRANSACTION_FROM_RECENT"

    return-object v0

    :cond_2
    const-string v0, "IN_CART_FROM_RECENT"

    return-object v0

    :cond_3
    const-string v0, "IN_SPLIT_TICKET_FROM_RECENT"

    return-object v0
.end method

.method private getDetailFromSearch()Ljava/lang/String;
    .locals 2

    .line 300
    sget-object v0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$1;->$SwitchMap$com$squareup$ui$crm$flow$CrmScopeType:[I

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;->runner:Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Runner;

    invoke-interface {v1}, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Runner;->getPathType()Lcom/squareup/ui/crm/flow/CrmScopeType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/ui/crm/flow/CrmScopeType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    goto :goto_0

    .line 309
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid flow type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    :goto_0
    const-string v0, "POST_TRANSACTION_FROM_SEARCH"

    return-object v0

    :cond_2
    const-string v0, "IN_CART_FROM_SEARCH"

    return-object v0

    :cond_3
    const-string v0, "IN_SPLIT_TICKET_FROM_SEARCH"

    return-object v0
.end method

.method static synthetic lambda$null$3(Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;Lkotlin/Unit;)V
    .locals 0

    .line 145
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 146
    invoke-static {p0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    return-void
.end method

.method static synthetic lambda$null$7(Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;Lcom/squareup/util/Optional;)V
    .locals 0

    .line 168
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 169
    invoke-virtual {p1}, Lcom/squareup/util/Optional;->isPresent()Z

    move-result p1

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->showSearchProgress(Z)V

    return-void
.end method

.method static synthetic lambda$null$9(Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;Lcom/squareup/util/Optional;)V
    .locals 0

    .line 176
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 177
    invoke-virtual {p1}, Lcom/squareup/util/Optional;->isPresent()Z

    move-result p1

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->showRecentProgress(Z)V

    return-void
.end method

.method static synthetic lambda$onLoad$4(Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;)Lrx/Subscription;
    .locals 2

    .line 143
    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->onSearchClicked()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomer2ScreenV2$Presenter$WKvyiD8ynou0RpcTQB83Xxyi248;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomer2ScreenV2$Presenter$WKvyiD8ynou0RpcTQB83Xxyi248;-><init>(Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;)V

    .line 144
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p0

    return-object p0
.end method

.method private onRecentContacts(Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;)V"
        }
    .end annotation

    .line 238
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;->recentContacts:Lcom/squareup/crm/RolodexRecentContactLoader;

    invoke-interface {v0}, Lcom/squareup/crm/RolodexRecentContactLoader;->getFirstContactJustSaved()Z

    move-result v0

    invoke-virtual {p1, p2, v0}, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->setRecentContacts(Ljava/util/List;Z)V

    return-void
.end method

.method private onSearchContacts(Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;Ljava/lang/String;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;>;)V"
        }
    .end annotation

    .line 214
    invoke-virtual {p1, p3}, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->setSearchContacts(Ljava/util/List;)V

    .line 216
    invoke-static {p2}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p2

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p2, :cond_0

    .line 218
    invoke-virtual {p1, v1}, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->showSearchContacts(Z)V

    .line 219
    invoke-virtual {p1, v1}, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->showSearchMessage(Z)V

    .line 220
    invoke-virtual {p1, v0}, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->showRecentList(Z)V

    goto :goto_1

    .line 222
    :cond_0
    invoke-interface {p3}, Ljava/util/List;->isEmpty()Z

    move-result p2

    if-nez p2, :cond_2

    invoke-interface {p3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result p2

    if-eqz p2, :cond_1

    goto :goto_0

    .line 230
    :cond_1
    invoke-virtual {p1, v0}, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->showSearchContacts(Z)V

    .line 231
    invoke-virtual {p1, v1}, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->showSearchMessage(Z)V

    .line 232
    invoke-virtual {p1, v1}, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->showRecentList(Z)V

    goto :goto_1

    .line 224
    :cond_2
    :goto_0
    invoke-virtual {p1, v1}, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->showSearchContacts(Z)V

    .line 225
    iget-object p2, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;->res:Lcom/squareup/util/Res;

    sget p3, Lcom/squareup/crm/R$string;->crm_contact_search_empty:I

    invoke-interface {p2, p3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->setSearchMessage(Ljava/lang/String;)V

    .line 226
    invoke-virtual {p1, v0}, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->showSearchMessage(Z)V

    .line 227
    invoke-virtual {p1, v1}, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->showRecentList(Z)V

    :goto_1
    return-void
.end method


# virtual methods
.method formatRecentlyCreated(Ljava/lang/Long;)Ljava/lang/String;
    .locals 7

    if-eqz p1, :cond_0

    .line 263
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;->res:Lcom/squareup/util/Res;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    const/4 v5, 0x1

    sget-object v6, Lcom/squareup/util/ShortTimes$MaxUnit;->DAY:Lcom/squareup/util/ShortTimes$MaxUnit;

    invoke-static/range {v0 .. v6}, Lcom/squareup/util/ShortTimes;->shortTimeSince(Lcom/squareup/util/Res;JJZLcom/squareup/util/ShortTimes$MaxUnit;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 265
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;->res:Lcom/squareup/util/Res;

    const-wide/16 v0, 0x0

    const/4 v2, 0x1

    sget-object v3, Lcom/squareup/util/ShortTimes$MaxUnit;->DAY:Lcom/squareup/util/ShortTimes$MaxUnit;

    invoke-static {p1, v0, v1, v2, v3}, Lcom/squareup/util/ShortTimes;->shortTimeSince(Lcom/squareup/util/Res;JZLcom/squareup/util/ShortTimes$MaxUnit;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method formatStatusLine(Lcom/squareup/protos/client/rolodex/Contact;)Ljava/lang/String;
    .locals 2

    .line 270
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 272
    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    if-eqz v1, :cond_1

    .line 273
    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    iget-object v1, v1, Lcom/squareup/protos/client/rolodex/CustomerProfile;->phone_number:Ljava/lang/String;

    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 274
    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    iget-object v1, v1, Lcom/squareup/protos/client/rolodex/CustomerProfile;->phone_number:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 277
    :cond_0
    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    iget-object v1, v1, Lcom/squareup/protos/client/rolodex/CustomerProfile;->email_address:Ljava/lang/String;

    invoke-static {v1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 278
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Contact;->profile:Lcom/squareup/protos/client/rolodex/CustomerProfile;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/CustomerProfile;->email_address:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 282
    :cond_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object p1

    const-string v0, ", "

    invoke-static {p1, v0}, Lcom/squareup/util/Strings;->join([Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$null$11$ChooseCustomer2ScreenV2$Presenter(Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;Lcom/squareup/datafetch/Rx1AbstractLoader$Results;)V
    .locals 1

    .line 184
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 185
    iget-object v0, p2, Lcom/squareup/datafetch/Rx1AbstractLoader$Results;->input:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/crm/RolodexContactLoader$Input;

    iget-object v0, v0, Lcom/squareup/crm/RolodexContactLoader$Input;->searchTerm:Ljava/lang/String;

    iget-object p2, p2, Lcom/squareup/datafetch/Rx1AbstractLoader$Results;->items:Ljava/util/List;

    invoke-direct {p0, p1, v0, p2}, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;->onSearchContacts(Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;Ljava/lang/String;Ljava/util/List;)V

    return-void
.end method

.method public synthetic lambda$null$13$ChooseCustomer2ScreenV2$Presenter(Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;Ljava/util/List;)V
    .locals 0

    .line 192
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 193
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;->onRecentContacts(Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;Ljava/util/List;)V

    return-void
.end method

.method public synthetic lambda$null$15$ChooseCustomer2ScreenV2$Presenter(Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;Lcom/squareup/util/Optional;)V
    .locals 2

    .line 200
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 201
    invoke-virtual {p2}, Lcom/squareup/util/Optional;->isPresent()Z

    move-result p2

    if-eqz p2, :cond_0

    const/4 p2, 0x0

    .line 203
    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->showSearchContacts(Z)V

    .line 204
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/crm/R$string;->crm_contact_search_error:I

    .line 205
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 204
    invoke-virtual {p1, v0}, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->setSearchMessage(Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 206
    invoke-virtual {p1, v0}, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->showSearchMessage(Z)V

    .line 207
    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->showRecentList(Z)V

    :cond_0
    return-void
.end method

.method public synthetic lambda$null$5$ChooseCustomer2ScreenV2$Presenter(Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;Ljava/lang/String;)V
    .locals 2

    .line 153
    invoke-static {p2}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 154
    iget-object p2, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/crm/R$string;->crm_create_new_customer_label:I

    .line 155
    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    .line 154
    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->showCreateNewButton(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 157
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/crmviewcustomer/R$string;->crm_create_new_customer_label_format:I

    .line 158
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string/jumbo v1, "term"

    .line 159
    invoke-virtual {v0, v1, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p2

    .line 160
    invoke-virtual {p2}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p2

    .line 157
    invoke-virtual {p1, p2}, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->showCreateNewButton(Ljava/lang/CharSequence;)V

    :goto_0
    return-void
.end method

.method public synthetic lambda$onLoad$0$ChooseCustomer2ScreenV2$Presenter(Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;)V
    .locals 0

    .line 124
    invoke-static {p1}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 125
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;->runner:Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Runner;

    invoke-interface {p1}, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Runner;->closeChooseCustomerScreen()V

    return-void
.end method

.method public synthetic lambda$onLoad$1$ChooseCustomer2ScreenV2$Presenter(Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;)Lrx/Subscription;
    .locals 2

    .line 133
    invoke-virtual {p1}, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->searchTerm()Lrx/Observable;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->onSearchClicked()Lrx/Observable;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/squareup/ui/crm/cards/lookup/CustomerLookupHelper;->contactLoaderSearchTerm(Lrx/Observable;Lrx/Observable;)Lrx/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;->searchContacts:Lcom/squareup/crm/RolodexContactLoader;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$raW7PjDsZrIp-L_lCXeGebchZgg;

    invoke-direct {v1, v0}, Lcom/squareup/ui/crm/v2/-$$Lambda$raW7PjDsZrIp-L_lCXeGebchZgg;-><init>(Lcom/squareup/crm/RolodexContactLoader;)V

    .line 134
    invoke-virtual {p1, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$10$ChooseCustomer2ScreenV2$Presenter(Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;)Lrx/Subscription;
    .locals 2

    .line 174
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;->recentContacts:Lcom/squareup/crm/RolodexRecentContactLoader;

    invoke-interface {v0}, Lcom/squareup/crm/RolodexRecentContactLoader;->progress()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomer2ScreenV2$Presenter$OGknw4lY4652SoVfsPe7VJqdwc8;

    invoke-direct {v1, p1}, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomer2ScreenV2$Presenter$OGknw4lY4652SoVfsPe7VJqdwc8;-><init>(Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;)V

    .line 175
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$12$ChooseCustomer2ScreenV2$Presenter(Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;)Lrx/Subscription;
    .locals 2

    .line 182
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;->searchContacts:Lcom/squareup/crm/RolodexContactLoader;

    invoke-virtual {v0}, Lcom/squareup/crm/RolodexContactLoader;->results()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomer2ScreenV2$Presenter$sVqPXSUW9QYYk7Yhx9s-i3fkam4;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomer2ScreenV2$Presenter$sVqPXSUW9QYYk7Yhx9s-i3fkam4;-><init>(Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;)V

    .line 183
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$14$ChooseCustomer2ScreenV2$Presenter(Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;)Lrx/Subscription;
    .locals 2

    .line 190
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;->recentContacts:Lcom/squareup/crm/RolodexRecentContactLoader;

    invoke-interface {v0}, Lcom/squareup/crm/RolodexRecentContactLoader;->contacts()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomer2ScreenV2$Presenter$fRWB_xtTwrByubRYfv2yAm-DYAQ;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomer2ScreenV2$Presenter$fRWB_xtTwrByubRYfv2yAm-DYAQ;-><init>(Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;)V

    .line 191
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$16$ChooseCustomer2ScreenV2$Presenter(Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;)Lrx/Subscription;
    .locals 2

    .line 198
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;->searchContacts:Lcom/squareup/crm/RolodexContactLoader;

    invoke-virtual {v0}, Lcom/squareup/crm/RolodexContactLoader;->failure()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomer2ScreenV2$Presenter$dxOX1jCp-DegtWLrYkTgGGb4PYg;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomer2ScreenV2$Presenter$dxOX1jCp-DegtWLrYkTgGGb4PYg;-><init>(Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;)V

    .line 199
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$2$ChooseCustomer2ScreenV2$Presenter(Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;)Lrx/Subscription;
    .locals 2

    .line 138
    invoke-virtual {p1}, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->searchTerm()Lrx/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;->runner:Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Runner;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$t5ECgM8lFV5fnfVZ9XlkwmC3gm8;

    invoke-direct {v1, v0}, Lcom/squareup/ui/crm/v2/-$$Lambda$t5ECgM8lFV5fnfVZ9XlkwmC3gm8;-><init>(Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Runner;)V

    .line 139
    invoke-virtual {p1, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$6$ChooseCustomer2ScreenV2$Presenter(Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;)Lrx/Subscription;
    .locals 2

    .line 151
    invoke-virtual {p1}, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->searchTerm()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomer2ScreenV2$Presenter$VMr4U0gyrJF2uhjcSzxKjDNpfMw;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomer2ScreenV2$Presenter$VMr4U0gyrJF2uhjcSzxKjDNpfMw;-><init>(Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;)V

    .line 152
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$8$ChooseCustomer2ScreenV2$Presenter(Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;)Lrx/Subscription;
    .locals 2

    .line 166
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;->searchContacts:Lcom/squareup/crm/RolodexContactLoader;

    invoke-virtual {v0}, Lcom/squareup/crm/RolodexContactLoader;->progress()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomer2ScreenV2$Presenter$ltnB2WL2CBJje2R7z8DoH8Uu3hg;

    invoke-direct {v1, p1}, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomer2ScreenV2$Presenter$ltnB2WL2CBJje2R7z8DoH8Uu3hg;-><init>(Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;)V

    .line 167
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method onCreateNewPressed()V
    .locals 1

    .line 242
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;->runner:Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Runner;->showCreateCustomerScreen()V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 4

    .line 118
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 119
    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;

    .line 121
    new-instance v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/crmviewcustomer/R$string;->crm_add_customer_title:I

    .line 122
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomer2ScreenV2$Presenter$4zV4aebLbcXdnb9JsQvjEgbo6O8;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomer2ScreenV2$Presenter$4zV4aebLbcXdnb9JsQvjEgbo6O8;-><init>(Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;)V

    .line 123
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 127
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    .line 121
    invoke-virtual {p1, v0}, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->setActionBarConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 129
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;->runner:Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Runner;->getSearchTerm()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;->setSearchTerm(Ljava/lang/String;)V

    .line 132
    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomer2ScreenV2$Presenter$EpRrCd-PxB1Mnxj9oSjI9G1qvzM;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomer2ScreenV2$Presenter$EpRrCd-PxB1Mnxj9oSjI9G1qvzM;-><init>(Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 137
    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomer2ScreenV2$Presenter$KfYeVgFFE_VWu2tRod24i5IJ_ro;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomer2ScreenV2$Presenter$KfYeVgFFE_VWu2tRod24i5IJ_ro;-><init>(Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 142
    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomer2ScreenV2$Presenter$Ez7PQ4m-HgtrAqw1o1XkKaMa0ZU;

    invoke-direct {v0, p1}, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomer2ScreenV2$Presenter$Ez7PQ4m-HgtrAqw1o1XkKaMa0ZU;-><init>(Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 150
    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomer2ScreenV2$Presenter$Rf-WetQwanNmr1iHxs42YZ8qiXM;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomer2ScreenV2$Presenter$Rf-WetQwanNmr1iHxs42YZ8qiXM;-><init>(Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 165
    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomer2ScreenV2$Presenter$609NLLFYTKeQnjcCUmNu0fNMO5M;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomer2ScreenV2$Presenter$609NLLFYTKeQnjcCUmNu0fNMO5M;-><init>(Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 173
    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomer2ScreenV2$Presenter$BhC1kJtGT4fUpM27TOJmAvOUQo8;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomer2ScreenV2$Presenter$BhC1kJtGT4fUpM27TOJmAvOUQo8;-><init>(Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 181
    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomer2ScreenV2$Presenter$ePUcHIrm1RHzFZ43rB0XCJp_Nf4;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomer2ScreenV2$Presenter$ePUcHIrm1RHzFZ43rB0XCJp_Nf4;-><init>(Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 189
    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomer2ScreenV2$Presenter$FffeH0-kbNYArEQWYPvsDLBPBmA;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomer2ScreenV2$Presenter$FffeH0-kbNYArEQWYPvsDLBPBmA;-><init>(Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 197
    new-instance v0, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomer2ScreenV2$Presenter$TtmijXh3IT9XRC9-s5qXb-CE-wE;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/v2/-$$Lambda$ChooseCustomer2ScreenV2$Presenter$TtmijXh3IT9XRC9-s5qXb-CE-wE;-><init>(Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method onRecentCustomerPressed(Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;Lcom/squareup/protos/client/rolodex/Contact;)V
    .locals 3

    .line 246
    invoke-static {p1}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 248
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v0, Lcom/squareup/analytics/event/v1/DetailEvent;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_ADD_CUSTOMER_TO_SALE:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {p0}, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;->getDetailFromRecent()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/analytics/event/v1/DetailEvent;-><init>(Lcom/squareup/analytics/RegisterActionName;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 249
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v0, Lcom/squareup/crm/events/CrmAddCustomerToSaleEvent;

    invoke-direct {p0}, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;->getDetailFromRecent()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/crm/events/CrmAddCustomerToSaleEvent;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    .line 250
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;->runner:Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Runner;

    invoke-interface {p1, p2}, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Runner;->closeChooseCustomerScreen(Lcom/squareup/protos/client/rolodex/Contact;)V

    return-void
.end method

.method onSearchCustomerPressed(Lcom/squareup/ui/crm/v2/ChooseCustomer2ViewV2;Lcom/squareup/protos/client/rolodex/Contact;)V
    .locals 3

    .line 254
    invoke-static {p1}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 256
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v0, Lcom/squareup/analytics/event/v1/DetailEvent;

    sget-object v1, Lcom/squareup/analytics/RegisterActionName;->CRM_ADD_CUSTOMER_TO_SALE:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {p0}, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;->getDetailFromSearch()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/analytics/event/v1/DetailEvent;-><init>(Lcom/squareup/analytics/RegisterActionName;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 257
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v0, Lcom/squareup/crm/events/CrmAddCustomerToSaleEvent;

    invoke-direct {p0}, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;->getDetailFromRecent()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/crm/events/CrmAddCustomerToSaleEvent;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    .line 258
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Presenter;->runner:Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Runner;

    invoke-interface {p1, p2}, Lcom/squareup/ui/crm/v2/ChooseCustomer2ScreenV2$Runner;->closeChooseCustomerScreen(Lcom/squareup/protos/client/rolodex/Contact;)V

    return-void
.end method
