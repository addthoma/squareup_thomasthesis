.class public final Lcom/squareup/ui/crm/edit/EditCustomerViewFactory_Factory;
.super Ljava/lang/Object;
.source "EditCustomerViewFactory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/crm/edit/EditCustomerViewFactory;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumLayoutRunner$Factory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumLayoutRunner$Factory;",
            ">;)V"
        }
    .end annotation

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/squareup/ui/crm/edit/EditCustomerViewFactory_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 20
    iput-object p2, p0, Lcom/squareup/ui/crm/edit/EditCustomerViewFactory_Factory;->arg1Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/crm/edit/EditCustomerViewFactory_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumLayoutRunner$Factory;",
            ">;)",
            "Lcom/squareup/ui/crm/edit/EditCustomerViewFactory_Factory;"
        }
    .end annotation

    .line 31
    new-instance v0, Lcom/squareup/ui/crm/edit/EditCustomerViewFactory_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/edit/EditCustomerViewFactory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$Factory;Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumLayoutRunner$Factory;)Lcom/squareup/ui/crm/edit/EditCustomerViewFactory;
    .locals 1

    .line 36
    new-instance v0, Lcom/squareup/ui/crm/edit/EditCustomerViewFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/edit/EditCustomerViewFactory;-><init>(Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$Factory;Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumLayoutRunner$Factory;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/crm/edit/EditCustomerViewFactory;
    .locals 2

    .line 25
    iget-object v0, p0, Lcom/squareup/ui/crm/edit/EditCustomerViewFactory_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$Factory;

    iget-object v1, p0, Lcom/squareup/ui/crm/edit/EditCustomerViewFactory_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumLayoutRunner$Factory;

    invoke-static {v0, v1}, Lcom/squareup/ui/crm/edit/EditCustomerViewFactory_Factory;->newInstance(Lcom/squareup/ui/crm/edit/EditCustomerLayoutRunner$Factory;Lcom/squareup/ui/crm/edit/enums/EditCustomerEnumLayoutRunner$Factory;)Lcom/squareup/ui/crm/edit/EditCustomerViewFactory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/ui/crm/edit/EditCustomerViewFactory_Factory;->get()Lcom/squareup/ui/crm/edit/EditCustomerViewFactory;

    move-result-object v0

    return-object v0
.end method
