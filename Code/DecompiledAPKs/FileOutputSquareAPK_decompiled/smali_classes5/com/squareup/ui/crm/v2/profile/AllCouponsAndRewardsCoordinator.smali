.class public Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "AllCouponsAndRewardsCoordinator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter;
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private adapter:Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter;

.field private final formatter:Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;

.field private holdsCoupons:Lcom/squareup/checkout/HoldsCoupons;

.field private final locale:Ljava/util/Locale;

.field private recyclerView:Landroidx/recyclerview/widget/RecyclerView;

.field private final res:Lcom/squareup/util/Res;

.field private final runner:Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsScreen$Runner;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsScreen$Runner;Lcom/squareup/util/Res;Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;Ljava/util/Locale;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 50
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    .line 51
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator;->runner:Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsScreen$Runner;

    .line 52
    iput-object p2, p0, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator;->res:Lcom/squareup/util/Res;

    .line 53
    iput-object p3, p0, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator;->formatter:Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;

    .line 54
    iput-object p4, p0, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator;->locale:Ljava/util/Locale;

    .line 55
    invoke-interface {p1}, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsScreen$Runner;->getHoldsCoupons()Lcom/squareup/checkout/HoldsCoupons;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator;->holdsCoupons:Lcom/squareup/checkout/HoldsCoupons;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator;)Lcom/squareup/checkout/HoldsCoupons;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator;->holdsCoupons:Lcom/squareup/checkout/HoldsCoupons;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator;)Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator;->formatter:Lcom/squareup/ui/library/coupon/CouponDiscountFormatter;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator;)Lcom/squareup/util/Res;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator;->res:Lcom/squareup/util/Res;

    return-object p0
.end method

.method static synthetic access$500(Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator;)Ljava/util/Locale;
    .locals 0

    .line 37
    iget-object p0, p0, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator;->locale:Ljava/util/Locale;

    return-object p0
.end method

.method private bindViews(Landroid/view/View;)V
    .locals 1

    .line 84
    invoke-static {p1}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 85
    sget v0, Lcom/squareup/crmscreens/R$id;->view_all_coupons_and_rewards_list:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView;

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    return-void
.end method

.method static synthetic lambda$null$0(Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;)Ljava/util/List;
    .locals 0

    .line 79
    iget-object p0, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;->coupon:Ljava/util/List;

    return-object p0
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 5

    .line 60
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 62
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator;->bindViews(Landroid/view/View;)V

    .line 64
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    new-instance v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v3, p0, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/crmscreens/R$string;->crm_coupons_and_rewards_view_all_title:I

    .line 66
    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 65
    invoke-virtual {v1, v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/crmscreens/R$string;->crm_coupons_and_rewards_manage_action:I

    .line 67
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator;->runner:Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsScreen$Runner;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v3, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$yw3lRwmU5nkdIg9nHVRjzfkE67g;

    invoke-direct {v3, v2}, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$yw3lRwmU5nkdIg9nHVRjzfkE67g;-><init>(Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsScreen$Runner;)V

    .line 68
    invoke-virtual {v1, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator;->runner:Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsScreen$Runner;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v3, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$u91gtAlSqrN2_NqMyCnFP7TFUF4;

    invoke-direct {v3, v2}, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$u91gtAlSqrN2_NqMyCnFP7TFUF4;-><init>(Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsScreen$Runner;)V

    .line 69
    invoke-virtual {v1, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 70
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v1

    .line 64
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 72
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator;->runner:Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsScreen$Runner;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$u91gtAlSqrN2_NqMyCnFP7TFUF4;

    invoke-direct {v1, v0}, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$u91gtAlSqrN2_NqMyCnFP7TFUF4;-><init>(Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsScreen$Runner;)V

    invoke-static {p1, v1}, Lcom/squareup/workflow/ui/HandlesBack$Helper;->setBackHandler(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 74
    new-instance v0, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter;-><init>(Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator;)V

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator;->adapter:Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter;

    .line 75
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator;->adapter:Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 77
    new-instance v0, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$AllCouponsAndRewardsCoordinator$qLZI-9BDfGcclZT3WQfrxDYWwpU;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$AllCouponsAndRewardsCoordinator$qLZI-9BDfGcclZT3WQfrxDYWwpU;-><init>(Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public synthetic lambda$attach$1$AllCouponsAndRewardsCoordinator()Lrx/Subscription;
    .locals 3

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator;->runner:Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsScreen$Runner;->loyaltyStatus()Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$AllCouponsAndRewardsCoordinator$s9H3ZIxnL6pB9eRNlt9JLv98pWA;->INSTANCE:Lcom/squareup/ui/crm/v2/profile/-$$Lambda$AllCouponsAndRewardsCoordinator$s9H3ZIxnL6pB9eRNlt9JLv98pWA;

    .line 79
    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator;->adapter:Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$kQfVvRaIBvaewdjoNa9lNcwIRg8;

    invoke-direct {v2, v1}, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$kQfVvRaIBvaewdjoNa9lNcwIRg8;-><init>(Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsCoordinator$AllCouponsAndRewardsAdapter;)V

    .line 80
    invoke-virtual {v0, v2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method
