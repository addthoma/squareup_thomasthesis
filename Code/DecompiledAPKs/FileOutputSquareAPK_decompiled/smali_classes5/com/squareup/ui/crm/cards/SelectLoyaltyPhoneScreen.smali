.class public Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneScreen;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "SelectLoyaltyPhoneScreen.java"

# interfaces
.implements Lcom/squareup/coordinators/CoordinatorProvider;
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneScreen$Runner;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final parentKey:Lcom/squareup/ui/main/RegisterTreeKey;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 69
    sget-object v0, Lcom/squareup/ui/crm/cards/-$$Lambda$SelectLoyaltyPhoneScreen$SrD3YM6bWPrXcSF0HC9GFZIBv78;->INSTANCE:Lcom/squareup/ui/crm/cards/-$$Lambda$SelectLoyaltyPhoneScreen$SrD3YM6bWPrXcSF0HC9GFZIBv78;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/main/RegisterTreeKey;)V
    .locals 0

    .line 28
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneScreen;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneScreen;
    .locals 1

    .line 71
    const-class v0, Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/main/RegisterTreeKey;

    .line 72
    new-instance v0, Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 65
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/main/RegisterTreeKey;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 66
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneScreen;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 61
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_SELECT_LOYALTY_PHONE_NUMBER:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getParentKey()Ljava/lang/Object;
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneScreen;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    return-object v0
.end method

.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 1

    .line 41
    const-class v0, Lcom/squareup/ui/crm/applet/SelectCustomersScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/applet/SelectCustomersScope$Component;

    invoke-interface {p1}, Lcom/squareup/ui/crm/applet/SelectCustomersScope$Component;->selectLoyaltyPhoneCoordinator()Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneCoordinator;

    move-result-object p1

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 37
    sget v0, Lcom/squareup/crm/applet/R$layout;->crm_select_loyalty_phone_number_card_view:I

    return v0
.end method
