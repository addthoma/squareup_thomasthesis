.class Lcom/squareup/ui/crm/v2/profile/RewardsSectionPresenter$2;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "RewardsSectionPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/v2/profile/RewardsSectionPresenter;->lambda$null$3(Lcom/squareup/ui/account/view/SmartLineRow;Lcom/squareup/protos/client/coupons/Coupon;Ljava/lang/Boolean;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/crm/v2/profile/RewardsSectionPresenter;

.field final synthetic val$coupon:Lcom/squareup/protos/client/coupons/Coupon;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/v2/profile/RewardsSectionPresenter;Lcom/squareup/protos/client/coupons/Coupon;)V
    .locals 0

    .line 140
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/RewardsSectionPresenter$2;->this$0:Lcom/squareup/ui/crm/v2/profile/RewardsSectionPresenter;

    iput-object p2, p0, Lcom/squareup/ui/crm/v2/profile/RewardsSectionPresenter$2;->val$coupon:Lcom/squareup/protos/client/coupons/Coupon;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 1

    .line 142
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/profile/RewardsSectionPresenter$2;->this$0:Lcom/squareup/ui/crm/v2/profile/RewardsSectionPresenter;

    invoke-static {p1}, Lcom/squareup/ui/crm/v2/profile/RewardsSectionPresenter;->access$000(Lcom/squareup/ui/crm/v2/profile/RewardsSectionPresenter;)Lcom/squareup/checkout/HoldsCoupons;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/RewardsSectionPresenter$2;->val$coupon:Lcom/squareup/protos/client/coupons/Coupon;

    invoke-interface {p1, v0}, Lcom/squareup/checkout/HoldsCoupons;->apply(Lcom/squareup/protos/client/coupons/Coupon;)V

    return-void
.end method
