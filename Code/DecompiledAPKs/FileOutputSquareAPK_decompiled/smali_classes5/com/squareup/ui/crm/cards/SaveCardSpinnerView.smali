.class public Lcom/squareup/ui/crm/cards/SaveCardSpinnerView;
.super Landroid/widget/ScrollView;
.source "SaveCardSpinnerView.java"

# interfaces
.implements Lcom/squareup/workflow/ui/HandlesBack;


# instance fields
.field private confirmButton:Lcom/squareup/marketfont/MarketButton;

.field private messageView:Lcom/squareup/widgets/MessageView;

.field presenter:Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private spinnerGlyph:Lcom/squareup/marin/widgets/MarinSpinnerGlyph;

.field private titleView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 30
    invoke-direct {p0, p1, p2}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31
    const-class p2, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Component;->inject(Lcom/squareup/ui/crm/cards/SaveCardSpinnerView;)V

    return-void
.end method

.method private bindViews()V
    .locals 1

    .line 102
    sget v0, Lcom/squareup/crm/R$id;->spinner_glyph:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerView;->spinnerGlyph:Lcom/squareup/marin/widgets/MarinSpinnerGlyph;

    .line 103
    sget v0, Lcom/squareup/crm/R$id;->spinner_title:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerView;->titleView:Landroid/widget/TextView;

    .line 104
    sget v0, Lcom/squareup/crm/R$id;->spinner_message:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerView;->messageView:Lcom/squareup/widgets/MessageView;

    .line 105
    sget v0, Lcom/squareup/crm/R$id;->confirm_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketButton;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerView;->confirmButton:Lcom/squareup/marketfont/MarketButton;

    return-void
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .locals 1

    .line 40
    invoke-super {p0}, Landroid/widget/ScrollView;->onAttachedToWindow()V

    .line 41
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerView;->presenter:Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerView;->presenter:Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->onBackPressed()Z

    move-result v0

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerView;->presenter:Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/cards/SaveCardSpinnerScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 46
    invoke-super {p0}, Landroid/widget/ScrollView;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 0

    .line 35
    invoke-super {p0}, Landroid/widget/ScrollView;->onFinishInflate()V

    .line 36
    invoke-direct {p0}, Lcom/squareup/ui/crm/cards/SaveCardSpinnerView;->bindViews()V

    return-void
.end method

.method setText(Ljava/lang/String;Ljava/lang/CharSequence;)V
    .locals 1

    const-string/jumbo v0, "title"

    .line 54
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonBlank(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/CharSequence;

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerView;->titleView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 57
    invoke-static {p2}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 58
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerView;->messageView:Lcom/squareup/widgets/MessageView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    .line 59
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerView;->messageView:Lcom/squareup/widgets/MessageView;

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 61
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerView;->messageView:Lcom/squareup/widgets/MessageView;

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method setToFailure()V
    .locals 1

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerView;->spinnerGlyph:Lcom/squareup/marin/widgets/MarinSpinnerGlyph;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->setToFailure()V

    return-void
.end method

.method setToSuccess()V
    .locals 1

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerView;->spinnerGlyph:Lcom/squareup/marin/widgets/MarinSpinnerGlyph;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->setToSuccess()V

    return-void
.end method

.method showDoneButton()V
    .locals 2

    .line 92
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerView;->confirmButton:Lcom/squareup/marketfont/MarketButton;

    sget v1, Lcom/squareup/common/strings/R$string;->done:I

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketButton;->setText(I)V

    .line 93
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerView;->confirmButton:Lcom/squareup/marketfont/MarketButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketButton;->setVisibility(I)V

    .line 94
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerView;->confirmButton:Lcom/squareup/marketfont/MarketButton;

    new-instance v1, Lcom/squareup/ui/crm/cards/SaveCardSpinnerView$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/SaveCardSpinnerView$2;-><init>(Lcom/squareup/ui/crm/cards/SaveCardSpinnerView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method showOkButton()V
    .locals 2

    .line 82
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerView;->confirmButton:Lcom/squareup/marketfont/MarketButton;

    sget v1, Lcom/squareup/common/strings/R$string;->ok:I

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketButton;->setText(I)V

    .line 83
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerView;->confirmButton:Lcom/squareup/marketfont/MarketButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketButton;->setVisibility(I)V

    .line 84
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerView;->confirmButton:Lcom/squareup/marketfont/MarketButton;

    new-instance v1, Lcom/squareup/ui/crm/cards/SaveCardSpinnerView$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/SaveCardSpinnerView$1;-><init>(Lcom/squareup/ui/crm/cards/SaveCardSpinnerView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method transitionToFailure()V
    .locals 1

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerView;->spinnerGlyph:Lcom/squareup/marin/widgets/MarinSpinnerGlyph;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->transitionToFailure()V

    return-void
.end method

.method transitionToSuccess()V
    .locals 1

    .line 66
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardSpinnerView;->spinnerGlyph:Lcom/squareup/marin/widgets/MarinSpinnerGlyph;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinSpinnerGlyph;->transitionToSuccess()V

    return-void
.end method
