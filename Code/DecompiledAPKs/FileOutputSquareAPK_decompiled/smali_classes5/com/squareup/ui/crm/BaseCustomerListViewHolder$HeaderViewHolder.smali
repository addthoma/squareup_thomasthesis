.class public final Lcom/squareup/ui/crm/BaseCustomerListViewHolder$HeaderViewHolder;
.super Lcom/squareup/ui/crm/BaseCustomerListViewHolder;
.source "BaseCustomerListViewHolder.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/BaseCustomerListViewHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "HeaderViewHolder"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\u0016R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/ui/crm/BaseCustomerListViewHolder$HeaderViewHolder;",
        "Lcom/squareup/ui/crm/BaseCustomerListViewHolder;",
        "view",
        "Landroid/view/View;",
        "(Landroid/view/View;)V",
        "rowText",
        "Landroid/widget/TextView;",
        "bind",
        "",
        "item",
        "Lcom/squareup/ui/crm/BaseCustomerListItem;",
        "crm-choose-customer_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final rowText:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 33
    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/crm/BaseCustomerListViewHolder;-><init>(Landroid/view/View;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 34
    sget v0, Lcom/squareup/crmchoosecustomer/R$id;->crm_list_header_row_text:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string/jumbo v0, "view.findViewById(R.id.crm_list_header_row_text)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/ui/crm/BaseCustomerListViewHolder$HeaderViewHolder;->rowText:Landroid/widget/TextView;

    return-void
.end method


# virtual methods
.method public bind(Lcom/squareup/ui/crm/BaseCustomerListItem;)V
    .locals 1

    const-string v0, "item"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    iget-object v0, p0, Lcom/squareup/ui/crm/BaseCustomerListViewHolder$HeaderViewHolder;->rowText:Landroid/widget/TextView;

    check-cast p1, Lcom/squareup/ui/crm/BaseCustomerListItem$HeaderItem;

    invoke-virtual {p1}, Lcom/squareup/ui/crm/BaseCustomerListItem$HeaderItem;->getStr()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
