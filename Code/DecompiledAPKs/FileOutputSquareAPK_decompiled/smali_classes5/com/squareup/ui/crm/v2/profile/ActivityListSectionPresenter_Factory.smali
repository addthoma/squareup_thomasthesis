.class public final Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter_Factory;
.super Ljava/lang/Object;
.source "ActivityListSectionPresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final customerActivityHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/cards/CustomerActivityHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final dateFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field

.field private final eventLoaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexEventLoader;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final timeFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/cards/CustomerActivityHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexEventLoader;",
            ">;)V"
        }
    .end annotation

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter_Factory;->customerActivityHelperProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p2, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter_Factory;->dateFormatterProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p3, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter_Factory;->timeFormatterProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p4, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter_Factory;->localeProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p5, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p6, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter_Factory;->eventLoaderProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/cards/CustomerActivityHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/text/DateFormat;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/RolodexEventLoader;",
            ">;)",
            "Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter_Factory;"
        }
    .end annotation

    .line 56
    new-instance v7, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Lcom/squareup/ui/crm/cards/CustomerActivityHelper;Ljava/text/DateFormat;Ljava/text/DateFormat;Ljava/util/Locale;Lcom/squareup/settings/server/Features;Lcom/squareup/crm/RolodexEventLoader;)Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;
    .locals 8

    .line 62
    new-instance v7, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;-><init>(Lcom/squareup/ui/crm/cards/CustomerActivityHelper;Ljava/text/DateFormat;Ljava/text/DateFormat;Ljava/util/Locale;Lcom/squareup/settings/server/Features;Lcom/squareup/crm/RolodexEventLoader;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;
    .locals 7

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter_Factory;->customerActivityHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/ui/crm/cards/CustomerActivityHelper;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter_Factory;->dateFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Ljava/text/DateFormat;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter_Factory;->timeFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Ljava/text/DateFormat;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter_Factory;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Ljava/util/Locale;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter_Factory;->eventLoaderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/crm/RolodexEventLoader;

    invoke-static/range {v1 .. v6}, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter_Factory;->newInstance(Lcom/squareup/ui/crm/cards/CustomerActivityHelper;Ljava/text/DateFormat;Ljava/text/DateFormat;Ljava/util/Locale;Lcom/squareup/settings/server/Features;Lcom/squareup/crm/RolodexEventLoader;)Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter_Factory;->get()Lcom/squareup/ui/crm/v2/profile/ActivityListSectionPresenter;

    move-result-object v0

    return-object v0
.end method
