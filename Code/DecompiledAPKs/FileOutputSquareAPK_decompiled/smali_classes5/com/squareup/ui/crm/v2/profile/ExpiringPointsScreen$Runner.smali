.class public interface abstract Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen$Runner;
.super Ljava/lang/Object;
.source "ExpiringPointsScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation


# virtual methods
.method public abstract closeExpiringPointsScreen()V
.end method

.method public abstract expiringPointsScreenData()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen$ScreenData;",
            ">;"
        }
    .end annotation
.end method
