.class Lcom/squareup/ui/crm/cards/contact/ContactListViewV2$1;
.super Ljava/lang/Object;
.source "ContactListViewV2.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;)V
    .locals 0

    .line 67
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2$1;->this$0:Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 1

    add-int/2addr p3, p2

    const/4 p1, 0x1

    sub-int/2addr p4, p1

    .line 76
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2$1;->this$0:Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;

    invoke-static {v0}, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->access$000(Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {v0, p2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 77
    iget-object p2, p0, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2$1;->this$0:Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;

    invoke-static {p2}, Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;->access$100(Lcom/squareup/ui/crm/cards/contact/ContactListViewV2;)Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object p2

    add-int/lit8 p4, p4, -0x32

    if-lt p3, p4, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/jakewharton/rxrelay/PublishRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    return-void
.end method
