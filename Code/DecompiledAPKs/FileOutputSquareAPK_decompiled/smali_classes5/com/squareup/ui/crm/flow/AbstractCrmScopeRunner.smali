.class public abstract Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;
.super Lcom/squareup/ui/crm/flow/CrmScope$BaseRunner;
.source "AbstractCrmScopeRunner.java"

# interfaces
.implements Lcom/squareup/ui/crm/v2/ViewCustomerCoordinator$Runner;
.implements Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Runner;
.implements Lcom/squareup/ui/crm/cards/CreateNoteScreen$Runner;
.implements Lcom/squareup/ui/crm/cards/AllNotesScreen$Runner;
.implements Lcom/squareup/ui/crm/cards/ViewNoteScreen$Runner;
.implements Lcom/squareup/ui/crm/cards/AllFrequentItemsScreen$Runner;
.implements Lcom/squareup/ui/crm/cards/SendMessageScreen$Runner;
.implements Lcom/squareup/ui/crm/cards/ConversationCardScreen$Runner;
.implements Lcom/squareup/ui/crm/cards/AddCouponScreen$Runner;
.implements Lcom/squareup/ui/crm/cards/ReminderScreen$Runner;
.implements Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Runner;
.implements Lcom/squareup/ui/crm/cards/CustomerInvoiceScreen$Runner;
.implements Lcom/squareup/invoices/ui/CrmInvoiceListRunner;
.implements Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneScreen$Runner;
.implements Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneConflictDialogScreen$Runner;
.implements Lcom/squareup/ui/crm/v2/DeleteSingleCustomerConfirmationScreen$Runner;
.implements Lcom/squareup/ui/crm/cards/RemovingCardOnFileScreen$Runner;
.implements Lcom/squareup/ui/crm/v2/profile/LoyaltySectionBottomDialogScreen$Runner;
.implements Lcom/squareup/ui/crm/cards/loyalty/ConfirmSendLoyaltyStatusDialogScreen$Runner;
.implements Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersScreen$Runner;
.implements Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen$Runner;
.implements Lcom/squareup/ui/crm/v2/DeleteLoyaltyAccountConfirmationScreen$Runner;
.implements Lcom/squareup/ui/crm/cards/DeletingLoyaltyAccountScreen$Runner;
.implements Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountScreen$Runner;
.implements Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsScreen$Runner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$ContactLoadingState;
    }
.end annotation


# static fields
.field protected static final EMPTY_LOYALTY_ACCOUNT_MAPPING:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;


# instance fields
.field private final accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

.field protected final addCardOnFileFlow:Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;

.field private final adjustPointsFlow:Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;

.field protected final analytics:Lcom/squareup/analytics/Analytics;

.field private final appointmentLinkingHandler:Lcom/squareup/appointmentsapi/AppointmentLinkingHandler;

.field private final baseContact:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;"
        }
    .end annotation
.end field

.field private final billHistoryFlow:Lcom/squareup/ui/crm/flow/BillHistoryFlow;

.field private final busyWithRedeemRewardTierRpc:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field protected final cameraHelper:Lcom/squareup/camerahelper/CameraHelper;

.field protected final chooseCustomerFlow:Lcom/squareup/ui/crm/ChooseCustomerFlow;

.field protected conflictingLoyaltyContact:Lcom/squareup/protos/client/rolodex/Contact;

.field protected conflictingLoyaltyPhoneNumber:Ljava/lang/String;

.field protected contact:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;"
        }
    .end annotation
.end field

.field protected final contactLoadingState:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$ContactLoadingState;",
            ">;"
        }
    .end annotation
.end field

.field protected crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

.field private final currentInvoice:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;",
            ">;>;"
        }
    .end annotation
.end field

.field private final currentInvoiceId:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

.field protected final features:Lcom/squareup/settings/server/Features;

.field protected final flow:Lflow/Flow;

.field protected holdsCoupons:Lcom/squareup/checkout/HoldsCoupons;

.field protected holdsCustomer:Lcom/squareup/payment/crm/HoldsCustomer;

.field private instrumentForUnlinkInstrumentDialog:Lcom/squareup/protos/client/instruments/InstrumentSummary;

.field private final invoiceService:Lcom/squareup/invoices/ClientInvoiceServiceHelper;

.field protected final invoicesCustomerLoader:Lcom/squareup/invoices/InvoicesCustomerLoader;

.field protected final loyalty:Lcom/squareup/loyalty/LoyaltyServiceHelper;

.field protected final loyaltyLoadingState:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$ContactLoadingState;",
            ">;"
        }
    .end annotation
.end field

.field protected final loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

.field protected loyaltyStatus:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final mainContainer:Lcom/squareup/ui/main/PosContainer;

.field private final manageCouponFlow:Lcom/squareup/ui/crm/v2/flow/ManageCouponsAndRewardsFlow;

.field protected final mergeCustomersFlow:Lcom/squareup/ui/crm/flow/MergeCustomersFlow;

.field protected noteForViewNoteScreen:Lcom/squareup/protos/client/rolodex/Note;

.field private final onboardingDiverter:Lcom/squareup/onboarding/OnboardingDiverter;

.field protected final permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

.field private final phoneHelper:Lcom/squareup/text/PhoneNumberHelper;

.field protected final profileAttachmentsVisibility:Lcom/squareup/ui/crm/ProfileAttachmentsVisibility;

.field protected redeemRewardTierRpcSubscription:Lrx/Subscription;

.field protected reminder:Lcom/squareup/protos/client/rolodex/Reminder;

.field private requestGetLoyaltyStatus:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected final res:Lcom/squareup/util/Res;

.field private final retrofitQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

.field protected final rolodex:Lcom/squareup/crm/RolodexServiceHelper;

.field protected saveLoyaltyPhoneInProgress:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field protected sendMessageScreenMessage:Ljava/lang/String;

.field private tokenForConversationScreen:Ljava/lang/String;

.field protected unlinkInstrumentSubscription:Lrx/Subscription;

.field protected final updateCustomerFlow:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;

.field protected updateLoyaltyPhoneError:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field protected final viewCustomerConfiguration:Lcom/squareup/crm/viewcustomerconfiguration/api/ViewCustomerConfiguration;

.field protected final x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 246
    new-instance v0, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Builder;-><init>()V

    .line 247
    invoke-virtual {v0}, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Builder;->build()Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->EMPTY_LOYALTY_ACCOUNT_MAPPING:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    return-void
.end method

.method protected constructor <init>(Lcom/squareup/crm/RolodexServiceHelper;Lcom/squareup/loyalty/LoyaltyServiceHelper;Lcom/squareup/settings/server/Features;Lcom/squareup/analytics/Analytics;Lflow/Flow;Lcom/squareup/util/Res;Lcom/squareup/text/PhoneNumberHelper;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/crm/flow/BillHistoryFlow;Lcom/squareup/invoices/ClientInvoiceServiceHelper;Lcom/squareup/ui/crm/v2/flow/ManageCouponsAndRewardsFlow;Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;Lcom/squareup/ui/crm/flow/MergeCustomersFlow;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/onboarding/OnboardingDiverter;Lcom/squareup/crm/viewcustomerconfiguration/api/ViewCustomerConfiguration;Lcom/squareup/invoices/InvoicesCustomerLoader;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/appointmentsapi/AppointmentLinkingHandler;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/ui/crm/ProfileAttachmentsVisibility;Lcom/squareup/camerahelper/CameraHelper;Lcom/squareup/ui/crm/ChooseCustomerFlow;)V
    .locals 3

    move-object v0, p0

    .line 330
    invoke-direct {p0}, Lcom/squareup/ui/crm/flow/CrmScope$BaseRunner;-><init>()V

    const/4 v1, 0x0

    .line 224
    iput-object v1, v0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->instrumentForUnlinkInstrumentDialog:Lcom/squareup/protos/client/instruments/InstrumentSummary;

    .line 225
    invoke-static {}, Lrx/subscriptions/Subscriptions;->empty()Lrx/Subscription;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->unlinkInstrumentSubscription:Lrx/Subscription;

    .line 227
    invoke-static {}, Lrx/subscriptions/Subscriptions;->empty()Lrx/Subscription;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->redeemRewardTierRpcSubscription:Lrx/Subscription;

    const/4 v2, 0x0

    .line 233
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->busyWithRedeemRewardTierRpc:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 235
    iput-object v1, v0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->noteForViewNoteScreen:Lcom/squareup/protos/client/rolodex/Note;

    .line 236
    iput-object v1, v0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->tokenForConversationScreen:Ljava/lang/String;

    .line 238
    invoke-static {}, Lcom/squareup/util/Optional;->empty()Lcom/squareup/util/Optional;

    move-result-object v2

    invoke-static {v2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->currentInvoice:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 239
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->currentInvoiceId:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 242
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->saveLoyaltyPhoneInProgress:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 243
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->updateLoyaltyPhoneError:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 253
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->baseContact:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 259
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->contact:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 268
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->loyaltyStatus:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 274
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->requestGetLoyaltyStatus:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 279
    sget-object v2, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$ContactLoadingState;->UNINITIALIZED:Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$ContactLoadingState;

    .line 280
    invoke-static {v2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->contactLoadingState:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 283
    sget-object v2, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$ContactLoadingState;->UNINITIALIZED:Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$ContactLoadingState;

    .line 284
    invoke-static {v2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create(Ljava/lang/Object;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v2

    iput-object v2, v0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->loyaltyLoadingState:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 301
    iput-object v1, v0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->reminder:Lcom/squareup/protos/client/rolodex/Reminder;

    move-object v1, p1

    .line 331
    iput-object v1, v0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    move-object v1, p2

    .line 332
    iput-object v1, v0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->loyalty:Lcom/squareup/loyalty/LoyaltyServiceHelper;

    move-object v1, p3

    .line 333
    iput-object v1, v0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->features:Lcom/squareup/settings/server/Features;

    move-object v1, p4

    .line 334
    iput-object v1, v0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    move-object v1, p5

    .line 335
    iput-object v1, v0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->flow:Lflow/Flow;

    move-object v1, p6

    .line 336
    iput-object v1, v0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->res:Lcom/squareup/util/Res;

    move-object v1, p7

    .line 337
    iput-object v1, v0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->phoneHelper:Lcom/squareup/text/PhoneNumberHelper;

    move-object v1, p8

    .line 338
    iput-object v1, v0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->retrofitQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    move-object v1, p9

    .line 339
    iput-object v1, v0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    move-object v1, p10

    .line 340
    iput-object v1, v0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    move-object v1, p11

    .line 341
    iput-object v1, v0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->billHistoryFlow:Lcom/squareup/ui/crm/flow/BillHistoryFlow;

    move-object/from16 v1, p13

    .line 342
    iput-object v1, v0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->manageCouponFlow:Lcom/squareup/ui/crm/v2/flow/ManageCouponsAndRewardsFlow;

    move-object/from16 v1, p14

    .line 343
    iput-object v1, v0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->addCardOnFileFlow:Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;

    move-object/from16 v1, p15

    .line 344
    iput-object v1, v0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->mergeCustomersFlow:Lcom/squareup/ui/crm/flow/MergeCustomersFlow;

    move-object v1, p12

    .line 345
    iput-object v1, v0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->invoiceService:Lcom/squareup/invoices/ClientInvoiceServiceHelper;

    move-object/from16 v1, p16

    .line 346
    iput-object v1, v0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    move-object/from16 v1, p17

    .line 347
    iput-object v1, v0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->onboardingDiverter:Lcom/squareup/onboarding/OnboardingDiverter;

    move-object/from16 v1, p18

    .line 348
    iput-object v1, v0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->viewCustomerConfiguration:Lcom/squareup/crm/viewcustomerconfiguration/api/ViewCustomerConfiguration;

    move-object/from16 v1, p19

    .line 349
    iput-object v1, v0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->invoicesCustomerLoader:Lcom/squareup/invoices/InvoicesCustomerLoader;

    move-object/from16 v1, p20

    .line 350
    iput-object v1, v0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    move-object/from16 v1, p21

    .line 351
    iput-object v1, v0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->appointmentLinkingHandler:Lcom/squareup/appointmentsapi/AppointmentLinkingHandler;

    move-object/from16 v1, p22

    .line 352
    iput-object v1, v0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->mainContainer:Lcom/squareup/ui/main/PosContainer;

    move-object/from16 v1, p23

    .line 353
    iput-object v1, v0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->updateCustomerFlow:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;

    move-object/from16 v1, p24

    .line 354
    iput-object v1, v0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->adjustPointsFlow:Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;

    move-object/from16 v1, p25

    .line 355
    iput-object v1, v0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    move-object/from16 v1, p26

    .line 356
    iput-object v1, v0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->profileAttachmentsVisibility:Lcom/squareup/ui/crm/ProfileAttachmentsVisibility;

    move-object/from16 v1, p27

    .line 357
    iput-object v1, v0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->cameraHelper:Lcom/squareup/camerahelper/CameraHelper;

    move-object/from16 v1, p28

    .line 358
    iput-object v1, v0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->chooseCustomerFlow:Lcom/squareup/ui/crm/ChooseCustomerFlow;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;)Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;
    .locals 0

    .line 161
    iget-object p0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->adjustPointsFlow:Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;)Lcom/squareup/ui/crm/v2/flow/ManageCouponsAndRewardsFlow;
    .locals 0

    .line 161
    iget-object p0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->manageCouponFlow:Lcom/squareup/ui/crm/v2/flow/ManageCouponsAndRewardsFlow;

    return-object p0
.end method

.method private getContactFromRolodex(Ljava/lang/String;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/rolodex/GetContactResponse;",
            ">;>;"
        }
    .end annotation

    .line 609
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->rolodex:Lcom/squareup/crm/RolodexServiceHelper;

    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->showProfileAttachmentsSection()Z

    move-result v1

    invoke-interface {v0, p1, v1}, Lcom/squareup/crm/RolodexServiceHelper;->getContact(Ljava/lang/String;Z)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method private getFormattedLoyaltyPhoneNumber()Ljava/lang/String;
    .locals 4

    .line 1412
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->loyaltyStatus:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;

    iget-object v0, v0, Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;->loyalty_account:Lcom/squareup/protos/client/loyalty/LoyaltyAccount;

    iget-object v0, v0, Lcom/squareup/protos/client/loyalty/LoyaltyAccount;->mappings:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    .line 1413
    iget-object v2, v1, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;->type:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Type;

    sget-object v3, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Type;->TYPE_PHONE:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Type;

    if-ne v2, v3, :cond_0

    .line 1414
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->phoneHelper:Lcom/squareup/text/PhoneNumberHelper;

    iget-object v1, v1, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;->raw_id:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/squareup/text/PhoneNumberHelper;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const-string v0, ""

    :goto_0
    return-object v0
.end method

.method static synthetic lambda$contactLoadingState$24(Lkotlin/Triple;)Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$ContactLoadingState;
    .locals 3

    .line 562
    invoke-virtual {p0}, Lkotlin/Triple;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$ContactLoadingState;

    .line 563
    invoke-virtual {p0}, Lkotlin/Triple;->getSecond()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$ContactLoadingState;

    .line 564
    invoke-virtual {p0}, Lkotlin/Triple;->getThird()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    .line 569
    sget-object v2, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$ContactLoadingState;->UNINITIALIZED:Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$ContactLoadingState;

    if-eq v0, v2, :cond_2

    sget-object v2, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$ContactLoadingState;->LOADING:Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$ContactLoadingState;

    if-eq v0, v2, :cond_2

    sget-object v2, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$ContactLoadingState;->UNINITIALIZED:Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$ContactLoadingState;

    if-eq v1, v2, :cond_2

    sget-object v2, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$ContactLoadingState;->LOADING:Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$ContactLoadingState;

    if-eq v1, v2, :cond_2

    if-eqz p0, :cond_0

    goto :goto_0

    .line 575
    :cond_0
    sget-object p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$ContactLoadingState;->VALID:Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$ContactLoadingState;

    if-ne v0, p0, :cond_1

    .line 576
    sget-object p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$ContactLoadingState;->VALID:Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$ContactLoadingState;

    return-object p0

    .line 579
    :cond_1
    sget-object p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$ContactLoadingState;->ERROR:Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$ContactLoadingState;

    return-object p0

    .line 574
    :cond_2
    :goto_0
    sget-object p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$ContactLoadingState;->LOADING:Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$ContactLoadingState;

    return-object p0
.end method

.method static synthetic lambda$getLoyaltyAccountMapping$37(Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;)Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;
    .locals 1

    .line 1200
    iget-object v0, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;->loyalty_account:Lcom/squareup/protos/client/loyalty/LoyaltyAccount;

    if-nez v0, :cond_0

    sget-object p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->EMPTY_LOYALTY_ACCOUNT_MAPPING:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    goto :goto_0

    :cond_0
    iget-object p0, p0, Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;->loyalty_account:Lcom/squareup/protos/client/loyalty/LoyaltyAccount;

    iget-object p0, p0, Lcom/squareup/protos/client/loyalty/LoyaltyAccount;->mappings:Ljava/util/List;

    const/4 v0, 0x0

    .line 1202
    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    :goto_0
    return-object p0
.end method

.method static synthetic lambda$null$19(Lcom/squareup/protos/client/invoice/GetInvoiceResponse;)Lcom/squareup/util/Optional;
    .locals 0

    .line 477
    iget-object p0, p0, Lcom/squareup/protos/client/invoice/GetInvoiceResponse;->invoice:Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;

    invoke-static {p0}, Lcom/squareup/util/Optional;->ofNullable(Ljava/lang/Object;)Lcom/squareup/util/Optional;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$null$20(Ljava/lang/Throwable;)Lcom/squareup/util/Optional;
    .locals 1

    .line 479
    instance-of v0, p0, Lretrofit/RetrofitError;

    if-eqz v0, :cond_0

    .line 480
    invoke-static {}, Lcom/squareup/util/Optional;->empty()Lcom/squareup/util/Optional;

    move-result-object p0

    return-object p0

    .line 482
    :cond_0
    new-instance v0, Lrx/exceptions/OnErrorNotImplementedException;

    invoke-direct {v0, p0}, Lrx/exceptions/OnErrorNotImplementedException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method static synthetic lambda$onEnterScope$0(Lcom/squareup/protos/client/rolodex/Contact;)Ljava/lang/String;
    .locals 0

    .line 383
    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/Contact;->contact_token:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic lambda$onEnterScope$12(Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 429
    invoke-virtual {p0}, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;->getUpdateCustomerResultKey()Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;

    move-result-object p0

    sget-object v0, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;->VIEW_CUSTOMER:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;

    if-ne p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$onEnterScope$13(Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 430
    invoke-virtual {p0}, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;->getType()Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;

    move-result-object p0

    sget-object v0, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;->EDIT:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;

    if-ne p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$onEnterScope$16(Lkotlin/Unit;Lcom/squareup/protos/client/rolodex/Contact;)Lcom/squareup/protos/client/rolodex/Contact;
    .locals 0

    return-object p1
.end method

.method static synthetic lambda$onEnterScope$22(Lcom/squareup/ui/crm/ChooseCustomerFlow$Result;)Ljava/lang/Boolean;
    .locals 1

    .line 488
    invoke-virtual {p0}, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result;->getChooseCustomerResultKey()Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;

    move-result-object p0

    sget-object v0, Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;->TRANSFER_LOYALTY:Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;

    if-ne p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$onEnterScope$6(Lcom/squareup/protos/client/rolodex/Contact;)Ljava/lang/String;
    .locals 0

    .line 405
    iget-object p0, p0, Lcom/squareup/protos/client/rolodex/Contact;->contact_token:Ljava/lang/String;

    return-object p0
.end method

.method private updateMaybeHoldsCustomers(Lcom/squareup/protos/client/rolodex/Contact;)V
    .locals 2

    .line 629
    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->isContactAddedToHoldsCustomer(Lcom/squareup/protos/client/rolodex/Contact;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 630
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->holdsCustomer:Lcom/squareup/payment/crm/HoldsCustomer;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1, v1}, Lcom/squareup/payment/crm/HoldsCustomer;->setCustomer(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/bills/Cart$FeatureDetails$BuyerInfo;Ljava/util/List;)V

    :cond_0
    return-void
.end method


# virtual methods
.method protected abstract applyCouponToCartAndAddCustomer(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/coupons/Coupon;)V
.end method

.method public canStartNewSaleWithCustomerFromApplet()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public canViewExpiringPoints()Z
    .locals 2

    .line 883
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->LOYALTY_EXPIRE_POINTS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    .line 884
    invoke-interface {v0}, Lcom/squareup/loyalty/LoyaltySettings;->getExpirationPolicy()Lcom/squareup/server/account/protos/ExpirationPolicy;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method protected cancelAddCardOnFile()V
    .locals 2

    .line 617
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->cancelSaveCard()Z

    .line 618
    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->getPathType()Lcom/squareup/ui/crm/flow/CrmScopeType;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/flow/CrmScopeType;->X2_SAVE_CARD_POST_TRANSACTION:Lcom/squareup/ui/crm/flow/CrmScopeType;

    if-eq v0, v1, :cond_0

    .line 619
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    :cond_0
    return-void
.end method

.method public cancelDeleteLoyaltyAccount()V
    .locals 4

    .line 898
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->flow:Lflow/Flow;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const-class v2, Lcom/squareup/ui/crm/v2/DeleteLoyaltyAccountConfirmationScreen;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    return-void
.end method

.method public cancelSeeAllRewardTiersScreen()V
    .locals 2

    .line 1312
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public cancelUpdateLoyaltyPhoneConflictScreen()V
    .locals 4

    .line 1223
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->flow:Lflow/Flow;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Class;

    const-class v2, Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneScreen;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-class v2, Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneConflictDialogScreen;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    return-void
.end method

.method public clearCurrentInvoice()V
    .locals 2

    .line 776
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->currentInvoiceId:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public closeAddCouponScreen(Lcom/squareup/api/items/Discount;)V
    .locals 1

    .line 1029
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->flow:Lflow/Flow;

    const-class v0, Lcom/squareup/ui/crm/cards/AddCouponScreen;

    invoke-static {p1, v0}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public closeAllAppointmentsScreen()V
    .locals 2

    .line 725
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/crm/cards/AllAppointmentsScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public closeAllCouponsAndRewardsScreen()V
    .locals 2

    .line 1048
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public closeAllFrequentItemsScreen()V
    .locals 2

    .line 865
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/crm/cards/AllFrequentItemsScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public closeAllNotesScreen()V
    .locals 2

    .line 829
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/crm/cards/AllNotesScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public closeConversationScreen()V
    .locals 2

    const/4 v0, 0x0

    .line 1062
    iput-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->tokenForConversationScreen:Ljava/lang/String;

    .line 1063
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/crm/cards/ConversationCardScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public closeCreateNoteScreen()V
    .locals 2

    const/4 v0, 0x0

    .line 841
    iput-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->reminder:Lcom/squareup/protos/client/rolodex/Reminder;

    .line 842
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/crm/cards/CreateNoteScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public closeCreateNoteScreen(Lcom/squareup/protos/client/rolodex/Contact;)V
    .locals 2

    const/4 v0, 0x0

    .line 846
    iput-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->reminder:Lcom/squareup/protos/client/rolodex/Reminder;

    .line 847
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/crm/cards/CreateNoteScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    .line 849
    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->setLoadedContact(Lcom/squareup/protos/client/rolodex/Contact;)V

    return-void
.end method

.method public closeCustomerInvoiceScreen()V
    .locals 2

    .line 761
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CUSTOMER_INVOICE_LINKING:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    invoke-static {v0}, Lcom/squareup/util/Preconditions;->checkState(Z)V

    .line 762
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/crm/cards/CustomerInvoiceScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public closeDeletingLoyaltyAccountScreen()V
    .locals 2

    .line 910
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/crm/cards/DeletingLoyaltyAccountScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public closeExpiringPointsScreen()V
    .locals 2

    .line 1407
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public closeReminderScreen()V
    .locals 2

    .line 1090
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/crm/cards/ReminderScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public closeReminderScreen(Lcom/squareup/protos/client/rolodex/Reminder;)V
    .locals 1

    .line 1094
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->noteForViewNoteScreen:Lcom/squareup/protos/client/rolodex/Note;

    if-eqz v0, :cond_0

    .line 1095
    invoke-virtual {v0}, Lcom/squareup/protos/client/rolodex/Note;->newBuilder()Lcom/squareup/protos/client/rolodex/Note$Builder;

    move-result-object v0

    .line 1096
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/rolodex/Note$Builder;->reminder(Lcom/squareup/protos/client/rolodex/Reminder;)Lcom/squareup/protos/client/rolodex/Note$Builder;

    move-result-object v0

    .line 1097
    invoke-virtual {v0}, Lcom/squareup/protos/client/rolodex/Note$Builder;->build()Lcom/squareup/protos/client/rolodex/Note;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->noteForViewNoteScreen:Lcom/squareup/protos/client/rolodex/Note;

    .line 1099
    :cond_0
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->reminder:Lcom/squareup/protos/client/rolodex/Reminder;

    .line 1100
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->flow:Lflow/Flow;

    const-class v0, Lcom/squareup/ui/crm/cards/ReminderScreen;

    invoke-static {p1, v0}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public closeRemovingCardOnFileScreen()V
    .locals 2

    .line 695
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/crm/cards/RemovingCardOnFileScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public closeTransferringLoyaltyAccountScreen(Z)V
    .locals 4

    if-eqz p1, :cond_1

    .line 947
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    iget-object p1, p1, Lcom/squareup/ui/crm/flow/CrmScope;->type:Lcom/squareup/ui/crm/flow/CrmScopeType;

    invoke-static {p1}, Lcom/squareup/ui/crm/flow/CrmScopeTypeKt;->inAddCustomerToSale(Lcom/squareup/ui/crm/flow/CrmScopeType;)Z

    move-result p1

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    .line 950
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->flow:Lflow/Flow;

    sget-object v2, Lflow/Direction;->REPLACE:Lflow/Direction;

    new-array v0, v0, [Lkotlin/jvm/functions/Function1;

    new-instance v3, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$t7ZeG2kv6jLnyc8iwMsazXderQI;

    invoke-direct {v3, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$t7ZeG2kv6jLnyc8iwMsazXderQI;-><init>(Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;)V

    aput-object v3, v0, v1

    invoke-static {p1, v2, v0}, Lcom/squareup/container/Flows;->editHistory(Lflow/Flow;Lflow/Direction;[Lkotlin/jvm/functions/Function1;)V

    goto :goto_0

    .line 963
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->flow:Lflow/Flow;

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Class;

    const-class v3, Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountScreen;

    aput-object v3, v2, v1

    const-class v1, Lcom/squareup/ui/crm/v2/ViewCustomerCardScreen;

    aput-object v1, v2, v0

    const/4 v0, 0x2

    const-class v1, Lcom/squareup/ui/crm/ChooseCustomerScreen;

    aput-object v1, v2, v0

    const/4 v0, 0x3

    const-class v1, Lcom/squareup/ui/crm/v2/ViewCustomerDetailScreen;

    aput-object v1, v2, v0

    invoke-static {p1, v2}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public closeUpdateLoyaltyPhoneScreen()V
    .locals 2

    .line 1124
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    .line 1125
    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->requestGetLoyaltyStatus()V

    return-void
.end method

.method public closeViewNoteScreen()V
    .locals 2

    const/4 v0, 0x0

    .line 803
    iput-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->noteForViewNoteScreen:Lcom/squareup/protos/client/rolodex/Note;

    .line 804
    iput-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->reminder:Lcom/squareup/protos/client/rolodex/Reminder;

    .line 805
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/crm/cards/ViewNoteScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public closeViewNoteScreen(Lcom/squareup/protos/client/rolodex/Note;)V
    .locals 2

    const/4 v0, 0x0

    .line 809
    iput-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->reminder:Lcom/squareup/protos/client/rolodex/Reminder;

    if-eqz p1, :cond_0

    .line 811
    iget-object v1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->contact:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 812
    invoke-virtual {v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/rolodex/Contact;

    invoke-static {v1, p1}, Lcom/squareup/crm/util/RolodexContactHelper;->withUpdatedNote(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/rolodex/Note;)Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p1

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->contact:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 813
    invoke-virtual {p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/rolodex/Contact;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->noteForViewNoteScreen:Lcom/squareup/protos/client/rolodex/Note;

    iget-object v1, v1, Lcom/squareup/protos/client/rolodex/Note;->note_token:Ljava/lang/String;

    invoke-static {p1, v1}, Lcom/squareup/crm/util/RolodexContactHelper;->withRemovedNote(Lcom/squareup/protos/client/rolodex/Contact;Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p1

    .line 815
    :goto_0
    iput-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->noteForViewNoteScreen:Lcom/squareup/protos/client/rolodex/Note;

    .line 816
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/crm/cards/ViewNoteScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    .line 817
    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->setLoadedContact(Lcom/squareup/protos/client/rolodex/Contact;)V

    return-void
.end method

.method public confirmSendLoyaltyStatus()V
    .locals 4

    .line 996
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->loyaltyStatus:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;

    iget-object v0, v0, Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;->loyalty_account:Lcom/squareup/protos/client/loyalty/LoyaltyAccount;

    iget-object v0, v0, Lcom/squareup/protos/client/loyalty/LoyaltyAccount;->mappings:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    .line 997
    iget-object v2, v1, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;->type:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Type;

    sget-object v3, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Type;->TYPE_PHONE:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Type;

    if-ne v2, v3, :cond_0

    .line 998
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->retrofitQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    new-instance v2, Lcom/squareup/queue/crm/SendBuyerLoyaltyStatusTask$Builder;

    invoke-direct {v2}, Lcom/squareup/queue/crm/SendBuyerLoyaltyStatusTask$Builder;-><init>()V

    iget-object v1, v1, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;->id:Ljava/lang/String;

    .line 999
    invoke-virtual {v2, v1}, Lcom/squareup/queue/crm/SendBuyerLoyaltyStatusTask$Builder;->phoneToken(Ljava/lang/String;)Lcom/squareup/queue/crm/SendBuyerLoyaltyStatusTask$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    .line 1000
    invoke-interface {v2}, Lcom/squareup/permissions/EmployeeManagement;->getCurrentEmployeeToken()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/queue/crm/SendBuyerLoyaltyStatusTask$Builder;->employeeToken(Ljava/lang/String;)Lcom/squareup/queue/crm/SendBuyerLoyaltyStatusTask$Builder;

    move-result-object v1

    .line 1001
    invoke-virtual {v1}, Lcom/squareup/queue/crm/SendBuyerLoyaltyStatusTask$Builder;->build()Lcom/squareup/queue/crm/SendBuyerLoyaltyStatusTask;

    move-result-object v1

    .line 998
    invoke-interface {v0, v1}, Lcom/squareup/queue/retrofit/RetrofitQueue;->add(Ljava/lang/Object;)V

    :cond_1
    return-void
.end method

.method public contactForViewCustomerScreen()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ">;"
        }
    .end annotation

    .line 550
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->contact:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public contactLoadingState()Lrx/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$ContactLoadingState;",
            ">;"
        }
    .end annotation

    .line 554
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->contactLoadingState:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 555
    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->loyaltyLoadingState:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 556
    invoke-virtual {v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->distinctUntilChanged()Lrx/Observable;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->busyWithRedeemRewardTierRpc:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 559
    invoke-virtual {v2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->distinctUntilChanged()Lrx/Observable;

    move-result-object v2

    sget-object v3, Lcom/squareup/ui/crm/flow/-$$Lambda$GwveTlVw08lhbKqUih2rCjfEY4k;->INSTANCE:Lcom/squareup/ui/crm/flow/-$$Lambda$GwveTlVw08lhbKqUih2rCjfEY4k;

    .line 554
    invoke-static {v0, v1, v2, v3}, Lrx/Observable;->combineLatest(Lrx/Observable;Lrx/Observable;Lrx/Observable;Lrx/functions/Func3;)Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$1LX9e3HKtUiPBXNHm7Cwaslt_P8;->INSTANCE:Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$1LX9e3HKtUiPBXNHm7Cwaslt_P8;

    .line 561
    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 582
    invoke-virtual {v0}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method protected contactToken()Ljava/lang/String;
    .locals 1

    .line 546
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->contact:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/rolodex/Contact;

    iget-object v0, v0, Lcom/squareup/protos/client/rolodex/Contact;->contact_token:Ljava/lang/String;

    return-object v0
.end method

.method public deleteLoyaltyAccount()V
    .locals 5

    .line 893
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/crm/cards/DeletingLoyaltyAccountScreen;

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    invoke-direct {v1, v2}, Lcom/squareup/ui/crm/cards/DeletingLoyaltyAccountScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const-class v3, Lcom/squareup/ui/crm/v2/DeleteLoyaltyAccountConfirmationScreen;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Lcom/squareup/container/Flows;->goBackPastAndAdd(Lflow/Flow;Lcom/squareup/container/ContainerTreeKey;[Ljava/lang/Class;)V

    return-void
.end method

.method public enterAdjustPointsFlow()V
    .locals 3

    .line 1010
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    sget-object v1, Lcom/squareup/permissions/Permission;->UPDATE_LOYALTY:Lcom/squareup/permissions/Permission;

    new-instance v2, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$2;

    invoke-direct {v2, p0}, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$2;-><init>(Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;)V

    invoke-virtual {v0, v1, v2}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    return-void
.end method

.method public expiringPointsScreenData()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen$ScreenData;",
            ">;"
        }
    .end annotation

    .line 1377
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->loyalty:Lcom/squareup/loyalty/LoyaltyServiceHelper;

    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->getLoyaltyAccountToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/loyalty/LoyaltyServiceHelper;->getExpiringPoints(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v0

    invoke-static {v0}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Single(Lio/reactivex/SingleSource;)Lrx/Single;

    move-result-object v0

    .line 1378
    invoke-virtual {v0}, Lrx/Single;->toObservable()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$oYiXwLwIajatdaly4gXyMbjDMCg;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$oYiXwLwIajatdaly4gXyMbjDMCg;-><init>(Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;)V

    .line 1379
    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 1403
    invoke-static {}, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen$ScreenData;->forLoading()Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen$ScreenData;

    move-result-object v1

    invoke-virtual {v0, v1}, Lrx/Observable;->startWith(Ljava/lang/Object;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public getConfirmSendLoyaltyStatusCopy()Ljava/lang/String;
    .locals 3

    .line 988
    invoke-direct {p0}, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->getFormattedLoyaltyPhoneNumber()Ljava/lang/String;

    move-result-object v0

    .line 989
    iget-object v1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/crmviewcustomer/R$string;->crm_loyalty_program_section_send_status_body:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/phrase/Phrase;->from(Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    const-string v2, "phone"

    .line 990
    invoke-virtual {v1, v2, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 991
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 992
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getConflictingPhoneNumber()Ljava/lang/String;
    .locals 1

    .line 1227
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->conflictingLoyaltyPhoneNumber:Ljava/lang/String;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, ""

    :goto_0
    return-object v0
.end method

.method public getContactForAllFrequentItemsScreen()Lcom/squareup/protos/client/rolodex/Contact;
    .locals 1

    .line 869
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->contact:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/rolodex/Contact;

    return-object v0
.end method

.method public getContactForAllNotesScreen()Lcom/squareup/protos/client/rolodex/Contact;
    .locals 1

    .line 821
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->contact:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/rolodex/Contact;

    return-object v0
.end method

.method public getContactForCreateNoteScreen()Lcom/squareup/protos/client/rolodex/Contact;
    .locals 1

    .line 833
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->contact:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/rolodex/Contact;

    return-object v0
.end method

.method public getContactForSendMessageScreen()Lcom/squareup/protos/client/rolodex/Contact;
    .locals 1

    .line 1076
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->contact:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/rolodex/Contact;

    return-object v0
.end method

.method public getContactForUnlinkInstrumentDialog()Lcom/squareup/protos/client/rolodex/Contact;
    .locals 1

    .line 661
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->contact:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/rolodex/Contact;

    return-object v0
.end method

.method public getContactTokenForUnlinkInstrumentDialog()Ljava/lang/String;
    .locals 1

    .line 687
    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->contactToken()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getConversationTokenForConversationScreen()Ljava/lang/String;
    .locals 1

    .line 1053
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->tokenForConversationScreen:Ljava/lang/String;

    return-object v0
.end method

.method public getCurrentInvoiceDisplayDetails()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;",
            ">;>;"
        }
    .end annotation

    .line 772
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->currentInvoice:Lcom/jakewharton/rxrelay/BehaviorRelay;

    return-object v0
.end method

.method public getDeleteLoyaltyAccountConfirmationText()Ljava/lang/String;
    .locals 3

    .line 914
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->res:Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->LOYALTY_DELETE_WILL_SEND_SMS:Lcom/squareup/settings/server/Features$Feature;

    .line 915
    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget v1, Lcom/squareup/crmscreens/R$string;->crm_loyalty_delete_account_body_sms_warning:I

    goto :goto_0

    :cond_0
    sget v1, Lcom/squareup/crmscreens/R$string;->crm_loyalty_delete_account_body:I

    .line 914
    :goto_0
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 919
    invoke-direct {p0}, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->getFormattedLoyaltyPhoneNumber()Ljava/lang/String;

    move-result-object v1

    const-string v2, "phone_number"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 920
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 921
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDeleteProfileConfirmationText()Ljava/lang/String;
    .locals 3

    .line 925
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->res:Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->LOYALTY_DELETE_WILL_SEND_SMS:Lcom/squareup/settings/server/Features$Feature;

    .line 926
    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget v1, Lcom/squareup/crmscreens/R$string;->crm_confirm_customer_deletion_one_details_loyalty_sms:I

    goto :goto_0

    :cond_0
    sget v1, Lcom/squareup/crmscreens/R$string;->crm_confirm_customer_deletion_one_details:I

    .line 925
    :goto_0
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getHoldsCoupons()Lcom/squareup/checkout/HoldsCoupons;
    .locals 1

    .line 504
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->holdsCoupons:Lcom/squareup/checkout/HoldsCoupons;

    return-object v0
.end method

.method public getInstrumentForUnlinkInstrumentDialog()Lcom/squareup/protos/client/instruments/InstrumentSummary;
    .locals 1

    .line 665
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->instrumentForUnlinkInstrumentDialog:Lcom/squareup/protos/client/instruments/InstrumentSummary;

    return-object v0
.end method

.method public getInstrumentTokenForUnlinkInstrumentDialog()Ljava/lang/String;
    .locals 1

    .line 691
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->instrumentForUnlinkInstrumentDialog:Lcom/squareup/protos/client/instruments/InstrumentSummary;

    iget-object v0, v0, Lcom/squareup/protos/client/instruments/InstrumentSummary;->instrument_token:Ljava/lang/String;

    return-object v0
.end method

.method public getInvoiceMetrics(Lcom/squareup/protos/client/rolodex/Contact;)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/rolodex/Contact;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/invoice/GetMetricsResponse;",
            ">;"
        }
    .end annotation

    .line 1333
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->invoiceService:Lcom/squareup/invoices/ClientInvoiceServiceHelper;

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Contact;->contact_token:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->getMetricQueries()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->getMetrics(Ljava/lang/String;Ljava/util/List;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public getLoyaltyAccountMapping()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;",
            ">;"
        }
    .end annotation

    .line 1199
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->loyaltyStatus:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v1, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$zTPJg1iVVpXIeHDFJrSz21Sn2mg;->INSTANCE:Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$zTPJg1iVVpXIeHDFJrSz21Sn2mg;

    .line 1200
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public getLoyaltyAccountToken()Ljava/lang/String;
    .locals 1

    .line 902
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->loyaltyStatus:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;

    iget-object v0, v0, Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;->loyalty_account:Lcom/squareup/protos/client/loyalty/LoyaltyAccount;

    iget-object v0, v0, Lcom/squareup/protos/client/loyalty/LoyaltyAccount;->loyalty_account_token:Ljava/lang/String;

    return-object v0
.end method

.method public getMessageForSendMessageScreen()Ljava/lang/String;
    .locals 1

    .line 1068
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->sendMessageScreenMessage:Ljava/lang/String;

    return-object v0
.end method

.method public getMetricQueries()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery;",
            ">;"
        }
    .end annotation

    .line 1341
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1342
    new-instance v1, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery$Builder;-><init>()V

    sget-object v2, Lcom/squareup/protos/client/invoice/MetricType;->PAID_MONEY:Lcom/squareup/protos/client/invoice/MetricType;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery$Builder;->metric_type(Lcom/squareup/protos/client/invoice/MetricType;)Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery$Builder;->build()Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1343
    new-instance v1, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery$Builder;-><init>()V

    sget-object v2, Lcom/squareup/protos/client/invoice/MetricType;->OUTSTANDING_MONEY:Lcom/squareup/protos/client/invoice/MetricType;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery$Builder;->metric_type(Lcom/squareup/protos/client/invoice/MetricType;)Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery$Builder;->build()Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1344
    new-instance v1, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery$Builder;-><init>()V

    sget-object v2, Lcom/squareup/protos/client/invoice/MetricType;->DRAFT_MONEY:Lcom/squareup/protos/client/invoice/MetricType;

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery$Builder;->metric_type(Lcom/squareup/protos/client/invoice/MetricType;)Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery$Builder;->build()Lcom/squareup/protos/client/invoice/GetMetricsRequest$MetricQuery;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method public getNoteForViewNoteScreen()Lcom/squareup/protos/client/rolodex/Note;
    .locals 1

    .line 794
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->noteForViewNoteScreen:Lcom/squareup/protos/client/rolodex/Note;

    return-object v0
.end method

.method public getPathType()Lcom/squareup/ui/crm/flow/CrmScopeType;
    .locals 1

    .line 652
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    iget-object v0, v0, Lcom/squareup/ui/crm/flow/CrmScope;->type:Lcom/squareup/ui/crm/flow/CrmScopeType;

    return-object v0
.end method

.method public getReminder()Lcom/squareup/protos/client/rolodex/Reminder;
    .locals 1

    .line 1081
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->reminder:Lcom/squareup/protos/client/rolodex/Reminder;

    return-object v0
.end method

.method public getSourceLoyaltyAccountToken()Ljava/lang/String;
    .locals 1

    .line 938
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    iget-object v0, v0, Lcom/squareup/ui/crm/flow/CrmScope;->maybeSourceContactToTransferFrom:Lcom/squareup/protos/client/rolodex/Contact;

    invoke-static {v0}, Lcom/squareup/crm/util/RolodexContactHelper;->getLoyaltyTokenForContact(Lcom/squareup/protos/client/rolodex/Contact;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public invoiceResults()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/datafetch/Rx1AbstractLoader$Results<",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/invoice/InvoiceDisplayDetails;",
            ">;>;"
        }
    .end annotation

    .line 781
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->invoicesCustomerLoader:Lcom/squareup/invoices/InvoicesCustomerLoader;

    invoke-virtual {v0}, Lcom/squareup/invoices/InvoicesCustomerLoader;->results()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method protected isContactAddedToHoldsCustomer(Lcom/squareup/protos/client/rolodex/Contact;)Z
    .locals 1

    .line 635
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->holdsCustomer:Lcom/squareup/payment/crm/HoldsCustomer;

    invoke-interface {v0}, Lcom/squareup/payment/crm/HoldsCustomer;->hasCustomer()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/squareup/protos/client/rolodex/Contact;->contact_token:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/Contact;->contact_token:Ljava/lang/String;

    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->holdsCustomer:Lcom/squareup/payment/crm/HoldsCustomer;

    .line 637
    invoke-interface {v0}, Lcom/squareup/payment/crm/HoldsCustomer;->getCustomerId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public isCustomerAddedToSale(Ljava/lang/String;)Z
    .locals 0

    .line 1361
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1
.end method

.method public isForTransferringLoyalty()Z
    .locals 1

    .line 979
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    iget-object v0, v0, Lcom/squareup/ui/crm/flow/CrmScope;->maybeSourceContactToTransferFrom:Lcom/squareup/protos/client/rolodex/Contact;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isViewCustomerReadOnly()Z
    .locals 1

    .line 1373
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    iget-boolean v0, v0, Lcom/squareup/ui/crm/flow/CrmScope;->readOnly:Z

    return v0
.end method

.method public synthetic lambda$closeTransferringLoyaltyAccountScreen$26$AbstractCrmScopeRunner(Lflow/History$Builder;)Lflow/History$Builder;
    .locals 2

    .line 951
    const-class v0, Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountScreen;

    invoke-static {p1, v0}, Lcom/squareup/container/Histories;->popLastScreen(Lflow/History$Builder;Ljava/lang/Class;)Lflow/History$Builder;

    .line 952
    const-class v0, Lcom/squareup/ui/crm/v2/ViewCustomerCardScreen;

    invoke-static {p1, v0}, Lcom/squareup/container/Histories;->popLastScreen(Lflow/History$Builder;Ljava/lang/Class;)Lflow/History$Builder;

    .line 953
    const-class v0, Lcom/squareup/ui/crm/ChooseCustomerScreen;

    invoke-static {p1, v0}, Lcom/squareup/container/Histories;->popLastScreen(Lflow/History$Builder;Ljava/lang/Class;)Lflow/History$Builder;

    .line 955
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    iget-object v0, v0, Lcom/squareup/ui/crm/flow/CrmScope;->type:Lcom/squareup/ui/crm/flow/CrmScopeType;

    sget-object v1, Lcom/squareup/ui/crm/flow/CrmScopeType;->ADD_CUSTOMER_TO_SALE_IN_SPLIT_TICKET:Lcom/squareup/ui/crm/flow/CrmScopeType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    iget-object v0, v0, Lcom/squareup/ui/crm/flow/CrmScope;->type:Lcom/squareup/ui/crm/flow/CrmScopeType;

    sget-object v1, Lcom/squareup/ui/crm/flow/CrmScopeType;->VIEW_CUSTOMER_ADDED_TO_SALE_IN_SPLIT_TICKET:Lcom/squareup/ui/crm/flow/CrmScopeType;

    if-ne v0, v1, :cond_1

    .line 957
    :cond_0
    const-class v0, Lcom/squareup/ui/crm/v2/ViewCustomerCardScreen;

    invoke-static {p1, v0}, Lcom/squareup/container/Histories;->popLastScreen(Lflow/History$Builder;Ljava/lang/Class;)Lflow/History$Builder;

    :cond_1
    return-object p1
.end method

.method public synthetic lambda$expiringPointsScreenData$42$AbstractCrmScopeRunner(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen$ScreenData;
    .locals 6

    .line 1380
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_1

    .line 1381
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    .line 1382
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;->getResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/loyalty/GetExpiringPointsStatusResponse;

    .line 1384
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    invoke-interface {v0}, Lcom/squareup/loyalty/LoyaltySettings;->getExpirationPolicy()Lcom/squareup/server/account/protos/ExpirationPolicy;

    move-result-object v0

    .line 1386
    iget-object v1, p1, Lcom/squareup/protos/client/loyalty/GetExpiringPointsStatusResponse;->upcoming_deadlines:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    if-lez v1, :cond_0

    if-eqz v0, :cond_0

    .line 1387
    new-instance v1, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen$ScreenData;

    iget-object p1, p1, Lcom/squareup/protos/client/loyalty/GetExpiringPointsStatusResponse;->upcoming_deadlines:Ljava/util/List;

    iget-object v4, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    .line 1389
    invoke-interface {v4}, Lcom/squareup/loyalty/LoyaltySettings;->pointsTerms()Lcom/squareup/loyalty/PointsTerms;

    move-result-object v4

    invoke-virtual {v4}, Lcom/squareup/loyalty/PointsTerms;->getPlural()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->res:Lcom/squareup/util/Res;

    .line 1388
    invoke-static {v0, v4, v5}, Lcom/squareup/crm/util/RolodexProtoHelper;->formatExpirationPolicy(Lcom/squareup/server/account/protos/ExpirationPolicy;Ljava/lang/String;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, p1, v0, v3, v2}, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen$ScreenData;-><init>(Ljava/util/List;Ljava/lang/String;ZLjava/lang/String;)V

    return-object v1

    .line 1391
    :cond_0
    new-instance v0, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen$ScreenData;

    iget-object p1, p1, Lcom/squareup/protos/client/loyalty/GetExpiringPointsStatusResponse;->upcoming_deadlines:Ljava/util/List;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/crmviewcustomer/R$string;->crm_loyalty_program_section_view_expiring_points_empty:I

    .line 1392
    invoke-interface {v1, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p1, v1, v3, v2}, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen$ScreenData;-><init>(Ljava/util/List;Ljava/lang/String;ZLjava/lang/String;)V

    return-object v0

    .line 1396
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/crmviewcustomer/R$string;->crm_loyalty_program_section_view_expiring_points_error:I

    .line 1397
    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    .line 1398
    invoke-interface {v0}, Lcom/squareup/loyalty/LoyaltySettings;->pointsTerms()Lcom/squareup/loyalty/PointsTerms;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/loyalty/PointsTerms;->getPlural()Ljava/lang/String;

    move-result-object v0

    const-string v1, "points_terminology"

    invoke-virtual {p1, v1, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 1399
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 1400
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    .line 1396
    invoke-static {p1}, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen$ScreenData;->forError(Ljava/lang/String;)Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen$ScreenData;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$null$1$AbstractCrmScopeRunner(Lio/reactivex/disposables/Disposable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 386
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->contactLoadingState:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$ContactLoadingState;->LOADING:Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$ContactLoadingState;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$null$10$AbstractCrmScopeRunner(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 423
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->loyaltyLoadingState:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$ContactLoadingState;->ERROR:Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$ContactLoadingState;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$null$29$AbstractCrmScopeRunner(Ljava/lang/String;Lcom/squareup/protos/client/loyalty/CreateLoyaltyAccountResponse;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1143
    iget-object v0, p2, Lcom/squareup/protos/client/loyalty/CreateLoyaltyAccountResponse;->conflicting_contacts:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1144
    iget-object p2, p2, Lcom/squareup/protos/client/loyalty/CreateLoyaltyAccountResponse;->conflicting_contacts:Ljava/util/List;

    const/4 v0, 0x0

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/protos/client/rolodex/Contact;

    iput-object p2, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->conflictingLoyaltyContact:Lcom/squareup/protos/client/rolodex/Contact;

    .line 1145
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->conflictingLoyaltyPhoneNumber:Ljava/lang/String;

    .line 1146
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->conflictingLoyaltyPhoneNumber:Ljava/lang/String;

    iget-object p2, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->conflictingLoyaltyContact:Lcom/squareup/protos/client/rolodex/Contact;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->showUpdateLoyaltyPhoneConflictDialog(Ljava/lang/String;Lcom/squareup/protos/client/rolodex/Contact;)V

    goto :goto_0

    .line 1149
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->closeUpdateLoyaltyPhoneScreen()V

    .line 1151
    :goto_0
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->updateLoyaltyPhoneError:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object p2, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$null$3$AbstractCrmScopeRunner(Lcom/squareup/protos/client/rolodex/GetContactResponse;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 392
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/GetContactResponse;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->setLoadedContact(Lcom/squareup/protos/client/rolodex/Contact;)V

    .line 393
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->contactLoadingState:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$ContactLoadingState;->VALID:Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$ContactLoadingState;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$null$30$AbstractCrmScopeRunner(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1154
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->updateLoyaltyPhoneError:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$null$34$AbstractCrmScopeRunner(Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1168
    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->closeUpdateLoyaltyPhoneScreen()V

    .line 1169
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->updateLoyaltyPhoneError:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$null$35$AbstractCrmScopeRunner(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1172
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->getOkayResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;

    if-eqz p1, :cond_0

    .line 1173
    iget-object p2, p1, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;->conflicting_contacts:Ljava/util/List;

    .line 1174
    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result p2

    if-nez p2, :cond_0

    iget-object p2, p1, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;->conflicting_contacts:Ljava/util/List;

    const/4 v0, 0x0

    .line 1175
    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/protos/client/rolodex/Contact;

    iget-object p2, p2, Lcom/squareup/protos/client/rolodex/Contact;->contact_token:Ljava/lang/String;

    invoke-static {p2}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p2

    if-nez p2, :cond_0

    .line 1177
    iget-object p2, p1, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;->conflicting_contacts:Ljava/util/List;

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/protos/client/rolodex/Contact;

    iput-object p2, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->conflictingLoyaltyContact:Lcom/squareup/protos/client/rolodex/Contact;

    .line 1178
    iget-object p1, p1, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;->errors:Ljava/util/List;

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;

    iget-object p1, p1, Lcom/squareup/protos/client/loyalty/LoyaltyRequestError;->raw_value:Ljava/lang/String;

    iput-object p1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->conflictingLoyaltyPhoneNumber:Ljava/lang/String;

    .line 1179
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->conflictingLoyaltyPhoneNumber:Ljava/lang/String;

    iget-object p2, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->conflictingLoyaltyContact:Lcom/squareup/protos/client/rolodex/Contact;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->showUpdateLoyaltyPhoneConflictDialog(Ljava/lang/String;Lcom/squareup/protos/client/rolodex/Contact;)V

    .line 1181
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->updateLoyaltyPhoneError:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object p2, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    goto :goto_0

    .line 1183
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->updateLoyaltyPhoneError:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object p2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method public synthetic lambda$null$38$AbstractCrmScopeRunner(Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1248
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->contact:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/rolodex/Contact;

    iget-object p1, p1, Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;->coupon:Lcom/squareup/protos/client/coupons/Coupon;

    invoke-virtual {p0, v0, p1}, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->applyCouponToCartAndAddCustomer(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/coupons/Coupon;)V

    return-void
.end method

.method public synthetic lambda$null$39$AbstractCrmScopeRunner(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 1251
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->busyWithRedeemRewardTierRpc:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$null$4$AbstractCrmScopeRunner(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 396
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->contactLoadingState:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$ContactLoadingState;->ERROR:Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$ContactLoadingState;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$null$7$AbstractCrmScopeRunner()V
    .locals 2

    .line 414
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->loyaltyLoadingState:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v1, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$ContactLoadingState;->LOADING:Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$ContactLoadingState;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$null$9$AbstractCrmScopeRunner(Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 419
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->loyaltyStatus:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 420
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->loyaltyLoadingState:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$ContactLoadingState;->VALID:Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$ContactLoadingState;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$onEnterScope$11$AbstractCrmScopeRunner(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 2

    .line 417
    new-instance v0, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$FwKLlBGWrZr4_uYS28YhAtzVjLA;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$FwKLlBGWrZr4_uYS28YhAtzVjLA;-><init>(Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;)V

    new-instance v1, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$HW2zYCNf5VZeCNjHaw_QOI4k-4M;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$HW2zYCNf5VZeCNjHaw_QOI4k-4M;-><init>(Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;)V

    invoke-virtual {p1, v0, v1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public synthetic lambda$onEnterScope$14$AbstractCrmScopeRunner(Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 432
    invoke-virtual {p1}, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;->getContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 433
    invoke-virtual {p1}, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;->getContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->setLoadedContact(Lcom/squareup/protos/client/rolodex/Contact;)V

    .line 435
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->flow:Lflow/Flow;

    sget-object v1, Lflow/Direction;->BACKWARD:Lflow/Direction;

    const/4 v2, 0x1

    new-array v2, v2, [Lkotlin/jvm/functions/Function1;

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Result;->getHistoryFunc()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, Lcom/squareup/container/Flows;->editHistory(Lflow/Flow;Lflow/Direction;[Lkotlin/jvm/functions/Function1;)V

    return-void
.end method

.method public synthetic lambda$onEnterScope$15$AbstractCrmScopeRunner(Lkotlin/Unit;)V
    .locals 0

    .line 442
    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->requestGetLoyaltyStatus()V

    return-void
.end method

.method public synthetic lambda$onEnterScope$17$AbstractCrmScopeRunner(Lcom/squareup/protos/client/rolodex/Contact;)V
    .locals 0

    .line 449
    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->requestGetLoyaltyStatus()V

    .line 452
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->updateMaybeHoldsCustomers(Lcom/squareup/protos/client/rolodex/Contact;)V

    return-void
.end method

.method public synthetic lambda$onEnterScope$18$AbstractCrmScopeRunner(Lkotlin/Unit;)V
    .locals 0

    .line 464
    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->cancelAddCardOnFile()V

    return-void
.end method

.method public synthetic lambda$onEnterScope$2$AbstractCrmScopeRunner(Ljava/lang/String;)Lrx/Observable;
    .locals 1

    .line 385
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->getContactFromRolodex(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$-EOWQo66XAX5quZAq1d0lDDZaNo;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$-EOWQo66XAX5quZAq1d0lDDZaNo;-><init>(Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;)V

    .line 386
    invoke-virtual {p1, v0}, Lio/reactivex/Single;->doOnSubscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object p1

    .line 384
    invoke-static {p1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Single(Lio/reactivex/SingleSource;)Lrx/Single;

    move-result-object p1

    .line 387
    invoke-virtual {p1}, Lrx/Single;->toObservable()Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onEnterScope$21$AbstractCrmScopeRunner(Ljava/lang/String;)Lrx/Observable;
    .locals 1

    .line 472
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 473
    invoke-static {}, Lcom/squareup/util/Optional;->empty()Lcom/squareup/util/Optional;

    move-result-object p1

    .line 474
    invoke-static {p1}, Lrx/Observable;->just(Ljava/lang/Object;)Lrx/Observable;

    move-result-object p1

    return-object p1

    .line 476
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->invoiceService:Lcom/squareup/invoices/ClientInvoiceServiceHelper;

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/ClientInvoiceServiceHelper;->get(Ljava/lang/String;)Lrx/Observable;

    move-result-object p1

    sget-object v0, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$pC5lu1EOL9J-0ot4YiZIxwv5jAw;->INSTANCE:Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$pC5lu1EOL9J-0ot4YiZIxwv5jAw;

    .line 477
    invoke-virtual {p1, v0}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    sget-object v0, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$cI2N1_-qPL4AZwQN1UBLI4SVpqM;->INSTANCE:Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$cI2N1_-qPL4AZwQN1UBLI4SVpqM;

    .line 478
    invoke-virtual {p1, v0}, Lrx/Observable;->onErrorReturn(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onEnterScope$23$AbstractCrmScopeRunner(Lkotlin/Pair;)V
    .locals 1

    .line 491
    invoke-virtual {p1}, Lkotlin/Pair;->getFirst()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;

    if-eqz v0, :cond_0

    .line 492
    invoke-virtual {p1}, Lkotlin/Pair;->getSecond()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/client/rolodex/Contact;

    .line 493
    invoke-virtual {p1}, Lkotlin/Pair;->getFirst()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;

    invoke-virtual {p1}, Lcom/squareup/ui/crm/ChooseCustomerFlow$Result$ContactChosen;->getContact()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object p1

    .line 492
    invoke-virtual {p0, v0, p1}, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->showTransferLoyaltyAccount(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/rolodex/Contact;)V

    :cond_0
    return-void
.end method

.method public synthetic lambda$onEnterScope$5$AbstractCrmScopeRunner(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 2

    .line 390
    new-instance v0, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$MqA2WFUuWxRXPKTgFpkryxEqk78;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$MqA2WFUuWxRXPKTgFpkryxEqk78;-><init>(Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;)V

    new-instance v1, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$vDhoHp4gCPTg4gdIeFlZCrS3MwU;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$vDhoHp4gCPTg4gdIeFlZCrS3MwU;-><init>(Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;)V

    invoke-virtual {p1, v0, v1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public synthetic lambda$onEnterScope$8$AbstractCrmScopeRunner(Ljava/lang/String;)Lrx/Observable;
    .locals 1

    .line 411
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->loyalty:Lcom/squareup/loyalty/LoyaltyServiceHelper;

    invoke-virtual {v0, p1}, Lcom/squareup/loyalty/LoyaltyServiceHelper;->getLoyaltyStatusForContactToken(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p1

    invoke-static {p1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Single(Lio/reactivex/SingleSource;)Lrx/Single;

    move-result-object p1

    .line 412
    invoke-virtual {p1}, Lrx/Single;->toObservable()Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$64GK04Z4Ic70ncPoiKRigDxt-tc;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$64GK04Z4Ic70ncPoiKRigDxt-tc;-><init>(Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;)V

    .line 414
    invoke-virtual {p1, v0}, Lrx/Observable;->doOnSubscribe(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$rewardTierClicked$40$AbstractCrmScopeRunner(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 2

    .line 1247
    new-instance v0, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$ehOQEfJhtCM10QpN8Mc8d45NwPA;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$ehOQEfJhtCM10QpN8Mc8d45NwPA;-><init>(Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;)V

    new-instance v1, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$qHgxeqZ4Y3zlqt83KzNj25w_uX4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$qHgxeqZ4Y3zlqt83KzNj25w_uX4;-><init>(Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;)V

    invoke-virtual {p1, v0, v1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public synthetic lambda$saveLoyaltyPhone$27$AbstractCrmScopeRunner()V
    .locals 2

    .line 1138
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->saveLoyaltyPhoneInProgress:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$saveLoyaltyPhone$28$AbstractCrmScopeRunner()V
    .locals 2

    .line 1139
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->saveLoyaltyPhoneInProgress:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$saveLoyaltyPhone$31$AbstractCrmScopeRunner(Ljava/lang/String;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 1

    .line 1141
    new-instance v0, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$Y4ma4WjqPDX5qprg22UijurMwT8;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$Y4ma4WjqPDX5qprg22UijurMwT8;-><init>(Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;Ljava/lang/String;)V

    new-instance p1, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$VAthLXlP33AutczDATtqREMnDUI;

    invoke-direct {p1, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$VAthLXlP33AutczDATtqREMnDUI;-><init>(Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;)V

    invoke-virtual {p2, v0, p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public synthetic lambda$saveLoyaltyPhone$32$AbstractCrmScopeRunner()V
    .locals 2

    .line 1163
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->saveLoyaltyPhoneInProgress:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$saveLoyaltyPhone$33$AbstractCrmScopeRunner()V
    .locals 2

    .line 1164
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->saveLoyaltyPhoneInProgress:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public synthetic lambda$saveLoyaltyPhone$36$AbstractCrmScopeRunner(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 2

    .line 1166
    new-instance v0, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$zkifJH1wXJcbx4SvXi-o39RfaO8;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$zkifJH1wXJcbx4SvXi-o39RfaO8;-><init>(Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;)V

    new-instance v1, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$zv-8syCGsqDI0gyTkxJVtK0qbyo;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$zv-8syCGsqDI0gyTkxJVtK0qbyo;-><init>(Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V

    invoke-virtual {p1, v0, v1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->handle(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)V

    return-void
.end method

.method public synthetic lambda$title$25$AbstractCrmScopeRunner(Lcom/squareup/protos/client/rolodex/Contact;)Ljava/lang/String;
    .locals 1

    .line 588
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->res:Lcom/squareup/util/Res;

    invoke-static {p1, v0}, Lcom/squareup/crm/util/RolodexContactHelper;->getFullName(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$unredeemRewardTier$41$AbstractCrmScopeRunner(Ljava/lang/String;Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 2

    .line 1277
    instance-of p2, p2, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    .line 1279
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->holdsCoupons:Lcom/squareup/checkout/HoldsCoupons;

    xor-int/lit8 v1, p2, 0x1

    invoke-interface {v0, p1, v1}, Lcom/squareup/checkout/HoldsCoupons;->removeCoupon(Ljava/lang/String;Z)V

    if-eqz p2, :cond_0

    .line 1285
    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->requestGetLoyaltyStatus()V

    .line 1287
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->busyWithRedeemRewardTierRpc:Lcom/jakewharton/rxrelay/BehaviorRelay;

    const/4 p2, 0x0

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public loadMoreInvoices(Ljava/lang/Integer;)V
    .locals 1

    .line 789
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->invoicesCustomerLoader:Lcom/squareup/invoices/InvoicesCustomerLoader;

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/InvoicesCustomerLoader;->loadMore(Ljava/lang/Integer;)V

    return-void
.end method

.method public loadingInvoices()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/datafetch/Rx1AbstractLoader$Progress<",
            "Ljava/lang/String;",
            ">;>;>;"
        }
    .end annotation

    .line 785
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->invoicesCustomerLoader:Lcom/squareup/invoices/InvoicesCustomerLoader;

    invoke-virtual {v0}, Lcom/squareup/invoices/InvoicesCustomerLoader;->progress()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public loyaltyLoadingState()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$ContactLoadingState;",
            ">;"
        }
    .end annotation

    .line 598
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->loyaltyLoadingState:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public loyaltyStatus()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;",
            ">;"
        }
    .end annotation

    .line 594
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->loyaltyStatus:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public newSaleWithCustomerFromApplet()V
    .locals 1

    .line 1365
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public onDeletingLoyaltyAccountSuccess()V
    .locals 0

    .line 906
    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->requestGetLoyaltyStatus()V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    .line 363
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/flow/CrmScope;

    iput-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    .line 364
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    iget-object v0, v0, Lcom/squareup/ui/crm/flow/CrmScope;->holdsCustomer:Lcom/squareup/payment/crm/HoldsCustomer;

    iput-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->holdsCustomer:Lcom/squareup/payment/crm/HoldsCustomer;

    .line 365
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    iget-object v0, v0, Lcom/squareup/ui/crm/flow/CrmScope;->holdsCoupons:Lcom/squareup/checkout/HoldsCoupons;

    iput-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->holdsCoupons:Lcom/squareup/checkout/HoldsCoupons;

    .line 367
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->invoicesCustomerLoader:Lcom/squareup/invoices/InvoicesCustomerLoader;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 368
    invoke-static {p1}, Lmortar/bundler/BundleService;->getBundleService(Lmortar/MortarScope;)Lmortar/bundler/BundleService;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->addCardOnFileFlow:Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;

    invoke-virtual {v0, v1}, Lmortar/bundler/BundleService;->register(Lmortar/bundler/Bundler;)V

    .line 369
    invoke-static {p1}, Lmortar/bundler/BundleService;->getBundleService(Lmortar/MortarScope;)Lmortar/bundler/BundleService;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->billHistoryFlow:Lcom/squareup/ui/crm/flow/BillHistoryFlow;

    invoke-virtual {v0, v1}, Lmortar/bundler/BundleService;->register(Lmortar/bundler/Bundler;)V

    .line 370
    invoke-static {p1}, Lmortar/bundler/BundleService;->getBundleService(Lmortar/MortarScope;)Lmortar/bundler/BundleService;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->manageCouponFlow:Lcom/squareup/ui/crm/v2/flow/ManageCouponsAndRewardsFlow;

    invoke-virtual {v0, v1}, Lmortar/bundler/BundleService;->register(Lmortar/bundler/Bundler;)V

    .line 371
    invoke-static {p1}, Lmortar/bundler/BundleService;->getBundleService(Lmortar/MortarScope;)Lmortar/bundler/BundleService;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->adjustPointsFlow:Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;

    invoke-virtual {v0, v1}, Lmortar/bundler/BundleService;->register(Lmortar/bundler/Bundler;)V

    .line 374
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->x2SellerScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-interface {v0}, Lcom/squareup/x2/MaybeX2SellerScreenRunner;->enteringSaveCustomer()Z

    .line 381
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->baseContact:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v1, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$PIDhU-W7QrQLa0Kj4xnH-W2cKbE;->INSTANCE:Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$PIDhU-W7QrQLa0Kj4xnH-W2cKbE;

    .line 383
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$9wy8-rBzvaZkZz1UzpgLy3nNwRg;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$9wy8-rBzvaZkZz1UzpgLy3nNwRg;-><init>(Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;)V

    .line 384
    invoke-virtual {v0, v1}, Lrx/Observable;->switchMap(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$lBUexZ4frEbrxhZITG8nR9Dvf-U;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$lBUexZ4frEbrxhZITG8nR9Dvf-U;-><init>(Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;)V

    .line 389
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 381
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    .line 403
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->baseContact:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v1, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$VLsv-6VajXhRpV28VLcSpDZut7c;->INSTANCE:Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$VLsv-6VajXhRpV28VLcSpDZut7c;

    .line 405
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->requestGetLoyaltyStatus:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 406
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 403
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    .line 408
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->requestGetLoyaltyStatus:Lcom/jakewharton/rxrelay/BehaviorRelay;

    new-instance v1, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$lrZSLmzYNSLmDwPLIaQS5NiYg5Y;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$lrZSLmzYNSLmDwPLIaQS5NiYg5Y;-><init>(Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;)V

    .line 410
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->switchMap(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$D1hN0HkAbQvWyJ5N9dt4JRx7A3M;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$D1hN0HkAbQvWyJ5N9dt4JRx7A3M;-><init>(Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;)V

    .line 416
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 408
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    .line 427
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->updateCustomerFlow:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;

    .line 428
    invoke-interface {v0}, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;->results()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$Tmpa3DU7y1fjzel4s4fRzTjJTHA;->INSTANCE:Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$Tmpa3DU7y1fjzel4s4fRzTjJTHA;

    .line 429
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$wDp8SgnBENHN_vojYZO9j6dHymM;->INSTANCE:Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$wDp8SgnBENHN_vojYZO9j6dHymM;

    .line 430
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$bhLxbSwXatXCADo9kg6xT7gyusw;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$bhLxbSwXatXCADo9kg6xT7gyusw;-><init>(Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;)V

    .line 431
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 427
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 438
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->manageCouponFlow:Lcom/squareup/ui/crm/v2/flow/ManageCouponsAndRewardsFlow;

    .line 439
    invoke-virtual {v0}, Lcom/squareup/ui/crm/v2/flow/ManageCouponsAndRewardsFlow;->onResult()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$3U77bfFfNUSYdygzRTwztsY71gg;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$3U77bfFfNUSYdygzRTwztsY71gg;-><init>(Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;)V

    .line 440
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 438
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    .line 445
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->adjustPointsFlow:Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;

    .line 446
    invoke-virtual {v0}, Lcom/squareup/ui/crm/v2/flow/AdjustPointsFlow;->onResult()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->contact:Lcom/jakewharton/rxrelay/BehaviorRelay;

    sget-object v2, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$qWZDL30hgJBjw1OGrUxRXb0iZPA;->INSTANCE:Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$qWZDL30hgJBjw1OGrUxRXb0iZPA;

    .line 447
    invoke-virtual {v0, v1, v2}, Lrx/Observable;->withLatestFrom(Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$z-LXQyQamxaDzIxngwoP34i6vAA;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$z-LXQyQamxaDzIxngwoP34i6vAA;-><init>(Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;)V

    .line 448
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 445
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    .line 456
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->mergeCustomersFlow:Lcom/squareup/ui/crm/flow/MergeCustomersFlow;

    .line 457
    invoke-virtual {v0}, Lcom/squareup/ui/crm/flow/MergeCustomersFlow;->onResult()Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lio/reactivex/BackpressureStrategy;->LATEST:Lio/reactivex/BackpressureStrategy;

    invoke-static {v0, v1}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/flow/-$$Lambda$LGTdSmEBAPFWLNsJf3fX0PWoZiA;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$LGTdSmEBAPFWLNsJf3fX0PWoZiA;-><init>(Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;)V

    .line 459
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 456
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    .line 463
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->addCardOnFileFlow:Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/flow/AddCardOnFileFlow;->onCancel()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$Q_-GggGD5UMznsanOTfXPVc6Hjc;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$Q_-GggGD5UMznsanOTfXPVc6Hjc;-><init>(Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;)V

    .line 464
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 463
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    .line 467
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->invoicesCustomerLoader:Lcom/squareup/invoices/InvoicesCustomerLoader;

    sget-object v1, Lcom/squareup/ui/crm/cards/CustomerInvoiceCoordinator;->INVOICES_FIRST_PAGE_SIZE:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Lcom/squareup/invoices/InvoicesCustomerLoader;->setDefaultPageSize(Ljava/lang/Integer;)V

    .line 469
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->currentInvoiceId:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 470
    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$BDN1jjz7YjLy-hI5V8nX8Dkg_YQ;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$BDN1jjz7YjLy-hI5V8nX8Dkg_YQ;-><init>(Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;)V

    .line 471
    invoke-virtual {v0, v1}, Lrx/Observable;->switchMap(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->currentInvoice:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 485
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 469
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    .line 487
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->chooseCustomerFlow:Lcom/squareup/ui/crm/ChooseCustomerFlow;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/ChooseCustomerFlow;->getResults()Lrx/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$WFIHYlY2qqw2N8H5TOdgqCOry68;->INSTANCE:Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$WFIHYlY2qqw2N8H5TOdgqCOry68;

    .line 488
    invoke-virtual {v0, v1}, Lrx/Observable;->filter(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->contact:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 489
    invoke-static {}, Lcom/squareup/util/RxTuples;->toPair()Lrx/functions/Func2;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lrx/Observable;->withLatestFrom(Lrx/Observable;Lrx/functions/Func2;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$X-19Dr09d-Fk1yRFr-nP25n66IM;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$X-19Dr09d-Fk1yRFr-nP25n66IM;-><init>(Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;)V

    .line 490
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    .line 487
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    return-void
.end method

.method public onExitScope()V
    .locals 1

    .line 499
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->unlinkInstrumentSubscription:Lrx/Subscription;

    invoke-interface {v0}, Lrx/Subscription;->unsubscribe()V

    .line 500
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->redeemRewardTierRpcSubscription:Lrx/Subscription;

    invoke-interface {v0}, Lrx/Subscription;->unsubscribe()V

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 0

    if-nez p1, :cond_0

    :cond_0
    return-void
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 0

    return-void
.end method

.method public onUpdateLoyaltyError()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 1195
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->updateLoyaltyPhoneError:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public pastAppointments()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel$CrmAppointmentRowViewModel;",
            ">;>;"
        }
    .end annotation

    .line 730
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->viewCustomerConfiguration:Lcom/squareup/crm/viewcustomerconfiguration/api/ViewCustomerConfiguration;

    invoke-interface {v0}, Lcom/squareup/crm/viewcustomerconfiguration/api/ViewCustomerConfiguration;->getCrmAppointmentsDataRenderer()Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsDataRenderer;

    move-result-object v0

    .line 731
    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->contactToken()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsDataRenderer;->getAllPastAppointmentData(Ljava/lang/String;)Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lio/reactivex/BackpressureStrategy;->ERROR:Lio/reactivex/BackpressureStrategy;

    .line 730
    invoke-static {v0, v1}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public removeThisCustomer()V
    .locals 1

    .line 1357
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method protected requestGetContact()V
    .locals 2

    .line 537
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->baseContact:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method protected requestGetLoyaltyStatus()V
    .locals 2

    .line 541
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->requestGetLoyaltyStatus:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public rewardTierClicked(Lcom/squareup/protos/client/loyalty/RewardTier;)V
    .locals 5

    .line 1231
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->holdsCoupons:Lcom/squareup/checkout/HoldsCoupons;

    if-nez v0, :cond_0

    return-void

    .line 1236
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->flow:Lflow/Flow;

    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/Class;

    const-class v3, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersScreen;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-static {v0, v2}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    .line 1239
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->busyWithRedeemRewardTierRpc:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 1240
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->redeemRewardTierRpcSubscription:Lrx/Subscription;

    invoke-interface {v0}, Lrx/Subscription;->unsubscribe()V

    .line 1241
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->loyalty:Lcom/squareup/loyalty/LoyaltyServiceHelper;

    iget-object p1, p1, Lcom/squareup/protos/client/loyalty/RewardTier;->coupon_definition_token:Ljava/lang/String;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->loyaltyStatus:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 1245
    invoke-virtual {v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;

    iget-object v1, v1, Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;->loyalty_status:Ljava/util/List;

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId;

    iget-object v1, v1, Lcom/squareup/protos/client/loyalty/LoyaltyStatusWithId;->customer_identifier:Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier;

    iget-object v1, v1, Lcom/squareup/protos/client/loyalty/LoyaltyCustomerIdentifier;->phone_token:Ljava/lang/String;

    const/4 v2, 0x0

    .line 1243
    invoke-virtual {v0, p1, v1, v2}, Lcom/squareup/loyalty/LoyaltyServiceHelper;->redeemPoints(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object p1

    .line 1242
    invoke-static {p1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Single(Lio/reactivex/SingleSource;)Lrx/Single;

    move-result-object p1

    .line 1246
    invoke-virtual {p1}, Lrx/Single;->toObservable()Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$dqhnuf6QzFopfJ45CBFoXxLur9s;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$dqhnuf6QzFopfJ45CBFoXxLur9s;-><init>(Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;)V

    .line 1247
    invoke-virtual {p1, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->redeemRewardTierRpcSubscription:Lrx/Subscription;

    return-void
.end method

.method public saveLoyaltyPhone(Ljava/lang/String;)V
    .locals 3

    .line 1129
    new-instance v0, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Builder;-><init>()V

    sget-object v1, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Type;->TYPE_PHONE:Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Type;

    .line 1130
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Builder;->type(Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Type;)Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Builder;

    move-result-object v0

    .line 1131
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Builder;->raw_id(Ljava/lang/String;)Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping$Builder;->build()Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    move-result-object v0

    .line 1134
    iget-object v1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->loyaltyStatus:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;

    invoke-static {v1}, Lcom/squareup/loyalty/LoyaltyServiceHelper;->hasLoyaltyAccount(Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1136
    iget-object v1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->loyalty:Lcom/squareup/loyalty/LoyaltyServiceHelper;

    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->contactToken()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/squareup/loyalty/LoyaltyServiceHelper;->createLoyaltyAccount(Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v0

    invoke-static {v0}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Single(Lio/reactivex/SingleSource;)Lrx/Single;

    move-result-object v0

    .line 1137
    invoke-virtual {v0}, Lrx/Single;->toObservable()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$P_IMOjlhDhIyO4n8GO8f8ArA0AQ;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$P_IMOjlhDhIyO4n8GO8f8ArA0AQ;-><init>(Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;)V

    .line 1138
    invoke-virtual {v0, v1}, Lrx/Observable;->doOnSubscribe(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$hAYeDS3DtDegwgk9JZL5he9fQME;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$hAYeDS3DtDegwgk9JZL5he9fQME;-><init>(Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;)V

    .line 1139
    invoke-virtual {v0, v1}, Lrx/Observable;->doOnTerminate(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$yo-6g8Skx-vzMVlCAFXKGKeqZ14;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$yo-6g8Skx-vzMVlCAFXKGKeqZ14;-><init>(Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;Ljava/lang/String;)V

    .line 1140
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    goto :goto_0

    .line 1159
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->loyaltyStatus:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;

    iget-object p1, p1, Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;->loyalty_account:Lcom/squareup/protos/client/loyalty/LoyaltyAccount;

    iget-object p1, p1, Lcom/squareup/protos/client/loyalty/LoyaltyAccount;->mappings:Ljava/util/List;

    const/4 v1, 0x0

    .line 1160
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;

    iget-object p1, p1, Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;->loyalty_account_mapping_token:Ljava/lang/String;

    .line 1161
    iget-object v1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->loyalty:Lcom/squareup/loyalty/LoyaltyServiceHelper;

    invoke-virtual {v1, p1, v0}, Lcom/squareup/loyalty/LoyaltyServiceHelper;->updateLoyaltyAccountMappings(Ljava/lang/String;Lcom/squareup/protos/client/loyalty/LoyaltyAccountMapping;)Lio/reactivex/Single;

    move-result-object p1

    invoke-static {p1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Single(Lio/reactivex/SingleSource;)Lrx/Single;

    move-result-object p1

    .line 1162
    invoke-virtual {p1}, Lrx/Single;->toObservable()Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$3qWAMYC5pCRPWV8kVkTjt8czmag;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$3qWAMYC5pCRPWV8kVkTjt8czmag;-><init>(Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;)V

    .line 1163
    invoke-virtual {p1, v0}, Lrx/Observable;->doOnSubscribe(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$v7IS-910eTJUWMDbDLNdvCDgOKs;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$v7IS-910eTJUWMDbDLNdvCDgOKs;-><init>(Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;)V

    .line 1164
    invoke-virtual {p1, v0}, Lrx/Observable;->doOnTerminate(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$Li7IWJmGoFecmV_XNp1SL1GqvfU;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$Li7IWJmGoFecmV_XNp1SL1GqvfU;-><init>(Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;)V

    .line 1165
    invoke-virtual {p1, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    :goto_0
    return-void
.end method

.method public saveLoyaltyPhoneInProgress()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 1191
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->saveLoyaltyPhoneInProgress:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method abstract scopeSupportsProfileAttachments()Z
.end method

.method public seeAllRewardTiersClicked()V
    .locals 3

    .line 1308
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersScreen;

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    invoke-direct {v1, v2}, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method protected setBaseAndLoadedContact(Lcom/squareup/protos/client/rolodex/Contact;)V
    .locals 0

    .line 532
    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->setLoadedContact(Lcom/squareup/protos/client/rolodex/Contact;)V

    .line 533
    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->requestGetLoyaltyStatus()V

    return-void
.end method

.method protected setBaseContact(Lcom/squareup/protos/client/rolodex/Contact;)V
    .locals 1

    .line 514
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->baseContact:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public setCurrentInvoiceId(Ljava/lang/String;)V
    .locals 1

    .line 767
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->currentInvoiceId:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method public setInvoiceLoaderToken(Ljava/lang/String;)V
    .locals 1

    .line 1337
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->invoicesCustomerLoader:Lcom/squareup/invoices/InvoicesCustomerLoader;

    invoke-virtual {v0, p1}, Lcom/squareup/invoices/InvoicesCustomerLoader;->setContactToken(Ljava/lang/String;)V

    return-void
.end method

.method protected setLoadedContact(Lcom/squareup/protos/client/rolodex/Contact;)V
    .locals 1

    .line 521
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->contact:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0, p1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 524
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->updateMaybeHoldsCustomers(Lcom/squareup/protos/client/rolodex/Contact;)V

    return-void
.end method

.method public showAddCouponScreen()V
    .locals 1

    const/4 v0, 0x0

    .line 1020
    invoke-virtual {p0, v0}, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->showAddCouponScreen(Ljava/lang/String;)V

    return-void
.end method

.method public showAddCouponScreen(Ljava/lang/String;)V
    .locals 2

    .line 1024
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->sendMessageScreenMessage:Ljava/lang/String;

    .line 1025
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->flow:Lflow/Flow;

    new-instance v0, Lcom/squareup/ui/crm/cards/AddCouponScreen;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    invoke-direct {v0, v1}, Lcom/squareup/ui/crm/cards/AddCouponScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public showAllCouponsAndRewardsScreen()V
    .locals 3

    .line 1043
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsScreen;

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    invoke-direct {v1, v2}, Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public showAllFrequentItemsScreen()V
    .locals 3

    .line 853
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/crm/cards/AllFrequentItemsScreen;

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    invoke-direct {v1, v2}, Lcom/squareup/ui/crm/cards/AllFrequentItemsScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public showAllNotesScreen()V
    .locals 3

    .line 825
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/crm/cards/AllNotesScreen;

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    invoke-direct {v1, v2}, Lcom/squareup/ui/crm/cards/AllNotesScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public showAllPastAppointmentsScreen()V
    .locals 4

    .line 857
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/crm/cards/AllAppointmentsScreen;

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    sget-object v3, Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Type;->PAST:Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Type;

    invoke-direct {v1, v2, v3}, Lcom/squareup/ui/crm/cards/AllAppointmentsScreen;-><init>(Lcom/squareup/ui/crm/flow/CrmScope;Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Type;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public showAllUpcomingAppointmentsScreen()V
    .locals 4

    .line 861
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/crm/cards/AllAppointmentsScreen;

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    sget-object v3, Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Type;->UPCOMING:Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Type;

    invoke-direct {v1, v2, v3}, Lcom/squareup/ui/crm/cards/AllAppointmentsScreen;-><init>(Lcom/squareup/ui/crm/flow/CrmScope;Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Type;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public showAppointmentDetail(Ljava/lang/String;Lorg/threeten/bp/Instant;)V
    .locals 2

    .line 741
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->appointmentLinkingHandler:Lcom/squareup/appointmentsapi/AppointmentLinkingHandler;

    new-instance v1, Lcom/squareup/appointmentsapi/AppointmentLinkingTarget$ViewAppointmentTarget;

    invoke-direct {v1, p1, p2}, Lcom/squareup/appointmentsapi/AppointmentLinkingTarget$ViewAppointmentTarget;-><init>(Ljava/lang/String;Lorg/threeten/bp/Instant;)V

    invoke-interface {v0, v1}, Lcom/squareup/appointmentsapi/AppointmentLinkingHandler;->handle(Lcom/squareup/appointmentsapi/AppointmentLinkingTarget;)V

    .line 743
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->mainContainer:Lcom/squareup/ui/main/PosContainer;

    invoke-interface {p1}, Lcom/squareup/ui/main/PosContainer;->resetHistory()V

    return-void
.end method

.method public showBillHistoryScreen(Lcom/squareup/billhistory/model/BillHistory;)V
    .locals 3

    .line 1105
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->billHistoryFlow:Lcom/squareup/ui/crm/flow/BillHistoryFlow;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->showFirstScreen(Lcom/squareup/ui/main/RegisterTreeKey;ZLcom/squareup/billhistory/model/BillHistory;)V

    return-void
.end method

.method public showBillHistoryScreen(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .line 1110
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->billHistoryFlow:Lcom/squareup/ui/crm/flow/BillHistoryFlow;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1, p2}, Lcom/squareup/ui/crm/flow/BillHistoryFlow;->showFirstScreen(Lcom/squareup/ui/main/RegisterTreeKey;ZLjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public showChooseContactForTransfer()V
    .locals 8

    .line 970
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->flow:Lflow/Flow;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    sget-object v2, Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;->TRANSFER_LOYALTY:Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;

    sget v3, Lcom/squareup/crmviewcustomer/R$string;->crm_loyalty_transfer_account_overflow:I

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    invoke-static/range {v1 .. v7}, Lcom/squareup/ui/crm/ChooseCustomerFlow;->start(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/ui/crm/ChooseCustomerFlow$ChooseCustomerResultKey;ILcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;Lcom/squareup/ui/crm/flow/CrmScopeType;ZZ)Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public showConfirmUnlinkInstrumentDialog(Lcom/squareup/protos/client/instruments/InstrumentSummary;)V
    .locals 2

    .line 656
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->instrumentForUnlinkInstrumentDialog:Lcom/squareup/protos/client/instruments/InstrumentSummary;

    .line 657
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->flow:Lflow/Flow;

    new-instance v0, Lcom/squareup/ui/crm/cards/ConfirmRemoveCardDialogScreen;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    invoke-direct {v0, v1}, Lcom/squareup/ui/crm/cards/ConfirmRemoveCardDialogScreen;-><init>(Lcom/squareup/ui/crm/flow/CrmScope;)V

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public showConversationScreen(Ljava/lang/String;)V
    .locals 2

    .line 1057
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->tokenForConversationScreen:Ljava/lang/String;

    .line 1058
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->flow:Lflow/Flow;

    new-instance v0, Lcom/squareup/ui/crm/cards/ConversationCardScreen;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    invoke-direct {v0, v1}, Lcom/squareup/ui/crm/cards/ConversationCardScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public showCreateAppointment()V
    .locals 5

    .line 747
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->appointmentLinkingHandler:Lcom/squareup/appointmentsapi/AppointmentLinkingHandler;

    new-instance v1, Lcom/squareup/appointmentsapi/AppointmentLinkingTarget$CreateAppointmentForCustomerTarget;

    .line 750
    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->contactToken()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->contact:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 751
    invoke-virtual {v3}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/client/rolodex/Contact;

    iget-object v3, v3, Lcom/squareup/protos/client/rolodex/Contact;->display_name:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-direct {v1, v4, v2, v3}, Lcom/squareup/appointmentsapi/AppointmentLinkingTarget$CreateAppointmentForCustomerTarget;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 747
    invoke-interface {v0, v1}, Lcom/squareup/appointmentsapi/AppointmentLinkingHandler;->handle(Lcom/squareup/appointmentsapi/AppointmentLinkingTarget;)V

    .line 752
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->mainContainer:Lcom/squareup/ui/main/PosContainer;

    invoke-interface {v0}, Lcom/squareup/ui/main/PosContainer;->resetHistory()V

    return-void
.end method

.method public showCreateNoteScreen()V
    .locals 3

    .line 837
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/crm/cards/CreateNoteScreen;

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    invoke-direct {v1, v2}, Lcom/squareup/ui/crm/cards/CreateNoteScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public showCustomerActivityScreen()V
    .locals 3

    .line 720
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CUSTOMERS_ACTIVITY_LIST:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    invoke-static {v0}, Lcom/squareup/util/Preconditions;->checkState(Z)V

    .line 721
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/crm/cards/CustomerActivityScreen;

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    invoke-direct {v1, v2}, Lcom/squareup/ui/crm/cards/CustomerActivityScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public showCustomerInvoiceScreen()V
    .locals 3

    .line 756
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->CUSTOMER_INVOICE_LINKING:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    invoke-static {v0}, Lcom/squareup/util/Preconditions;->checkState(Z)V

    .line 757
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/crm/cards/CustomerInvoiceScreen;

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    invoke-direct {v1, v2}, Lcom/squareup/ui/crm/cards/CustomerInvoiceScreen;-><init>(Lcom/squareup/ui/crm/flow/CrmScope;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public showDeleteLoyaltyAccount()V
    .locals 3

    .line 889
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/crm/v2/DeleteLoyaltyAccountConfirmationScreen;

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    invoke-direct {v1, v2}, Lcom/squareup/ui/crm/v2/DeleteLoyaltyAccountConfirmationScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public showLoyaltyMergeProposal()V
    .locals 5

    .line 1216
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->mergeCustomersFlow:Lcom/squareup/ui/crm/flow/MergeCustomersFlow;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->contact:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v2}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/rolodex/Contact;

    iget-object v3, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->conflictingLoyaltyContact:Lcom/squareup/protos/client/rolodex/Contact;

    iget-object v4, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->conflictingLoyaltyPhoneNumber:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/squareup/ui/crm/flow/MergeCustomersFlow;->showMergeProposalScreen(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/rolodex/Contact;Ljava/lang/String;)V

    return-void
.end method

.method public showLoyaltySectionDropDown()V
    .locals 3

    .line 875
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionBottomDialogScreen;

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    invoke-direct {v1, v2}, Lcom/squareup/ui/crm/v2/profile/LoyaltySectionBottomDialogScreen;-><init>(Lcom/squareup/ui/crm/flow/CrmScope;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public showManageCouponsAndRewardsScreen()V
    .locals 3

    .line 1034
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    sget-object v1, Lcom/squareup/permissions/Permission;->UPDATE_LOYALTY:Lcom/squareup/permissions/Permission;

    new-instance v2, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$3;

    invoke-direct {v2, p0}, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$3;-><init>(Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;)V

    invoke-virtual {v0, v1, v2}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    return-void
.end method

.method public showProfileAttachmentsScreen()V
    .locals 1

    .line 1349
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public showProfileAttachmentsSection()Z
    .locals 1

    .line 1302
    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->scopeSupportsProfileAttachments()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->profileAttachmentsVisibility:Lcom/squareup/ui/crm/ProfileAttachmentsVisibility;

    .line 1303
    invoke-interface {v0}, Lcom/squareup/ui/crm/ProfileAttachmentsVisibility;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->cameraHelper:Lcom/squareup/camerahelper/CameraHelper;

    .line 1304
    invoke-interface {v0}, Lcom/squareup/camerahelper/CameraHelper;->isCameraOrGalleryAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public showReminderScreen(Lcom/squareup/protos/client/rolodex/Reminder;)V
    .locals 2

    .line 1085
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->reminder:Lcom/squareup/protos/client/rolodex/Reminder;

    .line 1086
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->flow:Lflow/Flow;

    new-instance v0, Lcom/squareup/ui/crm/cards/ReminderScreen;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    invoke-direct {v0, v1}, Lcom/squareup/ui/crm/cards/ReminderScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public showSaveCardToCustomerScreenV2()V
    .locals 3

    .line 702
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->contact:Lcom/jakewharton/rxrelay/BehaviorRelay;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Lcom/squareup/util/Preconditions;->checkState(Z)V

    .line 704
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getOnboardingSettings()Lcom/squareup/settings/server/OnboardingSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/OnboardingSettings;->acceptsCards()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 705
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->unlinkInstrumentSubscription:Lrx/Subscription;

    invoke-interface {v0}, Lrx/Subscription;->unsubscribe()V

    .line 706
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    sget-object v1, Lcom/squareup/permissions/Permission;->CARD_ON_FILE:Lcom/squareup/permissions/Permission;

    new-instance v2, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$1;

    invoke-direct {v2, p0}, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$1;-><init>(Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;)V

    invoke-virtual {v0, v1, v2}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    goto :goto_1

    .line 713
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->onboardingDiverter:Lcom/squareup/onboarding/OnboardingDiverter;

    invoke-interface {v0}, Lcom/squareup/onboarding/OnboardingDiverter;->shouldDivertToOnboarding()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 714
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->onboardingDiverter:Lcom/squareup/onboarding/OnboardingDiverter;

    sget-object v1, Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;->PROMPT:Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;

    invoke-interface {v0, v1}, Lcom/squareup/onboarding/OnboardingDiverter;->divertToOnboarding(Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;)V

    :cond_2
    :goto_1
    return-void
.end method

.method public showSendLoyaltyStatusScreen()V
    .locals 3

    .line 984
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/crm/cards/loyalty/ConfirmSendLoyaltyStatusDialogScreen;

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    invoke-direct {v1, v2}, Lcom/squareup/ui/crm/cards/loyalty/ConfirmSendLoyaltyStatusDialogScreen;-><init>(Lcom/squareup/ui/crm/flow/CrmScope;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public showSendMessageScreen()V
    .locals 3

    .line 1072
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/crm/cards/SendMessageScreen;

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    invoke-direct {v1, v2}, Lcom/squareup/ui/crm/cards/SendMessageScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public showTransferLoyaltyAccount(Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/rolodex/Contact;)V
    .locals 0

    return-void
.end method

.method public showUpdateCustomerV2()V
    .locals 3

    .line 1316
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    sget-object v1, Lcom/squareup/permissions/Permission;->MANAGE_CUSTOMERS:Lcom/squareup/permissions/Permission;

    new-instance v2, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$5;

    invoke-direct {v2, p0}, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$5;-><init>(Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;)V

    invoke-virtual {v0, v1, v2}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    return-void
.end method

.method public showUpdateLoyaltyPhoneConflictDialog(Ljava/lang/String;Lcom/squareup/protos/client/rolodex/Contact;)V
    .locals 4

    .line 1208
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->conflictingLoyaltyPhoneNumber:Ljava/lang/String;

    .line 1209
    iput-object p2, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->conflictingLoyaltyContact:Lcom/squareup/protos/client/rolodex/Contact;

    .line 1210
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->flow:Lflow/Flow;

    sget-object p2, Lflow/Direction;->REPLACE:Lflow/Direction;

    new-instance v0, Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneConflictDialogScreen;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    invoke-direct {v0, v1}, Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneConflictDialogScreen;-><init>(Lcom/squareup/ui/crm/flow/CrmScope;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const-class v2, Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneScreen;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {p1, p2, v0, v1}, Lcom/squareup/container/Flows;->goBackPastAndAdd(Lflow/Flow;Lflow/Direction;Lcom/squareup/container/ContainerTreeKey;[Ljava/lang/Class;)V

    return-void
.end method

.method public showUpdateLoyaltyPhoneScreen()V
    .locals 3

    .line 1114
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    sget-object v1, Lcom/squareup/permissions/Permission;->UPDATE_LOYALTY:Lcom/squareup/permissions/Permission;

    new-instance v2, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$4;

    invoke-direct {v2, p0}, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner$4;-><init>(Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;)V

    invoke-virtual {v0, v1, v2}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    return-void
.end method

.method public showUploadFileBottomSheet()V
    .locals 1

    .line 1353
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public showViewNoteScreen(Lcom/squareup/protos/client/rolodex/Note;)V
    .locals 2

    .line 798
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->noteForViewNoteScreen:Lcom/squareup/protos/client/rolodex/Note;

    .line 799
    iget-object p1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->flow:Lflow/Flow;

    new-instance v0, Lcom/squareup/ui/crm/cards/ViewNoteScreen;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    invoke-direct {v0, v1}, Lcom/squareup/ui/crm/cards/ViewNoteScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public successRemoveCardOnFile()V
    .locals 5

    .line 674
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->instrumentForUnlinkInstrumentDialog:Lcom/squareup/protos/client/instruments/InstrumentSummary;

    if-nez v0, :cond_0

    return-void

    .line 679
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->requestGetContact()V

    .line 681
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/analytics/event/v1/CardOnFileActionEvent;

    sget-object v2, Lcom/squareup/analytics/RegisterActionName;->CARD_ON_FILE_REMOVE_CARD:Lcom/squareup/analytics/RegisterActionName;

    .line 682
    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->contactToken()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->instrumentForUnlinkInstrumentDialog:Lcom/squareup/protos/client/instruments/InstrumentSummary;

    iget-object v4, v4, Lcom/squareup/protos/client/instruments/InstrumentSummary;->instrument_token:Ljava/lang/String;

    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/analytics/event/v1/CardOnFileActionEvent;-><init>(Lcom/squareup/analytics/RegisterActionName;Ljava/lang/String;Ljava/lang/String;)V

    .line 681
    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    const/4 v0, 0x0

    .line 683
    iput-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->instrumentForUnlinkInstrumentDialog:Lcom/squareup/protos/client/instruments/InstrumentSummary;

    return-void
.end method

.method public title()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 587
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->baseContact:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->contact:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-static {v0, v1}, Lrx/Observable;->merge(Lrx/Observable;Lrx/Observable;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$n23afUPooHfdObt3F2QJei5Zdr8;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$n23afUPooHfdObt3F2QJei5Zdr8;-><init>(Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;)V

    .line 588
    invoke-virtual {v0, v1}, Lrx/Observable;->map(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 589
    invoke-virtual {v0}, Lrx/Observable;->distinctUntilChanged()Lrx/Observable;

    move-result-object v0

    const-string v1, ""

    .line 590
    invoke-virtual {v0, v1}, Lrx/Observable;->startWith(Ljava/lang/Object;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public transferLoyaltyAccount()V
    .locals 5

    .line 933
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountScreen;

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    invoke-direct {v1, v2}, Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountScreen;-><init>(Lcom/squareup/ui/crm/flow/CrmScope;)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const-class v3, Lcom/squareup/ui/crm/ChooseCustomerScreen;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Lcom/squareup/container/Flows;->goBackPastAndAdd(Lflow/Flow;Lcom/squareup/container/ContainerTreeKey;[Ljava/lang/Class;)V

    return-void
.end method

.method public transferLoyaltyTargetContactToken()Ljava/lang/String;
    .locals 1

    .line 942
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    iget-object v0, v0, Lcom/squareup/ui/crm/flow/CrmScope;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    iget-object v0, v0, Lcom/squareup/protos/client/rolodex/Contact;->contact_token:Ljava/lang/String;

    return-object v0
.end method

.method public unlinkInstrumentConfirmClicked()V
    .locals 5

    .line 669
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/crm/cards/RemovingCardOnFileScreen;

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    invoke-direct {v1, v2}, Lcom/squareup/ui/crm/cards/RemovingCardOnFileScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const-class v3, Lcom/squareup/ui/crm/cards/ConfirmRemoveCardDialogScreen;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Lcom/squareup/container/Flows;->goBackPastAndAdd(Lflow/Flow;Lcom/squareup/container/ContainerTreeKey;[Ljava/lang/Class;)V

    return-void
.end method

.method public unredeemRewardTier(Lcom/squareup/protos/client/loyalty/RewardTier;)V
    .locals 5

    .line 1257
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->holdsCoupons:Lcom/squareup/checkout/HoldsCoupons;

    if-nez v0, :cond_0

    return-void

    .line 1262
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->flow:Lflow/Flow;

    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/Class;

    const-class v3, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersScreen;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-static {v0, v2}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    .line 1264
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->holdsCoupons:Lcom/squareup/checkout/HoldsCoupons;

    iget-object p1, p1, Lcom/squareup/protos/client/loyalty/RewardTier;->coupon_definition_token:Ljava/lang/String;

    .line 1265
    invoke-interface {v0, p1}, Lcom/squareup/checkout/HoldsCoupons;->getAllAddedCoupons(Ljava/lang/String;)Ljava/util/List;

    move-result-object p1

    .line 1267
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1268
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/checkout/Discount;

    invoke-virtual {p1}, Lcom/squareup/checkout/Discount;->getCouponToken()Ljava/lang/String;

    move-result-object p1

    .line 1270
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->busyWithRedeemRewardTierRpc:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    .line 1271
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->redeemRewardTierRpcSubscription:Lrx/Subscription;

    invoke-interface {v0}, Lrx/Subscription;->unsubscribe()V

    .line 1272
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->loyalty:Lcom/squareup/loyalty/LoyaltyServiceHelper;

    .line 1273
    invoke-virtual {v0, p1}, Lcom/squareup/loyalty/LoyaltyServiceHelper;->returnReward(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v0

    invoke-static {v0}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Single(Lio/reactivex/SingleSource;)Lrx/Single;

    move-result-object v0

    invoke-virtual {v0}, Lrx/Single;->toObservable()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$eL-7dpMxHL8aWYq9G63h_1BrppI;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/flow/-$$Lambda$AbstractCrmScopeRunner$eL-7dpMxHL8aWYq9G63h_1BrppI;-><init>(Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;Ljava/lang/String;)V

    .line 1274
    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->redeemRewardTierRpcSubscription:Lrx/Subscription;

    :cond_1
    return-void
.end method

.method public upcomingAppointments()Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/util/List<",
            "Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsViewModel$CrmAppointmentsSectionViewModel$CrmAppointmentRowViewModel;",
            ">;>;"
        }
    .end annotation

    .line 736
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->viewCustomerConfiguration:Lcom/squareup/crm/viewcustomerconfiguration/api/ViewCustomerConfiguration;

    invoke-interface {v0}, Lcom/squareup/crm/viewcustomerconfiguration/api/ViewCustomerConfiguration;->getCrmAppointmentsDataRenderer()Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsDataRenderer;

    move-result-object v0

    .line 737
    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->contactToken()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/crm/viewcustomerconfiguration/api/CrmAppointmentsDataRenderer;->getAllUpcomingAppointmentData(Ljava/lang/String;)Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lio/reactivex/BackpressureStrategy;->ERROR:Lio/reactivex/BackpressureStrategy;

    .line 736
    invoke-static {v0, v1}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public viewExpiringPoints()V
    .locals 3

    .line 879
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen;

    iget-object v2, p0, Lcom/squareup/ui/crm/flow/AbstractCrmScopeRunner;->crmPath:Lcom/squareup/ui/crm/flow/CrmScope;

    invoke-direct {v1, v2}, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method
