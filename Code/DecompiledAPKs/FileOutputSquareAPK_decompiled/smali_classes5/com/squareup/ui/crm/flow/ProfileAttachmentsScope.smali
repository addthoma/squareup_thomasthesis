.class public Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope;
.super Lcom/squareup/ui/crm/flow/InCrmScope;
.source "ProfileAttachmentsScope.java"

# interfaces
.implements Lcom/squareup/container/RegistersInScope;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope$Component;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field attachment:Lcom/squareup/protos/client/rolodex/Attachment;

.field contact:Lcom/squareup/protos/client/rolodex/Contact;

.field private crmPath:Lcom/squareup/ui/main/RegisterTreeKey;

.field uploadUri:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 64
    sget-object v0, Lcom/squareup/ui/crm/flow/-$$Lambda$ProfileAttachmentsScope$z_LlrbWcSKKuGMsgLfiZD3xu9d4;->INSTANCE:Lcom/squareup/ui/crm/flow/-$$Lambda$ProfileAttachmentsScope$z_LlrbWcSKKuGMsgLfiZD3xu9d4;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method protected constructor <init>(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/rolodex/Attachment;Landroid/net/Uri;)V
    .locals 0

    .line 31
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/flow/InCrmScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    .line 32
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope;->crmPath:Lcom/squareup/ui/main/RegisterTreeKey;

    .line 33
    iput-object p2, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    .line 34
    iput-object p3, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope;->attachment:Lcom/squareup/protos/client/rolodex/Attachment;

    .line 35
    iput-object p4, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope;->uploadUri:Landroid/net/Uri;

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope;
    .locals 4

    .line 65
    const-class v0, Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/main/RegisterTreeKey;

    .line 66
    sget-object v1, Lcom/squareup/protos/client/rolodex/Contact;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {p0}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v2

    invoke-static {v1, v2}, Lcom/squareup/util/ProtosPure;->decodeOrNull(Lcom/squareup/wire/ProtoAdapter;[B)Lcom/squareup/wire/Message;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/client/rolodex/Contact;

    .line 67
    sget-object v2, Lcom/squareup/protos/client/rolodex/Attachment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {p0}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v3

    invoke-static {v2, v3}, Lcom/squareup/util/ProtosPure;->decodeOrNull(Lcom/squareup/wire/ProtoAdapter;[B)Lcom/squareup/wire/Message;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/rolodex/Attachment;

    .line 68
    const-class v3, Landroid/net/Uri;

    invoke-virtual {v3}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    invoke-virtual {p0, v3}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Landroid/net/Uri;

    .line 69
    new-instance v3, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope;

    invoke-direct {v3, v0, v1, v2, p0}, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/protos/client/rolodex/Contact;Lcom/squareup/protos/client/rolodex/Attachment;Landroid/net/Uri;)V

    return-object v3
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 57
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/crm/flow/InCrmScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 58
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope;->crmPath:Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope;->contact:Lcom/squareup/protos/client/rolodex/Contact;

    invoke-static {v0}, Lcom/squareup/util/ProtosPure;->encodeOrNull(Lcom/squareup/wire/Message;)[B

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope;->attachment:Lcom/squareup/protos/client/rolodex/Attachment;

    invoke-static {v0}, Lcom/squareup/util/ProtosPure;->encodeOrNull(Lcom/squareup/wire/Message;)[B

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope;->uploadUri:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public register(Lmortar/MortarScope;)V
    .locals 1

    .line 39
    const-class v0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope$Component;

    .line 40
    invoke-interface {v0}, Lcom/squareup/ui/crm/flow/ProfileAttachmentsScope$Component;->runner()Lcom/squareup/ui/crm/flow/ProfileAttachmentsScopeRunner;

    move-result-object v0

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    return-void
.end method
