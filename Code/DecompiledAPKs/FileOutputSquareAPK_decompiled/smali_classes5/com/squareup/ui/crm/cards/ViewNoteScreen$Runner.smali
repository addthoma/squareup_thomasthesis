.class public interface abstract Lcom/squareup/ui/crm/cards/ViewNoteScreen$Runner;
.super Ljava/lang/Object;
.source "ViewNoteScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/ViewNoteScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation


# virtual methods
.method public abstract closeViewNoteScreen()V
.end method

.method public abstract closeViewNoteScreen(Lcom/squareup/protos/client/rolodex/Note;)V
.end method

.method public abstract getNoteForViewNoteScreen()Lcom/squareup/protos/client/rolodex/Note;
.end method

.method public abstract showReminderScreen(Lcom/squareup/protos/client/rolodex/Reminder;)V
.end method
