.class public final Lcom/squareup/ui/crm/flow/CreateFilterFlow_Factory;
.super Ljava/lang/Object;
.source "CreateFilterFlow_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/crm/flow/CreateFilterFlow;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/CreateFilterFlow_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/ui/crm/flow/CreateFilterFlow_Factory;->analyticsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/crm/flow/CreateFilterFlow_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;)",
            "Lcom/squareup/ui/crm/flow/CreateFilterFlow_Factory;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/ui/crm/flow/CreateFilterFlow_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/flow/CreateFilterFlow_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lflow/Flow;Lcom/squareup/analytics/Analytics;)Lcom/squareup/ui/crm/flow/CreateFilterFlow;
    .locals 1

    .line 39
    new-instance v0, Lcom/squareup/ui/crm/flow/CreateFilterFlow;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/flow/CreateFilterFlow;-><init>(Lflow/Flow;Lcom/squareup/analytics/Analytics;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/crm/flow/CreateFilterFlow;
    .locals 2

    .line 30
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/CreateFilterFlow_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflow/Flow;

    iget-object v1, p0, Lcom/squareup/ui/crm/flow/CreateFilterFlow_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/analytics/Analytics;

    invoke-static {v0, v1}, Lcom/squareup/ui/crm/flow/CreateFilterFlow_Factory;->newInstance(Lflow/Flow;Lcom/squareup/analytics/Analytics;)Lcom/squareup/ui/crm/flow/CreateFilterFlow;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/ui/crm/flow/CreateFilterFlow_Factory;->get()Lcom/squareup/ui/crm/flow/CreateFilterFlow;

    move-result-object v0

    return-object v0
.end method
