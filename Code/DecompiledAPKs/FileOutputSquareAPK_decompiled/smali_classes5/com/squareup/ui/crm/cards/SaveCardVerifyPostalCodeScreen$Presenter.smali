.class public Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "SaveCardVerifyPostalCodeScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeView;",
        ">;"
    }
.end annotation


# instance fields
.field private final res:Lcom/squareup/util/Res;

.field private final runner:Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Runner;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final state:Lcom/squareup/ui/crm/flow/SaveCardSharedState;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Runner;Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 84
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 85
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Runner;

    .line 86
    iput-object p2, p0, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 87
    iput-object p3, p0, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 89
    invoke-interface {p1}, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Runner;->getStateForSaveCardScreens()Lcom/squareup/ui/crm/flow/SaveCardSharedState;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Presenter;->state:Lcom/squareup/ui/crm/flow/SaveCardSharedState;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Presenter;)Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Runner;
    .locals 0

    .line 77
    iget-object p0, p0, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Runner;

    return-object p0
.end method

.method private initializeUi(Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeView;)V
    .locals 6

    .line 133
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/UserSettings;->getCountryCode()Lcom/squareup/CountryCode;

    move-result-object v0

    .line 134
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Presenter;->res:Lcom/squareup/util/Res;

    invoke-static {v0}, Lcom/squareup/address/CountryResources;->billingTitle(Lcom/squareup/CountryCode;)I

    move-result v2

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 135
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Presenter;->res:Lcom/squareup/util/Res;

    invoke-static {v0}, Lcom/squareup/address/CountryResources;->postalHint(Lcom/squareup/CountryCode;)I

    move-result v2

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeView;->setHintText(Ljava/lang/CharSequence;)V

    .line 136
    invoke-virtual {p1, v0}, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeView;->setCountryCode(Lcom/squareup/CountryCode;)V

    .line 138
    iget-object v1, p0, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Runner;

    invoke-interface {v1}, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Runner;->getContactForVerifyPostalCodeScreen()Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v1

    .line 139
    iget-object v2, p0, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v2}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/settings/server/UserSettings;->getBusinessName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "."

    .line 140
    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x0

    .line 141
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 145
    :cond_0
    iget-object v3, v1, Lcom/squareup/protos/client/rolodex/Contact;->display_name:Ljava/lang/String;

    if-eqz v3, :cond_1

    .line 146
    iget-object v1, v1, Lcom/squareup/protos/client/rolodex/Contact;->display_name:Ljava/lang/String;

    goto :goto_0

    .line 148
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Presenter;->state:Lcom/squareup/ui/crm/flow/SaveCardSharedState;

    iget-object v3, v3, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->firstName:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Presenter;->state:Lcom/squareup/ui/crm/flow/SaveCardSharedState;

    iget-object v3, v3, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->lastName:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 151
    :goto_0
    iget-object v3, p0, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Presenter;->state:Lcom/squareup/ui/crm/flow/SaveCardSharedState;

    invoke-virtual {v3}, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->getCard()Lcom/squareup/Card;

    move-result-object v3

    .line 152
    invoke-virtual {v3}, Lcom/squareup/Card;->getPan()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    xor-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_2

    .line 154
    invoke-static {v0}, Lcom/squareup/address/CountryResources;->billingMessage(Lcom/squareup/CountryCode;)I

    move-result v0

    goto :goto_1

    :cond_2
    invoke-static {v0}, Lcom/squareup/address/CountryResources;->billingMessageNoCardName(Lcom/squareup/CountryCode;)I

    move-result v0

    .line 155
    :goto_1
    iget-object v5, p0, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Presenter;->res:Lcom/squareup/util/Res;

    invoke-interface {v5, v0}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v5, "cardowner_name"

    .line 156
    invoke-virtual {v0, v5, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v1, "merchant_name"

    .line 157
    invoke-virtual {v0, v1, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    if-eqz v4, :cond_3

    .line 159
    iget-object v4, p0, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Presenter;->res:Lcom/squareup/util/Res;

    invoke-static {v4, v3}, Lcom/squareup/text/Cards;->formattedBrandAndUnmaskedDigits(Lcom/squareup/util/Res;Lcom/squareup/Card;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "cardname"

    invoke-virtual {v0, v4, v3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    .line 161
    :cond_3
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeView;->setMessageText(Ljava/lang/CharSequence;)V

    .line 163
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/ui/crm/R$string;->crm_cardonfile_verifypostal_help_text:I

    invoke-interface {v0, v3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 164
    invoke-virtual {v0, v1, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 165
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 166
    invoke-virtual {p1, v0}, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeView;->setHelpText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private onAgreeClicked()V
    .locals 2

    .line 174
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Presenter;->state:Lcom/squareup/ui/crm/flow/SaveCardSharedState;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->getCard()Lcom/squareup/Card;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/Card;->getPostalCode()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/text/CardPostalScrubber;->isValid(Ljava/lang/String;)Z

    move-result v0

    const-string v1, "Trying to verify card with invalid postal code"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 176
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeView;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeView;->hideSoftKeyboard()V

    .line 177
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Presenter;->runner:Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Runner;->showSaveCardSpinnerScreen()V

    return-void
.end method

.method private savePostalCode(Ljava/lang/String;)V
    .locals 3

    .line 126
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Presenter;->state:Lcom/squareup/ui/crm/flow/SaveCardSharedState;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->getCard()Lcom/squareup/Card;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/Card;->toBuilder()Lcom/squareup/Card$Builder;

    move-result-object v0

    .line 127
    invoke-virtual {v0, p1}, Lcom/squareup/Card$Builder;->postalCode(Ljava/lang/String;)Lcom/squareup/Card$Builder;

    move-result-object p1

    .line 128
    invoke-virtual {p1}, Lcom/squareup/Card$Builder;->build()Lcom/squareup/Card;

    move-result-object p1

    .line 129
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Presenter;->state:Lcom/squareup/ui/crm/flow/SaveCardSharedState;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->getCardData()Lcom/squareup/protos/client/bills/CardData;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Presenter;->state:Lcom/squareup/ui/crm/flow/SaveCardSharedState;

    invoke-virtual {v2}, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->getEntryMethod()Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    move-result-object v2

    invoke-virtual {v0, p1, v1, v2}, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->setCard(Lcom/squareup/Card;Lcom/squareup/protos/client/bills/CardData;Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)V

    return-void
.end method

.method private updateAgreeButton()V
    .locals 2

    .line 170
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeView;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Presenter;->state:Lcom/squareup/ui/crm/flow/SaveCardSharedState;

    invoke-virtual {v1}, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->getCard()Lcom/squareup/Card;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/Card;->getPostalCode()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/text/CardPostalScrubber;->isValid(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeView;->setAgreeEnabled(Z)V

    return-void
.end method


# virtual methods
.method public synthetic lambda$null$0$SaveCardVerifyPostalCodeScreen$Presenter(Ljava/lang/String;)V
    .locals 0

    .line 109
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 110
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Presenter;->savePostalCode(Ljava/lang/String;)V

    .line 111
    invoke-direct {p0}, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Presenter;->updateAgreeButton()V

    return-void
.end method

.method public synthetic lambda$null$2$SaveCardVerifyPostalCodeScreen$Presenter(Lkotlin/Unit;)V
    .locals 0

    .line 118
    invoke-static {}, Lcom/squareup/thread/enforcer/AndroidMainThreadEnforcer;->checkMainThread()V

    .line 119
    invoke-direct {p0}, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Presenter;->onAgreeClicked()V

    return-void
.end method

.method public synthetic lambda$onLoad$1$SaveCardVerifyPostalCodeScreen$Presenter(Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeView;)Lrx/Subscription;
    .locals 1

    .line 107
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeView;->postalCode()Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$SaveCardVerifyPostalCodeScreen$Presenter$NvrBkdzEGrqA9uamr2TqWdZbOPU;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$SaveCardVerifyPostalCodeScreen$Presenter$NvrBkdzEGrqA9uamr2TqWdZbOPU;-><init>(Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Presenter;)V

    .line 108
    invoke-virtual {p1, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onLoad$3$SaveCardVerifyPostalCodeScreen$Presenter(Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeView;)Lrx/Subscription;
    .locals 1

    .line 116
    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeView;->onAgreeClicked()Lrx/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$SaveCardVerifyPostalCodeScreen$Presenter$yZBTdeVyc9QMcD3XvHo9egsFh-E;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$SaveCardVerifyPostalCodeScreen$Presenter$yZBTdeVyc9QMcD3XvHo9egsFh-E;-><init>(Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Presenter;)V

    .line 117
    invoke-virtual {p1, v0}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 2

    .line 93
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 95
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeView;

    .line 97
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    new-instance v1, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Presenter$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Presenter$1;-><init>(Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Presenter;)V

    invoke-virtual {p1, v0, v1}, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeView;->setUpButton(Lcom/squareup/glyph/GlyphTypeface$Glyph;Landroid/view/View$OnClickListener;)V

    .line 103
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Presenter;->initializeUi(Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeView;)V

    .line 106
    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$SaveCardVerifyPostalCodeScreen$Presenter$spnktFKYieOGx1gEOs7XiPX1d-s;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$SaveCardVerifyPostalCodeScreen$Presenter$spnktFKYieOGx1gEOs7XiPX1d-s;-><init>(Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Presenter;Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 115
    new-instance v0, Lcom/squareup/ui/crm/cards/-$$Lambda$SaveCardVerifyPostalCodeScreen$Presenter$zMlxX02GRqsPht5o5AxYlI8u_DU;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/crm/cards/-$$Lambda$SaveCardVerifyPostalCodeScreen$Presenter$zMlxX02GRqsPht5o5AxYlI8u_DU;-><init>(Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Presenter;Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeView;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 122
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Presenter;->state:Lcom/squareup/ui/crm/flow/SaveCardSharedState;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/flow/SaveCardSharedState;->getCard()Lcom/squareup/Card;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/Card;->getPostalCode()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeView;->showPostalCode(Ljava/lang/String;)V

    return-void
.end method
