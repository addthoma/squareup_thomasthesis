.class public Lcom/squareup/ui/crm/rows/EditEmailAttributeRow;
.super Lcom/squareup/ui/XableAutoCompleteEditText;
.source "EditEmailAttributeRow.java"

# interfaces
.implements Lcom/squareup/ui/crm/rows/HasAttribute;


# instance fields
.field private attributeDefinition:Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;

.field private onEmailChanged:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 27
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/XableAutoCompleteEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/crm/rows/EditEmailAttributeRow;->onEmailChanged:Lcom/jakewharton/rxrelay/PublishRelay;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/crm/rows/EditEmailAttributeRow;)Lcom/jakewharton/rxrelay/PublishRelay;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/squareup/ui/crm/rows/EditEmailAttributeRow;->onEmailChanged:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object p0
.end method


# virtual methods
.method public getAttribute()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;
    .locals 3

    .line 55
    invoke-virtual {p0}, Lcom/squareup/ui/crm/rows/EditEmailAttributeRow;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 56
    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 60
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/crm/rows/EditEmailAttributeRow;->attributeDefinition:Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;

    invoke-static {v1}, Lcom/squareup/crm/util/RolodexContactHelper;->toAttributeBuilder(Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;)Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;

    move-result-object v1

    new-instance v2, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;-><init>()V

    .line 62
    invoke-virtual {v2, v0}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;->email(Ljava/lang/String;)Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;

    move-result-object v0

    .line 63
    invoke-virtual {v0}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data$Builder;->build()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;

    move-result-object v0

    .line 61
    invoke-virtual {v1, v0}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;->data(Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;)Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;

    move-result-object v0

    .line 64
    invoke-virtual {v0}, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Builder;->build()Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;

    move-result-object v0

    return-object v0
.end method

.method public onEmailChanged()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 51
    iget-object v0, p0, Lcom/squareup/ui/crm/rows/EditEmailAttributeRow;->onEmailChanged:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object v0
.end method

.method public showAttribute(Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;)V
    .locals 3

    .line 31
    iput-object p1, p0, Lcom/squareup/ui/crm/rows/EditEmailAttributeRow;->attributeDefinition:Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;

    if-eqz p2, :cond_0

    .line 34
    iget-object p2, p2, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute;->data:Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;

    iget-object p2, p2, Lcom/squareup/protos/client/rolodex/AttributeSchema$Attribute$Data;->email:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    invoke-virtual {p0, p2}, Lcom/squareup/ui/crm/rows/EditEmailAttributeRow;->setText(Ljava/lang/CharSequence;)V

    .line 36
    new-instance p2, Lcom/squareup/ui/crm/rows/EditEmailAttributeRow$1;

    new-instance v0, Lcom/squareup/text/EmailScrubber;

    invoke-direct {v0}, Lcom/squareup/text/EmailScrubber;-><init>()V

    invoke-direct {p2, p0, v0, p0}, Lcom/squareup/ui/crm/rows/EditEmailAttributeRow$1;-><init>(Lcom/squareup/ui/crm/rows/EditEmailAttributeRow;Lcom/squareup/text/InsertingScrubber;Lcom/squareup/text/HasSelectableText;)V

    invoke-virtual {p0, p2}, Lcom/squareup/ui/crm/rows/EditEmailAttributeRow;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 42
    new-instance p2, Lcom/squareup/widgets/EmailAutoCompleteFilterAdapter;

    .line 43
    invoke-virtual {p0}, Lcom/squareup/ui/crm/rows/EditEmailAttributeRow;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Lcom/squareup/util/Res$RealRes;

    invoke-virtual {p0}, Lcom/squareup/ui/crm/rows/EditEmailAttributeRow;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/util/Res$RealRes;-><init>(Landroid/content/res/Resources;)V

    invoke-direct {p2, v0, v1}, Lcom/squareup/widgets/EmailAutoCompleteFilterAdapter;-><init>(Landroid/content/Context;Lcom/squareup/util/Res;)V

    .line 42
    invoke-virtual {p0, p2}, Lcom/squareup/ui/crm/rows/EditEmailAttributeRow;->setAutoCompleteAdapter(Landroid/widget/ArrayAdapter;)V

    .line 45
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->name:Ljava/lang/String;

    invoke-virtual {p0, p2}, Lcom/squareup/ui/crm/rows/EditEmailAttributeRow;->setHint(Ljava/lang/String;)V

    .line 46
    iget-object p2, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->is_visible_in_profile:Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    if-eqz p2, :cond_1

    const/4 p2, 0x0

    goto :goto_1

    :cond_1
    const/16 p2, 0x8

    :goto_1
    invoke-virtual {p0, p2}, Lcom/squareup/ui/crm/rows/EditEmailAttributeRow;->setVisibility(I)V

    .line 47
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/AttributeSchema$AttributeDefinition;->is_read_only:Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/rows/EditEmailAttributeRow;->setFocusable(Z)V

    return-void
.end method
