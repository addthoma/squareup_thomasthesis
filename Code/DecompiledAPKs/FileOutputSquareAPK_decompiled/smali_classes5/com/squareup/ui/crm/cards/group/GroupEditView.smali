.class public Lcom/squareup/ui/crm/cards/group/GroupEditView;
.super Landroid/widget/LinearLayout;
.source "GroupEditView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/cards/group/GroupEditView$Component;,
        Lcom/squareup/ui/crm/cards/group/GroupEditView$SharedScope;
    }
.end annotation


# instance fields
.field private final groupName:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private groupNameEdit:Lcom/squareup/ui/XableEditText;

.field presenter:Lcom/squareup/ui/crm/cards/group/GroupEditPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 37
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 34
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/crm/cards/group/GroupEditView;->groupName:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 38
    const-class p2, Lcom/squareup/ui/crm/cards/group/GroupEditView$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/ui/crm/cards/group/GroupEditView$Component;

    invoke-interface {p2, p0}, Lcom/squareup/ui/crm/cards/group/GroupEditView$Component;->inject(Lcom/squareup/ui/crm/cards/group/GroupEditView;)V

    .line 39
    sget p2, Lcom/squareup/crm/R$layout;->crm_group_edit_view:I

    invoke-static {p1, p2, p0}, Lcom/squareup/ui/crm/cards/group/GroupEditView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    const/4 p1, 0x1

    .line 40
    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/cards/group/GroupEditView;->setOrientation(I)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/crm/cards/group/GroupEditView;)Lcom/jakewharton/rxrelay2/PublishRelay;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/squareup/ui/crm/cards/group/GroupEditView;->groupName:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-object p0
.end method


# virtual methods
.method public group()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/protos/client/rolodex/Group;",
            ">;"
        }
    .end annotation

    .line 76
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/group/GroupEditView;->presenter:Lcom/squareup/ui/crm/cards/group/GroupEditPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/crm/cards/group/GroupEditPresenter;->group()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method groupName()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 84
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/group/GroupEditView;->groupName:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-object v0
.end method

.method public synthetic lambda$setInitialFocus$0$GroupEditView()V
    .locals 1

    .line 72
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/group/GroupEditView;->groupNameEdit:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0}, Lcom/squareup/ui/XableEditText;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Views;->showSoftKeyboard(Landroid/view/View;)V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 56
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/group/GroupEditView;->presenter:Lcom/squareup/ui/crm/cards/group/GroupEditPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/cards/group/GroupEditPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/group/GroupEditView;->presenter:Lcom/squareup/ui/crm/cards/group/GroupEditPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/cards/group/GroupEditPresenter;->dropView(Ljava/lang/Object;)V

    .line 62
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 44
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 46
    sget v0, Lcom/squareup/crm/R$id;->crm_group_name_field:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/XableEditText;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/group/GroupEditView;->groupNameEdit:Lcom/squareup/ui/XableEditText;

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/group/GroupEditView;->groupNameEdit:Lcom/squareup/ui/XableEditText;

    new-instance v1, Lcom/squareup/ui/crm/cards/group/GroupEditView$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/group/GroupEditView$1;-><init>(Lcom/squareup/ui/crm/cards/group/GroupEditView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/XableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method

.method public setEnabled(Z)V
    .locals 1

    .line 66
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/group/GroupEditView;->groupNameEdit:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/XableEditText;->setEnabled(Z)V

    return-void
.end method

.method public setGroup(Lcom/squareup/protos/client/rolodex/Group;)V
    .locals 1

    .line 80
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/group/GroupEditView;->presenter:Lcom/squareup/ui/crm/cards/group/GroupEditPresenter;

    invoke-virtual {v0, p0, p1}, Lcom/squareup/ui/crm/cards/group/GroupEditPresenter;->setGroup(Lcom/squareup/ui/crm/cards/group/GroupEditView;Lcom/squareup/protos/client/rolodex/Group;)V

    return-void
.end method

.method setGroupName(Ljava/lang/String;)V
    .locals 1

    .line 88
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/group/GroupEditView;->groupNameEdit:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/XableEditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setInitialFocus()V
    .locals 2

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/group/GroupEditView;->groupNameEdit:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0}, Lcom/squareup/ui/XableEditText;->requestFocus()Z

    .line 72
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/group/GroupEditView;->groupNameEdit:Lcom/squareup/ui/XableEditText;

    new-instance v1, Lcom/squareup/ui/crm/cards/group/-$$Lambda$GroupEditView$LS41fncHn3K8YuDDH6IMMyFxSmY;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/group/-$$Lambda$GroupEditView$LS41fncHn3K8YuDDH6IMMyFxSmY;-><init>(Lcom/squareup/ui/crm/cards/group/GroupEditView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/XableEditText;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
