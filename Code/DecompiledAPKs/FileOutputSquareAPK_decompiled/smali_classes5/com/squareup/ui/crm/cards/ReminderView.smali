.class public Lcom/squareup/ui/crm/cards/ReminderView;
.super Landroid/widget/LinearLayout;
.source "ReminderView.java"


# instance fields
.field private datePicker:Lcom/squareup/marin/widgets/MarinDatePicker;

.field private final onReminderOptionClicked:Lcom/jakewharton/rxrelay/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/PublishRelay<",
            "Lcom/squareup/ui/crm/cards/ReminderScreen$ReminderOption;",
            ">;"
        }
    .end annotation
.end field

.field private oneMonth:Landroid/widget/Button;

.field private oneWeek:Landroid/widget/Button;

.field presenter:Lcom/squareup/ui/crm/cards/ReminderScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private removeReminder:Lcom/squareup/ui/ConfirmButton;

.field private timePicker:Lcom/squareup/marin/widgets/MarinTimePicker;

.field private tomorrow:Landroid/widget/Button;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 41
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31
    invoke-static {}, Lcom/jakewharton/rxrelay/PublishRelay;->create()Lcom/jakewharton/rxrelay/PublishRelay;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/crm/cards/ReminderView;->onReminderOptionClicked:Lcom/jakewharton/rxrelay/PublishRelay;

    .line 42
    const-class p2, Lcom/squareup/ui/crm/cards/ReminderScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/cards/ReminderScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/crm/cards/ReminderScreen$Component;->inject(Lcom/squareup/ui/crm/cards/ReminderView;)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/crm/cards/ReminderView;)Lcom/jakewharton/rxrelay/PublishRelay;
    .locals 0

    .line 27
    iget-object p0, p0, Lcom/squareup/ui/crm/cards/ReminderView;->onReminderOptionClicked:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object p0
.end method

.method private bindViews()V
    .locals 1

    .line 105
    sget v0, Lcom/squareup/crmscreens/R$id;->crm_reminder_tomorrow:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/ReminderView;->tomorrow:Landroid/widget/Button;

    .line 106
    sget v0, Lcom/squareup/crmscreens/R$id;->crm_reminder_one_week:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/ReminderView;->oneWeek:Landroid/widget/Button;

    .line 107
    sget v0, Lcom/squareup/crmscreens/R$id;->crm_reminder_one_month:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/ReminderView;->oneMonth:Landroid/widget/Button;

    .line 108
    sget v0, Lcom/squareup/crmscreens/R$id;->reminder_date:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/MarinDatePicker;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/ReminderView;->datePicker:Lcom/squareup/marin/widgets/MarinDatePicker;

    .line 109
    sget v0, Lcom/squareup/crmscreens/R$id;->reminder_time:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/MarinTimePicker;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/ReminderView;->timePicker:Lcom/squareup/marin/widgets/MarinTimePicker;

    .line 110
    sget v0, Lcom/squareup/crmscreens/R$id;->crm_remove_reminder:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ConfirmButton;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/ReminderView;->removeReminder:Lcom/squareup/ui/ConfirmButton;

    return-void
.end method


# virtual methods
.method actionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 83
    invoke-static {p0}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method getCalendar()Ljava/util/Calendar;
    .locals 7

    .line 87
    new-instance v6, Ljava/util/GregorianCalendar;

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ReminderView;->datePicker:Lcom/squareup/marin/widgets/MarinDatePicker;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinDatePicker;->getMonth()I

    move-result v2

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ReminderView;->datePicker:Lcom/squareup/marin/widgets/MarinDatePicker;

    .line 88
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinDatePicker;->getDayOfMonth()I

    move-result v3

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ReminderView;->timePicker:Lcom/squareup/marin/widgets/MarinTimePicker;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinTimePicker;->getCurrentHour()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ReminderView;->timePicker:Lcom/squareup/marin/widgets/MarinTimePicker;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinTimePicker;->getCurrentMinute()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Ljava/util/GregorianCalendar;-><init>(IIIII)V

    return-object v6
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 73
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ReminderView;->presenter:Lcom/squareup/ui/crm/cards/ReminderScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/cards/ReminderScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ReminderView;->presenter:Lcom/squareup/ui/crm/cards/ReminderScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/cards/ReminderScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 79
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    .line 46
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 47
    invoke-direct {p0}, Lcom/squareup/ui/crm/cards/ReminderView;->bindViews()V

    .line 49
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ReminderView;->datePicker:Lcom/squareup/marin/widgets/MarinDatePicker;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinDatePicker;->getYearPicker()Landroid/widget/NumberPicker;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/NumberPicker;->setVisibility(I)V

    .line 51
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ReminderView;->removeReminder:Lcom/squareup/ui/ConfirmButton;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/ReminderView;->presenter:Lcom/squareup/ui/crm/cards/ReminderScreen$Presenter;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/crm/cards/-$$Lambda$-BxYS9SjXACgeMZ8KjRwSqvs_Nk;

    invoke-direct {v2, v1}, Lcom/squareup/ui/crm/cards/-$$Lambda$-BxYS9SjXACgeMZ8KjRwSqvs_Nk;-><init>(Lcom/squareup/ui/crm/cards/ReminderScreen$Presenter;)V

    invoke-virtual {v0, v2}, Lcom/squareup/ui/ConfirmButton;->setOnConfirmListener(Lcom/squareup/ui/ConfirmableButton$OnConfirmListener;)V

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ReminderView;->tomorrow:Landroid/widget/Button;

    new-instance v1, Lcom/squareup/ui/crm/cards/ReminderView$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/ReminderView$1;-><init>(Lcom/squareup/ui/crm/cards/ReminderView;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ReminderView;->oneWeek:Landroid/widget/Button;

    new-instance v1, Lcom/squareup/ui/crm/cards/ReminderView$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/ReminderView$2;-><init>(Lcom/squareup/ui/crm/cards/ReminderView;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ReminderView;->oneMonth:Landroid/widget/Button;

    new-instance v1, Lcom/squareup/ui/crm/cards/ReminderView$3;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/ReminderView$3;-><init>(Lcom/squareup/ui/crm/cards/ReminderView;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method onReminderOptionClicked()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/ui/crm/cards/ReminderScreen$ReminderOption;",
            ">;"
        }
    .end annotation

    .line 101
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/ReminderView;->onReminderOptionClicked:Lcom/jakewharton/rxrelay/PublishRelay;

    return-object v0
.end method

.method setTime(Ljava/util/Date;)V
    .locals 4

    .line 92
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 93
    invoke-virtual {v0, p1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 94
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/ReminderView;->datePicker:Lcom/squareup/marin/widgets/MarinDatePicker;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    const/4 v3, 0x5

    .line 95
    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    .line 94
    invoke-virtual {p1, v1, v2, v3}, Lcom/squareup/marin/widgets/MarinDatePicker;->updateDate(III)V

    .line 96
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/ReminderView;->timePicker:Lcom/squareup/marin/widgets/MarinTimePicker;

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/marin/widgets/MarinTimePicker;->setCurrentHour(Ljava/lang/Integer;)V

    .line 97
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/ReminderView;->timePicker:Lcom/squareup/marin/widgets/MarinTimePicker;

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinTimePicker;->setCurrentMinute(Ljava/lang/Integer;)V

    return-void
.end method
