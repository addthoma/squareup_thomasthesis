.class Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$GroupViewHolder$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "ViewGroupsListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$GroupViewHolder;->bind(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$GroupViewHolder;

.field final synthetic val$group:Lcom/squareup/protos/client/rolodex/Group;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$GroupViewHolder;Lcom/squareup/protos/client/rolodex/Group;)V
    .locals 0

    .line 67
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$GroupViewHolder$1;->this$1:Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$GroupViewHolder;

    iput-object p2, p0, Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$GroupViewHolder$1;->val$group:Lcom/squareup/protos/client/rolodex/Group;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 1

    .line 69
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$GroupViewHolder$1;->val$group:Lcom/squareup/protos/client/rolodex/Group;

    if-nez p1, :cond_0

    .line 70
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$GroupViewHolder$1;->this$1:Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$GroupViewHolder;

    iget-object p1, p1, Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$GroupViewHolder;->this$0:Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter;

    invoke-static {p1}, Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter;->access$100(Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter;)Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator;

    move-result-object p1

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator;->gotoCreateManualGroupScreen(Z)V

    goto :goto_0

    .line 72
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$GroupViewHolder$1;->this$1:Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$GroupViewHolder;

    iget-object p1, p1, Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$GroupViewHolder;->this$0:Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter;

    invoke-static {p1}, Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter;->access$100(Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter;)Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/ViewGroupsListAdapter$GroupViewHolder$1;->val$group:Lcom/squareup/protos/client/rolodex/Group;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/crm/v2/ViewGroupsListCoordinator;->showGroup(Lcom/squareup/protos/client/rolodex/Group;)V

    :goto_0
    return-void
.end method
