.class public Lcom/squareup/ui/crm/cards/SendMessageView;
.super Landroid/widget/LinearLayout;
.source "SendMessageView.java"


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field private addCouponButton:Lcom/squareup/marin/widgets/MarinGlyphTextView;

.field private addCouponContainer:Landroid/view/View;

.field private addCouponRow:Lcom/squareup/ui/crm/rows/AddCouponRow;

.field private closeAddCouponRowButton:Landroid/widget/ImageView;

.field private messageEdit:Landroid/widget/EditText;

.field presenter:Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private progressBar:Landroid/view/View;

.field private sendMessageWarning:Lcom/squareup/marketfont/MarketTextView;

.field private final shortAnimTimeMs:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 42
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 43
    const-class p2, Lcom/squareup/ui/crm/cards/SendMessageScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/cards/SendMessageScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/crm/cards/SendMessageScreen$Component;->inject(Lcom/squareup/ui/crm/cards/SendMessageView;)V

    .line 45
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/SendMessageView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const/high16 p2, 0x10e0000

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result p1

    iput p1, p0, Lcom/squareup/ui/crm/cards/SendMessageView;->shortAnimTimeMs:I

    return-void
.end method

.method private bindViews()V
    .locals 2

    .line 126
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/SendMessageView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    .line 127
    sget v0, Lcom/squareup/crmscreens/R$id;->crm_message_edit:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/SendMessageView;->messageEdit:Landroid/widget/EditText;

    .line 128
    sget v0, Lcom/squareup/crmscreens/R$id;->crm_progress_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/SendMessageView;->progressBar:Landroid/view/View;

    .line 129
    sget v0, Lcom/squareup/crmscreens/R$id;->crm_coupon_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/SendMessageView;->addCouponContainer:Landroid/view/View;

    .line 130
    sget v0, Lcom/squareup/crmscreens/R$id;->crm_add_coupon_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/MarinGlyphTextView;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/SendMessageView;->addCouponButton:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    .line 131
    sget v0, Lcom/squareup/crmscreens/R$id;->add_coupon_row:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/rows/AddCouponRow;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/SendMessageView;->addCouponRow:Lcom/squareup/ui/crm/rows/AddCouponRow;

    .line 132
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SendMessageView;->addCouponRow:Lcom/squareup/ui/crm/rows/AddCouponRow;

    sget v1, Lcom/squareup/crm/R$id;->crm_remove_add_coupon_row:I

    .line 133
    invoke-static {v0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/SendMessageView;->closeAddCouponRowButton:Landroid/widget/ImageView;

    .line 134
    sget v0, Lcom/squareup/crmscreens/R$id;->crm_send_message_warning:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/SendMessageView;->sendMessageWarning:Lcom/squareup/marketfont/MarketTextView;

    return-void
.end method


# virtual methods
.method public getMessage()Ljava/lang/String;
    .locals 1

    .line 122
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SendMessageView;->messageEdit:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$onFinishInflate$0$SendMessageView(Landroid/view/View;)V
    .locals 0

    .line 52
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/SendMessageView;->presenter:Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;

    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;->onAddCouponClicked()V

    return-void
.end method

.method public synthetic lambda$onFinishInflate$1$SendMessageView(Landroid/view/View;)V
    .locals 0

    .line 53
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/SendMessageView;->presenter:Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;

    invoke-virtual {p1}, Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;->onRemoveCouponClicked()V

    return-void
.end method

.method public synthetic lambda$setInitialFocus$2$SendMessageView()V
    .locals 1

    .line 73
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SendMessageView;->messageEdit:Landroid/widget/EditText;

    invoke-static {v0}, Lcom/squareup/util/Views;->showSoftKeyboard(Landroid/view/View;)V

    return-void
.end method

.method messageIsBlank()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 118
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SendMessageView;->messageEdit:Landroid/widget/EditText;

    invoke-static {v0}, Lcom/squareup/util/RxViews;->isBlank(Landroid/widget/TextView;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 57
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 58
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SendMessageView;->presenter:Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SendMessageView;->presenter:Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/cards/SendMessageScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 63
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 49
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 50
    invoke-direct {p0}, Lcom/squareup/ui/crm/cards/SendMessageView;->bindViews()V

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SendMessageView;->addCouponButton:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$SendMessageView$WyDY6xAFxnuUMz8GdfP-y2Ml6HM;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$SendMessageView$WyDY6xAFxnuUMz8GdfP-y2Ml6HM;-><init>(Lcom/squareup/ui/crm/cards/SendMessageView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SendMessageView;->closeAddCouponRowButton:Landroid/widget/ImageView;

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$SendMessageView$L32ueTRb7aVKeqaT4u8z5Vl0iyo;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$SendMessageView$L32ueTRb7aVKeqaT4u8z5Vl0iyo;-><init>(Lcom/squareup/ui/crm/cards/SendMessageView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method setActionBarConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V
    .locals 1

    .line 89
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SendMessageView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method setActionBarPrimaryButtonEnabled(Z)V
    .locals 1

    .line 93
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SendMessageView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setPrimaryButtonEnabled(Z)V

    return-void
.end method

.method setActionBarUpButtonEnabled(Z)V
    .locals 1

    .line 97
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SendMessageView;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonEnabled(Z)V

    return-void
.end method

.method public setEnabled(Z)V
    .locals 1

    .line 67
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SendMessageView;->messageEdit:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setEnabled(Z)V

    return-void
.end method

.method setInitialFocus()V
    .locals 2

    .line 71
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SendMessageView;->messageEdit:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 73
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SendMessageView;->messageEdit:Landroid/widget/EditText;

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$SendMessageView$HeGMMK67ZDOQEslhOLABRRQTpq0;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$SendMessageView$HeGMMK67ZDOQEslhOLABRRQTpq0;-><init>(Lcom/squareup/ui/crm/cards/SendMessageView;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method showAddCouponButton()V
    .locals 2

    .line 107
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SendMessageView;->addCouponRow:Lcom/squareup/ui/crm/rows/AddCouponRow;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/ui/crm/rows/AddCouponRow;->setVisibility(I)V

    .line 108
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SendMessageView;->addCouponButton:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setVisibility(I)V

    return-void
.end method

.method showAddCouponContainer()V
    .locals 2

    .line 102
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SendMessageView;->addCouponContainer:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 103
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SendMessageView;->sendMessageWarning:Lcom/squareup/marketfont/MarketTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    return-void
.end method

.method public showCouponRow(Ljava/lang/CharSequence;)V
    .locals 2

    .line 112
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SendMessageView;->addCouponButton:Lcom/squareup/marin/widgets/MarinGlyphTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setVisibility(I)V

    .line 113
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SendMessageView;->addCouponRow:Lcom/squareup/ui/crm/rows/AddCouponRow;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/ui/crm/rows/AddCouponRow;->setVisibility(I)V

    .line 114
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SendMessageView;->addCouponRow:Lcom/squareup/ui/crm/rows/AddCouponRow;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/crm/rows/AddCouponRow;->setTitle(Ljava/lang/CharSequence;)V

    return-void
.end method

.method showMessage(Ljava/lang/String;)V
    .locals 1

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SendMessageView;->messageEdit:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method showProgress(Z)V
    .locals 1

    if-eqz p1, :cond_0

    .line 82
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/SendMessageView;->progressBar:Landroid/view/View;

    iget v0, p0, Lcom/squareup/ui/crm/cards/SendMessageView;->shortAnimTimeMs:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->fadeIn(Landroid/view/View;I)V

    goto :goto_0

    .line 84
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/SendMessageView;->progressBar:Landroid/view/View;

    iget v0, p0, Lcom/squareup/ui/crm/cards/SendMessageView;->shortAnimTimeMs:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->fadeOutToGone(Landroid/view/View;I)V

    :goto_0
    return-void
.end method
