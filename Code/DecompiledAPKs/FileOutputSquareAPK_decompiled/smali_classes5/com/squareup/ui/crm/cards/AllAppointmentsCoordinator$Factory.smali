.class public Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator$Factory;
.super Ljava/lang/Object;
.source "AllAppointmentsCoordinator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# instance fields
.field private final res:Lcom/squareup/util/Res;

.field private final runner:Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Runner;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Runner;Lcom/squareup/util/Res;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator$Factory;->runner:Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Runner;

    .line 48
    iput-object p2, p0, Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator$Factory;->res:Lcom/squareup/util/Res;

    return-void
.end method


# virtual methods
.method public create(Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Type;)Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator;
    .locals 4

    .line 52
    new-instance v0, Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator$Factory;->runner:Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Runner;

    iget-object v2, p0, Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator$Factory;->res:Lcom/squareup/util/Res;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, p1, v3}, Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator;-><init>(Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Runner;Lcom/squareup/util/Res;Lcom/squareup/ui/crm/cards/AllAppointmentsScreen$Type;Lcom/squareup/ui/crm/cards/AllAppointmentsCoordinator$1;)V

    return-object v0
.end method
