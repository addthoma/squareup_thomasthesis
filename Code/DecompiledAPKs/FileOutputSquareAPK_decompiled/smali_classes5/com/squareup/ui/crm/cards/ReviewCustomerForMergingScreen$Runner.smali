.class public interface abstract Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingScreen$Runner;
.super Ljava/lang/Object;
.source "ReviewCustomerForMergingScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation


# virtual methods
.method public abstract cancelReviewCustomerForMergingScreen()V
.end method

.method public abstract getContactForReviewCustomerForMergingScreen()Lcom/squareup/protos/client/rolodex/Contact;
.end method

.method public abstract mergeContact(Lcom/squareup/protos/client/rolodex/Contact;)V
.end method
