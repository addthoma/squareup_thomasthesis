.class public Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "ExpiringPointsCoordinator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator$ExpiringPointsAdapter;
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private adapter:Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator$ExpiringPointsAdapter;

.field private final dateFormat:Ljava/text/DateFormat;

.field private expirationPolicy:Lcom/squareup/marketfont/MarketTextView;

.field private final pointsTermsFormatter:Lcom/squareup/loyalty/PointsTermsFormatter;

.field private progressBar:Landroid/view/View;

.field private recyclerView:Landroidx/recyclerview/widget/RecyclerView;

.field private final runner:Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen$Runner;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen$Runner;Lcom/squareup/loyalty/PointsTermsFormatter;Ljava/text/DateFormat;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 42
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator;->runner:Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen$Runner;

    .line 44
    iput-object p2, p0, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator;->pointsTermsFormatter:Lcom/squareup/loyalty/PointsTermsFormatter;

    .line 45
    iput-object p3, p0, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator;->dateFormat:Ljava/text/DateFormat;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator;)Ljava/text/DateFormat;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator;->dateFormat:Ljava/text/DateFormat;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator;)Lcom/squareup/loyalty/PointsTermsFormatter;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator;->pointsTermsFormatter:Lcom/squareup/loyalty/PointsTermsFormatter;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator;)Lcom/squareup/marketfont/MarketTextView;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator;->expirationPolicy:Lcom/squareup/marketfont/MarketTextView;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator;)Landroid/view/View;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator;->progressBar:Landroid/view/View;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator;)Landroidx/recyclerview/widget/RecyclerView;
    .locals 0

    .line 29
    iget-object p0, p0, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    return-object p0
.end method

.method private bindViews(Landroid/view/View;)V
    .locals 1

    .line 71
    invoke-static {p1}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 72
    sget v0, Lcom/squareup/crmscreens/R$id;->crm_progress_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator;->progressBar:Landroid/view/View;

    .line 73
    sget v0, Lcom/squareup/crmscreens/R$id;->expiring_points_policy:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator;->expirationPolicy:Lcom/squareup/marketfont/MarketTextView;

    .line 74
    sget v0, Lcom/squareup/crmscreens/R$id;->expiring_points_list:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView;

    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 4

    .line 49
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 51
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator;->bindViews(Landroid/view/View;)V

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    new-instance v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator;->pointsTermsFormatter:Lcom/squareup/loyalty/PointsTermsFormatter;

    sget v3, Lcom/squareup/crmscreens/R$string;->crm_loyalty_program_section_view_expiring_points_title:I

    .line 54
    invoke-virtual {v2, v3}, Lcom/squareup/loyalty/PointsTermsFormatter;->plural(I)Ljava/lang/String;

    move-result-object v2

    .line 53
    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonTextBackArrow(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator;->runner:Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen$Runner;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v3, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$O7nyg1uUnTrCbYK_MQQz9VoM0LU;

    invoke-direct {v3, v2}, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$O7nyg1uUnTrCbYK_MQQz9VoM0LU;-><init>(Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen$Runner;)V

    .line 56
    invoke-virtual {v1, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 57
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v1

    .line 53
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator;->runner:Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen$Runner;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$O7nyg1uUnTrCbYK_MQQz9VoM0LU;

    invoke-direct {v1, v0}, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$O7nyg1uUnTrCbYK_MQQz9VoM0LU;-><init>(Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen$Runner;)V

    invoke-static {p1, v1}, Lcom/squareup/workflow/ui/HandlesBack$Helper;->setBackHandler(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 61
    new-instance v0, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator$ExpiringPointsAdapter;

    invoke-static {}, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen$ScreenData;->forLoading()Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen$ScreenData;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator$ExpiringPointsAdapter;-><init>(Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator;Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen$ScreenData;)V

    iput-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator;->adapter:Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator$ExpiringPointsAdapter;

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    iget-object v1, p0, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator;->adapter:Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator$ExpiringPointsAdapter;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    new-instance v1, Lcom/squareup/noho/NohoEdgeDecoration;

    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/noho/NohoEdgeDecoration;-><init>(Landroid/content/res/Resources;)V

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;)V

    .line 65
    new-instance v0, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ExpiringPointsCoordinator$GbRdcQ-vaYpTRu-HZJ0XGsmA9D8;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ExpiringPointsCoordinator$GbRdcQ-vaYpTRu-HZJ0XGsmA9D8;-><init>(Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public synthetic lambda$attach$1$ExpiringPointsCoordinator()Lrx/Subscription;
    .locals 2

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator;->runner:Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen$Runner;

    invoke-interface {v0}, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen$Runner;->expiringPointsScreenData()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ExpiringPointsCoordinator$ace0Yksl1CtOkapOoRPJBJ6I8uQ;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/v2/profile/-$$Lambda$ExpiringPointsCoordinator$ace0Yksl1CtOkapOoRPJBJ6I8uQ;-><init>(Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$null$0$ExpiringPointsCoordinator(Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen$ScreenData;)V
    .locals 1

    .line 66
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator;->adapter:Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator$ExpiringPointsAdapter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/crm/v2/profile/ExpiringPointsCoordinator$ExpiringPointsAdapter;->updateView(Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen$ScreenData;)V

    return-void
.end method
