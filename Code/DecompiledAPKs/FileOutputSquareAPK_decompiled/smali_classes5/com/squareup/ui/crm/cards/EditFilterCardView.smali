.class public Lcom/squareup/ui/crm/cards/EditFilterCardView;
.super Landroid/widget/LinearLayout;
.source "EditFilterCardView.java"


# instance fields
.field private emptyFilterContentView:Lcom/squareup/ui/crm/cards/filters/EmptyFilterContentView;

.field private multiOptionFilterContentView:Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentView;

.field presenter:Lcom/squareup/ui/crm/cards/EditFilterScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field removeFilterButton:Lcom/squareup/marketfont/MarketButton;

.field private singleOptionFilterContentView:Lcom/squareup/ui/crm/cards/filters/SingleOptionFilterContentView;

.field private singleTextFilterContentView:Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentView;

.field private visitFrequencyFilterContentView:Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 39
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    const-class p2, Lcom/squareup/ui/crm/cards/EditFilterScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/cards/EditFilterScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/crm/cards/EditFilterScreen$Component;->inject(Lcom/squareup/ui/crm/cards/EditFilterCardView;)V

    return-void
.end method


# virtual methods
.method actionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 60
    invoke-static {p0}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method inflateEmptyFilterContent()Lcom/squareup/ui/crm/cards/filters/EmptyFilterContentView;
    .locals 1

    .line 72
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/EditFilterCardView;->emptyFilterContentView:Lcom/squareup/ui/crm/cards/filters/EmptyFilterContentView;

    if-nez v0, :cond_0

    .line 73
    sget v0, Lcom/squareup/crm/applet/R$id;->crm_empty_filter_content_stub:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 74
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 75
    invoke-virtual {v0}, Landroid/view/ViewStub;->getInflatedId()I

    move-result v0

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/cards/filters/EmptyFilterContentView;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/EditFilterCardView;->emptyFilterContentView:Lcom/squareup/ui/crm/cards/filters/EmptyFilterContentView;

    .line 78
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/EditFilterCardView;->emptyFilterContentView:Lcom/squareup/ui/crm/cards/filters/EmptyFilterContentView;

    return-object v0
.end method

.method inflateMultiOptionFilterContent()Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentView;
    .locals 1

    .line 82
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/EditFilterCardView;->multiOptionFilterContentView:Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentView;

    if-nez v0, :cond_0

    .line 83
    sget v0, Lcom/squareup/crm/applet/R$id;->crm_multi_option_filter_content_stub:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 84
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 85
    invoke-virtual {v0}, Landroid/view/ViewStub;->getInflatedId()I

    move-result v0

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentView;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/EditFilterCardView;->multiOptionFilterContentView:Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentView;

    .line 88
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/EditFilterCardView;->multiOptionFilterContentView:Lcom/squareup/ui/crm/cards/filters/MultiOptionFilterContentView;

    return-object v0
.end method

.method inflateSingleOptionFilterContent()Lcom/squareup/ui/crm/cards/filters/SingleOptionFilterContentView;
    .locals 1

    .line 92
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/EditFilterCardView;->singleOptionFilterContentView:Lcom/squareup/ui/crm/cards/filters/SingleOptionFilterContentView;

    if-nez v0, :cond_0

    .line 93
    sget v0, Lcom/squareup/crm/applet/R$id;->crm_single_option_filter_content_stub:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 94
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 95
    invoke-virtual {v0}, Landroid/view/ViewStub;->getInflatedId()I

    move-result v0

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/cards/filters/SingleOptionFilterContentView;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/EditFilterCardView;->singleOptionFilterContentView:Lcom/squareup/ui/crm/cards/filters/SingleOptionFilterContentView;

    .line 98
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/EditFilterCardView;->singleOptionFilterContentView:Lcom/squareup/ui/crm/cards/filters/SingleOptionFilterContentView;

    return-object v0
.end method

.method inflateSingleTextFilterContent()Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentView;
    .locals 1

    .line 102
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/EditFilterCardView;->singleTextFilterContentView:Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentView;

    if-nez v0, :cond_0

    .line 103
    sget v0, Lcom/squareup/crm/applet/R$id;->crm_single_text_filter_content_stub:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 104
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 105
    invoke-virtual {v0}, Landroid/view/ViewStub;->getInflatedId()I

    move-result v0

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentView;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/EditFilterCardView;->singleTextFilterContentView:Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentView;

    .line 108
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/EditFilterCardView;->singleTextFilterContentView:Lcom/squareup/ui/crm/cards/filters/SingleTextFilterContentView;

    return-object v0
.end method

.method inflateVisitFrequencyFilterContent()Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentView;
    .locals 1

    .line 112
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/EditFilterCardView;->visitFrequencyFilterContentView:Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentView;

    if-nez v0, :cond_0

    .line 113
    sget v0, Lcom/squareup/crm/applet/R$id;->crm_visit_frequency_filter_content_stub:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 114
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 115
    invoke-virtual {v0}, Landroid/view/ViewStub;->getInflatedId()I

    move-result v0

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentView;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/EditFilterCardView;->visitFrequencyFilterContentView:Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentView;

    .line 118
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/EditFilterCardView;->visitFrequencyFilterContentView:Lcom/squareup/ui/crm/cards/filters/VisitFrequencyFilterContentView;

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 50
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 51
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/EditFilterCardView;->presenter:Lcom/squareup/ui/crm/cards/EditFilterScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/cards/EditFilterScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/EditFilterCardView;->presenter:Lcom/squareup/ui/crm/cards/EditFilterScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/cards/EditFilterScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 56
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 44
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 46
    sget v0, Lcom/squareup/crm/applet/R$id;->crm_remove_filter_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketButton;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/EditFilterCardView;->removeFilterButton:Lcom/squareup/marketfont/MarketButton;

    return-void
.end method

.method onRemoveFilterClicked()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/EditFilterCardView;->removeFilterButton:Lcom/squareup/marketfont/MarketButton;

    invoke-static {v0}, Lcom/squareup/util/RxViews;->debouncedOnClicked(Landroid/view/View;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method showRemoveFilterButton()V
    .locals 2

    .line 64
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/EditFilterCardView;->removeFilterButton:Lcom/squareup/marketfont/MarketButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketButton;->setVisibility(I)V

    return-void
.end method
