.class public interface abstract Lcom/squareup/ui/crm/flow/CrmScope$SharedCustomerProfileComponent;
.super Ljava/lang/Object;
.source "CrmScope.java"

# interfaces
.implements Lcom/squareup/ui/crm/flow/CrmScope$BaseComponent;
.implements Lcom/squareup/ui/crm/flow/UpdateCustomerScope$ParentComponent;
.implements Lcom/squareup/ui/crm/edit/EditCustomerScope$ParentComponent;
.implements Lcom/squareup/ui/activity/IssueRefundScope$ParentComponent;
.implements Lcom/squareup/ui/crm/ChooseCustomerScope$ParentComponent;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/flow/CrmScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SharedCustomerProfileComponent"
.end annotation


# virtual methods
.method public abstract addCouponScreen()Lcom/squareup/ui/crm/cards/AddCouponScreen$Component;
.end method

.method public abstract adjustPointsScreen()Lcom/squareup/ui/crm/cards/loyalty/AdjustPointsScreen$Component;
.end method

.method public abstract allCouponsAndRewardsScreen()Lcom/squareup/ui/crm/v2/profile/AllCouponsAndRewardsScreen$Component;
.end method

.method public abstract allNotesScreen()Lcom/squareup/ui/crm/cards/AllNotesScreen$Component;
.end method

.method public abstract billHistoryFlow()Lcom/squareup/ui/crm/flow/BillHistoryFlow;
.end method

.method public abstract billHistoryScreen()Lcom/squareup/ui/crm/cards/BillHistoryScreen$Component;
.end method

.method public abstract confirmSendLoyaltyStatusDialogScreen()Lcom/squareup/ui/crm/cards/loyalty/ConfirmSendLoyaltyStatusDialogScreen$Component;
.end method

.method public abstract conversationCard()Lcom/squareup/ui/crm/cards/ConversationCardScreen$Component;
.end method

.method public abstract createNoteScreen()Lcom/squareup/ui/crm/cards/CreateNoteScreen$Component;
.end method

.method public abstract customerActivityScreen()Lcom/squareup/ui/crm/cards/CustomerActivityScreen$Component;
.end method

.method public abstract customerSaveCardScreen()Lcom/squareup/ui/crm/cards/CustomerSaveCardScreen$Component;
.end method

.method public abstract deleteLoyaltyAccountScreen()Lcom/squareup/ui/crm/v2/DeleteLoyaltyAccountConfirmationScreen$Component;
.end method

.method public abstract deleteSingleCustomerScreen()Lcom/squareup/ui/crm/v2/DeleteSingleCustomerConfirmationScreen$Component;
.end method

.method public abstract deletingLoyaltyAccountScreen()Lcom/squareup/ui/crm/cards/DeletingLoyaltyAccountScreen$Component;
.end method

.method public abstract expiringPointsScreen()Lcom/squareup/ui/crm/v2/profile/ExpiringPointsScreen$Component;
.end method

.method public abstract frequentItemsScreen()Lcom/squareup/ui/crm/cards/AllFrequentItemsScreen$Component;
.end method

.method public abstract invoiceDetailReadOnlyScreen()Lcom/squareup/invoices/ui/InvoiceDetailReadOnlyScreen$Component;
.end method

.method public abstract issueReceiptScreen()Lcom/squareup/ui/activity/IssueReceiptScreen$Component;
.end method

.method public abstract loyaltySectionDropDownDialogScreen()Lcom/squareup/ui/crm/v2/profile/LoyaltySectionBottomDialogScreen$Component;
.end method

.method public abstract manageCouponsAndRewardsScreen()Lcom/squareup/ui/crm/v2/profile/ManageCouponsAndRewardsScreen$Component;
.end method

.method public abstract mergingCustomersScreen()Lcom/squareup/ui/crm/cards/MergingCustomersScreen$Component;
.end method

.method public abstract reminderScreen()Lcom/squareup/ui/crm/cards/ReminderScreen$Component;
.end method

.method public abstract removingCardOnFileScreen()Lcom/squareup/ui/crm/cards/RemovingCardOnFileScreen$Component;
.end method

.method public abstract reviewCustomerForMergingScreen()Lcom/squareup/ui/crm/cards/ReviewCustomerForMergingScreen$Component;
.end method

.method public abstract seeAllRewardTiersScreen()Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersScreen$Component;
.end method

.method public abstract selectGiftReceiptTenderScreen()Lcom/squareup/ui/activity/SelectGiftReceiptTenderScreen$Component;
.end method

.method public abstract selectReceiptTenderScreen()Lcom/squareup/ui/activity/SelectReceiptTenderScreen$Component;
.end method

.method public abstract selectRefundTenderScreen()Lcom/squareup/ui/activity/SelectRefundTenderScreen$Component;
.end method

.method public abstract sendMessageScreen()Lcom/squareup/ui/crm/cards/SendMessageScreen$Component;
.end method

.method public abstract transferringLoyaltyAccountScreen()Lcom/squareup/ui/crm/cards/TransferringLoyaltyAccountScreen$Component;
.end method

.method public abstract updateLoyaltyConflictScreen()Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneConflictDialogScreen$Component;
.end method

.method public abstract updateLoyaltyPhoneScreen()Lcom/squareup/ui/crm/cards/UpdateLoyaltyPhoneScreen$Component;
.end method

.method public abstract viewCustomerCardScreen()Lcom/squareup/ui/crm/v2/ViewCustomerCardScreen$Component;
.end method

.method public abstract viewCustomerDetailScreen()Lcom/squareup/ui/crm/v2/ViewCustomerDetailScreen$Component;
.end method

.method public abstract viewLoyaltyBalanceScreen()Lcom/squareup/ui/crm/cards/loyalty/ViewLoyaltyBalanceScreen$Component;
.end method

.method public abstract viewNoteScreen()Lcom/squareup/ui/crm/cards/ViewNoteScreen$Component;
.end method

.method public abstract voidingCouponsScreen()Lcom/squareup/ui/crm/cards/loyalty/VoidingCouponsScreen$Component;
.end method
