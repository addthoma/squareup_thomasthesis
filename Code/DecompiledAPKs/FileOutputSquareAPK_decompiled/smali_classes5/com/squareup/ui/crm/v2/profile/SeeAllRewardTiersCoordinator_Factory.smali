.class public final Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator_Factory;
.super Ljava/lang/Object;
.source "SeeAllRewardTiersCoordinator_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersScreen$Runner;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/ui/RewardAdapterHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltySettings;",
            ">;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final arg5Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersScreen$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/ui/RewardAdapterHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltySettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper;",
            ">;)V"
        }
    .end annotation

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 33
    iput-object p2, p0, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 34
    iput-object p3, p0, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 35
    iput-object p4, p0, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator_Factory;->arg3Provider:Ljavax/inject/Provider;

    .line 36
    iput-object p5, p0, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator_Factory;->arg4Provider:Ljavax/inject/Provider;

    .line 37
    iput-object p6, p0, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator_Factory;->arg5Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersScreen$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/ui/RewardAdapterHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltySettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper;",
            ">;)",
            "Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator_Factory;"
        }
    .end annotation

    .line 49
    new-instance v7, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersScreen$Runner;Lcom/squareup/util/Res;Lcom/squareup/loyalty/ui/RewardAdapterHelper;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper;)Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;
    .locals 8

    .line 55
    new-instance v7, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;-><init>(Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersScreen$Runner;Lcom/squareup/util/Res;Lcom/squareup/loyalty/ui/RewardAdapterHelper;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;
    .locals 7

    .line 42
    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersScreen$Runner;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/loyalty/ui/RewardAdapterHelper;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/loyalty/LoyaltySettings;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator_Factory;->arg4Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator_Factory;->arg5Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper;

    invoke-static/range {v1 .. v6}, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator_Factory;->newInstance(Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersScreen$Runner;Lcom/squareup/util/Res;Lcom/squareup/loyalty/ui/RewardAdapterHelper;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/crm/v2/profile/RewardRecyclerViewHelper;)Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator_Factory;->get()Lcom/squareup/ui/crm/v2/profile/SeeAllRewardTiersCoordinator;

    move-result-object v0

    return-object v0
.end method
