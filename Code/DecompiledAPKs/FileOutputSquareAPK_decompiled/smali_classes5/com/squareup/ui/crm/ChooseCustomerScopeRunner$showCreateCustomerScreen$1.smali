.class public final Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$showCreateCustomerScreen$1;
.super Lcom/squareup/permissions/PermissionGatekeeper$When;
.source "ChooseCustomerScopeRunner.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->showCreateCustomerScreen()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0011\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H\u0016\u00a8\u0006\u0004"
    }
    d2 = {
        "com/squareup/ui/crm/ChooseCustomerScopeRunner$showCreateCustomerScreen$1",
        "Lcom/squareup/permissions/PermissionGatekeeper$When;",
        "success",
        "",
        "crm-choose-customer_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 301
    iput-object p1, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$showCreateCustomerScreen$1;->this$0:Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;

    invoke-direct {p0}, Lcom/squareup/permissions/PermissionGatekeeper$When;-><init>()V

    return-void
.end method


# virtual methods
.method public success()V
    .locals 9

    .line 303
    iget-object v0, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$showCreateCustomerScreen$1;->this$0:Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;

    invoke-static {v0}, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->access$getFlow$p(Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;)Lflow/Flow;

    move-result-object v0

    .line 304
    iget-object v1, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$showCreateCustomerScreen$1;->this$0:Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;

    invoke-static {v1}, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->access$getUpdateCustomerFlow$p(Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;)Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;

    move-result-object v2

    .line 305
    iget-object v1, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$showCreateCustomerScreen$1;->this$0:Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;

    invoke-static {v1}, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->access$getChooseCustomerScope$p(Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;)Lcom/squareup/ui/crm/ChooseCustomerScope;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/ui/main/RegisterTreeKey;

    .line 306
    sget-object v4, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;->CHOOSE_CUSTOMER:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;

    .line 307
    sget-object v5, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;->CREATE:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;

    .line 308
    iget-object v1, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$showCreateCustomerScreen$1;->this$0:Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;

    invoke-static {v1}, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->access$getChooseCustomerScope$p(Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;)Lcom/squareup/ui/crm/ChooseCustomerScope;

    move-result-object v1

    iget-object v6, v1, Lcom/squareup/ui/crm/ChooseCustomerScope;->createNewCustomerValidationType:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;

    if-nez v6, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    const-string v1, "chooseCustomerScope.crea\u2026wCustomerValidationType!!"

    invoke-static {v6, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 309
    iget-object v1, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$showCreateCustomerScreen$1;->this$0:Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;

    invoke-static {v1}, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->access$getChooseCustomerScope$p(Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;)Lcom/squareup/ui/crm/ChooseCustomerScope;

    move-result-object v1

    iget-object v7, v1, Lcom/squareup/ui/crm/ChooseCustomerScope;->crmScopeType:Lcom/squareup/ui/crm/flow/CrmScopeType;

    if-nez v7, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    .line 310
    :cond_1
    iget-object v1, p0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$showCreateCustomerScreen$1;->this$0:Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;

    invoke-static {v1}, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->access$getSearchTerm$p(Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;)Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v1

    const-string v8, "searchTerm"

    invoke-static {v1, v8}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/jakewharton/rxrelay/BehaviorRelay;->getValue()Ljava/lang/Object;

    move-result-object v1

    const-string v8, "searchTerm.value"

    invoke-static {v1, v8}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/lang/String;

    const/4 v8, 0x0

    invoke-static {v1, v8}, Lcom/squareup/crm/util/RolodexContactHelper;->newContactFromSearchTermOrGroup(Ljava/lang/String;Lcom/squareup/protos/client/rolodex/Group;)Lcom/squareup/protos/client/rolodex/Contact;

    move-result-object v8

    .line 304
    invoke-interface/range {v2 .. v8}, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;->getFirstScreen(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$Type;Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$ContactValidationType;Lcom/squareup/ui/crm/flow/CrmScopeType;Lcom/squareup/protos/client/rolodex/Contact;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v1

    .line 303
    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method
