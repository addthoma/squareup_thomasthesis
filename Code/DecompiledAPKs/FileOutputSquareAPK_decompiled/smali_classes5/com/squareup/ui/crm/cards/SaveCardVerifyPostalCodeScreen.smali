.class public Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen;
.super Lcom/squareup/ui/crm/flow/InCrmScope;
.source "SaveCardVerifyPostalCodeScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/container/layer/FullSheet;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Component;,
        Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Module;,
        Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Presenter;,
        Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen$Runner;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 206
    sget-object v0, Lcom/squareup/ui/crm/cards/-$$Lambda$SaveCardVerifyPostalCodeScreen$weN8_Azli5n23dJ4rVhjEO9rHFA;->INSTANCE:Lcom/squareup/ui/crm/cards/-$$Lambda$SaveCardVerifyPostalCodeScreen$weN8_Azli5n23dJ4rVhjEO9rHFA;

    .line 207
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/main/RegisterTreeKey;)V
    .locals 0

    .line 54
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/flow/InCrmScope;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen;
    .locals 1

    .line 208
    const-class v0, Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/main/RegisterTreeKey;

    .line 209
    new-instance v0, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 202
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/crm/flow/InCrmScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 203
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardVerifyPostalCodeScreen;->crmPath:Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 62
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_CUSTOMER_VERIFY_ZIPCODE:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 58
    sget v0, Lcom/squareup/ui/crm/R$layout;->crm_customer_verify_postalcode_view:I

    return v0
.end method
