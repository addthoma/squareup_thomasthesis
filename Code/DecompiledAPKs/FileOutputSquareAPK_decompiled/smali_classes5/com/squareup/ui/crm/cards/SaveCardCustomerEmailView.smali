.class public Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView;
.super Landroid/widget/FrameLayout;
.source "SaveCardCustomerEmailView.java"


# instance fields
.field private disclaimerView:Lcom/squareup/widgets/MessageView;

.field private emailTextWatcher:Landroid/text/TextWatcher;

.field private emailView:Lcom/squareup/widgets/SensitiveAutoCompleteEditText;

.field private messageView:Lcom/squareup/widgets/MessageView;

.field private nextButton:Lcom/squareup/marketfont/MarketButton;

.field presenter:Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field res:Lcom/squareup/util/Res;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private titleView:Landroid/widget/TextView;

.field private upButton:Lcom/squareup/glyph/SquareGlyphView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 45
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 46
    const-class p2, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen$Component;->inject(Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView;)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView;)Lcom/squareup/widgets/SensitiveAutoCompleteEditText;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView;->emailView:Lcom/squareup/widgets/SensitiveAutoCompleteEditText;

    return-object p0
.end method

.method private bindViews()V
    .locals 1

    .line 119
    sget v0, Lcom/squareup/ui/crm/R$id;->up_button_glyph:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/glyph/SquareGlyphView;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView;->upButton:Lcom/squareup/glyph/SquareGlyphView;

    .line 120
    sget v0, Lcom/squareup/ui/crm/R$id;->crm_cardonfile_customeremail_title:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView;->titleView:Landroid/widget/TextView;

    .line 121
    sget v0, Lcom/squareup/ui/crm/R$id;->crm_cardonfile_customeremail_message:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView;->messageView:Lcom/squareup/widgets/MessageView;

    .line 122
    sget v0, Lcom/squareup/ui/crm/R$id;->crm_cardonfile_email:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/SensitiveAutoCompleteEditText;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView;->emailView:Lcom/squareup/widgets/SensitiveAutoCompleteEditText;

    .line 123
    sget v0, Lcom/squareup/ui/crm/R$id;->crm_cardonfile_next_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketButton;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView;->nextButton:Lcom/squareup/marketfont/MarketButton;

    .line 124
    sget v0, Lcom/squareup/ui/crm/R$id;->crm_cardonfile_email_disclaimer:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView;->disclaimerView:Lcom/squareup/widgets/MessageView;

    return-void
.end method


# virtual methods
.method email()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 94
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView;->emailView:Lcom/squareup/widgets/SensitiveAutoCompleteEditText;

    invoke-static {v0}, Lcom/squareup/util/RxViews;->debouncedShortText(Landroid/widget/TextView;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public hideSoftKeyboard()V
    .locals 0

    .line 115
    invoke-static {p0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    return-void
.end method

.method public synthetic lambda$onFinishInflate$0$SaveCardCustomerEmailView()V
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView;->emailView:Lcom/squareup/widgets/SensitiveAutoCompleteEditText;

    invoke-static {v0}, Lcom/squareup/util/Views;->showSoftKeyboard(Landroid/view/View;)V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 67
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView;->presenter:Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 72
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView;->presenter:Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 73
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 4

    .line 50
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 51
    invoke-direct {p0}, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView;->bindViews()V

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView;->emailView:Lcom/squareup/widgets/SensitiveAutoCompleteEditText;

    invoke-static {v0}, Lcom/squareup/text/EmailScrubber;->watcher(Lcom/squareup/text/HasSelectableText;)Landroid/text/TextWatcher;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView;->emailTextWatcher:Landroid/text/TextWatcher;

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView;->emailView:Lcom/squareup/widgets/SensitiveAutoCompleteEditText;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView;->emailTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SensitiveAutoCompleteEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView;->emailView:Lcom/squareup/widgets/SensitiveAutoCompleteEditText;

    new-instance v1, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView$1;-><init>(Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SensitiveAutoCompleteEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView;->emailView:Lcom/squareup/widgets/SensitiveAutoCompleteEditText;

    new-instance v1, Lcom/squareup/widgets/EmailAutoCompleteFilterAdapter;

    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView;->res:Lcom/squareup/util/Res;

    invoke-direct {v1, v2, v3}, Lcom/squareup/widgets/EmailAutoCompleteFilterAdapter;-><init>(Landroid/content/Context;Lcom/squareup/util/Res;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SensitiveAutoCompleteEditText;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView;->emailView:Lcom/squareup/widgets/SensitiveAutoCompleteEditText;

    invoke-virtual {v0}, Lcom/squareup/widgets/SensitiveAutoCompleteEditText;->requestFocus()Z

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView;->emailView:Lcom/squareup/widgets/SensitiveAutoCompleteEditText;

    new-instance v1, Lcom/squareup/ui/crm/cards/-$$Lambda$SaveCardCustomerEmailView$lwR6lzHnx4B2GeN-IJcjxsZTX1I;

    invoke-direct {v1, p0}, Lcom/squareup/ui/crm/cards/-$$Lambda$SaveCardCustomerEmailView$lwR6lzHnx4B2GeN-IJcjxsZTX1I;-><init>(Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SensitiveAutoCompleteEditText;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method onNextClicked()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 98
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView;->nextButton:Lcom/squareup/marketfont/MarketButton;

    invoke-static {v0}, Lcom/squareup/util/RxViews;->debouncedOnClicked(Landroid/view/View;)Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public setDisclaimerText(Ljava/lang/CharSequence;)V
    .locals 1

    .line 90
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView;->disclaimerView:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method setEmail(Ljava/lang/CharSequence;)V
    .locals 2

    .line 104
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView;->emailView:Lcom/squareup/widgets/SensitiveAutoCompleteEditText;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView;->emailTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SensitiveAutoCompleteEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 105
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView;->emailView:Lcom/squareup/widgets/SensitiveAutoCompleteEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/SensitiveAutoCompleteEditText;->setText(Ljava/lang/CharSequence;)V

    .line 106
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView;->emailView:Lcom/squareup/widgets/SensitiveAutoCompleteEditText;

    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView;->emailTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/SensitiveAutoCompleteEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method

.method public setMessageText(Ljava/lang/CharSequence;)V
    .locals 1

    .line 86
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView;->messageView:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setNextEnabled(Z)V
    .locals 1

    .line 110
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView;->nextButton:Lcom/squareup/marketfont/MarketButton;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketButton;->setEnabled(Z)V

    return-void
.end method

.method public setTitleText(Ljava/lang/CharSequence;)V
    .locals 1

    .line 82
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView;->titleView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setUpButton(Lcom/squareup/glyph/GlyphTypeface$Glyph;Landroid/view/View$OnClickListener;)V
    .locals 1

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView;->upButton:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, p1}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    .line 78
    iget-object p1, p0, Lcom/squareup/ui/crm/cards/SaveCardCustomerEmailView;->upButton:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {p1, p2}, Lcom/squareup/glyph/SquareGlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
