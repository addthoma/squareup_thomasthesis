.class public Lcom/squareup/ui/crm/cards/dedupe/MergeProposalItemView;
.super Landroid/widget/LinearLayout;
.source "MergeProposalItemView.java"


# instance fields
.field private final checkbox:Lcom/squareup/marin/widgets/MarinCheckBox;

.field private final newContactTitle:Landroid/widget/TextView;

.field private final titleRow:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 30
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 31
    sget v0, Lcom/squareup/crm/applet/R$layout;->crm_merge_proposal_item_view:I

    invoke-static {p1, v0, p0}, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalItemView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 33
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalItemView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    .line 36
    sget v0, Lcom/squareup/crm/applet/R$drawable;->crm_merge_proposal_border:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalItemView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 37
    sget v0, Lcom/squareup/marin/R$drawable;->marin_divider_horizontal_light_gray:I

    .line 38
    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    .line 37
    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalItemView;->setDividerDrawable(Landroid/graphics/drawable/Drawable;)V

    const/4 p1, 0x1

    .line 39
    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalItemView;->setOrientation(I)V

    const/4 p1, 0x2

    .line 40
    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalItemView;->setShowDividers(I)V

    .line 42
    sget p1, Lcom/squareup/crm/applet/R$id;->crm_merge_proposal_title_row:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalItemView;->titleRow:Landroid/widget/LinearLayout;

    .line 43
    sget p1, Lcom/squareup/crm/applet/R$id;->crm_merge_proposal_new_contact_title:I

    .line 44
    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalItemView;->newContactTitle:Landroid/widget/TextView;

    .line 45
    sget p1, Lcom/squareup/crm/applet/R$id;->crm_merge_proposal_included_check:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marin/widgets/MarinCheckBox;

    iput-object p1, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalItemView;->checkbox:Lcom/squareup/marin/widgets/MarinCheckBox;

    return-void
.end method


# virtual methods
.method isChecked()Z
    .locals 1

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalItemView;->checkbox:Lcom/squareup/marin/widgets/MarinCheckBox;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinCheckBox;->isChecked()Z

    move-result v0

    return v0
.end method

.method onTitleClicked()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 76
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalItemView;->titleRow:Landroid/widget/LinearLayout;

    invoke-static {v0}, Lcom/squareup/util/rx2/Rx2Views;->debouncedOnClicked(Landroid/view/View;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method setChecked(Z)V
    .locals 1

    .line 72
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalItemView;->checkbox:Lcom/squareup/marin/widgets/MarinCheckBox;

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinCheckBox;->setChecked(Z)V

    return-void
.end method

.method show(Lcom/squareup/protos/client/rolodex/MergeProposal;)V
    .locals 5

    .line 49
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalItemView;->newContactTitle:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/squareup/protos/client/rolodex/MergeProposal;->new_contact:Lcom/squareup/protos/client/rolodex/Contact;

    invoke-static {v1}, Lcom/squareup/crm/util/RolodexContactHelper;->getNonEmptyDisplayName(Lcom/squareup/protos/client/rolodex/Contact;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 51
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalItemView;->getChildCount()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 52
    invoke-virtual {p0, v1}, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalItemView;->removeViewAt(I)V

    goto :goto_0

    .line 55
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalItemView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 56
    iget-object p1, p1, Lcom/squareup/protos/client/rolodex/MergeProposal;->duplicate_contacts:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/protos/client/rolodex/Contact;

    .line 57
    sget v3, Lcom/squareup/widgets/pos/R$layout;->smart_line_row_listitem:I

    const/4 v4, 0x0

    .line 58
    invoke-virtual {v0, v3, p0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/squareup/ui/account/view/SmartLineRow;

    .line 59
    invoke-static {v2}, Lcom/squareup/crm/util/RolodexContactHelper;->getNonEmptyDisplayName(Lcom/squareup/protos/client/rolodex/Contact;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleText(Ljava/lang/CharSequence;)V

    .line 60
    sget-object v4, Lcom/squareup/marketfont/MarketFont$Weight;->REGULAR:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-virtual {v3, v4}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    .line 61
    invoke-static {v2}, Lcom/squareup/crm/util/RolodexContactHelper;->getEmail(Lcom/squareup/protos/client/rolodex/Contact;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueText(Ljava/lang/CharSequence;)V

    .line 62
    invoke-virtual {v3, v1}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueVisible(Z)V

    .line 63
    invoke-virtual {p0, v3}, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalItemView;->addView(Landroid/view/View;)V

    goto :goto_1

    :cond_1
    return-void
.end method

.method showDuplicateContacts(Z)V
    .locals 2

    .line 80
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalItemView;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-lez v0, :cond_0

    .line 81
    invoke-virtual {p0, v0}, Lcom/squareup/ui/crm/cards/dedupe/MergeProposalItemView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_0
    return-void
.end method
