.class public Lcom/squareup/ui/crm/flow/BillHistoryFlow$BillHistoryOrServerError;
.super Ljava/lang/Object;
.source "BillHistoryFlow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/flow/BillHistoryFlow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "BillHistoryOrServerError"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/flow/BillHistoryFlow$BillHistoryOrServerError$ServerErrorDetails;
    }
.end annotation


# instance fields
.field public final billHistory:Lcom/squareup/billhistory/model/BillHistory;

.field public final serverError:Lcom/squareup/ui/crm/flow/BillHistoryFlow$BillHistoryOrServerError$ServerErrorDetails;


# direct methods
.method private constructor <init>(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/ui/crm/flow/BillHistoryFlow$BillHistoryOrServerError$ServerErrorDetails;)V
    .locals 0

    .line 273
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 274
    iput-object p1, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow$BillHistoryOrServerError;->billHistory:Lcom/squareup/billhistory/model/BillHistory;

    .line 275
    iput-object p2, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow$BillHistoryOrServerError;->serverError:Lcom/squareup/ui/crm/flow/BillHistoryFlow$BillHistoryOrServerError$ServerErrorDetails;

    return-void
.end method

.method public static withBillHistory(Lcom/squareup/billhistory/model/BillHistory;)Lcom/squareup/ui/crm/flow/BillHistoryFlow$BillHistoryOrServerError;
    .locals 2

    .line 279
    new-instance v0, Lcom/squareup/ui/crm/flow/BillHistoryFlow$BillHistoryOrServerError;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/ui/crm/flow/BillHistoryFlow$BillHistoryOrServerError;-><init>(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/ui/crm/flow/BillHistoryFlow$BillHistoryOrServerError$ServerErrorDetails;)V

    return-object v0
.end method

.method public static withError(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/ui/crm/flow/BillHistoryFlow$BillHistoryOrServerError;
    .locals 2

    .line 283
    new-instance v0, Lcom/squareup/ui/crm/flow/BillHistoryFlow$BillHistoryOrServerError;

    new-instance v1, Lcom/squareup/ui/crm/flow/BillHistoryFlow$BillHistoryOrServerError$ServerErrorDetails;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/crm/flow/BillHistoryFlow$BillHistoryOrServerError$ServerErrorDetails;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p0, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/ui/crm/flow/BillHistoryFlow$BillHistoryOrServerError;-><init>(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/ui/crm/flow/BillHistoryFlow$BillHistoryOrServerError$ServerErrorDetails;)V

    return-object v0
.end method


# virtual methods
.method public hasBillHistory()Z
    .locals 1

    .line 287
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow$BillHistoryOrServerError;->billHistory:Lcom/squareup/billhistory/model/BillHistory;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hasError()Z
    .locals 1

    .line 291
    iget-object v0, p0, Lcom/squareup/ui/crm/flow/BillHistoryFlow$BillHistoryOrServerError;->serverError:Lcom/squareup/ui/crm/flow/BillHistoryFlow$BillHistoryOrServerError$ServerErrorDetails;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
