.class public Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "MergeCustomersConfirmationScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen$Presenter;,
        Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen$Controller;,
        Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen$Component;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final parentKey:Lcom/squareup/ui/main/RegisterTreeKey;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 156
    sget-object v0, Lcom/squareup/ui/crm/cards/-$$Lambda$MergeCustomersConfirmationScreen$eXsiK9iIaGSwY1lpd7ZmznanwLk;->INSTANCE:Lcom/squareup/ui/crm/cards/-$$Lambda$MergeCustomersConfirmationScreen$eXsiK9iIaGSwY1lpd7ZmznanwLk;

    .line 157
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/main/RegisterTreeKey;)V
    .locals 0

    .line 42
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen;
    .locals 1

    .line 158
    const-class v0, Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/main/RegisterTreeKey;

    .line 159
    new-instance v0, Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 152
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/main/RegisterTreeKey;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 153
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 51
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->CRM_MERGE_CUSTOMERS_CONFIRMATION:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getParentKey()Ljava/lang/Object;
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/MergeCustomersConfirmationScreen;->parentKey:Lcom/squareup/ui/main/RegisterTreeKey;

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 55
    sget v0, Lcom/squareup/crm/applet/R$layout;->crm_merge_customers_confirmation_view:I

    return v0
.end method
