.class public interface abstract Lcom/squareup/ui/crm/cards/EditFilterScreen$ContentView;
.super Ljava/lang/Object;
.source "EditFilterScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/EditFilterScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ContentView"
.end annotation


# virtual methods
.method public abstract filter()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/rolodex/Filter;",
            ">;"
        }
    .end annotation
.end method

.method public abstract isValid()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end method

.method public abstract setFilter(Lcom/squareup/protos/client/rolodex/Filter;)V
.end method
