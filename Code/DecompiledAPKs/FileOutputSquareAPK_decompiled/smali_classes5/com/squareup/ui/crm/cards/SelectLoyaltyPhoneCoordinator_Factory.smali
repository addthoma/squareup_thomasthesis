.class public final Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneCoordinator_Factory;
.super Ljava/lang/Object;
.source "SelectLoyaltyPhoneCoordinator_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneCoordinator;",
        ">;"
    }
.end annotation


# instance fields
.field private final phoneHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/PhoneNumberHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final pointsTermsFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/PointsTermsFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final runnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneScreen$Runner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneScreen$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/PhoneNumberHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/PointsTermsFormatter;",
            ">;)V"
        }
    .end annotation

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneCoordinator_Factory;->runnerProvider:Ljavax/inject/Provider;

    .line 32
    iput-object p2, p0, Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneCoordinator_Factory;->resProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p3, p0, Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneCoordinator_Factory;->phoneHelperProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p4, p0, Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneCoordinator_Factory;->pointsTermsFormatterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneCoordinator_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneScreen$Runner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/PhoneNumberHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/PointsTermsFormatter;",
            ">;)",
            "Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneCoordinator_Factory;"
        }
    .end annotation

    .line 46
    new-instance v0, Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneCoordinator_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneCoordinator_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneScreen$Runner;Lcom/squareup/util/Res;Lcom/squareup/text/PhoneNumberHelper;Lcom/squareup/loyalty/PointsTermsFormatter;)Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneCoordinator;
    .locals 1

    .line 51
    new-instance v0, Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneCoordinator;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneCoordinator;-><init>(Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneScreen$Runner;Lcom/squareup/util/Res;Lcom/squareup/text/PhoneNumberHelper;Lcom/squareup/loyalty/PointsTermsFormatter;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneCoordinator;
    .locals 4

    .line 39
    iget-object v0, p0, Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneCoordinator_Factory;->runnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneScreen$Runner;

    iget-object v1, p0, Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneCoordinator_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/util/Res;

    iget-object v2, p0, Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneCoordinator_Factory;->phoneHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/text/PhoneNumberHelper;

    iget-object v3, p0, Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneCoordinator_Factory;->pointsTermsFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/loyalty/PointsTermsFormatter;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneCoordinator_Factory;->newInstance(Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneScreen$Runner;Lcom/squareup/util/Res;Lcom/squareup/text/PhoneNumberHelper;Lcom/squareup/loyalty/PointsTermsFormatter;)Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneCoordinator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneCoordinator_Factory;->get()Lcom/squareup/ui/crm/cards/SelectLoyaltyPhoneCoordinator;

    move-result-object v0

    return-object v0
.end method
