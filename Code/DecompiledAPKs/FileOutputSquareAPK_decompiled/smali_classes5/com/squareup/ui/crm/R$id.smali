.class public final Lcom/squareup/ui/crm/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final crm_card_input_row_editor:I = 0x7f0a03f9

.field public static final crm_cardonfile_authorize_add_button:I = 0x7f0a03fa

.field public static final crm_cardonfile_customeremail_message:I = 0x7f0a03fe

.field public static final crm_cardonfile_customeremail_title:I = 0x7f0a03ff

.field public static final crm_cardonfile_email:I = 0x7f0a0400

.field public static final crm_cardonfile_email_disclaimer:I = 0x7f0a0401

.field public static final crm_cardonfile_next_button:I = 0x7f0a0402

.field public static final crm_cardonfile_postal_disclaimer:I = 0x7f0a0403

.field public static final crm_cardonfile_postal_keyboard_switch:I = 0x7f0a0404

.field public static final crm_cardonfile_postalcode:I = 0x7f0a0405

.field public static final crm_cardonfile_postalcode_message:I = 0x7f0a0406

.field public static final crm_cardonfile_postalcode_title:I = 0x7f0a0407

.field public static final crm_customer_card_row_view:I = 0x7f0a0421

.field public static final crm_first_name_field:I = 0x7f0a044d

.field public static final crm_footer:I = 0x7f0a044e

.field public static final crm_last_name_field:I = 0x7f0a0467

.field public static final crm_save_card_name_number:I = 0x7f0a04e2

.field public static final crm_save_card_remove_button:I = 0x7f0a04e3

.field public static final crm_title:I = 0x7f0a0506

.field public static final dipped_card_spinner_glyph:I = 0x7f0a05bc

.field public static final dipped_card_spinner_title:I = 0x7f0a05bd

.field public static final up_button_glyph:I = 0x7f0a10c1


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
