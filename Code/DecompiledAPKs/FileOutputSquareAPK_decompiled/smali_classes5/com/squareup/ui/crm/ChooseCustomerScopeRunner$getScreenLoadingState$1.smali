.class final Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$getScreenLoadingState$1;
.super Ljava/lang/Object;
.source "ChooseCustomerScopeRunner.kt"

# interfaces
.implements Lrx/functions/Func1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/crm/ChooseCustomerScopeRunner;->getScreenLoadingState()Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func1<",
        "TT;",
        "Lrx/Observable<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a&\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002 \u0003*\u0012\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002\u0018\u00010\u00010\u00012\u000e\u0010\u0004\u001a\n \u0003*\u0004\u0018\u00010\u00050\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lrx/Observable;",
        "Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;",
        "kotlin.jvm.PlatformType",
        "it",
        "Lcom/squareup/crm/RolodexContactLoader;",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$getScreenLoadingState$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$getScreenLoadingState$1;

    invoke-direct {v0}, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$getScreenLoadingState$1;-><init>()V

    sput-object v0, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$getScreenLoadingState$1;->INSTANCE:Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$getScreenLoadingState$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 65
    check-cast p1, Lcom/squareup/crm/RolodexContactLoader;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/crm/ChooseCustomerScopeRunner$getScreenLoadingState$1;->call(Lcom/squareup/crm/RolodexContactLoader;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method

.method public final call(Lcom/squareup/crm/RolodexContactLoader;)Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/crm/RolodexContactLoader;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/crm/RolodexContactLoaderHelper$VisualState;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    .line 285
    invoke-static {p1, v0}, Lcom/squareup/crm/RolodexContactLoaderHelper;->visualStateOf(Lcom/squareup/crm/RolodexContactLoader;Ljava/lang/String;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method
