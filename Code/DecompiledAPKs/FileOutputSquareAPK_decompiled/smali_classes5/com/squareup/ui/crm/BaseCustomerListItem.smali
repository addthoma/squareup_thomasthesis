.class public abstract Lcom/squareup/ui/crm/BaseCustomerListItem;
.super Ljava/lang/Object;
.source "BaseCustomerListViewHolder.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/BaseCustomerListItem$HeaderItem;,
        Lcom/squareup/ui/crm/BaseCustomerListItem$ContactItem;,
        Lcom/squareup/ui/crm/BaseCustomerListItem$LoadingItem;,
        Lcom/squareup/ui/crm/BaseCustomerListItem$ErrorItem;,
        Lcom/squareup/ui/crm/BaseCustomerListItem$CreateCustomerItem;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0005\u0007\u0008\t\n\u000bB\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u0082\u0001\u0005\u000c\r\u000e\u000f\u0010\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/ui/crm/BaseCustomerListItem;",
        "",
        "ordinal",
        "",
        "(I)V",
        "getOrdinal",
        "()I",
        "ContactItem",
        "CreateCustomerItem",
        "ErrorItem",
        "HeaderItem",
        "LoadingItem",
        "Lcom/squareup/ui/crm/BaseCustomerListItem$HeaderItem;",
        "Lcom/squareup/ui/crm/BaseCustomerListItem$ContactItem;",
        "Lcom/squareup/ui/crm/BaseCustomerListItem$LoadingItem;",
        "Lcom/squareup/ui/crm/BaseCustomerListItem$ErrorItem;",
        "Lcom/squareup/ui/crm/BaseCustomerListItem$CreateCustomerItem;",
        "crm-choose-customer_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final ordinal:I


# direct methods
.method private constructor <init>(I)V
    .locals 0

    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/squareup/ui/crm/BaseCustomerListItem;->ordinal:I

    return-void
.end method

.method public synthetic constructor <init>(ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 71
    invoke-direct {p0, p1}, Lcom/squareup/ui/crm/BaseCustomerListItem;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final getOrdinal()I
    .locals 1

    .line 72
    iget v0, p0, Lcom/squareup/ui/crm/BaseCustomerListItem;->ordinal:I

    return v0
.end method
