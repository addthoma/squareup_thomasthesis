.class public final Lcom/squareup/ui/crm/BaseCustomerListViewHolder$ContactViewHolder;
.super Lcom/squareup/ui/crm/BaseCustomerListViewHolder;
.source "BaseCustomerListViewHolder.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/BaseCustomerListViewHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ContactViewHolder"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBaseCustomerListViewHolder.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BaseCustomerListViewHolder.kt\ncom/squareup/ui/crm/BaseCustomerListViewHolder$ContactViewHolder\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,93:1\n1103#2,7:94\n*E\n*S KotlinDebug\n*F\n+ 1 BaseCustomerListViewHolder.kt\ncom/squareup/ui/crm/BaseCustomerListViewHolder$ContactViewHolder\n*L\n47#1,7:94\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/ui/crm/BaseCustomerListViewHolder$ContactViewHolder;",
        "Lcom/squareup/ui/crm/BaseCustomerListViewHolder;",
        "customerUnitRow",
        "Lcom/squareup/ui/crm/rows/CustomerUnitRow;",
        "(Lcom/squareup/ui/crm/rows/CustomerUnitRow;)V",
        "bind",
        "",
        "item",
        "Lcom/squareup/ui/crm/BaseCustomerListItem;",
        "crm-choose-customer_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final customerUnitRow:Lcom/squareup/ui/crm/rows/CustomerUnitRow;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/crm/rows/CustomerUnitRow;)V
    .locals 2

    const-string v0, "customerUnitRow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    move-object v0, p1

    check-cast v0, Landroid/view/View;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/squareup/ui/crm/BaseCustomerListViewHolder;-><init>(Landroid/view/View;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/ui/crm/BaseCustomerListViewHolder$ContactViewHolder;->customerUnitRow:Lcom/squareup/ui/crm/rows/CustomerUnitRow;

    return-void
.end method


# virtual methods
.method public bind(Lcom/squareup/ui/crm/BaseCustomerListItem;)V
    .locals 2

    const-string v0, "item"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/crm/BaseCustomerListViewHolder$ContactViewHolder;->customerUnitRow:Lcom/squareup/ui/crm/rows/CustomerUnitRow;

    move-object v1, p1

    check-cast v1, Lcom/squareup/ui/crm/BaseCustomerListItem$ContactItem;

    invoke-virtual {v1}, Lcom/squareup/ui/crm/BaseCustomerListItem$ContactItem;->getViewData()Lcom/squareup/ui/crm/rows/CustomerUnitRow$ViewData;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/crm/rows/CustomerUnitRow;->setViewData(Lcom/squareup/ui/crm/rows/CustomerUnitRow$ViewData;)V

    .line 47
    iget-object v0, p0, Lcom/squareup/ui/crm/BaseCustomerListViewHolder$ContactViewHolder;->customerUnitRow:Lcom/squareup/ui/crm/rows/CustomerUnitRow;

    check-cast v0, Landroid/view/View;

    .line 94
    new-instance v1, Lcom/squareup/ui/crm/BaseCustomerListViewHolder$ContactViewHolder$bind$$inlined$onClickDebounced$1;

    invoke-direct {v1, p1}, Lcom/squareup/ui/crm/BaseCustomerListViewHolder$ContactViewHolder$bind$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/ui/crm/BaseCustomerListItem;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
