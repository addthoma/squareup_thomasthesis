.class public final Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailScreen;
.super Lcom/squareup/ui/crm/applet/InCustomersAppletScope;
.source "NoCustomerSelectedDetailScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/coordinators/CoordinatorProvider;
.implements Lcom/squareup/ui/crm/v2/CustomersAppletDetailScreen;
.implements Lcom/squareup/container/MaybePersistent;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailScreen$Component;,
        Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailScreen$Events;,
        Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailScreen$ScreenData;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 32
    new-instance v0, Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailScreen;

    invoke-direct {v0}, Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailScreen;->INSTANCE:Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailScreen;

    .line 46
    sget-object v0, Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailScreen;->INSTANCE:Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailScreen;

    .line 47
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 23
    invoke-direct {p0}, Lcom/squareup/ui/crm/applet/InCustomersAppletScope;-><init>()V

    return-void
.end method


# virtual methods
.method public isPersistent()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 1

    .line 36
    const-class v0, Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailScreen$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailScreen$Component;

    .line 37
    invoke-interface {p1}, Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailScreen$Component;->coordinator()Lcom/squareup/ui/crm/v2/NoCustomerSelectedDetailCoordinator;

    move-result-object p1

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 54
    sget v0, Lcom/squareup/crm/applet/R$layout;->crm_v2_no_customer_selected_tablet:I

    return v0
.end method
