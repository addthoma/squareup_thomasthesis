.class synthetic Lcom/squareup/ui/crm/cards/CustomerActivityHelper$1;
.super Ljava/lang/Object;
.source "CustomerActivityHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/crm/cards/CustomerActivityHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$squareup$protos$client$rolodex$EventType:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 28
    invoke-static {}, Lcom/squareup/protos/client/rolodex/EventType;->values()[Lcom/squareup/protos/client/rolodex/EventType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/ui/crm/cards/CustomerActivityHelper$1;->$SwitchMap$com$squareup$protos$client$rolodex$EventType:[I

    :try_start_0
    sget-object v0, Lcom/squareup/ui/crm/cards/CustomerActivityHelper$1;->$SwitchMap$com$squareup$protos$client$rolodex$EventType:[I

    sget-object v1, Lcom/squareup/protos/client/rolodex/EventType;->PAYMENT:Lcom/squareup/protos/client/rolodex/EventType;

    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/EventType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :try_start_1
    sget-object v0, Lcom/squareup/ui/crm/cards/CustomerActivityHelper$1;->$SwitchMap$com$squareup$protos$client$rolodex$EventType:[I

    sget-object v1, Lcom/squareup/protos/client/rolodex/EventType;->FEEDBACK_CONVERSATION:Lcom/squareup/protos/client/rolodex/EventType;

    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/EventType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    :try_start_2
    sget-object v0, Lcom/squareup/ui/crm/cards/CustomerActivityHelper$1;->$SwitchMap$com$squareup$protos$client$rolodex$EventType:[I

    sget-object v1, Lcom/squareup/protos/client/rolodex/EventType;->LOYALTY_ADJUSTMENT:Lcom/squareup/protos/client/rolodex/EventType;

    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/EventType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    :try_start_3
    sget-object v0, Lcom/squareup/ui/crm/cards/CustomerActivityHelper$1;->$SwitchMap$com$squareup$protos$client$rolodex$EventType:[I

    sget-object v1, Lcom/squareup/protos/client/rolodex/EventType;->SKIPPED:Lcom/squareup/protos/client/rolodex/EventType;

    invoke-virtual {v1}, Lcom/squareup/protos/client/rolodex/EventType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    return-void
.end method
