.class public Lcom/squareup/ui/crm/v2/CreateManualGroupScreen;
.super Lcom/squareup/ui/crm/applet/InCustomersAppletScope;
.source "CreateManualGroupScreen.java"

# interfaces
.implements Lcom/squareup/coordinators/CoordinatorProvider;
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/ui/buyer/PaymentExempt;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/crm/v2/CreateManualGroupScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/crm/v2/CreateManualGroupScreen$Component;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/crm/v2/CreateManualGroupScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/crm/v2/CreateManualGroupScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 27
    new-instance v0, Lcom/squareup/ui/crm/v2/CreateManualGroupScreen;

    invoke-direct {v0}, Lcom/squareup/ui/crm/v2/CreateManualGroupScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/crm/v2/CreateManualGroupScreen;->INSTANCE:Lcom/squareup/ui/crm/v2/CreateManualGroupScreen;

    .line 45
    sget-object v0, Lcom/squareup/ui/crm/v2/CreateManualGroupScreen;->INSTANCE:Lcom/squareup/ui/crm/v2/CreateManualGroupScreen;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/crm/v2/CreateManualGroupScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 24
    invoke-direct {p0}, Lcom/squareup/ui/crm/applet/InCustomersAppletScope;-><init>()V

    return-void
.end method


# virtual methods
.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 1

    .line 30
    const-class v0, Lcom/squareup/ui/crm/v2/CreateManualGroupScreen$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/crm/v2/CreateManualGroupScreen$Component;

    invoke-interface {p1}, Lcom/squareup/ui/crm/v2/CreateManualGroupScreen$Component;->coordinator()Lcom/squareup/ui/crm/v2/CreateManualGroupCoordinator;

    move-result-object p1

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 34
    sget v0, Lcom/squareup/crm/applet/R$layout;->crm_v2_create_manual_group_view:I

    return v0
.end method
