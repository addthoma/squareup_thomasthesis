.class public Lcom/squareup/ui/items/EditCategoryScreen$Module;
.super Ljava/lang/Object;
.source "EditCategoryScreen.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/EditCategoryScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Module"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 352
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method provideAppliedLocationsBannerPresenter(Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;Lcom/squareup/util/Res;Lcom/squareup/ui/items/EditCategoryScreen$Presenter;)Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;
    .locals 8
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 357
    new-instance v7, Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;

    sget-object v3, Lcom/squareup/ui/items/ItemEditingStringIds;->CATEGORY:Lcom/squareup/ui/items/ItemEditingStringIds;

    .line 358
    invoke-virtual {p3}, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->isNewObject()Z

    move-result v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, v7

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;-><init>(Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;Lcom/squareup/util/Res;Lcom/squareup/ui/items/ItemEditingStringIds;ZZZ)V

    return-object v7
.end method
