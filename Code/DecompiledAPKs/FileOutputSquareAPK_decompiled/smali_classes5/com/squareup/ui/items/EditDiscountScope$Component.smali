.class public interface abstract Lcom/squareup/ui/items/EditDiscountScope$Component;
.super Ljava/lang/Object;
.source "EditDiscountScope.java"


# annotations
.annotation runtime Ldagger/Subcomponent;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/EditDiscountScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract editDiscountComponent(Lcom/squareup/ui/items/EditDiscountScreen$Module;)Lcom/squareup/ui/items/EditDiscountScreen$Component;
.end method

.method public abstract scopeRunner()Lcom/squareup/ui/items/EditDiscountScopeRunner;
.end method
