.class public Lcom/squareup/ui/items/EditModifierSetScopeRunner;
.super Ljava/lang/Object;
.source "EditModifierSetScopeRunner.java"

# interfaces
.implements Lmortar/Scoped;


# instance fields
.field private final appliedLocationCountFetcher:Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;

.field private final cogs:Lcom/squareup/cogs/Cogs;

.field private editModifierSetPath:Lcom/squareup/ui/items/EditModifierSetScope;

.field private final editModifierSetStateBehaviorSubject:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Lcom/squareup/ui/items/ModifierSetEditState;",
            ">;"
        }
    .end annotation
.end field

.field private final modifierSetEditState:Lcom/squareup/ui/items/ModifierSetEditState;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;


# direct methods
.method public constructor <init>(Lcom/squareup/cogs/Cogs;Lcom/squareup/ui/items/ModifierSetEditState;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 29
    check-cast v0, Lcom/squareup/ui/items/ModifierSetEditState;

    .line 30
    invoke-static {v0}, Lrx/subjects/BehaviorSubject;->create(Ljava/lang/Object;)Lrx/subjects/BehaviorSubject;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/items/EditModifierSetScopeRunner;->editModifierSetStateBehaviorSubject:Lrx/subjects/BehaviorSubject;

    .line 37
    iput-object p1, p0, Lcom/squareup/ui/items/EditModifierSetScopeRunner;->cogs:Lcom/squareup/cogs/Cogs;

    .line 38
    iput-object p2, p0, Lcom/squareup/ui/items/EditModifierSetScopeRunner;->modifierSetEditState:Lcom/squareup/ui/items/ModifierSetEditState;

    .line 39
    iput-object p3, p0, Lcom/squareup/ui/items/EditModifierSetScopeRunner;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 40
    iput-object p4, p0, Lcom/squareup/ui/items/EditModifierSetScopeRunner;->appliedLocationCountFetcher:Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;

    return-void
.end method

.method static synthetic lambda$onEnterScope$0(Ljava/lang/String;Lcom/squareup/shared/catalog/Catalog$Local;)Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;
    .locals 2

    .line 57
    const-class v0, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;

    .line 58
    invoke-interface {p1, v0, p0}, Lcom/squareup/shared/catalog/Catalog$Local;->findById(Ljava/lang/Class;Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;

    .line 59
    new-instance v1, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;

    invoke-direct {v1, v0}, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;-><init>(Lcom/squareup/shared/catalog/models/CatalogItemModifierList;)V

    .line 62
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemModifierList;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/shared/catalog/Catalog$Local;->findModifierItemMemberships(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 63
    invoke-virtual {v1, v0}, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->addItemItemModifierListMemberships(Ljava/util/List;)V

    .line 64
    invoke-interface {p1, p0}, Lcom/squareup/shared/catalog/Catalog$Local;->findModifierOptions(Ljava/lang/String;)Ljava/util/List;

    move-result-object p0

    .line 65
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;

    .line 66
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemModifierOption;->object()Lcom/squareup/wire/Message;

    move-result-object p1

    check-cast p1, Lcom/squareup/api/items/ItemModifierOption;

    invoke-virtual {p1}, Lcom/squareup/api/items/ItemModifierOption;->newBuilder()Lcom/squareup/api/items/ItemModifierOption$Builder;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;->addModifierOption(Lcom/squareup/api/items/ItemModifierOption$Builder;)V

    goto :goto_0

    :cond_0
    return-object v1
.end method


# virtual methods
.method getEditModifierSetStateObservable()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/ui/items/ModifierSetEditState;",
            ">;"
        }
    .end annotation

    .line 92
    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetScopeRunner;->editModifierSetStateBehaviorSubject:Lrx/subjects/BehaviorSubject;

    return-object v0
.end method

.method getModifierSetId()Ljava/lang/String;
    .locals 1

    .line 96
    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetScopeRunner;->editModifierSetPath:Lcom/squareup/ui/items/EditModifierSetScope;

    iget-object v0, v0, Lcom/squareup/ui/items/EditModifierSetScope;->modifierSetId:Ljava/lang/String;

    return-object v0
.end method

.method isNewModifierSet()Z
    .locals 2

    .line 88
    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetScopeRunner;->editModifierSetPath:Lcom/squareup/ui/items/EditModifierSetScope;

    iget-object v0, v0, Lcom/squareup/ui/items/EditModifierSetScope;->modifierSetId:Ljava/lang/String;

    sget-object v1, Lcom/squareup/ui/items/EditModifierSetScope;->NEW_MODIFIER_SET:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public synthetic lambda$onEnterScope$1$EditModifierSetScopeRunner(Lcom/squareup/shared/catalog/CatalogResult;)V
    .locals 1

    .line 71
    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetScopeRunner;->modifierSetEditState:Lcom/squareup/ui/items/ModifierSetEditState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/ModifierSetEditState;->loaded()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 74
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetScopeRunner;->modifierSetEditState:Lcom/squareup/ui/items/ModifierSetEditState;

    invoke-interface {p1}, Lcom/squareup/shared/catalog/CatalogResult;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/items/ModifierSetEditState;->setModifierSetData(Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;)V

    .line 75
    iget-object p1, p0, Lcom/squareup/ui/items/EditModifierSetScopeRunner;->modifierSetEditState:Lcom/squareup/ui/items/ModifierSetEditState;

    invoke-virtual {p1}, Lcom/squareup/ui/items/ModifierSetEditState;->saveCopyOfItemData()V

    .line 76
    iget-object p1, p0, Lcom/squareup/ui/items/EditModifierSetScopeRunner;->editModifierSetStateBehaviorSubject:Lrx/subjects/BehaviorSubject;

    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetScopeRunner;->modifierSetEditState:Lcom/squareup/ui/items/ModifierSetEditState;

    invoke-virtual {p1, v0}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 44
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/EditModifierSetScope;

    iput-object v0, p0, Lcom/squareup/ui/items/EditModifierSetScopeRunner;->editModifierSetPath:Lcom/squareup/ui/items/EditModifierSetScope;

    .line 45
    invoke-static {p1}, Lmortar/bundler/BundleService;->getBundleService(Lmortar/MortarScope;)Lmortar/bundler/BundleService;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/items/EditModifierSetScopeRunner;->modifierSetEditState:Lcom/squareup/ui/items/ModifierSetEditState;

    invoke-virtual {v0, v1}, Lmortar/bundler/BundleService;->register(Lmortar/bundler/Bundler;)V

    .line 47
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditModifierSetScopeRunner;->isNewModifierSet()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetScopeRunner;->appliedLocationCountFetcher:Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;

    invoke-interface {v0}, Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;->getActiveLocationCount()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetScopeRunner;->appliedLocationCountFetcher:Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 49
    iget-object p1, p0, Lcom/squareup/ui/items/EditModifierSetScopeRunner;->appliedLocationCountFetcher:Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;

    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetScopeRunner;->editModifierSetPath:Lcom/squareup/ui/items/EditModifierSetScope;

    iget-object v0, v0, Lcom/squareup/ui/items/EditModifierSetScope;->modifierSetId:Ljava/lang/String;

    invoke-interface {p1, v0}, Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;->fetchAppliedLocationCount(Ljava/lang/String;)V

    .line 52
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/items/EditModifierSetScopeRunner;->modifierSetEditState:Lcom/squareup/ui/items/ModifierSetEditState;

    new-instance v0, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;

    invoke-direct {v0}, Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;-><init>()V

    invoke-virtual {p1, v0}, Lcom/squareup/ui/items/ModifierSetEditState;->setModifierSetData(Lcom/squareup/ui/items/ModifierSetEditState$ModifierSetData;)V

    .line 54
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditModifierSetScopeRunner;->isNewModifierSet()Z

    move-result p1

    if-nez p1, :cond_1

    .line 55
    iget-object p1, p0, Lcom/squareup/ui/items/EditModifierSetScopeRunner;->editModifierSetPath:Lcom/squareup/ui/items/EditModifierSetScope;

    iget-object p1, p1, Lcom/squareup/ui/items/EditModifierSetScope;->modifierSetId:Ljava/lang/String;

    .line 56
    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetScopeRunner;->cogs:Lcom/squareup/cogs/Cogs;

    new-instance v1, Lcom/squareup/ui/items/-$$Lambda$EditModifierSetScopeRunner$aDgTqFNqCuMd2lKsZVjK5JaUujA;

    invoke-direct {v1, p1}, Lcom/squareup/ui/items/-$$Lambda$EditModifierSetScopeRunner$aDgTqFNqCuMd2lKsZVjK5JaUujA;-><init>(Ljava/lang/String;)V

    new-instance p1, Lcom/squareup/ui/items/-$$Lambda$EditModifierSetScopeRunner$mB2Fd0TTDw79H6TRclXf8DqgdZs;

    invoke-direct {p1, p0}, Lcom/squareup/ui/items/-$$Lambda$EditModifierSetScopeRunner$mB2Fd0TTDw79H6TRclXf8DqgdZs;-><init>(Lcom/squareup/ui/items/EditModifierSetScopeRunner;)V

    invoke-interface {v0, v1, p1}, Lcom/squareup/cogs/Cogs;->execute(Lcom/squareup/shared/catalog/CatalogTask;Lcom/squareup/shared/catalog/CatalogCallback;)V

    goto :goto_0

    .line 79
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/items/EditModifierSetScopeRunner;->modifierSetEditState:Lcom/squareup/ui/items/ModifierSetEditState;

    invoke-virtual {p1}, Lcom/squareup/ui/items/ModifierSetEditState;->saveCopyOfItemData()V

    .line 80
    iget-object p1, p0, Lcom/squareup/ui/items/EditModifierSetScopeRunner;->editModifierSetStateBehaviorSubject:Lrx/subjects/BehaviorSubject;

    iget-object v0, p0, Lcom/squareup/ui/items/EditModifierSetScopeRunner;->modifierSetEditState:Lcom/squareup/ui/items/ModifierSetEditState;

    invoke-virtual {p1, v0}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method
