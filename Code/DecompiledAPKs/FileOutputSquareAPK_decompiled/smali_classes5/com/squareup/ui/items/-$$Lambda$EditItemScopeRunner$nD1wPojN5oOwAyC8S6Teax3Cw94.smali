.class public final synthetic Lcom/squareup/ui/items/-$$Lambda$EditItemScopeRunner$nD1wPojN5oOwAyC8S6Teax3Cw94;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lcom/squareup/shared/catalog/CatalogTask;


# instance fields
.field private final synthetic f$0:Lcom/squareup/ui/items/EditItemScopeRunner;

.field private final synthetic f$1:Lcom/squareup/ui/items/EditItemState$ItemData;

.field private final synthetic f$2:Ljava/lang/String;

.field private final synthetic f$3:Lcom/squareup/ui/items/EditItemState$TaxStates;

.field private final synthetic f$4:Lcom/squareup/ui/items/EditItemState$ModifierStates;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/ui/items/EditItemScopeRunner;Lcom/squareup/ui/items/EditItemState$ItemData;Ljava/lang/String;Lcom/squareup/ui/items/EditItemState$TaxStates;Lcom/squareup/ui/items/EditItemState$ModifierStates;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/items/-$$Lambda$EditItemScopeRunner$nD1wPojN5oOwAyC8S6Teax3Cw94;->f$0:Lcom/squareup/ui/items/EditItemScopeRunner;

    iput-object p2, p0, Lcom/squareup/ui/items/-$$Lambda$EditItemScopeRunner$nD1wPojN5oOwAyC8S6Teax3Cw94;->f$1:Lcom/squareup/ui/items/EditItemState$ItemData;

    iput-object p3, p0, Lcom/squareup/ui/items/-$$Lambda$EditItemScopeRunner$nD1wPojN5oOwAyC8S6Teax3Cw94;->f$2:Ljava/lang/String;

    iput-object p4, p0, Lcom/squareup/ui/items/-$$Lambda$EditItemScopeRunner$nD1wPojN5oOwAyC8S6Teax3Cw94;->f$3:Lcom/squareup/ui/items/EditItemState$TaxStates;

    iput-object p5, p0, Lcom/squareup/ui/items/-$$Lambda$EditItemScopeRunner$nD1wPojN5oOwAyC8S6Teax3Cw94;->f$4:Lcom/squareup/ui/items/EditItemState$ModifierStates;

    return-void
.end method


# virtual methods
.method public final perform(Lcom/squareup/shared/catalog/Catalog$Local;)Ljava/lang/Object;
    .locals 6

    iget-object v0, p0, Lcom/squareup/ui/items/-$$Lambda$EditItemScopeRunner$nD1wPojN5oOwAyC8S6Teax3Cw94;->f$0:Lcom/squareup/ui/items/EditItemScopeRunner;

    iget-object v1, p0, Lcom/squareup/ui/items/-$$Lambda$EditItemScopeRunner$nD1wPojN5oOwAyC8S6Teax3Cw94;->f$1:Lcom/squareup/ui/items/EditItemState$ItemData;

    iget-object v2, p0, Lcom/squareup/ui/items/-$$Lambda$EditItemScopeRunner$nD1wPojN5oOwAyC8S6Teax3Cw94;->f$2:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/ui/items/-$$Lambda$EditItemScopeRunner$nD1wPojN5oOwAyC8S6Teax3Cw94;->f$3:Lcom/squareup/ui/items/EditItemState$TaxStates;

    iget-object v4, p0, Lcom/squareup/ui/items/-$$Lambda$EditItemScopeRunner$nD1wPojN5oOwAyC8S6Teax3Cw94;->f$4:Lcom/squareup/ui/items/EditItemState$ModifierStates;

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, Lcom/squareup/ui/items/EditItemScopeRunner;->lambda$createNewItem$1$EditItemScopeRunner(Lcom/squareup/ui/items/EditItemState$ItemData;Ljava/lang/String;Lcom/squareup/ui/items/EditItemState$TaxStates;Lcom/squareup/ui/items/EditItemState$ModifierStates;Lcom/squareup/shared/catalog/Catalog$Local;)Lcom/squareup/ui/items/EditItemState;

    move-result-object p1

    return-object p1
.end method
