.class public final Lcom/squareup/ui/items/RealItemsAppletGateway;
.super Ljava/lang/Object;
.source "RealItemsAppletGateway.kt"

# interfaces
.implements Lcom/squareup/ui/items/ItemsAppletGateway;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0008\u0010\u000b\u001a\u00020\u000cH\u0016J\u0008\u0010\r\u001a\u00020\u000cH\u0016R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/ui/items/RealItemsAppletGateway;",
        "Lcom/squareup/ui/items/ItemsAppletGateway;",
        "flow",
        "Lflow/Flow;",
        "itemsApplet",
        "Lcom/squareup/ui/items/ItemsApplet;",
        "(Lflow/Flow;Lcom/squareup/ui/items/ItemsApplet;)V",
        "getFlow",
        "()Lflow/Flow;",
        "getItemsApplet",
        "()Lcom/squareup/ui/items/ItemsApplet;",
        "activateItemsApplet",
        "",
        "goBackToItemsAppletWithHistoryPreserved",
        "items-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final flow:Lflow/Flow;

.field private final itemsApplet:Lcom/squareup/ui/items/ItemsApplet;


# direct methods
.method public constructor <init>(Lflow/Flow;Lcom/squareup/ui/items/ItemsApplet;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "flow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "itemsApplet"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/items/RealItemsAppletGateway;->flow:Lflow/Flow;

    iput-object p2, p0, Lcom/squareup/ui/items/RealItemsAppletGateway;->itemsApplet:Lcom/squareup/ui/items/ItemsApplet;

    return-void
.end method


# virtual methods
.method public activateItemsApplet()V
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/squareup/ui/items/RealItemsAppletGateway;->itemsApplet:Lcom/squareup/ui/items/ItemsApplet;

    invoke-virtual {v0}, Lcom/squareup/ui/items/ItemsApplet;->activate()V

    return-void
.end method

.method public final getFlow()Lflow/Flow;
    .locals 1

    .line 14
    iget-object v0, p0, Lcom/squareup/ui/items/RealItemsAppletGateway;->flow:Lflow/Flow;

    return-object v0
.end method

.method public final getItemsApplet()Lcom/squareup/ui/items/ItemsApplet;
    .locals 1

    .line 15
    iget-object v0, p0, Lcom/squareup/ui/items/RealItemsAppletGateway;->itemsApplet:Lcom/squareup/ui/items/ItemsApplet;

    return-object v0
.end method

.method public goBackToItemsAppletWithHistoryPreserved()V
    .locals 4

    .line 22
    iget-object v0, p0, Lcom/squareup/ui/items/RealItemsAppletGateway;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/container/CalculatedKey;

    sget-object v2, Lcom/squareup/ui/items/RealItemsAppletGateway$goBackToItemsAppletWithHistoryPreserved$1;->INSTANCE:Lcom/squareup/ui/items/RealItemsAppletGateway$goBackToItemsAppletWithHistoryPreserved$1;

    check-cast v2, Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;

    const-string v3, "goBackToItemsAppletAfterDoneEditing"

    invoke-direct {v1, v3, v2}, Lcom/squareup/container/CalculatedKey;-><init>(Ljava/lang/String;Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method
