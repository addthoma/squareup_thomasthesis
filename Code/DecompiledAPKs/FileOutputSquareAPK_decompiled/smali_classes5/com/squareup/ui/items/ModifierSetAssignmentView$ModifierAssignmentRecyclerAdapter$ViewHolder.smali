.class public Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter$ViewHolder;
.super Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
.source "ModifierSetAssignmentView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ViewHolder"
.end annotation


# instance fields
.field private row:Lcom/squareup/ui/items/CategoryAssignmentListRow;

.field final synthetic this$1:Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter;Landroid/view/View;)V
    .locals 0

    .line 112
    iput-object p1, p0, Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter$ViewHolder;->this$1:Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter;

    .line 113
    invoke-direct {p0, p2}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 114
    check-cast p2, Lcom/squareup/ui/items/CategoryAssignmentListRow;

    iput-object p2, p0, Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter$ViewHolder;->row:Lcom/squareup/ui/items/CategoryAssignmentListRow;

    return-void
.end method


# virtual methods
.method bindItem(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;I)V
    .locals 17

    move-object/from16 v0, p0

    move/from16 v1, p2

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-nez v1, :cond_0

    const/4 v4, 0x1

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    .line 119
    :goto_0
    iget-object v5, v0, Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter$ViewHolder;->this$1:Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter;

    iget-object v5, v5, Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter;->this$0:Lcom/squareup/ui/items/ModifierSetAssignmentView;

    iget-object v5, v5, Lcom/squareup/ui/items/ModifierSetAssignmentView;->presenter:Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getObjectId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;->itemAssignedToModifier(Ljava/lang/String;)Z

    move-result v15

    .line 120
    iget-object v5, v0, Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter$ViewHolder;->this$1:Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter;

    iget-object v5, v5, Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter;->this$0:Lcom/squareup/ui/items/ModifierSetAssignmentView;

    iget-object v5, v5, Lcom/squareup/ui/items/ModifierSetAssignmentView;->presenter:Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getCategoryId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/squareup/ui/items/ModifierSetAssignmentScreen$Presenter;->getCategoryNameForId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 121
    iget-object v5, v0, Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter$ViewHolder;->row:Lcom/squareup/ui/items/CategoryAssignmentListRow;

    invoke-virtual {v5, v4, v2}, Lcom/squareup/ui/items/CategoryAssignmentListRow;->setHorizontalBorders(ZZ)V

    .line 122
    iget-object v2, v0, Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter$ViewHolder;->row:Lcom/squareup/ui/items/CategoryAssignmentListRow;

    invoke-virtual {v2, v3}, Lcom/squareup/ui/items/CategoryAssignmentListRow;->setUseUnselectedColor(Z)V

    .line 124
    iget-object v7, v0, Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter$ViewHolder;->row:Lcom/squareup/ui/items/CategoryAssignmentListRow;

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getName()Ljava/lang/String;

    move-result-object v8

    iget-object v2, v0, Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter$ViewHolder;->this$1:Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter;

    iget-object v2, v2, Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter;->this$0:Lcom/squareup/ui/items/ModifierSetAssignmentView;

    iget-object v9, v2, Lcom/squareup/ui/items/ModifierSetAssignmentView;->itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getObjectId()Ljava/lang/String;

    move-result-object v10

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getImageUrl()Ljava/lang/String;

    move-result-object v11

    .line 125
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getAbbreviation()Ljava/lang/String;

    move-result-object v12

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getColor()Ljava/lang/String;

    move-result-object v13

    iget-object v2, v0, Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter$ViewHolder;->this$1:Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter;

    iget-object v2, v2, Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter;->this$0:Lcom/squareup/ui/items/ModifierSetAssignmentView;

    iget-object v2, v2, Lcom/squareup/ui/items/ModifierSetAssignmentView;->tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    .line 126
    invoke-virtual {v2}, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;->isTextTileMode()Z

    move-result v16

    .line 124
    invoke-virtual/range {v7 .. v16}, Lcom/squareup/ui/items/CategoryAssignmentListRow;->showItem(Ljava/lang/String;Lcom/squareup/ui/photo/ItemPhoto$Factory;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 127
    iget-object v2, v0, Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter$ViewHolder;->row:Lcom/squareup/ui/items/CategoryAssignmentListRow;

    new-instance v3, Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter$ViewHolder$1;

    move-object/from16 v4, p1

    invoke-direct {v3, v0, v4, v1}, Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter$ViewHolder$1;-><init>(Lcom/squareup/ui/items/ModifierSetAssignmentView$ModifierAssignmentRecyclerAdapter$ViewHolder;Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;I)V

    invoke-virtual {v2, v3}, Lcom/squareup/ui/items/CategoryAssignmentListRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
