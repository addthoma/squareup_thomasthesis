.class public final Lcom/squareup/ui/items/EditItemScope;
.super Lcom/squareup/ui/main/InMainActivityScope;
.source "EditItemScope.java"

# interfaces
.implements Lcom/squareup/container/RegistersInScope;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/items/EditItemScope$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/EditItemScope$Component;,
        Lcom/squareup/ui/items/EditItemScope$ParentComponent;,
        Lcom/squareup/ui/items/EditItemScope$VariationRunnerModule;,
        Lcom/squareup/ui/items/EditItemScope$Module;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/items/EditItemScope;",
            ">;"
        }
    .end annotation
.end field

.field static final NEW_ITEM:Ljava/lang/String;


# instance fields
.field final categoryId:Ljava/lang/String;

.field final categoryName:Ljava/lang/String;

.field final imageUrl:Ljava/lang/String;

.field final isFromItemSuggestion:Z

.field final itemAbbreviation:Ljava/lang/String;

.field final itemAmount:J

.field final itemId:Ljava/lang/String;

.field final itemName:Ljava/lang/String;

.field final sku:Ljava/lang/String;

.field final type:Lcom/squareup/api/items/Item$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 195
    sget-object v0, Lcom/squareup/ui/items/-$$Lambda$EditItemScope$B52xVhZNJckkIhjCSSu0xFCJyvg;->INSTANCE:Lcom/squareup/ui/items/-$$Lambda$EditItemScope$B52xVhZNJckkIhjCSSu0xFCJyvg;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/items/EditItemScope;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Lcom/squareup/api/items/Item$Type;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .line 62
    sget-object v2, Lcom/squareup/ui/items/EditItemScope;->NEW_ITEM:Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/items/EditItemScope;-><init>(Lcom/squareup/api/items/Item$Type;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method constructor <init>(Lcom/squareup/api/items/Item$Type;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 12

    const/4 v7, 0x0

    const/4 v8, 0x0

    const-string v9, ""

    const-wide/16 v10, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    .line 74
    invoke-direct/range {v0 .. v11}, Lcom/squareup/ui/items/EditItemScope;-><init>(Lcom/squareup/api/items/Item$Type;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;J)V

    return-void
.end method

.method constructor <init>(Lcom/squareup/api/items/Item$Type;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;J)V
    .locals 0

    .line 81
    invoke-direct {p0}, Lcom/squareup/ui/main/InMainActivityScope;-><init>()V

    .line 82
    iput-object p1, p0, Lcom/squareup/ui/items/EditItemScope;->type:Lcom/squareup/api/items/Item$Type;

    .line 83
    iput-object p2, p0, Lcom/squareup/ui/items/EditItemScope;->itemId:Ljava/lang/String;

    .line 84
    iput-object p3, p0, Lcom/squareup/ui/items/EditItemScope;->imageUrl:Ljava/lang/String;

    .line 85
    iput-object p4, p0, Lcom/squareup/ui/items/EditItemScope;->categoryId:Ljava/lang/String;

    .line 86
    iput-object p5, p0, Lcom/squareup/ui/items/EditItemScope;->categoryName:Ljava/lang/String;

    .line 87
    iput-object p6, p0, Lcom/squareup/ui/items/EditItemScope;->sku:Ljava/lang/String;

    .line 88
    iput-boolean p7, p0, Lcom/squareup/ui/items/EditItemScope;->isFromItemSuggestion:Z

    .line 89
    iput-object p8, p0, Lcom/squareup/ui/items/EditItemScope;->itemAbbreviation:Ljava/lang/String;

    .line 90
    iput-object p9, p0, Lcom/squareup/ui/items/EditItemScope;->itemName:Ljava/lang/String;

    .line 91
    iput-wide p10, p0, Lcom/squareup/ui/items/EditItemScope;->itemAmount:J

    return-void
.end method

.method constructor <init>(Ljava/lang/String;)V
    .locals 7

    .line 67
    sget-object v1, Lcom/squareup/api/items/Item$Type;->REGULAR:Lcom/squareup/api/items/Item$Type;

    sget-object v2, Lcom/squareup/ui/items/EditItemScope;->NEW_ITEM:Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/items/EditItemScope;-><init>(Lcom/squareup/api/items/Item$Type;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/items/EditItemScope;
    .locals 14

    .line 196
    invoke-static {}, Lcom/squareup/api/items/Item$Type;->values()[Lcom/squareup/api/items/Item$Type;

    move-result-object v0

    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v3, v0, v1

    .line 197
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 198
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    .line 199
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    .line 200
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v7

    .line 201
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v8

    .line 202
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const/4 v9, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    const/4 v9, 0x0

    .line 203
    :goto_0
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v10

    .line 204
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v11

    .line 205
    invoke-virtual {p0}, Landroid/os/Parcel;->readLong()J

    move-result-wide v12

    .line 206
    new-instance p0, Lcom/squareup/ui/items/EditItemScope;

    move-object v2, p0

    invoke-direct/range {v2 .. v13}, Lcom/squareup/ui/items/EditItemScope;-><init>(Lcom/squareup/api/items/Item$Type;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;J)V

    return-object p0
.end method


# virtual methods
.method public doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .line 95
    iget-object p2, p0, Lcom/squareup/ui/items/EditItemScope;->type:Lcom/squareup/api/items/Item$Type;

    invoke-virtual {p2}, Lcom/squareup/api/items/Item$Type;->ordinal()I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 96
    iget-object p2, p0, Lcom/squareup/ui/items/EditItemScope;->itemId:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 97
    iget-object p2, p0, Lcom/squareup/ui/items/EditItemScope;->imageUrl:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 98
    iget-object p2, p0, Lcom/squareup/ui/items/EditItemScope;->categoryId:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 99
    iget-object p2, p0, Lcom/squareup/ui/items/EditItemScope;->categoryName:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 100
    iget-object p2, p0, Lcom/squareup/ui/items/EditItemScope;->sku:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 101
    iget-boolean p2, p0, Lcom/squareup/ui/items/EditItemScope;->isFromItemSuggestion:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 102
    iget-object p2, p0, Lcom/squareup/ui/items/EditItemScope;->itemAbbreviation:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 103
    iget-object p2, p0, Lcom/squareup/ui/items/EditItemScope;->itemName:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 104
    iget-wide v0, p0, Lcom/squareup/ui/items/EditItemScope;->itemAmount:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    return-void
.end method

.method public register(Lmortar/MortarScope;)V
    .locals 1

    .line 108
    const-class v0, Lcom/squareup/ui/items/EditItemScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/EditItemScope$Component;

    .line 109
    invoke-interface {v0}, Lcom/squareup/ui/items/EditItemScope$Component;->scopeRunner()Lcom/squareup/ui/items/EditItemScopeRunner;

    move-result-object v0

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    return-void
.end method
