.class public Lcom/squareup/ui/items/ConfirmDiscardCategoryChangesDialogScreen;
.super Lcom/squareup/ui/items/InEditCategoryScope;
.source "ConfirmDiscardCategoryChangesDialogScreen.java"


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/ui/items/ConfirmDiscardCategoryChangesDialogScreen$Factory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/ConfirmDiscardCategoryChangesDialogScreen$Factory;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/items/ConfirmDiscardCategoryChangesDialogScreen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 35
    sget-object v0, Lcom/squareup/ui/items/-$$Lambda$ConfirmDiscardCategoryChangesDialogScreen$zDwkhfVSBCpeH9Lg8aDkyNrxMaI;->INSTANCE:Lcom/squareup/ui/items/-$$Lambda$ConfirmDiscardCategoryChangesDialogScreen$zDwkhfVSBCpeH9Lg8aDkyNrxMaI;

    .line 36
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/items/ConfirmDiscardCategoryChangesDialogScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 17
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/InEditCategoryScope;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/items/ConfirmDiscardCategoryChangesDialogScreen;
    .locals 1

    .line 37
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object p0

    .line 38
    new-instance v0, Lcom/squareup/ui/items/ConfirmDiscardCategoryChangesDialogScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/items/ConfirmDiscardCategoryChangesDialogScreen;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 32
    iget-object p2, p0, Lcom/squareup/ui/items/ConfirmDiscardCategoryChangesDialogScreen;->categoryId:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
