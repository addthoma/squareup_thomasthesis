.class public final Lcom/squareup/ui/items/RealItemsAppletGateway_Factory;
.super Ljava/lang/Object;
.source "RealItemsAppletGateway_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/items/RealItemsAppletGateway;",
        ">;"
    }
.end annotation


# instance fields
.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final itemsAppletProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/ItemsApplet;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/ItemsApplet;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/ui/items/RealItemsAppletGateway_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 24
    iput-object p2, p0, Lcom/squareup/ui/items/RealItemsAppletGateway_Factory;->itemsAppletProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/items/RealItemsAppletGateway_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/ItemsApplet;",
            ">;)",
            "Lcom/squareup/ui/items/RealItemsAppletGateway_Factory;"
        }
    .end annotation

    .line 34
    new-instance v0, Lcom/squareup/ui/items/RealItemsAppletGateway_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/items/RealItemsAppletGateway_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lflow/Flow;Lcom/squareup/ui/items/ItemsApplet;)Lcom/squareup/ui/items/RealItemsAppletGateway;
    .locals 1

    .line 38
    new-instance v0, Lcom/squareup/ui/items/RealItemsAppletGateway;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/items/RealItemsAppletGateway;-><init>(Lflow/Flow;Lcom/squareup/ui/items/ItemsApplet;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/items/RealItemsAppletGateway;
    .locals 2

    .line 29
    iget-object v0, p0, Lcom/squareup/ui/items/RealItemsAppletGateway_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflow/Flow;

    iget-object v1, p0, Lcom/squareup/ui/items/RealItemsAppletGateway_Factory;->itemsAppletProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/items/ItemsApplet;

    invoke-static {v0, v1}, Lcom/squareup/ui/items/RealItemsAppletGateway_Factory;->newInstance(Lflow/Flow;Lcom/squareup/ui/items/ItemsApplet;)Lcom/squareup/ui/items/RealItemsAppletGateway;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/ui/items/RealItemsAppletGateway_Factory;->get()Lcom/squareup/ui/items/RealItemsAppletGateway;

    move-result-object v0

    return-object v0
.end method
