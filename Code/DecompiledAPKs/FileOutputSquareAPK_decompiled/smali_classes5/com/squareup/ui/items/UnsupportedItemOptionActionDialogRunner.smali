.class public final Lcom/squareup/ui/items/UnsupportedItemOptionActionDialogRunner;
.super Ljava/lang/Object;
.source "UnsupportedItemOptionActionDialogRunner.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/ui/items/EditItemScope;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0005\u0008\u0007\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0006\u0010\u0005\u001a\u00020\u0006J\u0006\u0010\u0007\u001a\u00020\u0006J\u0006\u0010\u0008\u001a\u00020\u0006J\u0006\u0010\t\u001a\u00020\u0006J\u0006\u0010\n\u001a\u00020\u0006R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/ui/items/UnsupportedItemOptionActionDialogRunner;",
        "",
        "flow",
        "Lflow/Flow;",
        "(Lflow/Flow;)V",
        "showUnsupportedAddOptionWithMultipleFlatVariationsDialog",
        "",
        "showUnsupportedAddVariationWithOptionsAndLocallyDisabledVariationDialog",
        "showUnsupportedEditOptionWithLocallyDisabledVariationsDialog",
        "showUnsupportedVariationAddErrorDialog",
        "showUnsupportedVariationDeleteErrorDialog",
        "edit-item_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final flow:Lflow/Flow;


# direct methods
.method public constructor <init>(Lflow/Flow;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "flow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/items/UnsupportedItemOptionActionDialogRunner;->flow:Lflow/Flow;

    return-void
.end method


# virtual methods
.method public final showUnsupportedAddOptionWithMultipleFlatVariationsDialog()V
    .locals 3

    .line 18
    iget-object v0, p0, Lcom/squareup/ui/items/UnsupportedItemOptionActionDialogRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/items/option/UnsupportedAddItemOptionsWithMultiFlatVariationsDialogScreen;

    invoke-direct {v1}, Lcom/squareup/ui/items/option/UnsupportedAddItemOptionsWithMultiFlatVariationsDialogScreen;-><init>()V

    check-cast v1, Lcom/squareup/container/ContainerTreeKey;

    sget-object v2, Lflow/Direction;->FORWARD:Lflow/Direction;

    invoke-static {v0, v1, v2}, Lcom/squareup/container/Flows;->goToDialogScreen(Lflow/Flow;Lcom/squareup/container/ContainerTreeKey;Lflow/Direction;)V

    return-void
.end method

.method public final showUnsupportedAddVariationWithOptionsAndLocallyDisabledVariationDialog()V
    .locals 4

    .line 29
    iget-object v0, p0, Lcom/squareup/ui/items/UnsupportedItemOptionActionDialogRunner;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->getHistory()Lflow/History;

    move-result-object v0

    invoke-virtual {v0}, Lflow/History;->buildUpon()Lflow/History$Builder;

    move-result-object v0

    const-string v1, "history"

    .line 30
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const-class v2, Lcom/squareup/ui/items/EditItemMainScreen;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/squareup/container/Histories;->popUntil(Lflow/History$Builder;[Ljava/lang/Class;)Lflow/History$Builder;

    .line 31
    new-instance v1, Lcom/squareup/ui/items/option/UnsupportedAddVariationWithOptionsAndLocallyDisabledVariationsDialog;

    invoke-direct {v1}, Lcom/squareup/ui/items/option/UnsupportedAddVariationWithOptionsAndLocallyDisabledVariationsDialog;-><init>()V

    invoke-virtual {v0, v1}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    .line 32
    iget-object v1, p0, Lcom/squareup/ui/items/UnsupportedItemOptionActionDialogRunner;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object v0

    sget-object v2, Lflow/Direction;->FORWARD:Lflow/Direction;

    invoke-virtual {v1, v0, v2}, Lflow/Flow;->setHistory(Lflow/History;Lflow/Direction;)V

    return-void
.end method

.method public final showUnsupportedEditOptionWithLocallyDisabledVariationsDialog()V
    .locals 4

    .line 22
    iget-object v0, p0, Lcom/squareup/ui/items/UnsupportedItemOptionActionDialogRunner;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->getHistory()Lflow/History;

    move-result-object v0

    invoke-virtual {v0}, Lflow/History;->buildUpon()Lflow/History$Builder;

    move-result-object v0

    const-string v1, "history"

    .line 23
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const-class v2, Lcom/squareup/ui/items/EditItemMainScreen;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/squareup/container/Histories;->popUntil(Lflow/History$Builder;[Ljava/lang/Class;)Lflow/History$Builder;

    .line 24
    new-instance v1, Lcom/squareup/ui/items/option/UnsupportedEditItemOptionsWithDisabledVariationsDialogScreen;

    invoke-direct {v1}, Lcom/squareup/ui/items/option/UnsupportedEditItemOptionsWithDisabledVariationsDialogScreen;-><init>()V

    invoke-virtual {v0, v1}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    .line 25
    iget-object v1, p0, Lcom/squareup/ui/items/UnsupportedItemOptionActionDialogRunner;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object v0

    sget-object v2, Lflow/Direction;->FORWARD:Lflow/Direction;

    invoke-virtual {v1, v0, v2}, Lflow/Flow;->setHistory(Lflow/History;Lflow/Direction;)V

    return-void
.end method

.method public final showUnsupportedVariationAddErrorDialog()V
    .locals 3

    .line 36
    iget-object v0, p0, Lcom/squareup/ui/items/UnsupportedItemOptionActionDialogRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/items/UnsupportedVariationAddWithItemOptionsDialogScreen;

    invoke-direct {v1}, Lcom/squareup/ui/items/UnsupportedVariationAddWithItemOptionsDialogScreen;-><init>()V

    check-cast v1, Lcom/squareup/container/ContainerTreeKey;

    sget-object v2, Lflow/Direction;->FORWARD:Lflow/Direction;

    invoke-static {v0, v1, v2}, Lcom/squareup/container/Flows;->goToDialogScreen(Lflow/Flow;Lcom/squareup/container/ContainerTreeKey;Lflow/Direction;)V

    return-void
.end method

.method public final showUnsupportedVariationDeleteErrorDialog()V
    .locals 3

    .line 40
    iget-object v0, p0, Lcom/squareup/ui/items/UnsupportedItemOptionActionDialogRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/items/UnsupportedVariationDeleteWithItemOptionsDialogScreen;

    invoke-direct {v1}, Lcom/squareup/ui/items/UnsupportedVariationDeleteWithItemOptionsDialogScreen;-><init>()V

    check-cast v1, Lcom/squareup/container/ContainerTreeKey;

    sget-object v2, Lflow/Direction;->FORWARD:Lflow/Direction;

    invoke-static {v0, v1, v2}, Lcom/squareup/container/Flows;->goToDialogScreen(Lflow/Flow;Lcom/squareup/container/ContainerTreeKey;Lflow/Direction;)V

    return-void
.end method
