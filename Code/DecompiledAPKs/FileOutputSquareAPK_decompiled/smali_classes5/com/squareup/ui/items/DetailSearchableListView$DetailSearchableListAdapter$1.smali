.class Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "DetailSearchableListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;->buildAndBindButtonRow(Landroid/view/View;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;

.field final synthetic val$currentIndex:I


# direct methods
.method constructor <init>(Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;I)V
    .locals 0

    .line 296
    iput-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter$1;->this$1:Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;

    iput p2, p0, Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter$1;->val$currentIndex:I

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 2

    .line 298
    iget-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter$1;->this$1:Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;

    iget-object p1, p1, Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;->this$0:Lcom/squareup/ui/items/DetailSearchableListView;

    iget-object p1, p1, Lcom/squareup/ui/items/DetailSearchableListView;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    iget v0, p0, Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter$1;->val$currentIndex:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v1, "Items Section List Add Button Tapped"

    invoke-interface {p1, v1, v0}, Lcom/squareup/tutorialv2/TutorialCore;->post(Ljava/lang/String;Ljava/lang/Object;)V

    .line 299
    iget-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter$1;->this$1:Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;

    iget-object p1, p1, Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;->this$0:Lcom/squareup/ui/items/DetailSearchableListView;

    invoke-virtual {p1}, Lcom/squareup/ui/items/DetailSearchableListView;->getPresenter()Lcom/squareup/ui/items/DetailSearchableListPresenter;

    move-result-object p1

    iget v0, p0, Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter$1;->val$currentIndex:I

    invoke-virtual {p1, v0}, Lcom/squareup/ui/items/DetailSearchableListPresenter;->onButtonClicked(I)V

    return-void
.end method
