.class public final Lcom/squareup/ui/items/DiscountRow;
.super Lcom/squareup/ui/items/DetailSearchableListRow$DetailSearchableObjectRow$DetailSearchableCogsObjectRow;
.source "DetailSearchableListRow.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000h\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000f\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001BS\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u000c\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000f\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u0012\u0006\u0010\u0013\u001a\u00020\u0014\u00a2\u0006\u0002\u0010\u0015J\t\u0010\u001a\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u001b\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u001c\u001a\u00020\u0007H\u00c2\u0003J\t\u0010\u001d\u001a\u00020\tH\u00c2\u0003J\t\u0010\u001e\u001a\u00020\u000bH\u00c2\u0003J\t\u0010\u001f\u001a\u00020\rH\u00c2\u0003J\u000f\u0010 \u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000fH\u00c2\u0003J\t\u0010!\u001a\u00020\u0012H\u00c2\u0003J\t\u0010\"\u001a\u00020\u0014H\u00c2\u0003J\u0010\u0010#\u001a\u00020$2\u0006\u0010%\u001a\u00020&H\u0016Ji\u0010\'\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\t2\u0008\u0008\u0002\u0010\n\u001a\u00020\u000b2\u0008\u0008\u0002\u0010\u000c\u001a\u00020\r2\u000e\u0008\u0002\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000f2\u0008\u0008\u0002\u0010\u0011\u001a\u00020\u00122\u0008\u0008\u0002\u0010\u0013\u001a\u00020\u0014H\u00c6\u0001J\u0013\u0010(\u001a\u00020)2\u0008\u0010*\u001a\u0004\u0018\u00010+H\u00d6\u0003J\t\u0010,\u001a\u00020-H\u00d6\u0001J\t\u0010.\u001a\u00020\tH\u00d6\u0001R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0004\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0017R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u0019R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006/"
    }
    d2 = {
        "Lcom/squareup/ui/items/DiscountRow;",
        "Lcom/squareup/ui/items/DetailSearchableListRow$DetailSearchableObjectRow$DetailSearchableCogsObjectRow;",
        "screen",
        "Lcom/squareup/ui/items/DetailSearchableListScreen;",
        "res",
        "Lcom/squareup/util/Res;",
        "discount",
        "Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Discount;",
        "discountDescription",
        "",
        "itemPhotos",
        "Lcom/squareup/ui/photo/ItemPhoto$Factory;",
        "priceFormatter",
        "Lcom/squareup/quantity/PerUnitFormatter;",
        "percentageFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/util/Percentage;",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "tileAppearanceSettings",
        "Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;",
        "(Lcom/squareup/ui/items/DetailSearchableListScreen;Lcom/squareup/util/Res;Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Discount;Ljava/lang/String;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;)V",
        "getRes",
        "()Lcom/squareup/util/Res;",
        "getScreen",
        "()Lcom/squareup/ui/items/DetailSearchableListScreen;",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "component8",
        "component9",
        "configureRow",
        "",
        "row",
        "Lcom/squareup/librarylist/LibraryItemListNohoRow;",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "items-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final discount:Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Discount;

.field private final discountDescription:Ljava/lang/String;

.field private final itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

.field private final percentageFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;"
        }
    .end annotation
.end field

.field private final priceFormatter:Lcom/squareup/quantity/PerUnitFormatter;

.field private final res:Lcom/squareup/util/Res;

.field private final screen:Lcom/squareup/ui/items/DetailSearchableListScreen;

.field private final tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/items/DetailSearchableListScreen;Lcom/squareup/util/Res;Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Discount;Ljava/lang/String;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/items/DetailSearchableListScreen;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Discount;",
            "Ljava/lang/String;",
            "Lcom/squareup/ui/photo/ItemPhoto$Factory;",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;",
            ")V"
        }
    .end annotation

    const-string v0, "screen"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "discount"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "discountDescription"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "itemPhotos"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "priceFormatter"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "percentageFormatter"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currencyCode"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "tileAppearanceSettings"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 118
    move-object v0, p3

    check-cast v0, Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/ui/items/DetailSearchableListRow$DetailSearchableObjectRow$DetailSearchableCogsObjectRow;-><init>(Lcom/squareup/ui/items/DetailSearchableListScreen;Lcom/squareup/util/Res;Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;)V

    iput-object p1, p0, Lcom/squareup/ui/items/DiscountRow;->screen:Lcom/squareup/ui/items/DetailSearchableListScreen;

    iput-object p2, p0, Lcom/squareup/ui/items/DiscountRow;->res:Lcom/squareup/util/Res;

    iput-object p3, p0, Lcom/squareup/ui/items/DiscountRow;->discount:Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Discount;

    iput-object p4, p0, Lcom/squareup/ui/items/DiscountRow;->discountDescription:Ljava/lang/String;

    iput-object p5, p0, Lcom/squareup/ui/items/DiscountRow;->itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    iput-object p6, p0, Lcom/squareup/ui/items/DiscountRow;->priceFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    iput-object p7, p0, Lcom/squareup/ui/items/DiscountRow;->percentageFormatter:Lcom/squareup/text/Formatter;

    iput-object p8, p0, Lcom/squareup/ui/items/DiscountRow;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    iput-object p9, p0, Lcom/squareup/ui/items/DiscountRow;->tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    return-void
.end method

.method private final component3()Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Discount;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/items/DiscountRow;->discount:Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Discount;

    return-object v0
.end method

.method private final component4()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/items/DiscountRow;->discountDescription:Ljava/lang/String;

    return-object v0
.end method

.method private final component5()Lcom/squareup/ui/photo/ItemPhoto$Factory;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/items/DiscountRow;->itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    return-object v0
.end method

.method private final component6()Lcom/squareup/quantity/PerUnitFormatter;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/items/DiscountRow;->priceFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    return-object v0
.end method

.method private final component7()Lcom/squareup/text/Formatter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ui/items/DiscountRow;->percentageFormatter:Lcom/squareup/text/Formatter;

    return-object v0
.end method

.method private final component8()Lcom/squareup/protos/common/CurrencyCode;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/items/DiscountRow;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    return-object v0
.end method

.method private final component9()Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/items/DiscountRow;->tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    return-object v0
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/items/DiscountRow;Lcom/squareup/ui/items/DetailSearchableListScreen;Lcom/squareup/util/Res;Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Discount;Ljava/lang/String;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;ILjava/lang/Object;)Lcom/squareup/ui/items/DiscountRow;
    .locals 10

    move-object v0, p0

    move/from16 v1, p10

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/squareup/ui/items/DiscountRow;->getScreen()Lcom/squareup/ui/items/DetailSearchableListScreen;

    move-result-object v2

    goto :goto_0

    :cond_0
    move-object v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    invoke-virtual {p0}, Lcom/squareup/ui/items/DiscountRow;->getRes()Lcom/squareup/util/Res;

    move-result-object v3

    goto :goto_1

    :cond_1
    move-object v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget-object v4, v0, Lcom/squareup/ui/items/DiscountRow;->discount:Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Discount;

    goto :goto_2

    :cond_2
    move-object v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget-object v5, v0, Lcom/squareup/ui/items/DiscountRow;->discountDescription:Ljava/lang/String;

    goto :goto_3

    :cond_3
    move-object v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    iget-object v6, v0, Lcom/squareup/ui/items/DiscountRow;->itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    goto :goto_4

    :cond_4
    move-object v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget-object v7, v0, Lcom/squareup/ui/items/DiscountRow;->priceFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    goto :goto_5

    :cond_5
    move-object/from16 v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    iget-object v8, v0, Lcom/squareup/ui/items/DiscountRow;->percentageFormatter:Lcom/squareup/text/Formatter;

    goto :goto_6

    :cond_6
    move-object/from16 v8, p7

    :goto_6
    and-int/lit16 v9, v1, 0x80

    if-eqz v9, :cond_7

    iget-object v9, v0, Lcom/squareup/ui/items/DiscountRow;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    goto :goto_7

    :cond_7
    move-object/from16 v9, p8

    :goto_7
    and-int/lit16 v1, v1, 0x100

    if-eqz v1, :cond_8

    iget-object v1, v0, Lcom/squareup/ui/items/DiscountRow;->tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    goto :goto_8

    :cond_8
    move-object/from16 v1, p9

    :goto_8
    move-object p1, v2

    move-object p2, v3

    move-object p3, v4

    move-object p4, v5

    move-object p5, v6

    move-object/from16 p6, v7

    move-object/from16 p7, v8

    move-object/from16 p8, v9

    move-object/from16 p9, v1

    invoke-virtual/range {p0 .. p9}, Lcom/squareup/ui/items/DiscountRow;->copy(Lcom/squareup/ui/items/DetailSearchableListScreen;Lcom/squareup/util/Res;Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Discount;Ljava/lang/String;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;)Lcom/squareup/ui/items/DiscountRow;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()Lcom/squareup/ui/items/DetailSearchableListScreen;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/ui/items/DiscountRow;->getScreen()Lcom/squareup/ui/items/DetailSearchableListScreen;

    move-result-object v0

    return-object v0
.end method

.method public final component2()Lcom/squareup/util/Res;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/ui/items/DiscountRow;->getRes()Lcom/squareup/util/Res;

    move-result-object v0

    return-object v0
.end method

.method public configureRow(Lcom/squareup/librarylist/LibraryItemListNohoRow;)V
    .locals 10

    const-string v0, "row"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 120
    iget-object v0, p0, Lcom/squareup/ui/items/DiscountRow;->discount:Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Discount;

    invoke-virtual {v0}, Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Discount;->getDiscount()Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-result-object v2

    .line 123
    iget-object v3, p0, Lcom/squareup/ui/items/DiscountRow;->itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    .line 124
    iget-object v4, p0, Lcom/squareup/ui/items/DiscountRow;->priceFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    .line 125
    iget-object v5, p0, Lcom/squareup/ui/items/DiscountRow;->percentageFormatter:Lcom/squareup/text/Formatter;

    .line 126
    iget-object v6, p0, Lcom/squareup/ui/items/DiscountRow;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    .line 127
    iget-object v0, p0, Lcom/squareup/ui/items/DiscountRow;->tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;->isTextTileMode()Z

    move-result v7

    .line 128
    iget-object v8, p0, Lcom/squareup/ui/items/DiscountRow;->discountDescription:Ljava/lang/String;

    .line 129
    invoke-virtual {p0}, Lcom/squareup/ui/items/DiscountRow;->getRes()Lcom/squareup/util/Res;

    move-result-object v9

    move-object v1, p1

    .line 121
    invoke-virtual/range {v1 .. v9}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->bindDiscountItem(Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/CurrencyCode;ZLjava/lang/String;Lcom/squareup/util/Res;)V

    return-void
.end method

.method public final copy(Lcom/squareup/ui/items/DetailSearchableListScreen;Lcom/squareup/util/Res;Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Discount;Ljava/lang/String;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;)Lcom/squareup/ui/items/DiscountRow;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/items/DetailSearchableListScreen;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Discount;",
            "Ljava/lang/String;",
            "Lcom/squareup/ui/photo/ItemPhoto$Factory;",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/util/Percentage;",
            ">;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;",
            ")",
            "Lcom/squareup/ui/items/DiscountRow;"
        }
    .end annotation

    const-string v0, "screen"

    move-object v2, p1

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    move-object v3, p2

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "discount"

    move-object v4, p3

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "discountDescription"

    move-object v5, p4

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "itemPhotos"

    move-object/from16 v6, p5

    invoke-static {v6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "priceFormatter"

    move-object/from16 v7, p6

    invoke-static {v7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "percentageFormatter"

    move-object/from16 v8, p7

    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currencyCode"

    move-object/from16 v9, p8

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "tileAppearanceSettings"

    move-object/from16 v10, p9

    invoke-static {v10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/items/DiscountRow;

    move-object v1, v0

    invoke-direct/range {v1 .. v10}, Lcom/squareup/ui/items/DiscountRow;-><init>(Lcom/squareup/ui/items/DetailSearchableListScreen;Lcom/squareup/util/Res;Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Discount;Ljava/lang/String;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/items/DiscountRow;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/items/DiscountRow;

    invoke-virtual {p0}, Lcom/squareup/ui/items/DiscountRow;->getScreen()Lcom/squareup/ui/items/DetailSearchableListScreen;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/ui/items/DiscountRow;->getScreen()Lcom/squareup/ui/items/DetailSearchableListScreen;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/ui/items/DiscountRow;->getRes()Lcom/squareup/util/Res;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/ui/items/DiscountRow;->getRes()Lcom/squareup/util/Res;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/items/DiscountRow;->discount:Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Discount;

    iget-object v1, p1, Lcom/squareup/ui/items/DiscountRow;->discount:Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Discount;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/items/DiscountRow;->discountDescription:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/ui/items/DiscountRow;->discountDescription:Ljava/lang/String;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/items/DiscountRow;->itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    iget-object v1, p1, Lcom/squareup/ui/items/DiscountRow;->itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/items/DiscountRow;->priceFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    iget-object v1, p1, Lcom/squareup/ui/items/DiscountRow;->priceFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/items/DiscountRow;->percentageFormatter:Lcom/squareup/text/Formatter;

    iget-object v1, p1, Lcom/squareup/ui/items/DiscountRow;->percentageFormatter:Lcom/squareup/text/Formatter;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/items/DiscountRow;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    iget-object v1, p1, Lcom/squareup/ui/items/DiscountRow;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/items/DiscountRow;->tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    iget-object p1, p1, Lcom/squareup/ui/items/DiscountRow;->tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public getRes()Lcom/squareup/util/Res;
    .locals 1

    .line 110
    iget-object v0, p0, Lcom/squareup/ui/items/DiscountRow;->res:Lcom/squareup/util/Res;

    return-object v0
.end method

.method public getScreen()Lcom/squareup/ui/items/DetailSearchableListScreen;
    .locals 1

    .line 109
    iget-object v0, p0, Lcom/squareup/ui/items/DiscountRow;->screen:Lcom/squareup/ui/items/DetailSearchableListScreen;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/squareup/ui/items/DiscountRow;->getScreen()Lcom/squareup/ui/items/DetailSearchableListScreen;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/ui/items/DiscountRow;->getRes()Lcom/squareup/util/Res;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/items/DiscountRow;->discount:Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Discount;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/items/DiscountRow;->discountDescription:Ljava/lang/String;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/items/DiscountRow;->itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_4
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/items/DiscountRow;->priceFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_5

    :cond_5
    const/4 v2, 0x0

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/items/DiscountRow;->percentageFormatter:Lcom/squareup/text/Formatter;

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_6

    :cond_6
    const/4 v2, 0x0

    :goto_6
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/items/DiscountRow;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_7

    :cond_7
    const/4 v2, 0x0

    :goto_7
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/items/DiscountRow;->tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    if-eqz v2, :cond_8

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_8
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DiscountRow(screen="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/ui/items/DiscountRow;->getScreen()Lcom/squareup/ui/items/DetailSearchableListScreen;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", res="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/ui/items/DiscountRow;->getRes()Lcom/squareup/util/Res;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", discount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/items/DiscountRow;->discount:Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Discount;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", discountDescription="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/items/DiscountRow;->discountDescription:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", itemPhotos="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/items/DiscountRow;->itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", priceFormatter="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/items/DiscountRow;->priceFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", percentageFormatter="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/items/DiscountRow;->percentageFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", currencyCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/items/DiscountRow;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", tileAppearanceSettings="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/items/DiscountRow;->tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
