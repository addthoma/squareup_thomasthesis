.class final Lcom/squareup/ui/items/EditServiceVariationCoordinator$attach$3;
.super Ljava/lang/Object;
.source "EditServiceVariationCoordinator.kt"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/EditServiceVariationCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $view:Landroid/view/View;

.field final synthetic this$0:Lcom/squareup/ui/items/EditServiceVariationCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/items/EditServiceVariationCoordinator;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator$attach$3;->this$0:Lcom/squareup/ui/items/EditServiceVariationCoordinator;

    iput-object p2, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator$attach$3;->$view:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;)V
    .locals 3

    .line 108
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator$attach$3;->this$0:Lcom/squareup/ui/items/EditServiceVariationCoordinator;

    iget-object v1, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator$attach$3;->$view:Landroid/view/View;

    const-string v2, "it"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, v1, p1}, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->access$configureActionBar(Lcom/squareup/ui/items/EditServiceVariationCoordinator;Landroid/view/View;Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;)V

    .line 109
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator$attach$3;->this$0:Lcom/squareup/ui/items/EditServiceVariationCoordinator;

    invoke-static {v0, p1}, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->access$updateFields(Lcom/squareup/ui/items/EditServiceVariationCoordinator;Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;)V

    .line 110
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator$attach$3;->this$0:Lcom/squareup/ui/items/EditServiceVariationCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->access$getCatalogIntegrationController$p(Lcom/squareup/ui/items/EditServiceVariationCoordinator;)Lcom/squareup/catalogapi/CatalogIntegrationController;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/catalogapi/CatalogIntegrationController;->hasExtraServiceOptions()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;->getMeasurementUnit()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v0

    if-nez v0, :cond_0

    .line 111
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator$attach$3;->this$0:Lcom/squareup/ui/items/EditServiceVariationCoordinator;

    invoke-static {v0, p1}, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->access$updateAPoSFields(Lcom/squareup/ui/items/EditServiceVariationCoordinator;Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;)V

    :cond_0
    return-void
.end method

.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    .line 51
    check-cast p1, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/EditServiceVariationCoordinator$attach$3;->call(Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;)V

    return-void
.end method
