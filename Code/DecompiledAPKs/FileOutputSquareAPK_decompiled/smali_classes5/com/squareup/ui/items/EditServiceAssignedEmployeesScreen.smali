.class public final Lcom/squareup/ui/items/EditServiceAssignedEmployeesScreen;
.super Lcom/squareup/ui/items/InEditItemScope;
.source "EditServiceAssignedEmployeesScreen.kt"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/coordinators/CoordinatorProvider;


# annotations
.annotation runtime Lcom/squareup/container/layer/FullSheet;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/EditServiceAssignedEmployeesScreen$EditServiceAssignedEmployeesRunner;,
        Lcom/squareup/ui/items/EditServiceAssignedEmployeesScreen$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditServiceAssignedEmployeesScreen.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditServiceAssignedEmployeesScreen.kt\ncom/squareup/ui/items/EditServiceAssignedEmployeesScreen\n+ 2 Components.kt\ncom/squareup/dagger/Components\n*L\n1#1,50:1\n52#2:51\n*E\n*S KotlinDebug\n*F\n+ 1 EditServiceAssignedEmployeesScreen.kt\ncom/squareup/ui/items/EditServiceAssignedEmployeesScreen\n*L\n23#1:51\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008\u0007\u0018\u0000 \u00122\u00020\u00012\u00020\u00022\u00020\u0003:\u0002\u0012\u0013B\r\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0018\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0014J\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\u0008\u0010\u0011\u001a\u00020\u000cH\u0016\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/ui/items/EditServiceAssignedEmployeesScreen;",
        "Lcom/squareup/ui/items/InEditItemScope;",
        "Lcom/squareup/container/LayoutScreen;",
        "Lcom/squareup/coordinators/CoordinatorProvider;",
        "parent",
        "Lcom/squareup/ui/items/EditItemScope;",
        "(Lcom/squareup/ui/items/EditItemScope;)V",
        "doWriteToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "",
        "provideCoordinator",
        "Lcom/squareup/coordinators/Coordinator;",
        "view",
        "Landroid/view/View;",
        "screenLayout",
        "Companion",
        "EditServiceAssignedEmployeesRunner",
        "edit-item_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Lcom/squareup/container/ContainerTreeKey$PathCreator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/container/ContainerTreeKey$PathCreator<",
            "Lcom/squareup/ui/items/EditServiceAssignedEmployeesScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/ui/items/EditServiceAssignedEmployeesScreen$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/items/EditServiceAssignedEmployeesScreen$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/items/EditServiceAssignedEmployeesScreen$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/items/EditServiceAssignedEmployeesScreen;->Companion:Lcom/squareup/ui/items/EditServiceAssignedEmployeesScreen$Companion;

    .line 35
    sget-object v0, Lcom/squareup/ui/items/EditServiceAssignedEmployeesScreen$Companion$CREATOR$1;->INSTANCE:Lcom/squareup/ui/items/EditServiceAssignedEmployeesScreen$Companion$CREATOR$1;

    check-cast v0, Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    const-string v1, "fromParcel { parcel ->\n \u2026ssLoader)!!\n      )\n    }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/ui/items/EditServiceAssignedEmployeesScreen;->CREATOR:Lcom/squareup/container/ContainerTreeKey$PathCreator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/items/EditItemScope;)V
    .locals 1

    const-string v0, "parent"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/InEditItemScope;-><init>(Lcom/squareup/ui/items/EditItemScope;)V

    return-void
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceAssignedEmployeesScreen;->editItemPath:Lcom/squareup/ui/items/EditItemScope;

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    const-string/jumbo v0, "view.context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    const-class v0, Lcom/squareup/ui/items/EditItemScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    .line 23
    check-cast p1, Lcom/squareup/ui/items/EditItemScope$Component;

    .line 24
    new-instance v0, Lcom/squareup/ui/items/EditServiceAssignedEmployeesCoordinator;

    invoke-interface {p1}, Lcom/squareup/ui/items/EditItemScope$Component;->scopeRunner()Lcom/squareup/ui/items/EditItemScopeRunner;

    move-result-object p1

    const-string v1, "component.scopeRunner()"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/ui/items/EditServiceAssignedEmployeesScreen$EditServiceAssignedEmployeesRunner;

    invoke-direct {v0, p1}, Lcom/squareup/ui/items/EditServiceAssignedEmployeesCoordinator;-><init>(Lcom/squareup/ui/items/EditServiceAssignedEmployeesScreen$EditServiceAssignedEmployeesRunner;)V

    check-cast v0, Lcom/squareup/coordinators/Coordinator;

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 20
    sget v0, Lcom/squareup/edititem/R$layout;->edit_service_assigned_employees_view:I

    return v0
.end method
