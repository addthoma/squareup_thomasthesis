.class public final enum Lcom/squareup/ui/items/ExtraTimeActionName;
.super Ljava/lang/Enum;
.source "ExtraTimeAction.kt"

# interfaces
.implements Lcom/squareup/analytics/EventNamedAction;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/items/ExtraTimeActionName;",
        ">;",
        "Lcom/squareup/analytics/EventNamedAction;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0004\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u00012\u00020\u0002B\u000f\u0008\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0008\u0010\u0006\u001a\u00020\u0004H\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000j\u0002\u0008\u0007\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/ui/items/ExtraTimeActionName;",
        "",
        "Lcom/squareup/analytics/EventNamedAction;",
        "value",
        "",
        "(Ljava/lang/String;ILjava/lang/String;)V",
        "getName",
        "EXTRA_TIME_ACTION_NAME",
        "edit-item_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/items/ExtraTimeActionName;

.field public static final enum EXTRA_TIME_ACTION_NAME:Lcom/squareup/ui/items/ExtraTimeActionName;


# instance fields
.field private final value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/squareup/ui/items/ExtraTimeActionName;

    new-instance v1, Lcom/squareup/ui/items/ExtraTimeActionName;

    const/4 v2, 0x0

    const-string v3, "EXTRA_TIME_ACTION_NAME"

    const-string v4, "Service: Set After Appointment Time"

    .line 8
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/ui/items/ExtraTimeActionName;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/ui/items/ExtraTimeActionName;->EXTRA_TIME_ACTION_NAME:Lcom/squareup/ui/items/ExtraTimeActionName;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/ui/items/ExtraTimeActionName;->$VALUES:[Lcom/squareup/ui/items/ExtraTimeActionName;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 7
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/squareup/ui/items/ExtraTimeActionName;->value:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/items/ExtraTimeActionName;
    .locals 1

    const-class v0, Lcom/squareup/ui/items/ExtraTimeActionName;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/items/ExtraTimeActionName;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/items/ExtraTimeActionName;
    .locals 1

    sget-object v0, Lcom/squareup/ui/items/ExtraTimeActionName;->$VALUES:[Lcom/squareup/ui/items/ExtraTimeActionName;

    invoke-virtual {v0}, [Lcom/squareup/ui/items/ExtraTimeActionName;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/items/ExtraTimeActionName;

    return-object v0
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .line 10
    iget-object v0, p0, Lcom/squareup/ui/items/ExtraTimeActionName;->value:Ljava/lang/String;

    return-object v0
.end method
