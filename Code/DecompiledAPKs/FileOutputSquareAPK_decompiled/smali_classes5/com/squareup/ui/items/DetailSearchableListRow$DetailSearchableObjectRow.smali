.class public abstract Lcom/squareup/ui/items/DetailSearchableListRow$DetailSearchableObjectRow;
.super Lcom/squareup/ui/items/DetailSearchableListRow;
.source "DetailSearchableListRow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/DetailSearchableListRow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "DetailSearchableObjectRow"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/DetailSearchableListRow$DetailSearchableObjectRow$DetailSearchableConnectV2ObjectRow;,
        Lcom/squareup/ui/items/DetailSearchableListRow$DetailSearchableObjectRow$DetailSearchableCogsObjectRow;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0002\u000f\u0010B\u001f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008R\u0014\u0010\u0004\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0014\u0010\u0006\u001a\u00020\u0007X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000e\u0082\u0001\u0002\u0011\u0012\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/ui/items/DetailSearchableListRow$DetailSearchableObjectRow;",
        "Lcom/squareup/ui/items/DetailSearchableListRow;",
        "screen",
        "Lcom/squareup/ui/items/DetailSearchableListScreen;",
        "res",
        "Lcom/squareup/util/Res;",
        "searchableObject",
        "Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;",
        "(Lcom/squareup/ui/items/DetailSearchableListScreen;Lcom/squareup/util/Res;Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;)V",
        "getRes",
        "()Lcom/squareup/util/Res;",
        "getScreen",
        "()Lcom/squareup/ui/items/DetailSearchableListScreen;",
        "getSearchableObject",
        "()Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;",
        "DetailSearchableCogsObjectRow",
        "DetailSearchableConnectV2ObjectRow",
        "Lcom/squareup/ui/items/DetailSearchableListRow$DetailSearchableObjectRow$DetailSearchableConnectV2ObjectRow;",
        "Lcom/squareup/ui/items/DetailSearchableListRow$DetailSearchableObjectRow$DetailSearchableCogsObjectRow;",
        "items-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final res:Lcom/squareup/util/Res;

.field private final screen:Lcom/squareup/ui/items/DetailSearchableListScreen;

.field private final searchableObject:Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;


# direct methods
.method private constructor <init>(Lcom/squareup/ui/items/DetailSearchableListScreen;Lcom/squareup/util/Res;Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;)V
    .locals 2

    .line 57
    invoke-virtual {p3}, Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;->getId()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/squareup/ui/items/DetailSearchableListRow;-><init>(Lcom/squareup/ui/items/DetailSearchableListScreen;Lcom/squareup/util/Res;Ljava/lang/String;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListRow$DetailSearchableObjectRow;->screen:Lcom/squareup/ui/items/DetailSearchableListScreen;

    iput-object p2, p0, Lcom/squareup/ui/items/DetailSearchableListRow$DetailSearchableObjectRow;->res:Lcom/squareup/util/Res;

    iput-object p3, p0, Lcom/squareup/ui/items/DetailSearchableListRow$DetailSearchableObjectRow;->searchableObject:Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/ui/items/DetailSearchableListScreen;Lcom/squareup/util/Res;Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 53
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/ui/items/DetailSearchableListRow$DetailSearchableObjectRow;-><init>(Lcom/squareup/ui/items/DetailSearchableListScreen;Lcom/squareup/util/Res;Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;)V

    return-void
.end method


# virtual methods
.method public getRes()Lcom/squareup/util/Res;
    .locals 1

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListRow$DetailSearchableObjectRow;->res:Lcom/squareup/util/Res;

    return-object v0
.end method

.method public getScreen()Lcom/squareup/ui/items/DetailSearchableListScreen;
    .locals 1

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListRow$DetailSearchableObjectRow;->screen:Lcom/squareup/ui/items/DetailSearchableListScreen;

    return-object v0
.end method

.method public getSearchableObject()Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;
    .locals 1

    .line 56
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListRow$DetailSearchableObjectRow;->searchableObject:Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;

    return-object v0
.end method
