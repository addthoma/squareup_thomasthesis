.class public Lcom/squareup/ui/items/widgets/EditItemCategorySelectionListRow;
.super Lcom/squareup/marketfont/MarketCheckedTextView;
.source "EditItemCategorySelectionListRow.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 17
    invoke-direct {p0, p1, p2}, Lcom/squareup/marketfont/MarketCheckedTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public static inflate(Landroid/view/ViewGroup;)Lcom/squareup/ui/items/widgets/EditItemCategorySelectionListRow;
    .locals 1

    .line 13
    sget v0, Lcom/squareup/edititem/R$layout;->edit_item_category_selection_list_row:I

    invoke-static {v0, p0}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/items/widgets/EditItemCategorySelectionListRow;

    return-object p0
.end method


# virtual methods
.method public setContent(Ljava/lang/String;Z)V
    .locals 0

    .line 21
    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/widgets/EditItemCategorySelectionListRow;->setText(Ljava/lang/CharSequence;)V

    .line 22
    invoke-virtual {p0, p2}, Lcom/squareup/ui/items/widgets/EditItemCategorySelectionListRow;->setChecked(Z)V

    return-void
.end method
