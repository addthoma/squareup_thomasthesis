.class public final Lcom/squareup/ui/items/ItemRow;
.super Lcom/squareup/ui/items/DetailSearchableListRow$DetailSearchableObjectRow$DetailSearchableCogsObjectRow;
.source "DetailSearchableListRow.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\\\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\r\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B=\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u00a2\u0006\u0002\u0010\u0010J\t\u0010\u0015\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0017\u001a\u00020\u0007H\u00c2\u0003J\t\u0010\u0018\u001a\u00020\tH\u00c2\u0003J\t\u0010\u0019\u001a\u00020\u000bH\u00c2\u0003J\t\u0010\u001a\u001a\u00020\rH\u00c2\u0003J\t\u0010\u001b\u001a\u00020\u000fH\u00c2\u0003J\u0010\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u001fH\u0016JO\u0010 \u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\t2\u0008\u0008\u0002\u0010\n\u001a\u00020\u000b2\u0008\u0008\u0002\u0010\u000c\u001a\u00020\r2\u0008\u0008\u0002\u0010\u000e\u001a\u00020\u000fH\u00c6\u0001J\u0013\u0010!\u001a\u00020\"2\u0008\u0010#\u001a\u0004\u0018\u00010$H\u00d6\u0003J\t\u0010%\u001a\u00020&H\u00d6\u0001J\t\u0010\'\u001a\u00020(H\u00d6\u0001R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0004\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u0014R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006)"
    }
    d2 = {
        "Lcom/squareup/ui/items/ItemRow;",
        "Lcom/squareup/ui/items/DetailSearchableListRow$DetailSearchableObjectRow$DetailSearchableCogsObjectRow;",
        "screen",
        "Lcom/squareup/ui/items/DetailSearchableListScreen;",
        "res",
        "Lcom/squareup/util/Res;",
        "item",
        "Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Item;",
        "priceFormatter",
        "Lcom/squareup/quantity/PerUnitFormatter;",
        "itemPhotos",
        "Lcom/squareup/ui/photo/ItemPhoto$Factory;",
        "tileAppearanceSettings",
        "Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "(Lcom/squareup/ui/items/DetailSearchableListScreen;Lcom/squareup/util/Res;Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Item;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;Lcom/squareup/protos/common/CurrencyCode;)V",
        "getRes",
        "()Lcom/squareup/util/Res;",
        "getScreen",
        "()Lcom/squareup/ui/items/DetailSearchableListScreen;",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "configureRow",
        "",
        "row",
        "Lcom/squareup/librarylist/LibraryItemListNohoRow;",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "items-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final item:Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Item;

.field private final itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

.field private final priceFormatter:Lcom/squareup/quantity/PerUnitFormatter;

.field private final res:Lcom/squareup/util/Res;

.field private final screen:Lcom/squareup/ui/items/DetailSearchableListScreen;

.field private final tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/items/DetailSearchableListScreen;Lcom/squareup/util/Res;Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Item;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;Lcom/squareup/protos/common/CurrencyCode;)V
    .locals 1

    const-string v0, "screen"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "item"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "priceFormatter"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "itemPhotos"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "tileAppearanceSettings"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currencyCode"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 142
    move-object v0, p3

    check-cast v0, Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/ui/items/DetailSearchableListRow$DetailSearchableObjectRow$DetailSearchableCogsObjectRow;-><init>(Lcom/squareup/ui/items/DetailSearchableListScreen;Lcom/squareup/util/Res;Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;)V

    iput-object p1, p0, Lcom/squareup/ui/items/ItemRow;->screen:Lcom/squareup/ui/items/DetailSearchableListScreen;

    iput-object p2, p0, Lcom/squareup/ui/items/ItemRow;->res:Lcom/squareup/util/Res;

    iput-object p3, p0, Lcom/squareup/ui/items/ItemRow;->item:Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Item;

    iput-object p4, p0, Lcom/squareup/ui/items/ItemRow;->priceFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    iput-object p5, p0, Lcom/squareup/ui/items/ItemRow;->itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    iput-object p6, p0, Lcom/squareup/ui/items/ItemRow;->tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    iput-object p7, p0, Lcom/squareup/ui/items/ItemRow;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    return-void
.end method

.method private final component3()Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Item;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/items/ItemRow;->item:Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Item;

    return-object v0
.end method

.method private final component4()Lcom/squareup/quantity/PerUnitFormatter;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/items/ItemRow;->priceFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    return-object v0
.end method

.method private final component5()Lcom/squareup/ui/photo/ItemPhoto$Factory;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/items/ItemRow;->itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    return-object v0
.end method

.method private final component6()Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/items/ItemRow;->tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    return-object v0
.end method

.method private final component7()Lcom/squareup/protos/common/CurrencyCode;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/items/ItemRow;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    return-object v0
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/items/ItemRow;Lcom/squareup/ui/items/DetailSearchableListScreen;Lcom/squareup/util/Res;Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Item;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;Lcom/squareup/protos/common/CurrencyCode;ILjava/lang/Object;)Lcom/squareup/ui/items/ItemRow;
    .locals 5

    and-int/lit8 p9, p8, 0x1

    if-eqz p9, :cond_0

    invoke-virtual {p0}, Lcom/squareup/ui/items/ItemRow;->getScreen()Lcom/squareup/ui/items/DetailSearchableListScreen;

    move-result-object p1

    :cond_0
    and-int/lit8 p9, p8, 0x2

    if-eqz p9, :cond_1

    invoke-virtual {p0}, Lcom/squareup/ui/items/ItemRow;->getRes()Lcom/squareup/util/Res;

    move-result-object p2

    :cond_1
    move-object p9, p2

    and-int/lit8 p2, p8, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/squareup/ui/items/ItemRow;->item:Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Item;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p8, 0x8

    if-eqz p2, :cond_3

    iget-object p4, p0, Lcom/squareup/ui/items/ItemRow;->priceFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    :cond_3
    move-object v1, p4

    and-int/lit8 p2, p8, 0x10

    if-eqz p2, :cond_4

    iget-object p5, p0, Lcom/squareup/ui/items/ItemRow;->itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    :cond_4
    move-object v2, p5

    and-int/lit8 p2, p8, 0x20

    if-eqz p2, :cond_5

    iget-object p6, p0, Lcom/squareup/ui/items/ItemRow;->tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    :cond_5
    move-object v3, p6

    and-int/lit8 p2, p8, 0x40

    if-eqz p2, :cond_6

    iget-object p7, p0, Lcom/squareup/ui/items/ItemRow;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    :cond_6
    move-object v4, p7

    move-object p2, p0

    move-object p3, p1

    move-object p4, p9

    move-object p5, v0

    move-object p6, v1

    move-object p7, v2

    move-object p8, v3

    move-object p9, v4

    invoke-virtual/range {p2 .. p9}, Lcom/squareup/ui/items/ItemRow;->copy(Lcom/squareup/ui/items/DetailSearchableListScreen;Lcom/squareup/util/Res;Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Item;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/ui/items/ItemRow;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/ui/items/DetailSearchableListScreen;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/ui/items/ItemRow;->getScreen()Lcom/squareup/ui/items/DetailSearchableListScreen;

    move-result-object v0

    return-object v0
.end method

.method public final component2()Lcom/squareup/util/Res;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/ui/items/ItemRow;->getRes()Lcom/squareup/util/Res;

    move-result-object v0

    return-object v0
.end method

.method public configureRow(Lcom/squareup/librarylist/LibraryItemListNohoRow;)V
    .locals 13

    const-string v0, "row"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 144
    iget-object v0, p0, Lcom/squareup/ui/items/ItemRow;->item:Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Item;

    invoke-virtual {v0}, Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Item;->getItem()Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-result-object v2

    .line 147
    iget-object v3, p0, Lcom/squareup/ui/items/ItemRow;->itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    .line 148
    iget-object v4, p0, Lcom/squareup/ui/items/ItemRow;->priceFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    .line 149
    iget-object v7, p0, Lcom/squareup/ui/items/ItemRow;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    .line 150
    iget-object v0, p0, Lcom/squareup/ui/items/ItemRow;->tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;->isTextTileMode()Z

    move-result v9

    .line 151
    invoke-virtual {p0}, Lcom/squareup/ui/items/ItemRow;->getRes()Lcom/squareup/util/Res;

    move-result-object v10

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/16 v11, 0x58

    const/4 v12, 0x0

    move-object v1, p1

    .line 145
    invoke-static/range {v1 .. v12}, Lcom/squareup/librarylist/LibraryItemListNohoRow;->bindCatalogItem$default(Lcom/squareup/librarylist/LibraryItemListNohoRow;Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/DurationFormatter;Lcom/squareup/protos/common/CurrencyCode;Ljava/lang/Boolean;ZLcom/squareup/util/Res;ILjava/lang/Object;)V

    return-void
.end method

.method public final copy(Lcom/squareup/ui/items/DetailSearchableListScreen;Lcom/squareup/util/Res;Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Item;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;Lcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/ui/items/ItemRow;
    .locals 9

    const-string v0, "screen"

    move-object v2, p1

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    move-object v3, p2

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "item"

    move-object v4, p3

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "priceFormatter"

    move-object v5, p4

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "itemPhotos"

    move-object v6, p5

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "tileAppearanceSettings"

    move-object v7, p6

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currencyCode"

    move-object/from16 v8, p7

    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/items/ItemRow;

    move-object v1, v0

    invoke-direct/range {v1 .. v8}, Lcom/squareup/ui/items/ItemRow;-><init>(Lcom/squareup/ui/items/DetailSearchableListScreen;Lcom/squareup/util/Res;Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Item;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/ui/photo/ItemPhoto$Factory;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;Lcom/squareup/protos/common/CurrencyCode;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/items/ItemRow;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/items/ItemRow;

    invoke-virtual {p0}, Lcom/squareup/ui/items/ItemRow;->getScreen()Lcom/squareup/ui/items/DetailSearchableListScreen;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/ui/items/ItemRow;->getScreen()Lcom/squareup/ui/items/DetailSearchableListScreen;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/ui/items/ItemRow;->getRes()Lcom/squareup/util/Res;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/ui/items/ItemRow;->getRes()Lcom/squareup/util/Res;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/items/ItemRow;->item:Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Item;

    iget-object v1, p1, Lcom/squareup/ui/items/ItemRow;->item:Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Item;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/items/ItemRow;->priceFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    iget-object v1, p1, Lcom/squareup/ui/items/ItemRow;->priceFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/items/ItemRow;->itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    iget-object v1, p1, Lcom/squareup/ui/items/ItemRow;->itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/items/ItemRow;->tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    iget-object v1, p1, Lcom/squareup/ui/items/ItemRow;->tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/items/ItemRow;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    iget-object p1, p1, Lcom/squareup/ui/items/ItemRow;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public getRes()Lcom/squareup/util/Res;
    .locals 1

    .line 136
    iget-object v0, p0, Lcom/squareup/ui/items/ItemRow;->res:Lcom/squareup/util/Res;

    return-object v0
.end method

.method public getScreen()Lcom/squareup/ui/items/DetailSearchableListScreen;
    .locals 1

    .line 135
    iget-object v0, p0, Lcom/squareup/ui/items/ItemRow;->screen:Lcom/squareup/ui/items/DetailSearchableListScreen;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/squareup/ui/items/ItemRow;->getScreen()Lcom/squareup/ui/items/DetailSearchableListScreen;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/ui/items/ItemRow;->getRes()Lcom/squareup/util/Res;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/items/ItemRow;->item:Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Item;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/items/ItemRow;->priceFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_3

    :cond_3
    const/4 v2, 0x0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/items/ItemRow;->itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_4

    :cond_4
    const/4 v2, 0x0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/items/ItemRow;->tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_5

    :cond_5
    const/4 v2, 0x0

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/items/ItemRow;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_6
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ItemRow(screen="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/ui/items/ItemRow;->getScreen()Lcom/squareup/ui/items/DetailSearchableListScreen;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", res="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/ui/items/ItemRow;->getRes()Lcom/squareup/util/Res;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", item="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/items/ItemRow;->item:Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Item;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", priceFormatter="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/items/ItemRow;->priceFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", itemPhotos="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/items/ItemRow;->itemPhotos:Lcom/squareup/ui/photo/ItemPhoto$Factory;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", tileAppearanceSettings="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/items/ItemRow;->tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", currencyCode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/items/ItemRow;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
