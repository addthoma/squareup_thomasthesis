.class public final synthetic Lcom/squareup/ui/items/-$$Lambda$EditItemScopeRunner$Ahy6LEH014UJ0LZrAakNLlY4ToY;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lcom/squareup/ui/items/StockCountRowAction$RowClickedCallback;


# instance fields
.field private final synthetic f$0:Lcom/squareup/ui/items/EditItemScopeRunner;

.field private final synthetic f$1:Ljava/lang/String;

.field private final synthetic f$2:Ljava/lang/String;

.field private final synthetic f$3:I

.field private final synthetic f$4:Ljava/lang/String;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/ui/items/EditItemScopeRunner;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/items/-$$Lambda$EditItemScopeRunner$Ahy6LEH014UJ0LZrAakNLlY4ToY;->f$0:Lcom/squareup/ui/items/EditItemScopeRunner;

    iput-object p2, p0, Lcom/squareup/ui/items/-$$Lambda$EditItemScopeRunner$Ahy6LEH014UJ0LZrAakNLlY4ToY;->f$1:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/ui/items/-$$Lambda$EditItemScopeRunner$Ahy6LEH014UJ0LZrAakNLlY4ToY;->f$2:Ljava/lang/String;

    iput p4, p0, Lcom/squareup/ui/items/-$$Lambda$EditItemScopeRunner$Ahy6LEH014UJ0LZrAakNLlY4ToY;->f$3:I

    iput-object p5, p0, Lcom/squareup/ui/items/-$$Lambda$EditItemScopeRunner$Ahy6LEH014UJ0LZrAakNLlY4ToY;->f$4:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final viewStockCountFieldClicked(ZLjava/math/BigDecimal;Lcom/squareup/protos/common/Money;)V
    .locals 8

    iget-object v0, p0, Lcom/squareup/ui/items/-$$Lambda$EditItemScopeRunner$Ahy6LEH014UJ0LZrAakNLlY4ToY;->f$0:Lcom/squareup/ui/items/EditItemScopeRunner;

    iget-object v1, p0, Lcom/squareup/ui/items/-$$Lambda$EditItemScopeRunner$Ahy6LEH014UJ0LZrAakNLlY4ToY;->f$1:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/ui/items/-$$Lambda$EditItemScopeRunner$Ahy6LEH014UJ0LZrAakNLlY4ToY;->f$2:Ljava/lang/String;

    iget v3, p0, Lcom/squareup/ui/items/-$$Lambda$EditItemScopeRunner$Ahy6LEH014UJ0LZrAakNLlY4ToY;->f$3:I

    iget-object v4, p0, Lcom/squareup/ui/items/-$$Lambda$EditItemScopeRunner$Ahy6LEH014UJ0LZrAakNLlY4ToY;->f$4:Ljava/lang/String;

    move v5, p1

    move-object v6, p2

    move-object v7, p3

    invoke-virtual/range {v0 .. v7}, Lcom/squareup/ui/items/EditItemScopeRunner;->lambda$createStockCountRowAction$3$EditItemScopeRunner(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;ZLjava/math/BigDecimal;Lcom/squareup/protos/common/Money;)V

    return-void
.end method
