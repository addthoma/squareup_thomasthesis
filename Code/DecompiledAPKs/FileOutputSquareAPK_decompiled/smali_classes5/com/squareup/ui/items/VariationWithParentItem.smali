.class Lcom/squareup/ui/items/VariationWithParentItem;
.super Ljava/lang/Object;
.source "VariationWithParentItem.java"

# interfaces
.implements Lcom/squareup/ui/items/EditDiscountHydratedProduct;


# instance fields
.field private parentItem:Lcom/squareup/shared/catalog/models/CatalogItem;

.field private variation:Lcom/squareup/shared/catalog/models/CatalogItemVariation;


# direct methods
.method constructor <init>(Lcom/squareup/shared/catalog/models/CatalogItemVariation;Lcom/squareup/shared/catalog/models/CatalogItem;)V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/squareup/ui/items/VariationWithParentItem;->variation:Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    .line 17
    iput-object p2, p0, Lcom/squareup/ui/items/VariationWithParentItem;->parentItem:Lcom/squareup/shared/catalog/models/CatalogItem;

    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 2

    .line 22
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/squareup/ui/items/VariationWithParentItem;->parentItem:Lcom/squareup/shared/catalog/models/CatalogItem;

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogItem;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/items/VariationWithParentItem;->variation:Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getParentItemColor()Ljava/lang/String;
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/squareup/ui/items/VariationWithParentItem;->parentItem:Lcom/squareup/shared/catalog/models/CatalogItem;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItem;->getColor()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPrice()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/ui/items/VariationWithParentItem;->variation:Lcom/squareup/shared/catalog/models/CatalogItemVariation;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation;->getPrice()Lcom/squareup/protos/common/Money;

    move-result-object v0

    return-object v0
.end method
