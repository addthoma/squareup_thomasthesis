.class abstract Lcom/squareup/ui/items/EditItemMainScreen$AssignUnitToVariationWorkflowModule;
.super Ljava/lang/Object;
.source "EditItemMainScreen.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/EditItemMainScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x400
    name = "AssignUnitToVariationWorkflowModule"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/items/EditItemMainScreen;


# direct methods
.method constructor <init>(Lcom/squareup/ui/items/EditItemMainScreen;)V
    .locals 0

    .line 104
    iput-object p1, p0, Lcom/squareup/ui/items/EditItemMainScreen$AssignUnitToVariationWorkflowModule;->this$0:Lcom/squareup/ui/items/EditItemMainScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract provideAssignUnitToVariationWorkflowResultHandler(Lcom/squareup/ui/items/EditItemMainPresenter;)Lcom/squareup/ui/items/unit/AssignUnitToVariationWorkflowRunner$AssignUnitToVariationWorkflowResultHandler;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
