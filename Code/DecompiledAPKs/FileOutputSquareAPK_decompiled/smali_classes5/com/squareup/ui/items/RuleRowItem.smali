.class public Lcom/squareup/ui/items/RuleRowItem;
.super Ljava/lang/Object;
.source "RuleRowItem.java"


# static fields
.field public static final NO_COLOR_STRIP:I = -0x1


# instance fields
.field final colorStripColor:I

.field final key:Ljava/lang/String;

.field final value:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput p1, p0, Lcom/squareup/ui/items/RuleRowItem;->colorStripColor:I

    .line 25
    iput-object p2, p0, Lcom/squareup/ui/items/RuleRowItem;->key:Ljava/lang/String;

    .line 26
    iput-object p3, p0, Lcom/squareup/ui/items/RuleRowItem;->value:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/squareup/ui/items/RuleRowItem;->key:Ljava/lang/String;

    .line 19
    iput-object p2, p0, Lcom/squareup/ui/items/RuleRowItem;->value:Ljava/lang/String;

    const/4 p1, -0x1

    .line 20
    iput p1, p0, Lcom/squareup/ui/items/RuleRowItem;->colorStripColor:I

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_3

    .line 31
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_1

    .line 32
    :cond_1
    check-cast p1, Lcom/squareup/ui/items/RuleRowItem;

    .line 33
    iget v2, p0, Lcom/squareup/ui/items/RuleRowItem;->colorStripColor:I

    iget v3, p1, Lcom/squareup/ui/items/RuleRowItem;->colorStripColor:I

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lcom/squareup/ui/items/RuleRowItem;->key:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/ui/items/RuleRowItem;->key:Ljava/lang/String;

    .line 34
    invoke-static {v2, v3}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/ui/items/RuleRowItem;->value:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/ui/items/RuleRowItem;->value:Ljava/lang/String;

    .line 35
    invoke-static {v2, p1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_3
    :goto_1
    return v1
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    .line 39
    iget v1, p0, Lcom/squareup/ui/items/RuleRowItem;->colorStripColor:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/ui/items/RuleRowItem;->key:Ljava/lang/String;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/ui/items/RuleRowItem;->value:Ljava/lang/String;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Objects;->hash([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
