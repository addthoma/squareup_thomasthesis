.class public final Lcom/squareup/ui/items/EditItemScopeRunner_Factory;
.super Ljava/lang/Object;
.source "EditItemScopeRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/items/EditItemScopeRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final adAnalyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/adanalytics/AdAnalytics;",
            ">;"
        }
    .end annotation
.end field

.field private final adjustInventoryStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/inventory/AdjustInventoryStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final appliedLocationCountFetcherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;",
            ">;"
        }
    .end annotation
.end field

.field private final browserLauncherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;"
        }
    .end annotation
.end field

.field private final catalogIntegrationControllerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            ">;"
        }
    .end annotation
.end field

.field private final catalogServiceEndpointProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalog/CatalogServiceEndpoint;",
            ">;"
        }
    .end annotation
.end field

.field private final cogsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;"
        }
    .end annotation
.end field

.field private final connectivityMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final currencyCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field

.field private final editInventoryStateControllerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditInventoryStateController;",
            ">;"
        }
    .end annotation
.end field

.field private final editItemStateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditItemState;",
            ">;"
        }
    .end annotation
.end field

.field private final editVariationRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditVariationRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final eventLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditItemEventLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final favoritesTileItemSelectionEventsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final servicesCustomizationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/appointmentsapi/ServicesCustomization;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final setupGuideIntegrationRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/setupguide/SetupGuideIntegrationRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialCoreProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;"
        }
    .end annotation
.end field

.field private final unsupportedItemOptionActionDialogRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/UnsupportedItemOptionActionDialogRunner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditItemState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalog/CatalogServiceEndpoint;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/adanalytics/AdAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/appointmentsapi/ServicesCustomization;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/inventory/AdjustInventoryStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditInventoryStateController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditVariationRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditItemEventLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/setupguide/SetupGuideIntegrationRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/UnsupportedItemOptionActionDialogRunner;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 103
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner_Factory;->editItemStateProvider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 104
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner_Factory;->cogsProvider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 105
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner_Factory;->catalogServiceEndpointProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 106
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner_Factory;->resProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 107
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 108
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner_Factory;->settingsProvider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 109
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner_Factory;->connectivityMonitorProvider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 110
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner_Factory;->analyticsProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 111
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner_Factory;->adAnalyticsProvider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 112
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner_Factory;->browserLauncherProvider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 113
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner_Factory;->localeProvider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 114
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner_Factory;->favoritesTileItemSelectionEventsProvider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 115
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner_Factory;->servicesCustomizationProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 116
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner_Factory;->currencyCodeProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 117
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner_Factory;->adjustInventoryStarterProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 118
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner_Factory;->editInventoryStateControllerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p17

    .line 119
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner_Factory;->tutorialCoreProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p18

    .line 120
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner_Factory;->editVariationRunnerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p19

    .line 121
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner_Factory;->eventLoggerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p20

    .line 122
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner_Factory;->catalogIntegrationControllerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p21

    .line 123
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner_Factory;->setupGuideIntegrationRunnerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p22

    .line 124
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner_Factory;->featuresProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p23

    .line 125
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner_Factory;->appliedLocationCountFetcherProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p24

    .line 126
    iput-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner_Factory;->unsupportedItemOptionActionDialogRunnerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/items/EditItemScopeRunner_Factory;
    .locals 26
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditItemState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalog/CatalogServiceEndpoint;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/adanalytics/AdAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/appointmentsapi/ServicesCustomization;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/inventory/AdjustInventoryStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditInventoryStateController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditVariationRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/EditItemEventLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/setupguide/SetupGuideIntegrationRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/items/UnsupportedItemOptionActionDialogRunner;",
            ">;)",
            "Lcom/squareup/ui/items/EditItemScopeRunner_Factory;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    move-object/from16 v23, p22

    move-object/from16 v24, p23

    .line 154
    new-instance v25, Lcom/squareup/ui/items/EditItemScopeRunner_Factory;

    move-object/from16 v0, v25

    invoke-direct/range {v0 .. v24}, Lcom/squareup/ui/items/EditItemScopeRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v25
.end method

.method public static newInstance(Lcom/squareup/ui/items/EditItemState;Lcom/squareup/cogs/Cogs;Lcom/squareup/catalog/CatalogServiceEndpoint;Lcom/squareup/util/Res;Lflow/Flow;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/analytics/Analytics;Lcom/squareup/adanalytics/AdAnalytics;Lcom/squareup/util/BrowserLauncher;Ljavax/inject/Provider;Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;Lcom/squareup/appointmentsapi/ServicesCustomization;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/ui/inventory/AdjustInventoryStarter;Lcom/squareup/ui/items/EditInventoryStateController;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/ui/items/EditVariationRunner;Lcom/squareup/ui/items/EditItemEventLogger;Lcom/squareup/catalogapi/CatalogIntegrationController;Lcom/squareup/setupguide/SetupGuideIntegrationRunner;Lcom/squareup/settings/server/Features;Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;Lcom/squareup/ui/items/UnsupportedItemOptionActionDialogRunner;)Lcom/squareup/ui/items/EditItemScopeRunner;
    .locals 26
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/items/EditItemState;",
            "Lcom/squareup/cogs/Cogs;",
            "Lcom/squareup/catalog/CatalogServiceEndpoint;",
            "Lcom/squareup/util/Res;",
            "Lflow/Flow;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/adanalytics/AdAnalytics;",
            "Lcom/squareup/util/BrowserLauncher;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;",
            "Lcom/squareup/appointmentsapi/ServicesCustomization;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/ui/inventory/AdjustInventoryStarter;",
            "Lcom/squareup/ui/items/EditInventoryStateController;",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            "Lcom/squareup/ui/items/EditVariationRunner;",
            "Lcom/squareup/ui/items/EditItemEventLogger;",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            "Lcom/squareup/setupguide/SetupGuideIntegrationRunner;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;",
            "Lcom/squareup/ui/items/UnsupportedItemOptionActionDialogRunner;",
            ")",
            "Lcom/squareup/ui/items/EditItemScopeRunner;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    move-object/from16 v23, p22

    move-object/from16 v24, p23

    .line 170
    new-instance v25, Lcom/squareup/ui/items/EditItemScopeRunner;

    move-object/from16 v0, v25

    invoke-direct/range {v0 .. v24}, Lcom/squareup/ui/items/EditItemScopeRunner;-><init>(Lcom/squareup/ui/items/EditItemState;Lcom/squareup/cogs/Cogs;Lcom/squareup/catalog/CatalogServiceEndpoint;Lcom/squareup/util/Res;Lflow/Flow;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/analytics/Analytics;Lcom/squareup/adanalytics/AdAnalytics;Lcom/squareup/util/BrowserLauncher;Ljavax/inject/Provider;Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;Lcom/squareup/appointmentsapi/ServicesCustomization;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/ui/inventory/AdjustInventoryStarter;Lcom/squareup/ui/items/EditInventoryStateController;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/ui/items/EditVariationRunner;Lcom/squareup/ui/items/EditItemEventLogger;Lcom/squareup/catalogapi/CatalogIntegrationController;Lcom/squareup/setupguide/SetupGuideIntegrationRunner;Lcom/squareup/settings/server/Features;Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;Lcom/squareup/ui/items/UnsupportedItemOptionActionDialogRunner;)V

    return-object v25
.end method


# virtual methods
.method public get()Lcom/squareup/ui/items/EditItemScopeRunner;
    .locals 26

    move-object/from16 v0, p0

    .line 131
    iget-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner_Factory;->editItemStateProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/ui/items/EditItemState;

    iget-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner_Factory;->cogsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/cogs/Cogs;

    iget-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner_Factory;->catalogServiceEndpointProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/catalog/CatalogServiceEndpoint;

    iget-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/squareup/util/Res;

    iget-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lflow/Flow;

    iget-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner_Factory;->connectivityMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/squareup/connectivity/ConnectivityMonitor;

    iget-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/squareup/analytics/Analytics;

    iget-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner_Factory;->adAnalyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/squareup/adanalytics/AdAnalytics;

    iget-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner_Factory;->browserLauncherProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/squareup/util/BrowserLauncher;

    iget-object v12, v0, Lcom/squareup/ui/items/EditItemScopeRunner_Factory;->localeProvider:Ljavax/inject/Provider;

    iget-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner_Factory;->favoritesTileItemSelectionEventsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;

    iget-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner_Factory;->servicesCustomizationProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/squareup/appointmentsapi/ServicesCustomization;

    iget-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner_Factory;->currencyCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/squareup/protos/common/CurrencyCode;

    iget-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner_Factory;->adjustInventoryStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/ui/inventory/AdjustInventoryStarter;

    iget-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner_Factory;->editInventoryStateControllerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v17, v1

    check-cast v17, Lcom/squareup/ui/items/EditInventoryStateController;

    iget-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner_Factory;->tutorialCoreProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v18, v1

    check-cast v18, Lcom/squareup/tutorialv2/TutorialCore;

    iget-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner_Factory;->editVariationRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v19, v1

    check-cast v19, Lcom/squareup/ui/items/EditVariationRunner;

    iget-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner_Factory;->eventLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v20, v1

    check-cast v20, Lcom/squareup/ui/items/EditItemEventLogger;

    iget-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner_Factory;->catalogIntegrationControllerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v21, v1

    check-cast v21, Lcom/squareup/catalogapi/CatalogIntegrationController;

    iget-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner_Factory;->setupGuideIntegrationRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v22, v1

    check-cast v22, Lcom/squareup/setupguide/SetupGuideIntegrationRunner;

    iget-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v23, v1

    check-cast v23, Lcom/squareup/settings/server/Features;

    iget-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner_Factory;->appliedLocationCountFetcherProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v24, v1

    check-cast v24, Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;

    iget-object v1, v0, Lcom/squareup/ui/items/EditItemScopeRunner_Factory;->unsupportedItemOptionActionDialogRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v25, v1

    check-cast v25, Lcom/squareup/ui/items/UnsupportedItemOptionActionDialogRunner;

    invoke-static/range {v2 .. v25}, Lcom/squareup/ui/items/EditItemScopeRunner_Factory;->newInstance(Lcom/squareup/ui/items/EditItemState;Lcom/squareup/cogs/Cogs;Lcom/squareup/catalog/CatalogServiceEndpoint;Lcom/squareup/util/Res;Lflow/Flow;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/analytics/Analytics;Lcom/squareup/adanalytics/AdAnalytics;Lcom/squareup/util/BrowserLauncher;Ljavax/inject/Provider;Lcom/squareup/orderentry/FavoritesTileItemSelectionEvents;Lcom/squareup/appointmentsapi/ServicesCustomization;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/ui/inventory/AdjustInventoryStarter;Lcom/squareup/ui/items/EditInventoryStateController;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/ui/items/EditVariationRunner;Lcom/squareup/ui/items/EditItemEventLogger;Lcom/squareup/catalogapi/CatalogIntegrationController;Lcom/squareup/setupguide/SetupGuideIntegrationRunner;Lcom/squareup/settings/server/Features;Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;Lcom/squareup/ui/items/UnsupportedItemOptionActionDialogRunner;)Lcom/squareup/ui/items/EditItemScopeRunner;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 26
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemScopeRunner_Factory;->get()Lcom/squareup/ui/items/EditItemScopeRunner;

    move-result-object v0

    return-object v0
.end method
