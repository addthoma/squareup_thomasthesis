.class public final Lcom/squareup/ui/items/unit/UnitSelectionCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "UnitSelectionCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType;,
        Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nUnitSelectionCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 UnitSelectionCoordinator.kt\ncom/squareup/ui/items/unit/UnitSelectionCoordinator\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 RecyclerFactory.kt\ncom/squareup/recycler/RecyclerFactory\n+ 4 Recycler.kt\ncom/squareup/cycler/Recycler$Companion\n+ 5 RecyclerNoho.kt\ncom/squareup/noho/dsl/RecyclerNohoKt\n+ 6 Recycler.kt\ncom/squareup/cycler/Recycler$Config\n+ 7 StandardRowSpec.kt\ncom/squareup/cycler/StandardRowSpec\n+ 8 RecyclerEdges.kt\ncom/squareup/noho/dsl/RecyclerEdgesKt\n*L\n1#1,242:1\n1360#2:243\n1429#2,3:244\n1360#2:247\n1429#2,3:248\n49#3:251\n50#3,3:257\n53#3:293\n599#4,4:252\n601#4:256\n100#5:260\n114#5,5:261\n120#5:277\n101#5:278\n328#6:266\n342#6,5:267\n344#6,4:272\n329#6:276\n355#6,3:279\n358#6,3:288\n35#7,6:282\n43#8,2:291\n*E\n*S KotlinDebug\n*F\n+ 1 UnitSelectionCoordinator.kt\ncom/squareup/ui/items/unit/UnitSelectionCoordinator\n*L\n79#1:243\n79#1,3:244\n87#1:247\n87#1,3:248\n118#1:251\n118#1,3:257\n118#1:293\n118#1,4:252\n118#1:256\n118#1:260\n118#1,5:261\n118#1:277\n118#1:278\n118#1:266\n118#1,5:267\n118#1,4:272\n118#1:276\n118#1,3:279\n118#1,3:288\n118#1,6:282\n118#1,2:291\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000b\u0008\u0000\u0018\u0000 \u001e2\u00020\u0001:\u0002\u001e\u001fB;\u0008\u0007\u0012\"\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u0010\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0015H\u0016J\u0010\u0010\u0016\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0015H\u0002J\u0010\u0010\u0017\u001a\u00020\u00132\u0006\u0010\u0018\u001a\u00020\u0005H\u0002J\u0010\u0010\u0019\u001a\u00020\u00132\u0006\u0010\u001a\u001a\u00020\u0015H\u0002J\u0010\u0010\u001b\u001a\u00020\u00132\u0006\u0010\u0018\u001a\u00020\u0005H\u0002J\u0010\u0010\u001c\u001a\u00020\u00132\u0006\u0010\u0018\u001a\u00020\u0005H\u0002J\u0018\u0010\u001d\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0018\u001a\u00020\u0005H\u0002R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R*\u0010\u0002\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006 "
    }
    d2 = {
        "Lcom/squareup/ui/items/unit/UnitSelectionCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/ui/items/unit/UnitSelectionScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "recyclerFactory",
        "Lcom/squareup/recycler/RecyclerFactory;",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lio/reactivex/Observable;Lcom/squareup/recycler/RecyclerFactory;Lcom/squareup/util/Res;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/ActionBarView;",
        "recycler",
        "Lcom/squareup/cycler/Recycler;",
        "Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType;",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindView",
        "configureActionBar",
        "screen",
        "createRecycler",
        "coordinatorView",
        "discardButtonTapped",
        "saveButtonTapped",
        "update",
        "Companion",
        "UnitType",
        "edit-item_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$Companion;

.field public static final perDefaultStandardUnitIdPrefix:Ljava/lang/String; = "PerDefaultStandardUnit"


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field private recycler:Lcom/squareup/cycler/Recycler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cycler/Recycler<",
            "Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType;",
            ">;"
        }
    .end annotation
.end field

.field private final recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

.field private final res:Lcom/squareup/util/Res;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator;->Companion:Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$Companion;

    return-void
.end method

.method public constructor <init>(Lio/reactivex/Observable;Lcom/squareup/recycler/RecyclerFactory;Lcom/squareup/util/Res;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;",
            "Lcom/squareup/recycler/RecyclerFactory;",
            "Lcom/squareup/util/Res;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "recyclerFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator;->screens:Lio/reactivex/Observable;

    iput-object p2, p0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    iput-object p3, p0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator;->res:Lcom/squareup/util/Res;

    return-void
.end method

.method public static final synthetic access$discardButtonTapped(Lcom/squareup/ui/items/unit/UnitSelectionCoordinator;Lcom/squareup/ui/items/unit/UnitSelectionScreen;)V
    .locals 0

    .line 50
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator;->discardButtonTapped(Lcom/squareup/ui/items/unit/UnitSelectionScreen;)V

    return-void
.end method

.method public static final synthetic access$getRes$p(Lcom/squareup/ui/items/unit/UnitSelectionCoordinator;)Lcom/squareup/util/Res;
    .locals 0

    .line 50
    iget-object p0, p0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator;->res:Lcom/squareup/util/Res;

    return-object p0
.end method

.method public static final synthetic access$saveButtonTapped(Lcom/squareup/ui/items/unit/UnitSelectionCoordinator;Lcom/squareup/ui/items/unit/UnitSelectionScreen;)V
    .locals 0

    .line 50
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator;->saveButtonTapped(Lcom/squareup/ui/items/unit/UnitSelectionScreen;)V

    return-void
.end method

.method public static final synthetic access$update(Lcom/squareup/ui/items/unit/UnitSelectionCoordinator;Landroid/view/View;Lcom/squareup/ui/items/unit/UnitSelectionScreen;)V
    .locals 0

    .line 50
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator;->update(Landroid/view/View;Lcom/squareup/ui/items/unit/UnitSelectionScreen;)V

    return-void
.end method

.method private final bindView(Landroid/view/View;)V
    .locals 1

    .line 200
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object p1, p0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    return-void
.end method

.method private final configureActionBar(Lcom/squareup/ui/items/unit/UnitSelectionScreen;)V
    .locals 5

    .line 188
    iget-object v0, p0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    if-nez v0, :cond_0

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    const-string v1, "actionBar.presenter"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 183
    new-instance v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 184
    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v3, p0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/edititem/R$string;->edit_item_unit_type:I

    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 185
    new-instance v2, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$configureActionBar$1;

    invoke-direct {v2, p0, p1}, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$configureActionBar$1;-><init>(Lcom/squareup/ui/items/unit/UnitSelectionCoordinator;Lcom/squareup/ui/items/unit/UnitSelectionScreen;)V

    check-cast v2, Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 186
    iget-object v2, p0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/common/strings/R$string;->save:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 187
    new-instance v2, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$configureActionBar$2;

    invoke-direct {v2, p0, p1}, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$configureActionBar$2;-><init>(Lcom/squareup/ui/items/unit/UnitSelectionCoordinator;Lcom/squareup/ui/items/unit/UnitSelectionScreen;)V

    check-cast v2, Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 188
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method private final createRecycler(Landroid/view/View;)V
    .locals 5

    .line 116
    sget v0, Lcom/squareup/edititem/R$id;->selectable_unit_list:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView;

    .line 118
    iget-object v0, p0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    .line 251
    sget-object v1, Lcom/squareup/cycler/Recycler;->Companion:Lcom/squareup/cycler/Recycler$Companion;

    .line 252
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 253
    new-instance v1, Lcom/squareup/cycler/Recycler$Config;

    invoke-direct {v1}, Lcom/squareup/cycler/Recycler$Config;-><init>()V

    .line 257
    invoke-virtual {v0}, Lcom/squareup/recycler/RecyclerFactory;->getMainDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/cycler/Recycler$Config;->setMainDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 258
    invoke-virtual {v0}, Lcom/squareup/recycler/RecyclerFactory;->getBackgroundDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->setBackgroundDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 119
    sget-object v0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$createRecycler$1$1;->INSTANCE:Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$createRecycler$1$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->stableId(Lkotlin/jvm/functions/Function1;)V

    .line 124
    sget-object v0, Lcom/squareup/noho/CheckType$RADIO;->INSTANCE:Lcom/squareup/noho/CheckType$RADIO;

    check-cast v0, Lcom/squareup/noho/CheckType;

    .line 260
    new-instance v2, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$createRecycler$$inlined$adopt$lambda$1;

    invoke-direct {v2, p0}, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$createRecycler$$inlined$adopt$lambda$1;-><init>(Lcom/squareup/ui/items/unit/UnitSelectionCoordinator;)V

    check-cast v2, Lkotlin/jvm/functions/Function3;

    .line 262
    new-instance v3, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$$special$$inlined$nohoRow$2;

    invoke-direct {v3, v0}, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$$special$$inlined$nohoRow$2;-><init>(Lcom/squareup/noho/CheckType;)V

    check-cast v3, Lkotlin/jvm/functions/Function1;

    .line 268
    new-instance v0, Lcom/squareup/cycler/BinderRowSpec;

    .line 272
    sget-object v4, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$$special$$inlined$nohoRow$3;->INSTANCE:Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$$special$$inlined$nohoRow$3;

    check-cast v4, Lkotlin/jvm/functions/Function1;

    .line 268
    invoke-direct {v0, v4, v3}, Lcom/squareup/cycler/BinderRowSpec;-><init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 266
    invoke-virtual {v0, v2}, Lcom/squareup/cycler/BinderRowSpec;->bind(Lkotlin/jvm/functions/Function3;)V

    .line 271
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 267
    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 280
    new-instance v0, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v2, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$$special$$inlined$extraItem$1;->INSTANCE:Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$$special$$inlined$extraItem$1;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v2}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 151
    sget v2, Lcom/squareup/edititem/R$layout;->edit_item_unit_selection_create_unit_row:I

    .line 282
    new-instance v3, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$$special$$inlined$create$1;

    invoke-direct {v3, v2}, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$$special$$inlined$create$1;-><init>(I)V

    check-cast v3, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v0, v3}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 167
    check-cast v0, Lcom/squareup/cycler/Recycler$RowSpec;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/squareup/noho/dsl/RecyclerEdgesKt;->setEdges(Lcom/squareup/cycler/Recycler$RowSpec;I)V

    .line 279
    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->extraItem(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 291
    new-instance v0, Lcom/squareup/noho/dsl/EdgesExtensionSpec;

    invoke-direct {v0}, Lcom/squareup/noho/dsl/EdgesExtensionSpec;-><init>()V

    const/16 v2, 0xa

    .line 170
    invoke-virtual {v0, v2}, Lcom/squareup/noho/dsl/EdgesExtensionSpec;->setDefault(I)V

    check-cast v0, Lcom/squareup/cycler/ExtensionSpec;

    .line 291
    invoke-virtual {v1, v0}, Lcom/squareup/cycler/Recycler$Config;->extension(Lcom/squareup/cycler/ExtensionSpec;)V

    .line 255
    invoke-virtual {v1, p1}, Lcom/squareup/cycler/Recycler$Config;->setUp(Landroidx/recyclerview/widget/RecyclerView;)Lcom/squareup/cycler/Recycler;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator;->recycler:Lcom/squareup/cycler/Recycler;

    .line 175
    new-instance v0, Lcom/squareup/items/unit/ui/RecyclerViewTopSpacingDecoration;

    .line 176
    iget-object v1, p0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/noho/R$dimen;->noho_gutter_card_vertical:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getDimensionPixelSize(I)I

    move-result v1

    .line 175
    invoke-direct {v0, v1}, Lcom/squareup/items/unit/ui/RecyclerViewTopSpacingDecoration;-><init>(I)V

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;

    .line 174
    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;)V

    return-void

    .line 252
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "RecyclerView needs a layoutManager assigned."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method private final discardButtonTapped(Lcom/squareup/ui/items/unit/UnitSelectionScreen;)V
    .locals 1

    .line 196
    invoke-virtual {p1}, Lcom/squareup/ui/items/unit/UnitSelectionScreen;->getOnEvent()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    sget-object v0, Lcom/squareup/ui/items/unit/UnitSelectionScreen$Event$DiscardButtonTapped;->INSTANCE:Lcom/squareup/ui/items/unit/UnitSelectionScreen$Event$DiscardButtonTapped;

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private final saveButtonTapped(Lcom/squareup/ui/items/unit/UnitSelectionScreen;)V
    .locals 1

    .line 192
    invoke-virtual {p1}, Lcom/squareup/ui/items/unit/UnitSelectionScreen;->getOnEvent()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    sget-object v0, Lcom/squareup/ui/items/unit/UnitSelectionScreen$Event$SaveButtonTapped;->INSTANCE:Lcom/squareup/ui/items/unit/UnitSelectionScreen$Event$SaveButtonTapped;

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private final update(Landroid/view/View;Lcom/squareup/ui/items/unit/UnitSelectionScreen;)V
    .locals 5

    .line 74
    new-instance v0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$update$1;

    invoke-direct {v0, p0, p2}, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$update$1;-><init>(Lcom/squareup/ui/items/unit/UnitSelectionCoordinator;Lcom/squareup/ui/items/unit/UnitSelectionScreen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 75
    invoke-direct {p0, p2}, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator;->configureActionBar(Lcom/squareup/ui/items/unit/UnitSelectionScreen;)V

    .line 77
    invoke-virtual {p2}, Lcom/squareup/ui/items/unit/UnitSelectionScreen;->getState()Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;->getShouldDisableUnitCreation()Z

    move-result p1

    .line 79
    invoke-virtual {p2}, Lcom/squareup/ui/items/unit/UnitSelectionScreen;->getState()Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;->getSelectableUnits()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 243
    new-instance v1, Ljava/util/ArrayList;

    const/16 v2, 0xa

    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v1, Ljava/util/Collection;

    .line 244
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 245
    check-cast v3, Lcom/squareup/items/unit/SelectableUnit;

    .line 80
    new-instance v4, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType$PerUnit;

    invoke-direct {v4, p2, v3}, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType$PerUnit;-><init>(Lcom/squareup/ui/items/unit/UnitSelectionScreen;Lcom/squareup/items/unit/SelectableUnit;)V

    invoke-interface {v1, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 246
    :cond_0
    check-cast v1, Ljava/util/List;

    if-eqz p1, :cond_1

    .line 85
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p1

    goto :goto_2

    .line 87
    :cond_1
    invoke-virtual {p2}, Lcom/squareup/ui/items/unit/UnitSelectionScreen;->getState()Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;->getDefaultStandardUnits()Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    .line 247
    new-instance v0, Ljava/util/ArrayList;

    invoke-static {p1, v2}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 248
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 249
    check-cast v2, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    .line 88
    new-instance v3, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType$PerDefaultStandardUnit;

    .line 89
    iget-object v4, p0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator;->res:Lcom/squareup/util/Res;

    invoke-static {v2, v4}, Lcom/squareup/quantity/StandardMeasurementUnitLocalizationHelperKt;->localizedName(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v4

    .line 88
    invoke-direct {v3, p2, v2, v4}, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType$PerDefaultStandardUnit;-><init>(Lcom/squareup/ui/items/unit/UnitSelectionScreen;Lcom/squareup/protos/connect/v2/common/MeasurementUnit;Ljava/lang/String;)V

    .line 90
    invoke-interface {v0, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 250
    :cond_2
    move-object p1, v0

    check-cast p1, Ljava/util/List;

    .line 93
    :goto_2
    iget-object v0, p0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator;->recycler:Lcom/squareup/cycler/Recycler;

    if-nez v0, :cond_3

    const-string v2, "recycler"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    new-instance v2, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$update$2;

    invoke-direct {v2, p2, v1, p1}, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$update$2;-><init>(Lcom/squareup/ui/items/unit/UnitSelectionScreen;Ljava/util/List;Ljava/util/List;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v2}, Lcom/squareup/cycler/Recycler;->update(Lkotlin/jvm/functions/Function1;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 62
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator;->bindView(Landroid/view/View;)V

    .line 64
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator;->createRecycler(Landroid/view/View;)V

    .line 66
    iget-object v0, p0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator;->screens:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$attach$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$attach$1;-><init>(Lcom/squareup/ui/items/unit/UnitSelectionCoordinator;Landroid/view/View;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "screens.subscribe { s ->\u2026view, s.unwrapV2Screen) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    return-void
.end method
