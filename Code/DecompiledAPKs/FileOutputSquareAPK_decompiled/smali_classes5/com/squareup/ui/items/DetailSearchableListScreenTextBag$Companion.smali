.class public final Lcom/squareup/ui/items/DetailSearchableListScreenTextBag$Companion;
.super Ljava/lang/Object;
.source "DetailSearchableListScreenTextBag.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\t\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006R\u0011\u0010\u0007\u001a\u00020\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\u0006R\u0011\u0010\t\u001a\u00020\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u0006R\u0011\u0010\u000b\u001a\u00020\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\u0006\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/ui/items/DetailSearchableListScreenTextBag$Companion;",
        "",
        "()V",
        "DetailSearchableListScreenTextBagForDiscounts",
        "Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;",
        "getDetailSearchableListScreenTextBagForDiscounts",
        "()Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;",
        "DetailSearchableListScreenTextBagForItems",
        "getDetailSearchableListScreenTextBagForItems",
        "DetailSearchableListScreenTextBagForServices",
        "getDetailSearchableListScreenTextBagForServices",
        "DetailSearchableListScreenTextBagForUnits",
        "getDetailSearchableListScreenTextBagForUnits",
        "items-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 20
    invoke-direct {p0}, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final getDetailSearchableListScreenTextBagForDiscounts()Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;
    .locals 1

    .line 30
    invoke-static {}, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->access$getDetailSearchableListScreenTextBagForDiscounts$cp()Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;

    move-result-object v0

    return-object v0
.end method

.method public final getDetailSearchableListScreenTextBagForItems()Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;
    .locals 1

    .line 38
    invoke-static {}, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->access$getDetailSearchableListScreenTextBagForItems$cp()Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;

    move-result-object v0

    return-object v0
.end method

.method public final getDetailSearchableListScreenTextBagForServices()Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;
    .locals 1

    .line 46
    invoke-static {}, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->access$getDetailSearchableListScreenTextBagForServices$cp()Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;

    move-result-object v0

    return-object v0
.end method

.method public final getDetailSearchableListScreenTextBagForUnits()Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;
    .locals 1

    .line 21
    invoke-static {}, Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;->access$getDetailSearchableListScreenTextBagForUnits$cp()Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;

    move-result-object v0

    return-object v0
.end method
