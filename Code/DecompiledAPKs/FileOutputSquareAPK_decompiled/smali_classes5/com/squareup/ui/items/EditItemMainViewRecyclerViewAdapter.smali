.class public Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter;
.super Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;
.source "EditItemMainViewRecyclerViewAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter<",
        "Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter$ViewHolder;",
        "Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;",
        ">;"
    }
.end annotation


# static fields
.field private static final BOTTOM_STATIC_ROW:I = 0x3

.field private static final SINGLE_VARIATION_ROW:I = 0x1

.field private static final TOP_STATIC_ROW:I = 0x0

.field private static final VARIATION_ROW:I = 0x2


# instance fields
.field private final allSubscriptionsOnRecyclerView:Lrx/subscriptions/CompositeSubscription;

.field private final durationFormatter:Lcom/squareup/text/DurationFormatter;

.field private final moneyFormatter:Lcom/squareup/quantity/PerUnitFormatter;

.field private final presenter:Lcom/squareup/ui/items/EditItemMainPresenter;

.field private final res:Lcom/squareup/util/Res;

.field private screenData:Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;


# direct methods
.method public constructor <init>(Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/ui/items/EditItemMainPresenter;Lcom/squareup/util/Res;Lcom/squareup/text/DurationFormatter;)V
    .locals 1

    .line 63
    invoke-direct {p0}, Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;-><init>()V

    .line 57
    new-instance v0, Lrx/subscriptions/CompositeSubscription;

    invoke-direct {v0}, Lrx/subscriptions/CompositeSubscription;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter;->allSubscriptionsOnRecyclerView:Lrx/subscriptions/CompositeSubscription;

    .line 64
    iput-object p1, p0, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter;->moneyFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    .line 65
    iput-object p2, p0, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter;->presenter:Lcom/squareup/ui/items/EditItemMainPresenter;

    .line 66
    iput-object p3, p0, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter;->res:Lcom/squareup/util/Res;

    .line 67
    iput-object p4, p0, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter;->durationFormatter:Lcom/squareup/text/DurationFormatter;

    return-void
.end method


# virtual methods
.method public getItemViewType(I)I
    .locals 1

    .line 214
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter;->getStaticTopRowsCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    return p1

    .line 216
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter;->getItemCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_1

    const/4 p1, 0x3

    return p1

    :cond_1
    const/4 p1, 0x2

    return p1
.end method

.method public getStableIdForIndex(I)J
    .locals 2

    .line 224
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter;->list:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getId()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result p1

    int-to-long v0, p1

    return-wide v0
.end method

.method public getStaticBottomRowsCount()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public getStaticTopRowsCount()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .line 45
    check-cast p1, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter$ViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter;->onBindViewHolder(Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter$ViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter$ViewHolder;I)V
    .locals 5

    .line 269
    invoke-static {p1}, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter$ViewHolder;->access$000(Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter$ViewHolder;)Lrx/subscriptions/CompositeSubscription;

    move-result-object v0

    invoke-virtual {v0}, Lrx/subscriptions/CompositeSubscription;->clear()V

    .line 271
    invoke-virtual {p1}, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter$ViewHolder;->getItemViewType()I

    move-result v0

    if-eqz v0, :cond_4

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 p2, 0x3

    if-ne v0, p2, :cond_0

    .line 291
    iget-object p2, p0, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter;->screenData:Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter$ViewHolder;->bindStaticBottomRowContent(Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;)V

    goto :goto_0

    .line 294
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo p2, "viewType not supported"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 279
    :cond_1
    invoke-static {p1}, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter$ViewHolder;->access$100(Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter$ViewHolder;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/widgets/DraggableVariationRow;

    .line 280
    invoke-virtual {p0, p2}, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 281
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter;->getStaticTopRowsCount()I

    move-result v2

    sub-int/2addr p2, v2

    .line 282
    iget-object v2, p0, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter;->screenData:Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;

    invoke-virtual {p1, v1, p2, v2}, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter$ViewHolder;->bindVariation(Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;ILcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;)V

    .line 283
    iget-wide v1, p0, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter;->draggingRowId:J

    invoke-virtual {p0, p2}, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter;->getStableIdForIndex(I)J

    move-result-wide v3

    cmp-long p2, v1, v3

    if-nez p2, :cond_2

    .line 284
    iput-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter;->draggingRowView:Landroid/view/View;

    const/4 p2, 0x4

    .line 285
    invoke-virtual {v0, p2}, Lcom/squareup/ui/items/widgets/DraggableVariationRow;->setVisibility(I)V

    goto :goto_0

    :cond_2
    const/4 p2, 0x0

    .line 287
    invoke-virtual {v0, p2}, Lcom/squareup/ui/items/widgets/DraggableVariationRow;->setVisibility(I)V

    goto :goto_0

    .line 276
    :cond_3
    iget-object p2, p0, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter;->screenData:Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter$ViewHolder;->bindSingleVariationRowContent(Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;)V

    goto :goto_0

    .line 273
    :cond_4
    iget-object p2, p0, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter;->screenData:Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter$ViewHolder;->bindStaticTopRowContent(Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;)V

    .line 299
    :goto_0
    iget-object p2, p0, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter;->allSubscriptionsOnRecyclerView:Lrx/subscriptions/CompositeSubscription;

    invoke-static {p1}, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter$ViewHolder;->access$000(Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter$ViewHolder;)Lrx/subscriptions/CompositeSubscription;

    move-result-object p1

    invoke-virtual {p2, p1}, Lrx/subscriptions/CompositeSubscription;->add(Lrx/Subscription;)V

    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0

    .line 45
    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter$ViewHolder;

    move-result-object p1

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter$ViewHolder;
    .locals 6

    .line 231
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz p2, :cond_5

    const/4 v2, 0x1

    if-eq p2, v2, :cond_3

    const/4 v2, 0x2

    if-eq p2, v2, :cond_1

    const/4 v2, 0x3

    if-ne p2, v2, :cond_0

    .line 258
    sget p2, Lcom/squareup/edititem/R$layout;->edit_item_main_view_static_bottom:I

    invoke-virtual {v0, p2, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    goto :goto_0

    .line 261
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo p2, "viewType not supported"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 247
    :cond_1
    iget-object p2, p0, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter;->screenData:Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;

    iget-boolean p2, p2, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->isService:Z

    if-eqz p2, :cond_2

    .line 248
    sget p2, Lcom/squareup/edititem/R$layout;->draggable_service_variation_row:I

    .line 249
    invoke-virtual {v0, p2, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    goto :goto_0

    .line 252
    :cond_2
    sget p2, Lcom/squareup/edititem/R$layout;->draggable_item_variation_row:I

    .line 253
    invoke-virtual {v0, p2, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    goto :goto_0

    .line 238
    :cond_3
    iget-object p2, p0, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter;->screenData:Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;

    iget-boolean p2, p2, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->isService:Z

    if-eqz p2, :cond_4

    .line 239
    sget p2, Lcom/squareup/edititem/R$layout;->edit_service_main_view_single_variation_static_row:I

    invoke-virtual {v0, p2, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    goto :goto_0

    .line 242
    :cond_4
    sget p2, Lcom/squareup/edititem/R$layout;->edit_item_main_view_single_variation_static_row:I

    invoke-virtual {v0, p2, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    goto :goto_0

    .line 235
    :cond_5
    sget p2, Lcom/squareup/edititem/R$layout;->edit_item_main_view_static_top:I

    invoke-virtual {v0, p2, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    :goto_0
    move-object v1, p1

    .line 263
    new-instance p1, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter$ViewHolder;

    iget-object v2, p0, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter;->moneyFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    iget-object v3, p0, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter;->presenter:Lcom/squareup/ui/items/EditItemMainPresenter;

    iget-object v4, p0, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter;->res:Lcom/squareup/util/Res;

    iget-object v5, p0, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter;->durationFormatter:Lcom/squareup/text/DurationFormatter;

    move-object v0, p1

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter$ViewHolder;-><init>(Landroid/view/View;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/ui/items/EditItemMainPresenter;Lcom/squareup/util/Res;Lcom/squareup/text/DurationFormatter;)V

    return-object p1
.end method

.method public onDetachedFromRecyclerView(Landroidx/recyclerview/widget/RecyclerView;)V
    .locals 0

    .line 311
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter;->allSubscriptionsOnRecyclerView:Lrx/subscriptions/CompositeSubscription;

    invoke-virtual {p1}, Lrx/subscriptions/CompositeSubscription;->unsubscribe()V

    return-void
.end method

.method public reorderItems(II)V
    .locals 1

    .line 305
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;->reorderItems(II)V

    .line 307
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter;->presenter:Lcom/squareup/ui/items/EditItemMainPresenter;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/ui/items/EditItemMainPresenter;->variationOrderChanged(II)V

    return-void
.end method

.method updateScreenData(Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;)V
    .locals 1

    .line 71
    iput-object p1, p0, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter;->screenData:Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;

    .line 72
    iget-boolean v0, p1, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->shouldShowInlineVariation:Z

    if-nez v0, :cond_0

    .line 73
    iget-object p1, p1, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->variations:Ljava/util/List;

    iput-object p1, p0, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter;->list:Ljava/util/List;

    goto :goto_0

    .line 75
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/items/EditItemMainViewRecyclerViewAdapter;->list:Ljava/util/List;

    :goto_0
    return-void
.end method
