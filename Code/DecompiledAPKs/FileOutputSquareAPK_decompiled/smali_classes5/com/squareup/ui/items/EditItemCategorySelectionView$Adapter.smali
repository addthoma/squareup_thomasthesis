.class final Lcom/squareup/ui/items/EditItemCategorySelectionView$Adapter;
.super Landroid/widget/BaseAdapter;
.source "EditItemCategorySelectionView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/EditItemCategorySelectionView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "Adapter"
.end annotation


# instance fields
.field categories:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemCategory;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/squareup/ui/items/EditItemCategorySelectionView;


# direct methods
.method private constructor <init>(Lcom/squareup/ui/items/EditItemCategorySelectionView;)V
    .locals 0

    .line 86
    iput-object p1, p0, Lcom/squareup/ui/items/EditItemCategorySelectionView$Adapter;->this$0:Lcom/squareup/ui/items/EditItemCategorySelectionView;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/ui/items/EditItemCategorySelectionView;Lcom/squareup/ui/items/EditItemCategorySelectionView$1;)V
    .locals 0

    .line 86
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/EditItemCategorySelectionView$Adapter;-><init>(Lcom/squareup/ui/items/EditItemCategorySelectionView;)V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .line 92
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemCategorySelectionView$Adapter;->categories:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x3

    return v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 0

    .line 86
    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/EditItemCategorySelectionView$Adapter;->getItem(I)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method public getItem(I)Ljava/lang/Void;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1

    const/4 v0, 0x1

    if-eqz p1, :cond_1

    if-eq p1, v0, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    const/4 p1, 0x2

    return p1

    :cond_1
    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .line 104
    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/EditItemCategorySelectionView$Adapter;->getItemViewType(I)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    if-nez p2, :cond_0

    .line 108
    sget p1, Lcom/squareup/edititem/R$layout;->edit_item_new_category_button:I

    invoke-static {p1, p3}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 109
    sget p1, Lcom/squareup/edititem/R$id;->new_category_button:I

    invoke-static {p2, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    .line 110
    new-instance p3, Lcom/squareup/ui/items/EditItemCategorySelectionView$Adapter$1;

    invoke-direct {p3, p0}, Lcom/squareup/ui/items/EditItemCategorySelectionView$Adapter$1;-><init>(Lcom/squareup/ui/items/EditItemCategorySelectionView$Adapter;)V

    invoke-virtual {p1, p3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 116
    new-instance p3, Lcom/squareup/ui/items/-$$Lambda$EditItemCategorySelectionView$Adapter$UFJG82ecKKaDIPnodpy0iDq-gTY;

    invoke-direct {p3, p0}, Lcom/squareup/ui/items/-$$Lambda$EditItemCategorySelectionView$Adapter$UFJG82ecKKaDIPnodpy0iDq-gTY;-><init>(Lcom/squareup/ui/items/EditItemCategorySelectionView$Adapter;)V

    invoke-static {p1, p3}, Lcom/squareup/util/Views;->waitForMeasure(Landroid/view/View;Lcom/squareup/util/OnMeasuredCallback;)V

    :cond_0
    return-object p2

    :cond_1
    const/4 v2, 0x2

    if-ne v0, v2, :cond_3

    if-nez p2, :cond_2

    .line 125
    new-instance p2, Landroid/view/View;

    iget-object p1, p0, Lcom/squareup/ui/items/EditItemCategorySelectionView$Adapter;->this$0:Lcom/squareup/ui/items/EditItemCategorySelectionView;

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditItemCategorySelectionView;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-direct {p2, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    :cond_2
    return-object p2

    :cond_3
    if-nez p2, :cond_4

    .line 132
    invoke-static {p3}, Lcom/squareup/ui/items/widgets/EditItemCategorySelectionListRow;->inflate(Landroid/view/ViewGroup;)Lcom/squareup/ui/items/widgets/EditItemCategorySelectionListRow;

    move-result-object p2

    goto :goto_0

    .line 134
    :cond_4
    check-cast p2, Lcom/squareup/ui/items/widgets/EditItemCategorySelectionListRow;

    .line 137
    :goto_0
    iget-object p3, p0, Lcom/squareup/ui/items/EditItemCategorySelectionView$Adapter;->this$0:Lcom/squareup/ui/items/EditItemCategorySelectionView;

    invoke-virtual {p3}, Lcom/squareup/ui/items/EditItemCategorySelectionView;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    .line 138
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemCategorySelectionView$Adapter;->this$0:Lcom/squareup/ui/items/EditItemCategorySelectionView;

    iget-object v0, v0, Lcom/squareup/ui/items/EditItemCategorySelectionView;->presenter:Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Presenter;->getSelectedCategoryIndex()I

    move-result v0

    const/4 v3, 0x0

    if-ne p1, v2, :cond_6

    .line 140
    sget p1, Lcom/squareup/edititem/R$string;->edit_item_select_category_none:I

    invoke-virtual {p3, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    const/4 p3, -0x1

    if-ne v0, p3, :cond_5

    goto :goto_1

    :cond_5
    const/4 v1, 0x0

    :goto_1
    invoke-virtual {p2, p1, v1}, Lcom/squareup/ui/items/widgets/EditItemCategorySelectionListRow;->setContent(Ljava/lang/String;Z)V

    goto :goto_3

    :cond_6
    add-int/lit8 p1, p1, -0x3

    .line 144
    iget-object p3, p0, Lcom/squareup/ui/items/EditItemCategorySelectionView$Adapter;->categories:Ljava/util/List;

    invoke-interface {p3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lcom/squareup/shared/catalog/models/CatalogItemCategory;

    .line 145
    invoke-virtual {p3}, Lcom/squareup/shared/catalog/models/CatalogItemCategory;->getName()Ljava/lang/String;

    move-result-object p3

    if-ne p1, v0, :cond_7

    goto :goto_2

    :cond_7
    const/4 v1, 0x0

    :goto_2
    invoke-virtual {p2, p3, v1}, Lcom/squareup/ui/items/widgets/EditItemCategorySelectionListRow;->setContent(Ljava/lang/String;Z)V

    :goto_3
    return-object p2
.end method

.method public getViewTypeCount()I
    .locals 1

    const/4 v0, 0x3

    return v0
.end method

.method public isEnabled(I)Z
    .locals 1

    .line 163
    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/EditItemCategorySelectionView$Adapter;->getItemViewType(I)I

    move-result p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public synthetic lambda$getView$0$EditItemCategorySelectionView$Adapter(Landroid/view/View;II)V
    .locals 0

    .line 117
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemCategorySelectionView$Adapter;->this$0:Lcom/squareup/ui/items/EditItemCategorySelectionView;

    iget-object p1, p1, Lcom/squareup/ui/items/EditItemCategorySelectionView;->presenter:Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Presenter;

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditItemCategorySelectionScreen$Presenter;->newCategoryButtonShown()V

    return-void
.end method
