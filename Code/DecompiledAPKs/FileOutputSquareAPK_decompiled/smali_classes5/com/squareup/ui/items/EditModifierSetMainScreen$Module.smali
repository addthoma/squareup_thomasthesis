.class public Lcom/squareup/ui/items/EditModifierSetMainScreen$Module;
.super Ljava/lang/Object;
.source "EditModifierSetMainScreen.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/EditModifierSetMainScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Module"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 327
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideAppliedLocationsBannerPresenter(Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;Lcom/squareup/util/Res;Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;)Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;
    .locals 8
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 332
    new-instance v7, Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;

    sget-object v3, Lcom/squareup/ui/items/ItemEditingStringIds;->MODIFIER_SET:Lcom/squareup/ui/items/ItemEditingStringIds;

    .line 333
    invoke-virtual {p2}, Lcom/squareup/ui/items/EditModifierSetMainScreen$Presenter;->isNewObject()Z

    move-result v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;-><init>(Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;Lcom/squareup/util/Res;Lcom/squareup/ui/items/ItemEditingStringIds;ZZZ)V

    return-object v7
.end method
