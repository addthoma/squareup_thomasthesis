.class public Lcom/squareup/ui/items/EditItemMainScreen;
.super Lcom/squareup/ui/items/InEditItemScope;
.source "EditItemMainScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/layer/MayHideStatusBar;
.implements Lcom/squareup/container/RegistersInScope;


# annotations
.annotation runtime Lcom/squareup/container/layer/FullSheet;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent$FromFactory;
    value = Lcom/squareup/ui/items/EditItemMainScreen$ComponentFactory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/EditItemMainScreen$Component;,
        Lcom/squareup/ui/items/EditItemMainScreen$AssignUnitToVariationWorkflowModule;,
        Lcom/squareup/ui/items/EditItemMainScreen$Module;,
        Lcom/squareup/ui/items/EditItemMainScreen$ComponentFactory;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/items/EditItemMainScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final EDIT_ITEM_NAME_ENTERED:Ljava/lang/String; = "Edit Item Name Entered"

.field public static final EDIT_ITEM_NAME_FOCUSED:Ljava/lang/String; = "Edit Item Name Focused"

.field public static final EDIT_ITEM_PRICE_ENTERED:Ljava/lang/String; = "Edit Item Price Entered"

.field public static final EDIT_ITEM_PRICE_FOCUSED:Ljava/lang/String; = "Edit Item Price Focused"

.field public static final EDIT_ITEM_SAVE_PRESSED:Ljava/lang/String; = "Edit Item Save Pressed"

.field public static final EDIT_ITEM_SHOWN:Ljava/lang/String; = "Edit Item Shown"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 139
    sget-object v0, Lcom/squareup/ui/items/-$$Lambda$EditItemMainScreen$OXl2BPnONs4qT3bADAPr8yJBs7s;->INSTANCE:Lcom/squareup/ui/items/-$$Lambda$EditItemMainScreen$OXl2BPnONs4qT3bADAPr8yJBs7s;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/items/EditItemMainScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/items/EditItemScope;)V
    .locals 1

    .line 46
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/InEditItemScope;-><init>(Lcom/squareup/ui/items/EditItemScope;)V

    .line 47
    iget-object p1, p1, Lcom/squareup/ui/items/EditItemScope;->type:Lcom/squareup/api/items/Item$Type;

    sget-object v0, Lcom/squareup/api/items/Item$Type;->GIFT_CARD:Lcom/squareup/api/items/Item$Type;

    if-eq p1, v0, :cond_0

    return-void

    .line 48
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Gift cards should go to the EditGiftCardScreen"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/items/EditItemMainScreen;
    .locals 1

    .line 140
    const-class v0, Lcom/squareup/ui/items/EditItemScope;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/items/EditItemScope;

    .line 141
    new-instance v0, Lcom/squareup/ui/items/EditItemMainScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/items/EditItemMainScreen;-><init>(Lcom/squareup/ui/items/EditItemScope;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 136
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainScreen;->editItemPath:Lcom/squareup/ui/items/EditItemScope;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method getUpButton()Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 1

    .line 62
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object v0
.end method

.method public hideStatusBar()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method isPrimaryButtonEnabled(Lcom/squareup/ui/items/EditItemState;)Z
    .locals 1

    .line 66
    invoke-virtual {p1}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditItemState;->getItemData()Lcom/squareup/ui/items/EditItemState$ItemData;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/ui/items/EditItemState$ItemData;->item:Lcom/squareup/shared/catalog/models/CatalogItem$Builder;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItem$Builder;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public register(Lmortar/MortarScope;)V
    .locals 1

    .line 53
    const-class v0, Lcom/squareup/ui/items/EditItemMainScreen$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/EditItemMainScreen$Component;

    .line 54
    invoke-interface {v0}, Lcom/squareup/ui/items/EditItemMainScreen$Component;->duplicateSkuValidator()Lcom/squareup/ui/items/DuplicateSkuValidator;

    move-result-object v0

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    return-void
.end method

.method public screenLayout()I
    .locals 1

    .line 145
    sget v0, Lcom/squareup/edititem/R$layout;->edit_item_main_view:I

    return v0
.end method
