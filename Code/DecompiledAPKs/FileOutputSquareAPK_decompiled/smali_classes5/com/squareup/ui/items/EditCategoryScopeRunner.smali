.class public Lcom/squareup/ui/items/EditCategoryScopeRunner;
.super Ljava/lang/Object;
.source "EditCategoryScopeRunner.java"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/EditCategoryScopeRunner$EditCategoryLabelScreenData;
    }
.end annotation


# instance fields
.field private final appliedLocationCountFetcher:Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;

.field private final catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

.field private final cogs:Lcom/squareup/cogs/Cogs;

.field private editCategoryScope:Lcom/squareup/ui/items/EditCategoryScope;

.field private final editCategoryState:Lcom/squareup/ui/items/EditCategoryState;

.field private final editCategoryStateRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay/BehaviorRelay<",
            "Lcom/squareup/ui/items/EditCategoryState;",
            ">;"
        }
    .end annotation
.end field

.field private final flow:Lflow/Flow;

.field private final res:Lcom/squareup/util/Res;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;


# direct methods
.method public constructor <init>(Lcom/squareup/cogs/Cogs;Lcom/squareup/ui/items/EditCategoryState;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;Lflow/Flow;Lcom/squareup/util/Res;Lcom/squareup/catalogapi/CatalogIntegrationController;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    invoke-static {}, Lcom/jakewharton/rxrelay/BehaviorRelay;->create()Lcom/jakewharton/rxrelay/BehaviorRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/items/EditCategoryScopeRunner;->editCategoryStateRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    .line 53
    iput-object p1, p0, Lcom/squareup/ui/items/EditCategoryScopeRunner;->cogs:Lcom/squareup/cogs/Cogs;

    .line 54
    iput-object p2, p0, Lcom/squareup/ui/items/EditCategoryScopeRunner;->editCategoryState:Lcom/squareup/ui/items/EditCategoryState;

    .line 55
    iput-object p3, p0, Lcom/squareup/ui/items/EditCategoryScopeRunner;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 56
    iput-object p4, p0, Lcom/squareup/ui/items/EditCategoryScopeRunner;->appliedLocationCountFetcher:Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;

    .line 57
    iput-object p5, p0, Lcom/squareup/ui/items/EditCategoryScopeRunner;->flow:Lflow/Flow;

    .line 58
    iput-object p6, p0, Lcom/squareup/ui/items/EditCategoryScopeRunner;->res:Lcom/squareup/util/Res;

    .line 59
    iput-object p7, p0, Lcom/squareup/ui/items/EditCategoryScopeRunner;->catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

    .line 60
    iput-object p8, p0, Lcom/squareup/ui/items/EditCategoryScopeRunner;->tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    return-void
.end method

.method private getCategoryNameOrDefault()Ljava/lang/String;
    .locals 3

    .line 119
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryScopeRunner;->editCategoryState:Lcom/squareup/ui/items/EditCategoryState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditCategoryState;->getName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/items/EditCategoryScopeRunner;->editCategoryState:Lcom/squareup/ui/items/EditCategoryState;

    .line 120
    invoke-virtual {v1}, Lcom/squareup/ui/items/EditCategoryState;->isNewCategory()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/squareup/ui/items/EditCategoryScopeRunner;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/registerlib/R$string;->new_category:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_0
    const-string v1, ""

    .line 119
    :goto_0
    invoke-static {v0, v1}, Lcom/squareup/util/Strings;->valueOrDefault(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getLabelColorOrDefault()Ljava/lang/String;
    .locals 3

    .line 114
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryScopeRunner;->editCategoryState:Lcom/squareup/ui/items/EditCategoryState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditCategoryState;->getColor()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/items/EditCategoryScopeRunner;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/widgets/pos/R$color;->edit_item_gray:I

    .line 115
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getColor(I)I

    move-result v1

    invoke-static {v1}, Lcom/squareup/util/Colors;->toHex(I)Ljava/lang/String;

    move-result-object v1

    .line 114
    invoke-static {v0, v1}, Lcom/squareup/util/Strings;->valueOrDefault(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private isNewCategory()Z
    .locals 2

    .line 125
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryScopeRunner;->editCategoryScope:Lcom/squareup/ui/items/EditCategoryScope;

    iget-object v0, v0, Lcom/squareup/ui/items/EditCategoryScope;->categoryId:Ljava/lang/String;

    sget-object v1, Lcom/squareup/ui/items/EditCategoryScope;->NEW_CATEGORY_ID:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private loadCategoryData()Lrx/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Single<",
            "Lcom/squareup/ui/items/EditCategoryState$CategoryDataFromLibrary;",
            ">;"
        }
    .end annotation

    .line 129
    new-instance v0, Lcom/squareup/ui/items/-$$Lambda$EditCategoryScopeRunner$VXJD7xInba3TKoMcRDygaagqYzY;

    invoke-direct {v0, p0}, Lcom/squareup/ui/items/-$$Lambda$EditCategoryScopeRunner$VXJD7xInba3TKoMcRDygaagqYzY;-><init>(Lcom/squareup/ui/items/EditCategoryScopeRunner;)V

    .line 149
    iget-object v1, p0, Lcom/squareup/ui/items/EditCategoryScopeRunner;->cogs:Lcom/squareup/cogs/Cogs;

    invoke-interface {v1, v0}, Lcom/squareup/cogs/Cogs;->asSingle(Lcom/squareup/shared/catalog/CatalogTask;)Lrx/Single;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method abbreviationChanged(Ljava/lang/String;)V
    .locals 1

    .line 89
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryScopeRunner;->editCategoryState:Lcom/squareup/ui/items/EditCategoryState;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/items/EditCategoryState;->setAbbreviation(Ljava/lang/String;)V

    return-void
.end method

.method editCategoryLabelScreenData()Lrx/Single;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Single<",
            "Lcom/squareup/ui/items/EditCategoryScopeRunner$EditCategoryLabelScreenData;",
            ">;"
        }
    .end annotation

    .line 84
    new-instance v0, Lcom/squareup/ui/items/EditCategoryScopeRunner$EditCategoryLabelScreenData;

    invoke-virtual {p0}, Lcom/squareup/ui/items/EditCategoryScopeRunner;->isTextTileMode()Z

    move-result v1

    invoke-direct {p0}, Lcom/squareup/ui/items/EditCategoryScopeRunner;->getCategoryNameOrDefault()Ljava/lang/String;

    move-result-object v2

    .line 85
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditCategoryScopeRunner;->getAbbreviationOrDefault()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0}, Lcom/squareup/ui/items/EditCategoryScopeRunner;->getLabelColorOrDefault()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/ui/items/EditCategoryScopeRunner$EditCategoryLabelScreenData;-><init>(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    invoke-static {v0}, Lrx/Single;->just(Ljava/lang/Object;)Lrx/Single;

    move-result-object v0

    return-object v0
.end method

.method editCategoryState()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/ui/items/EditCategoryState;",
            ">;"
        }
    .end annotation

    .line 80
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryScopeRunner;->editCategoryStateRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    invoke-virtual {v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->asObservable()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method getAbbreviationOrDefault()Ljava/lang/String;
    .locals 2

    .line 109
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryScopeRunner;->editCategoryState:Lcom/squareup/ui/items/EditCategoryState;

    invoke-virtual {v0}, Lcom/squareup/ui/items/EditCategoryState;->getAbbreviation()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/items/EditCategoryScopeRunner;->editCategoryState:Lcom/squareup/ui/items/EditCategoryState;

    .line 110
    invoke-virtual {v1}, Lcom/squareup/ui/items/EditCategoryState;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/util/Strings;->abbreviate(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 109
    invoke-static {v0, v1}, Lcom/squareup/util/Strings;->valueOrDefault(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method goBack()V
    .locals 1

    .line 97
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryScopeRunner;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    return-void
.end method

.method isTextTileMode()Z
    .locals 1

    .line 105
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryScopeRunner;->tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;->isTextTileMode()Z

    move-result v0

    return v0
.end method

.method labelColorChanged(Ljava/lang/String;)V
    .locals 1

    .line 93
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryScopeRunner;->editCategoryState:Lcom/squareup/ui/items/EditCategoryState;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/items/EditCategoryState;->setColor(Ljava/lang/String;)V

    return-void
.end method

.method public synthetic lambda$loadCategoryData$1$EditCategoryScopeRunner(Lcom/squareup/shared/catalog/Catalog$Local;)Lcom/squareup/ui/items/EditCategoryState$CategoryDataFromLibrary;
    .locals 7

    .line 130
    invoke-direct {p0}, Lcom/squareup/ui/items/EditCategoryScopeRunner;->isNewCategory()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    const-class v0, Lcom/squareup/shared/catalog/models/CatalogItemCategory;

    iget-object v1, p0, Lcom/squareup/ui/items/EditCategoryScopeRunner;->editCategoryScope:Lcom/squareup/ui/items/EditCategoryScope;

    iget-object v1, v1, Lcom/squareup/ui/items/EditCategoryScope;->categoryId:Ljava/lang/String;

    .line 131
    invoke-interface {p1, v0, v1}, Lcom/squareup/shared/catalog/Catalog$Local;->findById(Ljava/lang/Class;Ljava/lang/String;)Lcom/squareup/shared/catalog/models/CatalogObject;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/models/CatalogItemCategory;

    .line 132
    :goto_0
    const-class v1, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;

    .line 133
    invoke-interface {p1, v1}, Lcom/squareup/shared/catalog/Catalog$Local;->getSyntheticTableReader(Ljava/lang/Class;)Lcom/squareup/shared/catalog/synthetictables/SyntheticTableReader;

    move-result-object v1

    check-cast v1, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;

    .line 135
    iget-object v2, p0, Lcom/squareup/ui/items/EditCategoryScopeRunner;->catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

    invoke-interface {v2}, Lcom/squareup/catalogapi/CatalogIntegrationController;->shouldShowNewFeature()Z

    move-result v2

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-eqz v2, :cond_1

    .line 136
    iget-object v2, p0, Lcom/squareup/ui/items/EditCategoryScopeRunner;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    new-array v5, v3, [Lcom/squareup/api/items/Item$Type;

    sget-object v6, Lcom/squareup/api/items/Item$Type;->GIFT_CARD:Lcom/squareup/api/items/Item$Type;

    aput-object v6, v5, v4

    .line 137
    invoke-virtual {v2, v5}, Lcom/squareup/settings/server/AccountStatusSettings;->supportedCatalogItemTypesExcluding([Lcom/squareup/api/items/Item$Type;)Ljava/util/List;

    move-result-object v2

    .line 136
    invoke-virtual {v1, v2}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->readItemsWithTypes(Ljava/util/List;)Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    move-result-object v1

    goto :goto_1

    .line 139
    :cond_1
    iget-object v2, p0, Lcom/squareup/ui/items/EditCategoryScopeRunner;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    const/4 v5, 0x2

    new-array v5, v5, [Lcom/squareup/api/items/Item$Type;

    sget-object v6, Lcom/squareup/api/items/Item$Type;->GIFT_CARD:Lcom/squareup/api/items/Item$Type;

    aput-object v6, v5, v4

    sget-object v6, Lcom/squareup/api/items/Item$Type;->APPOINTMENTS_SERVICE:Lcom/squareup/api/items/Item$Type;

    aput-object v6, v5, v3

    .line 140
    invoke-virtual {v2, v5}, Lcom/squareup/settings/server/AccountStatusSettings;->supportedCatalogItemTypesExcluding([Lcom/squareup/api/items/Item$Type;)Ljava/util/List;

    move-result-object v2

    .line 139
    invoke-virtual {v1, v2}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->readItemsWithTypes(Ljava/util/List;)Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    move-result-object v1

    .line 143
    :goto_1
    invoke-direct {p0}, Lcom/squareup/ui/items/EditCategoryScopeRunner;->isNewCategory()Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/squareup/ui/items/EditCategoryScopeRunner;->editCategoryScope:Lcom/squareup/ui/items/EditCategoryScope;

    iget-object v2, v2, Lcom/squareup/ui/items/EditCategoryScope;->categoryId:Ljava/lang/String;

    invoke-interface {p1, v2}, Lcom/squareup/shared/catalog/Catalog$Local;->findCategoryItems(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_2

    goto :goto_2

    :cond_2
    const/4 v3, 0x0

    .line 144
    :cond_3
    :goto_2
    invoke-interface {p1}, Lcom/squareup/shared/catalog/Catalog$Local;->getCategoryNameMap()Ljava/util/Map;

    move-result-object p1

    .line 145
    new-instance v2, Lcom/squareup/ui/items/EditCategoryState$CategoryDataFromLibrary;

    invoke-direct {v2, v0, v1, p1, v3}, Lcom/squareup/ui/items/EditCategoryState$CategoryDataFromLibrary;-><init>(Lcom/squareup/shared/catalog/models/CatalogItemCategory;Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;Ljava/util/Map;Z)V

    return-object v2
.end method

.method public synthetic lambda$onEnterScope$0$EditCategoryScopeRunner(Lcom/squareup/ui/items/EditCategoryState$CategoryDataFromLibrary;)V
    .locals 1

    .line 67
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryScopeRunner;->editCategoryState:Lcom/squareup/ui/items/EditCategoryState;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/items/EditCategoryState;->loadCategoryDataFromLibrary(Lcom/squareup/ui/items/EditCategoryState$CategoryDataFromLibrary;)V

    .line 68
    iget-object p1, p0, Lcom/squareup/ui/items/EditCategoryScopeRunner;->editCategoryStateRelay:Lcom/jakewharton/rxrelay/BehaviorRelay;

    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryScopeRunner;->editCategoryState:Lcom/squareup/ui/items/EditCategoryState;

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay/BehaviorRelay;->call(Ljava/lang/Object;)V

    return-void
.end method

.method onClickCategoryTile()V
    .locals 3

    .line 101
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryScopeRunner;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/ui/items/EditCategoryLabelScreen;

    iget-object v2, p0, Lcom/squareup/ui/items/EditCategoryScopeRunner;->editCategoryScope:Lcom/squareup/ui/items/EditCategoryScope;

    iget-object v2, v2, Lcom/squareup/ui/items/EditCategoryScope;->categoryId:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/squareup/ui/items/EditCategoryLabelScreen;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 64
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/EditCategoryScope;

    iput-object v0, p0, Lcom/squareup/ui/items/EditCategoryScopeRunner;->editCategoryScope:Lcom/squareup/ui/items/EditCategoryScope;

    .line 65
    invoke-static {p1}, Lmortar/bundler/BundleService;->getBundleService(Lmortar/MortarScope;)Lmortar/bundler/BundleService;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/items/EditCategoryScopeRunner;->editCategoryState:Lcom/squareup/ui/items/EditCategoryState;

    invoke-virtual {v0, v1}, Lmortar/bundler/BundleService;->register(Lmortar/bundler/Bundler;)V

    .line 66
    invoke-direct {p0}, Lcom/squareup/ui/items/EditCategoryScopeRunner;->loadCategoryData()Lrx/Single;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/items/-$$Lambda$EditCategoryScopeRunner$H5j2v1Zqux_fK4HrFzXc7ggTrJQ;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/-$$Lambda$EditCategoryScopeRunner$H5j2v1Zqux_fK4HrFzXc7ggTrJQ;-><init>(Lcom/squareup/ui/items/EditCategoryScopeRunner;)V

    invoke-virtual {v0, v1}, Lrx/Single;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    .line 70
    invoke-direct {p0}, Lcom/squareup/ui/items/EditCategoryScopeRunner;->isNewCategory()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryScopeRunner;->appliedLocationCountFetcher:Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;

    invoke-interface {v0}, Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;->getActiveLocationCount()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 71
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryScopeRunner;->appliedLocationCountFetcher:Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 72
    iget-object p1, p0, Lcom/squareup/ui/items/EditCategoryScopeRunner;->appliedLocationCountFetcher:Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;

    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryScopeRunner;->editCategoryScope:Lcom/squareup/ui/items/EditCategoryScope;

    iget-object v0, v0, Lcom/squareup/ui/items/EditCategoryScope;->categoryId:Ljava/lang/String;

    invoke-interface {p1, v0}, Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;->fetchAppliedLocationCount(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method
