.class Lcom/squareup/ui/items/CategoryWithItemCount;
.super Ljava/lang/Object;
.source "CategoryWithItemCount.java"

# interfaces
.implements Lcom/squareup/ui/items/EditDiscountHydratedProduct;


# instance fields
.field private category:Lcom/squareup/shared/catalog/models/CatalogItemCategory;

.field private itemCount:I


# direct methods
.method constructor <init>(Lcom/squareup/shared/catalog/models/CatalogItemCategory;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/models/CatalogItemCategory;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItem;",
            ">;)V"
        }
    .end annotation

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/squareup/ui/items/CategoryWithItemCount;->category:Lcom/squareup/shared/catalog/models/CatalogItemCategory;

    .line 16
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p1

    iput p1, p0, Lcom/squareup/ui/items/CategoryWithItemCount;->itemCount:I

    return-void
.end method


# virtual methods
.method public getColor()Ljava/lang/String;
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/ui/items/CategoryWithItemCount;->category:Lcom/squareup/shared/catalog/models/CatalogItemCategory;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemCategory;->getColor()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getItemCount()I
    .locals 1

    .line 25
    iget v0, p0, Lcom/squareup/ui/items/CategoryWithItemCount;->itemCount:I

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/squareup/ui/items/CategoryWithItemCount;->category:Lcom/squareup/shared/catalog/models/CatalogItemCategory;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemCategory;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
