.class public Lcom/squareup/ui/items/DiscountRuleSectionView;
.super Landroid/widget/LinearLayout;
.source "DiscountRuleSectionView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/DiscountRuleSectionView$RuleRowViewHolder;,
        Lcom/squareup/ui/items/DiscountRuleSectionView$RulesAdapter;
    }
.end annotation


# instance fields
.field private adapter:Lcom/squareup/ui/items/DiscountRuleSectionView$RulesAdapter;

.field private title:Lcom/squareup/marketfont/MarketTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    .line 28
    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/items/DiscountRuleSectionView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    .line 32
    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/ui/items/DiscountRuleSectionView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .line 37
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 25
    new-instance p3, Lcom/squareup/ui/items/DiscountRuleSectionView$RulesAdapter;

    invoke-direct {p3, p0}, Lcom/squareup/ui/items/DiscountRuleSectionView$RulesAdapter;-><init>(Lcom/squareup/ui/items/DiscountRuleSectionView;)V

    iput-object p3, p0, Lcom/squareup/ui/items/DiscountRuleSectionView;->adapter:Lcom/squareup/ui/items/DiscountRuleSectionView$RulesAdapter;

    .line 38
    sget p3, Lcom/squareup/edititem/R$layout;->discount_rule_section_view:I

    const/4 v0, 0x1

    invoke-static {p3, p0, v0}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 40
    sget-object p3, Lcom/squareup/edititem/R$styleable;->DiscountRuleSectionView:[I

    invoke-virtual {p1, p2, p3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p2

    .line 41
    sget p3, Lcom/squareup/edititem/R$id;->discount_bundle_title:I

    invoke-static {p0, p3}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Lcom/squareup/marketfont/MarketTextView;

    iput-object p3, p0, Lcom/squareup/ui/items/DiscountRuleSectionView;->title:Lcom/squareup/marketfont/MarketTextView;

    .line 42
    iget-object p3, p0, Lcom/squareup/ui/items/DiscountRuleSectionView;->title:Lcom/squareup/marketfont/MarketTextView;

    sget v1, Lcom/squareup/edititem/R$styleable;->DiscountRuleSectionView_sectionTitle:I

    invoke-virtual {p2, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 44
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    sget v1, Lcom/squareup/marin/R$dimen;->marin_gutter_half:I

    invoke-virtual {p3, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p3

    .line 46
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v1, Lcom/squareup/marin/R$dimen;->marin_gutter:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    .line 48
    sget v1, Lcom/squareup/edititem/R$styleable;->DiscountRuleSectionView_android_paddingLeft:I

    .line 49
    invoke-virtual {p2, v1, p1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    sget v2, Lcom/squareup/edititem/R$styleable;->DiscountRuleSectionView_android_paddingTop:I

    .line 51
    invoke-virtual {p2, v2, p3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    sget v3, Lcom/squareup/edititem/R$styleable;->DiscountRuleSectionView_android_paddingRight:I

    .line 52
    invoke-virtual {p2, v3, p1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p1

    sget v3, Lcom/squareup/edititem/R$styleable;->DiscountRuleSectionView_android_paddingBottom:I

    .line 54
    invoke-virtual {p2, v3, p3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result p3

    .line 48
    invoke-virtual {p0, v1, v2, p1, p3}, Lcom/squareup/ui/items/DiscountRuleSectionView;->setPadding(IIII)V

    .line 57
    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    .line 59
    invoke-virtual {p0, v0}, Lcom/squareup/ui/items/DiscountRuleSectionView;->setOrientation(I)V

    .line 62
    sget p1, Lcom/squareup/edititem/R$id;->discount_bundle_list:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView;

    .line 63
    new-instance p2, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {p0}, Lcom/squareup/ui/items/DiscountRuleSectionView;->getContext()Landroid/content/Context;

    move-result-object p3

    invoke-direct {p2, p3}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1, p2}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 64
    iget-object p2, p0, Lcom/squareup/ui/items/DiscountRuleSectionView;->adapter:Lcom/squareup/ui/items/DiscountRuleSectionView$RulesAdapter;

    invoke-virtual {p1, p2}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    const/4 p2, 0x0

    .line 65
    invoke-virtual {p1, p2}, Landroidx/recyclerview/widget/RecyclerView;->setNestedScrollingEnabled(Z)V

    const/16 p1, 0x8

    .line 66
    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/DiscountRuleSectionView;->setVisibility(I)V

    return-void
.end method


# virtual methods
.method public isEmpty()Z
    .locals 1

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/items/DiscountRuleSectionView;->adapter:Lcom/squareup/ui/items/DiscountRuleSectionView$RulesAdapter;

    invoke-static {v0}, Lcom/squareup/ui/items/DiscountRuleSectionView$RulesAdapter;->access$000(Lcom/squareup/ui/items/DiscountRuleSectionView$RulesAdapter;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/items/DiscountRuleSectionView;->adapter:Lcom/squareup/ui/items/DiscountRuleSectionView$RulesAdapter;

    invoke-static {v0}, Lcom/squareup/ui/items/DiscountRuleSectionView$RulesAdapter;->access$000(Lcom/squareup/ui/items/DiscountRuleSectionView$RulesAdapter;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public setData(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/ui/items/RuleRowItem;",
            ">;)V"
        }
    .end annotation

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/items/DiscountRuleSectionView;->adapter:Lcom/squareup/ui/items/DiscountRuleSectionView$RulesAdapter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/items/DiscountRuleSectionView$RulesAdapter;->setData(Ljava/util/List;)V

    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 1

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/items/DiscountRuleSectionView;->title:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
