.class public final Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$createRecycler$$inlined$adopt$lambda$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RecyclerNoho.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function3;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/unit/UnitSelectionCoordinator;->createRecycler(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function3<",
        "Ljava/lang/Integer;",
        "Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType;",
        "Lcom/squareup/noho/NohoCheckableRow;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRecyclerNoho.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RecyclerNoho.kt\ncom/squareup/noho/dsl/RecyclerNohoKt$nohoRow$5\n+ 2 UnitSelectionCoordinator.kt\ncom/squareup/ui/items/unit/UnitSelectionCoordinator\n+ 3 Views.kt\ncom/squareup/util/Views\n*L\n1#1,173:1\n125#2,8:174\n131#2:182\n130#2,8:183\n136#2:191\n134#2,7:192\n148#2:206\n1103#3,7:199\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000H\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0004\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\n\u0008\u0001\u0010\u0004\u0018\u0001*\u0002H\u00022\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u0002H\u00042\u0006\u0010\u0008\u001a\u00020\tH\n\u00a2\u0006\u0004\u0008\n\u0010\u000b\u00a8\u0006\r"
    }
    d2 = {
        "<anonymous>",
        "",
        "I",
        "",
        "S",
        "<anonymous parameter 0>",
        "",
        "item",
        "row",
        "Lcom/squareup/noho/NohoCheckableRow;",
        "invoke",
        "(ILjava/lang/Object;Lcom/squareup/noho/NohoCheckableRow;)V",
        "com/squareup/noho/dsl/RecyclerNohoKt$nohoRow$5",
        "com/squareup/ui/items/unit/UnitSelectionCoordinator$$special$$inlined$nohoRow$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/items/unit/UnitSelectionCoordinator;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/items/unit/UnitSelectionCoordinator;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$createRecycler$$inlined$adopt$lambda$1;->this$0:Lcom/squareup/ui/items/unit/UnitSelectionCoordinator;

    const/4 p1, 0x3

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    check-cast p3, Lcom/squareup/noho/NohoCheckableRow;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$createRecycler$$inlined$adopt$lambda$1;->invoke(ILjava/lang/Object;Lcom/squareup/noho/NohoCheckableRow;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(ILjava/lang/Object;Lcom/squareup/noho/NohoCheckableRow;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType;",
            "Lcom/squareup/noho/NohoCheckableRow;",
            ")V"
        }
    .end annotation

    const-string p1, "item"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "row"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 100
    check-cast p2, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType;

    .line 174
    invoke-virtual {p2}, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType;->getScreen()Lcom/squareup/ui/items/unit/UnitSelectionScreen;

    move-result-object p1

    .line 176
    instance-of v0, p2, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType$PerItem;

    if-eqz v0, :cond_0

    invoke-virtual {p3}, Lcom/squareup/noho/NohoCheckableRow;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/squareup/edititem/R$string;->edit_item_unit_type_default:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    goto :goto_0

    .line 177
    :cond_0
    instance-of v0, p2, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType$PerService;

    if-eqz v0, :cond_1

    invoke-virtual {p3}, Lcom/squareup/noho/NohoCheckableRow;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/squareup/edititem/R$string;->edit_service_unit_type_default:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    goto :goto_0

    .line 178
    :cond_1
    instance-of v0, p2, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType$PerUnit;

    const-string/jumbo v1, "unit_name"

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$createRecycler$$inlined$adopt$lambda$1;->this$0:Lcom/squareup/ui/items/unit/UnitSelectionCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator;->access$getRes$p(Lcom/squareup/ui/items/unit/UnitSelectionCoordinator;)Lcom/squareup/util/Res;

    move-result-object v0

    sget v2, Lcom/squareup/edititem/R$string;->edit_item_unit_type_value:I

    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 183
    move-object v2, p2

    check-cast v2, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType$PerUnit;

    invoke-virtual {v2}, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType$PerUnit;->getSelectableUnit()Lcom/squareup/items/unit/SelectableUnit;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/items/unit/SelectableUnit;->getName()Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1, v3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 182
    invoke-virtual {v2}, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType$PerUnit;->getSelectableUnit()Lcom/squareup/items/unit/SelectableUnit;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/items/unit/SelectableUnit;->getDisplayPrecision()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    const-string v2, "precision"

    invoke-virtual {v0, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 181
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0

    .line 186
    :cond_2
    instance-of v0, p2, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType$PerDefaultStandardUnit;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$createRecycler$$inlined$adopt$lambda$1;->this$0:Lcom/squareup/ui/items/unit/UnitSelectionCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator;->access$getRes$p(Lcom/squareup/ui/items/unit/UnitSelectionCoordinator;)Lcom/squareup/util/Res;

    move-result-object v0

    .line 192
    sget v2, Lcom/squareup/edititem/R$string;->edit_item_unit_type_default_standard_unit_value:I

    .line 186
    invoke-interface {v0, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 191
    move-object v2, p2

    check-cast v2, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType$PerDefaultStandardUnit;

    invoke-virtual {v2}, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType$PerDefaultStandardUnit;->getLocalizedUnitName()Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 190
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    :goto_0
    invoke-virtual {p3, v0}, Lcom/squareup/noho/NohoCheckableRow;->setLabel(Ljava/lang/CharSequence;)V

    .line 197
    invoke-virtual {p1}, Lcom/squareup/ui/items/unit/UnitSelectionScreen;->getState()Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/items/unit/AssignUnitToVariationState$SelectUnit;->getSelectedUnitId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    invoke-virtual {p3, v0}, Lcom/squareup/noho/NohoCheckableRow;->setChecked(Z)V

    .line 198
    check-cast p3, Landroid/view/View;

    .line 199
    new-instance v0, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$createRecycler$$inlined$adopt$lambda$1$1;

    invoke-direct {v0, p2, p1}, Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$createRecycler$$inlined$adopt$lambda$1$1;-><init>(Lcom/squareup/ui/items/unit/UnitSelectionCoordinator$UnitType;Lcom/squareup/ui/items/unit/UnitSelectionScreen;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p3, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    .line 190
    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method
