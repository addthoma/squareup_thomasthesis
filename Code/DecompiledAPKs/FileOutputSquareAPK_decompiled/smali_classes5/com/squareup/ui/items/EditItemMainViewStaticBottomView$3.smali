.class Lcom/squareup/ui/items/EditItemMainViewStaticBottomView$3;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "EditItemMainViewStaticBottomView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->setAlwaysEditableContentOnBottom(Ljava/util/List;Ljava/util/List;Ljava/lang/String;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;

.field final synthetic val$modifier:Lcom/squareup/ui/items/EditItemState$ModifierState;

.field final synthetic val$row:Lcom/squareup/ui/CheckBoxListRow;


# direct methods
.method constructor <init>(Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;Lcom/squareup/ui/CheckBoxListRow;Lcom/squareup/ui/items/EditItemState$ModifierState;)V
    .locals 0

    .line 220
    iput-object p1, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView$3;->this$0:Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;

    iput-object p2, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView$3;->val$row:Lcom/squareup/ui/CheckBoxListRow;

    iput-object p3, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView$3;->val$modifier:Lcom/squareup/ui/items/EditItemState$ModifierState;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 2

    .line 222
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView$3;->val$row:Lcom/squareup/ui/CheckBoxListRow;

    invoke-virtual {p1}, Lcom/squareup/ui/CheckBoxListRow;->toggle()V

    .line 223
    iget-object p1, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView$3;->this$0:Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;

    iget-object p1, p1, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView;->presenter:Lcom/squareup/ui/items/EditItemMainPresenter;

    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView$3;->val$modifier:Lcom/squareup/ui/items/EditItemState$ModifierState;

    iget-object v0, v0, Lcom/squareup/ui/items/EditItemState$ModifierState;->id:Ljava/lang/String;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemMainViewStaticBottomView$3;->val$row:Lcom/squareup/ui/CheckBoxListRow;

    invoke-virtual {v1}, Lcom/squareup/ui/CheckBoxListRow;->isChecked()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/squareup/ui/items/EditItemMainPresenter;->modifierCheckChanged(Ljava/lang/String;Z)V

    return-void
.end method
