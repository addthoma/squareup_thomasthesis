.class public abstract Lcom/squareup/ui/items/DetailSearchableListRow;
.super Ljava/lang/Object;
.source "DetailSearchableListRow.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/DetailSearchableListRow$DetailSearchableObjectRow;,
        Lcom/squareup/ui/items/DetailSearchableListRow$CreateButtonRow;,
        Lcom/squareup/ui/items/DetailSearchableListRow$FooterRow;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0003\u000f\u0010\u0011B\u001f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008R\u0014\u0010\u0006\u001a\u00020\u0007X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0014\u0010\u0004\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000e\u0082\u0001\u0003\u0012\u0013\u0014\u00a8\u0006\u0015"
    }
    d2 = {
        "Lcom/squareup/ui/items/DetailSearchableListRow;",
        "",
        "screen",
        "Lcom/squareup/ui/items/DetailSearchableListScreen;",
        "res",
        "Lcom/squareup/util/Res;",
        "id",
        "",
        "(Lcom/squareup/ui/items/DetailSearchableListScreen;Lcom/squareup/util/Res;Ljava/lang/String;)V",
        "getId",
        "()Ljava/lang/String;",
        "getRes",
        "()Lcom/squareup/util/Res;",
        "getScreen",
        "()Lcom/squareup/ui/items/DetailSearchableListScreen;",
        "CreateButtonRow",
        "DetailSearchableObjectRow",
        "FooterRow",
        "Lcom/squareup/ui/items/DetailSearchableListRow$DetailSearchableObjectRow;",
        "Lcom/squareup/ui/items/DetailSearchableListRow$CreateButtonRow;",
        "Lcom/squareup/ui/items/DetailSearchableListRow$FooterRow;",
        "items-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final id:Ljava/lang/String;

.field private final res:Lcom/squareup/util/Res;

.field private final screen:Lcom/squareup/ui/items/DetailSearchableListScreen;


# direct methods
.method private constructor <init>(Lcom/squareup/ui/items/DetailSearchableListScreen;Lcom/squareup/util/Res;Ljava/lang/String;)V
    .locals 0

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListRow;->screen:Lcom/squareup/ui/items/DetailSearchableListScreen;

    iput-object p2, p0, Lcom/squareup/ui/items/DetailSearchableListRow;->res:Lcom/squareup/util/Res;

    iput-object p3, p0, Lcom/squareup/ui/items/DetailSearchableListRow;->id:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/ui/items/DetailSearchableListScreen;Lcom/squareup/util/Res;Ljava/lang/String;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 47
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/ui/items/DetailSearchableListRow;-><init>(Lcom/squareup/ui/items/DetailSearchableListScreen;Lcom/squareup/util/Res;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public getId()Ljava/lang/String;
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListRow;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getRes()Lcom/squareup/util/Res;
    .locals 1

    .line 49
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListRow;->res:Lcom/squareup/util/Res;

    return-object v0
.end method

.method public getScreen()Lcom/squareup/ui/items/DetailSearchableListScreen;
    .locals 1

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListRow;->screen:Lcom/squareup/ui/items/DetailSearchableListScreen;

    return-object v0
.end method
