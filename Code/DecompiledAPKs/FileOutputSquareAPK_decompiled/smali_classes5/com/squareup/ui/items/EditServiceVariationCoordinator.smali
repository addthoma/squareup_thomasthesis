.class public final Lcom/squareup/ui/items/EditServiceVariationCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "EditServiceVariationCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditServiceVariationCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditServiceVariationCoordinator.kt\ncom/squareup/ui/items/EditServiceVariationCoordinator\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,380:1\n1103#2,7:381\n1103#2,7:388\n1103#2,7:395\n1103#2,7:402\n1103#2,7:409\n1103#2,7:416\n1103#2,7:423\n1103#2,7:430\n1103#2,7:437\n*E\n*S KotlinDebug\n*F\n+ 1 EditServiceVariationCoordinator.kt\ncom/squareup/ui/items/EditServiceVariationCoordinator\n*L\n216#1,7:381\n229#1,7:388\n269#1,7:395\n278#1,7:402\n334#1,7:409\n337#1,7:416\n340#1,7:423\n371#1,7:430\n373#1,7:437\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00a4\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0006\u0018\u00002\u00020\u0001BU\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\u0006\u0010\u0011\u001a\u00020\u0012\u0012\u0006\u0010\u0013\u001a\u00020\u0014\u00a2\u0006\u0002\u0010\u0015J\u0010\u00101\u001a\u0002022\u0006\u00103\u001a\u000204H\u0016J\u0010\u00105\u001a\u0002022\u0006\u00106\u001a\u000207H\u0002J\u0018\u00108\u001a\u0002022\u0006\u00103\u001a\u0002042\u0006\u00106\u001a\u000207H\u0002J\u0018\u00109\u001a\u0002022\u0006\u00103\u001a\u0002042\u0006\u00106\u001a\u000207H\u0002J\u0010\u0010:\u001a\u0002022\u0006\u0010;\u001a\u00020<H\u0002J\u0010\u0010=\u001a\u00020>2\u0006\u00106\u001a\u000207H\u0002J\u0010\u0010?\u001a\u0002022\u0006\u0010@\u001a\u00020>H\u0002J\u0010\u0010A\u001a\u0002022\u0006\u00106\u001a\u000207H\u0002J\u0010\u0010B\u001a\u0002022\u0006\u00106\u001a\u000207H\u0002J\u000c\u0010C\u001a\u000202*\u000204H\u0002R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u0019X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u001cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u001eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001f\u001a\u00020 X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010!\u001a\u00020\u001eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\"\u001a\u00020\u0019X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010#\u001a\u00020$X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010%\u001a\u00020\u001cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010&\u001a\u00020\u0019X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\'\u001a\u00020\u0019X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010(\u001a\u00020\u001cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010)\u001a\u00020\u0019X\u0082.\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010*\u001a\u00020+X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010,\u001a\u00020\u001eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010-\u001a\u00020\u0019X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010.\u001a\u00020/X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u00100\u001a\u00020\u0019X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006D"
    }
    d2 = {
        "Lcom/squareup/ui/items/EditServiceVariationCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "priceLocaleHelper",
        "Lcom/squareup/money/PriceLocaleHelper;",
        "perUnitFormatter",
        "Lcom/squareup/quantity/PerUnitFormatter;",
        "moneyFormatter",
        "Lcom/squareup/text/Formatter;",
        "Lcom/squareup/protos/common/Money;",
        "durationFormatter",
        "Lcom/squareup/text/DurationFormatter;",
        "editServiceVariationRunner",
        "Lcom/squareup/ui/items/EditServiceVariationScreen$EditServiceVariationRunner;",
        "catalogIntegrationController",
        "Lcom/squareup/catalogapi/CatalogIntegrationController;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "res",
        "Lcom/squareup/util/Res;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "(Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/DurationFormatter;Lcom/squareup/ui/items/EditServiceVariationScreen$EditServiceVariationRunner;Lcom/squareup/catalogapi/CatalogIntegrationController;Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/ActionBarView;",
        "afterAppointmentRow",
        "Lcom/squareup/noho/NohoRow;",
        "assignedEmployeesRow",
        "bookableByCustomerToggle",
        "Lcom/squareup/noho/NohoCheckableRow;",
        "cancellationFeeEditText",
        "Lcom/squareup/noho/NohoEditText;",
        "displayPriceContainer",
        "Landroid/view/ViewGroup;",
        "displayPriceEditText",
        "durationRow",
        "extraTimeHelperText",
        "Lcom/squareup/noho/NohoMessageView;",
        "extraTimeToggle",
        "finalDurationRow",
        "gapDurationRow",
        "gapTimeToggle",
        "initialDurationRow",
        "nameEditText",
        "Landroid/widget/EditText;",
        "priceEditText",
        "priceTypeRow",
        "removeButton",
        "Lcom/squareup/marketfont/MarketButton;",
        "unitTypeSelector",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "configureAPoSInputFields",
        "screenData",
        "Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;",
        "configureActionBar",
        "configureInputFields",
        "setGapTimeRowsVisibility",
        "visibility",
        "",
        "shouldEnabledPrimaryButton",
        "",
        "showAPoSFields",
        "show",
        "updateAPoSFields",
        "updateFields",
        "bindViews",
        "edit-item_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field private afterAppointmentRow:Lcom/squareup/noho/NohoRow;

.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private assignedEmployeesRow:Lcom/squareup/noho/NohoRow;

.field private bookableByCustomerToggle:Lcom/squareup/noho/NohoCheckableRow;

.field private cancellationFeeEditText:Lcom/squareup/noho/NohoEditText;

.field private final catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

.field private displayPriceContainer:Landroid/view/ViewGroup;

.field private displayPriceEditText:Lcom/squareup/noho/NohoEditText;

.field private final durationFormatter:Lcom/squareup/text/DurationFormatter;

.field private durationRow:Lcom/squareup/noho/NohoRow;

.field private final editServiceVariationRunner:Lcom/squareup/ui/items/EditServiceVariationScreen$EditServiceVariationRunner;

.field private extraTimeHelperText:Lcom/squareup/noho/NohoMessageView;

.field private extraTimeToggle:Lcom/squareup/noho/NohoCheckableRow;

.field private final features:Lcom/squareup/settings/server/Features;

.field private finalDurationRow:Lcom/squareup/noho/NohoRow;

.field private gapDurationRow:Lcom/squareup/noho/NohoRow;

.field private gapTimeToggle:Lcom/squareup/noho/NohoCheckableRow;

.field private initialDurationRow:Lcom/squareup/noho/NohoRow;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private nameEditText:Landroid/widget/EditText;

.field private final perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

.field private priceEditText:Lcom/squareup/noho/NohoEditText;

.field private final priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

.field private priceTypeRow:Lcom/squareup/noho/NohoRow;

.field private removeButton:Lcom/squareup/marketfont/MarketButton;

.field private final res:Lcom/squareup/util/Res;

.field private unitTypeSelector:Lcom/squareup/noho/NohoRow;


# direct methods
.method public constructor <init>(Lcom/squareup/money/PriceLocaleHelper;Lcom/squareup/quantity/PerUnitFormatter;Lcom/squareup/text/Formatter;Lcom/squareup/text/DurationFormatter;Lcom/squareup/ui/items/EditServiceVariationScreen$EditServiceVariationRunner;Lcom/squareup/catalogapi/CatalogIntegrationController;Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Res;Lcom/squareup/settings/server/Features;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/money/PriceLocaleHelper;",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/text/DurationFormatter;",
            "Lcom/squareup/ui/items/EditServiceVariationScreen$EditServiceVariationRunner;",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/settings/server/Features;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "priceLocaleHelper"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "perUnitFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyFormatter"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "durationFormatter"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "editServiceVariationRunner"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "catalogIntegrationController"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    iput-object p2, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    iput-object p3, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    iput-object p4, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->durationFormatter:Lcom/squareup/text/DurationFormatter;

    iput-object p5, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->editServiceVariationRunner:Lcom/squareup/ui/items/EditServiceVariationScreen$EditServiceVariationRunner;

    iput-object p6, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

    iput-object p7, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->analytics:Lcom/squareup/analytics/Analytics;

    iput-object p8, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->res:Lcom/squareup/util/Res;

    iput-object p9, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method

.method public static final synthetic access$configureAPoSInputFields(Lcom/squareup/ui/items/EditServiceVariationCoordinator;Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;)V
    .locals 0

    .line 51
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->configureAPoSInputFields(Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;)V

    return-void
.end method

.method public static final synthetic access$configureActionBar(Lcom/squareup/ui/items/EditServiceVariationCoordinator;Landroid/view/View;Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;)V
    .locals 0

    .line 51
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->configureActionBar(Landroid/view/View;Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;)V

    return-void
.end method

.method public static final synthetic access$configureInputFields(Lcom/squareup/ui/items/EditServiceVariationCoordinator;Landroid/view/View;Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;)V
    .locals 0

    .line 51
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->configureInputFields(Landroid/view/View;Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;)V

    return-void
.end method

.method public static final synthetic access$getActionBar$p(Lcom/squareup/ui/items/EditServiceVariationCoordinator;)Lcom/squareup/marin/widgets/ActionBarView;
    .locals 1

    .line 51
    iget-object p0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    if-nez p0, :cond_0

    const-string v0, "actionBar"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getAnalytics$p(Lcom/squareup/ui/items/EditServiceVariationCoordinator;)Lcom/squareup/analytics/Analytics;
    .locals 0

    .line 51
    iget-object p0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->analytics:Lcom/squareup/analytics/Analytics;

    return-object p0
.end method

.method public static final synthetic access$getCatalogIntegrationController$p(Lcom/squareup/ui/items/EditServiceVariationCoordinator;)Lcom/squareup/catalogapi/CatalogIntegrationController;
    .locals 0

    .line 51
    iget-object p0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

    return-object p0
.end method

.method public static final synthetic access$getEditServiceVariationRunner$p(Lcom/squareup/ui/items/EditServiceVariationCoordinator;)Lcom/squareup/ui/items/EditServiceVariationScreen$EditServiceVariationRunner;
    .locals 0

    .line 51
    iget-object p0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->editServiceVariationRunner:Lcom/squareup/ui/items/EditServiceVariationScreen$EditServiceVariationRunner;

    return-object p0
.end method

.method public static final synthetic access$setActionBar$p(Lcom/squareup/ui/items/EditServiceVariationCoordinator;Lcom/squareup/marin/widgets/ActionBarView;)V
    .locals 0

    .line 51
    iput-object p1, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    return-void
.end method

.method public static final synthetic access$shouldEnabledPrimaryButton(Lcom/squareup/ui/items/EditServiceVariationCoordinator;Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;)Z
    .locals 0

    .line 51
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->shouldEnabledPrimaryButton(Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;)Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$showAPoSFields(Lcom/squareup/ui/items/EditServiceVariationCoordinator;Z)V
    .locals 0

    .line 51
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->showAPoSFields(Z)V

    return-void
.end method

.method public static final synthetic access$updateAPoSFields(Lcom/squareup/ui/items/EditServiceVariationCoordinator;Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;)V
    .locals 0

    .line 51
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->updateAPoSFields(Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;)V

    return-void
.end method

.method public static final synthetic access$updateFields(Lcom/squareup/ui/items/EditServiceVariationCoordinator;Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;)V
    .locals 0

    .line 51
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->updateFields(Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;)V

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 1

    .line 118
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    .line 119
    sget v0, Lcom/squareup/edititem/R$id;->edit_service_variation_name_input_field:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->nameEditText:Landroid/widget/EditText;

    .line 120
    sget v0, Lcom/squareup/edititem/R$id;->edit_service_variaton_price_type_row:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoRow;

    iput-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->priceTypeRow:Lcom/squareup/noho/NohoRow;

    .line 121
    sget v0, Lcom/squareup/edititem/R$id;->edit_service_variation_price_input_field:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoEditText;

    iput-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->priceEditText:Lcom/squareup/noho/NohoEditText;

    .line 122
    sget v0, Lcom/squareup/edititem/R$id;->edit_service_variation_duration_row:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoRow;

    iput-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->durationRow:Lcom/squareup/noho/NohoRow;

    .line 123
    sget v0, Lcom/squareup/edititem/R$id;->edit_service_variation_cancellation_fee_row:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoEditText;

    iput-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->cancellationFeeEditText:Lcom/squareup/noho/NohoEditText;

    .line 124
    sget v0, Lcom/squareup/edititem/R$id;->edit_service_variation_gap_time_toggle:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoCheckableRow;

    iput-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->gapTimeToggle:Lcom/squareup/noho/NohoCheckableRow;

    .line 125
    sget v0, Lcom/squareup/edititem/R$id;->edit_service_variation_initial_duration:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoRow;

    iput-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->initialDurationRow:Lcom/squareup/noho/NohoRow;

    .line 126
    sget v0, Lcom/squareup/edititem/R$id;->edit_service_variation_gap_duration:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoRow;

    iput-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->gapDurationRow:Lcom/squareup/noho/NohoRow;

    .line 127
    sget v0, Lcom/squareup/edititem/R$id;->edit_service_variation_final_duration:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoRow;

    iput-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->finalDurationRow:Lcom/squareup/noho/NohoRow;

    .line 128
    sget v0, Lcom/squareup/edititem/R$id;->edit_service_variation_duration_extra_time_toggle:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoCheckableRow;

    iput-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->extraTimeToggle:Lcom/squareup/noho/NohoCheckableRow;

    .line 129
    sget v0, Lcom/squareup/edititem/R$id;->edit_service_variation_after_appointment_row:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoRow;

    iput-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->afterAppointmentRow:Lcom/squareup/noho/NohoRow;

    .line 130
    sget v0, Lcom/squareup/edititem/R$id;->edit_service_variation_extra_time_helper_text:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoMessageView;

    iput-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->extraTimeHelperText:Lcom/squareup/noho/NohoMessageView;

    .line 131
    sget v0, Lcom/squareup/edititem/R$id;->edit_service_variation_bookable_by_customers_online:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoCheckableRow;

    iput-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->bookableByCustomerToggle:Lcom/squareup/noho/NohoCheckableRow;

    .line 132
    sget v0, Lcom/squareup/edititem/R$id;->edit_service_variation_assigned_employees:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoRow;

    iput-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->assignedEmployeesRow:Lcom/squareup/noho/NohoRow;

    .line 133
    sget v0, Lcom/squareup/edititem/R$id;->edit_service_variation_display_price_container:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->displayPriceContainer:Landroid/view/ViewGroup;

    .line 134
    sget v0, Lcom/squareup/edititem/R$id;->edit_service_variation_display_price_row:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoEditText;

    iput-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->displayPriceEditText:Lcom/squareup/noho/NohoEditText;

    .line 135
    sget v0, Lcom/squareup/edititem/R$id;->edit_service_variation_remove_variation_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketButton;

    iput-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->removeButton:Lcom/squareup/marketfont/MarketButton;

    .line 136
    sget v0, Lcom/squareup/edititem/R$id;->edit_service_variation_unit_type:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/noho/NohoRow;

    iput-object p1, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->unitTypeSelector:Lcom/squareup/noho/NohoRow;

    return-void
.end method

.method private final configureAPoSInputFields(Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;)V
    .locals 9

    const/4 v0, 0x1

    .line 235
    invoke-direct {p0, v0}, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->showAPoSFields(Z)V

    .line 237
    invoke-virtual {p1}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;->getVariablePriceType()Z

    move-result v1

    const-string v2, "cancellationFeeEditText"

    if-nez v1, :cond_1

    .line 238
    iget-object v1, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->cancellationFeeEditText:Lcom/squareup/noho/NohoEditText;

    if-nez v1, :cond_0

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const/16 v3, 0xd

    invoke-virtual {v1, v3}, Lcom/squareup/noho/NohoEditText;->setBorderEdges(I)V

    goto :goto_0

    .line 240
    :cond_1
    iget-object v1, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->cancellationFeeEditText:Lcom/squareup/noho/NohoEditText;

    if-nez v1, :cond_2

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    const/16 v3, 0xf

    invoke-virtual {v1, v3}, Lcom/squareup/noho/NohoEditText;->setBorderEdges(I)V

    .line 243
    :goto_0
    iget-object v1, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->cancellationFeeEditText:Lcom/squareup/noho/NohoEditText;

    if-nez v1, :cond_3

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    iget-object v3, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;->getCancellationPrice()Lcom/squareup/protos/common/Money;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/squareup/noho/NohoEditText;->setText(Ljava/lang/CharSequence;)V

    .line 244
    iget-object v1, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    .line 245
    iget-object v3, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->cancellationFeeEditText:Lcom/squareup/noho/NohoEditText;

    if-nez v3, :cond_4

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    check-cast v3, Lcom/squareup/text/HasSelectableText;

    invoke-virtual {v1, v3}, Lcom/squareup/money/PriceLocaleHelper;->configure(Lcom/squareup/text/HasSelectableText;)Lcom/squareup/text/ScrubbingTextWatcher;

    move-result-object v1

    .line 247
    new-instance v3, Lcom/squareup/money/MaxMoneyScrubber;

    .line 248
    iget-object v4, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    check-cast v4, Lcom/squareup/money/MoneyExtractor;

    iget-object v5, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 249
    iget-object v6, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->editServiceVariationRunner:Lcom/squareup/ui/items/EditServiceVariationScreen$EditServiceVariationRunner;

    invoke-interface {v6}, Lcom/squareup/ui/items/EditServiceVariationScreen$EditServiceVariationRunner;->maxCancellationFeeMoney()Lcom/squareup/protos/common/Money;

    move-result-object v6

    .line 247
    invoke-direct {v3, v4, v5, v6}, Lcom/squareup/money/MaxMoneyScrubber;-><init>(Lcom/squareup/money/MoneyExtractor;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/Money;)V

    check-cast v3, Lcom/squareup/text/Scrubber;

    .line 246
    invoke-virtual {v1, v3}, Lcom/squareup/text/ScrubbingTextWatcher;->addScrubber(Lcom/squareup/text/Scrubber;)V

    .line 252
    iget-object v1, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    iget-object v3, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->cancellationFeeEditText:Lcom/squareup/noho/NohoEditText;

    if-nez v3, :cond_5

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    check-cast v3, Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Lcom/squareup/money/PriceLocaleHelper;->setHintToZeroMoney(Landroid/widget/TextView;)V

    .line 253
    iget-object v1, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->cancellationFeeEditText:Lcom/squareup/noho/NohoEditText;

    if-nez v1, :cond_6

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    new-instance v2, Lcom/squareup/ui/items/EditServiceVariationCoordinator$configureAPoSInputFields$1;

    invoke-direct {v2, p0}, Lcom/squareup/ui/items/EditServiceVariationCoordinator$configureAPoSInputFields$1;-><init>(Lcom/squareup/ui/items/EditServiceVariationCoordinator;)V

    check-cast v2, Landroid/text/TextWatcher;

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 259
    iget-object v1, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->gapTimeToggle:Lcom/squareup/noho/NohoCheckableRow;

    const-string v2, "gapTimeToggle"

    if-nez v1, :cond_7

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    invoke-virtual {p1}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;->getGapTimeInfo()Lcom/squareup/intermission/GapTimeInfo;

    move-result-object v3

    const/4 v4, 0x0

    if-eqz v3, :cond_8

    const/4 v3, 0x1

    goto :goto_1

    :cond_8
    const/4 v3, 0x0

    :goto_1
    invoke-virtual {v1, v3}, Lcom/squareup/noho/NohoCheckableRow;->setChecked(Z)V

    .line 260
    iget-object v1, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->gapTimeToggle:Lcom/squareup/noho/NohoCheckableRow;

    if-nez v1, :cond_9

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_9
    new-instance v2, Lcom/squareup/ui/items/EditServiceVariationCoordinator$configureAPoSInputFields$2;

    invoke-direct {v2, p0}, Lcom/squareup/ui/items/EditServiceVariationCoordinator$configureAPoSInputFields$2;-><init>(Lcom/squareup/ui/items/EditServiceVariationCoordinator;)V

    check-cast v2, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoCheckableRow;->onCheckedChange(Lkotlin/jvm/functions/Function2;)V

    .line 264
    iget-object v1, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->extraTimeToggle:Lcom/squareup/noho/NohoCheckableRow;

    const-string v2, "extraTimeToggle"

    if-nez v1, :cond_a

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_a
    invoke-virtual {p1}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;->getBlockedExtraTime()J

    move-result-wide v5

    const-wide/16 v7, 0x0

    cmp-long v3, v5, v7

    if-eqz v3, :cond_b

    goto :goto_2

    :cond_b
    const/4 v0, 0x0

    :goto_2
    invoke-virtual {v1, v0}, Lcom/squareup/noho/NohoCheckableRow;->setChecked(Z)V

    .line 265
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->extraTimeToggle:Lcom/squareup/noho/NohoCheckableRow;

    if-nez v0, :cond_c

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_c
    new-instance v1, Lcom/squareup/ui/items/EditServiceVariationCoordinator$configureAPoSInputFields$3;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/EditServiceVariationCoordinator$configureAPoSInputFields$3;-><init>(Lcom/squareup/ui/items/EditServiceVariationCoordinator;)V

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoCheckableRow;->setOnCheckedChange(Lkotlin/jvm/functions/Function2;)V

    .line 269
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->afterAppointmentRow:Lcom/squareup/noho/NohoRow;

    if-nez v0, :cond_d

    const-string v1, "afterAppointmentRow"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_d
    check-cast v0, Landroid/view/View;

    .line 395
    new-instance v1, Lcom/squareup/ui/items/EditServiceVariationCoordinator$configureAPoSInputFields$$inlined$onClickDebounced$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/EditServiceVariationCoordinator$configureAPoSInputFields$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/ui/items/EditServiceVariationCoordinator;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 273
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->bookableByCustomerToggle:Lcom/squareup/noho/NohoCheckableRow;

    const-string v1, "bookableByCustomerToggle"

    if-nez v0, :cond_e

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_e
    invoke-virtual {p1}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;->getBookableByCustomer()Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoCheckableRow;->setChecked(Z)V

    .line 274
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->bookableByCustomerToggle:Lcom/squareup/noho/NohoCheckableRow;

    if-nez v0, :cond_f

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_f
    new-instance v1, Lcom/squareup/ui/items/EditServiceVariationCoordinator$configureAPoSInputFields$5;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/EditServiceVariationCoordinator$configureAPoSInputFields$5;-><init>(Lcom/squareup/ui/items/EditServiceVariationCoordinator;)V

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoCheckableRow;->setOnCheckedChange(Lkotlin/jvm/functions/Function2;)V

    .line 278
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->assignedEmployeesRow:Lcom/squareup/noho/NohoRow;

    if-nez v0, :cond_10

    const-string v1, "assignedEmployeesRow"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_10
    check-cast v0, Landroid/view/View;

    .line 402
    new-instance v1, Lcom/squareup/ui/items/EditServiceVariationCoordinator$configureAPoSInputFields$$inlined$onClickDebounced$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/EditServiceVariationCoordinator$configureAPoSInputFields$$inlined$onClickDebounced$2;-><init>(Lcom/squareup/ui/items/EditServiceVariationCoordinator;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 282
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->displayPriceEditText:Lcom/squareup/noho/NohoEditText;

    const-string v1, "displayPriceEditText"

    if-nez v0, :cond_11

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_11
    invoke-virtual {p1}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;->getPriceDescription()Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoEditText;->setText(Ljava/lang/CharSequence;)V

    .line 283
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->displayPriceEditText:Lcom/squareup/noho/NohoEditText;

    if-nez v0, :cond_12

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_12
    new-instance v1, Lcom/squareup/ui/items/EditServiceVariationCoordinator$configureAPoSInputFields$7;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/EditServiceVariationCoordinator$configureAPoSInputFields$7;-><init>(Lcom/squareup/ui/items/EditServiceVariationCoordinator;)V

    check-cast v1, Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 289
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->updateAPoSFields(Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;)V

    return-void
.end method

.method private final configureActionBar(Landroid/view/View;Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;)V
    .locals 5

    .line 155
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    if-nez v0, :cond_0

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    const-string v1, "actionBar.presenter"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 143
    new-instance v1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 145
    new-instance v2, Lcom/squareup/ui/items/EditServiceVariationCoordinator$configureActionBar$$inlined$apply$lambda$1;

    invoke-direct {v2, p0, p2, p1}, Lcom/squareup/ui/items/EditServiceVariationCoordinator$configureActionBar$$inlined$apply$lambda$1;-><init>(Lcom/squareup/ui/items/EditServiceVariationCoordinator;Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;Landroid/view/View;)V

    check-cast v2, Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 147
    sget-object v2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 148
    invoke-virtual {p2}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;->isNewVariation()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/squareup/edititem/R$string;->add_variation:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 149
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/squareup/edititem/R$string;->edit_variation:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 148
    :goto_0
    check-cast v3, Ljava/lang/CharSequence;

    .line 146
    invoke-virtual {v1, v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 151
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/squareup/common/strings/R$string;->done:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 152
    invoke-direct {p0, p2}, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->shouldEnabledPrimaryButton(Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 153
    new-instance v2, Lcom/squareup/ui/items/EditServiceVariationCoordinator$configureActionBar$$inlined$apply$lambda$2;

    invoke-direct {v2, p0, p2, p1}, Lcom/squareup/ui/items/EditServiceVariationCoordinator$configureActionBar$$inlined$apply$lambda$2;-><init>(Lcom/squareup/ui/items/EditServiceVariationCoordinator;Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;Landroid/view/View;)V

    check-cast v2, Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 155
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method private final configureInputFields(Landroid/view/View;Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;)V
    .locals 8

    .line 163
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->nameEditText:Landroid/widget/EditText;

    const-string v1, "nameEditText"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p2}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;->getName()Ljava/lang/String;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 164
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->nameEditText:Landroid/widget/EditText;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    new-instance v1, Lcom/squareup/ui/items/EditServiceVariationCoordinator$configureInputFields$1;

    invoke-direct {v1, p0, p2}, Lcom/squareup/ui/items/EditServiceVariationCoordinator$configureInputFields$1;-><init>(Lcom/squareup/ui/items/EditServiceVariationCoordinator;Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;)V

    check-cast v1, Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 173
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->priceTypeRow:Lcom/squareup/noho/NohoRow;

    const-string v1, "priceTypeRow"

    if-nez v0, :cond_2

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    new-instance v2, Lcom/squareup/ui/items/EditServiceVariationCoordinator$configureInputFields$2;

    invoke-direct {v2, p0}, Lcom/squareup/ui/items/EditServiceVariationCoordinator$configureInputFields$2;-><init>(Lcom/squareup/ui/items/EditServiceVariationCoordinator;)V

    check-cast v2, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 176
    invoke-virtual {p2}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;->getEnableUnitPricedServices()Z

    move-result v0

    const/4 v2, 0x0

    const/16 v3, 0x8

    const-string/jumbo v4, "unitTypeSelector"

    const-string v5, "priceEditText"

    if-eqz v0, :cond_6

    .line 177
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->unitTypeSelector:Lcom/squareup/noho/NohoRow;

    if-nez v0, :cond_3

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoRow;->setVisibility(I)V

    .line 178
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->priceTypeRow:Lcom/squareup/noho/NohoRow;

    if-nez v0, :cond_4

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {v0, v3}, Lcom/squareup/noho/NohoRow;->setVisibility(I)V

    .line 179
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->priceEditText:Lcom/squareup/noho/NohoEditText;

    if-nez v0, :cond_5

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoEditText;->setVisibility(I)V

    goto :goto_0

    .line 181
    :cond_6
    invoke-virtual {p2}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;->getVariablePriceType()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 182
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->unitTypeSelector:Lcom/squareup/noho/NohoRow;

    if-nez v0, :cond_7

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    invoke-virtual {v0, v3}, Lcom/squareup/noho/NohoRow;->setVisibility(I)V

    .line 183
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->priceTypeRow:Lcom/squareup/noho/NohoRow;

    if-nez v0, :cond_8

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_8
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/squareup/edititem/R$string;->edit_service_variable_price_type:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    .line 184
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->priceEditText:Lcom/squareup/noho/NohoEditText;

    if-nez v0, :cond_9

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_9
    invoke-virtual {v0, v3}, Lcom/squareup/noho/NohoEditText;->setVisibility(I)V

    goto :goto_0

    .line 187
    :cond_a
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->unitTypeSelector:Lcom/squareup/noho/NohoRow;

    if-nez v0, :cond_b

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_b
    invoke-virtual {v0, v3}, Lcom/squareup/noho/NohoRow;->setVisibility(I)V

    .line 188
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->priceTypeRow:Lcom/squareup/noho/NohoRow;

    if-nez v0, :cond_c

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_c
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v6, Lcom/squareup/edititem/R$string;->edit_service_fixed_price_type:I

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    .line 189
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->priceEditText:Lcom/squareup/noho/NohoEditText;

    if-nez v0, :cond_d

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_d
    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoEditText;->setVisibility(I)V

    .line 194
    :goto_0
    sget-object v0, Lcom/squareup/quantity/UnitDisplayData;->Companion:Lcom/squareup/quantity/UnitDisplayData$Companion;

    iget-object v1, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->res:Lcom/squareup/util/Res;

    invoke-virtual {p2}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;->getMeasurementUnit()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/quantity/UnitDisplayData$Companion;->fromCatalogMeasurementUnit(Lcom/squareup/util/Res;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)Lcom/squareup/quantity/UnitDisplayData;

    move-result-object v0

    .line 196
    invoke-virtual {p2}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;->getEnableUnitPricedServices()Z

    move-result v1

    if-eqz v1, :cond_e

    invoke-virtual {v0}, Lcom/squareup/quantity/UnitDisplayData;->getUnitAbbreviation()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_e
    const-string v1, ""

    .line 200
    :goto_1
    iget-object v2, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->priceEditText:Lcom/squareup/noho/NohoEditText;

    if-nez v2, :cond_f

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_f
    iget-object v6, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    invoke-virtual {p2}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;->getPrice()Lcom/squareup/protos/common/Money;

    move-result-object v7

    invoke-virtual {v6, v7, v1}, Lcom/squareup/quantity/PerUnitFormatter;->format(Lcom/squareup/protos/common/Money;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/squareup/noho/NohoEditText;->setText(Ljava/lang/CharSequence;)V

    .line 201
    iget-object v2, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    iget-object v6, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->priceEditText:Lcom/squareup/noho/NohoEditText;

    if-nez v6, :cond_10

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_10
    check-cast v6, Lcom/squareup/text/HasSelectableText;

    sget-object v7, Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;->BLANKABLE:Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;

    invoke-virtual {v2, v6, v7, v1}, Lcom/squareup/money/PriceLocaleHelper;->configure(Lcom/squareup/text/HasSelectableText;Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;Ljava/lang/String;)Lcom/squareup/text/ScrubbingTextWatcher;

    .line 202
    iget-object v2, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    iget-object v6, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->priceEditText:Lcom/squareup/noho/NohoEditText;

    if-nez v6, :cond_11

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_11
    check-cast v6, Landroid/widget/TextView;

    invoke-virtual {v2, v6, v1}, Lcom/squareup/money/PriceLocaleHelper;->setHintToZeroMoney(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 204
    iget-object v1, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->priceEditText:Lcom/squareup/noho/NohoEditText;

    if-nez v1, :cond_12

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_12
    new-instance v2, Lcom/squareup/ui/items/EditServiceVariationCoordinator$configureInputFields$3;

    invoke-direct {v2, p0}, Lcom/squareup/ui/items/EditServiceVariationCoordinator$configureInputFields$3;-><init>(Lcom/squareup/ui/items/EditServiceVariationCoordinator;)V

    check-cast v2, Landroid/text/TextWatcher;

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 210
    invoke-direct {p0, p2}, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->updateFields(Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;)V

    .line 213
    invoke-virtual {p2}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;->isNewVariation()Z

    move-result p2

    const-string v1, "removeButton"

    if-eqz p2, :cond_14

    .line 214
    iget-object p2, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->removeButton:Lcom/squareup/marketfont/MarketButton;

    if-nez p2, :cond_13

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_13
    invoke-virtual {p2, v3}, Lcom/squareup/marketfont/MarketButton;->setVisibility(I)V

    goto :goto_2

    .line 216
    :cond_14
    iget-object p2, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->removeButton:Lcom/squareup/marketfont/MarketButton;

    if-nez p2, :cond_15

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_15
    check-cast p2, Landroid/view/View;

    .line 381
    new-instance v1, Lcom/squareup/ui/items/EditServiceVariationCoordinator$configureInputFields$$inlined$onClickDebounced$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/EditServiceVariationCoordinator$configureInputFields$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/ui/items/EditServiceVariationCoordinator;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {p2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 222
    :goto_2
    iget-object p2, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->unitTypeSelector:Lcom/squareup/noho/NohoRow;

    if-nez p2, :cond_16

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_16
    invoke-virtual {v0}, Lcom/squareup/quantity/UnitDisplayData;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_17

    .line 223
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v0, Lcom/squareup/edititem/R$string;->edit_service_unit_type_default:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    goto :goto_3

    .line 225
    :cond_17
    iget-object p1, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/edititem/R$string;->edit_item_unit_type_value:I

    invoke-interface {p1, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 226
    invoke-virtual {v0}, Lcom/squareup/quantity/UnitDisplayData;->getUnitName()Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    const-string/jumbo v2, "unit_name"

    invoke-virtual {p1, v2, v1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 227
    iget-object v1, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->res:Lcom/squareup/util/Res;

    invoke-virtual {v0, v1}, Lcom/squareup/quantity/UnitDisplayData;->getQuantityPrecisionHint(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    const-string v1, "precision"

    invoke-virtual {p1, v1, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 228
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 222
    :goto_3
    invoke-virtual {p2, p1}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    .line 229
    iget-object p1, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->unitTypeSelector:Lcom/squareup/noho/NohoRow;

    if-nez p1, :cond_18

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_18
    check-cast p1, Landroid/view/View;

    .line 388
    new-instance p2, Lcom/squareup/ui/items/EditServiceVariationCoordinator$configureInputFields$$inlined$onClickDebounced$2;

    invoke-direct {p2, p0}, Lcom/squareup/ui/items/EditServiceVariationCoordinator$configureInputFields$$inlined$onClickDebounced$2;-><init>(Lcom/squareup/ui/items/EditServiceVariationCoordinator;)V

    check-cast p2, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private final setGapTimeRowsVisibility(I)V
    .locals 2

    .line 366
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->initialDurationRow:Lcom/squareup/noho/NohoRow;

    if-nez v0, :cond_0

    const-string v1, "initialDurationRow"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoRow;->setVisibility(I)V

    .line 367
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->gapDurationRow:Lcom/squareup/noho/NohoRow;

    if-nez v0, :cond_1

    const-string v1, "gapDurationRow"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoRow;->setVisibility(I)V

    .line 368
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->finalDurationRow:Lcom/squareup/noho/NohoRow;

    if-nez v0, :cond_2

    const-string v1, "finalDurationRow"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoRow;->setVisibility(I)V

    const-string v0, "durationRow"

    if-nez p1, :cond_4

    .line 371
    iget-object p1, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->durationRow:Lcom/squareup/noho/NohoRow;

    if-nez p1, :cond_3

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    check-cast p1, Landroid/view/View;

    .line 430
    new-instance v0, Lcom/squareup/ui/items/EditServiceVariationCoordinator$setGapTimeRowsVisibility$$inlined$onClickDebounced$1;

    invoke-direct {v0}, Lcom/squareup/ui/items/EditServiceVariationCoordinator$setGapTimeRowsVisibility$$inlined$onClickDebounced$1;-><init>()V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 373
    :cond_4
    iget-object p1, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->durationRow:Lcom/squareup/noho/NohoRow;

    if-nez p1, :cond_5

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    check-cast p1, Landroid/view/View;

    .line 437
    new-instance v0, Lcom/squareup/ui/items/EditServiceVariationCoordinator$setGapTimeRowsVisibility$$inlined$onClickDebounced$2;

    invoke-direct {v0, p0}, Lcom/squareup/ui/items/EditServiceVariationCoordinator$setGapTimeRowsVisibility$$inlined$onClickDebounced$2;-><init>(Lcom/squareup/ui/items/EditServiceVariationCoordinator;)V

    check-cast v0, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_0
    return-void
.end method

.method private final shouldEnabledPrimaryButton(Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;)Z
    .locals 0

    .line 378
    invoke-virtual {p1}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;->getName()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    return p1
.end method

.method private final showAPoSFields(Z)V
    .locals 2

    .line 293
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->cancellationFeeEditText:Lcom/squareup/noho/NohoEditText;

    if-nez v0, :cond_0

    const-string v1, "cancellationFeeEditText"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Landroid/view/View;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 294
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->gapTimeToggle:Lcom/squareup/noho/NohoCheckableRow;

    if-nez v0, :cond_1

    const-string v1, "gapTimeToggle"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    check-cast v0, Landroid/view/View;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 295
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->initialDurationRow:Lcom/squareup/noho/NohoRow;

    if-nez v0, :cond_2

    const-string v1, "initialDurationRow"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    check-cast v0, Landroid/view/View;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 296
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->gapDurationRow:Lcom/squareup/noho/NohoRow;

    if-nez v0, :cond_3

    const-string v1, "gapDurationRow"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    check-cast v0, Landroid/view/View;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 297
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->finalDurationRow:Lcom/squareup/noho/NohoRow;

    if-nez v0, :cond_4

    const-string v1, "finalDurationRow"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    check-cast v0, Landroid/view/View;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 298
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->extraTimeToggle:Lcom/squareup/noho/NohoCheckableRow;

    if-nez v0, :cond_5

    const-string v1, "extraTimeToggle"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    check-cast v0, Landroid/view/View;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 299
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->afterAppointmentRow:Lcom/squareup/noho/NohoRow;

    if-nez v0, :cond_6

    const-string v1, "afterAppointmentRow"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    check-cast v0, Landroid/view/View;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 300
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->extraTimeHelperText:Lcom/squareup/noho/NohoMessageView;

    if-nez v0, :cond_7

    const-string v1, "extraTimeHelperText"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    check-cast v0, Landroid/view/View;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 301
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->bookableByCustomerToggle:Lcom/squareup/noho/NohoCheckableRow;

    if-nez v0, :cond_8

    const-string v1, "bookableByCustomerToggle"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_8
    check-cast v0, Landroid/view/View;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 302
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->assignedEmployeesRow:Lcom/squareup/noho/NohoRow;

    if-nez v0, :cond_9

    const-string v1, "assignedEmployeesRow"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_9
    check-cast v0, Landroid/view/View;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 303
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->displayPriceContainer:Landroid/view/ViewGroup;

    if-nez v0, :cond_a

    const-string v1, "displayPriceContainer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_a
    check-cast v0, Landroid/view/View;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 304
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->displayPriceEditText:Lcom/squareup/noho/NohoEditText;

    if-nez v0, :cond_b

    const-string v1, "displayPriceEditText"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_b
    check-cast v0, Landroid/view/View;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method private final updateAPoSFields(Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;)V
    .locals 8

    .line 319
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->cancellationFeeEditText:Lcom/squareup/noho/NohoEditText;

    if-nez v0, :cond_0

    const-string v1, "cancellationFeeEditText"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;->getShowCancellationField()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 321
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->APPOINTMENTS_GAP_TIME:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    const/4 v1, 0x0

    const/16 v2, 0x8

    if-nez v0, :cond_2

    .line 322
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->gapTimeToggle:Lcom/squareup/noho/NohoCheckableRow;

    if-nez v0, :cond_1

    const-string v3, "gapTimeToggle"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoCheckableRow;->setVisibility(I)V

    .line 323
    invoke-direct {p0, v2}, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->setGapTimeRowsVisibility(I)V

    goto/16 :goto_0

    .line 325
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;->getGapTimeInfo()Lcom/squareup/intermission/GapTimeInfo;

    move-result-object v0

    if-nez v0, :cond_3

    .line 326
    invoke-direct {p0, v2}, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->setGapTimeRowsVisibility(I)V

    goto/16 :goto_0

    .line 328
    :cond_3
    invoke-direct {p0, v1}, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->setGapTimeRowsVisibility(I)V

    .line 330
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->initialDurationRow:Lcom/squareup/noho/NohoRow;

    const-string v3, "initialDurationRow"

    if-nez v0, :cond_4

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {p1}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;->getGapTimeInfo()Lcom/squareup/intermission/GapTimeInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/squareup/intermission/GapTimeInfo;->getInitialDuration()Ljava/lang/String;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v0, v4}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    .line 331
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->gapDurationRow:Lcom/squareup/noho/NohoRow;

    const-string v4, "gapDurationRow"

    if-nez v0, :cond_5

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {p1}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;->getGapTimeInfo()Lcom/squareup/intermission/GapTimeInfo;

    move-result-object v5

    invoke-virtual {v5}, Lcom/squareup/intermission/GapTimeInfo;->getGapDuration()Ljava/lang/String;

    move-result-object v5

    check-cast v5, Ljava/lang/CharSequence;

    invoke-virtual {v0, v5}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    .line 332
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->finalDurationRow:Lcom/squareup/noho/NohoRow;

    const-string v5, "finalDurationRow"

    if-nez v0, :cond_6

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    invoke-virtual {p1}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;->getGapTimeInfo()Lcom/squareup/intermission/GapTimeInfo;

    move-result-object v6

    invoke-virtual {v6}, Lcom/squareup/intermission/GapTimeInfo;->getFinalDuration()Ljava/lang/String;

    move-result-object v6

    check-cast v6, Ljava/lang/CharSequence;

    invoke-virtual {v0, v6}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    .line 334
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->initialDurationRow:Lcom/squareup/noho/NohoRow;

    if-nez v0, :cond_7

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    check-cast v0, Landroid/view/View;

    .line 409
    new-instance v3, Lcom/squareup/ui/items/EditServiceVariationCoordinator$updateAPoSFields$$inlined$onClickDebounced$1;

    invoke-direct {v3, p0}, Lcom/squareup/ui/items/EditServiceVariationCoordinator$updateAPoSFields$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/ui/items/EditServiceVariationCoordinator;)V

    check-cast v3, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 337
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->gapDurationRow:Lcom/squareup/noho/NohoRow;

    if-nez v0, :cond_8

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_8
    check-cast v0, Landroid/view/View;

    .line 416
    new-instance v3, Lcom/squareup/ui/items/EditServiceVariationCoordinator$updateAPoSFields$$inlined$onClickDebounced$2;

    invoke-direct {v3, p0}, Lcom/squareup/ui/items/EditServiceVariationCoordinator$updateAPoSFields$$inlined$onClickDebounced$2;-><init>(Lcom/squareup/ui/items/EditServiceVariationCoordinator;)V

    check-cast v3, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 340
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->finalDurationRow:Lcom/squareup/noho/NohoRow;

    if-nez v0, :cond_9

    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_9
    check-cast v0, Landroid/view/View;

    .line 423
    new-instance v3, Lcom/squareup/ui/items/EditServiceVariationCoordinator$updateAPoSFields$$inlined$onClickDebounced$3;

    invoke-direct {v3, p0}, Lcom/squareup/ui/items/EditServiceVariationCoordinator$updateAPoSFields$$inlined$onClickDebounced$3;-><init>(Lcom/squareup/ui/items/EditServiceVariationCoordinator;)V

    check-cast v3, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 346
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;->getBlockedExtraTime()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    const-string v0, "afterAppointmentRow"

    cmp-long v7, v3, v5

    if-nez v7, :cond_b

    .line 347
    iget-object v3, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->afterAppointmentRow:Lcom/squareup/noho/NohoRow;

    if-nez v3, :cond_a

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_a
    invoke-virtual {v3, v2}, Lcom/squareup/noho/NohoRow;->setVisibility(I)V

    goto :goto_1

    .line 349
    :cond_b
    iget-object v3, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->afterAppointmentRow:Lcom/squareup/noho/NohoRow;

    if-nez v3, :cond_c

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_c
    invoke-virtual {v3, v1}, Lcom/squareup/noho/NohoRow;->setVisibility(I)V

    .line 351
    iget-object v3, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->afterAppointmentRow:Lcom/squareup/noho/NohoRow;

    if-nez v3, :cond_d

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_d
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->durationFormatter:Lcom/squareup/text/DurationFormatter;

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;->getBlockedExtraTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Lorg/threeten/bp/Duration;->ofMillis(J)Lorg/threeten/bp/Duration;

    move-result-object v4

    const-string v5, "Duration.ofMillis(screenData.blockedExtraTime)"

    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v5, 0x2

    const/4 v6, 0x0

    invoke-static {v0, v4, v1, v5, v6}, Lcom/squareup/text/DurationFormatter;->format$default(Lcom/squareup/text/DurationFormatter;Lorg/threeten/bp/Duration;ZILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v3, v0}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    .line 354
    :goto_1
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->displayPriceContainer:Landroid/view/ViewGroup;

    const-string v3, "displayPriceContainer"

    if-nez v0, :cond_e

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_e
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 355
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->assignedEmployeesRow:Lcom/squareup/noho/NohoRow;

    const-string v4, "assignedEmployeesRow"

    if-nez v0, :cond_f

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_f
    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoRow;->setVisibility(I)V

    .line 357
    invoke-virtual {p1}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;->getBookableByCustomer()Z

    move-result v0

    if-eqz v0, :cond_13

    .line 358
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->assignedEmployeesRow:Lcom/squareup/noho/NohoRow;

    if-nez v0, :cond_10

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_10
    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoRow;->setVisibility(I)V

    .line 359
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->assignedEmployeesRow:Lcom/squareup/noho/NohoRow;

    if-nez v0, :cond_11

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_11
    invoke-virtual {p1}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;->getAssignedEmployeesDescription()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    .line 361
    iget-object p1, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->displayPriceContainer:Landroid/view/ViewGroup;

    if-nez p1, :cond_12

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_12
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    :cond_13
    return-void
.end method

.method private final updateFields(Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;)V
    .locals 5

    .line 308
    invoke-virtual {p1}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;->getEnableUnitPricedServices()Z

    move-result v0

    const-string v1, "durationRow"

    const/4 v2, 0x0

    if-eqz v0, :cond_2

    .line 309
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->durationRow:Lcom/squareup/noho/NohoRow;

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Landroid/view/View;

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;->getMeasurementUnit()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v3

    if-nez v3, :cond_1

    const/4 v3, 0x1

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    :goto_0
    invoke-static {v0, v3}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 313
    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->durationRow:Lcom/squareup/noho/NohoRow;

    if-nez v0, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    iget-object v1, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->durationFormatter:Lcom/squareup/text/DurationFormatter;

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditVariationRunner$EditVariationScreenData$EditServiceVariationScreenData;->getDuration()J

    move-result-wide v3

    invoke-static {v3, v4}, Lorg/threeten/bp/Duration;->ofMillis(J)Lorg/threeten/bp/Duration;

    move-result-object p1

    const-string v3, "Duration.ofMillis(screenData.duration)"

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-static {v1, p1, v2, v3, v4}, Lcom/squareup/text/DurationFormatter;->format$default(Lcom/squareup/text/DurationFormatter;Lorg/threeten/bp/Duration;ZILjava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 3

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 85
    invoke-super {p0, p1}, Lcom/squareup/coordinators/Coordinator;->attach(Landroid/view/View;)V

    .line 87
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->bindViews(Landroid/view/View;)V

    .line 88
    new-instance v0, Lcom/squareup/ui/items/EditServiceVariationCoordinator$attach$1;

    iget-object v1, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->editServiceVariationRunner:Lcom/squareup/ui/items/EditServiceVariationScreen$EditServiceVariationRunner;

    invoke-direct {v0, v1}, Lcom/squareup/ui/items/EditServiceVariationCoordinator$attach$1;-><init>(Lcom/squareup/ui/items/EditServiceVariationScreen$EditServiceVariationRunner;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 91
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->editServiceVariationRunner:Lcom/squareup/ui/items/EditServiceVariationScreen$EditServiceVariationRunner;

    invoke-interface {v0}, Lcom/squareup/ui/items/EditServiceVariationScreen$EditServiceVariationRunner;->editServiceVariationScreenData()Lrx/Observable;

    move-result-object v0

    const/4 v1, 0x1

    .line 92
    invoke-virtual {v0, v1}, Lrx/Observable;->take(I)Lrx/Observable;

    move-result-object v0

    .line 93
    new-instance v2, Lcom/squareup/ui/items/EditServiceVariationCoordinator$attach$2;

    invoke-direct {v2, p0, p1}, Lcom/squareup/ui/items/EditServiceVariationCoordinator$attach$2;-><init>(Lcom/squareup/ui/items/EditServiceVariationCoordinator;Landroid/view/View;)V

    check-cast v2, Lrx/functions/Action1;

    invoke-virtual {v0, v2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    const-string v2, "editServiceVariationRunn\u2026se)\n          }\n        }"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 102
    invoke-static {v0, p1}, Lcom/squareup/util/SubscriptionsKt;->unsubscribeOnDetach(Lrx/Subscription;Landroid/view/View;)V

    .line 105
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceVariationCoordinator;->editServiceVariationRunner:Lcom/squareup/ui/items/EditServiceVariationScreen$EditServiceVariationRunner;

    invoke-interface {v0}, Lcom/squareup/ui/items/EditServiceVariationScreen$EditServiceVariationRunner;->editServiceVariationScreenData()Lrx/Observable;

    move-result-object v0

    .line 106
    invoke-virtual {v0, v1}, Lrx/Observable;->skip(I)Lrx/Observable;

    move-result-object v0

    .line 107
    new-instance v1, Lcom/squareup/ui/items/EditServiceVariationCoordinator$attach$3;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/items/EditServiceVariationCoordinator$attach$3;-><init>(Lcom/squareup/ui/items/EditServiceVariationCoordinator;Landroid/view/View;)V

    check-cast v1, Lrx/functions/Action1;

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    const-string v1, "editServiceVariationRunn\u2026it)\n          }\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 114
    invoke-static {v0, p1}, Lcom/squareup/util/SubscriptionsKt;->unsubscribeOnDetach(Lrx/Subscription;Landroid/view/View;)V

    return-void
.end method
