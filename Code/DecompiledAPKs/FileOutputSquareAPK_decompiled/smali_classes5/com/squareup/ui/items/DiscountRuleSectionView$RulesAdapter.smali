.class Lcom/squareup/ui/items/DiscountRuleSectionView$RulesAdapter;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;
.source "DiscountRuleSectionView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/DiscountRuleSectionView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "RulesAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "Lcom/squareup/ui/items/DiscountRuleSectionView$RuleRowViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private data:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/ui/items/RuleRowItem;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/squareup/ui/items/DiscountRuleSectionView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/items/DiscountRuleSectionView;)V
    .locals 0

    .line 81
    iput-object p1, p0, Lcom/squareup/ui/items/DiscountRuleSectionView$RulesAdapter;->this$0:Lcom/squareup/ui/items/DiscountRuleSectionView;

    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    .line 82
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/items/DiscountRuleSectionView$RulesAdapter;->data:Ljava/util/List;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/items/DiscountRuleSectionView$RulesAdapter;)Ljava/util/List;
    .locals 0

    .line 81
    iget-object p0, p0, Lcom/squareup/ui/items/DiscountRuleSectionView$RulesAdapter;->data:Ljava/util/List;

    return-object p0
.end method


# virtual methods
.method public getItemCount()I
    .locals 1

    .line 93
    iget-object v0, p0, Lcom/squareup/ui/items/DiscountRuleSectionView$RulesAdapter;->data:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .line 81
    check-cast p1, Lcom/squareup/ui/items/DiscountRuleSectionView$RuleRowViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/items/DiscountRuleSectionView$RulesAdapter;->onBindViewHolder(Lcom/squareup/ui/items/DiscountRuleSectionView$RuleRowViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/squareup/ui/items/DiscountRuleSectionView$RuleRowViewHolder;I)V
    .locals 1

    .line 89
    iget-object v0, p0, Lcom/squareup/ui/items/DiscountRuleSectionView$RulesAdapter;->data:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/RuleRowItem;

    invoke-virtual {p1, p2, v0}, Lcom/squareup/ui/items/DiscountRuleSectionView$RuleRowViewHolder;->bind(ILcom/squareup/ui/items/RuleRowItem;)V

    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0

    .line 81
    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/items/DiscountRuleSectionView$RulesAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/ui/items/DiscountRuleSectionView$RuleRowViewHolder;

    move-result-object p1

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/ui/items/DiscountRuleSectionView$RuleRowViewHolder;
    .locals 2

    .line 85
    new-instance p2, Lcom/squareup/ui/items/DiscountRuleSectionView$RuleRowViewHolder;

    iget-object v0, p0, Lcom/squareup/ui/items/DiscountRuleSectionView$RulesAdapter;->this$0:Lcom/squareup/ui/items/DiscountRuleSectionView;

    sget v1, Lcom/squareup/edititem/R$layout;->discount_rule_row:I

    invoke-static {v1, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    invoke-direct {p2, v0, p1}, Lcom/squareup/ui/items/DiscountRuleSectionView$RuleRowViewHolder;-><init>(Lcom/squareup/ui/items/DiscountRuleSectionView;Landroid/view/View;)V

    return-object p2
.end method

.method public setData(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/ui/items/RuleRowItem;",
            ">;)V"
        }
    .end annotation

    .line 97
    iput-object p1, p0, Lcom/squareup/ui/items/DiscountRuleSectionView$RulesAdapter;->data:Ljava/util/List;

    .line 98
    iget-object v0, p0, Lcom/squareup/ui/items/DiscountRuleSectionView$RulesAdapter;->this$0:Lcom/squareup/ui/items/DiscountRuleSectionView;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    if-lez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 99
    invoke-virtual {p0}, Lcom/squareup/ui/items/DiscountRuleSectionView$RulesAdapter;->notifyDataSetChanged()V

    return-void
.end method
