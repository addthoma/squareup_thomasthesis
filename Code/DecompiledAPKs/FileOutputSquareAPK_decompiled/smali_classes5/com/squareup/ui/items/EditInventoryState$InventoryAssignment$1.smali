.class final Lcom/squareup/ui/items/EditInventoryState$InventoryAssignment$1;
.super Ljava/lang/Object;
.source "EditInventoryState.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/EditInventoryState$InventoryAssignment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator<",
        "Lcom/squareup/ui/items/EditInventoryState$InventoryAssignment;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 186
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/squareup/ui/items/EditInventoryState$InventoryAssignment;
    .locals 2

    .line 188
    const-class v0, Ljava/math/BigDecimal;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/math/BigDecimal;

    .line 189
    const-class v1, Lcom/squareup/protos/common/Money;

    invoke-static {p1, v1}, Lcom/squareup/util/Parcels;->readProtoMessage(Landroid/os/Parcel;Ljava/lang/Class;)Lcom/squareup/wire/Message;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/common/Money;

    .line 190
    new-instance v1, Lcom/squareup/ui/items/EditInventoryState$InventoryAssignment;

    invoke-direct {v1, v0, p1}, Lcom/squareup/ui/items/EditInventoryState$InventoryAssignment;-><init>(Ljava/math/BigDecimal;Lcom/squareup/protos/common/Money;)V

    return-object v1
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 0

    .line 186
    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/EditInventoryState$InventoryAssignment$1;->createFromParcel(Landroid/os/Parcel;)Lcom/squareup/ui/items/EditInventoryState$InventoryAssignment;

    move-result-object p1

    return-object p1
.end method

.method public newArray(I)[Lcom/squareup/ui/items/EditInventoryState$InventoryAssignment;
    .locals 0

    .line 194
    new-array p1, p1, [Lcom/squareup/ui/items/EditInventoryState$InventoryAssignment;

    return-object p1
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 0

    .line 186
    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/EditInventoryState$InventoryAssignment$1;->newArray(I)[Lcom/squareup/ui/items/EditInventoryState$InventoryAssignment;

    move-result-object p1

    return-object p1
.end method
