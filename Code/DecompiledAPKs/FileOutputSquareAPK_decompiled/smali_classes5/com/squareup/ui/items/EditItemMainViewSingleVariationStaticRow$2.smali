.class Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow$2;
.super Lcom/squareup/text/EmptyTextWatcher;
.source "EditItemMainViewSingleVariationStaticRow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->setAlwaysEditableContent(Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;

.field final synthetic val$unitAbbreviation:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;Ljava/lang/String;)V
    .locals 0

    .line 183
    iput-object p1, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow$2;->this$0:Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;

    iput-object p2, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow$2;->val$unitAbbreviation:Ljava/lang/String;

    invoke-direct {p0}, Lcom/squareup/text/EmptyTextWatcher;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .line 185
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow$2;->this$0:Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;

    invoke-static {v0}, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->access$100(Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;)Lcom/squareup/noho/NohoEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/noho/NohoEditText;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 186
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow$2;->this$0:Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;

    iget-object v0, v0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    iget-object v1, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow$2;->val$unitAbbreviation:Ljava/lang/String;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/money/PriceLocaleHelper;->extractMoney(Ljava/lang/CharSequence;Ljava/lang/String;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    .line 187
    iget-object v0, p0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow$2;->this$0:Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;

    iget-object v0, v0, Lcom/squareup/ui/items/EditItemMainViewSingleVariationStaticRow;->presenter:Lcom/squareup/ui/items/EditItemMainPresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/items/EditItemMainPresenter;->onPriceChanged(Lcom/squareup/protos/common/Money;)V

    :cond_0
    return-void
.end method
