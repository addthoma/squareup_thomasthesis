.class final Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$onEnterScope$2$2;
.super Ljava/lang/Object;
.source "DetailSearchableListWorkflowRunner.kt"

# interfaces
.implements Lcom/squareup/shared/catalog/CatalogTask;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$onEnterScope$2;->accept(Lcom/squareup/ui/items/DetailSearchableListResult;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/shared/catalog/CatalogTask<",
        "Ljava/util/List<",
        "Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a&\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002 \u0003*\u0012\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002\u0018\u00010\u00040\u00012\u000e\u0010\u0005\u001a\n \u0003*\u0004\u0018\u00010\u00060\u0006H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;",
        "kotlin.jvm.PlatformType",
        "",
        "cogs1",
        "Lcom/squareup/shared/catalog/Catalog$Local;",
        "perform"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $it:Lcom/squareup/ui/items/DetailSearchableListResult;


# direct methods
.method constructor <init>(Lcom/squareup/ui/items/DetailSearchableListResult;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$onEnterScope$2$2;->$it:Lcom/squareup/ui/items/DetailSearchableListResult;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic perform(Lcom/squareup/shared/catalog/Catalog$Local;)Ljava/lang/Object;
    .locals 0

    .line 52
    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$onEnterScope$2$2;->perform(Lcom/squareup/shared/catalog/Catalog$Local;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public final perform(Lcom/squareup/shared/catalog/Catalog$Local;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/Catalog$Local;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader$SkuLookupInfo;",
            ">;"
        }
    .end annotation

    .line 118
    const-class v0, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader;

    .line 117
    invoke-interface {p1, v0}, Lcom/squareup/shared/catalog/Catalog$Local;->getSyntheticTableReader(Ljava/lang/Class;)Lcom/squareup/shared/catalog/synthetictables/SyntheticTableReader;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader;

    .line 120
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$onEnterScope$2$2;->$it:Lcom/squareup/ui/items/DetailSearchableListResult;

    check-cast v0, Lcom/squareup/ui/items/DetailSearchableListResult$GoToHandleScannedBarcode;

    invoke-virtual {v0}, Lcom/squareup/ui/items/DetailSearchableListResult$GoToHandleScannedBarcode;->getBarcode()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$onEnterScope$2$2;->$it:Lcom/squareup/ui/items/DetailSearchableListResult;

    check-cast v1, Lcom/squareup/ui/items/DetailSearchableListResult$GoToHandleScannedBarcode;

    invoke-virtual {v1}, Lcom/squareup/ui/items/DetailSearchableListResult$GoToHandleScannedBarcode;->getItemTypes()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/squareup/shared/catalog/synthetictables/ItemVariationLookupTableReader;->getInfoForSku(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method
