.class public final Lcom/squareup/ui/items/DetailSearchableListInput;
.super Ljava/lang/Object;
.source "DetailSearchableListWorkflow.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/DetailSearchableListInput$CREATOR;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\t\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u0086\u0008\u0018\u0000 \u001b2\u00020\u0001:\u0001\u001bB\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000c\u001a\u00020\u0005H\u00c6\u0003J\u001d\u0010\r\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001J\u0008\u0010\u000e\u001a\u00020\u000fH\u0016J\u0013\u0010\u0010\u001a\u00020\u00052\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u00d6\u0003J\t\u0010\u0013\u001a\u00020\u000fH\u00d6\u0001J\t\u0010\u0014\u001a\u00020\u0015H\u00d6\u0001J\u0018\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u000fH\u0016R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/ui/items/DetailSearchableListInput;",
        "Landroid/os/Parcelable;",
        "configuration",
        "Lcom/squareup/ui/items/DetailSearchableListConfiguration;",
        "supportSetUpItemGridButton",
        "",
        "(Lcom/squareup/ui/items/DetailSearchableListConfiguration;Z)V",
        "getConfiguration",
        "()Lcom/squareup/ui/items/DetailSearchableListConfiguration;",
        "getSupportSetUpItemGridButton",
        "()Z",
        "component1",
        "component2",
        "copy",
        "describeContents",
        "",
        "equals",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "writeToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "CREATOR",
        "items-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Lcom/squareup/ui/items/DetailSearchableListInput$CREATOR;


# instance fields
.field private final configuration:Lcom/squareup/ui/items/DetailSearchableListConfiguration;

.field private final supportSetUpItemGridButton:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/items/DetailSearchableListInput$CREATOR;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/items/DetailSearchableListInput$CREATOR;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/items/DetailSearchableListInput;->CREATOR:Lcom/squareup/ui/items/DetailSearchableListInput$CREATOR;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/items/DetailSearchableListConfiguration;Z)V
    .locals 1

    const-string v0, "configuration"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListInput;->configuration:Lcom/squareup/ui/items/DetailSearchableListConfiguration;

    iput-boolean p2, p0, Lcom/squareup/ui/items/DetailSearchableListInput;->supportSetUpItemGridButton:Z

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/ui/items/DetailSearchableListConfiguration;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x1

    .line 55
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/items/DetailSearchableListInput;-><init>(Lcom/squareup/ui/items/DetailSearchableListConfiguration;Z)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/items/DetailSearchableListInput;Lcom/squareup/ui/items/DetailSearchableListConfiguration;ZILjava/lang/Object;)Lcom/squareup/ui/items/DetailSearchableListInput;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListInput;->configuration:Lcom/squareup/ui/items/DetailSearchableListConfiguration;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-boolean p2, p0, Lcom/squareup/ui/items/DetailSearchableListInput;->supportSetUpItemGridButton:Z

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/items/DetailSearchableListInput;->copy(Lcom/squareup/ui/items/DetailSearchableListConfiguration;Z)Lcom/squareup/ui/items/DetailSearchableListInput;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/ui/items/DetailSearchableListConfiguration;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListInput;->configuration:Lcom/squareup/ui/items/DetailSearchableListConfiguration;

    return-object v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ui/items/DetailSearchableListInput;->supportSetUpItemGridButton:Z

    return v0
.end method

.method public final copy(Lcom/squareup/ui/items/DetailSearchableListConfiguration;Z)Lcom/squareup/ui/items/DetailSearchableListInput;
    .locals 1

    const-string v0, "configuration"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/items/DetailSearchableListInput;

    invoke-direct {v0, p1, p2}, Lcom/squareup/ui/items/DetailSearchableListInput;-><init>(Lcom/squareup/ui/items/DetailSearchableListConfiguration;Z)V

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/items/DetailSearchableListInput;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/items/DetailSearchableListInput;

    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListInput;->configuration:Lcom/squareup/ui/items/DetailSearchableListConfiguration;

    iget-object v1, p1, Lcom/squareup/ui/items/DetailSearchableListInput;->configuration:Lcom/squareup/ui/items/DetailSearchableListConfiguration;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/items/DetailSearchableListInput;->supportSetUpItemGridButton:Z

    iget-boolean p1, p1, Lcom/squareup/ui/items/DetailSearchableListInput;->supportSetUpItemGridButton:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getConfiguration()Lcom/squareup/ui/items/DetailSearchableListConfiguration;
    .locals 1

    .line 51
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListInput;->configuration:Lcom/squareup/ui/items/DetailSearchableListConfiguration;

    return-object v0
.end method

.method public final getSupportSetUpItemGridButton()Z
    .locals 1

    .line 55
    iget-boolean v0, p0, Lcom/squareup/ui/items/DetailSearchableListInput;->supportSetUpItemGridButton:Z

    return v0
.end method

.method public hashCode()I
    .locals 2

    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListInput;->configuration:Lcom/squareup/ui/items/DetailSearchableListConfiguration;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/ui/items/DetailSearchableListInput;->supportSetUpItemGridButton:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DetailSearchableListInput(configuration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/items/DetailSearchableListInput;->configuration:Lcom/squareup/ui/items/DetailSearchableListConfiguration;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", supportSetUpItemGridButton="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ui/items/DetailSearchableListInput;->supportSetUpItemGridButton:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListInput;->configuration:Lcom/squareup/ui/items/DetailSearchableListConfiguration;

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 62
    iget-boolean p2, p0, Lcom/squareup/ui/items/DetailSearchableListInput;->supportSetUpItemGridButton:Z

    invoke-static {p1, p2}, Lcom/squareup/util/Parcels;->writeBooleanAsInt(Landroid/os/Parcel;Z)V

    return-void
.end method
