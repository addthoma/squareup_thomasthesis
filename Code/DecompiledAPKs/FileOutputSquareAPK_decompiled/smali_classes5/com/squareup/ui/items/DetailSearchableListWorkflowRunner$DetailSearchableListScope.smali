.class public final Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$DetailSearchableListScope;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "DetailSearchableListWorkflowRunner.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DetailSearchableListScope"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$DetailSearchableListScope$ParentComponent;,
        Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$DetailSearchableListScope$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDetailSearchableListWorkflowRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DetailSearchableListWorkflowRunner.kt\ncom/squareup/ui/items/DetailSearchableListWorkflowRunner$DetailSearchableListScope\n+ 2 Components.kt\ncom/squareup/dagger/Components\n*L\n1#1,267:1\n35#2:268\n*E\n*S KotlinDebug\n*F\n+ 1 DetailSearchableListWorkflowRunner.kt\ncom/squareup/ui/items/DetailSearchableListWorkflowRunner$DetailSearchableListScope\n*L\n209#1:268\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0003\u0018\u0000 \u00182\u00020\u0001:\u0002\u0018\u0019B\u0017\u0012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u0001\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0010\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\rH\u0016J\u0018\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0014J\u0008\u0010\u0014\u001a\u00020\u0015H\u0016J\u0008\u0010\u0016\u001a\u00020\u0017H\u0016R\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0001\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\t\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$DetailSearchableListScope;",
        "Lcom/squareup/ui/main/RegisterTreeKey;",
        "parent",
        "configuration",
        "Lcom/squareup/ui/items/DetailSearchableListConfiguration;",
        "(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/ui/items/DetailSearchableListConfiguration;)V",
        "getConfiguration",
        "()Lcom/squareup/ui/items/DetailSearchableListConfiguration;",
        "getParent",
        "()Lcom/squareup/ui/main/RegisterTreeKey;",
        "buildScope",
        "Lmortar/MortarScope$Builder;",
        "parentScope",
        "Lmortar/MortarScope;",
        "doWriteToParcel",
        "",
        "parcel",
        "Landroid/os/Parcel;",
        "flags",
        "",
        "getName",
        "",
        "getParentKey",
        "",
        "Companion",
        "ParentComponent",
        "items-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$DetailSearchableListScope;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$DetailSearchableListScope$Companion;


# instance fields
.field private final configuration:Lcom/squareup/ui/items/DetailSearchableListConfiguration;

.field private final parent:Lcom/squareup/ui/main/RegisterTreeKey;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$DetailSearchableListScope$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$DetailSearchableListScope$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$DetailSearchableListScope;->Companion:Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$DetailSearchableListScope$Companion;

    .line 234
    sget-object v0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$DetailSearchableListScope$Companion$CREATOR$1;->INSTANCE:Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$DetailSearchableListScope$Companion$CREATOR$1;

    check-cast v0, Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    const-string v1, "PathCreator.fromParcel {\u2026      )\n        )\n      }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/os/Parcelable$Creator;

    sput-object v0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$DetailSearchableListScope;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/ui/items/DetailSearchableListConfiguration;)V
    .locals 1

    const-string v0, "configuration"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 204
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$DetailSearchableListScope;->parent:Lcom/squareup/ui/main/RegisterTreeKey;

    iput-object p2, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$DetailSearchableListScope;->configuration:Lcom/squareup/ui/items/DetailSearchableListConfiguration;

    return-void
.end method


# virtual methods
.method public buildScope(Lmortar/MortarScope;)Lmortar/MortarScope$Builder;
    .locals 2

    const-string v0, "parentScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 268
    const-class v0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$DetailSearchableListScope$ParentComponent;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    .line 209
    check-cast v0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$DetailSearchableListScope$ParentComponent;

    .line 210
    invoke-interface {v0}, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$DetailSearchableListScope$ParentComponent;->detailSearchableListWorkflowRunnerFactory()Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$Factory;

    move-result-object v0

    .line 211
    iget-object v1, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$DetailSearchableListScope;->configuration:Lcom/squareup/ui/items/DetailSearchableListConfiguration;

    invoke-virtual {v1}, Lcom/squareup/ui/items/DetailSearchableListConfiguration;->getDataType()Lcom/squareup/ui/items/DetailSearchableListDataType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$Factory;->build(Lcom/squareup/ui/items/DetailSearchableListDataType;)Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;

    move-result-object v0

    .line 212
    iget-object v1, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$DetailSearchableListScope;->configuration:Lcom/squareup/ui/items/DetailSearchableListConfiguration;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;->updateConfiguration(Lcom/squareup/ui/items/DetailSearchableListConfiguration;)V

    .line 213
    invoke-super {p0, p1}, Lcom/squareup/ui/main/RegisterTreeKey;->buildScope(Lmortar/MortarScope;)Lmortar/MortarScope$Builder;

    move-result-object p1

    const-string v1, "it"

    .line 214
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;->registerServices(Lmortar/MortarScope$Builder;)V

    const-string v0, "super.buildScope(parentS\u2026er.registerServices(it) }"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    const-string v0, "parcel"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 221
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$DetailSearchableListScope;->parent:Lcom/squareup/ui/main/RegisterTreeKey;

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 222
    iget-object p2, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$DetailSearchableListScope;->configuration:Lcom/squareup/ui/items/DetailSearchableListConfiguration;

    invoke-virtual {p2}, Lcom/squareup/ui/items/DetailSearchableListConfiguration;->getDataType()Lcom/squareup/ui/items/DetailSearchableListDataType;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/ui/items/DetailSearchableListDataType;->name()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method

.method public final getConfiguration()Lcom/squareup/ui/items/DetailSearchableListConfiguration;
    .locals 1

    .line 203
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$DetailSearchableListScope;->configuration:Lcom/squareup/ui/items/DetailSearchableListConfiguration;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .line 226
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Lcom/squareup/ui/main/RegisterTreeKey;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$DetailSearchableListScope;->configuration:Lcom/squareup/ui/items/DetailSearchableListConfiguration;

    invoke-virtual {v1}, Lcom/squareup/ui/items/DetailSearchableListConfiguration;->getDataType()Lcom/squareup/ui/items/DetailSearchableListDataType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/ui/items/DetailSearchableListDataType;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getParent()Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 1

    .line 202
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$DetailSearchableListScope;->parent:Lcom/squareup/ui/main/RegisterTreeKey;

    return-object v0
.end method

.method public getParentKey()Ljava/lang/Object;
    .locals 1

    .line 206
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$DetailSearchableListScope;->parent:Lcom/squareup/ui/main/RegisterTreeKey;

    if-nez v0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    return-object v0
.end method
