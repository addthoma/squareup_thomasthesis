.class public Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;
.super Lcom/squareup/widgets/ResponsiveLinearLayout;
.source "EditServiceMainViewSingleVariationStaticRow.java"


# instance fields
.field private afterAppointmentRow:Lcom/squareup/noho/NohoRow;

.field analytics:Lcom/squareup/analytics/Analytics;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private assignedEmployeesRow:Lcom/squareup/noho/NohoRow;

.field private bookableByCustomerToggle:Lcom/squareup/noho/NohoCheckableRow;

.field private cancellationFeeEditText:Lcom/squareup/noho/NohoEditText;

.field catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private defaultVariationContainer:Landroid/view/View;

.field private displayPriceContainer:Landroid/view/ViewGroup;

.field private displayPriceEditText:Lcom/squareup/noho/NohoEditText;

.field durationFormatter:Lcom/squareup/text/DurationFormatter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private durationRow:Lcom/squareup/noho/NohoRow;

.field private extraTimeHelperText:Lcom/squareup/noho/NohoMessageView;

.field private extraTimeToggle:Lcom/squareup/noho/NohoCheckableRow;

.field private finalDurationRow:Lcom/squareup/noho/NohoRow;

.field private gapDurationRow:Lcom/squareup/noho/NohoRow;

.field private gapTimeToggle:Lcom/squareup/noho/NohoCheckableRow;

.field private initialDurationRow:Lcom/squareup/noho/NohoRow;

.field intermissionHelper:Lcom/squareup/intermission/IntermissionHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field presenter:Lcom/squareup/ui/items/EditItemMainPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private priceEditScrubbingWatcher:Lcom/squareup/text/ScrubbingTextWatcher;

.field private priceEditText:Lcom/squareup/noho/NohoEditText;

.field private priceEditTextWatcher:Landroid/text/TextWatcher;

.field priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private priceTypeRow:Lcom/squareup/noho/NohoRow;

.field private readableDisplayPrices:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field res:Lcom/squareup/util/Res;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private unitTypeSelector:Lcom/squareup/noho/NohoRow;

.field private variationHeader:Lcom/squareup/marketfont/MarketTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 90
    invoke-direct {p0, p1, p2}, Lcom/squareup/widgets/ResponsiveLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 91
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->getContext()Landroid/content/Context;

    move-result-object p1

    const-class p2, Lcom/squareup/ui/items/EditItemMainScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/items/EditItemMainScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/items/EditItemMainScreen$Component;->inject(Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;)Lcom/squareup/noho/NohoEditText;
    .locals 0

    .line 55
    iget-object p0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->displayPriceEditText:Lcom/squareup/noho/NohoEditText;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;)Lcom/squareup/noho/NohoEditText;
    .locals 0

    .line 55
    iget-object p0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->priceEditText:Lcom/squareup/noho/NohoEditText;

    return-object p0
.end method

.method private cannotShowExtraServiceOptions(Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;)Z
    .locals 1

    .line 412
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

    invoke-interface {v0}, Lcom/squareup/catalogapi/CatalogIntegrationController;->hasExtraServiceOptions()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object p1, p1, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->defaultVariation:Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 413
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getCatalogMeasurementUnitToken()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/shared/catalog/utils/StringUtils;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method private createReadableDisplayPrices()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 222
    new-instance v0, Ljava/util/LinkedHashMap;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Ljava/util/LinkedHashMap;-><init>(I)V

    .line 224
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/squareup/edititem/R$string;->edit_service_starting_at:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "starting_at"

    .line 223
    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 226
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/squareup/edititem/R$string;->edit_service_variable_price:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "price_varies"

    .line 225
    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 228
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/squareup/edititem/R$string;->edit_service_call_for_price:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "call_us"

    .line 227
    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 229
    invoke-virtual {p0}, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/squareup/edititem/R$string;->edit_service_no_price:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "blank"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0
.end method

.method private registerListeners(Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;)V
    .locals 1

    .line 204
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->cannotShowExtraServiceOptions(Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;)Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    .line 207
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->gapTimeToggle:Lcom/squareup/noho/NohoCheckableRow;

    new-instance v0, Lcom/squareup/ui/items/-$$Lambda$EditServiceMainViewSingleVariationStaticRow$jMsyqXEBgE7_HLwiUpX3FO5sIYE;

    invoke-direct {v0, p0}, Lcom/squareup/ui/items/-$$Lambda$EditServiceMainViewSingleVariationStaticRow$jMsyqXEBgE7_HLwiUpX3FO5sIYE;-><init>(Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;)V

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoCheckableRow;->setOnCheckedChange(Lkotlin/jvm/functions/Function2;)V

    .line 209
    iget-object p1, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->extraTimeToggle:Lcom/squareup/noho/NohoCheckableRow;

    new-instance v0, Lcom/squareup/ui/items/-$$Lambda$EditServiceMainViewSingleVariationStaticRow$UwDfVIrJuK4Ius9okqqG9WHUoNU;

    invoke-direct {v0, p0}, Lcom/squareup/ui/items/-$$Lambda$EditServiceMainViewSingleVariationStaticRow$UwDfVIrJuK4Ius9okqqG9WHUoNU;-><init>(Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;)V

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoCheckableRow;->setOnCheckedChange(Lkotlin/jvm/functions/Function2;)V

    .line 214
    iget-object p1, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->bookableByCustomerToggle:Lcom/squareup/noho/NohoCheckableRow;

    new-instance v0, Lcom/squareup/ui/items/-$$Lambda$EditServiceMainViewSingleVariationStaticRow$4RfEFuDSq0zrGlmz34ZiMcKawoo;

    invoke-direct {v0, p0}, Lcom/squareup/ui/items/-$$Lambda$EditServiceMainViewSingleVariationStaticRow$4RfEFuDSq0zrGlmz34ZiMcKawoo;-><init>(Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;)V

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoCheckableRow;->setOnCheckedChange(Lkotlin/jvm/functions/Function2;)V

    return-void
.end method

.method private setBookableContent(Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;)V
    .locals 3

    .line 330
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->cannotShowExtraServiceOptions(Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 331
    iget-object p1, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->bookableByCustomerToggle:Lcom/squareup/noho/NohoCheckableRow;

    invoke-static {p1, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 332
    iget-object p1, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->assignedEmployeesRow:Lcom/squareup/noho/NohoRow;

    invoke-static {p1, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 333
    iget-object p1, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->displayPriceContainer:Landroid/view/ViewGroup;

    invoke-static {p1, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void

    .line 337
    :cond_0
    iget-object v0, p1, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->defaultVariation:Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 338
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getAvailableOnOnlineBookingSite()Z

    move-result v0

    .line 339
    iget-object v2, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->bookableByCustomerToggle:Lcom/squareup/noho/NohoCheckableRow;

    invoke-virtual {v2, v0}, Lcom/squareup/noho/NohoCheckableRow;->setChecked(Z)V

    if-eqz v0, :cond_2

    .line 342
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->assignedEmployeesRow:Lcom/squareup/noho/NohoRow;

    iget-object v1, p1, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->employeeDescription:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    .line 343
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->assignedEmployeesRow:Lcom/squareup/noho/NohoRow;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 345
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->displayPriceContainer:Landroid/view/ViewGroup;

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 346
    iget-object p1, p1, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->defaultVariation:Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getPriceDescription()Ljava/lang/String;

    move-result-object p1

    .line 347
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->readableDisplayPrices:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 348
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->readableDisplayPrices:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    .line 350
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->displayPriceEditText:Lcom/squareup/noho/NohoEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoEditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 352
    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->assignedEmployeesRow:Lcom/squareup/noho/NohoRow;

    invoke-static {p1, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 353
    iget-object p1, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->displayPriceContainer:Landroid/view/ViewGroup;

    invoke-static {p1, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 354
    iget-object p1, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->assignedEmployeesRow:Lcom/squareup/noho/NohoRow;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    :goto_0
    return-void
.end method

.method private setDurationContent(Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;)V
    .locals 6

    .line 262
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->durationRow:Lcom/squareup/noho/NohoRow;

    iget-object v1, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->durationFormatter:Lcom/squareup/text/DurationFormatter;

    iget-object v2, p1, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->defaultVariation:Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 263
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getDuration()J

    move-result-wide v2

    invoke-static {v2, v3}, Lorg/threeten/bp/Duration;->ofMillis(J)Lorg/threeten/bp/Duration;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/text/DurationFormatter;->format(Lorg/threeten/bp/Duration;)Ljava/lang/String;

    move-result-object v1

    .line 262
    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    .line 265
    iget-boolean v0, p1, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->enableRateBasedServices:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->defaultVariation:Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 266
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getCatalogMeasurementUnitToken()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 267
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->durationRow:Lcom/squareup/noho/NohoRow;

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 270
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->cannotShowExtraServiceOptions(Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;)Z

    move-result v0

    const/16 v2, 0x8

    if-eqz v0, :cond_1

    .line 271
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->extraTimeToggle:Lcom/squareup/noho/NohoCheckableRow;

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoCheckableRow;->setVisibility(I)V

    .line 272
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->gapTimeToggle:Lcom/squareup/noho/NohoCheckableRow;

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoCheckableRow;->setVisibility(I)V

    .line 273
    invoke-direct {p0, v2, p1}, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->showOrHideGapTimeRows(ILcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;)V

    .line 274
    iget-object p1, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->afterAppointmentRow:Lcom/squareup/noho/NohoRow;

    invoke-virtual {p1, v2}, Lcom/squareup/noho/NohoRow;->setVisibility(I)V

    .line 275
    iget-object p1, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->extraTimeHelperText:Lcom/squareup/noho/NohoMessageView;

    invoke-virtual {p1, v2}, Lcom/squareup/noho/NohoMessageView;->setVisibility(I)V

    return-void

    .line 279
    :cond_1
    iget-boolean v0, p1, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->showProcessingTime:Z

    const/4 v3, 0x1

    if-nez v0, :cond_2

    .line 280
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->gapTimeToggle:Lcom/squareup/noho/NohoCheckableRow;

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoCheckableRow;->setVisibility(I)V

    .line 281
    invoke-direct {p0, v2, p1}, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->showOrHideGapTimeRows(ILcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;)V

    goto :goto_0

    .line 283
    :cond_2
    iget-object v0, p1, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->defaultVariation:Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getIntermissions()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 284
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->gapTimeToggle:Lcom/squareup/noho/NohoCheckableRow;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoCheckableRow;->setChecked(Z)V

    .line 285
    invoke-direct {p0, v2, p1}, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->showOrHideGapTimeRows(ILcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;)V

    goto :goto_0

    .line 287
    :cond_3
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->gapTimeToggle:Lcom/squareup/noho/NohoCheckableRow;

    invoke-virtual {v0, v3}, Lcom/squareup/noho/NohoCheckableRow;->setChecked(Z)V

    .line 288
    invoke-direct {p0, v1, p1}, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->showOrHideGapTimeRows(ILcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;)V

    .line 290
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->intermissionHelper:Lcom/squareup/intermission/IntermissionHelper;

    iget-object v2, p1, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->defaultVariation:Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    invoke-interface {v0, v2}, Lcom/squareup/intermission/IntermissionHelper;->getGapTimeInfo(Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;)Lcom/squareup/intermission/GapTimeInfo;

    move-result-object v0

    .line 292
    iget-object v2, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->initialDurationRow:Lcom/squareup/noho/NohoRow;

    invoke-virtual {v0}, Lcom/squareup/intermission/GapTimeInfo;->getInitialDuration()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    .line 293
    iget-object v2, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->gapDurationRow:Lcom/squareup/noho/NohoRow;

    invoke-virtual {v0}, Lcom/squareup/intermission/GapTimeInfo;->getGapDuration()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    .line 294
    iget-object v2, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->finalDurationRow:Lcom/squareup/noho/NohoRow;

    invoke-virtual {v0}, Lcom/squareup/intermission/GapTimeInfo;->getFinalDuration()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    .line 296
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->initialDurationRow:Lcom/squareup/noho/NohoRow;

    new-instance v2, Lcom/squareup/ui/items/-$$Lambda$EditServiceMainViewSingleVariationStaticRow$grvS7w6owA7VBXJSumnf-5kDgS0;

    invoke-direct {v2, p0}, Lcom/squareup/ui/items/-$$Lambda$EditServiceMainViewSingleVariationStaticRow$grvS7w6owA7VBXJSumnf-5kDgS0;-><init>(Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;)V

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 297
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->gapDurationRow:Lcom/squareup/noho/NohoRow;

    new-instance v2, Lcom/squareup/ui/items/-$$Lambda$EditServiceMainViewSingleVariationStaticRow$NbZKHAQG8JYVb2id38Fcg_hsdhg;

    invoke-direct {v2, p0}, Lcom/squareup/ui/items/-$$Lambda$EditServiceMainViewSingleVariationStaticRow$NbZKHAQG8JYVb2id38Fcg_hsdhg;-><init>(Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;)V

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 298
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->finalDurationRow:Lcom/squareup/noho/NohoRow;

    new-instance v2, Lcom/squareup/ui/items/-$$Lambda$EditServiceMainViewSingleVariationStaticRow$DhT5HO2zArnjvTUdOZiDnvCviFk;

    invoke-direct {v2, p0}, Lcom/squareup/ui/items/-$$Lambda$EditServiceMainViewSingleVariationStaticRow$DhT5HO2zArnjvTUdOZiDnvCviFk;-><init>(Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;)V

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 302
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->extraTimeToggle:Lcom/squareup/noho/NohoCheckableRow;

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->isTransitionTimeRequired()Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoCheckableRow;->setChecked(Z)V

    .line 304
    invoke-virtual {p1}, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->isTransitionTimeRequired()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 305
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->afterAppointmentRow:Lcom/squareup/noho/NohoRow;

    iget-object v1, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->durationFormatter:Lcom/squareup/text/DurationFormatter;

    iget-object v2, p1, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->defaultVariation:Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 306
    invoke-virtual {v2}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getTransitionTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Lorg/threeten/bp/Duration;->ofMillis(J)Lorg/threeten/bp/Duration;

    move-result-object v2

    .line 305
    invoke-virtual {v1, v2}, Lcom/squareup/text/DurationFormatter;->format(Lorg/threeten/bp/Duration;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    .line 307
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->afterAppointmentRow:Lcom/squareup/noho/NohoRow;

    invoke-static {v0, v3}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 308
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->afterAppointmentRow:Lcom/squareup/noho/NohoRow;

    new-instance v1, Lcom/squareup/ui/items/-$$Lambda$EditServiceMainViewSingleVariationStaticRow$gs_QYCAjakmjw8iS5hrsVjPh0nk;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/items/-$$Lambda$EditServiceMainViewSingleVariationStaticRow$gs_QYCAjakmjw8iS5hrsVjPh0nk;-><init>(Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;)V

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 311
    :cond_4
    iget-object p1, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->afterAppointmentRow:Lcom/squareup/noho/NohoRow;

    invoke-static {p1, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 312
    iget-object p1, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->afterAppointmentRow:Lcom/squareup/noho/NohoRow;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    :goto_1
    return-void
.end method

.method private setLegacyPriceContent(Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;)V
    .locals 5

    .line 417
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->unitTypeSelector:Lcom/squareup/noho/NohoRow;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoRow;->setVisibility(I)V

    .line 418
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->priceTypeRow:Lcom/squareup/noho/NohoRow;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoRow;->setVisibility(I)V

    .line 420
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->priceEditScrubbingWatcher:Lcom/squareup/text/ScrubbingTextWatcher;

    if-eqz v0, :cond_0

    .line 421
    iget-object v3, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->priceEditText:Lcom/squareup/noho/NohoEditText;

    invoke-virtual {v3, v0}, Lcom/squareup/noho/NohoEditText;->removeSelectionWatcher(Lcom/squareup/text/SelectionWatcher;)V

    .line 422
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->priceEditText:Lcom/squareup/noho/NohoEditText;

    iget-object v3, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->priceEditScrubbingWatcher:Lcom/squareup/text/ScrubbingTextWatcher;

    invoke-virtual {v0, v3}, Lcom/squareup/noho/NohoEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 424
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    iget-object v3, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->priceEditText:Lcom/squareup/noho/NohoEditText;

    sget-object v4, Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;->BLANKABLE:Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;

    invoke-virtual {v0, v3, v4}, Lcom/squareup/money/PriceLocaleHelper;->configure(Lcom/squareup/text/HasSelectableText;Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;)Lcom/squareup/text/ScrubbingTextWatcher;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->priceEditScrubbingWatcher:Lcom/squareup/text/ScrubbingTextWatcher;

    .line 425
    new-instance v0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow$4;

    invoke-direct {v0, p0}, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow$4;-><init>(Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;)V

    iput-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->priceEditTextWatcher:Landroid/text/TextWatcher;

    .line 433
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->priceEditText:Lcom/squareup/noho/NohoEditText;

    iget-object v3, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->priceEditTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v3}, Lcom/squareup/noho/NohoEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 434
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    iget-object v3, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->priceEditText:Lcom/squareup/noho/NohoEditText;

    invoke-virtual {v0, v3}, Lcom/squareup/money/PriceLocaleHelper;->setHintToZeroMoney(Landroid/widget/TextView;)V

    .line 436
    iget-object v0, p1, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->defaultVariation:Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->isVariablePricing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 437
    iget-object p1, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->priceTypeRow:Lcom/squareup/noho/NohoRow;

    invoke-virtual {p0}, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v2, Lcom/squareup/edititem/R$string;->edit_service_variable_price_type:I

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    .line 438
    iget-object p1, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->priceEditText:Lcom/squareup/noho/NohoEditText;

    invoke-virtual {p1, v1}, Lcom/squareup/noho/NohoEditText;->setVisibility(I)V

    .line 439
    iget-object p1, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->cancellationFeeEditText:Lcom/squareup/noho/NohoEditText;

    const/16 v0, 0xf

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoEditText;->setBorderEdges(I)V

    goto :goto_0

    .line 441
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->priceTypeRow:Lcom/squareup/noho/NohoRow;

    invoke-virtual {p0}, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v3, Lcom/squareup/edititem/R$string;->edit_service_fixed_price_type:I

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    .line 442
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->priceEditText:Lcom/squareup/noho/NohoEditText;

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoEditText;->setVisibility(I)V

    .line 443
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->priceEditText:Lcom/squareup/noho/NohoEditText;

    iget-object v1, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object p1, p1, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->defaultVariation:Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getPrice()Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-interface {v1, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoEditText;->setText(Ljava/lang/CharSequence;)V

    .line 444
    iget-object p1, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->cancellationFeeEditText:Lcom/squareup/noho/NohoEditText;

    const/16 v0, 0xd

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoEditText;->setBorderEdges(I)V

    :goto_0
    return-void
.end method

.method private setPriceContent(Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;)V
    .locals 5

    .line 234
    iget-boolean v0, p1, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->enableRateBasedServices:Z

    if-eqz v0, :cond_0

    .line 235
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->setRateBasedServicesPriceContent(Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;)V

    .line 236
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->cancellationFeeEditText:Lcom/squareup/noho/NohoEditText;

    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditText;->setBorderEdges(I)V

    goto :goto_0

    .line 238
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->setLegacyPriceContent(Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;)V

    .line 241
    :goto_0
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->cannotShowExtraServiceOptions(Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 242
    iget-object p1, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->cancellationFeeEditText:Lcom/squareup/noho/NohoEditText;

    invoke-static {p1, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void

    .line 245
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->cancellationFeeEditText:Lcom/squareup/noho/NohoEditText;

    iget-boolean v2, p1, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->showCancellationFeeField:Z

    invoke-static {v0, v2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 249
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->priceEditText:Lcom/squareup/noho/NohoEditText;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoEditText;->getVisibility()I

    move-result v0

    const/16 v2, 0x8

    if-ne v0, v2, :cond_2

    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->cancellationFeeEditText:Lcom/squareup/noho/NohoEditText;

    invoke-virtual {v0}, Lcom/squareup/noho/NohoEditText;->getVisibility()I

    move-result v0

    if-ne v0, v2, :cond_2

    .line 250
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->priceTypeRow:Lcom/squareup/noho/NohoRow;

    .line 251
    invoke-virtual {v0}, Lcom/squareup/noho/NohoRow;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoLinearLayout$DividerLinearLayoutParams;

    .line 252
    iget v2, v0, Lcom/squareup/noho/NohoLinearLayout$DividerLinearLayoutParams;->leftMargin:I

    iget v3, v0, Lcom/squareup/noho/NohoLinearLayout$DividerLinearLayoutParams;->topMargin:I

    iget v4, v0, Lcom/squareup/noho/NohoLinearLayout$DividerLinearLayoutParams;->rightMargin:I

    invoke-virtual {v0, v2, v3, v4, v1}, Lcom/squareup/noho/NohoLinearLayout$DividerLinearLayoutParams;->setMargins(IIII)V

    .line 254
    iget-object v1, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->priceTypeRow:Lcom/squareup/noho/NohoRow;

    invoke-virtual {v1, v0}, Lcom/squareup/noho/NohoRow;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 257
    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->cancellationFeeEditText:Lcom/squareup/noho/NohoEditText;

    iget-object v1, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object p1, p1, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->defaultVariation:Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 258
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getNoShowFee()Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-interface {v1, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    .line 257
    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoEditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private setRateBasedServicesPriceContent(Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;)V
    .locals 6

    .line 359
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->priceTypeRow:Lcom/squareup/noho/NohoRow;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoRow;->setVisibility(I)V

    .line 360
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->priceEditText:Lcom/squareup/noho/NohoEditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditText;->setVisibility(I)V

    .line 361
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v1, p1, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->defaultVariation:Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getPrice()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 364
    iget-object v1, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->priceEditText:Lcom/squareup/noho/NohoEditText;

    invoke-static {v1}, Lcom/squareup/util/Views;->getValue(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 365
    iget-object v1, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->priceEditText:Lcom/squareup/noho/NohoEditText;

    invoke-virtual {v1, v0}, Lcom/squareup/noho/NohoEditText;->setText(Ljava/lang/CharSequence;)V

    .line 368
    :cond_0
    iget-object v0, p1, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->defaultVariation:Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getCatalogMeasurementUnitToken()Ljava/lang/String;

    move-result-object v0

    .line 370
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 371
    iget-object v1, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->res:Lcom/squareup/util/Res;

    iget-object v2, p1, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->measurementUnits:Ljava/util/Map;

    .line 372
    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    .line 371
    invoke-static {v1, v0}, Lcom/squareup/quantity/UnitDisplayData;->fromCatalogMeasurementUnit(Lcom/squareup/util/Res;Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;)Lcom/squareup/quantity/UnitDisplayData;

    move-result-object v0

    .line 373
    invoke-virtual {v0}, Lcom/squareup/quantity/UnitDisplayData;->getUnitAbbreviation()Ljava/lang/String;

    move-result-object v1

    .line 374
    iget-object v2, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->unitTypeSelector:Lcom/squareup/noho/NohoRow;

    iget-object v3, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/edititem/R$string;->edit_item_unit_type_value:I

    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v3

    .line 375
    invoke-virtual {v0}, Lcom/squareup/quantity/UnitDisplayData;->getUnitName()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "unit_name"

    invoke-virtual {v3, v5, v4}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->res:Lcom/squareup/util/Res;

    .line 376
    invoke-virtual {v0, v4}, Lcom/squareup/quantity/UnitDisplayData;->getQuantityPrecisionHint(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v0

    const-string v4, "precision"

    invoke-virtual {v3, v4, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 377
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 374
    invoke-virtual {v2, v0}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 380
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->unitTypeSelector:Lcom/squareup/noho/NohoRow;

    invoke-virtual {p0}, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/squareup/edititem/R$string;->edit_service_unit_type_default:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    const-string v1, ""

    .line 383
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->priceEditScrubbingWatcher:Lcom/squareup/text/ScrubbingTextWatcher;

    if-eqz v0, :cond_2

    .line 384
    iget-object v2, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->priceEditText:Lcom/squareup/noho/NohoEditText;

    invoke-virtual {v2, v0}, Lcom/squareup/noho/NohoEditText;->removeSelectionWatcher(Lcom/squareup/text/SelectionWatcher;)V

    .line 385
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->priceEditText:Lcom/squareup/noho/NohoEditText;

    iget-object v2, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->priceEditScrubbingWatcher:Lcom/squareup/text/ScrubbingTextWatcher;

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 387
    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    iget-object v2, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->priceEditText:Lcom/squareup/noho/NohoEditText;

    sget-object v3, Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;->BLANK_ON_ZERO:Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;

    invoke-virtual {v0, v2, v3, v1}, Lcom/squareup/money/PriceLocaleHelper;->configure(Lcom/squareup/text/HasSelectableText;Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;Ljava/lang/String;)Lcom/squareup/text/ScrubbingTextWatcher;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->priceEditScrubbingWatcher:Lcom/squareup/text/ScrubbingTextWatcher;

    .line 389
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->priceEditTextWatcher:Landroid/text/TextWatcher;

    if-eqz v0, :cond_3

    .line 390
    iget-object v2, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->priceEditText:Lcom/squareup/noho/NohoEditText;

    invoke-virtual {v2, v0}, Lcom/squareup/noho/NohoEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 392
    :cond_3
    new-instance v0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow$3;

    invoke-direct {v0, p0, v1}, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow$3;-><init>(Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->priceEditTextWatcher:Landroid/text/TextWatcher;

    .line 400
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->priceEditText:Lcom/squareup/noho/NohoEditText;

    iget-object v2, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->priceEditTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v0, v2}, Lcom/squareup/noho/NohoEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 401
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    iget-object v2, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->priceEditText:Lcom/squareup/noho/NohoEditText;

    invoke-virtual {v0, v2, v1}, Lcom/squareup/money/PriceLocaleHelper;->setHintToZeroMoney(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 402
    iget-object v0, p1, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->defaultVariation:Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->isVariablePricing()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 403
    iget-object p1, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->priceEditText:Lcom/squareup/noho/NohoEditText;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoEditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 405
    :cond_4
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->priceEditText:Lcom/squareup/noho/NohoEditText;

    iget-object v2, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->perUnitFormatter:Lcom/squareup/quantity/PerUnitFormatter;

    iget-object p1, p1, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->defaultVariation:Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getPrice()Lcom/squareup/protos/common/Money;

    move-result-object p1

    invoke-virtual {v2, p1, v1}, Lcom/squareup/quantity/PerUnitFormatter;->format(Lcom/squareup/protos/common/Money;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoEditText;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    return-void
.end method

.method private showOrHideGapTimeRows(ILcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;)V
    .locals 1

    .line 317
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->initialDurationRow:Lcom/squareup/noho/NohoRow;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoRow;->setVisibility(I)V

    .line 318
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->gapDurationRow:Lcom/squareup/noho/NohoRow;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoRow;->setVisibility(I)V

    .line 319
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->finalDurationRow:Lcom/squareup/noho/NohoRow;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoRow;->setVisibility(I)V

    if-nez p1, :cond_0

    .line 322
    iget-object p1, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->durationRow:Lcom/squareup/noho/NohoRow;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 324
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->durationRow:Lcom/squareup/noho/NohoRow;

    new-instance v0, Lcom/squareup/ui/items/-$$Lambda$EditServiceMainViewSingleVariationStaticRow$MbtO3NZ-rgA6-p3IBFqnWKl8Eew;

    invoke-direct {v0, p0, p2}, Lcom/squareup/ui/items/-$$Lambda$EditServiceMainViewSingleVariationStaticRow$MbtO3NZ-rgA6-p3IBFqnWKl8Eew;-><init>(Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;)V

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_0
    return-void
.end method

.method private unregisterListeners(Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;)V
    .locals 1

    .line 190
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->cannotShowExtraServiceOptions(Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;)Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    .line 193
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->gapTimeToggle:Lcom/squareup/noho/NohoCheckableRow;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoCheckableRow;->setOnCheckedChange(Lkotlin/jvm/functions/Function2;)V

    .line 194
    iget-object p1, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->extraTimeToggle:Lcom/squareup/noho/NohoCheckableRow;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoCheckableRow;->setOnCheckedChange(Lkotlin/jvm/functions/Function2;)V

    .line 195
    iget-object p1, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->bookableByCustomerToggle:Lcom/squareup/noho/NohoCheckableRow;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoCheckableRow;->setOnCheckedChange(Lkotlin/jvm/functions/Function2;)V

    return-void
.end method


# virtual methods
.method public synthetic lambda$onFinishInflate$0$EditServiceMainViewSingleVariationStaticRow(Landroid/view/View;)V
    .locals 1

    .line 105
    iget-object p1, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v0, Lcom/squareup/analytics/RegisterTapName;->SERVICES_PRICE_TYPE:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 106
    iget-object p1, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->presenter:Lcom/squareup/ui/items/EditItemMainPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditItemMainPresenter;->priceTypeButtonClicked()V

    return-void
.end method

.method public synthetic lambda$onFinishInflate$1$EditServiceMainViewSingleVariationStaticRow(Landroid/view/View;Z)V
    .locals 0

    .line 111
    iget-object p1, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->presenter:Lcom/squareup/ui/items/EditItemMainPresenter;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/items/EditItemMainPresenter;->priceFieldFocusChanged(Z)V

    return-void
.end method

.method public synthetic lambda$onFinishInflate$2$EditServiceMainViewSingleVariationStaticRow(Landroid/view/View;)V
    .locals 0

    .line 130
    iget-object p1, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->presenter:Lcom/squareup/ui/items/EditItemMainPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditItemMainPresenter;->unitPriceTypeButtonClicked()V

    return-void
.end method

.method public synthetic lambda$onFinishInflate$3$EditServiceMainViewSingleVariationStaticRow(Landroid/view/View;)V
    .locals 1

    .line 142
    iget-object p1, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v0, Lcom/squareup/analytics/RegisterTapName;->SERVICES_EMPLOYEE_SELECTION:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 143
    iget-object p1, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->presenter:Lcom/squareup/ui/items/EditItemMainPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditItemMainPresenter;->assignEmployeesClicked()V

    return-void
.end method

.method public synthetic lambda$onFinishInflate$4$EditServiceMainViewSingleVariationStaticRow(Landroid/view/View;Z)V
    .locals 0

    if-eqz p2, :cond_0

    .line 157
    iget-object p1, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object p2, Lcom/squareup/analytics/RegisterActionName;->SERVICE_EDIT_DISPLAY_PRICE:Lcom/squareup/analytics/RegisterActionName;

    invoke-interface {p1, p2}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    :cond_0
    return-void
.end method

.method public synthetic lambda$registerListeners$5$EditServiceMainViewSingleVariationStaticRow(Lcom/squareup/noho/NohoCheckableRow;Ljava/lang/Boolean;)Lkotlin/Unit;
    .locals 0

    .line 208
    iget-object p1, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->presenter:Lcom/squareup/ui/items/EditItemMainPresenter;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/items/EditItemMainPresenter;->gapTimeToggled(Z)Lkotlin/Unit;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$registerListeners$6$EditServiceMainViewSingleVariationStaticRow(Lcom/squareup/noho/NohoCheckableRow;Ljava/lang/Boolean;)Lkotlin/Unit;
    .locals 1

    .line 211
    iget-object p1, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v0, Lcom/squareup/analytics/RegisterTapName;->SERVICES_EXTRA_TIME:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 212
    iget-object p1, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->presenter:Lcom/squareup/ui/items/EditItemMainPresenter;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/items/EditItemMainPresenter;->blockExtraTimeToggled(Z)Lkotlin/Unit;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$registerListeners$7$EditServiceMainViewSingleVariationStaticRow(Lcom/squareup/noho/NohoCheckableRow;Ljava/lang/Boolean;)Lkotlin/Unit;
    .locals 2

    .line 216
    iget-object p1, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v0, Lcom/squareup/ui/items/BookableOnlineActionEvent;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-direct {v0, v1}, Lcom/squareup/ui/items/BookableOnlineActionEvent;-><init>(Z)V

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 217
    iget-object p1, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->presenter:Lcom/squareup/ui/items/EditItemMainPresenter;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/items/EditItemMainPresenter;->bookableByCustomerToggled(Z)Lkotlin/Unit;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$setDurationContent$10$EditServiceMainViewSingleVariationStaticRow(Landroid/view/View;)V
    .locals 0

    .line 298
    iget-object p1, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->presenter:Lcom/squareup/ui/items/EditItemMainPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditItemMainPresenter;->finalDurationChanged()V

    return-void
.end method

.method public synthetic lambda$setDurationContent$11$EditServiceMainViewSingleVariationStaticRow(Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;Landroid/view/View;)V
    .locals 2

    .line 308
    iget-object p2, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->presenter:Lcom/squareup/ui/items/EditItemMainPresenter;

    iget-object p1, p1, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->defaultVariation:Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 309
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getTransitionTime()J

    move-result-wide v0

    invoke-static {v0, v1}, Lorg/threeten/bp/Duration;->ofMillis(J)Lorg/threeten/bp/Duration;

    move-result-object p1

    .line 308
    invoke-virtual {p2, p1}, Lcom/squareup/ui/items/EditItemMainPresenter;->transitionTimeChanged(Lorg/threeten/bp/Duration;)V

    return-void
.end method

.method public synthetic lambda$setDurationContent$8$EditServiceMainViewSingleVariationStaticRow(Landroid/view/View;)V
    .locals 0

    .line 296
    iget-object p1, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->presenter:Lcom/squareup/ui/items/EditItemMainPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditItemMainPresenter;->initialDurationChanged()V

    return-void
.end method

.method public synthetic lambda$setDurationContent$9$EditServiceMainViewSingleVariationStaticRow(Landroid/view/View;)V
    .locals 0

    .line 297
    iget-object p1, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->presenter:Lcom/squareup/ui/items/EditItemMainPresenter;

    invoke-virtual {p1}, Lcom/squareup/ui/items/EditItemMainPresenter;->gapDurationChanged()V

    return-void
.end method

.method public synthetic lambda$showOrHideGapTimeRows$12$EditServiceMainViewSingleVariationStaticRow(Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;Landroid/view/View;)V
    .locals 2

    .line 324
    iget-object p2, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->presenter:Lcom/squareup/ui/items/EditItemMainPresenter;

    iget-object p1, p1, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->defaultVariation:Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 325
    invoke-virtual {p1}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getDuration()J

    move-result-wide v0

    invoke-static {v0, v1}, Lorg/threeten/bp/Duration;->ofMillis(J)Lorg/threeten/bp/Duration;

    move-result-object p1

    .line 324
    invoke-virtual {p2, p1}, Lcom/squareup/ui/items/EditItemMainPresenter;->durationRowClicked(Lorg/threeten/bp/Duration;)V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 5

    .line 95
    invoke-super {p0}, Lcom/squareup/widgets/ResponsiveLinearLayout;->onFinishInflate()V

    .line 97
    invoke-direct {p0}, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->createReadableDisplayPrices()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->readableDisplayPrices:Ljava/util/Map;

    .line 99
    sget v0, Lcom/squareup/edititem/R$id;->variation_header:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->variationHeader:Lcom/squareup/marketfont/MarketTextView;

    .line 100
    sget v0, Lcom/squareup/edititem/R$id;->edit_service_main_view_default_variation_container:I

    .line 101
    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->defaultVariationContainer:Landroid/view/View;

    .line 103
    sget v0, Lcom/squareup/edititem/R$id;->price_type_row:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoRow;

    iput-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->priceTypeRow:Lcom/squareup/noho/NohoRow;

    .line 104
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->priceTypeRow:Lcom/squareup/noho/NohoRow;

    new-instance v1, Lcom/squareup/ui/items/-$$Lambda$EditServiceMainViewSingleVariationStaticRow$3z4Wi4Y_5mLU30MU_ATfh29nCTU;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/-$$Lambda$EditServiceMainViewSingleVariationStaticRow$3z4Wi4Y_5mLU30MU_ATfh29nCTU;-><init>(Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;)V

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 109
    sget v0, Lcom/squareup/edititem/R$id;->price_row:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoEditText;

    iput-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->priceEditText:Lcom/squareup/noho/NohoEditText;

    .line 110
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->priceEditText:Lcom/squareup/noho/NohoEditText;

    new-instance v1, Lcom/squareup/ui/items/-$$Lambda$EditServiceMainViewSingleVariationStaticRow$lElWjDcWFXQ2z1UboYLLuxvKZeg;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/-$$Lambda$EditServiceMainViewSingleVariationStaticRow$lElWjDcWFXQ2z1UboYLLuxvKZeg;-><init>(Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;)V

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 113
    sget v0, Lcom/squareup/edititem/R$id;->cancellation_fee_row:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoEditText;

    iput-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->cancellationFeeEditText:Lcom/squareup/noho/NohoEditText;

    .line 114
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    iget-object v1, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->cancellationFeeEditText:Lcom/squareup/noho/NohoEditText;

    sget-object v2, Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;->BLANK_ON_ZERO:Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;

    .line 115
    invoke-virtual {v0, v1, v2}, Lcom/squareup/money/PriceLocaleHelper;->configure(Lcom/squareup/text/HasSelectableText;Lcom/squareup/money/WholeUnitAmountScrubber$ZeroState;)Lcom/squareup/text/ScrubbingTextWatcher;

    move-result-object v0

    new-instance v1, Lcom/squareup/money/MaxMoneyScrubber;

    iget-object v2, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    iget-object v3, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v4, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->presenter:Lcom/squareup/ui/items/EditItemMainPresenter;

    .line 117
    invoke-virtual {v4}, Lcom/squareup/ui/items/EditItemMainPresenter;->maxCancellationFeeMoney()Lcom/squareup/protos/common/Money;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/money/MaxMoneyScrubber;-><init>(Lcom/squareup/money/MoneyExtractor;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/Money;)V

    .line 116
    invoke-virtual {v0, v1}, Lcom/squareup/text/ScrubbingTextWatcher;->addScrubber(Lcom/squareup/text/Scrubber;)V

    .line 118
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->priceLocaleHelper:Lcom/squareup/money/PriceLocaleHelper;

    iget-object v1, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->cancellationFeeEditText:Lcom/squareup/noho/NohoEditText;

    invoke-virtual {v0, v1}, Lcom/squareup/money/PriceLocaleHelper;->setHintToZeroMoney(Landroid/widget/TextView;)V

    .line 119
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->cancellationFeeEditText:Lcom/squareup/noho/NohoEditText;

    new-instance v1, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow$1;-><init>(Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;)V

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 126
    sget v0, Lcom/squareup/edititem/R$id;->edit_service_duration_row:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoRow;

    iput-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->durationRow:Lcom/squareup/noho/NohoRow;

    .line 127
    sget v0, Lcom/squareup/edititem/R$id;->edit_service_duration_extra_time_toggle:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoCheckableRow;

    iput-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->extraTimeToggle:Lcom/squareup/noho/NohoCheckableRow;

    .line 129
    sget v0, Lcom/squareup/edititem/R$id;->unit_type_row:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoRow;

    iput-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->unitTypeSelector:Lcom/squareup/noho/NohoRow;

    .line 130
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->unitTypeSelector:Lcom/squareup/noho/NohoRow;

    new-instance v1, Lcom/squareup/ui/items/-$$Lambda$EditServiceMainViewSingleVariationStaticRow$DtGA9VtYe_D4_p7ePKuWHUkTvC4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/-$$Lambda$EditServiceMainViewSingleVariationStaticRow$DtGA9VtYe_D4_p7ePKuWHUkTvC4;-><init>(Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;)V

    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 132
    sget v0, Lcom/squareup/edititem/R$id;->edit_service_gap_time_toggle:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoCheckableRow;

    iput-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->gapTimeToggle:Lcom/squareup/noho/NohoCheckableRow;

    .line 133
    sget v0, Lcom/squareup/edititem/R$id;->edit_service_initial_duration:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoRow;

    iput-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->initialDurationRow:Lcom/squareup/noho/NohoRow;

    .line 134
    sget v0, Lcom/squareup/edititem/R$id;->edit_service_gap_duration:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoRow;

    iput-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->gapDurationRow:Lcom/squareup/noho/NohoRow;

    .line 135
    sget v0, Lcom/squareup/edititem/R$id;->edit_service_final_duration:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoRow;

    iput-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->finalDurationRow:Lcom/squareup/noho/NohoRow;

    .line 137
    sget v0, Lcom/squareup/edititem/R$id;->edit_service_after_appointment_row:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoRow;

    iput-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->afterAppointmentRow:Lcom/squareup/noho/NohoRow;

    .line 138
    sget v0, Lcom/squareup/edititem/R$id;->extra_time_helper_text:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoMessageView;

    iput-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->extraTimeHelperText:Lcom/squareup/noho/NohoMessageView;

    .line 139
    sget v0, Lcom/squareup/edititem/R$id;->bookable_by_customers_online:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoCheckableRow;

    iput-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->bookableByCustomerToggle:Lcom/squareup/noho/NohoCheckableRow;

    .line 140
    sget v0, Lcom/squareup/edititem/R$id;->assigned_employees:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoRow;

    iput-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->assignedEmployeesRow:Lcom/squareup/noho/NohoRow;

    .line 141
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->assignedEmployeesRow:Lcom/squareup/noho/NohoRow;

    new-instance v1, Lcom/squareup/ui/items/-$$Lambda$EditServiceMainViewSingleVariationStaticRow$H1GuvEwc91ib2EC3UGIkxSGb9SI;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/-$$Lambda$EditServiceMainViewSingleVariationStaticRow$H1GuvEwc91ib2EC3UGIkxSGb9SI;-><init>(Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;)V

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 146
    sget v0, Lcom/squareup/edititem/R$id;->display_price_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->displayPriceContainer:Landroid/view/ViewGroup;

    .line 147
    sget v0, Lcom/squareup/edititem/R$id;->display_price_row:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoEditText;

    iput-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->displayPriceEditText:Lcom/squareup/noho/NohoEditText;

    .line 148
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->displayPriceEditText:Lcom/squareup/noho/NohoEditText;

    new-instance v1, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow$2;-><init>(Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;)V

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 155
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->displayPriceEditText:Lcom/squareup/noho/NohoEditText;

    new-instance v1, Lcom/squareup/ui/items/-$$Lambda$EditServiceMainViewSingleVariationStaticRow$6Bi4KDI7uPdbdAPQ5B0rP17LBDs;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/-$$Lambda$EditServiceMainViewSingleVariationStaticRow$6Bi4KDI7uPdbdAPQ5B0rP17LBDs;-><init>(Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;)V

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    return-void
.end method

.method showContent(Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;)V
    .locals 2

    .line 163
    iget-boolean v0, p1, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->shouldShowInlineVariation:Z

    if-nez v0, :cond_0

    .line 164
    iget-object p1, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->variationHeader:Lcom/squareup/marketfont/MarketTextView;

    sget v0, Lcom/squareup/edititem/R$string;->uppercase_variations:I

    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketTextView;->setText(I)V

    .line 165
    iget-object p1, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->defaultVariationContainer:Landroid/view/View;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void

    .line 168
    :cond_0
    iget-boolean v0, p1, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->enableRateBasedServices:Z

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;->defaultVariation:Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;

    .line 169
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogItemVariation$Builder;->getCatalogMeasurementUnitToken()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 170
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->variationHeader:Lcom/squareup/marketfont/MarketTextView;

    sget v1, Lcom/squareup/edititem/R$string;->uppercase_price:I

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setText(I)V

    goto :goto_0

    .line 172
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->variationHeader:Lcom/squareup/marketfont/MarketTextView;

    sget v1, Lcom/squareup/edititem/R$string;->uppercase_price_and_duration:I

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setText(I)V

    .line 174
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->defaultVariationContainer:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 176
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->unregisterListeners(Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;)V

    .line 177
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->setPriceContent(Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;)V

    .line 178
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->setDurationContent(Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;)V

    .line 179
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->setBookableContent(Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;)V

    .line 180
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/EditServiceMainViewSingleVariationStaticRow;->registerListeners(Lcom/squareup/ui/items/EditItemMainPresenter$EditItemMainScreenData;)V

    return-void
.end method
