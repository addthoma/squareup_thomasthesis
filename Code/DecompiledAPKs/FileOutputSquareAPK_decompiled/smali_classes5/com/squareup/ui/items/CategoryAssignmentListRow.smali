.class public Lcom/squareup/ui/items/CategoryAssignmentListRow;
.super Lcom/squareup/marin/widgets/BorderedLinearLayout;
.source "CategoryAssignmentListRow.java"

# interfaces
.implements Lcom/squareup/picasso/Target;
.implements Landroid/widget/Checkable;
.implements Lcom/squareup/ui/items/EditCategoryView$CategoryNameChangeListener;


# instance fields
.field private categoryNameView:Landroid/widget/TextView;

.field private checkbox:Landroid/widget/CompoundButton;

.field private final colorStrip:Lcom/squareup/marin/widgets/BorderPainter;

.field private isChecked:Z

.field private isTextTile:Z

.field private itemPhoto:Lcom/squareup/ui/photo/ItemPhoto;

.field private final marginWithColorStrip:I

.field private nameView:Landroid/widget/TextView;

.field private final selectedColor:I

.field private final thumbSize:I

.field private thumbnail:Landroid/widget/ImageView;

.field private final transitionTime:I

.field private final unselectedColor:I

.field private useUnselectedColor:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .line 55
    invoke-direct {p0, p1, p2}, Lcom/squareup/marin/widgets/BorderedLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 56
    invoke-virtual {p0}, Lcom/squareup/ui/items/CategoryAssignmentListRow;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const/high16 p2, 0x10e0000

    .line 57
    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result p2

    iput p2, p0, Lcom/squareup/ui/items/CategoryAssignmentListRow;->transitionTime:I

    const/4 p2, 0x1

    .line 58
    iput-boolean p2, p0, Lcom/squareup/ui/items/CategoryAssignmentListRow;->useUnselectedColor:Z

    .line 59
    sget v0, Lcom/squareup/marin/R$color;->marin_light_gray:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/squareup/ui/items/CategoryAssignmentListRow;->unselectedColor:I

    .line 60
    sget v0, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/squareup/ui/items/CategoryAssignmentListRow;->selectedColor:I

    .line 61
    sget v0, Lcom/squareup/marin/R$dimen;->marin_min_height:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/squareup/ui/items/CategoryAssignmentListRow;->thumbSize:I

    .line 62
    sget v0, Lcom/squareup/marin/R$dimen;->marin_divider_width_1px:I

    .line 63
    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 62
    invoke-virtual {p0, v0}, Lcom/squareup/ui/items/CategoryAssignmentListRow;->setBorderWidth(I)V

    .line 64
    sget v0, Lcom/squareup/marin/R$dimen;->marin_gutter_half:I

    .line 65
    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 64
    invoke-virtual {p0, v0}, Lcom/squareup/ui/items/CategoryAssignmentListRow;->setHorizontalBordersRightInset(I)V

    const/16 v0, 0x8

    .line 66
    invoke-virtual {p0, v0}, Lcom/squareup/ui/items/CategoryAssignmentListRow;->addBorder(I)V

    .line 67
    new-instance v0, Lcom/squareup/marin/widgets/BorderPainter;

    sget v1, Lcom/squareup/marin/R$dimen;->marin_text_tile_color_block_width:I

    invoke-direct {v0, p0, v1}, Lcom/squareup/marin/widgets/BorderPainter;-><init>(Landroid/view/View;I)V

    iput-object v0, p0, Lcom/squareup/ui/items/CategoryAssignmentListRow;->colorStrip:Lcom/squareup/marin/widgets/BorderPainter;

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/items/CategoryAssignmentListRow;->colorStrip:Lcom/squareup/marin/widgets/BorderPainter;

    invoke-virtual {v0, p2}, Lcom/squareup/marin/widgets/BorderPainter;->addBorder(I)V

    .line 70
    sget p2, Lcom/squareup/marin/R$dimen;->marin_gap_thumbnail_content:I

    .line 71
    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p2

    sget v0, Lcom/squareup/marin/R$dimen;->marin_text_tile_color_block_width:I

    .line 72
    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    add-int/2addr p2, p1

    iput p2, p0, Lcom/squareup/ui/items/CategoryAssignmentListRow;->marginWithColorStrip:I

    return-void
.end method

.method private setCategoryName(Ljava/lang/String;)V
    .locals 2

    .line 178
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 179
    iget-object p1, p0, Lcom/squareup/ui/items/CategoryAssignmentListRow;->categoryNameView:Landroid/widget/TextView;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 181
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/items/CategoryAssignmentListRow;->categoryNameView:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 182
    iget-object v0, p0, Lcom/squareup/ui/items/CategoryAssignmentListRow;->categoryNameView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void
.end method


# virtual methods
.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .line 128
    iget-boolean v0, p0, Lcom/squareup/ui/items/CategoryAssignmentListRow;->isTextTile:Z

    if-eqz v0, :cond_0

    .line 129
    iget-object v0, p0, Lcom/squareup/ui/items/CategoryAssignmentListRow;->colorStrip:Lcom/squareup/marin/widgets/BorderPainter;

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/BorderPainter;->drawBorders(Landroid/graphics/Canvas;)V

    .line 131
    :cond_0
    invoke-super {p0, p1}, Lcom/squareup/marin/widgets/BorderedLinearLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method public isChecked()Z
    .locals 1

    .line 120
    iget-boolean v0, p0, Lcom/squareup/ui/items/CategoryAssignmentListRow;->isChecked:Z

    return v0
.end method

.method public onBitmapFailed(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    return-void
.end method

.method public onBitmapLoaded(Landroid/graphics/Bitmap;Lcom/squareup/picasso/Picasso$LoadedFrom;)V
    .locals 3

    .line 85
    sget-object v0, Lcom/squareup/picasso/Picasso$LoadedFrom;->MEMORY:Lcom/squareup/picasso/Picasso$LoadedFrom;

    if-ne p2, v0, :cond_0

    .line 86
    iget-object p2, p0, Lcom/squareup/ui/items/CategoryAssignmentListRow;->thumbnail:Landroid/widget/ImageView;

    invoke-virtual {p2, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    return-void

    .line 92
    :cond_0
    iget-object p2, p0, Lcom/squareup/ui/items/CategoryAssignmentListRow;->thumbnail:Landroid/widget/ImageView;

    invoke-virtual {p2}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object p2

    if-nez p2, :cond_1

    return-void

    :cond_1
    const/4 v0, 0x2

    new-array v0, v0, [Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    const/4 p2, 0x1

    .line 97
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/squareup/ui/items/CategoryAssignmentListRow;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2, p1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    aput-object v1, v0, p2

    .line 98
    new-instance p1, Landroid/graphics/drawable/TransitionDrawable;

    invoke-direct {p1, v0}, Landroid/graphics/drawable/TransitionDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 99
    iget-object p2, p0, Lcom/squareup/ui/items/CategoryAssignmentListRow;->thumbnail:Landroid/widget/ImageView;

    invoke-virtual {p2, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 100
    iget p2, p0, Lcom/squareup/ui/items/CategoryAssignmentListRow;->transitionTime:I

    invoke-virtual {p1, p2}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    return-void
.end method

.method public onCurrentCategoryNameChanged(Ljava/lang/String;)V
    .locals 0

    .line 112
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/CategoryAssignmentListRow;->setCategoryName(Ljava/lang/String;)V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 77
    invoke-super {p0}, Lcom/squareup/marin/widgets/BorderedLinearLayout;->onFinishInflate()V

    .line 78
    sget v0, Lcom/squareup/itemsapplet/R$id;->name:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/items/CategoryAssignmentListRow;->nameView:Landroid/widget/TextView;

    .line 79
    sget v0, Lcom/squareup/itemsapplet/R$id;->thumbnail:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/squareup/ui/items/CategoryAssignmentListRow;->thumbnail:Landroid/widget/ImageView;

    .line 80
    sget v0, Lcom/squareup/itemsapplet/R$id;->category_name:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/items/CategoryAssignmentListRow;->categoryNameView:Landroid/widget/TextView;

    .line 81
    sget v0, Lcom/squareup/itemsapplet/R$id;->checkbox:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CompoundButton;

    iput-object v0, p0, Lcom/squareup/ui/items/CategoryAssignmentListRow;->checkbox:Landroid/widget/CompoundButton;

    return-void
.end method

.method public onPrepareLoad(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    return-void
.end method

.method public setChecked(Z)V
    .locals 0

    .line 116
    iput-boolean p1, p0, Lcom/squareup/ui/items/CategoryAssignmentListRow;->isChecked:Z

    return-void
.end method

.method public setUseUnselectedColor(Z)V
    .locals 0

    .line 174
    iput-boolean p1, p0, Lcom/squareup/ui/items/CategoryAssignmentListRow;->useUnselectedColor:Z

    return-void
.end method

.method public showItem(Ljava/lang/String;Lcom/squareup/ui/photo/ItemPhoto$Factory;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 1

    .line 138
    iput-boolean p8, p0, Lcom/squareup/ui/items/CategoryAssignmentListRow;->isChecked:Z

    .line 139
    iput-boolean p9, p0, Lcom/squareup/ui/items/CategoryAssignmentListRow;->isTextTile:Z

    .line 141
    iget-object v0, p0, Lcom/squareup/ui/items/CategoryAssignmentListRow;->itemPhoto:Lcom/squareup/ui/photo/ItemPhoto;

    if-eqz v0, :cond_0

    .line 142
    invoke-virtual {v0, p0}, Lcom/squareup/ui/photo/ItemPhoto;->cancel(Lcom/squareup/picasso/Target;)V

    const/4 v0, 0x0

    .line 143
    iput-object v0, p0, Lcom/squareup/ui/items/CategoryAssignmentListRow;->itemPhoto:Lcom/squareup/ui/photo/ItemPhoto;

    .line 146
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/items/CategoryAssignmentListRow;->nameView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 147
    invoke-virtual {p0}, Lcom/squareup/ui/items/CategoryAssignmentListRow;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p5, p6, p1}, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->forItem(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Lcom/squareup/register/widgets/ItemPlaceholderDrawable;

    move-result-object p1

    .line 148
    iget p5, p0, Lcom/squareup/ui/items/CategoryAssignmentListRow;->thumbSize:I

    invoke-virtual {p1, p5, p5}, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->setSize(II)V

    .line 149
    iget-object p5, p0, Lcom/squareup/ui/items/CategoryAssignmentListRow;->thumbnail:Landroid/widget/ImageView;

    invoke-virtual {p5, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 150
    invoke-virtual {p2, p3, p4}, Lcom/squareup/ui/photo/ItemPhoto$Factory;->get(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/ui/photo/ItemPhoto;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/items/CategoryAssignmentListRow;->itemPhoto:Lcom/squareup/ui/photo/ItemPhoto;

    .line 151
    iget-object p1, p0, Lcom/squareup/ui/items/CategoryAssignmentListRow;->itemPhoto:Lcom/squareup/ui/photo/ItemPhoto;

    iget p2, p0, Lcom/squareup/ui/items/CategoryAssignmentListRow;->thumbSize:I

    invoke-virtual {p1, p2, p0}, Lcom/squareup/ui/photo/ItemPhoto;->into(ILcom/squareup/picasso/Target;)V

    .line 153
    invoke-direct {p0, p7}, Lcom/squareup/ui/items/CategoryAssignmentListRow;->setCategoryName(Ljava/lang/String;)V

    .line 155
    iget-object p1, p0, Lcom/squareup/ui/items/CategoryAssignmentListRow;->categoryNameView:Landroid/widget/TextView;

    iget-boolean p2, p0, Lcom/squareup/ui/items/CategoryAssignmentListRow;->useUnselectedColor:Z

    if-eqz p2, :cond_1

    if-nez p8, :cond_1

    iget p2, p0, Lcom/squareup/ui/items/CategoryAssignmentListRow;->unselectedColor:I

    goto :goto_0

    :cond_1
    iget p2, p0, Lcom/squareup/ui/items/CategoryAssignmentListRow;->selectedColor:I

    :goto_0
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 158
    iget-object p1, p0, Lcom/squareup/ui/items/CategoryAssignmentListRow;->checkbox:Landroid/widget/CompoundButton;

    invoke-virtual {p1, p8}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 160
    iget-object p1, p0, Lcom/squareup/ui/items/CategoryAssignmentListRow;->thumbnail:Landroid/widget/ImageView;

    xor-int/lit8 p2, p9, 0x1

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 161
    iget-object p1, p0, Lcom/squareup/ui/items/CategoryAssignmentListRow;->colorStrip:Lcom/squareup/marin/widgets/BorderPainter;

    invoke-virtual {p0}, Lcom/squareup/ui/items/CategoryAssignmentListRow;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    invoke-static {p2}, Lcom/squareup/register/widgets/ItemPlaceholderDrawable;->defaultItemColor(Landroid/content/res/Resources;)I

    move-result p2

    invoke-static {p6, p2}, Lcom/squareup/util/Colors;->parseHex(Ljava/lang/String;I)I

    move-result p2

    invoke-virtual {p1, p2}, Lcom/squareup/marin/widgets/BorderPainter;->setColor(I)V

    if-eqz p9, :cond_2

    .line 165
    iget-object p1, p0, Lcom/squareup/ui/items/CategoryAssignmentListRow;->nameView:Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout$LayoutParams;

    .line 166
    iget p2, p1, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    iget p3, p0, Lcom/squareup/ui/items/CategoryAssignmentListRow;->marginWithColorStrip:I

    if-eq p2, p3, :cond_2

    .line 167
    iput p3, p1, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 168
    iget-object p2, p0, Lcom/squareup/ui/items/CategoryAssignmentListRow;->nameView:Landroid/widget/TextView;

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_2
    return-void
.end method

.method public toggle()V
    .locals 1

    .line 124
    iget-boolean v0, p0, Lcom/squareup/ui/items/CategoryAssignmentListRow;->isChecked:Z

    xor-int/lit8 v0, v0, 0x1

    iput-boolean v0, p0, Lcom/squareup/ui/items/CategoryAssignmentListRow;->isChecked:Z

    return-void
.end method
