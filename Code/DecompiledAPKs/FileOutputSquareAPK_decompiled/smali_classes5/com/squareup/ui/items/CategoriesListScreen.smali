.class public final Lcom/squareup/ui/items/CategoriesListScreen;
.super Lcom/squareup/ui/items/InItemsAppletScope;
.source "CategoriesListScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/layer/InSection;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/items/CategoriesListScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/CategoriesListScreen$Component;,
        Lcom/squareup/ui/items/CategoriesListScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/items/CategoriesListScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/items/CategoriesListScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 34
    new-instance v0, Lcom/squareup/ui/items/CategoriesListScreen;

    invoke-direct {v0}, Lcom/squareup/ui/items/CategoriesListScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/items/CategoriesListScreen;->INSTANCE:Lcom/squareup/ui/items/CategoriesListScreen;

    .line 108
    sget-object v0, Lcom/squareup/ui/items/CategoriesListScreen;->INSTANCE:Lcom/squareup/ui/items/CategoriesListScreen;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/items/CategoriesListScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/squareup/ui/items/InItemsAppletScope;-><init>()V

    return-void
.end method


# virtual methods
.method public getSection()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 40
    const-class v0, Lcom/squareup/ui/items/ItemsAppletSection$Categories;

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 111
    sget v0, Lcom/squareup/itemsapplet/R$layout;->categories_list_view:I

    return v0
.end method
