.class Lcom/squareup/ui/items/RecyclerViewDragController$1;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "RecyclerViewDragController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/RecyclerViewDragController;-><init>(Landroidx/recyclerview/widget/RecyclerView;Landroid/widget/ImageView;Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/items/RecyclerViewDragController;

.field final synthetic val$adapter:Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;

.field final synthetic val$dragHandleResourceId:I

.field final synthetic val$recyclerView:Landroidx/recyclerview/widget/RecyclerView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/items/RecyclerViewDragController;Landroidx/recyclerview/widget/RecyclerView;Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;I)V
    .locals 0

    .line 39
    iput-object p1, p0, Lcom/squareup/ui/items/RecyclerViewDragController$1;->this$0:Lcom/squareup/ui/items/RecyclerViewDragController;

    iput-object p2, p0, Lcom/squareup/ui/items/RecyclerViewDragController$1;->val$recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    iput-object p3, p0, Lcom/squareup/ui/items/RecyclerViewDragController$1;->val$adapter:Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;

    iput p4, p0, Lcom/squareup/ui/items/RecyclerViewDragController$1;->val$dragHandleResourceId:I

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 7

    .line 41
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 42
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result p1

    .line 44
    iget-object v1, p0, Lcom/squareup/ui/items/RecyclerViewDragController$1;->val$recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v1, v0, p1}, Landroidx/recyclerview/widget/RecyclerView;->findChildViewUnder(FF)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    if-nez v1, :cond_0

    return v2

    .line 47
    :cond_0
    iget-object v3, p0, Lcom/squareup/ui/items/RecyclerViewDragController$1;->val$recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v3, v1}, Landroidx/recyclerview/widget/RecyclerView;->getChildPosition(Landroid/view/View;)I

    move-result v3

    iget-object v4, p0, Lcom/squareup/ui/items/RecyclerViewDragController$1;->val$adapter:Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;

    .line 48
    invoke-virtual {v4}, Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;->getItemCount()I

    move-result v4

    iget-object v5, p0, Lcom/squareup/ui/items/RecyclerViewDragController$1;->val$adapter:Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;

    invoke-virtual {v5}, Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;->getStaticBottomRowsCount()I

    move-result v5

    sub-int/2addr v4, v5

    if-lt v3, v4, :cond_1

    return v2

    .line 52
    :cond_1
    iget v3, p0, Lcom/squareup/ui/items/RecyclerViewDragController$1;->val$dragHandleResourceId:I

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    if-nez v3, :cond_2

    return v2

    .line 56
    :cond_2
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v4

    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v5

    add-int/2addr v4, v5

    .line 57
    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v5

    add-int/2addr v5, v4

    .line 58
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v1

    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v6

    add-int/2addr v1, v6

    .line 59
    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v3

    add-int/2addr v3, v1

    int-to-float v1, v1

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_3

    int-to-float v1, v3

    cmpg-float v1, v0, v1

    if-gtz v1, :cond_3

    int-to-float v1, v4

    cmpl-float v1, p1, v1

    if-ltz v1, :cond_3

    int-to-float v1, v5

    cmpg-float v1, p1, v1

    if-gtz v1, :cond_3

    .line 63
    iget-object v1, p0, Lcom/squareup/ui/items/RecyclerViewDragController$1;->this$0:Lcom/squareup/ui/items/RecyclerViewDragController;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/squareup/ui/items/RecyclerViewDragController;->access$002(Lcom/squareup/ui/items/RecyclerViewDragController;Z)Z

    .line 64
    iget-object v1, p0, Lcom/squareup/ui/items/RecyclerViewDragController$1;->this$0:Lcom/squareup/ui/items/RecyclerViewDragController;

    invoke-static {v1, v0, p1}, Lcom/squareup/ui/items/RecyclerViewDragController;->access$100(Lcom/squareup/ui/items/RecyclerViewDragController;FF)V

    :cond_3
    return v2
.end method
