.class public final Lcom/squareup/ui/items/UnitRow;
.super Lcom/squareup/ui/items/DetailSearchableListRow$DetailSearchableObjectRow$DetailSearchableConnectV2ObjectRow;
.source "DetailSearchableListRow.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDetailSearchableListRow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DetailSearchableListRow.kt\ncom/squareup/ui/items/UnitRow\n*L\n1#1,179:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000b\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0007H\u00c6\u0003J\u0010\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0015H\u0016J\'\u0010\u0016\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007H\u00c6\u0001J\u0013\u0010\u0017\u001a\u00020\u00182\u0008\u0010\u0019\u001a\u0004\u0018\u00010\u001aH\u00d6\u0003J\t\u0010\u001b\u001a\u00020\u001cH\u00d6\u0001J\t\u0010\u001d\u001a\u00020\u001eH\u00d6\u0001R\u0014\u0010\u0004\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000e\u00a8\u0006\u001f"
    }
    d2 = {
        "Lcom/squareup/ui/items/UnitRow;",
        "Lcom/squareup/ui/items/DetailSearchableListRow$DetailSearchableObjectRow$DetailSearchableConnectV2ObjectRow;",
        "screen",
        "Lcom/squareup/ui/items/DetailSearchableListScreen;",
        "res",
        "Lcom/squareup/util/Res;",
        "unit",
        "Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$MeasurementUnit;",
        "(Lcom/squareup/ui/items/DetailSearchableListScreen;Lcom/squareup/util/Res;Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$MeasurementUnit;)V",
        "getRes",
        "()Lcom/squareup/util/Res;",
        "getScreen",
        "()Lcom/squareup/ui/items/DetailSearchableListScreen;",
        "getUnit",
        "()Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$MeasurementUnit;",
        "component1",
        "component2",
        "component3",
        "configureRow",
        "",
        "row",
        "Lcom/squareup/noho/NohoRow;",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "items-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final res:Lcom/squareup/util/Res;

.field private final screen:Lcom/squareup/ui/items/DetailSearchableListScreen;

.field private final unit:Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$MeasurementUnit;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/items/DetailSearchableListScreen;Lcom/squareup/util/Res;Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$MeasurementUnit;)V
    .locals 1

    const-string v0, "screen"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "unit"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 93
    move-object v0, p3

    check-cast v0, Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/ui/items/DetailSearchableListRow$DetailSearchableObjectRow$DetailSearchableConnectV2ObjectRow;-><init>(Lcom/squareup/ui/items/DetailSearchableListScreen;Lcom/squareup/util/Res;Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;)V

    iput-object p1, p0, Lcom/squareup/ui/items/UnitRow;->screen:Lcom/squareup/ui/items/DetailSearchableListScreen;

    iput-object p2, p0, Lcom/squareup/ui/items/UnitRow;->res:Lcom/squareup/util/Res;

    iput-object p3, p0, Lcom/squareup/ui/items/UnitRow;->unit:Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$MeasurementUnit;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/items/UnitRow;Lcom/squareup/ui/items/DetailSearchableListScreen;Lcom/squareup/util/Res;Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$MeasurementUnit;ILjava/lang/Object;)Lcom/squareup/ui/items/UnitRow;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    invoke-virtual {p0}, Lcom/squareup/ui/items/UnitRow;->getScreen()Lcom/squareup/ui/items/DetailSearchableListScreen;

    move-result-object p1

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    invoke-virtual {p0}, Lcom/squareup/ui/items/UnitRow;->getRes()Lcom/squareup/util/Res;

    move-result-object p2

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/squareup/ui/items/UnitRow;->unit:Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$MeasurementUnit;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/items/UnitRow;->copy(Lcom/squareup/ui/items/DetailSearchableListScreen;Lcom/squareup/util/Res;Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$MeasurementUnit;)Lcom/squareup/ui/items/UnitRow;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/ui/items/DetailSearchableListScreen;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/ui/items/UnitRow;->getScreen()Lcom/squareup/ui/items/DetailSearchableListScreen;

    move-result-object v0

    return-object v0
.end method

.method public final component2()Lcom/squareup/util/Res;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/ui/items/UnitRow;->getRes()Lcom/squareup/util/Res;

    move-result-object v0

    return-object v0
.end method

.method public final component3()Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$MeasurementUnit;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/items/UnitRow;->unit:Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$MeasurementUnit;

    return-object v0
.end method

.method public configureRow(Lcom/squareup/noho/NohoRow;)V
    .locals 5

    const-string v0, "row"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 95
    iget-object v0, p0, Lcom/squareup/ui/items/UnitRow;->unit:Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$MeasurementUnit;

    invoke-virtual {v0}, Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$MeasurementUnit;->getMeasurementUnit()Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;

    move-result-object v0

    .line 96
    invoke-virtual {p0}, Lcom/squareup/ui/items/UnitRow;->getRes()Lcom/squareup/util/Res;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/quantity/UnitDisplayDataKt;->getUnitDisplayData(Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;Lcom/squareup/util/Res;)Lcom/squareup/quantity/UnitDisplayData;

    move-result-object v1

    .line 97
    invoke-virtual {v1}, Lcom/squareup/quantity/UnitDisplayData;->getUnitAbbreviation()Ljava/lang/String;

    move-result-object v2

    .line 100
    move-object v3, v2

    check-cast v3, Ljava/lang/CharSequence;

    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-nez v3, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    :goto_0
    if-eqz v3, :cond_1

    invoke-virtual {v1}, Lcom/squareup/quantity/UnitDisplayData;->getUnitName()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Lcom/squareup/quantity/UnitDisplayData;->getUnitName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, " ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v2, 0x29

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_1
    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {p1, v2}, Lcom/squareup/noho/NohoRow;->setLabel(Ljava/lang/CharSequence;)V

    .line 101
    invoke-virtual {p0}, Lcom/squareup/ui/items/UnitRow;->getRes()Lcom/squareup/util/Res;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/quantity/UnitDisplayData;->getQuantityPrecisionHint(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {p1, v1}, Lcom/squareup/noho/NohoRow;->setValue(Ljava/lang/CharSequence;)V

    .line 102
    invoke-virtual {v0}, Lcom/squareup/shared/catalog/connectv2/models/CatalogMeasurementUnit;->getMeasurementUnit()Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    move-result-object v0

    const-string/jumbo v1, "unit.measurementUnit"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/squareup/items/unit/ui/EditUnitCoordinatorKt;->isCustomUnit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 103
    invoke-virtual {p0}, Lcom/squareup/ui/items/UnitRow;->getRes()Lcom/squareup/util/Res;

    move-result-object v0

    sget v1, Lcom/squareup/itemsapplet/R$string;->unit_detail_searchable_list_custom_label:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    goto :goto_2

    :cond_2
    const/4 v0, 0x0

    .line 102
    :goto_2
    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoRow;->setDescription(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final copy(Lcom/squareup/ui/items/DetailSearchableListScreen;Lcom/squareup/util/Res;Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$MeasurementUnit;)Lcom/squareup/ui/items/UnitRow;
    .locals 1

    const-string v0, "screen"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "unit"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/items/UnitRow;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/ui/items/UnitRow;-><init>(Lcom/squareup/ui/items/DetailSearchableListScreen;Lcom/squareup/util/Res;Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$MeasurementUnit;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/items/UnitRow;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/items/UnitRow;

    invoke-virtual {p0}, Lcom/squareup/ui/items/UnitRow;->getScreen()Lcom/squareup/ui/items/DetailSearchableListScreen;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/ui/items/UnitRow;->getScreen()Lcom/squareup/ui/items/DetailSearchableListScreen;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/ui/items/UnitRow;->getRes()Lcom/squareup/util/Res;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/ui/items/UnitRow;->getRes()Lcom/squareup/util/Res;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/items/UnitRow;->unit:Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$MeasurementUnit;

    iget-object p1, p1, Lcom/squareup/ui/items/UnitRow;->unit:Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$MeasurementUnit;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public getRes()Lcom/squareup/util/Res;
    .locals 1

    .line 91
    iget-object v0, p0, Lcom/squareup/ui/items/UnitRow;->res:Lcom/squareup/util/Res;

    return-object v0
.end method

.method public getScreen()Lcom/squareup/ui/items/DetailSearchableListScreen;
    .locals 1

    .line 90
    iget-object v0, p0, Lcom/squareup/ui/items/UnitRow;->screen:Lcom/squareup/ui/items/DetailSearchableListScreen;

    return-object v0
.end method

.method public final getUnit()Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$MeasurementUnit;
    .locals 1

    .line 92
    iget-object v0, p0, Lcom/squareup/ui/items/UnitRow;->unit:Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$MeasurementUnit;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    invoke-virtual {p0}, Lcom/squareup/ui/items/UnitRow;->getScreen()Lcom/squareup/ui/items/DetailSearchableListScreen;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/squareup/ui/items/UnitRow;->getRes()Lcom/squareup/util/Res;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/items/UnitRow;->unit:Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$MeasurementUnit;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "UnitRow(screen="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/ui/items/UnitRow;->getScreen()Lcom/squareup/ui/items/DetailSearchableListScreen;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", res="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/squareup/ui/items/UnitRow;->getRes()Lcom/squareup/util/Res;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", unit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/items/UnitRow;->unit:Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$MeasurementUnit;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
