.class public Lcom/squareup/ui/RealTouchEventMonitor;
.super Ljava/lang/Object;
.source "RealTouchEventMonitor.java"

# interfaces
.implements Lcom/squareup/ui/TouchEventMonitor;


# instance fields
.field private final publisher:Lrx/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/PublishSubject<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    invoke-static {}, Lrx/subjects/PublishSubject;->create()Lrx/subjects/PublishSubject;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/RealTouchEventMonitor;->publisher:Lrx/subjects/PublishSubject;

    return-void
.end method


# virtual methods
.method public observeTouchEvents()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 25
    iget-object v0, p0, Lcom/squareup/ui/RealTouchEventMonitor;->publisher:Lrx/subjects/PublishSubject;

    return-object v0
.end method

.method public onTouchEvent()V
    .locals 2

    .line 17
    iget-object v0, p0, Lcom/squareup/ui/RealTouchEventMonitor;->publisher:Lrx/subjects/PublishSubject;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lrx/subjects/PublishSubject;->onNext(Ljava/lang/Object;)V

    return-void
.end method
