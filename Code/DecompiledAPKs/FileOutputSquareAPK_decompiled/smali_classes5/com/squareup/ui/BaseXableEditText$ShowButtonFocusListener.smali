.class Lcom/squareup/ui/BaseXableEditText$ShowButtonFocusListener;
.super Ljava/lang/Object;
.source "BaseXableEditText.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/BaseXableEditText;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ShowButtonFocusListener"
.end annotation


# instance fields
.field private delegate:Landroid/view/View$OnFocusChangeListener;

.field final synthetic this$0:Lcom/squareup/ui/BaseXableEditText;


# direct methods
.method private constructor <init>(Lcom/squareup/ui/BaseXableEditText;)V
    .locals 0

    .line 372
    iput-object p1, p0, Lcom/squareup/ui/BaseXableEditText$ShowButtonFocusListener;->this$0:Lcom/squareup/ui/BaseXableEditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/ui/BaseXableEditText;Lcom/squareup/ui/BaseXableEditText$1;)V
    .locals 0

    .line 372
    invoke-direct {p0, p1}, Lcom/squareup/ui/BaseXableEditText$ShowButtonFocusListener;-><init>(Lcom/squareup/ui/BaseXableEditText;)V

    return-void
.end method

.method static synthetic access$300(Lcom/squareup/ui/BaseXableEditText$ShowButtonFocusListener;Landroid/view/View$OnFocusChangeListener;)V
    .locals 0

    .line 372
    invoke-direct {p0, p1}, Lcom/squareup/ui/BaseXableEditText$ShowButtonFocusListener;->setDelegate(Landroid/view/View$OnFocusChangeListener;)V

    return-void
.end method

.method private setDelegate(Landroid/view/View$OnFocusChangeListener;)V
    .locals 0

    .line 377
    iput-object p1, p0, Lcom/squareup/ui/BaseXableEditText$ShowButtonFocusListener;->delegate:Landroid/view/View$OnFocusChangeListener;

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 1

    .line 381
    iget-object v0, p0, Lcom/squareup/ui/BaseXableEditText$ShowButtonFocusListener;->delegate:Landroid/view/View$OnFocusChangeListener;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1, p2}, Landroid/view/View$OnFocusChangeListener;->onFocusChange(Landroid/view/View;Z)V

    :cond_0
    if-eqz p2, :cond_1

    .line 382
    invoke-static {p1}, Lcom/squareup/util/Views;->showSoftKeyboard(Landroid/view/View;)V

    .line 383
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/BaseXableEditText$ShowButtonFocusListener;->this$0:Lcom/squareup/ui/BaseXableEditText;

    iget-object p1, p1, Lcom/squareup/ui/BaseXableEditText;->editText:Landroid/widget/EditText;

    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/util/Strings;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    .line 384
    iget-object v0, p0, Lcom/squareup/ui/BaseXableEditText$ShowButtonFocusListener;->this$0:Lcom/squareup/ui/BaseXableEditText;

    invoke-static {v0, p2, p1}, Lcom/squareup/ui/BaseXableEditText;->access$800(Lcom/squareup/ui/BaseXableEditText;ZZ)V

    return-void
.end method
