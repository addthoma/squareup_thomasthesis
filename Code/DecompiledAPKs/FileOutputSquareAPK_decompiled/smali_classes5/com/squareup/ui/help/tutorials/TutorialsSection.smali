.class public Lcom/squareup/ui/help/tutorials/TutorialsSection;
.super Lcom/squareup/ui/help/AbstractHelpSection;
.source "TutorialsSection.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/help/tutorials/TutorialsSection$ListEntry;
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 20
    new-instance v0, Lcom/squareup/applet/SectionAccess;

    invoke-direct {v0}, Lcom/squareup/applet/SectionAccess;-><init>()V

    invoke-direct {p0, v0}, Lcom/squareup/ui/help/AbstractHelpSection;-><init>(Lcom/squareup/applet/SectionAccess;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic getInitialScreen()Lcom/squareup/container/ContainerTreeKey;
    .locals 1

    .line 16
    invoke-virtual {p0}, Lcom/squareup/ui/help/tutorials/TutorialsSection;->getInitialScreen()Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object v0

    return-object v0
.end method

.method public getInitialScreen()Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 1

    .line 24
    sget-object v0, Lcom/squareup/ui/help/tutorials/TutorialsScreen;->INSTANCE:Lcom/squareup/ui/help/tutorials/TutorialsScreen;

    return-object v0
.end method

.method public tapName()Lcom/squareup/analytics/RegisterTapName;
    .locals 1

    .line 28
    sget-object v0, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_TUTORIALS:Lcom/squareup/analytics/RegisterTapName;

    return-object v0
.end method
