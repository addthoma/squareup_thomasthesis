.class public final synthetic Lcom/squareup/ui/help/orders/-$$Lambda$ik_wxPhJ7h0nY0t8hU_30VeHM-g;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lio/reactivex/functions/Function;


# static fields
.field public static final synthetic INSTANCE:Lcom/squareup/ui/help/orders/-$$Lambda$ik_wxPhJ7h0nY0t8hU_30VeHM-g;


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/ui/help/orders/-$$Lambda$ik_wxPhJ7h0nY0t8hU_30VeHM-g;

    invoke-direct {v0}, Lcom/squareup/ui/help/orders/-$$Lambda$ik_wxPhJ7h0nY0t8hU_30VeHM-g;-><init>()V

    sput-object v0, Lcom/squareup/ui/help/orders/-$$Lambda$ik_wxPhJ7h0nY0t8hU_30VeHM-g;->INSTANCE:Lcom/squareup/ui/help/orders/-$$Lambda$ik_wxPhJ7h0nY0t8hU_30VeHM-g;

    return-void
.end method

.method private synthetic constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/squareup/protos/client/solidshop/GetOrderStatusResponse;

    invoke-static {p1}, Lcom/squareup/ui/help/orders/OrderHistory$Snapshot;->toSnapshot(Lcom/squareup/protos/client/solidshop/GetOrderStatusResponse;)Lcom/squareup/ui/help/orders/OrderHistory$Snapshot;

    move-result-object p1

    return-object p1
.end method
