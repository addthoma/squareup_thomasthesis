.class public interface abstract Lcom/squareup/ui/help/HelpAppletScope$SupportComponents;
.super Ljava/lang/Object;
.source "HelpAppletScope.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/help/HelpAppletScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SupportComponents"
.end annotation


# virtual methods
.method public abstract orderHistoryScreenComponent()Lcom/squareup/ui/help/orders/OrderHistoryScreen$Component;
.end method

.method public abstract orderReadersScreenComponent()Lcom/squareup/ui/help/orders/OrderHardwareScreen$Component;
.end method
