.class final Lcom/squareup/ui/help/about/NtepCoordinator$attach$1$1;
.super Ljava/lang/Object;
.source "NtepCoordinator.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/help/about/NtepCoordinator$attach$1;->invoke()Lio/reactivex/disposables/Disposable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/ui/help/about/AboutScreenData;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "screenData",
        "Lcom/squareup/ui/help/about/AboutScreenData;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/help/about/NtepCoordinator$attach$1;


# direct methods
.method constructor <init>(Lcom/squareup/ui/help/about/NtepCoordinator$attach$1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/help/about/NtepCoordinator$attach$1$1;->this$0:Lcom/squareup/ui/help/about/NtepCoordinator$attach$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/ui/help/about/AboutScreenData;)V
    .locals 2

    .line 43
    iget-object v0, p0, Lcom/squareup/ui/help/about/NtepCoordinator$attach$1$1;->this$0:Lcom/squareup/ui/help/about/NtepCoordinator$attach$1;

    iget-object v0, v0, Lcom/squareup/ui/help/about/NtepCoordinator$attach$1;->this$0:Lcom/squareup/ui/help/about/NtepCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/help/about/NtepCoordinator;->access$getNtepCompanyRow$p(Lcom/squareup/ui/help/about/NtepCoordinator;)Lcom/squareup/widgets/list/NameValueRow;

    move-result-object v0

    const-string v1, "Square, Inc."

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/NameValueRow;->setName(Ljava/lang/CharSequence;)V

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/help/about/NtepCoordinator$attach$1$1;->this$0:Lcom/squareup/ui/help/about/NtepCoordinator$attach$1;

    iget-object v0, v0, Lcom/squareup/ui/help/about/NtepCoordinator$attach$1;->this$0:Lcom/squareup/ui/help/about/NtepCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/help/about/NtepCoordinator;->access$getNtepVersionRow$p(Lcom/squareup/ui/help/about/NtepCoordinator;)Lcom/squareup/widgets/list/NameValueRow;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/ui/help/about/AboutScreenData;->versionString:Ljava/lang/String;

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/list/NameValueRow;->setValue(Ljava/lang/CharSequence;)V

    .line 45
    iget-object p1, p0, Lcom/squareup/ui/help/about/NtepCoordinator$attach$1$1;->this$0:Lcom/squareup/ui/help/about/NtepCoordinator$attach$1;

    iget-object p1, p1, Lcom/squareup/ui/help/about/NtepCoordinator$attach$1;->this$0:Lcom/squareup/ui/help/about/NtepCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/help/about/NtepCoordinator;->access$getNtepModelRow$p(Lcom/squareup/ui/help/about/NtepCoordinator;)Lcom/squareup/widgets/list/NameValueRow;

    move-result-object p1

    const-string v0, "Square Point of Sale Application"

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/list/NameValueRow;->setValue(Ljava/lang/CharSequence;)V

    .line 46
    iget-object p1, p0, Lcom/squareup/ui/help/about/NtepCoordinator$attach$1$1;->this$0:Lcom/squareup/ui/help/about/NtepCoordinator$attach$1;

    iget-object p1, p1, Lcom/squareup/ui/help/about/NtepCoordinator$attach$1;->this$0:Lcom/squareup/ui/help/about/NtepCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/help/about/NtepCoordinator;->access$getNtepCertificationNumberRow$p(Lcom/squareup/ui/help/about/NtepCoordinator;)Lcom/squareup/widgets/list/NameValueRow;

    move-result-object p1

    const-string v0, "19-065"

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/list/NameValueRow;->setValue(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 23
    check-cast p1, Lcom/squareup/ui/help/about/AboutScreenData;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/help/about/NtepCoordinator$attach$1$1;->accept(Lcom/squareup/ui/help/about/AboutScreenData;)V

    return-void
.end method
