.class final enum Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadType;
.super Ljava/lang/Enum;
.source "HelpTroubleshootingRunner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "UploadType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadType;

.field public static final enum SEND_DIAGNOSTICS:Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadType;

.field public static final enum UPLOAD_LEDGER:Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadType;


# instance fields
.field public final failureToast:I

.field public final successToast:I

.field public final uploadName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .line 219
    new-instance v6, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadType;

    sget v4, Lcom/squareup/applet/help/R$string;->upload_diagnostics_data_success:I

    sget v5, Lcom/squareup/applet/help/R$string;->upload_diagnostics_data_failure:I

    const-string v1, "SEND_DIAGNOSTICS"

    const/4 v2, 0x0

    const-string v3, "Diagnostics Upload"

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadType;-><init>(Ljava/lang/String;ILjava/lang/String;II)V

    sput-object v6, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadType;->SEND_DIAGNOSTICS:Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadType;

    .line 222
    new-instance v0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadType;

    sget v11, Lcom/squareup/applet/help/R$string;->upload_transaction_ledger_success:I

    sget v12, Lcom/squareup/applet/help/R$string;->upload_transaction_ledger_failure:I

    const-string v8, "UPLOAD_LEDGER"

    const/4 v9, 0x1

    const-string v10, "Ledger Upload"

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadType;-><init>(Ljava/lang/String;ILjava/lang/String;II)V

    sput-object v0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadType;->UPLOAD_LEDGER:Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadType;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadType;

    .line 218
    sget-object v1, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadType;->SEND_DIAGNOSTICS:Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadType;->UPLOAD_LEDGER:Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadType;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadType;->$VALUES:[Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "II)V"
        }
    .end annotation

    .line 230
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 231
    iput-object p3, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadType;->uploadName:Ljava/lang/String;

    .line 232
    iput p4, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadType;->successToast:I

    .line 233
    iput p5, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadType;->failureToast:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadType;
    .locals 1

    .line 218
    const-class v0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadType;
    .locals 1

    .line 218
    sget-object v0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadType;->$VALUES:[Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadType;

    invoke-virtual {v0}, [Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadType;

    return-object v0
.end method
