.class final Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier$onEnterScope$1;
.super Ljava/lang/Object;
.source "SupportMessagingNotificationNotifier.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lkotlin/Pair<",
        "+",
        "Lcom/squareup/pushmessages/PushMessage$SupportMessagingNotification;",
        "+",
        "Lcom/squareup/ui/help/messages/ForegroundedActivity;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012&\u0010\u0002\u001a\"\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005 \u0006*\u0010\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "",
        "<name for destructuring parameter 0>",
        "Lkotlin/Pair;",
        "Lcom/squareup/pushmessages/PushMessage$SupportMessagingNotification;",
        "Lcom/squareup/ui/help/messages/ForegroundedActivity;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier;


# direct methods
.method constructor <init>(Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier$onEnterScope$1;->this$0:Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 32
    check-cast p1, Lkotlin/Pair;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier$onEnterScope$1;->accept(Lkotlin/Pair;)V

    return-void
.end method

.method public final accept(Lkotlin/Pair;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "Lcom/squareup/pushmessages/PushMessage$SupportMessagingNotification;",
            "+",
            "Lcom/squareup/ui/help/messages/ForegroundedActivity;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/pushmessages/PushMessage$SupportMessagingNotification;

    invoke-virtual {p1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/help/messages/ForegroundedActivity;

    .line 47
    sget-object v1, Lcom/squareup/ui/help/messages/ForegroundedActivity$ForegroundIsHelpshiftActivity;->INSTANCE:Lcom/squareup/ui/help/messages/ForegroundedActivity$ForegroundIsHelpshiftActivity;

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    const/4 v1, 0x1

    xor-int/2addr p1, v1

    if-eqz p1, :cond_0

    .line 49
    iget-object p1, p0, Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier$onEnterScope$1;->this$0:Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier;

    invoke-static {p1}, Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier;->access$getMessagesVisibility$p(Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier;)Lcom/squareup/ui/help/messages/MessagesVisibility;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/help/messages/MessagesVisibility;->showMessagingIcon()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 50
    iget-object p1, p0, Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier$onEnterScope$1;->this$0:Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier;

    invoke-static {p1}, Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier;->access$getMessagesVisibility$p(Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier;)Lcom/squareup/ui/help/messages/MessagesVisibility;

    move-result-object p1

    invoke-virtual {p1, v1}, Lcom/squareup/ui/help/messages/MessagesVisibility;->updateUnreadMessagesCount(I)V

    .line 51
    iget-object p1, p0, Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier$onEnterScope$1;->this$0:Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier;

    invoke-virtual {v0}, Lcom/squareup/pushmessages/PushMessage$SupportMessagingNotification;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/squareup/pushmessages/PushMessage$SupportMessagingNotification;->getBody()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v1, v0}, Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier;->access$showNotification(Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method
