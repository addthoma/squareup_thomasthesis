.class public final Lcom/squareup/ui/help/setupguide/SetupGuideSection;
.super Lcom/squareup/ui/help/AbstractHelpSection;
.source "SetupGuideSection.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/help/setupguide/SetupGuideSection$ListEntry;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0007\u0018\u00002\u00020\u0001:\u0001\rB\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0008\u0010\u0007\u001a\u00020\u0008H\u0016J\u0006\u0010\t\u001a\u00020\nJ\u0008\u0010\u000b\u001a\u00020\u000cH\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/ui/help/setupguide/SetupGuideSection;",
        "Lcom/squareup/ui/help/AbstractHelpSection;",
        "setupGuideAccess",
        "Lcom/squareup/ui/help/setupguide/SetupGuideAccess;",
        "setupGuideLogger",
        "Lcom/squareup/setupguide/SetupGuideLogger;",
        "(Lcom/squareup/ui/help/setupguide/SetupGuideAccess;Lcom/squareup/setupguide/SetupGuideLogger;)V",
        "getInitialScreen",
        "Lcom/squareup/container/ContainerTreeKey;",
        "logSetupGuideOpened",
        "",
        "tapName",
        "Lcom/squareup/analytics/RegisterTapName;",
        "ListEntry",
        "help_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final setupGuideLogger:Lcom/squareup/setupguide/SetupGuideLogger;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/help/setupguide/SetupGuideAccess;Lcom/squareup/setupguide/SetupGuideLogger;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "setupGuideAccess"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "setupGuideLogger"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    check-cast p1, Lcom/squareup/applet/SectionAccess;

    invoke-direct {p0, p1}, Lcom/squareup/ui/help/AbstractHelpSection;-><init>(Lcom/squareup/applet/SectionAccess;)V

    iput-object p2, p0, Lcom/squareup/ui/help/setupguide/SetupGuideSection;->setupGuideLogger:Lcom/squareup/setupguide/SetupGuideLogger;

    return-void
.end method


# virtual methods
.method public getInitialScreen()Lcom/squareup/container/ContainerTreeKey;
    .locals 1

    .line 22
    sget-object v0, Lcom/squareup/setupguide/SetupGuideWorkflowRunner$BootstrapScreen;->INSTANCE:Lcom/squareup/setupguide/SetupGuideWorkflowRunner$BootstrapScreen;

    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    return-object v0
.end method

.method public final logSetupGuideOpened()V
    .locals 2

    .line 29
    iget-object v0, p0, Lcom/squareup/ui/help/setupguide/SetupGuideSection;->setupGuideLogger:Lcom/squareup/setupguide/SetupGuideLogger;

    invoke-interface {v0}, Lcom/squareup/setupguide/SetupGuideLogger;->getSupportAppletName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/setupguide/SetupGuideLogger;->logGuideOpened(Ljava/lang/String;)V

    return-void
.end method

.method public tapName()Lcom/squareup/analytics/RegisterTapName;
    .locals 1

    .line 25
    sget-object v0, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_SETUP_GUIDE:Lcom/squareup/analytics/RegisterTapName;

    return-object v0
.end method
