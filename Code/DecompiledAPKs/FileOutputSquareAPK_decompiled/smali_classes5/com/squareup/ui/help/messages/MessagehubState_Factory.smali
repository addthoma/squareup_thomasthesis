.class public final Lcom/squareup/ui/help/messages/MessagehubState_Factory;
.super Ljava/lang/Object;
.source "MessagehubState_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/help/messages/MessagehubState;",
        ">;"
    }
.end annotation


# instance fields
.field private final accountStatusSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final messagehubServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/messagehub/MessagehubService;",
            ">;"
        }
    .end annotation
.end field

.field private final pushServiceRegistrationSettingValueProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/pushmessages/PushServiceRegistrationSettingValue;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/messagehub/MessagehubService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/pushmessages/PushServiceRegistrationSettingValue;",
            ">;>;)V"
        }
    .end annotation

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/squareup/ui/help/messages/MessagehubState_Factory;->messagehubServiceProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p2, p0, Lcom/squareup/ui/help/messages/MessagehubState_Factory;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    .line 31
    iput-object p3, p0, Lcom/squareup/ui/help/messages/MessagehubState_Factory;->pushServiceRegistrationSettingValueProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/help/messages/MessagehubState_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/messagehub/MessagehubService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/pushmessages/PushServiceRegistrationSettingValue;",
            ">;>;)",
            "Lcom/squareup/ui/help/messages/MessagehubState_Factory;"
        }
    .end annotation

    .line 43
    new-instance v0, Lcom/squareup/ui/help/messages/MessagehubState_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/help/messages/MessagehubState_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/server/messagehub/MessagehubService;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/LocalSetting;)Lcom/squareup/ui/help/messages/MessagehubState;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/messagehub/MessagehubService;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/pushmessages/PushServiceRegistrationSettingValue;",
            ">;)",
            "Lcom/squareup/ui/help/messages/MessagehubState;"
        }
    .end annotation

    .line 49
    new-instance v0, Lcom/squareup/ui/help/messages/MessagehubState;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/help/messages/MessagehubState;-><init>(Lcom/squareup/server/messagehub/MessagehubService;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/LocalSetting;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/help/messages/MessagehubState;
    .locals 3

    .line 36
    iget-object v0, p0, Lcom/squareup/ui/help/messages/MessagehubState_Factory;->messagehubServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/messagehub/MessagehubService;

    iget-object v1, p0, Lcom/squareup/ui/help/messages/MessagehubState_Factory;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v2, p0, Lcom/squareup/ui/help/messages/MessagehubState_Factory;->pushServiceRegistrationSettingValueProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/settings/LocalSetting;

    invoke-static {v0, v1, v2}, Lcom/squareup/ui/help/messages/MessagehubState_Factory;->newInstance(Lcom/squareup/server/messagehub/MessagehubService;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/settings/LocalSetting;)Lcom/squareup/ui/help/messages/MessagehubState;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/ui/help/messages/MessagehubState_Factory;->get()Lcom/squareup/ui/help/messages/MessagehubState;

    move-result-object v0

    return-object v0
.end method
