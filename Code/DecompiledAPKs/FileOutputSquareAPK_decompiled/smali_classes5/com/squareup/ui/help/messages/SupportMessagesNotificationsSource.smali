.class public final Lcom/squareup/ui/help/messages/SupportMessagesNotificationsSource;
.super Ljava/lang/Object;
.source "SupportMessagesNotificationsSource.kt"

# interfaces
.implements Lcom/squareup/notificationcenterdata/NotificationsSource;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001B/\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u0018\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u000f2\u0006\u0010\u0014\u001a\u00020\u0015H\u0002J\u0010\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u000fH\u0002J\u000e\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u001a0\u000eH\u0016J4\u0010\u001b\u001a&\u0012\u000c\u0012\n \u0010*\u0004\u0018\u00010\u00150\u0015 \u0010*\u0012\u0012\u000c\u0012\n \u0010*\u0004\u0018\u00010\u00150\u0015\u0018\u00010\u000e0\u000e2\u0006\u0010\u001c\u001a\u00020\u0017H\u0002R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R2\u0010\r\u001a&\u0012\u000c\u0012\n \u0010*\u0004\u0018\u00010\u000f0\u000f \u0010*\u0012\u0012\u000c\u0012\n \u0010*\u0004\u0018\u00010\u000f0\u000f\u0018\u00010\u000e0\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/ui/help/messages/SupportMessagesNotificationsSource;",
        "Lcom/squareup/notificationcenterdata/NotificationsSource;",
        "res",
        "Lcom/squareup/util/Res;",
        "currentTime",
        "Lcom/squareup/time/CurrentTime;",
        "localNotificationAnalytics",
        "Lcom/squareup/notificationcenterdata/logging/LocalNotificationAnalytics;",
        "notificationStateStore",
        "Lcom/squareup/notificationcenterdata/NotificationStateStore;",
        "messagesVisibility",
        "Lcom/squareup/ui/help/messages/MessagesVisibility;",
        "(Lcom/squareup/util/Res;Lcom/squareup/time/CurrentTime;Lcom/squareup/notificationcenterdata/logging/LocalNotificationAnalytics;Lcom/squareup/notificationcenterdata/NotificationStateStore;Lcom/squareup/ui/help/messages/MessagesVisibility;)V",
        "supportMessageCount",
        "Lio/reactivex/Observable;",
        "",
        "kotlin.jvm.PlatformType",
        "createMessagesNotification",
        "Lcom/squareup/notificationcenterdata/Notification;",
        "supportMessagesCount",
        "state",
        "Lcom/squareup/notificationcenterdata/Notification$State;",
        "id",
        "",
        "count",
        "notifications",
        "Lcom/squareup/notificationcenterdata/NotificationsSource$Result;",
        "supportMessageState",
        "notificationId",
        "help_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final currentTime:Lcom/squareup/time/CurrentTime;

.field private final localNotificationAnalytics:Lcom/squareup/notificationcenterdata/logging/LocalNotificationAnalytics;

.field private final notificationStateStore:Lcom/squareup/notificationcenterdata/NotificationStateStore;

.field private final res:Lcom/squareup/util/Res;

.field private final supportMessageCount:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/time/CurrentTime;Lcom/squareup/notificationcenterdata/logging/LocalNotificationAnalytics;Lcom/squareup/notificationcenterdata/NotificationStateStore;Lcom/squareup/ui/help/messages/MessagesVisibility;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currentTime"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "localNotificationAnalytics"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "notificationStateStore"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "messagesVisibility"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/help/messages/SupportMessagesNotificationsSource;->res:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/ui/help/messages/SupportMessagesNotificationsSource;->currentTime:Lcom/squareup/time/CurrentTime;

    iput-object p3, p0, Lcom/squareup/ui/help/messages/SupportMessagesNotificationsSource;->localNotificationAnalytics:Lcom/squareup/notificationcenterdata/logging/LocalNotificationAnalytics;

    iput-object p4, p0, Lcom/squareup/ui/help/messages/SupportMessagesNotificationsSource;->notificationStateStore:Lcom/squareup/notificationcenterdata/NotificationStateStore;

    .line 37
    invoke-virtual {p5}, Lcom/squareup/ui/help/messages/MessagesVisibility;->badgeCount()Lio/reactivex/Observable;

    move-result-object p1

    const/4 p2, 0x0

    .line 38
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object p1

    .line 39
    invoke-virtual {p1}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/help/messages/SupportMessagesNotificationsSource;->supportMessageCount:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$createMessagesNotification(Lcom/squareup/ui/help/messages/SupportMessagesNotificationsSource;ILcom/squareup/notificationcenterdata/Notification$State;)Lcom/squareup/notificationcenterdata/Notification;
    .locals 0

    .line 28
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/help/messages/SupportMessagesNotificationsSource;->createMessagesNotification(ILcom/squareup/notificationcenterdata/Notification$State;)Lcom/squareup/notificationcenterdata/Notification;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$id(Lcom/squareup/ui/help/messages/SupportMessagesNotificationsSource;I)Ljava/lang/String;
    .locals 0

    .line 28
    invoke-direct {p0, p1}, Lcom/squareup/ui/help/messages/SupportMessagesNotificationsSource;->id(I)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$supportMessageState(Lcom/squareup/ui/help/messages/SupportMessagesNotificationsSource;Ljava/lang/String;)Lio/reactivex/Observable;
    .locals 0

    .line 28
    invoke-direct {p0, p1}, Lcom/squareup/ui/help/messages/SupportMessagesNotificationsSource;->supportMessageState(Ljava/lang/String;)Lio/reactivex/Observable;

    move-result-object p0

    return-object p0
.end method

.method private final createMessagesNotification(ILcom/squareup/notificationcenterdata/Notification$State;)Lcom/squareup/notificationcenterdata/Notification;
    .locals 13

    .line 65
    new-instance v12, Lcom/squareup/notificationcenterdata/Notification;

    .line 66
    invoke-direct {p0, p1}, Lcom/squareup/ui/help/messages/SupportMessagesNotificationsSource;->id(I)Ljava/lang/String;

    move-result-object v1

    .line 68
    iget-object p1, p0, Lcom/squareup/ui/help/messages/SupportMessagesNotificationsSource;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/applet/help/R$string;->messages_notification_title:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 69
    iget-object p1, p0, Lcom/squareup/ui/help/messages/SupportMessagesNotificationsSource;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/applet/help/R$string;->messages_notification_content:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 71
    sget-object p1, Lcom/squareup/notificationcenterdata/Notification$Priority$Important;->INSTANCE:Lcom/squareup/notificationcenterdata/Notification$Priority$Important;

    move-object v6, p1

    check-cast v6, Lcom/squareup/notificationcenterdata/Notification$Priority;

    .line 72
    sget-object p1, Lcom/squareup/notificationcenterdata/Notification$DisplayType$Normal;->INSTANCE:Lcom/squareup/notificationcenterdata/Notification$DisplayType$Normal;

    move-object v7, p1

    check-cast v7, Lcom/squareup/notificationcenterdata/Notification$DisplayType;

    .line 73
    sget-object v9, Lcom/squareup/notificationcenterdata/Notification$Source;->LOCAL:Lcom/squareup/notificationcenterdata/Notification$Source;

    .line 74
    new-instance p1, Lcom/squareup/notificationcenterdata/Notification$Destination$SupportedClientAction;

    .line 75
    new-instance v0, Lcom/squareup/protos/client/ClientAction$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/ClientAction$Builder;-><init>()V

    .line 76
    new-instance v2, Lcom/squareup/protos/client/ClientAction$ViewSupportMessageCenter$Builder;

    invoke-direct {v2}, Lcom/squareup/protos/client/ClientAction$ViewSupportMessageCenter$Builder;-><init>()V

    invoke-virtual {v2}, Lcom/squareup/protos/client/ClientAction$ViewSupportMessageCenter$Builder;->build()Lcom/squareup/protos/client/ClientAction$ViewSupportMessageCenter;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/protos/client/ClientAction$Builder;->view_support_message_center(Lcom/squareup/protos/client/ClientAction$ViewSupportMessageCenter;)Lcom/squareup/protos/client/ClientAction$Builder;

    move-result-object v0

    .line 77
    sget-object v2, Lcom/squareup/protos/client/ClientAction$FallbackBehavior;->NONE:Lcom/squareup/protos/client/ClientAction$FallbackBehavior;

    invoke-virtual {v0, v2}, Lcom/squareup/protos/client/ClientAction$Builder;->fallback_behavior(Lcom/squareup/protos/client/ClientAction$FallbackBehavior;)Lcom/squareup/protos/client/ClientAction$Builder;

    move-result-object v0

    .line 78
    invoke-virtual {v0}, Lcom/squareup/protos/client/ClientAction$Builder;->build()Lcom/squareup/protos/client/ClientAction;

    move-result-object v0

    const-string v2, "ClientAction.Builder()\n \u2026\n                .build()"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    invoke-direct {p1, v0}, Lcom/squareup/notificationcenterdata/Notification$Destination$SupportedClientAction;-><init>(Lcom/squareup/protos/client/ClientAction;)V

    move-object v8, p1

    check-cast v8, Lcom/squareup/notificationcenterdata/Notification$Destination;

    .line 80
    iget-object p1, p0, Lcom/squareup/ui/help/messages/SupportMessagesNotificationsSource;->currentTime:Lcom/squareup/time/CurrentTime;

    invoke-interface {p1}, Lcom/squareup/time/CurrentTime;->instant()Lorg/threeten/bp/Instant;

    move-result-object v10

    .line 81
    sget-object p1, Lcom/squareup/communications/Message$Type$AlertSupportCenter;->INSTANCE:Lcom/squareup/communications/Message$Type$AlertSupportCenter;

    move-object v11, p1

    check-cast v11, Lcom/squareup/communications/Message$Type;

    const-string v2, ""

    move-object v0, v12

    move-object v5, p2

    .line 65
    invoke-direct/range {v0 .. v11}, Lcom/squareup/notificationcenterdata/Notification;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/notificationcenterdata/Notification$State;Lcom/squareup/notificationcenterdata/Notification$Priority;Lcom/squareup/notificationcenterdata/Notification$DisplayType;Lcom/squareup/notificationcenterdata/Notification$Destination;Lcom/squareup/notificationcenterdata/Notification$Source;Lorg/threeten/bp/Instant;Lcom/squareup/communications/Message$Type;)V

    .line 84
    iget-object p1, p0, Lcom/squareup/ui/help/messages/SupportMessagesNotificationsSource;->localNotificationAnalytics:Lcom/squareup/notificationcenterdata/logging/LocalNotificationAnalytics;

    invoke-interface {p1, v12}, Lcom/squareup/notificationcenterdata/logging/LocalNotificationAnalytics;->logLocalNotificationCreated(Lcom/squareup/notificationcenterdata/Notification;)V

    return-object v12
.end method

.method private final id(I)Ljava/lang/String;
    .locals 2

    .line 41
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "support-messages-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private final supportMessageState(Ljava/lang/String;)Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/notificationcenterdata/Notification$State;",
            ">;"
        }
    .end annotation

    .line 43
    iget-object v0, p0, Lcom/squareup/ui/help/messages/SupportMessagesNotificationsSource;->notificationStateStore:Lcom/squareup/notificationcenterdata/NotificationStateStore;

    .line 44
    sget-object v1, Lcom/squareup/notificationcenterdata/Notification$Source;->LOCAL:Lcom/squareup/notificationcenterdata/Notification$Source;

    invoke-interface {v0, p1, v1}, Lcom/squareup/notificationcenterdata/NotificationStateStore;->getNotificationState(Ljava/lang/String;Lcom/squareup/notificationcenterdata/Notification$Source;)Lio/reactivex/Observable;

    move-result-object p1

    .line 45
    sget-object v0, Lcom/squareup/notificationcenterdata/Notification$State;->UNREAD:Lcom/squareup/notificationcenterdata/Notification$State;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public notifications()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/notificationcenterdata/NotificationsSource$Result;",
            ">;"
        }
    .end annotation

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/help/messages/SupportMessagesNotificationsSource;->supportMessageCount:Lio/reactivex/Observable;

    .line 49
    new-instance v1, Lcom/squareup/ui/help/messages/SupportMessagesNotificationsSource$notifications$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/help/messages/SupportMessagesNotificationsSource$notifications$1;-><init>(Lcom/squareup/ui/help/messages/SupportMessagesNotificationsSource;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->switchMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "supportMessageCount\n    \u2026  )\n          }\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
