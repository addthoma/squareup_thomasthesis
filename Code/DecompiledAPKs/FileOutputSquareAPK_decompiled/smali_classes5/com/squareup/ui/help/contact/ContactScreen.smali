.class public Lcom/squareup/ui/help/contact/ContactScreen;
.super Lcom/squareup/ui/help/jedi/InJediHelpScope;
.source "ContactScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/coordinators/CoordinatorProvider;
.implements Lcom/squareup/container/layer/InSection;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/help/contact/ContactScreen$Runner;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/help/contact/ContactScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/help/contact/ContactScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 22
    new-instance v0, Lcom/squareup/ui/help/contact/ContactScreen;

    invoke-direct {v0}, Lcom/squareup/ui/help/contact/ContactScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/help/contact/ContactScreen;->INSTANCE:Lcom/squareup/ui/help/contact/ContactScreen;

    .line 47
    sget-object v0, Lcom/squareup/ui/help/contact/ContactScreen;->INSTANCE:Lcom/squareup/ui/help/contact/ContactScreen;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/help/contact/ContactScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 24
    invoke-direct {p0}, Lcom/squareup/ui/help/jedi/InJediHelpScope;-><init>()V

    return-void
.end method


# virtual methods
.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 40
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->SUPPORT_CONTACT:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getSection()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 32
    const-class v0, Lcom/squareup/ui/help/contact/ContactSection;

    return-object v0
.end method

.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 1

    .line 44
    const-class v0, Lcom/squareup/ui/help/jedi/JediHelpScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/help/jedi/JediHelpScope$Component;

    invoke-interface {p1}, Lcom/squareup/ui/help/jedi/JediHelpScope$Component;->jediHelpCallUsCoordinator()Lcom/squareup/ui/help/contact/ContactCoordinator;

    move-result-object p1

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 36
    sget v0, Lcom/squareup/applet/help/R$layout;->jedi_help_section:I

    return v0
.end method
