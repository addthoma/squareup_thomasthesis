.class public final Lcom/squareup/ui/help/orders/magstripe/HelpAppletOrderMagstripeBootstrapScreen;
.super Lcom/squareup/container/BootstrapTreeKey;
.source "HelpAppletOrderMagstripeBootstrapScreen.kt"

# interfaces
.implements Lcom/squareup/container/layer/InSection;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u00c6\u0002\u0018\u00002\u00020\u00012\u00020\u0002B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0003J\u0010\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH\u0016J\u0008\u0010\u000c\u001a\u00020\rH\u0016R\u0018\u0010\u0004\u001a\u0006\u0012\u0002\u0008\u00030\u00058VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0006\u0010\u0007\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/ui/help/orders/magstripe/HelpAppletOrderMagstripeBootstrapScreen;",
        "Lcom/squareup/container/BootstrapTreeKey;",
        "Lcom/squareup/container/layer/InSection;",
        "()V",
        "section",
        "Ljava/lang/Class;",
        "getSection",
        "()Ljava/lang/Class;",
        "doRegister",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "getParentKey",
        "Lcom/squareup/ordermagstripe/OrderMagstripeBootstrapScreenScope;",
        "help_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/help/orders/magstripe/HelpAppletOrderMagstripeBootstrapScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 16
    new-instance v0, Lcom/squareup/ui/help/orders/magstripe/HelpAppletOrderMagstripeBootstrapScreen;

    invoke-direct {v0}, Lcom/squareup/ui/help/orders/magstripe/HelpAppletOrderMagstripeBootstrapScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/help/orders/magstripe/HelpAppletOrderMagstripeBootstrapScreen;->INSTANCE:Lcom/squareup/ui/help/orders/magstripe/HelpAppletOrderMagstripeBootstrapScreen;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Lcom/squareup/container/BootstrapTreeKey;-><init>()V

    return-void
.end method


# virtual methods
.method public doRegister(Lmortar/MortarScope;)V
    .locals 1

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    sget-object v0, Lcom/squareup/ordermagstripe/OrderMagstripeWorkflowRunner;->Companion:Lcom/squareup/ordermagstripe/OrderMagstripeWorkflowRunner$Companion;

    invoke-virtual {v0, p1}, Lcom/squareup/ordermagstripe/OrderMagstripeWorkflowRunner$Companion;->getRunner(Lmortar/MortarScope;)Lcom/squareup/ordermagstripe/OrderMagstripeWorkflowRunner;

    move-result-object p1

    .line 21
    invoke-interface {p1}, Lcom/squareup/ordermagstripe/OrderMagstripeWorkflowRunner;->startWorkflow()V

    return-void
.end method

.method public getParentKey()Lcom/squareup/ordermagstripe/OrderMagstripeBootstrapScreenScope;
    .locals 3

    .line 17
    new-instance v0, Lcom/squareup/ordermagstripe/OrderMagstripeBootstrapScreenScope;

    sget-object v1, Lcom/squareup/ui/help/HelpAppletScope;->INSTANCE:Lcom/squareup/ui/help/HelpAppletScope;

    const-string v2, "HelpAppletScope.INSTANCE"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/container/ContainerTreeKey;

    invoke-direct {v0, v1}, Lcom/squareup/ordermagstripe/OrderMagstripeBootstrapScreenScope;-><init>(Lcom/squareup/container/ContainerTreeKey;)V

    return-object v0
.end method

.method public bridge synthetic getParentKey()Ljava/lang/Object;
    .locals 1

    .line 16
    invoke-virtual {p0}, Lcom/squareup/ui/help/orders/magstripe/HelpAppletOrderMagstripeBootstrapScreen;->getParentKey()Lcom/squareup/ordermagstripe/OrderMagstripeBootstrapScreenScope;

    move-result-object v0

    return-object v0
.end method

.method public getSection()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 24
    const-class v0, Lcom/squareup/ui/help/orders/OrdersSection;

    return-object v0
.end method
