.class public Lcom/squareup/ui/help/help/HelpScreenData$Builder;
.super Ljava/lang/Object;
.source "HelpScreenData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/help/help/HelpScreenData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private frequentlyAskedQuestionsContent:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/ui/help/HelpAppletContent;",
            ">;"
        }
    .end annotation
.end field

.field private learnMoreContent:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/ui/help/HelpAppletContent;",
            ">;"
        }
    .end annotation
.end field

.field private troubleshootingContent:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/ui/help/HelpAppletContent;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/help/help/HelpScreenData$Builder;->learnMoreContent:Ljava/util/List;

    .line 22
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/help/help/HelpScreenData$Builder;->frequentlyAskedQuestionsContent:Ljava/util/List;

    .line 23
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/help/help/HelpScreenData$Builder;->troubleshootingContent:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/ui/help/help/HelpScreenData;
    .locals 4

    .line 42
    new-instance v0, Lcom/squareup/ui/help/help/HelpScreenData;

    iget-object v1, p0, Lcom/squareup/ui/help/help/HelpScreenData$Builder;->learnMoreContent:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/ui/help/help/HelpScreenData$Builder;->frequentlyAskedQuestionsContent:Ljava/util/List;

    iget-object v3, p0, Lcom/squareup/ui/help/help/HelpScreenData$Builder;->troubleshootingContent:Ljava/util/List;

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/ui/help/help/HelpScreenData;-><init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    return-object v0
.end method

.method public setFrequentlyAskedQuestionsContent(Ljava/util/List;)Lcom/squareup/ui/help/help/HelpScreenData$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/ui/help/HelpAppletContent;",
            ">;)",
            "Lcom/squareup/ui/help/help/HelpScreenData$Builder;"
        }
    .end annotation

    .line 32
    iput-object p1, p0, Lcom/squareup/ui/help/help/HelpScreenData$Builder;->frequentlyAskedQuestionsContent:Ljava/util/List;

    return-object p0
.end method

.method public setLearnMoreContentContent(Ljava/util/List;)Lcom/squareup/ui/help/help/HelpScreenData$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/ui/help/HelpAppletContent;",
            ">;)",
            "Lcom/squareup/ui/help/help/HelpScreenData$Builder;"
        }
    .end annotation

    .line 26
    iput-object p1, p0, Lcom/squareup/ui/help/help/HelpScreenData$Builder;->learnMoreContent:Ljava/util/List;

    return-object p0
.end method

.method public setTroubleshootingContent(Ljava/util/List;)Lcom/squareup/ui/help/help/HelpScreenData$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/ui/help/HelpAppletContent;",
            ">;)",
            "Lcom/squareup/ui/help/help/HelpScreenData$Builder;"
        }
    .end annotation

    .line 37
    iput-object p1, p0, Lcom/squareup/ui/help/help/HelpScreenData$Builder;->troubleshootingContent:Ljava/util/List;

    return-object p0
.end method
