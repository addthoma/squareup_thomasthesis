.class public Lcom/squareup/ui/help/contact/ContactCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "ContactCoordinator.java"


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private final helpAppletScopeRunner:Lcom/squareup/ui/help/HelpAppletScopeRunner;

.field private final jediHelpScopeRunner:Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;

.field private jediPanelView:Lcom/squareup/jedi/ui/JediPanelView;

.field private final mainScheduler:Lio/reactivex/Scheduler;


# direct methods
.method constructor <init>(Lcom/squareup/ui/help/HelpAppletScopeRunner;Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;Lio/reactivex/Scheduler;)V
    .locals 0
    .param p3    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 28
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/squareup/ui/help/contact/ContactCoordinator;->helpAppletScopeRunner:Lcom/squareup/ui/help/HelpAppletScopeRunner;

    .line 30
    iput-object p2, p0, Lcom/squareup/ui/help/contact/ContactCoordinator;->jediHelpScopeRunner:Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;

    .line 31
    iput-object p3, p0, Lcom/squareup/ui/help/contact/ContactCoordinator;->mainScheduler:Lio/reactivex/Scheduler;

    return-void
.end method

.method private bindView(Landroid/view/View;)V
    .locals 1

    .line 51
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    .line 52
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/help/contact/ContactCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 53
    sget v0, Lcom/squareup/applet/help/R$id;->jedi_panel:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/jedi/ui/JediPanelView;

    iput-object p1, p0, Lcom/squareup/ui/help/contact/ContactCoordinator;->jediPanelView:Lcom/squareup/jedi/ui/JediPanelView;

    return-void
.end method

.method private getActionBarConfig(Ljava/lang/String;)Lcom/squareup/marin/widgets/MarinActionBar$Config;
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/squareup/ui/help/contact/ContactCoordinator;->helpAppletScopeRunner:Lcom/squareup/ui/help/HelpAppletScopeRunner;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/help/HelpAppletScopeRunner;->getActionBarConfig(Ljava/lang/String;)Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 3

    .line 35
    invoke-direct {p0, p1}, Lcom/squareup/ui/help/contact/ContactCoordinator;->bindView(Landroid/view/View;)V

    .line 37
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 38
    iget-object v1, p0, Lcom/squareup/ui/help/contact/ContactCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    sget v2, Lcom/squareup/applet/help/R$string;->call_support:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/ui/help/contact/ContactCoordinator;->getActionBarConfig(Ljava/lang/String;)Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 41
    new-instance v0, Lcom/squareup/ui/help/contact/-$$Lambda$ContactCoordinator$QPIj7RMzwIBqlHQjsWaSGNlJ98k;

    invoke-direct {v0, p0}, Lcom/squareup/ui/help/contact/-$$Lambda$ContactCoordinator$QPIj7RMzwIBqlHQjsWaSGNlJ98k;-><init>(Lcom/squareup/ui/help/contact/ContactCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public synthetic lambda$attach$1$ContactCoordinator()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 41
    iget-object v0, p0, Lcom/squareup/ui/help/contact/ContactCoordinator;->jediHelpScopeRunner:Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;->getJediPhoneScreenData()Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/help/contact/ContactCoordinator;->mainScheduler:Lio/reactivex/Scheduler;

    .line 42
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/help/contact/-$$Lambda$ContactCoordinator$0Y4CCBWSyinqLqpaJlbgXK8ilq4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/help/contact/-$$Lambda$ContactCoordinator$0Y4CCBWSyinqLqpaJlbgXK8ilq4;-><init>(Lcom/squareup/ui/help/contact/ContactCoordinator;)V

    .line 43
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$null$0$ContactCoordinator(Lcom/squareup/jedi/JediHelpScreenData;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 43
    iget-object v0, p0, Lcom/squareup/ui/help/contact/ContactCoordinator;->jediPanelView:Lcom/squareup/jedi/ui/JediPanelView;

    iget-object v1, p0, Lcom/squareup/ui/help/contact/ContactCoordinator;->jediHelpScopeRunner:Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/jedi/ui/JediPanelView;->update(Lcom/squareup/jedi/JediHelpScreenData;Lcom/squareup/jedi/JediComponentInputHandler;)V

    return-void
.end method
