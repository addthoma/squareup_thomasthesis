.class public final Lcom/squareup/ui/help/troubleshooting/TroubleshootingSection_Factory;
.super Ljava/lang/Object;
.source "TroubleshootingSection_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/help/troubleshooting/TroubleshootingSection;",
        ">;"
    }
.end annotation


# instance fields
.field private final troubleshootingVisibilityProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/troubleshooting/TroubleshootingVisibility;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/troubleshooting/TroubleshootingVisibility;",
            ">;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/ui/help/troubleshooting/TroubleshootingSection_Factory;->troubleshootingVisibilityProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/ui/help/troubleshooting/TroubleshootingSection_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/troubleshooting/TroubleshootingVisibility;",
            ">;)",
            "Lcom/squareup/ui/help/troubleshooting/TroubleshootingSection_Factory;"
        }
    .end annotation

    .line 30
    new-instance v0, Lcom/squareup/ui/help/troubleshooting/TroubleshootingSection_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/ui/help/troubleshooting/TroubleshootingSection_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/ui/help/troubleshooting/TroubleshootingVisibility;)Lcom/squareup/ui/help/troubleshooting/TroubleshootingSection;
    .locals 1

    .line 35
    new-instance v0, Lcom/squareup/ui/help/troubleshooting/TroubleshootingSection;

    invoke-direct {v0, p0}, Lcom/squareup/ui/help/troubleshooting/TroubleshootingSection;-><init>(Lcom/squareup/ui/help/troubleshooting/TroubleshootingVisibility;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/help/troubleshooting/TroubleshootingSection;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/ui/help/troubleshooting/TroubleshootingSection_Factory;->troubleshootingVisibilityProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/help/troubleshooting/TroubleshootingVisibility;

    invoke-static {v0}, Lcom/squareup/ui/help/troubleshooting/TroubleshootingSection_Factory;->newInstance(Lcom/squareup/ui/help/troubleshooting/TroubleshootingVisibility;)Lcom/squareup/ui/help/troubleshooting/TroubleshootingSection;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/ui/help/troubleshooting/TroubleshootingSection_Factory;->get()Lcom/squareup/ui/help/troubleshooting/TroubleshootingSection;

    move-result-object v0

    return-object v0
.end method
