.class public final Lcom/squareup/ui/help/tutorials/content/FeeTutorialEntry;
.super Lcom/squareup/ui/help/HelpAppletContent;
.source "FeeTutorialEntry.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0008\u0010\u0007\u001a\u00020\u0008H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/ui/help/tutorials/content/FeeTutorialEntry;",
        "Lcom/squareup/ui/help/HelpAppletContent;",
        "feeTutorial",
        "Lcom/squareup/feetutorial/FeeTutorial;",
        "res",
        "Lcom/squareup/util/Res;",
        "(Lcom/squareup/feetutorial/FeeTutorial;Lcom/squareup/util/Res;)V",
        "shouldDisplay",
        "",
        "help_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final feeTutorial:Lcom/squareup/feetutorial/FeeTutorial;


# direct methods
.method public constructor <init>(Lcom/squareup/feetutorial/FeeTutorial;Lcom/squareup/util/Res;)V
    .locals 9
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "feeTutorial"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    sget v2, Lcom/squareup/applet/help/R$string;->fee_walkthrough:I

    .line 15
    sget v0, Lcom/squareup/applet/help/R$string;->fee_walkthrough_subtext:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    move-object v3, p2

    check-cast v3, Ljava/lang/CharSequence;

    .line 16
    sget-object v5, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_FEE_TUTORIAL:Lcom/squareup/analytics/RegisterTapName;

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x14

    const/4 v8, 0x0

    move-object v1, p0

    .line 13
    invoke-direct/range {v1 .. v8}, Lcom/squareup/ui/help/HelpAppletContent;-><init>(ILjava/lang/CharSequence;Ljava/lang/Integer;Lcom/squareup/analytics/RegisterTapName;Ljava/lang/Integer;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/ui/help/tutorials/content/FeeTutorialEntry;->feeTutorial:Lcom/squareup/feetutorial/FeeTutorial;

    return-void
.end method


# virtual methods
.method public shouldDisplay()Z
    .locals 1

    .line 18
    iget-object v0, p0, Lcom/squareup/ui/help/tutorials/content/FeeTutorialEntry;->feeTutorial:Lcom/squareup/feetutorial/FeeTutorial;

    invoke-interface {v0}, Lcom/squareup/feetutorial/FeeTutorial;->getCanShow()Z

    move-result v0

    return v0
.end method
