.class public final Lcom/squareup/ui/help/messages/MessagesVisibility_Factory;
.super Ljava/lang/Object;
.source "MessagesVisibility_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/help/messages/MessagesVisibility;",
        ">;"
    }
.end annotation


# instance fields
.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/ui/help/messages/MessagesVisibility_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/ui/help/messages/MessagesVisibility_Factory;->deviceProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/help/messages/MessagesVisibility_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;)",
            "Lcom/squareup/ui/help/messages/MessagesVisibility_Factory;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/ui/help/messages/MessagesVisibility_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/help/messages/MessagesVisibility_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/settings/server/Features;Lcom/squareup/util/Device;)Lcom/squareup/ui/help/messages/MessagesVisibility;
    .locals 1

    .line 39
    new-instance v0, Lcom/squareup/ui/help/messages/MessagesVisibility;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/help/messages/MessagesVisibility;-><init>(Lcom/squareup/settings/server/Features;Lcom/squareup/util/Device;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/help/messages/MessagesVisibility;
    .locals 2

    .line 30
    iget-object v0, p0, Lcom/squareup/ui/help/messages/MessagesVisibility_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/Features;

    iget-object v1, p0, Lcom/squareup/ui/help/messages/MessagesVisibility_Factory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/util/Device;

    invoke-static {v0, v1}, Lcom/squareup/ui/help/messages/MessagesVisibility_Factory;->newInstance(Lcom/squareup/settings/server/Features;Lcom/squareup/util/Device;)Lcom/squareup/ui/help/messages/MessagesVisibility;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/ui/help/messages/MessagesVisibility_Factory;->get()Lcom/squareup/ui/help/messages/MessagesVisibility;

    move-result-object v0

    return-object v0
.end method
