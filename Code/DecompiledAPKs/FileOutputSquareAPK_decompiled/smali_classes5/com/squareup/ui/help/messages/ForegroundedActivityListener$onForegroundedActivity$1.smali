.class final Lcom/squareup/ui/help/messages/ForegroundedActivityListener$onForegroundedActivity$1;
.super Ljava/lang/Object;
.source "ForegroundedActivityListener.kt"

# interfaces
.implements Lio/reactivex/ObservableOnSubscribe;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/help/messages/ForegroundedActivityListener;->onForegroundedActivity()Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/ObservableOnSubscribe<",
        "TT;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0014\u0010\u0002\u001a\u0010\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "emitter",
        "Lio/reactivex/ObservableEmitter;",
        "Lcom/squareup/ui/help/messages/ForegroundedActivity;",
        "kotlin.jvm.PlatformType",
        "subscribe"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/help/messages/ForegroundedActivityListener;


# direct methods
.method constructor <init>(Lcom/squareup/ui/help/messages/ForegroundedActivityListener;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/help/messages/ForegroundedActivityListener$onForegroundedActivity$1;->this$0:Lcom/squareup/ui/help/messages/ForegroundedActivityListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final subscribe(Lio/reactivex/ObservableEmitter;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/ObservableEmitter<",
            "Lcom/squareup/ui/help/messages/ForegroundedActivity;",
            ">;)V"
        }
    .end annotation

    const-string v0, "emitter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    new-instance v0, Lcom/squareup/ui/help/messages/ForegroundedActivityListener$onForegroundedActivity$1$listener$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/help/messages/ForegroundedActivityListener$onForegroundedActivity$1$listener$1;-><init>(Lcom/squareup/ui/help/messages/ForegroundedActivityListener$onForegroundedActivity$1;Lio/reactivex/ObservableEmitter;)V

    .line 42
    iget-object v1, p0, Lcom/squareup/ui/help/messages/ForegroundedActivityListener$onForegroundedActivity$1;->this$0:Lcom/squareup/ui/help/messages/ForegroundedActivityListener;

    invoke-static {v1}, Lcom/squareup/ui/help/messages/ForegroundedActivityListener;->access$getActivityListener$p(Lcom/squareup/ui/help/messages/ForegroundedActivityListener;)Lcom/squareup/ActivityListener;

    move-result-object v1

    move-object v2, v0

    check-cast v2, Lcom/squareup/ActivityListener$ResumedPausedListener;

    invoke-virtual {v1, v2}, Lcom/squareup/ActivityListener;->registerResumedPausedListener(Lcom/squareup/ActivityListener$ResumedPausedListener;)V

    .line 43
    new-instance v1, Lcom/squareup/ui/help/messages/ForegroundedActivityListener$onForegroundedActivity$1$1;

    invoke-direct {v1, p0, v0}, Lcom/squareup/ui/help/messages/ForegroundedActivityListener$onForegroundedActivity$1$1;-><init>(Lcom/squareup/ui/help/messages/ForegroundedActivityListener$onForegroundedActivity$1;Lcom/squareup/ui/help/messages/ForegroundedActivityListener$onForegroundedActivity$1$listener$1;)V

    check-cast v1, Lio/reactivex/functions/Cancellable;

    invoke-interface {p1, v1}, Lio/reactivex/ObservableEmitter;->setCancellable(Lio/reactivex/functions/Cancellable;)V

    .line 46
    sget-object v0, Lcom/squareup/ui/help/messages/ForegroundedActivity$Background;->INSTANCE:Lcom/squareup/ui/help/messages/ForegroundedActivity$Background;

    invoke-interface {p1, v0}, Lio/reactivex/ObservableEmitter;->onNext(Ljava/lang/Object;)V

    return-void
.end method
