.class public Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "OrderHistoryCoordinator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/help/orders/OrderHistoryCoordinator$MoreViewHolder;,
        Lcom/squareup/ui/help/orders/OrderHistoryCoordinator$OrderViewHolder;,
        Lcom/squareup/ui/help/orders/OrderHistoryCoordinator$OrdersAdapter;
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private actionButton:Landroid/widget/Button;

.field private actionReload:Z

.field private final adapter:Lcom/squareup/ui/help/orders/OrderHistoryCoordinator$OrdersAdapter;

.field private emptyView:Lcom/squareup/ui/EmptyView;

.field private list:Landroidx/recyclerview/widget/RecyclerView;

.field private final locale:Ljava/util/Locale;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final orderHistory:Lcom/squareup/ui/help/orders/OrderHistory;

.field private orderMessage:Landroid/view/View;

.field private final runner:Lcom/squareup/ui/help/HelpAppletScopeRunner;

.field private final shortDate:Ljava/text/DateFormat;

.field private final spinner:Lcom/squareup/register/widgets/GlassSpinner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/help/HelpAppletScopeRunner;Lcom/squareup/ui/help/orders/OrderHistory;Lcom/squareup/register/widgets/GlassSpinner;Ljava/text/DateFormat;Lcom/squareup/text/Formatter;Ljava/util/Locale;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/help/HelpAppletScopeRunner;",
            "Lcom/squareup/ui/help/orders/OrderHistory;",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            "Ljava/text/DateFormat;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Ljava/util/Locale;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 68
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    .line 69
    iput-object p1, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;->runner:Lcom/squareup/ui/help/HelpAppletScopeRunner;

    .line 70
    iput-object p2, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;->orderHistory:Lcom/squareup/ui/help/orders/OrderHistory;

    .line 71
    iput-object p3, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;->spinner:Lcom/squareup/register/widgets/GlassSpinner;

    .line 72
    iput-object p4, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;->shortDate:Ljava/text/DateFormat;

    .line 73
    iput-object p5, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 74
    iput-object p6, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;->locale:Ljava/util/Locale;

    .line 75
    new-instance p1, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator$OrdersAdapter;

    const/4 p2, 0x0

    invoke-direct {p1, p0, p2}, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator$OrdersAdapter;-><init>(Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;Lcom/squareup/ui/help/orders/OrderHistoryCoordinator$1;)V

    iput-object p1, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;->adapter:Lcom/squareup/ui/help/orders/OrderHistoryCoordinator$OrdersAdapter;

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;)Ljava/util/Locale;
    .locals 0

    .line 48
    iget-object p0, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;->locale:Ljava/util/Locale;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;)Ljava/text/DateFormat;
    .locals 0

    .line 48
    iget-object p0, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;->shortDate:Ljava/text/DateFormat;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;)Lcom/squareup/text/Formatter;
    .locals 0

    .line 48
    iget-object p0, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;->moneyFormatter:Lcom/squareup/text/Formatter;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;)Lcom/squareup/ui/help/HelpAppletScopeRunner;
    .locals 0

    .line 48
    iget-object p0, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;->runner:Lcom/squareup/ui/help/HelpAppletScopeRunner;

    return-object p0
.end method

.method private bindViews(Landroid/view/View;)V
    .locals 1

    .line 135
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    .line 136
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 137
    sget v0, Lcom/squareup/applet/help/R$id;->order_list:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    iput-object v0, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;->list:Landroidx/recyclerview/widget/RecyclerView;

    .line 138
    sget v0, Lcom/squareup/applet/help/R$id;->order_message:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;->orderMessage:Landroid/view/View;

    .line 139
    sget v0, Lcom/squareup/applet/help/R$id;->empty_view:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/EmptyView;

    iput-object v0, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;->emptyView:Lcom/squareup/ui/EmptyView;

    .line 140
    sget v0, Lcom/squareup/applet/help/R$id;->history_action_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    iput-object p1, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;->actionButton:Landroid/widget/Button;

    return-void
.end method

.method public static synthetic lambda$eHFBdkhnlEw3sg-S3Mda_NQVUdk(Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;->onAction(Landroid/view/View;)V

    return-void
.end method

.method public static synthetic lambda$iTeo-8idqYdmXpEirclUV6wGVs8(Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;Lcom/squareup/ui/help/orders/OrderHistory$Snapshot;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;->updateView(Lcom/squareup/ui/help/orders/OrderHistory$Snapshot;)V

    return-void
.end method

.method private onAction(Landroid/view/View;)V
    .locals 1

    .line 95
    iget-boolean v0, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;->actionReload:Z

    if-eqz v0, :cond_0

    .line 96
    iget-object p1, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;->orderHistory:Lcom/squareup/ui/help/orders/OrderHistory;

    invoke-virtual {p1}, Lcom/squareup/ui/help/orders/OrderHistory;->reload()V

    return-void

    .line 100
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;->runner:Lcom/squareup/ui/help/HelpAppletScopeRunner;

    invoke-static {p1}, Lcom/squareup/util/Views;->getActivity(Landroid/view/View;)Landroid/app/Activity;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/help/HelpAppletScopeRunner;->onOrderReaderFromHistory(Landroid/app/Activity;)V

    return-void
.end method

.method private setEmptyView(IIII)V
    .locals 1

    .line 128
    iget-object v0, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;->emptyView:Lcom/squareup/ui/EmptyView;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/EmptyView;->setGlyphVisibility(I)V

    .line 129
    iget-object p1, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;->emptyView:Lcom/squareup/ui/EmptyView;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/EmptyView;->setTitle(I)V

    .line 130
    iget-object p1, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;->emptyView:Lcom/squareup/ui/EmptyView;

    invoke-virtual {p1, p3}, Lcom/squareup/ui/EmptyView;->setMessage(I)V

    .line 131
    iget-object p1, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;->actionButton:Landroid/widget/Button;

    invoke-virtual {p1, p4}, Landroid/widget/Button;->setText(I)V

    return-void
.end method

.method private updateView(Lcom/squareup/ui/help/orders/OrderHistory$Snapshot;)V
    .locals 3

    .line 104
    iget-boolean v0, p1, Lcom/squareup/ui/help/orders/OrderHistory$Snapshot;->hasError:Z

    iput-boolean v0, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;->actionReload:Z

    .line 105
    iget-object v0, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;->adapter:Lcom/squareup/ui/help/orders/OrderHistoryCoordinator$OrdersAdapter;

    iget-object v1, p1, Lcom/squareup/ui/help/orders/OrderHistory$Snapshot;->orders:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator$OrdersAdapter;->setOrders(Ljava/util/List;)V

    .line 106
    iget-object v0, p1, Lcom/squareup/ui/help/orders/OrderHistory$Snapshot;->orders:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    const/16 v1, 0x8

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    .line 107
    iget-object v0, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;->list:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setVisibility(I)V

    .line 108
    iget-object v0, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;->orderMessage:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 109
    iget-boolean p1, p1, Lcom/squareup/ui/help/orders/OrderHistory$Snapshot;->hasError:Z

    if-eqz p1, :cond_0

    .line 110
    sget p1, Lcom/squareup/applet/help/R$string;->orders_error_title:I

    sget v0, Lcom/squareup/applet/help/R$string;->orders_error_message:I

    sget v1, Lcom/squareup/applet/help/R$string;->orders_reload:I

    invoke-direct {p0, v2, p1, v0, v1}, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;->setEmptyView(IIII)V

    return-void

    .line 116
    :cond_0
    sget p1, Lcom/squareup/applet/help/R$string;->orders_none_title:I

    sget v0, Lcom/squareup/applet/help/R$string;->orders_none_message:I

    sget v2, Lcom/squareup/applet/help/R$string;->order_a_reader:I

    invoke-direct {p0, v1, p1, v0, v2}, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;->setEmptyView(IIII)V

    return-void

    .line 122
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;->list:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {p1, v2}, Landroidx/recyclerview/widget/RecyclerView;->setVisibility(I)V

    .line 123
    iget-object p1, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;->orderMessage:Landroid/view/View;

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 3

    .line 79
    invoke-direct {p0, p1}, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;->bindViews(Landroid/view/View;)V

    .line 80
    iget-object v0, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/squareup/applet/help/R$string;->order_history:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonTextBackArrow(Ljava/lang/CharSequence;)V

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    iget-object v1, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;->runner:Lcom/squareup/ui/help/HelpAppletScopeRunner;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/help/orders/-$$Lambda$txCgS4Jx2Zb2EznzFQFzMe2ZISE;

    invoke-direct {v2, v1}, Lcom/squareup/ui/help/orders/-$$Lambda$txCgS4Jx2Zb2EznzFQFzMe2ZISE;-><init>(Lcom/squareup/ui/help/HelpAppletScopeRunner;)V

    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->showUpButton(Ljava/lang/Runnable;)V

    .line 82
    iget-object v0, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar;->hideSecondaryButton()V

    .line 83
    iget-object v0, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;->list:Landroidx/recyclerview/widget/RecyclerView;

    iget-object v1, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;->adapter:Lcom/squareup/ui/help/orders/OrderHistoryCoordinator$OrdersAdapter;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 84
    iget-object v0, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;->emptyView:Lcom/squareup/ui/EmptyView;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/EmptyView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    .line 85
    iget-object v0, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;->emptyView:Lcom/squareup/ui/EmptyView;

    sget v1, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    invoke-virtual {v0, v1}, Lcom/squareup/ui/EmptyView;->setTitleColorRes(I)V

    .line 86
    iget-object v0, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;->actionButton:Landroid/widget/Button;

    new-instance v1, Lcom/squareup/ui/help/orders/-$$Lambda$OrderHistoryCoordinator$eHFBdkhnlEw3sg-S3Mda_NQVUdk;

    invoke-direct {v1, p0}, Lcom/squareup/ui/help/orders/-$$Lambda$OrderHistoryCoordinator$eHFBdkhnlEw3sg-S3Mda_NQVUdk;-><init>(Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;)V

    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 88
    new-instance v0, Lcom/squareup/ui/help/orders/-$$Lambda$OrderHistoryCoordinator$mH2EtQiinEd5w-r-ZfqTr_eKwkA;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/help/orders/-$$Lambda$OrderHistoryCoordinator$mH2EtQiinEd5w-r-ZfqTr_eKwkA;-><init>(Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;Landroid/view/View;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 89
    new-instance v0, Lcom/squareup/ui/help/orders/-$$Lambda$OrderHistoryCoordinator$TFL6gqGRjWX1phpzhG7TjMSrqCU;

    invoke-direct {v0, p0}, Lcom/squareup/ui/help/orders/-$$Lambda$OrderHistoryCoordinator$TFL6gqGRjWX1phpzhG7TjMSrqCU;-><init>(Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public synthetic lambda$attach$0$OrderHistoryCoordinator(Landroid/view/View;)Lrx/Subscription;
    .locals 1

    .line 88
    iget-object v0, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;->spinner:Lcom/squareup/register/widgets/GlassSpinner;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/register/widgets/GlassSpinner;->showOrHideSpinner(Landroid/content/Context;)Lrx/Subscription;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$attach$1$OrderHistoryCoordinator()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 89
    iget-object v0, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;->orderHistory:Lcom/squareup/ui/help/orders/OrderHistory;

    invoke-virtual {v0}, Lcom/squareup/ui/help/orders/OrderHistory;->snapshot()Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;->spinner:Lcom/squareup/register/widgets/GlassSpinner;

    .line 90
    invoke-virtual {v1}, Lcom/squareup/register/widgets/GlassSpinner;->spinnerTransformRx2()Lio/reactivex/ObservableTransformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/help/orders/-$$Lambda$OrderHistoryCoordinator$iTeo-8idqYdmXpEirclUV6wGVs8;

    invoke-direct {v1, p0}, Lcom/squareup/ui/help/orders/-$$Lambda$OrderHistoryCoordinator$iTeo-8idqYdmXpEirclUV6wGVs8;-><init>(Lcom/squareup/ui/help/orders/OrderHistoryCoordinator;)V

    .line 91
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method
