.class public final Lcom/squareup/ui/help/jedi/JediHelpCoordinator_Factory;
.super Ljava/lang/Object;
.source "JediHelpCoordinator_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/help/jedi/JediHelpCoordinator;",
        ">;"
    }
.end annotation


# instance fields
.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final glassSpinnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            ">;"
        }
    .end annotation
.end field

.field private final helpAppletScopeRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/HelpAppletScopeRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final jediHelpScopeRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final messagesVisibilityProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/messages/MessagesVisibility;",
            ">;"
        }
    .end annotation
.end field

.field private final messagingControllerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/MessagingController;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/HelpAppletScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/MessagingController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/messages/MessagesVisibility;",
            ">;)V"
        }
    .end annotation

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator_Factory;->helpAppletScopeRunnerProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p2, p0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator_Factory;->jediHelpScopeRunnerProvider:Ljavax/inject/Provider;

    .line 44
    iput-object p3, p0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    .line 45
    iput-object p4, p0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 46
    iput-object p5, p0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator_Factory;->glassSpinnerProvider:Ljavax/inject/Provider;

    .line 47
    iput-object p6, p0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator_Factory;->messagingControllerProvider:Ljavax/inject/Provider;

    .line 48
    iput-object p7, p0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator_Factory;->messagesVisibilityProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/help/jedi/JediHelpCoordinator_Factory;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/HelpAppletScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/MessagingController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/messages/MessagesVisibility;",
            ">;)",
            "Lcom/squareup/ui/help/jedi/JediHelpCoordinator_Factory;"
        }
    .end annotation

    .line 63
    new-instance v8, Lcom/squareup/ui/help/jedi/JediHelpCoordinator_Factory;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/help/jedi/JediHelpCoordinator_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v8
.end method

.method public static newInstance(Lcom/squareup/ui/help/HelpAppletScopeRunner;Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;Lio/reactivex/Scheduler;Lflow/Flow;Lcom/squareup/register/widgets/GlassSpinner;Lcom/squareup/ui/help/MessagingController;Lcom/squareup/ui/help/messages/MessagesVisibility;)Lcom/squareup/ui/help/jedi/JediHelpCoordinator;
    .locals 9

    .line 70
    new-instance v8, Lcom/squareup/ui/help/jedi/JediHelpCoordinator;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/help/jedi/JediHelpCoordinator;-><init>(Lcom/squareup/ui/help/HelpAppletScopeRunner;Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;Lio/reactivex/Scheduler;Lflow/Flow;Lcom/squareup/register/widgets/GlassSpinner;Lcom/squareup/ui/help/MessagingController;Lcom/squareup/ui/help/messages/MessagesVisibility;)V

    return-object v8
.end method


# virtual methods
.method public get()Lcom/squareup/ui/help/jedi/JediHelpCoordinator;
    .locals 8

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator_Factory;->helpAppletScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/ui/help/HelpAppletScopeRunner;

    iget-object v0, p0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator_Factory;->jediHelpScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;

    iget-object v0, p0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lio/reactivex/Scheduler;

    iget-object v0, p0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lflow/Flow;

    iget-object v0, p0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator_Factory;->glassSpinnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/register/widgets/GlassSpinner;

    iget-object v0, p0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator_Factory;->messagingControllerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/ui/help/MessagingController;

    iget-object v0, p0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator_Factory;->messagesVisibilityProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/ui/help/messages/MessagesVisibility;

    invoke-static/range {v1 .. v7}, Lcom/squareup/ui/help/jedi/JediHelpCoordinator_Factory;->newInstance(Lcom/squareup/ui/help/HelpAppletScopeRunner;Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;Lio/reactivex/Scheduler;Lflow/Flow;Lcom/squareup/register/widgets/GlassSpinner;Lcom/squareup/ui/help/MessagingController;Lcom/squareup/ui/help/messages/MessagesVisibility;)Lcom/squareup/ui/help/jedi/JediHelpCoordinator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/ui/help/jedi/JediHelpCoordinator_Factory;->get()Lcom/squareup/ui/help/jedi/JediHelpCoordinator;

    move-result-object v0

    return-object v0
.end method
