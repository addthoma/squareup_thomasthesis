.class public Lcom/squareup/ui/help/orders/OrderHardwareItemView;
.super Landroid/widget/LinearLayout;
.source "OrderHardwareItemView.java"


# instance fields
.field private final imageView:Landroid/widget/ImageView;

.field private final orderView:Lcom/squareup/marketfont/MarketButton;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .line 25
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x1

    .line 26
    invoke-virtual {p0, v0}, Lcom/squareup/ui/help/orders/OrderHardwareItemView;->setOrientation(I)V

    .line 27
    sget v0, Lcom/squareup/applet/help/R$layout;->order_reader_item_view:I

    invoke-static {p1, v0, p0}, Lcom/squareup/ui/help/orders/OrderHardwareItemView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 29
    sget-object v0, Lcom/squareup/applet/help/R$styleable;->OrderHardwareItemView:[I

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 30
    sget p2, Lcom/squareup/applet/help/R$styleable;->OrderHardwareItemView_title:I

    invoke-virtual {p1, p2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object p2

    .line 31
    sget v0, Lcom/squareup/applet/help/R$styleable;->OrderHardwareItemView_readerSrc:I

    invoke-virtual {p1, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 32
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    .line 34
    sget p1, Lcom/squareup/applet/help/R$id;->image_reader:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/squareup/ui/help/orders/OrderHardwareItemView;->imageView:Landroid/widget/ImageView;

    .line 35
    sget p1, Lcom/squareup/applet/help/R$id;->order_reader:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marketfont/MarketButton;

    iput-object p1, p0, Lcom/squareup/ui/help/orders/OrderHardwareItemView;->orderView:Lcom/squareup/marketfont/MarketButton;

    .line 37
    iget-object p1, p0, Lcom/squareup/ui/help/orders/OrderHardwareItemView;->imageView:Landroid/widget/ImageView;

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 38
    iget-object p1, p0, Lcom/squareup/ui/help/orders/OrderHardwareItemView;->orderView:Lcom/squareup/marketfont/MarketButton;

    invoke-virtual {p1, p2}, Lcom/squareup/marketfont/MarketButton;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .line 42
    iget-object v0, p0, Lcom/squareup/ui/help/orders/OrderHardwareItemView;->imageView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 43
    iget-object v0, p0, Lcom/squareup/ui/help/orders/OrderHardwareItemView;->orderView:Lcom/squareup/marketfont/MarketButton;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setText(I)V
    .locals 1

    .line 51
    iget-object v0, p0, Lcom/squareup/ui/help/orders/OrderHardwareItemView;->orderView:Lcom/squareup/marketfont/MarketButton;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketButton;->setText(I)V

    return-void
.end method

.method public setText(Ljava/lang/CharSequence;)V
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/squareup/ui/help/orders/OrderHardwareItemView;->orderView:Lcom/squareup/marketfont/MarketButton;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketButton;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
