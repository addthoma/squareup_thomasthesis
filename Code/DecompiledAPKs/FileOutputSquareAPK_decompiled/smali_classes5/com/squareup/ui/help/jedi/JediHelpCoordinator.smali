.class public final Lcom/squareup/ui/help/jedi/JediHelpCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "JediHelpCoordinator.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000v\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u0001BA\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0008\u0008\u0001\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u00a2\u0006\u0002\u0010\u0010J\u0010\u0010\u001e\u001a\u00020\u001b2\u0006\u0010\u001f\u001a\u00020 H\u0016J\u0010\u0010!\u001a\u00020\u001b2\u0006\u0010\u001f\u001a\u00020 H\u0002J\u0018\u0010\"\u001a\u00020#2\u0006\u0010$\u001a\u00020\u00152\u0006\u0010%\u001a\u00020&H\u0002R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R2\u0010\u0013\u001a&\u0012\u000c\u0012\n \u0016*\u0004\u0018\u00010\u00150\u0015 \u0016*\u0012\u0012\u000c\u0012\n \u0016*\u0004\u0018\u00010\u00150\u0015\u0018\u00010\u00140\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0019\u001a\u0010\u0012\u000c\u0012\n \u0016*\u0004\u0018\u00010\u001b0\u001b0\u001aX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u001dX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\'"
    }
    d2 = {
        "Lcom/squareup/ui/help/jedi/JediHelpCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "helpAppletScopeRunner",
        "Lcom/squareup/ui/help/HelpAppletScopeRunner;",
        "jediHelpScopeRunner",
        "Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "flow",
        "Lflow/Flow;",
        "glassSpinner",
        "Lcom/squareup/register/widgets/GlassSpinner;",
        "messagingController",
        "Lcom/squareup/ui/help/MessagingController;",
        "messagesVisibility",
        "Lcom/squareup/ui/help/messages/MessagesVisibility;",
        "(Lcom/squareup/ui/help/HelpAppletScopeRunner;Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;Lio/reactivex/Scheduler;Lflow/Flow;Lcom/squareup/register/widgets/GlassSpinner;Lcom/squareup/ui/help/MessagingController;Lcom/squareup/ui/help/messages/MessagesVisibility;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/MarinActionBar;",
        "headerSubject",
        "Lrx/subjects/BehaviorSubject;",
        "",
        "kotlin.jvm.PlatformType",
        "jediPanelView",
        "Lcom/squareup/jedi/ui/JediPanelView;",
        "onLaunchHelpshift",
        "Lcom/jakewharton/rxrelay2/PublishRelay;",
        "",
        "speechBubbleImageView",
        "Lcom/squareup/jedi/SpeechBubbleImageView;",
        "attach",
        "view",
        "Landroid/view/View;",
        "bindView",
        "getActionBarConfig",
        "Lcom/squareup/marin/widgets/MarinActionBar$Config;",
        "name",
        "firstScreen",
        "",
        "help_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private final flow:Lflow/Flow;

.field private final glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

.field private final headerSubject:Lrx/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lrx/subjects/BehaviorSubject<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final helpAppletScopeRunner:Lcom/squareup/ui/help/HelpAppletScopeRunner;

.field private final jediHelpScopeRunner:Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;

.field private jediPanelView:Lcom/squareup/jedi/ui/JediPanelView;

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final messagesVisibility:Lcom/squareup/ui/help/messages/MessagesVisibility;

.field private final messagingController:Lcom/squareup/ui/help/MessagingController;

.field private final onLaunchHelpshift:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private speechBubbleImageView:Lcom/squareup/jedi/SpeechBubbleImageView;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/help/HelpAppletScopeRunner;Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;Lio/reactivex/Scheduler;Lflow/Flow;Lcom/squareup/register/widgets/GlassSpinner;Lcom/squareup/ui/help/MessagingController;Lcom/squareup/ui/help/messages/MessagesVisibility;)V
    .locals 1
    .param p3    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "helpAppletScopeRunner"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "jediHelpScopeRunner"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "flow"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "glassSpinner"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "messagingController"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "messagesVisibility"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator;->helpAppletScopeRunner:Lcom/squareup/ui/help/HelpAppletScopeRunner;

    iput-object p2, p0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator;->jediHelpScopeRunner:Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;

    iput-object p3, p0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator;->mainScheduler:Lio/reactivex/Scheduler;

    iput-object p4, p0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator;->flow:Lflow/Flow;

    iput-object p5, p0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    iput-object p6, p0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator;->messagingController:Lcom/squareup/ui/help/MessagingController;

    iput-object p7, p0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator;->messagesVisibility:Lcom/squareup/ui/help/messages/MessagesVisibility;

    .line 43
    invoke-static {}, Lrx/subjects/BehaviorSubject;->create()Lrx/subjects/BehaviorSubject;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator;->headerSubject:Lrx/subjects/BehaviorSubject;

    .line 44
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object p1

    const-string p2, "PublishRelay.create<Unit>()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator;->onLaunchHelpshift:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-void
.end method

.method public static final synthetic access$getActionBar$p(Lcom/squareup/ui/help/jedi/JediHelpCoordinator;)Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 32
    iget-object p0, p0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    if-nez p0, :cond_0

    const-string v0, "actionBar"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getActionBarConfig(Lcom/squareup/ui/help/jedi/JediHelpCoordinator;Ljava/lang/String;Z)Lcom/squareup/marin/widgets/MarinActionBar$Config;
    .locals 0

    .line 32
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/help/jedi/JediHelpCoordinator;->getActionBarConfig(Ljava/lang/String;Z)Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getFlow$p(Lcom/squareup/ui/help/jedi/JediHelpCoordinator;)Lflow/Flow;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator;->flow:Lflow/Flow;

    return-object p0
.end method

.method public static final synthetic access$getGlassSpinner$p(Lcom/squareup/ui/help/jedi/JediHelpCoordinator;)Lcom/squareup/register/widgets/GlassSpinner;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    return-object p0
.end method

.method public static final synthetic access$getHeaderSubject$p(Lcom/squareup/ui/help/jedi/JediHelpCoordinator;)Lrx/subjects/BehaviorSubject;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator;->headerSubject:Lrx/subjects/BehaviorSubject;

    return-object p0
.end method

.method public static final synthetic access$getJediHelpScopeRunner$p(Lcom/squareup/ui/help/jedi/JediHelpCoordinator;)Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator;->jediHelpScopeRunner:Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;

    return-object p0
.end method

.method public static final synthetic access$getJediPanelView$p(Lcom/squareup/ui/help/jedi/JediHelpCoordinator;)Lcom/squareup/jedi/ui/JediPanelView;
    .locals 1

    .line 32
    iget-object p0, p0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator;->jediPanelView:Lcom/squareup/jedi/ui/JediPanelView;

    if-nez p0, :cond_0

    const-string v0, "jediPanelView"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getMainScheduler$p(Lcom/squareup/ui/help/jedi/JediHelpCoordinator;)Lio/reactivex/Scheduler;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator;->mainScheduler:Lio/reactivex/Scheduler;

    return-object p0
.end method

.method public static final synthetic access$getMessagesVisibility$p(Lcom/squareup/ui/help/jedi/JediHelpCoordinator;)Lcom/squareup/ui/help/messages/MessagesVisibility;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator;->messagesVisibility:Lcom/squareup/ui/help/messages/MessagesVisibility;

    return-object p0
.end method

.method public static final synthetic access$getMessagingController$p(Lcom/squareup/ui/help/jedi/JediHelpCoordinator;)Lcom/squareup/ui/help/MessagingController;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator;->messagingController:Lcom/squareup/ui/help/MessagingController;

    return-object p0
.end method

.method public static final synthetic access$getOnLaunchHelpshift$p(Lcom/squareup/ui/help/jedi/JediHelpCoordinator;)Lcom/jakewharton/rxrelay2/PublishRelay;
    .locals 0

    .line 32
    iget-object p0, p0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator;->onLaunchHelpshift:Lcom/jakewharton/rxrelay2/PublishRelay;

    return-object p0
.end method

.method public static final synthetic access$getSpeechBubbleImageView$p(Lcom/squareup/ui/help/jedi/JediHelpCoordinator;)Lcom/squareup/jedi/SpeechBubbleImageView;
    .locals 1

    .line 32
    iget-object p0, p0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator;->speechBubbleImageView:Lcom/squareup/jedi/SpeechBubbleImageView;

    if-nez p0, :cond_0

    const-string v0, "speechBubbleImageView"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$setActionBar$p(Lcom/squareup/ui/help/jedi/JediHelpCoordinator;Lcom/squareup/marin/widgets/MarinActionBar;)V
    .locals 0

    .line 32
    iput-object p1, p0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    return-void
.end method

.method public static final synthetic access$setJediPanelView$p(Lcom/squareup/ui/help/jedi/JediHelpCoordinator;Lcom/squareup/jedi/ui/JediPanelView;)V
    .locals 0

    .line 32
    iput-object p1, p0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator;->jediPanelView:Lcom/squareup/jedi/ui/JediPanelView;

    return-void
.end method

.method public static final synthetic access$setSpeechBubbleImageView$p(Lcom/squareup/ui/help/jedi/JediHelpCoordinator;Lcom/squareup/jedi/SpeechBubbleImageView;)V
    .locals 0

    .line 32
    iput-object p1, p0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator;->speechBubbleImageView:Lcom/squareup/jedi/SpeechBubbleImageView;

    return-void
.end method

.method private final bindView(Landroid/view/View;)V
    .locals 3

    .line 154
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    const-string v1, "(view.findById<ActionBar\u2026ar))\n          .presenter"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 156
    sget v0, Lcom/squareup/applet/help/R$id;->jedi_panel:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/jedi/ui/JediPanelView;

    iput-object v0, p0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator;->jediPanelView:Lcom/squareup/jedi/ui/JediPanelView;

    .line 159
    new-instance v0, Lcom/squareup/jedi/SpeechBubbleImageView;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string/jumbo v2, "view.context"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const-string/jumbo v2, "view.resources"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, v1, p1}, Lcom/squareup/jedi/SpeechBubbleImageView;-><init>(Landroid/content/Context;Landroid/content/res/Resources;)V

    iput-object v0, p0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator;->speechBubbleImageView:Lcom/squareup/jedi/SpeechBubbleImageView;

    return-void
.end method

.method private final getActionBarConfig(Ljava/lang/String;Z)Lcom/squareup/marin/widgets/MarinActionBar$Config;
    .locals 2

    .line 143
    iget-object v0, p0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator;->helpAppletScopeRunner:Lcom/squareup/ui/help/HelpAppletScopeRunner;

    xor-int/lit8 v1, p2, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/ui/help/HelpAppletScopeRunner;->getActionBarConfigBuilder(Ljava/lang/String;Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    if-eqz p2, :cond_0

    .line 144
    iget-object p2, p0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator;->messagingController:Lcom/squareup/ui/help/MessagingController;

    invoke-interface {p2}, Lcom/squareup/ui/help/MessagingController;->showMessagingIcon()Z

    move-result p2

    if-eqz p2, :cond_0

    .line 146
    new-instance p2, Lcom/squareup/ui/help/jedi/JediHelpCoordinator$getActionBarConfig$1;

    invoke-direct {p2, p0}, Lcom/squareup/ui/help/jedi/JediHelpCoordinator$getActionBarConfig$1;-><init>(Lcom/squareup/ui/help/jedi/JediHelpCoordinator;)V

    check-cast p2, Ljava/lang/Runnable;

    invoke-virtual {p1, p2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showCustomView(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p2

    .line 147
    new-instance v0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator$getActionBarConfig$2;

    invoke-direct {v0, p0}, Lcom/squareup/ui/help/jedi/JediHelpCoordinator$getActionBarConfig$2;-><init>(Lcom/squareup/ui/help/jedi/JediHelpCoordinator;)V

    check-cast v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$CustomViewConfig$CustomViewFactory;

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setCustomView(Lcom/squareup/marin/widgets/MarinActionBar$Config$CustomViewConfig$CustomViewFactory;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 149
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    const-string p2, "config.build()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 4

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    invoke-direct {p0, p1}, Lcom/squareup/ui/help/jedi/JediHelpCoordinator;->bindView(Landroid/view/View;)V

    .line 53
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 54
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Landroid/content/Context;)Lflow/path/Path;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/help/jedi/JediHelpScreen;

    .line 57
    iget-object v2, p0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator;->headerSubject:Lrx/subjects/BehaviorSubject;

    invoke-virtual {v1}, Lcom/squareup/ui/help/jedi/JediHelpScreen;->getFirstScreen()Z

    move-result v3

    if-eqz v3, :cond_0

    sget v3, Lcom/squareup/applet/help/R$string;->help:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const-string v0, ""

    :goto_0
    invoke-virtual {v2, v0}, Lrx/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    .line 60
    new-instance v0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$1;

    invoke-direct {v0, p0, v1}, Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$1;-><init>(Lcom/squareup/ui/help/jedi/JediHelpCoordinator;Lcom/squareup/ui/help/jedi/JediHelpScreen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 73
    new-instance v0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$2;

    invoke-direct {v0, p0, v1}, Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$2;-><init>(Lcom/squareup/ui/help/jedi/JediHelpCoordinator;Lcom/squareup/ui/help/jedi/JediHelpScreen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 86
    new-instance v0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$3;

    invoke-direct {v0, p0, v1}, Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$3;-><init>(Lcom/squareup/ui/help/jedi/JediHelpCoordinator;Lcom/squareup/ui/help/jedi/JediHelpScreen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 102
    new-instance v0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$4;

    invoke-direct {v0, p0, v1}, Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$4;-><init>(Lcom/squareup/ui/help/jedi/JediHelpCoordinator;Lcom/squareup/ui/help/jedi/JediHelpScreen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 109
    new-instance v0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$5;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$5;-><init>(Lcom/squareup/ui/help/jedi/JediHelpCoordinator;Landroid/view/View;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 115
    new-instance v0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$6;

    invoke-direct {v0, p0}, Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$6;-><init>(Lcom/squareup/ui/help/jedi/JediHelpCoordinator;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 135
    new-instance v0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$7;

    invoke-direct {v0, p0}, Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$7;-><init>(Lcom/squareup/ui/help/jedi/JediHelpCoordinator;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
