.class public Lcom/squareup/ui/help/messages/HelpshiftMessagesActivity;
.super Landroidx/appcompat/app/AppCompatActivity;
.source "HelpshiftMessagesActivity.java"


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private helpshiftIsRunning:Z

.field private useHelpshift:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 31
    invoke-direct {p0}, Landroidx/appcompat/app/AppCompatActivity;-><init>()V

    const/4 v0, 0x0

    .line 35
    iput-boolean v0, p0, Lcom/squareup/ui/help/messages/HelpshiftMessagesActivity;->helpshiftIsRunning:Z

    return-void
.end method

.method private attachMessagingFragment(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/messagehub/service/CustomIssueField;",
            ">;)V"
        }
    .end annotation

    .line 76
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 77
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/protos/messagehub/service/CustomIssueField;

    .line 78
    iget-object v3, v1, Lcom/squareup/protos/messagehub/service/CustomIssueField;->issue_key:Ljava/lang/String;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v6, v1, Lcom/squareup/protos/messagehub/service/CustomIssueField;->issue_data_type:Ljava/lang/String;

    aput-object v6, v4, v5

    iget-object v1, v1, Lcom/squareup/protos/messagehub/service/CustomIssueField;->issue_data_value:Ljava/lang/String;

    aput-object v1, v4, v2

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 87
    :cond_0
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    .line 88
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const-string v2, "enableDefaultConversationalFiling"

    invoke-virtual {p1, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    new-instance v1, Lcom/helpshift/support/ApiConfig$Builder;

    invoke-direct {v1}, Lcom/helpshift/support/ApiConfig$Builder;-><init>()V

    .line 91
    invoke-virtual {v1, v0}, Lcom/helpshift/support/ApiConfig$Builder;->setCustomIssueFields(Ljava/util/Map;)Lcom/helpshift/support/ApiConfig$Builder;

    move-result-object v0

    .line 92
    invoke-virtual {v0, p1}, Lcom/helpshift/support/ApiConfig$Builder;->setExtras(Ljava/util/Map;)Lcom/helpshift/support/ApiConfig$Builder;

    move-result-object p1

    .line 93
    invoke-virtual {p1}, Lcom/helpshift/support/ApiConfig$Builder;->build()Lcom/helpshift/support/ApiConfig;

    move-result-object p1

    .line 96
    iget-boolean v0, p0, Lcom/squareup/ui/help/messages/HelpshiftMessagesActivity;->useHelpshift:Z

    if-eqz v0, :cond_1

    .line 97
    invoke-static {p0, p1}, Lcom/helpshift/support/Support;->getConversationFragment(Landroid/app/Activity;Lcom/helpshift/support/ApiConfig;)Lcom/helpshift/support/fragments/SupportFragment;

    move-result-object p1

    goto :goto_1

    .line 100
    :cond_1
    new-instance p1, Landroidx/fragment/app/Fragment;

    invoke-direct {p1}, Landroidx/fragment/app/Fragment;-><init>()V

    .line 103
    :goto_1
    invoke-virtual {p0}, Lcom/squareup/ui/help/messages/HelpshiftMessagesActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    .line 104
    invoke-virtual {v0}, Landroidx/fragment/app/FragmentManager;->beginTransaction()Landroidx/fragment/app/FragmentTransaction;

    move-result-object v0

    .line 105
    sget v1, Lcom/squareup/applet/help/R$id;->fragment_container:I

    invoke-virtual {v0, v1, p1}, Landroidx/fragment/app/FragmentTransaction;->add(ILandroidx/fragment/app/Fragment;)Landroidx/fragment/app/FragmentTransaction;

    .line 106
    invoke-virtual {v0}, Landroidx/fragment/app/FragmentTransaction;->commit()I

    return-void
.end method

.method private startHelpShift()V
    .locals 1

    .line 136
    iget-boolean v0, p0, Lcom/squareup/ui/help/messages/HelpshiftMessagesActivity;->helpshiftIsRunning:Z

    if-nez v0, :cond_0

    .line 137
    invoke-static {}, Lcom/helpshift/CoreInternal;->onAppForeground()V

    const/4 v0, 0x1

    .line 138
    iput-boolean v0, p0, Lcom/squareup/ui/help/messages/HelpshiftMessagesActivity;->helpshiftIsRunning:Z

    :cond_0
    return-void
.end method

.method private stopHelpShift()V
    .locals 1

    .line 143
    invoke-static {}, Lcom/helpshift/CoreInternal;->onAppBackground()V

    const/4 v0, 0x0

    .line 144
    iput-boolean v0, p0, Lcom/squareup/ui/help/messages/HelpshiftMessagesActivity;->helpshiftIsRunning:Z

    return-void
.end method


# virtual methods
.method public finish()V
    .locals 2

    .line 121
    invoke-super {p0}, Landroidx/appcompat/app/AppCompatActivity;->finish()V

    .line 124
    iget-boolean v0, p0, Lcom/squareup/ui/help/messages/HelpshiftMessagesActivity;->useHelpshift:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/ui/help/messages/HelpshiftMessagesActivity;->isChangingConfigurations()Z

    move-result v0

    if-nez v0, :cond_0

    .line 125
    invoke-direct {p0}, Lcom/squareup/ui/help/messages/HelpshiftMessagesActivity;->stopHelpShift()V

    .line 128
    :cond_0
    sget v0, Lcom/squareup/widgets/R$anim;->stay_put:I

    sget v1, Lcom/squareup/widgets/R$anim;->slide_out_bottom_opaque:I

    invoke-virtual {p0, v0, v1}, Lcom/squareup/ui/help/messages/HelpshiftMessagesActivity;->overridePendingTransition(II)V

    return-void
.end method

.method public getActionBarConfig(Ljava/lang/String;)Lcom/squareup/marin/widgets/MarinActionBar$Config;
    .locals 2

    .line 66
    new-instance v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 67
    invoke-virtual {v0, v1, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/help/messages/-$$Lambda$esaCMP2v6Wf16g596OIYxNloNbo;

    invoke-direct {v0, p0}, Lcom/squareup/ui/help/messages/-$$Lambda$esaCMP2v6Wf16g596OIYxNloNbo;-><init>(Lcom/squareup/ui/help/messages/HelpshiftMessagesActivity;)V

    .line 68
    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 69
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    return-object p1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .line 38
    invoke-super {p0, p1}, Landroidx/appcompat/app/AppCompatActivity;->onCreate(Landroid/os/Bundle;)V

    .line 41
    invoke-virtual {p0}, Lcom/squareup/ui/help/messages/HelpshiftMessagesActivity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string/jumbo v0, "useHelpshift"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/ui/help/messages/HelpshiftMessagesActivity;->useHelpshift:Z

    .line 43
    invoke-virtual {p0}, Lcom/squareup/ui/help/messages/HelpshiftMessagesActivity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    const-string v0, "IntentBundle"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object p1

    const-string v0, "customIssueFields"

    .line 45
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    .line 52
    iget-boolean v0, p0, Lcom/squareup/ui/help/messages/HelpshiftMessagesActivity;->useHelpshift:Z

    if-eqz v0, :cond_0

    .line 53
    invoke-direct {p0}, Lcom/squareup/ui/help/messages/HelpshiftMessagesActivity;->startHelpShift()V

    .line 56
    :cond_0
    sget v0, Lcom/squareup/applet/help/R$layout;->messages_view:I

    invoke-virtual {p0, v0}, Lcom/squareup/ui/help/messages/HelpshiftMessagesActivity;->setContentView(I)V

    .line 58
    invoke-direct {p0, p1}, Lcom/squareup/ui/help/messages/HelpshiftMessagesActivity;->attachMessagingFragment(Ljava/util/List;)V

    .line 60
    sget p1, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-virtual {p0, p1}, Lcom/squareup/ui/help/messages/HelpshiftMessagesActivity;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marin/widgets/ActionBarView;

    .line 61
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/help/messages/HelpshiftMessagesActivity;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 62
    iget-object p1, p0, Lcom/squareup/ui/help/messages/HelpshiftMessagesActivity;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    sget v0, Lcom/squareup/applet/help/R$string;->message_us:I

    invoke-virtual {p0, v0}, Lcom/squareup/ui/help/messages/HelpshiftMessagesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/ui/help/messages/HelpshiftMessagesActivity;->getActionBarConfig(Ljava/lang/String;)Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method protected onStart()V
    .locals 2

    .line 110
    invoke-super {p0}, Landroidx/appcompat/app/AppCompatActivity;->onStart()V

    .line 112
    iget-boolean v0, p0, Lcom/squareup/ui/help/messages/HelpshiftMessagesActivity;->useHelpshift:Z

    if-eqz v0, :cond_0

    .line 113
    invoke-direct {p0}, Lcom/squareup/ui/help/messages/HelpshiftMessagesActivity;->startHelpShift()V

    .line 116
    :cond_0
    sget v0, Lcom/squareup/widgets/R$anim;->slide_in_bottom_opaque:I

    sget v1, Lcom/squareup/widgets/R$anim;->stay_put:I

    invoke-virtual {p0, v0, v1}, Lcom/squareup/ui/help/messages/HelpshiftMessagesActivity;->overridePendingTransition(II)V

    return-void
.end method
