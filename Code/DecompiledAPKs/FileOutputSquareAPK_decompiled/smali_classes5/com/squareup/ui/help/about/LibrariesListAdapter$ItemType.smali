.class public final enum Lcom/squareup/ui/help/about/LibrariesListAdapter$ItemType;
.super Ljava/lang/Enum;
.source "LibrariesListAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/help/about/LibrariesListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ItemType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/help/about/LibrariesListAdapter$ItemType;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0008\u0008\u0080\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006j\u0002\u0008\u0007j\u0002\u0008\u0008j\u0002\u0008\tj\u0002\u0008\n\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/ui/help/about/LibrariesListAdapter$ItemType;",
        "",
        "value",
        "",
        "(Ljava/lang/String;II)V",
        "getValue",
        "()I",
        "LicenseHeader",
        "LibraryItem",
        "Divider",
        "Footer",
        "help_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/help/about/LibrariesListAdapter$ItemType;

.field public static final enum Divider:Lcom/squareup/ui/help/about/LibrariesListAdapter$ItemType;

.field public static final enum Footer:Lcom/squareup/ui/help/about/LibrariesListAdapter$ItemType;

.field public static final enum LibraryItem:Lcom/squareup/ui/help/about/LibrariesListAdapter$ItemType;

.field public static final enum LicenseHeader:Lcom/squareup/ui/help/about/LibrariesListAdapter$ItemType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/ui/help/about/LibrariesListAdapter$ItemType;

    new-instance v1, Lcom/squareup/ui/help/about/LibrariesListAdapter$ItemType;

    const/4 v2, 0x0

    const-string v3, "LicenseHeader"

    .line 61
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/ui/help/about/LibrariesListAdapter$ItemType;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/ui/help/about/LibrariesListAdapter$ItemType;->LicenseHeader:Lcom/squareup/ui/help/about/LibrariesListAdapter$ItemType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/LibrariesListAdapter$ItemType;

    const/4 v2, 0x1

    const-string v3, "LibraryItem"

    .line 62
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/ui/help/about/LibrariesListAdapter$ItemType;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/ui/help/about/LibrariesListAdapter$ItemType;->LibraryItem:Lcom/squareup/ui/help/about/LibrariesListAdapter$ItemType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/LibrariesListAdapter$ItemType;

    const/4 v2, 0x2

    const-string v3, "Divider"

    .line 63
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/ui/help/about/LibrariesListAdapter$ItemType;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/ui/help/about/LibrariesListAdapter$ItemType;->Divider:Lcom/squareup/ui/help/about/LibrariesListAdapter$ItemType;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/ui/help/about/LibrariesListAdapter$ItemType;

    const/4 v2, 0x3

    const-string v3, "Footer"

    .line 64
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/ui/help/about/LibrariesListAdapter$ItemType;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/ui/help/about/LibrariesListAdapter$ItemType;->Footer:Lcom/squareup/ui/help/about/LibrariesListAdapter$ItemType;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/ui/help/about/LibrariesListAdapter$ItemType;->$VALUES:[Lcom/squareup/ui/help/about/LibrariesListAdapter$ItemType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 60
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/squareup/ui/help/about/LibrariesListAdapter$ItemType;->value:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/help/about/LibrariesListAdapter$ItemType;
    .locals 1

    const-class v0, Lcom/squareup/ui/help/about/LibrariesListAdapter$ItemType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/help/about/LibrariesListAdapter$ItemType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/help/about/LibrariesListAdapter$ItemType;
    .locals 1

    sget-object v0, Lcom/squareup/ui/help/about/LibrariesListAdapter$ItemType;->$VALUES:[Lcom/squareup/ui/help/about/LibrariesListAdapter$ItemType;

    invoke-virtual {v0}, [Lcom/squareup/ui/help/about/LibrariesListAdapter$ItemType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/help/about/LibrariesListAdapter$ItemType;

    return-object v0
.end method


# virtual methods
.method public final getValue()I
    .locals 1

    .line 60
    iget v0, p0, Lcom/squareup/ui/help/about/LibrariesListAdapter$ItemType;->value:I

    return v0
.end method
