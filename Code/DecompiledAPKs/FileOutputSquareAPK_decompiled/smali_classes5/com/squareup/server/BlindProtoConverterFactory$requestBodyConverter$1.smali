.class final Lcom/squareup/server/BlindProtoConverterFactory$requestBodyConverter$1;
.super Ljava/lang/Object;
.source "BlindProtoConverterFactory.kt"

# interfaces
.implements Lretrofit2/Converter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/server/BlindProtoConverterFactory;->requestBodyConverter(Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;[Ljava/lang/annotation/Annotation;Lretrofit2/Retrofit;)Lretrofit2/Converter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<F:",
        "Ljava/lang/Object;",
        "T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lretrofit2/Converter<",
        "[B",
        "Lokhttp3/RequestBody;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0012\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lokhttp3/RequestBody;",
        "it",
        "",
        "kotlin.jvm.PlatformType",
        "convert"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/server/BlindProtoConverterFactory$requestBodyConverter$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/server/BlindProtoConverterFactory$requestBodyConverter$1;

    invoke-direct {v0}, Lcom/squareup/server/BlindProtoConverterFactory$requestBodyConverter$1;-><init>()V

    sput-object v0, Lcom/squareup/server/BlindProtoConverterFactory$requestBodyConverter$1;->INSTANCE:Lcom/squareup/server/BlindProtoConverterFactory$requestBodyConverter$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic convert(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 17
    check-cast p1, [B

    invoke-virtual {p0, p1}, Lcom/squareup/server/BlindProtoConverterFactory$requestBodyConverter$1;->convert([B)Lokhttp3/RequestBody;

    move-result-object p1

    return-object p1
.end method

.method public final convert([B)Lokhttp3/RequestBody;
    .locals 4

    .line 36
    sget-object v0, Lokhttp3/RequestBody;->Companion:Lokhttp3/RequestBody$Companion;

    const-string v1, "it"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Lcom/squareup/server/BlindProtoConverterFactoryKt;->access$getMIME_TYPE$p()Lokhttp3/MediaType;

    move-result-object v1

    array-length v2, p1

    const/4 v3, 0x0

    invoke-virtual {v0, p1, v1, v3, v2}, Lokhttp3/RequestBody$Companion;->create([BLokhttp3/MediaType;II)Lokhttp3/RequestBody;

    move-result-object p1

    return-object p1
.end method
