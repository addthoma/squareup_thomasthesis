.class public Lcom/squareup/server/employees/ClockInOutResponse;
.super Lcom/squareup/server/SimpleResponse;
.source "ClockInOutResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/employees/ClockInOutResponse$TimeCard;
    }
.end annotation


# instance fields
.field public final status:Ljava/lang/String;

.field public final timecard:Lcom/squareup/server/employees/ClockInOutResponse$TimeCard;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/squareup/server/employees/ClockInOutResponse$TimeCard;)V
    .locals 1

    const/4 v0, 0x1

    .line 11
    invoke-direct {p0, v0}, Lcom/squareup/server/SimpleResponse;-><init>(Z)V

    .line 12
    iput-object p1, p0, Lcom/squareup/server/employees/ClockInOutResponse;->status:Ljava/lang/String;

    .line 13
    iput-object p2, p0, Lcom/squareup/server/employees/ClockInOutResponse;->timecard:Lcom/squareup/server/employees/ClockInOutResponse$TimeCard;

    return-void
.end method
