.class public interface abstract Lcom/squareup/server/bills/StoreAndForwardBillService;
.super Ljava/lang/Object;
.source "StoreAndForwardBillService.java"


# static fields
.field public static final ENQUEUE_URL:Ljava/lang/String; = "/1.0/bills/enqueue"


# virtual methods
.method public abstract enqueue(Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest;)Lrx/Observable;
    .param p1    # Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/store_and_forward/bills/QueueRequest;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/store_and_forward/bills/QueueResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit/http/Headers;
        value = {
            "X-Square-Gzip: true"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/1.0/bills/enqueue"
    .end annotation
.end method

.method public abstract getBillsStatus(Lcom/squareup/protos/client/store_and_forward/bills/GetBillsStatusRequest;)Lrx/Observable;
    .param p1    # Lcom/squareup/protos/client/store_and_forward/bills/GetBillsStatusRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/store_and_forward/bills/GetBillsStatusRequest;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/store_and_forward/bills/GetBillsStatusResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/1.0/bills/get-bills-status"
    .end annotation
.end method
