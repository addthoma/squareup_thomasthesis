.class public Lcom/squareup/server/ContentProviderImageResolver;
.super Ljava/lang/Object;
.source "ContentProviderImageResolver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/ContentProviderImageResolver$Info;
    }
.end annotation


# instance fields
.field private final context:Landroid/content/Context;

.field private info:Lcom/squareup/server/ContentProviderImageResolver$Info;

.field private final uri:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/squareup/server/ContentProviderImageResolver;->context:Landroid/content/Context;

    .line 38
    iput-object p2, p0, Lcom/squareup/server/ContentProviderImageResolver;->uri:Landroid/net/Uri;

    return-void
.end method

.method private info()Lcom/squareup/server/ContentProviderImageResolver$Info;
    .locals 12

    .line 68
    iget-object v0, p0, Lcom/squareup/server/ContentProviderImageResolver;->info:Lcom/squareup/server/ContentProviderImageResolver$Info;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    .line 74
    :try_start_0
    invoke-direct {p0}, Lcom/squareup/server/ContentProviderImageResolver;->queryUri()Landroid/database/Cursor;

    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-eqz v1, :cond_1

    .line 76
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-nez v2, :cond_1

    .line 78
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move-object v1, v0

    goto :goto_0

    :catchall_0
    move-exception v0

    goto/16 :goto_7

    :cond_1
    :goto_0
    const/4 v2, -0x1

    if-eqz v1, :cond_2

    const-string v2, "_display_name"

    .line 90
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    const-string v3, "mime_type"

    .line 91
    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    const-string v4, "_size"

    .line 92
    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    :cond_2
    const/4 v3, -0x1

    const/4 v4, -0x1

    :goto_1
    if-ltz v2, :cond_3

    .line 99
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    :goto_2
    move-object v6, v5

    goto :goto_3

    .line 101
    :cond_3
    iget-object v0, p0, Lcom/squareup/server/ContentProviderImageResolver;->uri:Landroid/net/Uri;

    invoke-virtual {p0, v0}, Lcom/squareup/server/ContentProviderImageResolver;->getFile(Landroid/net/Uri;)Ljava/io/File;

    move-result-object v0

    .line 102
    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    goto :goto_2

    :goto_3
    if-ltz v3, :cond_5

    .line 107
    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    :cond_4
    :goto_4
    move-object v7, v5

    goto :goto_5

    .line 109
    :cond_5
    iget-object v5, p0, Lcom/squareup/server/ContentProviderImageResolver;->uri:Landroid/net/Uri;

    invoke-virtual {p0, v5}, Lcom/squareup/server/ContentProviderImageResolver;->getMimeType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_4

    const-string v5, "application/octet-stream"

    goto :goto_4

    :goto_5
    if-ltz v4, :cond_6

    .line 117
    invoke-interface {v1, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    goto :goto_6

    :cond_6
    if-nez v0, :cond_7

    .line 120
    iget-object v0, p0, Lcom/squareup/server/ContentProviderImageResolver;->uri:Landroid/net/Uri;

    invoke-virtual {p0, v0}, Lcom/squareup/server/ContentProviderImageResolver;->getFile(Landroid/net/Uri;)Ljava/io/File;

    move-result-object v0

    .line 122
    :cond_7
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_8

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v8

    goto :goto_6

    :cond_8
    const-wide/16 v8, -0x1

    .line 125
    :goto_6
    new-instance v0, Lcom/squareup/server/ContentProviderImageResolver$Info;

    const/4 v10, 0x0

    move-object v5, v0

    invoke-direct/range {v5 .. v10}, Lcom/squareup/server/ContentProviderImageResolver$Info;-><init>(Ljava/lang/String;Ljava/lang/String;JLcom/squareup/server/ContentProviderImageResolver$1;)V

    if-ltz v2, :cond_9

    if-ltz v3, :cond_9

    if-ltz v4, :cond_9

    .line 127
    iput-object v0, p0, Lcom/squareup/server/ContentProviderImageResolver;->info:Lcom/squareup/server/ContentProviderImageResolver$Info;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_9
    if-eqz v1, :cond_a

    .line 133
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_a
    return-object v0

    :catchall_1
    move-exception v1

    move-object v11, v1

    move-object v1, v0

    move-object v0, v11

    :goto_7
    if-eqz v1, :cond_b

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 135
    :cond_b
    throw v0
.end method

.method private isActivityDestroyed(Landroid/app/Activity;)Z
    .locals 0

    .line 167
    invoke-virtual {p1}, Landroid/app/Activity;->isDestroyed()Z

    move-result p1

    return p1
.end method

.method private queryUri()Landroid/database/Cursor;
    .locals 9

    const-string v0, "_display_name"

    const-string v1, "mime_type"

    const-string v2, "_size"

    .line 139
    filled-new-array {v0, v1, v2}, [Ljava/lang/String;

    move-result-object v5

    .line 144
    iget-object v0, p0, Lcom/squareup/server/ContentProviderImageResolver;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    if-nez v3, :cond_2

    .line 147
    iget-object v0, p0, Lcom/squareup/server/ContentProviderImageResolver;->context:Landroid/content/Context;

    .line 148
    :goto_0
    instance-of v1, v0, Landroid/content/ContextWrapper;

    if-eqz v1, :cond_0

    instance-of v1, v0, Landroid/app/Activity;

    if-nez v1, :cond_0

    .line 149
    check-cast v0, Landroid/content/ContextWrapper;

    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    goto :goto_0

    .line 153
    :cond_0
    instance-of v1, v0, Landroid/app/Activity;

    if-eqz v1, :cond_1

    .line 154
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, " is destroyed? "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    check-cast v0, Landroid/app/Activity;

    invoke-direct {p0, v0}, Lcom/squareup/server/ContentProviderImageResolver;->isActivityDestroyed(Landroid/app/Activity;)Z

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 156
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "baseContext is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 159
    :goto_1
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "contentResolver is null"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    invoke-static {v1, v0}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;Ljava/lang/String;)V

    const/4 v0, 0x0

    return-object v0

    .line 163
    :cond_2
    iget-object v4, p0, Lcom/squareup/server/ContentProviderImageResolver;->uri:Landroid/net/Uri;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public fileName()Ljava/lang/String;
    .locals 1

    .line 42
    invoke-direct {p0}, Lcom/squareup/server/ContentProviderImageResolver;->info()Lcom/squareup/server/ContentProviderImageResolver$Info;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/server/ContentProviderImageResolver$Info;->name:Ljava/lang/String;

    return-object v0
.end method

.method getFile(Landroid/net/Uri;)Ljava/io/File;
    .locals 1

    .line 171
    new-instance v0, Ljava/io/File;

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method getMimeType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1

    .line 175
    invoke-static {}, Landroid/webkit/MimeTypeMap;->getSingleton()Landroid/webkit/MimeTypeMap;

    move-result-object v0

    .line 176
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/webkit/MimeTypeMap;->getFileExtensionFromUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/webkit/MimeTypeMap;->getMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public length()J
    .locals 2

    .line 50
    invoke-direct {p0}, Lcom/squareup/server/ContentProviderImageResolver;->info()Lcom/squareup/server/ContentProviderImageResolver$Info;

    move-result-object v0

    iget-wide v0, v0, Lcom/squareup/server/ContentProviderImageResolver$Info;->length:J

    return-wide v0
.end method

.method public mimeType()Ljava/lang/String;
    .locals 1

    .line 46
    invoke-direct {p0}, Lcom/squareup/server/ContentProviderImageResolver;->info()Lcom/squareup/server/ContentProviderImageResolver$Info;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/server/ContentProviderImageResolver$Info;->mimeType:Ljava/lang/String;

    return-object v0
.end method

.method public toByteString()Lokio/ByteString;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 55
    iget-object v0, p0, Lcom/squareup/server/ContentProviderImageResolver;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/server/ContentProviderImageResolver;->uri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 61
    :cond_0
    :try_start_0
    invoke-static {v0}, Lokio/Okio;->source(Ljava/io/InputStream;)Lokio/Source;

    move-result-object v1

    invoke-static {v1}, Lokio/Okio;->buffer(Lokio/Source;)Lokio/BufferedSource;

    move-result-object v1

    invoke-interface {v1}, Lokio/BufferedSource;->readByteString()Lokio/ByteString;

    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 63
    invoke-static {v0}, Lcom/squareup/util/Streams;->closeQuietly(Ljava/io/Closeable;)V

    return-object v1

    :catchall_0
    move-exception v1

    invoke-static {v0}, Lcom/squareup/util/Streams;->closeQuietly(Ljava/io/Closeable;)V

    .line 64
    throw v1
.end method
