.class public interface abstract Lcom/squareup/server/ImageService;
.super Ljava/lang/Object;
.source "ImageService.java"


# virtual methods
.method public abstract upload(Ljava/lang/String;Lokhttp3/MultipartBody$Part;)Lcom/squareup/server/SimpleStandardResponse;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Path;
            value = "id"
        .end annotation
    .end param
    .param p2    # Lokhttp3/MultipartBody$Part;
        .annotation runtime Lretrofit2/http/Part;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lokhttp3/MultipartBody$Part;",
            ")",
            "Lcom/squareup/server/SimpleStandardResponse<",
            "Lcom/squareup/server/ItemImageUploadResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/Multipart;
    .end annotation

    .annotation runtime Lretrofit2/http/PUT;
        value = "/1.0/images/item/{id}.jpg"
    .end annotation
.end method
