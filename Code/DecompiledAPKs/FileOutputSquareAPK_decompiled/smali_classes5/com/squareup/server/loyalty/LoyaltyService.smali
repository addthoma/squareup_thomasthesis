.class public interface abstract Lcom/squareup/server/loyalty/LoyaltyService;
.super Ljava/lang/Object;
.source "LoyaltyService.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/loyalty/LoyaltyService$LoyaltyStandardResponse;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00c4\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008f\u0018\u00002\u00020\u0001:\u00019J\u0018\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0006H\'J\u0018\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u00082\u0008\u0008\u0001\u0010\u0005\u001a\u00020\nH\'J\u0018\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000c2\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u000eH\'J\u0018\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u00082\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0011H\'J\u0018\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u00130\u000c2\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0014H\'J\u0018\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u00160\u00082\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0017H\'J\u0018\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u00020\u00190\u000c2\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u001aH\'J\u0018\u0010\u001b\u001a\u0008\u0012\u0004\u0012\u00020\u001c0\u00082\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u001dH\'J\u0018\u0010\u001e\u001a\u0008\u0012\u0004\u0012\u00020\u001f0\u00082\u0008\u0008\u0001\u0010\u0005\u001a\u00020 H\'J\u0018\u0010!\u001a\u0008\u0012\u0004\u0012\u00020\"0\u00082\u0008\u0008\u0001\u0010\u0005\u001a\u00020#H\'J\u0018\u0010$\u001a\u0008\u0012\u0004\u0012\u00020%0\u00082\u0008\u0008\u0001\u0010\u0005\u001a\u00020&H\'J\u0018\u0010\'\u001a\u0008\u0012\u0004\u0012\u00020(0\u00082\u0008\u0008\u0001\u0010\u0005\u001a\u00020)H\'J\u0018\u0010*\u001a\u0008\u0012\u0004\u0012\u00020+0\u00082\u0008\u0008\u0001\u0010\u0005\u001a\u00020,H\'J\u0018\u0010-\u001a\u0008\u0012\u0004\u0012\u00020.0\u00082\u0008\u0008\u0001\u0010\u0005\u001a\u00020/H\'J\u0018\u00100\u001a\u0008\u0012\u0004\u0012\u0002010\u00082\u0008\u0008\u0001\u0010\u0005\u001a\u000202H\'J\u0018\u00103\u001a\u0008\u0012\u0004\u0012\u0002040\u000c2\u0008\u0008\u0001\u0010\u0005\u001a\u000205H\'J\u0018\u00106\u001a\u0008\u0012\u0004\u0012\u0002070\u00082\u0008\u0008\u0001\u0010\u0005\u001a\u000208H\'\u00a8\u0006:"
    }
    d2 = {
        "Lcom/squareup/server/loyalty/LoyaltyService;",
        "",
        "accumulateLoyaltyStatus",
        "Lcom/squareup/server/AcceptedResponse;",
        "Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse;",
        "request",
        "Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;",
        "adjustPunches",
        "Lcom/squareup/server/StatusResponse;",
        "Lcom/squareup/protos/client/loyalty/AdjustPunchesResponse;",
        "Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;",
        "batchGetLoyaltyAccounts",
        "Lcom/squareup/server/loyalty/LoyaltyService$LoyaltyStandardResponse;",
        "Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;",
        "Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsRequest;",
        "createLoyaltyAccount",
        "Lcom/squareup/protos/client/loyalty/CreateLoyaltyAccountResponse;",
        "Lcom/squareup/protos/client/loyalty/CreateLoyaltyAccountRequest;",
        "deleteLoyaltyAccount",
        "Lcom/squareup/protos/client/loyalty/DeleteLoyaltyAccountResponse;",
        "Lcom/squareup/protos/client/loyalty/DeleteLoyaltyAccountRequest;",
        "getExpiringPoints",
        "Lcom/squareup/protos/client/loyalty/GetExpiringPointsStatusResponse;",
        "Lcom/squareup/protos/client/loyalty/GetExpiringPointsStatusRequest;",
        "getLoyaltyAccount",
        "Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountResponse;",
        "Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountRequest;",
        "getLoyaltyAccountFromMapping",
        "Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingResponse;",
        "Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest;",
        "getLoyaltyReport",
        "Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;",
        "Lcom/squareup/protos/client/loyalty/GetLoyaltyReportRequest;",
        "getLoyaltyStatusForContact",
        "Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;",
        "Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactRequest;",
        "recordMissedLoyaltyOpportunity",
        "Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityResponse;",
        "Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;",
        "redeemPoints",
        "Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;",
        "Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;",
        "returnReward",
        "Lcom/squareup/protos/client/loyalty/ReturnRewardResponse;",
        "Lcom/squareup/protos/client/loyalty/ReturnRewardRequest;",
        "sendLoyaltyStatus",
        "Lcom/squareup/protos/client/loyalty/SendBuyerLoyaltyStatusResponse;",
        "Lcom/squareup/protos/client/loyalty/SendBuyerLoyaltyStatusRequest;",
        "transferLoyaltyAccount",
        "Lcom/squareup/protos/client/loyalty/TransferLoyaltyAccountResponse;",
        "Lcom/squareup/protos/client/loyalty/TransferLoyaltyAccountRequest;",
        "updateLoyaltyAccountMapping",
        "Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;",
        "Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingRequest;",
        "voidCoupons",
        "Lcom/squareup/protos/client/loyalty/VoidCouponsResponse;",
        "Lcom/squareup/protos/client/loyalty/VoidCouponsRequest;",
        "LoyaltyStandardResponse",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract accumulateLoyaltyStatus(Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;)Lcom/squareup/server/AcceptedResponse;
    .param p1    # Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusRequest;",
            ")",
            "Lcom/squareup/server/AcceptedResponse<",
            "Lcom/squareup/protos/client/loyalty/AccumulateLoyaltyStatusResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/loyalty/accumulate-loyalty-status"
    .end annotation
.end method

.method public abstract adjustPunches(Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/loyalty/AdjustPunchesRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/loyalty/AdjustPunchesResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/loyalty/adjust-punches"
    .end annotation
.end method

.method public abstract batchGetLoyaltyAccounts(Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsRequest;)Lcom/squareup/server/loyalty/LoyaltyService$LoyaltyStandardResponse;
    .param p1    # Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsRequest;",
            ")",
            "Lcom/squareup/server/loyalty/LoyaltyService$LoyaltyStandardResponse<",
            "Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/loyalty/batch-get-loyalty-accounts"
    .end annotation
.end method

.method public abstract createLoyaltyAccount(Lcom/squareup/protos/client/loyalty/CreateLoyaltyAccountRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/loyalty/CreateLoyaltyAccountRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/loyalty/CreateLoyaltyAccountRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/loyalty/CreateLoyaltyAccountResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/loyalty/create-loyalty-account"
    .end annotation
.end method

.method public abstract deleteLoyaltyAccount(Lcom/squareup/protos/client/loyalty/DeleteLoyaltyAccountRequest;)Lcom/squareup/server/loyalty/LoyaltyService$LoyaltyStandardResponse;
    .param p1    # Lcom/squareup/protos/client/loyalty/DeleteLoyaltyAccountRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/loyalty/DeleteLoyaltyAccountRequest;",
            ")",
            "Lcom/squareup/server/loyalty/LoyaltyService$LoyaltyStandardResponse<",
            "Lcom/squareup/protos/client/loyalty/DeleteLoyaltyAccountResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/loyalty/delete-loyalty-account"
    .end annotation
.end method

.method public abstract getExpiringPoints(Lcom/squareup/protos/client/loyalty/GetExpiringPointsStatusRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/loyalty/GetExpiringPointsStatusRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/loyalty/GetExpiringPointsStatusRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/loyalty/GetExpiringPointsStatusResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/loyalty/get-expiring-points"
    .end annotation
.end method

.method public abstract getLoyaltyAccount(Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountRequest;)Lcom/squareup/server/loyalty/LoyaltyService$LoyaltyStandardResponse;
    .param p1    # Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountRequest;",
            ")",
            "Lcom/squareup/server/loyalty/LoyaltyService$LoyaltyStandardResponse<",
            "Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/loyalty/get-loyalty-account"
    .end annotation
.end method

.method public abstract getLoyaltyAccountFromMapping(Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountFromMappingResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/loyalty/get-loyalty-account-from-mapping"
    .end annotation
.end method

.method public abstract getLoyaltyReport(Lcom/squareup/protos/client/loyalty/GetLoyaltyReportRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/loyalty/GetLoyaltyReportRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/loyalty/GetLoyaltyReportRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/loyalty/GetLoyaltyReportResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "1.0/loyalty/get-loyalty-report"
    .end annotation
.end method

.method public abstract getLoyaltyStatusForContact(Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/loyalty/GetLoyaltyStatusForContactResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/loyalty/get-loyalty-status-for-contact"
    .end annotation
.end method

.method public abstract recordMissedLoyaltyOpportunity(Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/loyalty/RecordMissedLoyaltyOpportunityResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/loyalty/record-missed-loyalty-opportunity"
    .end annotation
.end method

.method public abstract redeemPoints(Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/loyalty/RedeemPointsRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/loyalty/RedeemPointsResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/loyalty/redeem-points"
    .end annotation
.end method

.method public abstract returnReward(Lcom/squareup/protos/client/loyalty/ReturnRewardRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/loyalty/ReturnRewardRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/loyalty/ReturnRewardRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/loyalty/ReturnRewardResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/loyalty/return-reward"
    .end annotation
.end method

.method public abstract sendLoyaltyStatus(Lcom/squareup/protos/client/loyalty/SendBuyerLoyaltyStatusRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/loyalty/SendBuyerLoyaltyStatusRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/loyalty/SendBuyerLoyaltyStatusRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/loyalty/SendBuyerLoyaltyStatusResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/loyalty/send-loyalty-status"
    .end annotation
.end method

.method public abstract transferLoyaltyAccount(Lcom/squareup/protos/client/loyalty/TransferLoyaltyAccountRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/loyalty/TransferLoyaltyAccountRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/loyalty/TransferLoyaltyAccountRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/loyalty/TransferLoyaltyAccountResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/loyalty/transfer-loyalty-account"
    .end annotation
.end method

.method public abstract updateLoyaltyAccountMapping(Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingRequest;)Lcom/squareup/server/loyalty/LoyaltyService$LoyaltyStandardResponse;
    .param p1    # Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingRequest;",
            ")",
            "Lcom/squareup/server/loyalty/LoyaltyService$LoyaltyStandardResponse<",
            "Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/loyalty/update-loyalty-account-mapping"
    .end annotation
.end method

.method public abstract voidCoupons(Lcom/squareup/protos/client/loyalty/VoidCouponsRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/loyalty/VoidCouponsRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/loyalty/VoidCouponsRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/loyalty/VoidCouponsResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/loyalty/void-coupons"
    .end annotation
.end method
