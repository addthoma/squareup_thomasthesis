.class public final Lcom/squareup/server/loyalty/LoyaltyService$LoyaltyStandardResponse;
.super Lcom/squareup/server/StandardResponse;
.source "LoyaltyService.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/loyalty/LoyaltyService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LoyaltyStandardResponse"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/squareup/server/StandardResponse<",
        "TT;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0003\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u00022\u0008\u0012\u0004\u0012\u0002H\u00010\u0003B\u0013\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0005\u00a2\u0006\u0002\u0010\u0006J\u0015\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00028\u0000H\u0014\u00a2\u0006\u0002\u0010\n\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/server/loyalty/LoyaltyService$LoyaltyStandardResponse;",
        "T",
        "",
        "Lcom/squareup/server/StandardResponse;",
        "factory",
        "Lcom/squareup/server/StandardResponse$Factory;",
        "(Lcom/squareup/server/StandardResponse$Factory;)V",
        "isSuccessful",
        "",
        "response",
        "(Ljava/lang/Object;)Z",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/server/StandardResponse$Factory;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/StandardResponse$Factory<",
            "TT;>;)V"
        }
    .end annotation

    const-string v0, "factory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 130
    invoke-direct {p0, p1}, Lcom/squareup/server/StandardResponse;-><init>(Lcom/squareup/server/StandardResponse$Factory;)V

    return-void
.end method


# virtual methods
.method protected isSuccessful(Ljava/lang/Object;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    const-string v0, "response"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 133
    instance-of v0, p1, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;

    iget-object p1, p1, Lcom/squareup/protos/client/loyalty/BatchGetLoyaltyAccountsResponse;->status:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    goto :goto_0

    .line 134
    :cond_0
    instance-of v0, p1, Lcom/squareup/protos/client/loyalty/DeleteLoyaltyAccountResponse;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/squareup/protos/client/loyalty/DeleteLoyaltyAccountResponse;

    iget-object p1, p1, Lcom/squareup/protos/client/loyalty/DeleteLoyaltyAccountResponse;->status:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    goto :goto_0

    .line 135
    :cond_1
    instance-of v0, p1, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountResponse;

    if-eqz v0, :cond_2

    check-cast p1, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountResponse;

    iget-object p1, p1, Lcom/squareup/protos/client/loyalty/GetLoyaltyAccountResponse;->status:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    goto :goto_0

    .line 136
    :cond_2
    instance-of v0, p1, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;

    if-eqz v0, :cond_4

    check-cast p1, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;

    iget-object p1, p1, Lcom/squareup/protos/client/loyalty/UpdateLoyaltyAccountMappingResponse;->status:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    .line 132
    :goto_0
    sget-object v0, Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;->STATUS_SUCCESS:Lcom/squareup/protos/client/loyalty/LoyaltyRequestStatus;

    if-ne p1, v0, :cond_3

    const/4 p1, 0x1

    goto :goto_1

    :cond_3
    const/4 p1, 0x0

    :goto_1
    return p1

    .line 137
    :cond_4
    new-instance v0, Lkotlin/NotImplementedError;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Don\'t know how to handle "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Lkotlin/NotImplementedError;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method
