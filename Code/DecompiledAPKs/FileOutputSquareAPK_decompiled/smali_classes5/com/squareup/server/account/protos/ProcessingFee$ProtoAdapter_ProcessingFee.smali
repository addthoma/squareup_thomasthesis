.class final Lcom/squareup/server/account/protos/ProcessingFee$ProtoAdapter_ProcessingFee;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ProcessingFee.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/ProcessingFee;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ProcessingFee"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/server/account/protos/ProcessingFee;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 150
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/server/account/protos/ProcessingFee;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/server/account/protos/ProcessingFee;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 169
    new-instance v0, Lcom/squareup/server/account/protos/ProcessingFee$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/ProcessingFee$Builder;-><init>()V

    .line 170
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 171
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 176
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 174
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/ProcessingFee$Builder;->interchange_cents(Ljava/lang/Integer;)Lcom/squareup/server/account/protos/ProcessingFee$Builder;

    goto :goto_0

    .line 173
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/ProcessingFee$Builder;->discount_basis_points(Ljava/lang/Integer;)Lcom/squareup/server/account/protos/ProcessingFee$Builder;

    goto :goto_0

    .line 180
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/ProcessingFee$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 181
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/ProcessingFee$Builder;->build()Lcom/squareup/server/account/protos/ProcessingFee;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 148
    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/ProcessingFee$ProtoAdapter_ProcessingFee;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/server/account/protos/ProcessingFee;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/server/account/protos/ProcessingFee;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 162
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/ProcessingFee;->discount_basis_points:Ljava/lang/Integer;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 163
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/ProcessingFee;->interchange_cents:Ljava/lang/Integer;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 164
    invoke-virtual {p2}, Lcom/squareup/server/account/protos/ProcessingFee;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 148
    check-cast p2, Lcom/squareup/server/account/protos/ProcessingFee;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/server/account/protos/ProcessingFee$ProtoAdapter_ProcessingFee;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/server/account/protos/ProcessingFee;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/server/account/protos/ProcessingFee;)I
    .locals 4

    .line 155
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/ProcessingFee;->discount_basis_points:Ljava/lang/Integer;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/ProcessingFee;->interchange_cents:Ljava/lang/Integer;

    const/4 v3, 0x2

    .line 156
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 157
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/ProcessingFee;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 148
    check-cast p1, Lcom/squareup/server/account/protos/ProcessingFee;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/ProcessingFee$ProtoAdapter_ProcessingFee;->encodedSize(Lcom/squareup/server/account/protos/ProcessingFee;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/server/account/protos/ProcessingFee;)Lcom/squareup/server/account/protos/ProcessingFee;
    .locals 0

    .line 186
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/ProcessingFee;->newBuilder()Lcom/squareup/server/account/protos/ProcessingFee$Builder;

    move-result-object p1

    .line 187
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/ProcessingFee$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 188
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/ProcessingFee$Builder;->build()Lcom/squareup/server/account/protos/ProcessingFee;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 148
    check-cast p1, Lcom/squareup/server/account/protos/ProcessingFee;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/ProcessingFee$ProtoAdapter_ProcessingFee;->redact(Lcom/squareup/server/account/protos/ProcessingFee;)Lcom/squareup/server/account/protos/ProcessingFee;

    move-result-object p1

    return-object p1
.end method
