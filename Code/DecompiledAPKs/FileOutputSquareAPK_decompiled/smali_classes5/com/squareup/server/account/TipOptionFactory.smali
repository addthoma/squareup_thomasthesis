.class public Lcom/squareup/server/account/TipOptionFactory;
.super Ljava/lang/Object;
.source "TipOptionFactory.java"


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public static forMoney(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/tipping/TipOption;
    .locals 1

    .line 10
    new-instance v0, Lcom/squareup/protos/common/tipping/TipOption$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/common/tipping/TipOption$Builder;-><init>()V

    .line 11
    invoke-virtual {v0, p0}, Lcom/squareup/protos/common/tipping/TipOption$Builder;->tip_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/tipping/TipOption$Builder;

    move-result-object p0

    const/4 v0, 0x0

    .line 12
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/protos/common/tipping/TipOption$Builder;->is_remaining_balance(Ljava/lang/Boolean;)Lcom/squareup/protos/common/tipping/TipOption$Builder;

    move-result-object p0

    .line 13
    invoke-virtual {p0}, Lcom/squareup/protos/common/tipping/TipOption$Builder;->build()Lcom/squareup/protos/common/tipping/TipOption;

    move-result-object p0

    return-object p0
.end method

.method public static forPercentage(Lcom/squareup/util/Percentage;)Lcom/squareup/protos/common/tipping/TipOption;
    .locals 3

    .line 17
    new-instance v0, Lcom/squareup/protos/common/tipping/TipOption$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/common/tipping/TipOption$Builder;-><init>()V

    .line 18
    invoke-virtual {p0}, Lcom/squareup/util/Percentage;->doubleValue()D

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/protos/common/tipping/TipOption$Builder;->percentage(Ljava/lang/Double;)Lcom/squareup/protos/common/tipping/TipOption$Builder;

    move-result-object p0

    const/4 v0, 0x0

    .line 19
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/protos/common/tipping/TipOption$Builder;->is_remaining_balance(Ljava/lang/Boolean;)Lcom/squareup/protos/common/tipping/TipOption$Builder;

    move-result-object p0

    .line 20
    invoke-virtual {p0}, Lcom/squareup/protos/common/tipping/TipOption$Builder;->build()Lcom/squareup/protos/common/tipping/TipOption;

    move-result-object p0

    return-object p0
.end method

.method public static forPercentage(Lcom/squareup/util/Percentage;Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/tipping/TipOption;
    .locals 2

    .line 25
    new-instance v0, Lcom/squareup/protos/common/tipping/TipOption$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/common/tipping/TipOption$Builder;-><init>()V

    .line 26
    invoke-virtual {v0, p1}, Lcom/squareup/protos/common/tipping/TipOption$Builder;->tip_money(Lcom/squareup/protos/common/Money;)Lcom/squareup/protos/common/tipping/TipOption$Builder;

    move-result-object p1

    .line 27
    invoke-virtual {p0}, Lcom/squareup/util/Percentage;->doubleValue()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p0

    invoke-virtual {p1, p0}, Lcom/squareup/protos/common/tipping/TipOption$Builder;->percentage(Ljava/lang/Double;)Lcom/squareup/protos/common/tipping/TipOption$Builder;

    move-result-object p0

    const/4 p1, 0x0

    .line 28
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/protos/common/tipping/TipOption$Builder;->is_remaining_balance(Ljava/lang/Boolean;)Lcom/squareup/protos/common/tipping/TipOption$Builder;

    move-result-object p0

    .line 29
    invoke-virtual {p0}, Lcom/squareup/protos/common/tipping/TipOption$Builder;->build()Lcom/squareup/protos/common/tipping/TipOption;

    move-result-object p0

    return-object p0
.end method
