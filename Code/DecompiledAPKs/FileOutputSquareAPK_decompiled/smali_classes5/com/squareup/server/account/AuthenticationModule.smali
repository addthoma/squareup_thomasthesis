.class public abstract Lcom/squareup/server/account/AuthenticationModule;
.super Ljava/lang/Object;
.source "AuthenticationModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideAuthenticationService(Lcom/squareup/api/ServiceCreator;)Lcom/squareup/server/account/AuthenticationService;
    .locals 1
    .param p0    # Lcom/squareup/api/ServiceCreator;
        .annotation runtime Lcom/squareup/api/RetrofitUnauthenticated;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 15
    const-class v0, Lcom/squareup/server/account/AuthenticationService;

    invoke-interface {p0, v0}, Lcom/squareup/api/ServiceCreator;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/account/AuthenticationService;

    return-object p0
.end method

.method static providePasswordService(Lcom/squareup/api/ServiceCreator;)Lcom/squareup/server/account/PasswordService;
    .locals 1
    .param p0    # Lcom/squareup/api/ServiceCreator;
        .annotation runtime Lcom/squareup/api/RetrofitUnauthenticated;
        .end annotation
    .end param
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 20
    const-class v0, Lcom/squareup/server/account/PasswordService;

    invoke-interface {p0, v0}, Lcom/squareup/api/ServiceCreator;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/account/PasswordService;

    return-object p0
.end method
