.class final Lcom/squareup/server/account/protos/User$MerchantProfile$ProtoAdapter_MerchantProfile;
.super Lcom/squareup/wire/ProtoAdapter;
.source "User.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/User$MerchantProfile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_MerchantProfile"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/server/account/protos/User$MerchantProfile;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1184
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/server/account/protos/User$MerchantProfile;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/server/account/protos/User$MerchantProfile;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1205
    new-instance v0, Lcom/squareup/server/account/protos/User$MerchantProfile$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/User$MerchantProfile$Builder;-><init>()V

    .line 1206
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 1207
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 1213
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 1211
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/User$MerchantProfile$Builder;->custom_receipt_text(Ljava/lang/String;)Lcom/squareup/server/account/protos/User$MerchantProfile$Builder;

    goto :goto_0

    .line 1210
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/User$MerchantProfile$Builder;->printed_receipt_image_url(Ljava/lang/String;)Lcom/squareup/server/account/protos/User$MerchantProfile$Builder;

    goto :goto_0

    .line 1209
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/User$MerchantProfile$Builder;->curated_image_url(Ljava/lang/String;)Lcom/squareup/server/account/protos/User$MerchantProfile$Builder;

    goto :goto_0

    .line 1217
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/User$MerchantProfile$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 1218
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/User$MerchantProfile$Builder;->build()Lcom/squareup/server/account/protos/User$MerchantProfile;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1182
    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/User$MerchantProfile$ProtoAdapter_MerchantProfile;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/server/account/protos/User$MerchantProfile;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/server/account/protos/User$MerchantProfile;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1197
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/User$MerchantProfile;->curated_image_url:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1198
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/User$MerchantProfile;->printed_receipt_image_url:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1199
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/User$MerchantProfile;->custom_receipt_text:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1200
    invoke-virtual {p2}, Lcom/squareup/server/account/protos/User$MerchantProfile;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1182
    check-cast p2, Lcom/squareup/server/account/protos/User$MerchantProfile;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/server/account/protos/User$MerchantProfile$ProtoAdapter_MerchantProfile;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/server/account/protos/User$MerchantProfile;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/server/account/protos/User$MerchantProfile;)I
    .locals 4

    .line 1189
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/User$MerchantProfile;->curated_image_url:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/User$MerchantProfile;->printed_receipt_image_url:Ljava/lang/String;

    const/4 v3, 0x2

    .line 1190
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/User$MerchantProfile;->custom_receipt_text:Ljava/lang/String;

    const/4 v3, 0x3

    .line 1191
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1192
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/User$MerchantProfile;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 1182
    check-cast p1, Lcom/squareup/server/account/protos/User$MerchantProfile;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/User$MerchantProfile$ProtoAdapter_MerchantProfile;->encodedSize(Lcom/squareup/server/account/protos/User$MerchantProfile;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/server/account/protos/User$MerchantProfile;)Lcom/squareup/server/account/protos/User$MerchantProfile;
    .locals 0

    .line 1223
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/User$MerchantProfile;->newBuilder()Lcom/squareup/server/account/protos/User$MerchantProfile$Builder;

    move-result-object p1

    .line 1224
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/User$MerchantProfile$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 1225
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/User$MerchantProfile$Builder;->build()Lcom/squareup/server/account/protos/User$MerchantProfile;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1182
    check-cast p1, Lcom/squareup/server/account/protos/User$MerchantProfile;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/User$MerchantProfile$ProtoAdapter_MerchantProfile;->redact(Lcom/squareup/server/account/protos/User$MerchantProfile;)Lcom/squareup/server/account/protos/User$MerchantProfile;

    move-result-object p1

    return-object p1
.end method
