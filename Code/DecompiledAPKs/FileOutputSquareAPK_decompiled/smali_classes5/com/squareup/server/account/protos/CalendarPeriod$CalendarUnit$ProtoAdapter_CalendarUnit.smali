.class final Lcom/squareup/server/account/protos/CalendarPeriod$CalendarUnit$ProtoAdapter_CalendarUnit;
.super Lcom/squareup/wire/EnumAdapter;
.source "CalendarPeriod.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/CalendarPeriod$CalendarUnit;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_CalendarUnit"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/EnumAdapter<",
        "Lcom/squareup/server/account/protos/CalendarPeriod$CalendarUnit;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 1

    .line 139
    const-class v0, Lcom/squareup/server/account/protos/CalendarPeriod$CalendarUnit;

    invoke-direct {p0, v0}, Lcom/squareup/wire/EnumAdapter;-><init>(Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method protected fromValue(I)Lcom/squareup/server/account/protos/CalendarPeriod$CalendarUnit;
    .locals 0

    .line 144
    invoke-static {p1}, Lcom/squareup/server/account/protos/CalendarPeriod$CalendarUnit;->fromValue(I)Lcom/squareup/server/account/protos/CalendarPeriod$CalendarUnit;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic fromValue(I)Lcom/squareup/wire/WireEnum;
    .locals 0

    .line 137
    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/CalendarPeriod$CalendarUnit$ProtoAdapter_CalendarUnit;->fromValue(I)Lcom/squareup/server/account/protos/CalendarPeriod$CalendarUnit;

    move-result-object p1

    return-object p1
.end method
