.class public Lcom/squareup/server/account/UserUtils;
.super Ljava/lang/Object;
.source "UserUtils.java"


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public static getCountryCodeOrNull(Lcom/squareup/server/account/protos/User;)Lcom/squareup/CountryCode;
    .locals 4

    const/4 v0, 0x0

    if-nez p0, :cond_0

    const/4 p0, 0x0

    new-array p0, p0, [Ljava/lang/Object;

    const-string v1, "User was null (expected during UI test app reset)"

    .line 17
    invoke-static {v1, p0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-object v0

    .line 21
    :cond_0
    iget-object p0, p0, Lcom/squareup/server/account/protos/User;->locale:Lcom/squareup/server/account/protos/User$SquareLocale;

    if-nez p0, :cond_1

    .line 23
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string v1, "Unexpected: user\'s locale was null"

    invoke-direct {p0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;)V

    return-object v0

    .line 27
    :cond_1
    iget-object v1, p0, Lcom/squareup/server/account/protos/User$SquareLocale;->country_code:Ljava/lang/String;

    if-nez v1, :cond_2

    .line 28
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string v1, "Unexpected: user\'s country code was null"

    invoke-direct {p0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;)V

    return-object v0

    .line 33
    :cond_2
    :try_start_0
    iget-object v1, p0, Lcom/squareup/server/account/protos/User$SquareLocale;->country_code:Ljava/lang/String;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/CountryCode;->valueOf(Ljava/lang/String;)Lcom/squareup/CountryCode;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    .line 36
    :catch_0
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected: unrecognized user country_code: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p0, p0, Lcom/squareup/server/account/protos/User$SquareLocale;->country_code:Ljava/lang/String;

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v1, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;)V

    return-object v0
.end method

.method public static getCurrency(Lcom/squareup/server/account/protos/User;)Lcom/squareup/protos/common/CurrencyCode;
    .locals 1

    if-eqz p0, :cond_2

    .line 47
    iget-object v0, p0, Lcom/squareup/server/account/protos/User;->locale:Lcom/squareup/server/account/protos/User$SquareLocale;

    if-nez v0, :cond_0

    goto :goto_1

    .line 50
    :cond_0
    iget-object p0, p0, Lcom/squareup/server/account/protos/User;->locale:Lcom/squareup/server/account/protos/User$SquareLocale;

    .line 51
    iget-object v0, p0, Lcom/squareup/server/account/protos/User$SquareLocale;->currency_codes:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object p0, Lcom/squareup/protos/common/CurrencyCode;->XXX:Lcom/squareup/protos/common/CurrencyCode;

    goto :goto_0

    :cond_1
    iget-object p0, p0, Lcom/squareup/server/account/protos/User$SquareLocale;->currency_codes:Ljava/util/List;

    const/4 v0, 0x0

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/protos/common/CurrencyCode;

    :goto_0
    return-object p0

    .line 48
    :cond_2
    :goto_1
    sget-object p0, Lcom/squareup/protos/common/CurrencyCode;->XXX:Lcom/squareup/protos/common/CurrencyCode;

    return-object p0
.end method
