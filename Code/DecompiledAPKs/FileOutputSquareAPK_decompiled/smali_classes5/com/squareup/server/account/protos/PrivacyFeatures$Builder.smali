.class public final Lcom/squareup/server/account/protos/PrivacyFeatures$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "PrivacyFeatures.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/PrivacyFeatures;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/PrivacyFeatures;",
        "Lcom/squareup/server/account/protos/PrivacyFeatures$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public opted_out_of_tracking:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 109
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/server/account/protos/PrivacyFeatures;
    .locals 3

    .line 119
    new-instance v0, Lcom/squareup/server/account/protos/PrivacyFeatures;

    iget-object v1, p0, Lcom/squareup/server/account/protos/PrivacyFeatures$Builder;->opted_out_of_tracking:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/server/account/protos/PrivacyFeatures;-><init>(Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 106
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/PrivacyFeatures$Builder;->build()Lcom/squareup/server/account/protos/PrivacyFeatures;

    move-result-object v0

    return-object v0
.end method

.method public opted_out_of_tracking(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PrivacyFeatures$Builder;
    .locals 0

    .line 113
    iput-object p1, p0, Lcom/squareup/server/account/protos/PrivacyFeatures$Builder;->opted_out_of_tracking:Ljava/lang/Boolean;

    return-object p0
.end method
