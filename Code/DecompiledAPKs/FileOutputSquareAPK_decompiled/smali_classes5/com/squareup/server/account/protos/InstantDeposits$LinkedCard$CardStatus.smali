.class public final enum Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;
.super Ljava/lang/Enum;
.source "InstantDeposits.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CardStatus"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus$ProtoAdapter_CardStatus;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum FAILED:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;

.field public static final enum UNKNOWN:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;

.field public static final enum VERIFICATION_EXPIRED:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;

.field public static final enum VERIFICATION_PENDING:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;

.field public static final enum VERIFIED:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 512
    new-instance v0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const-string v3, "UNKNOWN"

    invoke-direct {v0, v3, v1, v2}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;->UNKNOWN:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;

    .line 514
    new-instance v0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;

    const/4 v3, 0x2

    const-string v4, "VERIFIED"

    invoke-direct {v0, v4, v2, v3}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;->VERIFIED:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;

    .line 516
    new-instance v0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;

    const/4 v4, 0x3

    const-string v5, "FAILED"

    invoke-direct {v0, v5, v3, v4}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;->FAILED:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;

    .line 518
    new-instance v0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;

    const/4 v5, 0x4

    const-string v6, "VERIFICATION_PENDING"

    invoke-direct {v0, v6, v4, v5}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;->VERIFICATION_PENDING:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;

    .line 520
    new-instance v0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;

    const/4 v6, 0x5

    const-string v7, "VERIFICATION_EXPIRED"

    invoke-direct {v0, v7, v5, v6}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;->VERIFICATION_EXPIRED:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;

    new-array v0, v6, [Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;

    .line 511
    sget-object v6, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;->UNKNOWN:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;

    aput-object v6, v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;->VERIFIED:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;->FAILED:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;->VERIFICATION_PENDING:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;->VERIFICATION_EXPIRED:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;

    aput-object v1, v0, v5

    sput-object v0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;->$VALUES:[Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;

    .line 522
    new-instance v0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus$ProtoAdapter_CardStatus;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus$ProtoAdapter_CardStatus;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 526
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 527
    iput p3, p0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;
    .locals 1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_4

    const/4 v0, 0x2

    if-eq p0, v0, :cond_3

    const/4 v0, 0x3

    if-eq p0, v0, :cond_2

    const/4 v0, 0x4

    if-eq p0, v0, :cond_1

    const/4 v0, 0x5

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 539
    :cond_0
    sget-object p0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;->VERIFICATION_EXPIRED:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;

    return-object p0

    .line 538
    :cond_1
    sget-object p0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;->VERIFICATION_PENDING:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;

    return-object p0

    .line 537
    :cond_2
    sget-object p0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;->FAILED:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;

    return-object p0

    .line 536
    :cond_3
    sget-object p0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;->VERIFIED:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;

    return-object p0

    .line 535
    :cond_4
    sget-object p0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;->UNKNOWN:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;
    .locals 1

    .line 511
    const-class v0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;

    return-object p0
.end method

.method public static values()[Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;
    .locals 1

    .line 511
    sget-object v0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;->$VALUES:[Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;

    invoke-virtual {v0}, [Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 546
    iget v0, p0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;->value:I

    return v0
.end method
