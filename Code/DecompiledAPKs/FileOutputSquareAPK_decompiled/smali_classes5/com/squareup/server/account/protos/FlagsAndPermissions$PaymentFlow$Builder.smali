.class public final Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FlagsAndPermissions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public allow_cnp_signatures:Ljava/lang/Boolean;

.field public allow_cof_signatures:Ljava/lang/Boolean;

.field public always_show_itemized_cart:Ljava/lang/Boolean;

.field public android_use_payment_config:Ljava/lang/Boolean;

.field public buyer_checkout:Ljava/lang/Boolean;

.field public can_always_skip_signatures:Ljava/lang/Boolean;

.field public can_split_emoney:Ljava/lang/Boolean;

.field public can_use_buyer_language_selection:Ljava/lang/Boolean;

.field public can_use_cash_qr_codes:Ljava/lang/Boolean;

.field public can_use_cash_workflow_android:Ljava/lang/Boolean;

.field public can_use_cto_workflow_android:Ljava/lang/Boolean;

.field public can_use_installments:Ljava/lang/Boolean;

.field public can_use_jp_formal_printed_receipts:Ljava/lang/Boolean;

.field public can_use_payment_method_v2:Ljava/lang/Boolean;

.field public can_use_select_method_workflow_android:Ljava/lang/Boolean;

.field public can_use_seller_cash_received_workflow_android:Ljava/lang/Boolean;

.field public can_use_separate_local_payments_queue_android:Ljava/lang/Boolean;

.field public can_use_split_tender_workflow_android:Ljava/lang/Boolean;

.field public can_use_tender_in_edit_refactor:Ljava/lang/Boolean;

.field public do_not_user_task_queue_for_payments_android:Ljava/lang/Boolean;

.field public emoney:Ljava/lang/Boolean;

.field public even_splits_android_v2:Ljava/lang/Boolean;

.field public orders_integration_process_cnp_via_orders:Ljava/lang/Boolean;

.field public portrait_signature:Ljava/lang/Boolean;

.field public prefer_sqlite_tasks_queue:Ljava/lang/Boolean;

.field public print_tax_percentage:Ljava/lang/Boolean;

.field public show_accidental_cash_other_modal:Ljava/lang/Boolean;

.field public show_noho_receipt_android:Ljava/lang/Boolean;

.field public use_auth_workflow_android:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 10748
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public allow_cnp_signatures(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;
    .locals 0

    .line 10752
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->allow_cnp_signatures:Ljava/lang/Boolean;

    return-object p0
.end method

.method public allow_cof_signatures(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;
    .locals 0

    .line 10757
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->allow_cof_signatures:Ljava/lang/Boolean;

    return-object p0
.end method

.method public always_show_itemized_cart(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;
    .locals 0

    .line 10870
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->always_show_itemized_cart:Ljava/lang/Boolean;

    return-object p0
.end method

.method public android_use_payment_config(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;
    .locals 0

    .line 10890
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->android_use_payment_config:Ljava/lang/Boolean;

    return-object p0
.end method

.method public build()Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;
    .locals 2

    .line 10908
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;-><init>(Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 10689
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow;

    move-result-object v0

    return-object v0
.end method

.method public buyer_checkout(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;
    .locals 0

    .line 10784
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->buyer_checkout:Ljava/lang/Boolean;

    return-object p0
.end method

.method public can_always_skip_signatures(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;
    .locals 0

    .line 10800
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->can_always_skip_signatures:Ljava/lang/Boolean;

    return-object p0
.end method

.method public can_split_emoney(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;
    .locals 0

    .line 10885
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->can_split_emoney:Ljava/lang/Boolean;

    return-object p0
.end method

.method public can_use_buyer_language_selection(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;
    .locals 0

    .line 10795
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->can_use_buyer_language_selection:Ljava/lang/Boolean;

    return-object p0
.end method

.method public can_use_cash_qr_codes(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;
    .locals 0

    .line 10880
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->can_use_cash_qr_codes:Ljava/lang/Boolean;

    return-object p0
.end method

.method public can_use_cash_workflow_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;
    .locals 0

    .line 10875
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->can_use_cash_workflow_android:Ljava/lang/Boolean;

    return-object p0
.end method

.method public can_use_cto_workflow_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;
    .locals 0

    .line 10853
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->can_use_cto_workflow_android:Ljava/lang/Boolean;

    return-object p0
.end method

.method public can_use_installments(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;
    .locals 0

    .line 10835
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->can_use_installments:Ljava/lang/Boolean;

    return-object p0
.end method

.method public can_use_jp_formal_printed_receipts(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;
    .locals 0

    .line 10769
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->can_use_jp_formal_printed_receipts:Ljava/lang/Boolean;

    return-object p0
.end method

.method public can_use_payment_method_v2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 10763
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->can_use_payment_method_v2:Ljava/lang/Boolean;

    return-object p0
.end method

.method public can_use_select_method_workflow_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;
    .locals 0

    .line 10902
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->can_use_select_method_workflow_android:Ljava/lang/Boolean;

    return-object p0
.end method

.method public can_use_seller_cash_received_workflow_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 10842
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->can_use_seller_cash_received_workflow_android:Ljava/lang/Boolean;

    return-object p0
.end method

.method public can_use_separate_local_payments_queue_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;
    .locals 0

    .line 10859
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->can_use_separate_local_payments_queue_android:Ljava/lang/Boolean;

    return-object p0
.end method

.method public can_use_split_tender_workflow_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;
    .locals 0

    .line 10848
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->can_use_split_tender_workflow_android:Ljava/lang/Boolean;

    return-object p0
.end method

.method public can_use_tender_in_edit_refactor(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;
    .locals 0

    .line 10805
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->can_use_tender_in_edit_refactor:Ljava/lang/Boolean;

    return-object p0
.end method

.method public do_not_user_task_queue_for_payments_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;
    .locals 0

    .line 10865
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->do_not_user_task_queue_for_payments_android:Ljava/lang/Boolean;

    return-object p0
.end method

.method public emoney(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;
    .locals 0

    .line 10815
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->emoney:Ljava/lang/Boolean;

    return-object p0
.end method

.method public even_splits_android_v2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 10790
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->even_splits_android_v2:Ljava/lang/Boolean;

    return-object p0
.end method

.method public orders_integration_process_cnp_via_orders(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;
    .locals 0

    .line 10896
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->orders_integration_process_cnp_via_orders:Ljava/lang/Boolean;

    return-object p0
.end method

.method public portrait_signature(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;
    .locals 0

    .line 10779
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->portrait_signature:Ljava/lang/Boolean;

    return-object p0
.end method

.method public prefer_sqlite_tasks_queue(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;
    .locals 0

    .line 10820
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->prefer_sqlite_tasks_queue:Ljava/lang/Boolean;

    return-object p0
.end method

.method public print_tax_percentage(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;
    .locals 0

    .line 10774
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->print_tax_percentage:Ljava/lang/Boolean;

    return-object p0
.end method

.method public show_accidental_cash_other_modal(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;
    .locals 0

    .line 10810
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->show_accidental_cash_other_modal:Ljava/lang/Boolean;

    return-object p0
.end method

.method public show_noho_receipt_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;
    .locals 0

    .line 10825
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->show_noho_receipt_android:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_auth_workflow_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;
    .locals 0

    .line 10830
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$PaymentFlow$Builder;->use_auth_workflow_android:Ljava/lang/Boolean;

    return-object p0
.end method
