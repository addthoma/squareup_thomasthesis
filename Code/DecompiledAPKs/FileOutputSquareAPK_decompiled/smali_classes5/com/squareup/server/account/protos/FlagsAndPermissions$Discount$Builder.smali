.class public final Lcom/squareup/server/account/protos/FlagsAndPermissions$Discount$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FlagsAndPermissions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions$Discount;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Discount;",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Discount$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public allow_stacking_coupons_of_same_discount:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 17502
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public allow_stacking_coupons_of_same_discount(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Discount$Builder;
    .locals 0

    .line 17510
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Discount$Builder;->allow_stacking_coupons_of_same_discount:Ljava/lang/Boolean;

    return-object p0
.end method

.method public build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Discount;
    .locals 3

    .line 17516
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Discount;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Discount$Builder;->allow_stacking_coupons_of_same_discount:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Discount;-><init>(Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 17499
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Discount$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Discount;

    move-result-object v0

    return-object v0
.end method
