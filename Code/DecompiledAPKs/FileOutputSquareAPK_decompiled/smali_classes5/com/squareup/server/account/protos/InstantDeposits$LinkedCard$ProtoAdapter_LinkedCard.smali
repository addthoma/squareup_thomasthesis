.class final Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$ProtoAdapter_LinkedCard;
.super Lcom/squareup/wire/ProtoAdapter;
.source "InstantDeposits.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_LinkedCard"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 563
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 582
    new-instance v0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$Builder;-><init>()V

    .line 583
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 584
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 596
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 589
    :cond_0
    :try_start_0
    sget-object v4, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;

    invoke-virtual {v0, v4}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$Builder;->card_status(Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;)Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 591
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 586
    :cond_1
    sget-object v3, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$Builder;->card_details(Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;)Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$Builder;

    goto :goto_0

    .line 600
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 601
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$Builder;->build()Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 561
    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$ProtoAdapter_LinkedCard;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 575
    sget-object v0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;->card_details:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 576
    sget-object v0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;->card_status:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 577
    invoke-virtual {p2}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 561
    check-cast p2, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$ProtoAdapter_LinkedCard;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;)I
    .locals 4

    .line 568
    sget-object v0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;->card_details:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;->card_status:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardStatus;

    const/4 v3, 0x2

    .line 569
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 570
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 561
    check-cast p1, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$ProtoAdapter_LinkedCard;->encodedSize(Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;)Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;
    .locals 2

    .line 606
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;->newBuilder()Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$Builder;

    move-result-object p1

    .line 607
    iget-object v0, p1, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$Builder;->card_details:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$Builder;->card_details:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;

    iput-object v0, p1, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$Builder;->card_details:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;

    .line 608
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 609
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$Builder;->build()Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 561
    check-cast p1, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$ProtoAdapter_LinkedCard;->redact(Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;)Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;

    move-result-object p1

    return-object p1
.end method
