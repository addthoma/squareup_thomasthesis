.class public final Lcom/squareup/server/account/protos/User$ReceiptAddress;
.super Lcom/squareup/wire/AndroidMessage;
.source "User.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/User;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ReceiptAddress"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/User$ReceiptAddress$ProtoAdapter_ReceiptAddress;,
        Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/User$ReceiptAddress;",
        "Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/User$ReceiptAddress;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/User$ReceiptAddress;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/User$ReceiptAddress;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/User$ReceiptAddress;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ADDRESS_LINE_3:Ljava/lang/String; = ""

.field public static final DEFAULT_CITY:Ljava/lang/String; = ""

.field public static final DEFAULT_COUNTRY_CODE:Ljava/lang/String; = ""

.field public static final DEFAULT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_POSTAL_CODE:Ljava/lang/String; = ""

.field public static final DEFAULT_STATE:Ljava/lang/String; = ""

.field public static final DEFAULT_STREET1:Ljava/lang/String; = ""

.field public static final DEFAULT_STREET2:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final address_line_3:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x3
    .end annotation
.end field

.field public final city:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x4
    .end annotation
.end field

.field public final country_code:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x7
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x8
    .end annotation
.end field

.field public final postal_code:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x6
    .end annotation
.end field

.field public final state:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x5
    .end annotation
.end field

.field public final street1:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x1
    .end annotation
.end field

.field public final street2:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 707
    new-instance v0, Lcom/squareup/server/account/protos/User$ReceiptAddress$ProtoAdapter_ReceiptAddress;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/User$ReceiptAddress$ProtoAdapter_ReceiptAddress;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/User$ReceiptAddress;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 709
    sget-object v0, Lcom/squareup/server/account/protos/User$ReceiptAddress;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/User$ReceiptAddress;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 10

    .line 796
    sget-object v9, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v0 .. v9}, Lcom/squareup/server/account/protos/User$ReceiptAddress;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 803
    sget-object v0, Lcom/squareup/server/account/protos/User$ReceiptAddress;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p9}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 804
    iput-object p1, p0, Lcom/squareup/server/account/protos/User$ReceiptAddress;->street1:Ljava/lang/String;

    .line 805
    iput-object p2, p0, Lcom/squareup/server/account/protos/User$ReceiptAddress;->street2:Ljava/lang/String;

    .line 806
    iput-object p3, p0, Lcom/squareup/server/account/protos/User$ReceiptAddress;->address_line_3:Ljava/lang/String;

    .line 807
    iput-object p4, p0, Lcom/squareup/server/account/protos/User$ReceiptAddress;->city:Ljava/lang/String;

    .line 808
    iput-object p5, p0, Lcom/squareup/server/account/protos/User$ReceiptAddress;->state:Ljava/lang/String;

    .line 809
    iput-object p6, p0, Lcom/squareup/server/account/protos/User$ReceiptAddress;->postal_code:Ljava/lang/String;

    .line 810
    iput-object p7, p0, Lcom/squareup/server/account/protos/User$ReceiptAddress;->country_code:Ljava/lang/String;

    .line 811
    iput-object p8, p0, Lcom/squareup/server/account/protos/User$ReceiptAddress;->name:Ljava/lang/String;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;)Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 898
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/User$ReceiptAddress;->newBuilder()Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 832
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/User$ReceiptAddress;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 833
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/User$ReceiptAddress;

    .line 834
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/User$ReceiptAddress;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/User$ReceiptAddress;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/User$ReceiptAddress;->street1:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/User$ReceiptAddress;->street1:Ljava/lang/String;

    .line 835
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/User$ReceiptAddress;->street2:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/User$ReceiptAddress;->street2:Ljava/lang/String;

    .line 836
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/User$ReceiptAddress;->address_line_3:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/User$ReceiptAddress;->address_line_3:Ljava/lang/String;

    .line 837
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/User$ReceiptAddress;->city:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/User$ReceiptAddress;->city:Ljava/lang/String;

    .line 838
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/User$ReceiptAddress;->state:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/User$ReceiptAddress;->state:Ljava/lang/String;

    .line 839
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/User$ReceiptAddress;->postal_code:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/User$ReceiptAddress;->postal_code:Ljava/lang/String;

    .line 840
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/User$ReceiptAddress;->country_code:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/User$ReceiptAddress;->country_code:Ljava/lang/String;

    .line 841
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/User$ReceiptAddress;->name:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/server/account/protos/User$ReceiptAddress;->name:Ljava/lang/String;

    .line 842
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 847
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_8

    .line 849
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/User$ReceiptAddress;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 850
    iget-object v1, p0, Lcom/squareup/server/account/protos/User$ReceiptAddress;->street1:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 851
    iget-object v1, p0, Lcom/squareup/server/account/protos/User$ReceiptAddress;->street2:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 852
    iget-object v1, p0, Lcom/squareup/server/account/protos/User$ReceiptAddress;->address_line_3:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 853
    iget-object v1, p0, Lcom/squareup/server/account/protos/User$ReceiptAddress;->city:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 854
    iget-object v1, p0, Lcom/squareup/server/account/protos/User$ReceiptAddress;->state:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 855
    iget-object v1, p0, Lcom/squareup/server/account/protos/User$ReceiptAddress;->postal_code:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 856
    iget-object v1, p0, Lcom/squareup/server/account/protos/User$ReceiptAddress;->country_code:Ljava/lang/String;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 857
    iget-object v1, p0, Lcom/squareup/server/account/protos/User$ReceiptAddress;->name:Ljava/lang/String;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_7
    add-int/2addr v0, v2

    .line 858
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_8
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;
    .locals 2

    .line 816
    new-instance v0, Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;-><init>()V

    .line 817
    iget-object v1, p0, Lcom/squareup/server/account/protos/User$ReceiptAddress;->street1:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;->street1:Ljava/lang/String;

    .line 818
    iget-object v1, p0, Lcom/squareup/server/account/protos/User$ReceiptAddress;->street2:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;->street2:Ljava/lang/String;

    .line 819
    iget-object v1, p0, Lcom/squareup/server/account/protos/User$ReceiptAddress;->address_line_3:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;->address_line_3:Ljava/lang/String;

    .line 820
    iget-object v1, p0, Lcom/squareup/server/account/protos/User$ReceiptAddress;->city:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;->city:Ljava/lang/String;

    .line 821
    iget-object v1, p0, Lcom/squareup/server/account/protos/User$ReceiptAddress;->state:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;->state:Ljava/lang/String;

    .line 822
    iget-object v1, p0, Lcom/squareup/server/account/protos/User$ReceiptAddress;->postal_code:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;->postal_code:Ljava/lang/String;

    .line 823
    iget-object v1, p0, Lcom/squareup/server/account/protos/User$ReceiptAddress;->country_code:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;->country_code:Ljava/lang/String;

    .line 824
    iget-object v1, p0, Lcom/squareup/server/account/protos/User$ReceiptAddress;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;->name:Ljava/lang/String;

    .line 825
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/User$ReceiptAddress;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 706
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/User$ReceiptAddress;->newBuilder()Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/User$ReceiptAddress;)Lcom/squareup/server/account/protos/User$ReceiptAddress;
    .locals 2

    .line 886
    iget-object v0, p1, Lcom/squareup/server/account/protos/User$ReceiptAddress;->street1:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/User$ReceiptAddress;->requireBuilder(Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;)Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/User$ReceiptAddress;->street1:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;->street1(Ljava/lang/String;)Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;

    move-result-object v1

    .line 887
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/User$ReceiptAddress;->street2:Ljava/lang/String;

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/User$ReceiptAddress;->requireBuilder(Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;)Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/User$ReceiptAddress;->street2:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;->street2(Ljava/lang/String;)Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;

    move-result-object v1

    .line 888
    :cond_1
    iget-object v0, p1, Lcom/squareup/server/account/protos/User$ReceiptAddress;->address_line_3:Ljava/lang/String;

    if-eqz v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/User$ReceiptAddress;->requireBuilder(Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;)Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/User$ReceiptAddress;->address_line_3:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;->address_line_3(Ljava/lang/String;)Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;

    move-result-object v1

    .line 889
    :cond_2
    iget-object v0, p1, Lcom/squareup/server/account/protos/User$ReceiptAddress;->city:Ljava/lang/String;

    if-eqz v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/User$ReceiptAddress;->requireBuilder(Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;)Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/User$ReceiptAddress;->city:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;->city(Ljava/lang/String;)Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;

    move-result-object v1

    .line 890
    :cond_3
    iget-object v0, p1, Lcom/squareup/server/account/protos/User$ReceiptAddress;->state:Ljava/lang/String;

    if-eqz v0, :cond_4

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/User$ReceiptAddress;->requireBuilder(Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;)Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/User$ReceiptAddress;->state:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;->state(Ljava/lang/String;)Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;

    move-result-object v1

    .line 891
    :cond_4
    iget-object v0, p1, Lcom/squareup/server/account/protos/User$ReceiptAddress;->postal_code:Ljava/lang/String;

    if-eqz v0, :cond_5

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/User$ReceiptAddress;->requireBuilder(Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;)Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/User$ReceiptAddress;->postal_code:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;->postal_code(Ljava/lang/String;)Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;

    move-result-object v1

    .line 892
    :cond_5
    iget-object v0, p1, Lcom/squareup/server/account/protos/User$ReceiptAddress;->country_code:Ljava/lang/String;

    if-eqz v0, :cond_6

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/User$ReceiptAddress;->requireBuilder(Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;)Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/User$ReceiptAddress;->country_code:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;->country_code(Ljava/lang/String;)Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;

    move-result-object v1

    .line 893
    :cond_6
    iget-object v0, p1, Lcom/squareup/server/account/protos/User$ReceiptAddress;->name:Ljava/lang/String;

    if-eqz v0, :cond_7

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/User$ReceiptAddress;->requireBuilder(Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;)Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/User$ReceiptAddress;->name:Ljava/lang/String;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;->name(Ljava/lang/String;)Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;

    move-result-object v1

    :cond_7
    if-nez v1, :cond_8

    move-object p1, p0

    goto :goto_0

    .line 894
    :cond_8
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/User$ReceiptAddress$Builder;->build()Lcom/squareup/server/account/protos/User$ReceiptAddress;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 706
    check-cast p1, Lcom/squareup/server/account/protos/User$ReceiptAddress;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/User$ReceiptAddress;->overlay(Lcom/squareup/server/account/protos/User$ReceiptAddress;)Lcom/squareup/server/account/protos/User$ReceiptAddress;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/User$ReceiptAddress;
    .locals 0

    return-object p0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 706
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/User$ReceiptAddress;->populateDefaults()Lcom/squareup/server/account/protos/User$ReceiptAddress;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 865
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 866
    iget-object v1, p0, Lcom/squareup/server/account/protos/User$ReceiptAddress;->street1:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", street1=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 867
    :cond_0
    iget-object v1, p0, Lcom/squareup/server/account/protos/User$ReceiptAddress;->street2:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", street2=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 868
    :cond_1
    iget-object v1, p0, Lcom/squareup/server/account/protos/User$ReceiptAddress;->address_line_3:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", address_line_3=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 869
    :cond_2
    iget-object v1, p0, Lcom/squareup/server/account/protos/User$ReceiptAddress;->city:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", city=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 870
    :cond_3
    iget-object v1, p0, Lcom/squareup/server/account/protos/User$ReceiptAddress;->state:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", state=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 871
    :cond_4
    iget-object v1, p0, Lcom/squareup/server/account/protos/User$ReceiptAddress;->postal_code:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", postal_code=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 872
    :cond_5
    iget-object v1, p0, Lcom/squareup/server/account/protos/User$ReceiptAddress;->country_code:Ljava/lang/String;

    if-eqz v1, :cond_6

    const-string v1, ", country_code=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 873
    :cond_6
    iget-object v1, p0, Lcom/squareup/server/account/protos/User$ReceiptAddress;->name:Ljava/lang/String;

    if-eqz v1, :cond_7

    const-string v1, ", name=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_7
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ReceiptAddress{"

    .line 874
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
