.class final Lcom/squareup/server/account/protos/Notification$ProtoAdapter_Notification;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Notification.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/Notification;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Notification"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/server/account/protos/Notification;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 410
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/server/account/protos/Notification;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/server/account/protos/Notification;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 437
    new-instance v0, Lcom/squareup/server/account/protos/Notification$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/Notification$Builder;-><init>()V

    .line 438
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 439
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 448
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 446
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/Notification$Builder;->lock_out(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Notification$Builder;

    goto :goto_0

    .line 445
    :pswitch_1
    iget-object v3, v0, Lcom/squareup/server/account/protos/Notification$Builder;->buttons:Ljava/util/List;

    sget-object v4, Lcom/squareup/server/account/protos/Notification$Button;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 444
    :pswitch_2
    sget-object v3, Lcom/squareup/server/account/protos/Notification$Button;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/server/account/protos/Notification$Button;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/Notification$Builder;->button(Lcom/squareup/server/account/protos/Notification$Button;)Lcom/squareup/server/account/protos/Notification$Builder;

    goto :goto_0

    .line 443
    :pswitch_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/Notification$Builder;->message(Ljava/lang/String;)Lcom/squareup/server/account/protos/Notification$Builder;

    goto :goto_0

    .line 442
    :pswitch_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/Notification$Builder;->title(Ljava/lang/String;)Lcom/squareup/server/account/protos/Notification$Builder;

    goto :goto_0

    .line 441
    :pswitch_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/Notification$Builder;->delivery_id(Ljava/lang/String;)Lcom/squareup/server/account/protos/Notification$Builder;

    goto :goto_0

    .line 452
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/Notification$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 453
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/Notification$Builder;->build()Lcom/squareup/server/account/protos/Notification;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 408
    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/Notification$ProtoAdapter_Notification;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/server/account/protos/Notification;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/server/account/protos/Notification;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 426
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/Notification;->delivery_id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 427
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/Notification;->title:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 428
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/Notification;->message:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 429
    sget-object v0, Lcom/squareup/server/account/protos/Notification$Button;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/Notification;->button:Lcom/squareup/server/account/protos/Notification$Button;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 430
    sget-object v0, Lcom/squareup/server/account/protos/Notification$Button;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/server/account/protos/Notification;->buttons:Ljava/util/List;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 431
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/Notification;->lock_out:Ljava/lang/Boolean;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 432
    invoke-virtual {p2}, Lcom/squareup/server/account/protos/Notification;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 408
    check-cast p2, Lcom/squareup/server/account/protos/Notification;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/server/account/protos/Notification$ProtoAdapter_Notification;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/server/account/protos/Notification;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/server/account/protos/Notification;)I
    .locals 4

    .line 415
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/Notification;->delivery_id:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/Notification;->title:Ljava/lang/String;

    const/4 v3, 0x2

    .line 416
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/Notification;->message:Ljava/lang/String;

    const/4 v3, 0x3

    .line 417
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/Notification$Button;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/Notification;->button:Lcom/squareup/server/account/protos/Notification$Button;

    const/4 v3, 0x4

    .line 418
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/Notification$Button;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 419
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/server/account/protos/Notification;->buttons:Ljava/util/List;

    const/4 v3, 0x5

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/Notification;->lock_out:Ljava/lang/Boolean;

    const/4 v3, 0x6

    .line 420
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 421
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/Notification;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 408
    check-cast p1, Lcom/squareup/server/account/protos/Notification;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/Notification$ProtoAdapter_Notification;->encodedSize(Lcom/squareup/server/account/protos/Notification;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/server/account/protos/Notification;)Lcom/squareup/server/account/protos/Notification;
    .locals 2

    .line 458
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/Notification;->newBuilder()Lcom/squareup/server/account/protos/Notification$Builder;

    move-result-object p1

    .line 459
    iget-object v0, p1, Lcom/squareup/server/account/protos/Notification$Builder;->button:Lcom/squareup/server/account/protos/Notification$Button;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/server/account/protos/Notification$Button;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/Notification$Builder;->button:Lcom/squareup/server/account/protos/Notification$Button;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/Notification$Button;

    iput-object v0, p1, Lcom/squareup/server/account/protos/Notification$Builder;->button:Lcom/squareup/server/account/protos/Notification$Button;

    .line 460
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/Notification$Builder;->buttons:Ljava/util/List;

    sget-object v1, Lcom/squareup/server/account/protos/Notification$Button;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 461
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/Notification$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 462
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/Notification$Builder;->build()Lcom/squareup/server/account/protos/Notification;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 408
    check-cast p1, Lcom/squareup/server/account/protos/Notification;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/Notification$ProtoAdapter_Notification;->redact(Lcom/squareup/server/account/protos/Notification;)Lcom/squareup/server/account/protos/Notification;

    move-result-object p1

    return-object p1
.end method
