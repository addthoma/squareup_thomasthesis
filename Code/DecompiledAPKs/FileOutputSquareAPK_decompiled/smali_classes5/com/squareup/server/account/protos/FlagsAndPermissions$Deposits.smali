.class public final Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;
.super Lcom/squareup/wire/AndroidMessage;
.source "FlagsAndPermissions.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Deposits"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$ProtoAdapter_Deposits;,
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ALLOW_ADD_MONEY:Ljava/lang/Boolean;

.field public static final DEFAULT_BANK_LINKING_WITH_TWO_ACCOUNTS:Ljava/lang/Boolean;

.field public static final DEFAULT_BANK_LINK_IN_APP:Ljava/lang/Boolean;

.field public static final DEFAULT_BANK_LINK_POST_SIGNUP:Ljava/lang/Boolean;

.field public static final DEFAULT_BANK_RESEND_EMAIL:Ljava/lang/Boolean;

.field public static final DEFAULT_CAN_PAUSE_NIGHTLY_DEPOSIT:Ljava/lang/Boolean;

.field public static final DEFAULT_INSTANT_DEPOSIT_REQUIRES_LINKED_CARD:Ljava/lang/Boolean;

.field public static final DEFAULT_SHOW_WEEKEND_BALANCE_TOGGLE:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_DEPOSITS_REPORT:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_DEPOSITS_REPORT_CARD_PAYMENTS:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_DEPOSITS_REPORT_DETAIL:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_DEPOSITS_REPORT_IN_DEPOSITS_APPLET:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_DEPOSIT_SETTINGS:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_DEPOSIT_SETTINGS_CARD_LINKING:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_DEPOSIT_SETTINGS_DEPOSIT_SCHEDULE:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_INSTANT_DEPOSIT:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_SAME_DAY_DEPOSIT:Ljava/lang/Boolean;

.field public static final DEFAULT_VIEW_BANK_ACCOUNT:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final allow_add_money:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x13
    .end annotation
.end field

.field public final bank_link_in_app:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xc
    .end annotation
.end field

.field public final bank_link_post_signup:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xb
    .end annotation
.end field

.field public final bank_linking_with_two_accounts:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xf
    .end annotation
.end field

.field public final bank_resend_email:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xd
    .end annotation
.end field

.field public final can_pause_nightly_deposit:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x12
    .end annotation
.end field

.field public final instant_deposit_requires_linked_card:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x6
    .end annotation
.end field

.field public final show_weekend_balance_toggle:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x11
    .end annotation
.end field

.field public final use_deposit_settings:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x10
    .end annotation
.end field

.field public final use_deposit_settings_card_linking:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x7
    .end annotation
.end field

.field public final use_deposit_settings_deposit_schedule:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x8
    .end annotation
.end field

.field public final use_deposits_report:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1
    .end annotation
.end field

.field public final use_deposits_report_card_payments:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3
    .end annotation
.end field

.field public final use_deposits_report_detail:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2
    .end annotation
.end field

.field public final use_deposits_report_in_deposits_applet:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xa
    .end annotation
.end field

.field public final use_instant_deposit:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x5
    .end annotation
.end field

.field public final use_same_day_deposit:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x9
    .end annotation
.end field

.field public final view_bank_account:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x4
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 9137
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$ProtoAdapter_Deposits;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$ProtoAdapter_Deposits;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 9139
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->CREATOR:Landroid/os/Parcelable$Creator;

    const/4 v0, 0x0

    .line 9143
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->DEFAULT_USE_DEPOSITS_REPORT:Ljava/lang/Boolean;

    .line 9145
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->DEFAULT_USE_DEPOSITS_REPORT_DETAIL:Ljava/lang/Boolean;

    .line 9147
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->DEFAULT_USE_DEPOSITS_REPORT_CARD_PAYMENTS:Ljava/lang/Boolean;

    .line 9149
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->DEFAULT_VIEW_BANK_ACCOUNT:Ljava/lang/Boolean;

    .line 9151
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->DEFAULT_USE_INSTANT_DEPOSIT:Ljava/lang/Boolean;

    .line 9153
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->DEFAULT_INSTANT_DEPOSIT_REQUIRES_LINKED_CARD:Ljava/lang/Boolean;

    .line 9155
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->DEFAULT_USE_DEPOSIT_SETTINGS_CARD_LINKING:Ljava/lang/Boolean;

    .line 9157
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->DEFAULT_USE_DEPOSIT_SETTINGS_DEPOSIT_SCHEDULE:Ljava/lang/Boolean;

    .line 9159
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->DEFAULT_USE_SAME_DAY_DEPOSIT:Ljava/lang/Boolean;

    .line 9161
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->DEFAULT_USE_DEPOSITS_REPORT_IN_DEPOSITS_APPLET:Ljava/lang/Boolean;

    .line 9163
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->DEFAULT_BANK_LINK_POST_SIGNUP:Ljava/lang/Boolean;

    .line 9165
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->DEFAULT_BANK_LINK_IN_APP:Ljava/lang/Boolean;

    .line 9167
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->DEFAULT_BANK_RESEND_EMAIL:Ljava/lang/Boolean;

    .line 9169
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->DEFAULT_BANK_LINKING_WITH_TWO_ACCOUNTS:Ljava/lang/Boolean;

    .line 9171
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->DEFAULT_USE_DEPOSIT_SETTINGS:Ljava/lang/Boolean;

    .line 9173
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->DEFAULT_SHOW_WEEKEND_BALANCE_TOGGLE:Ljava/lang/Boolean;

    .line 9175
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->DEFAULT_CAN_PAUSE_NIGHTLY_DEPOSIT:Ljava/lang/Boolean;

    .line 9177
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->DEFAULT_ALLOW_ADD_MONEY:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;Lokio/ByteString;)V
    .locals 1

    .line 9360
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 9361
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->use_deposits_report:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposits_report:Ljava/lang/Boolean;

    .line 9362
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->use_deposits_report_detail:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposits_report_detail:Ljava/lang/Boolean;

    .line 9363
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->use_deposits_report_card_payments:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposits_report_card_payments:Ljava/lang/Boolean;

    .line 9364
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->view_bank_account:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->view_bank_account:Ljava/lang/Boolean;

    .line 9365
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->use_instant_deposit:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_instant_deposit:Ljava/lang/Boolean;

    .line 9366
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->instant_deposit_requires_linked_card:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->instant_deposit_requires_linked_card:Ljava/lang/Boolean;

    .line 9367
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->use_deposit_settings_card_linking:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposit_settings_card_linking:Ljava/lang/Boolean;

    .line 9368
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->use_deposit_settings_deposit_schedule:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposit_settings_deposit_schedule:Ljava/lang/Boolean;

    .line 9369
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->use_same_day_deposit:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_same_day_deposit:Ljava/lang/Boolean;

    .line 9370
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->use_deposits_report_in_deposits_applet:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposits_report_in_deposits_applet:Ljava/lang/Boolean;

    .line 9371
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->bank_link_post_signup:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->bank_link_post_signup:Ljava/lang/Boolean;

    .line 9372
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->bank_link_in_app:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->bank_link_in_app:Ljava/lang/Boolean;

    .line 9373
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->bank_resend_email:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->bank_resend_email:Ljava/lang/Boolean;

    .line 9374
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->bank_linking_with_two_accounts:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->bank_linking_with_two_accounts:Ljava/lang/Boolean;

    .line 9375
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->use_deposit_settings:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposit_settings:Ljava/lang/Boolean;

    .line 9376
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->show_weekend_balance_toggle:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->show_weekend_balance_toggle:Ljava/lang/Boolean;

    .line 9377
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->can_pause_nightly_deposit:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->can_pause_nightly_deposit:Ljava/lang/Boolean;

    .line 9378
    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->allow_add_money:Ljava/lang/Boolean;

    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->allow_add_money:Ljava/lang/Boolean;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 9533
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 9409
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 9410
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;

    .line 9411
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposits_report:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposits_report:Ljava/lang/Boolean;

    .line 9412
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposits_report_detail:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposits_report_detail:Ljava/lang/Boolean;

    .line 9413
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposits_report_card_payments:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposits_report_card_payments:Ljava/lang/Boolean;

    .line 9414
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->view_bank_account:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->view_bank_account:Ljava/lang/Boolean;

    .line 9415
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_instant_deposit:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_instant_deposit:Ljava/lang/Boolean;

    .line 9416
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->instant_deposit_requires_linked_card:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->instant_deposit_requires_linked_card:Ljava/lang/Boolean;

    .line 9417
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposit_settings_card_linking:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposit_settings_card_linking:Ljava/lang/Boolean;

    .line 9418
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposit_settings_deposit_schedule:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposit_settings_deposit_schedule:Ljava/lang/Boolean;

    .line 9419
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_same_day_deposit:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_same_day_deposit:Ljava/lang/Boolean;

    .line 9420
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposits_report_in_deposits_applet:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposits_report_in_deposits_applet:Ljava/lang/Boolean;

    .line 9421
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->bank_link_post_signup:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->bank_link_post_signup:Ljava/lang/Boolean;

    .line 9422
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->bank_link_in_app:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->bank_link_in_app:Ljava/lang/Boolean;

    .line 9423
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->bank_resend_email:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->bank_resend_email:Ljava/lang/Boolean;

    .line 9424
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->bank_linking_with_two_accounts:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->bank_linking_with_two_accounts:Ljava/lang/Boolean;

    .line 9425
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposit_settings:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposit_settings:Ljava/lang/Boolean;

    .line 9426
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->show_weekend_balance_toggle:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->show_weekend_balance_toggle:Ljava/lang/Boolean;

    .line 9427
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->can_pause_nightly_deposit:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->can_pause_nightly_deposit:Ljava/lang/Boolean;

    .line 9428
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->allow_add_money:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->allow_add_money:Ljava/lang/Boolean;

    .line 9429
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 9434
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_12

    .line 9436
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 9437
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposits_report:Ljava/lang/Boolean;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 9438
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposits_report_detail:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 9439
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposits_report_card_payments:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 9440
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->view_bank_account:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 9441
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_instant_deposit:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 9442
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->instant_deposit_requires_linked_card:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 9443
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposit_settings_card_linking:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 9444
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposit_settings_deposit_schedule:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 9445
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_same_day_deposit:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 9446
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposits_report_in_deposits_applet:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 9447
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->bank_link_post_signup:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 9448
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->bank_link_in_app:Ljava/lang/Boolean;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 9449
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->bank_resend_email:Ljava/lang/Boolean;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_c

    :cond_c
    const/4 v1, 0x0

    :goto_c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 9450
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->bank_linking_with_two_accounts:Ljava/lang/Boolean;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_d

    :cond_d
    const/4 v1, 0x0

    :goto_d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 9451
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposit_settings:Ljava/lang/Boolean;

    if-eqz v1, :cond_e

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_e

    :cond_e
    const/4 v1, 0x0

    :goto_e
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 9452
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->show_weekend_balance_toggle:Ljava/lang/Boolean;

    if-eqz v1, :cond_f

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_f

    :cond_f
    const/4 v1, 0x0

    :goto_f
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 9453
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->can_pause_nightly_deposit:Ljava/lang/Boolean;

    if-eqz v1, :cond_10

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_10

    :cond_10
    const/4 v1, 0x0

    :goto_10
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 9454
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->allow_add_money:Ljava/lang/Boolean;

    if-eqz v1, :cond_11

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_11
    add-int/2addr v0, v2

    .line 9455
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_12
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;
    .locals 2

    .line 9383
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;-><init>()V

    .line 9384
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposits_report:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->use_deposits_report:Ljava/lang/Boolean;

    .line 9385
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposits_report_detail:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->use_deposits_report_detail:Ljava/lang/Boolean;

    .line 9386
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposits_report_card_payments:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->use_deposits_report_card_payments:Ljava/lang/Boolean;

    .line 9387
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->view_bank_account:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->view_bank_account:Ljava/lang/Boolean;

    .line 9388
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_instant_deposit:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->use_instant_deposit:Ljava/lang/Boolean;

    .line 9389
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->instant_deposit_requires_linked_card:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->instant_deposit_requires_linked_card:Ljava/lang/Boolean;

    .line 9390
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposit_settings_card_linking:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->use_deposit_settings_card_linking:Ljava/lang/Boolean;

    .line 9391
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposit_settings_deposit_schedule:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->use_deposit_settings_deposit_schedule:Ljava/lang/Boolean;

    .line 9392
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_same_day_deposit:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->use_same_day_deposit:Ljava/lang/Boolean;

    .line 9393
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposits_report_in_deposits_applet:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->use_deposits_report_in_deposits_applet:Ljava/lang/Boolean;

    .line 9394
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->bank_link_post_signup:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->bank_link_post_signup:Ljava/lang/Boolean;

    .line 9395
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->bank_link_in_app:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->bank_link_in_app:Ljava/lang/Boolean;

    .line 9396
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->bank_resend_email:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->bank_resend_email:Ljava/lang/Boolean;

    .line 9397
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->bank_linking_with_two_accounts:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->bank_linking_with_two_accounts:Ljava/lang/Boolean;

    .line 9398
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposit_settings:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->use_deposit_settings:Ljava/lang/Boolean;

    .line 9399
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->show_weekend_balance_toggle:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->show_weekend_balance_toggle:Ljava/lang/Boolean;

    .line 9400
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->can_pause_nightly_deposit:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->can_pause_nightly_deposit:Ljava/lang/Boolean;

    .line 9401
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->allow_add_money:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->allow_add_money:Ljava/lang/Boolean;

    .line 9402
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 9136
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;
    .locals 2

    .line 9511
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposits_report:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposits_report:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->use_deposits_report(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v1

    .line 9512
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposits_report_detail:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposits_report_detail:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->use_deposits_report_detail(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v1

    .line 9513
    :cond_1
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposits_report_card_payments:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposits_report_card_payments:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->use_deposits_report_card_payments(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v1

    .line 9514
    :cond_2
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->view_bank_account:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->view_bank_account:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->view_bank_account(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v1

    .line 9515
    :cond_3
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_instant_deposit:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_instant_deposit:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->use_instant_deposit(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v1

    .line 9516
    :cond_4
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->instant_deposit_requires_linked_card:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->instant_deposit_requires_linked_card:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->instant_deposit_requires_linked_card(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v1

    .line 9517
    :cond_5
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposit_settings_card_linking:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposit_settings_card_linking:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->use_deposit_settings_card_linking(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v1

    .line 9518
    :cond_6
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposit_settings_deposit_schedule:Ljava/lang/Boolean;

    if-eqz v0, :cond_7

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposit_settings_deposit_schedule:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->use_deposit_settings_deposit_schedule(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v1

    .line 9519
    :cond_7
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_same_day_deposit:Ljava/lang/Boolean;

    if-eqz v0, :cond_8

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_same_day_deposit:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->use_same_day_deposit(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v1

    .line 9520
    :cond_8
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposits_report_in_deposits_applet:Ljava/lang/Boolean;

    if-eqz v0, :cond_9

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposits_report_in_deposits_applet:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->use_deposits_report_in_deposits_applet(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v1

    .line 9521
    :cond_9
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->bank_link_post_signup:Ljava/lang/Boolean;

    if-eqz v0, :cond_a

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->bank_link_post_signup:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->bank_link_post_signup(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v1

    .line 9522
    :cond_a
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->bank_link_in_app:Ljava/lang/Boolean;

    if-eqz v0, :cond_b

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->bank_link_in_app:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->bank_link_in_app(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v1

    .line 9523
    :cond_b
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->bank_resend_email:Ljava/lang/Boolean;

    if-eqz v0, :cond_c

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->bank_resend_email:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->bank_resend_email(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v1

    .line 9524
    :cond_c
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->bank_linking_with_two_accounts:Ljava/lang/Boolean;

    if-eqz v0, :cond_d

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->bank_linking_with_two_accounts:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->bank_linking_with_two_accounts(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v1

    .line 9525
    :cond_d
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposit_settings:Ljava/lang/Boolean;

    if-eqz v0, :cond_e

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposit_settings:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->use_deposit_settings(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v1

    .line 9526
    :cond_e
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->show_weekend_balance_toggle:Ljava/lang/Boolean;

    if-eqz v0, :cond_f

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->show_weekend_balance_toggle:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->show_weekend_balance_toggle(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v1

    .line 9527
    :cond_f
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->can_pause_nightly_deposit:Ljava/lang/Boolean;

    if-eqz v0, :cond_10

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->can_pause_nightly_deposit:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->can_pause_nightly_deposit(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v1

    .line 9528
    :cond_10
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->allow_add_money:Ljava/lang/Boolean;

    if-eqz v0, :cond_11

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->allow_add_money:Ljava/lang/Boolean;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->allow_add_money(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v1

    :cond_11
    if-nez v1, :cond_12

    move-object p1, p0

    goto :goto_0

    .line 9529
    :cond_12
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 9136
    check-cast p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->overlay(Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;
    .locals 2

    .line 9487
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposits_report:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->DEFAULT_USE_DEPOSITS_REPORT:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->use_deposits_report(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v1

    .line 9488
    :cond_0
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposits_report_detail:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->DEFAULT_USE_DEPOSITS_REPORT_DETAIL:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->use_deposits_report_detail(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v1

    .line 9489
    :cond_1
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposits_report_card_payments:Ljava/lang/Boolean;

    if-nez v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->DEFAULT_USE_DEPOSITS_REPORT_CARD_PAYMENTS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->use_deposits_report_card_payments(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v1

    .line 9490
    :cond_2
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->view_bank_account:Ljava/lang/Boolean;

    if-nez v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->DEFAULT_VIEW_BANK_ACCOUNT:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->view_bank_account(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v1

    .line 9491
    :cond_3
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_instant_deposit:Ljava/lang/Boolean;

    if-nez v0, :cond_4

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->DEFAULT_USE_INSTANT_DEPOSIT:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->use_instant_deposit(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v1

    .line 9492
    :cond_4
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->instant_deposit_requires_linked_card:Ljava/lang/Boolean;

    if-nez v0, :cond_5

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->DEFAULT_INSTANT_DEPOSIT_REQUIRES_LINKED_CARD:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->instant_deposit_requires_linked_card(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v1

    .line 9493
    :cond_5
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposit_settings_card_linking:Ljava/lang/Boolean;

    if-nez v0, :cond_6

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->DEFAULT_USE_DEPOSIT_SETTINGS_CARD_LINKING:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->use_deposit_settings_card_linking(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v1

    .line 9494
    :cond_6
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposit_settings_deposit_schedule:Ljava/lang/Boolean;

    if-nez v0, :cond_7

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->DEFAULT_USE_DEPOSIT_SETTINGS_DEPOSIT_SCHEDULE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->use_deposit_settings_deposit_schedule(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v1

    .line 9495
    :cond_7
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_same_day_deposit:Ljava/lang/Boolean;

    if-nez v0, :cond_8

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->DEFAULT_USE_SAME_DAY_DEPOSIT:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->use_same_day_deposit(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v1

    .line 9496
    :cond_8
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposits_report_in_deposits_applet:Ljava/lang/Boolean;

    if-nez v0, :cond_9

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->DEFAULT_USE_DEPOSITS_REPORT_IN_DEPOSITS_APPLET:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->use_deposits_report_in_deposits_applet(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v1

    .line 9497
    :cond_9
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->bank_link_post_signup:Ljava/lang/Boolean;

    if-nez v0, :cond_a

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->DEFAULT_BANK_LINK_POST_SIGNUP:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->bank_link_post_signup(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v1

    .line 9498
    :cond_a
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->bank_link_in_app:Ljava/lang/Boolean;

    if-nez v0, :cond_b

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->DEFAULT_BANK_LINK_IN_APP:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->bank_link_in_app(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v1

    .line 9499
    :cond_b
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->bank_resend_email:Ljava/lang/Boolean;

    if-nez v0, :cond_c

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->DEFAULT_BANK_RESEND_EMAIL:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->bank_resend_email(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v1

    .line 9500
    :cond_c
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->bank_linking_with_two_accounts:Ljava/lang/Boolean;

    if-nez v0, :cond_d

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->DEFAULT_BANK_LINKING_WITH_TWO_ACCOUNTS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->bank_linking_with_two_accounts(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v1

    .line 9501
    :cond_d
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposit_settings:Ljava/lang/Boolean;

    if-nez v0, :cond_e

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->DEFAULT_USE_DEPOSIT_SETTINGS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->use_deposit_settings(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v1

    .line 9502
    :cond_e
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->show_weekend_balance_toggle:Ljava/lang/Boolean;

    if-nez v0, :cond_f

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->DEFAULT_SHOW_WEEKEND_BALANCE_TOGGLE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->show_weekend_balance_toggle(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v1

    .line 9503
    :cond_f
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->can_pause_nightly_deposit:Ljava/lang/Boolean;

    if-nez v0, :cond_10

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->DEFAULT_CAN_PAUSE_NIGHTLY_DEPOSIT:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->can_pause_nightly_deposit(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v1

    .line 9504
    :cond_10
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->allow_add_money:Ljava/lang/Boolean;

    if-nez v0, :cond_11

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->DEFAULT_ALLOW_ADD_MONEY:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->allow_add_money(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;

    move-result-object v1

    :cond_11
    if-nez v1, :cond_12

    move-object v0, p0

    goto :goto_0

    .line 9505
    :cond_12
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 9136
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 9462
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 9463
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposits_report:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const-string v1, ", use_deposits_report="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposits_report:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 9464
    :cond_0
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposits_report_detail:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    const-string v1, ", use_deposits_report_detail="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposits_report_detail:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 9465
    :cond_1
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposits_report_card_payments:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", use_deposits_report_card_payments="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposits_report_card_payments:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 9466
    :cond_2
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->view_bank_account:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    const-string v1, ", view_bank_account="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->view_bank_account:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 9467
    :cond_3
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_instant_deposit:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    const-string v1, ", use_instant_deposit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_instant_deposit:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 9468
    :cond_4
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->instant_deposit_requires_linked_card:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    const-string v1, ", instant_deposit_requires_linked_card="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->instant_deposit_requires_linked_card:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 9469
    :cond_5
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposit_settings_card_linking:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    const-string v1, ", use_deposit_settings_card_linking="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposit_settings_card_linking:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 9470
    :cond_6
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposit_settings_deposit_schedule:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    const-string v1, ", use_deposit_settings_deposit_schedule="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposit_settings_deposit_schedule:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 9471
    :cond_7
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_same_day_deposit:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    const-string v1, ", use_same_day_deposit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_same_day_deposit:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 9472
    :cond_8
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposits_report_in_deposits_applet:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    const-string v1, ", use_deposits_report_in_deposits_applet="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposits_report_in_deposits_applet:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 9473
    :cond_9
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->bank_link_post_signup:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    const-string v1, ", bank_link_post_signup="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->bank_link_post_signup:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 9474
    :cond_a
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->bank_link_in_app:Ljava/lang/Boolean;

    if-eqz v1, :cond_b

    const-string v1, ", bank_link_in_app="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->bank_link_in_app:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 9475
    :cond_b
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->bank_resend_email:Ljava/lang/Boolean;

    if-eqz v1, :cond_c

    const-string v1, ", bank_resend_email="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->bank_resend_email:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 9476
    :cond_c
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->bank_linking_with_two_accounts:Ljava/lang/Boolean;

    if-eqz v1, :cond_d

    const-string v1, ", bank_linking_with_two_accounts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->bank_linking_with_two_accounts:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 9477
    :cond_d
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposit_settings:Ljava/lang/Boolean;

    if-eqz v1, :cond_e

    const-string v1, ", use_deposit_settings="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->use_deposit_settings:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 9478
    :cond_e
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->show_weekend_balance_toggle:Ljava/lang/Boolean;

    if-eqz v1, :cond_f

    const-string v1, ", show_weekend_balance_toggle="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->show_weekend_balance_toggle:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 9479
    :cond_f
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->can_pause_nightly_deposit:Ljava/lang/Boolean;

    if-eqz v1, :cond_10

    const-string v1, ", can_pause_nightly_deposit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->can_pause_nightly_deposit:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 9480
    :cond_10
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->allow_add_money:Ljava/lang/Boolean;

    if-eqz v1, :cond_11

    const-string v1, ", allow_add_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;->allow_add_money:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_11
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Deposits{"

    .line 9481
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
