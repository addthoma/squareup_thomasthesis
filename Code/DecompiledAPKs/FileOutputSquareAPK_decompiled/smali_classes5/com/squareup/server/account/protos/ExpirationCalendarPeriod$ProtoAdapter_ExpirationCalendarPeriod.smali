.class final Lcom/squareup/server/account/protos/ExpirationCalendarPeriod$ProtoAdapter_ExpirationCalendarPeriod;
.super Lcom/squareup/wire/ProtoAdapter;
.source "ExpirationCalendarPeriod.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ExpirationCalendarPeriod"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 162
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 181
    new-instance v0, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod$Builder;-><init>()V

    .line 182
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 183
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 195
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 193
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->UINT64:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod$Builder;->period(Ljava/lang/Long;)Lcom/squareup/server/account/protos/ExpirationCalendarPeriod$Builder;

    goto :goto_0

    .line 187
    :cond_1
    :try_start_0
    sget-object v4, Lcom/squareup/server/account/protos/CalendarPeriod$CalendarUnit;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/server/account/protos/CalendarPeriod$CalendarUnit;

    invoke-virtual {v0, v4}, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod$Builder;->unit(Lcom/squareup/server/account/protos/CalendarPeriod$CalendarUnit;)Lcom/squareup/server/account/protos/ExpirationCalendarPeriod$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 189
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 199
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 200
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod$Builder;->build()Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 160
    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod$ProtoAdapter_ExpirationCalendarPeriod;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 174
    sget-object v0, Lcom/squareup/server/account/protos/CalendarPeriod$CalendarUnit;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;->unit:Lcom/squareup/server/account/protos/CalendarPeriod$CalendarUnit;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 175
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->UINT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;->period:Ljava/lang/Long;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 176
    invoke-virtual {p2}, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 160
    check-cast p2, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod$ProtoAdapter_ExpirationCalendarPeriod;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;)I
    .locals 4

    .line 167
    sget-object v0, Lcom/squareup/server/account/protos/CalendarPeriod$CalendarUnit;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;->unit:Lcom/squareup/server/account/protos/CalendarPeriod$CalendarUnit;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->UINT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;->period:Ljava/lang/Long;

    const/4 v3, 0x2

    .line 168
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 169
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 160
    check-cast p1, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod$ProtoAdapter_ExpirationCalendarPeriod;->encodedSize(Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;)Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;
    .locals 0

    .line 205
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;->newBuilder()Lcom/squareup/server/account/protos/ExpirationCalendarPeriod$Builder;

    move-result-object p1

    .line 206
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 207
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod$Builder;->build()Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 160
    check-cast p1, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod$ProtoAdapter_ExpirationCalendarPeriod;->redact(Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;)Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;

    move-result-object p1

    return-object p1
.end method
