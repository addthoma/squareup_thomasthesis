.class public final Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FlagsAndPermissions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC;",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public show_spoc_version_number:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 17646
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC;
    .locals 3

    .line 17659
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC$Builder;->show_spoc_version_number:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC;-><init>(Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 17643
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC;

    move-result-object v0

    return-object v0
.end method

.method public show_spoc_version_number(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC$Builder;
    .locals 0

    .line 17653
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SPoC$Builder;->show_spoc_version_number:Ljava/lang/Boolean;

    return-object p0
.end method
