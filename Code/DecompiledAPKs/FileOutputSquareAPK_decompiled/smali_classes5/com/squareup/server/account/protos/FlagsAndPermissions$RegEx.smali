.class public final Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;
.super Lcom/squareup/wire/AndroidMessage;
.source "FlagsAndPermissions.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RegEx"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$ProtoAdapter_RegEx;,
        Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_BUNDLE_LOGGING:Ljava/lang/Boolean;

.field public static final DEFAULT_BUYER_CHECKOUT_DISPLAY_TRANSACTION_TYPE:Ljava/lang/Boolean;

.field public static final DEFAULT_CAN_CLOSE_REMOTE_CASH_DRAWERS_ANDROID:Ljava/lang/Boolean;

.field public static final DEFAULT_CAN_PRINT_SINGLE_TICKET_PER_ITEM:Ljava/lang/Boolean;

.field public static final DEFAULT_CHECKOUT_APPLET_V2_ANDROID:Ljava/lang/Boolean;

.field public static final DEFAULT_DELETE_SESSION_TOKEN_PLAINTEXT:Ljava/lang/Boolean;

.field public static final DEFAULT_DIP_TAP_TO_CREATE_OPEN_TICKET_WITH_NAME:Ljava/lang/Boolean;

.field public static final DEFAULT_ENABLE_VERBOSE_LOGIN_RESPONSE_CACHE_LOGGING:Ljava/lang/Boolean;

.field public static final DEFAULT_ENHANCED_TRANSACTION_SEARCH_ANDROID:Ljava/lang/Boolean;

.field public static final DEFAULT_FIRMWARE_UPDATE_JAIL_T2:Ljava/lang/Boolean;

.field public static final DEFAULT_FIRMWARE_UPDATE_T2:Ljava/lang/Boolean;

.field public static final DEFAULT_HARDWARE_SECURE_TOUCH:Ljava/lang/Boolean;

.field public static final DEFAULT_HARDWARE_SECURE_TOUCH_ACCESSIBILITY_MODE:Ljava/lang/Boolean;

.field public static final DEFAULT_HARDWARE_SECURE_TOUCH_HIGH_CONTRAST_MODE:Ljava/lang/Boolean;

.field public static final DEFAULT_IGNORE_PII_ACCESSIBILITY_SCRUBBER_ANDROID:Ljava/lang/Boolean;

.field public static final DEFAULT_INTERCEPT_MAGSWIPE_EVENTS_DURING_PRINTING_T2:Ljava/lang/Boolean;

.field public static final DEFAULT_LOYALTY_CHECKOUT_X2:Ljava/lang/Boolean;

.field public static final DEFAULT_ORDER_ENTRY_LIBRARY_CREATE_NEW_ITEM:Ljava/lang/Boolean;

.field public static final DEFAULT_ORDER_ENTRY_LIBRARY_HIDE_GIFTCARDS_REWARDS_IF_NO_ITEMS:Ljava/lang/Boolean;

.field public static final DEFAULT_ORDER_ENTRY_LIBRARY_ITEM_SUGGESTIONS:Ljava/lang/Boolean;

.field public static final DEFAULT_PRINTER_DEBUGGING:Ljava/lang/Boolean;

.field public static final DEFAULT_PRINT_ITEMIZED_DISCOUNTS:Ljava/lang/Boolean;

.field public static final DEFAULT_RECEIPTS_PRINT_DISPOSITION:Ljava/lang/Boolean;

.field public static final DEFAULT_RESILIENT_BUS:Ljava/lang/Boolean;

.field public static final DEFAULT_RESTART_APP_AFTER_CRASH:Ljava/lang/Boolean;

.field public static final DEFAULT_SEPARATED_PRINTOUTS:Ljava/lang/Boolean;

.field public static final DEFAULT_SHOW_FEE_BREAKDOWN_TABLE:Ljava/lang/Boolean;

.field public static final DEFAULT_SKYHOOK_INTEGRATION_V2:Ljava/lang/Boolean;

.field public static final DEFAULT_SKYHOOK_T2_USE_3_HOUR_CHECK_INTERVAL:Ljava/lang/Boolean;

.field public static final DEFAULT_SKYHOOK_T2_USE_6_HOUR_CACHE_LIFESPAN:Ljava/lang/Boolean;

.field public static final DEFAULT_SKYHOOK_V2_ONLY_USE_IP_LOCATION_WITH_NO_WPS:Ljava/lang/Boolean;

.field public static final DEFAULT_SKYHOOK_X2_USE_24_HOUR_CACHE_LIFESPAN:Ljava/lang/Boolean;

.field public static final DEFAULT_UPLOAD_LEDGER_AND_DIAGNOSTICS_X2:Ljava/lang/Boolean;

.field public static final DEFAULT_UPLOAD_X2_WIFI_EVENTS:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_API_URL_LIST_ANDROID:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_CLOCK_SKEW_LOCKOUT_ANDROID:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_DEVICE_SETTINGS_ANDROID:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_FEATURE_TOUR_X2:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_JEDI_HELP_APPLET:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_JEDI_HELP_APPLET_T2:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_JEDI_HELP_APPLET_X2:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_ORDER_ENTRY_SCREEN_V2_ANDROID:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_PERSISTENT_BUNDLE_ANDROID:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_PREDEFINED_TICKETS_T2:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_PRE_TAX_TIPPING_ANDROID:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_SESSION_TOKEN_ENCRYPTION_ANDROID:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_SESSION_TOKEN_ENCRYPTION_ANDROID_V2:Ljava/lang/Boolean;

.field public static final DEFAULT_WAIT_FOR_BRAN_X2:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final bundle_logging:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x90
    .end annotation
.end field

.field public final buyer_checkout_display_transaction_type:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xae
    .end annotation
.end field

.field public final can_close_remote_cash_drawers_android:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xa6
    .end annotation
.end field

.field public final can_print_single_ticket_per_item:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xb8
    .end annotation
.end field

.field public final checkout_applet_v2_android:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xc3
    .end annotation
.end field

.field public final delete_session_token_plaintext:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xbc
    .end annotation
.end field

.field public final dip_tap_to_create_open_ticket_with_name:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xe
    .end annotation
.end field

.field public final enable_verbose_login_response_cache_logging:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xbd
    .end annotation
.end field

.field public final enhanced_transaction_search_android:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xba
    .end annotation
.end field

.field public final firmware_update_jail_t2:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xa0
    .end annotation
.end field

.field public final firmware_update_t2:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x9b
    .end annotation
.end field

.field public final hardware_secure_touch:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xa5
    .end annotation
.end field

.field public final hardware_secure_touch_accessibility_mode:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xb9
    .end annotation
.end field

.field public final hardware_secure_touch_high_contrast_mode:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xbf
    .end annotation
.end field

.field public final ignore_pii_accessibility_scrubber_android:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3
    .end annotation
.end field

.field public final intercept_magswipe_events_during_printing_t2:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1a
    .end annotation
.end field

.field public final loyalty_checkout_x2:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x5
    .end annotation
.end field

.field public final order_entry_library_create_new_item:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xb0
    .end annotation
.end field

.field public final order_entry_library_hide_giftcards_rewards_if_no_items:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xb2
    .end annotation
.end field

.field public final order_entry_library_item_suggestions:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xb7
    .end annotation
.end field

.field public final print_itemized_discounts:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x13
    .end annotation
.end field

.field public final printer_debugging:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x16
    .end annotation
.end field

.field public final receipts_print_disposition:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xaf
    .end annotation
.end field

.field public final resilient_bus:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x9f
    .end annotation
.end field

.field public final restart_app_after_crash:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x17
    .end annotation
.end field

.field public final separated_printouts:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x9d
    .end annotation
.end field

.field public final show_fee_breakdown_table:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xb1
    .end annotation
.end field

.field public final skyhook_integration_v2:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xa9
    .end annotation
.end field

.field public final skyhook_t2_use_3_hour_check_interval:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xab
    .end annotation
.end field

.field public final skyhook_t2_use_6_hour_cache_lifespan:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xac
    .end annotation
.end field

.field public final skyhook_v2_only_use_ip_location_with_no_wps:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xad
    .end annotation
.end field

.field public final skyhook_x2_use_24_hour_cache_lifespan:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xaa
    .end annotation
.end field

.field public final upload_ledger_and_diagnostics_x2:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x12
    .end annotation
.end field

.field public final upload_x2_wifi_events:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x4
    .end annotation
.end field

.field public final use_api_url_list_android:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2
    .end annotation
.end field

.field public final use_clock_skew_lockout_android:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x6
    .end annotation
.end field

.field public final use_device_settings_android:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1b
    .end annotation
.end field

.field public final use_feature_tour_x2:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x15
    .end annotation
.end field

.field public final use_jedi_help_applet:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xc
    .end annotation
.end field

.field public final use_jedi_help_applet_t2:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1c
    .end annotation
.end field

.field public final use_jedi_help_applet_x2:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x11
    .end annotation
.end field

.field public final use_order_entry_screen_v2_android:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xb5
    .end annotation
.end field

.field public final use_persistent_bundle_android:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xd
    .end annotation
.end field

.field public final use_pre_tax_tipping_android:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final use_predefined_tickets_t2:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x9e
    .end annotation
.end field

.field public final use_session_token_encryption_android:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x10
    .end annotation
.end field

.field public final use_session_token_encryption_android_v2:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xbe
    .end annotation
.end field

.field public final wait_for_bran_x2:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xa8
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 5839
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$ProtoAdapter_RegEx;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$ProtoAdapter_RegEx;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 5841
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->CREATOR:Landroid/os/Parcelable$Creator;

    const/4 v0, 0x1

    .line 5845
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_BUNDLE_LOGGING:Ljava/lang/Boolean;

    const/4 v1, 0x0

    .line 5847
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    sput-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_USE_PRE_TAX_TIPPING_ANDROID:Ljava/lang/Boolean;

    .line 5849
    sput-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_USE_API_URL_LIST_ANDROID:Ljava/lang/Boolean;

    .line 5851
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_IGNORE_PII_ACCESSIBILITY_SCRUBBER_ANDROID:Ljava/lang/Boolean;

    .line 5853
    sput-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_UPLOAD_X2_WIFI_EVENTS:Ljava/lang/Boolean;

    .line 5855
    sput-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_LOYALTY_CHECKOUT_X2:Ljava/lang/Boolean;

    .line 5857
    sput-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_USE_CLOCK_SKEW_LOCKOUT_ANDROID:Ljava/lang/Boolean;

    .line 5859
    sput-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_USE_JEDI_HELP_APPLET:Ljava/lang/Boolean;

    .line 5861
    sput-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_USE_PERSISTENT_BUNDLE_ANDROID:Ljava/lang/Boolean;

    .line 5863
    sput-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_DIP_TAP_TO_CREATE_OPEN_TICKET_WITH_NAME:Ljava/lang/Boolean;

    .line 5865
    sput-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_USE_SESSION_TOKEN_ENCRYPTION_ANDROID:Ljava/lang/Boolean;

    .line 5867
    sput-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_USE_JEDI_HELP_APPLET_X2:Ljava/lang/Boolean;

    .line 5869
    sput-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_UPLOAD_LEDGER_AND_DIAGNOSTICS_X2:Ljava/lang/Boolean;

    .line 5871
    sput-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_PRINT_ITEMIZED_DISCOUNTS:Ljava/lang/Boolean;

    .line 5873
    sput-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_USE_FEATURE_TOUR_X2:Ljava/lang/Boolean;

    .line 5875
    sput-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_PRINTER_DEBUGGING:Ljava/lang/Boolean;

    .line 5877
    sput-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_RESTART_APP_AFTER_CRASH:Ljava/lang/Boolean;

    .line 5879
    sput-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_INTERCEPT_MAGSWIPE_EVENTS_DURING_PRINTING_T2:Ljava/lang/Boolean;

    .line 5881
    sput-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_USE_DEVICE_SETTINGS_ANDROID:Ljava/lang/Boolean;

    .line 5883
    sput-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_USE_JEDI_HELP_APPLET_T2:Ljava/lang/Boolean;

    .line 5885
    sput-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_FIRMWARE_UPDATE_T2:Ljava/lang/Boolean;

    .line 5887
    sput-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_SEPARATED_PRINTOUTS:Ljava/lang/Boolean;

    .line 5889
    sput-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_USE_PREDEFINED_TICKETS_T2:Ljava/lang/Boolean;

    .line 5891
    sput-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_RESILIENT_BUS:Ljava/lang/Boolean;

    .line 5893
    sput-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_FIRMWARE_UPDATE_JAIL_T2:Ljava/lang/Boolean;

    .line 5895
    sput-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_HARDWARE_SECURE_TOUCH:Ljava/lang/Boolean;

    .line 5897
    sput-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_CAN_CLOSE_REMOTE_CASH_DRAWERS_ANDROID:Ljava/lang/Boolean;

    .line 5899
    sput-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_WAIT_FOR_BRAN_X2:Ljava/lang/Boolean;

    .line 5901
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_SKYHOOK_INTEGRATION_V2:Ljava/lang/Boolean;

    .line 5903
    sput-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_SKYHOOK_X2_USE_24_HOUR_CACHE_LIFESPAN:Ljava/lang/Boolean;

    .line 5905
    sput-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_SKYHOOK_T2_USE_3_HOUR_CHECK_INTERVAL:Ljava/lang/Boolean;

    .line 5907
    sput-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_SKYHOOK_T2_USE_6_HOUR_CACHE_LIFESPAN:Ljava/lang/Boolean;

    .line 5909
    sput-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_SKYHOOK_V2_ONLY_USE_IP_LOCATION_WITH_NO_WPS:Ljava/lang/Boolean;

    .line 5911
    sput-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_BUYER_CHECKOUT_DISPLAY_TRANSACTION_TYPE:Ljava/lang/Boolean;

    .line 5913
    sput-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_RECEIPTS_PRINT_DISPOSITION:Ljava/lang/Boolean;

    .line 5915
    sput-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_ORDER_ENTRY_LIBRARY_CREATE_NEW_ITEM:Ljava/lang/Boolean;

    .line 5917
    sput-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_SHOW_FEE_BREAKDOWN_TABLE:Ljava/lang/Boolean;

    .line 5919
    sput-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_ORDER_ENTRY_LIBRARY_HIDE_GIFTCARDS_REWARDS_IF_NO_ITEMS:Ljava/lang/Boolean;

    .line 5921
    sput-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_USE_ORDER_ENTRY_SCREEN_V2_ANDROID:Ljava/lang/Boolean;

    .line 5923
    sput-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_ORDER_ENTRY_LIBRARY_ITEM_SUGGESTIONS:Ljava/lang/Boolean;

    .line 5925
    sput-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_CAN_PRINT_SINGLE_TICKET_PER_ITEM:Ljava/lang/Boolean;

    .line 5927
    sput-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_HARDWARE_SECURE_TOUCH_ACCESSIBILITY_MODE:Ljava/lang/Boolean;

    .line 5929
    sput-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_ENHANCED_TRANSACTION_SEARCH_ANDROID:Ljava/lang/Boolean;

    .line 5931
    sput-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_DELETE_SESSION_TOKEN_PLAINTEXT:Ljava/lang/Boolean;

    .line 5933
    sput-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_ENABLE_VERBOSE_LOGIN_RESPONSE_CACHE_LOGGING:Ljava/lang/Boolean;

    .line 5935
    sput-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_USE_SESSION_TOKEN_ENCRYPTION_ANDROID_V2:Ljava/lang/Boolean;

    .line 5937
    sput-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_HARDWARE_SECURE_TOUCH_HIGH_CONTRAST_MODE:Ljava/lang/Boolean;

    .line 5939
    sput-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_CHECKOUT_APPLET_V2_ANDROID:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;Lokio/ByteString;)V
    .locals 1

    .line 6414
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 6415
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->bundle_logging:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->bundle_logging:Ljava/lang/Boolean;

    .line 6416
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_pre_tax_tipping_android:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_pre_tax_tipping_android:Ljava/lang/Boolean;

    .line 6417
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_api_url_list_android:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_api_url_list_android:Ljava/lang/Boolean;

    .line 6418
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->ignore_pii_accessibility_scrubber_android:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->ignore_pii_accessibility_scrubber_android:Ljava/lang/Boolean;

    .line 6419
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->upload_x2_wifi_events:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->upload_x2_wifi_events:Ljava/lang/Boolean;

    .line 6420
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->loyalty_checkout_x2:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->loyalty_checkout_x2:Ljava/lang/Boolean;

    .line 6421
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_clock_skew_lockout_android:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_clock_skew_lockout_android:Ljava/lang/Boolean;

    .line 6422
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_jedi_help_applet:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_jedi_help_applet:Ljava/lang/Boolean;

    .line 6423
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_persistent_bundle_android:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_persistent_bundle_android:Ljava/lang/Boolean;

    .line 6424
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->dip_tap_to_create_open_ticket_with_name:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->dip_tap_to_create_open_ticket_with_name:Ljava/lang/Boolean;

    .line 6425
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_session_token_encryption_android:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_session_token_encryption_android:Ljava/lang/Boolean;

    .line 6426
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_jedi_help_applet_x2:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_jedi_help_applet_x2:Ljava/lang/Boolean;

    .line 6427
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->upload_ledger_and_diagnostics_x2:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->upload_ledger_and_diagnostics_x2:Ljava/lang/Boolean;

    .line 6428
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->print_itemized_discounts:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->print_itemized_discounts:Ljava/lang/Boolean;

    .line 6429
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_feature_tour_x2:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_feature_tour_x2:Ljava/lang/Boolean;

    .line 6430
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->printer_debugging:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->printer_debugging:Ljava/lang/Boolean;

    .line 6431
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->restart_app_after_crash:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->restart_app_after_crash:Ljava/lang/Boolean;

    .line 6432
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->intercept_magswipe_events_during_printing_t2:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->intercept_magswipe_events_during_printing_t2:Ljava/lang/Boolean;

    .line 6433
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_device_settings_android:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_device_settings_android:Ljava/lang/Boolean;

    .line 6434
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_jedi_help_applet_t2:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_jedi_help_applet_t2:Ljava/lang/Boolean;

    .line 6435
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->firmware_update_t2:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->firmware_update_t2:Ljava/lang/Boolean;

    .line 6436
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->separated_printouts:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->separated_printouts:Ljava/lang/Boolean;

    .line 6437
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_predefined_tickets_t2:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_predefined_tickets_t2:Ljava/lang/Boolean;

    .line 6438
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->resilient_bus:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->resilient_bus:Ljava/lang/Boolean;

    .line 6439
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->firmware_update_jail_t2:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->firmware_update_jail_t2:Ljava/lang/Boolean;

    .line 6440
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->hardware_secure_touch:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->hardware_secure_touch:Ljava/lang/Boolean;

    .line 6441
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->can_close_remote_cash_drawers_android:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->can_close_remote_cash_drawers_android:Ljava/lang/Boolean;

    .line 6442
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->wait_for_bran_x2:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->wait_for_bran_x2:Ljava/lang/Boolean;

    .line 6443
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->skyhook_integration_v2:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->skyhook_integration_v2:Ljava/lang/Boolean;

    .line 6444
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->skyhook_x2_use_24_hour_cache_lifespan:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->skyhook_x2_use_24_hour_cache_lifespan:Ljava/lang/Boolean;

    .line 6445
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->skyhook_t2_use_3_hour_check_interval:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->skyhook_t2_use_3_hour_check_interval:Ljava/lang/Boolean;

    .line 6446
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->skyhook_t2_use_6_hour_cache_lifespan:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->skyhook_t2_use_6_hour_cache_lifespan:Ljava/lang/Boolean;

    .line 6447
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->skyhook_v2_only_use_ip_location_with_no_wps:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->skyhook_v2_only_use_ip_location_with_no_wps:Ljava/lang/Boolean;

    .line 6448
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->buyer_checkout_display_transaction_type:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->buyer_checkout_display_transaction_type:Ljava/lang/Boolean;

    .line 6449
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->receipts_print_disposition:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->receipts_print_disposition:Ljava/lang/Boolean;

    .line 6450
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->order_entry_library_create_new_item:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->order_entry_library_create_new_item:Ljava/lang/Boolean;

    .line 6451
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->show_fee_breakdown_table:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->show_fee_breakdown_table:Ljava/lang/Boolean;

    .line 6452
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->order_entry_library_hide_giftcards_rewards_if_no_items:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->order_entry_library_hide_giftcards_rewards_if_no_items:Ljava/lang/Boolean;

    .line 6453
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_order_entry_screen_v2_android:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_order_entry_screen_v2_android:Ljava/lang/Boolean;

    .line 6454
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->order_entry_library_item_suggestions:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->order_entry_library_item_suggestions:Ljava/lang/Boolean;

    .line 6455
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->can_print_single_ticket_per_item:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->can_print_single_ticket_per_item:Ljava/lang/Boolean;

    .line 6456
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->hardware_secure_touch_accessibility_mode:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->hardware_secure_touch_accessibility_mode:Ljava/lang/Boolean;

    .line 6457
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->enhanced_transaction_search_android:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->enhanced_transaction_search_android:Ljava/lang/Boolean;

    .line 6458
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->delete_session_token_plaintext:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->delete_session_token_plaintext:Ljava/lang/Boolean;

    .line 6459
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->enable_verbose_login_response_cache_logging:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->enable_verbose_login_response_cache_logging:Ljava/lang/Boolean;

    .line 6460
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_session_token_encryption_android_v2:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_session_token_encryption_android_v2:Ljava/lang/Boolean;

    .line 6461
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->hardware_secure_touch_high_contrast_mode:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->hardware_secure_touch_high_contrast_mode:Ljava/lang/Boolean;

    .line 6462
    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->checkout_applet_v2_android:Ljava/lang/Boolean;

    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->checkout_applet_v2_android:Ljava/lang/Boolean;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 6797
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 6523
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 6524
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    .line 6525
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->bundle_logging:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->bundle_logging:Ljava/lang/Boolean;

    .line 6526
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_pre_tax_tipping_android:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_pre_tax_tipping_android:Ljava/lang/Boolean;

    .line 6527
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_api_url_list_android:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_api_url_list_android:Ljava/lang/Boolean;

    .line 6528
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->ignore_pii_accessibility_scrubber_android:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->ignore_pii_accessibility_scrubber_android:Ljava/lang/Boolean;

    .line 6529
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->upload_x2_wifi_events:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->upload_x2_wifi_events:Ljava/lang/Boolean;

    .line 6530
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->loyalty_checkout_x2:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->loyalty_checkout_x2:Ljava/lang/Boolean;

    .line 6531
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_clock_skew_lockout_android:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_clock_skew_lockout_android:Ljava/lang/Boolean;

    .line 6532
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_jedi_help_applet:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_jedi_help_applet:Ljava/lang/Boolean;

    .line 6533
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_persistent_bundle_android:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_persistent_bundle_android:Ljava/lang/Boolean;

    .line 6534
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->dip_tap_to_create_open_ticket_with_name:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->dip_tap_to_create_open_ticket_with_name:Ljava/lang/Boolean;

    .line 6535
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_session_token_encryption_android:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_session_token_encryption_android:Ljava/lang/Boolean;

    .line 6536
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_jedi_help_applet_x2:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_jedi_help_applet_x2:Ljava/lang/Boolean;

    .line 6537
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->upload_ledger_and_diagnostics_x2:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->upload_ledger_and_diagnostics_x2:Ljava/lang/Boolean;

    .line 6538
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->print_itemized_discounts:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->print_itemized_discounts:Ljava/lang/Boolean;

    .line 6539
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_feature_tour_x2:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_feature_tour_x2:Ljava/lang/Boolean;

    .line 6540
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->printer_debugging:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->printer_debugging:Ljava/lang/Boolean;

    .line 6541
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->restart_app_after_crash:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->restart_app_after_crash:Ljava/lang/Boolean;

    .line 6542
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->intercept_magswipe_events_during_printing_t2:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->intercept_magswipe_events_during_printing_t2:Ljava/lang/Boolean;

    .line 6543
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_device_settings_android:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_device_settings_android:Ljava/lang/Boolean;

    .line 6544
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_jedi_help_applet_t2:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_jedi_help_applet_t2:Ljava/lang/Boolean;

    .line 6545
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->firmware_update_t2:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->firmware_update_t2:Ljava/lang/Boolean;

    .line 6546
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->separated_printouts:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->separated_printouts:Ljava/lang/Boolean;

    .line 6547
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_predefined_tickets_t2:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_predefined_tickets_t2:Ljava/lang/Boolean;

    .line 6548
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->resilient_bus:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->resilient_bus:Ljava/lang/Boolean;

    .line 6549
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->firmware_update_jail_t2:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->firmware_update_jail_t2:Ljava/lang/Boolean;

    .line 6550
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->hardware_secure_touch:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->hardware_secure_touch:Ljava/lang/Boolean;

    .line 6551
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->can_close_remote_cash_drawers_android:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->can_close_remote_cash_drawers_android:Ljava/lang/Boolean;

    .line 6552
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->wait_for_bran_x2:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->wait_for_bran_x2:Ljava/lang/Boolean;

    .line 6553
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->skyhook_integration_v2:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->skyhook_integration_v2:Ljava/lang/Boolean;

    .line 6554
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->skyhook_x2_use_24_hour_cache_lifespan:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->skyhook_x2_use_24_hour_cache_lifespan:Ljava/lang/Boolean;

    .line 6555
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->skyhook_t2_use_3_hour_check_interval:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->skyhook_t2_use_3_hour_check_interval:Ljava/lang/Boolean;

    .line 6556
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->skyhook_t2_use_6_hour_cache_lifespan:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->skyhook_t2_use_6_hour_cache_lifespan:Ljava/lang/Boolean;

    .line 6557
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->skyhook_v2_only_use_ip_location_with_no_wps:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->skyhook_v2_only_use_ip_location_with_no_wps:Ljava/lang/Boolean;

    .line 6558
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->buyer_checkout_display_transaction_type:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->buyer_checkout_display_transaction_type:Ljava/lang/Boolean;

    .line 6559
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->receipts_print_disposition:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->receipts_print_disposition:Ljava/lang/Boolean;

    .line 6560
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->order_entry_library_create_new_item:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->order_entry_library_create_new_item:Ljava/lang/Boolean;

    .line 6561
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->show_fee_breakdown_table:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->show_fee_breakdown_table:Ljava/lang/Boolean;

    .line 6562
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->order_entry_library_hide_giftcards_rewards_if_no_items:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->order_entry_library_hide_giftcards_rewards_if_no_items:Ljava/lang/Boolean;

    .line 6563
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_order_entry_screen_v2_android:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_order_entry_screen_v2_android:Ljava/lang/Boolean;

    .line 6564
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->order_entry_library_item_suggestions:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->order_entry_library_item_suggestions:Ljava/lang/Boolean;

    .line 6565
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->can_print_single_ticket_per_item:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->can_print_single_ticket_per_item:Ljava/lang/Boolean;

    .line 6566
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->hardware_secure_touch_accessibility_mode:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->hardware_secure_touch_accessibility_mode:Ljava/lang/Boolean;

    .line 6567
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->enhanced_transaction_search_android:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->enhanced_transaction_search_android:Ljava/lang/Boolean;

    .line 6568
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->delete_session_token_plaintext:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->delete_session_token_plaintext:Ljava/lang/Boolean;

    .line 6569
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->enable_verbose_login_response_cache_logging:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->enable_verbose_login_response_cache_logging:Ljava/lang/Boolean;

    .line 6570
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_session_token_encryption_android_v2:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_session_token_encryption_android_v2:Ljava/lang/Boolean;

    .line 6571
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->hardware_secure_touch_high_contrast_mode:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->hardware_secure_touch_high_contrast_mode:Ljava/lang/Boolean;

    .line 6572
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->checkout_applet_v2_android:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->checkout_applet_v2_android:Ljava/lang/Boolean;

    .line 6573
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 6578
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_30

    .line 6580
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 6581
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->bundle_logging:Ljava/lang/Boolean;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6582
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_pre_tax_tipping_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6583
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_api_url_list_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6584
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->ignore_pii_accessibility_scrubber_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6585
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->upload_x2_wifi_events:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6586
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->loyalty_checkout_x2:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6587
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_clock_skew_lockout_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6588
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_jedi_help_applet:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6589
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_persistent_bundle_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6590
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->dip_tap_to_create_open_ticket_with_name:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6591
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_session_token_encryption_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6592
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_jedi_help_applet_x2:Ljava/lang/Boolean;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6593
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->upload_ledger_and_diagnostics_x2:Ljava/lang/Boolean;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_c

    :cond_c
    const/4 v1, 0x0

    :goto_c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6594
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->print_itemized_discounts:Ljava/lang/Boolean;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_d

    :cond_d
    const/4 v1, 0x0

    :goto_d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6595
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_feature_tour_x2:Ljava/lang/Boolean;

    if-eqz v1, :cond_e

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_e

    :cond_e
    const/4 v1, 0x0

    :goto_e
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6596
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->printer_debugging:Ljava/lang/Boolean;

    if-eqz v1, :cond_f

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_f

    :cond_f
    const/4 v1, 0x0

    :goto_f
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6597
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->restart_app_after_crash:Ljava/lang/Boolean;

    if-eqz v1, :cond_10

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_10

    :cond_10
    const/4 v1, 0x0

    :goto_10
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6598
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->intercept_magswipe_events_during_printing_t2:Ljava/lang/Boolean;

    if-eqz v1, :cond_11

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_11

    :cond_11
    const/4 v1, 0x0

    :goto_11
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6599
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_device_settings_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_12

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_12

    :cond_12
    const/4 v1, 0x0

    :goto_12
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6600
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_jedi_help_applet_t2:Ljava/lang/Boolean;

    if-eqz v1, :cond_13

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_13

    :cond_13
    const/4 v1, 0x0

    :goto_13
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6601
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->firmware_update_t2:Ljava/lang/Boolean;

    if-eqz v1, :cond_14

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_14

    :cond_14
    const/4 v1, 0x0

    :goto_14
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6602
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->separated_printouts:Ljava/lang/Boolean;

    if-eqz v1, :cond_15

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_15

    :cond_15
    const/4 v1, 0x0

    :goto_15
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6603
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_predefined_tickets_t2:Ljava/lang/Boolean;

    if-eqz v1, :cond_16

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_16

    :cond_16
    const/4 v1, 0x0

    :goto_16
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6604
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->resilient_bus:Ljava/lang/Boolean;

    if-eqz v1, :cond_17

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_17

    :cond_17
    const/4 v1, 0x0

    :goto_17
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6605
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->firmware_update_jail_t2:Ljava/lang/Boolean;

    if-eqz v1, :cond_18

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_18

    :cond_18
    const/4 v1, 0x0

    :goto_18
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6606
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->hardware_secure_touch:Ljava/lang/Boolean;

    if-eqz v1, :cond_19

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_19

    :cond_19
    const/4 v1, 0x0

    :goto_19
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6607
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->can_close_remote_cash_drawers_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_1a

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1a

    :cond_1a
    const/4 v1, 0x0

    :goto_1a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6608
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->wait_for_bran_x2:Ljava/lang/Boolean;

    if-eqz v1, :cond_1b

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1b

    :cond_1b
    const/4 v1, 0x0

    :goto_1b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6609
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->skyhook_integration_v2:Ljava/lang/Boolean;

    if-eqz v1, :cond_1c

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1c

    :cond_1c
    const/4 v1, 0x0

    :goto_1c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6610
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->skyhook_x2_use_24_hour_cache_lifespan:Ljava/lang/Boolean;

    if-eqz v1, :cond_1d

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1d

    :cond_1d
    const/4 v1, 0x0

    :goto_1d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6611
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->skyhook_t2_use_3_hour_check_interval:Ljava/lang/Boolean;

    if-eqz v1, :cond_1e

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1e

    :cond_1e
    const/4 v1, 0x0

    :goto_1e
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6612
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->skyhook_t2_use_6_hour_cache_lifespan:Ljava/lang/Boolean;

    if-eqz v1, :cond_1f

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1f

    :cond_1f
    const/4 v1, 0x0

    :goto_1f
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6613
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->skyhook_v2_only_use_ip_location_with_no_wps:Ljava/lang/Boolean;

    if-eqz v1, :cond_20

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_20

    :cond_20
    const/4 v1, 0x0

    :goto_20
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6614
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->buyer_checkout_display_transaction_type:Ljava/lang/Boolean;

    if-eqz v1, :cond_21

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_21

    :cond_21
    const/4 v1, 0x0

    :goto_21
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6615
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->receipts_print_disposition:Ljava/lang/Boolean;

    if-eqz v1, :cond_22

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_22

    :cond_22
    const/4 v1, 0x0

    :goto_22
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6616
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->order_entry_library_create_new_item:Ljava/lang/Boolean;

    if-eqz v1, :cond_23

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_23

    :cond_23
    const/4 v1, 0x0

    :goto_23
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6617
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->show_fee_breakdown_table:Ljava/lang/Boolean;

    if-eqz v1, :cond_24

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_24

    :cond_24
    const/4 v1, 0x0

    :goto_24
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6618
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->order_entry_library_hide_giftcards_rewards_if_no_items:Ljava/lang/Boolean;

    if-eqz v1, :cond_25

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_25

    :cond_25
    const/4 v1, 0x0

    :goto_25
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6619
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_order_entry_screen_v2_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_26

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_26

    :cond_26
    const/4 v1, 0x0

    :goto_26
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6620
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->order_entry_library_item_suggestions:Ljava/lang/Boolean;

    if-eqz v1, :cond_27

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_27

    :cond_27
    const/4 v1, 0x0

    :goto_27
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6621
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->can_print_single_ticket_per_item:Ljava/lang/Boolean;

    if-eqz v1, :cond_28

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_28

    :cond_28
    const/4 v1, 0x0

    :goto_28
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6622
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->hardware_secure_touch_accessibility_mode:Ljava/lang/Boolean;

    if-eqz v1, :cond_29

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_29

    :cond_29
    const/4 v1, 0x0

    :goto_29
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6623
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->enhanced_transaction_search_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_2a

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2a

    :cond_2a
    const/4 v1, 0x0

    :goto_2a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6624
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->delete_session_token_plaintext:Ljava/lang/Boolean;

    if-eqz v1, :cond_2b

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2b

    :cond_2b
    const/4 v1, 0x0

    :goto_2b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6625
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->enable_verbose_login_response_cache_logging:Ljava/lang/Boolean;

    if-eqz v1, :cond_2c

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2c

    :cond_2c
    const/4 v1, 0x0

    :goto_2c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6626
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_session_token_encryption_android_v2:Ljava/lang/Boolean;

    if-eqz v1, :cond_2d

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2d

    :cond_2d
    const/4 v1, 0x0

    :goto_2d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6627
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->hardware_secure_touch_high_contrast_mode:Ljava/lang/Boolean;

    if-eqz v1, :cond_2e

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2e

    :cond_2e
    const/4 v1, 0x0

    :goto_2e
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6628
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->checkout_applet_v2_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_2f

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_2f
    add-int/2addr v0, v2

    .line 6629
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_30
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;
    .locals 2

    .line 6467
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;-><init>()V

    .line 6468
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->bundle_logging:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->bundle_logging:Ljava/lang/Boolean;

    .line 6469
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_pre_tax_tipping_android:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_pre_tax_tipping_android:Ljava/lang/Boolean;

    .line 6470
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_api_url_list_android:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_api_url_list_android:Ljava/lang/Boolean;

    .line 6471
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->ignore_pii_accessibility_scrubber_android:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->ignore_pii_accessibility_scrubber_android:Ljava/lang/Boolean;

    .line 6472
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->upload_x2_wifi_events:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->upload_x2_wifi_events:Ljava/lang/Boolean;

    .line 6473
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->loyalty_checkout_x2:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->loyalty_checkout_x2:Ljava/lang/Boolean;

    .line 6474
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_clock_skew_lockout_android:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_clock_skew_lockout_android:Ljava/lang/Boolean;

    .line 6475
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_jedi_help_applet:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_jedi_help_applet:Ljava/lang/Boolean;

    .line 6476
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_persistent_bundle_android:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_persistent_bundle_android:Ljava/lang/Boolean;

    .line 6477
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->dip_tap_to_create_open_ticket_with_name:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->dip_tap_to_create_open_ticket_with_name:Ljava/lang/Boolean;

    .line 6478
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_session_token_encryption_android:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_session_token_encryption_android:Ljava/lang/Boolean;

    .line 6479
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_jedi_help_applet_x2:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_jedi_help_applet_x2:Ljava/lang/Boolean;

    .line 6480
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->upload_ledger_and_diagnostics_x2:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->upload_ledger_and_diagnostics_x2:Ljava/lang/Boolean;

    .line 6481
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->print_itemized_discounts:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->print_itemized_discounts:Ljava/lang/Boolean;

    .line 6482
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_feature_tour_x2:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_feature_tour_x2:Ljava/lang/Boolean;

    .line 6483
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->printer_debugging:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->printer_debugging:Ljava/lang/Boolean;

    .line 6484
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->restart_app_after_crash:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->restart_app_after_crash:Ljava/lang/Boolean;

    .line 6485
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->intercept_magswipe_events_during_printing_t2:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->intercept_magswipe_events_during_printing_t2:Ljava/lang/Boolean;

    .line 6486
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_device_settings_android:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_device_settings_android:Ljava/lang/Boolean;

    .line 6487
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_jedi_help_applet_t2:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_jedi_help_applet_t2:Ljava/lang/Boolean;

    .line 6488
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->firmware_update_t2:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->firmware_update_t2:Ljava/lang/Boolean;

    .line 6489
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->separated_printouts:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->separated_printouts:Ljava/lang/Boolean;

    .line 6490
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_predefined_tickets_t2:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_predefined_tickets_t2:Ljava/lang/Boolean;

    .line 6491
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->resilient_bus:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->resilient_bus:Ljava/lang/Boolean;

    .line 6492
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->firmware_update_jail_t2:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->firmware_update_jail_t2:Ljava/lang/Boolean;

    .line 6493
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->hardware_secure_touch:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->hardware_secure_touch:Ljava/lang/Boolean;

    .line 6494
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->can_close_remote_cash_drawers_android:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->can_close_remote_cash_drawers_android:Ljava/lang/Boolean;

    .line 6495
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->wait_for_bran_x2:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->wait_for_bran_x2:Ljava/lang/Boolean;

    .line 6496
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->skyhook_integration_v2:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->skyhook_integration_v2:Ljava/lang/Boolean;

    .line 6497
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->skyhook_x2_use_24_hour_cache_lifespan:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->skyhook_x2_use_24_hour_cache_lifespan:Ljava/lang/Boolean;

    .line 6498
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->skyhook_t2_use_3_hour_check_interval:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->skyhook_t2_use_3_hour_check_interval:Ljava/lang/Boolean;

    .line 6499
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->skyhook_t2_use_6_hour_cache_lifespan:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->skyhook_t2_use_6_hour_cache_lifespan:Ljava/lang/Boolean;

    .line 6500
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->skyhook_v2_only_use_ip_location_with_no_wps:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->skyhook_v2_only_use_ip_location_with_no_wps:Ljava/lang/Boolean;

    .line 6501
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->buyer_checkout_display_transaction_type:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->buyer_checkout_display_transaction_type:Ljava/lang/Boolean;

    .line 6502
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->receipts_print_disposition:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->receipts_print_disposition:Ljava/lang/Boolean;

    .line 6503
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->order_entry_library_create_new_item:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->order_entry_library_create_new_item:Ljava/lang/Boolean;

    .line 6504
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->show_fee_breakdown_table:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->show_fee_breakdown_table:Ljava/lang/Boolean;

    .line 6505
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->order_entry_library_hide_giftcards_rewards_if_no_items:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->order_entry_library_hide_giftcards_rewards_if_no_items:Ljava/lang/Boolean;

    .line 6506
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_order_entry_screen_v2_android:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_order_entry_screen_v2_android:Ljava/lang/Boolean;

    .line 6507
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->order_entry_library_item_suggestions:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->order_entry_library_item_suggestions:Ljava/lang/Boolean;

    .line 6508
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->can_print_single_ticket_per_item:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->can_print_single_ticket_per_item:Ljava/lang/Boolean;

    .line 6509
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->hardware_secure_touch_accessibility_mode:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->hardware_secure_touch_accessibility_mode:Ljava/lang/Boolean;

    .line 6510
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->enhanced_transaction_search_android:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->enhanced_transaction_search_android:Ljava/lang/Boolean;

    .line 6511
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->delete_session_token_plaintext:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->delete_session_token_plaintext:Ljava/lang/Boolean;

    .line 6512
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->enable_verbose_login_response_cache_logging:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->enable_verbose_login_response_cache_logging:Ljava/lang/Boolean;

    .line 6513
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_session_token_encryption_android_v2:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_session_token_encryption_android_v2:Ljava/lang/Boolean;

    .line 6514
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->hardware_secure_touch_high_contrast_mode:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->hardware_secure_touch_high_contrast_mode:Ljava/lang/Boolean;

    .line 6515
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->checkout_applet_v2_android:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->checkout_applet_v2_android:Ljava/lang/Boolean;

    .line 6516
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 5838
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;
    .locals 2

    .line 6745
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->bundle_logging:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->bundle_logging:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->bundle_logging(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6746
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_pre_tax_tipping_android:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_pre_tax_tipping_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_pre_tax_tipping_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6747
    :cond_1
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_api_url_list_android:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_api_url_list_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_api_url_list_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6748
    :cond_2
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->ignore_pii_accessibility_scrubber_android:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->ignore_pii_accessibility_scrubber_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->ignore_pii_accessibility_scrubber_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6749
    :cond_3
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->upload_x2_wifi_events:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->upload_x2_wifi_events:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->upload_x2_wifi_events(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6750
    :cond_4
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->loyalty_checkout_x2:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->loyalty_checkout_x2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->loyalty_checkout_x2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6751
    :cond_5
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_clock_skew_lockout_android:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_clock_skew_lockout_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_clock_skew_lockout_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6752
    :cond_6
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_jedi_help_applet:Ljava/lang/Boolean;

    if-eqz v0, :cond_7

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_jedi_help_applet:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_jedi_help_applet(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6753
    :cond_7
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_persistent_bundle_android:Ljava/lang/Boolean;

    if-eqz v0, :cond_8

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_persistent_bundle_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_persistent_bundle_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6754
    :cond_8
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->dip_tap_to_create_open_ticket_with_name:Ljava/lang/Boolean;

    if-eqz v0, :cond_9

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->dip_tap_to_create_open_ticket_with_name:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->dip_tap_to_create_open_ticket_with_name(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6755
    :cond_9
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_session_token_encryption_android:Ljava/lang/Boolean;

    if-eqz v0, :cond_a

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_session_token_encryption_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_session_token_encryption_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6756
    :cond_a
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_jedi_help_applet_x2:Ljava/lang/Boolean;

    if-eqz v0, :cond_b

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_jedi_help_applet_x2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_jedi_help_applet_x2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6757
    :cond_b
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->upload_ledger_and_diagnostics_x2:Ljava/lang/Boolean;

    if-eqz v0, :cond_c

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->upload_ledger_and_diagnostics_x2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->upload_ledger_and_diagnostics_x2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6758
    :cond_c
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->print_itemized_discounts:Ljava/lang/Boolean;

    if-eqz v0, :cond_d

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->print_itemized_discounts:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->print_itemized_discounts(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6759
    :cond_d
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_feature_tour_x2:Ljava/lang/Boolean;

    if-eqz v0, :cond_e

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_feature_tour_x2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_feature_tour_x2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6760
    :cond_e
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->printer_debugging:Ljava/lang/Boolean;

    if-eqz v0, :cond_f

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->printer_debugging:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->printer_debugging(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6761
    :cond_f
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->restart_app_after_crash:Ljava/lang/Boolean;

    if-eqz v0, :cond_10

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->restart_app_after_crash:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->restart_app_after_crash(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6762
    :cond_10
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->intercept_magswipe_events_during_printing_t2:Ljava/lang/Boolean;

    if-eqz v0, :cond_11

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->intercept_magswipe_events_during_printing_t2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->intercept_magswipe_events_during_printing_t2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6763
    :cond_11
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_device_settings_android:Ljava/lang/Boolean;

    if-eqz v0, :cond_12

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_device_settings_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_device_settings_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6764
    :cond_12
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_jedi_help_applet_t2:Ljava/lang/Boolean;

    if-eqz v0, :cond_13

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_jedi_help_applet_t2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_jedi_help_applet_t2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6765
    :cond_13
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->firmware_update_t2:Ljava/lang/Boolean;

    if-eqz v0, :cond_14

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->firmware_update_t2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->firmware_update_t2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6766
    :cond_14
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->separated_printouts:Ljava/lang/Boolean;

    if-eqz v0, :cond_15

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->separated_printouts:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->separated_printouts(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6767
    :cond_15
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_predefined_tickets_t2:Ljava/lang/Boolean;

    if-eqz v0, :cond_16

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_predefined_tickets_t2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_predefined_tickets_t2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6768
    :cond_16
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->resilient_bus:Ljava/lang/Boolean;

    if-eqz v0, :cond_17

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->resilient_bus:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->resilient_bus(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6769
    :cond_17
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->firmware_update_jail_t2:Ljava/lang/Boolean;

    if-eqz v0, :cond_18

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->firmware_update_jail_t2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->firmware_update_jail_t2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6770
    :cond_18
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->hardware_secure_touch:Ljava/lang/Boolean;

    if-eqz v0, :cond_19

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->hardware_secure_touch:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->hardware_secure_touch(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6771
    :cond_19
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->can_close_remote_cash_drawers_android:Ljava/lang/Boolean;

    if-eqz v0, :cond_1a

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->can_close_remote_cash_drawers_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->can_close_remote_cash_drawers_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6772
    :cond_1a
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->wait_for_bran_x2:Ljava/lang/Boolean;

    if-eqz v0, :cond_1b

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->wait_for_bran_x2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->wait_for_bran_x2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6773
    :cond_1b
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->skyhook_integration_v2:Ljava/lang/Boolean;

    if-eqz v0, :cond_1c

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->skyhook_integration_v2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->skyhook_integration_v2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6774
    :cond_1c
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->skyhook_x2_use_24_hour_cache_lifespan:Ljava/lang/Boolean;

    if-eqz v0, :cond_1d

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->skyhook_x2_use_24_hour_cache_lifespan:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->skyhook_x2_use_24_hour_cache_lifespan(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6775
    :cond_1d
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->skyhook_t2_use_3_hour_check_interval:Ljava/lang/Boolean;

    if-eqz v0, :cond_1e

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->skyhook_t2_use_3_hour_check_interval:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->skyhook_t2_use_3_hour_check_interval(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6776
    :cond_1e
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->skyhook_t2_use_6_hour_cache_lifespan:Ljava/lang/Boolean;

    if-eqz v0, :cond_1f

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->skyhook_t2_use_6_hour_cache_lifespan:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->skyhook_t2_use_6_hour_cache_lifespan(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6777
    :cond_1f
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->skyhook_v2_only_use_ip_location_with_no_wps:Ljava/lang/Boolean;

    if-eqz v0, :cond_20

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->skyhook_v2_only_use_ip_location_with_no_wps:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->skyhook_v2_only_use_ip_location_with_no_wps(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6778
    :cond_20
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->buyer_checkout_display_transaction_type:Ljava/lang/Boolean;

    if-eqz v0, :cond_21

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->buyer_checkout_display_transaction_type:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->buyer_checkout_display_transaction_type(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6779
    :cond_21
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->receipts_print_disposition:Ljava/lang/Boolean;

    if-eqz v0, :cond_22

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->receipts_print_disposition:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->receipts_print_disposition(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6780
    :cond_22
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->order_entry_library_create_new_item:Ljava/lang/Boolean;

    if-eqz v0, :cond_23

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->order_entry_library_create_new_item:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->order_entry_library_create_new_item(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6781
    :cond_23
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->show_fee_breakdown_table:Ljava/lang/Boolean;

    if-eqz v0, :cond_24

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->show_fee_breakdown_table:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->show_fee_breakdown_table(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6782
    :cond_24
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->order_entry_library_hide_giftcards_rewards_if_no_items:Ljava/lang/Boolean;

    if-eqz v0, :cond_25

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->order_entry_library_hide_giftcards_rewards_if_no_items:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->order_entry_library_hide_giftcards_rewards_if_no_items(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6783
    :cond_25
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_order_entry_screen_v2_android:Ljava/lang/Boolean;

    if-eqz v0, :cond_26

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_order_entry_screen_v2_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_order_entry_screen_v2_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6784
    :cond_26
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->order_entry_library_item_suggestions:Ljava/lang/Boolean;

    if-eqz v0, :cond_27

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->order_entry_library_item_suggestions:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->order_entry_library_item_suggestions(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6785
    :cond_27
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->can_print_single_ticket_per_item:Ljava/lang/Boolean;

    if-eqz v0, :cond_28

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->can_print_single_ticket_per_item:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->can_print_single_ticket_per_item(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6786
    :cond_28
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->hardware_secure_touch_accessibility_mode:Ljava/lang/Boolean;

    if-eqz v0, :cond_29

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->hardware_secure_touch_accessibility_mode:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->hardware_secure_touch_accessibility_mode(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6787
    :cond_29
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->enhanced_transaction_search_android:Ljava/lang/Boolean;

    if-eqz v0, :cond_2a

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->enhanced_transaction_search_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->enhanced_transaction_search_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6788
    :cond_2a
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->delete_session_token_plaintext:Ljava/lang/Boolean;

    if-eqz v0, :cond_2b

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->delete_session_token_plaintext:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->delete_session_token_plaintext(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6789
    :cond_2b
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->enable_verbose_login_response_cache_logging:Ljava/lang/Boolean;

    if-eqz v0, :cond_2c

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->enable_verbose_login_response_cache_logging:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->enable_verbose_login_response_cache_logging(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6790
    :cond_2c
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_session_token_encryption_android_v2:Ljava/lang/Boolean;

    if-eqz v0, :cond_2d

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_session_token_encryption_android_v2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_session_token_encryption_android_v2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6791
    :cond_2d
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->hardware_secure_touch_high_contrast_mode:Ljava/lang/Boolean;

    if-eqz v0, :cond_2e

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->hardware_secure_touch_high_contrast_mode:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->hardware_secure_touch_high_contrast_mode(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6792
    :cond_2e
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->checkout_applet_v2_android:Ljava/lang/Boolean;

    if-eqz v0, :cond_2f

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->checkout_applet_v2_android:Ljava/lang/Boolean;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->checkout_applet_v2_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    :cond_2f
    if-nez v1, :cond_30

    move-object p1, p0

    goto :goto_0

    .line 6793
    :cond_30
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 5838
    check-cast p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->overlay(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;
    .locals 2

    .line 6691
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->bundle_logging:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_BUNDLE_LOGGING:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->bundle_logging(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6692
    :cond_0
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_pre_tax_tipping_android:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_USE_PRE_TAX_TIPPING_ANDROID:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_pre_tax_tipping_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6693
    :cond_1
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_api_url_list_android:Ljava/lang/Boolean;

    if-nez v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_USE_API_URL_LIST_ANDROID:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_api_url_list_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6694
    :cond_2
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->ignore_pii_accessibility_scrubber_android:Ljava/lang/Boolean;

    if-nez v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_IGNORE_PII_ACCESSIBILITY_SCRUBBER_ANDROID:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->ignore_pii_accessibility_scrubber_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6695
    :cond_3
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->upload_x2_wifi_events:Ljava/lang/Boolean;

    if-nez v0, :cond_4

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_UPLOAD_X2_WIFI_EVENTS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->upload_x2_wifi_events(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6696
    :cond_4
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->loyalty_checkout_x2:Ljava/lang/Boolean;

    if-nez v0, :cond_5

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_LOYALTY_CHECKOUT_X2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->loyalty_checkout_x2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6697
    :cond_5
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_clock_skew_lockout_android:Ljava/lang/Boolean;

    if-nez v0, :cond_6

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_USE_CLOCK_SKEW_LOCKOUT_ANDROID:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_clock_skew_lockout_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6698
    :cond_6
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_jedi_help_applet:Ljava/lang/Boolean;

    if-nez v0, :cond_7

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_USE_JEDI_HELP_APPLET:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_jedi_help_applet(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6699
    :cond_7
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_persistent_bundle_android:Ljava/lang/Boolean;

    if-nez v0, :cond_8

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_USE_PERSISTENT_BUNDLE_ANDROID:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_persistent_bundle_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6700
    :cond_8
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->dip_tap_to_create_open_ticket_with_name:Ljava/lang/Boolean;

    if-nez v0, :cond_9

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_DIP_TAP_TO_CREATE_OPEN_TICKET_WITH_NAME:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->dip_tap_to_create_open_ticket_with_name(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6701
    :cond_9
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_session_token_encryption_android:Ljava/lang/Boolean;

    if-nez v0, :cond_a

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_USE_SESSION_TOKEN_ENCRYPTION_ANDROID:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_session_token_encryption_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6702
    :cond_a
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_jedi_help_applet_x2:Ljava/lang/Boolean;

    if-nez v0, :cond_b

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_USE_JEDI_HELP_APPLET_X2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_jedi_help_applet_x2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6703
    :cond_b
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->upload_ledger_and_diagnostics_x2:Ljava/lang/Boolean;

    if-nez v0, :cond_c

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_UPLOAD_LEDGER_AND_DIAGNOSTICS_X2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->upload_ledger_and_diagnostics_x2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6704
    :cond_c
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->print_itemized_discounts:Ljava/lang/Boolean;

    if-nez v0, :cond_d

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_PRINT_ITEMIZED_DISCOUNTS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->print_itemized_discounts(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6705
    :cond_d
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_feature_tour_x2:Ljava/lang/Boolean;

    if-nez v0, :cond_e

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_USE_FEATURE_TOUR_X2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_feature_tour_x2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6706
    :cond_e
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->printer_debugging:Ljava/lang/Boolean;

    if-nez v0, :cond_f

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_PRINTER_DEBUGGING:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->printer_debugging(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6707
    :cond_f
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->restart_app_after_crash:Ljava/lang/Boolean;

    if-nez v0, :cond_10

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_RESTART_APP_AFTER_CRASH:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->restart_app_after_crash(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6708
    :cond_10
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->intercept_magswipe_events_during_printing_t2:Ljava/lang/Boolean;

    if-nez v0, :cond_11

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_INTERCEPT_MAGSWIPE_EVENTS_DURING_PRINTING_T2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->intercept_magswipe_events_during_printing_t2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6709
    :cond_11
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_device_settings_android:Ljava/lang/Boolean;

    if-nez v0, :cond_12

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_USE_DEVICE_SETTINGS_ANDROID:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_device_settings_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6710
    :cond_12
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_jedi_help_applet_t2:Ljava/lang/Boolean;

    if-nez v0, :cond_13

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_USE_JEDI_HELP_APPLET_T2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_jedi_help_applet_t2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6711
    :cond_13
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->firmware_update_t2:Ljava/lang/Boolean;

    if-nez v0, :cond_14

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_FIRMWARE_UPDATE_T2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->firmware_update_t2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6712
    :cond_14
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->separated_printouts:Ljava/lang/Boolean;

    if-nez v0, :cond_15

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_SEPARATED_PRINTOUTS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->separated_printouts(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6713
    :cond_15
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_predefined_tickets_t2:Ljava/lang/Boolean;

    if-nez v0, :cond_16

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_USE_PREDEFINED_TICKETS_T2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_predefined_tickets_t2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6714
    :cond_16
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->resilient_bus:Ljava/lang/Boolean;

    if-nez v0, :cond_17

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_RESILIENT_BUS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->resilient_bus(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6715
    :cond_17
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->firmware_update_jail_t2:Ljava/lang/Boolean;

    if-nez v0, :cond_18

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_FIRMWARE_UPDATE_JAIL_T2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->firmware_update_jail_t2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6716
    :cond_18
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->hardware_secure_touch:Ljava/lang/Boolean;

    if-nez v0, :cond_19

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_HARDWARE_SECURE_TOUCH:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->hardware_secure_touch(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6717
    :cond_19
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->can_close_remote_cash_drawers_android:Ljava/lang/Boolean;

    if-nez v0, :cond_1a

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_CAN_CLOSE_REMOTE_CASH_DRAWERS_ANDROID:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->can_close_remote_cash_drawers_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6718
    :cond_1a
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->wait_for_bran_x2:Ljava/lang/Boolean;

    if-nez v0, :cond_1b

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_WAIT_FOR_BRAN_X2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->wait_for_bran_x2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6719
    :cond_1b
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->skyhook_integration_v2:Ljava/lang/Boolean;

    if-nez v0, :cond_1c

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_SKYHOOK_INTEGRATION_V2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->skyhook_integration_v2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6720
    :cond_1c
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->skyhook_x2_use_24_hour_cache_lifespan:Ljava/lang/Boolean;

    if-nez v0, :cond_1d

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_SKYHOOK_X2_USE_24_HOUR_CACHE_LIFESPAN:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->skyhook_x2_use_24_hour_cache_lifespan(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6721
    :cond_1d
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->skyhook_t2_use_3_hour_check_interval:Ljava/lang/Boolean;

    if-nez v0, :cond_1e

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_SKYHOOK_T2_USE_3_HOUR_CHECK_INTERVAL:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->skyhook_t2_use_3_hour_check_interval(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6722
    :cond_1e
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->skyhook_t2_use_6_hour_cache_lifespan:Ljava/lang/Boolean;

    if-nez v0, :cond_1f

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_SKYHOOK_T2_USE_6_HOUR_CACHE_LIFESPAN:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->skyhook_t2_use_6_hour_cache_lifespan(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6723
    :cond_1f
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->skyhook_v2_only_use_ip_location_with_no_wps:Ljava/lang/Boolean;

    if-nez v0, :cond_20

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_SKYHOOK_V2_ONLY_USE_IP_LOCATION_WITH_NO_WPS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->skyhook_v2_only_use_ip_location_with_no_wps(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6724
    :cond_20
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->buyer_checkout_display_transaction_type:Ljava/lang/Boolean;

    if-nez v0, :cond_21

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_BUYER_CHECKOUT_DISPLAY_TRANSACTION_TYPE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->buyer_checkout_display_transaction_type(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6725
    :cond_21
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->receipts_print_disposition:Ljava/lang/Boolean;

    if-nez v0, :cond_22

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_RECEIPTS_PRINT_DISPOSITION:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->receipts_print_disposition(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6726
    :cond_22
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->order_entry_library_create_new_item:Ljava/lang/Boolean;

    if-nez v0, :cond_23

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_ORDER_ENTRY_LIBRARY_CREATE_NEW_ITEM:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->order_entry_library_create_new_item(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6727
    :cond_23
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->show_fee_breakdown_table:Ljava/lang/Boolean;

    if-nez v0, :cond_24

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_SHOW_FEE_BREAKDOWN_TABLE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->show_fee_breakdown_table(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6728
    :cond_24
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->order_entry_library_hide_giftcards_rewards_if_no_items:Ljava/lang/Boolean;

    if-nez v0, :cond_25

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_ORDER_ENTRY_LIBRARY_HIDE_GIFTCARDS_REWARDS_IF_NO_ITEMS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->order_entry_library_hide_giftcards_rewards_if_no_items(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6729
    :cond_25
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_order_entry_screen_v2_android:Ljava/lang/Boolean;

    if-nez v0, :cond_26

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_USE_ORDER_ENTRY_SCREEN_V2_ANDROID:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_order_entry_screen_v2_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6730
    :cond_26
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->order_entry_library_item_suggestions:Ljava/lang/Boolean;

    if-nez v0, :cond_27

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_ORDER_ENTRY_LIBRARY_ITEM_SUGGESTIONS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->order_entry_library_item_suggestions(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6731
    :cond_27
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->can_print_single_ticket_per_item:Ljava/lang/Boolean;

    if-nez v0, :cond_28

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_CAN_PRINT_SINGLE_TICKET_PER_ITEM:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->can_print_single_ticket_per_item(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6732
    :cond_28
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->hardware_secure_touch_accessibility_mode:Ljava/lang/Boolean;

    if-nez v0, :cond_29

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_HARDWARE_SECURE_TOUCH_ACCESSIBILITY_MODE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->hardware_secure_touch_accessibility_mode(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6733
    :cond_29
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->enhanced_transaction_search_android:Ljava/lang/Boolean;

    if-nez v0, :cond_2a

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_ENHANCED_TRANSACTION_SEARCH_ANDROID:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->enhanced_transaction_search_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6734
    :cond_2a
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->delete_session_token_plaintext:Ljava/lang/Boolean;

    if-nez v0, :cond_2b

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_DELETE_SESSION_TOKEN_PLAINTEXT:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->delete_session_token_plaintext(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6735
    :cond_2b
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->enable_verbose_login_response_cache_logging:Ljava/lang/Boolean;

    if-nez v0, :cond_2c

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_ENABLE_VERBOSE_LOGIN_RESPONSE_CACHE_LOGGING:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->enable_verbose_login_response_cache_logging(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6736
    :cond_2c
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_session_token_encryption_android_v2:Ljava/lang/Boolean;

    if-nez v0, :cond_2d

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_USE_SESSION_TOKEN_ENCRYPTION_ANDROID_V2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->use_session_token_encryption_android_v2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6737
    :cond_2d
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->hardware_secure_touch_high_contrast_mode:Ljava/lang/Boolean;

    if-nez v0, :cond_2e

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_HARDWARE_SECURE_TOUCH_HIGH_CONTRAST_MODE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->hardware_secure_touch_high_contrast_mode(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    .line 6738
    :cond_2e
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->checkout_applet_v2_android:Ljava/lang/Boolean;

    if-nez v0, :cond_2f

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->DEFAULT_CHECKOUT_APPLET_V2_ANDROID:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->checkout_applet_v2_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;

    move-result-object v1

    :cond_2f
    if-nez v1, :cond_30

    move-object v0, p0

    goto :goto_0

    .line 6739
    :cond_30
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 5838
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 6636
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 6637
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->bundle_logging:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const-string v1, ", bundle_logging="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->bundle_logging:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6638
    :cond_0
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_pre_tax_tipping_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    const-string v1, ", use_pre_tax_tipping_android="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_pre_tax_tipping_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6639
    :cond_1
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_api_url_list_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", use_api_url_list_android="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_api_url_list_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6640
    :cond_2
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->ignore_pii_accessibility_scrubber_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    const-string v1, ", ignore_pii_accessibility_scrubber_android="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->ignore_pii_accessibility_scrubber_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6641
    :cond_3
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->upload_x2_wifi_events:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    const-string v1, ", upload_x2_wifi_events="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->upload_x2_wifi_events:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6642
    :cond_4
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->loyalty_checkout_x2:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    const-string v1, ", loyalty_checkout_x2="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->loyalty_checkout_x2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6643
    :cond_5
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_clock_skew_lockout_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    const-string v1, ", use_clock_skew_lockout_android="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_clock_skew_lockout_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6644
    :cond_6
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_jedi_help_applet:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    const-string v1, ", use_jedi_help_applet="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_jedi_help_applet:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6645
    :cond_7
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_persistent_bundle_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    const-string v1, ", use_persistent_bundle_android="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_persistent_bundle_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6646
    :cond_8
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->dip_tap_to_create_open_ticket_with_name:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    const-string v1, ", dip_tap_to_create_open_ticket_with_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->dip_tap_to_create_open_ticket_with_name:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6647
    :cond_9
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_session_token_encryption_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    const-string v1, ", use_session_token_encryption_android="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_session_token_encryption_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6648
    :cond_a
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_jedi_help_applet_x2:Ljava/lang/Boolean;

    if-eqz v1, :cond_b

    const-string v1, ", use_jedi_help_applet_x2="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_jedi_help_applet_x2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6649
    :cond_b
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->upload_ledger_and_diagnostics_x2:Ljava/lang/Boolean;

    if-eqz v1, :cond_c

    const-string v1, ", upload_ledger_and_diagnostics_x2="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->upload_ledger_and_diagnostics_x2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6650
    :cond_c
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->print_itemized_discounts:Ljava/lang/Boolean;

    if-eqz v1, :cond_d

    const-string v1, ", print_itemized_discounts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->print_itemized_discounts:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6651
    :cond_d
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_feature_tour_x2:Ljava/lang/Boolean;

    if-eqz v1, :cond_e

    const-string v1, ", use_feature_tour_x2="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_feature_tour_x2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6652
    :cond_e
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->printer_debugging:Ljava/lang/Boolean;

    if-eqz v1, :cond_f

    const-string v1, ", printer_debugging="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->printer_debugging:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6653
    :cond_f
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->restart_app_after_crash:Ljava/lang/Boolean;

    if-eqz v1, :cond_10

    const-string v1, ", restart_app_after_crash="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->restart_app_after_crash:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6654
    :cond_10
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->intercept_magswipe_events_during_printing_t2:Ljava/lang/Boolean;

    if-eqz v1, :cond_11

    const-string v1, ", intercept_magswipe_events_during_printing_t2="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->intercept_magswipe_events_during_printing_t2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6655
    :cond_11
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_device_settings_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_12

    const-string v1, ", use_device_settings_android="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_device_settings_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6656
    :cond_12
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_jedi_help_applet_t2:Ljava/lang/Boolean;

    if-eqz v1, :cond_13

    const-string v1, ", use_jedi_help_applet_t2="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_jedi_help_applet_t2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6657
    :cond_13
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->firmware_update_t2:Ljava/lang/Boolean;

    if-eqz v1, :cond_14

    const-string v1, ", firmware_update_t2="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->firmware_update_t2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6658
    :cond_14
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->separated_printouts:Ljava/lang/Boolean;

    if-eqz v1, :cond_15

    const-string v1, ", separated_printouts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->separated_printouts:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6659
    :cond_15
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_predefined_tickets_t2:Ljava/lang/Boolean;

    if-eqz v1, :cond_16

    const-string v1, ", use_predefined_tickets_t2="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_predefined_tickets_t2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6660
    :cond_16
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->resilient_bus:Ljava/lang/Boolean;

    if-eqz v1, :cond_17

    const-string v1, ", resilient_bus="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->resilient_bus:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6661
    :cond_17
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->firmware_update_jail_t2:Ljava/lang/Boolean;

    if-eqz v1, :cond_18

    const-string v1, ", firmware_update_jail_t2="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->firmware_update_jail_t2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6662
    :cond_18
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->hardware_secure_touch:Ljava/lang/Boolean;

    if-eqz v1, :cond_19

    const-string v1, ", hardware_secure_touch="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->hardware_secure_touch:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6663
    :cond_19
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->can_close_remote_cash_drawers_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_1a

    const-string v1, ", can_close_remote_cash_drawers_android="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->can_close_remote_cash_drawers_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6664
    :cond_1a
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->wait_for_bran_x2:Ljava/lang/Boolean;

    if-eqz v1, :cond_1b

    const-string v1, ", wait_for_bran_x2="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->wait_for_bran_x2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6665
    :cond_1b
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->skyhook_integration_v2:Ljava/lang/Boolean;

    if-eqz v1, :cond_1c

    const-string v1, ", skyhook_integration_v2="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->skyhook_integration_v2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6666
    :cond_1c
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->skyhook_x2_use_24_hour_cache_lifespan:Ljava/lang/Boolean;

    if-eqz v1, :cond_1d

    const-string v1, ", skyhook_x2_use_24_hour_cache_lifespan="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->skyhook_x2_use_24_hour_cache_lifespan:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6667
    :cond_1d
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->skyhook_t2_use_3_hour_check_interval:Ljava/lang/Boolean;

    if-eqz v1, :cond_1e

    const-string v1, ", skyhook_t2_use_3_hour_check_interval="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->skyhook_t2_use_3_hour_check_interval:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6668
    :cond_1e
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->skyhook_t2_use_6_hour_cache_lifespan:Ljava/lang/Boolean;

    if-eqz v1, :cond_1f

    const-string v1, ", skyhook_t2_use_6_hour_cache_lifespan="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->skyhook_t2_use_6_hour_cache_lifespan:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6669
    :cond_1f
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->skyhook_v2_only_use_ip_location_with_no_wps:Ljava/lang/Boolean;

    if-eqz v1, :cond_20

    const-string v1, ", skyhook_v2_only_use_ip_location_with_no_wps="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->skyhook_v2_only_use_ip_location_with_no_wps:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6670
    :cond_20
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->buyer_checkout_display_transaction_type:Ljava/lang/Boolean;

    if-eqz v1, :cond_21

    const-string v1, ", buyer_checkout_display_transaction_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->buyer_checkout_display_transaction_type:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6671
    :cond_21
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->receipts_print_disposition:Ljava/lang/Boolean;

    if-eqz v1, :cond_22

    const-string v1, ", receipts_print_disposition="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->receipts_print_disposition:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6672
    :cond_22
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->order_entry_library_create_new_item:Ljava/lang/Boolean;

    if-eqz v1, :cond_23

    const-string v1, ", order_entry_library_create_new_item="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->order_entry_library_create_new_item:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6673
    :cond_23
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->show_fee_breakdown_table:Ljava/lang/Boolean;

    if-eqz v1, :cond_24

    const-string v1, ", show_fee_breakdown_table="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->show_fee_breakdown_table:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6674
    :cond_24
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->order_entry_library_hide_giftcards_rewards_if_no_items:Ljava/lang/Boolean;

    if-eqz v1, :cond_25

    const-string v1, ", order_entry_library_hide_giftcards_rewards_if_no_items="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->order_entry_library_hide_giftcards_rewards_if_no_items:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6675
    :cond_25
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_order_entry_screen_v2_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_26

    const-string v1, ", use_order_entry_screen_v2_android="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_order_entry_screen_v2_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6676
    :cond_26
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->order_entry_library_item_suggestions:Ljava/lang/Boolean;

    if-eqz v1, :cond_27

    const-string v1, ", order_entry_library_item_suggestions="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->order_entry_library_item_suggestions:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6677
    :cond_27
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->can_print_single_ticket_per_item:Ljava/lang/Boolean;

    if-eqz v1, :cond_28

    const-string v1, ", can_print_single_ticket_per_item="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->can_print_single_ticket_per_item:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6678
    :cond_28
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->hardware_secure_touch_accessibility_mode:Ljava/lang/Boolean;

    if-eqz v1, :cond_29

    const-string v1, ", hardware_secure_touch_accessibility_mode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->hardware_secure_touch_accessibility_mode:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6679
    :cond_29
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->enhanced_transaction_search_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_2a

    const-string v1, ", enhanced_transaction_search_android="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->enhanced_transaction_search_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6680
    :cond_2a
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->delete_session_token_plaintext:Ljava/lang/Boolean;

    if-eqz v1, :cond_2b

    const-string v1, ", delete_session_token_plaintext="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->delete_session_token_plaintext:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6681
    :cond_2b
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->enable_verbose_login_response_cache_logging:Ljava/lang/Boolean;

    if-eqz v1, :cond_2c

    const-string v1, ", enable_verbose_login_response_cache_logging="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->enable_verbose_login_response_cache_logging:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6682
    :cond_2c
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_session_token_encryption_android_v2:Ljava/lang/Boolean;

    if-eqz v1, :cond_2d

    const-string v1, ", use_session_token_encryption_android_v2="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->use_session_token_encryption_android_v2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6683
    :cond_2d
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->hardware_secure_touch_high_contrast_mode:Ljava/lang/Boolean;

    if-eqz v1, :cond_2e

    const-string v1, ", hardware_secure_touch_high_contrast_mode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->hardware_secure_touch_high_contrast_mode:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6684
    :cond_2e
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->checkout_applet_v2_android:Ljava/lang/Boolean;

    if-eqz v1, :cond_2f

    const-string v1, ", checkout_applet_v2_android="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$RegEx;->checkout_applet_v2_android:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2f
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "RegEx{"

    .line 6685
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
