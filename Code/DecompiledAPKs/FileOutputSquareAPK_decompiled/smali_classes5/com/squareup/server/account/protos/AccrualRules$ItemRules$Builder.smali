.class public final Lcom/squareup/server/account/protos/AccrualRules$ItemRules$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "AccrualRules.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/AccrualRules$ItemRules;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/AccrualRules$ItemRules;",
        "Lcom/squareup/server/account/protos/AccrualRules$ItemRules$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public catalog_object_rules:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 949
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 950
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$Builder;->catalog_object_rules:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/server/account/protos/AccrualRules$ItemRules;
    .locals 3

    .line 966
    new-instance v0, Lcom/squareup/server/account/protos/AccrualRules$ItemRules;

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$Builder;->catalog_object_rules:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/server/account/protos/AccrualRules$ItemRules;-><init>(Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 946
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$Builder;->build()Lcom/squareup/server/account/protos/AccrualRules$ItemRules;

    move-result-object v0

    return-object v0
.end method

.method public catalog_object_rules(Ljava/util/List;)Lcom/squareup/server/account/protos/AccrualRules$ItemRules$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/server/account/protos/AccrualRules$ItemRules$CatalogObjectRule;",
            ">;)",
            "Lcom/squareup/server/account/protos/AccrualRules$ItemRules$Builder;"
        }
    .end annotation

    .line 959
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 960
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccrualRules$ItemRules$Builder;->catalog_object_rules:Ljava/util/List;

    return-object p0
.end method
