.class public final Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FlagsAndPermissions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public alphanumeric_coupons:Ljava/lang/Boolean;

.field public can_support_negative_balances:Ljava/lang/Boolean;

.field public can_use_cash_integration:Ljava/lang/Boolean;

.field public delete_will_send_sms:Ljava/lang/Boolean;

.field public expire_points:Ljava/lang/Boolean;

.field public loyalty_enrollment_workflow:Ljava/lang/Boolean;

.field public loyalty_sms:Ljava/lang/Boolean;

.field public see_enrollment_tutorial:Ljava/lang/Boolean;

.field public should_loyalty_handle_returns:Ljava/lang/Boolean;

.field public show_loyalty:Ljava/lang/Boolean;

.field public show_loyalty_value_metrics:Ljava/lang/Boolean;

.field public use_coupon_item_selection:Ljava/lang/Boolean;

.field public x2_front_of_transaction_checkin:Ljava/lang/Boolean;

.field public x2_front_of_transaction_checkin_seller_override:Ljava/lang/Boolean;

.field public x2_post_transaction_dark_theme:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 4639
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public alphanumeric_coupons(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;
    .locals 0

    .line 4646
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->alphanumeric_coupons:Ljava/lang/Boolean;

    return-object p0
.end method

.method public build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;
    .locals 20

    move-object/from16 v0, p0

    .line 4767
    new-instance v18, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;

    move-object/from16 v1, v18

    iget-object v2, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->alphanumeric_coupons:Ljava/lang/Boolean;

    iget-object v3, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->can_use_cash_integration:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->see_enrollment_tutorial:Ljava/lang/Boolean;

    iget-object v5, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->expire_points:Ljava/lang/Boolean;

    iget-object v6, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->show_loyalty:Ljava/lang/Boolean;

    iget-object v7, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->loyalty_sms:Ljava/lang/Boolean;

    iget-object v8, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->x2_post_transaction_dark_theme:Ljava/lang/Boolean;

    iget-object v9, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->x2_front_of_transaction_checkin:Ljava/lang/Boolean;

    iget-object v10, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->use_coupon_item_selection:Ljava/lang/Boolean;

    iget-object v11, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->delete_will_send_sms:Ljava/lang/Boolean;

    iget-object v12, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->should_loyalty_handle_returns:Ljava/lang/Boolean;

    iget-object v13, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->can_support_negative_balances:Ljava/lang/Boolean;

    iget-object v14, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->show_loyalty_value_metrics:Ljava/lang/Boolean;

    iget-object v15, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->x2_front_of_transaction_checkin_seller_override:Ljava/lang/Boolean;

    move-object/from16 v19, v1

    iget-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->loyalty_enrollment_workflow:Ljava/lang/Boolean;

    move-object/from16 v16, v1

    invoke-super/range {p0 .. p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v17

    move-object/from16 v1, v19

    invoke-direct/range {v1 .. v17}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v18
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 4608
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty;

    move-result-object v0

    return-object v0
.end method

.method public can_support_negative_balances(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;
    .locals 0

    .line 4736
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->can_support_negative_balances:Ljava/lang/Boolean;

    return-object p0
.end method

.method public can_use_cash_integration(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;
    .locals 0

    .line 4655
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->can_use_cash_integration:Ljava/lang/Boolean;

    return-object p0
.end method

.method public delete_will_send_sms(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;
    .locals 0

    .line 4720
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->delete_will_send_sms:Ljava/lang/Boolean;

    return-object p0
.end method

.method public expire_points(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;
    .locals 0

    .line 4672
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->expire_points:Ljava/lang/Boolean;

    return-object p0
.end method

.method public loyalty_enrollment_workflow(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;
    .locals 0

    .line 4761
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->loyalty_enrollment_workflow:Ljava/lang/Boolean;

    return-object p0
.end method

.method public loyalty_sms(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;
    .locals 0

    .line 4688
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->loyalty_sms:Ljava/lang/Boolean;

    return-object p0
.end method

.method public see_enrollment_tutorial(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;
    .locals 0

    .line 4663
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->see_enrollment_tutorial:Ljava/lang/Boolean;

    return-object p0
.end method

.method public should_loyalty_handle_returns(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;
    .locals 0

    .line 4728
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->should_loyalty_handle_returns:Ljava/lang/Boolean;

    return-object p0
.end method

.method public show_loyalty(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;
    .locals 0

    .line 4680
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->show_loyalty:Ljava/lang/Boolean;

    return-object p0
.end method

.method public show_loyalty_value_metrics(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;
    .locals 0

    .line 4744
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->show_loyalty_value_metrics:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_coupon_item_selection(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;
    .locals 0

    .line 4712
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->use_coupon_item_selection:Ljava/lang/Boolean;

    return-object p0
.end method

.method public x2_front_of_transaction_checkin(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;
    .locals 0

    .line 4704
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->x2_front_of_transaction_checkin:Ljava/lang/Boolean;

    return-object p0
.end method

.method public x2_front_of_transaction_checkin_seller_override(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;
    .locals 0

    .line 4753
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->x2_front_of_transaction_checkin_seller_override:Ljava/lang/Boolean;

    return-object p0
.end method

.method public x2_post_transaction_dark_theme(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;
    .locals 0

    .line 4696
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Loyalty$Builder;->x2_post_transaction_dark_theme:Ljava/lang/Boolean;

    return-object p0
.end method
