.class public final Lcom/squareup/server/account/MerchantProfileResponse$Entity;
.super Ljava/lang/Object;
.source "MerchantProfileResponse.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/MerchantProfileResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Entity"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/MerchantProfileResponse$Entity$MerchantImages;,
        Lcom/squareup/server/account/MerchantProfileResponse$Entity$Image;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0010\u000e\n\u0002\u0008\u0010\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0018\u00002\u00020\u0001:\u0002 !B\u0097\u0002\u0012\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0008\u0012\n\u0008\u0002\u0010\t\u001a\u0004\u0018\u00010\u0008\u0012\n\u0008\u0002\u0010\n\u001a\u0004\u0018\u00010\u0008\u0012\n\u0008\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u0008\u0012\n\u0008\u0002\u0010\u000c\u001a\u0004\u0018\u00010\u0008\u0012\n\u0008\u0002\u0010\r\u001a\u0004\u0018\u00010\u0008\u0012\n\u0008\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u0008\u0012\n\u0008\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u0008\u0012\n\u0008\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u0008\u0012\n\u0008\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u0008\u0012\n\u0008\u0002\u0010\u0012\u001a\u0004\u0018\u00010\u0008\u0012\n\u0008\u0002\u0010\u0013\u001a\u0004\u0018\u00010\u0008\u0012\n\u0008\u0002\u0010\u0014\u001a\u0004\u0018\u00010\u0008\u0012\n\u0008\u0002\u0010\u0015\u001a\u0004\u0018\u00010\u0008\u0012\n\u0008\u0002\u0010\u0016\u001a\u0004\u0018\u00010\u0008\u0012\n\u0008\u0002\u0010\u0017\u001a\u0004\u0018\u00010\u0008\u0012\n\u0008\u0002\u0010\u0018\u001a\u0004\u0018\u00010\u0019\u0012\n\u0008\u0002\u0010\u001a\u001a\u0004\u0018\u00010\u0019\u0012\u0010\u0008\u0002\u0010\u001b\u001a\n\u0012\u0004\u0012\u00020\u001d\u0018\u00010\u001c\u00a2\u0006\u0002\u0010\u001eR\u0012\u0010\u000c\u001a\u0004\u0018\u00010\u00088\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0017\u001a\u0004\u0018\u00010\u00088\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0007\u001a\u0004\u0018\u00010\u00088\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0016\u001a\u0004\u0018\u00010\u00088\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\r\u001a\u0004\u0018\u00010\u00088\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\t\u001a\u0004\u0018\u00010\u00088\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0012\u001a\u0004\u0018\u00010\u00088\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0014\u001a\u0004\u0018\u00010\u00088\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u001a\u001a\u0004\u0018\u00010\u00198\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u001b\u001a\n\u0012\u0004\u0012\u00020\u001d\u0018\u00010\u001c8\u0006X\u0087\u0004\u00a2\u0006\u0004\n\u0002\u0010\u001fR\u0010\u0010\u0004\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0011\u001a\u0004\u0018\u00010\u00088\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0010\u001a\u0004\u0018\u00010\u00088\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u000f\u001a\u0004\u0018\u00010\u00088\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0018\u001a\u0004\u0018\u00010\u00198\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u000e\u001a\u0004\u0018\u00010\u00088\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\n\u001a\u0004\u0018\u00010\u00088\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u000b\u001a\u0004\u0018\u00010\u00088\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0013\u001a\u0004\u0018\u00010\u00088\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0015\u001a\u0004\u0018\u00010\u00088\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\""
    }
    d2 = {
        "Lcom/squareup/server/account/MerchantProfileResponse$Entity;",
        "",
        "has_customized_merchant_card",
        "",
        "mobile_business",
        "published",
        "use_generated_menu",
        "business_type",
        "",
        "country_code",
        "street1",
        "street2",
        "address_line_3",
        "city",
        "state",
        "postal_code",
        "phone",
        "name",
        "email",
        "twitter",
        "facebook",
        "website",
        "card_color",
        "bio",
        "profile_image",
        "Lcom/squareup/server/account/MerchantProfileResponse$Entity$Image;",
        "featured_image",
        "merchant_images",
        "",
        "Lcom/squareup/server/account/MerchantProfileResponse$Entity$MerchantImages;",
        "(ZZZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/server/account/MerchantProfileResponse$Entity$Image;Lcom/squareup/server/account/MerchantProfileResponse$Entity$Image;[Lcom/squareup/server/account/MerchantProfileResponse$Entity$MerchantImages;)V",
        "[Lcom/squareup/server/account/MerchantProfileResponse$Entity$MerchantImages;",
        "Image",
        "MerchantImages",
        "services-data_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public final address_line_3:Ljava/lang/String;

.field public final bio:Ljava/lang/String;

.field public final business_type:Ljava/lang/String;

.field public final card_color:Ljava/lang/String;

.field public final city:Ljava/lang/String;

.field public final country_code:Ljava/lang/String;

.field public final email:Ljava/lang/String;

.field public final facebook:Ljava/lang/String;

.field public final featured_image:Lcom/squareup/server/account/MerchantProfileResponse$Entity$Image;

.field public final has_customized_merchant_card:Z

.field public final merchant_images:[Lcom/squareup/server/account/MerchantProfileResponse$Entity$MerchantImages;

.field public final mobile_business:Z

.field public final name:Ljava/lang/String;

.field public final phone:Ljava/lang/String;

.field public final postal_code:Ljava/lang/String;

.field public final profile_image:Lcom/squareup/server/account/MerchantProfileResponse$Entity$Image;

.field public final published:Z

.field public final state:Ljava/lang/String;

.field public final street1:Ljava/lang/String;

.field public final street2:Ljava/lang/String;

.field public final twitter:Ljava/lang/String;

.field public final use_generated_menu:Z

.field public final website:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 26

    move-object/from16 v0, p0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    const v24, 0x7fffff

    const/16 v25, 0x0

    invoke-direct/range {v0 .. v25}, Lcom/squareup/server/account/MerchantProfileResponse$Entity;-><init>(ZZZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/server/account/MerchantProfileResponse$Entity$Image;Lcom/squareup/server/account/MerchantProfileResponse$Entity$Image;[Lcom/squareup/server/account/MerchantProfileResponse$Entity$MerchantImages;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(ZZZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/server/account/MerchantProfileResponse$Entity$Image;Lcom/squareup/server/account/MerchantProfileResponse$Entity$Image;[Lcom/squareup/server/account/MerchantProfileResponse$Entity$MerchantImages;)V
    .locals 2

    move-object v0, p0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move v1, p1

    iput-boolean v1, v0, Lcom/squareup/server/account/MerchantProfileResponse$Entity;->has_customized_merchant_card:Z

    move v1, p2

    iput-boolean v1, v0, Lcom/squareup/server/account/MerchantProfileResponse$Entity;->mobile_business:Z

    move v1, p3

    iput-boolean v1, v0, Lcom/squareup/server/account/MerchantProfileResponse$Entity;->published:Z

    move v1, p4

    iput-boolean v1, v0, Lcom/squareup/server/account/MerchantProfileResponse$Entity;->use_generated_menu:Z

    move-object v1, p5

    iput-object v1, v0, Lcom/squareup/server/account/MerchantProfileResponse$Entity;->business_type:Ljava/lang/String;

    move-object v1, p6

    iput-object v1, v0, Lcom/squareup/server/account/MerchantProfileResponse$Entity;->country_code:Ljava/lang/String;

    move-object v1, p7

    iput-object v1, v0, Lcom/squareup/server/account/MerchantProfileResponse$Entity;->street1:Ljava/lang/String;

    move-object v1, p8

    iput-object v1, v0, Lcom/squareup/server/account/MerchantProfileResponse$Entity;->street2:Ljava/lang/String;

    move-object v1, p9

    iput-object v1, v0, Lcom/squareup/server/account/MerchantProfileResponse$Entity;->address_line_3:Ljava/lang/String;

    move-object v1, p10

    iput-object v1, v0, Lcom/squareup/server/account/MerchantProfileResponse$Entity;->city:Ljava/lang/String;

    move-object v1, p11

    iput-object v1, v0, Lcom/squareup/server/account/MerchantProfileResponse$Entity;->state:Ljava/lang/String;

    move-object v1, p12

    iput-object v1, v0, Lcom/squareup/server/account/MerchantProfileResponse$Entity;->postal_code:Ljava/lang/String;

    move-object v1, p13

    iput-object v1, v0, Lcom/squareup/server/account/MerchantProfileResponse$Entity;->phone:Ljava/lang/String;

    move-object/from16 v1, p14

    iput-object v1, v0, Lcom/squareup/server/account/MerchantProfileResponse$Entity;->name:Ljava/lang/String;

    move-object/from16 v1, p15

    iput-object v1, v0, Lcom/squareup/server/account/MerchantProfileResponse$Entity;->email:Ljava/lang/String;

    move-object/from16 v1, p16

    iput-object v1, v0, Lcom/squareup/server/account/MerchantProfileResponse$Entity;->twitter:Ljava/lang/String;

    move-object/from16 v1, p17

    iput-object v1, v0, Lcom/squareup/server/account/MerchantProfileResponse$Entity;->facebook:Ljava/lang/String;

    move-object/from16 v1, p18

    iput-object v1, v0, Lcom/squareup/server/account/MerchantProfileResponse$Entity;->website:Ljava/lang/String;

    move-object/from16 v1, p19

    iput-object v1, v0, Lcom/squareup/server/account/MerchantProfileResponse$Entity;->card_color:Ljava/lang/String;

    move-object/from16 v1, p20

    iput-object v1, v0, Lcom/squareup/server/account/MerchantProfileResponse$Entity;->bio:Ljava/lang/String;

    move-object/from16 v1, p21

    iput-object v1, v0, Lcom/squareup/server/account/MerchantProfileResponse$Entity;->profile_image:Lcom/squareup/server/account/MerchantProfileResponse$Entity$Image;

    move-object/from16 v1, p22

    iput-object v1, v0, Lcom/squareup/server/account/MerchantProfileResponse$Entity;->featured_image:Lcom/squareup/server/account/MerchantProfileResponse$Entity$Image;

    move-object/from16 v1, p23

    iput-object v1, v0, Lcom/squareup/server/account/MerchantProfileResponse$Entity;->merchant_images:[Lcom/squareup/server/account/MerchantProfileResponse$Entity$MerchantImages;

    return-void
.end method

.method public synthetic constructor <init>(ZZZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/server/account/MerchantProfileResponse$Entity$Image;Lcom/squareup/server/account/MerchantProfileResponse$Entity$Image;[Lcom/squareup/server/account/MerchantProfileResponse$Entity$MerchantImages;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 24

    move/from16 v0, p24

    and-int/lit8 v1, v0, 0x1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    move/from16 v1, p1

    :goto_0
    and-int/lit8 v3, v0, 0x2

    if-eqz v3, :cond_1

    const/4 v3, 0x0

    goto :goto_1

    :cond_1
    move/from16 v3, p2

    :goto_1
    and-int/lit8 v4, v0, 0x4

    if-eqz v4, :cond_2

    const/4 v4, 0x0

    goto :goto_2

    :cond_2
    move/from16 v4, p3

    :goto_2
    and-int/lit8 v5, v0, 0x8

    if-eqz v5, :cond_3

    goto :goto_3

    :cond_3
    move/from16 v2, p4

    :goto_3
    and-int/lit8 v5, v0, 0x10

    const/4 v6, 0x0

    if-eqz v5, :cond_4

    .line 20
    move-object v5, v6

    check-cast v5, Ljava/lang/String;

    goto :goto_4

    :cond_4
    move-object/from16 v5, p5

    :goto_4
    and-int/lit8 v7, v0, 0x20

    if-eqz v7, :cond_5

    .line 21
    move-object v7, v6

    check-cast v7, Ljava/lang/String;

    goto :goto_5

    :cond_5
    move-object/from16 v7, p6

    :goto_5
    and-int/lit8 v8, v0, 0x40

    if-eqz v8, :cond_6

    .line 22
    move-object v8, v6

    check-cast v8, Ljava/lang/String;

    goto :goto_6

    :cond_6
    move-object/from16 v8, p7

    :goto_6
    and-int/lit16 v9, v0, 0x80

    if-eqz v9, :cond_7

    .line 23
    move-object v9, v6

    check-cast v9, Ljava/lang/String;

    goto :goto_7

    :cond_7
    move-object/from16 v9, p8

    :goto_7
    and-int/lit16 v10, v0, 0x100

    if-eqz v10, :cond_8

    .line 24
    move-object v10, v6

    check-cast v10, Ljava/lang/String;

    goto :goto_8

    :cond_8
    move-object/from16 v10, p9

    :goto_8
    and-int/lit16 v11, v0, 0x200

    if-eqz v11, :cond_9

    .line 25
    move-object v11, v6

    check-cast v11, Ljava/lang/String;

    goto :goto_9

    :cond_9
    move-object/from16 v11, p10

    :goto_9
    and-int/lit16 v12, v0, 0x400

    if-eqz v12, :cond_a

    .line 26
    move-object v12, v6

    check-cast v12, Ljava/lang/String;

    goto :goto_a

    :cond_a
    move-object/from16 v12, p11

    :goto_a
    and-int/lit16 v13, v0, 0x800

    if-eqz v13, :cond_b

    .line 27
    move-object v13, v6

    check-cast v13, Ljava/lang/String;

    goto :goto_b

    :cond_b
    move-object/from16 v13, p12

    :goto_b
    and-int/lit16 v14, v0, 0x1000

    if-eqz v14, :cond_c

    .line 28
    move-object v14, v6

    check-cast v14, Ljava/lang/String;

    goto :goto_c

    :cond_c
    move-object/from16 v14, p13

    :goto_c
    and-int/lit16 v15, v0, 0x2000

    if-eqz v15, :cond_d

    .line 29
    move-object v15, v6

    check-cast v15, Ljava/lang/String;

    goto :goto_d

    :cond_d
    move-object/from16 v15, p14

    :goto_d
    move-object/from16 p25, v15

    and-int/lit16 v15, v0, 0x4000

    if-eqz v15, :cond_e

    .line 30
    move-object v15, v6

    check-cast v15, Ljava/lang/String;

    goto :goto_e

    :cond_e
    move-object/from16 v15, p15

    :goto_e
    const v16, 0x8000

    and-int v16, v0, v16

    if-eqz v16, :cond_f

    .line 31
    move-object/from16 v16, v6

    check-cast v16, Ljava/lang/String;

    goto :goto_f

    :cond_f
    move-object/from16 v16, p16

    :goto_f
    const/high16 v17, 0x10000

    and-int v17, v0, v17

    if-eqz v17, :cond_10

    .line 32
    move-object/from16 v17, v6

    check-cast v17, Ljava/lang/String;

    goto :goto_10

    :cond_10
    move-object/from16 v17, p17

    :goto_10
    const/high16 v18, 0x20000

    and-int v18, v0, v18

    if-eqz v18, :cond_11

    .line 33
    move-object/from16 v18, v6

    check-cast v18, Ljava/lang/String;

    goto :goto_11

    :cond_11
    move-object/from16 v18, p18

    :goto_11
    const/high16 v19, 0x40000

    and-int v19, v0, v19

    if-eqz v19, :cond_12

    .line 34
    move-object/from16 v19, v6

    check-cast v19, Ljava/lang/String;

    goto :goto_12

    :cond_12
    move-object/from16 v19, p19

    :goto_12
    const/high16 v20, 0x80000

    and-int v20, v0, v20

    if-eqz v20, :cond_13

    .line 35
    move-object/from16 v20, v6

    check-cast v20, Ljava/lang/String;

    goto :goto_13

    :cond_13
    move-object/from16 v20, p20

    :goto_13
    const/high16 v21, 0x100000

    and-int v21, v0, v21

    if-eqz v21, :cond_14

    .line 36
    move-object/from16 v21, v6

    check-cast v21, Lcom/squareup/server/account/MerchantProfileResponse$Entity$Image;

    goto :goto_14

    :cond_14
    move-object/from16 v21, p21

    :goto_14
    const/high16 v22, 0x200000

    and-int v22, v0, v22

    if-eqz v22, :cond_15

    .line 37
    move-object/from16 v22, v6

    check-cast v22, Lcom/squareup/server/account/MerchantProfileResponse$Entity$Image;

    goto :goto_15

    :cond_15
    move-object/from16 v22, p22

    :goto_15
    const/high16 v23, 0x400000

    and-int v0, v0, v23

    if-eqz v0, :cond_16

    .line 38
    move-object v0, v6

    check-cast v0, [Lcom/squareup/server/account/MerchantProfileResponse$Entity$MerchantImages;

    goto :goto_16

    :cond_16
    move-object/from16 v0, p23

    :goto_16
    move-object/from16 p1, p0

    move/from16 p2, v1

    move/from16 p3, v3

    move/from16 p4, v4

    move/from16 p5, v2

    move-object/from16 p6, v5

    move-object/from16 p7, v7

    move-object/from16 p8, v8

    move-object/from16 p9, v9

    move-object/from16 p10, v10

    move-object/from16 p11, v11

    move-object/from16 p12, v12

    move-object/from16 p13, v13

    move-object/from16 p14, v14

    move-object/from16 p15, p25

    move-object/from16 p16, v15

    move-object/from16 p17, v16

    move-object/from16 p18, v17

    move-object/from16 p19, v18

    move-object/from16 p20, v19

    move-object/from16 p21, v20

    move-object/from16 p22, v21

    move-object/from16 p23, v22

    move-object/from16 p24, v0

    invoke-direct/range {p1 .. p24}, Lcom/squareup/server/account/MerchantProfileResponse$Entity;-><init>(ZZZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/server/account/MerchantProfileResponse$Entity$Image;Lcom/squareup/server/account/MerchantProfileResponse$Entity$Image;[Lcom/squareup/server/account/MerchantProfileResponse$Entity$MerchantImages;)V

    return-void
.end method
