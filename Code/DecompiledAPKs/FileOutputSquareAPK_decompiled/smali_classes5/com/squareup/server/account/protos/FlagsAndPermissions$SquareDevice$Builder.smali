.class public final Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FlagsAndPermissions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public bran_cart_scroll_logging:Ljava/lang/Boolean;

.field public bran_display_api_x2:Ljava/lang/Boolean;

.field public bran_display_cart_monitor_workflow_x2:Ljava/lang/Boolean;

.field public bran_multiple_images:Ljava/lang/Boolean;

.field public check_pts_compliance:Ljava/lang/Boolean;

.field public diagnostic_data_reporter_2_finger_bug_reports:Ljava/lang/Boolean;

.field public jumbotron_service_key_t2:Ljava/lang/Boolean;

.field public printer_dithering_t2:Ljava/lang/Boolean;

.field public spe_fwup_crq:Ljava/lang/Boolean;

.field public spe_fwup_without_matching_tms:Ljava/lang/Boolean;

.field public spe_tms_login_check:Ljava/lang/Boolean;

.field public spm:Ljava/lang/Boolean;

.field public spm_es:Ljava/lang/Boolean;

.field public status_bar_fullscreen_t2:Ljava/lang/Boolean;

.field public t2_buyer_checkout_defaults_to_us_english:Ljava/lang/Boolean;

.field public use_accessible_pin_tutorial:Ljava/lang/Boolean;

.field public use_bran_payment_prompt_variations_experiment:Ljava/lang/Boolean;

.field public use_buyer_display_settings_v2_x2:Ljava/lang/Boolean;

.field public use_file_size_analytics:Ljava/lang/Boolean;

.field public x2_use_payment_workflows:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15618
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public bran_cart_scroll_logging(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;
    .locals 0

    .line 15781
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->bran_cart_scroll_logging:Ljava/lang/Boolean;

    return-object p0
.end method

.method public bran_display_api_x2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;
    .locals 0

    .line 15657
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->bran_display_api_x2:Ljava/lang/Boolean;

    return-object p0
.end method

.method public bran_display_cart_monitor_workflow_x2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;
    .locals 0

    .line 15674
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->bran_display_cart_monitor_workflow_x2:Ljava/lang/Boolean;

    return-object p0
.end method

.method public bran_multiple_images(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;
    .locals 0

    .line 15714
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->bran_multiple_images:Ljava/lang/Boolean;

    return-object p0
.end method

.method public build()Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;
    .locals 2

    .line 15787
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;-><init>(Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 15577
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice;

    move-result-object v0

    return-object v0
.end method

.method public check_pts_compliance(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;
    .locals 0

    .line 15690
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->check_pts_compliance:Ljava/lang/Boolean;

    return-object p0
.end method

.method public diagnostic_data_reporter_2_finger_bug_reports(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;
    .locals 0

    .line 15740
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->diagnostic_data_reporter_2_finger_bug_reports:Ljava/lang/Boolean;

    return-object p0
.end method

.method public jumbotron_service_key_t2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;
    .locals 0

    .line 15649
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->jumbotron_service_key_t2:Ljava/lang/Boolean;

    return-object p0
.end method

.method public printer_dithering_t2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;
    .locals 0

    .line 15625
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->printer_dithering_t2:Ljava/lang/Boolean;

    return-object p0
.end method

.method public spe_fwup_crq(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;
    .locals 0

    .line 15665
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->spe_fwup_crq:Ljava/lang/Boolean;

    return-object p0
.end method

.method public spe_fwup_without_matching_tms(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;
    .locals 0

    .line 15641
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->spe_fwup_without_matching_tms:Ljava/lang/Boolean;

    return-object p0
.end method

.method public spe_tms_login_check(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;
    .locals 0

    .line 15706
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->spe_tms_login_check:Ljava/lang/Boolean;

    return-object p0
.end method

.method public spm(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;
    .locals 0

    .line 15682
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->spm:Ljava/lang/Boolean;

    return-object p0
.end method

.method public spm_es(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;
    .locals 0

    .line 15698
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->spm_es:Ljava/lang/Boolean;

    return-object p0
.end method

.method public status_bar_fullscreen_t2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;
    .locals 0

    .line 15633
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->status_bar_fullscreen_t2:Ljava/lang/Boolean;

    return-object p0
.end method

.method public t2_buyer_checkout_defaults_to_us_english(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;
    .locals 0

    .line 15757
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->t2_buyer_checkout_defaults_to_us_english:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_accessible_pin_tutorial(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;
    .locals 0

    .line 15748
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->use_accessible_pin_tutorial:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_bran_payment_prompt_variations_experiment(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;
    .locals 0

    .line 15723
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->use_bran_payment_prompt_variations_experiment:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_buyer_display_settings_v2_x2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;
    .locals 0

    .line 15731
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->use_buyer_display_settings_v2_x2:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_file_size_analytics(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;
    .locals 0

    .line 15773
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->use_file_size_analytics:Ljava/lang/Boolean;

    return-object p0
.end method

.method public x2_use_payment_workflows(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;
    .locals 0

    .line 15765
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$SquareDevice$Builder;->x2_use_payment_workflows:Ljava/lang/Boolean;

    return-object p0
.end method
