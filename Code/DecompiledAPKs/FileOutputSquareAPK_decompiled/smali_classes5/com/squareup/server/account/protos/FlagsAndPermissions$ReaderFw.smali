.class public final Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;
.super Lcom/squareup/wire/AndroidMessage;
.source "FlagsAndPermissions.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ReaderFw"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$ProtoAdapter_ReaderFw;,
        Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ACCOUNT_TYPE_SELECTION:Ljava/lang/Boolean;

.field public static final DEFAULT_COMMON_DEBIT_SUPPORT:Ljava/lang/Boolean;

.field public static final DEFAULT_FELICA_NOTIFICATION:Ljava/lang/Boolean;

.field public static final DEFAULT_PINBLOCK_FORMAT_V2:Ljava/lang/Boolean;

.field public static final DEFAULT_QUICKCHIP_FW209030:Ljava/lang/Boolean;

.field public static final DEFAULT_SONIC_BRANDING:Ljava/lang/Boolean;

.field public static final DEFAULT_SPOC_PRNG_SEED:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final account_type_selection:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3
    .end annotation
.end field

.field public final common_debit_support:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x5
    .end annotation
.end field

.field public final felica_notification:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x7
    .end annotation
.end field

.field public final pinblock_format_v2:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2
    .end annotation
.end field

.field public final quickchip_fw209030:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1
    .end annotation
.end field

.field public final sonic_branding:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x6
    .end annotation
.end field

.field public final spoc_prng_seed:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x4
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 14013
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$ProtoAdapter_ReaderFw;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$ProtoAdapter_ReaderFw;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 14015
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->CREATOR:Landroid/os/Parcelable$Creator;

    const/4 v0, 0x0

    .line 14019
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->DEFAULT_QUICKCHIP_FW209030:Ljava/lang/Boolean;

    .line 14021
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->DEFAULT_PINBLOCK_FORMAT_V2:Ljava/lang/Boolean;

    .line 14023
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->DEFAULT_ACCOUNT_TYPE_SELECTION:Ljava/lang/Boolean;

    .line 14025
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->DEFAULT_SPOC_PRNG_SEED:Ljava/lang/Boolean;

    .line 14027
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->DEFAULT_COMMON_DEBIT_SUPPORT:Ljava/lang/Boolean;

    .line 14029
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->DEFAULT_SONIC_BRANDING:Ljava/lang/Boolean;

    .line 14031
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->DEFAULT_FELICA_NOTIFICATION:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 9

    .line 14086
    sget-object v8, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1

    .line 14093
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p8}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 14094
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->quickchip_fw209030:Ljava/lang/Boolean;

    .line 14095
    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->pinblock_format_v2:Ljava/lang/Boolean;

    .line 14096
    iput-object p3, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->account_type_selection:Ljava/lang/Boolean;

    .line 14097
    iput-object p4, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->spoc_prng_seed:Ljava/lang/Boolean;

    .line 14098
    iput-object p5, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->common_debit_support:Ljava/lang/Boolean;

    .line 14099
    iput-object p6, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->sonic_branding:Ljava/lang/Boolean;

    .line 14100
    iput-object p7, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->felica_notification:Ljava/lang/Boolean;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 14189
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 14120
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 14121
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;

    .line 14122
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->quickchip_fw209030:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->quickchip_fw209030:Ljava/lang/Boolean;

    .line 14123
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->pinblock_format_v2:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->pinblock_format_v2:Ljava/lang/Boolean;

    .line 14124
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->account_type_selection:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->account_type_selection:Ljava/lang/Boolean;

    .line 14125
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->spoc_prng_seed:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->spoc_prng_seed:Ljava/lang/Boolean;

    .line 14126
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->common_debit_support:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->common_debit_support:Ljava/lang/Boolean;

    .line 14127
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->sonic_branding:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->sonic_branding:Ljava/lang/Boolean;

    .line 14128
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->felica_notification:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->felica_notification:Ljava/lang/Boolean;

    .line 14129
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 14134
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_7

    .line 14136
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 14137
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->quickchip_fw209030:Ljava/lang/Boolean;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 14138
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->pinblock_format_v2:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 14139
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->account_type_selection:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 14140
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->spoc_prng_seed:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 14141
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->common_debit_support:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 14142
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->sonic_branding:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 14143
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->felica_notification:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_6
    add-int/2addr v0, v2

    .line 14144
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_7
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;
    .locals 2

    .line 14105
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;-><init>()V

    .line 14106
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->quickchip_fw209030:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;->quickchip_fw209030:Ljava/lang/Boolean;

    .line 14107
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->pinblock_format_v2:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;->pinblock_format_v2:Ljava/lang/Boolean;

    .line 14108
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->account_type_selection:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;->account_type_selection:Ljava/lang/Boolean;

    .line 14109
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->spoc_prng_seed:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;->spoc_prng_seed:Ljava/lang/Boolean;

    .line 14110
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->common_debit_support:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;->common_debit_support:Ljava/lang/Boolean;

    .line 14111
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->sonic_branding:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;->sonic_branding:Ljava/lang/Boolean;

    .line 14112
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->felica_notification:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;->felica_notification:Ljava/lang/Boolean;

    .line 14113
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 14012
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;)Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;
    .locals 2

    .line 14178
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->quickchip_fw209030:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->quickchip_fw209030:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;->quickchip_fw209030(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;

    move-result-object v1

    .line 14179
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->pinblock_format_v2:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->pinblock_format_v2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;->pinblock_format_v2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;

    move-result-object v1

    .line 14180
    :cond_1
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->account_type_selection:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->account_type_selection:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;->account_type_selection(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;

    move-result-object v1

    .line 14181
    :cond_2
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->spoc_prng_seed:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->spoc_prng_seed:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;->spoc_prng_seed(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;

    move-result-object v1

    .line 14182
    :cond_3
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->common_debit_support:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->common_debit_support:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;->common_debit_support(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;

    move-result-object v1

    .line 14183
    :cond_4
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->sonic_branding:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->sonic_branding:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;->sonic_branding(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;

    move-result-object v1

    .line 14184
    :cond_5
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->felica_notification:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->felica_notification:Ljava/lang/Boolean;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;->felica_notification(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;

    move-result-object v1

    :cond_6
    if-nez v1, :cond_7

    move-object p1, p0

    goto :goto_0

    .line 14185
    :cond_7
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 14012
    check-cast p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->overlay(Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;)Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;
    .locals 2

    .line 14165
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->quickchip_fw209030:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->DEFAULT_QUICKCHIP_FW209030:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;->quickchip_fw209030(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;

    move-result-object v1

    .line 14166
    :cond_0
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->pinblock_format_v2:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->DEFAULT_PINBLOCK_FORMAT_V2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;->pinblock_format_v2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;

    move-result-object v1

    .line 14167
    :cond_1
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->account_type_selection:Ljava/lang/Boolean;

    if-nez v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->DEFAULT_ACCOUNT_TYPE_SELECTION:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;->account_type_selection(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;

    move-result-object v1

    .line 14168
    :cond_2
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->spoc_prng_seed:Ljava/lang/Boolean;

    if-nez v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->DEFAULT_SPOC_PRNG_SEED:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;->spoc_prng_seed(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;

    move-result-object v1

    .line 14169
    :cond_3
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->common_debit_support:Ljava/lang/Boolean;

    if-nez v0, :cond_4

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->DEFAULT_COMMON_DEBIT_SUPPORT:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;->common_debit_support(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;

    move-result-object v1

    .line 14170
    :cond_4
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->sonic_branding:Ljava/lang/Boolean;

    if-nez v0, :cond_5

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->DEFAULT_SONIC_BRANDING:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;->sonic_branding(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;

    move-result-object v1

    .line 14171
    :cond_5
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->felica_notification:Ljava/lang/Boolean;

    if-nez v0, :cond_6

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->DEFAULT_FELICA_NOTIFICATION:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;->felica_notification(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;

    move-result-object v1

    :cond_6
    if-nez v1, :cond_7

    move-object v0, p0

    goto :goto_0

    .line 14172
    :cond_7
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 14012
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 14151
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 14152
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->quickchip_fw209030:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const-string v1, ", quickchip_fw209030="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->quickchip_fw209030:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 14153
    :cond_0
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->pinblock_format_v2:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    const-string v1, ", pinblock_format_v2="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->pinblock_format_v2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 14154
    :cond_1
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->account_type_selection:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", account_type_selection="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->account_type_selection:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 14155
    :cond_2
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->spoc_prng_seed:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    const-string v1, ", spoc_prng_seed="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->spoc_prng_seed:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 14156
    :cond_3
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->common_debit_support:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    const-string v1, ", common_debit_support="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->common_debit_support:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 14157
    :cond_4
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->sonic_branding:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    const-string v1, ", sonic_branding="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->sonic_branding:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 14158
    :cond_5
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->felica_notification:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    const-string v1, ", felica_notification="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$ReaderFw;->felica_notification:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_6
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ReaderFw{"

    .line 14159
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
