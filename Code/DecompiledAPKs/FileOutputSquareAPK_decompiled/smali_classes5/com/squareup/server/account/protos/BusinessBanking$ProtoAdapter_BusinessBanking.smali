.class final Lcom/squareup/server/account/protos/BusinessBanking$ProtoAdapter_BusinessBanking;
.super Lcom/squareup/wire/ProtoAdapter;
.source "BusinessBanking.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/BusinessBanking;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_BusinessBanking"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/server/account/protos/BusinessBanking;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 121
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/server/account/protos/BusinessBanking;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/server/account/protos/BusinessBanking;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 138
    new-instance v0, Lcom/squareup/server/account/protos/BusinessBanking$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/BusinessBanking$Builder;-><init>()V

    .line 139
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 140
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    .line 144
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 142
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/BusinessBanking$Builder;->show_card_spend(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/BusinessBanking$Builder;

    goto :goto_0

    .line 148
    :cond_1
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/BusinessBanking$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 149
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/BusinessBanking$Builder;->build()Lcom/squareup/server/account/protos/BusinessBanking;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 119
    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/BusinessBanking$ProtoAdapter_BusinessBanking;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/server/account/protos/BusinessBanking;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/server/account/protos/BusinessBanking;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 132
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/BusinessBanking;->show_card_spend:Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 133
    invoke-virtual {p2}, Lcom/squareup/server/account/protos/BusinessBanking;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 119
    check-cast p2, Lcom/squareup/server/account/protos/BusinessBanking;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/server/account/protos/BusinessBanking$ProtoAdapter_BusinessBanking;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/server/account/protos/BusinessBanking;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/server/account/protos/BusinessBanking;)I
    .locals 3

    .line 126
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/BusinessBanking;->show_card_spend:Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    .line 127
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/BusinessBanking;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 119
    check-cast p1, Lcom/squareup/server/account/protos/BusinessBanking;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/BusinessBanking$ProtoAdapter_BusinessBanking;->encodedSize(Lcom/squareup/server/account/protos/BusinessBanking;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/server/account/protos/BusinessBanking;)Lcom/squareup/server/account/protos/BusinessBanking;
    .locals 0

    .line 154
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/BusinessBanking;->newBuilder()Lcom/squareup/server/account/protos/BusinessBanking$Builder;

    move-result-object p1

    .line 155
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/BusinessBanking$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 156
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/BusinessBanking$Builder;->build()Lcom/squareup/server/account/protos/BusinessBanking;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 119
    check-cast p1, Lcom/squareup/server/account/protos/BusinessBanking;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/BusinessBanking$ProtoAdapter_BusinessBanking;->redact(Lcom/squareup/server/account/protos/BusinessBanking;)Lcom/squareup/server/account/protos/BusinessBanking;

    move-result-object p1

    return-object p1
.end method
