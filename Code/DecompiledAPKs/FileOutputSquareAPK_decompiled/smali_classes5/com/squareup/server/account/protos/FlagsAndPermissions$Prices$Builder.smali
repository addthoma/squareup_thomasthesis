.class public final Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FlagsAndPermissions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public enable_tax_basis:Ljava/lang/Boolean;

.field public enable_whole_purchase_discounts:Ljava/lang/Boolean;

.field public use_pricing_engine_heuristic:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 18001
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;
    .locals 5

    .line 18031
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices$Builder;->use_pricing_engine_heuristic:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices$Builder;->enable_tax_basis:Ljava/lang/Boolean;

    iget-object v3, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices$Builder;->enable_whole_purchase_discounts:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 17994
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices;

    move-result-object v0

    return-object v0
.end method

.method public enable_tax_basis(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices$Builder;
    .locals 0

    .line 18017
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices$Builder;->enable_tax_basis:Ljava/lang/Boolean;

    return-object p0
.end method

.method public enable_whole_purchase_discounts(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices$Builder;
    .locals 0

    .line 18025
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices$Builder;->enable_whole_purchase_discounts:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_pricing_engine_heuristic(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 18009
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Prices$Builder;->use_pricing_engine_heuristic:Ljava/lang/Boolean;

    return-object p0
.end method
