.class public final Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "AccrualRules.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;",
        "Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public exclude_comp_items:Ljava/lang/Boolean;

.field public exclude_discounted_items:Ljava/lang/Boolean;

.field public exclude_reward_items:Ljava/lang/Boolean;

.field public excluded_catalog_objects:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/server/account/protos/AccrualRules$CatalogObject;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1361
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 1362
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;->excluded_catalog_objects:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;
    .locals 7

    .line 1409
    new-instance v6, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;

    iget-object v1, p0, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;->exclude_reward_items:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;->exclude_discounted_items:Ljava/lang/Boolean;

    iget-object v3, p0, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;->exclude_comp_items:Ljava/lang/Boolean;

    iget-object v4, p0, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;->excluded_catalog_objects:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/util/List;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 1352
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;->build()Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;

    move-result-object v0

    return-object v0
.end method

.method public exclude_comp_items(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;
    .locals 0

    .line 1392
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;->exclude_comp_items:Ljava/lang/Boolean;

    return-object p0
.end method

.method public exclude_discounted_items(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;
    .locals 0

    .line 1382
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;->exclude_discounted_items:Ljava/lang/Boolean;

    return-object p0
.end method

.method public exclude_reward_items(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;
    .locals 0

    .line 1372
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;->exclude_reward_items:Ljava/lang/Boolean;

    return-object p0
.end method

.method public excluded_catalog_objects(Ljava/util/List;)Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/server/account/protos/AccrualRules$CatalogObject;",
            ">;)",
            "Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;"
        }
    .end annotation

    .line 1402
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 1403
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;->excluded_catalog_objects:Ljava/util/List;

    return-object p0
.end method
