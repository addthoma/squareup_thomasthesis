.class public final Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;
.super Lcom/squareup/wire/AndroidMessage;
.source "FlagsAndPermissions.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Timecards"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards$ProtoAdapter_Timecards;,
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_BREAK_TRACKING_ENABLED:Ljava/lang/Boolean;

.field public static final DEFAULT_MANDATORY_BREAK_COMPLETION:Ljava/lang/Boolean;

.field public static final DEFAULT_RECEIPT_SUMMARY:Ljava/lang/Boolean;

.field public static final DEFAULT_TEAM_MEMBER_NOTES:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final break_tracking_enabled:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1
    .end annotation
.end field

.field public final mandatory_break_completion:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3
    .end annotation
.end field

.field public final receipt_summary:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2
    .end annotation
.end field

.field public final team_member_notes:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x4
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 12747
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards$ProtoAdapter_Timecards;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards$ProtoAdapter_Timecards;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 12749
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->CREATOR:Landroid/os/Parcelable$Creator;

    const/4 v0, 0x0

    .line 12753
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->DEFAULT_BREAK_TRACKING_ENABLED:Ljava/lang/Boolean;

    .line 12755
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->DEFAULT_RECEIPT_SUMMARY:Ljava/lang/Boolean;

    .line 12757
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->DEFAULT_MANDATORY_BREAK_COMPLETION:Ljava/lang/Boolean;

    .line 12759
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->DEFAULT_TEAM_MEMBER_NOTES:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 6

    .line 12804
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1

    .line 12810
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 12811
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->break_tracking_enabled:Ljava/lang/Boolean;

    .line 12812
    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->receipt_summary:Ljava/lang/Boolean;

    .line 12813
    iput-object p3, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->mandatory_break_completion:Ljava/lang/Boolean;

    .line 12814
    iput-object p4, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->team_member_notes:Ljava/lang/Boolean;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 12885
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 12831
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 12832
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;

    .line 12833
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->break_tracking_enabled:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->break_tracking_enabled:Ljava/lang/Boolean;

    .line 12834
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->receipt_summary:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->receipt_summary:Ljava/lang/Boolean;

    .line 12835
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->mandatory_break_completion:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->mandatory_break_completion:Ljava/lang/Boolean;

    .line 12836
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->team_member_notes:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->team_member_notes:Ljava/lang/Boolean;

    .line 12837
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 12842
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_4

    .line 12844
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 12845
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->break_tracking_enabled:Ljava/lang/Boolean;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 12846
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->receipt_summary:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 12847
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->mandatory_break_completion:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 12848
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->team_member_notes:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 12849
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards$Builder;
    .locals 2

    .line 12819
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards$Builder;-><init>()V

    .line 12820
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->break_tracking_enabled:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards$Builder;->break_tracking_enabled:Ljava/lang/Boolean;

    .line 12821
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->receipt_summary:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards$Builder;->receipt_summary:Ljava/lang/Boolean;

    .line 12822
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->mandatory_break_completion:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards$Builder;->mandatory_break_completion:Ljava/lang/Boolean;

    .line 12823
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->team_member_notes:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards$Builder;->team_member_notes:Ljava/lang/Boolean;

    .line 12824
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 12746
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;
    .locals 2

    .line 12877
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->break_tracking_enabled:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->break_tracking_enabled:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards$Builder;->break_tracking_enabled(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards$Builder;

    move-result-object v1

    .line 12878
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->receipt_summary:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->receipt_summary:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards$Builder;->receipt_summary(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards$Builder;

    move-result-object v1

    .line 12879
    :cond_1
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->mandatory_break_completion:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->mandatory_break_completion:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards$Builder;->mandatory_break_completion(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards$Builder;

    move-result-object v1

    .line 12880
    :cond_2
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->team_member_notes:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->team_member_notes:Ljava/lang/Boolean;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards$Builder;->team_member_notes(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards$Builder;

    move-result-object v1

    :cond_3
    if-nez v1, :cond_4

    move-object p1, p0

    goto :goto_0

    .line 12881
    :cond_4
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 12746
    check-cast p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->overlay(Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;
    .locals 2

    .line 12867
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->break_tracking_enabled:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->DEFAULT_BREAK_TRACKING_ENABLED:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards$Builder;->break_tracking_enabled(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards$Builder;

    move-result-object v1

    .line 12868
    :cond_0
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->receipt_summary:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->DEFAULT_RECEIPT_SUMMARY:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards$Builder;->receipt_summary(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards$Builder;

    move-result-object v1

    .line 12869
    :cond_1
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->mandatory_break_completion:Ljava/lang/Boolean;

    if-nez v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->DEFAULT_MANDATORY_BREAK_COMPLETION:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards$Builder;->mandatory_break_completion(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards$Builder;

    move-result-object v1

    .line 12870
    :cond_2
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->team_member_notes:Ljava/lang/Boolean;

    if-nez v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->DEFAULT_TEAM_MEMBER_NOTES:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards$Builder;->team_member_notes(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards$Builder;

    move-result-object v1

    :cond_3
    if-nez v1, :cond_4

    move-object v0, p0

    goto :goto_0

    .line 12871
    :cond_4
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 12746
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 12856
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 12857
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->break_tracking_enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const-string v1, ", break_tracking_enabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->break_tracking_enabled:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 12858
    :cond_0
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->receipt_summary:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    const-string v1, ", receipt_summary="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->receipt_summary:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 12859
    :cond_1
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->mandatory_break_completion:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", mandatory_break_completion="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->mandatory_break_completion:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 12860
    :cond_2
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->team_member_notes:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    const-string v1, ", team_member_notes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Timecards;->team_member_notes:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Timecards{"

    .line 12861
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
