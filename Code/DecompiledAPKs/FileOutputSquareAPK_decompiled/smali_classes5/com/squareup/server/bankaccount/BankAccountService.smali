.class public interface abstract Lcom/squareup/server/bankaccount/BankAccountService;
.super Ljava/lang/Object;
.source "BankAccountService.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/bankaccount/BankAccountService$BankAccountStandardResponse;,
        Lcom/squareup/server/bankaccount/BankAccountService$GetDirectDebitInfoStandardResponse;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000`\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008f\u0018\u00002\u00020\u0001:\u0002\u001b\u001cJ\u0018\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0006H\'J\u0018\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u00082\u0008\u0008\u0001\u0010\u0005\u001a\u00020\nH\'J\u0018\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000c2\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u000eH\'J\u0012\u0010\u000f\u001a\u00020\u00102\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0011H\'J\u0018\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u00130\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0014H\'J\u0018\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u00160\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u0017H\'J\u0018\u0010\u0018\u001a\u0008\u0012\u0004\u0012\u00020\u00190\u000c2\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u001aH\'\u00a8\u0006\u001d"
    }
    d2 = {
        "Lcom/squareup/server/bankaccount/BankAccountService;",
        "",
        "cancelVerification",
        "Lcom/squareup/server/bankaccount/BankAccountService$BankAccountStandardResponse;",
        "Lcom/squareup/protos/client/bankaccount/CancelVerificationResponse;",
        "request",
        "Lcom/squareup/protos/client/bankaccount/CancelVerificationRequest;",
        "confirmBankAccount",
        "Lcom/squareup/server/StatusResponse;",
        "Lcom/squareup/protos/client/bankaccount/ConfirmBankAccountResponse;",
        "Lcom/squareup/protos/client/bankaccount/ConfirmBankAccountRequest;",
        "getBankAccounts",
        "Lcom/squareup/server/AcceptedResponse;",
        "Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse;",
        "Lcom/squareup/protos/client/bankaccount/GetBankAccountsRequest;",
        "getDirectDebitInfo",
        "Lcom/squareup/server/bankaccount/BankAccountService$GetDirectDebitInfoStandardResponse;",
        "Lcom/squareup/protos/deposits/GetDirectDebitInfoRequest;",
        "getLatestBankAccount",
        "Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse;",
        "Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountRequest;",
        "linkBankAccount",
        "Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse;",
        "Lcom/squareup/protos/client/bankaccount/LinkBankAccountRequest;",
        "resendVerificationEmail",
        "Lcom/squareup/protos/client/bankaccount/ResendVerificationEmailResponse;",
        "Lcom/squareup/protos/client/bankaccount/ResendVerificationEmailRequest;",
        "BankAccountStandardResponse",
        "GetDirectDebitInfoStandardResponse",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract cancelVerification(Lcom/squareup/protos/client/bankaccount/CancelVerificationRequest;)Lcom/squareup/server/bankaccount/BankAccountService$BankAccountStandardResponse;
    .param p1    # Lcom/squareup/protos/client/bankaccount/CancelVerificationRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bankaccount/CancelVerificationRequest;",
            ")",
            "Lcom/squareup/server/bankaccount/BankAccountService$BankAccountStandardResponse<",
            "Lcom/squareup/protos/client/bankaccount/CancelVerificationResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/bank-account/cancel-verification"
    .end annotation
.end method

.method public abstract confirmBankAccount(Lcom/squareup/protos/client/bankaccount/ConfirmBankAccountRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/bankaccount/ConfirmBankAccountRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bankaccount/ConfirmBankAccountRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/bankaccount/ConfirmBankAccountResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/bank-account/confirm"
    .end annotation
.end method

.method public abstract getBankAccounts(Lcom/squareup/protos/client/bankaccount/GetBankAccountsRequest;)Lcom/squareup/server/AcceptedResponse;
    .param p1    # Lcom/squareup/protos/client/bankaccount/GetBankAccountsRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bankaccount/GetBankAccountsRequest;",
            ")",
            "Lcom/squareup/server/AcceptedResponse<",
            "Lcom/squareup/protos/client/bankaccount/GetBankAccountsResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/bank-account/get-bank-accounts"
    .end annotation
.end method

.method public abstract getDirectDebitInfo(Lcom/squareup/protos/deposits/GetDirectDebitInfoRequest;)Lcom/squareup/server/bankaccount/BankAccountService$GetDirectDebitInfoStandardResponse;
    .param p1    # Lcom/squareup/protos/deposits/GetDirectDebitInfoRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/bank-account/direct-debit-info"
    .end annotation
.end method

.method public abstract getLatestBankAccount(Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountRequest;)Lcom/squareup/server/bankaccount/BankAccountService$BankAccountStandardResponse;
    .param p1    # Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountRequest;",
            ")",
            "Lcom/squareup/server/bankaccount/BankAccountService$BankAccountStandardResponse<",
            "Lcom/squareup/protos/client/bankaccount/GetLatestBankAccountResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/bank-account/get-latest-bank-account"
    .end annotation
.end method

.method public abstract linkBankAccount(Lcom/squareup/protos/client/bankaccount/LinkBankAccountRequest;)Lcom/squareup/server/bankaccount/BankAccountService$BankAccountStandardResponse;
    .param p1    # Lcom/squareup/protos/client/bankaccount/LinkBankAccountRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bankaccount/LinkBankAccountRequest;",
            ")",
            "Lcom/squareup/server/bankaccount/BankAccountService$BankAccountStandardResponse<",
            "Lcom/squareup/protos/client/bankaccount/LinkBankAccountResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/bank-account/link-bank-account"
    .end annotation
.end method

.method public abstract resendVerificationEmail(Lcom/squareup/protos/client/bankaccount/ResendVerificationEmailRequest;)Lcom/squareup/server/AcceptedResponse;
    .param p1    # Lcom/squareup/protos/client/bankaccount/ResendVerificationEmailRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bankaccount/ResendVerificationEmailRequest;",
            ")",
            "Lcom/squareup/server/AcceptedResponse<",
            "Lcom/squareup/protos/client/bankaccount/ResendVerificationEmailResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/bank-account/resend-verification-email"
    .end annotation
.end method
