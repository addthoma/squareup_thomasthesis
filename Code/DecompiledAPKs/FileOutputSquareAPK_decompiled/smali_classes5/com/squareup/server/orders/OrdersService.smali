.class public interface abstract Lcom/squareup/server/orders/OrdersService;
.super Ljava/lang/Object;
.source "OrdersService.java"


# virtual methods
.method public abstract getOrders(Lcom/squareup/protos/client/solidshop/GetOrderStatusRequest;)Lrx/Observable;
    .param p1    # Lcom/squareup/protos/client/solidshop/GetOrderStatusRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/solidshop/GetOrderStatusRequest;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/solidshop/GetOrderStatusResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/1.0/solidshop/get-order-status"
    .end annotation
.end method
