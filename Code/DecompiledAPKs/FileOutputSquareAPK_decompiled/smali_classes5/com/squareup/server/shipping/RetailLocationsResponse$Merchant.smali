.class public Lcom/squareup/server/shipping/RetailLocationsResponse$Merchant;
.super Ljava/lang/Object;
.source "RetailLocationsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/shipping/RetailLocationsResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Merchant"
.end annotation


# instance fields
.field public final city:Ljava/lang/String;

.field public final company:Ljava/lang/String;

.field public final country_code:Ljava/lang/String;

.field public final id:Ljava/lang/String;

.field public final phone_number:Ljava/lang/String;

.field public final position:Lcom/squareup/server/shipping/RetailLocationsResponse$LatLng;

.field public final postal_code:Ljava/lang/String;

.field public final state:Ljava/lang/String;

.field public final street1:Ljava/lang/String;

.field public final street2:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/server/shipping/RetailLocationsResponse$LatLng;)V
    .locals 0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/server/shipping/RetailLocationsResponse$Merchant;->company:Ljava/lang/String;

    .line 27
    iput-object p2, p0, Lcom/squareup/server/shipping/RetailLocationsResponse$Merchant;->street1:Ljava/lang/String;

    .line 28
    iput-object p3, p0, Lcom/squareup/server/shipping/RetailLocationsResponse$Merchant;->street2:Ljava/lang/String;

    .line 29
    iput-object p4, p0, Lcom/squareup/server/shipping/RetailLocationsResponse$Merchant;->city:Ljava/lang/String;

    .line 30
    iput-object p5, p0, Lcom/squareup/server/shipping/RetailLocationsResponse$Merchant;->state:Ljava/lang/String;

    .line 31
    iput-object p6, p0, Lcom/squareup/server/shipping/RetailLocationsResponse$Merchant;->postal_code:Ljava/lang/String;

    .line 32
    iput-object p7, p0, Lcom/squareup/server/shipping/RetailLocationsResponse$Merchant;->country_code:Ljava/lang/String;

    .line 33
    iput-object p8, p0, Lcom/squareup/server/shipping/RetailLocationsResponse$Merchant;->phone_number:Ljava/lang/String;

    .line 34
    iput-object p9, p0, Lcom/squareup/server/shipping/RetailLocationsResponse$Merchant;->id:Ljava/lang/String;

    .line 35
    iput-object p10, p0, Lcom/squareup/server/shipping/RetailLocationsResponse$Merchant;->position:Lcom/squareup/server/shipping/RetailLocationsResponse$LatLng;

    return-void
.end method
