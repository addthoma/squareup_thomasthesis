.class public abstract Lcom/squareup/server/StandardResponse;
.super Ljava/lang/Object;
.source "StandardResponse.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/StandardResponse$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008&\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u00022\u00020\u0002:\u0001\u0011B\u0013\u0012\u000c\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0004\u00a2\u0006\u0002\u0010\u0005J\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0007J\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00028\u00000\tJ\u0015\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00028\u0000H$\u00a2\u0006\u0002\u0010\rJ\u0012\u0010\u000e\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00028\u00000\u00070\u000fJ\u0012\u0010\u0010\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00028\u00000\t0\u000fR\u0014\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/server/StandardResponse;",
        "R",
        "",
        "factory",
        "Lcom/squareup/server/StandardResponse$Factory;",
        "(Lcom/squareup/server/StandardResponse$Factory;)V",
        "blocking",
        "Lcom/squareup/receiving/ReceivedResponse;",
        "blockingSuccessOrFailure",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "isSuccessful",
        "",
        "response",
        "(Ljava/lang/Object;)Z",
        "receivedResponse",
        "Lio/reactivex/Single;",
        "successOrFailure",
        "Factory",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final factory:Lcom/squareup/server/StandardResponse$Factory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/server/StandardResponse$Factory<",
            "TR;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/server/StandardResponse$Factory;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/StandardResponse$Factory<",
            "TR;>;)V"
        }
    .end annotation

    const-string v0, "factory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/server/StandardResponse;->factory:Lcom/squareup/server/StandardResponse$Factory;

    return-void
.end method


# virtual methods
.method public final blocking()Lcom/squareup/receiving/ReceivedResponse;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/receiving/ReceivedResponse<",
            "TR;>;"
        }
    .end annotation

    .line 104
    iget-object v0, p0, Lcom/squareup/server/StandardResponse;->factory:Lcom/squareup/server/StandardResponse$Factory;

    new-instance v1, Lcom/squareup/server/StandardResponse$blocking$1;

    move-object v2, p0

    check-cast v2, Lcom/squareup/server/StandardResponse;

    invoke-direct {v1, v2}, Lcom/squareup/server/StandardResponse$blocking$1;-><init>(Lcom/squareup/server/StandardResponse;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-interface {v0, v1}, Lcom/squareup/server/StandardResponse$Factory;->blocking(Lkotlin/jvm/functions/Function1;)Lcom/squareup/receiving/ReceivedResponse;

    move-result-object v0

    return-object v0
.end method

.method public final blockingSuccessOrFailure()Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "TR;>;"
        }
    .end annotation

    .line 97
    iget-object v0, p0, Lcom/squareup/server/StandardResponse;->factory:Lcom/squareup/server/StandardResponse$Factory;

    new-instance v1, Lcom/squareup/server/StandardResponse$blockingSuccessOrFailure$1;

    move-object v2, p0

    check-cast v2, Lcom/squareup/server/StandardResponse;

    invoke-direct {v1, v2}, Lcom/squareup/server/StandardResponse$blockingSuccessOrFailure$1;-><init>(Lcom/squareup/server/StandardResponse;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-interface {v0, v1}, Lcom/squareup/server/StandardResponse$Factory;->blockingSuccessOrFailure(Lkotlin/jvm/functions/Function1;)Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    move-result-object v0

    return-object v0
.end method

.method protected abstract isSuccessful(Ljava/lang/Object;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;)Z"
        }
    .end annotation
.end method

.method public final receivedResponse()Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/ReceivedResponse<",
            "TR;>;>;"
        }
    .end annotation

    .line 88
    iget-object v0, p0, Lcom/squareup/server/StandardResponse;->factory:Lcom/squareup/server/StandardResponse$Factory;

    new-instance v1, Lcom/squareup/server/StandardResponse$receivedResponse$1;

    move-object v2, p0

    check-cast v2, Lcom/squareup/server/StandardResponse;

    invoke-direct {v1, v2}, Lcom/squareup/server/StandardResponse$receivedResponse$1;-><init>(Lcom/squareup/server/StandardResponse;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-interface {v0, v1}, Lcom/squareup/server/StandardResponse$Factory;->receivedResponse(Lkotlin/jvm/functions/Function1;)Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method

.method public final successOrFailure()Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "TR;>;>;"
        }
    .end annotation

    .line 81
    iget-object v0, p0, Lcom/squareup/server/StandardResponse;->factory:Lcom/squareup/server/StandardResponse$Factory;

    new-instance v1, Lcom/squareup/server/StandardResponse$successOrFailure$1;

    move-object v2, p0

    check-cast v2, Lcom/squareup/server/StandardResponse;

    invoke-direct {v1, v2}, Lcom/squareup/server/StandardResponse$successOrFailure$1;-><init>(Lcom/squareup/server/StandardResponse;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-interface {v0, v1}, Lcom/squareup/server/StandardResponse$Factory;->successOrFailure(Lkotlin/jvm/functions/Function1;)Lio/reactivex/Single;

    move-result-object v0

    return-object v0
.end method
