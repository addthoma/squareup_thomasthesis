.class public Lcom/squareup/server/payment/LegacyRefundRequest;
.super Ljava/lang/Object;
.source "LegacyRefundRequest.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final NO_ENCRYPTED_CARD_DATA:Ljava/lang/String;

.field private static final NO_ENCRYPTED_TRACK2:Ljava/lang/String;

.field private static final NO_PAN:Ljava/lang/String;

.field private static final NO_TRACK2:Ljava/lang/String;


# instance fields
.field public final encrypted_card_data:Ljava/lang/String;

.field public final encrypted_track_2:Ljava/lang/String;

.field public final pan:Ljava/lang/String;

.field public final payment_id:Ljava/lang/String;

.field public final reason:Ljava/lang/String;

.field public final refund_money:Lcom/squareup/protos/common/Money;

.field public final refund_request_idempotence_key:Ljava/lang/String;

.field public final track_2:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;)V
    .locals 0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/server/payment/LegacyRefundRequest;->payment_id:Ljava/lang/String;

    .line 26
    iput-object p2, p0, Lcom/squareup/server/payment/LegacyRefundRequest;->pan:Ljava/lang/String;

    .line 27
    iput-object p3, p0, Lcom/squareup/server/payment/LegacyRefundRequest;->track_2:Ljava/lang/String;

    .line 28
    iput-object p4, p0, Lcom/squareup/server/payment/LegacyRefundRequest;->encrypted_track_2:Ljava/lang/String;

    .line 29
    iput-object p5, p0, Lcom/squareup/server/payment/LegacyRefundRequest;->encrypted_card_data:Ljava/lang/String;

    .line 30
    iput-object p6, p0, Lcom/squareup/server/payment/LegacyRefundRequest;->reason:Ljava/lang/String;

    .line 31
    iput-object p7, p0, Lcom/squareup/server/payment/LegacyRefundRequest;->refund_request_idempotence_key:Ljava/lang/String;

    .line 32
    iput-object p8, p0, Lcom/squareup/server/payment/LegacyRefundRequest;->refund_money:Lcom/squareup/protos/common/Money;

    return-void
.end method

.method public static forExemptCard(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;)Lcom/squareup/server/payment/LegacyRefundRequest;
    .locals 0

    .line 43
    invoke-static {p0, p1, p2, p3}, Lcom/squareup/server/payment/LegacyRefundRequest;->forNonCard(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;)Lcom/squareup/server/payment/LegacyRefundRequest;

    move-result-object p0

    return-object p0
.end method

.method public static forM1(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;)Lcom/squareup/server/payment/LegacyRefundRequest;
    .locals 10

    .line 66
    new-instance v9, Lcom/squareup/server/payment/LegacyRefundRequest;

    sget-object v2, Lcom/squareup/server/payment/LegacyRefundRequest;->NO_PAN:Ljava/lang/String;

    sget-object v3, Lcom/squareup/server/payment/LegacyRefundRequest;->NO_TRACK2:Ljava/lang/String;

    sget-object v4, Lcom/squareup/server/payment/LegacyRefundRequest;->NO_ENCRYPTED_TRACK2:Ljava/lang/String;

    move-object v0, v9

    move-object v1, p0

    move-object v5, p1

    move-object v6, p2

    move-object v7, p3

    move-object v8, p4

    invoke-direct/range {v0 .. v8}, Lcom/squareup/server/payment/LegacyRefundRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;)V

    return-object v9
.end method

.method public static forManual(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;)Lcom/squareup/server/payment/LegacyRefundRequest;
    .locals 10

    .line 48
    new-instance v9, Lcom/squareup/server/payment/LegacyRefundRequest;

    sget-object v3, Lcom/squareup/server/payment/LegacyRefundRequest;->NO_TRACK2:Ljava/lang/String;

    sget-object v4, Lcom/squareup/server/payment/LegacyRefundRequest;->NO_ENCRYPTED_TRACK2:Ljava/lang/String;

    sget-object v5, Lcom/squareup/server/payment/LegacyRefundRequest;->NO_ENCRYPTED_CARD_DATA:Ljava/lang/String;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v6, p2

    move-object v7, p3

    move-object v8, p4

    invoke-direct/range {v0 .. v8}, Lcom/squareup/server/payment/LegacyRefundRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;)V

    return-object v9
.end method

.method public static forNonCard(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;)Lcom/squareup/server/payment/LegacyRefundRequest;
    .locals 10

    .line 37
    new-instance v9, Lcom/squareup/server/payment/LegacyRefundRequest;

    sget-object v2, Lcom/squareup/server/payment/LegacyRefundRequest;->NO_PAN:Ljava/lang/String;

    sget-object v3, Lcom/squareup/server/payment/LegacyRefundRequest;->NO_TRACK2:Ljava/lang/String;

    sget-object v4, Lcom/squareup/server/payment/LegacyRefundRequest;->NO_ENCRYPTED_TRACK2:Ljava/lang/String;

    sget-object v5, Lcom/squareup/server/payment/LegacyRefundRequest;->NO_ENCRYPTED_CARD_DATA:Ljava/lang/String;

    move-object v0, v9

    move-object v1, p0

    move-object v6, p1

    move-object v7, p2

    move-object v8, p3

    invoke-direct/range {v0 .. v8}, Lcom/squareup/server/payment/LegacyRefundRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;)V

    return-object v9
.end method

.method public static forO1(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;)Lcom/squareup/server/payment/LegacyRefundRequest;
    .locals 10

    .line 60
    new-instance v9, Lcom/squareup/server/payment/LegacyRefundRequest;

    sget-object v2, Lcom/squareup/server/payment/LegacyRefundRequest;->NO_PAN:Ljava/lang/String;

    sget-object v3, Lcom/squareup/server/payment/LegacyRefundRequest;->NO_TRACK2:Ljava/lang/String;

    sget-object v5, Lcom/squareup/server/payment/LegacyRefundRequest;->NO_ENCRYPTED_CARD_DATA:Ljava/lang/String;

    move-object v0, v9

    move-object v1, p0

    move-object v4, p1

    move-object v6, p2

    move-object v7, p3

    move-object v8, p4

    invoke-direct/range {v0 .. v8}, Lcom/squareup/server/payment/LegacyRefundRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;)V

    return-object v9
.end method

.method public static forSwiped(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;)Lcom/squareup/server/payment/LegacyRefundRequest;
    .locals 10

    .line 54
    new-instance v9, Lcom/squareup/server/payment/LegacyRefundRequest;

    sget-object v2, Lcom/squareup/server/payment/LegacyRefundRequest;->NO_PAN:Ljava/lang/String;

    sget-object v4, Lcom/squareup/server/payment/LegacyRefundRequest;->NO_ENCRYPTED_TRACK2:Ljava/lang/String;

    sget-object v5, Lcom/squareup/server/payment/LegacyRefundRequest;->NO_ENCRYPTED_CARD_DATA:Ljava/lang/String;

    move-object v0, v9

    move-object v1, p0

    move-object v3, p1

    move-object v6, p2

    move-object v7, p3

    move-object v8, p4

    invoke-direct/range {v0 .. v8}, Lcom/squareup/server/payment/LegacyRefundRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;)V

    return-object v9
.end method
