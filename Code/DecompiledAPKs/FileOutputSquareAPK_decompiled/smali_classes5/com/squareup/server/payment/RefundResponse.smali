.class public Lcom/squareup/server/payment/RefundResponse;
.super Lcom/squareup/server/SimpleResponse;
.source "RefundResponse.java"


# instance fields
.field public final created_at:Ljava/lang/String;

.field public final id:Ljava/lang/String;

.field public final processed_at:Ljava/lang/String;

.field public final reason:Ljava/lang/String;

.field public final refunded_money:Lcom/squareup/protos/common/Money;

.field public final status:Ljava/lang/String;


# direct methods
.method public constructor <init>(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 17
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/server/SimpleResponse;-><init>(ZLjava/lang/String;Ljava/lang/String;)V

    .line 18
    iput-object p4, p0, Lcom/squareup/server/payment/RefundResponse;->id:Ljava/lang/String;

    .line 19
    iput-object p5, p0, Lcom/squareup/server/payment/RefundResponse;->status:Ljava/lang/String;

    .line 20
    iput-object p6, p0, Lcom/squareup/server/payment/RefundResponse;->refunded_money:Lcom/squareup/protos/common/Money;

    .line 21
    iput-object p7, p0, Lcom/squareup/server/payment/RefundResponse;->reason:Ljava/lang/String;

    .line 22
    iput-object p8, p0, Lcom/squareup/server/payment/RefundResponse;->created_at:Ljava/lang/String;

    .line 23
    iput-object p9, p0, Lcom/squareup/server/payment/RefundResponse;->processed_at:Ljava/lang/String;

    return-void
.end method
