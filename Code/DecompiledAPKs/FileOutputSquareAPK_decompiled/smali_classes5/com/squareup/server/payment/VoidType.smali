.class public final enum Lcom/squareup/server/payment/VoidType;
.super Ljava/lang/Enum;
.source "VoidType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/server/payment/VoidType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/server/payment/VoidType;

.field public static final enum VOID_CLIENT_INITIATED_APP_TERMINATION:Lcom/squareup/server/payment/VoidType;

.field public static final enum VOID_CLIENT_INITIATED_TIMEOUT:Lcom/squareup/server/payment/VoidType;

.field public static final enum VOID_HUMAN_INITIATED:Lcom/squareup/server/payment/VoidType;

.field public static final enum VOID_OTHER:Lcom/squareup/server/payment/VoidType;

.field public static final enum VOID_SERVER_INITIATED_CONFLICT_RESOLUTION:Lcom/squareup/server/payment/VoidType;

.field public static final enum VOID_SERVER_INITIATED_TIMEOUT:Lcom/squareup/server/payment/VoidType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 7
    new-instance v0, Lcom/squareup/server/payment/VoidType;

    const/4 v1, 0x0

    const-string v2, "VOID_OTHER"

    invoke-direct {v0, v2, v1}, Lcom/squareup/server/payment/VoidType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/server/payment/VoidType;->VOID_OTHER:Lcom/squareup/server/payment/VoidType;

    .line 13
    new-instance v0, Lcom/squareup/server/payment/VoidType;

    const/4 v2, 0x1

    const-string v3, "VOID_HUMAN_INITIATED"

    invoke-direct {v0, v3, v2}, Lcom/squareup/server/payment/VoidType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/server/payment/VoidType;->VOID_HUMAN_INITIATED:Lcom/squareup/server/payment/VoidType;

    .line 20
    new-instance v0, Lcom/squareup/server/payment/VoidType;

    const/4 v3, 0x2

    const-string v4, "VOID_CLIENT_INITIATED_TIMEOUT"

    invoke-direct {v0, v4, v3}, Lcom/squareup/server/payment/VoidType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/server/payment/VoidType;->VOID_CLIENT_INITIATED_TIMEOUT:Lcom/squareup/server/payment/VoidType;

    .line 26
    new-instance v0, Lcom/squareup/server/payment/VoidType;

    const/4 v4, 0x3

    const-string v5, "VOID_CLIENT_INITIATED_APP_TERMINATION"

    invoke-direct {v0, v5, v4}, Lcom/squareup/server/payment/VoidType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/server/payment/VoidType;->VOID_CLIENT_INITIATED_APP_TERMINATION:Lcom/squareup/server/payment/VoidType;

    .line 32
    new-instance v0, Lcom/squareup/server/payment/VoidType;

    const/4 v5, 0x4

    const-string v6, "VOID_SERVER_INITIATED_CONFLICT_RESOLUTION"

    invoke-direct {v0, v6, v5}, Lcom/squareup/server/payment/VoidType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/server/payment/VoidType;->VOID_SERVER_INITIATED_CONFLICT_RESOLUTION:Lcom/squareup/server/payment/VoidType;

    .line 37
    new-instance v0, Lcom/squareup/server/payment/VoidType;

    const/4 v6, 0x5

    const-string v7, "VOID_SERVER_INITIATED_TIMEOUT"

    invoke-direct {v0, v7, v6}, Lcom/squareup/server/payment/VoidType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/server/payment/VoidType;->VOID_SERVER_INITIATED_TIMEOUT:Lcom/squareup/server/payment/VoidType;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/squareup/server/payment/VoidType;

    .line 3
    sget-object v7, Lcom/squareup/server/payment/VoidType;->VOID_OTHER:Lcom/squareup/server/payment/VoidType;

    aput-object v7, v0, v1

    sget-object v1, Lcom/squareup/server/payment/VoidType;->VOID_HUMAN_INITIATED:Lcom/squareup/server/payment/VoidType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/server/payment/VoidType;->VOID_CLIENT_INITIATED_TIMEOUT:Lcom/squareup/server/payment/VoidType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/server/payment/VoidType;->VOID_CLIENT_INITIATED_APP_TERMINATION:Lcom/squareup/server/payment/VoidType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/server/payment/VoidType;->VOID_SERVER_INITIATED_CONFLICT_RESOLUTION:Lcom/squareup/server/payment/VoidType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/server/payment/VoidType;->VOID_SERVER_INITIATED_TIMEOUT:Lcom/squareup/server/payment/VoidType;

    aput-object v1, v0, v6

    sput-object v0, Lcom/squareup/server/payment/VoidType;->$VALUES:[Lcom/squareup/server/payment/VoidType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/server/payment/VoidType;
    .locals 1

    .line 3
    const-class v0, Lcom/squareup/server/payment/VoidType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/server/payment/VoidType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/server/payment/VoidType;
    .locals 1

    .line 3
    sget-object v0, Lcom/squareup/server/payment/VoidType;->$VALUES:[Lcom/squareup/server/payment/VoidType;

    invoke-virtual {v0}, [Lcom/squareup/server/payment/VoidType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/server/payment/VoidType;

    return-object v0
.end method
