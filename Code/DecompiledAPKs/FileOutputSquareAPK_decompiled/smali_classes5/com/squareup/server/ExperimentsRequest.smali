.class public Lcom/squareup/server/ExperimentsRequest;
.super Ljava/lang/Object;
.source "ExperimentsRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/ExperimentsRequest$Named;
    }
.end annotation


# instance fields
.field final last_updated_usec:J

.field final requests:[Lcom/squareup/server/ExperimentsRequest$Named;


# direct methods
.method public constructor <init>(Ljava/util/List;J)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;J)V"
        }
    .end annotation

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/squareup/server/ExperimentsRequest$Named;

    iput-object v0, p0, Lcom/squareup/server/ExperimentsRequest;->requests:[Lcom/squareup/server/ExperimentsRequest$Named;

    const/4 v0, 0x0

    .line 12
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 13
    iget-object v1, p0, Lcom/squareup/server/ExperimentsRequest;->requests:[Lcom/squareup/server/ExperimentsRequest$Named;

    new-instance v2, Lcom/squareup/server/ExperimentsRequest$Named;

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-direct {v2, v3}, Lcom/squareup/server/ExperimentsRequest$Named;-><init>(Ljava/lang/String;)V

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 15
    :cond_0
    iput-wide p2, p0, Lcom/squareup/server/ExperimentsRequest;->last_updated_usec:J

    return-void
.end method
