.class public Lcom/squareup/server/seller/OtherTender;
.super Ljava/lang/Object;
.source "OtherTender.java"


# instance fields
.field public final note:Ljava/lang/String;

.field public final type:Lcom/squareup/server/account/protos/OtherTenderType;


# direct methods
.method public constructor <init>(Lcom/squareup/server/account/protos/OtherTenderType;Ljava/lang/String;)V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/squareup/server/seller/OtherTender;->type:Lcom/squareup/server/account/protos/OtherTenderType;

    .line 17
    iput-object p2, p0, Lcom/squareup/server/seller/OtherTender;->note:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    .line 21
    instance-of v1, p1, Lcom/squareup/server/seller/OtherTender;

    if-nez v1, :cond_0

    goto :goto_0

    .line 22
    :cond_0
    check-cast p1, Lcom/squareup/server/seller/OtherTender;

    .line 23
    iget-object v1, p0, Lcom/squareup/server/seller/OtherTender;->type:Lcom/squareup/server/account/protos/OtherTenderType;

    iget-object v2, p1, Lcom/squareup/server/seller/OtherTender;->type:Lcom/squareup/server/account/protos/OtherTenderType;

    invoke-static {v1, v2}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/squareup/server/seller/OtherTender;->note:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/server/seller/OtherTender;->note:Ljava/lang/String;

    invoke-static {v1, p1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :cond_1
    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 27
    iget-object v1, p0, Lcom/squareup/server/seller/OtherTender;->type:Lcom/squareup/server/account/protos/OtherTenderType;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/server/seller/OtherTender;->note:Ljava/lang/String;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/squareup/util/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
