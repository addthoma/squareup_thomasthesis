.class public final Lcom/squareup/server/seller/SwipedCard;
.super Ljava/lang/Object;
.source "SwipedCard.java"


# instance fields
.field public final encrypted_track_2:Ljava/lang/String;

.field public final unencrypted_track_2:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-nez p1, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    if-nez p2, :cond_1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    if-eq v2, v0, :cond_2

    .line 15
    iput-object p1, p0, Lcom/squareup/server/seller/SwipedCard;->encrypted_track_2:Ljava/lang/String;

    .line 16
    iput-object p2, p0, Lcom/squareup/server/seller/SwipedCard;->unencrypted_track_2:Ljava/lang/String;

    return-void

    .line 13
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "arguments are mutually exclusive; one must be null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    .line 20
    instance-of v1, p1, Lcom/squareup/server/seller/SwipedCard;

    if-nez v1, :cond_0

    goto :goto_0

    .line 21
    :cond_0
    check-cast p1, Lcom/squareup/server/seller/SwipedCard;

    .line 22
    iget-object v1, p0, Lcom/squareup/server/seller/SwipedCard;->encrypted_track_2:Ljava/lang/String;

    iget-object v2, p1, Lcom/squareup/server/seller/SwipedCard;->encrypted_track_2:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/squareup/server/seller/SwipedCard;->unencrypted_track_2:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/server/seller/SwipedCard;->unencrypted_track_2:Ljava/lang/String;

    invoke-static {v1, p1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :cond_1
    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 27
    iget-object v1, p0, Lcom/squareup/server/seller/SwipedCard;->encrypted_track_2:Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/server/seller/SwipedCard;->unencrypted_track_2:Ljava/lang/String;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/squareup/util/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
