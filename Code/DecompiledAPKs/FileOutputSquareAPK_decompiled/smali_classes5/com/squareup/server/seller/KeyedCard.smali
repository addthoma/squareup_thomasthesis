.class public final Lcom/squareup/server/seller/KeyedCard;
.super Ljava/lang/Object;
.source "KeyedCard.java"


# instance fields
.field public final expiration:Ljava/lang/String;

.field public final postal_code:Ljava/lang/String;

.field public final security_code:Ljava/lang/String;

.field public final unencrypted_pan:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/squareup/server/seller/KeyedCard;->unencrypted_pan:Ljava/lang/String;

    .line 17
    iput-object p2, p0, Lcom/squareup/server/seller/KeyedCard;->expiration:Ljava/lang/String;

    .line 18
    iput-object p3, p0, Lcom/squareup/server/seller/KeyedCard;->security_code:Ljava/lang/String;

    .line 19
    iput-object p4, p0, Lcom/squareup/server/seller/KeyedCard;->postal_code:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    .line 23
    instance-of v1, p1, Lcom/squareup/server/seller/KeyedCard;

    if-nez v1, :cond_0

    goto :goto_0

    .line 24
    :cond_0
    check-cast p1, Lcom/squareup/server/seller/KeyedCard;

    .line 25
    iget-object v1, p0, Lcom/squareup/server/seller/KeyedCard;->unencrypted_pan:Ljava/lang/String;

    iget-object v2, p1, Lcom/squareup/server/seller/KeyedCard;->unencrypted_pan:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/squareup/server/seller/KeyedCard;->expiration:Ljava/lang/String;

    iget-object v2, p1, Lcom/squareup/server/seller/KeyedCard;->expiration:Ljava/lang/String;

    .line 26
    invoke-static {v1, v2}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/squareup/server/seller/KeyedCard;->security_code:Ljava/lang/String;

    iget-object v2, p1, Lcom/squareup/server/seller/KeyedCard;->security_code:Ljava/lang/String;

    .line 27
    invoke-static {v1, v2}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/squareup/server/seller/KeyedCard;->postal_code:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/server/seller/KeyedCard;->postal_code:Ljava/lang/String;

    .line 28
    invoke-static {v1, p1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :cond_1
    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    .line 32
    iget-object v1, p0, Lcom/squareup/server/seller/KeyedCard;->unencrypted_pan:Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/server/seller/KeyedCard;->expiration:Ljava/lang/String;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/server/seller/KeyedCard;->security_code:Ljava/lang/String;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/server/seller/KeyedCard;->postal_code:Ljava/lang/String;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/squareup/util/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
