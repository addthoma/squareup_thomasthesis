.class public final Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamV2Factory;
.super Ljava/lang/Object;
.source "EventStreamModule_ProvideEventStreamV2Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/eventstream/v2/EventstreamV2;",
        ">;"
    }
.end annotation


# instance fields
.field private final contextProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final esLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/analytics/EsLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final gsonProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;"
        }
    .end annotation
.end field

.field private final installationIdProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final isReaderSdkProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final posBuildProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/PosBuild;",
            ">;"
        }
    .end annotation
.end field

.field private final posSdkVersionNameProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final processUniqueIdProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/ProcessUniqueId;",
            ">;"
        }
    .end annotation
.end field

.field private final productVersionCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final productVersionNameProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/content/res/Resources;",
            ">;"
        }
    .end annotation
.end field

.field private final uploaderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/analytics/Es2BatchUploader;",
            ">;"
        }
    .end annotation
.end field

.field private final userAgentProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/analytics/Es2BatchUploader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/analytics/EsLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/content/res/Resources;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/PosBuild;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/ProcessUniqueId;",
            ">;)V"
        }
    .end annotation

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-object p1, p0, Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamV2Factory;->contextProvider:Ljavax/inject/Provider;

    .line 61
    iput-object p2, p0, Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamV2Factory;->uploaderProvider:Ljavax/inject/Provider;

    .line 62
    iput-object p3, p0, Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamV2Factory;->esLoggerProvider:Ljavax/inject/Provider;

    .line 63
    iput-object p4, p0, Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamV2Factory;->userAgentProvider:Ljavax/inject/Provider;

    .line 64
    iput-object p5, p0, Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamV2Factory;->posSdkVersionNameProvider:Ljavax/inject/Provider;

    .line 65
    iput-object p6, p0, Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamV2Factory;->gsonProvider:Ljavax/inject/Provider;

    .line 66
    iput-object p7, p0, Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamV2Factory;->installationIdProvider:Ljavax/inject/Provider;

    .line 67
    iput-object p8, p0, Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamV2Factory;->deviceProvider:Ljavax/inject/Provider;

    .line 68
    iput-object p9, p0, Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamV2Factory;->resProvider:Ljavax/inject/Provider;

    .line 69
    iput-object p10, p0, Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamV2Factory;->isReaderSdkProvider:Ljavax/inject/Provider;

    .line 70
    iput-object p11, p0, Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamV2Factory;->productVersionNameProvider:Ljavax/inject/Provider;

    .line 71
    iput-object p12, p0, Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamV2Factory;->productVersionCodeProvider:Ljavax/inject/Provider;

    .line 72
    iput-object p13, p0, Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamV2Factory;->posBuildProvider:Ljavax/inject/Provider;

    .line 73
    iput-object p14, p0, Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamV2Factory;->processUniqueIdProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamV2Factory;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/analytics/Es2BatchUploader;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/analytics/EsLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/content/res/Resources;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/String;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/PosBuild;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/ProcessUniqueId;",
            ">;)",
            "Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamV2Factory;"
        }
    .end annotation

    .line 89
    new-instance v15, Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamV2Factory;

    move-object v0, v15

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    invoke-direct/range {v0 .. v14}, Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamV2Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v15
.end method

.method public static provideEventStreamV2(Landroid/app/Application;Ljava/lang/Object;Lcom/squareup/server/analytics/EsLogger;Ljava/lang/String;Ljava/lang/String;Lcom/google/gson/Gson;Ljava/lang/String;Lcom/squareup/util/Device;Landroid/content/res/Resources;ZLjava/lang/String;ILcom/squareup/util/PosBuild;Lcom/squareup/analytics/ProcessUniqueId;)Lcom/squareup/eventstream/v2/EventstreamV2;
    .locals 14

    .line 97
    move-object v1, p1

    check-cast v1, Lcom/squareup/server/analytics/Es2BatchUploader;

    move-object v0, p0

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move/from16 v9, p9

    move-object/from16 v10, p10

    move/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    invoke-static/range {v0 .. v13}, Lcom/squareup/server/analytics/EventStreamModule;->provideEventStreamV2(Landroid/app/Application;Lcom/squareup/server/analytics/Es2BatchUploader;Lcom/squareup/server/analytics/EsLogger;Ljava/lang/String;Ljava/lang/String;Lcom/google/gson/Gson;Ljava/lang/String;Lcom/squareup/util/Device;Landroid/content/res/Resources;ZLjava/lang/String;ILcom/squareup/util/PosBuild;Lcom/squareup/analytics/ProcessUniqueId;)Lcom/squareup/eventstream/v2/EventstreamV2;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/eventstream/v2/EventstreamV2;

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/eventstream/v2/EventstreamV2;
    .locals 15

    .line 78
    iget-object v0, p0, Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamV2Factory;->contextProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Landroid/app/Application;

    iget-object v0, p0, Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamV2Factory;->uploaderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    iget-object v0, p0, Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamV2Factory;->esLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/server/analytics/EsLogger;

    iget-object v0, p0, Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamV2Factory;->userAgentProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Ljava/lang/String;

    iget-object v0, p0, Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamV2Factory;->posSdkVersionNameProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Ljava/lang/String;

    iget-object v0, p0, Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamV2Factory;->gsonProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/google/gson/Gson;

    iget-object v0, p0, Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamV2Factory;->installationIdProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Ljava/lang/String;

    iget-object v0, p0, Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamV2Factory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/util/Device;

    iget-object v0, p0, Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamV2Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Landroid/content/res/Resources;

    iget-object v0, p0, Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamV2Factory;->isReaderSdkProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v10

    iget-object v0, p0, Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamV2Factory;->productVersionNameProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Ljava/lang/String;

    iget-object v0, p0, Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamV2Factory;->productVersionCodeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v12

    iget-object v0, p0, Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamV2Factory;->posBuildProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v13, v0

    check-cast v13, Lcom/squareup/util/PosBuild;

    iget-object v0, p0, Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamV2Factory;->processUniqueIdProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v14, v0

    check-cast v14, Lcom/squareup/analytics/ProcessUniqueId;

    invoke-static/range {v1 .. v14}, Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamV2Factory;->provideEventStreamV2(Landroid/app/Application;Ljava/lang/Object;Lcom/squareup/server/analytics/EsLogger;Ljava/lang/String;Ljava/lang/String;Lcom/google/gson/Gson;Ljava/lang/String;Lcom/squareup/util/Device;Landroid/content/res/Resources;ZLjava/lang/String;ILcom/squareup/util/PosBuild;Lcom/squareup/analytics/ProcessUniqueId;)Lcom/squareup/eventstream/v2/EventstreamV2;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 15
    invoke-virtual {p0}, Lcom/squareup/server/analytics/EventStreamModule_ProvideEventStreamV2Factory;->get()Lcom/squareup/eventstream/v2/EventstreamV2;

    move-result-object v0

    return-object v0
.end method
