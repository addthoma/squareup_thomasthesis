.class public Lcom/squareup/server/analytics/EsLogger;
.super Ljava/lang/Object;
.source "EsLogger.java"

# interfaces
.implements Lcom/squareup/eventstream/EventStreamLog;


# direct methods
.method constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public varargs log(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 0

    .line 17
    invoke-static {p1, p2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public report(Ljava/lang/Throwable;)V
    .locals 1

    const-string v0, "A problem occurred processing EventStream events."

    .line 21
    invoke-static {p1, v0}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;Ljava/lang/String;)V

    return-void
.end method
