.class public final Lcom/squareup/server/onboard/PanelsKt;
.super Ljava/lang/Object;
.source "Panels.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPanels.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Panels.kt\ncom/squareup/server/onboard/PanelsKt\n*L\n1#1,379:1\n349#1,3:380\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000J\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010 \n\u0002\u0008\u0005\u001a\u001a\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u00020\u00012\n\u0008\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u0004H\u0000\u001a-\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u0002H\u00060\u0001\"\u0010\u0008\u0000\u0010\u0006\u0018\u0001*\u0008\u0012\u0004\u0012\u0002H\u00060\u00072\n\u0008\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u0004H\u0080\u0008\u001a\u001a\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\t0\u00012\n\u0008\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u0004H\u0000\u001a\'\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\u00042\u0017\u0010\r\u001a\u0013\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u00100\u000e\u00a2\u0006\u0002\u0008\u0011\u001a\u000e\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0004\u001a\u0016\u0010\u0015\u001a\u00020\u00132\u0006\u0010\u0016\u001a\u00020\u00042\u0006\u0010\u0014\u001a\u00020\u0004\u001a \u0010\u0017\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00040\u00180\u00012\n\u0008\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u0004H\u0000\u001a\u001a\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00012\n\u0008\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u0004H\u0000\u001a\u000e\u0010\u001a\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0004\u001a\"\u0010\u001b\u001a\u00020\u00132\u0006\u0010\u000c\u001a\u00020\u00042\n\u0008\u0002\u0010\u001c\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0014\u001a\u00020\u0004\u00a8\u0006\u001d"
    }
    d2 = {
        "booleanPropertyEntry",
        "Lcom/squareup/server/onboard/PropertyEntryDelegate;",
        "",
        "key",
        "",
        "enumPropertyEntry",
        "T",
        "",
        "intPropertyEntry",
        "",
        "panelWithName",
        "Lcom/squareup/protos/client/onboard/Panel;",
        "name",
        "configure",
        "Lkotlin/Function1;",
        "Lcom/squareup/server/onboard/PanelBuilder;",
        "",
        "Lkotlin/ExtensionFunctionType;",
        "present",
        "Lcom/squareup/protos/client/onboard/Validator;",
        "message",
        "regex",
        "pattern",
        "stringListPropertyEntry",
        "",
        "stringPropertyEntry",
        "valid",
        "validator",
        "value",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final booleanPropertyEntry(Ljava/lang/String;)Lcom/squareup/server/onboard/PropertyEntryDelegate;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/server/onboard/PropertyEntryDelegate<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 340
    new-instance v0, Lcom/squareup/server/onboard/PropertyEntryDelegate;

    .line 341
    sget-object v1, Lcom/squareup/server/onboard/PanelsKt$booleanPropertyEntry$1;->INSTANCE:Lcom/squareup/server/onboard/PanelsKt$booleanPropertyEntry$1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    .line 342
    sget-object v2, Lcom/squareup/server/onboard/PanelsKt$booleanPropertyEntry$2;->INSTANCE:Lcom/squareup/server/onboard/PanelsKt$booleanPropertyEntry$2;

    check-cast v2, Lkotlin/jvm/functions/Function2;

    .line 340
    invoke-direct {v0, p0, v1, v2}, Lcom/squareup/server/onboard/PropertyEntryDelegate;-><init>(Ljava/lang/String;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function2;)V

    return-object v0
.end method

.method public static synthetic booleanPropertyEntry$default(Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/server/onboard/PropertyEntryDelegate;
    .locals 0

    and-int/lit8 p1, p1, 0x1

    if-eqz p1, :cond_0

    const/4 p0, 0x0

    .line 339
    check-cast p0, Ljava/lang/String;

    :cond_0
    invoke-static {p0}, Lcom/squareup/server/onboard/PanelsKt;->booleanPropertyEntry(Ljava/lang/String;)Lcom/squareup/server/onboard/PropertyEntryDelegate;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic enumPropertyEntry(Ljava/lang/String;)Lcom/squareup/server/onboard/PropertyEntryDelegate;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Enum<",
            "TT;>;>(",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/server/onboard/PropertyEntryDelegate<",
            "TT;>;"
        }
    .end annotation

    .line 349
    new-instance v0, Lcom/squareup/server/onboard/PropertyEntryDelegate;

    .line 350
    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->needClassReification()V

    sget-object v1, Lcom/squareup/server/onboard/PanelsKt$enumPropertyEntry$1;->INSTANCE:Lcom/squareup/server/onboard/PanelsKt$enumPropertyEntry$1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    .line 351
    sget-object v2, Lcom/squareup/server/onboard/PanelsKt$enumPropertyEntry$2;->INSTANCE:Lcom/squareup/server/onboard/PanelsKt$enumPropertyEntry$2;

    check-cast v2, Lkotlin/jvm/functions/Function2;

    .line 349
    invoke-direct {v0, p0, v1, v2}, Lcom/squareup/server/onboard/PropertyEntryDelegate;-><init>(Ljava/lang/String;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function2;)V

    return-object v0
.end method

.method public static synthetic enumPropertyEntry$default(Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/server/onboard/PropertyEntryDelegate;
    .locals 1

    and-int/lit8 p1, p1, 0x1

    if-eqz p1, :cond_0

    const/4 p0, 0x0

    .line 347
    check-cast p0, Ljava/lang/String;

    .line 380
    :cond_0
    new-instance p1, Lcom/squareup/server/onboard/PropertyEntryDelegate;

    .line 381
    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->needClassReification()V

    sget-object p2, Lcom/squareup/server/onboard/PanelsKt$enumPropertyEntry$1;->INSTANCE:Lcom/squareup/server/onboard/PanelsKt$enumPropertyEntry$1;

    check-cast p2, Lkotlin/jvm/functions/Function1;

    .line 382
    sget-object v0, Lcom/squareup/server/onboard/PanelsKt$enumPropertyEntry$2;->INSTANCE:Lcom/squareup/server/onboard/PanelsKt$enumPropertyEntry$2;

    check-cast v0, Lkotlin/jvm/functions/Function2;

    .line 380
    invoke-direct {p1, p0, p2, v0}, Lcom/squareup/server/onboard/PropertyEntryDelegate;-><init>(Ljava/lang/String;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function2;)V

    return-object p1
.end method

.method public static final intPropertyEntry(Ljava/lang/String;)Lcom/squareup/server/onboard/PropertyEntryDelegate;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/server/onboard/PropertyEntryDelegate<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 333
    new-instance v0, Lcom/squareup/server/onboard/PropertyEntryDelegate;

    .line 334
    sget-object v1, Lcom/squareup/server/onboard/PanelsKt$intPropertyEntry$1;->INSTANCE:Lcom/squareup/server/onboard/PanelsKt$intPropertyEntry$1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    .line 335
    sget-object v2, Lcom/squareup/server/onboard/PanelsKt$intPropertyEntry$2;->INSTANCE:Lcom/squareup/server/onboard/PanelsKt$intPropertyEntry$2;

    check-cast v2, Lkotlin/jvm/functions/Function2;

    .line 333
    invoke-direct {v0, p0, v1, v2}, Lcom/squareup/server/onboard/PropertyEntryDelegate;-><init>(Ljava/lang/String;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function2;)V

    return-object v0
.end method

.method public static synthetic intPropertyEntry$default(Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/server/onboard/PropertyEntryDelegate;
    .locals 0

    and-int/lit8 p1, p1, 0x1

    if-eqz p1, :cond_0

    const/4 p0, 0x0

    .line 332
    check-cast p0, Ljava/lang/String;

    :cond_0
    invoke-static {p0}, Lcom/squareup/server/onboard/PanelsKt;->intPropertyEntry(Ljava/lang/String;)Lcom/squareup/server/onboard/PropertyEntryDelegate;

    move-result-object p0

    return-object p0
.end method

.method public static final panelWithName(Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lcom/squareup/protos/client/onboard/Panel;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/server/onboard/PanelBuilder;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/protos/client/onboard/Panel;"
        }
    .end annotation

    const-string v0, "name"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "configure"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    new-instance v0, Lcom/squareup/server/onboard/PanelBuilder;

    invoke-direct {v0}, Lcom/squareup/server/onboard/PanelBuilder;-><init>()V

    .line 49
    invoke-virtual {v0}, Lcom/squareup/server/onboard/PanelBuilder;->getBuilder$public_release()Lcom/squareup/protos/client/onboard/Panel$Builder;

    move-result-object v1

    new-instance v2, Lcom/squareup/server/onboard/NavigationBuilder;

    invoke-direct {v2}, Lcom/squareup/server/onboard/NavigationBuilder;-><init>()V

    invoke-virtual {v2}, Lcom/squareup/server/onboard/NavigationBuilder;->getBuilder$public_release()Lcom/squareup/protos/client/onboard/Navigation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/protos/client/onboard/Navigation$Builder;->build()Lcom/squareup/protos/client/onboard/Navigation;

    move-result-object v2

    iput-object v2, v1, Lcom/squareup/protos/client/onboard/Panel$Builder;->navigation:Lcom/squareup/protos/client/onboard/Navigation;

    .line 50
    invoke-virtual {v0}, Lcom/squareup/server/onboard/PanelBuilder;->getBuilder$public_release()Lcom/squareup/protos/client/onboard/Panel$Builder;

    move-result-object v1

    iput-object p0, v1, Lcom/squareup/protos/client/onboard/Panel$Builder;->name:Ljava/lang/String;

    .line 51
    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    invoke-virtual {v0}, Lcom/squareup/server/onboard/PanelBuilder;->getBuilder$public_release()Lcom/squareup/protos/client/onboard/Panel$Builder;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/protos/client/onboard/Panel$Builder;->build()Lcom/squareup/protos/client/onboard/Panel;

    move-result-object p0

    const-string p1, "panelBuilder.builder.build()"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final present(Ljava/lang/String;)Lcom/squareup/protos/client/onboard/Validator;
    .locals 3

    const-string v0, "message"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    const-string v1, "present"

    const/4 v2, 0x2

    .line 373
    invoke-static {v1, v0, p0, v2, v0}, Lcom/squareup/server/onboard/PanelsKt;->validator$default(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/protos/client/onboard/Validator;

    move-result-object p0

    return-object p0
.end method

.method public static final regex(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/protos/client/onboard/Validator;
    .locals 1

    const-string v0, "pattern"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "message"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "regex"

    .line 378
    invoke-static {v0, p0, p1}, Lcom/squareup/server/onboard/PanelsKt;->validator(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/protos/client/onboard/Validator;

    move-result-object p0

    return-object p0
.end method

.method public static final stringListPropertyEntry(Ljava/lang/String;)Lcom/squareup/server/onboard/PropertyEntryDelegate;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/server/onboard/PropertyEntryDelegate<",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .line 319
    new-instance v0, Lcom/squareup/server/onboard/PropertyEntryDelegate;

    .line 320
    sget-object v1, Lcom/squareup/server/onboard/PanelsKt$stringListPropertyEntry$1;->INSTANCE:Lcom/squareup/server/onboard/PanelsKt$stringListPropertyEntry$1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    .line 321
    sget-object v2, Lcom/squareup/server/onboard/PanelsKt$stringListPropertyEntry$2;->INSTANCE:Lcom/squareup/server/onboard/PanelsKt$stringListPropertyEntry$2;

    check-cast v2, Lkotlin/jvm/functions/Function2;

    .line 319
    invoke-direct {v0, p0, v1, v2}, Lcom/squareup/server/onboard/PropertyEntryDelegate;-><init>(Ljava/lang/String;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function2;)V

    return-object v0
.end method

.method public static synthetic stringListPropertyEntry$default(Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/server/onboard/PropertyEntryDelegate;
    .locals 0

    and-int/lit8 p1, p1, 0x1

    if-eqz p1, :cond_0

    const/4 p0, 0x0

    .line 318
    check-cast p0, Ljava/lang/String;

    :cond_0
    invoke-static {p0}, Lcom/squareup/server/onboard/PanelsKt;->stringListPropertyEntry(Ljava/lang/String;)Lcom/squareup/server/onboard/PropertyEntryDelegate;

    move-result-object p0

    return-object p0
.end method

.method public static final stringPropertyEntry(Ljava/lang/String;)Lcom/squareup/server/onboard/PropertyEntryDelegate;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/squareup/server/onboard/PropertyEntryDelegate<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 326
    new-instance v0, Lcom/squareup/server/onboard/PropertyEntryDelegate;

    .line 327
    sget-object v1, Lcom/squareup/server/onboard/PanelsKt$stringPropertyEntry$1;->INSTANCE:Lcom/squareup/server/onboard/PanelsKt$stringPropertyEntry$1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    .line 328
    sget-object v2, Lcom/squareup/server/onboard/PanelsKt$stringPropertyEntry$2;->INSTANCE:Lcom/squareup/server/onboard/PanelsKt$stringPropertyEntry$2;

    check-cast v2, Lkotlin/jvm/functions/Function2;

    .line 326
    invoke-direct {v0, p0, v1, v2}, Lcom/squareup/server/onboard/PropertyEntryDelegate;-><init>(Ljava/lang/String;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function2;)V

    return-object v0
.end method

.method public static synthetic stringPropertyEntry$default(Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/server/onboard/PropertyEntryDelegate;
    .locals 0

    and-int/lit8 p1, p1, 0x1

    if-eqz p1, :cond_0

    const/4 p0, 0x0

    .line 325
    check-cast p0, Ljava/lang/String;

    :cond_0
    invoke-static {p0}, Lcom/squareup/server/onboard/PanelsKt;->stringPropertyEntry(Ljava/lang/String;)Lcom/squareup/server/onboard/PropertyEntryDelegate;

    move-result-object p0

    return-object p0
.end method

.method public static final valid(Ljava/lang/String;)Lcom/squareup/protos/client/onboard/Validator;
    .locals 3

    const-string v0, "message"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    const-string/jumbo v1, "valid"

    const/4 v2, 0x2

    .line 371
    invoke-static {v1, v0, p0, v2, v0}, Lcom/squareup/server/onboard/PanelsKt;->validator$default(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/protos/client/onboard/Validator;

    move-result-object p0

    return-object p0
.end method

.method public static final validator(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/protos/client/onboard/Validator;
    .locals 1

    const-string v0, "name"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "message"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 369
    new-instance v0, Lcom/squareup/protos/client/onboard/Validator$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/onboard/Validator$Builder;-><init>()V

    invoke-virtual {v0, p0}, Lcom/squareup/protos/client/onboard/Validator$Builder;->name(Ljava/lang/String;)Lcom/squareup/protos/client/onboard/Validator$Builder;

    move-result-object p0

    invoke-virtual {p0, p1}, Lcom/squareup/protos/client/onboard/Validator$Builder;->value(Ljava/lang/String;)Lcom/squareup/protos/client/onboard/Validator$Builder;

    move-result-object p0

    invoke-virtual {p0, p2}, Lcom/squareup/protos/client/onboard/Validator$Builder;->message(Ljava/lang/String;)Lcom/squareup/protos/client/onboard/Validator$Builder;

    move-result-object p0

    invoke-virtual {p0}, Lcom/squareup/protos/client/onboard/Validator$Builder;->build()Lcom/squareup/protos/client/onboard/Validator;

    move-result-object p0

    const-string p1, "Validator.Builder().name\u2026.message(message).build()"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static synthetic validator$default(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/protos/client/onboard/Validator;
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p1, 0x0

    .line 367
    check-cast p1, Ljava/lang/String;

    :cond_0
    invoke-static {p0, p1, p2}, Lcom/squareup/server/onboard/PanelsKt;->validator(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/protos/client/onboard/Validator;

    move-result-object p0

    return-object p0
.end method
