.class public final Lcom/squareup/server/onboard/PanelBuilder;
.super Ljava/lang/Object;
.source "Panels.kt"


# annotations
.annotation runtime Lcom/squareup/server/onboard/PanelDsl;
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPanels.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Panels.kt\ncom/squareup/server/onboard/PanelBuilder\n*L\n1#1,379:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000e\n\u0002\u0008\u0008\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0007\u0018\u00002\u00020\u0001B\u0007\u0008\u0000\u00a2\u0006\u0002\u0010\u0002J\u001f\u0010\u0010\u001a\u00020\u00112\u0017\u0010\u0012\u001a\u0013\u0012\u0004\u0012\u00020\u0014\u0012\u0004\u0012\u00020\u00110\u0013\u00a2\u0006\u0002\u0008\u0015J\u001f\u0010\u0016\u001a\u00020\u00112\u0017\u0010\u0012\u001a\u0013\u0012\u0004\u0012\u00020\u0017\u0012\u0004\u0012\u00020\u00110\u0013\u00a2\u0006\u0002\u0008\u0015R\u0014\u0010\u0003\u001a\u00020\u0004X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006R/\u0010\t\u001a\u0004\u0018\u00010\u00082\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u00088F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\u0008\u000e\u0010\u000f\u001a\u0004\u0008\n\u0010\u000b\"\u0004\u0008\u000c\u0010\r\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/server/onboard/PanelBuilder;",
        "",
        "()V",
        "builder",
        "Lcom/squareup/protos/client/onboard/Panel$Builder;",
        "getBuilder$public_release",
        "()Lcom/squareup/protos/client/onboard/Panel$Builder;",
        "<set-?>",
        "",
        "title",
        "getTitle",
        "()Ljava/lang/String;",
        "setTitle",
        "(Ljava/lang/String;)V",
        "title$delegate",
        "Lkotlin/properties/ReadWriteProperty;",
        "components",
        "",
        "configure",
        "Lkotlin/Function1;",
        "Lcom/squareup/server/onboard/ComponentsBuilder;",
        "Lkotlin/ExtensionFunctionType;",
        "navigation",
        "Lcom/squareup/server/onboard/NavigationBuilder;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;


# instance fields
.field private final builder:Lcom/squareup/protos/client/onboard/Panel$Builder;

.field private final title$delegate:Lkotlin/properties/ReadWriteProperty;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lkotlin/jvm/internal/MutablePropertyReference1Impl;

    const-class v2, Lcom/squareup/server/onboard/PanelBuilder;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    const-string/jumbo v3, "title"

    const-string v4, "getTitle()Ljava/lang/String;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/jvm/internal/MutablePropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->mutableProperty1(Lkotlin/jvm/internal/MutablePropertyReference1;)Lkotlin/reflect/KMutableProperty1;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/server/onboard/PanelBuilder;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    new-instance v0, Lcom/squareup/protos/client/onboard/Panel$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/onboard/Panel$Builder;-><init>()V

    iput-object v0, p0, Lcom/squareup/server/onboard/PanelBuilder;->builder:Lcom/squareup/protos/client/onboard/Panel$Builder;

    .line 68
    new-instance v0, Lcom/squareup/server/onboard/PanelBuilder$title$2;

    iget-object v1, p0, Lcom/squareup/server/onboard/PanelBuilder;->builder:Lcom/squareup/protos/client/onboard/Panel$Builder;

    invoke-direct {v0, v1}, Lcom/squareup/server/onboard/PanelBuilder$title$2;-><init>(Lcom/squareup/protos/client/onboard/Panel$Builder;)V

    invoke-static {v0}, Lcom/squareup/util/DelegatesKt;->alias(Lkotlin/reflect/KMutableProperty0;)Lkotlin/properties/ReadWriteProperty;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/server/onboard/PanelBuilder;->title$delegate:Lkotlin/properties/ReadWriteProperty;

    return-void
.end method


# virtual methods
.method public final components(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/server/onboard/ComponentsBuilder;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "configure"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 84
    new-instance v0, Lcom/squareup/server/onboard/ComponentsBuilder;

    invoke-direct {v0}, Lcom/squareup/server/onboard/ComponentsBuilder;-><init>()V

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    iget-object p1, p0, Lcom/squareup/server/onboard/PanelBuilder;->builder:Lcom/squareup/protos/client/onboard/Panel$Builder;

    iget-object p1, p1, Lcom/squareup/protos/client/onboard/Panel$Builder;->components:Ljava/util/List;

    invoke-virtual {v0}, Lcom/squareup/server/onboard/ComponentsBuilder;->getComponents$public_release()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {p1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    return-void
.end method

.method public final getBuilder$public_release()Lcom/squareup/protos/client/onboard/Panel$Builder;
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/squareup/server/onboard/PanelBuilder;->builder:Lcom/squareup/protos/client/onboard/Panel$Builder;

    return-object v0
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/squareup/server/onboard/PanelBuilder;->title$delegate:Lkotlin/properties/ReadWriteProperty;

    sget-object v1, Lcom/squareup/server/onboard/PanelBuilder;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadWriteProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final navigation(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/server/onboard/NavigationBuilder;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "configure"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    new-instance v0, Lcom/squareup/server/onboard/NavigationBuilder;

    invoke-direct {v0}, Lcom/squareup/server/onboard/NavigationBuilder;-><init>()V

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    iget-object p1, p0, Lcom/squareup/server/onboard/PanelBuilder;->builder:Lcom/squareup/protos/client/onboard/Panel$Builder;

    invoke-virtual {v0}, Lcom/squareup/server/onboard/NavigationBuilder;->getBuilder$public_release()Lcom/squareup/protos/client/onboard/Navigation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/protos/client/onboard/Navigation$Builder;->build()Lcom/squareup/protos/client/onboard/Navigation;

    move-result-object v0

    iput-object v0, p1, Lcom/squareup/protos/client/onboard/Panel$Builder;->navigation:Lcom/squareup/protos/client/onboard/Navigation;

    return-void
.end method

.method public final setTitle(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/squareup/server/onboard/PanelBuilder;->title$delegate:Lkotlin/properties/ReadWriteProperty;

    sget-object v1, Lcom/squareup/server/onboard/PanelBuilder;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1, p1}, Lkotlin/properties/ReadWriteProperty;->setValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V

    return-void
.end method
