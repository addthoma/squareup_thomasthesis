.class public abstract Lcom/squareup/singlesignon/NoSingleSignOnModule;
.super Ljava/lang/Object;
.source "NoSingleSignOnModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideSingleSignOn()Lcom/squareup/singlesignon/SingleSignOn;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 9
    sget-object v0, Lcom/squareup/singlesignon/SingleSignOn$NoSingleSignOn;->INSTANCE:Lcom/squareup/singlesignon/SingleSignOn$NoSingleSignOn;

    return-object v0
.end method
