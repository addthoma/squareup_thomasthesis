.class public interface abstract Lcom/squareup/singlesignon/SingleSignOn;
.super Ljava/lang/Object;
.source "SingleSignOn.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/singlesignon/SingleSignOn$NoSingleSignOn;,
        Lcom/squareup/singlesignon/SingleSignOn$DefaultImpls;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0006\u0008f\u0018\u00002\u00020\u0001:\u0001\u000eJ\u0008\u0010\u0002\u001a\u00020\u0003H\u0016J\u0012\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005H&J\u0008\u0010\u0006\u001a\u00020\u0007H&J\u0008\u0010\u0008\u001a\u00020\tH&J\u0010\u0010\n\u001a\u00020\u00032\u0006\u0010\u000b\u001a\u00020\u0007H&J\u0010\u0010\u000c\u001a\u00020\u00032\u0006\u0010\r\u001a\u00020\u0007H&\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/singlesignon/SingleSignOn;",
        "",
        "clearSessionToken",
        "",
        "reason",
        "Lcom/squareup/singlesignon/ClearSessionReason;",
        "getSessionToken",
        "",
        "hasSessionToken",
        "",
        "logEvent",
        "message",
        "setSessionToken",
        "sessionToken",
        "NoSingleSignOn",
        "singlesignon_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract clearSessionToken()V
.end method

.method public abstract clearSessionToken(Lcom/squareup/singlesignon/ClearSessionReason;)V
.end method

.method public abstract getSessionToken()Ljava/lang/String;
.end method

.method public abstract hasSessionToken()Z
.end method

.method public abstract logEvent(Ljava/lang/String;)V
.end method

.method public abstract setSessionToken(Ljava/lang/String;)V
.end method
