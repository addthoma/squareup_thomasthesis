.class public final Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceSettingsAppLink$SettingsAppLink;
.super Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceSettingsAppLink;
.source "SquareDeviceDeepLink.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceSettingsAppLink;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SettingsAppLink"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceSettingsAppLink$SettingsAppLink;",
        "Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceSettingsAppLink;",
        "()V",
        "square-device_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceSettingsAppLink$SettingsAppLink;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 82
    new-instance v0, Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceSettingsAppLink$SettingsAppLink;

    invoke-direct {v0}, Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceSettingsAppLink$SettingsAppLink;-><init>()V

    sput-object v0, Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceSettingsAppLink$SettingsAppLink;->INSTANCE:Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceSettingsAppLink$SettingsAppLink;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 83
    invoke-direct {p0, v0, v1, v0}, Lcom/squareup/squid/SquareDeviceDeepLink$SquareDeviceSettingsAppLink;-><init>(Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method
