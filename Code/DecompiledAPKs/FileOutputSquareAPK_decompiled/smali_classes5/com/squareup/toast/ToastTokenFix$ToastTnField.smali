.class public abstract Lcom/squareup/toast/ToastTokenFix$ToastTnField;
.super Ljava/lang/Object;
.source "ToastTokenFix.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/toast/ToastTokenFix;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ToastTnField"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/toast/ToastTokenFix$ToastTnField$TnMShowField;,
        Lcom/squareup/toast/ToastTokenFix$ToastTnField$TnMHandlerfield;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0002\n\u000bB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0012\u0010\u0007\u001a\u00020\u00082\u0008\u0010\t\u001a\u0004\u0018\u00010\u0001H&R\u0012\u0010\u0003\u001a\u00020\u0004X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006\u0082\u0001\u0002\u000c\r\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/toast/ToastTokenFix$ToastTnField;",
        "",
        "()V",
        "field",
        "Ljava/lang/reflect/Field;",
        "getField",
        "()Ljava/lang/reflect/Field;",
        "swallowBadTokenException",
        "",
        "tn",
        "TnMHandlerfield",
        "TnMShowField",
        "Lcom/squareup/toast/ToastTokenFix$ToastTnField$TnMShowField;",
        "Lcom/squareup/toast/ToastTokenFix$ToastTnField$TnMHandlerfield;",
        "android-hairball_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 153
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 153
    invoke-direct {p0}, Lcom/squareup/toast/ToastTokenFix$ToastTnField;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getField()Ljava/lang/reflect/Field;
.end method

.method public abstract swallowBadTokenException(Ljava/lang/Object;)V
.end method
