.class Lcom/squareup/signature/SignatureView$SavedState;
.super Landroid/view/View$BaseSavedState;
.source "SignatureView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/signature/SignatureView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SavedState"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/signature/SignatureView$SavedState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final signatureRenderer:Lcom/squareup/signature/SignatureRenderer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 302
    new-instance v0, Lcom/squareup/signature/SignatureView$SavedState$1;

    invoke-direct {v0}, Lcom/squareup/signature/SignatureView$SavedState$1;-><init>()V

    sput-object v0, Lcom/squareup/signature/SignatureView$SavedState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .line 313
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcel;)V

    .line 315
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 316
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result p1

    .line 318
    invoke-static {v0, p1}, Lcom/squareup/signature/SignatureRenderer;->decode(Ljava/lang/String;F)Lcom/squareup/signature/SignatureRenderer;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/signature/SignatureView$SavedState;->signatureRenderer:Lcom/squareup/signature/SignatureRenderer;

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/squareup/signature/SignatureView$1;)V
    .locals 0

    .line 286
    invoke-direct {p0, p1}, Lcom/squareup/signature/SignatureView$SavedState;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method constructor <init>(Landroid/os/Parcelable;Lcom/squareup/signature/SignatureRenderer;)V
    .locals 0

    .line 291
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcelable;)V

    .line 292
    iput-object p2, p0, Lcom/squareup/signature/SignatureView$SavedState;->signatureRenderer:Lcom/squareup/signature/SignatureRenderer;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/signature/SignatureView$SavedState;)Lcom/squareup/signature/SignatureRenderer;
    .locals 0

    .line 286
    iget-object p0, p0, Lcom/squareup/signature/SignatureView$SavedState;->signatureRenderer:Lcom/squareup/signature/SignatureRenderer;

    return-object p0
.end method


# virtual methods
.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 296
    invoke-super {p0, p1, p2}, Landroid/view/View$BaseSavedState;->writeToParcel(Landroid/os/Parcel;I)V

    .line 297
    iget-object p2, p0, Lcom/squareup/signature/SignatureView$SavedState;->signatureRenderer:Lcom/squareup/signature/SignatureRenderer;

    invoke-virtual {p2}, Lcom/squareup/signature/SignatureRenderer;->getSignature()Lcom/squareup/signature/SignatureAsJson;

    move-result-object p2

    invoke-interface {p2}, Lcom/squareup/signature/SignatureAsJson;->encode()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 298
    iget-object p2, p0, Lcom/squareup/signature/SignatureView$SavedState;->signatureRenderer:Lcom/squareup/signature/SignatureRenderer;

    invoke-virtual {p2}, Lcom/squareup/signature/SignatureRenderer;->getStrokeWidth()F

    move-result p2

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeFloat(F)V

    return-void
.end method
