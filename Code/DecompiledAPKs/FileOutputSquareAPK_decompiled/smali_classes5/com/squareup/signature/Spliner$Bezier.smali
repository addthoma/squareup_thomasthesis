.class public Lcom/squareup/signature/Spliner$Bezier;
.super Ljava/lang/Object;
.source "Spliner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/signature/Spliner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Bezier"
.end annotation


# instance fields
.field private final control1:Lcom/squareup/signature/Point;

.field private final control2:Lcom/squareup/signature/Point;

.field private final endPoint:Lcom/squareup/signature/Point;

.field private final path:Landroid/graphics/Path;

.field private final startPoint:Lcom/squareup/signature/Point;


# direct methods
.method public constructor <init>(Lcom/squareup/signature/Point;Lcom/squareup/signature/Point;Lcom/squareup/signature/Point;Lcom/squareup/signature/Point;)V
    .locals 1

    .line 213
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 211
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/squareup/signature/Spliner$Bezier;->path:Landroid/graphics/Path;

    .line 214
    iput-object p1, p0, Lcom/squareup/signature/Spliner$Bezier;->startPoint:Lcom/squareup/signature/Point;

    .line 215
    iput-object p2, p0, Lcom/squareup/signature/Spliner$Bezier;->endPoint:Lcom/squareup/signature/Point;

    .line 216
    iput-object p3, p0, Lcom/squareup/signature/Spliner$Bezier;->control1:Lcom/squareup/signature/Point;

    .line 217
    iput-object p4, p0, Lcom/squareup/signature/Spliner$Bezier;->control2:Lcom/squareup/signature/Point;

    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 10

    .line 222
    iget-object v0, p0, Lcom/squareup/signature/Spliner$Bezier;->path:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 223
    iget-object v0, p0, Lcom/squareup/signature/Spliner$Bezier;->path:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/squareup/signature/Spliner$Bezier;->startPoint:Lcom/squareup/signature/Point;

    iget v1, v1, Lcom/squareup/signature/Point;->x:F

    iget-object v2, p0, Lcom/squareup/signature/Spliner$Bezier;->startPoint:Lcom/squareup/signature/Point;

    iget v2, v2, Lcom/squareup/signature/Point;->y:F

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 224
    iget-object v3, p0, Lcom/squareup/signature/Spliner$Bezier;->path:Landroid/graphics/Path;

    iget-object v0, p0, Lcom/squareup/signature/Spliner$Bezier;->control1:Lcom/squareup/signature/Point;

    iget v4, v0, Lcom/squareup/signature/Point;->x:F

    iget-object v0, p0, Lcom/squareup/signature/Spliner$Bezier;->control1:Lcom/squareup/signature/Point;

    iget v5, v0, Lcom/squareup/signature/Point;->y:F

    iget-object v0, p0, Lcom/squareup/signature/Spliner$Bezier;->control2:Lcom/squareup/signature/Point;

    iget v6, v0, Lcom/squareup/signature/Point;->x:F

    iget-object v0, p0, Lcom/squareup/signature/Spliner$Bezier;->control2:Lcom/squareup/signature/Point;

    iget v7, v0, Lcom/squareup/signature/Point;->y:F

    iget-object v0, p0, Lcom/squareup/signature/Spliner$Bezier;->endPoint:Lcom/squareup/signature/Point;

    iget v8, v0, Lcom/squareup/signature/Point;->x:F

    iget-object v0, p0, Lcom/squareup/signature/Spliner$Bezier;->endPoint:Lcom/squareup/signature/Point;

    iget v9, v0, Lcom/squareup/signature/Point;->y:F

    invoke-virtual/range {v3 .. v9}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 225
    iget-object v0, p0, Lcom/squareup/signature/Spliner$Bezier;->path:Landroid/graphics/Path;

    invoke-virtual {p1, v0, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    return-void
.end method
