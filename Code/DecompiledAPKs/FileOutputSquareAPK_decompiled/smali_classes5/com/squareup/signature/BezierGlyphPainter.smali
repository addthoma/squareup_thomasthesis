.class final Lcom/squareup/signature/BezierGlyphPainter;
.super Ljava/lang/Object;
.source "BezierGlyphPainter.java"


# instance fields
.field private final canvas:Landroid/graphics/Canvas;

.field private final glyph:Lcom/squareup/signature/Signature$Glyph;

.field private final paint:Landroid/graphics/Paint;

.field private final spliner:Lcom/squareup/signature/Spliner;


# direct methods
.method constructor <init>(Landroid/graphics/Canvas;Landroid/graphics/Paint;Lcom/squareup/signature/Signature$Glyph;)V
    .locals 1

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    new-instance v0, Lcom/squareup/signature/Spliner;

    invoke-direct {v0}, Lcom/squareup/signature/Spliner;-><init>()V

    iput-object v0, p0, Lcom/squareup/signature/BezierGlyphPainter;->spliner:Lcom/squareup/signature/Spliner;

    .line 19
    iput-object p1, p0, Lcom/squareup/signature/BezierGlyphPainter;->canvas:Landroid/graphics/Canvas;

    .line 20
    iput-object p2, p0, Lcom/squareup/signature/BezierGlyphPainter;->paint:Landroid/graphics/Paint;

    .line 21
    iput-object p3, p0, Lcom/squareup/signature/BezierGlyphPainter;->glyph:Lcom/squareup/signature/Signature$Glyph;

    return-void
.end method


# virtual methods
.method public addPoint(Lcom/squareup/signature/Point$Timestamped;)V
    .locals 6

    .line 30
    iget-object v0, p0, Lcom/squareup/signature/BezierGlyphPainter;->glyph:Lcom/squareup/signature/Signature$Glyph;

    iget-wide v0, v0, Lcom/squareup/signature/Signature$Glyph;->startTime:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-gez v4, :cond_0

    iget-object v0, p0, Lcom/squareup/signature/BezierGlyphPainter;->glyph:Lcom/squareup/signature/Signature$Glyph;

    iget-wide v1, p1, Lcom/squareup/signature/Point$Timestamped;->time:J

    iput-wide v1, v0, Lcom/squareup/signature/Signature$Glyph;->startTime:J

    :cond_0
    const/4 v0, 0x0

    .line 35
    iget-object v1, p0, Lcom/squareup/signature/BezierGlyphPainter;->glyph:Lcom/squareup/signature/Signature$Glyph;

    iget-object v1, v1, Lcom/squareup/signature/Signature$Glyph;->points:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_2

    .line 36
    iget-object v0, p0, Lcom/squareup/signature/BezierGlyphPainter;->glyph:Lcom/squareup/signature/Signature$Glyph;

    iget-object v0, v0, Lcom/squareup/signature/Signature$Glyph;->points:Ljava/util/List;

    iget-object v1, p0, Lcom/squareup/signature/BezierGlyphPainter;->glyph:Lcom/squareup/signature/Signature$Glyph;

    iget-object v1, v1, Lcom/squareup/signature/Signature$Glyph;->points:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/signature/Point$Timestamped;

    .line 38
    iget v1, v0, Lcom/squareup/signature/Point$Timestamped;->x:F

    iget v2, p1, Lcom/squareup/signature/Point$Timestamped;->x:F

    cmpl-float v1, v1, v2

    if-nez v1, :cond_1

    iget v1, v0, Lcom/squareup/signature/Point$Timestamped;->y:F

    iget v2, p1, Lcom/squareup/signature/Point$Timestamped;->y:F

    cmpl-float v1, v1, v2

    if-nez v1, :cond_1

    return-void

    .line 43
    :cond_1
    iget-wide v1, v0, Lcom/squareup/signature/Point$Timestamped;->time:J

    iget-wide v3, p1, Lcom/squareup/signature/Point$Timestamped;->time:J

    cmp-long v5, v1, v3

    if-nez v5, :cond_2

    return-void

    .line 48
    :cond_2
    iget-object v1, p0, Lcom/squareup/signature/BezierGlyphPainter;->glyph:Lcom/squareup/signature/Signature$Glyph;

    iget-object v1, v1, Lcom/squareup/signature/Signature$Glyph;->points:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 49
    iget-object v1, p0, Lcom/squareup/signature/BezierGlyphPainter;->spliner:Lcom/squareup/signature/Spliner;

    invoke-virtual {v1, p1}, Lcom/squareup/signature/Spliner;->addPoint(Lcom/squareup/signature/Point;)V

    if-nez v0, :cond_3

    return-void

    .line 53
    :cond_3
    iget-object p1, p0, Lcom/squareup/signature/BezierGlyphPainter;->spliner:Lcom/squareup/signature/Spliner;

    invoke-virtual {p1}, Lcom/squareup/signature/Spliner;->getBeziers()Ljava/util/List;

    move-result-object p1

    .line 54
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    return-void

    .line 56
    :cond_4
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/signature/Spliner$Bezier;

    .line 58
    iget-object v0, p0, Lcom/squareup/signature/BezierGlyphPainter;->canvas:Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/squareup/signature/BezierGlyphPainter;->paint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Lcom/squareup/signature/Spliner$Bezier;->draw(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    return-void
.end method

.method public finish()V
    .locals 4

    .line 90
    iget-object v0, p0, Lcom/squareup/signature/BezierGlyphPainter;->spliner:Lcom/squareup/signature/Spliner;

    invoke-virtual {v0}, Lcom/squareup/signature/Spliner;->getBeziers()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/signature/BezierGlyphPainter;->glyph:Lcom/squareup/signature/Signature$Glyph;

    iget-object v0, v0, Lcom/squareup/signature/Signature$Glyph;->points:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 91
    iget-object v0, p0, Lcom/squareup/signature/BezierGlyphPainter;->glyph:Lcom/squareup/signature/Signature$Glyph;

    iget-object v0, v0, Lcom/squareup/signature/Signature$Glyph;->points:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/signature/Point;

    .line 92
    iget-object v1, p0, Lcom/squareup/signature/BezierGlyphPainter;->canvas:Landroid/graphics/Canvas;

    iget v2, v0, Lcom/squareup/signature/Point;->x:F

    iget v0, v0, Lcom/squareup/signature/Point;->y:F

    iget-object v3, p0, Lcom/squareup/signature/BezierGlyphPainter;->paint:Landroid/graphics/Paint;

    invoke-virtual {v1, v2, v0, v3}, Landroid/graphics/Canvas;->drawPoint(FFLandroid/graphics/Paint;)V

    :cond_0
    return-void
.end method

.method public getPointCount()I
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/signature/BezierGlyphPainter;->glyph:Lcom/squareup/signature/Signature$Glyph;

    iget-object v0, v0, Lcom/squareup/signature/Signature$Glyph;->points:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public invalidate(Landroid/view/View;)V
    .locals 6

    .line 63
    iget-object v0, p0, Lcom/squareup/signature/BezierGlyphPainter;->paint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    neg-int v0, v0

    .line 68
    iget-object v1, p0, Lcom/squareup/signature/BezierGlyphPainter;->spliner:Lcom/squareup/signature/Spliner;

    invoke-virtual {v1}, Lcom/squareup/signature/Spliner;->getBeziers()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/squareup/signature/BezierGlyphPainter;->glyph:Lcom/squareup/signature/Signature$Glyph;

    iget-object v1, v1, Lcom/squareup/signature/Signature$Glyph;->points:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 69
    iget-object v1, p0, Lcom/squareup/signature/BezierGlyphPainter;->glyph:Lcom/squareup/signature/Signature$Glyph;

    iget-object v1, v1, Lcom/squareup/signature/Signature$Glyph;->points:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/signature/Point;

    .line 70
    new-instance v2, Landroid/graphics/Rect;

    iget v3, v1, Lcom/squareup/signature/Point;->x:F

    float-to-int v3, v3

    iget v4, v1, Lcom/squareup/signature/Point;->y:F

    float-to-int v4, v4

    iget v5, v1, Lcom/squareup/signature/Point;->x:F

    float-to-int v5, v5

    iget v1, v1, Lcom/squareup/signature/Point;->y:F

    float-to-int v1, v1

    invoke-direct {v2, v3, v4, v5, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 71
    invoke-virtual {v2, v0, v0}, Landroid/graphics/Rect;->inset(II)V

    .line 72
    invoke-virtual {p1, v2}, Landroid/view/View;->invalidate(Landroid/graphics/Rect;)V

    .line 76
    :cond_0
    iget-object v1, p0, Lcom/squareup/signature/BezierGlyphPainter;->spliner:Lcom/squareup/signature/Spliner;

    invoke-virtual {v1}, Lcom/squareup/signature/Spliner;->isDirty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 77
    iget-object v1, p0, Lcom/squareup/signature/BezierGlyphPainter;->spliner:Lcom/squareup/signature/Spliner;

    invoke-virtual {v1}, Lcom/squareup/signature/Spliner;->getDirtyRect()Landroid/graphics/Rect;

    move-result-object v1

    .line 78
    invoke-virtual {v1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 79
    invoke-virtual {v1, v0, v0}, Landroid/graphics/Rect;->inset(II)V

    .line 81
    :cond_1
    invoke-virtual {p1, v1}, Landroid/view/View;->invalidate(Landroid/graphics/Rect;)V

    .line 82
    iget-object p1, p0, Lcom/squareup/signature/BezierGlyphPainter;->spliner:Lcom/squareup/signature/Spliner;

    invoke-virtual {p1}, Lcom/squareup/signature/Spliner;->resetDirty()V

    :cond_2
    return-void
.end method
