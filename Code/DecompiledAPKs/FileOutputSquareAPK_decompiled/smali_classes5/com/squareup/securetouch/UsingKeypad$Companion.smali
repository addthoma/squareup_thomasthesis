.class public final Lcom/squareup/securetouch/UsingKeypad$Companion;
.super Ljava/lang/Object;
.source "SecureTouchState.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/securetouch/UsingKeypad;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/securetouch/UsingKeypad$Companion;",
        "",
        "()V",
        "fromAwaitingKeypad",
        "Lcom/squareup/securetouch/UsingKeypad;",
        "state",
        "Lcom/squareup/securetouch/AwaitingKeypad;",
        "secure-touch_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 41
    invoke-direct {p0}, Lcom/squareup/securetouch/UsingKeypad$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final fromAwaitingKeypad(Lcom/squareup/securetouch/AwaitingKeypad;)Lcom/squareup/securetouch/UsingKeypad;
    .locals 7

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    new-instance v0, Lcom/squareup/securetouch/UsingKeypad;

    .line 46
    invoke-virtual {p1}, Lcom/squareup/securetouch/AwaitingKeypad;->getPinEntryState()Lcom/squareup/securetouch/SecureTouchPinEntryState;

    move-result-object v3

    .line 47
    invoke-virtual {p1}, Lcom/squareup/securetouch/AwaitingKeypad;->getUseHighContrastMode()Z

    move-result v4

    const/4 v2, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object v1, v0

    .line 45
    invoke-direct/range {v1 .. v6}, Lcom/squareup/securetouch/UsingKeypad;-><init>(ILcom/squareup/securetouch/SecureTouchPinEntryState;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method
