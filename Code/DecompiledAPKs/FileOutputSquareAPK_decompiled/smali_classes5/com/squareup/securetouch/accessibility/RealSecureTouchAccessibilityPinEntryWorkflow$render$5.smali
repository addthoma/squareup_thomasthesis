.class final Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$render$5;
.super Lkotlin/jvm/internal/Lambda;
.source "RealSecureTouchAccessibilityPinEntryWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow;->render(Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityInput;Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/securetouch/MotionTouchEventDevOnly;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "touch",
        "Lcom/squareup/securetouch/MotionTouchEventDevOnly;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $context:Lcom/squareup/workflow/RenderContext;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/RenderContext;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$render$5;->$context:Lcom/squareup/workflow/RenderContext;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 63
    check-cast p1, Lcom/squareup/securetouch/MotionTouchEventDevOnly;

    invoke-virtual {p0, p1}, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$render$5;->invoke(Lcom/squareup/securetouch/MotionTouchEventDevOnly;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/securetouch/MotionTouchEventDevOnly;)V
    .locals 2

    const-string/jumbo v0, "touch"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 120
    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$render$5;->$context:Lcom/squareup/workflow/RenderContext;

    invoke-interface {v0}, Lcom/squareup/workflow/RenderContext;->getActionSink()Lcom/squareup/workflow/Sink;

    move-result-object v0

    new-instance v1, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action$TouchEventDevOnly;

    invoke-virtual {p1}, Lcom/squareup/securetouch/MotionTouchEventDevOnly;->getAtSecureTouchPoint()Lcom/squareup/securetouch/SecureTouchPoint;

    move-result-object p1

    invoke-direct {v1, p1}, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$Action$TouchEventDevOnly;-><init>(Lcom/squareup/securetouch/SecureTouchPoint;)V

    invoke-interface {v0, v1}, Lcom/squareup/workflow/Sink;->send(Ljava/lang/Object;)V

    return-void
.end method
