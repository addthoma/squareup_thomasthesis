.class final Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$render$7;
.super Lkotlin/jvm/internal/Lambda;
.source "RealSecureTouchAccessibilityPinEntryWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow;->render(Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityInput;Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/securetouch/SecureTouchAccessibilityKeypadActive;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealSecureTouchAccessibilityPinEntryWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealSecureTouchAccessibilityPinEntryWorkflow.kt\ncom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$render$7\n*L\n1#1,339:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/securetouch/SecureTouchAccessibilityKeypadActive;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$render$7;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$render$7;

    invoke-direct {v0}, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$render$7;-><init>()V

    sput-object v0, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$render$7;->INSTANCE:Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$render$7;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 63
    check-cast p1, Lcom/squareup/securetouch/SecureTouchAccessibilityKeypadActive;

    invoke-virtual {p0, p1}, Lcom/squareup/securetouch/accessibility/RealSecureTouchAccessibilityPinEntryWorkflow$render$7;->invoke(Lcom/squareup/securetouch/SecureTouchAccessibilityKeypadActive;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/securetouch/SecureTouchAccessibilityKeypadActive;)V
    .locals 1

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 137
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Unexpected active keypad!"

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method
