.class public final Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryViewFactory;
.super Lcom/squareup/workflow/AbstractWorkflowViewFactory;
.source "SecureTouchAccessibilityPinEntryViewFactory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004\u00a8\u0006\u0005"
    }
    d2 = {
        "Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryViewFactory;",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory;",
        "accessibleKeypadFactory",
        "Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator$Factory;",
        "(Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator$Factory;)V",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator$Factory;)V
    .locals 9
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "accessibleKeypadFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    .line 10
    sget-object v1, Lcom/squareup/workflow/AbstractWorkflowViewFactory;->Companion:Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;

    .line 11
    sget-object v2, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;->Companion:Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$Companion;

    invoke-virtual {v2}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$Companion;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    .line 12
    sget v3, Lcom/squareup/securetouch/accessibility/impl/R$layout;->screen_container:I

    .line 13
    new-instance v4, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryViewFactory$1;

    invoke-direct {v4, p1}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryViewFactory$1;-><init>(Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator$Factory;)V

    move-object v6, v4

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v7, 0xc

    const/4 v8, 0x0

    .line 10
    invoke-static/range {v1 .. v8}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;->bindLayout$default(Lcom/squareup/workflow/AbstractWorkflowViewFactory$Companion;Lcom/squareup/workflow/legacy/Screen$Key;ILcom/squareup/workflow/ScreenHint;Lcom/squareup/workflow/InflaterDelegate;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;

    move-result-object p1

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 9
    invoke-direct {p0, v0}, Lcom/squareup/workflow/AbstractWorkflowViewFactory;-><init>([Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;)V

    return-void
.end method
