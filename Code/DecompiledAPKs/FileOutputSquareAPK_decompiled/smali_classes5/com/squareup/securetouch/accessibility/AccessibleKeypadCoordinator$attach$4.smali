.class final Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator$attach$4;
.super Lkotlin/jvm/internal/Lambda;
.source "AccessibleKeypadCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lkotlin/Pair<",
        "+",
        "Lcom/squareup/workflow/legacy/Screen;",
        "+",
        "Ljava/lang/Boolean;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012R\u0010\u0002\u001aN\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u0007\u0012\u0004\u0012\u00020\u0008 \t*&\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0008\u0012\u0004\u0012\u00020\u0005`\u0007\u0012\u0004\u0012\u00020\u0008\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\n"
    }
    d2 = {
        "<anonymous>",
        "",
        "<name for destructuring parameter 0>",
        "Lkotlin/Pair;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "",
        "kotlin.jvm.PlatformType",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator$attach$4;->this$0:Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 61
    check-cast p1, Lkotlin/Pair;

    invoke-virtual {p0, p1}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator$attach$4;->invoke(Lkotlin/Pair;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lkotlin/Pair;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "Lcom/squareup/workflow/legacy/Screen;",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    .line 108
    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator$attach$4;->this$0:Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;

    invoke-static {p1}, Lcom/squareup/workflow/legacy/ScreenKt;->getUnwrapV2Screen(Lcom/squareup/workflow/legacy/Screen;)Lcom/squareup/workflow/legacy/V2Screen;

    move-result-object p1

    check-cast p1, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;

    iget-object v1, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator$attach$4;->this$0:Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;

    invoke-static {v1}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->access$getBuyerLocaleOverride$p(Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;)Lcom/squareup/buyer/language/BuyerLocaleOverride;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;->access$announceInstructions(Lcom/squareup/securetouch/accessibility/AccessibleKeypadCoordinator;Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;Lcom/squareup/buyer/language/BuyerLocaleOverride;)V

    return-void
.end method
