.class public final Lcom/squareup/securetouch/accessibility/impl/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/securetouch/accessibility/impl/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final accessible_keypad_cancel_button_margin:I = 0x7f07004e

.field public static final accessible_keypad_cancel_button_padding:I = 0x7f07004f

.field public static final accessible_keypad_digit_radius:I = 0x7f070050

.field public static final accessible_keypad_margin:I = 0x7f070051

.field public static final accessible_keypad_screen_title_size:I = 0x7f070052

.field public static final accessible_keypad_star_gap:I = 0x7f070053

.field public static final accessible_keypad_star_radius:I = 0x7f070054

.field public static final digits_0_through_9_text_size:I = 0x7f070138

.field public static final digits_1_through_9_radius:I = 0x7f070139

.field public static final noho_squid_keypad_action_bar_offset:I = 0x7f0703a9

.field public static final text_color_alpha:I = 0x7f07052e

.field public static final text_occluded_color_alpha:I = 0x7f07052f


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
