.class public final Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;
.super Ljava/lang/Object;
.source "AccessibleKeypadScreen.kt"

# interfaces
.implements Lcom/squareup/workflow/legacy/V2Screen;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData;,
        Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\r\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u0008\u0086\u0008\u0018\u0000 !2\u00020\u0001:\u0002 !BM\u0012\u0012\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003\u0012\u0014\u0008\u0002\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00050\u0003\u0012\u0014\u0008\u0002\u0010\u0008\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\u00050\u0003\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u0015\u0010\u0013\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003H\u00c6\u0003J\u0015\u0010\u0014\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00050\u0003H\u00c6\u0003J\u0015\u0010\u0015\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\u00050\u0003H\u00c6\u0003J\t\u0010\u0016\u001a\u00020\u000bH\u00c6\u0003JU\u0010\u0017\u001a\u00020\u00002\u0014\u0008\u0002\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u00032\u0014\u0008\u0002\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00050\u00032\u0014\u0008\u0002\u0010\u0008\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\u00050\u00032\u0008\u0008\u0002\u0010\n\u001a\u00020\u000bH\u00c6\u0001J\u0013\u0010\u0018\u001a\u00020\u00192\u0008\u0010\u001a\u001a\u0004\u0018\u00010\u001bH\u00d6\u0003J\t\u0010\u001c\u001a\u00020\u001dH\u00d6\u0001J\t\u0010\u001e\u001a\u00020\u001fH\u00d6\u0001R\u001d\u0010\u0008\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\u00050\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u001d\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00050\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u000eR\u001d\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u000eR\u0011\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012\u00a8\u0006\""
    }
    d2 = {
        "Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;",
        "Lcom/squareup/workflow/legacy/V2Screen;",
        "onKeypadActive",
        "Lkotlin/Function1;",
        "Lcom/squareup/securetouch/SecureTouchAccessibilityKeypadActive;",
        "",
        "motionTouchEventDevOnly",
        "Lcom/squareup/securetouch/MotionTouchEventDevOnly;",
        "motionReleaseEventDevOnly",
        "Lcom/squareup/securetouch/MotionReleaseEventDevOnly;",
        "screenData",
        "Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData;",
        "(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData;)V",
        "getMotionReleaseEventDevOnly",
        "()Lkotlin/jvm/functions/Function1;",
        "getMotionTouchEventDevOnly",
        "getOnKeypadActive",
        "getScreenData",
        "()Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData;",
        "component1",
        "component2",
        "component3",
        "component4",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "AccessibleKeypadScreenData",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$Companion;

.field private static final KEY:Lcom/squareup/workflow/legacy/Screen$Key;

.field public static final SECURE_TOUCH_ACCESSIBILITY_ANNOUNCE_DIGITS_ENTERED_DEBOUNCE_MS:J = 0xfaL

.field public static final SECURE_TOUCH_ACCESSIBILITY_EVENT_DEBOUNCE_MS:J = 0x1eL


# instance fields
.field private final motionReleaseEventDevOnly:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/securetouch/MotionReleaseEventDevOnly;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final motionTouchEventDevOnly:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/securetouch/MotionTouchEventDevOnly;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onKeypadActive:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/securetouch/SecureTouchAccessibilityKeypadActive;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final screenData:Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;->Companion:Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$Companion;

    .line 58
    const-class v0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    const/4 v2, 0x1

    invoke-static {v0, v1, v2, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey$default(Lkotlin/reflect/KClass;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v0

    sput-object v0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    return-void
.end method

.method public constructor <init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/securetouch/SecureTouchAccessibilityKeypadActive;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/securetouch/MotionTouchEventDevOnly;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/securetouch/MotionReleaseEventDevOnly;",
            "Lkotlin/Unit;",
            ">;",
            "Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData;",
            ")V"
        }
    .end annotation

    const-string v0, "onKeypadActive"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "motionTouchEventDevOnly"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "motionReleaseEventDevOnly"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "screenData"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;->onKeypadActive:Lkotlin/jvm/functions/Function1;

    iput-object p2, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;->motionTouchEventDevOnly:Lkotlin/jvm/functions/Function1;

    iput-object p3, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;->motionReleaseEventDevOnly:Lkotlin/jvm/functions/Function1;

    iput-object p4, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;->screenData:Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData;

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_0

    .line 18
    sget-object p2, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$1;->INSTANCE:Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$1;

    check-cast p2, Lkotlin/jvm/functions/Function1;

    :cond_0
    and-int/lit8 p5, p5, 0x4

    if-eqz p5, :cond_1

    .line 21
    sget-object p3, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$2;->INSTANCE:Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$2;

    check-cast p3, Lkotlin/jvm/functions/Function1;

    :cond_1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;-><init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData;)V

    return-void
.end method

.method public static final synthetic access$getKEY$cp()Lcom/squareup/workflow/legacy/Screen$Key;
    .locals 1

    .line 16
    sget-object v0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    return-object v0
.end method

.method public static synthetic copy$default(Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData;ILjava/lang/Object;)Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-object p1, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;->onKeypadActive:Lkotlin/jvm/functions/Function1;

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-object p2, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;->motionTouchEventDevOnly:Lkotlin/jvm/functions/Function1;

    :cond_1
    and-int/lit8 p6, p5, 0x4

    if-eqz p6, :cond_2

    iget-object p3, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;->motionReleaseEventDevOnly:Lkotlin/jvm/functions/Function1;

    :cond_2
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_3

    iget-object p4, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;->screenData:Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData;

    :cond_3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;->copy(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData;)Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/securetouch/SecureTouchAccessibilityKeypadActive;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;->onKeypadActive:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final component2()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/securetouch/MotionTouchEventDevOnly;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;->motionTouchEventDevOnly:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final component3()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/securetouch/MotionReleaseEventDevOnly;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;->motionReleaseEventDevOnly:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final component4()Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData;
    .locals 1

    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;->screenData:Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData;

    return-object v0
.end method

.method public final copy(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData;)Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/securetouch/SecureTouchAccessibilityKeypadActive;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/securetouch/MotionTouchEventDevOnly;",
            "Lkotlin/Unit;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/securetouch/MotionReleaseEventDevOnly;",
            "Lkotlin/Unit;",
            ">;",
            "Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData;",
            ")",
            "Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;"
        }
    .end annotation

    const-string v0, "onKeypadActive"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "motionTouchEventDevOnly"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "motionReleaseEventDevOnly"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "screenData"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;-><init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;

    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;->onKeypadActive:Lkotlin/jvm/functions/Function1;

    iget-object v1, p1, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;->onKeypadActive:Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;->motionTouchEventDevOnly:Lkotlin/jvm/functions/Function1;

    iget-object v1, p1, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;->motionTouchEventDevOnly:Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;->motionReleaseEventDevOnly:Lkotlin/jvm/functions/Function1;

    iget-object v1, p1, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;->motionReleaseEventDevOnly:Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;->screenData:Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData;

    iget-object p1, p1, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;->screenData:Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getMotionReleaseEventDevOnly()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/securetouch/MotionReleaseEventDevOnly;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 21
    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;->motionReleaseEventDevOnly:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getMotionTouchEventDevOnly()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/securetouch/MotionTouchEventDevOnly;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 18
    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;->motionTouchEventDevOnly:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getOnKeypadActive()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/squareup/securetouch/SecureTouchAccessibilityKeypadActive;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 17
    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;->onKeypadActive:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getScreenData()Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData;
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;->screenData:Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;->onKeypadActive:Lkotlin/jvm/functions/Function1;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;->motionTouchEventDevOnly:Lkotlin/jvm/functions/Function1;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;->motionReleaseEventDevOnly:Lkotlin/jvm/functions/Function1;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;->screenData:Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_3
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AccessibleKeypadScreen(onKeypadActive="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;->onKeypadActive:Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", motionTouchEventDevOnly="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;->motionTouchEventDevOnly:Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", motionReleaseEventDevOnly="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;->motionReleaseEventDevOnly:Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", screenData="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen;->screenData:Lcom/squareup/securetouch/accessibility/AccessibleKeypadScreen$AccessibleKeypadScreenData;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
