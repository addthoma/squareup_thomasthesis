.class public final Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer_Factory;
.super Ljava/lang/Object;
.source "SecureTouchAccessibilityPinEntryAudioPlayer_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/SerialExecutor;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/SerialExecutor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 25
    iput-object p2, p0, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 26
    iput-object p3, p0, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer_Factory;->arg2Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/executor/SerialExecutor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;)",
            "Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer_Factory;"
        }
    .end annotation

    .line 37
    new-instance v0, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/thread/executor/SerialExecutor;Landroid/app/Application;)Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer;
    .locals 1

    .line 42
    new-instance v0, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer;-><init>(Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/thread/executor/SerialExecutor;Landroid/app/Application;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer;
    .locals 3

    .line 31
    iget-object v0, p0, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/buyer/language/BuyerLocaleOverride;

    iget-object v1, p0, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/thread/executor/SerialExecutor;

    iget-object v2, p0, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/Application;

    invoke-static {v0, v1, v2}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer_Factory;->newInstance(Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/thread/executor/SerialExecutor;Landroid/app/Application;)Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer_Factory;->get()Lcom/squareup/securetouch/accessibility/SecureTouchAccessibilityPinEntryAudioPlayer;

    move-result-object v0

    return-object v0
.end method
