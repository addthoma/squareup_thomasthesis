.class public final Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;
.super Ljava/lang/Object;
.source "SecureTouchScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/securetouch/SecureTouchScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SecureTouchScreenData"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u001c\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u0000 \'2\u00020\u0001:\u0001\'BU\u0012\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0005\u0012\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u0005\u0012\u0008\u0008\u0002\u0010\u0008\u001a\u00020\t\u0012\u0008\u0008\u0002\u0010\n\u001a\u00020\u0005\u0012\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u0005\u0012\u0008\u0008\u0002\u0010\u000c\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\rJ\t\u0010\u0019\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u001a\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u001b\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u001c\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u001d\u001a\u00020\tH\u00c6\u0003J\t\u0010\u001e\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u001f\u001a\u00020\u0005H\u00c6\u0003J\t\u0010 \u001a\u00020\u0005H\u00c6\u0003JY\u0010!\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0007\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0008\u001a\u00020\t2\u0008\u0008\u0002\u0010\n\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u000b\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u000c\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\"\u001a\u00020\u00052\u0008\u0010#\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010$\u001a\u00020\u0003H\u00d6\u0001J\t\u0010%\u001a\u00020&H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0011\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u000fR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0011\u0010\u0012R\u0011\u0010\u0007\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0013\u0010\u000fR\u0011\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0015R\u0011\u0010\u000c\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u000fR\u0011\u0010\u000b\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\u000fR\u0011\u0010\n\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u000f\u00a8\u0006("
    }
    d2 = {
        "Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;",
        "",
        "digitsEntered",
        "",
        "clearEnabled",
        "",
        "deleteEnabled",
        "doneEnabled",
        "pinEntryState",
        "Lcom/squareup/securetouch/SecureTouchPinEntryState;",
        "waitingForReader",
        "waitingForKeypadLayout",
        "useHighContrastMode",
        "(IZZZLcom/squareup/securetouch/SecureTouchPinEntryState;ZZZ)V",
        "getClearEnabled",
        "()Z",
        "getDeleteEnabled",
        "getDigitsEntered",
        "()I",
        "getDoneEnabled",
        "getPinEntryState",
        "()Lcom/squareup/securetouch/SecureTouchPinEntryState;",
        "getUseHighContrastMode",
        "getWaitingForKeypadLayout",
        "getWaitingForReader",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "component6",
        "component7",
        "component8",
        "copy",
        "equals",
        "other",
        "hashCode",
        "toString",
        "",
        "Companion",
        "secure-touch_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData$Companion;


# instance fields
.field private final clearEnabled:Z

.field private final deleteEnabled:Z

.field private final digitsEntered:I

.field private final doneEnabled:Z

.field private final pinEntryState:Lcom/squareup/securetouch/SecureTouchPinEntryState;

.field private final useHighContrastMode:Z

.field private final waitingForKeypadLayout:Z

.field private final waitingForReader:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->Companion:Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData$Companion;

    return-void
.end method

.method public constructor <init>()V
    .locals 11

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v9, 0xff

    const/4 v10, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v10}, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;-><init>(IZZZLcom/squareup/securetouch/SecureTouchPinEntryState;ZZZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(IZZZLcom/squareup/securetouch/SecureTouchPinEntryState;ZZZ)V
    .locals 1

    const-string v0, "pinEntryState"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->digitsEntered:I

    iput-boolean p2, p0, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->clearEnabled:Z

    iput-boolean p3, p0, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->deleteEnabled:Z

    iput-boolean p4, p0, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->doneEnabled:Z

    iput-object p5, p0, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->pinEntryState:Lcom/squareup/securetouch/SecureTouchPinEntryState;

    iput-boolean p6, p0, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->waitingForReader:Z

    iput-boolean p7, p0, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->waitingForKeypadLayout:Z

    iput-boolean p8, p0, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->useHighContrastMode:Z

    return-void
.end method

.method public synthetic constructor <init>(IZZZLcom/squareup/securetouch/SecureTouchPinEntryState;ZZZILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 9

    move/from16 v0, p9

    and-int/lit8 v1, v0, 0x1

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    move v1, p1

    :goto_0
    and-int/lit8 v3, v0, 0x2

    if-eqz v3, :cond_1

    const/4 v3, 0x0

    goto :goto_1

    :cond_1
    move v3, p2

    :goto_1
    and-int/lit8 v4, v0, 0x4

    if-eqz v4, :cond_2

    const/4 v4, 0x0

    goto :goto_2

    :cond_2
    move v4, p3

    :goto_2
    and-int/lit8 v5, v0, 0x8

    if-eqz v5, :cond_3

    const/4 v5, 0x0

    goto :goto_3

    :cond_3
    move v5, p4

    :goto_3
    and-int/lit8 v6, v0, 0x10

    if-eqz v6, :cond_4

    .line 17
    sget-object v6, Lcom/squareup/securetouch/PinFirstTry;->INSTANCE:Lcom/squareup/securetouch/PinFirstTry;

    check-cast v6, Lcom/squareup/securetouch/SecureTouchPinEntryState;

    goto :goto_4

    :cond_4
    move-object v6, p5

    :goto_4
    and-int/lit8 v7, v0, 0x20

    if-eqz v7, :cond_5

    const/4 v7, 0x0

    goto :goto_5

    :cond_5
    move v7, p6

    :goto_5
    and-int/lit8 v8, v0, 0x40

    if-eqz v8, :cond_6

    const/4 v8, 0x0

    goto :goto_6

    :cond_6
    move/from16 v8, p7

    :goto_6
    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_7

    goto :goto_7

    :cond_7
    move/from16 v2, p8

    :goto_7
    move-object p1, p0

    move p2, v1

    move p3, v3

    move p4, v4

    move p5, v5

    move-object p6, v6

    move/from16 p7, v7

    move/from16 p8, v8

    move/from16 p9, v2

    .line 20
    invoke-direct/range {p1 .. p9}, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;-><init>(IZZZLcom/squareup/securetouch/SecureTouchPinEntryState;ZZZ)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;IZZZLcom/squareup/securetouch/SecureTouchPinEntryState;ZZZILjava/lang/Object;)Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;
    .locals 9

    move-object v0, p0

    move/from16 v1, p9

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_0

    iget v2, v0, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->digitsEntered:I

    goto :goto_0

    :cond_0
    move v2, p1

    :goto_0
    and-int/lit8 v3, v1, 0x2

    if-eqz v3, :cond_1

    iget-boolean v3, v0, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->clearEnabled:Z

    goto :goto_1

    :cond_1
    move v3, p2

    :goto_1
    and-int/lit8 v4, v1, 0x4

    if-eqz v4, :cond_2

    iget-boolean v4, v0, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->deleteEnabled:Z

    goto :goto_2

    :cond_2
    move v4, p3

    :goto_2
    and-int/lit8 v5, v1, 0x8

    if-eqz v5, :cond_3

    iget-boolean v5, v0, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->doneEnabled:Z

    goto :goto_3

    :cond_3
    move v5, p4

    :goto_3
    and-int/lit8 v6, v1, 0x10

    if-eqz v6, :cond_4

    iget-object v6, v0, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->pinEntryState:Lcom/squareup/securetouch/SecureTouchPinEntryState;

    goto :goto_4

    :cond_4
    move-object v6, p5

    :goto_4
    and-int/lit8 v7, v1, 0x20

    if-eqz v7, :cond_5

    iget-boolean v7, v0, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->waitingForReader:Z

    goto :goto_5

    :cond_5
    move v7, p6

    :goto_5
    and-int/lit8 v8, v1, 0x40

    if-eqz v8, :cond_6

    iget-boolean v8, v0, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->waitingForKeypadLayout:Z

    goto :goto_6

    :cond_6
    move/from16 v8, p7

    :goto_6
    and-int/lit16 v1, v1, 0x80

    if-eqz v1, :cond_7

    iget-boolean v1, v0, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->useHighContrastMode:Z

    goto :goto_7

    :cond_7
    move/from16 v1, p8

    :goto_7
    move p1, v2

    move p2, v3

    move p3, v4

    move p4, v5

    move-object p5, v6

    move p6, v7

    move/from16 p7, v8

    move/from16 p8, v1

    invoke-virtual/range {p0 .. p8}, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->copy(IZZZLcom/squareup/securetouch/SecureTouchPinEntryState;ZZZ)Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final component1()I
    .locals 1

    iget v0, p0, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->digitsEntered:I

    return v0
.end method

.method public final component2()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->clearEnabled:Z

    return v0
.end method

.method public final component3()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->deleteEnabled:Z

    return v0
.end method

.method public final component4()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->doneEnabled:Z

    return v0
.end method

.method public final component5()Lcom/squareup/securetouch/SecureTouchPinEntryState;
    .locals 1

    iget-object v0, p0, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->pinEntryState:Lcom/squareup/securetouch/SecureTouchPinEntryState;

    return-object v0
.end method

.method public final component6()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->waitingForReader:Z

    return v0
.end method

.method public final component7()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->waitingForKeypadLayout:Z

    return v0
.end method

.method public final component8()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->useHighContrastMode:Z

    return v0
.end method

.method public final copy(IZZZLcom/squareup/securetouch/SecureTouchPinEntryState;ZZZ)Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;
    .locals 10

    const-string v0, "pinEntryState"

    move-object v6, p5

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;

    move-object v1, v0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    invoke-direct/range {v1 .. v9}, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;-><init>(IZZZLcom/squareup/securetouch/SecureTouchPinEntryState;ZZZ)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;

    iget v0, p0, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->digitsEntered:I

    iget v1, p1, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->digitsEntered:I

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->clearEnabled:Z

    iget-boolean v1, p1, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->clearEnabled:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->deleteEnabled:Z

    iget-boolean v1, p1, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->deleteEnabled:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->doneEnabled:Z

    iget-boolean v1, p1, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->doneEnabled:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->pinEntryState:Lcom/squareup/securetouch/SecureTouchPinEntryState;

    iget-object v1, p1, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->pinEntryState:Lcom/squareup/securetouch/SecureTouchPinEntryState;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->waitingForReader:Z

    iget-boolean v1, p1, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->waitingForReader:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->waitingForKeypadLayout:Z

    iget-boolean v1, p1, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->waitingForKeypadLayout:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->useHighContrastMode:Z

    iget-boolean p1, p1, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->useHighContrastMode:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getClearEnabled()Z
    .locals 1

    .line 14
    iget-boolean v0, p0, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->clearEnabled:Z

    return v0
.end method

.method public final getDeleteEnabled()Z
    .locals 1

    .line 15
    iget-boolean v0, p0, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->deleteEnabled:Z

    return v0
.end method

.method public final getDigitsEntered()I
    .locals 1

    .line 13
    iget v0, p0, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->digitsEntered:I

    return v0
.end method

.method public final getDoneEnabled()Z
    .locals 1

    .line 16
    iget-boolean v0, p0, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->doneEnabled:Z

    return v0
.end method

.method public final getPinEntryState()Lcom/squareup/securetouch/SecureTouchPinEntryState;
    .locals 1

    .line 17
    iget-object v0, p0, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->pinEntryState:Lcom/squareup/securetouch/SecureTouchPinEntryState;

    return-object v0
.end method

.method public final getUseHighContrastMode()Z
    .locals 1

    .line 20
    iget-boolean v0, p0, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->useHighContrastMode:Z

    return v0
.end method

.method public final getWaitingForKeypadLayout()Z
    .locals 1

    .line 19
    iget-boolean v0, p0, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->waitingForKeypadLayout:Z

    return v0
.end method

.method public final getWaitingForReader()Z
    .locals 1

    .line 18
    iget-boolean v0, p0, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->waitingForReader:Z

    return v0
.end method

.method public hashCode()I
    .locals 3

    iget v0, p0, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->digitsEntered:I

    invoke-static {v0}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->clearEnabled:Z

    const/4 v2, 0x1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :cond_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->deleteEnabled:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :cond_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->doneEnabled:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :cond_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->pinEntryState:Lcom/squareup/securetouch/SecureTouchPinEntryState;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->waitingForReader:Z

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    :cond_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->waitingForKeypadLayout:Z

    if-eqz v1, :cond_5

    const/4 v1, 0x1

    :cond_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->useHighContrastMode:Z

    if-eqz v1, :cond_6

    const/4 v1, 0x1

    :cond_6
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SecureTouchScreenData(digitsEntered="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->digitsEntered:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", clearEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->clearEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", deleteEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->deleteEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", doneEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->doneEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", pinEntryState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->pinEntryState:Lcom/squareup/securetouch/SecureTouchPinEntryState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", waitingForReader="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->waitingForReader:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", waitingForKeypadLayout="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->waitingForKeypadLayout:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", useHighContrastMode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/securetouch/SecureTouchScreen$SecureTouchScreenData;->useHighContrastMode:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
