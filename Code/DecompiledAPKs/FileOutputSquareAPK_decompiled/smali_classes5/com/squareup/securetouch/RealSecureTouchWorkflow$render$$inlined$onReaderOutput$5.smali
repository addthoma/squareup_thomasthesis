.class public final Lcom/squareup/securetouch/RealSecureTouchWorkflow$render$$inlined$onReaderOutput$5;
.super Lkotlin/jvm/internal/Lambda;
.source "RealSecureTouchWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/securetouch/RealSecureTouchWorkflow;->render(Lcom/squareup/securetouch/SecureTouchInput;Lcom/squareup/securetouch/SecureTouchState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/securetouch/SecureTouchEnabled;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/securetouch/SecureTouchState;",
        "+",
        "Lcom/squareup/securetouch/SecureTouchResult;",
        ">;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealSecureTouchWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealSecureTouchWorkflow.kt\ncom/squareup/securetouch/RealSecureTouchWorkflow$onReaderOutput$1\n+ 2 RealSecureTouchWorkflow.kt\ncom/squareup/securetouch/RealSecureTouchWorkflow\n*L\n1#1,386:1\n160#2:387\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0004\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001\"\n\u0008\u0000\u0010\u0004\u0018\u0001*\u00020\u00052\u0006\u0010\u0006\u001a\u0002H\u0004H\n\u00a2\u0006\u0004\u0008\u0007\u0010\u0008\u00a8\u0006\t"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/securetouch/SecureTouchState;",
        "Lcom/squareup/securetouch/SecureTouchResult;",
        "T",
        "Lcom/squareup/securetouch/SecureTouchFeatureEvent;",
        "event",
        "invoke",
        "(Lcom/squareup/securetouch/SecureTouchFeatureEvent;)Lcom/squareup/workflow/WorkflowAction;",
        "com/squareup/securetouch/RealSecureTouchWorkflow$onReaderOutput$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state$inlined:Lcom/squareup/securetouch/SecureTouchState;


# direct methods
.method public constructor <init>(Lcom/squareup/securetouch/SecureTouchState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflow$render$$inlined$onReaderOutput$5;->$state$inlined:Lcom/squareup/securetouch/SecureTouchState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/securetouch/SecureTouchFeatureEvent;)Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/securetouch/SecureTouchEnabled;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/securetouch/SecureTouchState;",
            "Lcom/squareup/securetouch/SecureTouchResult;",
            ">;"
        }
    .end annotation

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 339
    check-cast p1, Lcom/squareup/securetouch/SecureTouchEnabled;

    .line 387
    iget-object p1, p0, Lcom/squareup/securetouch/RealSecureTouchWorkflow$render$$inlined$onReaderOutput$5;->$state$inlined:Lcom/squareup/securetouch/SecureTouchState;

    check-cast p1, Lcom/squareup/securetouch/EnablingKeypad;

    invoke-virtual {p1}, Lcom/squareup/securetouch/EnablingKeypad;->getOnSecureTouchEnabled()Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 48
    check-cast p1, Lcom/squareup/securetouch/SecureTouchFeatureEvent;

    invoke-virtual {p0, p1}, Lcom/squareup/securetouch/RealSecureTouchWorkflow$render$$inlined$onReaderOutput$5;->invoke(Lcom/squareup/securetouch/SecureTouchFeatureEvent;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
