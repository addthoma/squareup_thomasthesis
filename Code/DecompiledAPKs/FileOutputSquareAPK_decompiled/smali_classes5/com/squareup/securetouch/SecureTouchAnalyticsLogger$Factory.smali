.class public final Lcom/squareup/securetouch/SecureTouchAnalyticsLogger$Factory;
.super Ljava/lang/Object;
.source "SecureTouchAnalyticsLogger.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/securetouch/SecureTouchAnalyticsLogger;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010\u0007\u001a\u00020\u00082\u0008\u0008\u0002\u0010\t\u001a\u00020\nR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/securetouch/SecureTouchAnalyticsLogger$Factory;",
        "",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "accountStatusSettings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "(Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/server/AccountStatusSettings;)V",
        "create",
        "Lcom/squareup/securetouch/SecureTouchAnalyticsLogger;",
        "sessionToken",
        "",
        "secure-touch_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final analytics:Lcom/squareup/analytics/Analytics;


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/server/AccountStatusSettings;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "analytics"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountStatusSettings"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsLogger$Factory;->analytics:Lcom/squareup/analytics/Analytics;

    iput-object p2, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsLogger$Factory;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    return-void
.end method

.method public static synthetic create$default(Lcom/squareup/securetouch/SecureTouchAnalyticsLogger$Factory;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/securetouch/SecureTouchAnalyticsLogger;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    .line 89
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "UUID.randomUUID().toString()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/securetouch/SecureTouchAnalyticsLogger$Factory;->create(Ljava/lang/String;)Lcom/squareup/securetouch/SecureTouchAnalyticsLogger;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final create(Ljava/lang/String;)Lcom/squareup/securetouch/SecureTouchAnalyticsLogger;
    .locals 4

    const-string v0, "sessionToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 89
    new-instance v0, Lcom/squareup/securetouch/SecureTouchAnalyticsLogger;

    .line 90
    iget-object v1, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsLogger$Factory;->analytics:Lcom/squareup/analytics/Analytics;

    .line 91
    iget-object v2, p0, Lcom/squareup/securetouch/SecureTouchAnalyticsLogger$Factory;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v2}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v2

    const-string v3, "accountStatusSettings.userSettings"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/squareup/settings/server/UserSettings;->getLocale()Lcom/squareup/server/account/protos/User$SquareLocale;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, v2, Lcom/squareup/server/account/protos/User$SquareLocale;->country_code:Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_0
    const-string v2, "<unknown>"

    :goto_0
    const/4 v3, 0x2

    .line 89
    invoke-direct {v0, v1, v2, p1, v3}, Lcom/squareup/securetouch/SecureTouchAnalyticsLogger;-><init>(Lcom/squareup/analytics/Analytics;Ljava/lang/String;Ljava/lang/String;I)V

    return-object v0
.end method
