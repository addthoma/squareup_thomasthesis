.class public final Lcom/squareup/securetouch/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/securetouch/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final SecureTouchKeyView:[I

.field public static final SecureTouchKeyView_digitText:I = 0x0

.field public static final SecureTouchKeyView_subText:I = 0x1


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 192
    fill-array-data v0, :array_0

    sput-object v0, Lcom/squareup/securetouch/R$styleable;->SecureTouchKeyView:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f040128
        0x7f0403fc
    .end array-data
.end method

.method private constructor <init>()V
    .locals 0

    .line 190
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
