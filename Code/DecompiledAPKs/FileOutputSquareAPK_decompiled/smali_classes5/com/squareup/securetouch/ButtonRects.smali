.class final Lcom/squareup/securetouch/ButtonRects;
.super Ljava/lang/Object;
.source "SecureTouchCoordinator.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000c\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0082\u0008\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000c\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\r\u001a\u00020\u0003H\u00c6\u0003J\'\u0010\u000e\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u0003H\u00c6\u0001J\u0013\u0010\u000f\u001a\u00020\u00102\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0012\u001a\u00020\u0013H\u00d6\u0001J\t\u0010\u0014\u001a\u00020\u0015H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\u0008R\u0011\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u0008\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/securetouch/ButtonRects;",
        "",
        "cancelButtonRect",
        "Lcom/squareup/securetouch/SecureTouchRect;",
        "accessibilityButtonRect",
        "doneButtonRect",
        "(Lcom/squareup/securetouch/SecureTouchRect;Lcom/squareup/securetouch/SecureTouchRect;Lcom/squareup/securetouch/SecureTouchRect;)V",
        "getAccessibilityButtonRect",
        "()Lcom/squareup/securetouch/SecureTouchRect;",
        "getCancelButtonRect",
        "getDoneButtonRect",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "secure-touch_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final accessibilityButtonRect:Lcom/squareup/securetouch/SecureTouchRect;

.field private final cancelButtonRect:Lcom/squareup/securetouch/SecureTouchRect;

.field private final doneButtonRect:Lcom/squareup/securetouch/SecureTouchRect;


# direct methods
.method public constructor <init>(Lcom/squareup/securetouch/SecureTouchRect;Lcom/squareup/securetouch/SecureTouchRect;Lcom/squareup/securetouch/SecureTouchRect;)V
    .locals 1

    const-string v0, "cancelButtonRect"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accessibilityButtonRect"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "doneButtonRect"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 246
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/securetouch/ButtonRects;->cancelButtonRect:Lcom/squareup/securetouch/SecureTouchRect;

    iput-object p2, p0, Lcom/squareup/securetouch/ButtonRects;->accessibilityButtonRect:Lcom/squareup/securetouch/SecureTouchRect;

    iput-object p3, p0, Lcom/squareup/securetouch/ButtonRects;->doneButtonRect:Lcom/squareup/securetouch/SecureTouchRect;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/securetouch/ButtonRects;Lcom/squareup/securetouch/SecureTouchRect;Lcom/squareup/securetouch/SecureTouchRect;Lcom/squareup/securetouch/SecureTouchRect;ILjava/lang/Object;)Lcom/squareup/securetouch/ButtonRects;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/squareup/securetouch/ButtonRects;->cancelButtonRect:Lcom/squareup/securetouch/SecureTouchRect;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/squareup/securetouch/ButtonRects;->accessibilityButtonRect:Lcom/squareup/securetouch/SecureTouchRect;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/squareup/securetouch/ButtonRects;->doneButtonRect:Lcom/squareup/securetouch/SecureTouchRect;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/securetouch/ButtonRects;->copy(Lcom/squareup/securetouch/SecureTouchRect;Lcom/squareup/securetouch/SecureTouchRect;Lcom/squareup/securetouch/SecureTouchRect;)Lcom/squareup/securetouch/ButtonRects;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/securetouch/SecureTouchRect;
    .locals 1

    iget-object v0, p0, Lcom/squareup/securetouch/ButtonRects;->cancelButtonRect:Lcom/squareup/securetouch/SecureTouchRect;

    return-object v0
.end method

.method public final component2()Lcom/squareup/securetouch/SecureTouchRect;
    .locals 1

    iget-object v0, p0, Lcom/squareup/securetouch/ButtonRects;->accessibilityButtonRect:Lcom/squareup/securetouch/SecureTouchRect;

    return-object v0
.end method

.method public final component3()Lcom/squareup/securetouch/SecureTouchRect;
    .locals 1

    iget-object v0, p0, Lcom/squareup/securetouch/ButtonRects;->doneButtonRect:Lcom/squareup/securetouch/SecureTouchRect;

    return-object v0
.end method

.method public final copy(Lcom/squareup/securetouch/SecureTouchRect;Lcom/squareup/securetouch/SecureTouchRect;Lcom/squareup/securetouch/SecureTouchRect;)Lcom/squareup/securetouch/ButtonRects;
    .locals 1

    const-string v0, "cancelButtonRect"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accessibilityButtonRect"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "doneButtonRect"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/securetouch/ButtonRects;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/securetouch/ButtonRects;-><init>(Lcom/squareup/securetouch/SecureTouchRect;Lcom/squareup/securetouch/SecureTouchRect;Lcom/squareup/securetouch/SecureTouchRect;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/securetouch/ButtonRects;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/securetouch/ButtonRects;

    iget-object v0, p0, Lcom/squareup/securetouch/ButtonRects;->cancelButtonRect:Lcom/squareup/securetouch/SecureTouchRect;

    iget-object v1, p1, Lcom/squareup/securetouch/ButtonRects;->cancelButtonRect:Lcom/squareup/securetouch/SecureTouchRect;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/securetouch/ButtonRects;->accessibilityButtonRect:Lcom/squareup/securetouch/SecureTouchRect;

    iget-object v1, p1, Lcom/squareup/securetouch/ButtonRects;->accessibilityButtonRect:Lcom/squareup/securetouch/SecureTouchRect;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/securetouch/ButtonRects;->doneButtonRect:Lcom/squareup/securetouch/SecureTouchRect;

    iget-object p1, p1, Lcom/squareup/securetouch/ButtonRects;->doneButtonRect:Lcom/squareup/securetouch/SecureTouchRect;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAccessibilityButtonRect()Lcom/squareup/securetouch/SecureTouchRect;
    .locals 1

    .line 248
    iget-object v0, p0, Lcom/squareup/securetouch/ButtonRects;->accessibilityButtonRect:Lcom/squareup/securetouch/SecureTouchRect;

    return-object v0
.end method

.method public final getCancelButtonRect()Lcom/squareup/securetouch/SecureTouchRect;
    .locals 1

    .line 247
    iget-object v0, p0, Lcom/squareup/securetouch/ButtonRects;->cancelButtonRect:Lcom/squareup/securetouch/SecureTouchRect;

    return-object v0
.end method

.method public final getDoneButtonRect()Lcom/squareup/securetouch/SecureTouchRect;
    .locals 1

    .line 249
    iget-object v0, p0, Lcom/squareup/securetouch/ButtonRects;->doneButtonRect:Lcom/squareup/securetouch/SecureTouchRect;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/securetouch/ButtonRects;->cancelButtonRect:Lcom/squareup/securetouch/SecureTouchRect;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/securetouch/ButtonRects;->accessibilityButtonRect:Lcom/squareup/securetouch/SecureTouchRect;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/securetouch/ButtonRects;->doneButtonRect:Lcom/squareup/securetouch/SecureTouchRect;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ButtonRects(cancelButtonRect="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/securetouch/ButtonRects;->cancelButtonRect:Lcom/squareup/securetouch/SecureTouchRect;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", accessibilityButtonRect="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/securetouch/ButtonRects;->accessibilityButtonRect:Lcom/squareup/securetouch/SecureTouchRect;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", doneButtonRect="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/securetouch/ButtonRects;->doneButtonRect:Lcom/squareup/securetouch/SecureTouchRect;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
