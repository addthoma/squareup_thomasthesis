.class final Lcom/squareup/securetouch/SecureTouchWorkflowLauncher$launch$1;
.super Ljava/lang/Object;
.source "SecureTouchWorkflowLauncher.kt"

# interfaces
.implements Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/securetouch/SecureTouchWorkflowLauncher;->launch(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/securetouch/SecureTouchPinRequestData;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0004\u0018\u00010\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/container/Command;",
        "history",
        "Lflow/History;",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $parentScope:Lcom/squareup/ui/main/RegisterTreeKey;

.field final synthetic $secureTouchPinRequestData:Lcom/squareup/securetouch/SecureTouchPinRequestData;

.field final synthetic this$0:Lcom/squareup/securetouch/SecureTouchWorkflowLauncher;


# direct methods
.method constructor <init>(Lcom/squareup/securetouch/SecureTouchWorkflowLauncher;Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/securetouch/SecureTouchPinRequestData;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/securetouch/SecureTouchWorkflowLauncher$launch$1;->this$0:Lcom/squareup/securetouch/SecureTouchWorkflowLauncher;

    iput-object p2, p0, Lcom/squareup/securetouch/SecureTouchWorkflowLauncher$launch$1;->$parentScope:Lcom/squareup/ui/main/RegisterTreeKey;

    iput-object p3, p0, Lcom/squareup/securetouch/SecureTouchWorkflowLauncher$launch$1;->$secureTouchPinRequestData:Lcom/squareup/securetouch/SecureTouchPinRequestData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lflow/History;)Lcom/squareup/container/Command;
    .locals 5

    const-string v0, "history"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-virtual {p1}, Lflow/History;->top()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    .line 26
    const-class v1, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner$SecureTouchWorkflowScope;

    invoke-virtual {v0, v1}, Lcom/squareup/container/ContainerTreeKey;->isInScopeOf(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 29
    sget-object v0, Lflow/Direction;->REPLACE:Lflow/Direction;

    invoke-static {p1, v0}, Lcom/squareup/container/Command;->setHistory(Lflow/History;Lflow/Direction;)Lcom/squareup/container/Command;

    move-result-object p1

    goto :goto_0

    .line 32
    :cond_0
    new-instance v0, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner$SecureTouchBootstrapScreen;

    .line 33
    iget-object v1, p0, Lcom/squareup/securetouch/SecureTouchWorkflowLauncher$launch$1;->$parentScope:Lcom/squareup/ui/main/RegisterTreeKey;

    .line 34
    new-instance v2, Lcom/squareup/securetouch/SecureTouchInput;

    .line 35
    iget-object v3, p0, Lcom/squareup/securetouch/SecureTouchWorkflowLauncher$launch$1;->$secureTouchPinRequestData:Lcom/squareup/securetouch/SecureTouchPinRequestData;

    invoke-virtual {v3}, Lcom/squareup/securetouch/SecureTouchPinRequestData;->getPinEntryState()Lcom/squareup/securetouch/SecureTouchPinEntryState;

    move-result-object v3

    .line 36
    iget-object v4, p0, Lcom/squareup/securetouch/SecureTouchWorkflowLauncher$launch$1;->this$0:Lcom/squareup/securetouch/SecureTouchWorkflowLauncher;

    invoke-static {v4}, Lcom/squareup/securetouch/SecureTouchWorkflowLauncher;->access$getCurrentSecureTouchMode$p(Lcom/squareup/securetouch/SecureTouchWorkflowLauncher;)Lcom/squareup/securetouch/CurrentSecureTouchMode;

    move-result-object v4

    invoke-virtual {v4}, Lcom/squareup/securetouch/CurrentSecureTouchMode;->getSecureTouchMode()Lcom/squareup/securetouch/SecureTouchMode;

    move-result-object v4

    .line 34
    invoke-direct {v2, v3, v4}, Lcom/squareup/securetouch/SecureTouchInput;-><init>(Lcom/squareup/securetouch/SecureTouchPinEntryState;Lcom/squareup/securetouch/SecureTouchMode;)V

    .line 32
    invoke-direct {v0, v1, v2}, Lcom/squareup/securetouch/RealSecureTouchWorkflowRunner$SecureTouchBootstrapScreen;-><init>(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/securetouch/SecureTouchInput;)V

    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    .line 31
    invoke-static {p1, v0}, Lcom/squareup/container/Command;->set(Lflow/History;Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/Command;

    move-result-object p1

    :goto_0
    return-object p1
.end method
