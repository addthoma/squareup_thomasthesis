.class public Lcom/epson/eposprint/EposException;
.super Ljava/lang/Exception;
.source "EposException.java"


# static fields
.field public static final ERR_CONNECT:I = 0x3

.field public static final ERR_FAILURE:I = 0xff

.field public static final ERR_ILLEGAL:I = 0x6

.field public static final ERR_MEMORY:I = 0x5

.field public static final ERR_OFF_LINE:I = 0x9

.field public static final ERR_OPEN:I = 0x2

.field public static final ERR_PARAM:I = 0x1

.field public static final ERR_PROCESSING:I = 0x7

.field public static final ERR_TIMEOUT:I = 0x4

.field public static final ERR_UNSUPPORTED:I = 0x8

.field public static final SUCCESS:I = 0x0

.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private mBatteryStatus:I

.field private mErrorStatus:I

.field private mPrinterStatus:I


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .line 27
    invoke-direct {p0}, Ljava/lang/Exception;-><init>()V

    const/16 v0, 0xff

    .line 19
    iput v0, p0, Lcom/epson/eposprint/EposException;->mErrorStatus:I

    const/4 v0, 0x0

    .line 20
    iput v0, p0, Lcom/epson/eposprint/EposException;->mPrinterStatus:I

    .line 21
    iput v0, p0, Lcom/epson/eposprint/EposException;->mBatteryStatus:I

    .line 28
    iput p1, p0, Lcom/epson/eposprint/EposException;->mErrorStatus:I

    .line 29
    iput v0, p0, Lcom/epson/eposprint/EposException;->mPrinterStatus:I

    .line 30
    iput v0, p0, Lcom/epson/eposprint/EposException;->mBatteryStatus:I

    return-void
.end method

.method public constructor <init>(II)V
    .locals 1

    .line 33
    invoke-direct {p0}, Ljava/lang/Exception;-><init>()V

    const/16 v0, 0xff

    .line 19
    iput v0, p0, Lcom/epson/eposprint/EposException;->mErrorStatus:I

    const/4 v0, 0x0

    .line 20
    iput v0, p0, Lcom/epson/eposprint/EposException;->mPrinterStatus:I

    .line 21
    iput v0, p0, Lcom/epson/eposprint/EposException;->mBatteryStatus:I

    .line 34
    iput p1, p0, Lcom/epson/eposprint/EposException;->mErrorStatus:I

    .line 35
    iput p2, p0, Lcom/epson/eposprint/EposException;->mPrinterStatus:I

    .line 36
    iput v0, p0, Lcom/epson/eposprint/EposException;->mBatteryStatus:I

    return-void
.end method

.method public constructor <init>(III)V
    .locals 1

    .line 39
    invoke-direct {p0}, Ljava/lang/Exception;-><init>()V

    const/16 v0, 0xff

    .line 19
    iput v0, p0, Lcom/epson/eposprint/EposException;->mErrorStatus:I

    const/4 v0, 0x0

    .line 20
    iput v0, p0, Lcom/epson/eposprint/EposException;->mPrinterStatus:I

    .line 21
    iput v0, p0, Lcom/epson/eposprint/EposException;->mBatteryStatus:I

    .line 40
    iput p1, p0, Lcom/epson/eposprint/EposException;->mErrorStatus:I

    .line 41
    iput p2, p0, Lcom/epson/eposprint/EposException;->mPrinterStatus:I

    .line 42
    iput p3, p0, Lcom/epson/eposprint/EposException;->mBatteryStatus:I

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0

    .line 24
    invoke-direct {p0, p1, p2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    const/16 p1, 0xff

    .line 19
    iput p1, p0, Lcom/epson/eposprint/EposException;->mErrorStatus:I

    const/4 p1, 0x0

    .line 20
    iput p1, p0, Lcom/epson/eposprint/EposException;->mPrinterStatus:I

    .line 21
    iput p1, p0, Lcom/epson/eposprint/EposException;->mBatteryStatus:I

    return-void
.end method


# virtual methods
.method public getBatteryStatus()I
    .locals 1

    .line 54
    iget v0, p0, Lcom/epson/eposprint/EposException;->mBatteryStatus:I

    return v0
.end method

.method public getErrorStatus()I
    .locals 1

    .line 46
    iget v0, p0, Lcom/epson/eposprint/EposException;->mErrorStatus:I

    return v0
.end method

.method public getPrinterStatus()I
    .locals 1

    .line 50
    iget v0, p0, Lcom/epson/eposprint/EposException;->mPrinterStatus:I

    return v0
.end method
