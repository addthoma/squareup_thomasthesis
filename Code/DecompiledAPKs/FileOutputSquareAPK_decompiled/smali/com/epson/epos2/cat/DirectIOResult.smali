.class public Lcom/epson/epos2/cat/DirectIOResult;
.super Ljava/lang/Object;
.source "DirectIOResult.java"


# instance fields
.field private accountNumber:Ljava/lang/String;

.field private additionalSecurityInformation:Ljava/lang/String;

.field private balance:I

.field private paymentCondition:I

.field private settledAmount:I

.field private slipNumber:Ljava/lang/String;

.field private transactionNumber:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private setAccountNumber(Ljava/lang/String;)V
    .locals 0

    .line 43
    iput-object p1, p0, Lcom/epson/epos2/cat/DirectIOResult;->accountNumber:Ljava/lang/String;

    return-void
.end method

.method private setAdditionalSecurityInformation(Ljava/lang/String;)V
    .locals 0

    .line 86
    iput-object p1, p0, Lcom/epson/epos2/cat/DirectIOResult;->additionalSecurityInformation:Ljava/lang/String;

    return-void
.end method

.method private setBalance(I)V
    .locals 0

    .line 78
    iput p1, p0, Lcom/epson/epos2/cat/DirectIOResult;->balance:I

    return-void
.end method

.method private setPaymentCondition(I)V
    .locals 0

    .line 71
    iput p1, p0, Lcom/epson/epos2/cat/DirectIOResult;->paymentCondition:I

    return-void
.end method

.method private setSettledAmount(I)V
    .locals 0

    .line 50
    iput p1, p0, Lcom/epson/epos2/cat/DirectIOResult;->settledAmount:I

    return-void
.end method

.method private setSlipNumber(Ljava/lang/String;)V
    .locals 0

    .line 57
    iput-object p1, p0, Lcom/epson/epos2/cat/DirectIOResult;->slipNumber:Ljava/lang/String;

    return-void
.end method

.method private setTransactionNumber(Ljava/lang/String;)V
    .locals 0

    .line 64
    iput-object p1, p0, Lcom/epson/epos2/cat/DirectIOResult;->transactionNumber:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getAccountNumber()Ljava/lang/String;
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/epson/epos2/cat/DirectIOResult;->accountNumber:Ljava/lang/String;

    return-object v0
.end method

.method public getAdditionalSecurityInformation()Ljava/lang/String;
    .locals 1

    .line 82
    iget-object v0, p0, Lcom/epson/epos2/cat/DirectIOResult;->additionalSecurityInformation:Ljava/lang/String;

    return-object v0
.end method

.method public getBalance()I
    .locals 1

    .line 75
    iget v0, p0, Lcom/epson/epos2/cat/DirectIOResult;->balance:I

    return v0
.end method

.method public getPaymentCondition()I
    .locals 1

    .line 68
    iget v0, p0, Lcom/epson/epos2/cat/DirectIOResult;->paymentCondition:I

    return v0
.end method

.method public getSettledAmount()I
    .locals 1

    .line 47
    iget v0, p0, Lcom/epson/epos2/cat/DirectIOResult;->settledAmount:I

    return v0
.end method

.method public getSlipNumber()Ljava/lang/String;
    .locals 1

    .line 54
    iget-object v0, p0, Lcom/epson/epos2/cat/DirectIOResult;->slipNumber:Ljava/lang/String;

    return-object v0
.end method

.method public getTransactionNumber()Ljava/lang/String;
    .locals 1

    .line 61
    iget-object v0, p0, Lcom/epson/epos2/cat/DirectIOResult;->transactionNumber:Ljava/lang/String;

    return-object v0
.end method
