.class public Lcom/epson/epos2/barcodescanner/BarcodeScanner;
.super Ljava/lang/Object;
.source "BarcodeScanner.java"


# static fields
.field public static final EVENT_DISCONNECT:I = 0x2

.field public static final EVENT_RECONNECT:I = 0x1

.field public static final EVENT_RECONNECTING:I = 0x0

.field public static final FALSE:I = 0x0

.field private static final NO_EXCEPTION:I = 0x0

.field public static final PARAM_DEFAULT:I = -0x2

.field private static final RETURN_NULL:I = 0x101

.field private static final RETURN_NULL_CHARACTER:I = 0x100

.field public static final TRUE:I = 0x1

.field private static connection:I


# instance fields
.field private mClassOutputLog:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation
.end field

.field private mConnectionListener:Lcom/epson/epos2/ConnectionListener;

.field private mContext:Landroid/content/Context;

.field private mOutputExceptionMethod:Ljava/lang/reflect/Method;

.field private mOutputLogCallFunctionMethod:Ljava/lang/reflect/Method;

.field private mOutputLogEventMethod:Ljava/lang/reflect/Method;

.field private mOutputLogReturnFunctionMethod:Ljava/lang/reflect/Method;

.field private mReadLogSettingsMethod:Ljava/lang/reflect/Method;

.field private mScanListener:Lcom/epson/epos2/barcodescanner/ScanListener;

.field private mScannerHandle:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    :try_start_0
    const-string v0, "epos2"

    .line 14
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const/4 v0, 0x0

    .line 55
    sput v0, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->connection:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    .line 21
    iput-wide v0, p0, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->mScannerHandle:J

    const/4 v0, 0x0

    .line 22
    iput-object v0, p0, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->mContext:Landroid/content/Context;

    .line 23
    iput-object v0, p0, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->mScanListener:Lcom/epson/epos2/barcodescanner/ScanListener;

    .line 24
    iput-object v0, p0, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->mConnectionListener:Lcom/epson/epos2/ConnectionListener;

    .line 26
    iput-object v0, p0, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->mClassOutputLog:Ljava/lang/Class;

    .line 27
    iput-object v0, p0, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->mOutputLogCallFunctionMethod:Ljava/lang/reflect/Method;

    .line 28
    iput-object v0, p0, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->mOutputLogReturnFunctionMethod:Ljava/lang/reflect/Method;

    .line 29
    iput-object v0, p0, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->mOutputExceptionMethod:Ljava/lang/reflect/Method;

    .line 30
    iput-object v0, p0, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->mOutputLogEventMethod:Ljava/lang/reflect/Method;

    .line 31
    iput-object v0, p0, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->mReadLogSettingsMethod:Ljava/lang/reflect/Method;

    .line 71
    invoke-direct {p0, p1}, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->initializeOuputLogFunctions(Landroid/content/Context;)V

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const-string v3, "BarcodeScanner"

    .line 72
    invoke-direct {p0, v3, v1}, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    :try_start_0
    const-string v1, "com.epson.epos2.NativeInitializer"

    .line 75
    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const-string v4, "initializeNativeEnv"

    new-array v5, v0, [Ljava/lang/Class;

    .line 76
    const-class v6, Landroid/content/Context;

    aput-object v6, v5, v2

    invoke-virtual {v1, v4, v5}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    .line 77
    invoke-virtual {v4, v0}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    new-array v5, v0, [Ljava/lang/Object;

    aput-object p1, v5, v2

    .line 78
    invoke-virtual {v4, v1, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 81
    invoke-direct {p0, v3, v1}, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 82
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 85
    :goto_0
    iput-object p1, p0, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->mContext:Landroid/content/Context;

    .line 87
    invoke-virtual {p0}, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->initializeScannerInstance()V

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    .line 89
    invoke-direct {p0, v3, v2, v0}, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void
.end method

.method private initializeOuputLogFunctions(Landroid/content/Context;)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    :try_start_0
    const-string v0, "com.epson.epos2.OutputLog"

    .line 447
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->mClassOutputLog:Ljava/lang/Class;

    .line 449
    iget-object v0, p0, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->mClassOutputLog:Ljava/lang/Class;

    const-string v1, "outputLogCallFunction"

    const/4 v2, 0x3

    new-array v3, v2, [Ljava/lang/Class;

    const-class v4, Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    sget-object v4, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    const/4 v6, 0x1

    aput-object v4, v3, v6

    const-class v4, [Ljava/lang/Object;

    const/4 v7, 0x2

    aput-object v4, v3, v7

    invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->mOutputLogCallFunctionMethod:Ljava/lang/reflect/Method;

    .line 450
    iget-object v0, p0, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->mOutputLogCallFunctionMethod:Ljava/lang/reflect/Method;

    invoke-virtual {v0, v6}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 451
    iget-object v0, p0, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->mClassOutputLog:Ljava/lang/Class;

    const-string v1, "outputLogReturnFunction"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Class;

    const-class v4, Ljava/lang/String;

    aput-object v4, v3, v5

    sget-object v4, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v4, v3, v6

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v4, v3, v7

    const-class v4, [Ljava/lang/Object;

    aput-object v4, v3, v2

    invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->mOutputLogReturnFunctionMethod:Ljava/lang/reflect/Method;

    .line 452
    iget-object v0, p0, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->mOutputLogReturnFunctionMethod:Ljava/lang/reflect/Method;

    invoke-virtual {v0, v6}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 453
    iget-object v0, p0, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->mClassOutputLog:Ljava/lang/Class;

    const-string v1, "outputException"

    new-array v3, v2, [Ljava/lang/Class;

    const-class v4, Ljava/lang/String;

    aput-object v4, v3, v5

    sget-object v4, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v4, v3, v6

    const-class v4, Ljava/lang/Exception;

    aput-object v4, v3, v7

    invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->mOutputExceptionMethod:Ljava/lang/reflect/Method;

    .line 454
    iget-object v0, p0, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->mOutputExceptionMethod:Ljava/lang/reflect/Method;

    invoke-virtual {v0, v6}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 455
    iget-object v0, p0, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->mClassOutputLog:Ljava/lang/Class;

    const-string v1, "outputLogEvent"

    new-array v2, v2, [Ljava/lang/Class;

    const-class v3, Ljava/lang/String;

    aput-object v3, v2, v5

    sget-object v3, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v3, v2, v6

    const-class v3, [Ljava/lang/Object;

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->mOutputLogEventMethod:Ljava/lang/reflect/Method;

    .line 456
    iget-object v0, p0, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->mOutputLogEventMethod:Ljava/lang/reflect/Method;

    invoke-virtual {v0, v6}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 457
    iget-object v0, p0, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->mClassOutputLog:Ljava/lang/Class;

    const-string v1, "readLogSettings"

    new-array v2, v6, [Ljava/lang/Class;

    const-class v3, Landroid/content/Context;

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->mReadLogSettingsMethod:Ljava/lang/reflect/Method;

    .line 458
    iget-object v0, p0, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->mReadLogSettingsMethod:Ljava/lang/reflect/Method;

    invoke-virtual {v0, v6}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 460
    iget-object v0, p0, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->mReadLogSettingsMethod:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->mClassOutputLog:Ljava/lang/Class;

    new-array v2, v6, [Ljava/lang/Object;

    aput-object p1, v2, v5

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 463
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    :goto_0
    return-void
.end method

.method private native nativeEpos2Connect(JLjava/lang/String;JLjava/lang/Object;)I
.end method

.method private native nativeEpos2CreateHandle([J)I
.end method

.method private native nativeEpos2DestroyHandle(J)I
.end method

.method private native nativeEpos2Disconnect(J)I
.end method

.method private onConnection(I)V
    .locals 6

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/Object;

    .line 428
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x1

    aput-object p0, v1, v2

    const-string v4, "onConnection"

    invoke-direct {p0, v4, v1}, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 430
    iget-object v1, p0, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->mConnectionListener:Lcom/epson/epos2/ConnectionListener;

    if-eqz v1, :cond_0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v5, "eventType->"

    aput-object v5, v1, v3

    .line 434
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v2

    .line 433
    invoke-direct {p0, v4, v1}, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 436
    iget-object v1, p0, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->mConnectionListener:Lcom/epson/epos2/ConnectionListener;

    invoke-interface {v1, p0, p1}, Lcom/epson/epos2/ConnectionListener;->onConnection(Ljava/lang/Object;I)V

    :cond_0
    new-array v0, v0, [Ljava/lang/Object;

    .line 440
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    aput-object p0, v0, v2

    invoke-direct {p0, v4, v3, v0}, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void
.end method

.method private outputException(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 5

    .line 487
    :try_start_0
    iget-object v0, p0, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->mOutputExceptionMethod:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->mClassOutputLog:Ljava/lang/Class;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 p1, 0x1

    iget-wide v3, p0, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->mScannerHandle:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, p1

    const/4 p1, 0x2

    aput-object p2, v2, p1

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method private varargs outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 5

    .line 469
    :try_start_0
    iget-object v0, p0, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->mOutputLogCallFunctionMethod:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->mClassOutputLog:Ljava/lang/Class;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 p1, 0x1

    iget-wide v3, p0, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->mScannerHandle:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, p1

    const/4 p1, 0x2

    aput-object p2, v2, p1

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method private varargs outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 5

    .line 496
    :try_start_0
    iget-object v0, p0, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->mOutputLogEventMethod:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->mClassOutputLog:Ljava/lang/Class;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 p1, 0x1

    iget-wide v3, p0, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->mScannerHandle:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, p1

    const/4 p1, 0x2

    aput-object p2, v2, p1

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method private varargs outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V
    .locals 5

    .line 478
    :try_start_0
    iget-object v0, p0, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->mOutputLogReturnFunctionMethod:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->mClassOutputLog:Ljava/lang/Class;

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 p1, 0x1

    iget-wide v3, p0, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->mScannerHandle:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, p1

    const/4 p1, 0x2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, v2, p1

    const/4 p1, 0x3

    aput-object p3, v2, p1

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method


# virtual methods
.method protected checkHandle()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 238
    iget-wide v0, p0, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->mScannerHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-eqz v4, :cond_0

    return-void

    .line 239
    :cond_0
    new-instance v0, Lcom/epson/epos2/Epos2Exception;

    const/16 v1, 0xff

    invoke-direct {v0, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v0
.end method

.method public connect(Ljava/lang/String;I)V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    .line 176
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v1, v4

    const-string v3, "connect"

    invoke-direct {p0, v3, v1}, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    if-eqz p1, :cond_1

    .line 182
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->checkHandle()V

    .line 184
    iget-wide v6, p0, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->mScannerHandle:J

    int-to-long v9, p2

    iget-object v11, p0, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->mContext:Landroid/content/Context;

    move-object v5, p0

    move-object v8, p1

    invoke-direct/range {v5 .. v11}, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->nativeEpos2Connect(JLjava/lang/String;JLjava/lang/Object;)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    .line 195
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v4

    invoke-direct {p0, v3, v2, v0}, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 186
    :cond_0
    :try_start_1
    new-instance v5, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v5, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v5

    :catch_0
    move-exception v1

    goto :goto_0

    .line 180
    :cond_1
    new-instance v1, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v1, v4}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v1
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 190
    :goto_0
    invoke-direct {p0, v3, v1}, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 191
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v5

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v4

    invoke-direct {p0, v3, v5, v0}, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 192
    throw v1
.end method

.method public disconnect()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "disconnect"

    .line 209
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 212
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->checkHandle()V

    .line 214
    iget-wide v3, p0, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->mScannerHandle:J

    invoke-direct {p0, v3, v4}, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->nativeEpos2Disconnect(J)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v1, v0, [Ljava/lang/Object;

    .line 225
    invoke-direct {p0, v2, v0, v1}, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 216
    :cond_0
    :try_start_1
    new-instance v3, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v3, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v3
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 220
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 221
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v3

    new-array v0, v0, [Ljava/lang/Object;

    invoke-direct {p0, v2, v3, v0}, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 222
    throw v1
.end method

.method protected finalize()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "finalize"

    .line 118
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v1, 0x0

    .line 120
    iput-object v1, p0, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->mScanListener:Lcom/epson/epos2/barcodescanner/ScanListener;

    .line 121
    iput-object v1, p0, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->mConnectionListener:Lcom/epson/epos2/ConnectionListener;

    .line 124
    :try_start_0
    iget-wide v3, p0, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->mScannerHandle:J

    const-wide/16 v5, 0x0

    cmp-long v1, v3, v5

    if-eqz v1, :cond_0

    .line 126
    iget-wide v3, p0, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->mScannerHandle:J

    invoke-direct {p0, v3, v4}, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->nativeEpos2Disconnect(J)I

    .line 127
    iget-wide v3, p0, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->mScannerHandle:J

    invoke-direct {p0, v3, v4}, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->nativeEpos2DestroyHandle(J)I

    .line 128
    iput-wide v5, p0, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->mScannerHandle:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 132
    :cond_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    new-array v1, v0, [Ljava/lang/Object;

    .line 135
    invoke-direct {p0, v2, v0, v1}, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    :catchall_0
    move-exception v0

    .line 132
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 133
    throw v0
.end method

.method public getAdmin()Ljava/lang/String;
    .locals 8

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "getAdmin"

    .line 361
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 363
    iget-wide v3, p0, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->mScannerHandle:J

    const/16 v1, 0x100

    const-wide/16 v5, 0x0

    cmp-long v7, v3, v5

    if-nez v7, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 364
    invoke-direct {p0, v2, v1, v0}, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    const-string v0, ""

    return-object v0

    .line 368
    :cond_0
    invoke-virtual {p0, v3, v4}, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->nativeEpos2GetAdmin(J)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v3, v1, v0

    .line 370
    invoke-direct {p0, v2, v0, v1}, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    new-array v0, v0, [Ljava/lang/Object;

    .line 373
    invoke-direct {p0, v2, v1, v0}, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    :goto_0
    return-object v3
.end method

.method public getLocation()Ljava/lang/String;
    .locals 8

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "getLocation"

    .line 388
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 390
    iget-wide v3, p0, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->mScannerHandle:J

    const/16 v1, 0x100

    const-wide/16 v5, 0x0

    cmp-long v7, v3, v5

    if-nez v7, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 391
    invoke-direct {p0, v2, v1, v0}, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    const-string v0, ""

    return-object v0

    .line 395
    :cond_0
    invoke-virtual {p0, v3, v4}, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->nativeEpos2GetLocation(J)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v3, v1, v0

    .line 397
    invoke-direct {p0, v2, v0, v1}, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    new-array v0, v0, [Ljava/lang/Object;

    .line 400
    invoke-direct {p0, v2, v1, v0}, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    :goto_0
    return-object v3
.end method

.method public getStatus()Lcom/epson/epos2/barcodescanner/ScannerStatusInfo;
    .locals 6

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "getStatus"

    .line 312
    invoke-direct {p0, v2, v1}, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 314
    iget-wide v3, p0, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->mScannerHandle:J

    invoke-virtual {p0, v3, v4}, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->nativeEpos2GetStatus(J)Lcom/epson/epos2/barcodescanner/ScannerStatusInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 316
    invoke-virtual {v1}, Lcom/epson/epos2/barcodescanner/ScannerStatusInfo;->getConnection()I

    move-result v3

    sput v3, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->connection:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    .line 317
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "connection->"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget v5, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->connection:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-direct {p0, v2, v0, v3}, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    const/16 v3, 0x101

    new-array v0, v0, [Ljava/lang/Object;

    .line 320
    invoke-direct {p0, v2, v3, v0}, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    :goto_0
    return-object v1
.end method

.method protected initializeScannerInstance()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v0, v0, [J

    .line 102
    invoke-direct {p0, v0}, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->nativeEpos2CreateHandle([J)I

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    .line 108
    aget-wide v2, v0, v1

    iput-wide v2, p0, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->mScannerHandle:J

    const-wide/16 v2, 0x0

    aput-wide v2, v0, v1

    return-void

    .line 105
    :cond_0
    new-instance v0, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v0, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v0
.end method

.method protected native nativeEpos2GetAdmin(J)Ljava/lang/String;
.end method

.method protected native nativeEpos2GetLocation(J)Ljava/lang/String;
.end method

.method protected native nativeEpos2GetStatus(J)Lcom/epson/epos2/barcodescanner/ScannerStatusInfo;
.end method

.method protected onScanData(Ljava/lang/String;)V
    .locals 6

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v3, 0x1

    aput-object p0, v1, v3

    const-string v4, "onScanData"

    .line 336
    invoke-direct {p0, v4, v1}, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 338
    iget-object v1, p0, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->mScanListener:Lcom/epson/epos2/barcodescanner/ScanListener;

    if-eqz v1, :cond_0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v5, "scanData->"

    aput-object v5, v1, v2

    aput-object p1, v1, v3

    .line 341
    invoke-direct {p0, v4, v1}, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 344
    iget-object v1, p0, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->mScanListener:Lcom/epson/epos2/barcodescanner/ScanListener;

    invoke-interface {v1, p0, p1}, Lcom/epson/epos2/barcodescanner/ScanListener;->onScanData(Lcom/epson/epos2/barcodescanner/BarcodeScanner;Ljava/lang/String;)V

    :cond_0
    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    aput-object p0, v0, v3

    .line 348
    invoke-direct {p0, v4, v2, v0}, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void
.end method

.method public setConnectionEventListener(Lcom/epson/epos2/ConnectionListener;)V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "setConnectionEventListener"

    .line 413
    invoke-direct {p0, v1, v0}, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 415
    iget-wide v0, p0, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->mScannerHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-eqz v4, :cond_1

    if-eqz p1, :cond_0

    .line 417
    iput-object p1, p0, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->mConnectionListener:Lcom/epson/epos2/ConnectionListener;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 420
    iput-object p1, p0, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->mConnectionListener:Lcom/epson/epos2/ConnectionListener;

    :cond_1
    :goto_0
    return-void
.end method

.method public setScanEventListener(Lcom/epson/epos2/barcodescanner/ScanListener;)V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "setScanEventListener"

    .line 274
    invoke-direct {p0, v1, v0}, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 276
    iget-wide v0, p0, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->mScannerHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-eqz v4, :cond_1

    if-eqz p1, :cond_0

    .line 278
    iput-object p1, p0, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->mScanListener:Lcom/epson/epos2/barcodescanner/ScanListener;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 281
    iput-object p1, p0, Lcom/epson/epos2/barcodescanner/BarcodeScanner;->mScanListener:Lcom/epson/epos2/barcodescanner/ScanListener;

    :cond_1
    :goto_0
    return-void
.end method
