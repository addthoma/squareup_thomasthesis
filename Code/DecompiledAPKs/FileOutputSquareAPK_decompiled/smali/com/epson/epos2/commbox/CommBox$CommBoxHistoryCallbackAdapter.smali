.class public Lcom/epson/epos2/commbox/CommBox$CommBoxHistoryCallbackAdapter;
.super Ljava/lang/Object;
.source "CommBox.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/epos2/commbox/CommBox;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "CommBoxHistoryCallbackAdapter"
.end annotation


# instance fields
.field private mAdapterCommBoxHandle:J

.field private mCallback:Lcom/epson/epos2/commbox/GetCommHistoryCallback;

.field private mCommBoxObj:Lcom/epson/epos2/commbox/CommBox;

.field private mHistoryList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/epson/epos2/commbox/CommBox;


# direct methods
.method public constructor <init>(Lcom/epson/epos2/commbox/CommBox;JLcom/epson/epos2/commbox/GetCommHistoryCallback;Lcom/epson/epos2/commbox/CommBox;)V
    .locals 2

    .line 614
    iput-object p1, p0, Lcom/epson/epos2/commbox/CommBox$CommBoxHistoryCallbackAdapter;->this$0:Lcom/epson/epos2/commbox/CommBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    .line 605
    iput-wide v0, p0, Lcom/epson/epos2/commbox/CommBox$CommBoxHistoryCallbackAdapter;->mAdapterCommBoxHandle:J

    const/4 p1, 0x0

    .line 606
    iput-object p1, p0, Lcom/epson/epos2/commbox/CommBox$CommBoxHistoryCallbackAdapter;->mCallback:Lcom/epson/epos2/commbox/GetCommHistoryCallback;

    .line 607
    iput-object p1, p0, Lcom/epson/epos2/commbox/CommBox$CommBoxHistoryCallbackAdapter;->mCommBoxObj:Lcom/epson/epos2/commbox/CommBox;

    .line 608
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/epson/epos2/commbox/CommBox$CommBoxHistoryCallbackAdapter;->mHistoryList:Ljava/util/ArrayList;

    .line 615
    iput-wide p2, p0, Lcom/epson/epos2/commbox/CommBox$CommBoxHistoryCallbackAdapter;->mAdapterCommBoxHandle:J

    .line 616
    iput-object p4, p0, Lcom/epson/epos2/commbox/CommBox$CommBoxHistoryCallbackAdapter;->mCallback:Lcom/epson/epos2/commbox/GetCommHistoryCallback;

    .line 617
    iput-object p5, p0, Lcom/epson/epos2/commbox/CommBox$CommBoxHistoryCallbackAdapter;->mCommBoxObj:Lcom/epson/epos2/commbox/CommBox;

    return-void
.end method


# virtual methods
.method public addHistory(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 647
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "senderId"

    .line 648
    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string p1, "receiverId"

    .line 649
    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string p1, "message"

    .line 650
    invoke-virtual {v0, p1, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 652
    iget-object p1, p0, Lcom/epson/epos2/commbox/CommBox$CommBoxHistoryCallbackAdapter;->mHistoryList:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public getAdapterCommBoxHandle()J
    .locals 2

    .line 611
    iget-wide v0, p0, Lcom/epson/epos2/commbox/CommBox$CommBoxHistoryCallbackAdapter;->mAdapterCommBoxHandle:J

    return-wide v0
.end method

.method public onCommBoxHistory(JI)V
    .locals 10

    .line 623
    iget-object p1, p0, Lcom/epson/epos2/commbox/CommBox$CommBoxHistoryCallbackAdapter;->this$0:Lcom/epson/epos2/commbox/CommBox;

    const/4 p2, 0x5

    new-array v0, p2, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/epson/epos2/commbox/CommBox$CommBoxHistoryCallbackAdapter;->mHistoryList:Ljava/util/ArrayList;

    const/4 v3, 0x1

    aput-object v1, v0, v3

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v4, 0x2

    aput-object v1, v0, v4

    const-string v1, "-"

    const/4 v5, 0x3

    aput-object v1, v0, v5

    iget-object v6, p0, Lcom/epson/epos2/commbox/CommBox$CommBoxHistoryCallbackAdapter;->mCommBoxObj:Lcom/epson/epos2/commbox/CommBox;

    const/4 v7, 0x4

    aput-object v6, v0, v7

    const-string v6, "onCommBoxHistory"

    invoke-static {p1, v6, v0}, Lcom/epson/epos2/commbox/CommBox;->access$000(Lcom/epson/epos2/commbox/CommBox;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 625
    iget-object p1, p0, Lcom/epson/epos2/commbox/CommBox$CommBoxHistoryCallbackAdapter;->mCallback:Lcom/epson/epos2/commbox/GetCommHistoryCallback;

    if-eqz p1, :cond_1

    .line 628
    iget-object p1, p0, Lcom/epson/epos2/commbox/CommBox$CommBoxHistoryCallbackAdapter;->this$0:Lcom/epson/epos2/commbox/CommBox;

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const-string v8, "code->"

    aput-object v8, v0, v2

    .line 629
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v0, v3

    const-string v8, "historyList->"

    aput-object v8, v0, v4

    iget-object v8, p0, Lcom/epson/epos2/commbox/CommBox$CommBoxHistoryCallbackAdapter;->mHistoryList:Ljava/util/ArrayList;

    aput-object v8, v0, v5

    const-string v9, "count->"

    aput-object v9, v0, v7

    .line 631
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v0, p2

    .line 628
    invoke-static {p1, v6, v0}, Lcom/epson/epos2/commbox/CommBox;->access$100(Lcom/epson/epos2/commbox/CommBox;Ljava/lang/String;[Ljava/lang/Object;)V

    if-nez p3, :cond_0

    .line 634
    iget-object p1, p0, Lcom/epson/epos2/commbox/CommBox$CommBoxHistoryCallbackAdapter;->mCallback:Lcom/epson/epos2/commbox/GetCommHistoryCallback;

    iget-object v0, p0, Lcom/epson/epos2/commbox/CommBox$CommBoxHistoryCallbackAdapter;->mCommBoxObj:Lcom/epson/epos2/commbox/CommBox;

    iget-object v8, p0, Lcom/epson/epos2/commbox/CommBox$CommBoxHistoryCallbackAdapter;->mHistoryList:Ljava/util/ArrayList;

    invoke-interface {p1, v0, p3, v8}, Lcom/epson/epos2/commbox/GetCommHistoryCallback;->onGetCommHistory(Lcom/epson/epos2/commbox/CommBox;ILjava/util/ArrayList;)V

    goto :goto_0

    .line 637
    :cond_0
    iget-object p1, p0, Lcom/epson/epos2/commbox/CommBox$CommBoxHistoryCallbackAdapter;->mCallback:Lcom/epson/epos2/commbox/GetCommHistoryCallback;

    iget-object v0, p0, Lcom/epson/epos2/commbox/CommBox$CommBoxHistoryCallbackAdapter;->mCommBoxObj:Lcom/epson/epos2/commbox/CommBox;

    const/4 v8, 0x0

    invoke-interface {p1, v0, p3, v8}, Lcom/epson/epos2/commbox/GetCommHistoryCallback;->onGetCommHistory(Lcom/epson/epos2/commbox/CommBox;ILjava/util/ArrayList;)V

    .line 642
    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/epson/epos2/commbox/CommBox$CommBoxHistoryCallbackAdapter;->this$0:Lcom/epson/epos2/commbox/CommBox;

    new-array p2, p2, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    aput-object p3, p2, v2

    iget-object p3, p0, Lcom/epson/epos2/commbox/CommBox$CommBoxHistoryCallbackAdapter;->mHistoryList:Ljava/util/ArrayList;

    aput-object p3, p2, v3

    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result p3

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    aput-object p3, p2, v4

    aput-object v1, p2, v5

    iget-object p3, p0, Lcom/epson/epos2/commbox/CommBox$CommBoxHistoryCallbackAdapter;->mCommBoxObj:Lcom/epson/epos2/commbox/CommBox;

    aput-object p3, p2, v7

    invoke-static {p1, v6, v2, p2}, Lcom/epson/epos2/commbox/CommBox;->access$200(Lcom/epson/epos2/commbox/CommBox;Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void
.end method
