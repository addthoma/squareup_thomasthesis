.class abstract Lcom/epson/eposdevice/simpleserial/NativeSimpleSerial;
.super Ljava/lang/Object;
.source "NativeSimpleSerial.java"


# direct methods
.method constructor <init>()V
    .locals 0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method protected abstract nativeOnSimpleSerialCommandReply(Ljava/lang/String;Ljava/lang/String;[B)V
.end method

.method protected native nativeSetSscCommandReplyEventCallback(JLcom/epson/eposdevice/simpleserial/NativeSimpleSerial;)I
.end method

.method protected native nativeSscSendCommand(J[B)I
.end method
