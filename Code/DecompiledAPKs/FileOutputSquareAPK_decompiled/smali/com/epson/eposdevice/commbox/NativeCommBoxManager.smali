.class abstract Lcom/epson/eposdevice/commbox/NativeCommBoxManager;
.super Ljava/lang/Object;
.source "NativeCommBoxManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/eposdevice/commbox/NativeCommBoxManager$NativeCloseCommBoxCallbackAdapter;,
        Lcom/epson/eposdevice/commbox/NativeCommBoxManager$NativeOpenCommBoxCallbackAdapter;
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method protected native nativeCloseCommBox(JJ[JLcom/epson/eposdevice/commbox/NativeCommBoxManager$NativeCloseCommBoxCallbackAdapter;)I
.end method

.method protected native nativeOpenCommBox(JLjava/lang/String;Ljava/lang/String;[JLcom/epson/eposdevice/commbox/NativeCommBoxManager$NativeOpenCommBoxCallbackAdapter;)I
.end method
