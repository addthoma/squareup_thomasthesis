.class public abstract Lcom/epson/eposdevice/commbox/CommBoxManager;
.super Lcom/epson/eposdevice/commbox/NativeCommBoxManager;
.source "CommBoxManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/eposdevice/commbox/CommBoxManager$CommBoxAccessor;,
        Lcom/epson/eposdevice/commbox/CommBoxManager$CloseCommBoxCallbackAdapter;,
        Lcom/epson/eposdevice/commbox/CommBoxManager$OpenCommBoxCallbackAdapter;
    }
.end annotation


# instance fields
.field private mCommBoxList:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector<",
            "Lcom/epson/eposdevice/commbox/CommBoxManager$CommBoxAccessor;",
            ">;"
        }
    .end annotation
.end field

.field private mCommBoxManagerHandle:J


# direct methods
.method protected constructor <init>(J)V
    .locals 2

    .line 13
    invoke-direct {p0}, Lcom/epson/eposdevice/commbox/NativeCommBoxManager;-><init>()V

    const-wide/16 v0, 0x0

    .line 10
    iput-wide v0, p0, Lcom/epson/eposdevice/commbox/CommBoxManager;->mCommBoxManagerHandle:J

    .line 11
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lcom/epson/eposdevice/commbox/CommBoxManager;->mCommBoxList:Ljava/util/Vector;

    .line 14
    iput-wide p1, p0, Lcom/epson/eposdevice/commbox/CommBoxManager;->mCommBoxManagerHandle:J

    return-void
.end method

.method static synthetic access$000(Lcom/epson/eposdevice/commbox/CommBoxManager;)Ljava/util/Vector;
    .locals 0

    .line 9
    iget-object p0, p0, Lcom/epson/eposdevice/commbox/CommBoxManager;->mCommBoxList:Ljava/util/Vector;

    return-object p0
.end method


# virtual methods
.method public closeCommBox(Lcom/epson/eposdevice/commbox/CommBox;[ILcom/epson/eposdevice/commbox/CloseCommBoxListener;)V
    .locals 18
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    move-object/from16 v8, p0

    move-object/from16 v0, p1

    move-object/from16 v9, p2

    move-object/from16 v10, p3

    const/4 v11, 0x3

    new-array v1, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object v0, v1, v12

    const/4 v13, 0x1

    aput-object v9, v1, v13

    const/4 v14, 0x2

    aput-object v10, v1, v14

    const-string v15, "closeCommBox"

    .line 56
    invoke-virtual {v8, v15, v1}, Lcom/epson/eposdevice/commbox/CommBoxManager;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 58
    :try_start_0
    iget-wide v1, v8, Lcom/epson/eposdevice/commbox/CommBoxManager;->mCommBoxManagerHandle:J

    const-wide/16 v3, 0x0

    cmp-long v5, v1, v3

    if-eqz v5, :cond_4

    if-eqz v9, :cond_3

    .line 64
    array-length v1, v9

    if-eqz v1, :cond_2

    .line 67
    instance-of v1, v0, Lcom/epson/eposdevice/commbox/CommBoxManager$CommBoxAccessor;

    if-eqz v1, :cond_1

    .line 71
    move-object v1, v0

    check-cast v1, Lcom/epson/eposdevice/commbox/CommBoxManager$CommBoxAccessor;

    new-array v7, v13, [J

    .line 73
    iget-wide v2, v8, Lcom/epson/eposdevice/commbox/CommBoxManager;->mCommBoxManagerHandle:J

    invoke-virtual {v1}, Lcom/epson/eposdevice/commbox/CommBoxManager$CommBoxAccessor;->getHandle()J

    move-result-wide v4

    new-instance v6, Lcom/epson/eposdevice/commbox/CommBoxManager$CloseCommBoxCallbackAdapter;

    invoke-direct {v6, v8, v10, v1}, Lcom/epson/eposdevice/commbox/CommBoxManager$CloseCommBoxCallbackAdapter;-><init>(Lcom/epson/eposdevice/commbox/CommBoxManager;Lcom/epson/eposdevice/commbox/CloseCommBoxListener;Lcom/epson/eposdevice/commbox/CommBoxManager$CommBoxAccessor;)V

    move-object/from16 v1, p0

    move-object/from16 v16, v6

    move-object v6, v7

    move-object/from16 v17, v7

    move-object/from16 v7, v16

    invoke-virtual/range {v1 .. v7}, Lcom/epson/eposdevice/commbox/CommBoxManager;->nativeCloseCommBox(JJ[JLcom/epson/eposdevice/commbox/NativeCommBoxManager$NativeCloseCommBoxCallbackAdapter;)I

    move-result v1

    if-nez v1, :cond_0

    .line 77
    aget-wide v1, v17, v12

    long-to-int v2, v1

    aput v2, v9, v12
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_0

    new-array v1, v11, [Ljava/lang/Object;

    aput-object v0, v1, v12

    aput-object v9, v1, v13

    aput-object v10, v1, v14

    .line 83
    invoke-virtual {v8, v15, v1}, Lcom/epson/eposdevice/commbox/CommBoxManager;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 75
    :cond_0
    :try_start_1
    new-instance v0, Lcom/epson/eposdevice/EposException;

    invoke-direct {v0, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw v0

    .line 68
    :cond_1
    new-instance v0, Lcom/epson/eposdevice/EposException;

    invoke-direct {v0, v13}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw v0

    .line 65
    :cond_2
    new-instance v0, Lcom/epson/eposdevice/EposException;

    invoke-direct {v0, v13}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw v0

    .line 62
    :cond_3
    new-instance v0, Lcom/epson/eposdevice/EposException;

    invoke-direct {v0, v13}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw v0

    .line 59
    :cond_4
    new-instance v0, Lcom/epson/eposdevice/EposException;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw v0
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v0

    .line 80
    invoke-virtual {v8, v15, v0}, Lcom/epson/eposdevice/commbox/CommBoxManager;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 81
    throw v0
.end method

.method protected abstract createCommBoxInstance(J)Lcom/epson/eposdevice/commbox/CommBoxManager$CommBoxAccessor;
.end method

.method protected getInnerHandle()J
    .locals 2

    .line 17
    iget-wide v0, p0, Lcom/epson/eposdevice/commbox/CommBoxManager;->mCommBoxManagerHandle:J

    return-wide v0
.end method

.method protected innerDeleteInstance()V
    .locals 2

    const-wide/16 v0, 0x0

    .line 20
    iput-wide v0, p0, Lcom/epson/eposdevice/commbox/CommBoxManager;->mCommBoxManagerHandle:J

    .line 21
    iget-object v0, p0, Lcom/epson/eposdevice/commbox/CommBoxManager;->mCommBoxList:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 22
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 23
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/epson/eposdevice/commbox/CommBoxManager$CommBoxAccessor;

    invoke-virtual {v1}, Lcom/epson/eposdevice/commbox/CommBoxManager$CommBoxAccessor;->deleteInstance()V

    goto :goto_0

    .line 25
    :cond_0
    iget-object v0, p0, Lcom/epson/eposdevice/commbox/CommBoxManager;->mCommBoxList:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    return-void
.end method

.method public openCommBox(Ljava/lang/String;Ljava/lang/String;[ILcom/epson/eposdevice/commbox/OpenCommBoxListener;)V
    .locals 18
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    move-object/from16 v8, p0

    move-object/from16 v0, p3

    move-object/from16 v9, p4

    const/4 v10, 0x4

    new-array v1, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object p1, v1, v11

    const/4 v12, 0x1

    aput-object p2, v1, v12

    const/4 v13, 0x2

    aput-object v0, v1, v13

    const/4 v14, 0x3

    aput-object v9, v1, v14

    const-string v15, "openCommBox"

    .line 29
    invoke-virtual {v8, v15, v1}, Lcom/epson/eposdevice/commbox/CommBoxManager;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 31
    :try_start_0
    iget-wide v1, v8, Lcom/epson/eposdevice/commbox/CommBoxManager;->mCommBoxManagerHandle:J

    const-wide/16 v3, 0x0

    cmp-long v5, v1, v3

    if-eqz v5, :cond_3

    if-eqz v0, :cond_2

    .line 37
    array-length v1, v0

    if-eqz v1, :cond_1

    new-array v7, v12, [J

    .line 42
    iget-wide v2, v8, Lcom/epson/eposdevice/commbox/CommBoxManager;->mCommBoxManagerHandle:J

    new-instance v6, Lcom/epson/eposdevice/commbox/CommBoxManager$OpenCommBoxCallbackAdapter;

    invoke-direct {v6, v8, v9}, Lcom/epson/eposdevice/commbox/CommBoxManager$OpenCommBoxCallbackAdapter;-><init>(Lcom/epson/eposdevice/commbox/CommBoxManager;Lcom/epson/eposdevice/commbox/OpenCommBoxListener;)V

    move-object/from16 v1, p0

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move-object/from16 v16, v6

    move-object v6, v7

    move-object/from16 v17, v7

    move-object/from16 v7, v16

    invoke-virtual/range {v1 .. v7}, Lcom/epson/eposdevice/commbox/CommBoxManager;->nativeOpenCommBox(JLjava/lang/String;Ljava/lang/String;[JLcom/epson/eposdevice/commbox/NativeCommBoxManager$NativeOpenCommBoxCallbackAdapter;)I

    move-result v1

    if-nez v1, :cond_0

    .line 46
    aget-wide v1, v17, v11

    long-to-int v2, v1

    aput v2, v0, v11
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_0

    new-array v1, v10, [Ljava/lang/Object;

    aput-object p1, v1, v11

    aput-object p2, v1, v12

    aput-object v0, v1, v13

    aput-object v9, v1, v14

    .line 52
    invoke-virtual {v8, v15, v1}, Lcom/epson/eposdevice/commbox/CommBoxManager;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 44
    :cond_0
    :try_start_1
    new-instance v0, Lcom/epson/eposdevice/EposException;

    invoke-direct {v0, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw v0

    .line 38
    :cond_1
    new-instance v0, Lcom/epson/eposdevice/EposException;

    invoke-direct {v0, v12}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw v0

    .line 35
    :cond_2
    new-instance v0, Lcom/epson/eposdevice/EposException;

    invoke-direct {v0, v12}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw v0

    .line 32
    :cond_3
    new-instance v0, Lcom/epson/eposdevice/EposException;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw v0
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v0

    .line 49
    invoke-virtual {v8, v15, v0}, Lcom/epson/eposdevice/commbox/CommBoxManager;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 50
    throw v0
.end method

.method protected abstract outputException(Ljava/lang/String;Ljava/lang/Exception;)V
.end method

.method protected varargs abstract outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V
.end method

.method protected varargs abstract outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V
.end method

.method protected varargs abstract outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V
.end method
