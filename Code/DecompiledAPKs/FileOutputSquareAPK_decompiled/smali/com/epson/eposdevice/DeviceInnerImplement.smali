.class abstract Lcom/epson/eposdevice/DeviceInnerImplement;
.super Lcom/epson/eposdevice/NativeDevice;
.source "DeviceInnerImplement.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/eposdevice/DeviceInnerImplement$CommBoxManagerInner;,
        Lcom/epson/eposdevice/DeviceInnerImplement$SimpleSerialInner;,
        Lcom/epson/eposdevice/DeviceInnerImplement$ScannerInner;,
        Lcom/epson/eposdevice/DeviceInnerImplement$KeyboardInner;,
        Lcom/epson/eposdevice/DeviceInnerImplement$DisplayInner;,
        Lcom/epson/eposdevice/DeviceInnerImplement$PrinterInner;,
        Lcom/epson/eposdevice/DeviceInnerImplement$IDeviceObject;,
        Lcom/epson/eposdevice/DeviceInnerImplement$IHandleObject;
    }
.end annotation


# instance fields
.field protected LOGIF_FUNC_CB_EVENT:I

.field protected LOGIF_FUNC_IN:I

.field protected LOGIF_FUNC_OUT_WITHOUT_RET:I

.field protected LOGIF_FUNC_OUT_WITH_RET:I


# direct methods
.method constructor <init>()V
    .locals 1

    .line 13
    invoke-direct {p0}, Lcom/epson/eposdevice/NativeDevice;-><init>()V

    const/4 v0, 0x0

    .line 262
    iput v0, p0, Lcom/epson/eposdevice/DeviceInnerImplement;->LOGIF_FUNC_IN:I

    const/4 v0, 0x1

    .line 263
    iput v0, p0, Lcom/epson/eposdevice/DeviceInnerImplement;->LOGIF_FUNC_OUT_WITH_RET:I

    const/4 v0, 0x2

    .line 264
    iput v0, p0, Lcom/epson/eposdevice/DeviceInnerImplement;->LOGIF_FUNC_OUT_WITHOUT_RET:I

    const/4 v0, 0x3

    .line 265
    iput v0, p0, Lcom/epson/eposdevice/DeviceInnerImplement;->LOGIF_FUNC_CB_EVENT:I

    return-void
.end method


# virtual methods
.method protected processOutputExceptionLog(IJLjava/lang/String;Ljava/lang/Exception;)V
    .locals 8

    .line 296
    new-instance v0, Ljava/io/StringWriter;

    invoke-direct {v0}, Ljava/io/StringWriter;-><init>()V

    .line 297
    new-instance v1, Ljava/io/PrintWriter;

    invoke-direct {v1, v0}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    .line 299
    invoke-virtual {p5, v1}, Ljava/lang/Exception;->printStackTrace(Ljava/io/PrintWriter;)V

    .line 301
    invoke-virtual {v1}, Ljava/io/PrintWriter;->flush()V

    .line 303
    invoke-virtual {v0}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object v7

    move-object v2, p0

    move v3, p1

    move-wide v4, p2

    move-object v6, p4

    invoke-virtual/range {v2 .. v7}, Lcom/epson/eposdevice/DeviceInnerImplement;->nativeLogOutput(IJLjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected processOutputLogData(IJLjava/lang/String;[Ljava/lang/Object;)V
    .locals 7

    const-string v0, "("

    if-eqz p5, :cond_4

    const/4 v1, 0x0

    const-string v2, ""

    move-object v3, v2

    move-object v2, v0

    const/4 v0, 0x0

    .line 273
    :goto_0
    array-length v4, p5

    if-ge v0, v4, :cond_3

    .line 274
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 275
    aget-object v3, p5, v0

    if-nez v3, :cond_0

    .line 276
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "null"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 278
    :cond_0
    aget-object v3, p5, v0

    instance-of v3, v3, Ljava/lang/String;

    if-eqz v3, :cond_1

    .line 279
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v2, p5, v0

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 281
    :cond_1
    aget-object v3, p5, v0

    instance-of v3, v3, Lcom/epson/eposdevice/DeviceInnerImplement$IHandleObject;

    if-eqz v3, :cond_2

    .line 282
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aget-object v4, p5, v0

    check-cast v4, Lcom/epson/eposdevice/DeviceInnerImplement$IHandleObject;

    invoke-interface {v4}, Lcom/epson/eposdevice/DeviceInnerImplement$IHandleObject;->getDeviceHandle()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v1

    const-string v4, "%08x"

    invoke-static {v4, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 285
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v2, p5, v0

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_1
    add-int/lit8 v0, v0, 0x1

    const-string v3, ", "

    goto/16 :goto_0

    :cond_3
    move-object v0, v2

    .line 290
    :cond_4
    new-instance p5, Ljava/lang/StringBuilder;

    invoke-direct {p5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ")"

    invoke-virtual {p5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object v1, p0

    move v2, p1

    move-wide v3, p2

    move-object v5, p4

    .line 292
    invoke-virtual/range {v1 .. v6}, Lcom/epson/eposdevice/DeviceInnerImplement;->nativeLogOutput(IJLjava/lang/String;Ljava/lang/String;)V

    return-void
.end method
