.class abstract Lcom/epson/eposdevice/NativeDevice;
.super Ljava/lang/Object;
.source "NativeDevice.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/epson/eposdevice/NativeDevice$NativeDeleteDeviceCallbackAdapter;,
        Lcom/epson/eposdevice/NativeDevice$NativeCreateDeviceCallbackAdapter;,
        Lcom/epson/eposdevice/NativeDevice$NativeConnectCallbackAdapter;
    }
.end annotation


# static fields
.field protected static final NATIVE_DEV_TYPE_DISPLAY:I = 0x1

.field protected static final NATIVE_DEV_TYPE_KEYBOARD:I = 0x2

.field protected static final NATIVE_DEV_TYPE_PRINTER:I = 0x0

.field protected static final NATIVE_DEV_TYPE_SCANNER:I = 0x3

.field protected static final NATIVE_DEV_TYPE_SIMPLE_SERIAL:I = 0x4

.field protected static final NATIVE_FALSE:I = 0x0

.field protected static final NATIVE_PARAM_DEFAULT:I = -0x2

.field protected static final NATIVE_PARAM_UNSPECIFIED:I = -0x1

.field protected static final NATIVE_TRUE:I = 0x1


# direct methods
.method constructor <init>()V
    .locals 0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method protected native nativeConnect(JLjava/lang/String;Lcom/epson/eposdevice/NativeDevice$NativeConnectCallbackAdapter;)I
.end method

.method protected native nativeCreateDevice(JLjava/lang/String;IIILcom/epson/eposdevice/NativeDevice$NativeCreateDeviceCallbackAdapter;)I
.end method

.method protected native nativeCreateHandle([J)I
.end method

.method protected native nativeDeleteDevice(JJLcom/epson/eposdevice/NativeDevice$NativeDeleteDeviceCallbackAdapter;)I
.end method

.method protected native nativeDeleteHandle(J)I
.end method

.method protected native nativeDisconnect(J)I
.end method

.method protected native nativeGetAdmin(J)Ljava/lang/String;
.end method

.method protected native nativeGetCommBoxManager(J[J)I
.end method

.method protected native nativeGetLocal(J)Ljava/lang/String;
.end method

.method protected native nativeIsConnect(J)Z
.end method

.method protected native nativeLogOutput(IJLjava/lang/String;Ljava/lang/String;)V
.end method

.method protected abstract nativeOnDisconnect(Ljava/lang/String;)V
.end method

.method protected abstract nativeOnReconnect(Ljava/lang/String;)V
.end method

.method protected abstract nativeOnReconnecting(Ljava/lang/String;)V
.end method

.method protected native nativeReadLogSettings(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
.end method

.method protected native nativeSetDeviceCallback(JLcom/epson/eposdevice/NativeDevice;)V
.end method
