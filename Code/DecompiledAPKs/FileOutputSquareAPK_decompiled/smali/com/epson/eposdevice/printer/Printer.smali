.class public abstract Lcom/epson/eposdevice/printer/Printer;
.super Lcom/epson/eposdevice/printer/NativePrinter;
.source "Printer.java"


# static fields
.field public static final ALIGN_CENTER:I = 0x1

.field public static final ALIGN_LEFT:I = 0x0

.field public static final ALIGN_RIGHT:I = 0x2

.field public static final BARCODE_CODABAR:I = 0x8

.field public static final BARCODE_CODE128:I = 0xa

.field public static final BARCODE_CODE39:I = 0x6

.field public static final BARCODE_CODE93:I = 0x9

.field public static final BARCODE_EAN13:I = 0x2

.field public static final BARCODE_EAN8:I = 0x4

.field public static final BARCODE_GS1_128:I = 0xb

.field public static final BARCODE_GS1_DATABAR_EXPANDED:I = 0xf

.field public static final BARCODE_GS1_DATABAR_LIMITED:I = 0xe

.field public static final BARCODE_GS1_DATABAR_OMNIDIRECTIONAL:I = 0xc

.field public static final BARCODE_GS1_DATABAR_TRUNCATED:I = 0xd

.field public static final BARCODE_ITF:I = 0x7

.field public static final BARCODE_JAN13:I = 0x3

.field public static final BARCODE_JAN8:I = 0x5

.field public static final BARCODE_UPC_A:I = 0x0

.field public static final BARCODE_UPC_E:I = 0x1

.field public static final COLOR_1:I = 0x1

.field public static final COLOR_2:I = 0x2

.field public static final COLOR_3:I = 0x3

.field public static final COLOR_4:I = 0x4

.field public static final COLOR_NONE:I = 0x0

.field public static final CUT_FEED:I = 0x1

.field public static final CUT_NO_FEED:I = 0x0

.field public static final CUT_RESERVE:I = 0x2

.field private static final DEFAULT_BRIGHTNESS:D = 1.0

.field private static final DEFAULT_INTERVAL:I = 0xbb8

.field private static final DEFAULT_TIMEOUT:I = 0x2710

.field public static final DIRECTION_BOTTOM_TO_TOP:I = 0x1

.field public static final DIRECTION_LEFT_TO_RIGHT:I = 0x0

.field public static final DIRECTION_RIGHT_TO_LEFT:I = 0x2

.field public static final DIRECTION_TOP_TO_BOTTOM:I = 0x3

.field public static final DRAWER_1:I = 0x0

.field public static final DRAWER_2:I = 0x1

.field public static final DRAWER_OPEN_LEVEL_HIGH:I = 0x1

.field public static final DRAWER_OPEN_LEVEL_LOW:I = 0x0

.field public static final FALSE:I = 0x0

.field public static final FEED_CURRENT_TOF:I = 0x2

.field public static final FEED_CUTTING:I = 0x1

.field public static final FEED_NEXT_TOF:I = 0x3

.field public static final FEED_PEELING:I = 0x0

.field public static final FONT_A:I = 0x0

.field public static final FONT_B:I = 0x1

.field public static final FONT_C:I = 0x2

.field public static final FONT_D:I = 0x3

.field public static final FONT_E:I = 0x4

.field public static final HALFTONE_DITHER:I = 0x0

.field public static final HALFTONE_ERROR_DIFFUSION:I = 0x1

.field public static final HALFTONE_THRESHOLD:I = 0x2

.field public static final HRI_ABOVE:I = 0x1

.field public static final HRI_BELOW:I = 0x2

.field public static final HRI_BOTH:I = 0x3

.field public static final HRI_NONE:I = 0x0

.field public static final LANG_EN:I = 0x0

.field public static final LANG_JA:I = 0x1

.field public static final LANG_KO:I = 0x4

.field public static final LANG_TH:I = 0x5

.field public static final LANG_ZH_CN:I = 0x2

.field public static final LANG_ZH_TW:I = 0x3

.field public static final LAYOUT_LABEL:I = 0x1

.field public static final LAYOUT_LABEL_BM:I = 0x2

.field public static final LAYOUT_RECEIPT:I = 0x0

.field public static final LAYOUT_RECEIPT_BM:I = 0x3

.field public static final LEVEL_0:I = 0x0

.field public static final LEVEL_1:I = 0x1

.field public static final LEVEL_2:I = 0x2

.field public static final LEVEL_3:I = 0x3

.field public static final LEVEL_4:I = 0x4

.field public static final LEVEL_5:I = 0x5

.field public static final LEVEL_6:I = 0x6

.field public static final LEVEL_7:I = 0x7

.field public static final LEVEL_8:I = 0x8

.field public static final LEVEL_DEFAULT:I = 0xd

.field public static final LEVEL_H:I = 0xc

.field public static final LEVEL_L:I = 0x9

.field public static final LEVEL_M:I = 0xa

.field public static final LEVEL_Q:I = 0xb

.field public static final LINE_MEDIUM:I = 0x1

.field public static final LINE_MEDIUM_DOUBLE:I = 0x4

.field public static final LINE_THICK:I = 0x2

.field public static final LINE_THICK_DOUBLE:I = 0x5

.field public static final LINE_THIN:I = 0x0

.field public static final LINE_THIN_DOUBLE:I = 0x3

.field public static final MODE_GRAY16:I = 0x1

.field public static final MODE_MONO:I = 0x0

.field public static final PARAM_DEFAULT:I = -0x2

.field public static final PARAM_UNSPECIFIED:I = -0x1

.field public static final PATTERN_1:I = 0x8

.field public static final PATTERN_10:I = 0x11

.field public static final PATTERN_2:I = 0x9

.field public static final PATTERN_3:I = 0xa

.field public static final PATTERN_4:I = 0xb

.field public static final PATTERN_5:I = 0xc

.field public static final PATTERN_6:I = 0xd

.field public static final PATTERN_7:I = 0xe

.field public static final PATTERN_8:I = 0xf

.field public static final PATTERN_9:I = 0x10

.field public static final PATTERN_A:I = 0x1

.field public static final PATTERN_B:I = 0x2

.field public static final PATTERN_C:I = 0x3

.field public static final PATTERN_D:I = 0x4

.field public static final PATTERN_E:I = 0x5

.field public static final PATTERN_ERROR:I = 0x6

.field public static final PATTERN_NONE:I = 0x0

.field public static final PATTERN_PAPER_END:I = 0x7

.field public static final PULSE_100:I = 0x0

.field public static final PULSE_200:I = 0x1

.field public static final PULSE_300:I = 0x2

.field public static final PULSE_400:I = 0x3

.field public static final PULSE_500:I = 0x4

.field public static final ST_AUTOCUTTER_ERR:I = 0x800

.field public static final ST_AUTORECOVER_ERR:I = 0x4000

.field public static final ST_BATTERY_OFFLINE:I = 0x4

.field public static final ST_BUZZER:I = 0x1000000

.field public static final ST_COVER_OPEN:I = 0x20

.field public static final ST_DRAWER_KICK:I = 0x4

.field public static final ST_MECHANICAL_ERR:I = 0x400

.field public static final ST_NO_RESPONSE:I = 0x1

.field public static final ST_OFF_LINE:I = 0x8

.field public static final ST_PANEL_SWITCH:I = 0x200

.field public static final ST_PAPER_FEED:I = 0x40

.field public static final ST_PRINT_SUCCESS:I = 0x2

.field public static final ST_RECEIPT_END:I = 0x80000

.field public static final ST_RECEIPT_NEAR_END:I = 0x20000

.field public static final ST_SPOOLER_IS_STOPPED:I = -0x80000000

.field public static final ST_UNRECOVER_ERR:I = 0x2000

.field public static final ST_WAIT_ON_LINE:I = 0x100

.field public static final SYMBOL_AZTECCODE_COMPACT:I = 0xd

.field public static final SYMBOL_AZTECCODE_FULLRANGE:I = 0xc

.field public static final SYMBOL_DATAMATRIX_RECTANGLE_12:I = 0x10

.field public static final SYMBOL_DATAMATRIX_RECTANGLE_16:I = 0x11

.field public static final SYMBOL_DATAMATRIX_RECTANGLE_8:I = 0xf

.field public static final SYMBOL_DATAMATRIX_SQUARE:I = 0xe

.field public static final SYMBOL_GS1_DATABAR_EXPANDED_STACKED:I = 0xb

.field public static final SYMBOL_GS1_DATABAR_STACKED:I = 0x9

.field public static final SYMBOL_GS1_DATABAR_STACKED_OMNIDIRECTIONAL:I = 0xa

.field public static final SYMBOL_MAXICODE_MODE_2:I = 0x4

.field public static final SYMBOL_MAXICODE_MODE_3:I = 0x5

.field public static final SYMBOL_MAXICODE_MODE_4:I = 0x6

.field public static final SYMBOL_MAXICODE_MODE_5:I = 0x7

.field public static final SYMBOL_MAXICODE_MODE_6:I = 0x8

.field public static final SYMBOL_PDF417_STANDARD:I = 0x0

.field public static final SYMBOL_PDF417_TRUNCATED:I = 0x1

.field public static final SYMBOL_QRCODE_MICRO:I = 0x12

.field public static final SYMBOL_QRCODE_MODEL_1:I = 0x2

.field public static final SYMBOL_QRCODE_MODEL_2:I = 0x3

.field public static final TRUE:I = 0x1


# instance fields
.field private final BLANK_FALSE:Z

.field private final BLANK_TRUE:Z

.field private final MAX_PRINTJOBID_LENGTH:I

.field private final MIN_IMAGE_HEIGHT:I

.field private final MIN_IMAGE_WIDTH:I

.field private final MIN_PRINTJOBID_LENGTH:I

.field private mBatteryLowListener:Lcom/epson/eposdevice/printer/BatteryLowListener;

.field private mBatteryOkListener:Lcom/epson/eposdevice/printer/BatteryOkListener;

.field private mBatteryStatusChangeListener:Lcom/epson/eposdevice/printer/BatteryStatusChangeListener;

.field private mBrightness:D

.field private mCoverOkListener:Lcom/epson/eposdevice/printer/CoverOkListener;

.field private mCoverOpenListener:Lcom/epson/eposdevice/printer/CoverOpenListener;

.field private mDrawerClosedListener:Lcom/epson/eposdevice/printer/DrawerClosedListener;

.field private mDrawerOpenLevel:I

.field private mDrawerOpenListener:Lcom/epson/eposdevice/printer/DrawerOpenListener;

.field private mHalftone:I

.field private mInterval:I

.field private mJobReceiveListener:Lcom/epson/eposdevice/printer/JobReceiveListener;

.field private mOfflineListener:Lcom/epson/eposdevice/printer/OfflineListener;

.field private mOnlineListener:Lcom/epson/eposdevice/printer/OnlineListener;

.field private mPaperEndListener:Lcom/epson/eposdevice/printer/PaperEndListener;

.field private mPaperNearEndListener:Lcom/epson/eposdevice/printer/PaperNearEndListener;

.field private mPaperOkListener:Lcom/epson/eposdevice/printer/PaperOkListener;

.field private mPowerOffListener:Lcom/epson/eposdevice/printer/PowerOffListener;

.field private mPrinterHandle:J

.field private mReceiveListener:Lcom/epson/eposdevice/printer/ReceiveListener;

.field private mStatusChangeListener:Lcom/epson/eposdevice/printer/StatusChangeListener;

.field private mTimeout:I

.field private mforce:Z


# direct methods
.method protected constructor <init>(J)V
    .locals 3

    .line 372
    invoke-direct {p0}, Lcom/epson/eposdevice/printer/NativePrinter;-><init>()V

    const-wide/16 v0, 0x0

    .line 17
    iput-wide v0, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    const/4 v0, 0x0

    .line 18
    iput v0, p0, Lcom/epson/eposdevice/printer/Printer;->mHalftone:I

    const-wide/16 v1, 0x0

    .line 19
    iput-wide v1, p0, Lcom/epson/eposdevice/printer/Printer;->mBrightness:D

    .line 20
    iput-boolean v0, p0, Lcom/epson/eposdevice/printer/Printer;->mforce:Z

    .line 21
    iput v0, p0, Lcom/epson/eposdevice/printer/Printer;->mTimeout:I

    .line 22
    iput v0, p0, Lcom/epson/eposdevice/printer/Printer;->mInterval:I

    .line 24
    iput v0, p0, Lcom/epson/eposdevice/printer/Printer;->mDrawerOpenLevel:I

    const/4 v1, 0x1

    .line 182
    iput v1, p0, Lcom/epson/eposdevice/printer/Printer;->MIN_IMAGE_WIDTH:I

    .line 183
    iput v1, p0, Lcom/epson/eposdevice/printer/Printer;->MIN_IMAGE_HEIGHT:I

    .line 186
    iput v1, p0, Lcom/epson/eposdevice/printer/Printer;->MIN_PRINTJOBID_LENGTH:I

    const/16 v2, 0x1e

    .line 187
    iput v2, p0, Lcom/epson/eposdevice/printer/Printer;->MAX_PRINTJOBID_LENGTH:I

    .line 188
    iput-boolean v1, p0, Lcom/epson/eposdevice/printer/Printer;->BLANK_TRUE:Z

    .line 189
    iput-boolean v0, p0, Lcom/epson/eposdevice/printer/Printer;->BLANK_FALSE:Z

    .line 373
    iput-wide p1, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    .line 374
    iput v0, p0, Lcom/epson/eposdevice/printer/Printer;->mHalftone:I

    const-wide/high16 p1, 0x3ff0000000000000L    # 1.0

    .line 375
    iput-wide p1, p0, Lcom/epson/eposdevice/printer/Printer;->mBrightness:D

    .line 376
    iput-boolean v0, p0, Lcom/epson/eposdevice/printer/Printer;->mforce:Z

    const/16 p1, 0x2710

    .line 377
    iput p1, p0, Lcom/epson/eposdevice/printer/Printer;->mTimeout:I

    const/16 p1, 0xbb8

    .line 378
    iput p1, p0, Lcom/epson/eposdevice/printer/Printer;->mInterval:I

    .line 380
    iput v0, p0, Lcom/epson/eposdevice/printer/Printer;->mDrawerOpenLevel:I

    return-void
.end method

.method private onPtrBatteryLow(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 335
    iget-object v0, p0, Lcom/epson/eposdevice/printer/Printer;->mBatteryLowListener:Lcom/epson/eposdevice/printer/BatteryLowListener;

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    aput-object p2, v0, v1

    const-string v1, "onPtrBatteryLow"

    .line 336
    invoke-virtual {p0, v1, v0}, Lcom/epson/eposdevice/printer/Printer;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 337
    iget-object v0, p0, Lcom/epson/eposdevice/printer/Printer;->mBatteryLowListener:Lcom/epson/eposdevice/printer/BatteryLowListener;

    invoke-interface {v0, p1, p2}, Lcom/epson/eposdevice/printer/BatteryLowListener;->onPtrBatteryLow(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private onPtrBatteryOk(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 342
    iget-object v0, p0, Lcom/epson/eposdevice/printer/Printer;->mBatteryOkListener:Lcom/epson/eposdevice/printer/BatteryOkListener;

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    aput-object p2, v0, v1

    const-string v1, "onPtrBatteryOk"

    .line 343
    invoke-virtual {p0, v1, v0}, Lcom/epson/eposdevice/printer/Printer;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 344
    iget-object v0, p0, Lcom/epson/eposdevice/printer/Printer;->mBatteryOkListener:Lcom/epson/eposdevice/printer/BatteryOkListener;

    invoke-interface {v0, p1, p2}, Lcom/epson/eposdevice/printer/BatteryOkListener;->onPtrBatteryOk(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private onPtrBatteryStatusChange(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3

    .line 349
    iget-object v0, p0, Lcom/epson/eposdevice/printer/Printer;->mBatteryStatusChangeListener:Lcom/epson/eposdevice/printer/BatteryStatusChangeListener;

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    aput-object p2, v0, v1

    const/4 v1, 0x2

    .line 350
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const-string v1, "onPtrBatteryStatusChange"

    invoke-virtual {p0, v1, v0}, Lcom/epson/eposdevice/printer/Printer;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 351
    iget-object v0, p0, Lcom/epson/eposdevice/printer/Printer;->mBatteryStatusChangeListener:Lcom/epson/eposdevice/printer/BatteryStatusChangeListener;

    invoke-interface {v0, p1, p2, p3}, Lcom/epson/eposdevice/printer/BatteryStatusChangeListener;->onPtrBatteryStatusChange(Ljava/lang/String;Ljava/lang/String;I)V

    :cond_0
    return-void
.end method

.method private onPtrCoverOk(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 286
    iget-object v0, p0, Lcom/epson/eposdevice/printer/Printer;->mCoverOkListener:Lcom/epson/eposdevice/printer/CoverOkListener;

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    aput-object p2, v0, v1

    const-string v1, "onPtrCoverOk"

    .line 287
    invoke-virtual {p0, v1, v0}, Lcom/epson/eposdevice/printer/Printer;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 288
    iget-object v0, p0, Lcom/epson/eposdevice/printer/Printer;->mCoverOkListener:Lcom/epson/eposdevice/printer/CoverOkListener;

    invoke-interface {v0, p1, p2}, Lcom/epson/eposdevice/printer/CoverOkListener;->onPtrCoverOk(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private onPtrCoverOpen(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 293
    iget-object v0, p0, Lcom/epson/eposdevice/printer/Printer;->mCoverOpenListener:Lcom/epson/eposdevice/printer/CoverOpenListener;

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    aput-object p2, v0, v1

    const-string v1, "onPtrCoverOpen"

    .line 294
    invoke-virtual {p0, v1, v0}, Lcom/epson/eposdevice/printer/Printer;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 295
    iget-object v0, p0, Lcom/epson/eposdevice/printer/Printer;->mCoverOpenListener:Lcom/epson/eposdevice/printer/CoverOpenListener;

    invoke-interface {v0, p1, p2}, Lcom/epson/eposdevice/printer/CoverOpenListener;->onPtrCoverOpen(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private onPtrDrawerClosed(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 321
    iget-object v0, p0, Lcom/epson/eposdevice/printer/Printer;->mDrawerClosedListener:Lcom/epson/eposdevice/printer/DrawerClosedListener;

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    aput-object p2, v0, v1

    const-string v1, "onPtrDrawerClosed"

    .line 322
    invoke-virtual {p0, v1, v0}, Lcom/epson/eposdevice/printer/Printer;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 323
    iget-object v0, p0, Lcom/epson/eposdevice/printer/Printer;->mDrawerClosedListener:Lcom/epson/eposdevice/printer/DrawerClosedListener;

    invoke-interface {v0, p1, p2}, Lcom/epson/eposdevice/printer/DrawerClosedListener;->onPtrDrawerClosed(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private onPtrDrawerOpen(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 328
    iget-object v0, p0, Lcom/epson/eposdevice/printer/Printer;->mDrawerOpenListener:Lcom/epson/eposdevice/printer/DrawerOpenListener;

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    aput-object p2, v0, v1

    const-string v1, "onPtrDrawerOpen"

    .line 329
    invoke-virtual {p0, v1, v0}, Lcom/epson/eposdevice/printer/Printer;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 330
    iget-object v0, p0, Lcom/epson/eposdevice/printer/Printer;->mDrawerOpenListener:Lcom/epson/eposdevice/printer/DrawerOpenListener;

    invoke-interface {v0, p1, p2}, Lcom/epson/eposdevice/printer/DrawerOpenListener;->onPtrDrawerOpen(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private onPtrOffline(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 272
    iget-object v0, p0, Lcom/epson/eposdevice/printer/Printer;->mOfflineListener:Lcom/epson/eposdevice/printer/OfflineListener;

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    aput-object p2, v0, v1

    const-string v1, "onPtrOffline"

    .line 273
    invoke-virtual {p0, v1, v0}, Lcom/epson/eposdevice/printer/Printer;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 274
    iget-object v0, p0, Lcom/epson/eposdevice/printer/Printer;->mOfflineListener:Lcom/epson/eposdevice/printer/OfflineListener;

    invoke-interface {v0, p1, p2}, Lcom/epson/eposdevice/printer/OfflineListener;->onPtrOffline(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private onPtrOnline(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 265
    iget-object v0, p0, Lcom/epson/eposdevice/printer/Printer;->mOnlineListener:Lcom/epson/eposdevice/printer/OnlineListener;

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    aput-object p2, v0, v1

    const-string v1, "onPtrOnline"

    .line 266
    invoke-virtual {p0, v1, v0}, Lcom/epson/eposdevice/printer/Printer;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 267
    iget-object v0, p0, Lcom/epson/eposdevice/printer/Printer;->mOnlineListener:Lcom/epson/eposdevice/printer/OnlineListener;

    invoke-interface {v0, p1, p2}, Lcom/epson/eposdevice/printer/OnlineListener;->onPtrOnline(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private onPtrPaperEnd(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 314
    iget-object v0, p0, Lcom/epson/eposdevice/printer/Printer;->mPaperEndListener:Lcom/epson/eposdevice/printer/PaperEndListener;

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    aput-object p2, v0, v1

    const-string v1, "onPtrPaperEnd"

    .line 315
    invoke-virtual {p0, v1, v0}, Lcom/epson/eposdevice/printer/Printer;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 316
    iget-object v0, p0, Lcom/epson/eposdevice/printer/Printer;->mPaperEndListener:Lcom/epson/eposdevice/printer/PaperEndListener;

    invoke-interface {v0, p1, p2}, Lcom/epson/eposdevice/printer/PaperEndListener;->onPtrPaperEnd(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private onPtrPaperNearEnd(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 307
    iget-object v0, p0, Lcom/epson/eposdevice/printer/Printer;->mPaperNearEndListener:Lcom/epson/eposdevice/printer/PaperNearEndListener;

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    aput-object p2, v0, v1

    const-string v1, "onPtrPaperNearEnd"

    .line 308
    invoke-virtual {p0, v1, v0}, Lcom/epson/eposdevice/printer/Printer;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 309
    iget-object v0, p0, Lcom/epson/eposdevice/printer/Printer;->mPaperNearEndListener:Lcom/epson/eposdevice/printer/PaperNearEndListener;

    invoke-interface {v0, p1, p2}, Lcom/epson/eposdevice/printer/PaperNearEndListener;->onPtrPaperNearEnd(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private onPtrPaperOk(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 300
    iget-object v0, p0, Lcom/epson/eposdevice/printer/Printer;->mPaperOkListener:Lcom/epson/eposdevice/printer/PaperOkListener;

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    aput-object p2, v0, v1

    const-string v1, "onPtrPaperOk"

    .line 301
    invoke-virtual {p0, v1, v0}, Lcom/epson/eposdevice/printer/Printer;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 302
    iget-object v0, p0, Lcom/epson/eposdevice/printer/Printer;->mPaperOkListener:Lcom/epson/eposdevice/printer/PaperOkListener;

    invoke-interface {v0, p1, p2}, Lcom/epson/eposdevice/printer/PaperOkListener;->onPtrPaperOk(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private onPtrPowerOff(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 279
    iget-object v0, p0, Lcom/epson/eposdevice/printer/Printer;->mPowerOffListener:Lcom/epson/eposdevice/printer/PowerOffListener;

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    aput-object p2, v0, v1

    const-string v1, "onPtrPowerOff"

    .line 280
    invoke-virtual {p0, v1, v0}, Lcom/epson/eposdevice/printer/Printer;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 281
    iget-object v0, p0, Lcom/epson/eposdevice/printer/Printer;->mPowerOffListener:Lcom/epson/eposdevice/printer/PowerOffListener;

    invoke-interface {v0, p1, p2}, Lcom/epson/eposdevice/printer/PowerOffListener;->onPtrPowerOff(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private onPtrReceive(Ljava/lang/String;Ljava/lang/String;IIII)V
    .locals 9

    .line 356
    iget-object v0, p0, Lcom/epson/eposdevice/printer/Printer;->mReceiveListener:Lcom/epson/eposdevice/printer/ReceiveListener;

    if-eqz v0, :cond_0

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    aput-object p2, v0, v1

    const/4 v1, 0x2

    .line 357
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const-string v1, "onPtrReceive"

    invoke-virtual {p0, v1, v0}, Lcom/epson/eposdevice/printer/Printer;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 358
    iget-object v2, p0, Lcom/epson/eposdevice/printer/Printer;->mReceiveListener:Lcom/epson/eposdevice/printer/ReceiveListener;

    move-object v3, p1

    move-object v4, p2

    move v5, p3

    move v6, p4

    move v7, p5

    move v8, p6

    invoke-interface/range {v2 .. v8}, Lcom/epson/eposdevice/printer/ReceiveListener;->onPtrReceive(Ljava/lang/String;Ljava/lang/String;IIII)V

    :cond_0
    return-void
.end method

.method private onPtrReceive(Ljava/lang/String;Ljava/lang/String;IIIILjava/lang/String;)V
    .locals 11

    move-object v0, p0

    .line 364
    iget-object v1, v0, Lcom/epson/eposdevice/printer/Printer;->mJobReceiveListener:Lcom/epson/eposdevice/printer/JobReceiveListener;

    if-eqz v1, :cond_0

    const/4 v1, 0x7

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    aput-object p2, v1, v2

    const/4 v2, 0x2

    .line 365
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x5

    invoke-static/range {p6 .. p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x6

    aput-object p7, v1, v2

    const-string v2, "onPtrReceive"

    invoke-virtual {p0, v2, v1}, Lcom/epson/eposdevice/printer/Printer;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 366
    iget-object v3, v0, Lcom/epson/eposdevice/printer/Printer;->mJobReceiveListener:Lcom/epson/eposdevice/printer/JobReceiveListener;

    move-object v4, p1

    move-object v5, p2

    move v6, p3

    move v7, p4

    move/from16 v8, p5

    move/from16 v9, p6

    move-object/from16 v10, p7

    invoke-interface/range {v3 .. v10}, Lcom/epson/eposdevice/printer/JobReceiveListener;->onPtrReceive(Ljava/lang/String;Ljava/lang/String;IIIILjava/lang/String;)V

    :cond_0
    return-void
.end method

.method private onPtrStatusChange(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3

    .line 258
    iget-object v0, p0, Lcom/epson/eposdevice/printer/Printer;->mStatusChangeListener:Lcom/epson/eposdevice/printer/StatusChangeListener;

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    aput-object p2, v0, v1

    const/4 v1, 0x2

    .line 259
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const-string v1, "onPtrStatusChange"

    invoke-virtual {p0, v1, v0}, Lcom/epson/eposdevice/printer/Printer;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 260
    iget-object v0, p0, Lcom/epson/eposdevice/printer/Printer;->mStatusChangeListener:Lcom/epson/eposdevice/printer/StatusChangeListener;

    invoke-interface {v0, p1, p2, p3}, Lcom/epson/eposdevice/printer/StatusChangeListener;->onPtrStatusChange(Ljava/lang/String;Ljava/lang/String;I)V

    :cond_0
    return-void
.end method


# virtual methods
.method public addBarcode(Ljava/lang/String;IIIII)V
    .locals 21
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    move-object/from16 v12, p0

    const/4 v0, 0x5

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v13, 0x0

    aput-object p1, v1, v13

    .line 985
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v14, 0x1

    aput-object v2, v1, v14

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v15, 0x2

    aput-object v2, v1, v15

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v16, 0x3

    aput-object v2, v1, v16

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v17, 0x4

    aput-object v2, v1, v17

    const-string v10, "addBarcode"

    invoke-virtual {v12, v10, v1}, Lcom/epson/eposdevice/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 987
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/epson/eposdevice/printer/Printer;->checkHandle()V

    if-eqz p1, :cond_1

    .line 991
    iget-wide v2, v12, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_2

    move/from16 v11, p5

    int-to-long v8, v11

    move/from16 v1, p6

    int-to-long v6, v1

    move-object/from16 v1, p0

    move-object/from16 v4, p1

    move/from16 v5, p2

    move-wide/from16 v18, v6

    move/from16 v6, p3

    move/from16 v7, p4

    move-object/from16 v20, v10

    move-wide/from16 v10, v18

    :try_start_1
    invoke-virtual/range {v1 .. v11}, Lcom/epson/eposdevice/printer/Printer;->nativeAddBarcode(JLjava/lang/String;IIIJJ)I

    move-result v1
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v13

    .line 1000
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v14

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v15

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v16

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v17

    move-object/from16 v2, v20

    invoke-virtual {v12, v2, v0}, Lcom/epson/eposdevice/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    :cond_0
    move-object/from16 v2, v20

    .line 993
    :try_start_2
    new-instance v0, Lcom/epson/eposdevice/EposException;

    invoke-direct {v0, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw v0

    :catch_0
    move-exception v0

    move-object/from16 v2, v20

    goto :goto_0

    :cond_1
    move-object v2, v10

    .line 989
    new-instance v0, Lcom/epson/eposdevice/EposException;

    invoke-direct {v0, v14}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw v0
    :try_end_2
    .catch Lcom/epson/eposdevice/EposException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception v0

    goto :goto_0

    :catch_2
    move-exception v0

    move-object v2, v10

    .line 997
    :goto_0
    invoke-virtual {v12, v2, v0}, Lcom/epson/eposdevice/printer/Printer;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 998
    throw v0
.end method

.method public addCommand([B)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const-string v3, "addCommand"

    .line 1282
    invoke-virtual {p0, v3, v1}, Lcom/epson/eposdevice/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1284
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/eposdevice/printer/Printer;->checkHandle()V

    if-eqz p1, :cond_1

    .line 1288
    iget-wide v4, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    invoke-virtual {p0, v4, v5, p1}, Lcom/epson/eposdevice/printer/Printer;->nativeAddCommand(J[B)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    .line 1297
    invoke-virtual {p0, v3, v0}, Lcom/epson/eposdevice/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 1290
    :cond_0
    :try_start_1
    new-instance p1, Lcom/epson/eposdevice/EposException;

    invoke-direct {p1, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw p1

    .line 1286
    :cond_1
    new-instance p1, Lcom/epson/eposdevice/EposException;

    invoke-direct {p1, v0}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw p1
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception p1

    .line 1294
    invoke-virtual {p0, v3, p1}, Lcom/epson/eposdevice/printer/Printer;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1295
    throw p1
.end method

.method public addCut(I)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 1195
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "addCut"

    invoke-virtual {p0, v2, v1}, Lcom/epson/eposdevice/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1197
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/eposdevice/printer/Printer;->checkHandle()V

    .line 1198
    iget-wide v4, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    invoke-virtual {p0, v4, v5, p1}, Lcom/epson/eposdevice/printer/Printer;->nativeAddCut(JI)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 1207
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {p0, v2, v0}, Lcom/epson/eposdevice/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 1200
    :cond_0
    :try_start_1
    new-instance p1, Lcom/epson/eposdevice/EposException;

    invoke-direct {p1, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw p1
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception p1

    .line 1204
    invoke-virtual {p0, v2, p1}, Lcom/epson/eposdevice/printer/Printer;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1205
    throw p1
.end method

.method public addFeed()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "addFeed"

    .line 846
    invoke-virtual {p0, v2, v1}, Lcom/epson/eposdevice/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 848
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/eposdevice/printer/Printer;->checkHandle()V

    .line 849
    iget-wide v3, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    invoke-virtual {p0, v3, v4}, Lcom/epson/eposdevice/printer/Printer;->nativeAddFeed(J)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 858
    invoke-virtual {p0, v2, v0}, Lcom/epson/eposdevice/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 851
    :cond_0
    :try_start_1
    new-instance v0, Lcom/epson/eposdevice/EposException;

    invoke-direct {v0, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw v0
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v0

    .line 855
    invoke-virtual {p0, v2, v0}, Lcom/epson/eposdevice/printer/Printer;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 856
    throw v0
.end method

.method public addFeedLine(I)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 829
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "addFeedLine"

    invoke-virtual {p0, v2, v1}, Lcom/epson/eposdevice/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 831
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/eposdevice/printer/Printer;->checkHandle()V

    .line 832
    iget-wide v4, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    int-to-long v6, p1

    invoke-virtual {p0, v4, v5, v6, v7}, Lcom/epson/eposdevice/printer/Printer;->nativeAddFeedLine(JJ)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 841
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {p0, v2, v0}, Lcom/epson/eposdevice/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 834
    :cond_0
    :try_start_1
    new-instance p1, Lcom/epson/eposdevice/EposException;

    invoke-direct {p1, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw p1
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception p1

    .line 838
    invoke-virtual {p0, v2, p1}, Lcom/epson/eposdevice/printer/Printer;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 839
    throw p1
.end method

.method public addFeedPosition(I)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 1246
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "addFeedPosition"

    invoke-virtual {p0, v2, v1}, Lcom/epson/eposdevice/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1248
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/eposdevice/printer/Printer;->checkHandle()V

    .line 1249
    iget-wide v4, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    invoke-virtual {p0, v4, v5, p1}, Lcom/epson/eposdevice/printer/Printer;->nativeAddFeedPosition(JI)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 1258
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {p0, v2, v0}, Lcom/epson/eposdevice/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 1251
    :cond_0
    :try_start_1
    new-instance p1, Lcom/epson/eposdevice/EposException;

    invoke-direct {p1, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw p1
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception p1

    .line 1255
    invoke-virtual {p0, v2, p1}, Lcom/epson/eposdevice/printer/Printer;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1256
    throw p1
.end method

.method public addFeedUnit(I)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 812
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "addFeedUnit"

    invoke-virtual {p0, v2, v1}, Lcom/epson/eposdevice/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 814
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/eposdevice/printer/Printer;->checkHandle()V

    .line 815
    iget-wide v4, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    int-to-long v6, p1

    invoke-virtual {p0, v4, v5, v6, v7}, Lcom/epson/eposdevice/printer/Printer;->nativeAddFeedUnit(JJ)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 824
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {p0, v2, v0}, Lcom/epson/eposdevice/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 817
    :cond_0
    :try_start_1
    new-instance p1, Lcom/epson/eposdevice/EposException;

    invoke-direct {p1, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw p1
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception p1

    .line 821
    invoke-virtual {p0, v2, p1}, Lcom/epson/eposdevice/printer/Printer;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 822
    throw p1
.end method

.method public addHLine(III)V
    .locals 16
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    move-object/from16 v9, p0

    const/4 v0, 0x3

    new-array v1, v0, [Ljava/lang/Object;

    .line 1025
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v10, 0x0

    aput-object v2, v1, v10

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v11, 0x1

    aput-object v2, v1, v11

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v12, 0x2

    aput-object v2, v1, v12

    const-string v13, "addHLine"

    invoke-virtual {v9, v13, v1}, Lcom/epson/eposdevice/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1027
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/epson/eposdevice/printer/Printer;->checkHandle()V

    .line 1028
    iget-wide v2, v9, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    move/from16 v14, p1

    int-to-long v4, v14

    move/from16 v15, p2

    int-to-long v6, v15

    move-object/from16 v1, p0

    move/from16 v8, p3

    invoke-virtual/range {v1 .. v8}, Lcom/epson/eposdevice/printer/Printer;->nativeAddHLine(JJJI)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 1037
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v10

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v11

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v12

    invoke-virtual {v9, v13, v0}, Lcom/epson/eposdevice/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 1030
    :cond_0
    :try_start_1
    new-instance v0, Lcom/epson/eposdevice/EposException;

    invoke-direct {v0, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw v0
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v0

    .line 1034
    invoke-virtual {v9, v13, v0}, Lcom/epson/eposdevice/printer/Printer;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1035
    throw v0
.end method

.method public addImage(Landroid/graphics/Bitmap;IIIIII)V
    .locals 33
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    move-object/from16 v15, p0

    move-object/from16 v0, p1

    const/4 v13, 0x7

    new-array v1, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v0, v1, v14

    .line 863
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v11, 0x1

    aput-object v2, v1, v11

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v22, 0x2

    aput-object v2, v1, v22

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v23, 0x3

    aput-object v2, v1, v23

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v24, 0x4

    aput-object v2, v1, v24

    invoke-static/range {p6 .. p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v25, 0x5

    aput-object v2, v1, v25

    invoke-static/range {p7 .. p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v26, 0x6

    aput-object v2, v1, v26

    const-string v12, "addImage"

    invoke-virtual {v15, v12, v1}, Lcom/epson/eposdevice/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 865
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/epson/eposdevice/printer/Printer;->checkHandle()V

    if-eqz v0, :cond_9

    .line 871
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    .line 872
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    .line 885
    new-array v7, v10, [I

    const/16 v1, 0x100

    new-array v6, v1, [D

    .line 889
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    if-gt v11, v10, :cond_8

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    if-gt v11, v8, :cond_8

    .line 896
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v2

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    if-ne v2, v3, :cond_0

    move-object/from16 v16, v0

    goto :goto_0

    .line 900
    :cond_0
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v0, v2, v14}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v2
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_8
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_5

    if-eqz v2, :cond_7

    move-object/from16 v16, v2

    :goto_0
    const/4 v2, 0x0

    :goto_1
    const-wide v17, 0x406fe00000000000L    # 255.0

    if-ge v2, v1, :cond_1

    const-wide/high16 v3, 0x3ff0000000000000L    # 1.0

    move-object/from16 v20, v12

    int-to-double v11, v2

    div-double v11, v11, v17

    sub-double/2addr v3, v11

    .line 909
    :try_start_1
    aput-wide v3, v6, v2

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v12, v20

    const/4 v11, 0x1

    goto :goto_1

    :cond_1
    move-object/from16 v20, v12

    mul-int/lit8 v1, v10, 0x3

    mul-int v1, v1, v8

    .line 912
    new-array v11, v1, [B

    const/4 v12, 0x0

    const/16 v21, 0x0

    :goto_2
    if-ge v12, v8, :cond_5

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/16 v27, 0x1

    move-object/from16 v1, v16

    move-object v2, v7

    move v4, v10

    move-object/from16 v28, v6

    move v6, v12

    move-object/from16 v29, v7

    move v7, v10

    move v9, v8

    move/from16 v8, v27

    .line 915
    invoke-virtual/range {v1 .. v8}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_3
    if-ge v1, v10, :cond_4

    .line 919
    aget v3, v29, v2

    shr-int/lit8 v3, v3, 0x18

    const/16 v4, 0xff

    and-int/2addr v3, v4

    .line 920
    aget v5, v29, v2

    shr-int/lit8 v5, v5, 0x10

    and-int/2addr v5, v4

    .line 921
    aget v6, v29, v2

    shr-int/lit8 v6, v6, 0x8

    and-int/2addr v6, v4

    .line 922
    aget v7, v29, v2

    and-int/2addr v7, v4

    if-ne v4, v3, :cond_2

    int-to-byte v3, v7

    .line 925
    aput-byte v3, v11, v21

    add-int/lit8 v3, v21, 0x1

    int-to-byte v4, v6

    .line 926
    aput-byte v4, v11, v3

    add-int/lit8 v3, v21, 0x2

    int-to-byte v4, v5

    .line 927
    aput-byte v4, v11, v3

    goto :goto_4

    :cond_2
    if-nez v3, :cond_3

    const/4 v3, -0x1

    .line 931
    aput-byte v3, v11, v21

    add-int/lit8 v4, v21, 0x1

    .line 932
    aput-byte v3, v11, v4

    add-int/lit8 v4, v21, 0x2

    .line 933
    aput-byte v3, v11, v4

    goto :goto_4

    :cond_3
    int-to-double v3, v3

    .line 937
    aget-wide v7, v28, v7

    mul-double v7, v7, v3

    sub-double v7, v17, v7

    double-to-int v7, v7

    int-to-byte v7, v7

    aput-byte v7, v11, v21

    add-int/lit8 v7, v21, 0x1

    .line 938
    aget-wide v30, v28, v6

    mul-double v30, v30, v3

    sub-double v13, v17, v30

    double-to-int v6, v13

    int-to-byte v6, v6

    aput-byte v6, v11, v7

    add-int/lit8 v6, v21, 0x2

    .line 939
    aget-wide v7, v28, v5

    mul-double v3, v3, v7

    sub-double v3, v17, v3

    double-to-int v3, v3

    int-to-byte v3, v3

    aput-byte v3, v11, v6

    :goto_4
    add-int/lit8 v21, v21, 0x3

    add-int/lit8 v2, v2, 0x1

    add-int/lit8 v1, v1, 0x1

    const/4 v13, 0x7

    const/4 v14, 0x0

    goto :goto_3

    :cond_4
    add-int/lit8 v12, v12, 0x1

    move v8, v9

    move-object/from16 v6, v28

    move-object/from16 v7, v29

    const/4 v13, 0x7

    const/4 v14, 0x0

    goto/16 :goto_2

    :cond_5
    move v9, v8

    .line 947
    iget-wide v2, v15, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    int-to-long v5, v10

    int-to-long v7, v9

    move/from16 v13, p2

    int-to-long v9, v13

    move/from16 v14, p3

    int-to-long v12, v14

    move/from16 v4, p4

    move-wide/from16 v16, v12

    int-to-long v12, v4

    move/from16 v1, p5

    move-wide/from16 v28, v12

    int-to-long v13, v1

    iget v12, v15, Lcom/epson/eposdevice/printer/Printer;->mHalftone:I

    move-wide/from16 v30, v13

    iget-wide v13, v15, Lcom/epson/eposdevice/printer/Printer;->mBrightness:D
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_2

    move-object/from16 v1, p0

    move-object v4, v11

    const/16 v11, 0xc

    move-object/from16 v32, v20

    move-wide/from16 v18, v28

    move/from16 v20, v12

    move-wide/from16 v11, v16

    move-wide/from16 v27, v13

    move-wide/from16 v16, v30

    const/4 v0, 0x7

    const/16 v29, 0x0

    move-wide/from16 v13, v18

    move-wide/from16 v15, v16

    move/from16 v17, p6

    move/from16 v18, p7

    move/from16 v19, v20

    move-wide/from16 v20, v27

    :try_start_2
    invoke-virtual/range {v1 .. v21}, Lcom/epson/eposdevice/printer/Printer;->nativeAddImage(J[BJJJJJJIIID)I

    move-result v1
    :try_end_2
    .catch Lcom/epson/eposdevice/EposException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_0

    if-nez v1, :cond_6

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v29

    .line 963
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v22

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v23

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v24

    invoke-static/range {p6 .. p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v25

    invoke-static/range {p7 .. p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v26

    move-object/from16 v3, p0

    move-object/from16 v4, v32

    invoke-virtual {v3, v4, v0}, Lcom/epson/eposdevice/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    :cond_6
    move-object/from16 v3, p0

    move-object/from16 v4, v32

    .line 949
    :try_start_3
    new-instance v0, Lcom/epson/eposdevice/EposException;

    invoke-direct {v0, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw v0

    :catch_0
    move-object/from16 v3, p0

    move-object/from16 v4, v32

    goto :goto_5

    :catch_1
    move-exception v0

    move-object/from16 v3, p0

    move-object/from16 v4, v32

    goto :goto_6

    :catch_2
    move-object v3, v15

    move-object/from16 v4, v20

    goto :goto_5

    :catch_3
    move-exception v0

    move-object v3, v15

    move-object/from16 v4, v20

    goto :goto_6

    :cond_7
    move-object v4, v12

    move-object v3, v15

    .line 903
    new-instance v0, Lcom/epson/eposdevice/EposException;
    :try_end_3
    .catch Lcom/epson/eposdevice/EposException; {:try_start_3 .. :try_end_3} :catch_4
    .catch Ljava/lang/OutOfMemoryError; {:try_start_3 .. :try_end_3} :catch_6

    const/16 v1, 0xc

    :try_start_4
    invoke-direct {v0, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw v0

    :cond_8
    move-object v4, v12

    move-object v3, v15

    const/16 v1, 0xc

    const/4 v2, 0x1

    .line 891
    new-instance v0, Lcom/epson/eposdevice/EposException;

    invoke-direct {v0, v2}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw v0

    :cond_9
    move-object v4, v12

    move-object v3, v15

    const/16 v1, 0xc

    const/4 v2, 0x1

    .line 867
    new-instance v0, Lcom/epson/eposdevice/EposException;

    invoke-direct {v0, v2}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw v0
    :try_end_4
    .catch Lcom/epson/eposdevice/EposException; {:try_start_4 .. :try_end_4} :catch_4
    .catch Ljava/lang/OutOfMemoryError; {:try_start_4 .. :try_end_4} :catch_7

    :catch_4
    move-exception v0

    goto :goto_6

    :catch_5
    move-object v4, v12

    move-object v3, v15

    :catch_6
    :goto_5
    const/16 v1, 0xc

    .line 958
    :catch_7
    new-instance v0, Lcom/epson/eposdevice/EposException;

    invoke-direct {v0, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    .line 959
    invoke-virtual {v3, v4, v0}, Lcom/epson/eposdevice/printer/Printer;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 960
    throw v0

    :catch_8
    move-exception v0

    move-object v4, v12

    move-object v3, v15

    .line 953
    :goto_6
    invoke-virtual {v3, v4, v0}, Lcom/epson/eposdevice/printer/Printer;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 954
    throw v0
.end method

.method public addLayout(IIIIIII)V
    .locals 29
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    move-object/from16 v15, p0

    const/4 v0, 0x7

    new-array v1, v0, [Ljava/lang/Object;

    .line 1264
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v17, 0x0

    aput-object v2, v1, v17

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v18, 0x1

    aput-object v2, v1, v18

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v19, 0x2

    aput-object v2, v1, v19

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v20, 0x3

    aput-object v2, v1, v20

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v21, 0x4

    aput-object v2, v1, v21

    invoke-static/range {p6 .. p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v22, 0x5

    aput-object v2, v1, v22

    invoke-static/range {p7 .. p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v23, 0x6

    aput-object v2, v1, v23

    const-string v13, "addLayout"

    invoke-virtual {v15, v13, v1}, Lcom/epson/eposdevice/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1266
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/epson/eposdevice/printer/Printer;->checkHandle()V

    .line 1267
    iget-wide v2, v15, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_2

    move/from16 v14, p2

    int-to-long v5, v14

    move/from16 v11, p3

    int-to-long v7, v11

    move/from16 v12, p4

    int-to-long v9, v12

    move/from16 v4, p5

    int-to-long v0, v4

    move-wide/from16 v24, v0

    move-object/from16 v16, v13

    move/from16 v0, p6

    int-to-long v13, v0

    move/from16 v1, p7

    move-wide/from16 v26, v13

    int-to-long v13, v1

    move-object/from16 v1, p0

    move/from16 v4, p1

    move-wide/from16 v11, v24

    move-wide/from16 v24, v13

    move-object/from16 v28, v16

    move-wide/from16 v13, v26

    move-wide/from16 v15, v24

    :try_start_1
    invoke-virtual/range {v1 .. v16}, Lcom/epson/eposdevice/printer/Printer;->nativeAddLayout(JIJJJJJJ)I

    move-result v1
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_1

    if-nez v1, :cond_0

    const/4 v1, 0x7

    new-array v1, v1, [Ljava/lang/Object;

    .line 1277
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v17

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v18

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v19

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v20

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v21

    invoke-static/range {p6 .. p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v1, v22

    invoke-static/range {p7 .. p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v1, v23

    move-object/from16 v2, p0

    move-object/from16 v3, v28

    invoke-virtual {v2, v3, v1}, Lcom/epson/eposdevice/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    :cond_0
    move-object/from16 v2, p0

    move-object/from16 v3, v28

    .line 1270
    :try_start_2
    new-instance v0, Lcom/epson/eposdevice/EposException;

    invoke-direct {v0, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw v0
    :try_end_2
    .catch Lcom/epson/eposdevice/EposException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    move-object/from16 v2, p0

    move-object/from16 v3, v28

    goto :goto_0

    :catch_2
    move-exception v0

    move-object v3, v13

    move-object v2, v15

    .line 1274
    :goto_0
    invoke-virtual {v2, v3, v0}, Lcom/epson/eposdevice/printer/Printer;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1275
    throw v0
.end method

.method public addLogo(II)V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/Object;

    .line 968
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v4, 0x1

    aput-object v2, v1, v4

    const-string v2, "addLogo"

    invoke-virtual {p0, v2, v1}, Lcom/epson/eposdevice/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 970
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/eposdevice/printer/Printer;->checkHandle()V

    .line 971
    iget-wide v6, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    int-to-long v8, p1

    int-to-long v10, p2

    move-object v5, p0

    invoke-virtual/range {v5 .. v11}, Lcom/epson/eposdevice/printer/Printer;->nativeAddLogo(JJJ)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 980
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v4

    invoke-virtual {p0, v2, v0}, Lcom/epson/eposdevice/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 973
    :cond_0
    :try_start_1
    new-instance p1, Lcom/epson/eposdevice/EposException;

    invoke-direct {p1, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw p1
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception p1

    .line 977
    invoke-virtual {p0, v2, p1}, Lcom/epson/eposdevice/printer/Printer;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 978
    throw p1
.end method

.method public addPageArea(IIII)V
    .locals 19
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    move-object/from16 v12, p0

    const/4 v0, 0x4

    new-array v1, v0, [Ljava/lang/Object;

    .line 1110
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v13, 0x0

    aput-object v2, v1, v13

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v14, 0x1

    aput-object v2, v1, v14

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v15, 0x2

    aput-object v2, v1, v15

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v16, 0x3

    aput-object v2, v1, v16

    const-string v10, "addPageArea"

    invoke-virtual {v12, v10, v1}, Lcom/epson/eposdevice/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1112
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/epson/eposdevice/printer/Printer;->checkHandle()V

    .line 1113
    iget-wide v2, v12, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_1

    move/from16 v11, p1

    int-to-long v4, v11

    move/from16 v8, p2

    int-to-long v6, v8

    move/from16 v9, p3

    int-to-long v14, v9

    move/from16 v1, p4

    move-wide/from16 v17, v14

    int-to-long v13, v1

    move-object/from16 v1, p0

    move-wide/from16 v8, v17

    move-object v15, v10

    move-wide v10, v13

    :try_start_1
    invoke-virtual/range {v1 .. v11}, Lcom/epson/eposdevice/printer/Printer;->nativeAddPageArea(JJJJJ)I

    move-result v1
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 1122
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x2

    aput-object v1, v0, v2

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v16

    invoke-virtual {v12, v15, v0}, Lcom/epson/eposdevice/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 1115
    :cond_0
    :try_start_2
    new-instance v0, Lcom/epson/eposdevice/EposException;

    invoke-direct {v0, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw v0
    :try_end_2
    .catch Lcom/epson/eposdevice/EposException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    move-object v15, v10

    .line 1119
    :goto_0
    invoke-virtual {v12, v15, v0}, Lcom/epson/eposdevice/printer/Printer;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1120
    throw v0
.end method

.method public addPageBegin()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "addPageBegin"

    .line 1076
    invoke-virtual {p0, v2, v1}, Lcom/epson/eposdevice/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1078
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/eposdevice/printer/Printer;->checkHandle()V

    .line 1079
    iget-wide v3, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    invoke-virtual {p0, v3, v4}, Lcom/epson/eposdevice/printer/Printer;->nativeAddPageBegin(J)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 1088
    invoke-virtual {p0, v2, v0}, Lcom/epson/eposdevice/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 1081
    :cond_0
    :try_start_1
    new-instance v0, Lcom/epson/eposdevice/EposException;

    invoke-direct {v0, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw v0
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v0

    .line 1085
    invoke-virtual {p0, v2, v0}, Lcom/epson/eposdevice/printer/Printer;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1086
    throw v0
.end method

.method public addPageDirection(I)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 1127
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "addPageDirection"

    invoke-virtual {p0, v2, v1}, Lcom/epson/eposdevice/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1129
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/eposdevice/printer/Printer;->checkHandle()V

    .line 1130
    iget-wide v4, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    invoke-virtual {p0, v4, v5, p1}, Lcom/epson/eposdevice/printer/Printer;->nativeAddPageDirection(JI)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 1139
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {p0, v2, v0}, Lcom/epson/eposdevice/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 1132
    :cond_0
    :try_start_1
    new-instance p1, Lcom/epson/eposdevice/EposException;

    invoke-direct {p1, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw p1
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception p1

    .line 1136
    invoke-virtual {p0, v2, p1}, Lcom/epson/eposdevice/printer/Printer;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1137
    throw p1
.end method

.method public addPageEnd()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "addPageEnd"

    .line 1093
    invoke-virtual {p0, v2, v1}, Lcom/epson/eposdevice/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1095
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/eposdevice/printer/Printer;->checkHandle()V

    .line 1096
    iget-wide v3, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    invoke-virtual {p0, v3, v4}, Lcom/epson/eposdevice/printer/Printer;->nativeAddPageEnd(J)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 1105
    invoke-virtual {p0, v2, v0}, Lcom/epson/eposdevice/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 1098
    :cond_0
    :try_start_1
    new-instance v0, Lcom/epson/eposdevice/EposException;

    invoke-direct {v0, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw v0
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v0

    .line 1102
    invoke-virtual {p0, v2, v0}, Lcom/epson/eposdevice/printer/Printer;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1103
    throw v0
.end method

.method public addPageLine(IIIII)V
    .locals 19
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    move-object/from16 v13, p0

    const/4 v0, 0x5

    new-array v1, v0, [Ljava/lang/Object;

    .line 1161
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v14, 0x0

    aput-object v2, v1, v14

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v15, 0x1

    aput-object v2, v1, v15

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v16, 0x2

    aput-object v2, v1, v16

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v17, 0x3

    aput-object v2, v1, v17

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v18, 0x4

    aput-object v2, v1, v18

    const-string v12, "addPageLine"

    invoke-virtual {v13, v12, v1}, Lcom/epson/eposdevice/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1163
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/epson/eposdevice/printer/Printer;->checkHandle()V

    .line 1164
    iget-wide v2, v13, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_1

    move/from16 v10, p1

    int-to-long v4, v10

    move/from16 v11, p2

    int-to-long v6, v11

    move/from16 v8, p3

    int-to-long v14, v8

    move/from16 v9, p4

    int-to-long v10, v9

    move-object/from16 v1, p0

    move-wide v8, v14

    move-object v14, v12

    move/from16 v12, p5

    :try_start_1
    invoke-virtual/range {v1 .. v12}, Lcom/epson/eposdevice/printer/Printer;->nativeAddPageLine(JJJJJI)I

    move-result v1
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 1173
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v16

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v17

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v18

    invoke-virtual {v13, v14, v0}, Lcom/epson/eposdevice/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 1166
    :cond_0
    :try_start_2
    new-instance v0, Lcom/epson/eposdevice/EposException;

    invoke-direct {v0, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw v0
    :try_end_2
    .catch Lcom/epson/eposdevice/EposException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    move-object v14, v12

    .line 1170
    :goto_0
    invoke-virtual {v13, v14, v0}, Lcom/epson/eposdevice/printer/Printer;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1171
    throw v0
.end method

.method public addPagePosition(II)V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/Object;

    .line 1144
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v4, 0x1

    aput-object v2, v1, v4

    const-string v2, "addPagePosition"

    invoke-virtual {p0, v2, v1}, Lcom/epson/eposdevice/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1146
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/eposdevice/printer/Printer;->checkHandle()V

    .line 1147
    iget-wide v6, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    int-to-long v8, p1

    int-to-long v10, p2

    move-object v5, p0

    invoke-virtual/range {v5 .. v11}, Lcom/epson/eposdevice/printer/Printer;->nativeAddPagePosition(JJJ)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 1156
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v4

    invoke-virtual {p0, v2, v0}, Lcom/epson/eposdevice/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 1149
    :cond_0
    :try_start_1
    new-instance p1, Lcom/epson/eposdevice/EposException;

    invoke-direct {p1, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw p1
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception p1

    .line 1153
    invoke-virtual {p0, v2, p1}, Lcom/epson/eposdevice/printer/Printer;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1154
    throw p1
.end method

.method public addPageRectangle(IIIII)V
    .locals 19
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    move-object/from16 v13, p0

    const/4 v0, 0x5

    new-array v1, v0, [Ljava/lang/Object;

    .line 1178
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v14, 0x0

    aput-object v2, v1, v14

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v15, 0x1

    aput-object v2, v1, v15

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v16, 0x2

    aput-object v2, v1, v16

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v17, 0x3

    aput-object v2, v1, v17

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v18, 0x4

    aput-object v2, v1, v18

    const-string v12, "addPageRectangle"

    invoke-virtual {v13, v12, v1}, Lcom/epson/eposdevice/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1180
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/epson/eposdevice/printer/Printer;->checkHandle()V

    .line 1181
    iget-wide v2, v13, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_1

    move/from16 v10, p1

    int-to-long v4, v10

    move/from16 v11, p2

    int-to-long v6, v11

    move/from16 v8, p3

    int-to-long v14, v8

    move/from16 v9, p4

    int-to-long v10, v9

    move-object/from16 v1, p0

    move-wide v8, v14

    move-object v14, v12

    move/from16 v12, p5

    :try_start_1
    invoke-virtual/range {v1 .. v12}, Lcom/epson/eposdevice/printer/Printer;->nativeAddPageRectangle(JJJJJI)I

    move-result v1
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 1190
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v16

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v17

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v18

    invoke-virtual {v13, v14, v0}, Lcom/epson/eposdevice/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 1183
    :cond_0
    :try_start_2
    new-instance v0, Lcom/epson/eposdevice/EposException;

    invoke-direct {v0, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw v0
    :try_end_2
    .catch Lcom/epson/eposdevice/EposException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    move-object v14, v12

    .line 1187
    :goto_0
    invoke-virtual {v13, v14, v0}, Lcom/epson/eposdevice/printer/Printer;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1188
    throw v0
.end method

.method public addPulse(II)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/Object;

    .line 1212
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v4, 0x1

    aput-object v2, v1, v4

    const-string v2, "addPulse"

    invoke-virtual {p0, v2, v1}, Lcom/epson/eposdevice/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1214
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/eposdevice/printer/Printer;->checkHandle()V

    .line 1215
    iget-wide v5, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    invoke-virtual {p0, v5, v6, p1, p2}, Lcom/epson/eposdevice/printer/Printer;->nativeAddPulse(JII)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 1224
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v4

    invoke-virtual {p0, v2, v0}, Lcom/epson/eposdevice/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 1217
    :cond_0
    :try_start_1
    new-instance p1, Lcom/epson/eposdevice/EposException;

    invoke-direct {p1, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw p1
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception p1

    .line 1221
    invoke-virtual {p0, v2, p1}, Lcom/epson/eposdevice/printer/Printer;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1222
    throw p1
.end method

.method public addRecovery()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "addRecovery"

    .line 1302
    invoke-virtual {p0, v2, v1}, Lcom/epson/eposdevice/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1304
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/eposdevice/printer/Printer;->checkHandle()V

    .line 1305
    iget-wide v3, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    invoke-virtual {p0, v3, v4}, Lcom/epson/eposdevice/printer/Printer;->nativeAddRecovery(J)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 1314
    invoke-virtual {p0, v2, v0}, Lcom/epson/eposdevice/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 1307
    :cond_0
    :try_start_1
    new-instance v0, Lcom/epson/eposdevice/EposException;

    invoke-direct {v0, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw v0
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v0

    .line 1311
    invoke-virtual {p0, v2, v0}, Lcom/epson/eposdevice/printer/Printer;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1312
    throw v0
.end method

.method public addReset()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "addReset"

    .line 1319
    invoke-virtual {p0, v2, v1}, Lcom/epson/eposdevice/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1321
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/eposdevice/printer/Printer;->checkHandle()V

    .line 1322
    iget-wide v3, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    invoke-virtual {p0, v3, v4}, Lcom/epson/eposdevice/printer/Printer;->nativeAddReset(J)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 1331
    invoke-virtual {p0, v2, v0}, Lcom/epson/eposdevice/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 1324
    :cond_0
    :try_start_1
    new-instance v0, Lcom/epson/eposdevice/EposException;

    invoke-direct {v0, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw v0
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v0

    .line 1328
    invoke-virtual {p0, v2, v0}, Lcom/epson/eposdevice/printer/Printer;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1329
    throw v0
.end method

.method public addSound(III)V
    .locals 16
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    move-object/from16 v9, p0

    const/4 v0, 0x3

    new-array v1, v0, [Ljava/lang/Object;

    .line 1229
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v10, 0x0

    aput-object v2, v1, v10

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v11, 0x1

    aput-object v2, v1, v11

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v12, 0x2

    aput-object v2, v1, v12

    const-string v13, "addSound"

    invoke-virtual {v9, v13, v1}, Lcom/epson/eposdevice/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1231
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/epson/eposdevice/printer/Printer;->checkHandle()V

    .line 1232
    iget-wide v2, v9, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    move/from16 v14, p2

    int-to-long v5, v14

    move/from16 v15, p3

    int-to-long v7, v15

    move-object/from16 v1, p0

    move/from16 v4, p1

    invoke-virtual/range {v1 .. v8}, Lcom/epson/eposdevice/printer/Printer;->nativeAddSound(JIJJ)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 1241
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v10

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v11

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v12

    invoke-virtual {v9, v13, v0}, Lcom/epson/eposdevice/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 1234
    :cond_0
    :try_start_1
    new-instance v0, Lcom/epson/eposdevice/EposException;

    invoke-direct {v0, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw v0
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v0

    .line 1238
    invoke-virtual {v9, v13, v0}, Lcom/epson/eposdevice/printer/Printer;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1239
    throw v0
.end method

.method public addSymbol(Ljava/lang/String;IIIII)V
    .locals 22
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    move-object/from16 v13, p0

    const/4 v0, 0x6

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object p1, v1, v14

    .line 1005
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v15, 0x1

    aput-object v2, v1, v15

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v16, 0x2

    aput-object v2, v1, v16

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v17, 0x3

    aput-object v2, v1, v17

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v18, 0x4

    aput-object v2, v1, v18

    invoke-static/range {p6 .. p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/16 v19, 0x5

    aput-object v2, v1, v19

    const-string v11, "addSymbol"

    invoke-virtual {v13, v11, v1}, Lcom/epson/eposdevice/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1007
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/epson/eposdevice/printer/Printer;->checkHandle()V

    if-eqz p1, :cond_1

    .line 1011
    iget-wide v2, v13, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_2

    move/from16 v12, p4

    int-to-long v7, v12

    move/from16 v9, p5

    int-to-long v5, v9

    move/from16 v10, p6

    int-to-long v14, v10

    move-object/from16 v1, p0

    move-object/from16 v4, p1

    move-wide/from16 v20, v5

    move/from16 v5, p2

    move/from16 v6, p3

    move-wide/from16 v9, v20

    move-object v13, v11

    move-wide v11, v14

    :try_start_1
    invoke-virtual/range {v1 .. v12}, Lcom/epson/eposdevice/printer/Printer;->nativeAddSymbol(JLjava/lang/String;IIJJJ)I

    move-result v1
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    .line 1020
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v16

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v17

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v18

    invoke-static/range {p6 .. p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v19

    move-object/from16 v2, p0

    move-object v3, v13

    invoke-virtual {v2, v3, v0}, Lcom/epson/eposdevice/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    :cond_0
    move-object/from16 v2, p0

    move-object v3, v13

    .line 1013
    :try_start_2
    new-instance v0, Lcom/epson/eposdevice/EposException;

    invoke-direct {v0, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw v0

    :catch_0
    move-exception v0

    move-object/from16 v2, p0

    move-object v3, v13

    goto :goto_0

    :cond_1
    move-object v3, v11

    move-object v2, v13

    .line 1009
    new-instance v0, Lcom/epson/eposdevice/EposException;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw v0
    :try_end_2
    .catch Lcom/epson/eposdevice/EposException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception v0

    goto :goto_0

    :catch_2
    move-exception v0

    move-object v3, v11

    move-object v2, v13

    .line 1017
    :goto_0
    invoke-virtual {v2, v3, v0}, Lcom/epson/eposdevice/printer/Printer;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1018
    throw v0
.end method

.method public addText(Ljava/lang/String;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const-string v3, "addText"

    .line 656
    invoke-virtual {p0, v3, v1}, Lcom/epson/eposdevice/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 658
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/eposdevice/printer/Printer;->checkHandle()V

    if-eqz p1, :cond_1

    .line 662
    iget-wide v4, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    invoke-virtual {p0, v4, v5, p1}, Lcom/epson/eposdevice/printer/Printer;->nativeAddText(JLjava/lang/String;)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    .line 671
    invoke-virtual {p0, v3, v0}, Lcom/epson/eposdevice/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 664
    :cond_0
    :try_start_1
    new-instance p1, Lcom/epson/eposdevice/EposException;

    invoke-direct {p1, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw p1

    .line 660
    :cond_1
    new-instance p1, Lcom/epson/eposdevice/EposException;

    invoke-direct {p1, v0}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw p1
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception p1

    .line 668
    invoke-virtual {p0, v3, p1}, Lcom/epson/eposdevice/printer/Printer;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 669
    throw p1
.end method

.method public addTextAlign(I)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 605
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "addTextAlign"

    invoke-virtual {p0, v2, v1}, Lcom/epson/eposdevice/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 607
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/eposdevice/printer/Printer;->checkHandle()V

    .line 608
    iget-wide v4, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    invoke-virtual {p0, v4, v5, p1}, Lcom/epson/eposdevice/printer/Printer;->nativeAddTextAlign(JI)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 617
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {p0, v2, v0}, Lcom/epson/eposdevice/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 610
    :cond_0
    :try_start_1
    new-instance p1, Lcom/epson/eposdevice/EposException;

    invoke-direct {p1, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw p1
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception p1

    .line 614
    invoke-virtual {p0, v2, p1}, Lcom/epson/eposdevice/printer/Printer;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 615
    throw p1
.end method

.method public addTextDouble(II)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/Object;

    .line 727
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v4, 0x1

    aput-object v2, v1, v4

    const-string v2, "addTextDouble"

    invoke-virtual {p0, v2, v1}, Lcom/epson/eposdevice/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 729
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/eposdevice/printer/Printer;->checkHandle()V

    .line 730
    iget-wide v5, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    invoke-virtual {p0, v5, v6, p1, p2}, Lcom/epson/eposdevice/printer/Printer;->nativeAddTextDouble(JII)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 739
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v4

    invoke-virtual {p0, v2, v0}, Lcom/epson/eposdevice/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 732
    :cond_0
    :try_start_1
    new-instance p1, Lcom/epson/eposdevice/EposException;

    invoke-direct {p1, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw p1
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception p1

    .line 736
    invoke-virtual {p0, v2, p1}, Lcom/epson/eposdevice/printer/Printer;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 737
    throw p1
.end method

.method public addTextFont(I)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 693
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "addTextFont"

    invoke-virtual {p0, v2, v1}, Lcom/epson/eposdevice/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 695
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/eposdevice/printer/Printer;->checkHandle()V

    .line 696
    iget-wide v4, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    invoke-virtual {p0, v4, v5, p1}, Lcom/epson/eposdevice/printer/Printer;->nativeAddTextFont(JI)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 705
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {p0, v2, v0}, Lcom/epson/eposdevice/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 698
    :cond_0
    :try_start_1
    new-instance p1, Lcom/epson/eposdevice/EposException;

    invoke-direct {p1, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw p1
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception p1

    .line 702
    invoke-virtual {p0, v2, p1}, Lcom/epson/eposdevice/printer/Printer;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 703
    throw p1
.end method

.method public addTextLang(I)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 676
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "addTextLang"

    invoke-virtual {p0, v2, v1}, Lcom/epson/eposdevice/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 678
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/eposdevice/printer/Printer;->checkHandle()V

    .line 679
    iget-wide v4, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    invoke-virtual {p0, v4, v5, p1}, Lcom/epson/eposdevice/printer/Printer;->nativeAddTextLang(JI)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 688
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {p0, v2, v0}, Lcom/epson/eposdevice/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 681
    :cond_0
    :try_start_1
    new-instance p1, Lcom/epson/eposdevice/EposException;

    invoke-direct {p1, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw p1
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception p1

    .line 685
    invoke-virtual {p0, v2, p1}, Lcom/epson/eposdevice/printer/Printer;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 686
    throw p1
.end method

.method public addTextLineSpace(I)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 622
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "addTextLineSpace"

    invoke-virtual {p0, v2, v1}, Lcom/epson/eposdevice/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 624
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/eposdevice/printer/Printer;->checkHandle()V

    .line 625
    iget-wide v4, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    int-to-long v6, p1

    invoke-virtual {p0, v4, v5, v6, v7}, Lcom/epson/eposdevice/printer/Printer;->nativeAddTextLineSpace(JJ)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 634
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {p0, v2, v0}, Lcom/epson/eposdevice/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 627
    :cond_0
    :try_start_1
    new-instance p1, Lcom/epson/eposdevice/EposException;

    invoke-direct {p1, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw p1
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception p1

    .line 631
    invoke-virtual {p0, v2, p1}, Lcom/epson/eposdevice/printer/Printer;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 632
    throw p1
.end method

.method public addTextPosition(I)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 778
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "addTextPosition"

    invoke-virtual {p0, v2, v1}, Lcom/epson/eposdevice/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 780
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/eposdevice/printer/Printer;->checkHandle()V

    .line 781
    iget-wide v4, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    int-to-long v6, p1

    invoke-virtual {p0, v4, v5, v6, v7}, Lcom/epson/eposdevice/printer/Printer;->nativeAddTextPosition(JJ)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 790
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {p0, v2, v0}, Lcom/epson/eposdevice/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 783
    :cond_0
    :try_start_1
    new-instance p1, Lcom/epson/eposdevice/EposException;

    invoke-direct {p1, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw p1
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception p1

    .line 787
    invoke-virtual {p0, v2, p1}, Lcom/epson/eposdevice/printer/Printer;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 788
    throw p1
.end method

.method public addTextRotate(I)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 639
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "addTextRotate"

    invoke-virtual {p0, v2, v1}, Lcom/epson/eposdevice/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 641
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/eposdevice/printer/Printer;->checkHandle()V

    .line 642
    iget-wide v4, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    invoke-virtual {p0, v4, v5, p1}, Lcom/epson/eposdevice/printer/Printer;->nativeAddTextRotate(JI)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 651
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {p0, v2, v0}, Lcom/epson/eposdevice/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 644
    :cond_0
    :try_start_1
    new-instance p1, Lcom/epson/eposdevice/EposException;

    invoke-direct {p1, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw p1
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception p1

    .line 648
    invoke-virtual {p0, v2, p1}, Lcom/epson/eposdevice/printer/Printer;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 649
    throw p1
.end method

.method public addTextSize(II)V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/Object;

    .line 744
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v4, 0x1

    aput-object v2, v1, v4

    const-string v2, "addTextSize"

    invoke-virtual {p0, v2, v1}, Lcom/epson/eposdevice/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 746
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/eposdevice/printer/Printer;->checkHandle()V

    .line 747
    iget-wide v6, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    int-to-long v8, p1

    int-to-long v10, p2

    move-object v5, p0

    invoke-virtual/range {v5 .. v11}, Lcom/epson/eposdevice/printer/Printer;->nativeAddTextSize(JJJ)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 756
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v4

    invoke-virtual {p0, v2, v0}, Lcom/epson/eposdevice/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 749
    :cond_0
    :try_start_1
    new-instance p1, Lcom/epson/eposdevice/EposException;

    invoke-direct {p1, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw p1
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception p1

    .line 753
    invoke-virtual {p0, v2, p1}, Lcom/epson/eposdevice/printer/Printer;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 754
    throw p1
.end method

.method public addTextSmooth(I)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 710
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "addTextSmooth"

    invoke-virtual {p0, v2, v1}, Lcom/epson/eposdevice/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 712
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/eposdevice/printer/Printer;->checkHandle()V

    .line 713
    iget-wide v4, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    invoke-virtual {p0, v4, v5, p1}, Lcom/epson/eposdevice/printer/Printer;->nativeAddTextSmooth(JI)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 722
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {p0, v2, v0}, Lcom/epson/eposdevice/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 715
    :cond_0
    :try_start_1
    new-instance p1, Lcom/epson/eposdevice/EposException;

    invoke-direct {p1, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw p1
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception p1

    .line 719
    invoke-virtual {p0, v2, p1}, Lcom/epson/eposdevice/printer/Printer;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 720
    throw p1
.end method

.method public addTextStyle(IIII)V
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    move-object v8, p0

    const/4 v0, 0x4

    new-array v1, v0, [Ljava/lang/Object;

    .line 761
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v9, 0x0

    aput-object v2, v1, v9

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v10, 0x1

    aput-object v2, v1, v10

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v11, 0x2

    aput-object v2, v1, v11

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v12, 0x3

    aput-object v2, v1, v12

    const-string v13, "addTextStyle"

    invoke-virtual {p0, v13, v1}, Lcom/epson/eposdevice/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 763
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/eposdevice/printer/Printer;->checkHandle()V

    .line 764
    iget-wide v2, v8, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    move-object v1, p0

    move v4, p1

    move/from16 v5, p2

    move/from16 v6, p3

    move/from16 v7, p4

    invoke-virtual/range {v1 .. v7}, Lcom/epson/eposdevice/printer/Printer;->nativeAddTextStyle(JIIII)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 773
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v9

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v10

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v11

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v12

    invoke-virtual {p0, v13, v0}, Lcom/epson/eposdevice/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 766
    :cond_0
    :try_start_1
    new-instance v0, Lcom/epson/eposdevice/EposException;

    invoke-direct {v0, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw v0
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v0

    .line 770
    invoke-virtual {p0, v13, v0}, Lcom/epson/eposdevice/printer/Printer;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 771
    throw v0
.end method

.method public addTextVPosition(I)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 795
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "addTextVPosition"

    invoke-virtual {p0, v2, v1}, Lcom/epson/eposdevice/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 797
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/eposdevice/printer/Printer;->checkHandle()V

    .line 798
    iget-wide v4, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    int-to-long v6, p1

    invoke-virtual {p0, v4, v5, v6, v7}, Lcom/epson/eposdevice/printer/Printer;->nativeAddTextVPosition(JJ)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 807
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {p0, v2, v0}, Lcom/epson/eposdevice/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 800
    :cond_0
    :try_start_1
    new-instance p1, Lcom/epson/eposdevice/EposException;

    invoke-direct {p1, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw p1
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception p1

    .line 804
    invoke-virtual {p0, v2, p1}, Lcom/epson/eposdevice/printer/Printer;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 805
    throw p1
.end method

.method public addVLineBegin(II)V
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/Object;

    .line 1042
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v4, 0x1

    aput-object v2, v1, v4

    const-string v2, "addVLineBegin"

    invoke-virtual {p0, v2, v1}, Lcom/epson/eposdevice/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1044
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/eposdevice/printer/Printer;->checkHandle()V

    .line 1045
    iget-wide v6, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    int-to-long v8, p1

    move-object v5, p0

    move v10, p2

    invoke-virtual/range {v5 .. v10}, Lcom/epson/eposdevice/printer/Printer;->nativeAddVLineBegin(JJI)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 1054
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v4

    invoke-virtual {p0, v2, v0}, Lcom/epson/eposdevice/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 1047
    :cond_0
    :try_start_1
    new-instance p1, Lcom/epson/eposdevice/EposException;

    invoke-direct {p1, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw p1
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception p1

    .line 1051
    invoke-virtual {p0, v2, p1}, Lcom/epson/eposdevice/printer/Printer;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1052
    throw p1
.end method

.method public addVLineEnd(II)V
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/Object;

    .line 1059
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v4, 0x1

    aput-object v2, v1, v4

    const-string v2, "addVLineEnd"

    invoke-virtual {p0, v2, v1}, Lcom/epson/eposdevice/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1061
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/eposdevice/printer/Printer;->checkHandle()V

    .line 1062
    iget-wide v6, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    int-to-long v8, p1

    move-object v5, p0

    move v10, p2

    invoke-virtual/range {v5 .. v10}, Lcom/epson/eposdevice/printer/Printer;->nativeAddVLineEnd(JJI)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 1071
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v4

    invoke-virtual {p0, v2, v0}, Lcom/epson/eposdevice/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 1064
    :cond_0
    :try_start_1
    new-instance p1, Lcom/epson/eposdevice/EposException;

    invoke-direct {p1, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw p1
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception p1

    .line 1068
    invoke-virtual {p0, v2, p1}, Lcom/epson/eposdevice/printer/Printer;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1069
    throw p1
.end method

.method protected checkHandle()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    .line 237
    iget-wide v0, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-eqz v4, :cond_0

    return-void

    .line 238
    :cond_0
    new-instance v0, Lcom/epson/eposdevice/EposException;

    const/16 v1, 0xff

    invoke-direct {v0, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw v0
.end method

.method protected checkPrintJobIdFormat(Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    const/4 v0, 0x1

    if-eqz p1, :cond_2

    .line 247
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-lt v1, v0, :cond_1

    const/16 v1, 0x1e

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-lt v1, v2, :cond_1

    const-string v1, ".*[^-._0-9A-Za-z].*"

    .line 250
    invoke-virtual {p1, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 251
    :cond_0
    new-instance p1, Lcom/epson/eposdevice/EposException;

    invoke-direct {p1, v0}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw p1

    .line 248
    :cond_1
    new-instance p1, Lcom/epson/eposdevice/EposException;

    invoke-direct {p1, v0}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw p1

    .line 245
    :cond_2
    new-instance p1, Lcom/epson/eposdevice/EposException;

    invoke-direct {p1, v0}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw p1
.end method

.method public clearCommandBuffer()V
    .locals 7

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "clearCommandBuffer"

    .line 596
    invoke-virtual {p0, v2, v1}, Lcom/epson/eposdevice/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 597
    iget-wide v3, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    const-wide/16 v5, 0x0

    cmp-long v1, v3, v5

    if-eqz v1, :cond_0

    .line 598
    invoke-virtual {p0, v3, v4}, Lcom/epson/eposdevice/printer/Printer;->nativeClearCommandBuffer(J)I

    :cond_0
    new-array v0, v0, [Ljava/lang/Object;

    .line 600
    invoke-virtual {p0, v2, v0}, Lcom/epson/eposdevice/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public getBrightness()D
    .locals 2

    .line 425
    iget-wide v0, p0, Lcom/epson/eposdevice/printer/Printer;->mBrightness:D

    return-wide v0
.end method

.method public getDrawerOpenLevel()I
    .locals 1

    .line 501
    iget v0, p0, Lcom/epson/eposdevice/printer/Printer;->mDrawerOpenLevel:I

    return v0
.end method

.method public getForce()Z
    .locals 1

    .line 435
    iget-boolean v0, p0, Lcom/epson/eposdevice/printer/Printer;->mforce:Z

    return v0
.end method

.method public getHalftone()I
    .locals 1

    .line 405
    iget v0, p0, Lcom/epson/eposdevice/printer/Printer;->mHalftone:I

    return v0
.end method

.method protected getInnerHandle()J
    .locals 2

    .line 215
    iget-wide v0, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    return-wide v0
.end method

.method public getInterval()I
    .locals 1

    .line 474
    iget v0, p0, Lcom/epson/eposdevice/printer/Printer;->mInterval:I

    return v0
.end method

.method public getPrintJobStatus(Ljava/lang/String;)V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "getPrintJobStatus"

    .line 576
    invoke-virtual {p0, v2, v1}, Lcom/epson/eposdevice/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 578
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/eposdevice/printer/Printer;->checkHandle()V

    .line 579
    invoke-virtual {p0, p1}, Lcom/epson/eposdevice/printer/Printer;->checkPrintJobIdFormat(Ljava/lang/String;)V

    .line 580
    iget-wide v4, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    iget v6, p0, Lcom/epson/eposdevice/printer/Printer;->mTimeout:I

    iget-boolean v8, p0, Lcom/epson/eposdevice/printer/Printer;->mforce:Z

    const/4 v9, 0x1

    move-object v3, p0

    move-object v7, p1

    invoke-virtual/range {v3 .. v9}, Lcom/epson/eposdevice/printer/Printer;->nativeSendData(JILjava/lang/String;ZZ)I

    move-result p1

    .line 581
    iput-boolean v0, p0, Lcom/epson/eposdevice/printer/Printer;->mforce:Z
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez p1, :cond_0

    new-array p1, v0, [Ljava/lang/Object;

    .line 590
    invoke-virtual {p0, v2, p1}, Lcom/epson/eposdevice/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 583
    :cond_0
    :try_start_1
    new-instance v0, Lcom/epson/eposdevice/EposException;

    invoke-direct {v0, p1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw v0
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception p1

    .line 587
    invoke-virtual {p0, v2, p1}, Lcom/epson/eposdevice/printer/Printer;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 588
    throw p1
.end method

.method public getTimeout()I
    .locals 1

    .line 454
    iget v0, p0, Lcom/epson/eposdevice/printer/Printer;->mTimeout:I

    return v0
.end method

.method protected innerDeleteInstance()V
    .locals 3

    .line 218
    iget-wide v0, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/epson/eposdevice/printer/Printer;->nativeSetPtrStatusChangeEventCallback(JLcom/epson/eposdevice/printer/NativePrinter;)I

    .line 219
    iget-wide v0, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    invoke-virtual {p0, v0, v1, v2}, Lcom/epson/eposdevice/printer/Printer;->nativeSetPtrOnlineEventCallback(JLcom/epson/eposdevice/printer/NativePrinter;)I

    .line 220
    iget-wide v0, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    invoke-virtual {p0, v0, v1, v2}, Lcom/epson/eposdevice/printer/Printer;->nativeSetPtrOfflineEventCallback(JLcom/epson/eposdevice/printer/NativePrinter;)I

    .line 221
    iget-wide v0, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    invoke-virtual {p0, v0, v1, v2}, Lcom/epson/eposdevice/printer/Printer;->nativeSetPtrPowerOffEventCallback(JLcom/epson/eposdevice/printer/NativePrinter;)I

    .line 222
    iget-wide v0, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    invoke-virtual {p0, v0, v1, v2}, Lcom/epson/eposdevice/printer/Printer;->nativeSetPtrCoverOkEventCallback(JLcom/epson/eposdevice/printer/NativePrinter;)I

    .line 223
    iget-wide v0, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    invoke-virtual {p0, v0, v1, v2}, Lcom/epson/eposdevice/printer/Printer;->nativeSetPtrCoverOpenEventCallback(JLcom/epson/eposdevice/printer/NativePrinter;)I

    .line 224
    iget-wide v0, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    invoke-virtual {p0, v0, v1, v2}, Lcom/epson/eposdevice/printer/Printer;->nativeSetPtrPaperOkEventCallback(JLcom/epson/eposdevice/printer/NativePrinter;)I

    .line 225
    iget-wide v0, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    invoke-virtual {p0, v0, v1, v2}, Lcom/epson/eposdevice/printer/Printer;->nativeSetPtrPaperNearEndEventCallback(JLcom/epson/eposdevice/printer/NativePrinter;)I

    .line 226
    iget-wide v0, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    invoke-virtual {p0, v0, v1, v2}, Lcom/epson/eposdevice/printer/Printer;->nativeSetPtrPaperEndEventCallback(JLcom/epson/eposdevice/printer/NativePrinter;)I

    .line 227
    iget-wide v0, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    invoke-virtual {p0, v0, v1, v2}, Lcom/epson/eposdevice/printer/Printer;->nativeSetPtrDrawerClosedEventCallback(JLcom/epson/eposdevice/printer/NativePrinter;)I

    .line 228
    iget-wide v0, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    invoke-virtual {p0, v0, v1, v2}, Lcom/epson/eposdevice/printer/Printer;->nativeSetPtrDrawerOpenEventCallback(JLcom/epson/eposdevice/printer/NativePrinter;)I

    .line 229
    iget-wide v0, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    invoke-virtual {p0, v0, v1, v2}, Lcom/epson/eposdevice/printer/Printer;->nativeSetPtrBatteryLowEventCallback(JLcom/epson/eposdevice/printer/NativePrinter;)I

    .line 230
    iget-wide v0, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    invoke-virtual {p0, v0, v1, v2}, Lcom/epson/eposdevice/printer/Printer;->nativeSetPtrBatteryOkEventCallback(JLcom/epson/eposdevice/printer/NativePrinter;)I

    .line 231
    iget-wide v0, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    invoke-virtual {p0, v0, v1, v2}, Lcom/epson/eposdevice/printer/Printer;->nativeSetPtrBatteryStatusChangeEventCallback(JLcom/epson/eposdevice/printer/NativePrinter;)I

    .line 232
    iget-wide v0, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    invoke-virtual {p0, v0, v1, v2}, Lcom/epson/eposdevice/printer/Printer;->nativeSetPtrReceiveEventCallback(JLcom/epson/eposdevice/printer/NativePrinter;)I

    const-wide/16 v0, 0x0

    .line 233
    iput-wide v0, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    return-void
.end method

.method protected nativeOnPtrBatteryLow(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1590
    invoke-direct {p0, p1, p2}, Lcom/epson/eposdevice/printer/Printer;->onPtrBatteryLow(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected nativeOnPtrBatteryOk(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1594
    invoke-direct {p0, p1, p2}, Lcom/epson/eposdevice/printer/Printer;->onPtrBatteryOk(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected nativeOnPtrBatteryStatusChange(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0

    .line 1599
    invoke-direct {p0, p1, p2, p3}, Lcom/epson/eposdevice/printer/Printer;->onPtrBatteryStatusChange(Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method protected nativeOnPtrCoverOk(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1603
    invoke-direct {p0, p1, p2}, Lcom/epson/eposdevice/printer/Printer;->onPtrCoverOk(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected nativeOnPtrCoverOpen(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1607
    invoke-direct {p0, p1, p2}, Lcom/epson/eposdevice/printer/Printer;->onPtrCoverOpen(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected nativeOnPtrDrawerClosed(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1611
    invoke-direct {p0, p1, p2}, Lcom/epson/eposdevice/printer/Printer;->onPtrDrawerClosed(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected nativeOnPtrDrawerOpen(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1615
    invoke-direct {p0, p1, p2}, Lcom/epson/eposdevice/printer/Printer;->onPtrDrawerOpen(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected nativeOnPtrOffline(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1619
    invoke-direct {p0, p1, p2}, Lcom/epson/eposdevice/printer/Printer;->onPtrOffline(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected nativeOnPtrOnline(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1623
    invoke-direct {p0, p1, p2}, Lcom/epson/eposdevice/printer/Printer;->onPtrOnline(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected nativeOnPtrPaperEnd(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1627
    invoke-direct {p0, p1, p2}, Lcom/epson/eposdevice/printer/Printer;->onPtrPaperEnd(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected nativeOnPtrPaperNearEnd(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1631
    invoke-direct {p0, p1, p2}, Lcom/epson/eposdevice/printer/Printer;->onPtrPaperNearEnd(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected nativeOnPtrPaperOk(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1635
    invoke-direct {p0, p1, p2}, Lcom/epson/eposdevice/printer/Printer;->onPtrPaperOk(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected nativeOnPtrPowerOff(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1640
    invoke-direct {p0, p1, p2}, Lcom/epson/eposdevice/printer/Printer;->onPtrPowerOff(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected nativeOnPtrReceive(Ljava/lang/String;Ljava/lang/String;IIIILjava/lang/String;)V
    .locals 1

    .line 1650
    iget-object v0, p0, Lcom/epson/eposdevice/printer/Printer;->mJobReceiveListener:Lcom/epson/eposdevice/printer/JobReceiveListener;

    if-eqz v0, :cond_0

    .line 1651
    invoke-direct/range {p0 .. p7}, Lcom/epson/eposdevice/printer/Printer;->onPtrReceive(Ljava/lang/String;Ljava/lang/String;IIIILjava/lang/String;)V

    goto :goto_0

    .line 1655
    :cond_0
    invoke-direct/range {p0 .. p6}, Lcom/epson/eposdevice/printer/Printer;->onPtrReceive(Ljava/lang/String;Ljava/lang/String;IIII)V

    :goto_0
    return-void
.end method

.method protected nativeOnPtrStatusChange(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0

    .line 1661
    invoke-direct {p0, p1, p2, p3}, Lcom/epson/eposdevice/printer/Printer;->onPtrStatusChange(Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method protected abstract outputException(Ljava/lang/String;Ljava/lang/Exception;)V
.end method

.method protected varargs abstract outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V
.end method

.method protected varargs abstract outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V
.end method

.method protected varargs abstract outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V
.end method

.method public recover()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "recover"

    .line 1336
    invoke-virtual {p0, v2, v1}, Lcom/epson/eposdevice/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1338
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/eposdevice/printer/Printer;->checkHandle()V

    .line 1339
    iget-wide v3, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    invoke-virtual {p0, v3, v4}, Lcom/epson/eposdevice/printer/Printer;->nativeRecovery(J)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 1348
    invoke-virtual {p0, v2, v0}, Lcom/epson/eposdevice/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 1341
    :cond_0
    :try_start_1
    new-instance v0, Lcom/epson/eposdevice/EposException;

    invoke-direct {v0, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw v0
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v0

    .line 1345
    invoke-virtual {p0, v2, v0}, Lcom/epson/eposdevice/printer/Printer;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1346
    throw v0
.end method

.method public reset()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "reset"

    .line 1353
    invoke-virtual {p0, v2, v1}, Lcom/epson/eposdevice/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1355
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/eposdevice/printer/Printer;->checkHandle()V

    .line 1356
    iget-wide v3, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    iget-boolean v1, p0, Lcom/epson/eposdevice/printer/Printer;->mforce:Z

    invoke-virtual {p0, v3, v4, v1}, Lcom/epson/eposdevice/printer/Printer;->nativeReset(JZ)I

    move-result v1

    .line 1357
    iput-boolean v0, p0, Lcom/epson/eposdevice/printer/Printer;->mforce:Z
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 1366
    invoke-virtual {p0, v2, v0}, Lcom/epson/eposdevice/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 1359
    :cond_0
    :try_start_1
    new-instance v0, Lcom/epson/eposdevice/EposException;

    invoke-direct {v0, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw v0
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v0

    .line 1363
    invoke-virtual {p0, v2, v0}, Lcom/epson/eposdevice/printer/Printer;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 1364
    throw v0
.end method

.method public sendData()V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "sendData"

    .line 535
    invoke-virtual {p0, v2, v1}, Lcom/epson/eposdevice/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 537
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/eposdevice/printer/Printer;->checkHandle()V

    .line 540
    iget-wide v4, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    iget v6, p0, Lcom/epson/eposdevice/printer/Printer;->mTimeout:I

    const-string v7, ""

    iget-boolean v8, p0, Lcom/epson/eposdevice/printer/Printer;->mforce:Z

    const/4 v9, 0x0

    move-object v3, p0

    invoke-virtual/range {v3 .. v9}, Lcom/epson/eposdevice/printer/Printer;->nativeSendData(JILjava/lang/String;ZZ)I

    move-result v1

    .line 542
    iput-boolean v0, p0, Lcom/epson/eposdevice/printer/Printer;->mforce:Z
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 551
    invoke-virtual {p0, v2, v0}, Lcom/epson/eposdevice/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 544
    :cond_0
    :try_start_1
    new-instance v0, Lcom/epson/eposdevice/EposException;

    invoke-direct {v0, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw v0
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v0

    .line 548
    invoke-virtual {p0, v2, v0}, Lcom/epson/eposdevice/printer/Printer;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 549
    throw v0
.end method

.method public sendData(Ljava/lang/String;)V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "sendData use JobId"

    .line 557
    invoke-virtual {p0, v2, v1}, Lcom/epson/eposdevice/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 559
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/eposdevice/printer/Printer;->checkHandle()V

    .line 560
    invoke-virtual {p0, p1}, Lcom/epson/eposdevice/printer/Printer;->checkPrintJobIdFormat(Ljava/lang/String;)V

    .line 561
    iget-wide v4, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    iget v6, p0, Lcom/epson/eposdevice/printer/Printer;->mTimeout:I

    iget-boolean v8, p0, Lcom/epson/eposdevice/printer/Printer;->mforce:Z

    const/4 v9, 0x0

    move-object v3, p0

    move-object v7, p1

    invoke-virtual/range {v3 .. v9}, Lcom/epson/eposdevice/printer/Printer;->nativeSendData(JILjava/lang/String;ZZ)I

    move-result p1

    .line 562
    iput-boolean v0, p0, Lcom/epson/eposdevice/printer/Printer;->mforce:Z
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez p1, :cond_0

    new-array p1, v0, [Ljava/lang/Object;

    .line 571
    invoke-virtual {p0, v2, p1}, Lcom/epson/eposdevice/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 564
    :cond_0
    :try_start_1
    new-instance v0, Lcom/epson/eposdevice/EposException;

    invoke-direct {v0, p1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw v0
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception p1

    .line 568
    invoke-virtual {p0, v2, p1}, Lcom/epson/eposdevice/printer/Printer;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 569
    throw p1
.end method

.method public setBatteryLowEventCallback(Lcom/epson/eposdevice/printer/BatteryLowListener;)V
    .locals 5

    .line 1524
    iget-wide v0, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_1

    .line 1528
    iput-object p1, p0, Lcom/epson/eposdevice/printer/Printer;->mBatteryLowListener:Lcom/epson/eposdevice/printer/BatteryLowListener;

    .line 1529
    invoke-virtual {p0, v0, v1, p0}, Lcom/epson/eposdevice/printer/Printer;->nativeSetPtrBatteryLowEventCallback(JLcom/epson/eposdevice/printer/NativePrinter;)I

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 1532
    invoke-virtual {p0, v0, v1, p1}, Lcom/epson/eposdevice/printer/Printer;->nativeSetPtrBatteryLowEventCallback(JLcom/epson/eposdevice/printer/NativePrinter;)I

    .line 1533
    iput-object p1, p0, Lcom/epson/eposdevice/printer/Printer;->mBatteryLowListener:Lcom/epson/eposdevice/printer/BatteryLowListener;

    :goto_0
    return-void
.end method

.method public setBatteryOkEventCallback(Lcom/epson/eposdevice/printer/BatteryOkListener;)V
    .locals 5

    .line 1538
    iget-wide v0, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_1

    .line 1542
    iput-object p1, p0, Lcom/epson/eposdevice/printer/Printer;->mBatteryOkListener:Lcom/epson/eposdevice/printer/BatteryOkListener;

    .line 1543
    invoke-virtual {p0, v0, v1, p0}, Lcom/epson/eposdevice/printer/Printer;->nativeSetPtrBatteryOkEventCallback(JLcom/epson/eposdevice/printer/NativePrinter;)I

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 1546
    invoke-virtual {p0, v0, v1, p1}, Lcom/epson/eposdevice/printer/Printer;->nativeSetPtrBatteryOkEventCallback(JLcom/epson/eposdevice/printer/NativePrinter;)I

    .line 1547
    iput-object p1, p0, Lcom/epson/eposdevice/printer/Printer;->mBatteryOkListener:Lcom/epson/eposdevice/printer/BatteryOkListener;

    :goto_0
    return-void
.end method

.method public setBatteryStatusChangeEventCallback(Lcom/epson/eposdevice/printer/BatteryStatusChangeListener;)V
    .locals 5

    .line 1552
    iget-wide v0, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_1

    .line 1556
    iput-object p1, p0, Lcom/epson/eposdevice/printer/Printer;->mBatteryStatusChangeListener:Lcom/epson/eposdevice/printer/BatteryStatusChangeListener;

    .line 1557
    invoke-virtual {p0, v0, v1, p0}, Lcom/epson/eposdevice/printer/Printer;->nativeSetPtrBatteryStatusChangeEventCallback(JLcom/epson/eposdevice/printer/NativePrinter;)I

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 1560
    invoke-virtual {p0, v0, v1, p1}, Lcom/epson/eposdevice/printer/Printer;->nativeSetPtrBatteryStatusChangeEventCallback(JLcom/epson/eposdevice/printer/NativePrinter;)I

    .line 1561
    iput-object p1, p0, Lcom/epson/eposdevice/printer/Printer;->mBatteryStatusChangeListener:Lcom/epson/eposdevice/printer/BatteryStatusChangeListener;

    :goto_0
    return-void
.end method

.method public setBrightness(D)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 409
    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "setBrightness"

    invoke-virtual {p0, v2, v1}, Lcom/epson/eposdevice/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    const-wide/high16 v4, 0x4024000000000000L    # 10.0

    mul-double v4, v4, p1

    double-to-int v1, v4

    if-lt v1, v0, :cond_0

    const/16 v4, 0x64

    if-lt v4, v1, :cond_0

    .line 415
    :try_start_0
    iput-wide p1, p0, Lcom/epson/eposdevice/printer/Printer;->mBrightness:D
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 421
    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {p0, v2, v0}, Lcom/epson/eposdevice/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 413
    :cond_0
    :try_start_1
    new-instance p1, Lcom/epson/eposdevice/EposException;

    invoke-direct {p1, v0}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw p1
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception p1

    .line 418
    invoke-virtual {p0, v2, p1}, Lcom/epson/eposdevice/printer/Printer;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 419
    throw p1
.end method

.method public setCoverOkEventCallback(Lcom/epson/eposdevice/printer/CoverOkListener;)V
    .locals 5

    .line 1426
    iget-wide v0, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_1

    .line 1430
    iput-object p1, p0, Lcom/epson/eposdevice/printer/Printer;->mCoverOkListener:Lcom/epson/eposdevice/printer/CoverOkListener;

    .line 1431
    invoke-virtual {p0, v0, v1, p0}, Lcom/epson/eposdevice/printer/Printer;->nativeSetPtrCoverOkEventCallback(JLcom/epson/eposdevice/printer/NativePrinter;)I

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 1434
    invoke-virtual {p0, v0, v1, p1}, Lcom/epson/eposdevice/printer/Printer;->nativeSetPtrCoverOkEventCallback(JLcom/epson/eposdevice/printer/NativePrinter;)I

    .line 1435
    iput-object p1, p0, Lcom/epson/eposdevice/printer/Printer;->mCoverOkListener:Lcom/epson/eposdevice/printer/CoverOkListener;

    :goto_0
    return-void
.end method

.method public setCoverOpenEventCallback(Lcom/epson/eposdevice/printer/CoverOpenListener;)V
    .locals 5

    .line 1482
    iget-wide v0, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_1

    .line 1486
    iput-object p1, p0, Lcom/epson/eposdevice/printer/Printer;->mCoverOpenListener:Lcom/epson/eposdevice/printer/CoverOpenListener;

    .line 1487
    invoke-virtual {p0, v0, v1, p0}, Lcom/epson/eposdevice/printer/Printer;->nativeSetPtrCoverOpenEventCallback(JLcom/epson/eposdevice/printer/NativePrinter;)I

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 1490
    invoke-virtual {p0, v0, v1, p1}, Lcom/epson/eposdevice/printer/Printer;->nativeSetPtrCoverOpenEventCallback(JLcom/epson/eposdevice/printer/NativePrinter;)I

    .line 1491
    iput-object p1, p0, Lcom/epson/eposdevice/printer/Printer;->mCoverOpenListener:Lcom/epson/eposdevice/printer/CoverOpenListener;

    :goto_0
    return-void
.end method

.method public setDrawerClosedEventCallback(Lcom/epson/eposdevice/printer/DrawerClosedListener;)V
    .locals 5

    .line 1496
    iget-wide v0, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_1

    .line 1500
    iput-object p1, p0, Lcom/epson/eposdevice/printer/Printer;->mDrawerClosedListener:Lcom/epson/eposdevice/printer/DrawerClosedListener;

    .line 1501
    invoke-virtual {p0, v0, v1, p0}, Lcom/epson/eposdevice/printer/Printer;->nativeSetPtrDrawerClosedEventCallback(JLcom/epson/eposdevice/printer/NativePrinter;)I

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 1504
    invoke-virtual {p0, v0, v1, p1}, Lcom/epson/eposdevice/printer/Printer;->nativeSetPtrDrawerClosedEventCallback(JLcom/epson/eposdevice/printer/NativePrinter;)I

    .line 1505
    iput-object p1, p0, Lcom/epson/eposdevice/printer/Printer;->mDrawerClosedListener:Lcom/epson/eposdevice/printer/DrawerClosedListener;

    :goto_0
    return-void
.end method

.method public setDrawerOpenEventCallback(Lcom/epson/eposdevice/printer/DrawerOpenListener;)V
    .locals 5

    .line 1510
    iget-wide v0, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_1

    .line 1514
    iput-object p1, p0, Lcom/epson/eposdevice/printer/Printer;->mDrawerOpenListener:Lcom/epson/eposdevice/printer/DrawerOpenListener;

    .line 1515
    invoke-virtual {p0, v0, v1, p0}, Lcom/epson/eposdevice/printer/Printer;->nativeSetPtrDrawerOpenEventCallback(JLcom/epson/eposdevice/printer/NativePrinter;)I

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 1518
    invoke-virtual {p0, v0, v1, p1}, Lcom/epson/eposdevice/printer/Printer;->nativeSetPtrDrawerOpenEventCallback(JLcom/epson/eposdevice/printer/NativePrinter;)I

    .line 1519
    iput-object p1, p0, Lcom/epson/eposdevice/printer/Printer;->mDrawerOpenListener:Lcom/epson/eposdevice/printer/DrawerOpenListener;

    :goto_0
    return-void
.end method

.method public setDrawerOpenLevel(I)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 479
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "setDrawerOpenLevel"

    invoke-virtual {p0, v2, v1}, Lcom/epson/eposdevice/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    if-eqz p1, :cond_1

    if-ne p1, v0, :cond_0

    goto :goto_0

    .line 486
    :cond_0
    :try_start_0
    new-instance p1, Lcom/epson/eposdevice/EposException;

    invoke-direct {p1, v0}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw p1

    :catch_0
    move-exception p1

    goto :goto_1

    .line 488
    :cond_1
    :goto_0
    iget-wide v0, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    invoke-virtual {p0, v0, v1, p1}, Lcom/epson/eposdevice/printer/Printer;->nativeSetDrawerOpenLevel(JI)I

    move-result v0

    if-nez v0, :cond_2

    .line 492
    iput p1, p0, Lcom/epson/eposdevice/printer/Printer;->mDrawerOpenLevel:I

    return-void

    .line 490
    :cond_2
    new-instance p1, Lcom/epson/eposdevice/EposException;

    invoke-direct {p1, v0}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw p1
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_0

    .line 495
    :goto_1
    invoke-virtual {p0, v2, p1}, Lcom/epson/eposdevice/printer/Printer;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 496
    throw p1
.end method

.method public setForce(Z)V
    .locals 4

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 429
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "setForce"

    invoke-virtual {p0, v2, v1}, Lcom/epson/eposdevice/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 430
    iput-boolean p1, p0, Lcom/epson/eposdevice/printer/Printer;->mforce:Z

    new-array v0, v0, [Ljava/lang/Object;

    .line 431
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {p0, v2, v0}, Lcom/epson/eposdevice/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public setHalftone(I)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 385
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "setHalftone"

    invoke-virtual {p0, v2, v1}, Lcom/epson/eposdevice/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    if-eqz p1, :cond_1

    if-eq p1, v0, :cond_1

    const/4 v1, 0x2

    if-ne p1, v1, :cond_0

    goto :goto_0

    .line 393
    :cond_0
    :try_start_0
    new-instance p1, Lcom/epson/eposdevice/EposException;

    invoke-direct {p1, v0}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw p1

    :catch_0
    move-exception p1

    goto :goto_1

    .line 395
    :cond_1
    :goto_0
    iput p1, p0, Lcom/epson/eposdevice/printer/Printer;->mHalftone:I
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 401
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {p0, v2, v0}, Lcom/epson/eposdevice/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 398
    :goto_1
    invoke-virtual {p0, v2, p1}, Lcom/epson/eposdevice/printer/Printer;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 399
    throw p1
.end method

.method public setInterval(I)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 458
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "setInterval"

    invoke-virtual {p0, v2, v1}, Lcom/epson/eposdevice/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    const/16 v1, 0x3e8

    if-lt p1, v1, :cond_0

    const/16 v1, 0x1770

    if-lt v1, p1, :cond_0

    .line 463
    :try_start_0
    iput p1, p0, Lcom/epson/eposdevice/printer/Printer;->mInterval:I

    .line 464
    iget-wide v4, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    int-to-long v6, p1

    invoke-virtual {p0, v4, v5, v6, v7}, Lcom/epson/eposdevice/printer/Printer;->nativeSetInterval(JJ)I
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 470
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {p0, v2, v0}, Lcom/epson/eposdevice/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 461
    :cond_0
    :try_start_1
    new-instance p1, Lcom/epson/eposdevice/EposException;

    invoke-direct {p1, v0}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw p1
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception p1

    .line 467
    invoke-virtual {p0, v2, p1}, Lcom/epson/eposdevice/printer/Printer;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 468
    throw p1
.end method

.method public setOfflineEventCallback(Lcom/epson/eposdevice/printer/OfflineListener;)V
    .locals 5

    .line 1398
    iget-wide v0, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_1

    .line 1402
    iput-object p1, p0, Lcom/epson/eposdevice/printer/Printer;->mOfflineListener:Lcom/epson/eposdevice/printer/OfflineListener;

    .line 1403
    invoke-virtual {p0, v0, v1, p0}, Lcom/epson/eposdevice/printer/Printer;->nativeSetPtrOfflineEventCallback(JLcom/epson/eposdevice/printer/NativePrinter;)I

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 1406
    invoke-virtual {p0, v0, v1, p1}, Lcom/epson/eposdevice/printer/Printer;->nativeSetPtrOfflineEventCallback(JLcom/epson/eposdevice/printer/NativePrinter;)I

    .line 1407
    iput-object p1, p0, Lcom/epson/eposdevice/printer/Printer;->mOfflineListener:Lcom/epson/eposdevice/printer/OfflineListener;

    :goto_0
    return-void
.end method

.method public setOnlineEventCallback(Lcom/epson/eposdevice/printer/OnlineListener;)V
    .locals 5

    .line 1384
    iget-wide v0, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_1

    .line 1388
    iput-object p1, p0, Lcom/epson/eposdevice/printer/Printer;->mOnlineListener:Lcom/epson/eposdevice/printer/OnlineListener;

    .line 1389
    invoke-virtual {p0, v0, v1, p0}, Lcom/epson/eposdevice/printer/Printer;->nativeSetPtrOnlineEventCallback(JLcom/epson/eposdevice/printer/NativePrinter;)I

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 1392
    invoke-virtual {p0, v0, v1, p1}, Lcom/epson/eposdevice/printer/Printer;->nativeSetPtrOnlineEventCallback(JLcom/epson/eposdevice/printer/NativePrinter;)I

    .line 1393
    iput-object p1, p0, Lcom/epson/eposdevice/printer/Printer;->mOnlineListener:Lcom/epson/eposdevice/printer/OnlineListener;

    :goto_0
    return-void
.end method

.method public setPaperEndEventCallback(Lcom/epson/eposdevice/printer/PaperEndListener;)V
    .locals 5

    .line 1468
    iget-wide v0, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_1

    .line 1472
    iput-object p1, p0, Lcom/epson/eposdevice/printer/Printer;->mPaperEndListener:Lcom/epson/eposdevice/printer/PaperEndListener;

    .line 1473
    invoke-virtual {p0, v0, v1, p0}, Lcom/epson/eposdevice/printer/Printer;->nativeSetPtrPaperEndEventCallback(JLcom/epson/eposdevice/printer/NativePrinter;)I

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 1476
    invoke-virtual {p0, v0, v1, p1}, Lcom/epson/eposdevice/printer/Printer;->nativeSetPtrPaperEndEventCallback(JLcom/epson/eposdevice/printer/NativePrinter;)I

    .line 1477
    iput-object p1, p0, Lcom/epson/eposdevice/printer/Printer;->mPaperEndListener:Lcom/epson/eposdevice/printer/PaperEndListener;

    :goto_0
    return-void
.end method

.method public setPaperNearEndEventCallback(Lcom/epson/eposdevice/printer/PaperNearEndListener;)V
    .locals 5

    .line 1454
    iget-wide v0, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_1

    .line 1458
    iput-object p1, p0, Lcom/epson/eposdevice/printer/Printer;->mPaperNearEndListener:Lcom/epson/eposdevice/printer/PaperNearEndListener;

    .line 1459
    invoke-virtual {p0, v0, v1, p0}, Lcom/epson/eposdevice/printer/Printer;->nativeSetPtrPaperNearEndEventCallback(JLcom/epson/eposdevice/printer/NativePrinter;)I

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 1462
    invoke-virtual {p0, v0, v1, p1}, Lcom/epson/eposdevice/printer/Printer;->nativeSetPtrPaperNearEndEventCallback(JLcom/epson/eposdevice/printer/NativePrinter;)I

    .line 1463
    iput-object p1, p0, Lcom/epson/eposdevice/printer/Printer;->mPaperNearEndListener:Lcom/epson/eposdevice/printer/PaperNearEndListener;

    :goto_0
    return-void
.end method

.method public setPaperOkEventCallback(Lcom/epson/eposdevice/printer/PaperOkListener;)V
    .locals 5

    .line 1440
    iget-wide v0, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_1

    .line 1444
    iput-object p1, p0, Lcom/epson/eposdevice/printer/Printer;->mPaperOkListener:Lcom/epson/eposdevice/printer/PaperOkListener;

    .line 1445
    invoke-virtual {p0, v0, v1, p0}, Lcom/epson/eposdevice/printer/Printer;->nativeSetPtrPaperOkEventCallback(JLcom/epson/eposdevice/printer/NativePrinter;)I

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 1448
    invoke-virtual {p0, v0, v1, p1}, Lcom/epson/eposdevice/printer/Printer;->nativeSetPtrPaperOkEventCallback(JLcom/epson/eposdevice/printer/NativePrinter;)I

    .line 1449
    iput-object p1, p0, Lcom/epson/eposdevice/printer/Printer;->mPaperOkListener:Lcom/epson/eposdevice/printer/PaperOkListener;

    :goto_0
    return-void
.end method

.method public setPowerOffEventCallback(Lcom/epson/eposdevice/printer/PowerOffListener;)V
    .locals 5

    .line 1412
    iget-wide v0, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_1

    .line 1416
    iput-object p1, p0, Lcom/epson/eposdevice/printer/Printer;->mPowerOffListener:Lcom/epson/eposdevice/printer/PowerOffListener;

    .line 1417
    invoke-virtual {p0, v0, v1, p0}, Lcom/epson/eposdevice/printer/Printer;->nativeSetPtrPowerOffEventCallback(JLcom/epson/eposdevice/printer/NativePrinter;)I

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 1420
    invoke-virtual {p0, v0, v1, p1}, Lcom/epson/eposdevice/printer/Printer;->nativeSetPtrPowerOffEventCallback(JLcom/epson/eposdevice/printer/NativePrinter;)I

    .line 1421
    iput-object p1, p0, Lcom/epson/eposdevice/printer/Printer;->mPowerOffListener:Lcom/epson/eposdevice/printer/PowerOffListener;

    :goto_0
    return-void
.end method

.method public setReceiveEventCallback(Lcom/epson/eposdevice/printer/ReceiveListener;)V
    .locals 5

    .line 1566
    iget-wide v0, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_2

    .line 1571
    instance-of v2, p1, Lcom/epson/eposdevice/printer/JobReceiveListener;

    if-eqz v2, :cond_1

    .line 1572
    check-cast p1, Lcom/epson/eposdevice/printer/JobReceiveListener;

    iput-object p1, p0, Lcom/epson/eposdevice/printer/Printer;->mJobReceiveListener:Lcom/epson/eposdevice/printer/JobReceiveListener;

    .line 1573
    invoke-virtual {p0, v0, v1, p0}, Lcom/epson/eposdevice/printer/Printer;->nativeSetPtrReceiveEventCallback(JLcom/epson/eposdevice/printer/NativePrinter;)I

    goto :goto_0

    .line 1577
    :cond_1
    iput-object p1, p0, Lcom/epson/eposdevice/printer/Printer;->mReceiveListener:Lcom/epson/eposdevice/printer/ReceiveListener;

    .line 1578
    invoke-virtual {p0, v0, v1, p0}, Lcom/epson/eposdevice/printer/Printer;->nativeSetPtrReceiveEventCallback(JLcom/epson/eposdevice/printer/NativePrinter;)I

    goto :goto_0

    :cond_2
    const/4 p1, 0x0

    .line 1582
    invoke-virtual {p0, v0, v1, p1}, Lcom/epson/eposdevice/printer/Printer;->nativeSetPtrReceiveEventCallback(JLcom/epson/eposdevice/printer/NativePrinter;)I

    .line 1583
    iput-object p1, p0, Lcom/epson/eposdevice/printer/Printer;->mReceiveListener:Lcom/epson/eposdevice/printer/ReceiveListener;

    .line 1584
    iput-object p1, p0, Lcom/epson/eposdevice/printer/Printer;->mJobReceiveListener:Lcom/epson/eposdevice/printer/JobReceiveListener;

    :goto_0
    return-void
.end method

.method public setStatusChangeEventCallback(Lcom/epson/eposdevice/printer/StatusChangeListener;)V
    .locals 5

    .line 1370
    iget-wide v0, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_1

    .line 1374
    iput-object p1, p0, Lcom/epson/eposdevice/printer/Printer;->mStatusChangeListener:Lcom/epson/eposdevice/printer/StatusChangeListener;

    .line 1375
    invoke-virtual {p0, v0, v1, p0}, Lcom/epson/eposdevice/printer/Printer;->nativeSetPtrStatusChangeEventCallback(JLcom/epson/eposdevice/printer/NativePrinter;)I

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 1378
    invoke-virtual {p0, v0, v1, p1}, Lcom/epson/eposdevice/printer/Printer;->nativeSetPtrStatusChangeEventCallback(JLcom/epson/eposdevice/printer/NativePrinter;)I

    .line 1379
    iput-object p1, p0, Lcom/epson/eposdevice/printer/Printer;->mStatusChangeListener:Lcom/epson/eposdevice/printer/StatusChangeListener;

    :goto_0
    return-void
.end method

.method public setTimeout(I)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 439
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "setTimeout"

    invoke-virtual {p0, v2, v1}, Lcom/epson/eposdevice/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    if-ltz p1, :cond_0

    const v1, 0x927c0

    if-lt v1, p1, :cond_0

    .line 444
    :try_start_0
    iput p1, p0, Lcom/epson/eposdevice/printer/Printer;->mTimeout:I
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 450
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {p0, v2, v0}, Lcom/epson/eposdevice/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 442
    :cond_0
    :try_start_1
    new-instance p1, Lcom/epson/eposdevice/EposException;

    invoke-direct {p1, v0}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw p1
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception p1

    .line 447
    invoke-virtual {p0, v2, p1}, Lcom/epson/eposdevice/printer/Printer;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 448
    throw p1
.end method

.method public startMonitor()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "startMonitor"

    .line 507
    invoke-virtual {p0, v2, v1}, Lcom/epson/eposdevice/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 509
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/eposdevice/printer/Printer;->checkHandle()V

    .line 510
    iget-wide v3, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    iget v1, p0, Lcom/epson/eposdevice/printer/Printer;->mInterval:I

    int-to-long v5, v1

    invoke-virtual {p0, v3, v4, v5, v6}, Lcom/epson/eposdevice/printer/Printer;->nativeStartMonitor(JJ)I
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 516
    invoke-virtual {p0, v2, v0}, Lcom/epson/eposdevice/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    :catch_0
    move-exception v0

    .line 513
    invoke-virtual {p0, v2, v0}, Lcom/epson/eposdevice/printer/Printer;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 514
    throw v0
.end method

.method public stopMonitor()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "stopMonitor"

    .line 521
    invoke-virtual {p0, v2, v1}, Lcom/epson/eposdevice/printer/Printer;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 523
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/eposdevice/printer/Printer;->checkHandle()V

    .line 524
    iget-wide v3, p0, Lcom/epson/eposdevice/printer/Printer;->mPrinterHandle:J

    invoke-virtual {p0, v3, v4}, Lcom/epson/eposdevice/printer/Printer;->nativeStopMonitor(J)I
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 530
    invoke-virtual {p0, v2, v0}, Lcom/epson/eposdevice/printer/Printer;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    :catch_0
    move-exception v0

    .line 527
    invoke-virtual {p0, v2, v0}, Lcom/epson/eposdevice/printer/Printer;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 528
    throw v0
.end method
