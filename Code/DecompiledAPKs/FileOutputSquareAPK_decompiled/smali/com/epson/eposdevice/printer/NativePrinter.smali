.class abstract Lcom/epson/eposdevice/printer/NativePrinter;
.super Ljava/lang/Object;
.source "NativePrinter.java"


# static fields
.field protected static final NATIVE_ALIGN_CENTER:I = 0x1

.field protected static final NATIVE_ALIGN_LEFT:I = 0x0

.field protected static final NATIVE_ALIGN_RIGHT:I = 0x2

.field protected static final NATIVE_BARCODE_CODABAR:I = 0x8

.field protected static final NATIVE_BARCODE_CODE128:I = 0xa

.field protected static final NATIVE_BARCODE_CODE39:I = 0x6

.field protected static final NATIVE_BARCODE_CODE93:I = 0x9

.field protected static final NATIVE_BARCODE_EAN13:I = 0x2

.field protected static final NATIVE_BARCODE_EAN8:I = 0x4

.field protected static final NATIVE_BARCODE_GS1_128:I = 0xb

.field protected static final NATIVE_BARCODE_GS1_DATABAR_EXPANDED:I = 0xf

.field protected static final NATIVE_BARCODE_GS1_DATABAR_LIMITED:I = 0xe

.field protected static final NATIVE_BARCODE_GS1_DATABAR_OMNIDIRECTIONAL:I = 0xc

.field protected static final NATIVE_BARCODE_GS1_DATABAR_TRUNCATED:I = 0xd

.field protected static final NATIVE_BARCODE_ITF:I = 0x7

.field protected static final NATIVE_BARCODE_JAN13:I = 0x3

.field protected static final NATIVE_BARCODE_JAN8:I = 0x5

.field protected static final NATIVE_BARCODE_UPC_A:I = 0x0

.field protected static final NATIVE_BARCODE_UPC_E:I = 0x1

.field protected static final NATIVE_COLOR_1:I = 0x1

.field protected static final NATIVE_COLOR_2:I = 0x2

.field protected static final NATIVE_COLOR_3:I = 0x3

.field protected static final NATIVE_COLOR_4:I = 0x4

.field protected static final NATIVE_COLOR_NONE:I = 0x0

.field protected static final NATIVE_CUT_FEED:I = 0x1

.field protected static final NATIVE_CUT_NO_FEED:I = 0x0

.field protected static final NATIVE_CUT_RESERVE:I = 0x2

.field protected static final NATIVE_DIRECTION_BOTTOM_TO_TOP:I = 0x1

.field protected static final NATIVE_DIRECTION_LEFT_TO_RIGHT:I = 0x0

.field protected static final NATIVE_DIRECTION_RIGHT_TO_LEFT:I = 0x2

.field protected static final NATIVE_DIRECTION_TOP_TO_BOTTOM:I = 0x3

.field protected static final NATIVE_DRAWER_1:I = 0x0

.field protected static final NATIVE_DRAWER_2:I = 0x1

.field protected static final NATIVE_DRAWER_OPEN_LEVEL_HIGH:I = 0x1

.field protected static final NATIVE_DRAWER_OPEN_LEVEL_LOW:I = 0x0

.field protected static final NATIVE_FALSE:I = 0x0

.field protected static final NATIVE_FEED_CURRENT_TOF:I = 0x2

.field protected static final NATIVE_FEED_CUTTING:I = 0x1

.field protected static final NATIVE_FEED_NEXT_TOF:I = 0x3

.field protected static final NATIVE_FEED_PEELING:I = 0x0

.field protected static final NATIVE_FONT_A:I = 0x0

.field protected static final NATIVE_FONT_B:I = 0x1

.field protected static final NATIVE_FONT_C:I = 0x2

.field protected static final NATIVE_FONT_D:I = 0x3

.field protected static final NATIVE_FONT_E:I = 0x4

.field protected static final NATIVE_HALFTONE_DITHER:I = 0x0

.field protected static final NATIVE_HALFTONE_ERROR_DIFFUSION:I = 0x1

.field protected static final NATIVE_HALFTONE_THRESHOLD:I = 0x2

.field protected static final NATIVE_HRI_ABOVE:I = 0x1

.field protected static final NATIVE_HRI_BELOW:I = 0x2

.field protected static final NATIVE_HRI_BOTH:I = 0x3

.field protected static final NATIVE_HRI_NONE:I = 0x0

.field protected static final NATIVE_LANG_EN:I = 0x0

.field protected static final NATIVE_LANG_JA:I = 0x1

.field protected static final NATIVE_LANG_KO:I = 0x4

.field protected static final NATIVE_LANG_TH:I = 0x5

.field protected static final NATIVE_LANG_ZH_CN:I = 0x2

.field protected static final NATIVE_LANG_ZH_TW:I = 0x3

.field protected static final NATIVE_LAYOUT_LABEL:I = 0x1

.field protected static final NATIVE_LAYOUT_LABEL_BM:I = 0x2

.field protected static final NATIVE_LAYOUT_RECEIPT:I = 0x0

.field protected static final NATIVE_LAYOUT_RECEIPT_BM:I = 0x3

.field protected static final NATIVE_LEVEL_0:I = 0x0

.field protected static final NATIVE_LEVEL_1:I = 0x1

.field protected static final NATIVE_LEVEL_2:I = 0x2

.field protected static final NATIVE_LEVEL_3:I = 0x3

.field protected static final NATIVE_LEVEL_4:I = 0x4

.field protected static final NATIVE_LEVEL_5:I = 0x5

.field protected static final NATIVE_LEVEL_6:I = 0x6

.field protected static final NATIVE_LEVEL_7:I = 0x7

.field protected static final NATIVE_LEVEL_8:I = 0x8

.field protected static final NATIVE_LEVEL_DEFAULT:I = 0xd

.field protected static final NATIVE_LEVEL_H:I = 0xc

.field protected static final NATIVE_LEVEL_L:I = 0x9

.field protected static final NATIVE_LEVEL_M:I = 0xa

.field protected static final NATIVE_LEVEL_Q:I = 0xb

.field protected static final NATIVE_LINE_MEDIUM:I = 0x1

.field protected static final NATIVE_LINE_MEDIUM_DOUBLE:I = 0x4

.field protected static final NATIVE_LINE_THICK:I = 0x2

.field protected static final NATIVE_LINE_THICK_DOUBLE:I = 0x5

.field protected static final NATIVE_LINE_THIN:I = 0x0

.field protected static final NATIVE_LINE_THIN_DOUBLE:I = 0x3

.field protected static final NATIVE_MODE_GRAY16:I = 0x1

.field protected static final NATIVE_MODE_MONO:I = 0x0

.field protected static final NATIVE_PARAM_DEFAULT:I = -0x2

.field protected static final NATIVE_PARAM_UNSPECIFIED:I = -0x1

.field protected static final NATIVE_PATTERN_1:I = 0x8

.field protected static final NATIVE_PATTERN_10:I = 0x11

.field protected static final NATIVE_PATTERN_2:I = 0x9

.field protected static final NATIVE_PATTERN_3:I = 0xa

.field protected static final NATIVE_PATTERN_4:I = 0xb

.field protected static final NATIVE_PATTERN_5:I = 0xc

.field protected static final NATIVE_PATTERN_6:I = 0xd

.field protected static final NATIVE_PATTERN_7:I = 0xe

.field protected static final NATIVE_PATTERN_8:I = 0xf

.field protected static final NATIVE_PATTERN_9:I = 0x10

.field protected static final NATIVE_PATTERN_A:I = 0x1

.field protected static final NATIVE_PATTERN_B:I = 0x2

.field protected static final NATIVE_PATTERN_C:I = 0x3

.field protected static final NATIVE_PATTERN_D:I = 0x4

.field protected static final NATIVE_PATTERN_E:I = 0x5

.field protected static final NATIVE_PATTERN_ERROR:I = 0x6

.field protected static final NATIVE_PATTERN_NONE:I = 0x0

.field protected static final NATIVE_PATTERN_PAPER_END:I = 0x7

.field protected static final NATIVE_PULSE_100:I = 0x0

.field protected static final NATIVE_PULSE_200:I = 0x1

.field protected static final NATIVE_PULSE_300:I = 0x2

.field protected static final NATIVE_PULSE_400:I = 0x3

.field protected static final NATIVE_PULSE_500:I = 0x4

.field protected static final NATIVE_ST_AUTOCUTTER_ERR:I = 0x800

.field protected static final NATIVE_ST_AUTORECOVER_ERR:I = 0x4000

.field protected static final NATIVE_ST_BATTERY_OFFLINE:I = 0x4

.field protected static final NATIVE_ST_BUZZER:I = 0x1000000

.field protected static final NATIVE_ST_COVER_OPEN:I = 0x20

.field protected static final NATIVE_ST_DRAWER_KICK:I = 0x4

.field protected static final NATIVE_ST_MECHANICAL_ERR:I = 0x400

.field protected static final NATIVE_ST_NO_RESPONSE:I = 0x1

.field protected static final NATIVE_ST_OFF_LINE:I = 0x8

.field protected static final NATIVE_ST_PANEL_SWITCH:I = 0x200

.field protected static final NATIVE_ST_PAPER_FEED:I = 0x40

.field protected static final NATIVE_ST_PRINT_SUCCESS:I = 0x2

.field protected static final NATIVE_ST_RECEIPT_END:I = 0x80000

.field protected static final NATIVE_ST_RECEIPT_NEAR_END:I = 0x20000

.field protected static final NATIVE_ST_SPOOLER_IS_STOPPED:I = -0x80000000

.field protected static final NATIVE_ST_UNRECOVER_ERR:I = 0x2000

.field protected static final NATIVE_ST_WAIT_ON_LINE:I = 0x100

.field protected static final NATIVE_SYMBOL_AZTECCODE_COMPACT:I = 0xd

.field protected static final NATIVE_SYMBOL_AZTECCODE_FULLRANGE:I = 0xc

.field protected static final NATIVE_SYMBOL_DATAMATRIX_RECTANGLE_12:I = 0x10

.field protected static final NATIVE_SYMBOL_DATAMATRIX_RECTANGLE_16:I = 0x11

.field protected static final NATIVE_SYMBOL_DATAMATRIX_RECTANGLE_8:I = 0xf

.field protected static final NATIVE_SYMBOL_DATAMATRIX_SQUARE:I = 0xe

.field protected static final NATIVE_SYMBOL_GS1_DATABAR_EXPANDED_STACKED:I = 0xb

.field protected static final NATIVE_SYMBOL_GS1_DATABAR_STACKED:I = 0x9

.field protected static final NATIVE_SYMBOL_GS1_DATABAR_STACKED_OMNIDIRECTIONAL:I = 0xa

.field protected static final NATIVE_SYMBOL_MAXICODE_MODE_2:I = 0x4

.field protected static final NATIVE_SYMBOL_MAXICODE_MODE_3:I = 0x5

.field protected static final NATIVE_SYMBOL_MAXICODE_MODE_4:I = 0x6

.field protected static final NATIVE_SYMBOL_MAXICODE_MODE_5:I = 0x7

.field protected static final NATIVE_SYMBOL_MAXICODE_MODE_6:I = 0x8

.field protected static final NATIVE_SYMBOL_PDF417_STANDARD:I = 0x0

.field protected static final NATIVE_SYMBOL_PDF417_TRUNCATED:I = 0x1

.field protected static final NATIVE_SYMBOL_QRCODE_MICRO:I = 0x12

.field protected static final NATIVE_SYMBOL_QRCODE_MODEL_1:I = 0x2

.field protected static final NATIVE_SYMBOL_QRCODE_MODEL_2:I = 0x3

.field protected static final NATIVE_TRUE:I = 0x1


# direct methods
.method constructor <init>()V
    .locals 0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method protected native nativeAddBarcode(JLjava/lang/String;IIIJJ)I
.end method

.method protected native nativeAddCommand(J[B)I
.end method

.method protected native nativeAddCut(JI)I
.end method

.method protected native nativeAddFeed(J)I
.end method

.method protected native nativeAddFeedLine(JJ)I
.end method

.method protected native nativeAddFeedPosition(JI)I
.end method

.method protected native nativeAddFeedUnit(JJ)I
.end method

.method protected native nativeAddHLine(JJJI)I
.end method

.method protected native nativeAddImage(J[BJJJJJJIIID)I
.end method

.method protected native nativeAddLayout(JIJJJJJJ)I
.end method

.method protected native nativeAddLogo(JJJ)I
.end method

.method protected native nativeAddPageArea(JJJJJ)I
.end method

.method protected native nativeAddPageBegin(J)I
.end method

.method protected native nativeAddPageDirection(JI)I
.end method

.method protected native nativeAddPageEnd(J)I
.end method

.method protected native nativeAddPageLine(JJJJJI)I
.end method

.method protected native nativeAddPagePosition(JJJ)I
.end method

.method protected native nativeAddPageRectangle(JJJJJI)I
.end method

.method protected native nativeAddPulse(JII)I
.end method

.method protected native nativeAddRecovery(J)I
.end method

.method protected native nativeAddReset(J)I
.end method

.method protected native nativeAddSound(JIJJ)I
.end method

.method protected native nativeAddSymbol(JLjava/lang/String;IIJJJ)I
.end method

.method protected native nativeAddText(JLjava/lang/String;)I
.end method

.method protected native nativeAddTextAlign(JI)I
.end method

.method protected native nativeAddTextDouble(JII)I
.end method

.method protected native nativeAddTextFont(JI)I
.end method

.method protected native nativeAddTextLang(JI)I
.end method

.method protected native nativeAddTextLineSpace(JJ)I
.end method

.method protected native nativeAddTextPosition(JJ)I
.end method

.method protected native nativeAddTextRotate(JI)I
.end method

.method protected native nativeAddTextSize(JJJ)I
.end method

.method protected native nativeAddTextSmooth(JI)I
.end method

.method protected native nativeAddTextStyle(JIIII)I
.end method

.method protected native nativeAddTextVPosition(JJ)I
.end method

.method protected native nativeAddVLineBegin(JJI)I
.end method

.method protected native nativeAddVLineEnd(JJI)I
.end method

.method protected native nativeClearCommandBuffer(J)I
.end method

.method protected abstract nativeOnPtrBatteryLow(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method protected abstract nativeOnPtrBatteryOk(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method protected abstract nativeOnPtrBatteryStatusChange(Ljava/lang/String;Ljava/lang/String;I)V
.end method

.method protected abstract nativeOnPtrCoverOk(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method protected abstract nativeOnPtrCoverOpen(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method protected abstract nativeOnPtrDrawerClosed(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method protected abstract nativeOnPtrDrawerOpen(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method protected abstract nativeOnPtrOffline(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method protected abstract nativeOnPtrOnline(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method protected abstract nativeOnPtrPaperEnd(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method protected abstract nativeOnPtrPaperNearEnd(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method protected abstract nativeOnPtrPaperOk(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method protected abstract nativeOnPtrPowerOff(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method protected abstract nativeOnPtrReceive(Ljava/lang/String;Ljava/lang/String;IIIILjava/lang/String;)V
.end method

.method protected abstract nativeOnPtrStatusChange(Ljava/lang/String;Ljava/lang/String;I)V
.end method

.method protected native nativeRecovery(J)I
.end method

.method protected native nativeReset(JZ)I
.end method

.method protected native nativeSendData(JILjava/lang/String;ZZ)I
.end method

.method protected native nativeSetDrawerOpenLevel(JI)I
.end method

.method protected native nativeSetInterval(JJ)I
.end method

.method protected native nativeSetPtrBatteryLowEventCallback(JLcom/epson/eposdevice/printer/NativePrinter;)I
.end method

.method protected native nativeSetPtrBatteryOkEventCallback(JLcom/epson/eposdevice/printer/NativePrinter;)I
.end method

.method protected native nativeSetPtrBatteryStatusChangeEventCallback(JLcom/epson/eposdevice/printer/NativePrinter;)I
.end method

.method protected native nativeSetPtrCoverOkEventCallback(JLcom/epson/eposdevice/printer/NativePrinter;)I
.end method

.method protected native nativeSetPtrCoverOpenEventCallback(JLcom/epson/eposdevice/printer/NativePrinter;)I
.end method

.method protected native nativeSetPtrDrawerClosedEventCallback(JLcom/epson/eposdevice/printer/NativePrinter;)I
.end method

.method protected native nativeSetPtrDrawerOpenEventCallback(JLcom/epson/eposdevice/printer/NativePrinter;)I
.end method

.method protected native nativeSetPtrOfflineEventCallback(JLcom/epson/eposdevice/printer/NativePrinter;)I
.end method

.method protected native nativeSetPtrOnlineEventCallback(JLcom/epson/eposdevice/printer/NativePrinter;)I
.end method

.method protected native nativeSetPtrPaperEndEventCallback(JLcom/epson/eposdevice/printer/NativePrinter;)I
.end method

.method protected native nativeSetPtrPaperNearEndEventCallback(JLcom/epson/eposdevice/printer/NativePrinter;)I
.end method

.method protected native nativeSetPtrPaperOkEventCallback(JLcom/epson/eposdevice/printer/NativePrinter;)I
.end method

.method protected native nativeSetPtrPowerOffEventCallback(JLcom/epson/eposdevice/printer/NativePrinter;)I
.end method

.method protected native nativeSetPtrReceiveEventCallback(JLcom/epson/eposdevice/printer/NativePrinter;)I
.end method

.method protected native nativeSetPtrStatusChangeEventCallback(JLcom/epson/eposdevice/printer/NativePrinter;)I
.end method

.method protected native nativeStartMonitor(JJ)I
.end method

.method protected native nativeStopMonitor(J)I
.end method
