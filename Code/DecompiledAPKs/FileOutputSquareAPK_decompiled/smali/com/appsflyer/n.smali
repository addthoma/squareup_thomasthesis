.class final Lcom/appsflyer/n;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/app/Application$ActivityLifecycleCallbacks;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/appsflyer/n$b;,
        Lcom/appsflyer/n$a;
    }
.end annotation


# static fields
.field private static ॱ:Lcom/appsflyer/n;


# instance fields
.field private ˊ:Lcom/appsflyer/n$a;

.field private ˋ:Z

.field private ˏ:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 62
    iput-boolean v0, p0, Lcom/appsflyer/n;->ˏ:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/appsflyer/n;->ˋ:Z

    const/4 v0, 0x0

    .line 63
    iput-object v0, p0, Lcom/appsflyer/n;->ˊ:Lcom/appsflyer/n$a;

    return-void
.end method

.method static ˊ()Lcom/appsflyer/n;
    .locals 1

    .line 74
    sget-object v0, Lcom/appsflyer/n;->ॱ:Lcom/appsflyer/n;

    if-nez v0, :cond_0

    .line 75
    new-instance v0, Lcom/appsflyer/n;

    invoke-direct {v0}, Lcom/appsflyer/n;-><init>()V

    sput-object v0, Lcom/appsflyer/n;->ॱ:Lcom/appsflyer/n;

    .line 77
    :cond_0
    sget-object v0, Lcom/appsflyer/n;->ॱ:Lcom/appsflyer/n;

    return-object v0
.end method

.method static synthetic ˊ(Lcom/appsflyer/n;)Z
    .locals 1

    const/4 v0, 0x0

    .line 48
    iput-boolean v0, p0, Lcom/appsflyer/n;->ˏ:Z

    return v0
.end method

.method public static ˋ()Lcom/appsflyer/n;
    .locals 2

    .line 101
    sget-object v0, Lcom/appsflyer/n;->ॱ:Lcom/appsflyer/n;

    if-eqz v0, :cond_0

    return-object v0

    .line 102
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Foreground is not initialised - invoke at least once with parameter init/get"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method static synthetic ˋ(Lcom/appsflyer/n;)Z
    .locals 0

    .line 48
    iget-boolean p0, p0, Lcom/appsflyer/n;->ˋ:Z

    return p0
.end method

.method static synthetic ˎ(Lcom/appsflyer/n;)Z
    .locals 0

    .line 48
    iget-boolean p0, p0, Lcom/appsflyer/n;->ˏ:Z

    return p0
.end method

.method static synthetic ॱ(Lcom/appsflyer/n;)Lcom/appsflyer/n$a;
    .locals 0

    .line 48
    iget-object p0, p0, Lcom/appsflyer/n;->ˊ:Lcom/appsflyer/n$a;

    return-object p0
.end method


# virtual methods
.method public final onActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 0

    .line 194
    invoke-static {}, Lcom/appsflyer/AFDeepLinkManager;->getInstance()Lcom/appsflyer/AFDeepLinkManager;

    move-result-object p2

    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/appsflyer/AFDeepLinkManager;->collectIntentsFromActivities(Landroid/content/Intent;)V

    return-void
.end method

.method public final onActivityDestroyed(Landroid/app/Activity;)V
    .locals 0

    return-void
.end method

.method public final onActivityPaused(Landroid/app/Activity;)V
    .locals 2

    const/4 v0, 0x1

    .line 149
    iput-boolean v0, p0, Lcom/appsflyer/n;->ˋ:Z

    .line 150
    new-instance v0, Lcom/appsflyer/n$b;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-direct {v0, p0, v1}, Lcom/appsflyer/n$b;-><init>(Lcom/appsflyer/n;Ljava/lang/ref/WeakReference;)V

    .line 152
    invoke-static {}, Lcom/appsflyer/AFExecutor;->getInstance()Lcom/appsflyer/AFExecutor;

    move-result-object p1

    invoke-virtual {p1}, Lcom/appsflyer/AFExecutor;->getThreadPoolExecutor()Ljava/util/concurrent/Executor;

    move-result-object p1

    const/4 v1, 0x0

    :try_start_0
    new-array v1, v1, [Ljava/lang/Void;

    .line 156
    invoke-virtual {v0, p1, v1}, Lcom/appsflyer/n$b;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_0
    .catch Ljava/util/concurrent/RejectedExecutionException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception p1

    const-string v0, "backgroundTask.executeOnExecutor failed with Exception"

    .line 160
    invoke-static {v0, p1}, Lcom/appsflyer/AFLogger;->afErrorLog(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void

    :catch_0
    move-exception p1

    const-string v0, "backgroundTask.executeOnExecutor failed with RejectedExecutionException Exception"

    .line 158
    invoke-static {v0, p1}, Lcom/appsflyer/AFLogger;->afErrorLog(Ljava/lang/String;Ljava/lang/Throwable;)V

    return-void
.end method

.method public final onActivityResumed(Landroid/app/Activity;)V
    .locals 2

    const/4 v0, 0x0

    .line 134
    iput-boolean v0, p0, Lcom/appsflyer/n;->ˋ:Z

    .line 135
    iget-boolean v0, p0, Lcom/appsflyer/n;->ˏ:Z

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    .line 136
    iput-boolean v1, p0, Lcom/appsflyer/n;->ˏ:Z

    if-eqz v0, :cond_0

    .line 140
    :try_start_0
    iget-object v0, p0, Lcom/appsflyer/n;->ˊ:Lcom/appsflyer/n$a;

    invoke-interface {v0, p1}, Lcom/appsflyer/n$a;->ॱ(Landroid/app/Activity;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    const-string v0, "Listener threw exception! "

    .line 142
    invoke-static {v0, p1}, Lcom/appsflyer/AFLogger;->afErrorLog(Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_0
    return-void
.end method

.method public final onActivitySaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 0

    return-void
.end method

.method public final onActivityStarted(Landroid/app/Activity;)V
    .locals 0

    return-void
.end method

.method public final onActivityStopped(Landroid/app/Activity;)V
    .locals 0

    return-void
.end method

.method public final ˊ(Landroid/app/Application;Lcom/appsflyer/n$a;)V
    .locals 1

    .line 122
    iput-object p2, p0, Lcom/appsflyer/n;->ˊ:Lcom/appsflyer/n$a;

    .line 123
    sget p2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v0, 0xe

    if-lt p2, v0, :cond_0

    .line 124
    sget-object p2, Lcom/appsflyer/n;->ॱ:Lcom/appsflyer/n;

    invoke-virtual {p1, p2}, Landroid/app/Application;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    :cond_0
    return-void
.end method
