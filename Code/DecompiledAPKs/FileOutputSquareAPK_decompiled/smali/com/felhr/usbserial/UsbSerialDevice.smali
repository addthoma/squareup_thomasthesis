.class public abstract Lcom/felhr/usbserial/UsbSerialDevice;
.super Ljava/lang/Object;
.source "UsbSerialDevice.java"

# interfaces
.implements Lcom/felhr/usbserial/UsbSerialInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/felhr/usbserial/UsbSerialDevice$ReadThread;,
        Lcom/felhr/usbserial/UsbSerialDevice$WriteThread;,
        Lcom/felhr/usbserial/UsbSerialDevice$WorkerThread;
    }
.end annotation


# static fields
.field public static final CDC:Ljava/lang/String; = "cdc"

.field public static final CH34x:Ljava/lang/String; = "ch34x"

.field protected static final COM_PORT:Ljava/lang/String; = "COM "

.field public static final CP210x:Ljava/lang/String; = "cp210x"

.field public static final FTDI:Ljava/lang/String; = "ftdi"

.field public static final PL2303:Ljava/lang/String; = "pl2303"

.field protected static final USB_TIMEOUT:I

.field static final mr1Version:Z


# instance fields
.field protected asyncMode:Z

.field protected final connection:Landroid/hardware/usb/UsbDeviceConnection;

.field protected final device:Landroid/hardware/usb/UsbDevice;

.field private inEndpoint:Landroid/hardware/usb/UsbEndpoint;

.field protected inputStream:Lcom/felhr/usbserial/SerialInputStream;

.field protected isOpen:Z

.field private outEndpoint:Landroid/hardware/usb/UsbEndpoint;

.field protected outputStream:Lcom/felhr/usbserial/SerialOutputStream;

.field private portName:Ljava/lang/String;

.field protected readThread:Lcom/felhr/usbserial/UsbSerialDevice$ReadThread;

.field protected serialBuffer:Lcom/felhr/usbserial/SerialBuffer;

.field protected workerThread:Lcom/felhr/usbserial/UsbSerialDevice$WorkerThread;

.field protected writeThread:Lcom/felhr/usbserial/UsbSerialDevice$WriteThread;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 27
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    sput-boolean v0, Lcom/felhr/usbserial/UsbSerialDevice;->mr1Version:Z

    return-void
.end method

.method public constructor <init>(Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbDeviceConnection;)V
    .locals 1

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, ""

    .line 50
    iput-object v0, p0, Lcom/felhr/usbserial/UsbSerialDevice;->portName:Ljava/lang/String;

    .line 55
    iput-object p1, p0, Lcom/felhr/usbserial/UsbSerialDevice;->device:Landroid/hardware/usb/UsbDevice;

    .line 56
    iput-object p2, p0, Lcom/felhr/usbserial/UsbSerialDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    const/4 p1, 0x1

    .line 57
    iput-boolean p1, p0, Lcom/felhr/usbserial/UsbSerialDevice;->asyncMode:Z

    .line 58
    new-instance p1, Lcom/felhr/usbserial/SerialBuffer;

    sget-boolean p2, Lcom/felhr/usbserial/UsbSerialDevice;->mr1Version:Z

    invoke-direct {p1, p2}, Lcom/felhr/usbserial/SerialBuffer;-><init>(Z)V

    iput-object p1, p0, Lcom/felhr/usbserial/UsbSerialDevice;->serialBuffer:Lcom/felhr/usbserial/SerialBuffer;

    return-void
.end method

.method static synthetic access$000(Lcom/felhr/usbserial/UsbSerialDevice;)Z
    .locals 0

    .line 16
    invoke-direct {p0}, Lcom/felhr/usbserial/UsbSerialDevice;->isFTDIDevice()Z

    move-result p0

    return p0
.end method

.method public static createUsbSerialDevice(Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbDeviceConnection;)Lcom/felhr/usbserial/UsbSerialDevice;
    .locals 1

    const/4 v0, -0x1

    .line 63
    invoke-static {p0, p1, v0}, Lcom/felhr/usbserial/UsbSerialDevice;->createUsbSerialDevice(Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbDeviceConnection;I)Lcom/felhr/usbserial/UsbSerialDevice;

    move-result-object p0

    return-object p0
.end method

.method public static createUsbSerialDevice(Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbDeviceConnection;I)Lcom/felhr/usbserial/UsbSerialDevice;
    .locals 3

    .line 73
    invoke-virtual {p0}, Landroid/hardware/usb/UsbDevice;->getVendorId()I

    move-result v0

    .line 74
    invoke-virtual {p0}, Landroid/hardware/usb/UsbDevice;->getProductId()I

    move-result v1

    .line 76
    invoke-static {p0}, Lcom/felhr/deviceids/FTDISioIds;->isDeviceSupported(Landroid/hardware/usb/UsbDevice;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 77
    new-instance v0, Lcom/felhr/usbserial/FTDISerialDevice;

    invoke-direct {v0, p0, p1, p2}, Lcom/felhr/usbserial/FTDISerialDevice;-><init>(Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbDeviceConnection;I)V

    return-object v0

    .line 78
    :cond_0
    invoke-static {v0, v1}, Lcom/felhr/deviceids/CP210xIds;->isDeviceSupported(II)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 79
    new-instance v0, Lcom/felhr/usbserial/CP2102SerialDevice;

    invoke-direct {v0, p0, p1, p2}, Lcom/felhr/usbserial/CP2102SerialDevice;-><init>(Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbDeviceConnection;I)V

    return-object v0

    .line 80
    :cond_1
    invoke-static {v0, v1}, Lcom/felhr/deviceids/PL2303Ids;->isDeviceSupported(II)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 81
    new-instance v0, Lcom/felhr/usbserial/PL2303SerialDevice;

    invoke-direct {v0, p0, p1, p2}, Lcom/felhr/usbserial/PL2303SerialDevice;-><init>(Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbDeviceConnection;I)V

    return-object v0

    .line 82
    :cond_2
    invoke-static {v0, v1}, Lcom/felhr/deviceids/CH34xIds;->isDeviceSupported(II)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 83
    new-instance v0, Lcom/felhr/usbserial/CH34xSerialDevice;

    invoke-direct {v0, p0, p1, p2}, Lcom/felhr/usbserial/CH34xSerialDevice;-><init>(Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbDeviceConnection;I)V

    return-object v0

    .line 84
    :cond_3
    invoke-static {p0}, Lcom/felhr/usbserial/UsbSerialDevice;->isCdcDevice(Landroid/hardware/usb/UsbDevice;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 85
    new-instance v0, Lcom/felhr/usbserial/CDCSerialDevice;

    invoke-direct {v0, p0, p1, p2}, Lcom/felhr/usbserial/CDCSerialDevice;-><init>(Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbDeviceConnection;I)V

    return-object v0

    :cond_4
    const/4 p0, 0x0

    return-object p0
.end method

.method public static createUsbSerialDevice(Ljava/lang/String;Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbDeviceConnection;I)Lcom/felhr/usbserial/UsbSerialDevice;
    .locals 1

    const-string v0, "ftdi"

    .line 91
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 92
    new-instance p0, Lcom/felhr/usbserial/FTDISerialDevice;

    invoke-direct {p0, p1, p2, p3}, Lcom/felhr/usbserial/FTDISerialDevice;-><init>(Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbDeviceConnection;I)V

    return-object p0

    :cond_0
    const-string v0, "cp210x"

    .line 93
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 94
    new-instance p0, Lcom/felhr/usbserial/CP2102SerialDevice;

    invoke-direct {p0, p1, p2, p3}, Lcom/felhr/usbserial/CP2102SerialDevice;-><init>(Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbDeviceConnection;I)V

    return-object p0

    :cond_1
    const-string v0, "pl2303"

    .line 95
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 96
    new-instance p0, Lcom/felhr/usbserial/PL2303SerialDevice;

    invoke-direct {p0, p1, p2, p3}, Lcom/felhr/usbserial/PL2303SerialDevice;-><init>(Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbDeviceConnection;I)V

    return-object p0

    :cond_2
    const-string v0, "ch34x"

    .line 97
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 98
    new-instance p0, Lcom/felhr/usbserial/CH34xSerialDevice;

    invoke-direct {p0, p1, p2, p3}, Lcom/felhr/usbserial/CH34xSerialDevice;-><init>(Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbDeviceConnection;I)V

    return-object p0

    :cond_3
    const-string v0, "cdc"

    .line 99
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 100
    new-instance p0, Lcom/felhr/usbserial/CDCSerialDevice;

    invoke-direct {p0, p1, p2, p3}, Lcom/felhr/usbserial/CDCSerialDevice;-><init>(Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbDeviceConnection;I)V

    return-object p0

    .line 102
    :cond_4
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "Invalid type argument. Must be:cdc, ch34x, cp210x, ftdi or pl2303"

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static isCdcDevice(Landroid/hardware/usb/UsbDevice;)Z
    .locals 6

    .line 315
    invoke-virtual {p0}, Landroid/hardware/usb/UsbDevice;->getInterfaceCount()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    const/4 v3, 0x1

    add-int/lit8 v4, v0, -0x1

    if-gt v2, v4, :cond_1

    .line 318
    invoke-virtual {p0, v2}, Landroid/hardware/usb/UsbDevice;->getInterface(I)Landroid/hardware/usb/UsbInterface;

    move-result-object v4

    .line 319
    invoke-virtual {v4}, Landroid/hardware/usb/UsbInterface;->getInterfaceClass()I

    move-result v4

    const/16 v5, 0xa

    if-ne v4, v5, :cond_0

    return v3

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return v1
.end method

.method private isFTDIDevice()Z
    .locals 1

    .line 310
    instance-of v0, p0, Lcom/felhr/usbserial/FTDISerialDevice;

    return v0
.end method

.method public static isSupported(Landroid/hardware/usb/UsbDevice;)Z
    .locals 4

    .line 108
    invoke-virtual {p0}, Landroid/hardware/usb/UsbDevice;->getVendorId()I

    move-result v0

    .line 109
    invoke-virtual {p0}, Landroid/hardware/usb/UsbDevice;->getProductId()I

    move-result v1

    .line 111
    invoke-static {p0}, Lcom/felhr/deviceids/FTDISioIds;->isDeviceSupported(Landroid/hardware/usb/UsbDevice;)Z

    move-result v2

    const/4 v3, 0x1

    if-eqz v2, :cond_0

    return v3

    .line 113
    :cond_0
    invoke-static {v0, v1}, Lcom/felhr/deviceids/CP210xIds;->isDeviceSupported(II)Z

    move-result v2

    if-eqz v2, :cond_1

    return v3

    .line 115
    :cond_1
    invoke-static {v0, v1}, Lcom/felhr/deviceids/PL2303Ids;->isDeviceSupported(II)Z

    move-result v2

    if-eqz v2, :cond_2

    return v3

    .line 117
    :cond_2
    invoke-static {v0, v1}, Lcom/felhr/deviceids/CH34xIds;->isDeviceSupported(II)Z

    move-result v0

    if-eqz v0, :cond_3

    return v3

    .line 119
    :cond_3
    invoke-static {p0}, Lcom/felhr/usbserial/UsbSerialDevice;->isCdcDevice(Landroid/hardware/usb/UsbDevice;)Z

    move-result p0

    if-eqz p0, :cond_4

    return v3

    :cond_4
    const/4 p0, 0x0

    return p0
.end method


# virtual methods
.method public abstract close()V
.end method

.method public debug(Z)V
    .locals 1

    .line 292
    iget-object v0, p0, Lcom/felhr/usbserial/UsbSerialDevice;->serialBuffer:Lcom/felhr/usbserial/SerialBuffer;

    if-eqz v0, :cond_0

    .line 293
    invoke-virtual {v0, p1}, Lcom/felhr/usbserial/SerialBuffer;->debug(Z)V

    :cond_0
    return-void
.end method

.method public getDeviceId()I
    .locals 1

    .line 286
    iget-object v0, p0, Lcom/felhr/usbserial/UsbSerialDevice;->device:Landroid/hardware/usb/UsbDevice;

    invoke-virtual {v0}, Landroid/hardware/usb/UsbDevice;->getDeviceId()I

    move-result v0

    return v0
.end method

.method public getInitialBaudRate()I
    .locals 1

    const/4 v0, -0x1

    return v0
.end method

.method public getInputStream()Lcom/felhr/usbserial/SerialInputStream;
    .locals 2

    .line 264
    iget-boolean v0, p0, Lcom/felhr/usbserial/UsbSerialDevice;->asyncMode:Z

    if-nez v0, :cond_0

    .line 267
    iget-object v0, p0, Lcom/felhr/usbserial/UsbSerialDevice;->inputStream:Lcom/felhr/usbserial/SerialInputStream;

    return-object v0

    .line 265
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "InputStream only available in Sync mode. \nOpen the port with syncOpen()"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getOutputStream()Lcom/felhr/usbserial/SerialOutputStream;
    .locals 2

    .line 271
    iget-boolean v0, p0, Lcom/felhr/usbserial/UsbSerialDevice;->asyncMode:Z

    if-nez v0, :cond_0

    .line 274
    iget-object v0, p0, Lcom/felhr/usbserial/UsbSerialDevice;->outputStream:Lcom/felhr/usbserial/SerialOutputStream;

    return-object v0

    .line 272
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "OutputStream only available in Sync mode. \nOpen the port with syncOpen()"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getPid()I
    .locals 1

    .line 282
    iget-object v0, p0, Lcom/felhr/usbserial/UsbSerialDevice;->device:Landroid/hardware/usb/UsbDevice;

    invoke-virtual {v0}, Landroid/hardware/usb/UsbDevice;->getProductId()I

    move-result v0

    return v0
.end method

.method public getPortName()Ljava/lang/String;
    .locals 1

    .line 301
    iget-object v0, p0, Lcom/felhr/usbserial/UsbSerialDevice;->portName:Ljava/lang/String;

    return-object v0
.end method

.method public getVid()I
    .locals 1

    .line 278
    iget-object v0, p0, Lcom/felhr/usbserial/UsbSerialDevice;->device:Landroid/hardware/usb/UsbDevice;

    invoke-virtual {v0}, Landroid/hardware/usb/UsbDevice;->getVendorId()I

    move-result v0

    return v0
.end method

.method public isOpen()Z
    .locals 1

    .line 305
    iget-boolean v0, p0, Lcom/felhr/usbserial/UsbSerialDevice;->isOpen:Z

    return v0
.end method

.method protected killWorkingThread()V
    .locals 2

    .line 498
    sget-boolean v0, Lcom/felhr/usbserial/UsbSerialDevice;->mr1Version:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/felhr/usbserial/UsbSerialDevice;->workerThread:Lcom/felhr/usbserial/UsbSerialDevice$WorkerThread;

    if-eqz v0, :cond_0

    .line 500
    invoke-virtual {v0}, Lcom/felhr/usbserial/UsbSerialDevice$WorkerThread;->stopThread()V

    .line 501
    iput-object v1, p0, Lcom/felhr/usbserial/UsbSerialDevice;->workerThread:Lcom/felhr/usbserial/UsbSerialDevice$WorkerThread;

    goto :goto_0

    .line 502
    :cond_0
    sget-boolean v0, Lcom/felhr/usbserial/UsbSerialDevice;->mr1Version:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/felhr/usbserial/UsbSerialDevice;->readThread:Lcom/felhr/usbserial/UsbSerialDevice$ReadThread;

    if-eqz v0, :cond_1

    .line 504
    invoke-virtual {v0}, Lcom/felhr/usbserial/UsbSerialDevice$ReadThread;->stopThread()V

    .line 505
    iput-object v1, p0, Lcom/felhr/usbserial/UsbSerialDevice;->readThread:Lcom/felhr/usbserial/UsbSerialDevice$ReadThread;

    :cond_1
    :goto_0
    return-void
.end method

.method protected killWriteThread()V
    .locals 1

    .line 529
    iget-object v0, p0, Lcom/felhr/usbserial/UsbSerialDevice;->writeThread:Lcom/felhr/usbserial/UsbSerialDevice$WriteThread;

    if-eqz v0, :cond_0

    .line 531
    invoke-virtual {v0}, Lcom/felhr/usbserial/UsbSerialDevice$WriteThread;->stopThread()V

    const/4 v0, 0x0

    .line 532
    iput-object v0, p0, Lcom/felhr/usbserial/UsbSerialDevice;->writeThread:Lcom/felhr/usbserial/UsbSerialDevice$WriteThread;

    :cond_0
    return-void
.end method

.method public abstract open()Z
.end method

.method public read(Lcom/felhr/usbserial/UsbSerialInterface$UsbReadCallback;)I
    .locals 2

    .line 163
    iget-boolean v0, p0, Lcom/felhr/usbserial/UsbSerialDevice;->asyncMode:Z

    if-nez v0, :cond_0

    const/4 p1, -0x1

    return p1

    .line 166
    :cond_0
    sget-boolean v0, Lcom/felhr/usbserial/UsbSerialDevice;->mr1Version:Z

    if-eqz v0, :cond_1

    .line 168
    iget-object v0, p0, Lcom/felhr/usbserial/UsbSerialDevice;->workerThread:Lcom/felhr/usbserial/UsbSerialDevice$WorkerThread;

    if-eqz v0, :cond_2

    .line 169
    invoke-virtual {v0, p1}, Lcom/felhr/usbserial/UsbSerialDevice$WorkerThread;->setCallback(Lcom/felhr/usbserial/UsbSerialInterface$UsbReadCallback;)V

    .line 170
    iget-object p1, p0, Lcom/felhr/usbserial/UsbSerialDevice;->workerThread:Lcom/felhr/usbserial/UsbSerialDevice$WorkerThread;

    invoke-virtual {p1}, Lcom/felhr/usbserial/UsbSerialDevice$WorkerThread;->getUsbRequest()Landroid/hardware/usb/UsbRequest;

    move-result-object p1

    iget-object v0, p0, Lcom/felhr/usbserial/UsbSerialDevice;->serialBuffer:Lcom/felhr/usbserial/SerialBuffer;

    invoke-virtual {v0}, Lcom/felhr/usbserial/SerialBuffer;->getReadBuffer()Ljava/nio/ByteBuffer;

    move-result-object v0

    const/16 v1, 0x4000

    invoke-virtual {p1, v0, v1}, Landroid/hardware/usb/UsbRequest;->queue(Ljava/nio/ByteBuffer;I)Z

    goto :goto_0

    .line 174
    :cond_1
    iget-object v0, p0, Lcom/felhr/usbserial/UsbSerialDevice;->readThread:Lcom/felhr/usbserial/UsbSerialDevice$ReadThread;

    invoke-virtual {v0, p1}, Lcom/felhr/usbserial/UsbSerialDevice$ReadThread;->setCallback(Lcom/felhr/usbserial/UsbSerialInterface$UsbReadCallback;)V

    :cond_2
    :goto_0
    const/4 p1, 0x0

    return p1
.end method

.method protected restartWorkingThread()V
    .locals 1

    .line 514
    sget-boolean v0, Lcom/felhr/usbserial/UsbSerialDevice;->mr1Version:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/felhr/usbserial/UsbSerialDevice;->workerThread:Lcom/felhr/usbserial/UsbSerialDevice$WorkerThread;

    if-nez v0, :cond_0

    .line 516
    new-instance v0, Lcom/felhr/usbserial/UsbSerialDevice$WorkerThread;

    invoke-direct {v0, p0, p0}, Lcom/felhr/usbserial/UsbSerialDevice$WorkerThread;-><init>(Lcom/felhr/usbserial/UsbSerialDevice;Lcom/felhr/usbserial/UsbSerialDevice;)V

    iput-object v0, p0, Lcom/felhr/usbserial/UsbSerialDevice;->workerThread:Lcom/felhr/usbserial/UsbSerialDevice$WorkerThread;

    .line 517
    iget-object v0, p0, Lcom/felhr/usbserial/UsbSerialDevice;->workerThread:Lcom/felhr/usbserial/UsbSerialDevice$WorkerThread;

    invoke-virtual {v0}, Lcom/felhr/usbserial/UsbSerialDevice$WorkerThread;->start()V

    .line 518
    :goto_0
    iget-object v0, p0, Lcom/felhr/usbserial/UsbSerialDevice;->workerThread:Lcom/felhr/usbserial/UsbSerialDevice$WorkerThread;

    invoke-virtual {v0}, Lcom/felhr/usbserial/UsbSerialDevice$WorkerThread;->isAlive()Z

    move-result v0

    if-nez v0, :cond_1

    goto :goto_0

    .line 519
    :cond_0
    sget-boolean v0, Lcom/felhr/usbserial/UsbSerialDevice;->mr1Version:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/felhr/usbserial/UsbSerialDevice;->readThread:Lcom/felhr/usbserial/UsbSerialDevice$ReadThread;

    if-nez v0, :cond_1

    .line 521
    new-instance v0, Lcom/felhr/usbserial/UsbSerialDevice$ReadThread;

    invoke-direct {v0, p0, p0}, Lcom/felhr/usbserial/UsbSerialDevice$ReadThread;-><init>(Lcom/felhr/usbserial/UsbSerialDevice;Lcom/felhr/usbserial/UsbSerialDevice;)V

    iput-object v0, p0, Lcom/felhr/usbserial/UsbSerialDevice;->readThread:Lcom/felhr/usbserial/UsbSerialDevice$ReadThread;

    .line 522
    iget-object v0, p0, Lcom/felhr/usbserial/UsbSerialDevice;->readThread:Lcom/felhr/usbserial/UsbSerialDevice$ReadThread;

    invoke-virtual {v0}, Lcom/felhr/usbserial/UsbSerialDevice$ReadThread;->start()V

    .line 523
    :goto_1
    iget-object v0, p0, Lcom/felhr/usbserial/UsbSerialDevice;->readThread:Lcom/felhr/usbserial/UsbSerialDevice$ReadThread;

    invoke-virtual {v0}, Lcom/felhr/usbserial/UsbSerialDevice$ReadThread;->isAlive()Z

    move-result v0

    if-nez v0, :cond_1

    goto :goto_1

    :cond_1
    return-void
.end method

.method protected restartWriteThread()V
    .locals 2

    .line 538
    iget-object v0, p0, Lcom/felhr/usbserial/UsbSerialDevice;->writeThread:Lcom/felhr/usbserial/UsbSerialDevice$WriteThread;

    if-nez v0, :cond_0

    .line 540
    new-instance v0, Lcom/felhr/usbserial/UsbSerialDevice$WriteThread;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/felhr/usbserial/UsbSerialDevice$WriteThread;-><init>(Lcom/felhr/usbserial/UsbSerialDevice;Lcom/felhr/usbserial/UsbSerialDevice$1;)V

    iput-object v0, p0, Lcom/felhr/usbserial/UsbSerialDevice;->writeThread:Lcom/felhr/usbserial/UsbSerialDevice$WriteThread;

    .line 541
    iget-object v0, p0, Lcom/felhr/usbserial/UsbSerialDevice;->writeThread:Lcom/felhr/usbserial/UsbSerialDevice$WriteThread;

    invoke-virtual {v0}, Lcom/felhr/usbserial/UsbSerialDevice$WriteThread;->start()V

    .line 542
    :goto_0
    iget-object v0, p0, Lcom/felhr/usbserial/UsbSerialDevice;->writeThread:Lcom/felhr/usbserial/UsbSerialDevice$WriteThread;

    invoke-virtual {v0}, Lcom/felhr/usbserial/UsbSerialDevice$WriteThread;->isAlive()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    return-void
.end method

.method public abstract setBaudRate(I)V
.end method

.method public abstract setBreak(Z)V
.end method

.method public abstract setDataBits(I)V
.end method

.method public abstract setFlowControl(I)V
.end method

.method public setInitialBaudRate(I)V
    .locals 0

    return-void
.end method

.method public abstract setParity(I)V
.end method

.method public setPortName(Ljava/lang/String;)V
    .locals 0

    .line 297
    iput-object p1, p0, Lcom/felhr/usbserial/UsbSerialDevice;->portName:Ljava/lang/String;

    return-void
.end method

.method public abstract setStopBits(I)V
.end method

.method protected setSyncParams(Landroid/hardware/usb/UsbEndpoint;Landroid/hardware/usb/UsbEndpoint;)V
    .locals 0

    .line 477
    iput-object p1, p0, Lcom/felhr/usbserial/UsbSerialDevice;->inEndpoint:Landroid/hardware/usb/UsbEndpoint;

    .line 478
    iput-object p2, p0, Lcom/felhr/usbserial/UsbSerialDevice;->outEndpoint:Landroid/hardware/usb/UsbEndpoint;

    return-void
.end method

.method protected setThreadsParams(Landroid/hardware/usb/UsbRequest;Landroid/hardware/usb/UsbEndpoint;)V
    .locals 1

    .line 483
    iget-object v0, p0, Lcom/felhr/usbserial/UsbSerialDevice;->writeThread:Lcom/felhr/usbserial/UsbSerialDevice$WriteThread;

    invoke-virtual {v0, p2}, Lcom/felhr/usbserial/UsbSerialDevice$WriteThread;->setUsbEndpoint(Landroid/hardware/usb/UsbEndpoint;)V

    .line 484
    sget-boolean p2, Lcom/felhr/usbserial/UsbSerialDevice;->mr1Version:Z

    if-eqz p2, :cond_0

    .line 486
    iget-object p2, p0, Lcom/felhr/usbserial/UsbSerialDevice;->workerThread:Lcom/felhr/usbserial/UsbSerialDevice$WorkerThread;

    invoke-virtual {p2, p1}, Lcom/felhr/usbserial/UsbSerialDevice$WorkerThread;->setUsbRequest(Landroid/hardware/usb/UsbRequest;)V

    goto :goto_0

    .line 489
    :cond_0
    iget-object p2, p0, Lcom/felhr/usbserial/UsbSerialDevice;->readThread:Lcom/felhr/usbserial/UsbSerialDevice$ReadThread;

    invoke-virtual {p1}, Landroid/hardware/usb/UsbRequest;->getEndpoint()Landroid/hardware/usb/UsbEndpoint;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/felhr/usbserial/UsbSerialDevice$ReadThread;->setUsbEndpoint(Landroid/hardware/usb/UsbEndpoint;)V

    :goto_0
    return-void
.end method

.method public abstract syncClose()V
.end method

.method public abstract syncOpen()Z
.end method

.method public syncRead([BI)I
    .locals 3

    .line 209
    iget-boolean v0, p0, Lcom/felhr/usbserial/UsbSerialDevice;->asyncMode:Z

    if-eqz v0, :cond_0

    const/4 p1, -0x1

    return p1

    :cond_0
    if-nez p1, :cond_1

    const/4 p1, 0x0

    return p1

    .line 217
    :cond_1
    iget-object v0, p0, Lcom/felhr/usbserial/UsbSerialDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v1, p0, Lcom/felhr/usbserial/UsbSerialDevice;->inEndpoint:Landroid/hardware/usb/UsbEndpoint;

    array-length v2, p1

    invoke-virtual {v0, v1, p1, v2, p2}, Landroid/hardware/usb/UsbDeviceConnection;->bulkTransfer(Landroid/hardware/usb/UsbEndpoint;[BII)I

    move-result p1

    return p1
.end method

.method public syncRead([BIII)I
    .locals 6

    .line 238
    iget-boolean v0, p0, Lcom/felhr/usbserial/UsbSerialDevice;->asyncMode:Z

    if-eqz v0, :cond_0

    const/4 p1, -0x1

    return p1

    :cond_0
    if-nez p1, :cond_1

    const/4 p1, 0x0

    return p1

    .line 246
    :cond_1
    iget-object v0, p0, Lcom/felhr/usbserial/UsbSerialDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v1, p0, Lcom/felhr/usbserial/UsbSerialDevice;->inEndpoint:Landroid/hardware/usb/UsbEndpoint;

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Landroid/hardware/usb/UsbDeviceConnection;->bulkTransfer(Landroid/hardware/usb/UsbEndpoint;[BIII)I

    move-result p1

    return p1
.end method

.method public syncWrite([BI)I
    .locals 3

    .line 194
    iget-boolean v0, p0, Lcom/felhr/usbserial/UsbSerialDevice;->asyncMode:Z

    if-nez v0, :cond_1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return p1

    .line 199
    :cond_0
    iget-object v0, p0, Lcom/felhr/usbserial/UsbSerialDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v1, p0, Lcom/felhr/usbserial/UsbSerialDevice;->outEndpoint:Landroid/hardware/usb/UsbEndpoint;

    array-length v2, p1

    invoke-virtual {v0, v1, p1, v2, p2}, Landroid/hardware/usb/UsbDeviceConnection;->bulkTransfer(Landroid/hardware/usb/UsbEndpoint;[BII)I

    move-result p1

    return p1

    :cond_1
    const/4 p1, -0x1

    return p1
.end method

.method public syncWrite([BIII)I
    .locals 6

    .line 223
    iget-boolean v0, p0, Lcom/felhr/usbserial/UsbSerialDevice;->asyncMode:Z

    if-nez v0, :cond_1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return p1

    .line 228
    :cond_0
    iget-object v0, p0, Lcom/felhr/usbserial/UsbSerialDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v1, p0, Lcom/felhr/usbserial/UsbSerialDevice;->outEndpoint:Landroid/hardware/usb/UsbEndpoint;

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Landroid/hardware/usb/UsbDeviceConnection;->bulkTransfer(Landroid/hardware/usb/UsbEndpoint;[BIII)I

    move-result p1

    return p1

    :cond_1
    const/4 p1, -0x1

    return p1
.end method

.method public write([B)V
    .locals 1

    .line 132
    iget-boolean v0, p0, Lcom/felhr/usbserial/UsbSerialDevice;->asyncMode:Z

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/felhr/usbserial/UsbSerialDevice;->serialBuffer:Lcom/felhr/usbserial/SerialBuffer;

    invoke-virtual {v0, p1}, Lcom/felhr/usbserial/SerialBuffer;->putWriteBuffer([B)V

    :cond_0
    return-void
.end method
