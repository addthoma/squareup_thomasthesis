.class public interface abstract Lcom/helpshift/support/contracts/ScreenshotPreviewListener;
.super Ljava/lang/Object;
.source "ScreenshotPreviewListener.java"


# virtual methods
.method public abstract addScreenshot(Lcom/helpshift/conversation/dto/ImagePickerFile;)V
.end method

.method public abstract changeScreenshot(Landroid/os/Bundle;)V
.end method

.method public abstract removeScreenshot()V
.end method

.method public abstract removeScreenshotPreviewFragment()V
.end method

.method public abstract sendScreenshot(Lcom/helpshift/conversation/dto/ImagePickerFile;Ljava/lang/String;)V
.end method
