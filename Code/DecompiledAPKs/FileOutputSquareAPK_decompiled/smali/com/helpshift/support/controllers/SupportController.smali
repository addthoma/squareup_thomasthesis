.class public Lcom/helpshift/support/controllers/SupportController;
.super Ljava/lang/Object;
.source "SupportController.java"

# interfaces
.implements Lcom/helpshift/support/contracts/SearchResultListener;
.implements Lcom/helpshift/support/contracts/ScreenshotPreviewListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "Helpshift_SupportContr"


# instance fields
.field private final KEY_CONVERSATION_ADD_TO_BACK_STACK:Ljava/lang/String;

.field private final KEY_CONVERSATION_BUNDLE:Ljava/lang/String;

.field private final KEY_SUPPORT_CONTROLLER_STARTED_STATE:Ljava/lang/String;

.field private final bundle:Landroid/os/Bundle;

.field private final context:Landroid/content/Context;

.field private conversationAddToBackStack:Z

.field private conversationBundle:Landroid/os/Bundle;

.field private fragmentManager:Landroidx/fragment/app/FragmentManager;

.field private isControllerStarted:Z

.field private searchPerformed:Z

.field private sourceSearchQuery:Ljava/lang/String;

.field private supportMode:I

.field private final supportScreenView:Lcom/helpshift/support/contracts/SupportScreenView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/helpshift/support/contracts/SupportScreenView;Landroidx/fragment/app/FragmentManager;Landroid/os/Bundle;)V
    .locals 1

    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "key_support_controller_started"

    .line 71
    iput-object v0, p0, Lcom/helpshift/support/controllers/SupportController;->KEY_SUPPORT_CONTROLLER_STARTED_STATE:Ljava/lang/String;

    const-string v0, "key_conversation_bundle"

    .line 72
    iput-object v0, p0, Lcom/helpshift/support/controllers/SupportController;->KEY_CONVERSATION_BUNDLE:Ljava/lang/String;

    const-string v0, "key_conversation_add_to_back_stack"

    .line 73
    iput-object v0, p0, Lcom/helpshift/support/controllers/SupportController;->KEY_CONVERSATION_ADD_TO_BACK_STACK:Ljava/lang/String;

    const/4 v0, 0x0

    .line 81
    iput-boolean v0, p0, Lcom/helpshift/support/controllers/SupportController;->searchPerformed:Z

    .line 87
    iput-object p1, p0, Lcom/helpshift/support/controllers/SupportController;->context:Landroid/content/Context;

    .line 88
    iput-object p2, p0, Lcom/helpshift/support/controllers/SupportController;->supportScreenView:Lcom/helpshift/support/contracts/SupportScreenView;

    .line 89
    iput-object p3, p0, Lcom/helpshift/support/controllers/SupportController;->fragmentManager:Landroidx/fragment/app/FragmentManager;

    .line 90
    iput-object p4, p0, Lcom/helpshift/support/controllers/SupportController;->bundle:Landroid/os/Bundle;

    return-void
.end method

.method private clearConversationStack()V
    .locals 5

    .line 615
    iget-object v0, p0, Lcom/helpshift/support/controllers/SupportController;->fragmentManager:Landroidx/fragment/app/FragmentManager;

    invoke-virtual {v0}, Landroidx/fragment/app/FragmentManager;->getFragments()Ljava/util/List;

    move-result-object v0

    .line 618
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    sub-int/2addr v1, v2

    :goto_0
    if-ltz v1, :cond_3

    .line 619
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroidx/fragment/app/Fragment;

    .line 622
    instance-of v4, v3, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;

    if-nez v4, :cond_0

    instance-of v4, v3, Lcom/helpshift/support/conversations/BaseConversationFragment;

    if-nez v4, :cond_0

    instance-of v4, v3, Lcom/helpshift/support/conversations/usersetup/UserSetupFragment;

    if-nez v4, :cond_0

    instance-of v4, v3, Lcom/helpshift/support/conversations/AuthenticationFailureFragment;

    if-eqz v4, :cond_2

    :cond_0
    if-nez v1, :cond_1

    .line 628
    iget-object v4, p0, Lcom/helpshift/support/controllers/SupportController;->fragmentManager:Landroidx/fragment/app/FragmentManager;

    invoke-static {v4, v3}, Lcom/helpshift/support/util/FragmentUtil;->removeFragment(Landroidx/fragment/app/FragmentManager;Landroidx/fragment/app/Fragment;)V

    .line 633
    iget-object v4, p0, Lcom/helpshift/support/controllers/SupportController;->fragmentManager:Landroidx/fragment/app/FragmentManager;

    invoke-virtual {v4}, Landroidx/fragment/app/FragmentManager;->getFragments()Ljava/util/List;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 634
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_2

    .line 635
    iget-object v4, p0, Lcom/helpshift/support/controllers/SupportController;->fragmentManager:Landroidx/fragment/app/FragmentManager;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Lcom/helpshift/support/util/FragmentUtil;->popBackStack(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_1

    .line 643
    :cond_1
    iget-object v4, p0, Lcom/helpshift/support/controllers/SupportController;->fragmentManager:Landroidx/fragment/app/FragmentManager;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Lcom/helpshift/support/util/FragmentUtil;->popBackStack(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    :cond_2
    :goto_1
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 650
    :cond_3
    iget-object v0, p0, Lcom/helpshift/support/controllers/SupportController;->fragmentManager:Landroidx/fragment/app/FragmentManager;

    const-string v1, "HSConversationFragment"

    invoke-virtual {v0, v1}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 652
    iget-object v1, p0, Lcom/helpshift/support/controllers/SupportController;->fragmentManager:Landroidx/fragment/app/FragmentManager;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/helpshift/support/util/FragmentUtil;->popBackStackImmediate(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    const/4 v0, 0x1

    goto :goto_2

    :cond_4
    const/4 v0, 0x0

    :goto_2
    if-nez v0, :cond_5

    .line 656
    iput-boolean v2, p0, Lcom/helpshift/support/controllers/SupportController;->conversationAddToBackStack:Z

    :cond_5
    return-void
.end method

.method private handleCustomContactUsFlows()Z
    .locals 2

    .line 465
    invoke-static {}, Lcom/helpshift/util/HelpshiftContext;->getCoreApi()Lcom/helpshift/CoreApi;

    move-result-object v0

    invoke-interface {v0}, Lcom/helpshift/CoreApi;->getActiveConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v0

    if-nez v0, :cond_0

    .line 467
    iget-object v0, p0, Lcom/helpshift/support/controllers/SupportController;->fragmentManager:Landroidx/fragment/app/FragmentManager;

    invoke-static {v0}, Lcom/helpshift/support/util/FragmentUtil;->getFaqFlowFragment(Landroidx/fragment/app/FragmentManager;)Lcom/helpshift/support/fragments/FaqFlowFragment;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 469
    invoke-virtual {v0}, Lcom/helpshift/support/fragments/FaqFlowFragment;->getCustomContactUsFlows()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 470
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    .line 471
    invoke-virtual {p0, v0, v1}, Lcom/helpshift/support/controllers/SupportController;->startDynamicForm(Ljava/util/List;Z)V

    return v1

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method private replaceConversationFlow(Landroid/os/Bundle;)V
    .locals 3

    const-string v0, "conversationIdInPush"

    .line 127
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 128
    iget-object v1, p0, Lcom/helpshift/support/controllers/SupportController;->conversationBundle:Landroid/os/Bundle;

    if-eqz v1, :cond_0

    const-string v2, "issueId"

    .line 129
    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 132
    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    .line 135
    iget-object v2, p0, Lcom/helpshift/support/controllers/SupportController;->fragmentManager:Landroidx/fragment/app/FragmentManager;

    invoke-virtual {v2}, Landroidx/fragment/app/FragmentManager;->getFragments()Ljava/util/List;

    move-result-object v2

    if-eqz v0, :cond_1

    .line 141
    invoke-direct {p0}, Lcom/helpshift/support/controllers/SupportController;->clearConversationStack()V

    goto :goto_1

    .line 147
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_3

    .line 152
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    sub-int/2addr v0, v1

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroidx/fragment/app/Fragment;

    .line 155
    instance-of v2, v0, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;

    if-eqz v2, :cond_2

    return-void

    .line 159
    :cond_2
    instance-of v0, v0, Lcom/helpshift/support/conversations/BaseConversationFragment;

    xor-int/2addr v1, v0

    :cond_3
    :goto_1
    if-eqz v1, :cond_4

    .line 169
    iput-object p1, p0, Lcom/helpshift/support/controllers/SupportController;->conversationBundle:Landroid/os/Bundle;

    .line 170
    invoke-virtual {p0}, Lcom/helpshift/support/controllers/SupportController;->startConversationFlow()V

    :cond_4
    return-void
.end method

.method private sendTicketAvoidedEvent()V
    .locals 5

    .line 498
    iget-object v0, p0, Lcom/helpshift/support/controllers/SupportController;->fragmentManager:Landroidx/fragment/app/FragmentManager;

    invoke-static {v0}, Lcom/helpshift/support/util/FragmentUtil;->getSingleQuestionFragment(Landroidx/fragment/app/FragmentManager;)Lcom/helpshift/support/fragments/SingleQuestionFragment;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 500
    invoke-virtual {v0}, Lcom/helpshift/support/fragments/SingleQuestionFragment;->getQuestionId()Ljava/lang/String;

    move-result-object v0

    .line 501
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 502
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    const-string v2, "id"

    .line 503
    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 504
    invoke-static {}, Lcom/helpshift/util/HelpshiftContext;->getCoreApi()Lcom/helpshift/CoreApi;

    move-result-object v0

    invoke-interface {v0}, Lcom/helpshift/CoreApi;->getUserManagerDM()Lcom/helpshift/account/domainmodel/UserManagerDM;

    move-result-object v0

    invoke-virtual {v0}, Lcom/helpshift/account/domainmodel/UserManagerDM;->getActiveUser()Lcom/helpshift/account/domainmodel/UserDM;

    move-result-object v0

    invoke-virtual {v0}, Lcom/helpshift/account/domainmodel/UserDM;->getLocalId()Ljava/lang/Long;

    move-result-object v0

    .line 506
    invoke-static {}, Lcom/helpshift/util/HelpshiftContext;->getPlatform()Lcom/helpshift/common/platform/Platform;

    move-result-object v2

    invoke-interface {v2}, Lcom/helpshift/common/platform/Platform;->getConversationInboxDAO()Lcom/helpshift/conversation/dao/ConversationInboxDAO;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-interface {v2, v3, v4}, Lcom/helpshift/conversation/dao/ConversationInboxDAO;->getDescriptionDetail(J)Lcom/helpshift/conversation/dto/ConversationDetailDTO;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 508
    iget-object v0, v0, Lcom/helpshift/conversation/dto/ConversationDetailDTO;->title:Ljava/lang/String;

    const-string v2, "str"

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 510
    :cond_0
    invoke-static {}, Lcom/helpshift/util/HelpshiftContext;->getCoreApi()Lcom/helpshift/CoreApi;

    move-result-object v0

    invoke-interface {v0}, Lcom/helpshift/CoreApi;->getAnalyticsEventDM()Lcom/helpshift/analytics/domainmodel/AnalyticsEventDM;

    move-result-object v0

    sget-object v2, Lcom/helpshift/analytics/AnalyticsEventType;->TICKET_AVOIDED:Lcom/helpshift/analytics/AnalyticsEventType;

    invoke-virtual {v0, v2, v1}, Lcom/helpshift/analytics/domainmodel/AnalyticsEventDM;->pushEvent(Lcom/helpshift/analytics/AnalyticsEventType;Ljava/util/Map;)V

    :cond_1
    return-void
.end method

.method private showConversationFragment(ZLjava/lang/Long;)V
    .locals 7

    .line 356
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Starting conversation fragment: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Helpshift_SupportContr"

    invoke-static {v1, v0}, Lcom/helpshift/util/HSLogger;->d(Ljava/lang/String;Ljava/lang/String;)V

    if-nez p1, :cond_1

    if-nez p2, :cond_0

    return-void

    .line 364
    :cond_0
    iget-object v0, p0, Lcom/helpshift/support/controllers/SupportController;->conversationBundle:Landroid/os/Bundle;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    const-string p2, "issueId"

    invoke-virtual {v0, p2, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 366
    :cond_1
    iget-object p2, p0, Lcom/helpshift/support/controllers/SupportController;->conversationBundle:Landroid/os/Bundle;

    const-string v0, "show_conv_history"

    invoke-virtual {p2, v0, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 367
    iget-object p1, p0, Lcom/helpshift/support/controllers/SupportController;->conversationBundle:Landroid/os/Bundle;

    invoke-static {p1}, Lcom/helpshift/support/conversations/ConversationalFragment;->newInstance(Landroid/os/Bundle;)Lcom/helpshift/support/conversations/ConversationalFragment;

    move-result-object v2

    const/4 p1, 0x0

    .line 369
    iget-boolean p2, p0, Lcom/helpshift/support/controllers/SupportController;->conversationAddToBackStack:Z

    if-eqz p2, :cond_2

    .line 370
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    .line 373
    invoke-direct {p0}, Lcom/helpshift/support/controllers/SupportController;->clearConversationStack()V

    :cond_2
    move-object v4, p1

    .line 375
    iget-object v0, p0, Lcom/helpshift/support/controllers/SupportController;->fragmentManager:Landroidx/fragment/app/FragmentManager;

    sget v1, Lcom/helpshift/R$id;->flow_fragment_container:I

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v3, "HSConversationFragment"

    invoke-static/range {v0 .. v6}, Lcom/helpshift/support/util/FragmentUtil;->startFragment(Landroidx/fragment/app/FragmentManager;ILandroidx/fragment/app/Fragment;Ljava/lang/String;Ljava/lang/String;ZZ)V

    return-void
.end method

.method private showNewConversationFragment()V
    .locals 8

    const-string v0, "Helpshift_SupportContr"

    const-string v1, "Starting new conversation fragment"

    .line 334
    invoke-static {v0, v1}, Lcom/helpshift/util/HSLogger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 335
    iget-object v0, p0, Lcom/helpshift/support/controllers/SupportController;->conversationBundle:Landroid/os/Bundle;

    iget-boolean v1, p0, Lcom/helpshift/support/controllers/SupportController;->searchPerformed:Z

    const-string v2, "search_performed"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 336
    iget-object v0, p0, Lcom/helpshift/support/controllers/SupportController;->conversationBundle:Landroid/os/Bundle;

    iget-object v1, p0, Lcom/helpshift/support/controllers/SupportController;->sourceSearchQuery:Ljava/lang/String;

    const-string v2, "source_search_query"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 337
    iget-object v0, p0, Lcom/helpshift/support/controllers/SupportController;->conversationBundle:Landroid/os/Bundle;

    invoke-static {v0}, Lcom/helpshift/support/conversations/NewConversationFragment;->newInstance(Landroid/os/Bundle;)Lcom/helpshift/support/conversations/NewConversationFragment;

    move-result-object v3

    .line 339
    iget-boolean v0, p0, Lcom/helpshift/support/controllers/SupportController;->conversationAddToBackStack:Z

    if-eqz v0, :cond_0

    .line 340
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    .line 343
    invoke-direct {p0}, Lcom/helpshift/support/controllers/SupportController;->clearConversationStack()V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    move-object v5, v0

    .line 345
    iget-object v1, p0, Lcom/helpshift/support/controllers/SupportController;->fragmentManager:Landroidx/fragment/app/FragmentManager;

    sget v2, Lcom/helpshift/R$id;->flow_fragment_container:I

    const/4 v6, 0x0

    const/4 v7, 0x0

    const-string v4, "HSNewConversationFragment"

    invoke-static/range {v1 .. v7}, Lcom/helpshift/support/util/FragmentUtil;->startFragment(Landroidx/fragment/app/FragmentManager;ILandroidx/fragment/app/Fragment;Ljava/lang/String;Ljava/lang/String;ZZ)V

    return-void
.end method

.method private startConversationFlowInternal()V
    .locals 10

    .line 212
    iget-object v0, p0, Lcom/helpshift/support/controllers/SupportController;->conversationBundle:Landroid/os/Bundle;

    if-nez v0, :cond_0

    .line 213
    iget-object v0, p0, Lcom/helpshift/support/controllers/SupportController;->bundle:Landroid/os/Bundle;

    iput-object v0, p0, Lcom/helpshift/support/controllers/SupportController;->conversationBundle:Landroid/os/Bundle;

    .line 216
    :cond_0
    invoke-static {}, Lcom/helpshift/util/HelpshiftContext;->getCoreApi()Lcom/helpshift/CoreApi;

    move-result-object v0

    invoke-interface {v0}, Lcom/helpshift/CoreApi;->getSDKConfigurationDM()Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    move-result-object v0

    const-string v1, "disableInAppConversation"

    invoke-virtual {v0, v1}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 217
    invoke-static {}, Lcom/helpshift/util/HelpshiftContext;->getCoreApi()Lcom/helpshift/CoreApi;

    move-result-object v1

    invoke-interface {v1}, Lcom/helpshift/CoreApi;->getSDKConfigurationDM()Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    move-result-object v1

    invoke-virtual {v1}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->shouldShowConversationHistory()Z

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v1, :cond_1

    if-nez v0, :cond_1

    .line 220
    invoke-direct {p0, v3, v2}, Lcom/helpshift/support/controllers/SupportController;->showConversationFragment(ZLjava/lang/Long;)V

    return-void

    .line 224
    :cond_1
    iget-object v1, p0, Lcom/helpshift/support/controllers/SupportController;->conversationBundle:Landroid/os/Bundle;

    const-wide/16 v4, 0x0

    const-string v6, "conversationIdInPush"

    invoke-virtual {v1, v6, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v7

    const/4 v1, 0x0

    cmp-long v9, v7, v4

    if-eqz v9, :cond_2

    .line 226
    iget-object v4, p0, Lcom/helpshift/support/controllers/SupportController;->conversationBundle:Landroid/os/Bundle;

    invoke-virtual {v4, v6}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 227
    invoke-static {}, Lcom/helpshift/util/HelpshiftContext;->getCoreApi()Lcom/helpshift/CoreApi;

    move-result-object v4

    invoke-interface {v4}, Lcom/helpshift/CoreApi;->getConversationInboxDM()Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    move-result-object v4

    .line 228
    invoke-virtual {v4, v7, v8}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->shouldOpenConversationFromNotification(J)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 231
    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lcom/helpshift/support/controllers/SupportController;->showConversationFragment(ZLjava/lang/Long;)V

    return-void

    :cond_2
    if-nez v0, :cond_3

    .line 238
    invoke-static {}, Lcom/helpshift/util/HelpshiftContext;->getCoreApi()Lcom/helpshift/CoreApi;

    move-result-object v0

    invoke-interface {v0}, Lcom/helpshift/CoreApi;->getActiveConversationOrPreIssue()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 240
    iget-object v2, v0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localId:Ljava/lang/Long;

    :cond_3
    if-nez v2, :cond_7

    .line 245
    invoke-static {}, Lcom/helpshift/support/flows/CustomContactUsFlowListHolder;->getFlowList()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 246
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    goto :goto_0

    .line 255
    :cond_4
    iget-object v1, p0, Lcom/helpshift/support/controllers/SupportController;->fragmentManager:Landroidx/fragment/app/FragmentManager;

    invoke-virtual {v1}, Landroidx/fragment/app/FragmentManager;->getBackStackEntryCount()I

    move-result v1

    sub-int/2addr v1, v3

    .line 256
    invoke-virtual {p0}, Lcom/helpshift/support/controllers/SupportController;->getFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroidx/fragment/app/FragmentManager;->getBackStackEntryAt(I)Landroidx/fragment/app/FragmentManager$BackStackEntry;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 258
    invoke-interface {v1}, Landroidx/fragment/app/FragmentManager$BackStackEntry;->getName()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 259
    const-class v2, Lcom/helpshift/support/conversations/ConversationFragment;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 260
    iget-object v2, p0, Lcom/helpshift/support/controllers/SupportController;->fragmentManager:Landroidx/fragment/app/FragmentManager;

    invoke-static {v2, v1}, Lcom/helpshift/support/util/FragmentUtil;->popBackStackImmediate(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    .line 263
    :cond_5
    invoke-virtual {p0, v0, v3}, Lcom/helpshift/support/controllers/SupportController;->startDynamicForm(Ljava/util/List;Z)V

    goto :goto_1

    .line 247
    :cond_6
    :goto_0
    invoke-direct {p0}, Lcom/helpshift/support/controllers/SupportController;->showNewConversationFragment()V

    goto :goto_1

    .line 267
    :cond_7
    invoke-direct {p0, v1, v2}, Lcom/helpshift/support/controllers/SupportController;->showConversationFragment(ZLjava/lang/Long;)V

    :goto_1
    return-void
.end method


# virtual methods
.method public actionDone()V
    .locals 6

    .line 480
    invoke-direct {p0}, Lcom/helpshift/support/controllers/SupportController;->sendTicketAvoidedEvent()V

    .line 482
    invoke-static {}, Lcom/helpshift/util/HelpshiftContext;->getCoreApi()Lcom/helpshift/CoreApi;

    move-result-object v0

    invoke-interface {v0}, Lcom/helpshift/CoreApi;->getUserManagerDM()Lcom/helpshift/account/domainmodel/UserManagerDM;

    move-result-object v0

    invoke-virtual {v0}, Lcom/helpshift/account/domainmodel/UserManagerDM;->getActiveUser()Lcom/helpshift/account/domainmodel/UserDM;

    move-result-object v0

    invoke-virtual {v0}, Lcom/helpshift/account/domainmodel/UserDM;->getLocalId()Ljava/lang/Long;

    move-result-object v0

    .line 483
    new-instance v1, Lcom/helpshift/conversation/dto/ConversationDetailDTO;

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    const-string v4, ""

    const/4 v5, 0x0

    invoke-direct {v1, v4, v2, v3, v5}, Lcom/helpshift/conversation/dto/ConversationDetailDTO;-><init>(Ljava/lang/String;JI)V

    .line 485
    invoke-static {}, Lcom/helpshift/util/HelpshiftContext;->getPlatform()Lcom/helpshift/common/platform/Platform;

    move-result-object v2

    invoke-interface {v2}, Lcom/helpshift/common/platform/Platform;->getConversationInboxDAO()Lcom/helpshift/conversation/dao/ConversationInboxDAO;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-interface {v2, v3, v4, v1}, Lcom/helpshift/conversation/dao/ConversationInboxDAO;->saveDescriptionDetail(JLcom/helpshift/conversation/dto/ConversationDetailDTO;)V

    .line 487
    invoke-static {}, Lcom/helpshift/util/HelpshiftContext;->getPlatform()Lcom/helpshift/common/platform/Platform;

    move-result-object v1

    invoke-interface {v1}, Lcom/helpshift/common/platform/Platform;->getConversationInboxDAO()Lcom/helpshift/conversation/dao/ConversationInboxDAO;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const/4 v0, 0x0

    invoke-interface {v1, v2, v3, v0}, Lcom/helpshift/conversation/dao/ConversationInboxDAO;->saveImageAttachment(JLcom/helpshift/conversation/dto/ImagePickerFile;)V

    .line 488
    invoke-virtual {p0}, Lcom/helpshift/support/controllers/SupportController;->getSupportMode()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 489
    iget-object v0, p0, Lcom/helpshift/support/controllers/SupportController;->supportScreenView:Lcom/helpshift/support/contracts/SupportScreenView;

    invoke-interface {v0}, Lcom/helpshift/support/contracts/SupportScreenView;->exitSdkSession()V

    goto :goto_0

    .line 492
    :cond_0
    invoke-virtual {p0}, Lcom/helpshift/support/controllers/SupportController;->getFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    const-class v1, Lcom/helpshift/support/conversations/NewConversationFragment;

    .line 493
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    .line 492
    invoke-static {v0, v1}, Lcom/helpshift/support/util/FragmentUtil;->popBackStackImmediate(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method public addScreenshot(Lcom/helpshift/conversation/dto/ImagePickerFile;)V
    .locals 2

    .line 555
    iget-object v0, p0, Lcom/helpshift/support/controllers/SupportController;->fragmentManager:Landroidx/fragment/app/FragmentManager;

    const-class v1, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/helpshift/support/util/FragmentUtil;->popBackStack(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    .line 556
    iget-object v0, p0, Lcom/helpshift/support/controllers/SupportController;->fragmentManager:Landroidx/fragment/app/FragmentManager;

    const-string v1, "HSNewConversationFragment"

    .line 557
    invoke-virtual {v0, v1}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/helpshift/support/conversations/NewConversationFragment;

    if-eqz v0, :cond_0

    .line 559
    sget-object v1, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment$ScreenshotAction;->ADD:Lcom/helpshift/support/fragments/ScreenshotPreviewFragment$ScreenshotAction;

    invoke-virtual {v0, v1, p1}, Lcom/helpshift/support/conversations/NewConversationFragment;->handleScreenshotAction(Lcom/helpshift/support/fragments/ScreenshotPreviewFragment$ScreenshotAction;Lcom/helpshift/conversation/dto/ImagePickerFile;)Z

    :cond_0
    return-void
.end method

.method public changeScreenshot(Landroid/os/Bundle;)V
    .locals 2

    .line 585
    iget-object v0, p0, Lcom/helpshift/support/controllers/SupportController;->supportScreenView:Lcom/helpshift/support/contracts/SupportScreenView;

    const/4 v1, 0x1

    invoke-interface {v0, v1, p1}, Lcom/helpshift/support/contracts/SupportScreenView;->launchImagePicker(ZLandroid/os/Bundle;)V

    return-void
.end method

.method public getFragmentManager()Landroidx/fragment/app/FragmentManager;
    .locals 1

    .line 444
    iget-object v0, p0, Lcom/helpshift/support/controllers/SupportController;->fragmentManager:Landroidx/fragment/app/FragmentManager;

    return-object v0
.end method

.method public getSupportMode()I
    .locals 1

    .line 440
    iget v0, p0, Lcom/helpshift/support/controllers/SupportController;->supportMode:I

    return v0
.end method

.method public onAdminSuggestedQuestionSelected(Ljava/lang/String;Ljava/lang/String;Lcom/helpshift/support/fragments/SingleQuestionFragment$QuestionReadListener;)V
    .locals 3

    .line 542
    iget-object v0, p0, Lcom/helpshift/support/controllers/SupportController;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/helpshift/support/util/Styles;->isTablet(Landroid/content/Context;)Z

    move-result v0

    .line 543
    iget-object v1, p0, Lcom/helpshift/support/controllers/SupportController;->bundle:Landroid/os/Bundle;

    const-string v2, "questionPublishId"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 544
    iget-object p1, p0, Lcom/helpshift/support/controllers/SupportController;->bundle:Landroid/os/Bundle;

    const-string v1, "questionLanguage"

    invoke-virtual {p1, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 545
    iget-object p1, p0, Lcom/helpshift/support/controllers/SupportController;->fragmentManager:Landroidx/fragment/app/FragmentManager;

    sget p2, Lcom/helpshift/R$id;->flow_fragment_container:I

    iget-object v1, p0, Lcom/helpshift/support/controllers/SupportController;->bundle:Landroid/os/Bundle;

    const/4 v2, 0x3

    .line 548
    invoke-static {v1, v2, v0, p3}, Lcom/helpshift/support/fragments/SingleQuestionFragment;->newInstance(Landroid/os/Bundle;IZLcom/helpshift/support/fragments/SingleQuestionFragment$QuestionReadListener;)Lcom/helpshift/support/fragments/SingleQuestionFragment;

    move-result-object p3

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 545
    invoke-static {p1, p2, p3, v0, v1}, Lcom/helpshift/support/util/FragmentUtil;->startFragmentWithBackStack(Landroidx/fragment/app/FragmentManager;ILandroidx/fragment/app/Fragment;Ljava/lang/String;Z)V

    return-void
.end method

.method public onAuthenticationFailure()V
    .locals 0

    .line 272
    invoke-virtual {p0}, Lcom/helpshift/support/controllers/SupportController;->showAuthenticationFailureFragment()V

    return-void
.end method

.method public onContactUsClicked(Ljava/lang/String;)V
    .locals 1

    .line 448
    invoke-direct {p0}, Lcom/helpshift/support/controllers/SupportController;->handleCustomContactUsFlows()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 452
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 453
    iput-object p1, p0, Lcom/helpshift/support/controllers/SupportController;->sourceSearchQuery:Ljava/lang/String;

    .line 456
    :cond_1
    iget-object p1, p0, Lcom/helpshift/support/controllers/SupportController;->bundle:Landroid/os/Bundle;

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/helpshift/support/controllers/SupportController;->startConversationFlow(Landroid/os/Bundle;Z)V

    return-void
.end method

.method public onFragmentManagerUpdate(Landroidx/fragment/app/FragmentManager;)V
    .locals 0

    .line 94
    iput-object p1, p0, Lcom/helpshift/support/controllers/SupportController;->fragmentManager:Landroidx/fragment/app/FragmentManager;

    return-void
.end method

.method public onNewIntent(Landroid/os/Bundle;)V
    .locals 3

    const-string v0, "support_mode"

    .line 594
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v2, 0x4

    if-eq v0, v2, :cond_0

    .line 604
    invoke-static {}, Lcom/helpshift/support/flows/CustomContactUsFlowListHolder;->getFlowList()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, p1, v1, v0}, Lcom/helpshift/support/controllers/SupportController;->startFaqFlow(Landroid/os/Bundle;ZLjava/util/List;)V

    goto :goto_0

    :cond_0
    const-string v0, "flow_title"

    .line 601
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-static {}, Lcom/helpshift/support/flows/DynamicFormFlowListHolder;->getFlowList()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, p1, v0, v1}, Lcom/helpshift/support/controllers/SupportController;->startDynamicForm(Ljava/lang/String;Ljava/util/List;Z)V

    goto :goto_0

    .line 598
    :cond_1
    invoke-direct {p0, p1}, Lcom/helpshift/support/controllers/SupportController;->replaceConversationFlow(Landroid/os/Bundle;)V

    :goto_0
    return-void
.end method

.method public onQuestionSelected(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 517
    iget-object v0, p0, Lcom/helpshift/support/controllers/SupportController;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/helpshift/support/util/Styles;->isTablet(Landroid/content/Context;)Z

    move-result v0

    .line 518
    iget-object v1, p0, Lcom/helpshift/support/controllers/SupportController;->bundle:Landroid/os/Bundle;

    const-string v2, "questionPublishId"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz p2, :cond_0

    .line 520
    iget-object p1, p0, Lcom/helpshift/support/controllers/SupportController;->bundle:Landroid/os/Bundle;

    const-string v1, "searchTerms"

    invoke-virtual {p1, v1, p2}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 522
    :cond_0
    iget-object p1, p0, Lcom/helpshift/support/controllers/SupportController;->fragmentManager:Landroidx/fragment/app/FragmentManager;

    sget p2, Lcom/helpshift/R$id;->flow_fragment_container:I

    iget-object v1, p0, Lcom/helpshift/support/controllers/SupportController;->bundle:Landroid/os/Bundle;

    const/4 v2, 0x2

    const/4 v3, 0x0

    .line 524
    invoke-static {v1, v2, v0, v3}, Lcom/helpshift/support/fragments/SingleQuestionFragment;->newInstance(Landroid/os/Bundle;IZLcom/helpshift/support/fragments/SingleQuestionFragment$QuestionReadListener;)Lcom/helpshift/support/fragments/SingleQuestionFragment;

    move-result-object v0

    const/4 v1, 0x0

    .line 522
    invoke-static {p1, p2, v0, v3, v1}, Lcom/helpshift/support/util/FragmentUtil;->startFragmentWithBackStack(Landroidx/fragment/app/FragmentManager;ILandroidx/fragment/app/Fragment;Ljava/lang/String;Z)V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .line 661
    iget-boolean v0, p0, Lcom/helpshift/support/controllers/SupportController;->isControllerStarted:Z

    const-string v1, "key_support_controller_started"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 662
    iget-object v0, p0, Lcom/helpshift/support/controllers/SupportController;->conversationBundle:Landroid/os/Bundle;

    const-string v1, "key_conversation_bundle"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 663
    iget-boolean v0, p0, Lcom/helpshift/support/controllers/SupportController;->conversationAddToBackStack:Z

    const-string v1, "key_conversation_add_to_back_stack"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public onUserSetupSyncCompleted()V
    .locals 0

    .line 704
    invoke-direct {p0}, Lcom/helpshift/support/controllers/SupportController;->startConversationFlowInternal()V

    return-void
.end method

.method public onViewStateRestored(Landroid/os/Bundle;)V
    .locals 3

    .line 667
    iget-boolean v0, p0, Lcom/helpshift/support/controllers/SupportController;->isControllerStarted:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const-string v0, "key_support_controller_started"

    .line 671
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 672
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/helpshift/support/controllers/SupportController;->isControllerStarted:Z

    .line 673
    iget-object v0, p0, Lcom/helpshift/support/controllers/SupportController;->bundle:Landroid/os/Bundle;

    const/4 v1, 0x0

    const-string v2, "support_mode"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/helpshift/support/controllers/SupportController;->supportMode:I

    .line 675
    iget-object v0, p0, Lcom/helpshift/support/controllers/SupportController;->fragmentManager:Landroidx/fragment/app/FragmentManager;

    if-eqz v0, :cond_3

    const-string v1, "ScreenshotPreviewFragment"

    .line 677
    invoke-virtual {v0, v1}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;

    if-eqz v0, :cond_1

    .line 679
    invoke-virtual {v0, p0}, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;->setScreenshotPreviewListener(Lcom/helpshift/support/contracts/ScreenshotPreviewListener;)V

    .line 682
    :cond_1
    iget-object v0, p0, Lcom/helpshift/support/controllers/SupportController;->fragmentManager:Landroidx/fragment/app/FragmentManager;

    const-string v1, "HSSearchResultFragment"

    .line 683
    invoke-virtual {v0, v1}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/helpshift/support/fragments/SearchResultFragment;

    if-eqz v0, :cond_2

    .line 685
    invoke-virtual {v0, p0}, Lcom/helpshift/support/fragments/SearchResultFragment;->setSearchResultListener(Lcom/helpshift/support/contracts/SearchResultListener;)V

    .line 688
    :cond_2
    iget-object v0, p0, Lcom/helpshift/support/controllers/SupportController;->fragmentManager:Landroidx/fragment/app/FragmentManager;

    const-string v1, "HSDynamicFormFragment"

    .line 689
    invoke-virtual {v0, v1}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/helpshift/support/fragments/DynamicFormFragment;

    if-eqz v0, :cond_3

    .line 691
    invoke-virtual {v0, p0}, Lcom/helpshift/support/fragments/DynamicFormFragment;->setSupportController(Lcom/helpshift/support/controllers/SupportController;)V

    :cond_3
    const-string v0, "key_conversation_bundle"

    .line 696
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "key_conversation_add_to_back_stack"

    .line 697
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 698
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/support/controllers/SupportController;->conversationBundle:Landroid/os/Bundle;

    .line 699
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/helpshift/support/controllers/SupportController;->conversationAddToBackStack:Z

    :cond_4
    return-void
.end method

.method public removeScreenshot()V
    .locals 3

    .line 575
    iget-object v0, p0, Lcom/helpshift/support/controllers/SupportController;->fragmentManager:Landroidx/fragment/app/FragmentManager;

    const-class v1, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/helpshift/support/util/FragmentUtil;->popBackStack(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    .line 576
    iget-object v0, p0, Lcom/helpshift/support/controllers/SupportController;->fragmentManager:Landroidx/fragment/app/FragmentManager;

    const-string v1, "HSNewConversationFragment"

    .line 577
    invoke-virtual {v0, v1}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/helpshift/support/conversations/NewConversationFragment;

    if-eqz v0, :cond_0

    .line 579
    sget-object v1, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment$ScreenshotAction;->REMOVE:Lcom/helpshift/support/fragments/ScreenshotPreviewFragment$ScreenshotAction;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/helpshift/support/conversations/NewConversationFragment;->handleScreenshotAction(Lcom/helpshift/support/fragments/ScreenshotPreviewFragment$ScreenshotAction;Lcom/helpshift/conversation/dto/ImagePickerFile;)Z

    :cond_0
    return-void
.end method

.method public removeScreenshotPreviewFragment()V
    .locals 2

    .line 590
    iget-object v0, p0, Lcom/helpshift/support/controllers/SupportController;->fragmentManager:Landroidx/fragment/app/FragmentManager;

    const-class v1, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/helpshift/support/util/FragmentUtil;->popBackStack(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method

.method public sendAnyway()V
    .locals 2

    .line 531
    invoke-static {}, Lcom/helpshift/util/HelpshiftContext;->getCoreApi()Lcom/helpshift/CoreApi;

    move-result-object v0

    invoke-interface {v0}, Lcom/helpshift/CoreApi;->getAnalyticsEventDM()Lcom/helpshift/analytics/domainmodel/AnalyticsEventDM;

    move-result-object v0

    sget-object v1, Lcom/helpshift/analytics/AnalyticsEventType;->TICKET_AVOIDANCE_FAILED:Lcom/helpshift/analytics/AnalyticsEventType;

    invoke-virtual {v0, v1}, Lcom/helpshift/analytics/domainmodel/AnalyticsEventDM;->pushEvent(Lcom/helpshift/analytics/AnalyticsEventType;)V

    .line 532
    invoke-virtual {p0}, Lcom/helpshift/support/controllers/SupportController;->getFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    const-class v1, Lcom/helpshift/support/fragments/SearchResultFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/helpshift/support/util/FragmentUtil;->popBackStackImmediate(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    .line 533
    iget-object v0, p0, Lcom/helpshift/support/controllers/SupportController;->fragmentManager:Landroidx/fragment/app/FragmentManager;

    const-string v1, "HSNewConversationFragment"

    .line 534
    invoke-virtual {v0, v1}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/helpshift/support/conversations/NewConversationFragment;

    if-eqz v0, :cond_0

    .line 536
    invoke-virtual {v0}, Lcom/helpshift/support/conversations/NewConversationFragment;->startNewConversation()V

    :cond_0
    return-void
.end method

.method public sendScreenshot(Lcom/helpshift/conversation/dto/ImagePickerFile;Ljava/lang/String;)V
    .locals 2

    .line 565
    iget-object v0, p0, Lcom/helpshift/support/controllers/SupportController;->fragmentManager:Landroidx/fragment/app/FragmentManager;

    const-class v1, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/helpshift/support/util/FragmentUtil;->popBackStack(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    .line 566
    iget-object v0, p0, Lcom/helpshift/support/controllers/SupportController;->fragmentManager:Landroidx/fragment/app/FragmentManager;

    const-string v1, "HSConversationFragment"

    .line 567
    invoke-virtual {v0, v1}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/helpshift/support/conversations/ConversationFragment;

    if-eqz v0, :cond_0

    .line 569
    sget-object v1, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment$ScreenshotAction;->SEND:Lcom/helpshift/support/fragments/ScreenshotPreviewFragment$ScreenshotAction;

    invoke-virtual {v0, v1, p1, p2}, Lcom/helpshift/support/conversations/ConversationFragment;->handleScreenshotAction(Lcom/helpshift/support/fragments/ScreenshotPreviewFragment$ScreenshotAction;Lcom/helpshift/conversation/dto/ImagePickerFile;Ljava/lang/String;)Z

    :cond_0
    return-void
.end method

.method public setSearchPerformed(Z)V
    .locals 0

    .line 98
    iput-boolean p1, p0, Lcom/helpshift/support/controllers/SupportController;->searchPerformed:Z

    return-void
.end method

.method public showAuthenticationFailureFragment()V
    .locals 9

    const-string v0, "Helpshift_SupportContr"

    const-string v1, "Starting authentication failure fragment"

    .line 277
    invoke-static {v0, v1}, Lcom/helpshift/util/HSLogger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    invoke-static {}, Lcom/helpshift/support/conversations/AuthenticationFailureFragment;->newInstance()Lcom/helpshift/support/conversations/AuthenticationFailureFragment;

    move-result-object v4

    .line 281
    iget-boolean v0, p0, Lcom/helpshift/support/controllers/SupportController;->conversationAddToBackStack:Z

    if-eqz v0, :cond_0

    .line 282
    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    move-object v6, v0

    .line 285
    invoke-direct {p0}, Lcom/helpshift/support/controllers/SupportController;->clearConversationStack()V

    .line 286
    iget-object v2, p0, Lcom/helpshift/support/controllers/SupportController;->fragmentManager:Landroidx/fragment/app/FragmentManager;

    sget v3, Lcom/helpshift/R$id;->flow_fragment_container:I

    const/4 v7, 0x0

    const/4 v8, 0x0

    const-string v5, "HSAuthenticationFailureFragment"

    invoke-static/range {v2 .. v8}, Lcom/helpshift/support/util/FragmentUtil;->startFragment(Landroidx/fragment/app/FragmentManager;ILandroidx/fragment/app/Fragment;Ljava/lang/String;Ljava/lang/String;ZZ)V

    return-void
.end method

.method public showConversationSearchResultFragment(Landroid/os/Bundle;)V
    .locals 4

    .line 385
    iget-object v0, p0, Lcom/helpshift/support/controllers/SupportController;->fragmentManager:Landroidx/fragment/app/FragmentManager;

    sget v1, Lcom/helpshift/R$id;->flow_fragment_container:I

    .line 387
    invoke-static {p1, p0}, Lcom/helpshift/support/fragments/SearchResultFragment;->newInstance(Landroid/os/Bundle;Lcom/helpshift/support/contracts/SearchResultListener;)Lcom/helpshift/support/fragments/SearchResultFragment;

    move-result-object p1

    const-string v2, "HSSearchResultFragment"

    const/4 v3, 0x0

    .line 385
    invoke-static {v0, v1, p1, v2, v3}, Lcom/helpshift/support/util/FragmentUtil;->startFragmentWithBackStack(Landroidx/fragment/app/FragmentManager;ILandroidx/fragment/app/Fragment;Ljava/lang/String;Z)V

    return-void
.end method

.method public showUserSetupFragment()V
    .locals 7

    .line 296
    invoke-static {}, Lcom/helpshift/support/conversations/usersetup/UserSetupFragment;->newInstance()Lcom/helpshift/support/conversations/usersetup/UserSetupFragment;

    move-result-object v2

    .line 298
    iget-boolean v0, p0, Lcom/helpshift/support/controllers/SupportController;->conversationAddToBackStack:Z

    if-eqz v0, :cond_0

    .line 299
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    .line 302
    invoke-direct {p0}, Lcom/helpshift/support/controllers/SupportController;->clearConversationStack()V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    move-object v4, v0

    .line 304
    iget-object v0, p0, Lcom/helpshift/support/controllers/SupportController;->fragmentManager:Landroidx/fragment/app/FragmentManager;

    sget v1, Lcom/helpshift/R$id;->flow_fragment_container:I

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v3, "HSUserSetupFragment"

    invoke-static/range {v0 .. v6}, Lcom/helpshift/support/util/FragmentUtil;->startFragment(Landroidx/fragment/app/FragmentManager;ILandroidx/fragment/app/Fragment;Ljava/lang/String;Ljava/lang/String;ZZ)V

    return-void
.end method

.method public start()V
    .locals 4

    .line 102
    iget-boolean v0, p0, Lcom/helpshift/support/controllers/SupportController;->isControllerStarted:Z

    const/4 v1, 0x1

    if-nez v0, :cond_2

    .line 103
    iget-object v0, p0, Lcom/helpshift/support/controllers/SupportController;->bundle:Landroid/os/Bundle;

    const/4 v2, 0x0

    const-string v3, "support_mode"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/helpshift/support/controllers/SupportController;->supportMode:I

    .line 104
    iget v0, p0, Lcom/helpshift/support/controllers/SupportController;->supportMode:I

    if-eq v0, v1, :cond_1

    const/4 v3, 0x4

    if-eq v0, v3, :cond_0

    .line 112
    iget-object v0, p0, Lcom/helpshift/support/controllers/SupportController;->bundle:Landroid/os/Bundle;

    invoke-static {}, Lcom/helpshift/support/flows/CustomContactUsFlowListHolder;->getFlowList()Ljava/util/List;

    move-result-object v3

    invoke-virtual {p0, v0, v2, v3}, Lcom/helpshift/support/controllers/SupportController;->startFaqFlow(Landroid/os/Bundle;ZLjava/util/List;)V

    goto :goto_0

    .line 109
    :cond_0
    invoke-static {}, Lcom/helpshift/support/flows/DynamicFormFlowListHolder;->getFlowList()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0, v2}, Lcom/helpshift/support/controllers/SupportController;->startDynamicForm(Ljava/util/List;Z)V

    goto :goto_0

    .line 106
    :cond_1
    iget-object v0, p0, Lcom/helpshift/support/controllers/SupportController;->bundle:Landroid/os/Bundle;

    invoke-virtual {p0, v0, v2}, Lcom/helpshift/support/controllers/SupportController;->startConversationFlow(Landroid/os/Bundle;Z)V

    .line 116
    :cond_2
    :goto_0
    iput-boolean v1, p0, Lcom/helpshift/support/controllers/SupportController;->isControllerStarted:Z

    return-void
.end method

.method public startConversationFlow()V
    .locals 2

    .line 190
    invoke-static {}, Lcom/helpshift/util/HelpshiftContext;->getCoreApi()Lcom/helpshift/CoreApi;

    move-result-object v0

    invoke-interface {v0}, Lcom/helpshift/CoreApi;->getUserManagerDM()Lcom/helpshift/account/domainmodel/UserManagerDM;

    move-result-object v0

    invoke-virtual {v0}, Lcom/helpshift/account/domainmodel/UserManagerDM;->getActiveUserSetupDM()Lcom/helpshift/account/domainmodel/UserSetupDM;

    move-result-object v0

    .line 191
    invoke-virtual {v0}, Lcom/helpshift/account/domainmodel/UserSetupDM;->getState()Lcom/helpshift/account/domainmodel/UserSetupState;

    move-result-object v0

    .line 196
    sget-object v1, Lcom/helpshift/support/controllers/SupportController$1;->$SwitchMap$com$helpshift$account$domainmodel$UserSetupState:[I

    invoke-virtual {v0}, Lcom/helpshift/account/domainmodel/UserSetupState;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    goto :goto_0

    .line 203
    :cond_0
    invoke-direct {p0}, Lcom/helpshift/support/controllers/SupportController;->startConversationFlowInternal()V

    goto :goto_0

    .line 200
    :cond_1
    invoke-virtual {p0}, Lcom/helpshift/support/controllers/SupportController;->showUserSetupFragment()V

    :goto_0
    return-void
.end method

.method public startConversationFlow(Landroid/os/Bundle;Z)V
    .locals 0

    .line 180
    iput-boolean p2, p0, Lcom/helpshift/support/controllers/SupportController;->conversationAddToBackStack:Z

    .line 181
    iput-object p1, p0, Lcom/helpshift/support/controllers/SupportController;->conversationBundle:Landroid/os/Bundle;

    .line 182
    invoke-virtual {p0}, Lcom/helpshift/support/controllers/SupportController;->startConversationFlow()V

    return-void
.end method

.method public startDynamicForm(ILjava/util/List;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "Lcom/helpshift/support/flows/Flow;",
            ">;Z)V"
        }
    .end annotation

    .line 432
    iget-object v0, p0, Lcom/helpshift/support/controllers/SupportController;->bundle:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 433
    iget-object v1, p0, Lcom/helpshift/support/controllers/SupportController;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    const-string v1, "flow_title"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 435
    :cond_0
    invoke-virtual {p0, p2, p3}, Lcom/helpshift/support/controllers/SupportController;->startDynamicForm(Ljava/util/List;Z)V

    return-void
.end method

.method public startDynamicForm(Ljava/lang/String;Ljava/util/List;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/helpshift/support/flows/Flow;",
            ">;Z)V"
        }
    .end annotation

    .line 424
    iget-object v0, p0, Lcom/helpshift/support/controllers/SupportController;->bundle:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    const-string v1, "flow_title"

    .line 425
    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 427
    :cond_0
    invoke-virtual {p0, p2, p3}, Lcom/helpshift/support/controllers/SupportController;->startDynamicForm(Ljava/util/List;Z)V

    return-void
.end method

.method public startDynamicForm(Ljava/util/List;Z)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/helpshift/support/flows/Flow;",
            ">;Z)V"
        }
    .end annotation

    .line 408
    iget-object v0, p0, Lcom/helpshift/support/controllers/SupportController;->bundle:Landroid/os/Bundle;

    invoke-static {v0, p1, p0}, Lcom/helpshift/support/fragments/DynamicFormFragment;->newInstance(Landroid/os/Bundle;Ljava/util/List;Lcom/helpshift/support/controllers/SupportController;)Lcom/helpshift/support/fragments/DynamicFormFragment;

    move-result-object v3

    if-eqz p2, :cond_0

    .line 411
    const-class p1, Lcom/helpshift/support/fragments/DynamicFormFragment;

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    move-object v5, p1

    .line 413
    iget-object v1, p0, Lcom/helpshift/support/controllers/SupportController;->fragmentManager:Landroidx/fragment/app/FragmentManager;

    sget v2, Lcom/helpshift/R$id;->flow_fragment_container:I

    const/4 v6, 0x0

    const/4 v7, 0x0

    const-string v4, "HSDynamicFormFragment"

    invoke-static/range {v1 .. v7}, Lcom/helpshift/support/util/FragmentUtil;->startFragment(Landroidx/fragment/app/FragmentManager;ILandroidx/fragment/app/Fragment;Ljava/lang/String;Ljava/lang/String;ZZ)V

    return-void
.end method

.method public startFaqFlow(Landroid/os/Bundle;ZLjava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            "Z",
            "Ljava/util/List<",
            "Lcom/helpshift/support/flows/Flow;",
            ">;)V"
        }
    .end annotation

    .line 316
    iget-object v0, p0, Lcom/helpshift/support/controllers/SupportController;->fragmentManager:Landroidx/fragment/app/FragmentManager;

    invoke-static {v0}, Lcom/helpshift/support/util/FragmentUtil;->getTopMostFragment(Landroidx/fragment/app/FragmentManager;)Landroidx/fragment/app/Fragment;

    move-result-object v0

    instance-of v0, v0, Lcom/helpshift/support/fragments/FaqFlowFragment;

    if-eqz v0, :cond_0

    return-void

    .line 319
    :cond_0
    invoke-static {p1, p3}, Lcom/helpshift/support/fragments/FaqFlowFragment;->newInstance(Landroid/os/Bundle;Ljava/util/List;)Lcom/helpshift/support/fragments/FaqFlowFragment;

    move-result-object v3

    const/4 p1, 0x0

    if-eqz p2, :cond_1

    .line 322
    const-class p1, Lcom/helpshift/support/fragments/FaqFlowFragment;

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    :cond_1
    move-object v5, p1

    .line 324
    iget-object v1, p0, Lcom/helpshift/support/controllers/SupportController;->fragmentManager:Landroidx/fragment/app/FragmentManager;

    sget v2, Lcom/helpshift/R$id;->flow_fragment_container:I

    const/4 v6, 0x0

    const/4 v7, 0x0

    const-string v4, "Helpshift_FaqFlowFrag"

    invoke-static/range {v1 .. v7}, Lcom/helpshift/support/util/FragmentUtil;->startFragment(Landroidx/fragment/app/FragmentManager;ILandroidx/fragment/app/Fragment;Ljava/lang/String;Ljava/lang/String;ZZ)V

    return-void
.end method

.method public startScreenshotPreviewFragment(Lcom/helpshift/conversation/dto/ImagePickerFile;Landroid/os/Bundle;Lcom/helpshift/support/fragments/ScreenshotPreviewFragment$LaunchSource;)V
    .locals 5

    .line 395
    invoke-virtual {p0}, Lcom/helpshift/support/controllers/SupportController;->getFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v0

    invoke-static {v0}, Lcom/helpshift/support/util/FragmentUtil;->getScreenshotPreviewFragment(Landroidx/fragment/app/FragmentManager;)Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;

    move-result-object v0

    if-nez v0, :cond_0

    .line 397
    invoke-static {p0}, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;->newInstance(Lcom/helpshift/support/contracts/ScreenshotPreviewListener;)Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;

    move-result-object v0

    .line 398
    invoke-virtual {p0}, Lcom/helpshift/support/controllers/SupportController;->getFragmentManager()Landroidx/fragment/app/FragmentManager;

    move-result-object v1

    sget v2, Lcom/helpshift/R$id;->flow_fragment_container:I

    const/4 v3, 0x0

    const-string v4, "ScreenshotPreviewFragment"

    invoke-static {v1, v2, v0, v4, v3}, Lcom/helpshift/support/util/FragmentUtil;->startFragmentWithBackStack(Landroidx/fragment/app/FragmentManager;ILandroidx/fragment/app/Fragment;Ljava/lang/String;Z)V

    .line 404
    :cond_0
    invoke-virtual {v0, p2, p1, p3}, Lcom/helpshift/support/fragments/ScreenshotPreviewFragment;->setParams(Landroid/os/Bundle;Lcom/helpshift/conversation/dto/ImagePickerFile;Lcom/helpshift/support/fragments/ScreenshotPreviewFragment$LaunchSource;)V

    return-void
.end method
