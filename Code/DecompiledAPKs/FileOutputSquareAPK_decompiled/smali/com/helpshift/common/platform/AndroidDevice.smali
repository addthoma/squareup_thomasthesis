.class public Lcom/helpshift/common/platform/AndroidDevice;
.super Ljava/lang/Object;
.source "AndroidDevice.java"

# interfaces
.implements Lcom/helpshift/common/platform/Device;


# static fields
.field public static final KEY_DEVICE_ID:Ljava/lang/String; = "key_support_device_id"

.field private static final KEY_PUSH_TOKEN:Ljava/lang/String; = "key_push_token"


# instance fields
.field private backupDAO:Lcom/helpshift/common/dao/BackupDAO;

.field private cacheDeviceId:Ljava/lang/String;

.field private cachedPushToken:Ljava/lang/String;

.field private final context:Landroid/content/Context;

.field private kvStore:Lcom/helpshift/common/platform/KVStore;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/helpshift/common/platform/KVStore;Lcom/helpshift/common/dao/BackupDAO;)V
    .locals 0

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/helpshift/common/platform/AndroidDevice;->context:Landroid/content/Context;

    .line 46
    iput-object p2, p0, Lcom/helpshift/common/platform/AndroidDevice;->kvStore:Lcom/helpshift/common/platform/KVStore;

    .line 47
    iput-object p3, p0, Lcom/helpshift/common/platform/AndroidDevice;->backupDAO:Lcom/helpshift/common/dao/BackupDAO;

    return-void
.end method

.method private checkStoragePermissions(Ljava/lang/String;)Lcom/helpshift/common/platform/Device$PermissionState;
    .locals 2

    .line 318
    invoke-virtual {p0}, Lcom/helpshift/common/platform/AndroidDevice;->getOSVersionNumber()I

    move-result v0

    const/16 v1, 0x13

    if-ge v0, v1, :cond_0

    .line 320
    sget-object p1, Lcom/helpshift/common/platform/Device$PermissionState;->AVAILABLE:Lcom/helpshift/common/platform/Device$PermissionState;

    goto :goto_0

    .line 323
    :cond_0
    iget-object v1, p0, Lcom/helpshift/common/platform/AndroidDevice;->context:Landroid/content/Context;

    invoke-static {v1, p1}, Lcom/helpshift/util/ApplicationUtil;->isPermissionGranted(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 324
    sget-object p1, Lcom/helpshift/common/platform/Device$PermissionState;->AVAILABLE:Lcom/helpshift/common/platform/Device$PermissionState;

    goto :goto_0

    :cond_1
    const/16 v1, 0x17

    if-ge v0, v1, :cond_2

    .line 328
    sget-object p1, Lcom/helpshift/common/platform/Device$PermissionState;->UNAVAILABLE:Lcom/helpshift/common/platform/Device$PermissionState;

    goto :goto_0

    .line 331
    :cond_2
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidDevice;->context:Landroid/content/Context;

    .line 332
    invoke-static {v0, p1}, Lcom/helpshift/support/util/PermissionUtil;->hasPermissionInManifest(Landroid/content/Context;Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_3

    .line 334
    sget-object p1, Lcom/helpshift/common/platform/Device$PermissionState;->REQUESTABLE:Lcom/helpshift/common/platform/Device$PermissionState;

    goto :goto_0

    .line 337
    :cond_3
    sget-object p1, Lcom/helpshift/common/platform/Device$PermissionState;->UNAVAILABLE:Lcom/helpshift/common/platform/Device$PermissionState;

    :goto_0
    return-object p1
.end method


# virtual methods
.method public changeLocale(Ljava/util/Locale;)V
    .locals 3

    .line 231
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidDevice;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 232
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 233
    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    .line 234
    iput-object p1, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 235
    invoke-virtual {v0, v2, v1}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    return-void
.end method

.method public checkPermission(Lcom/helpshift/common/platform/Device$PermissionType;)Lcom/helpshift/common/platform/Device$PermissionState;
    .locals 1

    .line 206
    sget-object v0, Lcom/helpshift/common/platform/AndroidDevice$1;->$SwitchMap$com$helpshift$common$platform$Device$PermissionType:[I

    invoke-virtual {p1}, Lcom/helpshift/common/platform/Device$PermissionType;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_1

    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    const-string p1, "android.permission.WRITE_EXTERNAL_STORAGE"

    .line 211
    invoke-direct {p0, p1}, Lcom/helpshift/common/platform/AndroidDevice;->checkStoragePermissions(Ljava/lang/String;)Lcom/helpshift/common/platform/Device$PermissionState;

    move-result-object p1

    goto :goto_0

    :cond_1
    const-string p1, "android.permission.READ_EXTERNAL_STORAGE"

    .line 208
    invoke-direct {p0, p1}, Lcom/helpshift/common/platform/AndroidDevice;->checkStoragePermissions(Ljava/lang/String;)Lcom/helpshift/common/platform/Device$PermissionState;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public getApiVersion()Ljava/lang/String;
    .locals 1

    const-string v0, "3"

    return-object v0
.end method

.method public getAppIdentifier()Ljava/lang/String;
    .locals 1

    .line 82
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidDevice;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAppName()Ljava/lang/String;
    .locals 1

    .line 77
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidDevice;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/helpshift/util/ApplicationUtil;->getApplicationName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAppVersion()Ljava/lang/String;
    .locals 1

    .line 72
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidDevice;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/helpshift/util/ApplicationUtil;->getApplicationVersion(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getBatteryLevel()Ljava/lang/String;
    .locals 4

    .line 157
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 158
    iget-object v1, p0, Lcom/helpshift/common/platform/AndroidDevice;->context:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, ""

    return-object v0

    :cond_0
    const/4 v1, -0x1

    const-string v2, "level"

    .line 164
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    const-string v3, "scale"

    .line 165
    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    int-to-float v1, v2

    int-to-float v0, v0

    div-float/2addr v1, v0

    const/high16 v0, 0x42c80000    # 100.0f

    mul-float v1, v1, v0

    float-to-int v0, v1

    .line 167
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, "%"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getBatteryStatus()Ljava/lang/String;
    .locals 4

    .line 142
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 143
    iget-object v1, p0, Lcom/helpshift/common/platform/AndroidDevice;->context:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "Not charging"

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    const/4 v2, -0x1

    const-string v3, "status"

    .line 149
    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    const/4 v2, 0x2

    if-eq v0, v2, :cond_2

    const/4 v2, 0x5

    if-ne v0, v2, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_3

    const-string v1, "Charging"

    :cond_3
    return-object v1
.end method

.method public getCarrierName()Ljava/lang/String;
    .locals 2

    .line 114
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidDevice;->context:Landroid/content/Context;

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    if-nez v0, :cond_0

    const-string v0, ""

    goto :goto_0

    .line 115
    :cond_0
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkOperatorName()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public getDeviceId()Ljava/lang/String;
    .locals 3

    .line 261
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidDevice;->cacheDeviceId:Ljava/lang/String;

    if-eqz v0, :cond_0

    return-object v0

    .line 266
    :cond_0
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidDevice;->kvStore:Lcom/helpshift/common/platform/KVStore;

    const-string v1, "key_support_device_id"

    invoke-interface {v0, v1}, Lcom/helpshift/common/platform/KVStore;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/common/platform/AndroidDevice;->cacheDeviceId:Ljava/lang/String;

    .line 268
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidDevice;->cacheDeviceId:Ljava/lang/String;

    invoke-static {v0}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 269
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidDevice;->backupDAO:Lcom/helpshift/common/dao/BackupDAO;

    invoke-interface {v0, v1}, Lcom/helpshift/common/dao/BackupDAO;->getValue(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/helpshift/common/platform/AndroidDevice;->cacheDeviceId:Ljava/lang/String;

    .line 271
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidDevice;->cacheDeviceId:Ljava/lang/String;

    invoke-static {v0}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 272
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidDevice;->kvStore:Lcom/helpshift/common/platform/KVStore;

    iget-object v2, p0, Lcom/helpshift/common/platform/AndroidDevice;->cacheDeviceId:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/helpshift/common/platform/KVStore;->setString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 277
    :cond_1
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidDevice;->backupDAO:Lcom/helpshift/common/dao/BackupDAO;

    iget-object v2, p0, Lcom/helpshift/common/platform/AndroidDevice;->cacheDeviceId:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/helpshift/common/dao/BackupDAO;->storeValue(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 282
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidDevice;->cacheDeviceId:Ljava/lang/String;

    invoke-static {v0}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 283
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/common/platform/AndroidDevice;->cacheDeviceId:Ljava/lang/String;

    .line 284
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidDevice;->kvStore:Lcom/helpshift/common/platform/KVStore;

    iget-object v2, p0, Lcom/helpshift/common/platform/AndroidDevice;->cacheDeviceId:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/helpshift/common/platform/KVStore;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidDevice;->backupDAO:Lcom/helpshift/common/dao/BackupDAO;

    iget-object v2, p0, Lcom/helpshift/common/platform/AndroidDevice;->cacheDeviceId:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/helpshift/common/dao/BackupDAO;->storeValue(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 287
    :cond_3
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidDevice;->cacheDeviceId:Ljava/lang/String;

    return-object v0
.end method

.method public getDeviceModel()Ljava/lang/String;
    .locals 1

    .line 92
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    return-object v0
.end method

.method public getDiskSpace()Lcom/helpshift/meta/dto/DeviceDiskSpaceDTO;
    .locals 10

    .line 175
    new-instance v0, Landroid/os/StatFs;

    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 176
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const-wide/high16 v2, 0x4059000000000000L    # 100.0

    const-wide/high16 v4, 0x41d0000000000000L    # 1.073741824E9

    const/16 v6, 0x12

    if-lt v1, v6, :cond_0

    .line 177
    invoke-virtual {v0}, Landroid/os/StatFs;->getAvailableBlocksLong()J

    move-result-wide v6

    long-to-double v6, v6

    .line 178
    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockSizeLong()J

    move-result-wide v8

    long-to-double v8, v8

    mul-double v6, v6, v8

    div-double/2addr v6, v4

    mul-double v6, v6, v2

    .line 179
    invoke-static {v6, v7}, Ljava/lang/Math;->round(D)J

    move-result-wide v6

    long-to-double v6, v6

    div-double/2addr v6, v2

    .line 181
    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockCountLong()J

    move-result-wide v8

    long-to-double v8, v8

    .line 182
    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockSizeLong()J

    move-result-wide v0

    long-to-double v0, v0

    mul-double v8, v8, v0

    div-double/2addr v8, v4

    mul-double v8, v8, v2

    .line 183
    invoke-static {v8, v9}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    goto :goto_0

    .line 187
    :cond_0
    invoke-virtual {v0}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v1

    int-to-double v6, v1

    .line 188
    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockSize()I

    move-result v1

    int-to-double v8, v1

    mul-double v6, v6, v8

    div-double/2addr v6, v4

    mul-double v6, v6, v2

    .line 189
    invoke-static {v6, v7}, Ljava/lang/Math;->round(D)J

    move-result-wide v6

    long-to-double v6, v6

    div-double/2addr v6, v2

    .line 191
    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockCount()I

    move-result v1

    int-to-double v8, v1

    .line 192
    invoke-virtual {v0}, Landroid/os/StatFs;->getBlockSize()I

    move-result v0

    int-to-double v0, v0

    mul-double v8, v8, v0

    div-double/2addr v8, v4

    mul-double v8, v8, v2

    .line 193
    invoke-static {v8, v9}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    :goto_0
    long-to-double v0, v0

    div-double/2addr v0, v2

    .line 195
    new-instance v2, Lcom/helpshift/meta/dto/DeviceDiskSpaceDTO;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string v0, " GB"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x0

    invoke-direct {v2, v1, v0, v3, v3}, Lcom/helpshift/meta/dto/DeviceDiskSpaceDTO;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v2
.end method

.method public getLanguage()Ljava/lang/String;
    .locals 1

    .line 87
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLocale()Ljava/util/Locale;
    .locals 3

    .line 219
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidDevice;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 220
    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 221
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x18

    if-lt v1, v2, :cond_0

    .line 222
    invoke-virtual {v0}, Landroid/content/res/Configuration;->getLocales()Landroid/os/LocaleList;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/LocaleList;->get(I)Ljava/util/Locale;

    move-result-object v0

    return-object v0

    .line 225
    :cond_0
    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    return-object v0
.end method

.method public getNetworkType()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x0

    .line 122
    :try_start_0
    iget-object v1, p0, Lcom/helpshift/common/platform/AndroidDevice;->context:Landroid/content/Context;

    const-string v2, "connectivity"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    if-eqz v1, :cond_0

    .line 124
    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 126
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    nop

    :cond_0
    :goto_0
    if-nez v0, :cond_1

    const-string v0, "Unknown"

    :cond_1
    return-object v0
.end method

.method public getOSVersion()Ljava/lang/String;
    .locals 1

    .line 62
    sget-object v0, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    return-object v0
.end method

.method public getOSVersionNumber()I
    .locals 1

    .line 67
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    return v0
.end method

.method public getPlatformName()Ljava/lang/String;
    .locals 1

    const-string v0, "Android"

    return-object v0
.end method

.method public getPushToken()Ljava/lang/String;
    .locals 2

    .line 292
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidDevice;->cachedPushToken:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 293
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidDevice;->kvStore:Lcom/helpshift/common/platform/KVStore;

    const-string v1, "key_push_token"

    invoke-interface {v0, v1}, Lcom/helpshift/common/platform/KVStore;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/common/platform/AndroidDevice;->cachedPushToken:Ljava/lang/String;

    .line 295
    :cond_0
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidDevice;->cachedPushToken:Ljava/lang/String;

    return-object v0
.end method

.method public getRom()Ljava/lang/String;
    .locals 2

    .line 97
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "os.version"

    invoke-static {v1}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSDKVersion()Ljava/lang/String;
    .locals 1

    const-string v0, "7.5.0"

    return-object v0
.end method

.method public getSimCountryIso()Ljava/lang/String;
    .locals 2

    .line 102
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidDevice;->context:Landroid/content/Context;

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    if-nez v0, :cond_0

    const-string v0, ""

    goto :goto_0

    .line 103
    :cond_0
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimCountryIso()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public getTimeStamp()Ljava/lang/String;
    .locals 3

    .line 108
    new-instance v0, Ljava/text/SimpleDateFormat;

    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string/jumbo v2, "yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'"

    invoke-direct {v0, v2, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    .line 109
    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTimeZoneId()Ljava/lang/String;
    .locals 1

    .line 245
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTimeZoneOffSet()J
    .locals 2

    .line 250
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0}, Ljava/util/GregorianCalendar;-><init>()V

    .line 251
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v0

    .line 255
    invoke-virtual {v0}, Ljava/util/TimeZone;->getRawOffset()I

    move-result v1

    invoke-virtual {v0}, Ljava/util/TimeZone;->getDSTSavings()I

    move-result v0

    add-int/2addr v1, v0

    int-to-long v0, v1

    return-wide v0
.end method

.method public is24HourFormat()Z
    .locals 1

    .line 240
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidDevice;->context:Landroid/content/Context;

    invoke-static {v0}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public setPushToken(Ljava/lang/String;)V
    .locals 2

    .line 300
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidDevice;->kvStore:Lcom/helpshift/common/platform/KVStore;

    const-string v1, "key_push_token"

    invoke-interface {v0, v1, p1}, Lcom/helpshift/common/platform/KVStore;->setString(Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    iput-object p1, p0, Lcom/helpshift/common/platform/AndroidDevice;->cachedPushToken:Ljava/lang/String;

    return-void
.end method

.method updateDeviceIdInBackupDAO()V
    .locals 3

    .line 309
    iget-object v0, p0, Lcom/helpshift/common/platform/AndroidDevice;->kvStore:Lcom/helpshift/common/platform/KVStore;

    const-string v1, "key_support_device_id"

    invoke-interface {v0, v1}, Lcom/helpshift/common/platform/KVStore;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 310
    invoke-static {v0}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 312
    iget-object v2, p0, Lcom/helpshift/common/platform/AndroidDevice;->backupDAO:Lcom/helpshift/common/dao/BackupDAO;

    invoke-interface {v2, v1, v0}, Lcom/helpshift/common/dao/BackupDAO;->storeValue(Ljava/lang/String;Ljava/io/Serializable;)V

    :cond_0
    return-void
.end method
