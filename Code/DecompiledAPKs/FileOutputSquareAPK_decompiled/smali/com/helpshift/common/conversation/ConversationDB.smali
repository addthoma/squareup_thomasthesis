.class public Lcom/helpshift/common/conversation/ConversationDB;
.super Ljava/lang/Object;
.source "ConversationDB.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/helpshift/common/conversation/ConversationDB$AttachmentInfo;,
        Lcom/helpshift/common/conversation/ConversationDB$ImageAttachmentInfo;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "Helpshift_ConverDB"

.field private static instance:Lcom/helpshift/common/conversation/ConversationDB;


# instance fields
.field private final KEY_BOT_ACTION_TYPE:Ljava/lang/String;

.field private final KEY_BOT_ENDED_REASON:Ljava/lang/String;

.field private final KEY_CHATBOT_INFO:Ljava/lang/String;

.field private final KEY_CONTENT_TYPE:Ljava/lang/String;

.field private final KEY_CONVERSATION_ENDED_DELEGATE_SENT:Ljava/lang/String;

.field private final KEY_CSAT_FEEDBACK:Ljava/lang/String;

.field private final KEY_CSAT_RATING:Ljava/lang/String;

.field private final KEY_CSAT_STATE:Ljava/lang/String;

.field private final KEY_DATE_TIME:Ljava/lang/String;

.field private final KEY_FAQS:Ljava/lang/String;

.field private final KEY_FAQ_LANGUAGE:Ljava/lang/String;

.field private final KEY_FAQ_PUBLISH_ID:Ljava/lang/String;

.field private final KEY_FAQ_TITLE:Ljava/lang/String;

.field private final KEY_FILE_NAME:Ljava/lang/String;

.field private final KEY_FILE_PATH:Ljava/lang/String;

.field private final KEY_FOLLOW_UP_REJECTED_OPEN_CONVERSATION:Ljava/lang/String;

.field private final KEY_FOLLOW_UP_REJECTED_REASON:Ljava/lang/String;

.field private final KEY_HAS_NEXT_BOT:Ljava/lang/String;

.field private final KEY_IMAGE_ATTACHMENT_COMPRESSION_COPYING_DONE:Ljava/lang/String;

.field private final KEY_IMAGE_ATTACHMENT_DRAFT_FILE_PATH:Ljava/lang/String;

.field private final KEY_IMAGE_ATTACHMENT_DRAFT_ORIGINAL_NAME:Ljava/lang/String;

.field private final KEY_IMAGE_ATTACHMENT_DRAFT_ORIGINAL_SIZE:Ljava/lang/String;

.field private final KEY_INCREMENT_MESSAGE_COUNT:Ljava/lang/String;

.field private final KEY_INPUT_KEYBOARD:Ljava/lang/String;

.field private final KEY_INPUT_LABEL:Ljava/lang/String;

.field private final KEY_INPUT_OPTIONS:Ljava/lang/String;

.field private final KEY_INPUT_PLACEHOLDER:Ljava/lang/String;

.field private final KEY_INPUT_REQUIRED:Ljava/lang/String;

.field private final KEY_INPUT_SKIP_LABEL:Ljava/lang/String;

.field private final KEY_IS_ANSWERED:Ljava/lang/String;

.field private final KEY_IS_MESSAGE_EMPTY:Ljava/lang/String;

.field private final KEY_IS_RESPONSE_SKIPPED:Ljava/lang/String;

.field private final KEY_IS_SUGGESTION_READ_EVENT_SENT:Ljava/lang/String;

.field private final KEY_MESSAGE_SYNC_STATUS:Ljava/lang/String;

.field private final KEY_OPTION_DATA:Ljava/lang/String;

.field private final KEY_OPTION_TITLE:Ljava/lang/String;

.field private final KEY_OPTION_TYPE:Ljava/lang/String;

.field private final KEY_READ_AT:Ljava/lang/String;

.field private final KEY_REFERRED_MESSAGE_ID:Ljava/lang/String;

.field private final KEY_REFERRED_MESSAGE_TYPE:Ljava/lang/String;

.field private final KEY_SECURE_ATTACHMENT:Ljava/lang/String;

.field private final KEY_SEEN_AT_MESSAGE_CURSOR:Ljava/lang/String;

.field private final KEY_SEEN_SYNC_STATUS:Ljava/lang/String;

.field private final KEY_SELECTED_OPTION_DATA:Ljava/lang/String;

.field private final KEY_SIZE:Ljava/lang/String;

.field private final KEY_SUGGESTION_READ_FAQ_PUBLISH_ID:Ljava/lang/String;

.field private final KEY_THUMBNAIL_FILE_PATH:Ljava/lang/String;

.field private final KEY_THUMBNAIL_URL:Ljava/lang/String;

.field private final KEY_TIMEZONE_ID:Ljava/lang/String;

.field private final KEY_URL:Ljava/lang/String;

.field private final dbHelper:Lcom/helpshift/platform/db/ConversationDBHelper;

.field private final dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 2

    .line 131
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "csat_rating"

    .line 75
    iput-object v0, p0, Lcom/helpshift/common/conversation/ConversationDB;->KEY_CSAT_RATING:Ljava/lang/String;

    const-string v0, "csat_state"

    .line 76
    iput-object v0, p0, Lcom/helpshift/common/conversation/ConversationDB;->KEY_CSAT_STATE:Ljava/lang/String;

    const-string v0, "csat_feedback"

    .line 77
    iput-object v0, p0, Lcom/helpshift/common/conversation/ConversationDB;->KEY_CSAT_FEEDBACK:Ljava/lang/String;

    const-string v0, "increment_message_count"

    .line 78
    iput-object v0, p0, Lcom/helpshift/common/conversation/ConversationDB;->KEY_INCREMENT_MESSAGE_COUNT:Ljava/lang/String;

    const-string v0, "ended_delegate_sent"

    .line 79
    iput-object v0, p0, Lcom/helpshift/common/conversation/ConversationDB;->KEY_CONVERSATION_ENDED_DELEGATE_SENT:Ljava/lang/String;

    const-string v0, "image_draft_orig_name"

    .line 80
    iput-object v0, p0, Lcom/helpshift/common/conversation/ConversationDB;->KEY_IMAGE_ATTACHMENT_DRAFT_ORIGINAL_NAME:Ljava/lang/String;

    const-string v0, "image_draft_orig_size"

    .line 81
    iput-object v0, p0, Lcom/helpshift/common/conversation/ConversationDB;->KEY_IMAGE_ATTACHMENT_DRAFT_ORIGINAL_SIZE:Ljava/lang/String;

    const-string v0, "image_draft_file_path"

    .line 82
    iput-object v0, p0, Lcom/helpshift/common/conversation/ConversationDB;->KEY_IMAGE_ATTACHMENT_DRAFT_FILE_PATH:Ljava/lang/String;

    const-string v0, "image_copy_done"

    .line 83
    iput-object v0, p0, Lcom/helpshift/common/conversation/ConversationDB;->KEY_IMAGE_ATTACHMENT_COMPRESSION_COPYING_DONE:Ljava/lang/String;

    const-string v0, "referredMessageId"

    .line 86
    iput-object v0, p0, Lcom/helpshift/common/conversation/ConversationDB;->KEY_REFERRED_MESSAGE_ID:Ljava/lang/String;

    const-string v0, "rejected_reason"

    .line 87
    iput-object v0, p0, Lcom/helpshift/common/conversation/ConversationDB;->KEY_FOLLOW_UP_REJECTED_REASON:Ljava/lang/String;

    const-string v0, "rejected_conv_id"

    .line 88
    iput-object v0, p0, Lcom/helpshift/common/conversation/ConversationDB;->KEY_FOLLOW_UP_REJECTED_OPEN_CONVERSATION:Ljava/lang/String;

    const-string v0, "is_answered"

    .line 89
    iput-object v0, p0, Lcom/helpshift/common/conversation/ConversationDB;->KEY_IS_ANSWERED:Ljava/lang/String;

    const-string v0, "content_type"

    .line 90
    iput-object v0, p0, Lcom/helpshift/common/conversation/ConversationDB;->KEY_CONTENT_TYPE:Ljava/lang/String;

    const-string v0, "file_name"

    .line 91
    iput-object v0, p0, Lcom/helpshift/common/conversation/ConversationDB;->KEY_FILE_NAME:Ljava/lang/String;

    const-string v0, "url"

    .line 92
    iput-object v0, p0, Lcom/helpshift/common/conversation/ConversationDB;->KEY_URL:Ljava/lang/String;

    const-string v0, "size"

    .line 93
    iput-object v0, p0, Lcom/helpshift/common/conversation/ConversationDB;->KEY_SIZE:Ljava/lang/String;

    const-string v0, "thumbnail_url"

    .line 94
    iput-object v0, p0, Lcom/helpshift/common/conversation/ConversationDB;->KEY_THUMBNAIL_URL:Ljava/lang/String;

    const-string v0, "thumbnailFilePath"

    .line 95
    iput-object v0, p0, Lcom/helpshift/common/conversation/ConversationDB;->KEY_THUMBNAIL_FILE_PATH:Ljava/lang/String;

    const-string v0, "filePath"

    .line 96
    iput-object v0, p0, Lcom/helpshift/common/conversation/ConversationDB;->KEY_FILE_PATH:Ljava/lang/String;

    const-string v0, "seen_cursor"

    .line 97
    iput-object v0, p0, Lcom/helpshift/common/conversation/ConversationDB;->KEY_SEEN_AT_MESSAGE_CURSOR:Ljava/lang/String;

    const-string v0, "seen_sync_status"

    .line 98
    iput-object v0, p0, Lcom/helpshift/common/conversation/ConversationDB;->KEY_SEEN_SYNC_STATUS:Ljava/lang/String;

    const-string v0, "read_at"

    .line 99
    iput-object v0, p0, Lcom/helpshift/common/conversation/ConversationDB;->KEY_READ_AT:Ljava/lang/String;

    const-string v0, "input_keyboard"

    .line 100
    iput-object v0, p0, Lcom/helpshift/common/conversation/ConversationDB;->KEY_INPUT_KEYBOARD:Ljava/lang/String;

    const-string v0, "input_required"

    .line 101
    iput-object v0, p0, Lcom/helpshift/common/conversation/ConversationDB;->KEY_INPUT_REQUIRED:Ljava/lang/String;

    const-string v0, "input_skip_label"

    .line 102
    iput-object v0, p0, Lcom/helpshift/common/conversation/ConversationDB;->KEY_INPUT_SKIP_LABEL:Ljava/lang/String;

    const-string v0, "input_placeholder"

    .line 103
    iput-object v0, p0, Lcom/helpshift/common/conversation/ConversationDB;->KEY_INPUT_PLACEHOLDER:Ljava/lang/String;

    const-string v0, "input_label"

    .line 104
    iput-object v0, p0, Lcom/helpshift/common/conversation/ConversationDB;->KEY_INPUT_LABEL:Ljava/lang/String;

    const-string v0, "input_options"

    .line 105
    iput-object v0, p0, Lcom/helpshift/common/conversation/ConversationDB;->KEY_INPUT_OPTIONS:Ljava/lang/String;

    const-string v0, "option_type"

    .line 106
    iput-object v0, p0, Lcom/helpshift/common/conversation/ConversationDB;->KEY_OPTION_TYPE:Ljava/lang/String;

    const-string v0, "option_title"

    .line 107
    iput-object v0, p0, Lcom/helpshift/common/conversation/ConversationDB;->KEY_OPTION_TITLE:Ljava/lang/String;

    const-string v0, "option_data"

    .line 108
    iput-object v0, p0, Lcom/helpshift/common/conversation/ConversationDB;->KEY_OPTION_DATA:Ljava/lang/String;

    const-string v0, "chatbot_info"

    .line 109
    iput-object v0, p0, Lcom/helpshift/common/conversation/ConversationDB;->KEY_CHATBOT_INFO:Ljava/lang/String;

    const-string v0, "has_next_bot"

    .line 110
    iput-object v0, p0, Lcom/helpshift/common/conversation/ConversationDB;->KEY_HAS_NEXT_BOT:Ljava/lang/String;

    const-string v0, "faqs"

    .line 111
    iput-object v0, p0, Lcom/helpshift/common/conversation/ConversationDB;->KEY_FAQS:Ljava/lang/String;

    const-string v0, "faq_title"

    .line 112
    iput-object v0, p0, Lcom/helpshift/common/conversation/ConversationDB;->KEY_FAQ_TITLE:Ljava/lang/String;

    const-string v0, "faq_publish_id"

    .line 113
    iput-object v0, p0, Lcom/helpshift/common/conversation/ConversationDB;->KEY_FAQ_PUBLISH_ID:Ljava/lang/String;

    const-string v0, "faq_language"

    .line 114
    iput-object v0, p0, Lcom/helpshift/common/conversation/ConversationDB;->KEY_FAQ_LANGUAGE:Ljava/lang/String;

    const-string v0, "is_response_skipped"

    .line 115
    iput-object v0, p0, Lcom/helpshift/common/conversation/ConversationDB;->KEY_IS_RESPONSE_SKIPPED:Ljava/lang/String;

    const-string v0, "selected_option_data"

    .line 116
    iput-object v0, p0, Lcom/helpshift/common/conversation/ConversationDB;->KEY_SELECTED_OPTION_DATA:Ljava/lang/String;

    const-string v0, "referred_message_type"

    .line 117
    iput-object v0, p0, Lcom/helpshift/common/conversation/ConversationDB;->KEY_REFERRED_MESSAGE_TYPE:Ljava/lang/String;

    const-string v0, "bot_action_type"

    .line 118
    iput-object v0, p0, Lcom/helpshift/common/conversation/ConversationDB;->KEY_BOT_ACTION_TYPE:Ljava/lang/String;

    const-string v0, "bot_ended_reason"

    .line 119
    iput-object v0, p0, Lcom/helpshift/common/conversation/ConversationDB;->KEY_BOT_ENDED_REASON:Ljava/lang/String;

    const-string v0, "message_sync_status"

    .line 120
    iput-object v0, p0, Lcom/helpshift/common/conversation/ConversationDB;->KEY_MESSAGE_SYNC_STATUS:Ljava/lang/String;

    const-string v0, "is_secure"

    .line 121
    iput-object v0, p0, Lcom/helpshift/common/conversation/ConversationDB;->KEY_SECURE_ATTACHMENT:Ljava/lang/String;

    const-string v0, "is_message_empty"

    .line 122
    iput-object v0, p0, Lcom/helpshift/common/conversation/ConversationDB;->KEY_IS_MESSAGE_EMPTY:Ljava/lang/String;

    const-string v0, "is_suggestion_read_event_sent"

    .line 123
    iput-object v0, p0, Lcom/helpshift/common/conversation/ConversationDB;->KEY_IS_SUGGESTION_READ_EVENT_SENT:Ljava/lang/String;

    const-string v0, "suggestion_read_faq_publish_id"

    .line 124
    iput-object v0, p0, Lcom/helpshift/common/conversation/ConversationDB;->KEY_SUGGESTION_READ_FAQ_PUBLISH_ID:Ljava/lang/String;

    const-string v0, "dt"

    .line 125
    iput-object v0, p0, Lcom/helpshift/common/conversation/ConversationDB;->KEY_DATE_TIME:Ljava/lang/String;

    const-string v0, "timezone_id"

    .line 126
    iput-object v0, p0, Lcom/helpshift/common/conversation/ConversationDB;->KEY_TIMEZONE_ID:Ljava/lang/String;

    .line 132
    new-instance v0, Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-direct {v0}, Lcom/helpshift/common/conversation/ConversationDBInfo;-><init>()V

    iput-object v0, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    .line 133
    new-instance v0, Lcom/helpshift/platform/db/ConversationDBHelper;

    iget-object v1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-direct {v0, p1, v1}, Lcom/helpshift/platform/db/ConversationDBHelper;-><init>(Landroid/content/Context;Lcom/helpshift/common/conversation/ConversationDBInfo;)V

    iput-object v0, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbHelper:Lcom/helpshift/platform/db/ConversationDBHelper;

    return-void
.end method

.method private buildJsonObjectForAttachmentMessage(Lorg/json/JSONObject;Lcom/helpshift/conversation/activeconversation/message/AttachmentMessageDM;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 1678
    iget-object v0, p2, Lcom/helpshift/conversation/activeconversation/message/AttachmentMessageDM;->contentType:Ljava/lang/String;

    const-string v1, "content_type"

    invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1679
    iget-object v0, p2, Lcom/helpshift/conversation/activeconversation/message/AttachmentMessageDM;->fileName:Ljava/lang/String;

    const-string v1, "file_name"

    invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1680
    iget-object v0, p2, Lcom/helpshift/conversation/activeconversation/message/AttachmentMessageDM;->filePath:Ljava/lang/String;

    const-string v1, "filePath"

    invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1681
    iget-object v0, p2, Lcom/helpshift/conversation/activeconversation/message/AttachmentMessageDM;->attachmentUrl:Ljava/lang/String;

    const-string v1, "url"

    invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1682
    iget v0, p2, Lcom/helpshift/conversation/activeconversation/message/AttachmentMessageDM;->size:I

    const-string v1, "size"

    invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 1683
    iget-boolean p2, p2, Lcom/helpshift/conversation/activeconversation/message/AttachmentMessageDM;->isSecureAttachment:Z

    const-string v0, "is_secure"

    invoke-virtual {p1, v0, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    return-void
.end method

.method private buildMetaForAdminBotControlMessage(Lorg/json/JSONObject;Lcom/helpshift/conversation/activeconversation/message/AdminBotControlMessageDM;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 1567
    iget-object v0, p2, Lcom/helpshift/conversation/activeconversation/message/AdminBotControlMessageDM;->actionType:Ljava/lang/String;

    const-string v1, "bot_action_type"

    invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1568
    iget-boolean p2, p2, Lcom/helpshift/conversation/activeconversation/message/AdminBotControlMessageDM;->hasNextBot:Z

    const-string v0, "has_next_bot"

    invoke-virtual {p1, v0, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    return-void
.end method

.method private buildMetaForAutoRetriableMessage(Lorg/json/JSONObject;Lcom/helpshift/conversation/activeconversation/message/AutoRetriableMessageDM;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 1707
    invoke-virtual {p2}, Lcom/helpshift/conversation/activeconversation/message/AutoRetriableMessageDM;->getSyncStatus()I

    move-result p2

    const-string v0, "message_sync_status"

    invoke-virtual {p1, v0, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    return-void
.end method

.method private buildMetaForBotInfo(Lorg/json/JSONObject;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    const-string v0, "chatbot_info"

    .line 1620
    invoke-virtual {p1, v0, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    return-void
.end method

.method private buildMetaForDateTime(Lorg/json/JSONObject;Lcom/helpshift/conversation/activeconversation/message/UserResponseMessageForTextInputDM;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 1613
    iget v0, p2, Lcom/helpshift/conversation/activeconversation/message/UserResponseMessageForTextInputDM;->keyboard:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 1614
    iget-wide v0, p2, Lcom/helpshift/conversation/activeconversation/message/UserResponseMessageForTextInputDM;->dateInMillis:J

    const-string v2, "dt"

    invoke-virtual {p1, v2, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 1615
    iget-object p2, p2, Lcom/helpshift/conversation/activeconversation/message/UserResponseMessageForTextInputDM;->timeZoneId:Ljava/lang/String;

    const-string v0, "timezone_id"

    invoke-virtual {p1, v0, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_0
    return-void
.end method

.method private buildMetaForFAQList(Lorg/json/JSONObject;Lcom/helpshift/conversation/activeconversation/message/FAQListMessageDM;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 1581
    iget-object v0, p2, Lcom/helpshift/conversation/activeconversation/message/FAQListMessageDM;->faqs:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 1582
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V

    .line 1583
    iget-object p2, p2, Lcom/helpshift/conversation/activeconversation/message/FAQListMessageDM;->faqs:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/helpshift/conversation/activeconversation/message/FAQListMessageDM$FAQ;

    .line 1584
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 1585
    iget-object v3, v1, Lcom/helpshift/conversation/activeconversation/message/FAQListMessageDM$FAQ;->title:Ljava/lang/String;

    const-string v4, "faq_title"

    invoke-virtual {v2, v4, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1586
    iget-object v3, v1, Lcom/helpshift/conversation/activeconversation/message/FAQListMessageDM$FAQ;->publishId:Ljava/lang/String;

    const-string v4, "faq_publish_id"

    invoke-virtual {v2, v4, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1587
    iget-object v1, v1, Lcom/helpshift/conversation/activeconversation/message/FAQListMessageDM$FAQ;->language:Ljava/lang/String;

    const-string v3, "faq_language"

    invoke-virtual {v2, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1588
    invoke-virtual {v0, v2}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_0

    :cond_0
    const-string p2, "faqs"

    .line 1590
    invoke-virtual {p1, p2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_1
    return-void
.end method

.method private buildMetaForFollowUpRejected(Lorg/json/JSONObject;Lcom/helpshift/conversation/activeconversation/message/FollowupRejectedMessageDM;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 1666
    iget-object v0, p2, Lcom/helpshift/conversation/activeconversation/message/FollowupRejectedMessageDM;->referredMessageId:Ljava/lang/String;

    const-string v1, "referredMessageId"

    invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1667
    iget v0, p2, Lcom/helpshift/conversation/activeconversation/message/FollowupRejectedMessageDM;->reason:I

    const-string v1, "rejected_reason"

    invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 1668
    iget-object p2, p2, Lcom/helpshift/conversation/activeconversation/message/FollowupRejectedMessageDM;->openConversationId:Ljava/lang/String;

    const-string v0, "rejected_conv_id"

    invoke-virtual {p1, v0, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    return-void
.end method

.method private buildMetaForImageAttachmentMessage(Lorg/json/JSONObject;Lcom/helpshift/conversation/activeconversation/message/ImageAttachmentMessageDM;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 1698
    invoke-direct {p0, p1, p2}, Lcom/helpshift/common/conversation/ConversationDB;->buildJsonObjectForAttachmentMessage(Lorg/json/JSONObject;Lcom/helpshift/conversation/activeconversation/message/AttachmentMessageDM;)V

    .line 1699
    iget-object v0, p2, Lcom/helpshift/conversation/activeconversation/message/ImageAttachmentMessageDM;->thumbnailUrl:Ljava/lang/String;

    const-string v1, "thumbnail_url"

    invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1700
    iget-object v0, p2, Lcom/helpshift/conversation/activeconversation/message/ImageAttachmentMessageDM;->thumbnailFilePath:Ljava/lang/String;

    const-string v1, "thumbnailFilePath"

    invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1701
    iget-boolean p2, p2, Lcom/helpshift/conversation/activeconversation/message/ImageAttachmentMessageDM;->isSecureAttachment:Z

    const-string v0, "is_secure"

    invoke-virtual {p1, v0, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    return-void
.end method

.method private buildMetaForInput(Lorg/json/JSONObject;Lcom/helpshift/conversation/activeconversation/message/input/OptionInput;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 1634
    iget-object v0, p2, Lcom/helpshift/conversation/activeconversation/message/input/OptionInput;->botInfo:Ljava/lang/String;

    invoke-direct {p0, p1, v0}, Lcom/helpshift/common/conversation/ConversationDB;->buildMetaForBotInfo(Lorg/json/JSONObject;Ljava/lang/String;)V

    .line 1635
    iget-boolean v0, p2, Lcom/helpshift/conversation/activeconversation/message/input/OptionInput;->required:Z

    const-string v1, "input_required"

    invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 1636
    iget-object v0, p2, Lcom/helpshift/conversation/activeconversation/message/input/OptionInput;->inputLabel:Ljava/lang/String;

    const-string v1, "input_label"

    invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1637
    iget-object v0, p2, Lcom/helpshift/conversation/activeconversation/message/input/OptionInput;->skipLabel:Ljava/lang/String;

    const-string v1, "input_skip_label"

    invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1638
    iget-object v0, p2, Lcom/helpshift/conversation/activeconversation/message/input/OptionInput;->options:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 1639
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V

    .line 1640
    iget-object v1, p2, Lcom/helpshift/conversation/activeconversation/message/input/OptionInput;->options:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/helpshift/conversation/activeconversation/message/input/OptionInput$Option;

    .line 1641
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    .line 1642
    iget-object v4, v2, Lcom/helpshift/conversation/activeconversation/message/input/OptionInput$Option;->title:Ljava/lang/String;

    const-string v5, "option_title"

    invoke-virtual {v3, v5, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1643
    iget-object v2, v2, Lcom/helpshift/conversation/activeconversation/message/input/OptionInput$Option;->jsonData:Ljava/lang/String;

    const-string v4, "option_data"

    invoke-virtual {v3, v4, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1644
    invoke-virtual {v0, v3}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_0

    :cond_0
    const-string v1, "input_options"

    .line 1646
    invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1648
    :cond_1
    iget-object p2, p2, Lcom/helpshift/conversation/activeconversation/message/input/OptionInput;->type:Lcom/helpshift/conversation/activeconversation/message/input/OptionInput$Type;

    invoke-virtual {p2}, Lcom/helpshift/conversation/activeconversation/message/input/OptionInput$Type;->toString()Ljava/lang/String;

    move-result-object p2

    const-string v0, "option_type"

    invoke-virtual {p1, v0, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    return-void
.end method

.method private buildMetaForInput(Lorg/json/JSONObject;Lcom/helpshift/conversation/activeconversation/message/input/TextInput;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 1624
    iget-object v0, p2, Lcom/helpshift/conversation/activeconversation/message/input/TextInput;->botInfo:Ljava/lang/String;

    invoke-direct {p0, p1, v0}, Lcom/helpshift/common/conversation/ConversationDB;->buildMetaForBotInfo(Lorg/json/JSONObject;Ljava/lang/String;)V

    .line 1625
    iget-boolean v0, p2, Lcom/helpshift/conversation/activeconversation/message/input/TextInput;->required:Z

    const-string v1, "input_required"

    invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 1626
    iget-object v0, p2, Lcom/helpshift/conversation/activeconversation/message/input/TextInput;->skipLabel:Ljava/lang/String;

    const-string v1, "input_skip_label"

    invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1627
    iget-object v0, p2, Lcom/helpshift/conversation/activeconversation/message/input/TextInput;->inputLabel:Ljava/lang/String;

    const-string v1, "input_label"

    invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1628
    iget-object v0, p2, Lcom/helpshift/conversation/activeconversation/message/input/TextInput;->placeholder:Ljava/lang/String;

    const-string v1, "input_placeholder"

    invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1629
    iget p2, p2, Lcom/helpshift/conversation/activeconversation/message/input/TextInput;->keyboard:I

    invoke-direct {p0, p1, p2}, Lcom/helpshift/common/conversation/ConversationDB;->buildMetaForInputKeyboard(Lorg/json/JSONObject;I)V

    return-void
.end method

.method private buildMetaForInputKeyboard(Lorg/json/JSONObject;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    const-string v0, "input_keyboard"

    .line 1608
    invoke-virtual {p1, v0, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    return-void
.end method

.method private buildMetaForIsAnswered(Lorg/json/JSONObject;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    const-string v0, "is_answered"

    .line 1673
    invoke-virtual {p1, v0, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    return-void
.end method

.method private buildMetaForIsMessageEmpty(Lorg/json/JSONObject;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    const-string v0, "is_message_empty"

    .line 1562
    invoke-virtual {p1, v0, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    return-void
.end method

.method private buildMetaForIsResponseSkipped(Lorg/json/JSONObject;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    const-string v0, "is_response_skipped"

    .line 1604
    invoke-virtual {p1, v0, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    return-void
.end method

.method private buildMetaForIsSuggestionsReadEvent(Lorg/json/JSONObject;Lcom/helpshift/conversation/activeconversation/message/FAQListMessageDM;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 1557
    iget-boolean v0, p2, Lcom/helpshift/conversation/activeconversation/message/FAQListMessageDM;->isSuggestionsReadEventSent:Z

    const-string v1, "is_suggestion_read_event_sent"

    invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 1558
    iget-object p2, p2, Lcom/helpshift/conversation/activeconversation/message/FAQListMessageDM;->suggestionsReadFAQPublishId:Ljava/lang/String;

    const-string v0, "suggestion_read_faq_publish_id"

    invoke-virtual {p1, v0, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    return-void
.end method

.method private buildMetaForMessageSeenData(Lorg/json/JSONObject;Lcom/helpshift/conversation/activeconversation/message/MessageDM;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 1653
    iget-object v0, p2, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->seenAtMessageCursor:Ljava/lang/String;

    const-string v1, "seen_cursor"

    invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1654
    iget-boolean v0, p2, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->isMessageSeenSynced:Z

    const-string v1, "seen_sync_status"

    invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 1655
    iget-object p2, p2, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->readAt:Ljava/lang/String;

    const-string v0, "read_at"

    invoke-virtual {p1, v0, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    return-void
.end method

.method private buildMetaForReferredMessageId(Lorg/json/JSONObject;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    const-string v0, "referredMessageId"

    .line 1660
    invoke-virtual {p1, v0, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    return-void
.end method

.method private buildMetaForReferredMessageType(Lorg/json/JSONObject;Lcom/helpshift/conversation/activeconversation/message/MessageType;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 1596
    invoke-virtual {p2}, Lcom/helpshift/conversation/activeconversation/message/MessageType;->getValue()Ljava/lang/String;

    move-result-object p2

    const-string v0, "referred_message_type"

    invoke-virtual {p1, v0, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    return-void
.end method

.method private buildMetaForScreenshotAttachmentMessage(Lorg/json/JSONObject;Lcom/helpshift/conversation/activeconversation/message/ScreenshotMessageDM;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 1689
    invoke-direct {p0, p1, p2}, Lcom/helpshift/common/conversation/ConversationDB;->buildJsonObjectForAttachmentMessage(Lorg/json/JSONObject;Lcom/helpshift/conversation/activeconversation/message/AttachmentMessageDM;)V

    .line 1690
    iget-object v0, p2, Lcom/helpshift/conversation/activeconversation/message/ScreenshotMessageDM;->thumbnailUrl:Ljava/lang/String;

    const-string v1, "thumbnail_url"

    invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1691
    iget-object v0, p2, Lcom/helpshift/conversation/activeconversation/message/ScreenshotMessageDM;->refersMessageId:Ljava/lang/String;

    const-string v1, "referredMessageId"

    invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1692
    iget-boolean p2, p2, Lcom/helpshift/conversation/activeconversation/message/ScreenshotMessageDM;->isSecureAttachment:Z

    const-string v0, "is_secure"

    invoke-virtual {p1, v0, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    return-void
.end method

.method private buildMetaForSelectedOptionData(Lorg/json/JSONObject;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    const-string v0, "selected_option_data"

    .line 1600
    invoke-virtual {p1, v0, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    return-void
.end method

.method private buildMetaForUserBotControlMessage(Lorg/json/JSONObject;Lcom/helpshift/conversation/activeconversation/message/UserBotControlMessageDM;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 1573
    iget-object v0, p2, Lcom/helpshift/conversation/activeconversation/message/UserBotControlMessageDM;->actionType:Ljava/lang/String;

    const-string v1, "bot_action_type"

    invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1574
    iget-object v0, p2, Lcom/helpshift/conversation/activeconversation/message/UserBotControlMessageDM;->botInfo:Ljava/lang/String;

    const-string v1, "chatbot_info"

    invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1575
    iget-object v0, p2, Lcom/helpshift/conversation/activeconversation/message/UserBotControlMessageDM;->reason:Ljava/lang/String;

    const-string v1, "bot_ended_reason"

    invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1577
    iget-object p2, p2, Lcom/helpshift/conversation/activeconversation/message/UserBotControlMessageDM;->refersMessageId:Ljava/lang/String;

    const-string v0, "referredMessageId"

    invoke-virtual {p1, v0, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    return-void
.end method

.method private conversationInboxRecordToContentValues(Lcom/helpshift/conversation/dto/dao/ConversationInboxRecord;)Landroid/content/ContentValues;
    .locals 3

    .line 805
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 806
    iget-object v1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-wide v1, p1, Lcom/helpshift/conversation/dto/dao/ConversationInboxRecord;->userLocalId:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const-string v2, "user_local_id"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 807
    iget-object v1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v1, p1, Lcom/helpshift/conversation/dto/dao/ConversationInboxRecord;->formName:Ljava/lang/String;

    const-string v2, "form_name"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 808
    iget-object v1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v1, p1, Lcom/helpshift/conversation/dto/dao/ConversationInboxRecord;->formEmail:Ljava/lang/String;

    const-string v2, "form_email"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 809
    iget-object v1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v1, p1, Lcom/helpshift/conversation/dto/dao/ConversationInboxRecord;->description:Ljava/lang/String;

    const-string v2, "description_draft"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 810
    iget-object v1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-wide v1, p1, Lcom/helpshift/conversation/dto/dao/ConversationInboxRecord;->descriptionTimeStamp:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const-string v2, "description_draft_timestamp"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 811
    iget-object v1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget v1, p1, Lcom/helpshift/conversation/dto/dao/ConversationInboxRecord;->descriptionType:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "description_type"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 812
    iget-object v1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v1, p1, Lcom/helpshift/conversation/dto/dao/ConversationInboxRecord;->archivalText:Ljava/lang/String;

    const-string v2, "archival_text"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 813
    iget-object v1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v1, p1, Lcom/helpshift/conversation/dto/dao/ConversationInboxRecord;->replyText:Ljava/lang/String;

    const-string v2, "reply_text"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 814
    iget-object v1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-boolean v1, p1, Lcom/helpshift/conversation/dto/dao/ConversationInboxRecord;->persistMessageBox:Z

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "persist_message_box"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 815
    iget-object v1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v1, p1, Lcom/helpshift/conversation/dto/dao/ConversationInboxRecord;->lastSyncTimestamp:Ljava/lang/String;

    const-string v2, "since"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 817
    iget-object v1, p1, Lcom/helpshift/conversation/dto/dao/ConversationInboxRecord;->hasOlderMessages:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 818
    iget-object v1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v1, p1, Lcom/helpshift/conversation/dto/dao/ConversationInboxRecord;->hasOlderMessages:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "has_older_messages"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 820
    :cond_0
    iget-object v1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v1, p1, Lcom/helpshift/conversation/dto/dao/ConversationInboxRecord;->lastConversationsRedactionTime:Ljava/lang/Long;

    const-string v2, "last_conv_redaction_time"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 823
    :try_start_0
    iget-object p1, p1, Lcom/helpshift/conversation/dto/dao/ConversationInboxRecord;->imageAttachmentDraft:Lcom/helpshift/conversation/dto/ImagePickerFile;

    invoke-direct {p0, p1}, Lcom/helpshift/common/conversation/ConversationDB;->getImageAttachmentDraftMeta(Lcom/helpshift/conversation/dto/ImagePickerFile;)Ljava/lang/String;

    move-result-object p1

    .line 824
    iget-object v1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v1, "attachment_draft"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    const-string v1, "Helpshift_ConverDB"

    const-string v2, "Error in generating meta string for image attachment"

    .line 827
    invoke-static {v1, v2, p1}, Lcom/helpshift/util/HSLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_0
    return-object v0
.end method

.method private cursorToConversationInboxRecord(Landroid/database/Cursor;)Lcom/helpshift/conversation/dto/dao/ConversationInboxRecord;
    .locals 19

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    .line 775
    iget-object v2, v0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v2, "user_local_id"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 776
    iget-object v2, v0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v2, "form_name"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 777
    iget-object v2, v0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v2, "form_email"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 778
    iget-object v2, v0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v2, "description_draft"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 779
    iget-object v2, v0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v2, "description_draft_timestamp"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v9

    .line 780
    iget-object v2, v0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v2, "attachment_draft"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 781
    invoke-direct {v0, v2}, Lcom/helpshift/common/conversation/ConversationDB;->parseAndGetImageAttachmentDraft(Ljava/lang/String;)Lcom/helpshift/conversation/dto/ImagePickerFile;

    move-result-object v11

    .line 782
    iget-object v2, v0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v2, "description_type"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    .line 783
    iget-object v2, v0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v2, "archival_text"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 784
    iget-object v2, v0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v2, "reply_text"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 785
    iget-object v2, v0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v2, "persist_message_box"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    const/4 v15, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    const/4 v15, 0x0

    .line 786
    :goto_0
    iget-object v2, v0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v2, "since"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 787
    iget-object v2, v0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v2, "has_older_messages"

    invoke-static {v1, v2}, Lcom/helpshift/util/DatabaseUtils;->parseBooleanColumnSafe(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v17

    .line 788
    iget-object v2, v0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-class v2, Ljava/lang/Long;

    const-string v3, "last_conv_redaction_time"

    invoke-static {v1, v3, v2}, Lcom/helpshift/util/DatabaseUtils;->parseColumnSafe(Landroid/database/Cursor;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v18, v1

    check-cast v18, Ljava/lang/Long;

    .line 789
    new-instance v1, Lcom/helpshift/conversation/dto/dao/ConversationInboxRecord;

    move-object v3, v1

    invoke-direct/range {v3 .. v18}, Lcom/helpshift/conversation/dto/dao/ConversationInboxRecord;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;JLcom/helpshift/conversation/dto/ImagePickerFile;ILjava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/Boolean;Ljava/lang/Long;)V

    return-object v1
.end method

.method private cursorToFaq(Landroid/database/Cursor;)Lcom/helpshift/support/Faq;
    .locals 14

    .line 1824
    new-instance v13, Lcom/helpshift/support/Faq;

    const-string v0, "_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    const-string v0, "question_id"

    .line 1825
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v0, "publish_id"

    .line 1826
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v0, "language"

    .line 1827
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v0, "section_id"

    .line 1828
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v0, "title"

    .line 1829
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    const-string v0, "body"

    .line 1830
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    const-string v0, "helpful"

    .line 1831
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    const-string v0, "rtl"

    .line 1832
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    const/4 v10, 0x1

    if-ne v0, v10, :cond_0

    goto :goto_0

    :cond_0
    const/4 v10, 0x0

    :goto_0
    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    const-string v0, "tags"

    .line 1833
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/helpshift/util/HSJSONUtils;->jsonToStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v11

    const-string v0, "c_tags"

    .line 1835
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/helpshift/util/HSJSONUtils;->jsonToStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v12

    move-object v0, v13

    invoke-direct/range {v0 .. v12}, Lcom/helpshift/support/Faq;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Boolean;Ljava/util/List;Ljava/util/List;)V

    return-object v13
.end method

.method private cursorToMessageDM(Landroid/database/Cursor;)Lcom/helpshift/conversation/activeconversation/message/MessageDM;
    .locals 27

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    .line 1000
    iget-object v2, v0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v2, "_id"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 1001
    iget-object v4, v0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v4, "conversation_id"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 1002
    iget-object v6, v0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v6, "server_id"

    invoke-interface {v1, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 1003
    iget-object v6, v0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v6, "body"

    invoke-interface {v1, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 1004
    iget-object v6, v0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v6, "author_name"

    invoke-interface {v1, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 1005
    iget-object v6, v0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v6, "meta"

    invoke-interface {v1, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 1006
    iget-object v7, v0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v7, "type"

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 1007
    iget-object v9, v0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v9}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v9, "created_at"

    invoke-interface {v1, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {v1, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 1008
    invoke-static {v11}, Lcom/helpshift/common/util/HSDateFormatSpec;->convertToEpochTime(Ljava/lang/String;)J

    move-result-wide v12

    .line 1009
    iget-object v9, v0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v9}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v9, "md_state"

    invoke-interface {v1, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {v1, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    .line 1010
    iget-object v9, v0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v9}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v9, "is_redacted"

    move/from16 v16, v15

    const/4 v15, 0x0

    invoke-static {v1, v9, v15}, Lcom/helpshift/util/DatabaseUtils;->parseBooleanColumnSafe(Landroid/database/Cursor;Ljava/lang/String;Z)Z

    move-result v1

    .line 1011
    invoke-static {v7}, Lcom/helpshift/conversation/activeconversation/message/MessageType;->fromValue(Ljava/lang/String;)Lcom/helpshift/conversation/activeconversation/message/MessageType;

    move-result-object v7

    .line 1013
    invoke-direct {v0, v6}, Lcom/helpshift/common/conversation/ConversationDB;->jsonify(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v6

    .line 1014
    sget-object v9, Lcom/helpshift/common/conversation/ConversationDB$1;->$SwitchMap$com$helpshift$conversation$activeconversation$message$MessageType:[I

    invoke-virtual {v7}, Lcom/helpshift/conversation/activeconversation/message/MessageType;->ordinal()I

    move-result v7

    aget v7, v9, v7

    packed-switch v7, :pswitch_data_0

    const/4 v1, 0x0

    return-object v1

    .line 1216
    :pswitch_0
    invoke-direct {v0, v6}, Lcom/helpshift/common/conversation/ConversationDB;->parseBotActionTypeFromMeta(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v15

    .line 1217
    invoke-direct {v0, v6}, Lcom/helpshift/common/conversation/ConversationDB;->parseBotInfoFromMeta(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v17

    .line 1218
    invoke-direct {v0, v6}, Lcom/helpshift/common/conversation/ConversationDB;->parseBotEndedReasonFromMeta(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v7

    .line 1219
    invoke-direct {v0, v6}, Lcom/helpshift/common/conversation/ConversationDB;->parseReferredMessageIdFromMeta(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v18

    .line 1220
    invoke-direct {v0, v8, v6}, Lcom/helpshift/common/conversation/ConversationDB;->parseAndGetMessageSyncState(Ljava/lang/String;Lorg/json/JSONObject;)I

    move-result v19

    .line 1222
    new-instance v9, Lcom/helpshift/conversation/activeconversation/message/UserBotControlMessageDM;

    move-object/from16 p1, v9

    move/from16 v22, v1

    move/from16 v1, v16

    move-object/from16 v16, v7

    invoke-direct/range {v9 .. v19}, Lcom/helpshift/conversation/activeconversation/message/UserBotControlMessageDM;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    move-object/from16 v7, p1

    .line 1224
    iput-object v8, v7, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->serverId:Ljava/lang/String;

    move/from16 p1, v1

    move-wide/from16 v23, v2

    move-wide/from16 v25, v4

    move-object v1, v7

    goto/16 :goto_2

    :pswitch_1
    move/from16 v22, v1

    move/from16 v1, v16

    .line 1206
    invoke-direct {v0, v6}, Lcom/helpshift/common/conversation/ConversationDB;->parseBotActionTypeFromMeta(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v15

    .line 1207
    invoke-direct {v0, v6}, Lcom/helpshift/common/conversation/ConversationDB;->parseBotInfoFromMeta(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v16

    .line 1208
    invoke-direct {v0, v6}, Lcom/helpshift/common/conversation/ConversationDB;->parseHasNextBotFromMeta(Lorg/json/JSONObject;)Ljava/lang/Boolean;

    move-result-object v17

    .line 1209
    new-instance v9, Lcom/helpshift/conversation/activeconversation/message/AdminBotControlMessageDM;

    move-object v7, v9

    move/from16 p1, v1

    move-object v1, v9

    move-object v9, v10

    move-object v10, v11

    move-wide v11, v12

    move-object v13, v14

    move-object v14, v15

    move-object/from16 v15, v16

    invoke-direct/range {v7 .. v15}, Lcom/helpshift/conversation/activeconversation/message/AdminBotControlMessageDM;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1212
    invoke-virtual/range {v17 .. v17}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    iput-boolean v7, v1, Lcom/helpshift/conversation/activeconversation/message/AdminBotControlMessageDM;->hasNextBot:Z

    goto :goto_0

    :pswitch_2
    move/from16 v22, v1

    move/from16 p1, v16

    .line 1196
    new-instance v1, Lcom/helpshift/conversation/activeconversation/message/RequestForReopenMessageDM;

    move-object v7, v1

    move-object v9, v10

    move-object v10, v11

    move-wide v11, v12

    move-object v13, v14

    invoke-direct/range {v7 .. v13}, Lcom/helpshift/conversation/activeconversation/message/RequestForReopenMessageDM;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V

    .line 1202
    invoke-direct {v0, v6}, Lcom/helpshift/common/conversation/ConversationDB;->parseIsAnsweredFromMeta(Lorg/json/JSONObject;)Z

    move-result v7

    invoke-virtual {v1, v7}, Lcom/helpshift/conversation/activeconversation/message/RequestForReopenMessageDM;->setAnswered(Z)V

    :goto_0
    move-wide/from16 v23, v2

    move-wide/from16 v25, v4

    goto/16 :goto_2

    :pswitch_3
    move/from16 v22, v1

    move/from16 p1, v16

    .line 1177
    invoke-direct {v0, v6}, Lcom/helpshift/common/conversation/ConversationDB;->parseImageAttachmentInfoFromMeta(Lorg/json/JSONObject;)Lcom/helpshift/common/conversation/ConversationDB$ImageAttachmentInfo;

    move-result-object v1

    .line 1178
    new-instance v15, Lcom/helpshift/conversation/activeconversation/message/AdminImageAttachmentMessageDM;

    iget-object v9, v1, Lcom/helpshift/common/conversation/ConversationDB$ImageAttachmentInfo;->url:Ljava/lang/String;

    iget-object v7, v1, Lcom/helpshift/common/conversation/ConversationDB$ImageAttachmentInfo;->fileName:Ljava/lang/String;

    move-wide/from16 v23, v2

    iget-object v2, v1, Lcom/helpshift/common/conversation/ConversationDB$ImageAttachmentInfo;->thumbnailUrl:Ljava/lang/String;

    iget-object v3, v1, Lcom/helpshift/common/conversation/ConversationDB$ImageAttachmentInfo;->contentType:Ljava/lang/String;

    move-wide/from16 v25, v4

    iget-boolean v4, v1, Lcom/helpshift/common/conversation/ConversationDB$ImageAttachmentInfo;->isSecure:Z

    iget v5, v1, Lcom/helpshift/common/conversation/ConversationDB$ImageAttachmentInfo;->size:I

    move-object/from16 v16, v7

    move-object v7, v15

    move-object/from16 v17, v9

    move-object v9, v10

    move-object v10, v11

    move-wide v11, v12

    move-object v13, v14

    move-object/from16 v14, v17

    move-object v0, v15

    move-object/from16 v15, v16

    move-object/from16 v16, v2

    move-object/from16 v17, v3

    move/from16 v18, v4

    move/from16 v19, v5

    invoke-direct/range {v7 .. v19}, Lcom/helpshift/conversation/activeconversation/message/AdminImageAttachmentMessageDM;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZI)V

    .line 1190
    iget-object v2, v1, Lcom/helpshift/common/conversation/ConversationDB$ImageAttachmentInfo;->filePath:Ljava/lang/String;

    iput-object v2, v0, Lcom/helpshift/conversation/activeconversation/message/AdminImageAttachmentMessageDM;->filePath:Ljava/lang/String;

    .line 1191
    iget-object v1, v1, Lcom/helpshift/common/conversation/ConversationDB$ImageAttachmentInfo;->thumbnailFilePath:Ljava/lang/String;

    iput-object v1, v0, Lcom/helpshift/conversation/activeconversation/message/AdminImageAttachmentMessageDM;->thumbnailFilePath:Ljava/lang/String;

    .line 1192
    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/message/AdminImageAttachmentMessageDM;->updateState()V

    move-object v1, v0

    move-object/from16 v0, p0

    goto/16 :goto_2

    :pswitch_4
    move/from16 v22, v1

    move-wide/from16 v23, v2

    move-wide/from16 v25, v4

    move/from16 p1, v16

    .line 1161
    invoke-direct {v0, v6}, Lcom/helpshift/common/conversation/ConversationDB;->parseAttachmentInfoFromMeta(Lorg/json/JSONObject;)Lcom/helpshift/common/conversation/ConversationDB$AttachmentInfo;

    move-result-object v1

    .line 1162
    new-instance v2, Lcom/helpshift/conversation/activeconversation/message/AdminAttachmentMessageDM;

    iget v3, v1, Lcom/helpshift/common/conversation/ConversationDB$AttachmentInfo;->size:I

    iget-object v15, v1, Lcom/helpshift/common/conversation/ConversationDB$AttachmentInfo;->contentType:Ljava/lang/String;

    iget-object v4, v1, Lcom/helpshift/common/conversation/ConversationDB$AttachmentInfo;->url:Ljava/lang/String;

    iget-object v5, v1, Lcom/helpshift/common/conversation/ConversationDB$AttachmentInfo;->fileName:Ljava/lang/String;

    iget-boolean v9, v1, Lcom/helpshift/common/conversation/ConversationDB$AttachmentInfo;->isSecure:Z

    move-object v7, v2

    move/from16 v18, v9

    move-object v9, v10

    move-object v10, v11

    move-wide v11, v12

    move-object v13, v14

    move v14, v3

    move-object/from16 v16, v4

    move-object/from16 v17, v5

    invoke-direct/range {v7 .. v18}, Lcom/helpshift/conversation/activeconversation/message/AdminAttachmentMessageDM;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1172
    iget-object v1, v1, Lcom/helpshift/common/conversation/ConversationDB$AttachmentInfo;->filePath:Ljava/lang/String;

    iput-object v1, v2, Lcom/helpshift/conversation/activeconversation/message/AdminAttachmentMessageDM;->filePath:Ljava/lang/String;

    .line 1173
    invoke-virtual {v2}, Lcom/helpshift/conversation/activeconversation/message/AdminAttachmentMessageDM;->updateState()V

    goto :goto_1

    :pswitch_5
    move/from16 v22, v1

    move-wide/from16 v23, v2

    move-wide/from16 v25, v4

    move/from16 p1, v16

    .line 1153
    new-instance v1, Lcom/helpshift/conversation/activeconversation/message/RequestScreenshotMessageDM;

    .line 1158
    invoke-direct {v0, v6}, Lcom/helpshift/common/conversation/ConversationDB;->parseIsAnsweredFromMeta(Lorg/json/JSONObject;)Z

    move-result v2

    move-object v7, v1

    move-object v9, v10

    move-object v10, v11

    move-wide v11, v12

    move-object v13, v14

    move v14, v2

    invoke-direct/range {v7 .. v14}, Lcom/helpshift/conversation/activeconversation/message/RequestScreenshotMessageDM;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Z)V

    goto/16 :goto_2

    :pswitch_6
    move/from16 v22, v1

    move-wide/from16 v23, v2

    move-wide/from16 v25, v4

    move/from16 p1, v16

    .line 1136
    invoke-direct {v0, v6}, Lcom/helpshift/common/conversation/ConversationDB;->parseImageAttachmentInfoFromMeta(Lorg/json/JSONObject;)Lcom/helpshift/common/conversation/ConversationDB$ImageAttachmentInfo;

    move-result-object v1

    .line 1137
    new-instance v2, Lcom/helpshift/conversation/activeconversation/message/ScreenshotMessageDM;

    iget-object v15, v1, Lcom/helpshift/common/conversation/ConversationDB$ImageAttachmentInfo;->contentType:Ljava/lang/String;

    iget-object v3, v1, Lcom/helpshift/common/conversation/ConversationDB$ImageAttachmentInfo;->thumbnailUrl:Ljava/lang/String;

    iget-object v4, v1, Lcom/helpshift/common/conversation/ConversationDB$ImageAttachmentInfo;->fileName:Ljava/lang/String;

    iget-object v5, v1, Lcom/helpshift/common/conversation/ConversationDB$ImageAttachmentInfo;->url:Ljava/lang/String;

    iget v7, v1, Lcom/helpshift/common/conversation/ConversationDB$ImageAttachmentInfo;->size:I

    iget-boolean v9, v1, Lcom/helpshift/common/conversation/ConversationDB$ImageAttachmentInfo;->isSecure:Z

    move/from16 v20, v9

    move-object v9, v2

    move-object/from16 v16, v3

    move-object/from16 v17, v4

    move-object/from16 v18, v5

    move/from16 v19, v7

    invoke-direct/range {v9 .. v20}, Lcom/helpshift/conversation/activeconversation/message/ScreenshotMessageDM;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)V

    .line 1147
    iget-object v1, v1, Lcom/helpshift/common/conversation/ConversationDB$ImageAttachmentInfo;->filePath:Ljava/lang/String;

    iput-object v1, v2, Lcom/helpshift/conversation/activeconversation/message/ScreenshotMessageDM;->filePath:Ljava/lang/String;

    .line 1148
    iput-object v8, v2, Lcom/helpshift/conversation/activeconversation/message/ScreenshotMessageDM;->serverId:Ljava/lang/String;

    .line 1149
    invoke-direct {v0, v6}, Lcom/helpshift/common/conversation/ConversationDB;->parseReferredMessageIdFromMeta(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/helpshift/conversation/activeconversation/message/ScreenshotMessageDM;->setRefersMessageId(Ljava/lang/String;)V

    :goto_1
    move-object v1, v2

    goto/16 :goto_2

    :pswitch_7
    move/from16 v22, v1

    move-wide/from16 v23, v2

    move-wide/from16 v25, v4

    move/from16 p1, v16

    .line 1128
    new-instance v1, Lcom/helpshift/conversation/activeconversation/message/ConfirmationRejectedMessageDM;

    .line 1132
    invoke-direct {v0, v8, v6}, Lcom/helpshift/common/conversation/ConversationDB;->parseAndGetMessageSyncState(Ljava/lang/String;Lorg/json/JSONObject;)I

    move-result v15

    move-object v9, v1

    invoke-direct/range {v9 .. v15}, Lcom/helpshift/conversation/activeconversation/message/ConfirmationRejectedMessageDM;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;I)V

    .line 1133
    iput-object v8, v1, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->serverId:Ljava/lang/String;

    goto/16 :goto_2

    :pswitch_8
    move/from16 v22, v1

    move-wide/from16 v23, v2

    move-wide/from16 v25, v4

    move/from16 p1, v16

    .line 1120
    new-instance v1, Lcom/helpshift/conversation/activeconversation/message/ConfirmationAcceptedMessageDM;

    .line 1124
    invoke-direct {v0, v8, v6}, Lcom/helpshift/common/conversation/ConversationDB;->parseAndGetMessageSyncState(Ljava/lang/String;Lorg/json/JSONObject;)I

    move-result v15

    move-object v9, v1

    invoke-direct/range {v9 .. v15}, Lcom/helpshift/conversation/activeconversation/message/ConfirmationAcceptedMessageDM;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;I)V

    .line 1125
    iput-object v8, v1, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->serverId:Ljava/lang/String;

    goto/16 :goto_2

    :pswitch_9
    move/from16 v22, v1

    move-wide/from16 v23, v2

    move-wide/from16 v25, v4

    move/from16 p1, v16

    .line 1110
    new-instance v1, Lcom/helpshift/conversation/activeconversation/message/FollowupRejectedMessageDM;

    .line 1114
    invoke-direct {v0, v6}, Lcom/helpshift/common/conversation/ConversationDB;->parseReferredMessageIdFromMeta(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v15

    .line 1115
    invoke-direct {v0, v8, v6}, Lcom/helpshift/common/conversation/ConversationDB;->parseAndGetMessageSyncState(Ljava/lang/String;Lorg/json/JSONObject;)I

    move-result v16

    move-object v9, v1

    invoke-direct/range {v9 .. v16}, Lcom/helpshift/conversation/activeconversation/message/FollowupRejectedMessageDM;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;I)V

    .line 1116
    iput-object v8, v1, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->serverId:Ljava/lang/String;

    .line 1117
    move-object v2, v1

    check-cast v2, Lcom/helpshift/conversation/activeconversation/message/FollowupRejectedMessageDM;

    invoke-direct {v0, v2, v6}, Lcom/helpshift/common/conversation/ConversationDB;->parseAndSetFollowUpRejectedDataFromMeta(Lcom/helpshift/conversation/activeconversation/message/FollowupRejectedMessageDM;Lorg/json/JSONObject;)V

    goto/16 :goto_2

    :pswitch_a
    move/from16 v22, v1

    move-wide/from16 v23, v2

    move-wide/from16 v25, v4

    move/from16 p1, v16

    .line 1101
    new-instance v1, Lcom/helpshift/conversation/activeconversation/message/FollowupAcceptedMessageDM;

    .line 1105
    invoke-direct {v0, v6}, Lcom/helpshift/common/conversation/ConversationDB;->parseReferredMessageIdFromMeta(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v15

    .line 1106
    invoke-direct {v0, v8, v6}, Lcom/helpshift/common/conversation/ConversationDB;->parseAndGetMessageSyncState(Ljava/lang/String;Lorg/json/JSONObject;)I

    move-result v16

    move-object v9, v1

    invoke-direct/range {v9 .. v16}, Lcom/helpshift/conversation/activeconversation/message/FollowupAcceptedMessageDM;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;I)V

    .line 1107
    iput-object v8, v1, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->serverId:Ljava/lang/String;

    goto/16 :goto_2

    :pswitch_b
    move/from16 v22, v1

    move-wide/from16 v23, v2

    move-wide/from16 v25, v4

    move/from16 p1, v16

    .line 1093
    new-instance v1, Lcom/helpshift/conversation/activeconversation/message/RequestAppReviewMessageDM;

    .line 1098
    invoke-direct {v0, v6}, Lcom/helpshift/common/conversation/ConversationDB;->parseIsAnsweredFromMeta(Lorg/json/JSONObject;)Z

    move-result v2

    move-object v7, v1

    move-object v9, v10

    move-object v10, v11

    move-wide v11, v12

    move-object v13, v14

    move v14, v2

    invoke-direct/range {v7 .. v14}, Lcom/helpshift/conversation/activeconversation/message/RequestAppReviewMessageDM;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Z)V

    goto/16 :goto_2

    :pswitch_c
    move/from16 v22, v1

    move-wide/from16 v23, v2

    move-wide/from16 v25, v4

    move/from16 p1, v16

    .line 1084
    new-instance v1, Lcom/helpshift/conversation/activeconversation/message/AcceptedAppReviewMessageDM;

    .line 1088
    invoke-direct {v0, v6}, Lcom/helpshift/common/conversation/ConversationDB;->parseReferredMessageIdFromMeta(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v15

    .line 1089
    invoke-direct {v0, v8, v6}, Lcom/helpshift/common/conversation/ConversationDB;->parseAndGetMessageSyncState(Ljava/lang/String;Lorg/json/JSONObject;)I

    move-result v16

    move-object v9, v1

    invoke-direct/range {v9 .. v16}, Lcom/helpshift/conversation/activeconversation/message/AcceptedAppReviewMessageDM;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;I)V

    .line 1090
    iput-object v8, v1, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->serverId:Ljava/lang/String;

    goto/16 :goto_2

    :pswitch_d
    move/from16 v22, v1

    move-wide/from16 v23, v2

    move-wide/from16 v25, v4

    move/from16 p1, v16

    .line 1073
    new-instance v1, Lcom/helpshift/conversation/activeconversation/message/FAQListMessageWithOptionInputDM;

    .line 1074
    invoke-direct {v0, v6}, Lcom/helpshift/common/conversation/ConversationDB;->parseFAQListFromMeta(Lorg/json/JSONObject;)Ljava/util/List;

    move-result-object v2

    .line 1075
    invoke-direct {v0, v6}, Lcom/helpshift/common/conversation/ConversationDB;->parseBotInfoFromMeta(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v15

    .line 1076
    invoke-direct {v0, v6}, Lcom/helpshift/common/conversation/ConversationDB;->parseInputRequiredFromMeta(Lorg/json/JSONObject;)Z

    move-result v16

    .line 1077
    invoke-direct {v0, v6}, Lcom/helpshift/common/conversation/ConversationDB;->parseInputLabelFromMeta(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v17

    .line 1078
    invoke-direct {v0, v6}, Lcom/helpshift/common/conversation/ConversationDB;->parseInputSkipLabelFromMeta(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v18

    .line 1079
    invoke-direct {v0, v6}, Lcom/helpshift/common/conversation/ConversationDB;->parseInputOptionsFromMeta(Lorg/json/JSONObject;)Ljava/util/List;

    move-result-object v19

    .line 1080
    invoke-direct {v0, v6}, Lcom/helpshift/common/conversation/ConversationDB;->parseIsSuggestionsReadEventSent(Lorg/json/JSONObject;)Z

    move-result v20

    .line 1081
    invoke-direct {v0, v6}, Lcom/helpshift/common/conversation/ConversationDB;->parseSuggestionReadFAQPublishId(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v21

    move-object v7, v1

    move-object v9, v10

    move-object v10, v11

    move-wide v11, v12

    move-object v13, v14

    move-object v14, v2

    invoke-direct/range {v7 .. v21}, Lcom/helpshift/conversation/activeconversation/message/FAQListMessageWithOptionInputDM;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/util/List;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/util/List;ZLjava/lang/String;)V

    goto/16 :goto_2

    :pswitch_e
    move/from16 v22, v1

    move-wide/from16 v23, v2

    move-wide/from16 v25, v4

    move/from16 p1, v16

    .line 1067
    new-instance v1, Lcom/helpshift/conversation/activeconversation/message/FAQListMessageDM;

    .line 1068
    invoke-direct {v0, v6}, Lcom/helpshift/common/conversation/ConversationDB;->parseFAQListFromMeta(Lorg/json/JSONObject;)Ljava/util/List;

    move-result-object v2

    .line 1069
    invoke-direct {v0, v6}, Lcom/helpshift/common/conversation/ConversationDB;->parseIsSuggestionsReadEventSent(Lorg/json/JSONObject;)Z

    move-result v15

    .line 1070
    invoke-direct {v0, v6}, Lcom/helpshift/common/conversation/ConversationDB;->parseSuggestionReadFAQPublishId(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v16

    move-object v7, v1

    move-object v9, v10

    move-object v10, v11

    move-wide v11, v12

    move-object v13, v14

    move-object v14, v2

    invoke-direct/range {v7 .. v16}, Lcom/helpshift/conversation/activeconversation/message/FAQListMessageDM;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/util/List;ZLjava/lang/String;)V

    goto/16 :goto_2

    :pswitch_f
    move/from16 v22, v1

    move-wide/from16 v23, v2

    move-wide/from16 v25, v4

    move/from16 p1, v16

    .line 1056
    invoke-direct {v0, v6}, Lcom/helpshift/common/conversation/ConversationDB;->parseInputOptionsFromMeta(Lorg/json/JSONObject;)Ljava/util/List;

    move-result-object v18

    .line 1057
    new-instance v1, Lcom/helpshift/conversation/activeconversation/message/AdminMessageWithOptionInputDM;

    .line 1059
    invoke-direct {v0, v6}, Lcom/helpshift/common/conversation/ConversationDB;->parseBotInfoFromMeta(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v2

    .line 1060
    invoke-direct {v0, v6}, Lcom/helpshift/common/conversation/ConversationDB;->parseInputRequiredFromMeta(Lorg/json/JSONObject;)Z

    move-result v15

    .line 1061
    invoke-direct {v0, v6}, Lcom/helpshift/common/conversation/ConversationDB;->parseInputLabelFromMeta(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v16

    .line 1062
    invoke-direct {v0, v6}, Lcom/helpshift/common/conversation/ConversationDB;->parseInputSkipLabelFromMeta(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v17

    .line 1064
    invoke-interface/range {v18 .. v18}, Ljava/util/List;->size()I

    move-result v3

    invoke-direct {v0, v6, v3}, Lcom/helpshift/common/conversation/ConversationDB;->parseInputOptionTypeFromMeta(Lorg/json/JSONObject;I)Lcom/helpshift/conversation/activeconversation/message/input/OptionInput$Type;

    move-result-object v19

    move-object v7, v1

    move-object v9, v10

    move-object v10, v11

    move-wide v11, v12

    move-object v13, v14

    move-object v14, v2

    invoke-direct/range {v7 .. v19}, Lcom/helpshift/conversation/activeconversation/message/AdminMessageWithOptionInputDM;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/helpshift/conversation/activeconversation/message/input/OptionInput$Type;)V

    goto/16 :goto_2

    :pswitch_10
    move/from16 v22, v1

    move-wide/from16 v23, v2

    move-wide/from16 v25, v4

    move/from16 p1, v16

    .line 1046
    new-instance v1, Lcom/helpshift/conversation/activeconversation/message/AdminMessageWithTextInputDM;

    .line 1047
    invoke-direct {v0, v6}, Lcom/helpshift/common/conversation/ConversationDB;->parseBotInfoFromMeta(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v2

    .line 1048
    invoke-direct {v0, v6}, Lcom/helpshift/common/conversation/ConversationDB;->parseInputPlaceholderFromMeta(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v15

    .line 1049
    invoke-direct {v0, v6}, Lcom/helpshift/common/conversation/ConversationDB;->parseInputRequiredFromMeta(Lorg/json/JSONObject;)Z

    move-result v16

    .line 1050
    invoke-direct {v0, v6}, Lcom/helpshift/common/conversation/ConversationDB;->parseInputLabelFromMeta(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v17

    .line 1051
    invoke-direct {v0, v6}, Lcom/helpshift/common/conversation/ConversationDB;->parseInputSkipLabelFromMeta(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v18

    .line 1052
    invoke-direct {v0, v6}, Lcom/helpshift/common/conversation/ConversationDB;->parseInputKeyboardFromMeta(Lorg/json/JSONObject;)I

    move-result v19

    .line 1053
    invoke-direct {v0, v6}, Lcom/helpshift/common/conversation/ConversationDB;->parseIsMessageEmptyFromMeta(Lorg/json/JSONObject;)Z

    move-result v20

    move-object v7, v1

    move-object v9, v10

    move-object v10, v11

    move-wide v11, v12

    move-object v13, v14

    move-object v14, v2

    invoke-direct/range {v7 .. v20}, Lcom/helpshift/conversation/activeconversation/message/AdminMessageWithTextInputDM;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;IZ)V

    goto/16 :goto_2

    :pswitch_11
    move/from16 v22, v1

    move-wide/from16 v23, v2

    move-wide/from16 v25, v4

    move/from16 p1, v16

    .line 1043
    new-instance v1, Lcom/helpshift/conversation/activeconversation/message/AdminMessageDM;

    move-object v7, v1

    move-object v9, v10

    move-object v10, v11

    move-wide v11, v12

    move-object v13, v14

    invoke-direct/range {v7 .. v13}, Lcom/helpshift/conversation/activeconversation/message/AdminMessageDM;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V

    goto :goto_2

    :pswitch_12
    move/from16 v22, v1

    move-wide/from16 v23, v2

    move-wide/from16 v25, v4

    move/from16 p1, v16

    .line 1034
    new-instance v1, Lcom/helpshift/conversation/activeconversation/message/UserResponseMessageForOptionInput;

    .line 1035
    invoke-direct {v0, v6}, Lcom/helpshift/common/conversation/ConversationDB;->parseBotInfoFromMeta(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v15

    .line 1036
    invoke-direct {v0, v6}, Lcom/helpshift/common/conversation/ConversationDB;->parseIsResponseSkippedFromMeta(Lorg/json/JSONObject;)Z

    move-result v16

    .line 1037
    invoke-direct {v0, v6}, Lcom/helpshift/common/conversation/ConversationDB;->parseSelectedOptionDataFromMeta(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v17

    .line 1038
    invoke-direct {v0, v6}, Lcom/helpshift/common/conversation/ConversationDB;->parseReferredMessageIdFromMeta(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v18

    .line 1039
    invoke-direct {v0, v6}, Lcom/helpshift/common/conversation/ConversationDB;->parseReferredMessageTypeFromMeta(Lorg/json/JSONObject;)Lcom/helpshift/conversation/activeconversation/message/MessageType;

    move-result-object v19

    move-object v9, v1

    invoke-direct/range {v9 .. v19}, Lcom/helpshift/conversation/activeconversation/message/UserResponseMessageForOptionInput;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Lcom/helpshift/conversation/activeconversation/message/MessageType;)V

    .line 1040
    iput-object v8, v1, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->serverId:Ljava/lang/String;

    goto :goto_2

    :pswitch_13
    move/from16 v22, v1

    move-wide/from16 v23, v2

    move-wide/from16 v25, v4

    move/from16 p1, v16

    .line 1020
    new-instance v1, Lcom/helpshift/conversation/activeconversation/message/UserResponseMessageForTextInputDM;

    .line 1022
    invoke-direct {v0, v6}, Lcom/helpshift/common/conversation/ConversationDB;->parseInputKeyboardFromMeta(Lorg/json/JSONObject;)I

    move-result v15

    .line 1023
    invoke-direct {v0, v6}, Lcom/helpshift/common/conversation/ConversationDB;->parseBotInfoFromMeta(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v16

    .line 1024
    invoke-direct {v0, v6}, Lcom/helpshift/common/conversation/ConversationDB;->parseIsResponseSkippedFromMeta(Lorg/json/JSONObject;)Z

    move-result v17

    .line 1025
    invoke-direct {v0, v6}, Lcom/helpshift/common/conversation/ConversationDB;->parseReferredMessageIdFromMeta(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v18

    .line 1026
    invoke-direct {v0, v6}, Lcom/helpshift/common/conversation/ConversationDB;->parseIsMessageEmptyFromMeta(Lorg/json/JSONObject;)Z

    move-result v19

    move-object v9, v1

    invoke-direct/range {v9 .. v19}, Lcom/helpshift/conversation/activeconversation/message/UserResponseMessageForTextInputDM;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;ILjava/lang/String;ZLjava/lang/String;Z)V

    .line 1028
    iput-object v8, v1, Lcom/helpshift/conversation/activeconversation/message/UserResponseMessageForTextInputDM;->serverId:Ljava/lang/String;

    .line 1029
    invoke-direct {v0, v6}, Lcom/helpshift/common/conversation/ConversationDB;->parseDateTimeFromMeta(Lorg/json/JSONObject;)J

    move-result-wide v2

    iput-wide v2, v1, Lcom/helpshift/conversation/activeconversation/message/UserResponseMessageForTextInputDM;->dateInMillis:J

    .line 1030
    invoke-direct {v0, v6}, Lcom/helpshift/common/conversation/ConversationDB;->parseTimeZoneIdFromMeta(Lorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/helpshift/conversation/activeconversation/message/UserResponseMessageForTextInputDM;->timeZoneId:Ljava/lang/String;

    goto :goto_2

    :pswitch_14
    move/from16 v22, v1

    move-wide/from16 v23, v2

    move-wide/from16 v25, v4

    move/from16 p1, v16

    .line 1016
    new-instance v1, Lcom/helpshift/conversation/activeconversation/message/UserMessageDM;

    move-object v9, v1

    invoke-direct/range {v9 .. v14}, Lcom/helpshift/conversation/activeconversation/message/UserMessageDM;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V

    .line 1017
    iput-object v8, v1, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->serverId:Ljava/lang/String;

    .line 1233
    :goto_2
    invoke-static/range {v25 .. v26}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->conversationLocalId:Ljava/lang/Long;

    .line 1234
    invoke-static/range {v23 .. v24}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->localId:Ljava/lang/Long;

    move/from16 v2, p1

    .line 1235
    iput v2, v1, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->deliveryState:I

    move/from16 v2, v22

    .line 1236
    iput-boolean v2, v1, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->isRedacted:Z

    .line 1237
    invoke-direct {v0, v1, v6}, Lcom/helpshift/common/conversation/ConversationDB;->parseAndSetMessageSeenData(Lcom/helpshift/conversation/activeconversation/message/MessageDM;Lorg/json/JSONObject;)V

    return-object v1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private cursorToReadableConversation(Landroid/database/Cursor;)Lcom/helpshift/conversation/activeconversation/ConversationDM;
    .locals 26

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    .line 889
    iget-object v2, v0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v2, "_id"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 890
    iget-object v3, v0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v3, "user_local_id"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    .line 891
    iget-object v5, v0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v5, "server_id"

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 892
    iget-object v6, v0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v6, "publish_id"

    invoke-interface {v1, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 893
    iget-object v6, v0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v6, "uuid"

    invoke-interface {v1, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 894
    iget-object v7, v0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v7, "title"

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 895
    iget-object v7, v0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v7, "show_agent_name"

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    const/4 v9, 0x1

    const/4 v15, 0x0

    if-ne v7, v9, :cond_0

    const/16 v16, 0x1

    goto :goto_0

    :cond_0
    const/16 v16, 0x0

    .line 897
    :goto_0
    iget-object v7, v0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v7, "message_cursor"

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 898
    iget-object v7, v0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v7, "start_new_conversation_action"

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    if-ne v7, v9, :cond_1

    const/4 v13, 0x1

    goto :goto_1

    :cond_1
    const/4 v13, 0x0

    .line 900
    :goto_1
    iget-object v7, v0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v7, "meta"

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 901
    iget-object v7, v0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v7, "created_at"

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 902
    iget-object v7, v0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v7, "epoch_time_created_at"

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v18

    .line 903
    iget-object v7, v0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v7, "updated_at"

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    .line 904
    iget-object v7, v0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v7, "pre_conv_server_id"

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 905
    iget-object v7, v0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v7, "last_user_activity_time"

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    move-wide/from16 v21, v3

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    .line 906
    iget-object v7, v0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v7, "issue_type"

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 907
    iget-object v7, v0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v7, "full_privacy_enabled"

    invoke-static {v1, v7, v15}, Lcom/helpshift/util/DatabaseUtils;->parseBooleanColumnSafe(Landroid/database/Cursor;Ljava/lang/String;Z)Z

    move-result v7

    .line 908
    iget-object v15, v0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v15}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v15, "state"

    invoke-interface {v1, v15}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v15

    invoke-interface {v1, v15}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    .line 909
    invoke-static {v15}, Lcom/helpshift/conversation/dto/IssueState;->fromInt(I)Lcom/helpshift/conversation/dto/IssueState;

    move-result-object v15

    move/from16 v23, v7

    .line 910
    iget-object v7, v0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v7, "is_redacted"

    const/4 v0, 0x0

    invoke-static {v1, v7, v0}, Lcom/helpshift/util/DatabaseUtils;->parseBooleanColumnSafe(Landroid/database/Cursor;Ljava/lang/String;Z)Z

    move-result v1

    .line 912
    new-instance v7, Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-object/from16 p1, v7

    move/from16 v0, v23

    move/from16 v23, v1

    move-object v1, v9

    move-object v9, v15

    move/from16 v25, v0

    move-object/from16 v24, v11

    move-object v0, v12

    move-wide/from16 v11, v18

    move-wide/from16 v18, v3

    move v3, v13

    move-object/from16 v13, v20

    move/from16 v20, v3

    move-object v4, v15

    const/4 v3, 0x0

    move-object/from16 v15, v17

    invoke-direct/range {v7 .. v16}, Lcom/helpshift/conversation/activeconversation/ConversationDM;-><init>(Ljava/lang/String;Lcom/helpshift/conversation/dto/IssueState;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 920
    iput-object v5, v7, Lcom/helpshift/conversation/activeconversation/ConversationDM;->serverId:Ljava/lang/String;

    .line 921
    iput-object v0, v7, Lcom/helpshift/conversation/activeconversation/ConversationDM;->preConversationServerId:Ljava/lang/String;

    .line 922
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->setLocalId(J)V

    .line 923
    iput-object v6, v7, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localUUID:Ljava/lang/String;

    .line 924
    iput-object v1, v7, Lcom/helpshift/conversation/activeconversation/ConversationDM;->issueType:Ljava/lang/String;

    .line 925
    iput-object v4, v7, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    move-wide/from16 v0, v21

    .line 926
    iput-wide v0, v7, Lcom/helpshift/conversation/activeconversation/ConversationDM;->userLocalId:J

    move/from16 v15, v20

    .line 927
    invoke-virtual {v7, v15, v3}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->setStartNewConversationButtonClicked(ZZ)V

    move-wide/from16 v0, v18

    .line 928
    iput-wide v0, v7, Lcom/helpshift/conversation/activeconversation/ConversationDM;->lastUserActivityTime:J

    move/from16 v0, v25

    .line 929
    iput-boolean v0, v7, Lcom/helpshift/conversation/activeconversation/ConversationDM;->wasFullPrivacyEnabledAtCreation:Z

    move/from16 v0, v23

    .line 930
    iput-boolean v0, v7, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isRedacted:Z

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    .line 931
    invoke-direct {v0, v7, v1}, Lcom/helpshift/common/conversation/ConversationDB;->parseAndSetMetaData(Lcom/helpshift/conversation/activeconversation/ConversationDM;Ljava/lang/String;)V

    return-object v7
.end method

.method private exists(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Z
    .locals 2

    .line 769
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SELECT COUNT(*) FROM "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, " WHERE "

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, " LIMIT 1"

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2, p4}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide p1

    const-wide/16 p3, 0x0

    cmp-long v0, p1, p3

    if-lez v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private static faqToContentValues(Lcom/helpshift/support/Faq;)Landroid/content/ContentValues;
    .locals 3

    .line 144
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 145
    invoke-virtual {p0}, Lcom/helpshift/support/Faq;->getId()Ljava/lang/String;

    move-result-object v1

    const-string v2, "question_id"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    iget-object v1, p0, Lcom/helpshift/support/Faq;->publish_id:Ljava/lang/String;

    const-string v2, "publish_id"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    iget-object v1, p0, Lcom/helpshift/support/Faq;->language:Ljava/lang/String;

    const-string v2, "language"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    iget-object v1, p0, Lcom/helpshift/support/Faq;->section_publish_id:Ljava/lang/String;

    const-string v2, "section_id"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    iget-object v1, p0, Lcom/helpshift/support/Faq;->title:Ljava/lang/String;

    const-string v2, "title"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    iget-object v1, p0, Lcom/helpshift/support/Faq;->body:Ljava/lang/String;

    const-string v2, "body"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    iget v1, p0, Lcom/helpshift/support/Faq;->is_helpful:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "helpful"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 152
    iget-object v1, p0, Lcom/helpshift/support/Faq;->is_rtl:Ljava/lang/Boolean;

    const-string v2, "rtl"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 153
    new-instance v1, Lorg/json/JSONArray;

    invoke-virtual {p0}, Lcom/helpshift/support/Faq;->getTags()Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/json/JSONArray;-><init>(Ljava/util/Collection;)V

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "tags"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    new-instance v1, Lorg/json/JSONArray;

    invoke-virtual {p0}, Lcom/helpshift/support/Faq;->getCategoryTags()Ljava/util/List;

    move-result-object p0

    invoke-direct {v1, p0}, Lorg/json/JSONArray;-><init>(Ljava/util/Collection;)V

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    const-string v1, "c_tags"

    invoke-virtual {v0, v1, p0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private getBooleanFromJson(Lorg/json/JSONObject;Ljava/lang/String;Z)Z
    .locals 0

    .line 1308
    invoke-virtual {p1, p2, p3}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result p1

    return p1
.end method

.method private getConversationMeta(Lcom/helpshift/conversation/activeconversation/ConversationDM;)Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 876
    iget-object v0, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->csatState:Lcom/helpshift/conversation/states/ConversationCSATState;

    .line 877
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 878
    iget-object v2, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->csatFeedback:Ljava/lang/String;

    .line 879
    iget v3, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->csatRating:I

    const-string v4, "csat_feedback"

    .line 880
    invoke-virtual {v1, v4, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v2, "csat_rating"

    .line 881
    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 882
    invoke-virtual {v0}, Lcom/helpshift/conversation/states/ConversationCSATState;->getValue()I

    move-result v0

    const-string v2, "csat_state"

    invoke-virtual {v1, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 883
    iget-boolean v0, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->shouldIncrementMessageCount:Z

    const-string v2, "increment_message_count"

    invoke-virtual {v1, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 884
    iget-boolean p1, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isConversationEndedDelegateSent:Z

    const-string v0, "ended_delegate_sent"

    invoke-virtual {v1, v0, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 885
    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private getImageAttachmentDraftMeta(Lcom/helpshift/conversation/dto/ImagePickerFile;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 867
    :cond_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 868
    iget-object v1, p1, Lcom/helpshift/conversation/dto/ImagePickerFile;->originalFileName:Ljava/lang/String;

    const-string v2, "image_draft_orig_name"

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 869
    iget-object v1, p1, Lcom/helpshift/conversation/dto/ImagePickerFile;->originalFileSize:Ljava/lang/Long;

    const-string v2, "image_draft_orig_size"

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 870
    iget-object v1, p1, Lcom/helpshift/conversation/dto/ImagePickerFile;->filePath:Ljava/lang/String;

    const-string v2, "image_draft_file_path"

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 871
    iget-boolean p1, p1, Lcom/helpshift/conversation/dto/ImagePickerFile;->isFileCompressionAndCopyingDone:Z

    const-string v1, "image_copy_done"

    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 872
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/helpshift/common/conversation/ConversationDB;
    .locals 2

    const-class v0, Lcom/helpshift/common/conversation/ConversationDB;

    monitor-enter v0

    .line 137
    :try_start_0
    sget-object v1, Lcom/helpshift/common/conversation/ConversationDB;->instance:Lcom/helpshift/common/conversation/ConversationDB;

    if-nez v1, :cond_0

    .line 138
    new-instance v1, Lcom/helpshift/common/conversation/ConversationDB;

    invoke-direct {v1, p0}, Lcom/helpshift/common/conversation/ConversationDB;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/helpshift/common/conversation/ConversationDB;->instance:Lcom/helpshift/common/conversation/ConversationDB;

    .line 140
    :cond_0
    sget-object p0, Lcom/helpshift/common/conversation/ConversationDB;->instance:Lcom/helpshift/common/conversation/ConversationDB;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object p0

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method private getIntFromJson(Lorg/json/JSONObject;Ljava/lang/String;I)I
    .locals 0

    .line 1300
    invoke-virtual {p1, p2, p3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result p1

    return p1
.end method

.method private getMessageMeta(Lcom/helpshift/conversation/activeconversation/message/MessageDM;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 1425
    iget-object v0, p1, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->messageType:Lcom/helpshift/conversation/activeconversation/message/MessageType;

    .line 1427
    sget-object v1, Lcom/helpshift/common/conversation/ConversationDB$1;->$SwitchMap$com$helpshift$conversation$activeconversation$message$MessageType:[I

    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/message/MessageType;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x0

    packed-switch v0, :pswitch_data_0

    move-object v0, v1

    goto/16 :goto_0

    .line 1534
    :pswitch_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 1535
    check-cast p1, Lcom/helpshift/conversation/activeconversation/message/UserBotControlMessageDM;

    .line 1536
    invoke-direct {p0, v0, p1}, Lcom/helpshift/common/conversation/ConversationDB;->buildMetaForUserBotControlMessage(Lorg/json/JSONObject;Lcom/helpshift/conversation/activeconversation/message/UserBotControlMessageDM;)V

    .line 1537
    invoke-direct {p0, v0, p1}, Lcom/helpshift/common/conversation/ConversationDB;->buildMetaForAutoRetriableMessage(Lorg/json/JSONObject;Lcom/helpshift/conversation/activeconversation/message/AutoRetriableMessageDM;)V

    goto/16 :goto_0

    .line 1530
    :pswitch_1
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 1531
    check-cast p1, Lcom/helpshift/conversation/activeconversation/message/AdminBotControlMessageDM;

    invoke-direct {p0, v0, p1}, Lcom/helpshift/common/conversation/ConversationDB;->buildMetaForAdminBotControlMessage(Lorg/json/JSONObject;Lcom/helpshift/conversation/activeconversation/message/AdminBotControlMessageDM;)V

    goto/16 :goto_0

    .line 1491
    :pswitch_2
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 1492
    move-object v2, p1

    check-cast v2, Lcom/helpshift/conversation/activeconversation/message/RequestForReopenMessageDM;

    .line 1493
    invoke-virtual {v2}, Lcom/helpshift/conversation/activeconversation/message/RequestForReopenMessageDM;->isAnswered()Z

    move-result v2

    invoke-direct {p0, v0, v2}, Lcom/helpshift/common/conversation/ConversationDB;->buildMetaForIsAnswered(Lorg/json/JSONObject;Z)V

    .line 1494
    invoke-direct {p0, v0, p1}, Lcom/helpshift/common/conversation/ConversationDB;->buildMetaForMessageSeenData(Lorg/json/JSONObject;Lcom/helpshift/conversation/activeconversation/message/MessageDM;)V

    goto/16 :goto_0

    .line 1521
    :pswitch_3
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 1522
    move-object v2, p1

    check-cast v2, Lcom/helpshift/conversation/activeconversation/message/ImageAttachmentMessageDM;

    invoke-direct {p0, v0, v2}, Lcom/helpshift/common/conversation/ConversationDB;->buildMetaForImageAttachmentMessage(Lorg/json/JSONObject;Lcom/helpshift/conversation/activeconversation/message/ImageAttachmentMessageDM;)V

    .line 1523
    invoke-direct {p0, v0, p1}, Lcom/helpshift/common/conversation/ConversationDB;->buildMetaForMessageSeenData(Lorg/json/JSONObject;Lcom/helpshift/conversation/activeconversation/message/MessageDM;)V

    goto/16 :goto_0

    .line 1515
    :pswitch_4
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 1516
    move-object v2, p1

    check-cast v2, Lcom/helpshift/conversation/activeconversation/message/AttachmentMessageDM;

    .line 1517
    invoke-direct {p0, v0, v2}, Lcom/helpshift/common/conversation/ConversationDB;->buildJsonObjectForAttachmentMessage(Lorg/json/JSONObject;Lcom/helpshift/conversation/activeconversation/message/AttachmentMessageDM;)V

    .line 1518
    invoke-direct {p0, v0, p1}, Lcom/helpshift/common/conversation/ConversationDB;->buildMetaForMessageSeenData(Lorg/json/JSONObject;Lcom/helpshift/conversation/activeconversation/message/MessageDM;)V

    goto/16 :goto_0

    .line 1485
    :pswitch_5
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 1486
    move-object v2, p1

    check-cast v2, Lcom/helpshift/conversation/activeconversation/message/RequestScreenshotMessageDM;

    .line 1487
    iget-boolean v2, v2, Lcom/helpshift/conversation/activeconversation/message/RequestScreenshotMessageDM;->isAnswered:Z

    invoke-direct {p0, v0, v2}, Lcom/helpshift/common/conversation/ConversationDB;->buildMetaForIsAnswered(Lorg/json/JSONObject;Z)V

    .line 1488
    invoke-direct {p0, v0, p1}, Lcom/helpshift/common/conversation/ConversationDB;->buildMetaForMessageSeenData(Lorg/json/JSONObject;Lcom/helpshift/conversation/activeconversation/message/MessageDM;)V

    goto/16 :goto_0

    .line 1526
    :pswitch_6
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 1527
    check-cast p1, Lcom/helpshift/conversation/activeconversation/message/ScreenshotMessageDM;

    invoke-direct {p0, v0, p1}, Lcom/helpshift/common/conversation/ConversationDB;->buildMetaForScreenshotAttachmentMessage(Lorg/json/JSONObject;Lcom/helpshift/conversation/activeconversation/message/ScreenshotMessageDM;)V

    goto/16 :goto_0

    .line 1545
    :pswitch_7
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 1546
    check-cast p1, Lcom/helpshift/conversation/activeconversation/message/ConfirmationRejectedMessageDM;

    .line 1547
    invoke-direct {p0, v0, p1}, Lcom/helpshift/common/conversation/ConversationDB;->buildMetaForAutoRetriableMessage(Lorg/json/JSONObject;Lcom/helpshift/conversation/activeconversation/message/AutoRetriableMessageDM;)V

    goto/16 :goto_0

    .line 1540
    :pswitch_8
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 1541
    check-cast p1, Lcom/helpshift/conversation/activeconversation/message/ConfirmationAcceptedMessageDM;

    .line 1542
    invoke-direct {p0, v0, p1}, Lcom/helpshift/common/conversation/ConversationDB;->buildMetaForAutoRetriableMessage(Lorg/json/JSONObject;Lcom/helpshift/conversation/activeconversation/message/AutoRetriableMessageDM;)V

    goto/16 :goto_0

    .line 1503
    :pswitch_9
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 1504
    check-cast p1, Lcom/helpshift/conversation/activeconversation/message/FollowupRejectedMessageDM;

    .line 1505
    invoke-direct {p0, v0, p1}, Lcom/helpshift/common/conversation/ConversationDB;->buildMetaForFollowUpRejected(Lorg/json/JSONObject;Lcom/helpshift/conversation/activeconversation/message/FollowupRejectedMessageDM;)V

    .line 1506
    invoke-direct {p0, v0, p1}, Lcom/helpshift/common/conversation/ConversationDB;->buildMetaForAutoRetriableMessage(Lorg/json/JSONObject;Lcom/helpshift/conversation/activeconversation/message/AutoRetriableMessageDM;)V

    goto/16 :goto_0

    .line 1497
    :pswitch_a
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 1498
    check-cast p1, Lcom/helpshift/conversation/activeconversation/message/FollowupAcceptedMessageDM;

    .line 1499
    iget-object v2, p1, Lcom/helpshift/conversation/activeconversation/message/FollowupAcceptedMessageDM;->referredMessageId:Ljava/lang/String;

    invoke-direct {p0, v0, v2}, Lcom/helpshift/common/conversation/ConversationDB;->buildMetaForReferredMessageId(Lorg/json/JSONObject;Ljava/lang/String;)V

    .line 1500
    invoke-direct {p0, v0, p1}, Lcom/helpshift/common/conversation/ConversationDB;->buildMetaForAutoRetriableMessage(Lorg/json/JSONObject;Lcom/helpshift/conversation/activeconversation/message/AutoRetriableMessageDM;)V

    goto/16 :goto_0

    .line 1509
    :pswitch_b
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 1510
    move-object v2, p1

    check-cast v2, Lcom/helpshift/conversation/activeconversation/message/RequestAppReviewMessageDM;

    .line 1511
    iget-boolean v2, v2, Lcom/helpshift/conversation/activeconversation/message/RequestAppReviewMessageDM;->isAnswered:Z

    invoke-direct {p0, v0, v2}, Lcom/helpshift/common/conversation/ConversationDB;->buildMetaForIsAnswered(Lorg/json/JSONObject;Z)V

    .line 1512
    invoke-direct {p0, v0, p1}, Lcom/helpshift/common/conversation/ConversationDB;->buildMetaForMessageSeenData(Lorg/json/JSONObject;Lcom/helpshift/conversation/activeconversation/message/MessageDM;)V

    goto/16 :goto_0

    .line 1479
    :pswitch_c
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 1480
    check-cast p1, Lcom/helpshift/conversation/activeconversation/message/AcceptedAppReviewMessageDM;

    .line 1481
    iget-object v2, p1, Lcom/helpshift/conversation/activeconversation/message/AcceptedAppReviewMessageDM;->referredMessageId:Ljava/lang/String;

    invoke-direct {p0, v0, v2}, Lcom/helpshift/common/conversation/ConversationDB;->buildMetaForReferredMessageId(Lorg/json/JSONObject;Ljava/lang/String;)V

    .line 1482
    invoke-direct {p0, v0, p1}, Lcom/helpshift/common/conversation/ConversationDB;->buildMetaForAutoRetriableMessage(Lorg/json/JSONObject;Lcom/helpshift/conversation/activeconversation/message/AutoRetriableMessageDM;)V

    goto/16 :goto_0

    .line 1472
    :pswitch_d
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 1473
    invoke-direct {p0, v0, p1}, Lcom/helpshift/common/conversation/ConversationDB;->buildMetaForMessageSeenData(Lorg/json/JSONObject;Lcom/helpshift/conversation/activeconversation/message/MessageDM;)V

    .line 1474
    move-object v2, p1

    check-cast v2, Lcom/helpshift/conversation/activeconversation/message/FAQListMessageDM;

    invoke-direct {p0, v0, v2}, Lcom/helpshift/common/conversation/ConversationDB;->buildMetaForFAQList(Lorg/json/JSONObject;Lcom/helpshift/conversation/activeconversation/message/FAQListMessageDM;)V

    .line 1475
    check-cast p1, Lcom/helpshift/conversation/activeconversation/message/FAQListMessageWithOptionInputDM;

    iget-object v2, p1, Lcom/helpshift/conversation/activeconversation/message/FAQListMessageWithOptionInputDM;->input:Lcom/helpshift/conversation/activeconversation/message/input/OptionInput;

    invoke-direct {p0, v0, v2}, Lcom/helpshift/common/conversation/ConversationDB;->buildMetaForInput(Lorg/json/JSONObject;Lcom/helpshift/conversation/activeconversation/message/input/OptionInput;)V

    .line 1476
    invoke-direct {p0, v0, p1}, Lcom/helpshift/common/conversation/ConversationDB;->buildMetaForIsSuggestionsReadEvent(Lorg/json/JSONObject;Lcom/helpshift/conversation/activeconversation/message/FAQListMessageDM;)V

    goto/16 :goto_0

    .line 1466
    :pswitch_e
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 1467
    invoke-direct {p0, v0, p1}, Lcom/helpshift/common/conversation/ConversationDB;->buildMetaForMessageSeenData(Lorg/json/JSONObject;Lcom/helpshift/conversation/activeconversation/message/MessageDM;)V

    .line 1468
    check-cast p1, Lcom/helpshift/conversation/activeconversation/message/FAQListMessageDM;

    invoke-direct {p0, v0, p1}, Lcom/helpshift/common/conversation/ConversationDB;->buildMetaForFAQList(Lorg/json/JSONObject;Lcom/helpshift/conversation/activeconversation/message/FAQListMessageDM;)V

    .line 1469
    invoke-direct {p0, v0, p1}, Lcom/helpshift/common/conversation/ConversationDB;->buildMetaForIsSuggestionsReadEvent(Lorg/json/JSONObject;Lcom/helpshift/conversation/activeconversation/message/FAQListMessageDM;)V

    goto/16 :goto_0

    .line 1461
    :pswitch_f
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 1462
    invoke-direct {p0, v0, p1}, Lcom/helpshift/common/conversation/ConversationDB;->buildMetaForMessageSeenData(Lorg/json/JSONObject;Lcom/helpshift/conversation/activeconversation/message/MessageDM;)V

    .line 1463
    check-cast p1, Lcom/helpshift/conversation/activeconversation/message/AdminMessageWithOptionInputDM;

    iget-object p1, p1, Lcom/helpshift/conversation/activeconversation/message/AdminMessageWithOptionInputDM;->input:Lcom/helpshift/conversation/activeconversation/message/input/OptionInput;

    invoke-direct {p0, v0, p1}, Lcom/helpshift/common/conversation/ConversationDB;->buildMetaForInput(Lorg/json/JSONObject;Lcom/helpshift/conversation/activeconversation/message/input/OptionInput;)V

    goto :goto_0

    .line 1454
    :pswitch_10
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 1455
    move-object v2, p1

    check-cast v2, Lcom/helpshift/conversation/activeconversation/message/AdminMessageWithTextInputDM;

    .line 1456
    invoke-direct {p0, v0, p1}, Lcom/helpshift/common/conversation/ConversationDB;->buildMetaForMessageSeenData(Lorg/json/JSONObject;Lcom/helpshift/conversation/activeconversation/message/MessageDM;)V

    .line 1457
    iget-object p1, v2, Lcom/helpshift/conversation/activeconversation/message/AdminMessageWithTextInputDM;->input:Lcom/helpshift/conversation/activeconversation/message/input/TextInput;

    invoke-direct {p0, v0, p1}, Lcom/helpshift/common/conversation/ConversationDB;->buildMetaForInput(Lorg/json/JSONObject;Lcom/helpshift/conversation/activeconversation/message/input/TextInput;)V

    .line 1458
    iget-boolean p1, v2, Lcom/helpshift/conversation/activeconversation/message/AdminMessageWithTextInputDM;->isMessageEmpty:Z

    invoke-direct {p0, v0, p1}, Lcom/helpshift/common/conversation/ConversationDB;->buildMetaForIsMessageEmpty(Lorg/json/JSONObject;Z)V

    goto :goto_0

    .line 1450
    :pswitch_11
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 1451
    invoke-direct {p0, v0, p1}, Lcom/helpshift/common/conversation/ConversationDB;->buildMetaForMessageSeenData(Lorg/json/JSONObject;Lcom/helpshift/conversation/activeconversation/message/MessageDM;)V

    goto :goto_0

    .line 1440
    :pswitch_12
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 1441
    check-cast p1, Lcom/helpshift/conversation/activeconversation/message/UserResponseMessageForOptionInput;

    .line 1443
    iget-object v2, p1, Lcom/helpshift/conversation/activeconversation/message/UserResponseMessageForOptionInput;->botInfo:Ljava/lang/String;

    invoke-direct {p0, v0, v2}, Lcom/helpshift/common/conversation/ConversationDB;->buildMetaForBotInfo(Lorg/json/JSONObject;Ljava/lang/String;)V

    .line 1444
    iget-boolean v2, p1, Lcom/helpshift/conversation/activeconversation/message/UserResponseMessageForOptionInput;->skipped:Z

    invoke-direct {p0, v0, v2}, Lcom/helpshift/common/conversation/ConversationDB;->buildMetaForIsResponseSkipped(Lorg/json/JSONObject;Z)V

    .line 1445
    invoke-virtual {p1}, Lcom/helpshift/conversation/activeconversation/message/UserResponseMessageForOptionInput;->getReferredMessageId()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lcom/helpshift/common/conversation/ConversationDB;->buildMetaForReferredMessageId(Lorg/json/JSONObject;Ljava/lang/String;)V

    .line 1446
    iget-object v2, p1, Lcom/helpshift/conversation/activeconversation/message/UserResponseMessageForOptionInput;->referredMessageType:Lcom/helpshift/conversation/activeconversation/message/MessageType;

    invoke-direct {p0, v0, v2}, Lcom/helpshift/common/conversation/ConversationDB;->buildMetaForReferredMessageType(Lorg/json/JSONObject;Lcom/helpshift/conversation/activeconversation/message/MessageType;)V

    .line 1447
    iget-object p1, p1, Lcom/helpshift/conversation/activeconversation/message/UserResponseMessageForOptionInput;->optionData:Ljava/lang/String;

    invoke-direct {p0, v0, p1}, Lcom/helpshift/common/conversation/ConversationDB;->buildMetaForSelectedOptionData(Lorg/json/JSONObject;Ljava/lang/String;)V

    goto :goto_0

    .line 1429
    :pswitch_13
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 1430
    check-cast p1, Lcom/helpshift/conversation/activeconversation/message/UserResponseMessageForTextInputDM;

    .line 1432
    iget-object v2, p1, Lcom/helpshift/conversation/activeconversation/message/UserResponseMessageForTextInputDM;->botInfo:Ljava/lang/String;

    invoke-direct {p0, v0, v2}, Lcom/helpshift/common/conversation/ConversationDB;->buildMetaForBotInfo(Lorg/json/JSONObject;Ljava/lang/String;)V

    .line 1433
    iget v2, p1, Lcom/helpshift/conversation/activeconversation/message/UserResponseMessageForTextInputDM;->keyboard:I

    invoke-direct {p0, v0, v2}, Lcom/helpshift/common/conversation/ConversationDB;->buildMetaForInputKeyboard(Lorg/json/JSONObject;I)V

    .line 1434
    iget-boolean v2, p1, Lcom/helpshift/conversation/activeconversation/message/UserResponseMessageForTextInputDM;->skipped:Z

    invoke-direct {p0, v0, v2}, Lcom/helpshift/common/conversation/ConversationDB;->buildMetaForIsResponseSkipped(Lorg/json/JSONObject;Z)V

    .line 1435
    invoke-virtual {p1}, Lcom/helpshift/conversation/activeconversation/message/UserResponseMessageForTextInputDM;->getReferredMessageId()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lcom/helpshift/common/conversation/ConversationDB;->buildMetaForReferredMessageId(Lorg/json/JSONObject;Ljava/lang/String;)V

    .line 1436
    iget-boolean v2, p1, Lcom/helpshift/conversation/activeconversation/message/UserResponseMessageForTextInputDM;->isMessageEmpty:Z

    invoke-direct {p0, v0, v2}, Lcom/helpshift/common/conversation/ConversationDB;->buildMetaForIsMessageEmpty(Lorg/json/JSONObject;Z)V

    .line 1437
    invoke-direct {p0, v0, p1}, Lcom/helpshift/common/conversation/ConversationDB;->buildMetaForDateTime(Lorg/json/JSONObject;Lcom/helpshift/conversation/activeconversation/message/UserResponseMessageForTextInputDM;)V

    :goto_0
    if-nez v0, :cond_0

    return-object v1

    .line 1553
    :cond_0
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private getStringFromJson(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 1304
    invoke-virtual {p1, p2, p3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private jsonify(Ljava/lang/String;)Lorg/json/JSONObject;
    .locals 3

    .line 1382
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 1384
    invoke-static {p1}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    return-object v0

    .line 1389
    :cond_0
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    goto :goto_0

    :catch_0
    move-exception p1

    const-string v1, "Helpshift_ConverDB"

    const-string v2, "Exception in jsonify"

    .line 1392
    invoke-static {v1, v2, p1}, Lcom/helpshift/util/HSLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_0
    return-object v0
.end method

.method private parseAndGetImageAttachmentDraft(Ljava/lang/String;)Lcom/helpshift/conversation/dto/ImagePickerFile;
    .locals 10

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return-object v0

    .line 984
    :cond_0
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string p1, "image_draft_orig_name"

    .line 985
    invoke-virtual {v1, p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-string v2, "image_draft_orig_size"

    const-wide/16 v3, -0x1

    .line 986
    invoke-virtual {v1, v2, v3, v4}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const-string v5, "image_draft_file_path"

    .line 987
    invoke-virtual {v1, v5, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "image_copy_done"

    const/4 v7, 0x0

    .line 988
    invoke-virtual {v1, v6, v7}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 989
    new-instance v6, Lcom/helpshift/conversation/dto/ImagePickerFile;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    cmp-long v9, v7, v3

    if-nez v9, :cond_1

    move-object v2, v0

    :cond_1
    invoke-direct {v6, v5, p1, v2}, Lcom/helpshift/conversation/dto/ImagePickerFile;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    .line 990
    :try_start_1
    iput-boolean v1, v6, Lcom/helpshift/conversation/dto/ImagePickerFile;->isFileCompressionAndCopyingDone:Z
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    goto :goto_0

    :catch_1
    move-exception p1

    move-object v6, v0

    :goto_0
    const-string v0, "Helpshift_ConverDB"

    const-string v1, "Error in parseAndGetImageAttachmentDraft"

    .line 993
    invoke-static {v0, v1, p1}, Lcom/helpshift/util/HSLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_1
    return-object v6
.end method

.method private parseAndGetMessageSyncState(Ljava/lang/String;Lorg/json/JSONObject;)I
    .locals 1

    .line 1363
    invoke-static {p1}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x2

    return p1

    :cond_0
    const/4 p1, 0x1

    const-string v0, "message_sync_status"

    .line 1367
    invoke-virtual {p2, v0, p1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result p1

    return p1
.end method

.method private parseAndSetFollowUpRejectedDataFromMeta(Lcom/helpshift/conversation/activeconversation/message/FollowupRejectedMessageDM;Lorg/json/JSONObject;)V
    .locals 3

    const-string v0, "rejected_reason"

    .line 1416
    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v0

    const-string v1, "rejected_conv_id"

    const/4 v2, 0x0

    .line 1417
    invoke-virtual {p2, v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 1419
    iput v0, p1, Lcom/helpshift/conversation/activeconversation/message/FollowupRejectedMessageDM;->reason:I

    .line 1420
    iput-object p2, p1, Lcom/helpshift/conversation/activeconversation/message/FollowupRejectedMessageDM;->openConversationId:Ljava/lang/String;

    return-void
.end method

.method private parseAndSetMessageSeenData(Lcom/helpshift/conversation/activeconversation/message/MessageDM;Lorg/json/JSONObject;)V
    .locals 4

    const-string v0, "read_at"

    const-string v1, ""

    .line 1372
    invoke-virtual {p2, v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "seen_cursor"

    const/4 v2, 0x0

    .line 1373
    invoke-virtual {p2, v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "seen_sync_status"

    const/4 v3, 0x0

    .line 1374
    invoke-virtual {p2, v2, v3}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result p2

    .line 1375
    iput-object v1, p1, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->seenAtMessageCursor:Ljava/lang/String;

    .line 1376
    iput-boolean p2, p1, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->isMessageSeenSynced:Z

    .line 1377
    iput-object v0, p1, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->readAt:Ljava/lang/String;

    return-void
.end method

.method private parseAndSetMetaData(Lcom/helpshift/conversation/activeconversation/ConversationDM;Ljava/lang/String;)V
    .locals 5

    if-nez p2, :cond_0

    return-void

    .line 960
    :cond_0
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string p2, "csat_rating"

    const/4 v1, 0x0

    .line 961
    invoke-virtual {v0, p2, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result p2

    const-string v2, "csat_state"

    .line 962
    sget-object v3, Lcom/helpshift/conversation/states/ConversationCSATState;->NONE:Lcom/helpshift/conversation/states/ConversationCSATState;

    invoke-virtual {v3}, Lcom/helpshift/conversation/states/ConversationCSATState;->getValue()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v2

    const-string v3, "csat_feedback"

    const/4 v4, 0x0

    .line 963
    invoke-virtual {v0, v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 964
    invoke-static {v2}, Lcom/helpshift/conversation/states/ConversationCSATState;->fromInt(I)Lcom/helpshift/conversation/states/ConversationCSATState;

    move-result-object v2

    invoke-virtual {p1, p2, v2, v3}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->setCSATData(ILcom/helpshift/conversation/states/ConversationCSATState;Ljava/lang/String;)V

    const-string p2, "increment_message_count"

    .line 967
    invoke-virtual {v0, p2, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result p2

    .line 968
    invoke-virtual {p1, p2, v1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->setShouldIncrementMessageCount(ZZ)V

    const-string p2, "ended_delegate_sent"

    .line 970
    invoke-virtual {v0, p2, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result p2

    iput-boolean p2, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isConversationEndedDelegateSent:Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    const-string p2, "Helpshift_ConverDB"

    const-string v0, "Error in parseAndSetMetaData"

    .line 974
    invoke-static {p2, v0, p1}, Lcom/helpshift/util/HSLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method private parseAttachmentInfoFromMeta(Lorg/json/JSONObject;)Lcom/helpshift/common/conversation/ConversationDB$AttachmentInfo;
    .locals 1

    .line 1406
    new-instance v0, Lcom/helpshift/common/conversation/ConversationDB$AttachmentInfo;

    invoke-direct {v0, p0, p1}, Lcom/helpshift/common/conversation/ConversationDB$AttachmentInfo;-><init>(Lcom/helpshift/common/conversation/ConversationDB;Lorg/json/JSONObject;)V

    return-object v0
.end method

.method private parseBotActionTypeFromMeta(Lorg/json/JSONObject;)Ljava/lang/String;
    .locals 2

    const-string v0, "bot_action_type"

    const-string v1, ""

    .line 1250
    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private parseBotEndedReasonFromMeta(Lorg/json/JSONObject;)Ljava/lang/String;
    .locals 2

    const-string v0, "bot_ended_reason"

    const-string v1, ""

    .line 1255
    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private parseBotInfoFromMeta(Lorg/json/JSONObject;)Ljava/lang/String;
    .locals 2

    const-string/jumbo v0, "{}"

    const-string v1, "chatbot_info"

    .line 1346
    invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private parseDateTimeFromMeta(Lorg/json/JSONObject;)J
    .locals 3

    const-string v0, "dt"

    const-wide/16 v1, 0x0

    .line 1351
    invoke-virtual {p1, v0, v1, v2}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method private parseFAQListFromMeta(Lorg/json/JSONObject;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            ")",
            "Ljava/util/List<",
            "Lcom/helpshift/conversation/activeconversation/message/FAQListMessageDM$FAQ;",
            ">;"
        }
    .end annotation

    .line 1267
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :try_start_0
    const-string v1, "faqs"

    .line 1269
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object p1

    const/4 v1, 0x0

    .line 1270
    :goto_0
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 1271
    invoke-virtual {p1, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    .line 1272
    new-instance v3, Lcom/helpshift/conversation/activeconversation/message/FAQListMessageDM$FAQ;

    const-string v4, "faq_title"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "faq_publish_id"

    .line 1273
    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "faq_language"

    .line 1274
    invoke-virtual {v2, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v4, v5, v2}, Lcom/helpshift/conversation/activeconversation/message/FAQListMessageDM$FAQ;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1272
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :catch_0
    :cond_0
    return-object v0
.end method

.method private parseHasNextBotFromMeta(Lorg/json/JSONObject;)Ljava/lang/Boolean;
    .locals 2

    const-string v0, "has_next_bot"

    const/4 v1, 0x0

    .line 1359
    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method private parseImageAttachmentInfoFromMeta(Lorg/json/JSONObject;)Lcom/helpshift/common/conversation/ConversationDB$ImageAttachmentInfo;
    .locals 1

    .line 1410
    new-instance v0, Lcom/helpshift/common/conversation/ConversationDB$ImageAttachmentInfo;

    invoke-direct {v0, p0, p1}, Lcom/helpshift/common/conversation/ConversationDB$ImageAttachmentInfo;-><init>(Lcom/helpshift/common/conversation/ConversationDB;Lorg/json/JSONObject;)V

    return-object v0
.end method

.method private parseInputKeyboardFromMeta(Lorg/json/JSONObject;)I
    .locals 2

    const-string v0, "input_keyboard"

    const/4 v1, 0x1

    .line 1324
    invoke-direct {p0, p1, v0, v1}, Lcom/helpshift/common/conversation/ConversationDB;->getIntFromJson(Lorg/json/JSONObject;Ljava/lang/String;I)I

    move-result p1

    return p1
.end method

.method private parseInputLabelFromMeta(Lorg/json/JSONObject;)Ljava/lang/String;
    .locals 2

    const-string v0, "input_label"

    const-string v1, ""

    .line 1332
    invoke-direct {p0, p1, v0, v1}, Lcom/helpshift/common/conversation/ConversationDB;->getStringFromJson(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private parseInputOptionTypeFromMeta(Lorg/json/JSONObject;I)Lcom/helpshift/conversation/activeconversation/message/input/OptionInput$Type;
    .locals 2

    const-string v0, "option_type"

    const-string v1, ""

    .line 1242
    invoke-direct {p0, p1, v0, v1}, Lcom/helpshift/common/conversation/ConversationDB;->getStringFromJson(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1, p2}, Lcom/helpshift/conversation/activeconversation/message/input/OptionInput$Type;->getType(Ljava/lang/String;I)Lcom/helpshift/conversation/activeconversation/message/input/OptionInput$Type;

    move-result-object p1

    return-object p1
.end method

.method private parseInputOptionsFromMeta(Lorg/json/JSONObject;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            ")",
            "Ljava/util/List<",
            "Lcom/helpshift/conversation/activeconversation/message/input/OptionInput$Option;",
            ">;"
        }
    .end annotation

    .line 1284
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :try_start_0
    const-string v1, "input_options"

    .line 1286
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object p1

    const/4 v1, 0x0

    .line 1287
    :goto_0
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 1288
    invoke-virtual {p1, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    .line 1289
    new-instance v3, Lcom/helpshift/conversation/activeconversation/message/input/OptionInput$Option;

    const-string v4, "option_title"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "option_data"

    .line 1290
    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v4, v2}, Lcom/helpshift/conversation/activeconversation/message/input/OptionInput$Option;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1289
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :catch_0
    :cond_0
    return-object v0
.end method

.method private parseInputPlaceholderFromMeta(Lorg/json/JSONObject;)Ljava/lang/String;
    .locals 2

    const-string v0, "input_placeholder"

    const-string v1, ""

    .line 1340
    invoke-direct {p0, p1, v0, v1}, Lcom/helpshift/common/conversation/ConversationDB;->getStringFromJson(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private parseInputRequiredFromMeta(Lorg/json/JSONObject;)Z
    .locals 2

    const-string v0, "input_required"

    const/4 v1, 0x0

    .line 1336
    invoke-direct {p0, p1, v0, v1}, Lcom/helpshift/common/conversation/ConversationDB;->getBooleanFromJson(Lorg/json/JSONObject;Ljava/lang/String;Z)Z

    move-result p1

    return p1
.end method

.method private parseInputSkipLabelFromMeta(Lorg/json/JSONObject;)Ljava/lang/String;
    .locals 2

    const-string v0, "input_skip_label"

    const-string v1, ""

    .line 1328
    invoke-direct {p0, p1, v0, v1}, Lcom/helpshift/common/conversation/ConversationDB;->getStringFromJson(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private parseIsAnsweredFromMeta(Lorg/json/JSONObject;)Z
    .locals 2

    const-string v0, "is_answered"

    const/4 v1, 0x0

    .line 1402
    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result p1

    return p1
.end method

.method private parseIsMessageEmptyFromMeta(Lorg/json/JSONObject;)Z
    .locals 2

    const-string v0, "is_message_empty"

    const/4 v1, 0x0

    .line 1246
    invoke-direct {p0, p1, v0, v1}, Lcom/helpshift/common/conversation/ConversationDB;->getBooleanFromJson(Lorg/json/JSONObject;Ljava/lang/String;Z)Z

    move-result p1

    return p1
.end method

.method private parseIsResponseSkippedFromMeta(Lorg/json/JSONObject;)Z
    .locals 2

    const-string v0, "is_response_skipped"

    const/4 v1, 0x0

    .line 1320
    invoke-direct {p0, p1, v0, v1}, Lcom/helpshift/common/conversation/ConversationDB;->getBooleanFromJson(Lorg/json/JSONObject;Ljava/lang/String;Z)Z

    move-result p1

    return p1
.end method

.method private parseIsSuggestionsReadEventSent(Lorg/json/JSONObject;)Z
    .locals 2

    const-string v0, "is_suggestion_read_event_sent"

    const/4 v1, 0x0

    .line 1263
    invoke-direct {p0, p1, v0, v1}, Lcom/helpshift/common/conversation/ConversationDB;->getBooleanFromJson(Lorg/json/JSONObject;Ljava/lang/String;Z)Z

    move-result p1

    return p1
.end method

.method private parseReferredMessageIdFromMeta(Lorg/json/JSONObject;)Ljava/lang/String;
    .locals 2

    const-string v0, "referredMessageId"

    const/4 v1, 0x0

    .line 1398
    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private parseReferredMessageTypeFromMeta(Lorg/json/JSONObject;)Lcom/helpshift/conversation/activeconversation/message/MessageType;
    .locals 2

    const-string v0, "referred_message_type"

    const-string v1, ""

    .line 1312
    invoke-direct {p0, p1, v0, v1}, Lcom/helpshift/common/conversation/ConversationDB;->getStringFromJson(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/helpshift/conversation/activeconversation/message/MessageType;->fromValue(Ljava/lang/String;)Lcom/helpshift/conversation/activeconversation/message/MessageType;

    move-result-object p1

    return-object p1
.end method

.method private parseSelectedOptionDataFromMeta(Lorg/json/JSONObject;)Ljava/lang/String;
    .locals 2

    const-string v0, "selected_option_data"

    const-string/jumbo v1, "{}"

    .line 1316
    invoke-direct {p0, p1, v0, v1}, Lcom/helpshift/common/conversation/ConversationDB;->getStringFromJson(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private parseSuggestionReadFAQPublishId(Lorg/json/JSONObject;)Ljava/lang/String;
    .locals 2

    const-string v0, "suggestion_read_faq_publish_id"

    const-string v1, ""

    .line 1259
    invoke-direct {p0, p1, v0, v1}, Lcom/helpshift/common/conversation/ConversationDB;->getStringFromJson(Lorg/json/JSONObject;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private parseTimeZoneIdFromMeta(Lorg/json/JSONObject;)Ljava/lang/String;
    .locals 1

    const-string v0, "timezone_id"

    .line 1355
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private readMessages(Ljava/lang/String;[Ljava/lang/String;)Ljava/util/List;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/helpshift/conversation/activeconversation/message/MessageDM;",
            ">;"
        }
    .end annotation

    .line 678
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    .line 681
    :try_start_0
    iget-object v2, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbHelper:Lcom/helpshift/platform/db/ConversationDBHelper;

    invoke-virtual {v2}, Lcom/helpshift/platform/db/ConversationDBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 682
    iget-object v2, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v4, "messages"

    const/4 v5, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v6, p1

    move-object v7, p2

    invoke-virtual/range {v3 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 690
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 692
    :cond_0
    invoke-direct {p0, v1}, Lcom/helpshift/common/conversation/ConversationDB;->cursorToMessageDM(Landroid/database/Cursor;)Lcom/helpshift/conversation/activeconversation/message/MessageDM;

    move-result-object p1

    if-eqz p1, :cond_1

    .line 696
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 698
    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez p1, :cond_0

    :cond_2
    if-eqz v1, :cond_3

    goto :goto_0

    :catchall_0
    move-exception p1

    goto :goto_1

    :catch_0
    move-exception p1

    :try_start_1
    const-string p2, "Helpshift_ConverDB"

    const-string v2, "Error in read messages"

    .line 702
    invoke-static {p2, v2, p1}, Lcom/helpshift/util/HSLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v1, :cond_3

    .line 706
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    return-object v0

    :goto_1
    if-eqz v1, :cond_4

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_4
    throw p1
.end method

.method private readableConversationToContentValues(Lcom/helpshift/conversation/activeconversation/ConversationDM;)Landroid/content/ContentValues;
    .locals 3

    .line 833
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 834
    iget-object v1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-wide v1, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->userLocalId:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const-string v2, "user_local_id"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 835
    iget-object v1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v1, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->serverId:Ljava/lang/String;

    const-string v2, "server_id"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 836
    iget-object v1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v1, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->preConversationServerId:Ljava/lang/String;

    const-string v2, "pre_conv_server_id"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 837
    iget-object v1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v1, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->publishId:Ljava/lang/String;

    const-string v2, "publish_id"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 838
    iget-object v1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v1, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localUUID:Ljava/lang/String;

    const-string v2, "uuid"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 839
    iget-object v1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v1, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->title:Ljava/lang/String;

    const-string v2, "title"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 840
    iget-object v1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v1, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->messageCursor:Ljava/lang/String;

    const-string v2, "message_cursor"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 841
    iget-object v1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-boolean v1, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->showAgentName:Z

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "show_agent_name"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 842
    iget-object v1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-boolean v1, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isStartNewConversationClicked:Z

    .line 843
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "start_new_conversation_action"

    .line 842
    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 844
    iget-object v1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-virtual {p1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->getCreatedAt()Ljava/lang/String;

    move-result-object v1

    const-string v2, "created_at"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 845
    iget-object v1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v1, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->updatedAt:Ljava/lang/String;

    const-string v2, "updated_at"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 846
    iget-object v1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-virtual {p1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->getEpochCreatedAtTime()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const-string v2, "epoch_time_created_at"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 847
    iget-object v1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-wide v1, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->lastUserActivityTime:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const-string v2, "last_user_activity_time"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 848
    iget-object v1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v1, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->issueType:Ljava/lang/String;

    const-string v2, "issue_type"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 849
    iget-object v1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-boolean v1, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->wasFullPrivacyEnabledAtCreation:Z

    .line 850
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "full_privacy_enabled"

    .line 849
    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 851
    iget-object v1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v1, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    if-nez v1, :cond_0

    const/4 v1, -0x1

    goto :goto_0

    :cond_0
    iget-object v1, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    invoke-virtual {v1}, Lcom/helpshift/conversation/dto/IssueState;->getValue()I

    move-result v1

    :goto_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "state"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 852
    iget-object v1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-boolean v1, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isRedacted:Z

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "is_redacted"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 854
    :try_start_0
    invoke-direct {p0, p1}, Lcom/helpshift/common/conversation/ConversationDB;->getConversationMeta(Lcom/helpshift/conversation/activeconversation/ConversationDM;)Ljava/lang/String;

    move-result-object p1

    .line 855
    iget-object v1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v1, "meta"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    const-string v1, "Helpshift_ConverDB"

    const-string v2, "Error in generating meta string for conversation"

    .line 858
    invoke-static {v1, v2, p1}, Lcom/helpshift/util/HSLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_1
    return-object v0
.end method

.method private readableMessageToContentValues(Lcom/helpshift/conversation/activeconversation/message/MessageDM;)Landroid/content/ContentValues;
    .locals 3

    .line 936
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 937
    iget-object v1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v1, p1, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->serverId:Ljava/lang/String;

    const-string v2, "server_id"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 938
    iget-object v1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v1, p1, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->conversationLocalId:Ljava/lang/Long;

    const-string v2, "conversation_id"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 939
    iget-object v1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v1, p1, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->body:Ljava/lang/String;

    const-string v2, "body"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 940
    iget-object v1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v1, p1, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->authorName:Ljava/lang/String;

    const-string v2, "author_name"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 941
    iget-object v1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-virtual {p1}, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->getCreatedAt()Ljava/lang/String;

    move-result-object v1

    const-string v2, "created_at"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 942
    iget-object v1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-virtual {p1}, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->getEpochCreatedAtTime()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const-string v2, "epoch_time_created_at"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 943
    iget-object v1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v1, p1, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->messageType:Lcom/helpshift/conversation/activeconversation/message/MessageType;

    invoke-virtual {v1}, Lcom/helpshift/conversation/activeconversation/message/MessageType;->getValue()Ljava/lang/String;

    move-result-object v1

    const-string v2, "type"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 944
    iget-object v1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget v1, p1, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->deliveryState:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "md_state"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 945
    iget-object v1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-boolean v1, p1, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->isRedacted:Z

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "is_redacted"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 947
    :try_start_0
    iget-object v1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v1, "meta"

    invoke-direct {p0, p1}, Lcom/helpshift/common/conversation/ConversationDB;->getMessageMeta(Lcom/helpshift/conversation/activeconversation/message/MessageDM;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    const-string v1, "Helpshift_ConverDB"

    const-string v2, "Error in generating meta string for message"

    .line 950
    invoke-static {v1, v2, p1}, Lcom/helpshift/util/HSLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_0
    return-object v0
.end method


# virtual methods
.method public declared-synchronized deleteConversationInboxData(J)V
    .locals 4

    monitor-enter p0

    .line 1855
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "delete from "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v1, "conversation_inbox"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " where "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v1, "user_local_id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " = ?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1858
    :try_start_1
    iget-object v1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbHelper:Lcom/helpshift/platform/db/ConversationDBHelper;

    invoke-virtual {v1}, Lcom/helpshift/platform/db/ConversationDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    .line 1859
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v2, v3

    invoke-virtual {v1, v0, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_2
    const-string p2, "Helpshift_ConverDB"

    const-string v0, "Error in delete conversationInboxData with UserLocalId"

    .line 1862
    invoke-static {p2, v0, p1}, Lcom/helpshift/util/HSLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1864
    :goto_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized deleteConversationWithLocalId(J)V
    .locals 3

    monitor-enter p0

    .line 221
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " = ?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    .line 222
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v1, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 224
    :try_start_1
    iget-object p1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbHelper:Lcom/helpshift/platform/db/ConversationDBHelper;

    invoke-virtual {p1}, Lcom/helpshift/platform/db/ConversationDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object p1

    .line 225
    iget-object p2, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string p2, "issues"

    invoke-virtual {p1, p2, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_2
    const-string p2, "Helpshift_ConverDB"

    const-string v0, "Error in delete conversation with localId"

    .line 228
    invoke-static {p2, v0, p1}, Lcom/helpshift/util/HSLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 230
    :goto_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized deleteConversations(J)V
    .locals 7

    monitor-enter p0

    .line 1867
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v0, "issues"

    .line 1868
    iget-object v1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v1, "messages"

    .line 1869
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v3, "_id"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1870
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "."

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v0, "user_local_id"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1871
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "."

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v1, "conversation_id"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1873
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "select "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " from  "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v2, "issues"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "  where "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " = ?"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1875
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "delete from "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v3, "messages"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " where "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " IN  ( "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " )"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1877
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "delete from "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v2, "issues"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " where "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v2, "user_local_id"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " = ?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const/4 v2, 0x0

    .line 1881
    :try_start_1
    iget-object v3, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbHelper:Lcom/helpshift/platform/db/ConversationDBHelper;

    invoke-virtual {v3}, Lcom/helpshift/platform/db/ConversationDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 1882
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    const/4 v3, 0x1

    new-array v4, v3, [Ljava/lang/String;

    .line 1883
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    aput-object v5, v4, v6

    invoke-virtual {v2, v0, v4}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    new-array v0, v3, [Ljava/lang/String;

    .line 1884
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v0, v6

    invoke-virtual {v2, v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1885
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v2, :cond_0

    .line 1892
    :goto_0
    :try_start_2
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_1

    :catchall_0
    move-exception p1

    goto :goto_2

    :catch_0
    move-exception p1

    :try_start_3
    const-string p2, "Helpshift_ConverDB"

    const-string v0, "Error in delete conversations with UserLocalId"

    .line 1888
    invoke-static {p2, v0, p1}, Lcom/helpshift/util/HSLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz v2, :cond_0

    goto :goto_0

    .line 1895
    :cond_0
    :goto_1
    monitor-exit p0

    return-void

    :goto_2
    if-eqz v2, :cond_1

    .line 1892
    :try_start_4
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    :cond_1
    throw p1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catchall_1
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized dropAndCreateDatabase()V
    .locals 2

    monitor-enter p0

    .line 1769
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbHelper:Lcom/helpshift/platform/db/ConversationDBHelper;

    iget-object v1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbHelper:Lcom/helpshift/platform/db/ConversationDBHelper;

    invoke-virtual {v1}, Lcom/helpshift/platform/db/ConversationDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/helpshift/platform/db/ConversationDBHelper;->dropAndCreateDatabase(Landroid/database/sqlite/SQLiteDatabase;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1770
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getAdminFAQSuggestion(Ljava/lang/String;Ljava/lang/String;)Lcom/helpshift/support/Faq;
    .locals 10

    monitor-enter p0

    .line 1773
    :try_start_0
    invoke-static {p1}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_4

    invoke-static {p2}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-eqz v0, :cond_0

    goto :goto_4

    .line 1780
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbHelper:Lcom/helpshift/platform/db/ConversationDBHelper;

    invoke-virtual {v0}, Lcom/helpshift/platform/db/ConversationDBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    const-string v3, "faq_suggestions"

    const/4 v4, 0x0

    const-string v5, "publish_id = ? AND language = ?"

    const/4 v0, 0x2

    new-array v6, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p1, v6, v0

    const/4 p1, 0x1

    aput-object p2, v6, p1

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    .line 1781
    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1787
    :try_start_2
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result p2

    if-eqz p2, :cond_1

    .line 1788
    invoke-direct {p0, p1}, Lcom/helpshift/common/conversation/ConversationDB;->cursorToFaq(Landroid/database/Cursor;)Lcom/helpshift/support/Faq;

    move-result-object v1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :cond_1
    if-eqz p1, :cond_2

    .line 1796
    :goto_0
    :try_start_3
    invoke-interface {p1}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    goto :goto_2

    :catch_0
    move-exception p2

    goto :goto_1

    :catchall_0
    move-exception p2

    goto :goto_3

    :catch_1
    move-exception p2

    move-object p1, v1

    :goto_1
    :try_start_4
    const-string v0, "Helpshift_ConverDB"

    const-string v2, "Error in getAdminFAQSuggestion"

    .line 1792
    invoke-static {v0, v2, p2}, Lcom/helpshift/util/HSLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    if-eqz p1, :cond_2

    goto :goto_0

    .line 1799
    :cond_2
    :goto_2
    monitor-exit p0

    return-object v1

    :catchall_1
    move-exception p2

    move-object v1, p1

    :goto_3
    if-eqz v1, :cond_3

    .line 1796
    :try_start_5
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw p2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 1774
    :cond_4
    :goto_4
    monitor-exit p0

    return-object v1

    :catchall_2
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized getMessagesCountForConversations(Ljava/util/List;[Ljava/lang/String;)Ljava/util/Map;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;[",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    monitor-enter p0

    .line 585
    :try_start_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 586
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const/4 v3, 0x0

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    .line 588
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_6

    goto :goto_0

    :cond_0
    const/16 v1, 0x384

    const/4 v2, 0x0

    .line 594
    :try_start_1
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v1, v4}, Lcom/helpshift/util/DatabaseUtils;->createBatches(ILjava/util/List;)Ljava/util/List;

    move-result-object p1

    .line 596
    iget-object v1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbHelper:Lcom/helpshift/platform/db/ConversationDBHelper;

    invoke-virtual {v1}, Lcom/helpshift/platform/db/ConversationDBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 597
    :try_start_2
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 598
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    .line 599
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    invoke-static {v5}, Lcom/helpshift/util/DatabaseUtils;->makePlaceholders(I)Ljava/lang/String;

    move-result-object v5

    .line 600
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 602
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v8, "conversation_id"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, " IN ("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, ")"

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 603
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 605
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 607
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Long;

    .line 608
    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v5, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_2
    if-eqz p2, :cond_3

    .line 612
    array-length v4, p2

    invoke-static {v4}, Lcom/helpshift/util/DatabaseUtils;->makePlaceholders(I)Ljava/lang/String;

    move-result-object v4

    .line 613
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v8, "type"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, " IN ("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, ")"

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v7, " AND "

    .line 615
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 616
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 618
    invoke-static {p2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v5, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 622
    :cond_3
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v4

    new-array v8, v4, [Ljava/lang/String;

    .line 623
    invoke-interface {v5, v8}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 625
    iget-object v4, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v5, "messages"

    const/4 v4, 0x2

    new-array v7, v4, [Ljava/lang/String;

    const-string v4, "COUNT(*) AS COUNT"

    aput-object v4, v7, v3

    const/4 v4, 0x1

    iget-object v9, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v9}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v9, "conversation_id"

    aput-object v9, v7, v4

    .line 628
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    iget-object v4, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v10, "conversation_id"

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object v4, v1

    move-object v6, v7

    move-object v7, v9

    move-object v9, v10

    move-object v10, v11

    move-object v11, v12

    .line 625
    invoke-virtual/range {v4 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 634
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 636
    :cond_4
    iget-object v4, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v4, "conversation_id"

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    const-string v6, "COUNT"

    .line 637
    invoke-interface {v2, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {v2, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 638
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v0, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 639
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_4

    goto/16 :goto_1

    .line 642
    :cond_5
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    if-eqz v1, :cond_7

    .line 649
    :try_start_3
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->inTransaction()Z

    move-result p1

    if-eqz p1, :cond_7

    .line 650
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_5

    :catchall_0
    move-exception p1

    goto :goto_4

    :catch_0
    move-exception p1

    :try_start_4
    const-string p2, "Helpshift_ConverDB"

    const-string v1, "Error in get messages count inside finally block, "

    .line 654
    invoke-static {p2, v1, p1}, Lcom/helpshift/util/HSLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    if-eqz v2, :cond_a

    .line 658
    :goto_3
    :try_start_5
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_a

    :goto_4
    if-eqz v2, :cond_6

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_6
    throw p1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_6

    :cond_7
    :goto_5
    if-eqz v2, :cond_a

    goto :goto_3

    :catchall_1
    move-exception p1

    goto :goto_b

    :catch_1
    move-exception p1

    move-object p2, v2

    move-object v2, v1

    goto :goto_6

    :catchall_2
    move-exception p1

    move-object v1, v2

    goto :goto_b

    :catch_2
    move-exception p1

    move-object p2, v2

    :goto_6
    :try_start_6
    const-string v1, "Helpshift_ConverDB"

    const-string v3, "Error in get messages count"

    .line 645
    invoke-static {v1, v3, p1}, Lcom/helpshift/util/HSLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_4

    if-eqz v2, :cond_9

    .line 649
    :try_start_7
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->inTransaction()Z

    move-result p1

    if-eqz p1, :cond_9

    .line 650
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    goto :goto_9

    :catchall_3
    move-exception p1

    goto :goto_8

    :catch_3
    move-exception p1

    :try_start_8
    const-string v1, "Helpshift_ConverDB"

    const-string v2, "Error in get messages count inside finally block, "

    .line 654
    invoke-static {v1, v2, p1}, Lcom/helpshift/util/HSLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    if-eqz p2, :cond_a

    .line 658
    :goto_7
    :try_start_9
    invoke-interface {p2}, Landroid/database/Cursor;->close()V

    goto :goto_a

    :goto_8
    if-eqz p2, :cond_8

    invoke-interface {p2}, Landroid/database/Cursor;->close()V

    :cond_8
    throw p1
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_6

    :cond_9
    :goto_9
    if-eqz p2, :cond_a

    goto :goto_7

    .line 662
    :cond_a
    :goto_a
    monitor-exit p0

    return-object v0

    :catchall_4
    move-exception p1

    move-object v1, v2

    move-object v2, p2

    :goto_b
    if-eqz v1, :cond_c

    .line 649
    :try_start_a
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->inTransaction()Z

    move-result p2

    if-eqz p2, :cond_c

    .line 650
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_4
    .catchall {:try_start_a .. :try_end_a} :catchall_5

    goto :goto_e

    :catchall_5
    move-exception p1

    goto :goto_d

    :catch_4
    move-exception p2

    :try_start_b
    const-string v0, "Helpshift_ConverDB"

    const-string v1, "Error in get messages count inside finally block, "

    .line 654
    invoke-static {v0, v1, p2}, Lcom/helpshift/util/HSLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_5

    if-eqz v2, :cond_d

    .line 658
    :goto_c
    :try_start_c
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_f

    :goto_d
    if-eqz v2, :cond_b

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_b
    throw p1

    :cond_c
    :goto_e
    if-eqz v2, :cond_d

    goto :goto_c

    :cond_d
    :goto_f
    throw p1
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_6

    :catchall_6
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized getOldestConversationEpochCreatedAtTime(J)Ljava/lang/Long;
    .locals 12

    monitor-enter p0

    .line 1938
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v1, "user_local_id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " = ?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v0, 0x1

    new-array v6, v0, [Ljava/lang/String;

    .line 1939
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x0

    aput-object p1, v6, p2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    const/4 p1, 0x0

    .line 1941
    :try_start_1
    iget-object v1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbHelper:Lcom/helpshift/platform/db/ConversationDBHelper;

    invoke-virtual {v1}, Lcom/helpshift/platform/db/ConversationDBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 1942
    iget-object v1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v3, "issues"

    new-array v4, v0, [Ljava/lang/String;

    iget-object v0, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v0, "epoch_time_created_at"

    aput-object v0, v4, p2

    const/4 v7, 0x0

    const/4 v8, 0x0

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v0, "epoch_time_created_at"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " ASC"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const-string v10, "1"

    invoke-virtual/range {v2 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p2
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1950
    :try_start_2
    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1951
    iget-object v0, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v0, "epoch_time_created_at"

    const-class v1, Ljava/lang/Long;

    invoke-static {p2, v0, v1}, Lcom/helpshift/util/DatabaseUtils;->parseColumnSafe(Landroid/database/Cursor;Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-object p1, v0

    :cond_0
    if-eqz p2, :cond_1

    .line 1959
    :goto_0
    :try_start_3
    invoke-interface {p2}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    goto :goto_2

    :catch_0
    move-exception v0

    goto :goto_1

    :catchall_0
    move-exception p2

    goto :goto_3

    :catch_1
    move-exception v0

    move-object p2, p1

    :goto_1
    :try_start_4
    const-string v1, "Helpshift_ConverDB"

    const-string v2, "Error in getting latest conversation created_at time"

    .line 1955
    invoke-static {v1, v2, v0}, Lcom/helpshift/util/HSLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    if-eqz p2, :cond_1

    goto :goto_0

    .line 1962
    :cond_1
    :goto_2
    monitor-exit p0

    return-object p1

    :catchall_1
    move-exception p1

    move-object v11, p2

    move-object p2, p1

    move-object p1, v11

    :goto_3
    if-eqz p1, :cond_2

    .line 1959
    :try_start_5
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    :cond_2
    throw p2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :catchall_2
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized getOldestMessageCursor(J)Ljava/lang/String;
    .locals 9

    monitor-enter p0

    :try_start_0
    const-string v0, "message_create_at"

    .line 1899
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v2, "issues"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v2, "user_local_id"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1900
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v3, "issues"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v3, "_id"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1901
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v4, "messages"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v4, "conversation_id"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1902
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v5, "messages"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v5, "created_at"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1903
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v6, "messages"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v6, "epoch_time_created_at"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1904
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "SELECT "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, " AS "

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, " FROM "

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v4, "issues"

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, " INNER JOIN "

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v4, "messages"

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, " ON "

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " = "

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " WHERE "

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " = ? ORDER BY "

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "  ASC LIMIT 1"

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    .line 1914
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v2, v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    const/4 p1, 0x0

    .line 1916
    :try_start_1
    iget-object p2, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbHelper:Lcom/helpshift/platform/db/ConversationDBHelper;

    invoke-virtual {p2}, Lcom/helpshift/platform/db/ConversationDBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object p2

    .line 1917
    invoke-virtual {p2, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p2
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1919
    :try_start_2
    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1920
    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object p1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :cond_0
    if-eqz p2, :cond_1

    .line 1928
    :goto_0
    :try_start_3
    invoke-interface {p2}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    goto :goto_2

    :catch_0
    move-exception v0

    goto :goto_1

    :catchall_0
    move-exception p2

    move-object v8, p2

    move-object p2, p1

    move-object p1, v8

    goto :goto_3

    :catch_1
    move-exception v0

    move-object p2, p1

    :goto_1
    :try_start_4
    const-string v1, "Helpshift_ConverDB"

    const-string v2, "Error in read messages"

    .line 1924
    invoke-static {v1, v2, v0}, Lcom/helpshift/util/HSLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    if-eqz p2, :cond_1

    goto :goto_0

    .line 1932
    :cond_1
    :goto_2
    monitor-exit p0

    return-object p1

    :catchall_1
    move-exception p1

    :goto_3
    if-eqz p2, :cond_2

    .line 1928
    :try_start_5
    invoke-interface {p2}, Landroid/database/Cursor;->close()V

    :cond_2
    throw p1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :catchall_2
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized insertConversation(Lcom/helpshift/conversation/activeconversation/ConversationDM;)J
    .locals 5

    monitor-enter p0

    .line 291
    :try_start_0
    invoke-direct {p0, p1}, Lcom/helpshift/common/conversation/ConversationDB;->readableConversationToContentValues(Lcom/helpshift/conversation/activeconversation/ConversationDM;)Landroid/content/ContentValues;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-wide/16 v0, -0x1

    .line 294
    :try_start_1
    iget-object v2, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbHelper:Lcom/helpshift/platform/db/ConversationDBHelper;

    invoke-virtual {v2}, Lcom/helpshift/platform/db/ConversationDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 295
    iget-object v3, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v3, "issues"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4, p1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_2
    const-string v2, "Helpshift_ConverDB"

    const-string v3, "Error in insert conversation"

    .line 298
    invoke-static {v2, v3, p1}, Lcom/helpshift/util/HSLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 300
    :goto_0
    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized insertConversations(Ljava/util/List;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/helpshift/conversation/activeconversation/ConversationDM;",
            ">;)",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    monitor-enter p0

    .line 304
    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 305
    monitor-exit p0

    return-object v1

    .line 308
    :cond_0
    :try_start_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 309
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/helpshift/conversation/activeconversation/ConversationDM;

    .line 310
    invoke-direct {p0, v2}, Lcom/helpshift/common/conversation/ConversationDB;->readableConversationToContentValues(Lcom/helpshift/conversation/activeconversation/ConversationDM;)Landroid/content/ContentValues;

    move-result-object v2

    .line 311
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 314
    :cond_1
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 316
    :try_start_2
    iget-object v2, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbHelper:Lcom/helpshift/platform/db/ConversationDBHelper;

    invoke-virtual {v2}, Lcom/helpshift/platform/db/ConversationDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 317
    :try_start_3
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 318
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/ContentValues;

    .line 319
    iget-object v4, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v4, "issues"

    invoke-virtual {v2, v4, v1, v3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 320
    invoke-interface {p1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 322
    :cond_2
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz v2, :cond_3

    .line 330
    :try_start_4
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    goto :goto_4

    :catch_0
    move-exception v0

    :try_start_5
    const-string v1, "Helpshift_ConverDB"

    const-string v2, "Error in insert conversations inside finally block"

    .line 333
    :goto_2
    invoke-static {v1, v2, v0}, Lcom/helpshift/util/HSLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    goto :goto_4

    :catchall_0
    move-exception p1

    goto :goto_5

    :catch_1
    move-exception v0

    move-object v1, v2

    goto :goto_3

    :catchall_1
    move-exception p1

    move-object v2, v1

    goto :goto_5

    :catch_2
    move-exception v0

    :goto_3
    :try_start_6
    const-string v2, "Helpshift_ConverDB"

    const-string v3, "Error in insert conversations"

    .line 325
    invoke-static {v2, v3, v0}, Lcom/helpshift/util/HSLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    if-eqz v1, :cond_3

    .line 330
    :try_start_7
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    goto :goto_4

    :catch_3
    move-exception v0

    :try_start_8
    const-string v1, "Helpshift_ConverDB"

    const-string v2, "Error in insert conversations inside finally block"
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    goto :goto_2

    .line 337
    :cond_3
    :goto_4
    monitor-exit p0

    return-object p1

    :goto_5
    if-eqz v2, :cond_4

    .line 330
    :try_start_9
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_4
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    goto :goto_6

    :catch_4
    move-exception v0

    :try_start_a
    const-string v1, "Helpshift_ConverDB"

    const-string v2, "Error in insert conversations inside finally block"

    .line 333
    invoke-static {v1, v2, v0}, Lcom/helpshift/util/HSLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 334
    :cond_4
    :goto_6
    throw p1
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    :catchall_2
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized insertMessage(Lcom/helpshift/conversation/activeconversation/message/MessageDM;)J
    .locals 5

    monitor-enter p0

    .line 470
    :try_start_0
    invoke-direct {p0, p1}, Lcom/helpshift/common/conversation/ConversationDB;->readableMessageToContentValues(Lcom/helpshift/conversation/activeconversation/message/MessageDM;)Landroid/content/ContentValues;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-wide/16 v0, -0x1

    .line 473
    :try_start_1
    iget-object v2, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbHelper:Lcom/helpshift/platform/db/ConversationDBHelper;

    invoke-virtual {v2}, Lcom/helpshift/platform/db/ConversationDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 474
    iget-object v3, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v3, "messages"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4, p1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_2
    const-string v2, "Helpshift_ConverDB"

    const-string v3, "Error in insert message"

    .line 477
    invoke-static {v2, v3, p1}, Lcom/helpshift/util/HSLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 479
    :goto_0
    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized insertMessages(Ljava/util/List;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/helpshift/conversation/activeconversation/message/MessageDM;",
            ">;)",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    monitor-enter p0

    .line 483
    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 484
    monitor-exit p0

    return-object v1

    .line 487
    :cond_0
    :try_start_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 488
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/helpshift/conversation/activeconversation/message/MessageDM;

    .line 489
    invoke-direct {p0, v2}, Lcom/helpshift/common/conversation/ConversationDB;->readableMessageToContentValues(Lcom/helpshift/conversation/activeconversation/message/MessageDM;)Landroid/content/ContentValues;

    move-result-object v2

    .line 490
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 494
    :cond_1
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 496
    :try_start_2
    iget-object v2, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbHelper:Lcom/helpshift/platform/db/ConversationDBHelper;

    invoke-virtual {v2}, Lcom/helpshift/platform/db/ConversationDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 497
    :try_start_3
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 498
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/ContentValues;

    .line 499
    iget-object v4, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v4, "messages"

    invoke-virtual {v2, v4, v1, v3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 500
    invoke-interface {p1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 502
    :cond_2
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz v2, :cond_3

    .line 510
    :try_start_4
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    goto :goto_4

    :catch_0
    move-exception v0

    :try_start_5
    const-string v1, "Helpshift_ConverDB"

    const-string v2, "Error in insert messages inside finally block"

    .line 513
    :goto_2
    invoke-static {v1, v2, v0}, Lcom/helpshift/util/HSLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    goto :goto_4

    :catchall_0
    move-exception p1

    goto :goto_5

    :catch_1
    move-exception v0

    move-object v1, v2

    goto :goto_3

    :catchall_1
    move-exception p1

    move-object v2, v1

    goto :goto_5

    :catch_2
    move-exception v0

    :goto_3
    :try_start_6
    const-string v2, "Helpshift_ConverDB"

    const-string v3, "Error in insert messages"

    .line 505
    invoke-static {v2, v3, v0}, Lcom/helpshift/util/HSLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    if-eqz v1, :cond_3

    .line 510
    :try_start_7
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    goto :goto_4

    :catch_3
    move-exception v0

    :try_start_8
    const-string v1, "Helpshift_ConverDB"

    const-string v2, "Error in insert messages inside finally block"
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    goto :goto_2

    .line 517
    :cond_3
    :goto_4
    monitor-exit p0

    return-object p1

    :goto_5
    if-eqz v2, :cond_4

    .line 510
    :try_start_9
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_4
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    goto :goto_6

    :catch_4
    move-exception v0

    :try_start_a
    const-string v1, "Helpshift_ConverDB"

    const-string v2, "Error in insert messages inside finally block"

    .line 513
    invoke-static {v1, v2, v0}, Lcom/helpshift/util/HSLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 514
    :cond_4
    :goto_6
    throw p1
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    :catchall_2
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized insertOrUpdateAdminFAQSuggestion(Lcom/helpshift/support/Faq;)V
    .locals 5

    monitor-enter p0

    .line 1803
    :try_start_0
    invoke-static {p1}, Lcom/helpshift/common/conversation/ConversationDB;->faqToContentValues(Lcom/helpshift/support/Faq;)Landroid/content/ContentValues;

    move-result-object v0

    const-string v1, "publish_id = ? AND language = ?"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    .line 1807
    iget-object v4, p1, Lcom/helpshift/support/Faq;->publish_id:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object p1, p1, Lcom/helpshift/support/Faq;->language:Ljava/lang/String;

    aput-object p1, v2, v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1809
    :try_start_1
    iget-object p1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbHelper:Lcom/helpshift/platform/db/ConversationDBHelper;

    invoke-virtual {p1}, Lcom/helpshift/platform/db/ConversationDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object p1

    const-string v3, "faq_suggestions"

    .line 1810
    invoke-direct {p0, p1, v3, v1, v2}, Lcom/helpshift/common/conversation/ConversationDB;->exists(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v1, "faq_suggestions"

    const/4 v2, 0x0

    .line 1812
    invoke-virtual {p1, v1, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    goto :goto_0

    :cond_0
    const-string v3, "faq_suggestions"

    .line 1815
    invoke-virtual {p1, v3, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_2
    const-string v0, "Helpshift_ConverDB"

    const-string v1, "Error in insertOrUpdateAdminFAQSuggestion"

    .line 1819
    invoke-static {v0, v1, p1}, Lcom/helpshift/util/HSLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1821
    :goto_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized readConversationInboxRecord(J)Lcom/helpshift/conversation/dto/dao/ConversationInboxRecord;
    .locals 11

    monitor-enter p0

    .line 443
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v1, "user_local_id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " = ?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v0, 0x1

    new-array v6, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    .line 444
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v6, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    const/4 p1, 0x0

    .line 446
    :try_start_1
    iget-object p2, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbHelper:Lcom/helpshift/platform/db/ConversationDBHelper;

    invoke-virtual {p2}, Lcom/helpshift/platform/db/ConversationDBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 447
    iget-object p2, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v3, "conversation_inbox"

    const/4 v4, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object p2
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 454
    :try_start_2
    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 455
    invoke-direct {p0, p2}, Lcom/helpshift/common/conversation/ConversationDB;->cursorToConversationInboxRecord(Landroid/database/Cursor;)Lcom/helpshift/conversation/dto/dao/ConversationInboxRecord;

    move-result-object p1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :cond_0
    if-eqz p2, :cond_1

    .line 463
    :goto_0
    :try_start_3
    invoke-interface {p2}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    goto :goto_2

    :catch_0
    move-exception v0

    goto :goto_1

    :catchall_0
    move-exception p2

    goto :goto_3

    :catch_1
    move-exception v0

    move-object p2, p1

    :goto_1
    :try_start_4
    const-string v1, "Helpshift_ConverDB"

    const-string v2, "Error in read conversation inbox record"

    .line 459
    invoke-static {v1, v2, v0}, Lcom/helpshift/util/HSLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    if-eqz p2, :cond_1

    goto :goto_0

    .line 466
    :cond_1
    :goto_2
    monitor-exit p0

    return-object p1

    :catchall_1
    move-exception p1

    move-object v10, p2

    move-object p2, p1

    move-object p1, v10

    :goto_3
    if-eqz p1, :cond_2

    .line 463
    :try_start_5
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    :cond_2
    throw p2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :catchall_2
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized readConversationWithLocalId(Ljava/lang/Long;)Lcom/helpshift/conversation/activeconversation/ConversationDM;
    .locals 11

    monitor-enter p0

    .line 194
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " = ?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v0, 0x1

    new-array v6, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    .line 195
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v6, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    const/4 p1, 0x0

    .line 197
    :try_start_1
    iget-object v0, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbHelper:Lcom/helpshift/platform/db/ConversationDBHelper;

    invoke-virtual {v0}, Lcom/helpshift/platform/db/ConversationDBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 198
    iget-object v0, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v3, "issues"

    const/4 v4, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 205
    :try_start_2
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 206
    invoke-direct {p0, v0}, Lcom/helpshift/common/conversation/ConversationDB;->cursorToReadableConversation(Landroid/database/Cursor;)Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object p1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :cond_0
    if-eqz v0, :cond_1

    .line 214
    :goto_0
    :try_start_3
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    goto :goto_2

    :catch_0
    move-exception v1

    goto :goto_1

    :catchall_0
    move-exception v0

    goto :goto_3

    :catch_1
    move-exception v1

    move-object v0, p1

    :goto_1
    :try_start_4
    const-string v2, "Helpshift_ConverDB"

    const-string v3, "Error in read conversations with localId"

    .line 210
    invoke-static {v2, v3, v1}, Lcom/helpshift/util/HSLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    if-eqz v0, :cond_1

    goto :goto_0

    .line 217
    :cond_1
    :goto_2
    monitor-exit p0

    return-object p1

    :catchall_1
    move-exception p1

    move-object v10, v0

    move-object v0, p1

    move-object p1, v10

    :goto_3
    if-eqz p1, :cond_2

    .line 214
    :try_start_5
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :catchall_2
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized readConversationWithServerId(Ljava/lang/String;)Lcom/helpshift/conversation/activeconversation/ConversationDM;
    .locals 11

    monitor-enter p0

    .line 235
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v1, "server_id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " = ?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v0, 0x1

    new-array v6, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    .line 236
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v6, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    const/4 p1, 0x0

    .line 238
    :try_start_1
    iget-object v0, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbHelper:Lcom/helpshift/platform/db/ConversationDBHelper;

    invoke-virtual {v0}, Lcom/helpshift/platform/db/ConversationDBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 239
    iget-object v0, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v3, "issues"

    const/4 v4, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 246
    :try_start_2
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 247
    invoke-direct {p0, v0}, Lcom/helpshift/common/conversation/ConversationDB;->cursorToReadableConversation(Landroid/database/Cursor;)Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object p1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :cond_0
    if-eqz v0, :cond_1

    .line 255
    :goto_0
    :try_start_3
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    goto :goto_2

    :catch_0
    move-exception v1

    goto :goto_1

    :catchall_0
    move-exception v0

    goto :goto_3

    :catch_1
    move-exception v1

    move-object v0, p1

    :goto_1
    :try_start_4
    const-string v2, "Helpshift_ConverDB"

    const-string v3, "Error in read conversations with serverId"

    .line 251
    invoke-static {v2, v3, v1}, Lcom/helpshift/util/HSLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    if-eqz v0, :cond_1

    goto :goto_0

    .line 258
    :cond_1
    :goto_2
    monitor-exit p0

    return-object p1

    :catchall_1
    move-exception p1

    move-object v10, v0

    move-object v0, p1

    move-object p1, v10

    :goto_3
    if-eqz p1, :cond_2

    .line 255
    :try_start_5
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :catchall_2
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized readConversationsWithLocalId(J)Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/List<",
            "Lcom/helpshift/conversation/activeconversation/ConversationDM;",
            ">;"
        }
    .end annotation

    monitor-enter p0

    .line 159
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    .line 161
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v3, "user_local_id"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " = ?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v2, 0x1

    new-array v8, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    .line 162
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v8, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 164
    :try_start_1
    iget-object p1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbHelper:Lcom/helpshift/platform/db/ConversationDBHelper;

    invoke-virtual {p1}, Lcom/helpshift/platform/db/ConversationDBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 165
    iget-object p1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v5, "issues"

    const/4 v6, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual/range {v4 .. v11}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 173
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 175
    :cond_0
    invoke-direct {p0, v1}, Lcom/helpshift/common/conversation/ConversationDB;->cursorToReadableConversation(Landroid/database/Cursor;)Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object p1

    .line 176
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 177
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez p1, :cond_0

    :cond_1
    if-eqz v1, :cond_2

    .line 185
    :goto_0
    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_1

    :catchall_0
    move-exception p1

    goto :goto_2

    :catch_0
    move-exception p1

    :try_start_3
    const-string p2, "Helpshift_ConverDB"

    const-string v2, "Error in read conversations with localId"

    .line 181
    invoke-static {p2, v2, p1}, Lcom/helpshift/util/HSLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz v1, :cond_2

    goto :goto_0

    .line 188
    :cond_2
    :goto_1
    monitor-exit p0

    return-object v0

    :goto_2
    if-eqz v1, :cond_3

    .line 185
    :try_start_4
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw p1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catchall_1
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized readMessageWithLocalId(Ljava/lang/Long;)Lcom/helpshift/conversation/activeconversation/message/MessageDM;
    .locals 11

    monitor-enter p0

    .line 1742
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " = ?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v0, 0x1

    new-array v6, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    .line 1743
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v6, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    const/4 p1, 0x0

    .line 1745
    :try_start_1
    iget-object v0, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbHelper:Lcom/helpshift/platform/db/ConversationDBHelper;

    invoke-virtual {v0}, Lcom/helpshift/platform/db/ConversationDBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 1746
    iget-object v0, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v3, "messages"

    const/4 v4, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1753
    :try_start_2
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1754
    invoke-direct {p0, v0}, Lcom/helpshift/common/conversation/ConversationDB;->cursorToMessageDM(Landroid/database/Cursor;)Lcom/helpshift/conversation/activeconversation/message/MessageDM;

    move-result-object p1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :cond_0
    if-eqz v0, :cond_1

    .line 1762
    :goto_0
    :try_start_3
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    goto :goto_2

    :catch_0
    move-exception v1

    goto :goto_1

    :catchall_0
    move-exception v0

    goto :goto_3

    :catch_1
    move-exception v1

    move-object v0, p1

    :goto_1
    :try_start_4
    const-string v2, "Helpshift_ConverDB"

    const-string v3, "Error in read message with localId"

    .line 1758
    invoke-static {v2, v3, v1}, Lcom/helpshift/util/HSLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    if-eqz v0, :cond_1

    goto :goto_0

    .line 1765
    :cond_1
    :goto_2
    monitor-exit p0

    return-object p1

    :catchall_1
    move-exception p1

    move-object v10, v0

    move-object v0, p1

    move-object p1, v10

    :goto_3
    if-eqz p1, :cond_2

    .line 1762
    :try_start_5
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :catchall_2
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized readMessageWithServerId(Ljava/lang/String;)Lcom/helpshift/conversation/activeconversation/message/MessageDM;
    .locals 11

    monitor-enter p0

    .line 1713
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v1, "server_id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " = ?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v0, 0x1

    new-array v6, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    .line 1714
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v6, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    const/4 p1, 0x0

    .line 1716
    :try_start_1
    iget-object v0, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbHelper:Lcom/helpshift/platform/db/ConversationDBHelper;

    invoke-virtual {v0}, Lcom/helpshift/platform/db/ConversationDBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 1717
    iget-object v0, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v3, "messages"

    const/4 v4, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1724
    :try_start_2
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1725
    invoke-direct {p0, v0}, Lcom/helpshift/common/conversation/ConversationDB;->cursorToMessageDM(Landroid/database/Cursor;)Lcom/helpshift/conversation/activeconversation/message/MessageDM;

    move-result-object p1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :cond_0
    if-eqz v0, :cond_1

    .line 1733
    :goto_0
    :try_start_3
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    goto :goto_2

    :catch_0
    move-exception v1

    goto :goto_1

    :catchall_0
    move-exception v0

    goto :goto_3

    :catch_1
    move-exception v1

    move-object v0, p1

    :goto_1
    :try_start_4
    const-string v2, "Helpshift_ConverDB"

    const-string v3, "Error in read message with serverId"

    .line 1729
    invoke-static {v2, v3, v1}, Lcom/helpshift/util/HSLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    if-eqz v0, :cond_1

    goto :goto_0

    .line 1736
    :cond_1
    :goto_2
    monitor-exit p0

    return-object p1

    :catchall_1
    move-exception p1

    move-object v10, v0

    move-object v0, p1

    move-object p1, v10

    :goto_3
    if-eqz p1, :cond_2

    .line 1733
    :try_start_5
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :catchall_2
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized readMessages(J)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/List<",
            "Lcom/helpshift/conversation/activeconversation/message/MessageDM;",
            ">;"
        }
    .end annotation

    monitor-enter p0

    .line 666
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v1, "conversation_id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " = ?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    .line 667
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v1, v2

    .line 668
    invoke-direct {p0, v0, v1}, Lcom/helpshift/common/conversation/ConversationDB;->readMessages(Ljava/lang/String;[Ljava/lang/String;)Ljava/util/List;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized readMessages(JLcom/helpshift/conversation/activeconversation/message/MessageType;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/helpshift/conversation/activeconversation/message/MessageType;",
            ")",
            "Ljava/util/List<",
            "Lcom/helpshift/conversation/activeconversation/message/MessageDM;",
            ">;"
        }
    .end annotation

    monitor-enter p0

    .line 672
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v1, "conversation_id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " = ? AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v1, "type"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " = ?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    .line 673
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v1, v2

    const/4 p1, 0x1

    invoke-virtual {p3}, Lcom/helpshift/conversation/activeconversation/message/MessageType;->getValue()Ljava/lang/String;

    move-result-object p2

    aput-object p2, v1, p1

    .line 674
    invoke-direct {p0, v0, v1}, Lcom/helpshift/common/conversation/ConversationDB;->readMessages(Ljava/lang/String;[Ljava/lang/String;)Ljava/util/List;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized readMessagesForConversations(Ljava/util/Collection;)Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Ljava/lang/Long;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/helpshift/conversation/activeconversation/message/MessageDM;",
            ">;"
        }
    .end annotation

    monitor-enter p0

    .line 521
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_6

    const/16 v1, 0x384

    const/4 v2, 0x0

    .line 526
    :try_start_1
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v1, v3}, Lcom/helpshift/util/DatabaseUtils;->createBatches(ILjava/util/List;)Ljava/util/List;

    move-result-object p1

    .line 528
    iget-object v1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbHelper:Lcom/helpshift/platform/db/ConversationDBHelper;

    invoke-virtual {v1}, Lcom/helpshift/platform/db/ConversationDBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 529
    :try_start_2
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 531
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    .line 532
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    invoke-static {v4}, Lcom/helpshift/util/DatabaseUtils;->makePlaceholders(I)Ljava/lang/String;

    move-result-object v4

    .line 533
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v6, "conversation_id"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, " IN ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, ")"

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 534
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    new-array v7, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    .line 535
    :goto_1
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v5

    if-ge v4, v5, :cond_1

    .line 536
    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v7, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 539
    :cond_1
    iget-object v3, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v4, "messages"

    const/4 v5, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v3, v1

    invoke-virtual/range {v3 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 547
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 549
    :cond_2
    invoke-direct {p0, v2}, Lcom/helpshift/common/conversation/ConversationDB;->cursorToMessageDM(Landroid/database/Cursor;)Lcom/helpshift/conversation/activeconversation/message/MessageDM;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 555
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 557
    :cond_3
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_2

    goto :goto_0

    .line 560
    :cond_4
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    if-eqz v1, :cond_6

    .line 567
    :try_start_3
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->inTransaction()Z

    move-result p1

    if-eqz p1, :cond_6

    .line 568
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_4

    :catchall_0
    move-exception p1

    goto :goto_3

    :catch_0
    move-exception p1

    :try_start_4
    const-string v1, "Helpshift_ConverDB"

    const-string v3, "Error in read messages inside finally block, "

    .line 572
    invoke-static {v1, v3, p1}, Lcom/helpshift/util/HSLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    if-eqz v2, :cond_9

    .line 576
    :goto_2
    :try_start_5
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_9

    :goto_3
    if-eqz v2, :cond_5

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_5
    throw p1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_6

    :cond_6
    :goto_4
    if-eqz v2, :cond_9

    goto :goto_2

    :catchall_1
    move-exception p1

    goto :goto_a

    :catch_1
    move-exception p1

    move-object v11, v2

    move-object v2, v1

    move-object v1, v11

    goto :goto_5

    :catchall_2
    move-exception p1

    move-object v1, v2

    goto :goto_a

    :catch_2
    move-exception p1

    move-object v1, v2

    :goto_5
    :try_start_6
    const-string v3, "Helpshift_ConverDB"

    const-string v4, "Error in read messages"

    .line 563
    invoke-static {v3, v4, p1}, Lcom/helpshift/util/HSLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_4

    if-eqz v2, :cond_8

    .line 567
    :try_start_7
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->inTransaction()Z

    move-result p1

    if-eqz p1, :cond_8

    .line 568
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    goto :goto_8

    :catchall_3
    move-exception p1

    goto :goto_7

    :catch_3
    move-exception p1

    :try_start_8
    const-string v2, "Helpshift_ConverDB"

    const-string v3, "Error in read messages inside finally block, "

    .line 572
    invoke-static {v2, v3, p1}, Lcom/helpshift/util/HSLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    if-eqz v1, :cond_9

    .line 576
    :goto_6
    :try_start_9
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_9

    :goto_7
    if-eqz v1, :cond_7

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_7
    throw p1
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_6

    :cond_8
    :goto_8
    if-eqz v1, :cond_9

    goto :goto_6

    .line 580
    :cond_9
    :goto_9
    monitor-exit p0

    return-object v0

    :catchall_4
    move-exception p1

    move-object v11, v2

    move-object v2, v1

    move-object v1, v11

    :goto_a
    if-eqz v1, :cond_b

    .line 567
    :try_start_a
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->inTransaction()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 568
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_4
    .catchall {:try_start_a .. :try_end_a} :catchall_5

    goto :goto_d

    :catchall_5
    move-exception p1

    goto :goto_c

    :catch_4
    move-exception v0

    :try_start_b
    const-string v1, "Helpshift_ConverDB"

    const-string v3, "Error in read messages inside finally block, "

    .line 572
    invoke-static {v1, v3, v0}, Lcom/helpshift/util/HSLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_5

    if-eqz v2, :cond_c

    .line 576
    :goto_b
    :try_start_c
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_e

    :goto_c
    if-eqz v2, :cond_a

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_a
    throw p1

    :cond_b
    :goto_d
    if-eqz v2, :cond_c

    goto :goto_b

    :cond_c
    :goto_e
    throw p1
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_6

    :catchall_6
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized readPreConversationWithServerId(Ljava/lang/String;)Lcom/helpshift/conversation/activeconversation/ConversationDM;
    .locals 11

    monitor-enter p0

    .line 264
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v1, "pre_conv_server_id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " = ?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v0, 0x1

    new-array v6, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    .line 265
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v6, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    const/4 p1, 0x0

    .line 267
    :try_start_1
    iget-object v0, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbHelper:Lcom/helpshift/platform/db/ConversationDBHelper;

    invoke-virtual {v0}, Lcom/helpshift/platform/db/ConversationDBHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 268
    iget-object v0, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v3, "issues"

    const/4 v4, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 275
    :try_start_2
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 276
    invoke-direct {p0, v0}, Lcom/helpshift/common/conversation/ConversationDB;->cursorToReadableConversation(Landroid/database/Cursor;)Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object p1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :cond_0
    if-eqz v0, :cond_1

    .line 284
    :goto_0
    :try_start_3
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    goto :goto_2

    :catch_0
    move-exception v1

    goto :goto_1

    :catchall_0
    move-exception v0

    goto :goto_3

    :catch_1
    move-exception v1

    move-object v0, p1

    :goto_1
    :try_start_4
    const-string v2, "Helpshift_ConverDB"

    const-string v3, "Error in read conversations with serverId"

    .line 280
    invoke-static {v2, v3, v1}, Lcom/helpshift/util/HSLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    if-eqz v0, :cond_1

    goto :goto_0

    .line 287
    :cond_1
    :goto_2
    monitor-exit p0

    return-object p1

    :catchall_1
    move-exception p1

    move-object v10, v0

    move-object v0, p1

    move-object p1, v10

    :goto_3
    if-eqz p1, :cond_2

    .line 284
    :try_start_5
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :catchall_2
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized removeAdminFAQSuggestion(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    monitor-enter p0

    .line 1839
    :try_start_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :try_start_1
    const-string v0, "publish_id = ? AND language = ?"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 p1, 0x1

    aput-object p2, v1, p1

    .line 1845
    iget-object p1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbHelper:Lcom/helpshift/platform/db/ConversationDBHelper;

    invoke-virtual {p1}, Lcom/helpshift/platform/db/ConversationDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object p1

    const-string p2, "faq_suggestions"

    .line 1846
    invoke-virtual {p1, p2, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_2
    const-string p2, "Helpshift_ConverDB"

    const-string v0, "Error in removeAdminFAQSuggestion"

    .line 1849
    invoke-static {p2, v0, p1}, Lcom/helpshift/util/HSLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1852
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized storeConversationInboxRecord(Lcom/helpshift/conversation/dto/dao/ConversationInboxRecord;)Lcom/helpshift/conversation/dto/dao/ConversationInboxRecord;
    .locals 5

    monitor-enter p0

    .line 418
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v1, "user_local_id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " = ?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    .line 419
    iget-wide v3, p1, Lcom/helpshift/conversation/dto/dao/ConversationInboxRecord;->userLocalId:J

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 420
    invoke-direct {p0, p1}, Lcom/helpshift/common/conversation/ConversationDB;->conversationInboxRecordToContentValues(Lcom/helpshift/conversation/dto/dao/ConversationInboxRecord;)Landroid/content/ContentValues;

    move-result-object v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 422
    :try_start_1
    iget-object v3, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbHelper:Lcom/helpshift/platform/db/ConversationDBHelper;

    invoke-virtual {v3}, Lcom/helpshift/platform/db/ConversationDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 423
    iget-object v4, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v4, "conversation_inbox"

    invoke-direct {p0, v3, v4, v0, v1}, Lcom/helpshift/common/conversation/ConversationDB;->exists(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 425
    iget-object v4, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v4, "conversation_inbox"

    invoke-virtual {v3, v4, v2, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0

    .line 431
    :cond_0
    iget-object v0, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v0, "conversation_inbox"

    const/4 v1, 0x0

    invoke-virtual {v3, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    const-string v1, "Helpshift_ConverDB"

    const-string v2, "Error in store conversation inbox record"

    .line 435
    invoke-static {v1, v2, v0}, Lcom/helpshift/util/HSLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 437
    :goto_0
    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized updateConversation(Lcom/helpshift/conversation/activeconversation/ConversationDM;)V
    .locals 4

    monitor-enter p0

    .line 341
    :try_start_0
    invoke-direct {p0, p1}, Lcom/helpshift/common/conversation/ConversationDB;->readableConversationToContentValues(Lcom/helpshift/conversation/activeconversation/ConversationDM;)Landroid/content/ContentValues;

    move-result-object v0

    .line 342
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v2, "_id"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " = ?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    .line 343
    iget-object p1, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localId:Ljava/lang/Long;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v2, v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 345
    :try_start_1
    iget-object p1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbHelper:Lcom/helpshift/platform/db/ConversationDBHelper;

    invoke-virtual {p1}, Lcom/helpshift/platform/db/ConversationDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object p1

    .line 346
    iget-object v3, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v3, "issues"

    invoke-virtual {p1, v3, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_2
    const-string v0, "Helpshift_ConverDB"

    const-string v1, "Error in update conversation"

    .line 352
    invoke-static {v0, v1, p1}, Lcom/helpshift/util/HSLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 354
    :goto_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized updateConversations(Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/helpshift/conversation/activeconversation/ConversationDM;",
            ">;)V"
        }
    .end annotation

    monitor-enter p0

    .line 375
    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-nez v0, :cond_0

    .line 376
    monitor-exit p0

    return-void

    .line 378
    :cond_0
    :try_start_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 379
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 380
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    const/4 v4, 0x0

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/helpshift/conversation/activeconversation/ConversationDM;

    .line 381
    invoke-direct {p0, v3}, Lcom/helpshift/common/conversation/ConversationDB;->readableConversationToContentValues(Lcom/helpshift/conversation/activeconversation/ConversationDM;)Landroid/content/ContentValues;

    move-result-object v5

    .line 382
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    .line 383
    iget-object v3, v3, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localId:Ljava/lang/Long;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v4

    .line 384
    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    .line 387
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v5, "_id"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " = ?"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 389
    :try_start_2
    iget-object v5, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbHelper:Lcom/helpshift/platform/db/ConversationDBHelper;

    invoke-virtual {v5}, Lcom/helpshift/platform/db/ConversationDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 390
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 391
    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    if-ge v4, v5, :cond_2

    .line 392
    iget-object v5, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v5, "issues"

    .line 393
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/ContentValues;

    .line 395
    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [Ljava/lang/String;

    .line 392
    invoke-virtual {v2, v5, v6, v3, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 397
    :cond_2
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v2, :cond_3

    .line 405
    :try_start_3
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_3

    :catch_0
    move-exception p1

    :try_start_4
    const-string v0, "Helpshift_ConverDB"

    const-string v1, "Error in update conversations inside finally block"

    .line 408
    :goto_2
    invoke-static {v0, v1, p1}, Lcom/helpshift/util/HSLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_3

    :catchall_0
    move-exception p1

    goto :goto_4

    :catch_1
    move-exception p1

    :try_start_5
    const-string v0, "Helpshift_ConverDB"

    const-string v1, "Error in update conversations"

    .line 400
    invoke-static {v0, v1, p1}, Lcom/helpshift/util/HSLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    if-eqz v2, :cond_3

    .line 405
    :try_start_6
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_3

    :catch_2
    move-exception p1

    :try_start_7
    const-string v0, "Helpshift_ConverDB"

    const-string v1, "Error in update conversations inside finally block"
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto :goto_2

    .line 412
    :cond_3
    :goto_3
    monitor-exit p0

    return-void

    :goto_4
    if-eqz v2, :cond_4

    .line 405
    :try_start_8
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_5

    :catch_3
    move-exception v0

    :try_start_9
    const-string v1, "Helpshift_ConverDB"

    const-string v2, "Error in update conversations inside finally block"

    .line 408
    invoke-static {v1, v2, v0}, Lcom/helpshift/util/HSLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 409
    :cond_4
    :goto_5
    throw p1
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    :catchall_1
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized updateLastUserActivityTimeInConversation(Ljava/lang/Long;J)V
    .locals 2

    monitor-enter p0

    .line 357
    :try_start_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 358
    iget-object v1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v1, "last_user_activity_time"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 360
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object p3, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string p3, "_id"

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p3, " = ?"

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    const/4 p3, 0x1

    new-array p3, p3, [Ljava/lang/String;

    const/4 v1, 0x0

    .line 361
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    aput-object p1, p3, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 363
    :try_start_1
    iget-object p1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbHelper:Lcom/helpshift/platform/db/ConversationDBHelper;

    invoke-virtual {p1}, Lcom/helpshift/platform/db/ConversationDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object p1

    .line 364
    iget-object v1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v1, "issues"

    invoke-virtual {p1, v1, v0, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_2
    const-string p2, "Helpshift_ConverDB"

    const-string p3, "Error in updateLastUserActivityTimeInConversation"

    .line 370
    invoke-static {p2, p3, p1}, Lcom/helpshift/util/HSLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 372
    :goto_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized updateMessage(Lcom/helpshift/conversation/activeconversation/message/MessageDM;)V
    .locals 4

    monitor-enter p0

    .line 713
    :try_start_0
    invoke-direct {p0, p1}, Lcom/helpshift/common/conversation/ConversationDB;->readableMessageToContentValues(Lcom/helpshift/conversation/activeconversation/message/MessageDM;)Landroid/content/ContentValues;

    move-result-object v0

    .line 714
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v2, "_id"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " = ?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    .line 715
    iget-object p1, p1, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->localId:Ljava/lang/Long;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v2, v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 717
    :try_start_1
    iget-object p1, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbHelper:Lcom/helpshift/platform/db/ConversationDBHelper;

    invoke-virtual {p1}, Lcom/helpshift/platform/db/ConversationDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object p1

    .line 718
    iget-object v3, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v3, "messages"

    invoke-virtual {p1, v3, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_2
    const-string v0, "Helpshift_ConverDB"

    const-string v1, "Error in update message"

    .line 724
    invoke-static {v0, v1, p1}, Lcom/helpshift/util/HSLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 726
    :goto_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized updateMessages(Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/helpshift/conversation/activeconversation/message/MessageDM;",
            ">;)V"
        }
    .end annotation

    monitor-enter p0

    .line 729
    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-nez v0, :cond_0

    .line 730
    monitor-exit p0

    return-void

    .line 732
    :cond_0
    :try_start_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 733
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 734
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    const/4 v4, 0x0

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/helpshift/conversation/activeconversation/message/MessageDM;

    .line 735
    invoke-direct {p0, v3}, Lcom/helpshift/common/conversation/ConversationDB;->readableMessageToContentValues(Lcom/helpshift/conversation/activeconversation/message/MessageDM;)Landroid/content/ContentValues;

    move-result-object v5

    .line 736
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    .line 737
    iget-object v3, v3, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->localId:Ljava/lang/Long;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v4

    .line 738
    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    .line 741
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v5, "_id"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " = ?"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 743
    :try_start_2
    iget-object v5, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbHelper:Lcom/helpshift/platform/db/ConversationDBHelper;

    invoke-virtual {v5}, Lcom/helpshift/platform/db/ConversationDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 744
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 745
    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    if-ge v4, v5, :cond_2

    .line 746
    iget-object v5, p0, Lcom/helpshift/common/conversation/ConversationDB;->dbInfo:Lcom/helpshift/common/conversation/ConversationDBInfo;

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v5, "messages"

    .line 747
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/ContentValues;

    .line 749
    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [Ljava/lang/String;

    .line 746
    invoke-virtual {v2, v5, v6, v3, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 751
    :cond_2
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v2, :cond_3

    .line 759
    :try_start_3
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_3

    :catch_0
    move-exception p1

    :try_start_4
    const-string v0, "Helpshift_ConverDB"

    const-string v1, "Error in update messages"

    .line 762
    :goto_2
    invoke-static {v0, v1, p1}, Lcom/helpshift/util/HSLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_3

    :catchall_0
    move-exception p1

    goto :goto_4

    :catch_1
    move-exception p1

    :try_start_5
    const-string v0, "Helpshift_ConverDB"

    const-string v1, "Error in update messages"

    .line 754
    invoke-static {v0, v1, p1}, Lcom/helpshift/util/HSLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    if-eqz v2, :cond_3

    .line 759
    :try_start_6
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_3

    :catch_2
    move-exception p1

    :try_start_7
    const-string v0, "Helpshift_ConverDB"

    const-string v1, "Error in update messages"
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto :goto_2

    .line 766
    :cond_3
    :goto_3
    monitor-exit p0

    return-void

    :goto_4
    if-eqz v2, :cond_4

    .line 759
    :try_start_8
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_5

    :catch_3
    move-exception v0

    :try_start_9
    const-string v1, "Helpshift_ConverDB"

    const-string v2, "Error in update messages"

    .line 762
    invoke-static {v1, v2, v0}, Lcom/helpshift/util/HSLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 763
    :cond_4
    :goto_5
    throw p1
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    :catchall_1
    move-exception p1

    monitor-exit p0

    throw p1
.end method
