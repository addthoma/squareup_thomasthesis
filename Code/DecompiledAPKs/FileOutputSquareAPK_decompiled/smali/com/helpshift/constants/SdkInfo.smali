.class public Lcom/helpshift/constants/SdkInfo;
.super Ljava/lang/Object;
.source "SdkInfo.java"


# static fields
.field public static final ACTION_EXECUTOR_SERIAL_VERSION_UID:J = -0x508cdfafb713c2c0L

.field public static final ACTION_MODEL_SERIAL_VERSION_UID:J = 0x1L

.field public static final ANALYTICS_EVENT_SERIAL_VERSION_UID:J = 0x7bf0d2bd71252200L

.field public static final CAMPAIGNS_ACTION_EXECUTOR_SERIAL_VERSION_UID:J = 0x73f4dd88f0a010f5L

.field public static final CAMPAIGN_SYNC_MODEL_SERIAL_VERSION_UID:J = 0x2L

.field public static final PROPERTY_VALUE_SERIAL_VERSION_UID:J = 0x2L

.field public static final SDK_VERSION:Ljava/lang/String; = "7.5.0"

.field public static final SUPPORT_CAMPAIGNS_ACTION_EXECUTOR_SERIAL_VERSION_UID:J = 0x3f0f957cfad4dc4aL


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
