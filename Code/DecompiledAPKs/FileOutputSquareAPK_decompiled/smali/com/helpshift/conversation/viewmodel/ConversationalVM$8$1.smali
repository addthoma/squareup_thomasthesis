.class Lcom/helpshift/conversation/viewmodel/ConversationalVM$8$1;
.super Lcom/helpshift/common/domain/F;
.source "ConversationalVM.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/helpshift/conversation/viewmodel/ConversationalVM$8;->f()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/helpshift/conversation/viewmodel/ConversationalVM$8;


# direct methods
.method constructor <init>(Lcom/helpshift/conversation/viewmodel/ConversationalVM$8;)V
    .locals 0

    .line 593
    iput-object p1, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM$8$1;->this$1:Lcom/helpshift/conversation/viewmodel/ConversationalVM$8;

    invoke-direct {p0}, Lcom/helpshift/common/domain/F;-><init>()V

    return-void
.end method


# virtual methods
.method public f()V
    .locals 4

    .line 597
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM$8$1;->this$1:Lcom/helpshift/conversation/viewmodel/ConversationalVM$8;

    iget-object v0, v0, Lcom/helpshift/conversation/viewmodel/ConversationalVM$8;->this$0:Lcom/helpshift/conversation/viewmodel/ConversationalVM;

    iget-object v0, v0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->viewableConversation:Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getActiveConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v0

    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM$8$1;->this$1:Lcom/helpshift/conversation/viewmodel/ConversationalVM$8;

    iget-object v1, v1, Lcom/helpshift/conversation/viewmodel/ConversationalVM$8;->val$message:Lcom/helpshift/conversation/activeconversation/message/OptionInputMessageDM;

    iget-object v2, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM$8$1;->this$1:Lcom/helpshift/conversation/viewmodel/ConversationalVM$8;

    iget-object v2, v2, Lcom/helpshift/conversation/viewmodel/ConversationalVM$8;->val$selectedOption:Lcom/helpshift/conversation/activeconversation/message/input/OptionInput$Option;

    iget-object v3, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM$8$1;->this$1:Lcom/helpshift/conversation/viewmodel/ConversationalVM$8;

    iget-boolean v3, v3, Lcom/helpshift/conversation/viewmodel/ConversationalVM$8;->val$isSkipped:Z

    invoke-virtual {v0, v1, v2, v3}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->sendOptionInputMessage(Lcom/helpshift/conversation/activeconversation/message/OptionInputMessageDM;Lcom/helpshift/conversation/activeconversation/message/input/OptionInput$Option;Z)V
    :try_end_0
    .catch Lcom/helpshift/common/exception/RootAPIException; {:try_start_0 .. :try_end_0} :catch_0

    .line 605
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM$8$1;->this$1:Lcom/helpshift/conversation/viewmodel/ConversationalVM$8;

    iget-object v0, v0, Lcom/helpshift/conversation/viewmodel/ConversationalVM$8;->this$0:Lcom/helpshift/conversation/viewmodel/ConversationalVM;

    iget-object v0, v0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->viewableConversation:Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getActiveConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v0

    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isIssueInProgress()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 606
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM$8$1;->this$1:Lcom/helpshift/conversation/viewmodel/ConversationalVM$8;

    iget-object v0, v0, Lcom/helpshift/conversation/viewmodel/ConversationalVM$8;->this$0:Lcom/helpshift/conversation/viewmodel/ConversationalVM;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->showFakeTypingIndicator(Z)V

    :cond_0
    return-void

    :catch_0
    move-exception v0

    .line 600
    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM$8$1;->this$1:Lcom/helpshift/conversation/viewmodel/ConversationalVM$8;

    iget-object v1, v1, Lcom/helpshift/conversation/viewmodel/ConversationalVM$8;->this$0:Lcom/helpshift/conversation/viewmodel/ConversationalVM;

    invoke-static {v1, v0}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->access$000(Lcom/helpshift/conversation/viewmodel/ConversationalVM;Lcom/helpshift/common/exception/RootAPIException;)V

    .line 601
    throw v0
.end method
