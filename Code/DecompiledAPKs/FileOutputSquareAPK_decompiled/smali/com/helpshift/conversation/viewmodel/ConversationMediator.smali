.class Lcom/helpshift/conversation/viewmodel/ConversationMediator;
.super Ljava/lang/Object;
.source "ConversationMediator.java"

# interfaces
.implements Lcom/helpshift/widget/WidgetMediator;


# instance fields
.field attachImageButton:Lcom/helpshift/widget/ButtonWidget;

.field confirmationBoxWidget:Lcom/helpshift/widget/ButtonWidget;

.field conversationFooterWidget:Lcom/helpshift/widget/ConversationFooterWidget;

.field private domain:Lcom/helpshift/common/domain/Domain;

.field historyLoadingWidget:Lcom/helpshift/widget/HistoryLoadingWidget;

.field protected isConversationRejected:Z

.field renderer:Lcom/helpshift/conversation/activeconversation/ConversationRenderer;

.field replyBoxWidget:Lcom/helpshift/widget/ButtonWidget;

.field replyButtonWidget:Lcom/helpshift/widget/ButtonWidget;

.field replyFieldWidget:Lcom/helpshift/widget/ReplyFieldWidget;

.field scrollJumperWidget:Lcom/helpshift/widget/ScrollJumperWidget;


# direct methods
.method constructor <init>(Lcom/helpshift/common/domain/Domain;)V
    .locals 0

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->domain:Lcom/helpshift/common/domain/Domain;

    return-void
.end method

.method static synthetic access$000(Lcom/helpshift/conversation/viewmodel/ConversationMediator;)V
    .locals 0

    .line 17
    invoke-direct {p0}, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->renderScrollJumperWidget()V

    return-void
.end method

.method static synthetic access$100(Lcom/helpshift/conversation/viewmodel/ConversationMediator;)V
    .locals 0

    .line 17
    invoke-direct {p0}, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->renderHistoryLoadingWidget()V

    return-void
.end method

.method private renderHistoryLoadingWidget()V
    .locals 2

    .line 86
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->renderer:Lcom/helpshift/conversation/activeconversation/ConversationRenderer;

    if-eqz v0, :cond_0

    .line 87
    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->historyLoadingWidget:Lcom/helpshift/widget/HistoryLoadingWidget;

    invoke-virtual {v1}, Lcom/helpshift/widget/HistoryLoadingWidget;->getState()Lcom/helpshift/conversation/activeconversation/message/HistoryLoadingState;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/helpshift/conversation/activeconversation/ConversationRenderer;->updateHistoryLoadingState(Lcom/helpshift/conversation/activeconversation/message/HistoryLoadingState;)V

    :cond_0
    return-void
.end method

.method private renderScrollJumperWidget()V
    .locals 2

    .line 74
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->renderer:Lcom/helpshift/conversation/activeconversation/ConversationRenderer;

    if-eqz v0, :cond_1

    .line 75
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->scrollJumperWidget:Lcom/helpshift/widget/ScrollJumperWidget;

    invoke-virtual {v0}, Lcom/helpshift/widget/ScrollJumperWidget;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->scrollJumperWidget:Lcom/helpshift/widget/ScrollJumperWidget;

    invoke-virtual {v0}, Lcom/helpshift/widget/ScrollJumperWidget;->shouldShowUnreadMessagesIndicator()Z

    move-result v0

    .line 77
    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->renderer:Lcom/helpshift/conversation/activeconversation/ConversationRenderer;

    invoke-interface {v1, v0}, Lcom/helpshift/conversation/activeconversation/ConversationRenderer;->showScrollJumperView(Z)V

    goto :goto_0

    .line 80
    :cond_0
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->renderer:Lcom/helpshift/conversation/activeconversation/ConversationRenderer;

    invoke-interface {v0}, Lcom/helpshift/conversation/activeconversation/ConversationRenderer;->hideScrollJumperView()V

    :cond_1
    :goto_0
    return-void
.end method


# virtual methods
.method confirmationBox()V
    .locals 2

    .line 209
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->replyBoxWidget:Lcom/helpshift/widget/ButtonWidget;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/helpshift/widget/ButtonWidget;->setVisible(Z)V

    .line 210
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->confirmationBoxWidget:Lcom/helpshift/widget/ButtonWidget;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/helpshift/widget/ButtonWidget;->setVisible(Z)V

    .line 211
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->conversationFooterWidget:Lcom/helpshift/widget/ConversationFooterWidget;

    sget-object v1, Lcom/helpshift/conversation/activeconversation/message/ConversationFooterState;->CONVERSATION_ENDED_MESSAGE:Lcom/helpshift/conversation/activeconversation/message/ConversationFooterState;

    invoke-virtual {v0, v1}, Lcom/helpshift/widget/ConversationFooterWidget;->setState(Lcom/helpshift/conversation/activeconversation/message/ConversationFooterState;)V

    return-void
.end method

.method hideAllFooterWidgets()V
    .locals 2

    .line 215
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->replyBoxWidget:Lcom/helpshift/widget/ButtonWidget;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/helpshift/widget/ButtonWidget;->setVisible(Z)V

    .line 216
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->confirmationBoxWidget:Lcom/helpshift/widget/ButtonWidget;

    invoke-virtual {v0, v1}, Lcom/helpshift/widget/ButtonWidget;->setVisible(Z)V

    .line 217
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->conversationFooterWidget:Lcom/helpshift/widget/ConversationFooterWidget;

    sget-object v1, Lcom/helpshift/conversation/activeconversation/message/ConversationFooterState;->NONE:Lcom/helpshift/conversation/activeconversation/message/ConversationFooterState;

    invoke-virtual {v0, v1}, Lcom/helpshift/widget/ConversationFooterWidget;->setState(Lcom/helpshift/conversation/activeconversation/message/ConversationFooterState;)V

    return-void
.end method

.method public onChanged(Lcom/helpshift/widget/Widget;)V
    .locals 2

    .line 41
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v1, Lcom/helpshift/conversation/viewmodel/ConversationMediator$1;

    invoke-direct {v1, p0, p1}, Lcom/helpshift/conversation/viewmodel/ConversationMediator$1;-><init>(Lcom/helpshift/conversation/viewmodel/ConversationMediator;Lcom/helpshift/widget/Widget;)V

    invoke-virtual {v0, v1}, Lcom/helpshift/common/domain/Domain;->runOnUI(Lcom/helpshift/common/domain/F;)V

    return-void
.end method

.method renderAll()V
    .locals 0

    .line 92
    invoke-virtual {p0}, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->renderAttachImageButton()V

    .line 93
    invoke-virtual {p0}, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->renderReplyButton()V

    .line 94
    invoke-virtual {p0}, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->renderReplyBoxWidget()V

    .line 95
    invoke-direct {p0}, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->renderScrollJumperWidget()V

    .line 96
    invoke-virtual {p0}, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->renderConfirmationBoxWidget()V

    .line 97
    invoke-virtual {p0}, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->renderConversationFooterWidget()V

    .line 98
    invoke-direct {p0}, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->renderHistoryLoadingWidget()V

    return-void
.end method

.method renderAttachImageButton()V
    .locals 1

    .line 142
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->renderer:Lcom/helpshift/conversation/activeconversation/ConversationRenderer;

    if-eqz v0, :cond_1

    .line 143
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->attachImageButton:Lcom/helpshift/widget/ButtonWidget;

    invoke-virtual {v0}, Lcom/helpshift/widget/ButtonWidget;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->replyBoxWidget:Lcom/helpshift/widget/ButtonWidget;

    .line 144
    invoke-virtual {v0}, Lcom/helpshift/widget/ButtonWidget;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->isConversationRejected:Z

    if-nez v0, :cond_0

    .line 146
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->renderer:Lcom/helpshift/conversation/activeconversation/ConversationRenderer;

    invoke-interface {v0}, Lcom/helpshift/conversation/activeconversation/ConversationRenderer;->showImageAttachmentButton()V

    goto :goto_0

    .line 149
    :cond_0
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->renderer:Lcom/helpshift/conversation/activeconversation/ConversationRenderer;

    invoke-interface {v0}, Lcom/helpshift/conversation/activeconversation/ConversationRenderer;->hideImageAttachmentButton()V

    :cond_1
    :goto_0
    return-void
.end method

.method renderConfirmationBoxWidget()V
    .locals 1

    .line 186
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->renderer:Lcom/helpshift/conversation/activeconversation/ConversationRenderer;

    if-eqz v0, :cond_1

    .line 187
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->confirmationBoxWidget:Lcom/helpshift/widget/ButtonWidget;

    invoke-virtual {v0}, Lcom/helpshift/widget/ButtonWidget;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 188
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->renderer:Lcom/helpshift/conversation/activeconversation/ConversationRenderer;

    invoke-interface {v0}, Lcom/helpshift/conversation/activeconversation/ConversationRenderer;->showConversationResolutionQuestionUI()V

    goto :goto_0

    .line 191
    :cond_0
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->renderer:Lcom/helpshift/conversation/activeconversation/ConversationRenderer;

    invoke-interface {v0}, Lcom/helpshift/conversation/activeconversation/ConversationRenderer;->hideConversationResolutionQuestionUI()V

    :cond_1
    :goto_0
    return-void
.end method

.method renderConversationFooterWidget()V
    .locals 2

    .line 197
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->renderer:Lcom/helpshift/conversation/activeconversation/ConversationRenderer;

    if-eqz v0, :cond_0

    .line 198
    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->conversationFooterWidget:Lcom/helpshift/widget/ConversationFooterWidget;

    invoke-virtual {v1}, Lcom/helpshift/widget/ConversationFooterWidget;->getState()Lcom/helpshift/conversation/activeconversation/message/ConversationFooterState;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/helpshift/conversation/activeconversation/ConversationRenderer;->updateConversationFooterState(Lcom/helpshift/conversation/activeconversation/message/ConversationFooterState;)V

    :cond_0
    return-void
.end method

.method protected renderReplyBoxWidget()V
    .locals 1

    .line 175
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->renderer:Lcom/helpshift/conversation/activeconversation/ConversationRenderer;

    if-eqz v0, :cond_1

    .line 176
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->replyBoxWidget:Lcom/helpshift/widget/ButtonWidget;

    invoke-virtual {v0}, Lcom/helpshift/widget/ButtonWidget;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 177
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->renderer:Lcom/helpshift/conversation/activeconversation/ConversationRenderer;

    invoke-interface {v0}, Lcom/helpshift/conversation/activeconversation/ConversationRenderer;->showSendReplyUI()V

    goto :goto_0

    .line 180
    :cond_0
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->renderer:Lcom/helpshift/conversation/activeconversation/ConversationRenderer;

    invoke-interface {v0}, Lcom/helpshift/conversation/activeconversation/ConversationRenderer;->hideSendReplyUI()V

    :cond_1
    :goto_0
    return-void
.end method

.method renderReplyButton()V
    .locals 2

    .line 155
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->renderer:Lcom/helpshift/conversation/activeconversation/ConversationRenderer;

    invoke-interface {v0}, Lcom/helpshift/conversation/activeconversation/ConversationRenderer;->getReply()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 156
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->replyButtonWidget:Lcom/helpshift/widget/ButtonWidget;

    invoke-virtual {v0}, Lcom/helpshift/widget/ButtonWidget;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 157
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->replyButtonWidget:Lcom/helpshift/widget/ButtonWidget;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/helpshift/widget/ButtonWidget;->setEnabled(Z)V

    goto :goto_0

    .line 160
    :cond_0
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->replyButtonWidget:Lcom/helpshift/widget/ButtonWidget;

    invoke-virtual {v0}, Lcom/helpshift/widget/ButtonWidget;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 161
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->replyButtonWidget:Lcom/helpshift/widget/ButtonWidget;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/helpshift/widget/ButtonWidget;->setEnabled(Z)V

    .line 164
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->renderer:Lcom/helpshift/conversation/activeconversation/ConversationRenderer;

    if-eqz v0, :cond_3

    .line 165
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->replyButtonWidget:Lcom/helpshift/widget/ButtonWidget;

    invoke-virtual {v0}, Lcom/helpshift/widget/ButtonWidget;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 166
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->renderer:Lcom/helpshift/conversation/activeconversation/ConversationRenderer;

    invoke-interface {v0}, Lcom/helpshift/conversation/activeconversation/ConversationRenderer;->enableSendReplyButton()V

    goto :goto_1

    .line 169
    :cond_2
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->renderer:Lcom/helpshift/conversation/activeconversation/ConversationRenderer;

    invoke-interface {v0}, Lcom/helpshift/conversation/activeconversation/ConversationRenderer;->disableSendReplyButton()V

    :cond_3
    :goto_1
    return-void
.end method

.method setAttachImageButton(Lcom/helpshift/widget/ButtonWidget;)V
    .locals 0

    .line 102
    invoke-virtual {p1, p0}, Lcom/helpshift/widget/ButtonWidget;->setMediator(Lcom/helpshift/widget/WidgetMediator;)V

    .line 103
    iput-object p1, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->attachImageButton:Lcom/helpshift/widget/ButtonWidget;

    return-void
.end method

.method setConfirmationBoxWidget(Lcom/helpshift/widget/ButtonWidget;)V
    .locals 0

    .line 127
    invoke-virtual {p1, p0}, Lcom/helpshift/widget/ButtonWidget;->setMediator(Lcom/helpshift/widget/WidgetMediator;)V

    .line 128
    iput-object p1, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->confirmationBoxWidget:Lcom/helpshift/widget/ButtonWidget;

    return-void
.end method

.method setConversationFooterWidget(Lcom/helpshift/widget/ConversationFooterWidget;)V
    .locals 0

    .line 132
    invoke-virtual {p1, p0}, Lcom/helpshift/widget/ConversationFooterWidget;->setMediator(Lcom/helpshift/widget/WidgetMediator;)V

    .line 133
    iput-object p1, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->conversationFooterWidget:Lcom/helpshift/widget/ConversationFooterWidget;

    return-void
.end method

.method setConversationRejected(Z)V
    .locals 1

    .line 227
    iput-boolean p1, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->isConversationRejected:Z

    .line 228
    iget-object p1, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v0, Lcom/helpshift/conversation/viewmodel/ConversationMediator$2;

    invoke-direct {v0, p0}, Lcom/helpshift/conversation/viewmodel/ConversationMediator$2;-><init>(Lcom/helpshift/conversation/viewmodel/ConversationMediator;)V

    invoke-virtual {p1, v0}, Lcom/helpshift/common/domain/Domain;->runOnUI(Lcom/helpshift/common/domain/F;)V

    return-void
.end method

.method setHistoryLoadingWidget(Lcom/helpshift/widget/HistoryLoadingWidget;)V
    .locals 0

    .line 137
    invoke-virtual {p1, p0}, Lcom/helpshift/widget/HistoryLoadingWidget;->setMediator(Lcom/helpshift/widget/WidgetMediator;)V

    .line 138
    iput-object p1, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->historyLoadingWidget:Lcom/helpshift/widget/HistoryLoadingWidget;

    return-void
.end method

.method setHistoryLoadingWidgetState(Lcom/helpshift/conversation/activeconversation/message/HistoryLoadingState;)V
    .locals 1

    .line 237
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->historyLoadingWidget:Lcom/helpshift/widget/HistoryLoadingWidget;

    invoke-virtual {v0, p1}, Lcom/helpshift/widget/HistoryLoadingWidget;->setState(Lcom/helpshift/conversation/activeconversation/message/HistoryLoadingState;)V

    return-void
.end method

.method setRenderer(Lcom/helpshift/conversation/activeconversation/ConversationRenderer;)V
    .locals 0

    .line 36
    iput-object p1, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->renderer:Lcom/helpshift/conversation/activeconversation/ConversationRenderer;

    return-void
.end method

.method setReplyBoxWidget(Lcom/helpshift/widget/ButtonWidget;)V
    .locals 0

    .line 117
    invoke-virtual {p1, p0}, Lcom/helpshift/widget/ButtonWidget;->setMediator(Lcom/helpshift/widget/WidgetMediator;)V

    .line 118
    iput-object p1, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->replyBoxWidget:Lcom/helpshift/widget/ButtonWidget;

    return-void
.end method

.method setReplyButtonWidget(Lcom/helpshift/widget/ButtonWidget;)V
    .locals 0

    .line 107
    invoke-virtual {p1, p0}, Lcom/helpshift/widget/ButtonWidget;->setMediator(Lcom/helpshift/widget/WidgetMediator;)V

    .line 108
    iput-object p1, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->replyButtonWidget:Lcom/helpshift/widget/ButtonWidget;

    return-void
.end method

.method setReplyFieldWidget(Lcom/helpshift/widget/ReplyFieldWidget;)V
    .locals 0

    .line 112
    invoke-virtual {p1, p0}, Lcom/helpshift/widget/ReplyFieldWidget;->setMediator(Lcom/helpshift/widget/WidgetMediator;)V

    .line 113
    iput-object p1, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->replyFieldWidget:Lcom/helpshift/widget/ReplyFieldWidget;

    return-void
.end method

.method setScrollJumperWidget(Lcom/helpshift/widget/ScrollJumperWidget;)V
    .locals 0

    .line 122
    invoke-virtual {p1, p0}, Lcom/helpshift/widget/ScrollJumperWidget;->setMediator(Lcom/helpshift/widget/WidgetMediator;)V

    .line 123
    iput-object p1, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->scrollJumperWidget:Lcom/helpshift/widget/ScrollJumperWidget;

    return-void
.end method

.method showMessageBox()V
    .locals 2

    .line 203
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->replyBoxWidget:Lcom/helpshift/widget/ButtonWidget;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/helpshift/widget/ButtonWidget;->setVisible(Z)V

    .line 204
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->confirmationBoxWidget:Lcom/helpshift/widget/ButtonWidget;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/helpshift/widget/ButtonWidget;->setVisible(Z)V

    .line 205
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->conversationFooterWidget:Lcom/helpshift/widget/ConversationFooterWidget;

    sget-object v1, Lcom/helpshift/conversation/activeconversation/message/ConversationFooterState;->NONE:Lcom/helpshift/conversation/activeconversation/message/ConversationFooterState;

    invoke-virtual {v0, v1}, Lcom/helpshift/widget/ConversationFooterWidget;->setState(Lcom/helpshift/conversation/activeconversation/message/ConversationFooterState;)V

    return-void
.end method

.method showStartNewConversation(Lcom/helpshift/conversation/activeconversation/message/ConversationFooterState;)V
    .locals 2

    .line 221
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->replyBoxWidget:Lcom/helpshift/widget/ButtonWidget;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/helpshift/widget/ButtonWidget;->setVisible(Z)V

    .line 222
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->confirmationBoxWidget:Lcom/helpshift/widget/ButtonWidget;

    invoke-virtual {v0, v1}, Lcom/helpshift/widget/ButtonWidget;->setVisible(Z)V

    .line 223
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationMediator;->conversationFooterWidget:Lcom/helpshift/widget/ConversationFooterWidget;

    invoke-virtual {v0, p1}, Lcom/helpshift/widget/ConversationFooterWidget;->setState(Lcom/helpshift/conversation/activeconversation/message/ConversationFooterState;)V

    return-void
.end method
