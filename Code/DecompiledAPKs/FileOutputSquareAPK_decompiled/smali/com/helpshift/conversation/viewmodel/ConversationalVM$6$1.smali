.class Lcom/helpshift/conversation/viewmodel/ConversationalVM$6$1;
.super Lcom/helpshift/common/domain/F;
.source "ConversationalVM.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/helpshift/conversation/viewmodel/ConversationalVM$6;->f()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/helpshift/conversation/viewmodel/ConversationalVM$6;


# direct methods
.method constructor <init>(Lcom/helpshift/conversation/viewmodel/ConversationalVM$6;)V
    .locals 0

    .line 458
    iput-object p1, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM$6$1;->this$1:Lcom/helpshift/conversation/viewmodel/ConversationalVM$6;

    invoke-direct {p0}, Lcom/helpshift/common/domain/F;-><init>()V

    return-void
.end method


# virtual methods
.method public f()V
    .locals 2

    .line 461
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM$6$1;->this$1:Lcom/helpshift/conversation/viewmodel/ConversationalVM$6;

    iget-object v0, v0, Lcom/helpshift/conversation/viewmodel/ConversationalVM$6;->this$0:Lcom/helpshift/conversation/viewmodel/ConversationalVM;

    iget-object v0, v0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->viewableConversation:Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getActiveConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v0

    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM$6$1;->this$1:Lcom/helpshift/conversation/viewmodel/ConversationalVM$6;

    iget-object v1, v1, Lcom/helpshift/conversation/viewmodel/ConversationalVM$6;->val$readableMessage:Lcom/helpshift/conversation/activeconversation/message/MessageDM;

    invoke-virtual {v0, v1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->retryMessage(Lcom/helpshift/conversation/activeconversation/message/MessageDM;)V

    .line 462
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM$6$1;->this$1:Lcom/helpshift/conversation/viewmodel/ConversationalVM$6;

    iget-object v0, v0, Lcom/helpshift/conversation/viewmodel/ConversationalVM$6;->this$0:Lcom/helpshift/conversation/viewmodel/ConversationalVM;

    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationalVM$6$1;->this$1:Lcom/helpshift/conversation/viewmodel/ConversationalVM$6;

    iget-object v1, v1, Lcom/helpshift/conversation/viewmodel/ConversationalVM$6;->this$0:Lcom/helpshift/conversation/viewmodel/ConversationalVM;

    iget-boolean v1, v1, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->isInBetweenBotExecution:Z

    invoke-virtual {v0, v1}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->showFakeTypingIndicator(Z)V

    return-void
.end method
