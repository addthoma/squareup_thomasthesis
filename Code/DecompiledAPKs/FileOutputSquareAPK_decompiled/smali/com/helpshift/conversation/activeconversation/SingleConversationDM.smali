.class public Lcom/helpshift/conversation/activeconversation/SingleConversationDM;
.super Lcom/helpshift/conversation/activeconversation/ViewableConversation;
.source "SingleConversationDM.java"


# instance fields
.field private conversationDM:Lcom/helpshift/conversation/activeconversation/ConversationDM;


# direct methods
.method public constructor <init>(Lcom/helpshift/common/platform/Platform;Lcom/helpshift/common/domain/Domain;Lcom/helpshift/account/domainmodel/UserDM;Lcom/helpshift/conversation/loaders/SingleConversationLoader;)V
    .locals 0

    .line 24
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;-><init>(Lcom/helpshift/common/platform/Platform;Lcom/helpshift/common/domain/Domain;Lcom/helpshift/account/domainmodel/UserDM;Lcom/helpshift/conversation/loaders/ConversationsLoader;)V

    return-void
.end method


# virtual methods
.method public getActiveConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;
    .locals 1

    .line 46
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/SingleConversationDM;->conversationDM:Lcom/helpshift/conversation/activeconversation/ConversationDM;

    return-object v0
.end method

.method public getAllConversations()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/helpshift/conversation/activeconversation/ConversationDM;",
            ">;"
        }
    .end annotation

    .line 72
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/SingleConversationDM;->conversationDM:Lcom/helpshift/conversation/activeconversation/ConversationDM;

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getIdentifier()Ljava/lang/Long;
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/SingleConversationDM;->conversationDM:Lcom/helpshift/conversation/activeconversation/ConversationDM;

    iget-object v0, v0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localId:Ljava/lang/Long;

    return-object v0
.end method

.method public getPaginationCursor()Lcom/helpshift/conversation/activeconversation/PaginationCursor;
    .locals 1

    .line 93
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/SingleConversationDM;->conversationDM:Lcom/helpshift/conversation/activeconversation/ConversationDM;

    invoke-virtual {p0, v0}, Lcom/helpshift/conversation/activeconversation/SingleConversationDM;->buildPaginationCursor(Lcom/helpshift/conversation/activeconversation/ConversationDM;)Lcom/helpshift/conversation/activeconversation/PaginationCursor;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized init()V
    .locals 4

    monitor-enter p0

    .line 29
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/SingleConversationDM;->conversationLoader:Lcom/helpshift/conversation/loaders/ConversationsLoader;

    invoke-virtual {v0}, Lcom/helpshift/conversation/loaders/ConversationsLoader;->fetchInitialConversations()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    .line 30
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/helpshift/conversation/activeconversation/ConversationDM;

    iput-object v0, p0, Lcom/helpshift/conversation/activeconversation/SingleConversationDM;->conversationDM:Lcom/helpshift/conversation/activeconversation/ConversationDM;

    .line 31
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/SingleConversationDM;->conversationDM:Lcom/helpshift/conversation/activeconversation/ConversationDM;

    iget-object v1, p0, Lcom/helpshift/conversation/activeconversation/SingleConversationDM;->platform:Lcom/helpshift/common/platform/Platform;

    iget-object v2, p0, Lcom/helpshift/conversation/activeconversation/SingleConversationDM;->domain:Lcom/helpshift/common/domain/Domain;

    iget-object v3, p0, Lcom/helpshift/conversation/activeconversation/SingleConversationDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-virtual {v0, v1, v2, v3}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->setDependencies(Lcom/helpshift/common/platform/Platform;Lcom/helpshift/common/domain/Domain;Lcom/helpshift/account/domainmodel/UserDM;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 32
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public initializeConversationsForUI()V
    .locals 2

    .line 51
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/SingleConversationDM;->conversationDM:Lcom/helpshift/conversation/activeconversation/ConversationDM;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->initializeMessagesForUI(Z)V

    return-void
.end method

.method public onNewConversationStarted(Lcom/helpshift/conversation/activeconversation/ConversationDM;)V
    .locals 0

    return-void
.end method

.method public prependConversations(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/helpshift/conversation/activeconversation/ConversationDM;",
            ">;)V"
        }
    .end annotation

    .line 82
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    .line 84
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/helpshift/conversation/activeconversation/ConversationDM;

    .line 85
    iget-object v3, p0, Lcom/helpshift/conversation/activeconversation/SingleConversationDM;->conversationDM:Lcom/helpshift/conversation/activeconversation/ConversationDM;

    iget-object v3, v3, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localId:Ljava/lang/Long;

    iget-object v4, v2, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localId:Ljava/lang/Long;

    invoke-virtual {v3, v4}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 86
    iget-object v3, p0, Lcom/helpshift/conversation/activeconversation/SingleConversationDM;->conversationDM:Lcom/helpshift/conversation/activeconversation/ConversationDM;

    iget-object v3, v3, Lcom/helpshift/conversation/activeconversation/ConversationDM;->messageDMs:Lcom/helpshift/common/util/HSObservableList;

    iget-object v2, v2, Lcom/helpshift/conversation/activeconversation/ConversationDM;->messageDMs:Lcom/helpshift/common/util/HSObservableList;

    invoke-virtual {v3, v2}, Lcom/helpshift/common/util/HSObservableList;->prependItems(Ljava/util/Collection;)V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public registerMessagesObserver(Lcom/helpshift/common/util/HSListObserver;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/helpshift/common/util/HSListObserver<",
            "Lcom/helpshift/conversation/activeconversation/message/MessageDM;",
            ">;)V"
        }
    .end annotation

    .line 56
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/SingleConversationDM;->conversationDM:Lcom/helpshift/conversation/activeconversation/ConversationDM;

    iget-object v0, v0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->messageDMs:Lcom/helpshift/common/util/HSObservableList;

    invoke-virtual {v0, p1}, Lcom/helpshift/common/util/HSObservableList;->setObserver(Lcom/helpshift/common/util/HSListObserver;)V

    .line 57
    iget-object p1, p0, Lcom/helpshift/conversation/activeconversation/SingleConversationDM;->conversationDM:Lcom/helpshift/conversation/activeconversation/ConversationDM;

    invoke-virtual {p1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->registerMessagesObserver()V

    return-void
.end method

.method public shouldOpen()Z
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/SingleConversationDM;->conversationDM:Lcom/helpshift/conversation/activeconversation/ConversationDM;

    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->shouldOpen()Z

    move-result v0

    return v0
.end method
