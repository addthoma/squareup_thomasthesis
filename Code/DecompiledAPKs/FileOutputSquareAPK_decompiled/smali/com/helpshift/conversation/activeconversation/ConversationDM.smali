.class public Lcom/helpshift/conversation/activeconversation/ConversationDM;
.super Ljava/lang/Object;
.source "ConversationDM.java"

# interfaces
.implements Ljava/util/Observer;
.implements Lcom/helpshift/conversation/activeconversation/ConversationServerInfo;


# static fields
.field private static final TAG:Ljava/lang/String; = "Helpshift_ConvDM"


# instance fields
.field private conversationDAO:Lcom/helpshift/conversation/dao/ConversationDAO;

.field private conversationDMListener:Lcom/helpshift/conversation/activeconversation/ConversationDMListener;

.field private createdAt:Ljava/lang/String;

.field public createdRequestId:Ljava/lang/String;

.field public csatFeedback:Ljava/lang/String;

.field public csatRating:I

.field public csatState:Lcom/helpshift/conversation/states/ConversationCSATState;

.field domain:Lcom/helpshift/common/domain/Domain;

.field private enableMessageClickOnResolutionRejected:Z

.field private epochCreatedAtTime:J

.field public isConversationEndedDelegateSent:Z

.field private isInBetweenBotExecution:Z

.field public isRedacted:Z

.field public isStartNewConversationClicked:Z

.field public issueType:Ljava/lang/String;

.field public lastUserActivityTime:J

.field public localId:Ljava/lang/Long;

.field public localUUID:Ljava/lang/String;

.field public messageCursor:Ljava/lang/String;

.field public messageDMs:Lcom/helpshift/common/util/HSObservableList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/helpshift/common/util/HSObservableList<",
            "Lcom/helpshift/conversation/activeconversation/message/MessageDM;",
            ">;"
        }
    .end annotation
.end field

.field private metaDataDM:Lcom/helpshift/meta/MetaDataDM;

.field platform:Lcom/helpshift/common/platform/Platform;

.field public preConversationServerId:Ljava/lang/String;

.field public publishId:Ljava/lang/String;

.field private sdkConfigurationDM:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

.field public serverId:Ljava/lang/String;

.field public shouldIncrementMessageCount:Z

.field public showAgentName:Z

.field public state:Lcom/helpshift/conversation/dto/IssueState;

.field public title:Ljava/lang/String;

.field private final unansweredRequestForReopenMessageDMs:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/helpshift/conversation/activeconversation/message/RequestForReopenMessageDM;",
            ">;"
        }
    .end annotation
.end field

.field public updatedAt:Ljava/lang/String;

.field userDM:Lcom/helpshift/account/domainmodel/UserDM;

.field public userLocalId:J

.field public wasFullPrivacyEnabledAtCreation:Z


# direct methods
.method public constructor <init>(Lcom/helpshift/common/platform/Platform;Lcom/helpshift/common/domain/Domain;Lcom/helpshift/account/domainmodel/UserDM;)V
    .locals 1

    .line 151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->unansweredRequestForReopenMessageDMs:Ljava/util/Map;

    .line 110
    new-instance v0, Lcom/helpshift/common/util/HSObservableList;

    invoke-direct {v0}, Lcom/helpshift/common/util/HSObservableList;-><init>()V

    iput-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->messageDMs:Lcom/helpshift/common/util/HSObservableList;

    .line 116
    sget-object v0, Lcom/helpshift/conversation/states/ConversationCSATState;->NONE:Lcom/helpshift/conversation/states/ConversationCSATState;

    iput-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->csatState:Lcom/helpshift/conversation/states/ConversationCSATState;

    .line 152
    invoke-virtual {p0, p1, p2, p3}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->setDependencies(Lcom/helpshift/common/platform/Platform;Lcom/helpshift/common/domain/Domain;Lcom/helpshift/account/domainmodel/UserDM;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/helpshift/conversation/dto/IssueState;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1

    .line 140
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->unansweredRequestForReopenMessageDMs:Ljava/util/Map;

    .line 110
    new-instance v0, Lcom/helpshift/common/util/HSObservableList;

    invoke-direct {v0}, Lcom/helpshift/common/util/HSObservableList;-><init>()V

    iput-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->messageDMs:Lcom/helpshift/common/util/HSObservableList;

    .line 116
    sget-object v0, Lcom/helpshift/conversation/states/ConversationCSATState;->NONE:Lcom/helpshift/conversation/states/ConversationCSATState;

    iput-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->csatState:Lcom/helpshift/conversation/states/ConversationCSATState;

    .line 141
    iput-object p1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->title:Ljava/lang/String;

    .line 142
    iput-object p3, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->createdAt:Ljava/lang/String;

    .line 143
    iput-wide p4, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->epochCreatedAtTime:J

    .line 144
    iput-object p6, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->updatedAt:Ljava/lang/String;

    .line 145
    iput-object p7, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->publishId:Ljava/lang/String;

    .line 146
    iput-object p8, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->messageCursor:Ljava/lang/String;

    .line 147
    iput-boolean p9, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->showAgentName:Z

    .line 148
    iput-object p2, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    return-void
.end method

.method static synthetic access$000(Lcom/helpshift/conversation/activeconversation/ConversationDM;)V
    .locals 0

    .line 95
    invoke-direct {p0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->deleteOptionsForAdminMessageWithOptionsInput()V

    return-void
.end method

.method static synthetic access$100(Lcom/helpshift/conversation/activeconversation/ConversationDM;)V
    .locals 0

    .line 95
    invoke-direct {p0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->sendConversationEndedDelegate()V

    return-void
.end method

.method private addMessageToDBAndGlobalList(Lcom/helpshift/conversation/activeconversation/message/MessageDM;)V
    .locals 2

    .line 779
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->conversationDAO:Lcom/helpshift/conversation/dao/ConversationDAO;

    invoke-interface {v0, p1}, Lcom/helpshift/conversation/dao/ConversationDAO;->insertOrUpdateMessage(Lcom/helpshift/conversation/activeconversation/message/MessageDM;)V

    .line 780
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->domain:Lcom/helpshift/common/domain/Domain;

    iget-object v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-virtual {p1, v0, v1}, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->setDependencies(Lcom/helpshift/common/domain/Domain;Lcom/helpshift/common/platform/Platform;)V

    .line 781
    invoke-virtual {p1, p0}, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->addObserver(Ljava/util/Observer;)V

    .line 782
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->messageDMs:Lcom/helpshift/common/util/HSObservableList;

    invoke-virtual {v0, p1}, Lcom/helpshift/common/util/HSObservableList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private addMessageToDbAndUI(Lcom/helpshift/conversation/activeconversation/message/MessageDM;)V
    .locals 1

    .line 786
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->conversationDAO:Lcom/helpshift/conversation/dao/ConversationDAO;

    invoke-interface {v0, p1}, Lcom/helpshift/conversation/dao/ConversationDAO;->insertOrUpdateMessage(Lcom/helpshift/conversation/activeconversation/message/MessageDM;)V

    .line 787
    invoke-virtual {p0, p1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->addMessageToUI(Lcom/helpshift/conversation/activeconversation/message/MessageDM;)V

    return-void
.end method

.method private deleteOptionsForAdminMessageWithOptionsInput()V
    .locals 4

    .line 403
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->conversationDAO:Lcom/helpshift/conversation/dao/ConversationDAO;

    iget-object v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localId:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    sget-object v3, Lcom/helpshift/conversation/activeconversation/message/MessageType;->ADMIN_TEXT_WITH_OPTION_INPUT:Lcom/helpshift/conversation/activeconversation/message/MessageType;

    invoke-interface {v0, v1, v2, v3}, Lcom/helpshift/conversation/dao/ConversationDAO;->readMessages(JLcom/helpshift/conversation/activeconversation/message/MessageType;)Ljava/util/List;

    move-result-object v0

    .line 404
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/helpshift/conversation/activeconversation/message/MessageDM;

    .line 405
    check-cast v2, Lcom/helpshift/conversation/activeconversation/message/AdminMessageWithOptionInputDM;

    iget-object v2, v2, Lcom/helpshift/conversation/activeconversation/message/AdminMessageWithOptionInputDM;->input:Lcom/helpshift/conversation/activeconversation/message/input/OptionInput;

    iget-object v2, v2, Lcom/helpshift/conversation/activeconversation/message/input/OptionInput;->options:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    goto :goto_0

    .line 407
    :cond_0
    iget-object v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->conversationDAO:Lcom/helpshift/conversation/dao/ConversationDAO;

    invoke-interface {v1, v0}, Lcom/helpshift/conversation/dao/ConversationDAO;->insertOrUpdateMessages(Ljava/util/List;)V

    return-void
.end method

.method private deleteOptionsForAdminMessageWithOptionsInput(Lcom/helpshift/conversation/activeconversation/message/OptionInputMessageDM;)V
    .locals 2

    .line 221
    iget-object v0, p1, Lcom/helpshift/conversation/activeconversation/message/OptionInputMessageDM;->referredMessageType:Lcom/helpshift/conversation/activeconversation/message/MessageType;

    sget-object v1, Lcom/helpshift/conversation/activeconversation/message/MessageType;->ADMIN_TEXT_WITH_OPTION_INPUT:Lcom/helpshift/conversation/activeconversation/message/MessageType;

    if-ne v0, v1, :cond_0

    .line 222
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->conversationDAO:Lcom/helpshift/conversation/dao/ConversationDAO;

    iget-object p1, p1, Lcom/helpshift/conversation/activeconversation/message/OptionInputMessageDM;->serverId:Ljava/lang/String;

    .line 223
    invoke-interface {v0, p1}, Lcom/helpshift/conversation/dao/ConversationDAO;->readMessage(Ljava/lang/String;)Lcom/helpshift/conversation/activeconversation/message/MessageDM;

    move-result-object p1

    check-cast p1, Lcom/helpshift/conversation/activeconversation/message/AdminMessageWithOptionInputDM;

    .line 224
    iget-object v0, p1, Lcom/helpshift/conversation/activeconversation/message/AdminMessageWithOptionInputDM;->input:Lcom/helpshift/conversation/activeconversation/message/input/OptionInput;

    iget-object v0, v0, Lcom/helpshift/conversation/activeconversation/message/input/OptionInput;->options:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 225
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->conversationDAO:Lcom/helpshift/conversation/dao/ConversationDAO;

    invoke-interface {v0, p1}, Lcom/helpshift/conversation/dao/ConversationDAO;->insertOrUpdateMessage(Lcom/helpshift/conversation/activeconversation/message/MessageDM;)V

    :cond_0
    return-void
.end method

.method private evaluateBotControlMessages(Ljava/util/Collection;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/helpshift/conversation/activeconversation/message/MessageDM;",
            ">;)V"
        }
    .end annotation

    .line 715
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/helpshift/conversation/activeconversation/message/MessageDM;

    .line 716
    iget-object v1, v0, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->messageType:Lcom/helpshift/conversation/activeconversation/message/MessageType;

    .line 718
    sget-object v2, Lcom/helpshift/conversation/activeconversation/ConversationDM$11;->$SwitchMap$com$helpshift$conversation$activeconversation$message$MessageType:[I

    invoke-virtual {v1}, Lcom/helpshift/conversation/activeconversation/message/MessageType;->ordinal()I

    move-result v1

    aget v1, v2, v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    goto :goto_0

    .line 723
    :cond_0
    iget-object v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-static {v1}, Lcom/helpshift/common/util/HSDateFormatSpec;->getCurrentAdjustedTimeForStorage(Lcom/helpshift/common/platform/Platform;)Ljava/lang/String;

    move-result-object v4

    .line 724
    invoke-static {v4}, Lcom/helpshift/common/util/HSDateFormatSpec;->convertToEpochTime(Ljava/lang/String;)J

    move-result-wide v5

    .line 725
    check-cast v0, Lcom/helpshift/conversation/activeconversation/message/UnsupportedAdminMessageWithInputDM;

    .line 727
    new-instance v1, Lcom/helpshift/conversation/activeconversation/message/UserBotControlMessageDM;

    iget-object v10, v0, Lcom/helpshift/conversation/activeconversation/message/UnsupportedAdminMessageWithInputDM;->botInfo:Ljava/lang/String;

    iget-object v11, v0, Lcom/helpshift/conversation/activeconversation/message/UnsupportedAdminMessageWithInputDM;->serverId:Ljava/lang/String;

    const/4 v12, 0x1

    const-string v3, "Unsupported bot input"

    const-string v7, "mobile"

    const-string v8, "bot_cancelled"

    const-string v9, "unsupported_bot_input"

    move-object v2, v1

    invoke-direct/range {v2 .. v12}, Lcom/helpshift/conversation/activeconversation/message/UserBotControlMessageDM;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 734
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localId:Ljava/lang/Long;

    iput-object v0, v1, Lcom/helpshift/conversation/activeconversation/message/UserBotControlMessageDM;->conversationLocalId:Ljava/lang/Long;

    .line 735
    invoke-direct {p0, v1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->addMessageToDbAndUI(Lcom/helpshift/conversation/activeconversation/message/MessageDM;)V

    .line 738
    new-instance v0, Lcom/helpshift/conversation/activeconversation/ConversationDM$3;

    invoke-direct {v0, p0, v1}, Lcom/helpshift/conversation/activeconversation/ConversationDM$3;-><init>(Lcom/helpshift/conversation/activeconversation/ConversationDM;Lcom/helpshift/conversation/activeconversation/message/UserBotControlMessageDM;)V

    invoke-direct {p0, v0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->sendMessageWithAutoRetry(Lcom/helpshift/common/domain/F;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private getLocalIdToPendingRequestIdMessageMap()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1085
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-interface {v0}, Lcom/helpshift/common/platform/Platform;->getNetworkRequestDAO()Lcom/helpshift/common/platform/network/NetworkRequestDAO;

    move-result-object v0

    invoke-direct {p0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->getRouteForSendingMessage()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/helpshift/common/platform/network/NetworkRequestDAO;->getPendingRequestIdMapForRoute(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method private getMessageDMForUpdate(Lcom/helpshift/conversation/activeconversation/message/MessageDM;Ljava/util/Map;Ljava/util/Map;Lcom/helpshift/conversation/activeconversation/ConversationUpdate;)Lcom/helpshift/conversation/activeconversation/message/MessageDM;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/helpshift/conversation/activeconversation/message/MessageDM;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/helpshift/conversation/activeconversation/message/MessageDM;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/helpshift/conversation/activeconversation/message/MessageDM;",
            ">;",
            "Lcom/helpshift/conversation/activeconversation/ConversationUpdate;",
            ")",
            "Lcom/helpshift/conversation/activeconversation/message/MessageDM;"
        }
    .end annotation

    .line 1740
    iget-object v0, p1, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->serverId:Ljava/lang/String;

    invoke-interface {p2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1741
    iget-object p1, p1, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->serverId:Ljava/lang/String;

    invoke-interface {p2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/helpshift/conversation/activeconversation/message/MessageDM;

    goto :goto_0

    .line 1743
    :cond_0
    iget-object p2, p1, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->createdRequestId:Ljava/lang/String;

    invoke-interface {p3, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_1

    .line 1744
    iget-object p1, p1, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->createdRequestId:Ljava/lang/String;

    invoke-interface {p3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/helpshift/conversation/activeconversation/message/MessageDM;

    .line 1745
    iget-object p2, p4, Lcom/helpshift/conversation/activeconversation/ConversationUpdate;->localIdsForResolvedRequestIds:Ljava/util/List;

    iget-object p3, p1, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->localId:Ljava/lang/Long;

    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p3

    invoke-interface {p2, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method private getRouteForSendingMessage()Ljava/lang/String;
    .locals 3

    .line 1090
    invoke-virtual {p0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isInPreIssueMode()Z

    move-result v0

    const-string v1, "/messages/"

    if-eqz v0, :cond_0

    .line 1091
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "/preissues/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->getPreIssueId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1094
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "/issues/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->getIssueId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method private markMessagesAsSeen(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/helpshift/conversation/activeconversation/message/MessageDM;",
            ">;)V"
        }
    .end annotation

    .line 1160
    invoke-static {p1}, Lcom/helpshift/common/ListUtils;->isEmpty(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    .line 1163
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/helpshift/conversation/activeconversation/message/MessageDM;

    iget-object v1, v1, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->readAt:Ljava/lang/String;

    .line 1164
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/helpshift/conversation/activeconversation/message/MessageDM;

    iget-object v0, v0, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->seenAtMessageCursor:Ljava/lang/String;

    .line 1167
    iget-object v2, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-static {v2}, Lcom/helpshift/common/domain/network/NetworkDataRequestUtil;->getUserRequestData(Lcom/helpshift/account/domainmodel/UserDM;)Ljava/util/HashMap;

    move-result-object v2

    const-string v3, "read_at"

    .line 1168
    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "mc"

    .line 1169
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "md_state"

    const-string v1, "read"

    .line 1170
    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1172
    invoke-direct {p0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->getRouteForSendingMessage()Ljava/lang/String;

    move-result-object v0

    .line 1174
    :try_start_0
    new-instance v1, Lcom/helpshift/common/domain/network/PUTNetwork;

    iget-object v3, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->domain:Lcom/helpshift/common/domain/Domain;

    iget-object v4, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-direct {v1, v0, v3, v4}, Lcom/helpshift/common/domain/network/PUTNetwork;-><init>(Ljava/lang/String;Lcom/helpshift/common/domain/Domain;Lcom/helpshift/common/platform/Platform;)V

    .line 1175
    new-instance v0, Lcom/helpshift/common/domain/network/AuthenticationFailureNetwork;

    invoke-direct {v0, v1}, Lcom/helpshift/common/domain/network/AuthenticationFailureNetwork;-><init>(Lcom/helpshift/common/domain/network/Network;)V

    .line 1176
    new-instance v1, Lcom/helpshift/common/domain/network/TSCorrectedNetwork;

    iget-object v3, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-direct {v1, v0, v3}, Lcom/helpshift/common/domain/network/TSCorrectedNetwork;-><init>(Lcom/helpshift/common/domain/network/Network;Lcom/helpshift/common/platform/Platform;)V

    .line 1177
    new-instance v0, Lcom/helpshift/common/domain/network/FailedAPICallNetworkDecorator;

    invoke-direct {v0, v1}, Lcom/helpshift/common/domain/network/FailedAPICallNetworkDecorator;-><init>(Lcom/helpshift/common/domain/network/Network;)V

    .line 1178
    new-instance v1, Lcom/helpshift/common/domain/network/GuardOKNetwork;

    invoke-direct {v1, v0}, Lcom/helpshift/common/domain/network/GuardOKNetwork;-><init>(Lcom/helpshift/common/domain/network/Network;)V

    .line 1179
    new-instance v0, Lcom/helpshift/common/platform/network/RequestData;

    invoke-direct {v0, v2}, Lcom/helpshift/common/platform/network/RequestData;-><init>(Ljava/util/Map;)V

    invoke-interface {v1, v0}, Lcom/helpshift/common/domain/network/Network;->makeRequest(Lcom/helpshift/common/platform/network/RequestData;)Lcom/helpshift/common/platform/network/Response;
    :try_end_0
    .catch Lcom/helpshift/common/exception/RootAPIException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    .line 1182
    iget-object v1, v0, Lcom/helpshift/common/exception/RootAPIException;->exceptionType:Lcom/helpshift/common/exception/ExceptionType;

    sget-object v2, Lcom/helpshift/common/exception/NetworkException;->INVALID_AUTH_TOKEN:Lcom/helpshift/common/exception/NetworkException;

    if-eq v1, v2, :cond_2

    iget-object v1, v0, Lcom/helpshift/common/exception/RootAPIException;->exceptionType:Lcom/helpshift/common/exception/ExceptionType;

    sget-object v2, Lcom/helpshift/common/exception/NetworkException;->AUTH_TOKEN_NOT_PROVIDED:Lcom/helpshift/common/exception/NetworkException;

    if-ne v1, v2, :cond_1

    goto :goto_0

    .line 1187
    :cond_1
    throw v0

    .line 1184
    :cond_2
    :goto_0
    iget-object v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->domain:Lcom/helpshift/common/domain/Domain;

    invoke-virtual {v1}, Lcom/helpshift/common/domain/Domain;->getAuthenticationFailureDM()Lcom/helpshift/account/AuthenticationFailureDM;

    move-result-object v1

    iget-object v2, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    iget-object v0, v0, Lcom/helpshift/common/exception/RootAPIException;->exceptionType:Lcom/helpshift/common/exception/ExceptionType;

    invoke-virtual {v1, v2, v0}, Lcom/helpshift/account/AuthenticationFailureDM;->notifyAuthenticationFailure(Lcom/helpshift/account/domainmodel/UserDM;Lcom/helpshift/common/exception/ExceptionType;)V

    .line 1191
    :goto_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/helpshift/conversation/activeconversation/message/MessageDM;

    const/4 v2, 0x1

    .line 1192
    iput-boolean v2, v1, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->isMessageSeenSynced:Z

    goto :goto_2

    .line 1194
    :cond_3
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-interface {v0}, Lcom/helpshift/common/platform/Platform;->getConversationDAO()Lcom/helpshift/conversation/dao/ConversationDAO;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/helpshift/conversation/dao/ConversationDAO;->insertOrUpdateMessages(Ljava/util/List;)V

    return-void
.end method

.method private populateMessageDMLookup(ZLjava/util/Map;Ljava/util/Map;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/helpshift/conversation/activeconversation/message/MessageDM;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/helpshift/conversation/activeconversation/message/MessageDM;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_4

    .line 1683
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 1684
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->conversationDAO:Lcom/helpshift/conversation/dao/ConversationDAO;

    iget-object v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localId:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-interface {v0, v1, v2}, Lcom/helpshift/conversation/dao/ConversationDAO;->readMessages(J)Ljava/util/List;

    move-result-object v0

    .line 1685
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1686
    iget-object v2, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->messageDMs:Lcom/helpshift/common/util/HSObservableList;

    invoke-virtual {v2}, Lcom/helpshift/common/util/HSObservableList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/helpshift/conversation/activeconversation/message/MessageDM;

    .line 1687
    iget-object v4, v3, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->localId:Ljava/lang/Long;

    if-eqz v4, :cond_0

    .line 1688
    iget-object v4, v3, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->localId:Ljava/lang/Long;

    invoke-interface {v1, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1691
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/helpshift/conversation/activeconversation/message/MessageDM;

    .line 1692
    iget-object v3, v2, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->localId:Ljava/lang/Long;

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/helpshift/conversation/activeconversation/message/MessageDM;

    if-nez v3, :cond_2

    .line 1694
    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1697
    :cond_2
    invoke-interface {p1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1700
    :cond_3
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    goto :goto_2

    .line 1703
    :cond_4
    iget-object p1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->conversationDAO:Lcom/helpshift/conversation/dao/ConversationDAO;

    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-interface {p1, v0, v1}, Lcom/helpshift/conversation/dao/ConversationDAO;->readMessages(J)Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    .line 1706
    :goto_2
    invoke-direct {p0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->getLocalIdToPendingRequestIdMessageMap()Ljava/util/Map;

    move-result-object v0

    .line 1708
    :cond_5
    :goto_3
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1709
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/helpshift/conversation/activeconversation/message/MessageDM;

    .line 1710
    iget-object v2, v1, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->serverId:Ljava/lang/String;

    invoke-static {v2}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 1711
    iget-object v2, v1, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->serverId:Ljava/lang/String;

    invoke-interface {p2, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1714
    :cond_6
    iget-object v2, v1, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->localId:Ljava/lang/Long;

    if-eqz v2, :cond_5

    .line 1715
    iget-object v2, v1, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->localId:Ljava/lang/Long;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    if-eqz v0, :cond_5

    .line 1717
    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1718
    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {p3, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    :cond_7
    return-void
.end method

.method private sendConversationEndedDelegate()V
    .locals 1

    .line 393
    iget-boolean v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isConversationEndedDelegateSent:Z

    if-nez v0, :cond_0

    .line 395
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->domain:Lcom/helpshift/common/domain/Domain;

    invoke-virtual {v0}, Lcom/helpshift/common/domain/Domain;->getDelegate()Lcom/helpshift/delegate/UIThreadDelegateDecorator;

    move-result-object v0

    invoke-virtual {v0}, Lcom/helpshift/delegate/UIThreadDelegateDecorator;->conversationEnded()V

    const/4 v0, 0x1

    .line 397
    iput-boolean v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isConversationEndedDelegateSent:Z

    .line 398
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->conversationDAO:Lcom/helpshift/conversation/dao/ConversationDAO;

    invoke-interface {v0, p0}, Lcom/helpshift/conversation/dao/ConversationDAO;->updateConversationWithoutMessages(Lcom/helpshift/conversation/activeconversation/ConversationDM;)V

    :cond_0
    return-void
.end method

.method private sendMessageWithAutoRetry(Lcom/helpshift/common/domain/F;)V
    .locals 2

    .line 1367
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v1, Lcom/helpshift/conversation/activeconversation/ConversationDM$8;

    invoke-direct {v1, p0, p1}, Lcom/helpshift/conversation/activeconversation/ConversationDM$8;-><init>(Lcom/helpshift/conversation/activeconversation/ConversationDM;Lcom/helpshift/common/domain/F;)V

    invoke-virtual {v0, v1}, Lcom/helpshift/common/domain/Domain;->runParallel(Lcom/helpshift/common/domain/F;)V

    return-void
.end method

.method private sendReOpenRejectedMessage(ILjava/lang/String;Ljava/lang/String;)V
    .locals 9

    .line 1388
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-static {v0}, Lcom/helpshift/common/util/HSDateFormatSpec;->getCurrentAdjustedTimeForStorage(Lcom/helpshift/common/platform/Platform;)Ljava/lang/String;

    move-result-object v3

    .line 1389
    invoke-static {v3}, Lcom/helpshift/common/util/HSDateFormatSpec;->convertToEpochTime(Ljava/lang/String;)J

    move-result-wide v4

    .line 1390
    new-instance v0, Lcom/helpshift/conversation/activeconversation/message/FollowupRejectedMessageDM;

    const/4 v2, 0x0

    const-string v6, "mobile"

    const/4 v8, 0x1

    move-object v1, v0

    move-object v7, p3

    invoke-direct/range {v1 .. v8}, Lcom/helpshift/conversation/activeconversation/message/FollowupRejectedMessageDM;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;I)V

    .line 1393
    iput p1, v0, Lcom/helpshift/conversation/activeconversation/message/FollowupRejectedMessageDM;->reason:I

    .line 1394
    iput-object p2, v0, Lcom/helpshift/conversation/activeconversation/message/FollowupRejectedMessageDM;->openConversationId:Ljava/lang/String;

    .line 1395
    iget-object p1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localId:Ljava/lang/Long;

    iput-object p1, v0, Lcom/helpshift/conversation/activeconversation/message/FollowupRejectedMessageDM;->conversationLocalId:Ljava/lang/Long;

    .line 1396
    iget-object p1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->domain:Lcom/helpshift/common/domain/Domain;

    iget-object p2, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-virtual {v0, p1, p2}, Lcom/helpshift/conversation/activeconversation/message/FollowupRejectedMessageDM;->setDependencies(Lcom/helpshift/common/domain/Domain;Lcom/helpshift/common/platform/Platform;)V

    .line 1398
    invoke-direct {p0, v0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->addMessageToDBAndGlobalList(Lcom/helpshift/conversation/activeconversation/message/MessageDM;)V

    .line 1400
    new-instance p1, Lcom/helpshift/conversation/activeconversation/ConversationDM$9;

    invoke-direct {p1, p0, v0}, Lcom/helpshift/conversation/activeconversation/ConversationDM$9;-><init>(Lcom/helpshift/conversation/activeconversation/ConversationDM;Lcom/helpshift/conversation/activeconversation/message/FollowupRejectedMessageDM;)V

    invoke-direct {p0, p1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->sendMessageWithAutoRetry(Lcom/helpshift/common/domain/F;)V

    return-void
.end method

.method private sendScreenshotMessageInternal(Lcom/helpshift/conversation/activeconversation/message/ScreenshotMessageDM;Z)V
    .locals 1

    .line 835
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-virtual {p1, v0, p0, p2}, Lcom/helpshift/conversation/activeconversation/message/ScreenshotMessageDM;->uploadImage(Lcom/helpshift/account/domainmodel/UserDM;Lcom/helpshift/conversation/activeconversation/ConversationServerInfo;Z)V

    .line 841
    iget-object p1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    sget-object p2, Lcom/helpshift/conversation/dto/IssueState;->RESOLUTION_REJECTED:Lcom/helpshift/conversation/dto/IssueState;

    if-ne p1, p2, :cond_0

    .line 842
    sget-object p1, Lcom/helpshift/conversation/dto/IssueState;->WAITING_FOR_AGENT:Lcom/helpshift/conversation/dto/IssueState;

    invoke-virtual {p0, p1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->updateIssueStatus(Lcom/helpshift/conversation/dto/IssueState;)V
    :try_end_0
    .catch Lcom/helpshift/common/exception/RootAPIException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 846
    iget-object p2, p1, Lcom/helpshift/common/exception/RootAPIException;->exceptionType:Lcom/helpshift/common/exception/ExceptionType;

    sget-object v0, Lcom/helpshift/common/exception/NetworkException;->CONVERSATION_ARCHIVED:Lcom/helpshift/common/exception/NetworkException;

    if-ne p2, v0, :cond_1

    .line 847
    sget-object p1, Lcom/helpshift/conversation/dto/IssueState;->ARCHIVED:Lcom/helpshift/conversation/dto/IssueState;

    invoke-virtual {p0, p1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->updateIssueStatus(Lcom/helpshift/conversation/dto/IssueState;)V

    :cond_0
    :goto_0
    return-void

    .line 850
    :cond_1
    throw p1
.end method

.method private sendTextMessage(Lcom/helpshift/conversation/activeconversation/message/UserMessageDM;)V
    .locals 2

    .line 246
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-virtual {p1, v0, p0}, Lcom/helpshift/conversation/activeconversation/message/UserMessageDM;->send(Lcom/helpshift/account/domainmodel/UserDM;Lcom/helpshift/conversation/activeconversation/ConversationServerInfo;)V

    .line 250
    iget-object p1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    sget-object v0, Lcom/helpshift/conversation/dto/IssueState;->RESOLUTION_REJECTED:Lcom/helpshift/conversation/dto/IssueState;

    if-ne p1, v0, :cond_1

    .line 251
    sget-object p1, Lcom/helpshift/conversation/dto/IssueState;->WAITING_FOR_AGENT:Lcom/helpshift/conversation/dto/IssueState;

    invoke-virtual {p0, p1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->updateIssueStatus(Lcom/helpshift/conversation/dto/IssueState;)V
    :try_end_0
    .catch Lcom/helpshift/common/exception/RootAPIException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    .line 255
    iget-object v0, p1, Lcom/helpshift/common/exception/RootAPIException;->exceptionType:Lcom/helpshift/common/exception/ExceptionType;

    sget-object v1, Lcom/helpshift/common/exception/NetworkException;->CONVERSATION_ARCHIVED:Lcom/helpshift/common/exception/NetworkException;

    if-ne v0, v1, :cond_0

    .line 256
    sget-object p1, Lcom/helpshift/conversation/dto/IssueState;->ARCHIVED:Lcom/helpshift/conversation/dto/IssueState;

    invoke-virtual {p0, p1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->updateIssueStatus(Lcom/helpshift/conversation/dto/IssueState;)V

    goto :goto_0

    .line 258
    :cond_0
    iget-object v0, p1, Lcom/helpshift/common/exception/RootAPIException;->exceptionType:Lcom/helpshift/common/exception/ExceptionType;

    sget-object v1, Lcom/helpshift/common/exception/NetworkException;->USER_PRE_CONDITION_FAILED:Lcom/helpshift/common/exception/NetworkException;

    if-ne v0, v1, :cond_2

    .line 259
    sget-object p1, Lcom/helpshift/conversation/dto/IssueState;->AUTHOR_MISMATCH:Lcom/helpshift/conversation/dto/IssueState;

    invoke-virtual {p0, p1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->updateIssueStatus(Lcom/helpshift/conversation/dto/IssueState;)V

    :cond_1
    :goto_0
    return-void

    .line 262
    :cond_2
    throw p1
.end method

.method private setCSATState(Lcom/helpshift/conversation/states/ConversationCSATState;)V
    .locals 2

    .line 1283
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->csatState:Lcom/helpshift/conversation/states/ConversationCSATState;

    if-eq v0, p1, :cond_0

    .line 1284
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Update CSAT state : Conversation : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->serverId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", state : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/helpshift/conversation/states/ConversationCSATState;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Helpshift_ConvDM"

    invoke-static {v1, v0}, Lcom/helpshift/util/HSLogger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1286
    :cond_0
    iput-object p1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->csatState:Lcom/helpshift/conversation/states/ConversationCSATState;

    .line 1289
    iget-object p1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->conversationDAO:Lcom/helpshift/conversation/dao/ConversationDAO;

    invoke-interface {p1, p0}, Lcom/helpshift/conversation/dao/ConversationDAO;->updateConversationWithoutMessages(Lcom/helpshift/conversation/activeconversation/ConversationDM;)V

    return-void
.end method

.method private updateMessageClickableState(Lcom/helpshift/conversation/activeconversation/message/MessageDM;Z)V
    .locals 1

    .line 694
    instance-of v0, p1, Lcom/helpshift/conversation/activeconversation/message/UserMessageDM;

    if-eqz v0, :cond_0

    .line 695
    check-cast p1, Lcom/helpshift/conversation/activeconversation/message/UserMessageDM;

    invoke-virtual {p1, p2}, Lcom/helpshift/conversation/activeconversation/message/UserMessageDM;->updateState(Z)V

    goto :goto_0

    .line 697
    :cond_0
    instance-of v0, p1, Lcom/helpshift/conversation/activeconversation/message/RequestScreenshotMessageDM;

    if-eqz v0, :cond_1

    .line 698
    check-cast p1, Lcom/helpshift/conversation/activeconversation/message/RequestScreenshotMessageDM;

    invoke-virtual {p1, p2}, Lcom/helpshift/conversation/activeconversation/message/RequestScreenshotMessageDM;->setAttachmentButtonClickable(Z)V

    goto :goto_0

    .line 700
    :cond_1
    instance-of v0, p1, Lcom/helpshift/conversation/activeconversation/message/ScreenshotMessageDM;

    if-eqz v0, :cond_2

    .line 701
    check-cast p1, Lcom/helpshift/conversation/activeconversation/message/ScreenshotMessageDM;

    invoke-virtual {p1, p2}, Lcom/helpshift/conversation/activeconversation/message/ScreenshotMessageDM;->updateState(Z)V

    :cond_2
    :goto_0
    return-void
.end method


# virtual methods
.method addMessageToUI(Lcom/helpshift/conversation/activeconversation/message/MessageDM;)V
    .locals 2

    .line 791
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->domain:Lcom/helpshift/common/domain/Domain;

    iget-object v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-virtual {p1, v0, v1}, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->setDependencies(Lcom/helpshift/common/domain/Domain;Lcom/helpshift/common/platform/Platform;)V

    .line 792
    invoke-virtual {p1}, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->isUISupportedMessage()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 793
    invoke-virtual {p1, p0}, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->addObserver(Ljava/util/Observer;)V

    .line 794
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->messageDMs:Lcom/helpshift/common/util/HSObservableList;

    invoke-virtual {v0, p1}, Lcom/helpshift/common/util/HSObservableList;->add(Ljava/lang/Object;)Z

    .line 795
    invoke-virtual {p0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->sortMessageDMs()V

    :cond_0
    return-void
.end method

.method public checkAndIncrementMessageCount(Lcom/helpshift/conversation/dto/IssueState;)V
    .locals 2

    .line 1572
    invoke-virtual {p0, p1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isInProgressState(Lcom/helpshift/conversation/dto/IssueState;)Z

    move-result p1

    const/4 v0, 0x1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    sget-object v1, Lcom/helpshift/conversation/dto/IssueState;->RESOLUTION_REQUESTED:Lcom/helpshift/conversation/dto/IssueState;

    if-eq p1, v1, :cond_0

    iget-object p1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    sget-object v1, Lcom/helpshift/conversation/dto/IssueState;->RESOLUTION_ACCEPTED:Lcom/helpshift/conversation/dto/IssueState;

    if-eq p1, v1, :cond_0

    iget-object p1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    sget-object v1, Lcom/helpshift/conversation/dto/IssueState;->RESOLUTION_REJECTED:Lcom/helpshift/conversation/dto/IssueState;

    if-ne p1, v1, :cond_1

    .line 1576
    :cond_0
    invoke-virtual {p0, v0, v0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->setShouldIncrementMessageCount(ZZ)V

    goto :goto_0

    .line 1578
    :cond_1
    invoke-virtual {p0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isIssueInProgress()Z

    move-result p1

    if-eqz p1, :cond_2

    const/4 p1, 0x0

    .line 1579
    invoke-virtual {p0, p1, v0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->setShouldIncrementMessageCount(ZZ)V

    :cond_2
    :goto_0
    return-void
.end method

.method public checkForReOpen(ILjava/lang/String;Z)Z
    .locals 12

    .line 1296
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->messageDMs:Lcom/helpshift/common/util/HSObservableList;

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Lcom/helpshift/common/util/HSObservableList;->size()I

    move-result v0

    if-lez v0, :cond_6

    .line 1297
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->messageDMs:Lcom/helpshift/common/util/HSObservableList;

    invoke-virtual {v0}, Lcom/helpshift/common/util/HSObservableList;->size()I

    move-result v3

    sub-int/2addr v3, v2

    invoke-virtual {v0, v3}, Lcom/helpshift/common/util/HSObservableList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/helpshift/conversation/activeconversation/message/MessageDM;

    .line 1299
    instance-of v3, v0, Lcom/helpshift/conversation/activeconversation/message/RequestForReopenMessageDM;

    if-nez v3, :cond_0

    return v1

    .line 1304
    :cond_0
    move-object v3, v0

    check-cast v3, Lcom/helpshift/conversation/activeconversation/message/RequestForReopenMessageDM;

    invoke-virtual {v3}, Lcom/helpshift/conversation/activeconversation/message/RequestForReopenMessageDM;->isAnswered()Z

    move-result v4

    if-eqz v4, :cond_1

    return v1

    :cond_1
    const/4 v4, 0x0

    if-ne p1, v2, :cond_2

    .line 1310
    iget-object p1, v0, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->serverId:Ljava/lang/String;

    invoke-direct {p0, v2, v4, p1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->sendReOpenRejectedMessage(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    if-eqz p3, :cond_3

    const/4 p1, 0x4

    .line 1315
    iget-object p2, v0, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->serverId:Ljava/lang/String;

    invoke-direct {p0, p1, v4, p2}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->sendReOpenRejectedMessage(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const/4 p3, 0x2

    if-ne p1, p3, :cond_4

    const/4 p1, 0x3

    .line 1320
    iget-object p2, v0, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->serverId:Ljava/lang/String;

    invoke-direct {p0, p1, v4, p2}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->sendReOpenRejectedMessage(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    if-eqz p2, :cond_5

    .line 1324
    iget-object p1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->serverId:Ljava/lang/String;

    .line 1325
    invoke-virtual {p2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_5

    .line 1326
    iget-object p1, v0, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->serverId:Ljava/lang/String;

    invoke-direct {p0, p3, p2, p1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->sendReOpenRejectedMessage(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1332
    :cond_5
    sget-object p1, Lcom/helpshift/conversation/dto/IssueState;->WAITING_FOR_AGENT:Lcom/helpshift/conversation/dto/IssueState;

    iput-object p1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    .line 1333
    iput-boolean v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isConversationEndedDelegateSent:Z

    .line 1334
    iget-object p1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->conversationDAO:Lcom/helpshift/conversation/dao/ConversationDAO;

    invoke-interface {p1, p0}, Lcom/helpshift/conversation/dao/ConversationDAO;->updateConversationWithoutMessages(Lcom/helpshift/conversation/activeconversation/ConversationDM;)V

    .line 1337
    iget-object p1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-static {p1}, Lcom/helpshift/common/util/HSDateFormatSpec;->getCurrentAdjustedTimeForStorage(Lcom/helpshift/common/platform/Platform;)Ljava/lang/String;

    move-result-object v6

    .line 1338
    invoke-static {v6}, Lcom/helpshift/common/util/HSDateFormatSpec;->convertToEpochTime(Ljava/lang/String;)J

    move-result-wide v7

    .line 1339
    new-instance p1, Lcom/helpshift/conversation/activeconversation/message/FollowupAcceptedMessageDM;

    const/4 v5, 0x0

    iget-object v10, v0, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->serverId:Ljava/lang/String;

    const/4 v11, 0x1

    const-string v9, "mobile"

    move-object v4, p1

    invoke-direct/range {v4 .. v11}, Lcom/helpshift/conversation/activeconversation/message/FollowupAcceptedMessageDM;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;I)V

    .line 1342
    iget-object p2, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localId:Ljava/lang/Long;

    iput-object p2, p1, Lcom/helpshift/conversation/activeconversation/message/FollowupAcceptedMessageDM;->conversationLocalId:Ljava/lang/Long;

    .line 1343
    iget-object p2, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->domain:Lcom/helpshift/common/domain/Domain;

    iget-object p3, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-virtual {p1, p2, p3}, Lcom/helpshift/conversation/activeconversation/message/FollowupAcceptedMessageDM;->setDependencies(Lcom/helpshift/common/domain/Domain;Lcom/helpshift/common/platform/Platform;)V

    .line 1345
    invoke-direct {p0, p1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->addMessageToDBAndGlobalList(Lcom/helpshift/conversation/activeconversation/message/MessageDM;)V

    .line 1349
    invoke-virtual {v3, v2}, Lcom/helpshift/conversation/activeconversation/message/RequestForReopenMessageDM;->setAnsweredAndNotify(Z)V

    .line 1350
    iget-object p2, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-interface {p2}, Lcom/helpshift/common/platform/Platform;->getConversationDAO()Lcom/helpshift/conversation/dao/ConversationDAO;

    move-result-object p2

    invoke-interface {p2, v3}, Lcom/helpshift/conversation/dao/ConversationDAO;->insertOrUpdateMessage(Lcom/helpshift/conversation/activeconversation/message/MessageDM;)V

    .line 1352
    new-instance p2, Lcom/helpshift/conversation/activeconversation/ConversationDM$7;

    invoke-direct {p2, p0, p1}, Lcom/helpshift/conversation/activeconversation/ConversationDM$7;-><init>(Lcom/helpshift/conversation/activeconversation/ConversationDM;Lcom/helpshift/conversation/activeconversation/message/FollowupAcceptedMessageDM;)V

    invoke-direct {p0, p2}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->sendMessageWithAutoRetry(Lcom/helpshift/common/domain/F;)V

    const/4 v1, 0x1

    :cond_6
    :goto_0
    return v1
.end method

.method public clearMessageUpdates(Lcom/helpshift/conversation/activeconversation/ConversationUpdate;)V
    .locals 4

    const/4 v0, 0x0

    .line 1872
    :goto_0
    iget-object v1, p1, Lcom/helpshift/conversation/activeconversation/ConversationUpdate;->localIdsForResolvedRequestIds:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1873
    iget-object v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-interface {v1}, Lcom/helpshift/common/platform/Platform;->getNetworkRequestDAO()Lcom/helpshift/common/platform/network/NetworkRequestDAO;

    move-result-object v1

    .line 1874
    invoke-direct {p0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->getRouteForSendingMessage()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p1, Lcom/helpshift/conversation/activeconversation/ConversationUpdate;->localIdsForResolvedRequestIds:Ljava/util/List;

    .line 1875
    invoke-interface {v3, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1874
    invoke-interface {v1, v2, v3}, Lcom/helpshift/common/platform/network/NetworkRequestDAO;->deletePendingRequestId(Ljava/lang/String;Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1877
    :cond_0
    iget-object v0, p1, Lcom/helpshift/conversation/activeconversation/ConversationUpdate;->updatedMessageDMs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1878
    iget-object p1, p1, Lcom/helpshift/conversation/activeconversation/ConversationUpdate;->newMessageDMs:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->clear()V

    return-void
.end method

.method public containsAtleastOneUserMessage()Z
    .locals 5

    .line 1885
    invoke-virtual {p0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isInPreIssueMode()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_3

    .line 1886
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1889
    iget-object v2, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->messageDMs:Lcom/helpshift/common/util/HSObservableList;

    invoke-virtual {v2}, Lcom/helpshift/common/util/HSObservableList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/helpshift/conversation/activeconversation/message/MessageDM;

    .line 1890
    invoke-virtual {v3}, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->isUISupportedMessage()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1891
    instance-of v4, v3, Lcom/helpshift/conversation/activeconversation/message/UserMessageDM;

    if-eqz v4, :cond_1

    return v1

    .line 1894
    :cond_1
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1896
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x3

    if-le v3, v4, :cond_0

    return v1

    :cond_2
    const/4 v0, 0x0

    return v0

    :cond_3
    return v1
.end method

.method public deleteCachedScreenshotFiles()V
    .locals 5

    .line 1584
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->conversationDAO:Lcom/helpshift/conversation/dao/ConversationDAO;

    iget-object v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localId:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-interface {v0, v1, v2}, Lcom/helpshift/conversation/dao/ConversationDAO;->readMessages(J)Ljava/util/List;

    move-result-object v0

    .line 1585
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1586
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/helpshift/conversation/activeconversation/message/MessageDM;

    .line 1587
    instance-of v3, v2, Lcom/helpshift/conversation/activeconversation/message/ScreenshotMessageDM;

    if-eqz v3, :cond_0

    .line 1588
    check-cast v2, Lcom/helpshift/conversation/activeconversation/message/ScreenshotMessageDM;

    .line 1589
    invoke-virtual {v2}, Lcom/helpshift/conversation/activeconversation/message/ScreenshotMessageDM;->getFilePath()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 1592
    :try_start_0
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1593
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x0

    .line 1594
    iput-object v3, v2, Lcom/helpshift/conversation/activeconversation/message/ScreenshotMessageDM;->filePath:Ljava/lang/String;

    .line 1595
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    nop

    goto :goto_0

    .line 1606
    :cond_1
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->conversationDAO:Lcom/helpshift/conversation/dao/ConversationDAO;

    invoke-interface {v0, v1}, Lcom/helpshift/conversation/dao/ConversationDAO;->insertOrUpdateMessages(Ljava/util/List;)V

    return-void
.end method

.method public dropCustomMetaData()V
    .locals 2

    .line 366
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->metaDataDM:Lcom/helpshift/meta/MetaDataDM;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/helpshift/meta/MetaDataDM;->setCustomMetaDataCallable(Lcom/helpshift/meta/RootMetaDataCallable;)V

    .line 367
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->metaDataDM:Lcom/helpshift/meta/MetaDataDM;

    invoke-virtual {v0}, Lcom/helpshift/meta/MetaDataDM;->clearCustomMetaData()V

    return-void
.end method

.method public evaluateBotExecutionState(Ljava/util/List;Z)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/helpshift/conversation/activeconversation/message/MessageDM;",
            ">;Z)Z"
        }
    .end annotation

    if-eqz p1, :cond_3

    .line 584
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_1

    .line 590
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    sub-int/2addr v0, v1

    :goto_0
    if-ltz v0, :cond_3

    .line 591
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/helpshift/conversation/activeconversation/message/MessageDM;

    .line 592
    iget-object v3, v2, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->messageType:Lcom/helpshift/conversation/activeconversation/message/MessageType;

    .line 594
    sget-object v4, Lcom/helpshift/conversation/activeconversation/message/MessageType;->ADMIN_BOT_CONTROL:Lcom/helpshift/conversation/activeconversation/message/MessageType;

    if-ne v4, v3, :cond_2

    .line 595
    check-cast v2, Lcom/helpshift/conversation/activeconversation/message/AdminBotControlMessageDM;

    .line 596
    iget-object v3, v2, Lcom/helpshift/conversation/activeconversation/message/AdminBotControlMessageDM;->actionType:Ljava/lang/String;

    const-string v4, "bot_started"

    .line 597
    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    return v1

    :cond_1
    const-string v4, "bot_ended"

    .line 600
    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 601
    iget-boolean p1, v2, Lcom/helpshift/conversation/activeconversation/message/AdminBotControlMessageDM;->hasNextBot:Z

    return p1

    :cond_2
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_3
    :goto_1
    return p2
.end method

.method public getCreatedAt()Ljava/lang/String;
    .locals 1

    .line 1911
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->createdAt:Ljava/lang/String;

    return-object v0
.end method

.method public getEpochCreatedAtTime()J
    .locals 2

    .line 1921
    iget-wide v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->epochCreatedAtTime:J

    return-wide v0
.end method

.method public getIssueId()Ljava/lang/String;
    .locals 1

    .line 1616
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->serverId:Ljava/lang/String;

    return-object v0
.end method

.method public getLatestActionableBotMessage()Lcom/helpshift/conversation/activeconversation/message/MessageDM;
    .locals 5

    .line 617
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->messageDMs:Lcom/helpshift/common/util/HSObservableList;

    invoke-virtual {v0}, Lcom/helpshift/common/util/HSObservableList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    const/4 v1, 0x0

    if-ltz v0, :cond_4

    .line 618
    iget-object v2, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->messageDMs:Lcom/helpshift/common/util/HSObservableList;

    invoke-virtual {v2, v0}, Lcom/helpshift/common/util/HSObservableList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/helpshift/conversation/activeconversation/message/MessageDM;

    .line 619
    iget-object v3, v2, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->messageType:Lcom/helpshift/conversation/activeconversation/message/MessageType;

    sget-object v4, Lcom/helpshift/conversation/activeconversation/message/MessageType;->ADMIN_BOT_CONTROL:Lcom/helpshift/conversation/activeconversation/message/MessageType;

    if-ne v3, v4, :cond_0

    return-object v1

    .line 622
    :cond_0
    iget-object v3, v2, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->messageType:Lcom/helpshift/conversation/activeconversation/message/MessageType;

    sget-object v4, Lcom/helpshift/conversation/activeconversation/message/MessageType;->USER_RESP_FOR_OPTION_INPUT:Lcom/helpshift/conversation/activeconversation/message/MessageType;

    if-eq v3, v4, :cond_4

    iget-object v3, v2, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->messageType:Lcom/helpshift/conversation/activeconversation/message/MessageType;

    sget-object v4, Lcom/helpshift/conversation/activeconversation/message/MessageType;->USER_RESP_FOR_TEXT_INPUT:Lcom/helpshift/conversation/activeconversation/message/MessageType;

    if-ne v3, v4, :cond_1

    goto :goto_2

    .line 628
    :cond_1
    iget-object v1, v2, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->messageType:Lcom/helpshift/conversation/activeconversation/message/MessageType;

    sget-object v3, Lcom/helpshift/conversation/activeconversation/message/MessageType;->ADMIN_TEXT_WITH_TEXT_INPUT:Lcom/helpshift/conversation/activeconversation/message/MessageType;

    if-eq v1, v3, :cond_3

    iget-object v1, v2, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->messageType:Lcom/helpshift/conversation/activeconversation/message/MessageType;

    sget-object v3, Lcom/helpshift/conversation/activeconversation/message/MessageType;->ADMIN_TEXT_WITH_OPTION_INPUT:Lcom/helpshift/conversation/activeconversation/message/MessageType;

    if-eq v1, v3, :cond_3

    iget-object v1, v2, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->messageType:Lcom/helpshift/conversation/activeconversation/message/MessageType;

    sget-object v3, Lcom/helpshift/conversation/activeconversation/message/MessageType;->FAQ_LIST_WITH_OPTION_INPUT:Lcom/helpshift/conversation/activeconversation/message/MessageType;

    if-eq v1, v3, :cond_3

    iget-object v1, v2, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->messageType:Lcom/helpshift/conversation/activeconversation/message/MessageType;

    sget-object v3, Lcom/helpshift/conversation/activeconversation/message/MessageType;->OPTION_INPUT:Lcom/helpshift/conversation/activeconversation/message/MessageType;

    if-ne v1, v3, :cond_2

    goto :goto_1

    :cond_2
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_3
    :goto_1
    return-object v2

    :cond_4
    :goto_2
    return-object v1
.end method

.method public getPreIssueId()Ljava/lang/String;
    .locals 1

    .line 1621
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->preConversationServerId:Ljava/lang/String;

    return-object v0
.end method

.method public getUnSeenMessageCount()I
    .locals 5

    .line 1427
    invoke-virtual {p0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->shouldOpen()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    .line 1431
    :cond_0
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->conversationDAO:Lcom/helpshift/conversation/dao/ConversationDAO;

    iget-object v2, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localId:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lcom/helpshift/conversation/dao/ConversationDAO;->readMessages(J)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1434
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/helpshift/conversation/activeconversation/message/MessageDM;

    .line 1435
    invoke-virtual {v2}, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->isUISupportedMessage()Z

    move-result v3

    if-eqz v3, :cond_1

    iget v3, v2, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->deliveryState:I

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    .line 1437
    sget-object v3, Lcom/helpshift/conversation/activeconversation/ConversationDM$11;->$SwitchMap$com$helpshift$conversation$activeconversation$message$MessageType:[I

    iget-object v4, v2, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->messageType:Lcom/helpshift/conversation/activeconversation/message/MessageType;

    invoke-virtual {v4}, Lcom/helpshift/conversation/activeconversation/message/MessageType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    goto :goto_0

    .line 1439
    :pswitch_0
    instance-of v3, v2, Lcom/helpshift/conversation/activeconversation/message/AdminMessageWithTextInputDM;

    if-eqz v3, :cond_1

    check-cast v2, Lcom/helpshift/conversation/activeconversation/message/AdminMessageWithTextInputDM;

    iget-boolean v2, v2, Lcom/helpshift/conversation/activeconversation/message/AdminMessageWithTextInputDM;->isMessageEmpty:Z

    if-nez v2, :cond_1

    :pswitch_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1460
    :cond_2
    iget-boolean v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->shouldIncrementMessageCount:Z

    if-eqz v0, :cond_3

    add-int/lit8 v1, v1, 0x1

    :cond_3
    return v1

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public handleAdminSuggestedQuestionRead(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .line 1644
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->messageDMs:Lcom/helpshift/common/util/HSObservableList;

    invoke-virtual {v0}, Lcom/helpshift/common/util/HSObservableList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/helpshift/conversation/activeconversation/message/MessageDM;

    .line 1645
    instance-of v2, v1, Lcom/helpshift/conversation/activeconversation/message/FAQListMessageDM;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->serverId:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1646
    new-instance p1, Lcom/helpshift/conversation/activeconversation/ConversationDM$10;

    invoke-direct {p1, p0, v1, p2, p3}, Lcom/helpshift/conversation/activeconversation/ConversationDM$10;-><init>(Lcom/helpshift/conversation/activeconversation/ConversationDM;Lcom/helpshift/conversation/activeconversation/message/MessageDM;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->sendMessageWithAutoRetry(Lcom/helpshift/common/domain/F;)V

    :cond_1
    return-void
.end method

.method public handleAppReviewRequestClick(Lcom/helpshift/conversation/activeconversation/message/RequestAppReviewMessageDM;)V
    .locals 2

    .line 268
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->domain:Lcom/helpshift/common/domain/Domain;

    iget-object v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->platform:Lcom/helpshift/common/platform/Platform;

    .line 269
    invoke-virtual {p1, v0, v1}, Lcom/helpshift/conversation/activeconversation/message/RequestAppReviewMessageDM;->handleRequestReviewClick(Lcom/helpshift/common/domain/Domain;Lcom/helpshift/common/platform/Platform;)Lcom/helpshift/conversation/activeconversation/message/AcceptedAppReviewMessageDM;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 271
    new-instance v1, Lcom/helpshift/conversation/activeconversation/ConversationDM$1;

    invoke-direct {v1, p0, v0, p1}, Lcom/helpshift/conversation/activeconversation/ConversationDM$1;-><init>(Lcom/helpshift/conversation/activeconversation/ConversationDM;Lcom/helpshift/conversation/activeconversation/message/AcceptedAppReviewMessageDM;Lcom/helpshift/conversation/activeconversation/message/RequestAppReviewMessageDM;)V

    invoke-direct {p0, v1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->sendMessageWithAutoRetry(Lcom/helpshift/common/domain/F;)V

    :cond_0
    return-void
.end method

.method public handleConversationEnded()V
    .locals 2

    .line 382
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->domain:Lcom/helpshift/common/domain/Domain;

    new-instance v1, Lcom/helpshift/conversation/activeconversation/ConversationDM$2;

    invoke-direct {v1, p0}, Lcom/helpshift/conversation/activeconversation/ConversationDM$2;-><init>(Lcom/helpshift/conversation/activeconversation/ConversationDM;)V

    invoke-virtual {v0, v1}, Lcom/helpshift/common/domain/Domain;->runParallel(Lcom/helpshift/common/domain/F;)V

    return-void
.end method

.method public handlePreIssueCreationSuccess()V
    .locals 2

    .line 1660
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->lastUserActivityTime:J

    return-void
.end method

.method public hasBotSwitchedToAnotherBotInPollerResponse(Ljava/util/Collection;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+",
            "Lcom/helpshift/conversation/activeconversation/message/MessageDM;",
            ">;)Z"
        }
    .end annotation

    const/4 v0, 0x0

    if-eqz p1, :cond_3

    .line 648
    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v1

    if-nez v1, :cond_0

    goto :goto_1

    .line 652
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 657
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result p1

    const/4 v2, 0x1

    sub-int/2addr p1, v2

    const/4 v3, 0x0

    :goto_0
    if-ltz p1, :cond_3

    .line 658
    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/helpshift/conversation/activeconversation/message/MessageDM;

    .line 659
    iget-object v5, v4, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->messageType:Lcom/helpshift/conversation/activeconversation/message/MessageType;

    .line 661
    sget-object v6, Lcom/helpshift/conversation/activeconversation/message/MessageType;->ADMIN_BOT_CONTROL:Lcom/helpshift/conversation/activeconversation/message/MessageType;

    if-ne v6, v5, :cond_2

    .line 662
    check-cast v4, Lcom/helpshift/conversation/activeconversation/message/AdminBotControlMessageDM;

    .line 663
    iget-object v4, v4, Lcom/helpshift/conversation/activeconversation/message/AdminBotControlMessageDM;->actionType:Ljava/lang/String;

    const-string v5, "bot_ended"

    .line 664
    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    return v3

    :cond_1
    const-string v5, "bot_started"

    .line 669
    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v3, 0x1

    :cond_2
    add-int/lit8 p1, p1, -0x1

    goto :goto_0

    :cond_3
    :goto_1
    return v0
.end method

.method public initializeIssueStatusForUI()V
    .locals 2

    .line 298
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    sget-object v1, Lcom/helpshift/conversation/dto/IssueState;->RESOLUTION_REQUESTED:Lcom/helpshift/conversation/dto/IssueState;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->sdkConfigurationDM:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    .line 299
    invoke-virtual {v0}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->shouldShowConversationResolutionQuestion()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 300
    invoke-virtual {p0, v0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->markConversationResolutionStatus(Z)V

    :cond_0
    return-void
.end method

.method initializeMessageListForUI(Ljava/util/List;Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/helpshift/conversation/activeconversation/message/MessageDM;",
            ">;Z)V"
        }
    .end annotation

    .line 456
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/helpshift/conversation/activeconversation/message/MessageDM;

    .line 457
    iget-object v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->domain:Lcom/helpshift/common/domain/Domain;

    iget-object v2, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-virtual {v0, v1, v2}, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->setDependencies(Lcom/helpshift/common/domain/Domain;Lcom/helpshift/common/platform/Platform;)V

    .line 458
    iget-boolean v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->showAgentName:Z

    iput-boolean v1, v0, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->shouldShowAgentNameForConversation:Z

    .line 459
    invoke-virtual {p0, v0, p2}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->updateMessageOnConversationUpdate(Lcom/helpshift/conversation/activeconversation/message/MessageDM;Z)V

    .line 460
    invoke-virtual {p0, v0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->updateAcceptedRequestForReopenMessageDMs(Lcom/helpshift/conversation/activeconversation/message/MessageDM;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method initializeMessagesForUI(Z)V
    .locals 4

    .line 418
    invoke-virtual {p0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->sortMessageDMs()V

    const/4 v0, 0x0

    if-eqz p1, :cond_3

    .line 422
    iget-object p1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->messageDMs:Lcom/helpshift/common/util/HSObservableList;

    invoke-virtual {p0, p1, v0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->evaluateBotExecutionState(Ljava/util/List;Z)Z

    move-result p1

    iput-boolean p1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isInBetweenBotExecution:Z

    .line 423
    iget-object p1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->messageDMs:Lcom/helpshift/common/util/HSObservableList;

    invoke-virtual {p1}, Lcom/helpshift/common/util/HSObservableList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/helpshift/conversation/activeconversation/message/MessageDM;

    .line 424
    iget-object v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->domain:Lcom/helpshift/common/domain/Domain;

    iget-object v2, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-virtual {v0, v1, v2}, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->setDependencies(Lcom/helpshift/common/domain/Domain;Lcom/helpshift/common/platform/Platform;)V

    .line 425
    iget-boolean v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->showAgentName:Z

    iput-boolean v1, v0, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->shouldShowAgentNameForConversation:Z

    .line 426
    instance-of v1, v0, Lcom/helpshift/conversation/activeconversation/message/AdminImageAttachmentMessageDM;

    if-eqz v1, :cond_0

    .line 427
    move-object v1, v0

    check-cast v1, Lcom/helpshift/conversation/activeconversation/message/AdminImageAttachmentMessageDM;

    iget-object v2, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-virtual {v1, v2}, Lcom/helpshift/conversation/activeconversation/message/AdminImageAttachmentMessageDM;->downloadThumbnailImage(Lcom/helpshift/common/platform/Platform;)V

    .line 429
    :cond_0
    invoke-virtual {p0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->shouldEnableMessagesClick()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->updateMessageOnConversationUpdate(Lcom/helpshift/conversation/activeconversation/message/MessageDM;Z)V

    .line 430
    invoke-virtual {p0, v0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->updateAcceptedRequestForReopenMessageDMs(Lcom/helpshift/conversation/activeconversation/message/MessageDM;)V

    goto :goto_0

    .line 432
    :cond_1
    iget-object p1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->messageDMs:Lcom/helpshift/common/util/HSObservableList;

    invoke-virtual {p1}, Lcom/helpshift/common/util/HSObservableList;->size()I

    move-result p1

    if-lez p1, :cond_5

    .line 434
    iget-object p1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->messageDMs:Lcom/helpshift/common/util/HSObservableList;

    invoke-virtual {p1}, Lcom/helpshift/common/util/HSObservableList;->size()I

    move-result v0

    const/4 v1, 0x1

    sub-int/2addr v0, v1

    invoke-virtual {p1, v0}, Lcom/helpshift/common/util/HSObservableList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/helpshift/conversation/activeconversation/message/MessageDM;

    .line 435
    iget-object v0, p1, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->messageType:Lcom/helpshift/conversation/activeconversation/message/MessageType;

    sget-object v2, Lcom/helpshift/conversation/activeconversation/message/MessageType;->USER_RESP_FOR_OPTION_INPUT:Lcom/helpshift/conversation/activeconversation/message/MessageType;

    if-eq v0, v2, :cond_2

    iget-object v0, p1, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->messageType:Lcom/helpshift/conversation/activeconversation/message/MessageType;

    sget-object v2, Lcom/helpshift/conversation/activeconversation/message/MessageType;->USER_RESP_FOR_TEXT_INPUT:Lcom/helpshift/conversation/activeconversation/message/MessageType;

    if-ne v0, v2, :cond_5

    .line 436
    :cond_2
    check-cast p1, Lcom/helpshift/conversation/activeconversation/message/UserMessageDM;

    invoke-virtual {p1, v1}, Lcom/helpshift/conversation/activeconversation/message/UserMessageDM;->updateState(Z)V

    goto :goto_2

    .line 441
    :cond_3
    iget-object p1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->messageDMs:Lcom/helpshift/common/util/HSObservableList;

    invoke-virtual {p1}, Lcom/helpshift/common/util/HSObservableList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/helpshift/conversation/activeconversation/message/MessageDM;

    .line 442
    iget-object v2, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->domain:Lcom/helpshift/common/domain/Domain;

    iget-object v3, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-virtual {v1, v2, v3}, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->setDependencies(Lcom/helpshift/common/domain/Domain;Lcom/helpshift/common/platform/Platform;)V

    .line 443
    iget-boolean v2, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->showAgentName:Z

    iput-boolean v2, v1, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->shouldShowAgentNameForConversation:Z

    .line 444
    instance-of v2, v1, Lcom/helpshift/conversation/activeconversation/message/AdminImageAttachmentMessageDM;

    if-eqz v2, :cond_4

    .line 445
    move-object v2, v1

    check-cast v2, Lcom/helpshift/conversation/activeconversation/message/AdminImageAttachmentMessageDM;

    iget-object v3, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-virtual {v2, v3}, Lcom/helpshift/conversation/activeconversation/message/AdminImageAttachmentMessageDM;->downloadThumbnailImage(Lcom/helpshift/common/platform/Platform;)V

    .line 447
    :cond_4
    invoke-virtual {p0, v1, v0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->updateMessageOnConversationUpdate(Lcom/helpshift/conversation/activeconversation/message/MessageDM;Z)V

    goto :goto_1

    :cond_5
    :goto_2
    return-void
.end method

.method public isInPreIssueMode()Z
    .locals 2

    .line 1611
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->issueType:Ljava/lang/String;

    const-string v1, "preissue"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public isInProgressState(Lcom/helpshift/conversation/dto/IssueState;)Z
    .locals 1

    .line 1634
    sget-object v0, Lcom/helpshift/conversation/dto/IssueState;->NEW:Lcom/helpshift/conversation/dto/IssueState;

    if-eq p1, v0, :cond_1

    sget-object v0, Lcom/helpshift/conversation/dto/IssueState;->NEW_FOR_AGENT:Lcom/helpshift/conversation/dto/IssueState;

    if-eq p1, v0, :cond_1

    sget-object v0, Lcom/helpshift/conversation/dto/IssueState;->AGENT_REPLIED:Lcom/helpshift/conversation/dto/IssueState;

    if-eq p1, v0, :cond_1

    sget-object v0, Lcom/helpshift/conversation/dto/IssueState;->WAITING_FOR_AGENT:Lcom/helpshift/conversation/dto/IssueState;

    if-eq p1, v0, :cond_1

    sget-object v0, Lcom/helpshift/conversation/dto/IssueState;->PENDING_REASSIGNMENT:Lcom/helpshift/conversation/dto/IssueState;

    if-eq p1, v0, :cond_1

    sget-object v0, Lcom/helpshift/conversation/dto/IssueState;->COMPLETED_ISSUE_CREATED:Lcom/helpshift/conversation/dto/IssueState;

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method public isIssueInProgress()Z
    .locals 1

    .line 1630
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    invoke-virtual {p0, v0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isInProgressState(Lcom/helpshift/conversation/dto/IssueState;)Z

    move-result v0

    return v0
.end method

.method public isSynced()Z
    .locals 1

    .line 546
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->serverId:Ljava/lang/String;

    invoke-static {v0}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->preConversationServerId:Ljava/lang/String;

    invoke-static {v0}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public markConversationResolutionStatus(Z)V
    .locals 8

    .line 916
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-static {v0}, Lcom/helpshift/common/util/HSDateFormatSpec;->getCurrentAdjustedTimeForStorage(Lcom/helpshift/common/platform/Platform;)Ljava/lang/String;

    move-result-object v3

    .line 917
    invoke-static {v3}, Lcom/helpshift/common/util/HSDateFormatSpec;->convertToEpochTime(Ljava/lang/String;)J

    move-result-wide v4

    if-eqz p1, :cond_0

    .line 920
    new-instance p1, Lcom/helpshift/conversation/activeconversation/message/ConfirmationAcceptedMessageDM;

    const/4 v7, 0x1

    const-string v2, "Accepted the solution"

    const-string v6, "mobile"

    move-object v1, p1

    invoke-direct/range {v1 .. v7}, Lcom/helpshift/conversation/activeconversation/message/ConfirmationAcceptedMessageDM;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;I)V

    .line 922
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->domain:Lcom/helpshift/common/domain/Domain;

    iget-object v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-virtual {p1, v0, v1}, Lcom/helpshift/conversation/activeconversation/message/ConfirmationAcceptedMessageDM;->setDependencies(Lcom/helpshift/common/domain/Domain;Lcom/helpshift/common/platform/Platform;)V

    .line 923
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localId:Ljava/lang/Long;

    iput-object v0, p1, Lcom/helpshift/conversation/activeconversation/message/ConfirmationAcceptedMessageDM;->conversationLocalId:Ljava/lang/Long;

    .line 925
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->conversationDAO:Lcom/helpshift/conversation/dao/ConversationDAO;

    invoke-interface {v0, p1}, Lcom/helpshift/conversation/dao/ConversationDAO;->insertOrUpdateMessage(Lcom/helpshift/conversation/activeconversation/message/MessageDM;)V

    .line 928
    new-instance v0, Lcom/helpshift/conversation/activeconversation/ConversationDM$4;

    invoke-direct {v0, p0, p1}, Lcom/helpshift/conversation/activeconversation/ConversationDM$4;-><init>(Lcom/helpshift/conversation/activeconversation/ConversationDM;Lcom/helpshift/conversation/activeconversation/message/ConfirmationAcceptedMessageDM;)V

    invoke-direct {p0, v0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->sendMessageWithAutoRetry(Lcom/helpshift/common/domain/F;)V

    .line 945
    sget-object p1, Lcom/helpshift/conversation/dto/IssueState;->RESOLUTION_ACCEPTED:Lcom/helpshift/conversation/dto/IssueState;

    invoke-virtual {p0, p1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->updateIssueStatus(Lcom/helpshift/conversation/dto/IssueState;)V

    .line 948
    iget-object p1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->domain:Lcom/helpshift/common/domain/Domain;

    invoke-virtual {p1}, Lcom/helpshift/common/domain/Domain;->getAnalyticsEventDM()Lcom/helpshift/analytics/domainmodel/AnalyticsEventDM;

    move-result-object p1

    sget-object v0, Lcom/helpshift/analytics/AnalyticsEventType;->RESOLUTION_ACCEPTED:Lcom/helpshift/analytics/AnalyticsEventType;

    iget-object v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->serverId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/helpshift/analytics/domainmodel/AnalyticsEventDM;->pushEvent(Lcom/helpshift/analytics/AnalyticsEventType;Ljava/lang/String;)V

    .line 951
    iget-object p1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->domain:Lcom/helpshift/common/domain/Domain;

    invoke-virtual {p1}, Lcom/helpshift/common/domain/Domain;->getDelegate()Lcom/helpshift/delegate/UIThreadDelegateDecorator;

    move-result-object p1

    const-string v0, "User accepted the solution"

    invoke-virtual {p1, v0}, Lcom/helpshift/delegate/UIThreadDelegateDecorator;->userRepliedToConversation(Ljava/lang/String;)V

    goto :goto_0

    .line 955
    :cond_0
    new-instance p1, Lcom/helpshift/conversation/activeconversation/message/ConfirmationRejectedMessageDM;

    const/4 v7, 0x1

    const-string v2, "Did not accept the solution"

    const-string v6, "mobile"

    move-object v1, p1

    invoke-direct/range {v1 .. v7}, Lcom/helpshift/conversation/activeconversation/message/ConfirmationRejectedMessageDM;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;I)V

    .line 957
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localId:Ljava/lang/Long;

    iput-object v0, p1, Lcom/helpshift/conversation/activeconversation/message/ConfirmationRejectedMessageDM;->conversationLocalId:Ljava/lang/Long;

    .line 959
    invoke-direct {p0, p1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->addMessageToDbAndUI(Lcom/helpshift/conversation/activeconversation/message/MessageDM;)V

    .line 962
    new-instance v0, Lcom/helpshift/conversation/activeconversation/ConversationDM$5;

    invoke-direct {v0, p0, p1}, Lcom/helpshift/conversation/activeconversation/ConversationDM$5;-><init>(Lcom/helpshift/conversation/activeconversation/ConversationDM;Lcom/helpshift/conversation/activeconversation/message/ConfirmationRejectedMessageDM;)V

    invoke-direct {p0, v0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->sendMessageWithAutoRetry(Lcom/helpshift/common/domain/F;)V

    .line 979
    sget-object p1, Lcom/helpshift/conversation/dto/IssueState;->RESOLUTION_REJECTED:Lcom/helpshift/conversation/dto/IssueState;

    invoke-virtual {p0, p1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->updateIssueStatus(Lcom/helpshift/conversation/dto/IssueState;)V

    .line 982
    iget-object p1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->domain:Lcom/helpshift/common/domain/Domain;

    invoke-virtual {p1}, Lcom/helpshift/common/domain/Domain;->getAnalyticsEventDM()Lcom/helpshift/analytics/domainmodel/AnalyticsEventDM;

    move-result-object p1

    sget-object v0, Lcom/helpshift/analytics/AnalyticsEventType;->RESOLUTION_REJECTED:Lcom/helpshift/analytics/AnalyticsEventType;

    iget-object v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->serverId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/helpshift/analytics/domainmodel/AnalyticsEventDM;->pushEvent(Lcom/helpshift/analytics/AnalyticsEventType;Ljava/lang/String;)V

    .line 985
    iget-object p1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->domain:Lcom/helpshift/common/domain/Domain;

    invoke-virtual {p1}, Lcom/helpshift/common/domain/Domain;->getDelegate()Lcom/helpshift/delegate/UIThreadDelegateDecorator;

    move-result-object p1

    const-string v0, "User rejected the solution"

    invoke-virtual {p1, v0}, Lcom/helpshift/delegate/UIThreadDelegateDecorator;->userRepliedToConversation(Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method public markMessagesAsSeen()V
    .locals 5

    .line 1101
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->conversationDAO:Lcom/helpshift/conversation/dao/ConversationDAO;

    iget-object v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localId:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-interface {v0, v1, v2}, Lcom/helpshift/conversation/dao/ConversationDAO;->readMessages(J)Ljava/util/List;

    move-result-object v0

    .line 1102
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 1103
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/helpshift/conversation/activeconversation/message/MessageDM;

    .line 1104
    iget v3, v2, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->deliveryState:I

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    .line 1105
    sget-object v3, Lcom/helpshift/conversation/activeconversation/ConversationDM$11;->$SwitchMap$com$helpshift$conversation$activeconversation$message$MessageType:[I

    iget-object v4, v2, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->messageType:Lcom/helpshift/conversation/activeconversation/message/MessageType;

    invoke-virtual {v4}, Lcom/helpshift/conversation/activeconversation/message/MessageType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    goto :goto_0

    .line 1116
    :pswitch_0
    iget-object v2, v2, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->localId:Ljava/lang/Long;

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1122
    :cond_1
    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v0

    if-nez v0, :cond_2

    return-void

    .line 1126
    :cond_2
    invoke-virtual {p0, v1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->markSeenMessagesAsRead(Ljava/util/Set;)V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public markSeenMessagesAsRead(Ljava/util/Set;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .line 1130
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-static {v0}, Lcom/helpshift/common/util/HSDateFormatSpec;->getCurrentAdjustedTimeForStorage(Lcom/helpshift/common/platform/Platform;)Ljava/lang/String;

    move-result-object v0

    .line 1132
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1133
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1135
    iget-object v3, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->messageDMs:Lcom/helpshift/common/util/HSObservableList;

    invoke-virtual {v3}, Lcom/helpshift/common/util/HSObservableList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/helpshift/conversation/activeconversation/message/MessageDM;

    .line 1136
    iget-object v5, v4, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->localId:Ljava/lang/Long;

    if-eqz v5, :cond_0

    .line 1137
    iget-object v5, v4, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->localId:Ljava/lang/Long;

    invoke-interface {v1, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1140
    :cond_1
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_2
    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    .line 1141
    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/helpshift/conversation/activeconversation/message/MessageDM;

    if-eqz v3, :cond_2

    .line 1143
    iput-object v0, v3, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->readAt:Ljava/lang/String;

    const/4 v4, 0x1

    .line 1144
    iput v4, v3, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->deliveryState:I

    .line 1145
    iget-object v4, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->messageCursor:Ljava/lang/String;

    iput-object v4, v3, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->seenAtMessageCursor:Ljava/lang/String;

    .line 1146
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1150
    :cond_3
    invoke-static {v2}, Lcom/helpshift/common/ListUtils;->isEmpty(Ljava/util/List;)Z

    move-result p1

    if-eqz p1, :cond_4

    return-void

    .line 1154
    :cond_4
    iget-object p1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-interface {p1}, Lcom/helpshift/common/platform/Platform;->getConversationDAO()Lcom/helpshift/conversation/dao/ConversationDAO;

    move-result-object p1

    invoke-interface {p1, v2}, Lcom/helpshift/conversation/dao/ConversationDAO;->insertOrUpdateMessages(Ljava/util/List;)V

    .line 1155
    invoke-direct {p0, v2}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->markMessagesAsSeen(Ljava/util/List;)V

    return-void
.end method

.method public mergeIssue(Lcom/helpshift/conversation/activeconversation/ConversationDM;ZLcom/helpshift/conversation/activeconversation/ConversationUpdate;)V
    .locals 4

    .line 1047
    iget-object v0, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    .line 1048
    iget-object v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    .line 1049
    sget-object v2, Lcom/helpshift/conversation/activeconversation/ConversationDM$11;->$SwitchMap$com$helpshift$conversation$dto$IssueState:[I

    invoke-virtual {v0}, Lcom/helpshift/conversation/dto/IssueState;->ordinal()I

    move-result v3

    aget v2, v2, v3

    const/4 v3, 0x5

    if-eq v2, v3, :cond_0

    goto :goto_0

    .line 1054
    :cond_0
    iget-object v2, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    sget-object v3, Lcom/helpshift/conversation/dto/IssueState;->RESOLUTION_ACCEPTED:Lcom/helpshift/conversation/dto/IssueState;

    if-eq v2, v3, :cond_1

    iget-object v2, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    sget-object v3, Lcom/helpshift/conversation/dto/IssueState;->RESOLUTION_REJECTED:Lcom/helpshift/conversation/dto/IssueState;

    if-eq v2, v3, :cond_1

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 1062
    :goto_0
    iget-object v1, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->messageCursor:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 1064
    iput-object v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->messageCursor:Ljava/lang/String;

    .line 1066
    :cond_2
    iget-object v1, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->serverId:Ljava/lang/String;

    iput-object v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->serverId:Ljava/lang/String;

    .line 1067
    iget-object v1, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->preConversationServerId:Ljava/lang/String;

    iput-object v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->preConversationServerId:Ljava/lang/String;

    .line 1068
    iget-object v1, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->issueType:Ljava/lang/String;

    iput-object v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->issueType:Ljava/lang/String;

    .line 1069
    iget-object v1, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->title:Ljava/lang/String;

    iput-object v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->title:Ljava/lang/String;

    .line 1070
    iget-boolean v1, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->showAgentName:Z

    iput-boolean v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->showAgentName:Z

    .line 1071
    iget-object v1, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->publishId:Ljava/lang/String;

    iput-object v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->publishId:Ljava/lang/String;

    .line 1072
    iget-object v1, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->createdAt:Ljava/lang/String;

    iput-object v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->createdAt:Ljava/lang/String;

    .line 1073
    iget-wide v1, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->epochCreatedAtTime:J

    iput-wide v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->epochCreatedAtTime:J

    .line 1074
    iget-boolean v1, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isRedacted:Z

    iput-boolean v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isRedacted:Z

    .line 1075
    iget-object v1, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->updatedAt:Ljava/lang/String;

    iput-object v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->updatedAt:Ljava/lang/String;

    .line 1077
    iget-object v1, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->csatState:Lcom/helpshift/conversation/states/ConversationCSATState;

    sget-object v2, Lcom/helpshift/conversation/states/ConversationCSATState;->SUBMITTED_SYNCED:Lcom/helpshift/conversation/states/ConversationCSATState;

    if-ne v1, v2, :cond_3

    .line 1078
    iget-object v1, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->csatState:Lcom/helpshift/conversation/states/ConversationCSATState;

    iput-object v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->csatState:Lcom/helpshift/conversation/states/ConversationCSATState;

    .line 1080
    :cond_3
    iput-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    .line 1081
    iget-object p1, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->messageDMs:Lcom/helpshift/common/util/HSObservableList;

    invoke-virtual {p0, p2, p1, p3}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->updateMessageDMs(ZLjava/util/List;Lcom/helpshift/conversation/activeconversation/ConversationUpdate;)V

    return-void
.end method

.method public mergeMessageFromConversationHistory(Ljava/util/List;Lcom/helpshift/conversation/activeconversation/ConversationUpdate;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/helpshift/conversation/activeconversation/message/MessageDM;",
            ">;",
            "Lcom/helpshift/conversation/activeconversation/ConversationUpdate;",
            ")V"
        }
    .end annotation

    .line 1758
    invoke-static {p1}, Lcom/helpshift/common/ListUtils;->isEmpty(Ljava/util/List;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 1759
    invoke-virtual {p0, v0, p1, p2}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->updateMessageDMs(ZLjava/util/List;Lcom/helpshift/conversation/activeconversation/ConversationUpdate;)V

    :cond_0
    return-void
.end method

.method public mergePreIssue(Lcom/helpshift/conversation/activeconversation/ConversationDM;ZLcom/helpshift/conversation/activeconversation/ConversationUpdate;)V
    .locals 3

    .line 1003
    iget-object v0, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    .line 1005
    sget-object v1, Lcom/helpshift/conversation/activeconversation/ConversationDM$11;->$SwitchMap$com$helpshift$conversation$dto$IssueState:[I

    invoke-virtual {v0}, Lcom/helpshift/conversation/dto/IssueState;->ordinal()I

    move-result v2

    aget v1, v1, v2

    const/4 v2, 0x4

    if-eq v1, v2, :cond_1

    const/4 v2, 0x5

    if-eq v1, v2, :cond_0

    goto :goto_0

    .line 1015
    :cond_0
    sget-object v0, Lcom/helpshift/conversation/dto/IssueState;->RESOLUTION_ACCEPTED:Lcom/helpshift/conversation/dto/IssueState;

    goto :goto_0

    .line 1007
    :cond_1
    sget-object v0, Lcom/helpshift/conversation/dto/IssueState;->COMPLETED_ISSUE_CREATED:Lcom/helpshift/conversation/dto/IssueState;

    .line 1009
    iget-object v1, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->serverId:Ljava/lang/String;

    iput-object v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->serverId:Ljava/lang/String;

    .line 1022
    :goto_0
    iget-object v1, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->messageCursor:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 1024
    iput-object v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->messageCursor:Ljava/lang/String;

    .line 1026
    :cond_2
    iget-object v1, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->preConversationServerId:Ljava/lang/String;

    iput-object v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->preConversationServerId:Ljava/lang/String;

    .line 1027
    iget-object v1, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->serverId:Ljava/lang/String;

    iput-object v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->serverId:Ljava/lang/String;

    .line 1028
    iget-object v1, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->issueType:Ljava/lang/String;

    iput-object v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->issueType:Ljava/lang/String;

    .line 1029
    iget-object v1, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->title:Ljava/lang/String;

    iput-object v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->title:Ljava/lang/String;

    .line 1030
    iget-boolean v1, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->showAgentName:Z

    iput-boolean v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->showAgentName:Z

    .line 1031
    iget-object v1, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->publishId:Ljava/lang/String;

    iput-object v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->publishId:Ljava/lang/String;

    .line 1032
    iget-object v1, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->createdAt:Ljava/lang/String;

    iput-object v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->createdAt:Ljava/lang/String;

    .line 1033
    iget-wide v1, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->epochCreatedAtTime:J

    iput-wide v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->epochCreatedAtTime:J

    .line 1034
    iget-object v1, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->updatedAt:Ljava/lang/String;

    iput-object v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->updatedAt:Ljava/lang/String;

    .line 1035
    iput-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    .line 1036
    iget-object p1, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->messageDMs:Lcom/helpshift/common/util/HSObservableList;

    invoke-virtual {p0, p2, p1, p3}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->updateMessageDMs(ZLjava/util/List;Lcom/helpshift/conversation/activeconversation/ConversationUpdate;)V

    return-void
.end method

.method public refreshConversationOnIssueStateUpdate()V
    .locals 4

    .line 338
    sget-object v0, Lcom/helpshift/conversation/activeconversation/ConversationDM$11;->$SwitchMap$com$helpshift$conversation$dto$IssueState:[I

    iget-object v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    invoke-virtual {v1}, Lcom/helpshift/conversation/dto/IssueState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    goto :goto_2

    .line 359
    :cond_0
    invoke-virtual {p0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->handleConversationEnded()V

    goto :goto_2

    .line 341
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 342
    iget-object v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->conversationDAO:Lcom/helpshift/conversation/dao/ConversationDAO;

    iget-object v2, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localId:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-interface {v1, v2, v3}, Lcom/helpshift/conversation/dao/ConversationDAO;->readMessages(J)Ljava/util/List;

    move-result-object v1

    .line 343
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/helpshift/conversation/activeconversation/message/MessageDM;

    .line 344
    instance-of v3, v2, Lcom/helpshift/conversation/activeconversation/message/UserMessageDM;

    if-eqz v3, :cond_2

    iget-object v3, v2, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->serverId:Ljava/lang/String;

    if-nez v3, :cond_2

    .line 345
    check-cast v2, Lcom/helpshift/conversation/activeconversation/message/UserMessageDM;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 348
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 349
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/helpshift/conversation/activeconversation/message/UserMessageDM;

    .line 350
    iget-object v2, v2, Lcom/helpshift/conversation/activeconversation/message/UserMessageDM;->body:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "\n"

    .line 351
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 353
    :cond_4
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-interface {v0}, Lcom/helpshift/common/platform/Platform;->getConversationInboxDAO()Lcom/helpshift/conversation/dao/ConversationInboxDAO;

    move-result-object v0

    iget-object v2, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-virtual {v2}, Lcom/helpshift/account/domainmodel/UserDM;->getLocalId()Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 354
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 353
    invoke-interface {v0, v2, v3, v1}, Lcom/helpshift/conversation/dao/ConversationInboxDAO;->saveConversationArchivalPrefillText(JLjava/lang/String;)V

    .line 355
    invoke-virtual {p0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->handleConversationEnded()V

    .line 362
    :goto_2
    invoke-virtual {p0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->updateMessagesOnIssueStatusUpdate()V

    return-void
.end method

.method registerMessagesObserver()V
    .locals 2

    .line 166
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->messageDMs:Lcom/helpshift/common/util/HSObservableList;

    invoke-virtual {v0}, Lcom/helpshift/common/util/HSObservableList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/helpshift/conversation/activeconversation/message/MessageDM;

    .line 167
    invoke-virtual {v1, p0}, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->addObserver(Ljava/util/Observer;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public retryMessage(Lcom/helpshift/conversation/activeconversation/message/MessageDM;)V
    .locals 1

    .line 172
    instance-of v0, p1, Lcom/helpshift/conversation/activeconversation/message/UserMessageDM;

    if-eqz v0, :cond_0

    .line 173
    check-cast p1, Lcom/helpshift/conversation/activeconversation/message/UserMessageDM;

    invoke-direct {p0, p1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->sendTextMessage(Lcom/helpshift/conversation/activeconversation/message/UserMessageDM;)V

    goto :goto_0

    .line 175
    :cond_0
    instance-of v0, p1, Lcom/helpshift/conversation/activeconversation/message/ScreenshotMessageDM;

    if-eqz v0, :cond_1

    .line 176
    check-cast p1, Lcom/helpshift/conversation/activeconversation/message/ScreenshotMessageDM;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->sendScreenshotMessageInternal(Lcom/helpshift/conversation/activeconversation/message/ScreenshotMessageDM;Z)V

    :cond_1
    :goto_0
    return-void
.end method

.method public retryMessages(Z)V
    .locals 9

    .line 1467
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->conversationDAO:Lcom/helpshift/conversation/dao/ConversationDAO;

    iget-object v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localId:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-interface {v0, v1, v2}, Lcom/helpshift/conversation/dao/ConversationDAO;->readMessages(J)Ljava/util/List;

    move-result-object v0

    .line 1468
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1469
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1470
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 1471
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1472
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/helpshift/conversation/activeconversation/message/MessageDM;

    .line 1473
    instance-of v6, v5, Lcom/helpshift/conversation/activeconversation/message/AutoRetriableMessageDM;

    if-eqz v6, :cond_1

    move-object v6, v5

    check-cast v6, Lcom/helpshift/conversation/activeconversation/message/AutoRetriableMessageDM;

    .line 1474
    invoke-virtual {v6}, Lcom/helpshift/conversation/activeconversation/message/AutoRetriableMessageDM;->isRetriable()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1475
    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1477
    :cond_1
    iget-object v6, v5, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->readAt:Ljava/lang/String;

    invoke-static {v6}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_2

    iget-boolean v6, v5, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->isMessageSeenSynced:Z

    if-nez v6, :cond_2

    .line 1478
    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1480
    :cond_2
    instance-of v6, v5, Lcom/helpshift/conversation/activeconversation/message/RequestAppReviewMessageDM;

    if-eqz v6, :cond_3

    .line 1481
    iget-object v6, v5, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->serverId:Ljava/lang/String;

    move-object v7, v5

    check-cast v7, Lcom/helpshift/conversation/activeconversation/message/RequestAppReviewMessageDM;

    invoke-interface {v3, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1483
    :cond_3
    instance-of v6, v5, Lcom/helpshift/conversation/activeconversation/message/FAQListMessageDM;

    if-eqz v6, :cond_0

    .line 1484
    check-cast v5, Lcom/helpshift/conversation/activeconversation/message/FAQListMessageDM;

    .line 1485
    invoke-virtual {v5}, Lcom/helpshift/conversation/activeconversation/message/FAQListMessageDM;->isSuggestionsReadEventPending()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1486
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1492
    :cond_4
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_5
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_c

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/helpshift/conversation/activeconversation/message/AutoRetriableMessageDM;

    .line 1494
    iget-object v5, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    sget-object v6, Lcom/helpshift/conversation/dto/IssueState;->ARCHIVED:Lcom/helpshift/conversation/dto/IssueState;

    if-eq v5, v6, :cond_b

    iget-object v5, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    sget-object v6, Lcom/helpshift/conversation/dto/IssueState;->AUTHOR_MISMATCH:Lcom/helpshift/conversation/dto/IssueState;

    if-ne v5, v6, :cond_6

    goto :goto_2

    .line 1499
    :cond_6
    :try_start_0
    iget-object v5, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->domain:Lcom/helpshift/common/domain/Domain;

    iget-object v6, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-virtual {v1, v5, v6}, Lcom/helpshift/conversation/activeconversation/message/AutoRetriableMessageDM;->setDependencies(Lcom/helpshift/common/domain/Domain;Lcom/helpshift/common/platform/Platform;)V

    .line 1500
    iget-object v5, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-virtual {v1, v5, p0}, Lcom/helpshift/conversation/activeconversation/message/AutoRetriableMessageDM;->send(Lcom/helpshift/account/domainmodel/UserDM;Lcom/helpshift/conversation/activeconversation/ConversationServerInfo;)V

    .line 1501
    instance-of v5, v1, Lcom/helpshift/conversation/activeconversation/message/AcceptedAppReviewMessageDM;

    if-eqz v5, :cond_5

    .line 1502
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1505
    move-object v6, v1

    check-cast v6, Lcom/helpshift/conversation/activeconversation/message/AcceptedAppReviewMessageDM;

    .line 1506
    iget-object v7, v6, Lcom/helpshift/conversation/activeconversation/message/AcceptedAppReviewMessageDM;->referredMessageId:Ljava/lang/String;

    .line 1507
    invoke-interface {v3, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 1508
    invoke-interface {v3, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/helpshift/conversation/activeconversation/message/RequestAppReviewMessageDM;

    .line 1509
    iget-object v8, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-virtual {v7, v8}, Lcom/helpshift/conversation/activeconversation/message/RequestAppReviewMessageDM;->handleAcceptedReviewSuccess(Lcom/helpshift/common/platform/Platform;)V

    .line 1512
    invoke-interface {v5, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_7
    if-eqz p1, :cond_5

    .line 1518
    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1519
    invoke-virtual {p0, v6}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->addMessageToUI(Lcom/helpshift/conversation/activeconversation/message/MessageDM;)V

    const/4 v1, 0x1

    const/4 v6, 0x0

    .line 1520
    invoke-virtual {p0, v1, v5, v6}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->updateMessageDMs(ZLjava/util/List;Lcom/helpshift/conversation/activeconversation/ConversationUpdate;)V
    :try_end_0
    .catch Lcom/helpshift/common/exception/RootAPIException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    .line 1525
    iget-object v5, v1, Lcom/helpshift/common/exception/RootAPIException;->exceptionType:Lcom/helpshift/common/exception/ExceptionType;

    sget-object v6, Lcom/helpshift/common/exception/NetworkException;->CONVERSATION_ARCHIVED:Lcom/helpshift/common/exception/NetworkException;

    if-ne v5, v6, :cond_8

    .line 1526
    sget-object v1, Lcom/helpshift/conversation/dto/IssueState;->ARCHIVED:Lcom/helpshift/conversation/dto/IssueState;

    invoke-virtual {p0, v1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->updateIssueStatus(Lcom/helpshift/conversation/dto/IssueState;)V

    goto :goto_1

    .line 1528
    :cond_8
    iget-object v5, v1, Lcom/helpshift/common/exception/RootAPIException;->exceptionType:Lcom/helpshift/common/exception/ExceptionType;

    sget-object v6, Lcom/helpshift/common/exception/NetworkException;->USER_PRE_CONDITION_FAILED:Lcom/helpshift/common/exception/NetworkException;

    if-ne v5, v6, :cond_9

    .line 1529
    sget-object v1, Lcom/helpshift/conversation/dto/IssueState;->AUTHOR_MISMATCH:Lcom/helpshift/conversation/dto/IssueState;

    invoke-virtual {p0, v1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->updateIssueStatus(Lcom/helpshift/conversation/dto/IssueState;)V

    goto :goto_1

    .line 1531
    :cond_9
    iget-object v5, v1, Lcom/helpshift/common/exception/RootAPIException;->exceptionType:Lcom/helpshift/common/exception/ExceptionType;

    sget-object v6, Lcom/helpshift/common/exception/NetworkException;->NON_RETRIABLE:Lcom/helpshift/common/exception/NetworkException;

    if-ne v5, v6, :cond_a

    goto :goto_1

    .line 1532
    :cond_a
    throw v1

    :cond_b
    :goto_2
    return-void

    .line 1539
    :cond_c
    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    .line 1540
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_e

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/helpshift/conversation/activeconversation/message/MessageDM;

    .line 1541
    iget-object v2, v1, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->readAt:Ljava/lang/String;

    .line 1542
    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    if-nez v3, :cond_d

    .line 1544
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1546
    :cond_d
    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1547
    invoke-interface {p1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    .line 1551
    :cond_e
    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_4
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_10

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1553
    :try_start_1
    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-direct {p0, v1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->markMessagesAsSeen(Ljava/util/List;)V
    :try_end_1
    .catch Lcom/helpshift/common/exception/RootAPIException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_4

    :catch_1
    move-exception v1

    .line 1556
    iget-object v2, v1, Lcom/helpshift/common/exception/RootAPIException;->exceptionType:Lcom/helpshift/common/exception/ExceptionType;

    sget-object v3, Lcom/helpshift/common/exception/NetworkException;->NON_RETRIABLE:Lcom/helpshift/common/exception/NetworkException;

    if-ne v2, v3, :cond_f

    goto :goto_4

    .line 1557
    :cond_f
    throw v1

    .line 1563
    :cond_10
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_5
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_11

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/helpshift/conversation/activeconversation/message/FAQListMessageDM;

    .line 1564
    iget-object v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->domain:Lcom/helpshift/common/domain/Domain;

    iget-object v2, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-virtual {v0, v1, v2}, Lcom/helpshift/conversation/activeconversation/message/FAQListMessageDM;->setDependencies(Lcom/helpshift/common/domain/Domain;Lcom/helpshift/common/platform/Platform;)V

    .line 1565
    iget-object v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-virtual {v0, p0, v1}, Lcom/helpshift/conversation/activeconversation/message/FAQListMessageDM;->sendSuggestionReadEvent(Lcom/helpshift/conversation/activeconversation/ConversationServerInfo;Lcom/helpshift/account/domainmodel/UserDM;)V

    goto :goto_5

    :cond_11
    return-void
.end method

.method public sendCSATSurvey(ILjava/lang/String;)V
    .locals 1

    const/4 v0, 0x5

    if-le p1, v0, :cond_0

    const/4 p1, 0x5

    goto :goto_0

    :cond_0
    if-gez p1, :cond_1

    const/4 p1, 0x0

    .line 1220
    :cond_1
    :goto_0
    iput p1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->csatRating:I

    if-eqz p2, :cond_2

    .line 1222
    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p2

    .line 1224
    :cond_2
    iput-object p2, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->csatFeedback:Ljava/lang/String;

    .line 1225
    sget-object p1, Lcom/helpshift/conversation/states/ConversationCSATState;->SUBMITTED_NOT_SYNCED:Lcom/helpshift/conversation/states/ConversationCSATState;

    invoke-direct {p0, p1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->setCSATState(Lcom/helpshift/conversation/states/ConversationCSATState;)V

    .line 1228
    new-instance p1, Lcom/helpshift/conversation/activeconversation/ConversationDM$6;

    invoke-direct {p1, p0}, Lcom/helpshift/conversation/activeconversation/ConversationDM$6;-><init>(Lcom/helpshift/conversation/activeconversation/ConversationDM;)V

    invoke-direct {p0, p1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->sendMessageWithAutoRetry(Lcom/helpshift/common/domain/F;)V

    .line 1236
    iget-object p1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->domain:Lcom/helpshift/common/domain/Domain;

    invoke-virtual {p1}, Lcom/helpshift/common/domain/Domain;->getDelegate()Lcom/helpshift/delegate/UIThreadDelegateDecorator;

    move-result-object p1

    iget p2, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->csatRating:I

    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->csatFeedback:Ljava/lang/String;

    invoke-virtual {p1, p2, v0}, Lcom/helpshift/delegate/UIThreadDelegateDecorator;->userCompletedCustomerSatisfactionSurvey(ILjava/lang/String;)V

    return-void
.end method

.method public sendCSATSurveyInternal()V
    .locals 8

    .line 1240
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/issues/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->serverId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "/customer-survey/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1242
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-static {v0}, Lcom/helpshift/common/domain/network/NetworkDataRequestUtil;->getUserRequestData(Lcom/helpshift/account/domainmodel/UserDM;)Ljava/util/HashMap;

    move-result-object v0

    .line 1243
    iget v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->csatRating:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "rating"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1244
    iget-object v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->csatFeedback:Ljava/lang/String;

    const-string v2, "feedback"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1249
    new-instance v3, Lcom/helpshift/common/domain/network/POSTNetwork;

    iget-object v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->domain:Lcom/helpshift/common/domain/Domain;

    iget-object v2, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-direct {v3, v6, v1, v2}, Lcom/helpshift/common/domain/network/POSTNetwork;-><init>(Ljava/lang/String;Lcom/helpshift/common/domain/Domain;Lcom/helpshift/common/platform/Platform;)V

    .line 1250
    new-instance v5, Lcom/helpshift/common/domain/idempotent/SuccessOrNonRetriableStatusCodeIdempotentPolicy;

    invoke-direct {v5}, Lcom/helpshift/common/domain/idempotent/SuccessOrNonRetriableStatusCodeIdempotentPolicy;-><init>()V

    .line 1251
    new-instance v1, Lcom/helpshift/common/domain/network/IdempotentNetwork;

    iget-object v4, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->platform:Lcom/helpshift/common/platform/Platform;

    iget-object v7, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->serverId:Ljava/lang/String;

    move-object v2, v1

    invoke-direct/range {v2 .. v7}, Lcom/helpshift/common/domain/network/IdempotentNetwork;-><init>(Lcom/helpshift/common/domain/network/Network;Lcom/helpshift/common/platform/Platform;Lcom/helpshift/common/domain/idempotent/IdempotentPolicy;Ljava/lang/String;Ljava/lang/String;)V

    .line 1252
    new-instance v2, Lcom/helpshift/common/domain/network/TSCorrectedNetwork;

    iget-object v3, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-direct {v2, v1, v3}, Lcom/helpshift/common/domain/network/TSCorrectedNetwork;-><init>(Lcom/helpshift/common/domain/network/Network;Lcom/helpshift/common/platform/Platform;)V

    .line 1253
    new-instance v1, Lcom/helpshift/common/domain/network/FailedAPICallNetworkDecorator;

    invoke-direct {v1, v2}, Lcom/helpshift/common/domain/network/FailedAPICallNetworkDecorator;-><init>(Lcom/helpshift/common/domain/network/Network;)V

    .line 1254
    new-instance v2, Lcom/helpshift/common/domain/network/GuardOKNetwork;

    invoke-direct {v2, v1}, Lcom/helpshift/common/domain/network/GuardOKNetwork;-><init>(Lcom/helpshift/common/domain/network/Network;)V

    const/4 v1, 0x0

    .line 1258
    :try_start_0
    new-instance v3, Lcom/helpshift/common/platform/network/RequestData;

    invoke-direct {v3, v0}, Lcom/helpshift/common/platform/network/RequestData;-><init>(Ljava/util/Map;)V

    .line 1259
    invoke-interface {v2, v3}, Lcom/helpshift/common/domain/network/Network;->makeRequest(Lcom/helpshift/common/platform/network/RequestData;)Lcom/helpshift/common/platform/network/Response;

    .line 1260
    sget-object v0, Lcom/helpshift/conversation/states/ConversationCSATState;->SUBMITTED_SYNCED:Lcom/helpshift/conversation/states/ConversationCSATState;
    :try_end_0
    .catch Lcom/helpshift/common/exception/RootAPIException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 1277
    invoke-direct {p0, v0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->setCSATState(Lcom/helpshift/conversation/states/ConversationCSATState;)V

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    goto :goto_1

    :catch_0
    move-exception v0

    .line 1265
    :try_start_1
    iget-object v2, v0, Lcom/helpshift/common/exception/RootAPIException;->exceptionType:Lcom/helpshift/common/exception/ExceptionType;

    sget-object v3, Lcom/helpshift/common/exception/NetworkException;->INVALID_AUTH_TOKEN:Lcom/helpshift/common/exception/NetworkException;

    if-eq v2, v3, :cond_1

    iget-object v2, v0, Lcom/helpshift/common/exception/RootAPIException;->exceptionType:Lcom/helpshift/common/exception/ExceptionType;

    sget-object v3, Lcom/helpshift/common/exception/NetworkException;->AUTH_TOKEN_NOT_PROVIDED:Lcom/helpshift/common/exception/NetworkException;

    if-eq v2, v3, :cond_1

    .line 1269
    iget-object v2, v0, Lcom/helpshift/common/exception/RootAPIException;->exceptionType:Lcom/helpshift/common/exception/ExceptionType;

    sget-object v3, Lcom/helpshift/common/exception/NetworkException;->NON_RETRIABLE:Lcom/helpshift/common/exception/NetworkException;

    if-ne v2, v3, :cond_2

    .line 1270
    sget-object v1, Lcom/helpshift/conversation/states/ConversationCSATState;->SUBMITTED_SYNCED:Lcom/helpshift/conversation/states/ConversationCSATState;

    goto :goto_0

    .line 1267
    :cond_1
    iget-object v2, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->domain:Lcom/helpshift/common/domain/Domain;

    invoke-virtual {v2}, Lcom/helpshift/common/domain/Domain;->getAuthenticationFailureDM()Lcom/helpshift/account/AuthenticationFailureDM;

    move-result-object v2

    iget-object v3, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    iget-object v4, v0, Lcom/helpshift/common/exception/RootAPIException;->exceptionType:Lcom/helpshift/common/exception/ExceptionType;

    invoke-virtual {v2, v3, v4}, Lcom/helpshift/account/AuthenticationFailureDM;->notifyAuthenticationFailure(Lcom/helpshift/account/domainmodel/UserDM;Lcom/helpshift/common/exception/ExceptionType;)V

    .line 1272
    :cond_2
    :goto_0
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_1
    if-eqz v1, :cond_3

    .line 1277
    invoke-direct {p0, v1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->setCSATState(Lcom/helpshift/conversation/states/ConversationCSATState;)V

    :cond_3
    throw v0
.end method

.method public sendConversationEndedDelegateForPreIssue()V
    .locals 2

    .line 996
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    sget-object v1, Lcom/helpshift/conversation/dto/IssueState;->RESOLUTION_ACCEPTED:Lcom/helpshift/conversation/dto/IssueState;

    if-ne v0, v1, :cond_0

    .line 997
    invoke-virtual {p0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->handleConversationEnded()V

    :cond_0
    return-void
.end method

.method public sendConversationPostedEvent(Lcom/helpshift/conversation/activeconversation/ConversationDM;)V
    .locals 2

    .line 990
    iget-object v0, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    sget-object v1, Lcom/helpshift/conversation/dto/IssueState;->COMPLETED_ISSUE_CREATED:Lcom/helpshift/conversation/dto/IssueState;

    if-ne v0, v1, :cond_0

    iget-object v0, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    iget-object v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    if-eq v0, v1, :cond_0

    .line 991
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->domain:Lcom/helpshift/common/domain/Domain;

    invoke-virtual {v0}, Lcom/helpshift/common/domain/Domain;->getAnalyticsEventDM()Lcom/helpshift/analytics/domainmodel/AnalyticsEventDM;

    move-result-object v0

    sget-object v1, Lcom/helpshift/analytics/AnalyticsEventType;->CONVERSATION_POSTED:Lcom/helpshift/analytics/AnalyticsEventType;

    iget-object p1, p1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->serverId:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/helpshift/analytics/domainmodel/AnalyticsEventDM;->pushEvent(Lcom/helpshift/analytics/AnalyticsEventType;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public sendOptionInputMessage(Lcom/helpshift/conversation/activeconversation/message/OptionInputMessageDM;Lcom/helpshift/conversation/activeconversation/message/input/OptionInput$Option;Z)V
    .locals 9

    .line 195
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-static {v0}, Lcom/helpshift/common/util/HSDateFormatSpec;->getCurrentAdjustedTimeForStorage(Lcom/helpshift/common/platform/Platform;)Ljava/lang/String;

    move-result-object v3

    .line 196
    invoke-static {v3}, Lcom/helpshift/common/util/HSDateFormatSpec;->convertToEpochTime(Ljava/lang/String;)J

    move-result-wide v4

    if-eqz p3, :cond_0

    .line 200
    iget-object p2, p1, Lcom/helpshift/conversation/activeconversation/message/OptionInputMessageDM;->input:Lcom/helpshift/conversation/activeconversation/message/input/OptionInput;

    iget-object p2, p2, Lcom/helpshift/conversation/activeconversation/message/input/OptionInput;->skipLabel:Ljava/lang/String;

    goto :goto_0

    .line 204
    :cond_0
    iget-object p2, p2, Lcom/helpshift/conversation/activeconversation/message/input/OptionInput$Option;->title:Ljava/lang/String;

    :goto_0
    move-object v2, p2

    .line 207
    new-instance p2, Lcom/helpshift/conversation/activeconversation/message/UserResponseMessageForOptionInput;

    const-string v6, "mobile"

    move-object v1, p2

    move-object v7, p1

    move v8, p3

    invoke-direct/range {v1 .. v8}, Lcom/helpshift/conversation/activeconversation/message/UserResponseMessageForOptionInput;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Lcom/helpshift/conversation/activeconversation/message/OptionInputMessageDM;Z)V

    .line 210
    iget-object p3, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localId:Ljava/lang/Long;

    iput-object p3, p2, Lcom/helpshift/conversation/activeconversation/message/UserResponseMessageForOptionInput;->conversationLocalId:Ljava/lang/Long;

    const/4 p3, 0x1

    .line 214
    invoke-virtual {p2, p3}, Lcom/helpshift/conversation/activeconversation/message/UserResponseMessageForOptionInput;->updateState(Z)V

    .line 215
    invoke-direct {p0, p2}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->addMessageToDbAndUI(Lcom/helpshift/conversation/activeconversation/message/MessageDM;)V

    .line 216
    invoke-direct {p0, p1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->deleteOptionsForAdminMessageWithOptionsInput(Lcom/helpshift/conversation/activeconversation/message/OptionInputMessageDM;)V

    .line 217
    invoke-direct {p0, p2}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->sendTextMessage(Lcom/helpshift/conversation/activeconversation/message/UserMessageDM;)V

    return-void
.end method

.method public sendScreenshot(Lcom/helpshift/conversation/dto/ImagePickerFile;Ljava/lang/String;)V
    .locals 13

    .line 801
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-static {v0}, Lcom/helpshift/common/util/HSDateFormatSpec;->getCurrentAdjustedTimeForStorage(Lcom/helpshift/common/platform/Platform;)Ljava/lang/String;

    move-result-object v3

    .line 802
    invoke-static {v3}, Lcom/helpshift/common/util/HSDateFormatSpec;->convertToEpochTime(Ljava/lang/String;)J

    move-result-wide v4

    .line 803
    new-instance v0, Lcom/helpshift/conversation/activeconversation/message/ScreenshotMessageDM;

    const/4 v2, 0x0

    const-string v6, "mobile"

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object v1, v0

    invoke-direct/range {v1 .. v12}, Lcom/helpshift/conversation/activeconversation/message/ScreenshotMessageDM;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)V

    .line 812
    iget-object v1, p1, Lcom/helpshift/conversation/dto/ImagePickerFile;->originalFileName:Ljava/lang/String;

    iput-object v1, v0, Lcom/helpshift/conversation/activeconversation/message/ScreenshotMessageDM;->fileName:Ljava/lang/String;

    .line 813
    iget-object v1, p1, Lcom/helpshift/conversation/dto/ImagePickerFile;->filePath:Ljava/lang/String;

    iput-object v1, v0, Lcom/helpshift/conversation/activeconversation/message/ScreenshotMessageDM;->filePath:Ljava/lang/String;

    .line 814
    invoke-virtual {v0, p2}, Lcom/helpshift/conversation/activeconversation/message/ScreenshotMessageDM;->setRefersMessageId(Ljava/lang/String;)V

    .line 815
    invoke-virtual {p0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->shouldEnableMessagesClick()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/helpshift/conversation/activeconversation/message/ScreenshotMessageDM;->updateState(Z)V

    .line 816
    iget-object v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localId:Ljava/lang/Long;

    iput-object v1, v0, Lcom/helpshift/conversation/activeconversation/message/ScreenshotMessageDM;->conversationLocalId:Ljava/lang/Long;

    .line 817
    invoke-direct {p0, v0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->addMessageToDbAndUI(Lcom/helpshift/conversation/activeconversation/message/MessageDM;)V

    const/4 v1, 0x1

    if-eqz p2, :cond_1

    .line 819
    iget-object v2, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->messageDMs:Lcom/helpshift/common/util/HSObservableList;

    invoke-virtual {v2}, Lcom/helpshift/common/util/HSObservableList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/helpshift/conversation/activeconversation/message/MessageDM;

    .line 820
    iget-object v4, v3, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->serverId:Ljava/lang/String;

    if-eqz v4, :cond_0

    iget-object v4, v3, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->serverId:Ljava/lang/String;

    .line 821
    invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, v3, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->messageType:Lcom/helpshift/conversation/activeconversation/message/MessageType;

    sget-object v5, Lcom/helpshift/conversation/activeconversation/message/MessageType;->REQUESTED_SCREENSHOT:Lcom/helpshift/conversation/activeconversation/message/MessageType;

    if-ne v4, v5, :cond_0

    .line 823
    check-cast v3, Lcom/helpshift/conversation/activeconversation/message/RequestScreenshotMessageDM;

    .line 824
    iget-object p2, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-virtual {v3, p2, v1}, Lcom/helpshift/conversation/activeconversation/message/RequestScreenshotMessageDM;->setIsAnswered(Lcom/helpshift/common/platform/Platform;Z)V

    .line 829
    :cond_1
    iget-boolean p1, p1, Lcom/helpshift/conversation/dto/ImagePickerFile;->isFileCompressionAndCopyingDone:Z

    xor-int/2addr p1, v1

    invoke-direct {p0, v0, p1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->sendScreenshotMessageInternal(Lcom/helpshift/conversation/activeconversation/message/ScreenshotMessageDM;Z)V

    return-void
.end method

.method public sendTextMessage(Ljava/lang/String;)V
    .locals 7

    .line 230
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-static {v0}, Lcom/helpshift/common/util/HSDateFormatSpec;->getCurrentAdjustedTimeForStorage(Lcom/helpshift/common/platform/Platform;)Ljava/lang/String;

    move-result-object v3

    .line 231
    invoke-static {v3}, Lcom/helpshift/common/util/HSDateFormatSpec;->convertToEpochTime(Ljava/lang/String;)J

    move-result-wide v4

    .line 232
    new-instance v0, Lcom/helpshift/conversation/activeconversation/message/UserMessageDM;

    const-string v6, "mobile"

    move-object v1, v0

    move-object v2, p1

    invoke-direct/range {v1 .. v6}, Lcom/helpshift/conversation/activeconversation/message/UserMessageDM;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V

    .line 233
    iget-object p1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localId:Ljava/lang/Long;

    iput-object p1, v0, Lcom/helpshift/conversation/activeconversation/message/UserMessageDM;->conversationLocalId:Ljava/lang/Long;

    .line 234
    invoke-virtual {p0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->shouldEnableMessagesClick()Z

    move-result p1

    invoke-virtual {v0, p1}, Lcom/helpshift/conversation/activeconversation/message/UserMessageDM;->updateState(Z)V

    .line 237
    invoke-direct {p0, v0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->addMessageToDbAndUI(Lcom/helpshift/conversation/activeconversation/message/MessageDM;)V

    .line 240
    invoke-direct {p0, v0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->sendTextMessage(Lcom/helpshift/conversation/activeconversation/message/UserMessageDM;)V

    return-void
.end method

.method public sendTextMessage(Ljava/lang/String;Lcom/helpshift/conversation/activeconversation/message/AdminMessageWithTextInputDM;Z)V
    .locals 9

    .line 181
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-static {v0}, Lcom/helpshift/common/util/HSDateFormatSpec;->getCurrentAdjustedTimeForStorage(Lcom/helpshift/common/platform/Platform;)Ljava/lang/String;

    move-result-object v3

    .line 182
    invoke-static {v3}, Lcom/helpshift/common/util/HSDateFormatSpec;->convertToEpochTime(Ljava/lang/String;)J

    move-result-wide v4

    .line 183
    new-instance v0, Lcom/helpshift/conversation/activeconversation/message/UserResponseMessageForTextInputDM;

    const-string v6, "mobile"

    move-object v1, v0

    move-object v2, p1

    move-object v7, p2

    move v8, p3

    invoke-direct/range {v1 .. v8}, Lcom/helpshift/conversation/activeconversation/message/UserResponseMessageForTextInputDM;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Lcom/helpshift/conversation/activeconversation/message/AdminMessageWithTextInputDM;Z)V

    .line 185
    iget-object p1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localId:Ljava/lang/Long;

    iput-object p1, v0, Lcom/helpshift/conversation/activeconversation/message/UserResponseMessageForTextInputDM;->conversationLocalId:Ljava/lang/Long;

    const/4 p1, 0x1

    .line 189
    invoke-virtual {v0, p1}, Lcom/helpshift/conversation/activeconversation/message/UserResponseMessageForTextInputDM;->updateState(Z)V

    .line 190
    invoke-direct {p0, v0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->addMessageToDbAndUI(Lcom/helpshift/conversation/activeconversation/message/MessageDM;)V

    .line 191
    invoke-direct {p0, v0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->sendTextMessage(Lcom/helpshift/conversation/activeconversation/message/UserMessageDM;)V

    return-void
.end method

.method public setCSATData(ILcom/helpshift/conversation/states/ConversationCSATState;Ljava/lang/String;)V
    .locals 0

    .line 466
    iput p1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->csatRating:I

    .line 467
    iput-object p2, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->csatState:Lcom/helpshift/conversation/states/ConversationCSATState;

    .line 468
    iput-object p3, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->csatFeedback:Ljava/lang/String;

    return-void
.end method

.method public setCreatedAt(Ljava/lang/String;)V
    .locals 1

    .line 1915
    invoke-static {p1}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1916
    iput-object p1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->createdAt:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public setDependencies(Lcom/helpshift/common/platform/Platform;Lcom/helpshift/common/domain/Domain;Lcom/helpshift/account/domainmodel/UserDM;)V
    .locals 0

    .line 156
    iput-object p1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->platform:Lcom/helpshift/common/platform/Platform;

    .line 157
    iput-object p2, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->domain:Lcom/helpshift/common/domain/Domain;

    .line 158
    iput-object p3, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->userDM:Lcom/helpshift/account/domainmodel/UserDM;

    .line 159
    invoke-interface {p1}, Lcom/helpshift/common/platform/Platform;->getConversationDAO()Lcom/helpshift/conversation/dao/ConversationDAO;

    move-result-object p1

    iput-object p1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->conversationDAO:Lcom/helpshift/conversation/dao/ConversationDAO;

    .line 160
    invoke-virtual {p2}, Lcom/helpshift/common/domain/Domain;->getMetaDataDM()Lcom/helpshift/meta/MetaDataDM;

    move-result-object p1

    iput-object p1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->metaDataDM:Lcom/helpshift/meta/MetaDataDM;

    .line 161
    invoke-virtual {p2}, Lcom/helpshift/common/domain/Domain;->getSDKConfigurationDM()Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    move-result-object p1

    iput-object p1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->sdkConfigurationDM:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    .line 162
    invoke-virtual {p3}, Lcom/helpshift/account/domainmodel/UserDM;->getLocalId()Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide p1

    iput-wide p1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->userLocalId:J

    return-void
.end method

.method public setEnableMessageClickOnResolutionRejected(Z)V
    .locals 1

    .line 1198
    iput-boolean p1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->enableMessageClickOnResolutionRejected:Z

    .line 1199
    iget-object p1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    sget-object v0, Lcom/helpshift/conversation/dto/IssueState;->RESOLUTION_REJECTED:Lcom/helpshift/conversation/dto/IssueState;

    if-ne p1, v0, :cond_0

    .line 1200
    invoke-virtual {p0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->updateMessagesOnIssueStatusUpdate()V

    :cond_0
    return-void
.end method

.method public setEpochCreatedAtTime(J)V
    .locals 0

    .line 1925
    iput-wide p1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->epochCreatedAtTime:J

    return-void
.end method

.method setListener(Lcom/helpshift/conversation/activeconversation/ConversationDMListener;)V
    .locals 0

    .line 1907
    iput-object p1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->conversationDMListener:Lcom/helpshift/conversation/activeconversation/ConversationDMListener;

    return-void
.end method

.method public setLocalId(J)V
    .locals 3

    .line 371
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localId:Ljava/lang/Long;

    .line 372
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->messageDMs:Lcom/helpshift/common/util/HSObservableList;

    invoke-virtual {v0}, Lcom/helpshift/common/util/HSObservableList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/helpshift/conversation/activeconversation/message/MessageDM;

    .line 373
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->conversationLocalId:Ljava/lang/Long;

    goto :goto_0

    :cond_0
    return-void
.end method

.method public setMessageDMs(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/helpshift/conversation/activeconversation/message/MessageDM;",
            ">;)V"
        }
    .end annotation

    .line 891
    new-instance v0, Lcom/helpshift/common/util/HSObservableList;

    invoke-direct {v0, p1}, Lcom/helpshift/common/util/HSObservableList;-><init>(Ljava/util/List;)V

    iput-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->messageDMs:Lcom/helpshift/common/util/HSObservableList;

    .line 892
    invoke-virtual {p0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->updateStateBasedOnMessages()V

    return-void
.end method

.method public setShouldIncrementMessageCount(ZZ)V
    .locals 1

    .line 1418
    iget-boolean v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->shouldIncrementMessageCount:Z

    if-eq v0, p1, :cond_0

    .line 1419
    iput-boolean p1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->shouldIncrementMessageCount:Z

    if-eqz p2, :cond_0

    .line 1421
    iget-object p1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->conversationDAO:Lcom/helpshift/conversation/dao/ConversationDAO;

    invoke-interface {p1, p0}, Lcom/helpshift/conversation/dao/ConversationDAO;->updateConversationWithoutMessages(Lcom/helpshift/conversation/activeconversation/ConversationDM;)V

    :cond_0
    return-void
.end method

.method public setStartNewConversationButtonClicked(ZZ)V
    .locals 0

    .line 1410
    iput-boolean p1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isStartNewConversationClicked:Z

    if-eqz p2, :cond_0

    .line 1412
    iget-object p1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->conversationDAO:Lcom/helpshift/conversation/dao/ConversationDAO;

    invoke-interface {p1, p0}, Lcom/helpshift/conversation/dao/ConversationDAO;->updateConversationWithoutMessages(Lcom/helpshift/conversation/activeconversation/ConversationDM;)V

    :cond_0
    return-void
.end method

.method public shouldEnableMessagesClick()Z
    .locals 3

    .line 752
    iget-boolean v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isInBetweenBotExecution:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    .line 758
    :cond_0
    invoke-virtual {p0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isIssueInProgress()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    .line 761
    :cond_1
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    sget-object v2, Lcom/helpshift/conversation/dto/IssueState;->RESOLUTION_REQUESTED:Lcom/helpshift/conversation/dto/IssueState;

    if-eq v0, v2, :cond_3

    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    sget-object v2, Lcom/helpshift/conversation/dto/IssueState;->RESOLUTION_ACCEPTED:Lcom/helpshift/conversation/dto/IssueState;

    if-eq v0, v2, :cond_3

    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    sget-object v2, Lcom/helpshift/conversation/dto/IssueState;->ARCHIVED:Lcom/helpshift/conversation/dto/IssueState;

    if-eq v0, v2, :cond_3

    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    sget-object v2, Lcom/helpshift/conversation/dto/IssueState;->REJECTED:Lcom/helpshift/conversation/dto/IssueState;

    if-ne v0, v2, :cond_2

    goto :goto_0

    .line 767
    :cond_2
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    sget-object v2, Lcom/helpshift/conversation/dto/IssueState;->RESOLUTION_REJECTED:Lcom/helpshift/conversation/dto/IssueState;

    if-ne v0, v2, :cond_3

    .line 768
    iget-boolean v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->enableMessageClickOnResolutionRejected:Z

    :cond_3
    :goto_0
    return v1
.end method

.method public shouldOpen()Z
    .locals 4

    .line 485
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->sdkConfigurationDM:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    const-string v1, "conversationalIssueFiling"

    invoke-virtual {v0, v1}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 486
    invoke-virtual {p0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isInPreIssueMode()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->preConversationServerId:Ljava/lang/String;

    .line 487
    invoke-static {v0}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    return v1

    .line 494
    :cond_0
    invoke-virtual {p0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isInPreIssueMode()Z

    move-result v0

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isIssueInProgress()Z

    move-result v0

    if-eqz v0, :cond_1

    return v2

    .line 500
    :cond_1
    iget-boolean v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isRedacted:Z

    if-eqz v0, :cond_2

    goto :goto_2

    .line 504
    :cond_2
    invoke-virtual {p0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isIssueInProgress()Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    sget-object v3, Lcom/helpshift/conversation/dto/IssueState;->RESOLUTION_REQUESTED:Lcom/helpshift/conversation/dto/IssueState;

    if-ne v0, v3, :cond_3

    goto :goto_1

    .line 507
    :cond_3
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    sget-object v3, Lcom/helpshift/conversation/dto/IssueState;->RESOLUTION_ACCEPTED:Lcom/helpshift/conversation/dto/IssueState;

    if-eq v0, v3, :cond_7

    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    sget-object v3, Lcom/helpshift/conversation/dto/IssueState;->RESOLUTION_REJECTED:Lcom/helpshift/conversation/dto/IssueState;

    if-eq v0, v3, :cond_7

    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    sget-object v3, Lcom/helpshift/conversation/dto/IssueState;->ARCHIVED:Lcom/helpshift/conversation/dto/IssueState;

    if-ne v0, v3, :cond_4

    goto :goto_0

    .line 512
    :cond_4
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    sget-object v3, Lcom/helpshift/conversation/dto/IssueState;->REJECTED:Lcom/helpshift/conversation/dto/IssueState;

    if-ne v0, v3, :cond_9

    .line 517
    iget-boolean v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isStartNewConversationClicked:Z

    if-eqz v0, :cond_5

    goto :goto_2

    .line 520
    :cond_5
    invoke-virtual {p0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isInPreIssueMode()Z

    move-result v0

    if-nez v0, :cond_6

    goto :goto_1

    .line 530
    :cond_6
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->conversationDAO:Lcom/helpshift/conversation/dao/ConversationDAO;

    iget-object v3, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localId:Ljava/lang/Long;

    invoke-static {v0, v3}, Lcom/helpshift/conversation/ConversationUtil;->getUserMessageCountForConversationLocalId(Lcom/helpshift/conversation/dao/ConversationDAO;Ljava/lang/Long;)I

    move-result v0

    if-lez v0, :cond_9

    goto :goto_1

    .line 510
    :cond_7
    :goto_0
    iget-boolean v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isStartNewConversationClicked:Z

    xor-int/lit8 v1, v0, 0x1

    goto :goto_2

    :cond_8
    :goto_1
    const/4 v1, 0x1

    :cond_9
    :goto_2
    return v1
.end method

.method public shouldShowCSATInFooter()Z
    .locals 3

    .line 1205
    invoke-virtual {p0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isInPreIssueMode()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    .line 1208
    :cond_0
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->csatState:Lcom/helpshift/conversation/states/ConversationCSATState;

    sget-object v2, Lcom/helpshift/conversation/states/ConversationCSATState;->NONE:Lcom/helpshift/conversation/states/ConversationCSATState;

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->sdkConfigurationDM:Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    const-string v2, "customerSatisfactionSurvey"

    .line 1209
    invoke-virtual {v0, v2}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method public sortMessageDMs()V
    .locals 1

    .line 887
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->messageDMs:Lcom/helpshift/common/util/HSObservableList;

    invoke-virtual {p0, v0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->sortMessageDMs(Ljava/util/List;)V

    return-void
.end method

.method sortMessageDMs(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/helpshift/conversation/activeconversation/message/MessageDM;",
            ">;)V"
        }
    .end annotation

    .line 883
    invoke-static {p1}, Lcom/helpshift/conversation/ConversationUtil;->sortMessagesBasedOnCreatedAt(Ljava/util/List;)V

    return-void
.end method

.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 1

    .line 551
    instance-of p2, p1, Lcom/helpshift/conversation/activeconversation/message/MessageDM;

    if-eqz p2, :cond_0

    .line 552
    check-cast p1, Lcom/helpshift/conversation/activeconversation/message/MessageDM;

    .line 553
    iget-object p2, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->messageDMs:Lcom/helpshift/common/util/HSObservableList;

    invoke-virtual {p2, p1}, Lcom/helpshift/common/util/HSObservableList;->indexOf(Ljava/lang/Object;)I

    move-result p2

    .line 554
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->messageDMs:Lcom/helpshift/common/util/HSObservableList;

    invoke-virtual {v0, p2, p1}, Lcom/helpshift/common/util/HSObservableList;->setAndNotifyObserver(ILjava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method updateAcceptedRequestForReopenMessageDMs(Lcom/helpshift/conversation/activeconversation/message/MessageDM;)V
    .locals 2

    .line 861
    instance-of v0, p1, Lcom/helpshift/conversation/activeconversation/message/RequestForReopenMessageDM;

    if-eqz v0, :cond_0

    .line 862
    move-object v0, p1

    check-cast v0, Lcom/helpshift/conversation/activeconversation/message/RequestForReopenMessageDM;

    .line 864
    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/message/RequestForReopenMessageDM;->isAnswered()Z

    move-result v1

    if-nez v1, :cond_1

    .line 865
    iget-object v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->unansweredRequestForReopenMessageDMs:Ljava/util/Map;

    iget-object p1, p1, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->serverId:Ljava/lang/String;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 868
    :cond_0
    instance-of v0, p1, Lcom/helpshift/conversation/activeconversation/message/FollowupAcceptedMessageDM;

    if-eqz v0, :cond_1

    .line 869
    check-cast p1, Lcom/helpshift/conversation/activeconversation/message/FollowupAcceptedMessageDM;

    iget-object p1, p1, Lcom/helpshift/conversation/activeconversation/message/FollowupAcceptedMessageDM;->referredMessageId:Ljava/lang/String;

    .line 870
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->unansweredRequestForReopenMessageDMs:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 871
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->unansweredRequestForReopenMessageDMs:Ljava/util/Map;

    .line 872
    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/helpshift/conversation/activeconversation/message/RequestForReopenMessageDM;

    .line 873
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->domain:Lcom/helpshift/common/domain/Domain;

    iget-object v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-virtual {p1, v0, v1}, Lcom/helpshift/conversation/activeconversation/message/RequestForReopenMessageDM;->setDependencies(Lcom/helpshift/common/domain/Domain;Lcom/helpshift/common/platform/Platform;)V

    .line 874
    iget-boolean v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->showAgentName:Z

    iput-boolean v0, p1, Lcom/helpshift/conversation/activeconversation/message/RequestForReopenMessageDM;->shouldShowAgentNameForConversation:Z

    const/4 v0, 0x1

    .line 875
    invoke-virtual {p1, v0}, Lcom/helpshift/conversation/activeconversation/message/RequestForReopenMessageDM;->setAnsweredAndNotify(Z)V

    .line 876
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->conversationDAO:Lcom/helpshift/conversation/dao/ConversationDAO;

    invoke-interface {v0, p1}, Lcom/helpshift/conversation/dao/ConversationDAO;->insertOrUpdateMessage(Lcom/helpshift/conversation/activeconversation/message/MessageDM;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public updateIssueStatus(Lcom/helpshift/conversation/dto/IssueState;)V
    .locals 2

    .line 313
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    if-ne v0, p1, :cond_0

    return-void

    .line 316
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Changing conversation status from: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", new status: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", for: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->serverId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Helpshift_ConvDM"

    invoke-static {v1, v0}, Lcom/helpshift/util/HSLogger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 318
    iput-object p1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    .line 320
    invoke-virtual {p0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->refreshConversationOnIssueStateUpdate()V

    .line 323
    iget-object p1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->conversationDAO:Lcom/helpshift/conversation/dao/ConversationDAO;

    invoke-interface {p1, p0}, Lcom/helpshift/conversation/dao/ConversationDAO;->updateConversationWithoutMessages(Lcom/helpshift/conversation/activeconversation/ConversationDM;)V

    .line 326
    iget-object p1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->conversationDMListener:Lcom/helpshift/conversation/activeconversation/ConversationDMListener;

    if-eqz p1, :cond_1

    .line 327
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    invoke-interface {p1, v0}, Lcom/helpshift/conversation/activeconversation/ConversationDMListener;->onIssueStatusChange(Lcom/helpshift/conversation/dto/IssueState;)V

    :cond_1
    return-void
.end method

.method public updateLastUserActivityTime(J)V
    .locals 2

    .line 1625
    iput-wide p1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->lastUserActivityTime:J

    .line 1626
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->conversationDAO:Lcom/helpshift/conversation/dao/ConversationDAO;

    iget-object v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localId:Ljava/lang/Long;

    invoke-interface {v0, v1, p1, p2}, Lcom/helpshift/conversation/dao/ConversationDAO;->updateLastUserActivityTimeInConversation(Ljava/lang/Long;J)V

    return-void
.end method

.method updateMessageDMs(ZLjava/util/List;Lcom/helpshift/conversation/activeconversation/ConversationUpdate;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/List<",
            "Lcom/helpshift/conversation/activeconversation/message/MessageDM;",
            ">;",
            "Lcom/helpshift/conversation/activeconversation/ConversationUpdate;",
            ")V"
        }
    .end annotation

    if-nez p3, :cond_0

    .line 1779
    new-instance p3, Lcom/helpshift/conversation/activeconversation/ConversationUpdate;

    invoke-direct {p3}, Lcom/helpshift/conversation/activeconversation/ConversationUpdate;-><init>()V

    .line 1783
    :cond_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1784
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1785
    invoke-direct {p0, p1, v0, v1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->populateMessageDMLookup(ZLjava/util/Map;Ljava/util/Map;)V

    .line 1787
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1789
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/helpshift/conversation/activeconversation/message/MessageDM;

    .line 1790
    invoke-direct {p0, v3, v0, v1, p3}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->getMessageDMForUpdate(Lcom/helpshift/conversation/activeconversation/message/MessageDM;Ljava/util/Map;Ljava/util/Map;Lcom/helpshift/conversation/activeconversation/ConversationUpdate;)Lcom/helpshift/conversation/activeconversation/message/MessageDM;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 1794
    instance-of v5, v4, Lcom/helpshift/conversation/activeconversation/message/UserMessageDM;

    if-eqz v5, :cond_1

    .line 1795
    invoke-virtual {v4, v3}, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->merge(Lcom/helpshift/conversation/activeconversation/message/MessageDM;)V

    .line 1796
    move-object v3, v4

    check-cast v3, Lcom/helpshift/conversation/activeconversation/message/UserMessageDM;

    sget-object v5, Lcom/helpshift/conversation/activeconversation/message/UserMessageState;->SENT:Lcom/helpshift/conversation/activeconversation/message/UserMessageState;

    invoke-virtual {v3, v5}, Lcom/helpshift/conversation/activeconversation/message/UserMessageDM;->setState(Lcom/helpshift/conversation/activeconversation/message/UserMessageState;)V

    goto :goto_1

    .line 1798
    :cond_1
    instance-of v5, v4, Lcom/helpshift/conversation/activeconversation/message/ScreenshotMessageDM;

    if-eqz v5, :cond_2

    .line 1799
    invoke-virtual {v4, v3}, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->merge(Lcom/helpshift/conversation/activeconversation/message/MessageDM;)V

    .line 1800
    move-object v3, v4

    check-cast v3, Lcom/helpshift/conversation/activeconversation/message/ScreenshotMessageDM;

    sget-object v5, Lcom/helpshift/conversation/activeconversation/message/UserMessageState;->SENT:Lcom/helpshift/conversation/activeconversation/message/UserMessageState;

    invoke-virtual {v3, v5}, Lcom/helpshift/conversation/activeconversation/message/ScreenshotMessageDM;->setState(Lcom/helpshift/conversation/activeconversation/message/UserMessageState;)V

    goto :goto_1

    .line 1803
    :cond_2
    invoke-virtual {v4, v3}, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->mergeAndNotify(Lcom/helpshift/conversation/activeconversation/message/MessageDM;)V

    .line 1805
    :goto_1
    iget-object v3, p3, Lcom/helpshift/conversation/activeconversation/ConversationUpdate;->updatedMessageDMs:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1810
    :cond_3
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1815
    :cond_4
    invoke-static {v2}, Lcom/helpshift/common/ListUtils;->isEmpty(Ljava/util/List;)Z

    move-result p2

    if-eqz p2, :cond_5

    return-void

    .line 1820
    :cond_5
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_2
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/helpshift/conversation/activeconversation/message/MessageDM;

    .line 1821
    iget-object v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->domain:Lcom/helpshift/common/domain/Domain;

    iget-object v3, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-virtual {v0, v1, v3}, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->setDependencies(Lcom/helpshift/common/domain/Domain;Lcom/helpshift/common/platform/Platform;)V

    .line 1822
    iget-object v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localId:Ljava/lang/Long;

    iput-object v1, v0, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->conversationLocalId:Ljava/lang/Long;

    .line 1823
    iget-boolean v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->showAgentName:Z

    iput-boolean v1, v0, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->shouldShowAgentNameForConversation:Z

    .line 1834
    instance-of v1, v0, Lcom/helpshift/conversation/activeconversation/message/UserMessageDM;

    if-eqz v1, :cond_6

    .line 1835
    move-object v1, v0

    check-cast v1, Lcom/helpshift/conversation/activeconversation/message/UserMessageDM;

    sget-object v3, Lcom/helpshift/conversation/activeconversation/message/UserMessageState;->SENT:Lcom/helpshift/conversation/activeconversation/message/UserMessageState;

    invoke-virtual {v1, v3}, Lcom/helpshift/conversation/activeconversation/message/UserMessageDM;->setState(Lcom/helpshift/conversation/activeconversation/message/UserMessageState;)V

    goto :goto_3

    .line 1837
    :cond_6
    instance-of v1, v0, Lcom/helpshift/conversation/activeconversation/message/ScreenshotMessageDM;

    if-eqz v1, :cond_7

    .line 1838
    move-object v1, v0

    check-cast v1, Lcom/helpshift/conversation/activeconversation/message/ScreenshotMessageDM;

    sget-object v3, Lcom/helpshift/conversation/activeconversation/message/UserMessageState;->SENT:Lcom/helpshift/conversation/activeconversation/message/UserMessageState;

    invoke-virtual {v1, v3}, Lcom/helpshift/conversation/activeconversation/message/ScreenshotMessageDM;->setState(Lcom/helpshift/conversation/activeconversation/message/UserMessageState;)V

    .line 1841
    :cond_7
    :goto_3
    invoke-virtual {v0, p0}, Lcom/helpshift/conversation/activeconversation/message/MessageDM;->addObserver(Ljava/util/Observer;)V

    goto :goto_2

    :cond_8
    if-eqz p1, :cond_a

    .line 1846
    invoke-virtual {p0, v2}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->sortMessageDMs(Ljava/util/List;)V

    .line 1847
    iget-boolean p1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isInBetweenBotExecution:Z

    invoke-virtual {p0, v2, p1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->evaluateBotExecutionState(Ljava/util/List;Z)Z

    move-result p1

    iput-boolean p1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->isInBetweenBotExecution:Z

    .line 1850
    iget-object p1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->messageDMs:Lcom/helpshift/common/util/HSObservableList;

    invoke-virtual {p1, v2}, Lcom/helpshift/common/util/HSObservableList;->addAll(Ljava/util/Collection;)Z

    .line 1854
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_4
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_b

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/helpshift/conversation/activeconversation/message/MessageDM;

    .line 1856
    instance-of v0, p2, Lcom/helpshift/conversation/activeconversation/message/AdminImageAttachmentMessageDM;

    if-eqz v0, :cond_9

    .line 1857
    move-object v0, p2

    check-cast v0, Lcom/helpshift/conversation/activeconversation/message/AdminImageAttachmentMessageDM;

    iget-object v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-virtual {v0, v1}, Lcom/helpshift/conversation/activeconversation/message/AdminImageAttachmentMessageDM;->downloadThumbnailImage(Lcom/helpshift/common/platform/Platform;)V

    .line 1859
    :cond_9
    invoke-virtual {p0, p2}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->updateAcceptedRequestForReopenMessageDMs(Lcom/helpshift/conversation/activeconversation/message/MessageDM;)V

    goto :goto_4

    .line 1863
    :cond_a
    iget-object p1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->messageDMs:Lcom/helpshift/common/util/HSObservableList;

    invoke-virtual {p1, v2}, Lcom/helpshift/common/util/HSObservableList;->addAll(Ljava/util/Collection;)Z

    .line 1867
    :cond_b
    iget-object p1, p3, Lcom/helpshift/conversation/activeconversation/ConversationUpdate;->newMessageDMs:Ljava/util/List;

    invoke-interface {p1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1868
    invoke-direct {p0, v2}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->evaluateBotControlMessages(Ljava/util/Collection;)V

    return-void
.end method

.method updateMessageOnConversationUpdate(Lcom/helpshift/conversation/activeconversation/message/MessageDM;Z)V
    .locals 0

    .line 568
    invoke-direct {p0, p1, p2}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->updateMessageClickableState(Lcom/helpshift/conversation/activeconversation/message/MessageDM;Z)V

    .line 569
    instance-of p2, p1, Lcom/helpshift/conversation/activeconversation/message/ScreenshotMessageDM;

    if-eqz p2, :cond_0

    .line 570
    check-cast p1, Lcom/helpshift/conversation/activeconversation/message/ScreenshotMessageDM;

    .line 571
    iget-object p2, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-virtual {p1, p2}, Lcom/helpshift/conversation/activeconversation/message/ScreenshotMessageDM;->checkAndReDownloadImageIfNotExist(Lcom/helpshift/common/platform/Platform;)V

    :cond_0
    return-void
.end method

.method public updateMessagesClickOnBotSwitch(Z)V
    .locals 2

    .line 687
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->messageDMs:Lcom/helpshift/common/util/HSObservableList;

    invoke-virtual {v0}, Lcom/helpshift/common/util/HSObservableList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/helpshift/conversation/activeconversation/message/MessageDM;

    .line 688
    invoke-direct {p0, v1, p1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->updateMessageClickableState(Lcom/helpshift/conversation/activeconversation/message/MessageDM;Z)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method updateMessagesOnIssueStatusUpdate()V
    .locals 3

    .line 559
    invoke-virtual {p0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->shouldEnableMessagesClick()Z

    move-result v0

    .line 560
    iget-object v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->messageDMs:Lcom/helpshift/common/util/HSObservableList;

    invoke-virtual {v1}, Lcom/helpshift/common/util/HSObservableList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/helpshift/conversation/activeconversation/message/MessageDM;

    .line 561
    invoke-virtual {p0, v2, v0}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->updateMessageOnConversationUpdate(Lcom/helpshift/conversation/activeconversation/message/MessageDM;Z)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public updateStateBasedOnMessages()V
    .locals 3

    .line 896
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    sget-object v1, Lcom/helpshift/conversation/dto/IssueState;->RESOLUTION_REQUESTED:Lcom/helpshift/conversation/dto/IssueState;

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->messageDMs:Lcom/helpshift/common/util/HSObservableList;

    if-eqz v0, :cond_3

    .line 897
    invoke-virtual {v0}, Lcom/helpshift/common/util/HSObservableList;->size()I

    move-result v0

    if-lez v0, :cond_3

    .line 898
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->messageDMs:Lcom/helpshift/common/util/HSObservableList;

    invoke-virtual {v0}, Lcom/helpshift/common/util/HSObservableList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    const/4 v1, 0x0

    :goto_0
    if-ltz v0, :cond_1

    .line 900
    iget-object v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->messageDMs:Lcom/helpshift/common/util/HSObservableList;

    .line 901
    invoke-virtual {v1, v0}, Lcom/helpshift/common/util/HSObservableList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/helpshift/conversation/activeconversation/message/MessageDM;

    instance-of v2, v1, Lcom/helpshift/conversation/activeconversation/message/FollowupRejectedMessageDM;

    if-nez v2, :cond_0

    instance-of v2, v1, Lcom/helpshift/conversation/activeconversation/message/RequestForReopenMessageDM;

    if-eqz v2, :cond_1

    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 905
    :cond_1
    instance-of v0, v1, Lcom/helpshift/conversation/activeconversation/message/ConfirmationAcceptedMessageDM;

    if-eqz v0, :cond_2

    .line 906
    sget-object v0, Lcom/helpshift/conversation/dto/IssueState;->RESOLUTION_ACCEPTED:Lcom/helpshift/conversation/dto/IssueState;

    iput-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    goto :goto_1

    .line 908
    :cond_2
    instance-of v0, v1, Lcom/helpshift/conversation/activeconversation/message/ConfirmationRejectedMessageDM;

    if-eqz v0, :cond_3

    .line 909
    sget-object v0, Lcom/helpshift/conversation/dto/IssueState;->RESOLUTION_REJECTED:Lcom/helpshift/conversation/dto/IssueState;

    iput-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->state:Lcom/helpshift/conversation/dto/IssueState;

    :cond_3
    :goto_1
    return-void
.end method
