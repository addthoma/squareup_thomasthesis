.class public Lcom/helpshift/android/commons/downloader/storage/DownloadManagerDbStorage;
.super Ljava/lang/Object;
.source "DownloadManagerDbStorage.java"

# interfaces
.implements Lcom/helpshift/android/commons/downloader/contracts/DownloadManagerStorage;


# static fields
.field private static final DOWNLOAD_MANAGER_DB_KEY:Ljava/lang/String; = "kDownloadManagerCachedFiles"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private context:Landroid/content/Context;

.field private externalStorageDirectoryPath:Ljava/lang/String;

.field private isNoMedia:Z

.field private storage:Lcom/helpshift/android/commons/downloader/contracts/DownloaderKeyValueStorage;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 18
    const-class v0, Lcom/helpshift/android/commons/downloader/storage/DownloadManagerDbStorage;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/helpshift/android/commons/downloader/storage/DownloadManagerDbStorage;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/helpshift/android/commons/downloader/contracts/DownloaderKeyValueStorage;Ljava/lang/String;Z)V
    .locals 0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/helpshift/android/commons/downloader/storage/DownloadManagerDbStorage;->context:Landroid/content/Context;

    .line 30
    iput-object p2, p0, Lcom/helpshift/android/commons/downloader/storage/DownloadManagerDbStorage;->storage:Lcom/helpshift/android/commons/downloader/contracts/DownloaderKeyValueStorage;

    .line 31
    iput-object p3, p0, Lcom/helpshift/android/commons/downloader/storage/DownloadManagerDbStorage;->externalStorageDirectoryPath:Ljava/lang/String;

    .line 32
    iput-boolean p4, p0, Lcom/helpshift/android/commons/downloader/storage/DownloadManagerDbStorage;->isNoMedia:Z

    return-void
.end method

.method private createCacheFile(Ljava/lang/String;)Ljava/io/File;
    .locals 4

    .line 74
    iget-object v0, p0, Lcom/helpshift/android/commons/downloader/storage/DownloadManagerDbStorage;->externalStorageDirectoryPath:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "android.permission.WRITE_EXTERNAL_STORAGE"

    .line 75
    invoke-direct {p0, v0}, Lcom/helpshift/android/commons/downloader/storage/DownloadManagerDbStorage;->isPermissionGranted(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/helpshift/android/commons/downloader/storage/DownloadManagerDbStorage;->externalStorageDirectoryPath:Ljava/lang/String;

    invoke-static {v0}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    goto :goto_0

    .line 79
    :cond_0
    iget-object v0, p0, Lcom/helpshift/android/commons/downloader/storage/DownloadManagerDbStorage;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    .line 82
    :goto_0
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_1

    .line 83
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 86
    :cond_1
    iget-boolean v1, p0, Lcom/helpshift/android/commons/downloader/storage/DownloadManagerDbStorage;->isNoMedia:Z

    if-eqz v1, :cond_2

    :try_start_0
    const-string v1, ".nomedia"

    .line 90
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 91
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_2

    .line 92
    invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    .line 96
    sget-object v2, Lcom/helpshift/android/commons/downloader/storage/DownloadManagerDbStorage;->TAG:Ljava/lang/String;

    const-string v3, "Exception while creating no media file"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_2
    :goto_1
    const/4 v1, 0x0

    .line 101
    invoke-virtual {v0}, Ljava/io/File;->canWrite()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 102
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Support_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v2, "/"

    invoke-virtual {p1, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 103
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    :cond_3
    return-object v1
.end method

.method private isPermissionGranted(Ljava/lang/String;)Z
    .locals 5

    const/4 v0, 0x0

    .line 110
    :try_start_0
    iget-object v1, p0, Lcom/helpshift/android/commons/downloader/storage/DownloadManagerDbStorage;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 111
    iget-object v2, p0, Lcom/helpshift/android/commons/downloader/storage/DownloadManagerDbStorage;->context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 112
    invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0

    :catch_0
    move-exception v1

    .line 115
    sget-object v2, Lcom/helpshift/android/commons/downloader/storage/DownloadManagerDbStorage;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error checking for permission : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v2, p1, v1}, Lcom/helpshift/util/HSLogger;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    return v0
.end method


# virtual methods
.method public getCachedFile(Ljava/lang/String;)Ljava/io/File;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 39
    iget-object v0, p0, Lcom/helpshift/android/commons/downloader/storage/DownloadManagerDbStorage;->storage:Lcom/helpshift/android/commons/downloader/contracts/DownloaderKeyValueStorage;

    const-string v1, "kDownloadManagerCachedFiles"

    invoke-interface {v0, v1}, Lcom/helpshift/android/commons/downloader/contracts/DownloaderKeyValueStorage;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    if-nez v0, :cond_0

    .line 42
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 45
    :cond_0
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 47
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    return-object v3

    .line 51
    :cond_1
    invoke-direct {p0, p1}, Lcom/helpshift/android/commons/downloader/storage/DownloadManagerDbStorage;->createCacheFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 57
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, p1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    iget-object p1, p0, Lcom/helpshift/android/commons/downloader/storage/DownloadManagerDbStorage;->storage:Lcom/helpshift/android/commons/downloader/contracts/DownloaderKeyValueStorage;

    invoke-interface {p1, v1, v0}, Lcom/helpshift/android/commons/downloader/contracts/DownloaderKeyValueStorage;->set(Ljava/lang/String;Ljava/io/Serializable;)Z

    return-object v2

    .line 55
    :cond_2
    new-instance p1, Ljava/io/IOException;

    const-string v0, "Error creating cache file"

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public removeCachedFile(Ljava/lang/String;)V
    .locals 2

    .line 66
    iget-object v0, p0, Lcom/helpshift/android/commons/downloader/storage/DownloadManagerDbStorage;->storage:Lcom/helpshift/android/commons/downloader/contracts/DownloaderKeyValueStorage;

    const-string v1, "kDownloadManagerCachedFiles"

    invoke-interface {v0, v1}, Lcom/helpshift/android/commons/downloader/contracts/DownloaderKeyValueStorage;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 67
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    iget-object p1, p0, Lcom/helpshift/android/commons/downloader/storage/DownloadManagerDbStorage;->storage:Lcom/helpshift/android/commons/downloader/contracts/DownloaderKeyValueStorage;

    invoke-interface {p1, v1, v0}, Lcom/helpshift/android/commons/downloader/contracts/DownloaderKeyValueStorage;->set(Ljava/lang/String;Ljava/io/Serializable;)Z

    return-void
.end method
