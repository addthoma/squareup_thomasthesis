.class public Lcom/helpshift/android/commons/downloader/DownloadRunnable;
.super Ljava/lang/Object;
.source "DownloadRunnable.java"

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field private static final TAG:Ljava/lang/String; = "Helpshift_DownloadRun"


# instance fields
.field private context:Landroid/content/Context;

.field private downloadConfig:Lcom/helpshift/android/commons/downloader/DownloadConfig;

.field private isSecureAttachment:Z

.field private networkAuthDataFetcher:Lcom/helpshift/android/commons/downloader/contracts/NetworkAuthDataFetcher;

.field private onDownloadFinishListener:Lcom/helpshift/android/commons/downloader/contracts/OnDownloadFinishListener;

.field private onProgressChangedListener:Lcom/helpshift/android/commons/downloader/contracts/OnProgressChangedListener;

.field private storage:Lcom/helpshift/android/commons/downloader/contracts/DownloaderKeyValueStorage;

.field private urlString:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/helpshift/android/commons/downloader/contracts/DownloaderKeyValueStorage;Ljava/lang/String;ZLcom/helpshift/android/commons/downloader/DownloadConfig;Lcom/helpshift/android/commons/downloader/contracts/NetworkAuthDataFetcher;Lcom/helpshift/android/commons/downloader/contracts/OnDownloadFinishListener;Lcom/helpshift/android/commons/downloader/contracts/OnProgressChangedListener;)V
    .locals 0

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object p1, p0, Lcom/helpshift/android/commons/downloader/DownloadRunnable;->context:Landroid/content/Context;

    .line 59
    iput-object p2, p0, Lcom/helpshift/android/commons/downloader/DownloadRunnable;->storage:Lcom/helpshift/android/commons/downloader/contracts/DownloaderKeyValueStorage;

    .line 60
    iput-object p3, p0, Lcom/helpshift/android/commons/downloader/DownloadRunnable;->urlString:Ljava/lang/String;

    .line 61
    iput-boolean p4, p0, Lcom/helpshift/android/commons/downloader/DownloadRunnable;->isSecureAttachment:Z

    .line 62
    iput-object p5, p0, Lcom/helpshift/android/commons/downloader/DownloadRunnable;->downloadConfig:Lcom/helpshift/android/commons/downloader/DownloadConfig;

    .line 63
    iput-object p7, p0, Lcom/helpshift/android/commons/downloader/DownloadRunnable;->onDownloadFinishListener:Lcom/helpshift/android/commons/downloader/contracts/OnDownloadFinishListener;

    .line 64
    iput-object p8, p0, Lcom/helpshift/android/commons/downloader/DownloadRunnable;->onProgressChangedListener:Lcom/helpshift/android/commons/downloader/contracts/OnProgressChangedListener;

    .line 65
    iput-object p6, p0, Lcom/helpshift/android/commons/downloader/DownloadRunnable;->networkAuthDataFetcher:Lcom/helpshift/android/commons/downloader/contracts/NetworkAuthDataFetcher;

    return-void
.end method

.method private deleteFile(Ljava/io/File;)V
    .locals 2

    if-nez p1, :cond_0

    return-void

    .line 324
    :cond_0
    :try_start_0
    invoke-virtual {p1}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    const-string v0, "Helpshift_DownloadRun"

    const-string v1, "Exception in deleting file "

    .line 327
    invoke-static {v0, v1, p1}, Lcom/helpshift/util/HSLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method private fixSSLSocketProtocols(Ljavax/net/ssl/HttpsURLConnection;)V
    .locals 4

    .line 263
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-gt v0, v1, :cond_0

    .line 267
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const-string v1, "TLSv1.2"

    .line 268
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 271
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const-string v2, "SSLv3"

    .line 272
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 274
    invoke-virtual {p1}, Ljavax/net/ssl/HttpsURLConnection;->getSSLSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v2

    .line 275
    new-instance v3, Lcom/helpshift/android/commons/downloader/HelpshiftSSLSocketFactory;

    invoke-direct {v3, v2, v0, v1}, Lcom/helpshift/android/commons/downloader/HelpshiftSSLSocketFactory;-><init>(Ljavax/net/ssl/SSLSocketFactory;Ljava/util/List;Ljava/util/List;)V

    .line 276
    invoke-virtual {p1, v3}, Ljavax/net/ssl/HttpsURLConnection;->setSSLSocketFactory(Ljavax/net/ssl/SSLSocketFactory;)V

    :cond_0
    return-void
.end method

.method private getQueryMap(Ljava/lang/String;)Ljava/util/Map;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const-string v0, "&"

    .line 281
    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    .line 282
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 283
    array-length v1, p1

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_1

    aget-object v4, p1, v3

    const-string v5, "="

    .line 284
    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 285
    array-length v5, v4

    const/4 v6, 0x2

    if-ne v5, v6, :cond_0

    .line 286
    aget-object v5, v4, v2

    const/4 v6, 0x1

    .line 287
    aget-object v4, v4, v6

    .line 288
    invoke-interface {v0, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method private join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;
    .locals 3

    if-nez p2, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 300
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x1

    .line 302
    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    goto :goto_1

    .line 307
    :cond_1
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 309
    :goto_1
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 311
    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public run()V
    .locals 22

    move-object/from16 v1, p0

    const-string v2, "Exception in closing download response"

    const-string v3, "route"

    .line 71
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Starting download : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, v1, Lcom/helpshift/android/commons/downloader/DownloadRunnable;->urlString:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Helpshift_DownloadRun"

    invoke-static {v5, v4}, Lcom/helpshift/util/HSLogger;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v4, 0xa

    .line 73
    invoke-static {v4}, Landroid/os/Process;->setThreadPriority(I)V

    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 76
    :try_start_0
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v7
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_17
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_16
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_15
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_14
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_13

    if-nez v7, :cond_10

    .line 81
    :try_start_1
    iget-boolean v7, v1, Lcom/helpshift/android/commons/downloader/DownloadRunnable;->isSecureAttachment:Z

    if-eqz v7, :cond_1

    .line 82
    new-instance v7, Ljava/net/URI;

    iget-object v8, v1, Lcom/helpshift/android/commons/downloader/DownloadRunnable;->urlString:Ljava/lang/String;

    invoke-direct {v7, v8}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    .line 83
    invoke-virtual {v7}, Ljava/net/URI;->getPath()Ljava/lang/String;

    move-result-object v8

    .line 84
    invoke-virtual {v7}, Ljava/net/URI;->getQuery()Ljava/lang/String;

    move-result-object v9

    .line 86
    invoke-direct {v1, v9}, Lcom/helpshift/android/commons/downloader/DownloadRunnable;->getQueryMap(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v9

    const-string v10, "v"

    const-string v11, "1"

    .line 87
    invoke-interface {v9, v10, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v10, "uri"

    .line 88
    invoke-interface {v9, v10, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    iget-object v8, v1, Lcom/helpshift/android/commons/downloader/DownloadRunnable;->networkAuthDataFetcher:Lcom/helpshift/android/commons/downloader/contracts/NetworkAuthDataFetcher;

    invoke-interface {v8, v9}, Lcom/helpshift/android/commons/downloader/contracts/NetworkAuthDataFetcher;->getAuthData(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v8

    .line 92
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 93
    invoke-interface {v8}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/util/Map$Entry;

    .line 94
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v10}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v12, "="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v10}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-interface {v9, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    const-string v8, "&"

    .line 96
    invoke-direct {v1, v8, v9}, Lcom/helpshift/android/commons/downloader/DownloadRunnable;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v14

    .line 98
    new-instance v8, Ljava/net/URI;

    invoke-virtual {v7}, Ljava/net/URI;->getScheme()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v7}, Ljava/net/URI;->getAuthority()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v7}, Ljava/net/URI;->getPath()Ljava/lang/String;

    move-result-object v13

    const/4 v15, 0x0

    move-object v10, v8

    invoke-direct/range {v10 .. v15}, Ljava/net/URI;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 101
    :cond_1
    new-instance v8, Ljava/net/URI;

    iget-object v7, v1, Lcom/helpshift/android/commons/downloader/DownloadRunnable;->urlString:Ljava/lang/String;

    invoke-direct {v8, v7}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    .line 104
    :goto_1
    new-instance v7, Ljava/net/URL;

    invoke-virtual {v8}, Ljava/net/URI;->toASCIIString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    const-string v8, "https"

    .line 107
    invoke-virtual {v7}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 108
    invoke-virtual {v7}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v7

    check-cast v7, Ljavax/net/ssl/HttpsURLConnection;

    .line 109
    move-object v8, v7

    check-cast v8, Ljavax/net/ssl/HttpsURLConnection;

    invoke-direct {v1, v8}, Lcom/helpshift/android/commons/downloader/DownloadRunnable;->fixSSLSocketProtocols(Ljavax/net/ssl/HttpsURLConnection;)V

    goto :goto_2

    .line 112
    :cond_2
    invoke-virtual {v7}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v7

    check-cast v7, Ljava/net/HttpURLConnection;

    .line 115
    :goto_2
    invoke-virtual {v7, v4}, Ljava/net/HttpURLConnection;->setInstanceFollowRedirects(Z)V

    .line 117
    new-instance v8, Lcom/helpshift/android/commons/downloader/storage/DownloadManagerDbStorage;

    iget-object v9, v1, Lcom/helpshift/android/commons/downloader/DownloadRunnable;->context:Landroid/content/Context;

    iget-object v10, v1, Lcom/helpshift/android/commons/downloader/DownloadRunnable;->storage:Lcom/helpshift/android/commons/downloader/contracts/DownloaderKeyValueStorage;

    iget-object v11, v1, Lcom/helpshift/android/commons/downloader/DownloadRunnable;->downloadConfig:Lcom/helpshift/android/commons/downloader/DownloadConfig;

    iget-object v11, v11, Lcom/helpshift/android/commons/downloader/DownloadConfig;->externalStorageDirectoryPath:Ljava/lang/String;

    iget-object v12, v1, Lcom/helpshift/android/commons/downloader/DownloadRunnable;->downloadConfig:Lcom/helpshift/android/commons/downloader/DownloadConfig;

    iget-boolean v12, v12, Lcom/helpshift/android/commons/downloader/DownloadConfig;->isNoMedia:Z

    invoke-direct {v8, v9, v10, v11, v12}, Lcom/helpshift/android/commons/downloader/storage/DownloadManagerDbStorage;-><init>(Landroid/content/Context;Lcom/helpshift/android/commons/downloader/contracts/DownloaderKeyValueStorage;Ljava/lang/String;Z)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_12
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_1} :catch_11
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_10
    .catch Ljava/security/GeneralSecurityException; {:try_start_1 .. :try_end_1} :catch_f
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_13

    .line 125
    :try_start_2
    iget-object v10, v1, Lcom/helpshift/android/commons/downloader/DownloadRunnable;->urlString:Ljava/lang/String;

    invoke-virtual {v8, v10}, Lcom/helpshift/android/commons/downloader/storage/DownloadManagerDbStorage;->getCachedFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v10

    .line 130
    invoke-virtual {v10}, Ljava/io/File;->length()J

    move-result-wide v11

    const-string v13, "Range"

    .line 131
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "bytes="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v14, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v15, "-"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v7, v13, v14}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v13

    const/16 v14, 0x1a0

    if-eq v13, v14, :cond_c

    .line 146
    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v15
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_c
    .catchall {:try_start_2 .. :try_end_2} :catchall_8

    .line 148
    :try_start_3
    iget-object v9, v1, Lcom/helpshift/android/commons/downloader/DownloadRunnable;->downloadConfig:Lcom/helpshift/android/commons/downloader/DownloadConfig;

    iget-boolean v9, v9, Lcom/helpshift/android/commons/downloader/DownloadConfig;->writeToFile:Z
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_a
    .catchall {:try_start_3 .. :try_end_3} :catchall_6

    if-nez v9, :cond_6

    .line 149
    :try_start_4
    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->getHeaderFields()Ljava/util/Map;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v8

    .line 150
    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-object v9, v15

    :cond_3
    :goto_3
    :try_start_5
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_4

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/util/Map$Entry;

    .line 151
    invoke-interface {v10}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v11

    if-eqz v11, :cond_3

    .line 152
    invoke-interface {v10}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    const-string v12, "Content-Encoding"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 153
    invoke-interface {v10}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/util/List;

    invoke-interface {v10, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    const-string v11, "gzip"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 154
    new-instance v10, Ljava/util/zip/GZIPInputStream;

    invoke-direct {v10, v9}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V

    move-object v9, v10

    goto :goto_3

    .line 158
    :cond_4
    new-instance v8, Ljava/io/InputStreamReader;

    invoke-direct {v8, v9}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    .line 159
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 161
    new-instance v11, Ljava/io/BufferedReader;

    invoke-direct {v11, v8}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 163
    :goto_4
    :try_start_6
    invoke-virtual {v11}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_5

    .line 164
    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_4

    :catch_0
    move-exception v0

    move-object v8, v0

    :try_start_7
    const-string v11, "IO Exception while reading response"

    .line 168
    invoke-static {v5, v11, v8}, Lcom/helpshift/util/HSLogger;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 171
    :cond_5
    :try_start_8
    new-instance v8, Lorg/json/JSONObject;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v8, v11}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 172
    iget-object v11, v1, Lcom/helpshift/android/commons/downloader/DownloadRunnable;->onDownloadFinishListener:Lcom/helpshift/android/commons/downloader/contracts/OnDownloadFinishListener;

    iget-object v12, v1, Lcom/helpshift/android/commons/downloader/DownloadRunnable;->urlString:Ljava/lang/String;

    invoke-interface {v11, v4, v12, v8}, Lcom/helpshift/android/commons/downloader/contracts/OnDownloadFinishListener;->onDownloadFinish(ZLjava/lang/String;Ljava/lang/Object;)V
    :try_end_8
    .catch Lorg/json/JSONException; {:try_start_8 .. :try_end_8} :catch_1
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_5

    .line 176
    :catch_1
    :try_start_9
    new-instance v8, Lorg/json/JSONArray;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v8, v11}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 177
    iget-object v11, v1, Lcom/helpshift/android/commons/downloader/DownloadRunnable;->onDownloadFinishListener:Lcom/helpshift/android/commons/downloader/contracts/OnDownloadFinishListener;

    iget-object v12, v1, Lcom/helpshift/android/commons/downloader/DownloadRunnable;->urlString:Ljava/lang/String;

    invoke-interface {v11, v4, v12, v8}, Lcom/helpshift/android/commons/downloader/contracts/OnDownloadFinishListener;->onDownloadFinish(ZLjava/lang/String;Ljava/lang/Object;)V
    :try_end_9
    .catch Lorg/json/JSONException; {:try_start_9 .. :try_end_9} :catch_2
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_3
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_5

    .line 180
    :catch_2
    :try_start_a
    iget-object v8, v1, Lcom/helpshift/android/commons/downloader/DownloadRunnable;->onDownloadFinishListener:Lcom/helpshift/android/commons/downloader/contracts/OnDownloadFinishListener;

    iget-object v11, v1, Lcom/helpshift/android/commons/downloader/DownloadRunnable;->urlString:Ljava/lang/String;

    invoke-interface {v8, v4, v11, v10}, Lcom/helpshift/android/commons/downloader/contracts/OnDownloadFinishListener;->onDownloadFinish(ZLjava/lang/String;Ljava/lang/Object;)V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_3
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    :goto_5
    move-object/from16 v19, v7

    move-object/from16 v21, v9

    const/16 v16, 0x0

    goto/16 :goto_9

    :catchall_0
    move-exception v0

    move-object v4, v0

    move-object/from16 v19, v7

    goto/16 :goto_11

    :catch_3
    move-exception v0

    move-object v4, v0

    move-object/from16 v19, v7

    goto/16 :goto_13

    :catchall_1
    move-exception v0

    move-object v4, v0

    move-object/from16 v19, v7

    move-object v9, v15

    goto/16 :goto_11

    :catch_4
    move-exception v0

    move-object v4, v0

    move-object/from16 v19, v7

    move-object v9, v15

    goto/16 :goto_13

    .line 186
    :cond_6
    :try_start_b
    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->getContentLength()I

    move-result v9

    .line 188
    new-instance v13, Ljava/io/FileOutputStream;

    invoke-direct {v13, v10, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_a
    .catchall {:try_start_b .. :try_end_b} :catchall_6

    const/16 v14, 0x2000

    :try_start_c
    new-array v4, v14, [B
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_9
    .catchall {:try_start_c .. :try_end_c} :catchall_5

    move-object/from16 v19, v7

    const-wide/16 v17, 0x0

    .line 194
    :goto_6
    :try_start_d
    invoke-virtual {v15, v4, v6, v14}, Ljava/io/InputStream;->read([BII)I

    move-result v7

    const/4 v14, -0x1

    if-eq v7, v14, :cond_a

    if-ltz v7, :cond_9

    .line 199
    invoke-virtual {v13, v4, v6, v7}, Ljava/io/FileOutputStream;->write([BII)V

    .line 200
    invoke-virtual {v10}, Ljava/io/File;->length()J

    move-result-wide v6
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_8
    .catchall {:try_start_d .. :try_end_d} :catchall_4

    long-to-float v6, v6

    move-object v7, v15

    int-to-long v14, v9

    add-long/2addr v14, v11

    long-to-float v14, v14

    div-float/2addr v6, v14

    const/high16 v14, 0x42c80000    # 100.0f

    mul-float v6, v6, v14

    float-to-long v14, v6

    cmp-long v6, v14, v17

    if-eqz v6, :cond_8

    .line 204
    :try_start_e
    iget-object v6, v1, Lcom/helpshift/android/commons/downloader/DownloadRunnable;->onProgressChangedListener:Lcom/helpshift/android/commons/downloader/contracts/OnProgressChangedListener;

    if-eqz v6, :cond_7

    .line 205
    iget-object v6, v1, Lcom/helpshift/android/commons/downloader/DownloadRunnable;->onProgressChangedListener:Lcom/helpshift/android/commons/downloader/contracts/OnProgressChangedListener;

    move-object/from16 v20, v4

    iget-object v4, v1, Lcom/helpshift/android/commons/downloader/DownloadRunnable;->urlString:Ljava/lang/String;
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_5
    .catchall {:try_start_e .. :try_end_e} :catchall_2

    move-object/from16 v21, v7

    long-to-int v7, v14

    :try_start_f
    invoke-interface {v6, v4, v7}, Lcom/helpshift/android/commons/downloader/contracts/OnProgressChangedListener;->onProgressChanged(Ljava/lang/String;I)V

    goto :goto_7

    :cond_7
    move-object/from16 v20, v4

    move-object/from16 v21, v7

    :goto_7
    move-wide/from16 v17, v14

    goto :goto_8

    :catchall_2
    move-exception v0

    move-object/from16 v21, v7

    goto/16 :goto_d

    :catch_5
    move-exception v0

    move-object/from16 v21, v7

    goto/16 :goto_f

    :cond_8
    move-object/from16 v20, v4

    move-object/from16 v21, v7

    :goto_8
    move-object/from16 v4, v20

    move-object/from16 v15, v21

    const/4 v6, 0x0

    const/16 v14, 0x2000

    goto :goto_6

    :cond_9
    move-object/from16 v21, v15

    .line 196
    new-instance v4, Ljava/io/EOFException;

    invoke-direct {v4}, Ljava/io/EOFException;-><init>()V

    throw v4

    :cond_a
    move-object/from16 v21, v15

    .line 209
    iget-object v4, v1, Lcom/helpshift/android/commons/downloader/DownloadRunnable;->urlString:Ljava/lang/String;

    invoke-virtual {v8, v4}, Lcom/helpshift/android/commons/downloader/storage/DownloadManagerDbStorage;->removeCachedFile(Ljava/lang/String;)V

    .line 211
    invoke-virtual {v10}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    .line 212
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Download finished : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v7, v1, Lcom/helpshift/android/commons/downloader/DownloadRunnable;->urlString:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/helpshift/util/HSLogger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    iget-object v6, v1, Lcom/helpshift/android/commons/downloader/DownloadRunnable;->onDownloadFinishListener:Lcom/helpshift/android/commons/downloader/contracts/OnDownloadFinishListener;

    iget-object v7, v1, Lcom/helpshift/android/commons/downloader/DownloadRunnable;->urlString:Ljava/lang/String;

    const/4 v8, 0x1

    invoke-interface {v6, v8, v7, v4}, Lcom/helpshift/android/commons/downloader/contracts/OnDownloadFinishListener;->onDownloadFinish(ZLjava/lang/String;Ljava/lang/Object;)V

    .line 214
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_7
    .catchall {:try_start_f .. :try_end_f} :catchall_3

    move-object/from16 v16, v13

    :goto_9
    if-eqz v21, :cond_b

    .line 225
    :try_start_10
    invoke-virtual/range {v21 .. v21}, Ljava/io/InputStream;->close()V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_6
    .catch Ljava/lang/InterruptedException; {:try_start_10 .. :try_end_10} :catch_12
    .catch Ljava/net/MalformedURLException; {:try_start_10 .. :try_end_10} :catch_11
    .catch Ljava/security/GeneralSecurityException; {:try_start_10 .. :try_end_10} :catch_f
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_13

    goto :goto_a

    :catch_6
    move-exception v0

    move-object v4, v0

    .line 228
    :try_start_11
    iget-object v6, v1, Lcom/helpshift/android/commons/downloader/DownloadRunnable;->onDownloadFinishListener:Lcom/helpshift/android/commons/downloader/contracts/OnDownloadFinishListener;

    iget-object v7, v1, Lcom/helpshift/android/commons/downloader/DownloadRunnable;->urlString:Ljava/lang/String;

    const/4 v8, 0x0

    invoke-interface {v6, v8, v7, v4}, Lcom/helpshift/android/commons/downloader/contracts/OnDownloadFinishListener;->onDownloadFinish(ZLjava/lang/String;Ljava/lang/Object;)V

    const/4 v6, 0x1

    new-array v7, v6, [Lcom/helpshift/logger/logmodels/ILogExtrasModel;

    .line 229
    iget-object v6, v1, Lcom/helpshift/android/commons/downloader/DownloadRunnable;->urlString:Ljava/lang/String;

    .line 230
    invoke-static {v3, v6}, Lcom/helpshift/logger/logmodels/LogExtrasModelProvider;->fromString(Ljava/lang/String;Ljava/lang/String;)Lcom/helpshift/logger/logmodels/ILogExtrasModel;

    move-result-object v6

    aput-object v6, v7, v8

    .line 229
    invoke-static {v5, v2, v4, v7}, Lcom/helpshift/util/HSLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;[Lcom/helpshift/logger/logmodels/ILogExtrasModel;)V

    .line 233
    :cond_b
    :goto_a
    invoke-virtual/range {v19 .. v19}, Ljava/net/HttpURLConnection;->disconnect()V

    if-eqz v16, :cond_11

    .line 235
    :goto_b
    invoke-virtual/range {v16 .. v16}, Ljava/io/FileOutputStream;->close()V
    :try_end_11
    .catch Ljava/lang/InterruptedException; {:try_start_11 .. :try_end_11} :catch_12
    .catch Ljava/net/MalformedURLException; {:try_start_11 .. :try_end_11} :catch_11
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_10
    .catch Ljava/security/GeneralSecurityException; {:try_start_11 .. :try_end_11} :catch_f
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_13

    goto/16 :goto_1c

    :catchall_3
    move-exception v0

    goto :goto_d

    :catch_7
    move-exception v0

    goto :goto_f

    :catchall_4
    move-exception v0

    goto :goto_c

    :catch_8
    move-exception v0

    goto :goto_e

    :catchall_5
    move-exception v0

    move-object/from16 v19, v7

    :goto_c
    move-object/from16 v21, v15

    :goto_d
    move-object v4, v0

    move-object/from16 v16, v13

    move-object/from16 v9, v21

    goto/16 :goto_16

    :catch_9
    move-exception v0

    move-object/from16 v19, v7

    :goto_e
    move-object/from16 v21, v15

    :goto_f
    move-object v4, v0

    move-object/from16 v16, v13

    move-object/from16 v9, v21

    goto :goto_14

    :catchall_6
    move-exception v0

    move-object/from16 v19, v7

    move-object/from16 v21, v15

    move-object v4, v0

    move-object/from16 v9, v21

    goto :goto_11

    :catch_a
    move-exception v0

    move-object/from16 v19, v7

    move-object/from16 v21, v15

    move-object v4, v0

    move-object/from16 v9, v21

    goto :goto_13

    :cond_c
    move-object/from16 v19, v7

    .line 140
    :try_start_12
    iget-object v4, v1, Lcom/helpshift/android/commons/downloader/DownloadRunnable;->urlString:Ljava/lang/String;

    invoke-virtual {v8, v4}, Lcom/helpshift/android/commons/downloader/storage/DownloadManagerDbStorage;->removeCachedFile(Ljava/lang/String;)V

    .line 141
    invoke-direct {v1, v10}, Lcom/helpshift/android/commons/downloader/DownloadRunnable;->deleteFile(Ljava/io/File;)V

    .line 142
    new-instance v4, Ljava/io/IOException;

    const-string v6, "Requested Range Not Satisfiable, failed with 416 status"

    invoke-direct {v4, v6}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_b
    .catchall {:try_start_12 .. :try_end_12} :catchall_7

    :catchall_7
    move-exception v0

    goto :goto_10

    :catch_b
    move-exception v0

    goto :goto_12

    :catchall_8
    move-exception v0

    move-object/from16 v19, v7

    :goto_10
    move-object v4, v0

    const/4 v9, 0x0

    :goto_11
    const/16 v16, 0x0

    goto :goto_16

    :catch_c
    move-exception v0

    move-object/from16 v19, v7

    :goto_12
    move-object v4, v0

    const/4 v9, 0x0

    :goto_13
    const/16 v16, 0x0

    .line 218
    :goto_14
    :try_start_13
    iget-object v6, v1, Lcom/helpshift/android/commons/downloader/DownloadRunnable;->onDownloadFinishListener:Lcom/helpshift/android/commons/downloader/contracts/OnDownloadFinishListener;

    iget-object v7, v1, Lcom/helpshift/android/commons/downloader/DownloadRunnable;->urlString:Ljava/lang/String;

    const/4 v8, 0x0

    invoke-interface {v6, v8, v7, v4}, Lcom/helpshift/android/commons/downloader/contracts/OnDownloadFinishListener;->onDownloadFinish(ZLjava/lang/String;Ljava/lang/Object;)V

    const-string v6, "Exception in download"

    const/4 v7, 0x1

    new-array v8, v7, [Lcom/helpshift/logger/logmodels/ILogExtrasModel;

    .line 219
    iget-object v7, v1, Lcom/helpshift/android/commons/downloader/DownloadRunnable;->urlString:Ljava/lang/String;

    .line 220
    invoke-static {v3, v7}, Lcom/helpshift/logger/logmodels/LogExtrasModelProvider;->fromString(Ljava/lang/String;Ljava/lang/String;)Lcom/helpshift/logger/logmodels/ILogExtrasModel;

    move-result-object v7

    const/4 v10, 0x0

    aput-object v7, v8, v10

    .line 219
    invoke-static {v5, v6, v4, v8}, Lcom/helpshift/util/HSLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;[Lcom/helpshift/logger/logmodels/ILogExtrasModel;)V
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_9

    if-eqz v9, :cond_d

    .line 225
    :try_start_14
    invoke-virtual {v9}, Ljava/io/InputStream;->close()V
    :try_end_14
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_14} :catch_d
    .catch Ljava/lang/InterruptedException; {:try_start_14 .. :try_end_14} :catch_12
    .catch Ljava/net/MalformedURLException; {:try_start_14 .. :try_end_14} :catch_11
    .catch Ljava/security/GeneralSecurityException; {:try_start_14 .. :try_end_14} :catch_f
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_14} :catch_13

    goto :goto_15

    :catch_d
    move-exception v0

    move-object v4, v0

    .line 228
    :try_start_15
    iget-object v6, v1, Lcom/helpshift/android/commons/downloader/DownloadRunnable;->onDownloadFinishListener:Lcom/helpshift/android/commons/downloader/contracts/OnDownloadFinishListener;

    iget-object v7, v1, Lcom/helpshift/android/commons/downloader/DownloadRunnable;->urlString:Ljava/lang/String;

    const/4 v8, 0x0

    invoke-interface {v6, v8, v7, v4}, Lcom/helpshift/android/commons/downloader/contracts/OnDownloadFinishListener;->onDownloadFinish(ZLjava/lang/String;Ljava/lang/Object;)V

    const/4 v6, 0x1

    new-array v7, v6, [Lcom/helpshift/logger/logmodels/ILogExtrasModel;

    .line 229
    iget-object v6, v1, Lcom/helpshift/android/commons/downloader/DownloadRunnable;->urlString:Ljava/lang/String;

    .line 230
    invoke-static {v3, v6}, Lcom/helpshift/logger/logmodels/LogExtrasModelProvider;->fromString(Ljava/lang/String;Ljava/lang/String;)Lcom/helpshift/logger/logmodels/ILogExtrasModel;

    move-result-object v6

    aput-object v6, v7, v8

    .line 229
    invoke-static {v5, v2, v4, v7}, Lcom/helpshift/util/HSLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;[Lcom/helpshift/logger/logmodels/ILogExtrasModel;)V

    .line 233
    :cond_d
    :goto_15
    invoke-virtual/range {v19 .. v19}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_15
    .catch Ljava/lang/InterruptedException; {:try_start_15 .. :try_end_15} :catch_12
    .catch Ljava/net/MalformedURLException; {:try_start_15 .. :try_end_15} :catch_11
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_15} :catch_10
    .catch Ljava/security/GeneralSecurityException; {:try_start_15 .. :try_end_15} :catch_f
    .catch Ljava/lang/Exception; {:try_start_15 .. :try_end_15} :catch_13

    if-eqz v16, :cond_11

    goto/16 :goto_b

    :catchall_9
    move-exception v0

    move-object v4, v0

    :goto_16
    if-eqz v9, :cond_e

    .line 225
    :try_start_16
    invoke-virtual {v9}, Ljava/io/InputStream;->close()V
    :try_end_16
    .catch Ljava/io/IOException; {:try_start_16 .. :try_end_16} :catch_e
    .catch Ljava/lang/InterruptedException; {:try_start_16 .. :try_end_16} :catch_12
    .catch Ljava/net/MalformedURLException; {:try_start_16 .. :try_end_16} :catch_11
    .catch Ljava/security/GeneralSecurityException; {:try_start_16 .. :try_end_16} :catch_f
    .catch Ljava/lang/Exception; {:try_start_16 .. :try_end_16} :catch_13

    goto :goto_17

    :catch_e
    move-exception v0

    move-object v6, v0

    .line 228
    :try_start_17
    iget-object v7, v1, Lcom/helpshift/android/commons/downloader/DownloadRunnable;->onDownloadFinishListener:Lcom/helpshift/android/commons/downloader/contracts/OnDownloadFinishListener;

    iget-object v8, v1, Lcom/helpshift/android/commons/downloader/DownloadRunnable;->urlString:Ljava/lang/String;

    const/4 v9, 0x0

    invoke-interface {v7, v9, v8, v6}, Lcom/helpshift/android/commons/downloader/contracts/OnDownloadFinishListener;->onDownloadFinish(ZLjava/lang/String;Ljava/lang/Object;)V

    const/4 v7, 0x1

    new-array v8, v7, [Lcom/helpshift/logger/logmodels/ILogExtrasModel;

    .line 229
    iget-object v7, v1, Lcom/helpshift/android/commons/downloader/DownloadRunnable;->urlString:Ljava/lang/String;

    .line 230
    invoke-static {v3, v7}, Lcom/helpshift/logger/logmodels/LogExtrasModelProvider;->fromString(Ljava/lang/String;Ljava/lang/String;)Lcom/helpshift/logger/logmodels/ILogExtrasModel;

    move-result-object v7

    aput-object v7, v8, v9

    .line 229
    invoke-static {v5, v2, v6, v8}, Lcom/helpshift/util/HSLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;[Lcom/helpshift/logger/logmodels/ILogExtrasModel;)V

    .line 233
    :cond_e
    :goto_17
    invoke-virtual/range {v19 .. v19}, Ljava/net/HttpURLConnection;->disconnect()V

    if-eqz v16, :cond_f

    .line 235
    invoke-virtual/range {v16 .. v16}, Ljava/io/FileOutputStream;->close()V

    :cond_f
    throw v4

    :catch_f
    move-exception v0

    move-object v2, v0

    const/4 v7, 0x0

    goto :goto_18

    :catch_10
    move-exception v0

    move-object v2, v0

    const/4 v7, 0x0

    goto :goto_19

    :catch_11
    move-exception v0

    move-object v2, v0

    const/4 v7, 0x0

    goto :goto_1a

    :catch_12
    move-exception v0

    move-object v2, v0

    const/4 v7, 0x0

    goto/16 :goto_1b

    .line 77
    :cond_10
    new-instance v2, Ljava/lang/InterruptedException;

    invoke-direct {v2}, Ljava/lang/InterruptedException;-><init>()V

    throw v2
    :try_end_17
    .catch Ljava/lang/InterruptedException; {:try_start_17 .. :try_end_17} :catch_12
    .catch Ljava/net/MalformedURLException; {:try_start_17 .. :try_end_17} :catch_11
    .catch Ljava/io/IOException; {:try_start_17 .. :try_end_17} :catch_10
    .catch Ljava/security/GeneralSecurityException; {:try_start_17 .. :try_end_17} :catch_f
    .catch Ljava/lang/Exception; {:try_start_17 .. :try_end_17} :catch_13

    :catch_13
    move-exception v0

    move-object v2, v0

    .line 256
    iget-object v4, v1, Lcom/helpshift/android/commons/downloader/DownloadRunnable;->onDownloadFinishListener:Lcom/helpshift/android/commons/downloader/contracts/OnDownloadFinishListener;

    iget-object v6, v1, Lcom/helpshift/android/commons/downloader/DownloadRunnable;->urlString:Ljava/lang/String;

    const/4 v7, 0x0

    invoke-interface {v4, v7, v6, v2}, Lcom/helpshift/android/commons/downloader/contracts/OnDownloadFinishListener;->onDownloadFinish(ZLjava/lang/String;Ljava/lang/Object;)V

    const/4 v4, 0x1

    new-array v4, v4, [Lcom/helpshift/logger/logmodels/ILogExtrasModel;

    .line 257
    iget-object v6, v1, Lcom/helpshift/android/commons/downloader/DownloadRunnable;->urlString:Ljava/lang/String;

    invoke-static {v3, v6}, Lcom/helpshift/logger/logmodels/LogExtrasModelProvider;->fromString(Ljava/lang/String;Ljava/lang/String;)Lcom/helpshift/logger/logmodels/ILogExtrasModel;

    move-result-object v3

    aput-object v3, v4, v7

    const-string v3, "Unknown Exception"

    invoke-static {v5, v3, v2, v4}, Lcom/helpshift/util/HSLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;[Lcom/helpshift/logger/logmodels/ILogExtrasModel;)V

    goto :goto_1c

    :catch_14
    move-exception v0

    const/4 v7, 0x0

    move-object v2, v0

    .line 252
    :goto_18
    iget-object v4, v1, Lcom/helpshift/android/commons/downloader/DownloadRunnable;->onDownloadFinishListener:Lcom/helpshift/android/commons/downloader/contracts/OnDownloadFinishListener;

    iget-object v6, v1, Lcom/helpshift/android/commons/downloader/DownloadRunnable;->urlString:Ljava/lang/String;

    invoke-interface {v4, v7, v6, v2}, Lcom/helpshift/android/commons/downloader/contracts/OnDownloadFinishListener;->onDownloadFinish(ZLjava/lang/String;Ljava/lang/Object;)V

    const/4 v4, 0x1

    new-array v4, v4, [Lcom/helpshift/logger/logmodels/ILogExtrasModel;

    .line 253
    iget-object v6, v1, Lcom/helpshift/android/commons/downloader/DownloadRunnable;->urlString:Ljava/lang/String;

    invoke-static {v3, v6}, Lcom/helpshift/logger/logmodels/LogExtrasModelProvider;->fromString(Ljava/lang/String;Ljava/lang/String;)Lcom/helpshift/logger/logmodels/ILogExtrasModel;

    move-result-object v3

    aput-object v3, v4, v7

    const-string v3, "GeneralSecurityException"

    invoke-static {v5, v3, v2, v4}, Lcom/helpshift/util/HSLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;[Lcom/helpshift/logger/logmodels/ILogExtrasModel;)V

    goto :goto_1c

    :catch_15
    move-exception v0

    const/4 v7, 0x0

    move-object v2, v0

    .line 248
    :goto_19
    iget-object v4, v1, Lcom/helpshift/android/commons/downloader/DownloadRunnable;->onDownloadFinishListener:Lcom/helpshift/android/commons/downloader/contracts/OnDownloadFinishListener;

    iget-object v6, v1, Lcom/helpshift/android/commons/downloader/DownloadRunnable;->urlString:Ljava/lang/String;

    invoke-interface {v4, v7, v6, v2}, Lcom/helpshift/android/commons/downloader/contracts/OnDownloadFinishListener;->onDownloadFinish(ZLjava/lang/String;Ljava/lang/Object;)V

    const/4 v4, 0x1

    new-array v4, v4, [Lcom/helpshift/logger/logmodels/ILogExtrasModel;

    .line 249
    iget-object v6, v1, Lcom/helpshift/android/commons/downloader/DownloadRunnable;->urlString:Ljava/lang/String;

    invoke-static {v3, v6}, Lcom/helpshift/logger/logmodels/LogExtrasModelProvider;->fromString(Ljava/lang/String;Ljava/lang/String;)Lcom/helpshift/logger/logmodels/ILogExtrasModel;

    move-result-object v3

    aput-object v3, v4, v7

    const-string v3, "Exception IO"

    invoke-static {v5, v3, v2, v4}, Lcom/helpshift/util/HSLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;[Lcom/helpshift/logger/logmodels/ILogExtrasModel;)V

    goto :goto_1c

    :catch_16
    move-exception v0

    const/4 v7, 0x0

    move-object v2, v0

    .line 244
    :goto_1a
    iget-object v4, v1, Lcom/helpshift/android/commons/downloader/DownloadRunnable;->onDownloadFinishListener:Lcom/helpshift/android/commons/downloader/contracts/OnDownloadFinishListener;

    iget-object v6, v1, Lcom/helpshift/android/commons/downloader/DownloadRunnable;->urlString:Ljava/lang/String;

    invoke-interface {v4, v7, v6, v2}, Lcom/helpshift/android/commons/downloader/contracts/OnDownloadFinishListener;->onDownloadFinish(ZLjava/lang/String;Ljava/lang/Object;)V

    const/4 v4, 0x1

    new-array v4, v4, [Lcom/helpshift/logger/logmodels/ILogExtrasModel;

    .line 245
    iget-object v6, v1, Lcom/helpshift/android/commons/downloader/DownloadRunnable;->urlString:Ljava/lang/String;

    invoke-static {v3, v6}, Lcom/helpshift/logger/logmodels/LogExtrasModelProvider;->fromString(Ljava/lang/String;Ljava/lang/String;)Lcom/helpshift/logger/logmodels/ILogExtrasModel;

    move-result-object v3

    aput-object v3, v4, v7

    const-string v3, "MalformedURLException"

    invoke-static {v5, v3, v2, v4}, Lcom/helpshift/util/HSLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;[Lcom/helpshift/logger/logmodels/ILogExtrasModel;)V

    goto :goto_1c

    :catch_17
    move-exception v0

    const/4 v7, 0x0

    move-object v2, v0

    .line 240
    :goto_1b
    iget-object v4, v1, Lcom/helpshift/android/commons/downloader/DownloadRunnable;->onDownloadFinishListener:Lcom/helpshift/android/commons/downloader/contracts/OnDownloadFinishListener;

    iget-object v6, v1, Lcom/helpshift/android/commons/downloader/DownloadRunnable;->urlString:Ljava/lang/String;

    invoke-interface {v4, v7, v6, v2}, Lcom/helpshift/android/commons/downloader/contracts/OnDownloadFinishListener;->onDownloadFinish(ZLjava/lang/String;Ljava/lang/Object;)V

    const/4 v4, 0x1

    new-array v4, v4, [Lcom/helpshift/logger/logmodels/ILogExtrasModel;

    .line 241
    iget-object v6, v1, Lcom/helpshift/android/commons/downloader/DownloadRunnable;->urlString:Ljava/lang/String;

    invoke-static {v3, v6}, Lcom/helpshift/logger/logmodels/LogExtrasModelProvider;->fromString(Ljava/lang/String;Ljava/lang/String;)Lcom/helpshift/logger/logmodels/ILogExtrasModel;

    move-result-object v3

    aput-object v3, v4, v7

    const-string v3, "Exception Interrupted"

    invoke-static {v5, v3, v2, v4}, Lcom/helpshift/util/HSLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;[Lcom/helpshift/logger/logmodels/ILogExtrasModel;)V

    :cond_11
    :goto_1c
    return-void
.end method
