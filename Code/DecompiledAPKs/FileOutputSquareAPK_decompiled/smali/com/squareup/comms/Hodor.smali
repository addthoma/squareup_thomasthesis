.class public interface abstract Lcom/squareup/comms/Hodor;
.super Ljava/lang/Object;
.source "Hodor.java"


# virtual methods
.method public abstract activateEGiftCardX2Event(Lcom/squareup/comms/protos/buyer/ActivateEGiftCardX2Event;)V
.end method

.method public abstract appCrashLogEvent(Lcom/squareup/comms/protos/buyer/AppCrashLogEvent;)V
.end method

.method public abstract branBugReport(Lcom/squareup/comms/protos/buyer/BranBugReport;)V
.end method

.method public abstract branCardReaderOnCardInserted(Lcom/squareup/comms/protos/buyer/BranCardReaderOnCardInserted;)V
.end method

.method public abstract branCardReaderOnCardRemoved(Lcom/squareup/comms/protos/buyer/BranCardReaderOnCardRemoved;)V
.end method

.method public abstract branCardReaderOnSecureSessionInvalid(Lcom/squareup/comms/protos/buyer/BranCardReaderOnSecureSessionInvalid;)V
.end method

.method public abstract branCardReaderOnSecureSessionValid(Lcom/squareup/comms/protos/buyer/BranCardReaderOnSecureSessionValid;)V
.end method

.method public abstract branCartMonitorClearSwipeClicked(Lcom/squareup/comms/protos/buyer/BranCartMonitorClearSwipeClicked;)V
.end method

.method public abstract branEmvEventOnAccountSelectionRequired(Lcom/squareup/comms/protos/buyer/BranEmvEventOnAccountSelectionRequired;)V
.end method

.method public abstract branEmvEventOnCardError(Lcom/squareup/comms/protos/buyer/BranEmvEventOnCardError;)V
.end method

.method public abstract branEmvEventOnCardRemovedDuringPayment(Lcom/squareup/comms/protos/buyer/BranEmvEventOnCardRemovedDuringPayment;)V
.end method

.method public abstract branEmvEventOnCardholderNameReceived(Lcom/squareup/comms/protos/buyer/BranEmvEventOnCardholderNameReceived;)V
.end method

.method public abstract branEmvEventOnListApplications(Lcom/squareup/comms/protos/buyer/BranEmvEventOnListApplications;)V
.end method

.method public abstract branEmvEventOnMagFallbackSwipeFailed(Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeFailed;)V
.end method

.method public abstract branEmvEventOnMagFallbackSwipeSuccess(Lcom/squareup/comms/protos/buyer/BranEmvEventOnMagFallbackSwipeSuccess;)V
.end method

.method public abstract branEmvEventOnSendAuthorization(Lcom/squareup/comms/protos/buyer/BranEmvEventOnSendAuthorization;)V
.end method

.method public abstract branEmvEventOnSigRequested(Lcom/squareup/comms/protos/buyer/BranEmvEventOnSigRequested;)V
.end method

.method public abstract branEmvEventOnSwipeForFallback(Lcom/squareup/comms/protos/buyer/BranEmvEventOnSwipeForFallback;)V
.end method

.method public abstract branEmvEventOnUseChipCardDuringFallback(Lcom/squareup/comms/protos/buyer/BranEmvEventOnUseChipCardDuringFallback;)V
.end method

.method public abstract branEmvMonitorOnApplicationSelected(Lcom/squareup/comms/protos/buyer/BranEmvMonitorOnApplicationSelected;)V
.end method

.method public abstract branEmvMonitorOnCardInserted(Lcom/squareup/comms/protos/buyer/BranEmvMonitorOnCardInserted;)V
.end method

.method public abstract branEmvMonitorOnEmvPreparing(Lcom/squareup/comms/protos/buyer/BranEmvMonitorOnEmvPreparing;)V
.end method

.method public abstract branMagSwipeEventOnMagSwipeFailed(Lcom/squareup/comms/protos/buyer/BranMagSwipeEventOnMagSwipeFailed;)V
.end method

.method public abstract branMagSwipeEventOnMagSwipePassthrough(Lcom/squareup/comms/protos/buyer/BranMagSwipeEventOnMagSwipePassthrough;)V
.end method

.method public abstract branMagSwipeEventOnMagSwipeSuccess(Lcom/squareup/comms/protos/buyer/BranMagSwipeEventOnMagSwipeSuccess;)V
.end method

.method public abstract branMagSwipeEventOnUseChipCard(Lcom/squareup/comms/protos/buyer/BranMagSwipeEventOnUseChipCard;)V
.end method

.method public abstract branNfcEventOnNfcActionRequired(Lcom/squareup/comms/protos/buyer/BranNfcEventOnNfcActionRequired;)V
.end method

.method public abstract branNfcEventOnNfcAuthorizationRequestReceived(Lcom/squareup/comms/protos/buyer/BranNfcEventOnNfcAuthorizationRequestReceived;)V
.end method

.method public abstract branNfcEventOnNfcCardBlocked(Lcom/squareup/comms/protos/buyer/BranNfcEventOnNfcCardBlocked;)V
.end method

.method public abstract branNfcEventOnNfcCardDeclined(Lcom/squareup/comms/protos/buyer/BranNfcEventOnNfcCardDeclined;)V
.end method

.method public abstract branNfcEventOnNfcCollisionDetected(Lcom/squareup/comms/protos/buyer/BranNfcEventOnNfcCollisionDetected;)V
.end method

.method public abstract branNfcEventOnNfcInterfaceUnavailable(Lcom/squareup/comms/protos/buyer/BranNfcEventOnNfcInterfaceUnavailable;)V
.end method

.method public abstract branNfcEventOnNfcLimitExceededInsertCard(Lcom/squareup/comms/protos/buyer/BranNfcEventOnNfcLimitExceededInsertCard;)V
.end method

.method public abstract branNfcEventOnNfcLimitExceededTryAnotherCard(Lcom/squareup/comms/protos/buyer/BranNfcEventOnNfcLimitExceededTryAnotherCard;)V
.end method

.method public abstract branNfcEventOnNfcPresentCardAgain(Lcom/squareup/comms/protos/buyer/BranNfcEventOnNfcPresentCardAgain;)V
.end method

.method public abstract branNfcEventOnNfcProcessingError(Lcom/squareup/comms/protos/buyer/BranNfcEventOnNfcProcessingError;)V
.end method

.method public abstract branNfcEventOnNfcRequestTapCard(Lcom/squareup/comms/protos/buyer/BranNfcEventOnNfcRequestTapCard;)V
.end method

.method public abstract branNfcEventOnNfcTimedOut(Lcom/squareup/comms/protos/buyer/BranNfcEventOnNfcTimedOut;)V
.end method

.method public abstract branNfcEventOnNfcTryAnotherCard(Lcom/squareup/comms/protos/buyer/BranNfcEventOnNfcTryAnotherCard;)V
.end method

.method public abstract branNfcEventOnNfcUnlockDevice(Lcom/squareup/comms/protos/buyer/BranNfcEventOnNfcUnlockDevice;)V
.end method

.method public abstract branPaymentCompletionEventOnPaymentApproved(Lcom/squareup/comms/protos/buyer/BranPaymentCompletionEventOnPaymentApproved;)V
.end method

.method public abstract branPaymentCompletionEventOnPaymentDeclined(Lcom/squareup/comms/protos/buyer/BranPaymentCompletionEventOnPaymentDeclined;)V
.end method

.method public abstract branPaymentCompletionEventOnPaymentReversed(Lcom/squareup/comms/protos/buyer/BranPaymentCompletionEventOnPaymentReversed;)V
.end method

.method public abstract branPaymentCompletionEventOnPaymentTerminated(Lcom/squareup/comms/protos/buyer/BranPaymentCompletionEventOnPaymentTerminated;)V
.end method

.method public abstract branPaymentCompletionEventOnPaymentTerminatedDueToSwipe(Lcom/squareup/comms/protos/buyer/BranPaymentCompletionEventOnPaymentTerminatedDueToSwipe;)V
.end method

.method public abstract branSlideshowProgress(Lcom/squareup/comms/protos/buyer/BranSlideshowProgress;)V
.end method

.method public abstract branSpeFirmware(Lcom/squareup/comms/protos/buyer/BranSpeFirmware;)V
.end method

.method public abstract buyerCardReaderInfo(Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;)V
.end method

.method public abstract buyerCreatingCustomer(Lcom/squareup/comms/protos/buyer/BuyerCreatingCustomer;)V
.end method

.method public abstract buyerDevice(Lcom/squareup/comms/protos/buyer/BuyerDevice;)V
.end method

.method public abstract capturePaymentForPasscodeLock(Lcom/squareup/comms/protos/buyer/CapturePaymentForPasscodeLock;)V
.end method

.method public abstract capturePaymentForSleep(Lcom/squareup/comms/protos/buyer/CapturePaymentForSleep;)V
.end method

.method public abstract cardInfo(Lcom/squareup/comms/protos/buyer/CardInfo;)V
.end method

.method public abstract cirqueSecurityEvent(Lcom/squareup/comms/protos/buyer/CirqueSecurityEvent;)V
.end method

.method public abstract claimLoyaltyReward(Lcom/squareup/comms/protos/buyer/ClaimLoyaltyReward;)V
.end method

.method public abstract declineLoyaltyReward(Lcom/squareup/comms/protos/buyer/DeclineLoyaltyReward;)V
.end method

.method public abstract displayCardOnFileEmail(Lcom/squareup/comms/protos/buyer/DisplayCardOnFileEmail;)V
.end method

.method public abstract displayCardOnFilePostalCode(Lcom/squareup/comms/protos/buyer/DisplayCardOnFilePostalCode;)V
.end method

.method public abstract displayPostTransactionConfirmation(Lcom/squareup/comms/protos/buyer/DisplayPostTransactionConfirmation;)V
.end method

.method public abstract doneReviewingCustomerDetails(Lcom/squareup/comms/protos/buyer/DoneReviewingCustomerDetails;)V
.end method

.method public abstract earnRewardsTimedOut(Lcom/squareup/comms/protos/buyer/EarnRewardsTimedOut;)V
.end method

.method public abstract emailDone(Lcom/squareup/comms/protos/buyer/EmailDone;)V
.end method

.method public abstract emvApplication(Lcom/squareup/comms/protos/buyer/EmvApplication;)V
.end method

.method public abstract emvCaptureArgs(Lcom/squareup/comms/protos/buyer/EmvCaptureArgs;)V
.end method

.method public abstract enteringEmail(Lcom/squareup/comms/protos/buyer/EnteringEmail;)V
.end method

.method public abstract enteringPhoneNumber(Lcom/squareup/comms/protos/buyer/EnteringPhoneNumber;)V
.end method

.method public abstract fallbackSwipe(Lcom/squareup/comms/protos/buyer/FallbackSwipe;)V
.end method

.method public abstract imageRequest(Lcom/squareup/comms/protos/buyer/ImageRequest;)V
.end method

.method public abstract instrumentOnFile(Lcom/squareup/comms/protos/buyer/InstrumentOnFile;)V
.end method

.method public abstract logEvents(Lcom/squareup/comms/protos/buyer/LogEvents;)V
.end method

.method public abstract logFirmwareEvent(Lcom/squareup/comms/protos/buyer/LogFirmwareEvent;)V
.end method

.method public abstract logReaderEvent(Lcom/squareup/comms/protos/buyer/LogReaderEvent;)V
.end method

.method public abstract logReaderEventWithTimings(Lcom/squareup/comms/protos/buyer/LogReaderEventWithTimings;)V
.end method

.method public abstract loyaltyCartBannerClicked(Lcom/squareup/comms/protos/buyer/LoyaltyCartBannerClicked;)V
.end method

.method public abstract loyaltyEnrollmentCancelled(Lcom/squareup/comms/protos/buyer/LoyaltyEnrollmentCancelled;)V
.end method

.method public abstract loyaltySendSmsStatusToBuyer(Lcom/squareup/comms/protos/buyer/LoyaltySendSmsStatusToBuyer;)V
.end method

.method public abstract magSwipe(Lcom/squareup/comms/protos/buyer/MagSwipe;)V
.end method

.method public abstract manuallyEnteredCard(Lcom/squareup/comms/protos/buyer/ManuallyEnteredCard;)V
.end method

.method public abstract messageReceived(Lcom/squareup/comms/protos/buyer/MessageReceived;)V
.end method

.method public abstract noReceipt(Lcom/squareup/comms/protos/buyer/NoReceipt;)V
.end method

.method public abstract ohSnapLogEvent(Lcom/squareup/comms/protos/buyer/OhSnapLogEvent;)V
.end method

.method public abstract onActivitySearchEnableContactlessField(Lcom/squareup/comms/protos/buyer/OnActivitySearchEnableContactlessField;)V
.end method

.method public abstract onApplications(Lcom/squareup/comms/protos/buyer/OnApplications;)V
.end method

.method public abstract onBranDisplayEvent(Lcom/squareup/comms/protos/buyer/OnBranDisplayEvent;)V
.end method

.method public abstract onBuyerCanceled(Lcom/squareup/comms/protos/buyer/OnBuyerCanceled;)V
.end method

.method public abstract onBuyerInformationEntered(Lcom/squareup/comms/protos/buyer/OnBuyerInformationEntered;)V
.end method

.method public abstract onCancelConfirmation(Lcom/squareup/comms/protos/buyer/OnCancelConfirmation;)V
.end method

.method public abstract onCancelProcessingDippedCardForCardOnFile(Lcom/squareup/comms/protos/buyer/OnCancelProcessingDippedCardForCardOnFile;)V
.end method

.method public abstract onCancelSaveCardConfirmed(Lcom/squareup/comms/protos/buyer/OnCancelSaveCardConfirmed;)V
.end method

.method public abstract onCardErrorInActivityApplet(Lcom/squareup/comms/protos/buyer/OnCardErrorInActivityApplet;)V
.end method

.method public abstract onCardInsertedInActivityApplet(Lcom/squareup/comms/protos/buyer/OnCardInsertedInActivityApplet;)V
.end method

.method public abstract onCardInsertedInCustomerApplet(Lcom/squareup/comms/protos/buyer/OnCardInsertedInCustomerApplet;)V
.end method

.method public abstract onCardRemovedDuringPayment(Lcom/squareup/comms/protos/buyer/OnCardRemovedDuringPayment;)V
.end method

.method public abstract onCardRemovedInActivityApplet(Lcom/squareup/comms/protos/buyer/OnCardRemovedInActivityApplet;)V
.end method

.method public abstract onCardholderNameReceived(Lcom/squareup/comms/protos/buyer/OnCardholderNameReceived;)V
.end method

.method public abstract onCart(Lcom/squareup/comms/protos/buyer/OnCart;)V
.end method

.method public abstract onCash(Lcom/squareup/comms/protos/buyer/OnCash;)V
.end method

.method public abstract onChipCardSwipedInPlaceOfGiftCard(Lcom/squareup/comms/protos/buyer/OnChipCardSwipedInPlaceOfGiftCard;)V
.end method

.method public abstract onChipInserted(Lcom/squareup/comms/protos/buyer/OnChipInserted;)V
.end method

.method public abstract onChipNotAcceptedTechnicalFallback(Lcom/squareup/comms/protos/buyer/OnChipNotAcceptedTechnicalFallback;)V
.end method

.method public abstract onChipNotAcceptedUnsupportedScheme(Lcom/squareup/comms/protos/buyer/OnChipNotAcceptedUnsupportedScheme;)V
.end method

.method public abstract onChipNotRead(Lcom/squareup/comms/protos/buyer/OnChipNotRead;)V
.end method

.method public abstract onChipRemoved(Lcom/squareup/comms/protos/buyer/OnChipRemoved;)V
.end method

.method public abstract onCoreDump(Lcom/squareup/comms/protos/buyer/OnCoreDump;)V
.end method

.method public abstract onDisplayCustomerDetails(Lcom/squareup/comms/protos/buyer/OnDisplayCustomerDetails;)V
.end method

.method public abstract onEmv(Lcom/squareup/comms/protos/buyer/OnEmv;)V
.end method

.method public abstract onEmvPaymentApproved(Lcom/squareup/comms/protos/buyer/OnEmvPaymentApproved;)V
.end method

.method public abstract onEmvPaymentDeclined(Lcom/squareup/comms/protos/buyer/OnEmvPaymentDeclined;)V
.end method

.method public abstract onEmvPaymentRemoteError(Lcom/squareup/comms/protos/buyer/OnEmvPaymentRemoteError;)V
.end method

.method public abstract onEmvPaymentReversed(Lcom/squareup/comms/protos/buyer/OnEmvPaymentReversed;)V
.end method

.method public abstract onEnteringActivitySearch(Lcom/squareup/comms/protos/buyer/OnEnteringActivitySearch;)V
.end method

.method public abstract onEnteringCustomersApplet(Lcom/squareup/comms/protos/buyer/OnEnteringCustomersApplet;)V
.end method

.method public abstract onEnteringEarnRewards(Lcom/squareup/comms/protos/buyer/OnEnteringEarnRewards;)V
.end method

.method public abstract onEnteringOpenTickets(Lcom/squareup/comms/protos/buyer/OnEnteringOpenTickets;)V
.end method

.method public abstract onEnteringOrderTicket(Lcom/squareup/comms/protos/buyer/OnEnteringOrderTicket;)V
.end method

.method public abstract onEnteringPaymentDone(Lcom/squareup/comms/protos/buyer/OnEnteringPaymentDone;)V
.end method

.method public abstract onEnteringPaymentDoneForPasscodeLock(Lcom/squareup/comms/protos/buyer/OnEnteringPaymentDoneForPasscodeLock;)V
.end method

.method public abstract onEnteringPaymentDoneForSleep(Lcom/squareup/comms/protos/buyer/OnEnteringPaymentDoneForSleep;)V
.end method

.method public abstract onEnteringSaveCard(Lcom/squareup/comms/protos/buyer/OnEnteringSaveCard;)V
.end method

.method public abstract onEnteringSaveCustomer(Lcom/squareup/comms/protos/buyer/OnEnteringSaveCustomer;)V
.end method

.method public abstract onExitingActivitySearch(Lcom/squareup/comms/protos/buyer/OnExitingActivitySearch;)V
.end method

.method public abstract onExitingCustomersApplet(Lcom/squareup/comms/protos/buyer/OnExitingCustomersApplet;)V
.end method

.method public abstract onFirmwareUpdateComplete(Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateComplete;)V
.end method

.method public abstract onFirmwareUpdateError(Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateError;)V
.end method

.method public abstract onFirmwareUpdateProgress(Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateProgress;)V
.end method

.method public abstract onFirmwareUpdateStarted(Lcom/squareup/comms/protos/buyer/OnFirmwareUpdateStarted;)V
.end method

.method public abstract onGiftCardActivation(Lcom/squareup/comms/protos/buyer/OnGiftCardActivation;)V
.end method

.method public abstract onGiftCardBalanceCheck(Lcom/squareup/comms/protos/buyer/OnGiftCardBalanceCheck;)V
.end method

.method public abstract onLethalTamper(Lcom/squareup/comms/protos/buyer/OnLethalTamper;)V
.end method

.method public abstract onLoyaltyPhoneNumberCancelled(Lcom/squareup/comms/protos/buyer/OnLoyaltyPhoneNumberCancelled;)V
.end method

.method public abstract onLoyaltyPhoneNumberEntered(Lcom/squareup/comms/protos/buyer/OnLoyaltyPhoneNumberEntered;)V
.end method

.method public abstract onNfcError(Lcom/squareup/comms/protos/buyer/OnNfcError;)V
.end method

.method public abstract onNotCanceling(Lcom/squareup/comms/protos/buyer/OnNotCanceling;)V
.end method

.method public abstract onOther(Lcom/squareup/comms/protos/buyer/OnOther;)V
.end method

.method public abstract onPaymentCanceled(Lcom/squareup/comms/protos/buyer/OnPaymentCanceled;)V
.end method

.method public abstract onPaymentTerminated(Lcom/squareup/comms/protos/buyer/OnPaymentTerminated;)V
.end method

.method public abstract onPaymentTerminatedDueToSwipe(Lcom/squareup/comms/protos/buyer/OnPaymentTerminatedDueToSwipe;)V
.end method

.method public abstract onPreviousCardRemoved(Lcom/squareup/comms/protos/buyer/OnPreviousCardRemoved;)V
.end method

.method public abstract onReaderCommsEstablished(Lcom/squareup/comms/protos/buyer/OnReaderCommsEstablished;)V
.end method

.method public abstract onReaderNotReady(Lcom/squareup/comms/protos/buyer/OnReaderNotReady;)V
.end method

.method public abstract onReceipt(Lcom/squareup/comms/protos/buyer/OnReceipt;)V
.end method

.method public abstract onRemoveCardPrompted(Lcom/squareup/comms/protos/buyer/OnRemoveCardPrompted;)V
.end method

.method public abstract onSaveCardErrorTryAgain(Lcom/squareup/comms/protos/buyer/OnSaveCardErrorTryAgain;)V
.end method

.method public abstract onSaveCardOnFileSuccess(Lcom/squareup/comms/protos/buyer/OnSaveCardOnFileSuccess;)V
.end method

.method public abstract onSellerCanceled(Lcom/squareup/comms/protos/buyer/OnSellerCanceled;)V
.end method

.method public abstract onSellerCanceling(Lcom/squareup/comms/protos/buyer/OnSellerCanceling;)V
.end method

.method public abstract onSignature(Lcom/squareup/comms/protos/buyer/OnSignature;)V
.end method

.method public abstract onSignatureRequested(Lcom/squareup/comms/protos/buyer/OnSignatureRequested;)V
.end method

.method public abstract onSwipePaymentApproved(Lcom/squareup/comms/protos/buyer/OnSwipePaymentApproved;)V
.end method

.method public abstract onSwipePaymentDeclined(Lcom/squareup/comms/protos/buyer/OnSwipePaymentDeclined;)V
.end method

.method public abstract onSwipePaymentRemoteError(Lcom/squareup/comms/protos/buyer/OnSwipePaymentRemoteError;)V
.end method

.method public abstract onTamperData(Lcom/squareup/comms/protos/buyer/OnTamperData;)V
.end method

.method public abstract onTender(Lcom/squareup/comms/protos/buyer/OnTender;)V
.end method

.method public abstract onTenderFromSaveCustomer(Lcom/squareup/comms/protos/buyer/OnTenderFromSaveCustomer;)V
.end method

.method public abstract onTenderSwipedCard(Lcom/squareup/comms/protos/buyer/OnTenderSwipedCard;)V
.end method

.method public abstract onTip(Lcom/squareup/comms/protos/buyer/OnTip;)V
.end method

.method public abstract onUseChipCard(Lcom/squareup/comms/protos/buyer/OnUseChipCard;)V
.end method

.method public abstract onWaitingForNextTender(Lcom/squareup/comms/protos/buyer/OnWaitingForNextTender;)V
.end method

.method public abstract onWaitingForSeller(Lcom/squareup/comms/protos/buyer/OnWaitingForSeller;)V
.end method

.method public abstract performPostTransactionAction(Lcom/squareup/comms/protos/buyer/PerformPostTransactionAction;)V
.end method

.method public abstract phoneNumberDone(Lcom/squareup/comms/protos/buyer/PhoneNumberDone;)V
.end method

.method public abstract preChargeCardStatus(Lcom/squareup/comms/protos/buyer/PreChargeCardStatus;)V
.end method

.method public abstract preSwipe(Lcom/squareup/comms/protos/buyer/PreSwipe;)V
.end method

.method public abstract printReceipt(Lcom/squareup/comms/protos/buyer/PrintReceipt;)V
.end method

.method public abstract processMessageFromReader(Lcom/squareup/comms/protos/buyer/ProcessMessageFromReader;)V
.end method

.method public abstract readerConnected(Lcom/squareup/comms/protos/buyer/ReaderConnected;)V
.end method

.method public abstract receiptEntryCanceled(Lcom/squareup/comms/protos/buyer/ReceiptEntryCanceled;)V
.end method

.method public abstract receiptSelectionTimedOut(Lcom/squareup/comms/protos/buyer/ReceiptSelectionTimedOut;)V
.end method

.method public abstract requestBranDisplayUpdate(Lcom/squareup/comms/protos/buyer/RequestBranDisplayUpdate;)V
.end method

.method public abstract resetToCardOnFileAuthAgreement(Lcom/squareup/comms/protos/buyer/ResetToCardOnFileAuthAgreement;)V
.end method

.method public abstract restartAutoCaptureTimer(Lcom/squareup/comms/protos/buyer/RestartAutoCaptureTimer;)V
.end method

.method public abstract saveCustomerPostalCode(Lcom/squareup/comms/protos/buyer/SaveCustomerPostalCode;)V
.end method

.method public abstract sendAuthorization(Lcom/squareup/comms/protos/buyer/SendAuthorization;)V
.end method

.method public abstract sendSecureSessionMessageToServer(Lcom/squareup/comms/protos/buyer/SendSecureSessionMessageToServer;)V
.end method

.method public abstract signatureProvided(Lcom/squareup/comms/protos/buyer/SignatureProvided;)V
.end method

.method public abstract throwableData(Lcom/squareup/comms/protos/buyer/ThrowableData;)V
.end method

.method public abstract timberLogEvent(Lcom/squareup/comms/protos/buyer/TimberLogEvent;)V
.end method

.method public abstract tipSelected(Lcom/squareup/comms/protos/buyer/TipSelected;)V
.end method
