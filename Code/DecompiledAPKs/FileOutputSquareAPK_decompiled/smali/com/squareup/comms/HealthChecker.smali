.class public interface abstract Lcom/squareup/comms/HealthChecker;
.super Ljava/lang/Object;
.source "HealthChecker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/comms/HealthChecker$NullHealthChecker;,
        Lcom/squareup/comms/HealthChecker$Callback;
    }
.end annotation


# virtual methods
.method public abstract start(Lcom/squareup/comms/MessagePoster;Lcom/squareup/comms/HealthChecker$Callback;)V
.end method

.method public abstract stop()V
.end method

.method public abstract tryConsumeMessage(Lcom/squareup/comms/MessagePoster;Lcom/squareup/wire/Message;)Z
.end method
