.class public final Lcom/squareup/comms/protos/buyer/TipSelected$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "TipSelected.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/buyer/TipSelected;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/comms/protos/buyer/TipSelected;",
        "Lcom/squareup/comms/protos/buyer/TipSelected$Builder;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0002\u0008\u0002\n\u0002\u0010\u0006\n\u0002\u0008\u0004\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00000\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u000e\u0010\u0004\u001a\u00020\u00002\u0006\u0010\u0004\u001a\u00020\u0005J\u0008\u0010\n\u001a\u00020\u0002H\u0016J\u0015\u0010\u0007\u001a\u00020\u00002\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u0008\u00a2\u0006\u0002\u0010\u000bR\u0016\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0006R\u0016\u0010\u0007\u001a\u0004\u0018\u00010\u00088\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\t\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/comms/protos/buyer/TipSelected$Builder;",
        "Lcom/squareup/wire/Message$Builder;",
        "Lcom/squareup/comms/protos/buyer/TipSelected;",
        "()V",
        "amount",
        "",
        "Ljava/lang/Long;",
        "percentage",
        "",
        "Ljava/lang/Double;",
        "build",
        "(Ljava/lang/Double;)Lcom/squareup/comms/protos/buyer/TipSelected$Builder;",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public amount:Ljava/lang/Long;

.field public percentage:Ljava/lang/Double;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 89
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public final amount(J)Lcom/squareup/comms/protos/buyer/TipSelected$Builder;
    .locals 0

    .line 100
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/TipSelected$Builder;->amount:Ljava/lang/Long;

    return-object p0
.end method

.method public build()Lcom/squareup/comms/protos/buyer/TipSelected;
    .locals 5

    .line 112
    new-instance v0, Lcom/squareup/comms/protos/buyer/TipSelected;

    .line 113
    iget-object v1, p0, Lcom/squareup/comms/protos/buyer/TipSelected$Builder;->amount:Ljava/lang/Long;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    .line 114
    iget-object v3, p0, Lcom/squareup/comms/protos/buyer/TipSelected$Builder;->percentage:Ljava/lang/Double;

    .line 115
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/TipSelected$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    .line 112
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/comms/protos/buyer/TipSelected;-><init>(JLjava/lang/Double;Lokio/ByteString;)V

    return-object v0

    :cond_0
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    const/4 v1, 0x1

    const-string v2, "amount"

    aput-object v2, v0, v1

    .line 113
    invoke-static {v0}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 89
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/TipSelected$Builder;->build()Lcom/squareup/comms/protos/buyer/TipSelected;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    return-object v0
.end method

.method public final percentage(Ljava/lang/Double;)Lcom/squareup/comms/protos/buyer/TipSelected$Builder;
    .locals 0

    .line 108
    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/TipSelected$Builder;->percentage:Ljava/lang/Double;

    return-object p0
.end method
