.class public final Lcom/squareup/comms/protos/common/CustomerInformation$Birthday$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CustomerInformation.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/common/CustomerInformation$Birthday;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/comms/protos/common/CustomerInformation$Birthday;",
        "Lcom/squareup/comms/protos/common/CustomerInformation$Birthday$Builder;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0006\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00000\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0008\u0010\t\u001a\u00020\u0002H\u0016J\u000e\u0010\u0004\u001a\u00020\u00002\u0006\u0010\u0004\u001a\u00020\u0005J\u000e\u0010\u0007\u001a\u00020\u00002\u0006\u0010\u0007\u001a\u00020\u0005J\u0015\u0010\u0008\u001a\u00020\u00002\u0008\u0010\u0008\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\nR\u0016\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0006R\u0016\u0010\u0007\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0006R\u0016\u0010\u0008\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0006\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/comms/protos/common/CustomerInformation$Birthday$Builder;",
        "Lcom/squareup/wire/Message$Builder;",
        "Lcom/squareup/comms/protos/common/CustomerInformation$Birthday;",
        "()V",
        "day",
        "",
        "Ljava/lang/Integer;",
        "month",
        "year",
        "build",
        "(Ljava/lang/Integer;)Lcom/squareup/comms/protos/common/CustomerInformation$Birthday$Builder;",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public day:Ljava/lang/Integer;

.field public month:Ljava/lang/Integer;

.field public year:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 442
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/comms/protos/common/CustomerInformation$Birthday;
    .locals 7

    .line 467
    new-instance v0, Lcom/squareup/comms/protos/common/CustomerInformation$Birthday;

    .line 468
    iget-object v1, p0, Lcom/squareup/comms/protos/common/CustomerInformation$Birthday$Builder;->year:Ljava/lang/Integer;

    .line 469
    iget-object v2, p0, Lcom/squareup/comms/protos/common/CustomerInformation$Birthday$Builder;->month:Ljava/lang/Integer;

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 470
    iget-object v6, p0, Lcom/squareup/comms/protos/common/CustomerInformation$Birthday$Builder;->day:Ljava/lang/Integer;

    if-eqz v6, :cond_0

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 471
    invoke-virtual {p0}, Lcom/squareup/comms/protos/common/CustomerInformation$Birthday$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    .line 467
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/comms/protos/common/CustomerInformation$Birthday;-><init>(Ljava/lang/Integer;IILokio/ByteString;)V

    return-object v0

    :cond_0
    new-array v0, v5, [Ljava/lang/Object;

    aput-object v6, v0, v4

    const-string v1, "day"

    aput-object v1, v0, v3

    .line 470
    invoke-static {v0}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_1
    new-array v0, v5, [Ljava/lang/Object;

    aput-object v2, v0, v4

    const-string v1, "month"

    aput-object v1, v0, v3

    .line 469
    invoke-static {v0}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 442
    invoke-virtual {p0}, Lcom/squareup/comms/protos/common/CustomerInformation$Birthday$Builder;->build()Lcom/squareup/comms/protos/common/CustomerInformation$Birthday;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    return-object v0
.end method

.method public final day(I)Lcom/squareup/comms/protos/common/CustomerInformation$Birthday$Builder;
    .locals 0

    .line 463
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/comms/protos/common/CustomerInformation$Birthday$Builder;->day:Ljava/lang/Integer;

    return-object p0
.end method

.method public final month(I)Lcom/squareup/comms/protos/common/CustomerInformation$Birthday$Builder;
    .locals 0

    .line 458
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/comms/protos/common/CustomerInformation$Birthday$Builder;->month:Ljava/lang/Integer;

    return-object p0
.end method

.method public final year(Ljava/lang/Integer;)Lcom/squareup/comms/protos/common/CustomerInformation$Birthday$Builder;
    .locals 0

    .line 453
    iput-object p1, p0, Lcom/squareup/comms/protos/common/CustomerInformation$Birthday$Builder;->year:Ljava/lang/Integer;

    return-object p0
.end method
