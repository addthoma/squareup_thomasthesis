.class public final Lcom/squareup/comms/protos/buyer/BranEmvEventOnListApplications$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "BranEmvEventOnListApplications.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/buyer/BranEmvEventOnListApplications;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/comms/protos/buyer/BranEmvEventOnListApplications;",
        "Lcom/squareup/comms/protos/buyer/BranEmvEventOnListApplications$Builder;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00000\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0014\u0010\u0004\u001a\u00020\u00002\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005J\u0008\u0010\u0007\u001a\u00020\u0002H\u0016R\u0018\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/comms/protos/buyer/BranEmvEventOnListApplications$Builder;",
        "Lcom/squareup/wire/Message$Builder;",
        "Lcom/squareup/comms/protos/buyer/BranEmvEventOnListApplications;",
        "()V",
        "applications",
        "",
        "Lcom/squareup/comms/protos/buyer/EmvApplication;",
        "build",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public applications:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/comms/protos/buyer/EmvApplication;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 69
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 71
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/comms/protos/buyer/BranEmvEventOnListApplications$Builder;->applications:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final applications(Ljava/util/List;)Lcom/squareup/comms/protos/buyer/BranEmvEventOnListApplications$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/comms/protos/buyer/EmvApplication;",
            ">;)",
            "Lcom/squareup/comms/protos/buyer/BranEmvEventOnListApplications$Builder;"
        }
    .end annotation

    const-string v0, "applications"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 75
    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/BranEmvEventOnListApplications$Builder;->applications:Ljava/util/List;

    return-object p0
.end method

.method public build()Lcom/squareup/comms/protos/buyer/BranEmvEventOnListApplications;
    .locals 3

    .line 79
    new-instance v0, Lcom/squareup/comms/protos/buyer/BranEmvEventOnListApplications;

    .line 80
    iget-object v1, p0, Lcom/squareup/comms/protos/buyer/BranEmvEventOnListApplications$Builder;->applications:Ljava/util/List;

    .line 81
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/BranEmvEventOnListApplications$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    .line 79
    invoke-direct {v0, v1, v2}, Lcom/squareup/comms/protos/buyer/BranEmvEventOnListApplications;-><init>(Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 69
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/BranEmvEventOnListApplications$Builder;->build()Lcom/squareup/comms/protos/buyer/BranEmvEventOnListApplications;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    return-object v0
.end method
