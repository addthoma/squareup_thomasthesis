.class public final Lcom/squareup/comms/protos/buyer/BranSpeFirmware$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "BranSpeFirmware.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/buyer/BranSpeFirmware;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/comms/protos/buyer/BranSpeFirmware;",
        "Lcom/squareup/comms/protos/buyer/BranSpeFirmware$Builder;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010$\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00000\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0008\u0010\t\u001a\u00020\u0002H\u0016J\u000e\u0010\u0004\u001a\u00020\u00002\u0006\u0010\u0004\u001a\u00020\u0005J\u001a\u0010\u0006\u001a\u00020\u00002\u0012\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\u0008\u0012\u0004\u0012\u00020\u00050\u0007R\u0014\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u001e\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\u0008\u0012\u0004\u0012\u00020\u00050\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/comms/protos/buyer/BranSpeFirmware$Builder;",
        "Lcom/squareup/wire/Message$Builder;",
        "Lcom/squareup/comms/protos/buyer/BranSpeFirmware;",
        "()V",
        "countryCode",
        "",
        "firmwareVersion",
        "",
        "",
        "build",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public countryCode:Ljava/lang/String;

.field public firmwareVersion:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 89
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 91
    invoke-static {}, Lkotlin/collections/MapsKt;->emptyMap()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/comms/protos/buyer/BranSpeFirmware$Builder;->firmwareVersion:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/comms/protos/buyer/BranSpeFirmware;
    .locals 4

    .line 113
    new-instance v0, Lcom/squareup/comms/protos/buyer/BranSpeFirmware;

    .line 114
    iget-object v1, p0, Lcom/squareup/comms/protos/buyer/BranSpeFirmware$Builder;->firmwareVersion:Ljava/util/Map;

    .line 115
    iget-object v2, p0, Lcom/squareup/comms/protos/buyer/BranSpeFirmware$Builder;->countryCode:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 116
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/BranSpeFirmware$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    .line 113
    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/comms/protos/buyer/BranSpeFirmware;-><init>(Ljava/util/Map;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0

    :cond_0
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "countryCode"

    aput-object v2, v0, v1

    .line 115
    invoke-static {v0}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 89
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/BranSpeFirmware$Builder;->build()Lcom/squareup/comms/protos/buyer/BranSpeFirmware;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    return-object v0
.end method

.method public final countryCode(Ljava/lang/String;)Lcom/squareup/comms/protos/buyer/BranSpeFirmware$Builder;
    .locals 1

    const-string v0, "countryCode"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 109
    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/BranSpeFirmware$Builder;->countryCode:Ljava/lang/String;

    return-object p0
.end method

.method public final firmwareVersion(Ljava/util/Map;)Lcom/squareup/comms/protos/buyer/BranSpeFirmware$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/comms/protos/buyer/BranSpeFirmware$Builder;"
        }
    .end annotation

    const-string v0, "firmwareVersion"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 101
    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/BranSpeFirmware$Builder;->firmwareVersion:Ljava/util/Map;

    return-object p0
.end method
