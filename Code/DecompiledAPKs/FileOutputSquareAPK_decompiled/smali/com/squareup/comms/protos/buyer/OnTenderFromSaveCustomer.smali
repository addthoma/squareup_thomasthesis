.class public final Lcom/squareup/comms/protos/buyer/OnTenderFromSaveCustomer;
.super Lcom/squareup/wire/AndroidMessage;
.source "OnTenderFromSaveCustomer.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/comms/protos/buyer/OnTenderFromSaveCustomer$Builder;,
        Lcom/squareup/comms/protos/buyer/OnTenderFromSaveCustomer$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/comms/protos/buyer/OnTenderFromSaveCustomer;",
        "Lcom/squareup/comms/protos/buyer/OnTenderFromSaveCustomer$Builder;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOnTenderFromSaveCustomer.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OnTenderFromSaveCustomer.kt\ncom/squareup/comms/protos/buyer/OnTenderFromSaveCustomer\n*L\n1#1,126:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0003\u0018\u0000 \u00122\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u00020\u0001:\u0002\u0011\u0012B\u0017\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u001a\u0010\u0008\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0003\u001a\u00020\u00042\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u0006J\u0013\u0010\t\u001a\u00020\u00042\u0008\u0010\n\u001a\u0004\u0018\u00010\u000bH\u0096\u0002J\u0008\u0010\u000c\u001a\u00020\rH\u0016J\u0008\u0010\u000e\u001a\u00020\u0002H\u0016J\u0008\u0010\u000f\u001a\u00020\u0010H\u0016R\u0010\u0010\u0003\u001a\u00020\u00048\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/comms/protos/buyer/OnTenderFromSaveCustomer;",
        "Lcom/squareup/wire/AndroidMessage;",
        "Lcom/squareup/comms/protos/buyer/OnTenderFromSaveCustomer$Builder;",
        "is_card_present",
        "",
        "unknownFields",
        "Lokio/ByteString;",
        "(ZLokio/ByteString;)V",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "",
        "newBuilder",
        "toString",
        "",
        "Builder",
        "Companion",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/comms/protos/buyer/OnTenderFromSaveCustomer;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/comms/protos/buyer/OnTenderFromSaveCustomer;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/comms/protos/buyer/OnTenderFromSaveCustomer$Companion;


# instance fields
.field public final is_card_present:Z
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        label = .enum Lcom/squareup/wire/WireField$Label;->REQUIRED:Lcom/squareup/wire/WireField$Label;
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/squareup/comms/protos/buyer/OnTenderFromSaveCustomer$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/comms/protos/buyer/OnTenderFromSaveCustomer$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/comms/protos/buyer/OnTenderFromSaveCustomer;->Companion:Lcom/squareup/comms/protos/buyer/OnTenderFromSaveCustomer$Companion;

    .line 88
    new-instance v0, Lcom/squareup/comms/protos/buyer/OnTenderFromSaveCustomer$Companion$ADAPTER$1;

    .line 90
    sget-object v1, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    .line 91
    const-class v2, Lcom/squareup/comms/protos/buyer/OnTenderFromSaveCustomer;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/comms/protos/buyer/OnTenderFromSaveCustomer$Companion$ADAPTER$1;-><init>(Lcom/squareup/wire/FieldEncoding;Lkotlin/reflect/KClass;)V

    check-cast v0, Lcom/squareup/wire/ProtoAdapter;

    sput-object v0, Lcom/squareup/comms/protos/buyer/OnTenderFromSaveCustomer;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 123
    sget-object v0, Lcom/squareup/wire/AndroidMessage;->Companion:Lcom/squareup/wire/AndroidMessage$Companion;

    sget-object v1, Lcom/squareup/comms/protos/buyer/OnTenderFromSaveCustomer;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/AndroidMessage$Companion;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/comms/protos/buyer/OnTenderFromSaveCustomer;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(ZLokio/ByteString;)V
    .locals 1

    const-string v0, "unknownFields"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    sget-object v0, Lcom/squareup/comms/protos/buyer/OnTenderFromSaveCustomer;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    iput-boolean p1, p0, Lcom/squareup/comms/protos/buyer/OnTenderFromSaveCustomer;->is_card_present:Z

    return-void
.end method

.method public synthetic constructor <init>(ZLokio/ByteString;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    .line 32
    sget-object p2, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/comms/protos/buyer/OnTenderFromSaveCustomer;-><init>(ZLokio/ByteString;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/comms/protos/buyer/OnTenderFromSaveCustomer;ZLokio/ByteString;ILjava/lang/Object;)Lcom/squareup/comms/protos/buyer/OnTenderFromSaveCustomer;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    .line 66
    iget-boolean p1, p0, Lcom/squareup/comms/protos/buyer/OnTenderFromSaveCustomer;->is_card_present:Z

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    .line 67
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/OnTenderFromSaveCustomer;->unknownFields()Lokio/ByteString;

    move-result-object p2

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/comms/protos/buyer/OnTenderFromSaveCustomer;->copy(ZLokio/ByteString;)Lcom/squareup/comms/protos/buyer/OnTenderFromSaveCustomer;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final copy(ZLokio/ByteString;)Lcom/squareup/comms/protos/buyer/OnTenderFromSaveCustomer;
    .locals 1

    const-string v0, "unknownFields"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    new-instance v0, Lcom/squareup/comms/protos/buyer/OnTenderFromSaveCustomer;

    invoke-direct {v0, p1, p2}, Lcom/squareup/comms/protos/buyer/OnTenderFromSaveCustomer;-><init>(ZLokio/ByteString;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .line 43
    move-object v0, p0

    check-cast v0, Lcom/squareup/comms/protos/buyer/OnTenderFromSaveCustomer;

    const/4 v1, 0x1

    if-ne p1, v0, :cond_0

    return v1

    .line 44
    :cond_0
    instance-of v0, p1, Lcom/squareup/comms/protos/buyer/OnTenderFromSaveCustomer;

    const/4 v2, 0x0

    if-nez v0, :cond_1

    return v2

    .line 46
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/OnTenderFromSaveCustomer;->unknownFields()Lokio/ByteString;

    move-result-object v0

    check-cast p1, Lcom/squareup/comms/protos/buyer/OnTenderFromSaveCustomer;

    invoke-virtual {p1}, Lcom/squareup/comms/protos/buyer/OnTenderFromSaveCustomer;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/squareup/comms/protos/buyer/OnTenderFromSaveCustomer;->is_card_present:Z

    iget-boolean p1, p1, Lcom/squareup/comms/protos/buyer/OnTenderFromSaveCustomer;->is_card_present:Z

    if-ne v0, p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public hashCode()I
    .locals 2

    .line 50
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_0

    .line 52
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/OnTenderFromSaveCustomer;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 53
    iget-boolean v1, p0, Lcom/squareup/comms/protos/buyer/OnTenderFromSaveCustomer;->is_card_present:Z

    invoke-static {v1}, L$r8$java8methods$utility$Boolean$hashCode$IZ;->hashCode(Z)I

    move-result v1

    add-int/2addr v0, v1

    .line 54
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_0
    return v0
.end method

.method public newBuilder()Lcom/squareup/comms/protos/buyer/OnTenderFromSaveCustomer$Builder;
    .locals 2

    .line 36
    new-instance v0, Lcom/squareup/comms/protos/buyer/OnTenderFromSaveCustomer$Builder;

    invoke-direct {v0}, Lcom/squareup/comms/protos/buyer/OnTenderFromSaveCustomer$Builder;-><init>()V

    .line 37
    iget-boolean v1, p0, Lcom/squareup/comms/protos/buyer/OnTenderFromSaveCustomer;->is_card_present:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/comms/protos/buyer/OnTenderFromSaveCustomer$Builder;->is_card_present:Ljava/lang/Boolean;

    .line 38
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/OnTenderFromSaveCustomer;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/comms/protos/buyer/OnTenderFromSaveCustomer$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/OnTenderFromSaveCustomer;->newBuilder()Lcom/squareup/comms/protos/buyer/OnTenderFromSaveCustomer$Builder;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 12

    .line 60
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast v0, Ljava/util/List;

    .line 61
    move-object v1, v0

    check-cast v1, Ljava/util/Collection;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "is_card_present="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v3, p0, Lcom/squareup/comms/protos/buyer/OnTenderFromSaveCustomer;->is_card_present:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 62
    move-object v3, v0

    check-cast v3, Ljava/lang/Iterable;

    const-string v0, "OnTenderFromSaveCustomer{"

    move-object v5, v0

    check-cast v5, Ljava/lang/CharSequence;

    const-string v0, ", "

    move-object v4, v0

    check-cast v4, Ljava/lang/CharSequence;

    const-string v0, "}"

    .line 63
    move-object v6, v0

    check-cast v6, Ljava/lang/CharSequence;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x38

    const/4 v11, 0x0

    .line 62
    invoke-static/range {v3 .. v11}, Lkotlin/collections/CollectionsKt;->joinToString$default(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
