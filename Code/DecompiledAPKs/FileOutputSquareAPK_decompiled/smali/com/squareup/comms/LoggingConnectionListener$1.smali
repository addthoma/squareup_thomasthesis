.class Lcom/squareup/comms/LoggingConnectionListener$1;
.super Ljava/lang/Object;
.source "LoggingConnectionListener.java"

# interfaces
.implements Lcom/squareup/comms/RemoteBusConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/comms/LoggingConnectionListener;->onConnected(Lcom/squareup/comms/RemoteBusConnection;Lcom/squareup/comms/net/Device;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/comms/LoggingConnectionListener;

.field final synthetic val$connection:Lcom/squareup/comms/RemoteBusConnection;


# direct methods
.method constructor <init>(Lcom/squareup/comms/LoggingConnectionListener;Lcom/squareup/comms/RemoteBusConnection;)V
    .locals 0

    .line 22
    iput-object p1, p0, Lcom/squareup/comms/LoggingConnectionListener$1;->this$0:Lcom/squareup/comms/LoggingConnectionListener;

    iput-object p2, p0, Lcom/squareup/comms/LoggingConnectionListener$1;->val$connection:Lcom/squareup/comms/RemoteBusConnection;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public observable()Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/wire/Message;",
            ">;"
        }
    .end annotation

    .line 29
    iget-object v0, p0, Lcom/squareup/comms/LoggingConnectionListener$1;->val$connection:Lcom/squareup/comms/RemoteBusConnection;

    invoke-interface {v0}, Lcom/squareup/comms/RemoteBusConnection;->observable()Lrx/Observable;

    move-result-object v0

    return-object v0
.end method

.method public post(Lcom/squareup/wire/Message;)V
    .locals 2

    .line 24
    iget-object v0, p0, Lcom/squareup/comms/LoggingConnectionListener$1;->this$0:Lcom/squareup/comms/LoggingConnectionListener;

    invoke-static {v0, p1}, Lcom/squareup/comms/LoggingConnectionListener;->access$000(Lcom/squareup/comms/LoggingConnectionListener;Lcom/squareup/wire/Message;)Lcom/squareup/wire/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/comms/LoggingConnectionListener;->logRpcMessageSent(Lcom/squareup/wire/Message;)V

    .line 25
    iget-object v0, p0, Lcom/squareup/comms/LoggingConnectionListener$1;->val$connection:Lcom/squareup/comms/RemoteBusConnection;

    invoke-interface {v0, p1}, Lcom/squareup/comms/RemoteBusConnection;->post(Lcom/squareup/wire/Message;)V

    return-void
.end method
