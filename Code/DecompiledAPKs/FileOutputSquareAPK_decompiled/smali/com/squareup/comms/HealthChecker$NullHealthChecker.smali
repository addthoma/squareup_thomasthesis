.class public Lcom/squareup/comms/HealthChecker$NullHealthChecker;
.super Ljava/lang/Object;
.source "HealthChecker.java"

# interfaces
.implements Lcom/squareup/comms/HealthChecker;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/HealthChecker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "NullHealthChecker"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public start(Lcom/squareup/comms/MessagePoster;Lcom/squareup/comms/HealthChecker$Callback;)V
    .locals 0

    .line 53
    invoke-interface {p2}, Lcom/squareup/comms/HealthChecker$Callback;->onHealthy()V

    return-void
.end method

.method public stop()V
    .locals 0

    return-void
.end method

.method public tryConsumeMessage(Lcom/squareup/comms/MessagePoster;Lcom/squareup/wire/Message;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method
