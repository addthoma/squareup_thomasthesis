.class public Lcom/squareup/comms/buyerapi/ThrowableUtil;
.super Ljava/lang/Object;
.source "ThrowableUtil.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static convertStackTrace([Ljava/lang/StackTraceElement;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/StackTraceElement;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/comms/protos/buyer/ThrowableData$StackTraceData;",
            ">;"
        }
    .end annotation

    .line 28
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 29
    array-length v1, p0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, p0, v2

    .line 30
    invoke-static {v3}, Lcom/squareup/comms/buyerapi/ThrowableUtil;->convertStackTraceElement(Ljava/lang/StackTraceElement;)Lcom/squareup/comms/protos/buyer/ThrowableData$StackTraceData;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method private static convertStackTraceElement(Ljava/lang/StackTraceElement;)Lcom/squareup/comms/protos/buyer/ThrowableData$StackTraceData;
    .locals 2

    .line 36
    new-instance v0, Lcom/squareup/comms/protos/buyer/ThrowableData$StackTraceData$Builder;

    invoke-direct {v0}, Lcom/squareup/comms/protos/buyer/ThrowableData$StackTraceData$Builder;-><init>()V

    .line 37
    invoke-virtual {p0}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/comms/protos/buyer/ThrowableData$StackTraceData$Builder;->declaring_class(Ljava/lang/String;)Lcom/squareup/comms/protos/buyer/ThrowableData$StackTraceData$Builder;

    move-result-object v0

    .line 38
    invoke-virtual {p0}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/comms/protos/buyer/ThrowableData$StackTraceData$Builder;->method_name(Ljava/lang/String;)Lcom/squareup/comms/protos/buyer/ThrowableData$StackTraceData$Builder;

    move-result-object v0

    .line 39
    invoke-virtual {p0}, Ljava/lang/StackTraceElement;->getFileName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/comms/protos/buyer/ThrowableData$StackTraceData$Builder;->file_name(Ljava/lang/String;)Lcom/squareup/comms/protos/buyer/ThrowableData$StackTraceData$Builder;

    move-result-object v0

    .line 40
    invoke-virtual {p0}, Ljava/lang/StackTraceElement;->getLineNumber()I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/comms/protos/buyer/ThrowableData$StackTraceData$Builder;->line_number(Ljava/lang/Integer;)Lcom/squareup/comms/protos/buyer/ThrowableData$StackTraceData$Builder;

    move-result-object p0

    .line 41
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/ThrowableData$StackTraceData$Builder;->build()Lcom/squareup/comms/protos/buyer/ThrowableData$StackTraceData;

    move-result-object p0

    return-object p0
.end method

.method public static dataToThrowable(Lcom/squareup/comms/protos/buyer/ThrowableData;)Lcom/squareup/comms/buyerapi/BuyerThrowable;
    .locals 1

    .line 23
    new-instance v0, Lcom/squareup/comms/buyerapi/BuyerThrowable;

    invoke-direct {v0, p0}, Lcom/squareup/comms/buyerapi/BuyerThrowable;-><init>(Lcom/squareup/comms/protos/buyer/ThrowableData;)V

    return-object v0
.end method

.method public static throwableToData(Ljava/lang/Throwable;)Lcom/squareup/comms/protos/buyer/ThrowableData;
    .locals 2

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 14
    :cond_0
    new-instance v0, Lcom/squareup/comms/protos/buyer/ThrowableData$Builder;

    invoke-direct {v0}, Lcom/squareup/comms/protos/buyer/ThrowableData$Builder;-><init>()V

    .line 15
    invoke-virtual {p0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/comms/buyerapi/ThrowableUtil;->throwableToData(Ljava/lang/Throwable;)Lcom/squareup/comms/protos/buyer/ThrowableData;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/comms/protos/buyer/ThrowableData$Builder;->cause(Lcom/squareup/comms/protos/buyer/ThrowableData;)Lcom/squareup/comms/protos/buyer/ThrowableData$Builder;

    move-result-object v0

    .line 16
    invoke-virtual {p0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/comms/protos/buyer/ThrowableData$Builder;->message(Ljava/lang/String;)Lcom/squareup/comms/protos/buyer/ThrowableData$Builder;

    move-result-object v0

    .line 17
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/comms/protos/buyer/ThrowableData$Builder;->original_class_name(Ljava/lang/String;)Lcom/squareup/comms/protos/buyer/ThrowableData$Builder;

    move-result-object v0

    .line 18
    invoke-virtual {p0}, Ljava/lang/Throwable;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/comms/buyerapi/ThrowableUtil;->convertStackTrace([Ljava/lang/StackTraceElement;)Ljava/util/List;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/comms/protos/buyer/ThrowableData$Builder;->stack_trace(Ljava/util/List;)Lcom/squareup/comms/protos/buyer/ThrowableData$Builder;

    move-result-object p0

    .line 19
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/ThrowableData$Builder;->build()Lcom/squareup/comms/protos/buyer/ThrowableData;

    move-result-object p0

    return-object p0
.end method
