.class public final Lcom/squareup/orders/CreateOrderRequest$Modifier$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CreateOrderRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/CreateOrderRequest$Modifier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/orders/CreateOrderRequest$Modifier;",
        "Lcom/squareup/orders/CreateOrderRequest$Modifier$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

.field public catalog_object_id:Ljava/lang/String;

.field public name:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1458
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public base_price_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/orders/CreateOrderRequest$Modifier$Builder;
    .locals 0

    .line 1490
    iput-object p1, p0, Lcom/squareup/orders/CreateOrderRequest$Modifier$Builder;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public build()Lcom/squareup/orders/CreateOrderRequest$Modifier;
    .locals 5

    .line 1496
    new-instance v0, Lcom/squareup/orders/CreateOrderRequest$Modifier;

    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$Modifier$Builder;->catalog_object_id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/orders/CreateOrderRequest$Modifier$Builder;->name:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/orders/CreateOrderRequest$Modifier$Builder;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/orders/CreateOrderRequest$Modifier;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Money;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 1451
    invoke-virtual {p0}, Lcom/squareup/orders/CreateOrderRequest$Modifier$Builder;->build()Lcom/squareup/orders/CreateOrderRequest$Modifier;

    move-result-object v0

    return-object v0
.end method

.method public catalog_object_id(Ljava/lang/String;)Lcom/squareup/orders/CreateOrderRequest$Modifier$Builder;
    .locals 0

    .line 1465
    iput-object p1, p0, Lcom/squareup/orders/CreateOrderRequest$Modifier$Builder;->catalog_object_id:Ljava/lang/String;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/orders/CreateOrderRequest$Modifier$Builder;
    .locals 0

    .line 1477
    iput-object p1, p0, Lcom/squareup/orders/CreateOrderRequest$Modifier$Builder;->name:Ljava/lang/String;

    return-object p0
.end method
