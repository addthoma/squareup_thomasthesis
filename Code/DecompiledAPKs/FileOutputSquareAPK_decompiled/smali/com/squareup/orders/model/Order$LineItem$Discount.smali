.class public final Lcom/squareup/orders/model/Order$LineItem$Discount;
.super Lcom/squareup/wire/Message;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order$LineItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Discount"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orders/model/Order$LineItem$Discount$ProtoAdapter_Discount;,
        Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;,
        Lcom/squareup/orders/model/Order$LineItem$Discount$Type;,
        Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/orders/model/Order$LineItem$Discount;",
        "Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/orders/model/Order$LineItem$Discount;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CATALOG_OBJECT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_CATALOG_OBJECT_VERSION:Ljava/lang/Long;

.field public static final DEFAULT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_PERCENTAGE:Ljava/lang/String; = ""

.field public static final DEFAULT_PRICING_RULE_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_QUANTITY:Ljava/lang/String; = "1"

.field public static final DEFAULT_SCOPE:Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;

.field public static final DEFAULT_TYPE:Lcom/squareup/orders/model/Order$LineItem$Discount$Type;

.field public static final DEFAULT_UID:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final amount_money:Lcom/squareup/protos/connect/v2/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Money#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final applied_money:Lcom/squareup/protos/connect/v2/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Money#ADAPTER"
        tag = 0x7
    .end annotation
.end field

.field public final catalog_object_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final catalog_object_version:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x3
    .end annotation
.end field

.field public final discount_code_ids:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0xe
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final metadata:Ljava/util/Map;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        keyAdapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0xa
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final percentage:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field

.field public final pricing_rule_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x10
    .end annotation
.end field

.field public final quantity:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x11
    .end annotation
.end field

.field public final reward_ids:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0xf
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final scope:Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$LineItem$Discount$Scope#ADAPTER"
        tag = 0xc
    .end annotation
.end field

.field public final type:Lcom/squareup/orders/model/Order$LineItem$Discount$Type;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$LineItem$Discount$Type#ADAPTER"
        tag = 0x8
    .end annotation
.end field

.field public final uid:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 3233
    new-instance v0, Lcom/squareup/orders/model/Order$LineItem$Discount$ProtoAdapter_Discount;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$LineItem$Discount$ProtoAdapter_Discount;-><init>()V

    sput-object v0, Lcom/squareup/orders/model/Order$LineItem$Discount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-wide/16 v0, 0x0

    .line 3241
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/orders/model/Order$LineItem$Discount;->DEFAULT_CATALOG_OBJECT_VERSION:Ljava/lang/Long;

    .line 3245
    sget-object v0, Lcom/squareup/orders/model/Order$LineItem$Discount$Type;->UNKNOWN_DISCOUNT:Lcom/squareup/orders/model/Order$LineItem$Discount$Type;

    sput-object v0, Lcom/squareup/orders/model/Order$LineItem$Discount;->DEFAULT_TYPE:Lcom/squareup/orders/model/Order$LineItem$Discount$Type;

    .line 3249
    sget-object v0, Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;->OTHER_DISCOUNT_SCOPE:Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;

    sput-object v0, Lcom/squareup/orders/model/Order$LineItem$Discount;->DEFAULT_SCOPE:Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Lcom/squareup/orders/model/Order$LineItem$Discount$Type;Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/common/Money;Ljava/util/Map;Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$LineItem$Discount$Type;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/connect/v2/common/Money;",
            "Lcom/squareup/protos/connect/v2/common/Money;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 3470
    sget-object v15, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    move-object/from16 v14, p14

    invoke-direct/range {v0 .. v15}, Lcom/squareup/orders/model/Order$LineItem$Discount;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Lcom/squareup/orders/model/Order$LineItem$Discount$Type;Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/common/Money;Ljava/util/Map;Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Lcom/squareup/orders/model/Order$LineItem$Discount$Type;Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/common/Money;Ljava/util/Map;Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$LineItem$Discount$Type;",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/connect/v2/common/Money;",
            "Lcom/squareup/protos/connect/v2/common/Money;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    move-object v0, p0

    .line 3478
    sget-object v1, Lcom/squareup/orders/model/Order$LineItem$Discount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    move-object/from16 v2, p15

    invoke-direct {p0, v1, v2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    move-object v1, p1

    .line 3479
    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Discount;->uid:Ljava/lang/String;

    move-object v1, p2

    .line 3480
    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Discount;->catalog_object_id:Ljava/lang/String;

    move-object v1, p3

    .line 3481
    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Discount;->catalog_object_version:Ljava/lang/Long;

    move-object v1, p4

    .line 3482
    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Discount;->name:Ljava/lang/String;

    move-object v1, p5

    .line 3483
    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Discount;->type:Lcom/squareup/orders/model/Order$LineItem$Discount$Type;

    move-object v1, p6

    .line 3484
    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Discount;->percentage:Ljava/lang/String;

    move-object v1, p7

    .line 3485
    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Discount;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    move-object v1, p8

    .line 3486
    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Discount;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    const-string v1, "metadata"

    move-object v2, p9

    .line 3487
    invoke-static {v1, p9}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Discount;->metadata:Ljava/util/Map;

    move-object v1, p10

    .line 3488
    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Discount;->scope:Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;

    const-string v1, "discount_code_ids"

    move-object v2, p11

    .line 3489
    invoke-static {v1, p11}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Discount;->discount_code_ids:Ljava/util/List;

    const-string v1, "reward_ids"

    move-object v2, p12

    .line 3490
    invoke-static {v1, p12}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Discount;->reward_ids:Ljava/util/List;

    move-object/from16 v1, p13

    .line 3491
    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Discount;->pricing_rule_id:Ljava/lang/String;

    move-object/from16 v1, p14

    .line 3492
    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Discount;->quantity:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 3519
    :cond_0
    instance-of v1, p1, Lcom/squareup/orders/model/Order$LineItem$Discount;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 3520
    :cond_1
    check-cast p1, Lcom/squareup/orders/model/Order$LineItem$Discount;

    .line 3521
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$LineItem$Discount;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$LineItem$Discount;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount;->uid:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$LineItem$Discount;->uid:Ljava/lang/String;

    .line 3522
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount;->catalog_object_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$LineItem$Discount;->catalog_object_id:Ljava/lang/String;

    .line 3523
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount;->catalog_object_version:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$LineItem$Discount;->catalog_object_version:Ljava/lang/Long;

    .line 3524
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$LineItem$Discount;->name:Ljava/lang/String;

    .line 3525
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount;->type:Lcom/squareup/orders/model/Order$LineItem$Discount$Type;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$LineItem$Discount;->type:Lcom/squareup/orders/model/Order$LineItem$Discount$Type;

    .line 3526
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount;->percentage:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$LineItem$Discount;->percentage:Ljava/lang/String;

    .line 3527
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$LineItem$Discount;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 3528
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$LineItem$Discount;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 3529
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount;->metadata:Ljava/util/Map;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$LineItem$Discount;->metadata:Ljava/util/Map;

    .line 3530
    invoke-interface {v1, v3}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount;->scope:Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$LineItem$Discount;->scope:Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;

    .line 3531
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount;->discount_code_ids:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$LineItem$Discount;->discount_code_ids:Ljava/util/List;

    .line 3532
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount;->reward_ids:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$LineItem$Discount;->reward_ids:Ljava/util/List;

    .line 3533
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount;->pricing_rule_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$LineItem$Discount;->pricing_rule_id:Ljava/lang/String;

    .line 3534
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount;->quantity:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/orders/model/Order$LineItem$Discount;->quantity:Ljava/lang/String;

    .line 3535
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 3540
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_b

    .line 3542
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$LineItem$Discount;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 3543
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount;->uid:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 3544
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount;->catalog_object_id:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 3545
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount;->catalog_object_version:Ljava/lang/Long;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 3546
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount;->name:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 3547
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount;->type:Lcom/squareup/orders/model/Order$LineItem$Discount$Type;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$LineItem$Discount$Type;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 3548
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount;->percentage:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 3549
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Money;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 3550
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Money;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 3551
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount;->metadata:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 3552
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount;->scope:Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 3553
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount;->discount_code_ids:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 3554
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount;->reward_ids:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 3555
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount;->pricing_rule_id:Ljava/lang/String;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 3556
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount;->quantity:Ljava/lang/String;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_a
    add-int/2addr v0, v2

    .line 3557
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_b
    return v0
.end method

.method public newBuilder()Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;
    .locals 2

    .line 3497
    new-instance v0, Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;-><init>()V

    .line 3498
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount;->uid:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;->uid:Ljava/lang/String;

    .line 3499
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount;->catalog_object_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;->catalog_object_id:Ljava/lang/String;

    .line 3500
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount;->catalog_object_version:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;->catalog_object_version:Ljava/lang/Long;

    .line 3501
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;->name:Ljava/lang/String;

    .line 3502
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount;->type:Lcom/squareup/orders/model/Order$LineItem$Discount$Type;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;->type:Lcom/squareup/orders/model/Order$LineItem$Discount$Type;

    .line 3503
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount;->percentage:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;->percentage:Ljava/lang/String;

    .line 3504
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 3505
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 3506
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount;->metadata:Ljava/util/Map;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;->metadata:Ljava/util/Map;

    .line 3507
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount;->scope:Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;->scope:Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;

    .line 3508
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount;->discount_code_ids:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;->discount_code_ids:Ljava/util/List;

    .line 3509
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount;->reward_ids:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;->reward_ids:Ljava/util/List;

    .line 3510
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount;->pricing_rule_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;->pricing_rule_id:Ljava/lang/String;

    .line 3511
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount;->quantity:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;->quantity:Ljava/lang/String;

    .line 3512
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$LineItem$Discount;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 3232
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$LineItem$Discount;->newBuilder()Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 3564
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 3565
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount;->uid:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount;->uid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3566
    :cond_0
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount;->catalog_object_id:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", catalog_object_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount;->catalog_object_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3567
    :cond_1
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount;->catalog_object_version:Ljava/lang/Long;

    if-eqz v1, :cond_2

    const-string v1, ", catalog_object_version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount;->catalog_object_version:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 3568
    :cond_2
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount;->name:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3569
    :cond_3
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount;->type:Lcom/squareup/orders/model/Order$LineItem$Discount$Type;

    if-eqz v1, :cond_4

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount;->type:Lcom/squareup/orders/model/Order$LineItem$Discount$Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 3570
    :cond_4
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount;->percentage:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", percentage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount;->percentage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3571
    :cond_5
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_6

    const-string v1, ", amount_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 3572
    :cond_6
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_7

    const-string v1, ", applied_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 3573
    :cond_7
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount;->metadata:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_8

    const-string v1, ", metadata=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3574
    :cond_8
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount;->scope:Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;

    if-eqz v1, :cond_9

    const-string v1, ", scope="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount;->scope:Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 3575
    :cond_9
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount;->discount_code_ids:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_a

    const-string v1, ", discount_code_ids="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount;->discount_code_ids:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 3576
    :cond_a
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount;->reward_ids:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_b

    const-string v1, ", reward_ids="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount;->reward_ids:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 3577
    :cond_b
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount;->pricing_rule_id:Ljava/lang/String;

    if-eqz v1, :cond_c

    const-string v1, ", pricing_rule_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount;->pricing_rule_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 3578
    :cond_c
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount;->quantity:Ljava/lang/String;

    if-eqz v1, :cond_d

    const-string v1, ", quantity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount;->quantity:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_d
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Discount{"

    .line 3579
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
