.class public final Lcom/squareup/orders/model/Order$LineItem$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order$LineItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/orders/model/Order$LineItem;",
        "Lcom/squareup/orders/model/Order$LineItem$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public applied_discounts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount;",
            ">;"
        }
    .end annotation
.end field

.field public applied_taxes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$LineItem$AppliedTax;",
            ">;"
        }
    .end annotation
.end field

.field public base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

.field public catalog_category_id:Ljava/lang/String;

.field public catalog_item_id:Ljava/lang/String;

.field public catalog_item_variation_count:Ljava/lang/Integer;

.field public catalog_object_id:Ljava/lang/String;

.field public catalog_object_version:Ljava/lang/Long;

.field public category_name:Ljava/lang/String;

.field public discounts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$LineItem$Discount;",
            ">;"
        }
    .end annotation
.end field

.field public gross_sales_money:Lcom/squareup/protos/connect/v2/common/Money;

.field public item_type:Lcom/squareup/orders/model/Order$LineItem$ItemType;

.field public metadata:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public modifiers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$LineItem$Modifier;",
            ">;"
        }
    .end annotation
.end field

.field public name:Ljava/lang/String;

.field public note:Ljava/lang/String;

.field public quantity:Ljava/lang/String;

.field public quantity_unit:Lcom/squareup/orders/model/Order$QuantityUnit;

.field public sku:Ljava/lang/String;

.field public taxes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$LineItem$Tax;",
            ">;"
        }
    .end annotation
.end field

.field public total_discount_money:Lcom/squareup/protos/connect/v2/common/Money;

.field public total_money:Lcom/squareup/protos/connect/v2/common/Money;

.field public total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;

.field public uid:Ljava/lang/String;

.field public variation_name:Ljava/lang/String;

.field public variation_total_price_money:Lcom/squareup/protos/connect/v2/common/Money;

.field public was_multiple_quantity_money:Lcom/squareup/protos/connect/v2/common/Money;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 2806
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 2807
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableMap()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orders/model/Order$LineItem$Builder;->metadata:Ljava/util/Map;

    .line 2808
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orders/model/Order$LineItem$Builder;->modifiers:Ljava/util/List;

    .line 2809
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orders/model/Order$LineItem$Builder;->taxes:Ljava/util/List;

    .line 2810
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orders/model/Order$LineItem$Builder;->discounts:Ljava/util/List;

    .line 2811
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orders/model/Order$LineItem$Builder;->applied_taxes:Ljava/util/List;

    .line 2812
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orders/model/Order$LineItem$Builder;->applied_discounts:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public applied_discounts(Ljava/util/List;)Lcom/squareup/orders/model/Order$LineItem$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount;",
            ">;)",
            "Lcom/squareup/orders/model/Order$LineItem$Builder;"
        }
    .end annotation

    .line 3065
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 3066
    iput-object p1, p0, Lcom/squareup/orders/model/Order$LineItem$Builder;->applied_discounts:Ljava/util/List;

    return-object p0
.end method

.method public applied_taxes(Ljava/util/List;)Lcom/squareup/orders/model/Order$LineItem$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$LineItem$AppliedTax;",
            ">;)",
            "Lcom/squareup/orders/model/Order$LineItem$Builder;"
        }
    .end annotation

    .line 3044
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 3045
    iput-object p1, p0, Lcom/squareup/orders/model/Order$LineItem$Builder;->applied_taxes:Ljava/util/List;

    return-object p0
.end method

.method public base_price_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/orders/model/Order$LineItem$Builder;
    .locals 0

    .line 3078
    iput-object p1, p0, Lcom/squareup/orders/model/Order$LineItem$Builder;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public build()Lcom/squareup/orders/model/Order$LineItem;
    .locals 2

    .line 3168
    new-instance v0, Lcom/squareup/orders/model/Order$LineItem;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/squareup/orders/model/Order$LineItem;-><init>(Lcom/squareup/orders/model/Order$LineItem$Builder;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 2751
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$LineItem$Builder;->build()Lcom/squareup/orders/model/Order$LineItem;

    move-result-object v0

    return-object v0
.end method

.method public catalog_category_id(Ljava/lang/String;)Lcom/squareup/orders/model/Order$LineItem$Builder;
    .locals 0

    .line 2914
    iput-object p1, p0, Lcom/squareup/orders/model/Order$LineItem$Builder;->catalog_category_id:Ljava/lang/String;

    return-object p0
.end method

.method public catalog_item_id(Ljava/lang/String;)Lcom/squareup/orders/model/Order$LineItem$Builder;
    .locals 0

    .line 2904
    iput-object p1, p0, Lcom/squareup/orders/model/Order$LineItem$Builder;->catalog_item_id:Ljava/lang/String;

    return-object p0
.end method

.method public catalog_item_variation_count(Ljava/lang/Integer;)Lcom/squareup/orders/model/Order$LineItem$Builder;
    .locals 0

    .line 3162
    iput-object p1, p0, Lcom/squareup/orders/model/Order$LineItem$Builder;->catalog_item_variation_count:Ljava/lang/Integer;

    return-object p0
.end method

.method public catalog_object_id(Ljava/lang/String;)Lcom/squareup/orders/model/Order$LineItem$Builder;
    .locals 0

    .line 2877
    iput-object p1, p0, Lcom/squareup/orders/model/Order$LineItem$Builder;->catalog_object_id:Ljava/lang/String;

    return-object p0
.end method

.method public catalog_object_version(Ljava/lang/Long;)Lcom/squareup/orders/model/Order$LineItem$Builder;
    .locals 0

    .line 2882
    iput-object p1, p0, Lcom/squareup/orders/model/Order$LineItem$Builder;->catalog_object_version:Ljava/lang/Long;

    return-object p0
.end method

.method public category_name(Ljava/lang/String;)Lcom/squareup/orders/model/Order$LineItem$Builder;
    .locals 0

    .line 2945
    iput-object p1, p0, Lcom/squareup/orders/model/Order$LineItem$Builder;->category_name:Ljava/lang/String;

    return-object p0
.end method

.method public discounts(Ljava/util/List;)Lcom/squareup/orders/model/Order$LineItem$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$LineItem$Discount;",
            ">;)",
            "Lcom/squareup/orders/model/Order$LineItem$Builder;"
        }
    .end annotation

    .line 3023
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 3024
    iput-object p1, p0, Lcom/squareup/orders/model/Order$LineItem$Builder;->discounts:Ljava/util/List;

    return-object p0
.end method

.method public gross_sales_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/orders/model/Order$LineItem$Builder;
    .locals 0

    .line 3106
    iput-object p1, p0, Lcom/squareup/orders/model/Order$LineItem$Builder;->gross_sales_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public item_type(Lcom/squareup/orders/model/Order$LineItem$ItemType;)Lcom/squareup/orders/model/Order$LineItem$Builder;
    .locals 0

    .line 2925
    iput-object p1, p0, Lcom/squareup/orders/model/Order$LineItem$Builder;->item_type:Lcom/squareup/orders/model/Order$LineItem$ItemType;

    return-object p0
.end method

.method public metadata(Ljava/util/Map;)Lcom/squareup/orders/model/Order$LineItem$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/orders/model/Order$LineItem$Builder;"
        }
    .end annotation

    .line 2972
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/Map;)V

    .line 2973
    iput-object p1, p0, Lcom/squareup/orders/model/Order$LineItem$Builder;->metadata:Ljava/util/Map;

    return-object p0
.end method

.method public modifiers(Ljava/util/List;)Lcom/squareup/orders/model/Order$LineItem$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$LineItem$Modifier;",
            ">;)",
            "Lcom/squareup/orders/model/Order$LineItem$Builder;"
        }
    .end annotation

    .line 2983
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 2984
    iput-object p1, p0, Lcom/squareup/orders/model/Order$LineItem$Builder;->modifiers:Ljava/util/List;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/orders/model/Order$LineItem$Builder;
    .locals 0

    .line 2833
    iput-object p1, p0, Lcom/squareup/orders/model/Order$LineItem$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public note(Ljava/lang/String;)Lcom/squareup/orders/model/Order$LineItem$Builder;
    .locals 0

    .line 2867
    iput-object p1, p0, Lcom/squareup/orders/model/Order$LineItem$Builder;->note:Ljava/lang/String;

    return-object p0
.end method

.method public quantity(Ljava/lang/String;)Lcom/squareup/orders/model/Order$LineItem$Builder;
    .locals 0

    .line 2847
    iput-object p1, p0, Lcom/squareup/orders/model/Order$LineItem$Builder;->quantity:Ljava/lang/String;

    return-object p0
.end method

.method public quantity_unit(Lcom/squareup/orders/model/Order$QuantityUnit;)Lcom/squareup/orders/model/Order$LineItem$Builder;
    .locals 0

    .line 2857
    iput-object p1, p0, Lcom/squareup/orders/model/Order$LineItem$Builder;->quantity_unit:Lcom/squareup/orders/model/Order$QuantityUnit;

    return-object p0
.end method

.method public sku(Ljava/lang/String;)Lcom/squareup/orders/model/Order$LineItem$Builder;
    .locals 0

    .line 2935
    iput-object p1, p0, Lcom/squareup/orders/model/Order$LineItem$Builder;->sku:Ljava/lang/String;

    return-object p0
.end method

.method public taxes(Ljava/util/List;)Lcom/squareup/orders/model/Order$LineItem$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$LineItem$Tax;",
            ">;)",
            "Lcom/squareup/orders/model/Order$LineItem$Builder;"
        }
    .end annotation

    .line 3003
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 3004
    iput-object p1, p0, Lcom/squareup/orders/model/Order$LineItem$Builder;->taxes:Ljava/util/List;

    return-object p0
.end method

.method public total_discount_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/orders/model/Order$LineItem$Builder;
    .locals 0

    .line 3132
    iput-object p1, p0, Lcom/squareup/orders/model/Order$LineItem$Builder;->total_discount_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public total_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/orders/model/Order$LineItem$Builder;
    .locals 0

    .line 3145
    iput-object p1, p0, Lcom/squareup/orders/model/Order$LineItem$Builder;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public total_tax_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/orders/model/Order$LineItem$Builder;
    .locals 0

    .line 3119
    iput-object p1, p0, Lcom/squareup/orders/model/Order$LineItem$Builder;->total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public uid(Ljava/lang/String;)Lcom/squareup/orders/model/Order$LineItem$Builder;
    .locals 0

    .line 2821
    iput-object p1, p0, Lcom/squareup/orders/model/Order$LineItem$Builder;->uid:Ljava/lang/String;

    return-object p0
.end method

.method public variation_name(Ljava/lang/String;)Lcom/squareup/orders/model/Order$LineItem$Builder;
    .locals 0

    .line 2894
    iput-object p1, p0, Lcom/squareup/orders/model/Order$LineItem$Builder;->variation_name:Ljava/lang/String;

    return-object p0
.end method

.method public variation_total_price_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/orders/model/Order$LineItem$Builder;
    .locals 0

    .line 3092
    iput-object p1, p0, Lcom/squareup/orders/model/Order$LineItem$Builder;->variation_total_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public was_multiple_quantity_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/orders/model/Order$LineItem$Builder;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 3157
    iput-object p1, p0, Lcom/squareup/orders/model/Order$LineItem$Builder;->was_multiple_quantity_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method
