.class public final Lcom/squareup/orders/model/Order$ReturnLineItemModifier;
.super Lcom/squareup/wire/Message;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ReturnLineItemModifier"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orders/model/Order$ReturnLineItemModifier$ProtoAdapter_ReturnLineItemModifier;,
        Lcom/squareup/orders/model/Order$ReturnLineItemModifier$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/orders/model/Order$ReturnLineItemModifier;",
        "Lcom/squareup/orders/model/Order$ReturnLineItemModifier$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/orders/model/Order$ReturnLineItemModifier;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CATALOG_OBJECT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_CATALOG_OBJECT_VERSION:Ljava/lang/Long;

.field public static final DEFAULT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_SOURCE_MODIFIER_UID:Ljava/lang/String; = ""

.field public static final DEFAULT_UID:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final base_price_money:Lcom/squareup/protos/connect/v2/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Money#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final catalog_object_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final catalog_object_version:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x4
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field

.field public final source_modifier_uid:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final total_price_money:Lcom/squareup/protos/connect/v2/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Money#ADAPTER"
        tag = 0x7
    .end annotation
.end field

.field public final uid:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 13054
    new-instance v0, Lcom/squareup/orders/model/Order$ReturnLineItemModifier$ProtoAdapter_ReturnLineItemModifier;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$ReturnLineItemModifier$ProtoAdapter_ReturnLineItemModifier;-><init>()V

    sput-object v0, Lcom/squareup/orders/model/Order$ReturnLineItemModifier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-wide/16 v0, 0x0

    .line 13064
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/orders/model/Order$ReturnLineItemModifier;->DEFAULT_CATALOG_OBJECT_VERSION:Ljava/lang/Long;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/common/Money;)V
    .locals 9

    .line 13149
    sget-object v8, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/orders/model/Order$ReturnLineItemModifier;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/common/Money;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/common/Money;Lokio/ByteString;)V
    .locals 1

    .line 13155
    sget-object v0, Lcom/squareup/orders/model/Order$ReturnLineItemModifier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p8}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 13156
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnLineItemModifier;->uid:Ljava/lang/String;

    .line 13157
    iput-object p2, p0, Lcom/squareup/orders/model/Order$ReturnLineItemModifier;->source_modifier_uid:Ljava/lang/String;

    .line 13158
    iput-object p3, p0, Lcom/squareup/orders/model/Order$ReturnLineItemModifier;->catalog_object_id:Ljava/lang/String;

    .line 13159
    iput-object p4, p0, Lcom/squareup/orders/model/Order$ReturnLineItemModifier;->catalog_object_version:Ljava/lang/Long;

    .line 13160
    iput-object p5, p0, Lcom/squareup/orders/model/Order$ReturnLineItemModifier;->name:Ljava/lang/String;

    .line 13161
    iput-object p6, p0, Lcom/squareup/orders/model/Order$ReturnLineItemModifier;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 13162
    iput-object p7, p0, Lcom/squareup/orders/model/Order$ReturnLineItemModifier;->total_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 13182
    :cond_0
    instance-of v1, p1, Lcom/squareup/orders/model/Order$ReturnLineItemModifier;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 13183
    :cond_1
    check-cast p1, Lcom/squareup/orders/model/Order$ReturnLineItemModifier;

    .line 13184
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$ReturnLineItemModifier;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$ReturnLineItemModifier;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItemModifier;->uid:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ReturnLineItemModifier;->uid:Ljava/lang/String;

    .line 13185
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItemModifier;->source_modifier_uid:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ReturnLineItemModifier;->source_modifier_uid:Ljava/lang/String;

    .line 13186
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItemModifier;->catalog_object_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ReturnLineItemModifier;->catalog_object_id:Ljava/lang/String;

    .line 13187
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItemModifier;->catalog_object_version:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ReturnLineItemModifier;->catalog_object_version:Ljava/lang/Long;

    .line 13188
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItemModifier;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ReturnLineItemModifier;->name:Ljava/lang/String;

    .line 13189
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItemModifier;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ReturnLineItemModifier;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 13190
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItemModifier;->total_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object p1, p1, Lcom/squareup/orders/model/Order$ReturnLineItemModifier;->total_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 13191
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 13196
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_7

    .line 13198
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$ReturnLineItemModifier;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 13199
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItemModifier;->uid:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 13200
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItemModifier;->source_modifier_uid:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 13201
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItemModifier;->catalog_object_id:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 13202
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItemModifier;->catalog_object_version:Ljava/lang/Long;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 13203
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItemModifier;->name:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 13204
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItemModifier;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Money;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 13205
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItemModifier;->total_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Money;->hashCode()I

    move-result v2

    :cond_6
    add-int/2addr v0, v2

    .line 13206
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_7
    return v0
.end method

.method public newBuilder()Lcom/squareup/orders/model/Order$ReturnLineItemModifier$Builder;
    .locals 2

    .line 13167
    new-instance v0, Lcom/squareup/orders/model/Order$ReturnLineItemModifier$Builder;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$ReturnLineItemModifier$Builder;-><init>()V

    .line 13168
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItemModifier;->uid:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnLineItemModifier$Builder;->uid:Ljava/lang/String;

    .line 13169
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItemModifier;->source_modifier_uid:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnLineItemModifier$Builder;->source_modifier_uid:Ljava/lang/String;

    .line 13170
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItemModifier;->catalog_object_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnLineItemModifier$Builder;->catalog_object_id:Ljava/lang/String;

    .line 13171
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItemModifier;->catalog_object_version:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnLineItemModifier$Builder;->catalog_object_version:Ljava/lang/Long;

    .line 13172
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItemModifier;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnLineItemModifier$Builder;->name:Ljava/lang/String;

    .line 13173
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItemModifier;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnLineItemModifier$Builder;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 13174
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItemModifier;->total_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnLineItemModifier$Builder;->total_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 13175
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$ReturnLineItemModifier;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/orders/model/Order$ReturnLineItemModifier$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 13053
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$ReturnLineItemModifier;->newBuilder()Lcom/squareup/orders/model/Order$ReturnLineItemModifier$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 13213
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 13214
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItemModifier;->uid:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItemModifier;->uid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 13215
    :cond_0
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItemModifier;->source_modifier_uid:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", source_modifier_uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItemModifier;->source_modifier_uid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 13216
    :cond_1
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItemModifier;->catalog_object_id:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", catalog_object_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItemModifier;->catalog_object_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 13217
    :cond_2
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItemModifier;->catalog_object_version:Ljava/lang/Long;

    if-eqz v1, :cond_3

    const-string v1, ", catalog_object_version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItemModifier;->catalog_object_version:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 13218
    :cond_3
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItemModifier;->name:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItemModifier;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 13219
    :cond_4
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItemModifier;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_5

    const-string v1, ", base_price_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItemModifier;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 13220
    :cond_5
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItemModifier;->total_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_6

    const-string v1, ", total_price_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItemModifier;->total_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_6
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ReturnLineItemModifier{"

    .line 13221
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
