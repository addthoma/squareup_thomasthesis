.class public final Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount;",
        "Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public applied_money:Lcom/squareup/protos/connect/v2/common/Money;

.field public discount_uid:Ljava/lang/String;

.field public quantity:Ljava/lang/String;

.field public uid:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 5204
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public applied_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount$Builder;
    .locals 0

    .line 5239
    iput-object p1, p0, Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount$Builder;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public build()Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount;
    .locals 7

    .line 5257
    new-instance v6, Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount$Builder;->uid:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount$Builder;->discount_uid:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount$Builder;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v4, p0, Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount$Builder;->quantity:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Money;Ljava/lang/String;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 5195
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount$Builder;->build()Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount;

    move-result-object v0

    return-object v0
.end method

.method public discount_uid(Ljava/lang/String;)Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount$Builder;
    .locals 0

    .line 5227
    iput-object p1, p0, Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount$Builder;->discount_uid:Ljava/lang/String;

    return-object p0
.end method

.method public quantity(Ljava/lang/String;)Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount$Builder;
    .locals 0

    .line 5251
    iput-object p1, p0, Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount$Builder;->quantity:Ljava/lang/String;

    return-object p0
.end method

.method public uid(Ljava/lang/String;)Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount$Builder;
    .locals 0

    .line 5213
    iput-object p1, p0, Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount$Builder;->uid:Ljava/lang/String;

    return-object p0
.end method
