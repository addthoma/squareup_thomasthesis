.class public final Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;
.super Lcom/squareup/wire/Message;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "FulfillmentDeliveryDetails"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$ProtoAdapter_FulfillmentDeliveryDetails;,
        Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;",
        "Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CANCELED_AT:Ljava/lang/String; = ""

.field public static final DEFAULT_CANCEL_REASON:Ljava/lang/String; = ""

.field public static final DEFAULT_COMPLETED_AT:Ljava/lang/String; = ""

.field public static final DEFAULT_DELIVERED_AT:Ljava/lang/String; = ""

.field public static final DEFAULT_DELIVERY_WINDOW_DURATION:Ljava/lang/String; = ""

.field public static final DEFAULT_DELIVER_AT:Ljava/lang/String; = ""

.field public static final DEFAULT_IN_PROGRESS_AT:Ljava/lang/String; = ""

.field public static final DEFAULT_NOTE:Ljava/lang/String; = ""

.field public static final DEFAULT_PLACED_AT:Ljava/lang/String; = ""

.field public static final DEFAULT_PREP_TIME_DURATION:Ljava/lang/String; = ""

.field public static final DEFAULT_READY_AT:Ljava/lang/String; = ""

.field public static final DEFAULT_REJECTED_AT:Ljava/lang/String; = ""

.field public static final DEFAULT_SCHEDULE_TYPE:Lcom/squareup/orders/model/Order$FulfillmentPickupDetailsScheduleType;

.field private static final serialVersionUID:J


# instance fields
.field public final cancel_reason:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xe
    .end annotation
.end field

.field public final canceled_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xd
    .end annotation
.end field

.field public final completed_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x8
    .end annotation
.end field

.field public final deliver_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final delivered_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xc
    .end annotation
.end field

.field public final delivery_window_duration:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x6
    .end annotation
.end field

.field public final in_progress_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x9
    .end annotation
.end field

.field public final note:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x7
    .end annotation
.end field

.field public final placed_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final prep_time_duration:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field

.field public final ready_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xb
    .end annotation
.end field

.field public final recipient:Lcom/squareup/orders/model/Order$FulfillmentRecipient;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$FulfillmentRecipient#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final rejected_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xa
    .end annotation
.end field

.field public final schedule_type:Lcom/squareup/orders/model/Order$FulfillmentPickupDetailsScheduleType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$FulfillmentPickupDetailsScheduleType#ADAPTER"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 10022
    new-instance v0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$ProtoAdapter_FulfillmentDeliveryDetails;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$ProtoAdapter_FulfillmentDeliveryDetails;-><init>()V

    sput-object v0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 10026
    sget-object v0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetailsScheduleType;->SCHEDULED:Lcom/squareup/orders/model/Order$FulfillmentPickupDetailsScheduleType;

    sput-object v0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->DEFAULT_SCHEDULE_TYPE:Lcom/squareup/orders/model/Order$FulfillmentPickupDetailsScheduleType;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/orders/model/Order$FulfillmentRecipient;Lcom/squareup/orders/model/Order$FulfillmentPickupDetailsScheduleType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 16

    .line 10225
    sget-object v15, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    move-object/from16 v14, p14

    invoke-direct/range {v0 .. v15}, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;-><init>(Lcom/squareup/orders/model/Order$FulfillmentRecipient;Lcom/squareup/orders/model/Order$FulfillmentPickupDetailsScheduleType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/orders/model/Order$FulfillmentRecipient;Lcom/squareup/orders/model/Order$FulfillmentPickupDetailsScheduleType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V
    .locals 3

    move-object v0, p0

    .line 10233
    sget-object v1, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    move-object/from16 v2, p15

    invoke-direct {p0, v1, v2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    move-object v1, p1

    .line 10234
    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->recipient:Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    move-object v1, p2

    .line 10235
    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->schedule_type:Lcom/squareup/orders/model/Order$FulfillmentPickupDetailsScheduleType;

    move-object v1, p3

    .line 10236
    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->placed_at:Ljava/lang/String;

    move-object v1, p4

    .line 10237
    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->deliver_at:Ljava/lang/String;

    move-object v1, p5

    .line 10238
    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->prep_time_duration:Ljava/lang/String;

    move-object v1, p6

    .line 10239
    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->delivery_window_duration:Ljava/lang/String;

    move-object v1, p7

    .line 10240
    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->note:Ljava/lang/String;

    move-object v1, p8

    .line 10241
    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->completed_at:Ljava/lang/String;

    move-object v1, p9

    .line 10242
    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->in_progress_at:Ljava/lang/String;

    move-object v1, p10

    .line 10243
    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->rejected_at:Ljava/lang/String;

    move-object v1, p11

    .line 10244
    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->ready_at:Ljava/lang/String;

    move-object v1, p12

    .line 10245
    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->delivered_at:Ljava/lang/String;

    move-object/from16 v1, p13

    .line 10246
    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->canceled_at:Ljava/lang/String;

    move-object/from16 v1, p14

    .line 10247
    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->cancel_reason:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 10274
    :cond_0
    instance-of v1, p1, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 10275
    :cond_1
    check-cast p1, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;

    .line 10276
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->recipient:Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->recipient:Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    .line 10277
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->schedule_type:Lcom/squareup/orders/model/Order$FulfillmentPickupDetailsScheduleType;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->schedule_type:Lcom/squareup/orders/model/Order$FulfillmentPickupDetailsScheduleType;

    .line 10278
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->placed_at:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->placed_at:Ljava/lang/String;

    .line 10279
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->deliver_at:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->deliver_at:Ljava/lang/String;

    .line 10280
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->prep_time_duration:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->prep_time_duration:Ljava/lang/String;

    .line 10281
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->delivery_window_duration:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->delivery_window_duration:Ljava/lang/String;

    .line 10282
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->note:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->note:Ljava/lang/String;

    .line 10283
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->completed_at:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->completed_at:Ljava/lang/String;

    .line 10284
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->in_progress_at:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->in_progress_at:Ljava/lang/String;

    .line 10285
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->rejected_at:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->rejected_at:Ljava/lang/String;

    .line 10286
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->ready_at:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->ready_at:Ljava/lang/String;

    .line 10287
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->delivered_at:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->delivered_at:Ljava/lang/String;

    .line 10288
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->canceled_at:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->canceled_at:Ljava/lang/String;

    .line 10289
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->cancel_reason:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->cancel_reason:Ljava/lang/String;

    .line 10290
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 10295
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_e

    .line 10297
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 10298
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->recipient:Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 10299
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->schedule_type:Lcom/squareup/orders/model/Order$FulfillmentPickupDetailsScheduleType;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$FulfillmentPickupDetailsScheduleType;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 10300
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->placed_at:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 10301
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->deliver_at:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 10302
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->prep_time_duration:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 10303
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->delivery_window_duration:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 10304
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->note:Ljava/lang/String;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 10305
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->completed_at:Ljava/lang/String;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 10306
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->in_progress_at:Ljava/lang/String;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 10307
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->rejected_at:Ljava/lang/String;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 10308
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->ready_at:Ljava/lang/String;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 10309
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->delivered_at:Ljava/lang/String;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 10310
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->canceled_at:Ljava/lang/String;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_c

    :cond_c
    const/4 v1, 0x0

    :goto_c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 10311
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->cancel_reason:Ljava/lang/String;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_d
    add-int/2addr v0, v2

    .line 10312
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_e
    return v0
.end method

.method public newBuilder()Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;
    .locals 2

    .line 10252
    new-instance v0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;-><init>()V

    .line 10253
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->recipient:Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;->recipient:Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    .line 10254
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->schedule_type:Lcom/squareup/orders/model/Order$FulfillmentPickupDetailsScheduleType;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;->schedule_type:Lcom/squareup/orders/model/Order$FulfillmentPickupDetailsScheduleType;

    .line 10255
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->placed_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;->placed_at:Ljava/lang/String;

    .line 10256
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->deliver_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;->deliver_at:Ljava/lang/String;

    .line 10257
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->prep_time_duration:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;->prep_time_duration:Ljava/lang/String;

    .line 10258
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->delivery_window_duration:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;->delivery_window_duration:Ljava/lang/String;

    .line 10259
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->note:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;->note:Ljava/lang/String;

    .line 10260
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->completed_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;->completed_at:Ljava/lang/String;

    .line 10261
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->in_progress_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;->in_progress_at:Ljava/lang/String;

    .line 10262
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->rejected_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;->rejected_at:Ljava/lang/String;

    .line 10263
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->ready_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;->ready_at:Ljava/lang/String;

    .line 10264
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->delivered_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;->delivered_at:Ljava/lang/String;

    .line 10265
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->canceled_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;->canceled_at:Ljava/lang/String;

    .line 10266
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->cancel_reason:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;->cancel_reason:Ljava/lang/String;

    .line 10267
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 10021
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->newBuilder()Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 10319
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 10320
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->recipient:Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    if-eqz v1, :cond_0

    const-string v1, ", recipient="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->recipient:Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 10321
    :cond_0
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->schedule_type:Lcom/squareup/orders/model/Order$FulfillmentPickupDetailsScheduleType;

    if-eqz v1, :cond_1

    const-string v1, ", schedule_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->schedule_type:Lcom/squareup/orders/model/Order$FulfillmentPickupDetailsScheduleType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 10322
    :cond_1
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->placed_at:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", placed_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->placed_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 10323
    :cond_2
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->deliver_at:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", deliver_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->deliver_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 10324
    :cond_3
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->prep_time_duration:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", prep_time_duration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->prep_time_duration:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 10325
    :cond_4
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->delivery_window_duration:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", delivery_window_duration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->delivery_window_duration:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 10326
    :cond_5
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->note:Ljava/lang/String;

    if-eqz v1, :cond_6

    const-string v1, ", note="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->note:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 10327
    :cond_6
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->completed_at:Ljava/lang/String;

    if-eqz v1, :cond_7

    const-string v1, ", completed_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->completed_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 10328
    :cond_7
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->in_progress_at:Ljava/lang/String;

    if-eqz v1, :cond_8

    const-string v1, ", in_progress_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->in_progress_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 10329
    :cond_8
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->rejected_at:Ljava/lang/String;

    if-eqz v1, :cond_9

    const-string v1, ", rejected_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->rejected_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 10330
    :cond_9
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->ready_at:Ljava/lang/String;

    if-eqz v1, :cond_a

    const-string v1, ", ready_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->ready_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 10331
    :cond_a
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->delivered_at:Ljava/lang/String;

    if-eqz v1, :cond_b

    const-string v1, ", delivered_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->delivered_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 10332
    :cond_b
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->canceled_at:Ljava/lang/String;

    if-eqz v1, :cond_c

    const-string v1, ", canceled_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->canceled_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 10333
    :cond_c
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->cancel_reason:Ljava/lang/String;

    if-eqz v1, :cond_d

    const-string v1, ", cancel_reason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->cancel_reason:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_d
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "FulfillmentDeliveryDetails{"

    .line 10334
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
