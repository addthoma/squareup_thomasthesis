.class public final enum Lcom/squareup/orders/model/Order$LineItem$Discount$Type;
.super Ljava/lang/Enum;
.source "Order.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order$LineItem$Discount;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orders/model/Order$LineItem$Discount$Type$ProtoAdapter_Type;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/orders/model/Order$LineItem$Discount$Type;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/orders/model/Order$LineItem$Discount$Type;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/orders/model/Order$LineItem$Discount$Type;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum FIXED_AMOUNT:Lcom/squareup/orders/model/Order$LineItem$Discount$Type;

.field public static final enum FIXED_PERCENTAGE:Lcom/squareup/orders/model/Order$LineItem$Discount$Type;

.field public static final enum UNKNOWN_DISCOUNT:Lcom/squareup/orders/model/Order$LineItem$Discount$Type;

.field public static final enum VARIABLE_AMOUNT:Lcom/squareup/orders/model/Order$LineItem$Discount$Type;

.field public static final enum VARIABLE_PERCENTAGE:Lcom/squareup/orders/model/Order$LineItem$Discount$Type;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 3832
    new-instance v0, Lcom/squareup/orders/model/Order$LineItem$Discount$Type;

    const/4 v1, 0x0

    const-string v2, "UNKNOWN_DISCOUNT"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/orders/model/Order$LineItem$Discount$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/orders/model/Order$LineItem$Discount$Type;->UNKNOWN_DISCOUNT:Lcom/squareup/orders/model/Order$LineItem$Discount$Type;

    .line 3839
    new-instance v0, Lcom/squareup/orders/model/Order$LineItem$Discount$Type;

    const/4 v2, 0x1

    const-string v3, "FIXED_PERCENTAGE"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/orders/model/Order$LineItem$Discount$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/orders/model/Order$LineItem$Discount$Type;->FIXED_PERCENTAGE:Lcom/squareup/orders/model/Order$LineItem$Discount$Type;

    .line 3846
    new-instance v0, Lcom/squareup/orders/model/Order$LineItem$Discount$Type;

    const/4 v3, 0x2

    const-string v4, "FIXED_AMOUNT"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/orders/model/Order$LineItem$Discount$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/orders/model/Order$LineItem$Discount$Type;->FIXED_AMOUNT:Lcom/squareup/orders/model/Order$LineItem$Discount$Type;

    .line 3857
    new-instance v0, Lcom/squareup/orders/model/Order$LineItem$Discount$Type;

    const/4 v4, 0x3

    const-string v5, "VARIABLE_PERCENTAGE"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/orders/model/Order$LineItem$Discount$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/orders/model/Order$LineItem$Discount$Type;->VARIABLE_PERCENTAGE:Lcom/squareup/orders/model/Order$LineItem$Discount$Type;

    .line 3867
    new-instance v0, Lcom/squareup/orders/model/Order$LineItem$Discount$Type;

    const/4 v5, 0x4

    const-string v6, "VARIABLE_AMOUNT"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/orders/model/Order$LineItem$Discount$Type;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/orders/model/Order$LineItem$Discount$Type;->VARIABLE_AMOUNT:Lcom/squareup/orders/model/Order$LineItem$Discount$Type;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/squareup/orders/model/Order$LineItem$Discount$Type;

    .line 3825
    sget-object v6, Lcom/squareup/orders/model/Order$LineItem$Discount$Type;->UNKNOWN_DISCOUNT:Lcom/squareup/orders/model/Order$LineItem$Discount$Type;

    aput-object v6, v0, v1

    sget-object v1, Lcom/squareup/orders/model/Order$LineItem$Discount$Type;->FIXED_PERCENTAGE:Lcom/squareup/orders/model/Order$LineItem$Discount$Type;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/orders/model/Order$LineItem$Discount$Type;->FIXED_AMOUNT:Lcom/squareup/orders/model/Order$LineItem$Discount$Type;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/orders/model/Order$LineItem$Discount$Type;->VARIABLE_PERCENTAGE:Lcom/squareup/orders/model/Order$LineItem$Discount$Type;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/orders/model/Order$LineItem$Discount$Type;->VARIABLE_AMOUNT:Lcom/squareup/orders/model/Order$LineItem$Discount$Type;

    aput-object v1, v0, v5

    sput-object v0, Lcom/squareup/orders/model/Order$LineItem$Discount$Type;->$VALUES:[Lcom/squareup/orders/model/Order$LineItem$Discount$Type;

    .line 3869
    new-instance v0, Lcom/squareup/orders/model/Order$LineItem$Discount$Type$ProtoAdapter_Type;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$LineItem$Discount$Type$ProtoAdapter_Type;-><init>()V

    sput-object v0, Lcom/squareup/orders/model/Order$LineItem$Discount$Type;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 3873
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 3874
    iput p3, p0, Lcom/squareup/orders/model/Order$LineItem$Discount$Type;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/orders/model/Order$LineItem$Discount$Type;
    .locals 1

    if-eqz p0, :cond_4

    const/4 v0, 0x1

    if-eq p0, v0, :cond_3

    const/4 v0, 0x2

    if-eq p0, v0, :cond_2

    const/4 v0, 0x3

    if-eq p0, v0, :cond_1

    const/4 v0, 0x4

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 3886
    :cond_0
    sget-object p0, Lcom/squareup/orders/model/Order$LineItem$Discount$Type;->VARIABLE_AMOUNT:Lcom/squareup/orders/model/Order$LineItem$Discount$Type;

    return-object p0

    .line 3885
    :cond_1
    sget-object p0, Lcom/squareup/orders/model/Order$LineItem$Discount$Type;->VARIABLE_PERCENTAGE:Lcom/squareup/orders/model/Order$LineItem$Discount$Type;

    return-object p0

    .line 3884
    :cond_2
    sget-object p0, Lcom/squareup/orders/model/Order$LineItem$Discount$Type;->FIXED_AMOUNT:Lcom/squareup/orders/model/Order$LineItem$Discount$Type;

    return-object p0

    .line 3883
    :cond_3
    sget-object p0, Lcom/squareup/orders/model/Order$LineItem$Discount$Type;->FIXED_PERCENTAGE:Lcom/squareup/orders/model/Order$LineItem$Discount$Type;

    return-object p0

    .line 3882
    :cond_4
    sget-object p0, Lcom/squareup/orders/model/Order$LineItem$Discount$Type;->UNKNOWN_DISCOUNT:Lcom/squareup/orders/model/Order$LineItem$Discount$Type;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/orders/model/Order$LineItem$Discount$Type;
    .locals 1

    .line 3825
    const-class v0, Lcom/squareup/orders/model/Order$LineItem$Discount$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/orders/model/Order$LineItem$Discount$Type;

    return-object p0
.end method

.method public static values()[Lcom/squareup/orders/model/Order$LineItem$Discount$Type;
    .locals 1

    .line 3825
    sget-object v0, Lcom/squareup/orders/model/Order$LineItem$Discount$Type;->$VALUES:[Lcom/squareup/orders/model/Order$LineItem$Discount$Type;

    invoke-virtual {v0}, [Lcom/squareup/orders/model/Order$LineItem$Discount$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/orders/model/Order$LineItem$Discount$Type;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 3893
    iget v0, p0, Lcom/squareup/orders/model/Order$LineItem$Discount$Type;->value:I

    return v0
.end method
