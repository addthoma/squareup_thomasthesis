.class public final Lcom/squareup/orders/model/Order$MoneyAmounts;
.super Lcom/squareup/wire/Message;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "MoneyAmounts"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orders/model/Order$MoneyAmounts$ProtoAdapter_MoneyAmounts;,
        Lcom/squareup/orders/model/Order$MoneyAmounts$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/orders/model/Order$MoneyAmounts;",
        "Lcom/squareup/orders/model/Order$MoneyAmounts$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/orders/model/Order$MoneyAmounts;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final discount_money:Lcom/squareup/protos/connect/v2/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Money#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final service_charge_money:Lcom/squareup/protos/connect/v2/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Money#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final tax_money:Lcom/squareup/protos/connect/v2/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Money#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final tip_money:Lcom/squareup/protos/connect/v2/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Money#ADAPTER"
        tag = 0x4
    .end annotation
.end field

.field public final total_money:Lcom/squareup/protos/connect/v2/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Money#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 10822
    new-instance v0, Lcom/squareup/orders/model/Order$MoneyAmounts$ProtoAdapter_MoneyAmounts;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$MoneyAmounts$ProtoAdapter_MoneyAmounts;-><init>()V

    sput-object v0, Lcom/squareup/orders/model/Order$MoneyAmounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/common/Money;)V
    .locals 7

    .line 10883
    sget-object v6, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/orders/model/Order$MoneyAmounts;-><init>(Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/common/Money;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/common/Money;Lokio/ByteString;)V
    .locals 1

    .line 10888
    sget-object v0, Lcom/squareup/orders/model/Order$MoneyAmounts;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p6}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 10889
    iput-object p1, p0, Lcom/squareup/orders/model/Order$MoneyAmounts;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 10890
    iput-object p2, p0, Lcom/squareup/orders/model/Order$MoneyAmounts;->tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 10891
    iput-object p3, p0, Lcom/squareup/orders/model/Order$MoneyAmounts;->discount_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 10892
    iput-object p4, p0, Lcom/squareup/orders/model/Order$MoneyAmounts;->tip_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 10893
    iput-object p5, p0, Lcom/squareup/orders/model/Order$MoneyAmounts;->service_charge_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 10911
    :cond_0
    instance-of v1, p1, Lcom/squareup/orders/model/Order$MoneyAmounts;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 10912
    :cond_1
    check-cast p1, Lcom/squareup/orders/model/Order$MoneyAmounts;

    .line 10913
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$MoneyAmounts;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$MoneyAmounts;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$MoneyAmounts;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$MoneyAmounts;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 10914
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$MoneyAmounts;->tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$MoneyAmounts;->tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 10915
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$MoneyAmounts;->discount_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$MoneyAmounts;->discount_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 10916
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$MoneyAmounts;->tip_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$MoneyAmounts;->tip_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 10917
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$MoneyAmounts;->service_charge_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object p1, p1, Lcom/squareup/orders/model/Order$MoneyAmounts;->service_charge_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 10918
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 10923
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_5

    .line 10925
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$MoneyAmounts;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 10926
    iget-object v1, p0, Lcom/squareup/orders/model/Order$MoneyAmounts;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Money;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 10927
    iget-object v1, p0, Lcom/squareup/orders/model/Order$MoneyAmounts;->tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Money;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 10928
    iget-object v1, p0, Lcom/squareup/orders/model/Order$MoneyAmounts;->discount_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Money;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 10929
    iget-object v1, p0, Lcom/squareup/orders/model/Order$MoneyAmounts;->tip_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Money;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 10930
    iget-object v1, p0, Lcom/squareup/orders/model/Order$MoneyAmounts;->service_charge_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Money;->hashCode()I

    move-result v2

    :cond_4
    add-int/2addr v0, v2

    .line 10931
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_5
    return v0
.end method

.method public newBuilder()Lcom/squareup/orders/model/Order$MoneyAmounts$Builder;
    .locals 2

    .line 10898
    new-instance v0, Lcom/squareup/orders/model/Order$MoneyAmounts$Builder;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$MoneyAmounts$Builder;-><init>()V

    .line 10899
    iget-object v1, p0, Lcom/squareup/orders/model/Order$MoneyAmounts;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$MoneyAmounts$Builder;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 10900
    iget-object v1, p0, Lcom/squareup/orders/model/Order$MoneyAmounts;->tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$MoneyAmounts$Builder;->tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 10901
    iget-object v1, p0, Lcom/squareup/orders/model/Order$MoneyAmounts;->discount_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$MoneyAmounts$Builder;->discount_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 10902
    iget-object v1, p0, Lcom/squareup/orders/model/Order$MoneyAmounts;->tip_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$MoneyAmounts$Builder;->tip_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 10903
    iget-object v1, p0, Lcom/squareup/orders/model/Order$MoneyAmounts;->service_charge_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$MoneyAmounts$Builder;->service_charge_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 10904
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$MoneyAmounts;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/orders/model/Order$MoneyAmounts$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 10821
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$MoneyAmounts;->newBuilder()Lcom/squareup/orders/model/Order$MoneyAmounts$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 10938
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 10939
    iget-object v1, p0, Lcom/squareup/orders/model/Order$MoneyAmounts;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_0

    const-string v1, ", total_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$MoneyAmounts;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 10940
    :cond_0
    iget-object v1, p0, Lcom/squareup/orders/model/Order$MoneyAmounts;->tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_1

    const-string v1, ", tax_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$MoneyAmounts;->tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 10941
    :cond_1
    iget-object v1, p0, Lcom/squareup/orders/model/Order$MoneyAmounts;->discount_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_2

    const-string v1, ", discount_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$MoneyAmounts;->discount_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 10942
    :cond_2
    iget-object v1, p0, Lcom/squareup/orders/model/Order$MoneyAmounts;->tip_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_3

    const-string v1, ", tip_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$MoneyAmounts;->tip_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 10943
    :cond_3
    iget-object v1, p0, Lcom/squareup/orders/model/Order$MoneyAmounts;->service_charge_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_4

    const-string v1, ", service_charge_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$MoneyAmounts;->service_charge_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_4
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "MoneyAmounts{"

    .line 10944
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
