.class final Lcom/squareup/orders/model/Order$FulfillmentType$ProtoAdapter_FulfillmentType;
.super Lcom/squareup/wire/EnumAdapter;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order$FulfillmentType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_FulfillmentType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/EnumAdapter<",
        "Lcom/squareup/orders/model/Order$FulfillmentType;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 1

    .line 7712
    const-class v0, Lcom/squareup/orders/model/Order$FulfillmentType;

    invoke-direct {p0, v0}, Lcom/squareup/wire/EnumAdapter;-><init>(Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method protected fromValue(I)Lcom/squareup/orders/model/Order$FulfillmentType;
    .locals 0

    .line 7717
    invoke-static {p1}, Lcom/squareup/orders/model/Order$FulfillmentType;->fromValue(I)Lcom/squareup/orders/model/Order$FulfillmentType;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic fromValue(I)Lcom/squareup/wire/WireEnum;
    .locals 0

    .line 7710
    invoke-virtual {p0, p1}, Lcom/squareup/orders/model/Order$FulfillmentType$ProtoAdapter_FulfillmentType;->fromValue(I)Lcom/squareup/orders/model/Order$FulfillmentType;

    move-result-object p1

    return-object p1
.end method
