.class public final Lcom/squareup/orders/model/Order;
.super Lcom/squareup/wire/Message;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orders/model/Order$ProtoAdapter_Order;,
        Lcom/squareup/orders/model/Order$ReservedReward;,
        Lcom/squareup/orders/model/Order$ReservedDiscountCode;,
        Lcom/squareup/orders/model/Order$PricingOptions;,
        Lcom/squareup/orders/model/Order$ReturnTip;,
        Lcom/squareup/orders/model/Order$ReturnDiscount;,
        Lcom/squareup/orders/model/Order$ReturnTax;,
        Lcom/squareup/orders/model/Order$ReturnServiceCharge;,
        Lcom/squareup/orders/model/Order$ReturnLineItemModifier;,
        Lcom/squareup/orders/model/Order$ReturnLineItem;,
        Lcom/squareup/orders/model/Order$RefundGroup;,
        Lcom/squareup/orders/model/Order$PaymentGroup;,
        Lcom/squareup/orders/model/Order$Return;,
        Lcom/squareup/orders/model/Order$MoneyAmounts;,
        Lcom/squareup/orders/model/Order$RoundingAdjustment;,
        Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;,
        Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;,
        Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;,
        Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;,
        Lcom/squareup/orders/model/Order$FulfillmentRecipient;,
        Lcom/squareup/orders/model/Order$FulfillmentPickupDetailsScheduleType;,
        Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;,
        Lcom/squareup/orders/model/Order$FulfillmentType;,
        Lcom/squareup/orders/model/Order$Fulfillment;,
        Lcom/squareup/orders/model/Order$ServiceCharge;,
        Lcom/squareup/orders/model/Order$Tip;,
        Lcom/squareup/orders/model/Order$LineItem;,
        Lcom/squareup/orders/model/Order$QuantityUnit;,
        Lcom/squareup/orders/model/Order$State;,
        Lcom/squareup/orders/model/Order$Source;,
        Lcom/squareup/orders/model/Order$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/orders/model/Order;",
        "Lcom/squareup/orders/model/Order$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/orders/model/Order;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CLOSED_AT:Ljava/lang/String; = ""

.field public static final DEFAULT_CREATED_AT:Ljava/lang/String; = ""

.field public static final DEFAULT_CREATOR_APP_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_CUSTOMER_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_LOCATION_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_MERCHANT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_REFERENCE_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_SHORT_REFERENCE_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_STATE:Lcom/squareup/orders/model/Order$State;

.field public static final DEFAULT_SUBSTATUS:Ljava/lang/String; = ""

.field public static final DEFAULT_TENDERS_FINALIZED:Ljava/lang/Boolean;

.field public static final DEFAULT_UPDATED_AT:Ljava/lang/String; = ""

.field public static final DEFAULT_VERSION:Ljava/lang/Integer;

.field public static final DEFAULT_WAS_STATUS:Lcom/squareup/orders/model/Order$State;

.field public static final DEFAULT_WORKFLOW:Ljava/lang/String; = ""

.field public static final DEFAULT_WORKFLOW_VERSION:Ljava/lang/Integer;

.field private static final serialVersionUID:J


# instance fields
.field public final closed_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1f
    .end annotation
.end field

.field public final created_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x10
    .end annotation
.end field

.field public final creator_app_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2c
    .end annotation
.end field

.field public final customer_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2e
    .end annotation
.end field

.field public final discounts:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$LineItem$Discount#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x8
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$LineItem$Discount;",
            ">;"
        }
    .end annotation
.end field

.field public final ext_order_client_details:Lcom/squareup/protos/client/orders/OrderClientDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.orders.OrderClientDetails#ADAPTER"
        tag = 0x2710
    .end annotation
.end field

.field public final fulfillments:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$Fulfillment#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0xa
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$Fulfillment;",
            ">;"
        }
    .end annotation
.end field

.field public final id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final line_items:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$LineItem#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x6
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$LineItem;",
            ">;"
        }
    .end annotation
.end field

.field public final location_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final merchant_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final metadata:Ljava/util/Map;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        keyAdapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0xf
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x21
    .end annotation
.end field

.field public final net_amounts:Lcom/squareup/orders/model/Order$MoneyAmounts;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$MoneyAmounts#ADAPTER"
        tag = 0x28
    .end annotation
.end field

.field public final old_metadata_map:Ljava/util/Map;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        keyAdapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x2b
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final payment_groups:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$PaymentGroup#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x2f
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$PaymentGroup;",
            ">;"
        }
    .end annotation
.end field

.field public final pricing_options:Lcom/squareup/orders/model/Order$PricingOptions;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$PricingOptions#ADAPTER"
        tag = 0x35
    .end annotation
.end field

.field public final reference_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x16
    .end annotation
.end field

.field public final refund_groups:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$RefundGroup#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x30
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$RefundGroup;",
            ">;"
        }
    .end annotation
.end field

.field public final refunds:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.resources.Refund#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x29
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/resources/Refund;",
            ">;"
        }
    .end annotation
.end field

.field public final reserved_discount_codes:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$ReservedDiscountCode#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x33
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$ReservedDiscountCode;",
            ">;"
        }
    .end annotation
.end field

.field public final reserved_rewards:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$ReservedReward#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x34
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$ReservedReward;",
            ">;"
        }
    .end annotation
.end field

.field public final return_amounts:Lcom/squareup/orders/model/Order$MoneyAmounts;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$MoneyAmounts#ADAPTER"
        tag = 0x27
    .end annotation
.end field

.field public final returns:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$Return#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x26
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$Return;",
            ">;"
        }
    .end annotation
.end field

.field public final rounding_adjustment:Lcom/squareup/orders/model/Order$RoundingAdjustment;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$RoundingAdjustment#ADAPTER"
        tag = 0x2a
    .end annotation
.end field

.field public final service_charges:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$ServiceCharge#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x22
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$ServiceCharge;",
            ">;"
        }
    .end annotation
.end field

.field public final short_reference_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x25
    .end annotation
.end field

.field public final source:Lcom/squareup/orders/model/Order$Source;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$Source#ADAPTER"
        tag = 0x2d
    .end annotation
.end field

.field public final state:Lcom/squareup/orders/model/Order$State;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$State#ADAPTER"
        tag = 0x12
    .end annotation
.end field

.field public final substatus:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1d
    .end annotation
.end field

.field public final taxes:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$LineItem$Tax#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x7
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$LineItem$Tax;",
            ">;"
        }
    .end annotation
.end field

.field public final tenders:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.resources.Tender#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x20
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/connect/v2/resources/Tender;",
            ">;"
        }
    .end annotation
.end field

.field public final tenders_finalized:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x24
    .end annotation
.end field

.field public final tips:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$Tip#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x9
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$Tip;",
            ">;"
        }
    .end annotation
.end field

.field public final total_discount_money:Lcom/squareup/protos/connect/v2/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Money#ADAPTER"
        tag = 0x1a
    .end annotation
.end field

.field public final total_money:Lcom/squareup/protos/connect/v2/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Money#ADAPTER"
        tag = 0x1c
    .end annotation
.end field

.field public final total_service_charge_money:Lcom/squareup/protos/connect/v2/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Money#ADAPTER"
        tag = 0x23
    .end annotation
.end field

.field public final total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Money#ADAPTER"
        tag = 0x17
    .end annotation
.end field

.field public final total_tip_money:Lcom/squareup/protos/connect/v2/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Money#ADAPTER"
        tag = 0x1b
    .end annotation
.end field

.field public final updated_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x11
    .end annotation
.end field

.field public final vaulted_data:Lcom/squareup/protos/common/privacyvault/VaultedData;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.privacyvault.VaultedData#ADAPTER"
        tag = 0x32
    .end annotation
.end field

.field public final version:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x13
    .end annotation
.end field

.field public final was_status:Lcom/squareup/orders/model/Order$State;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$State#ADAPTER"
        tag = 0x2328
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final workflow:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x14
    .end annotation
.end field

.field public final workflow_version:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT32"
        tag = 0x15
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 48
    new-instance v0, Lcom/squareup/orders/model/Order$ProtoAdapter_Order;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$ProtoAdapter_Order;-><init>()V

    sput-object v0, Lcom/squareup/orders/model/Order;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 66
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/orders/model/Order;->DEFAULT_TENDERS_FINALIZED:Ljava/lang/Boolean;

    .line 74
    sget-object v0, Lcom/squareup/orders/model/Order$State;->DO_NOT_USE:Lcom/squareup/orders/model/Order$State;

    sput-object v0, Lcom/squareup/orders/model/Order;->DEFAULT_STATE:Lcom/squareup/orders/model/Order$State;

    .line 78
    sput-object v1, Lcom/squareup/orders/model/Order;->DEFAULT_VERSION:Ljava/lang/Integer;

    .line 82
    sput-object v1, Lcom/squareup/orders/model/Order;->DEFAULT_WORKFLOW_VERSION:Ljava/lang/Integer;

    .line 86
    sget-object v0, Lcom/squareup/orders/model/Order$State;->DO_NOT_USE:Lcom/squareup/orders/model/Order$State;

    sput-object v0, Lcom/squareup/orders/model/Order;->DEFAULT_WAS_STATUS:Lcom/squareup/orders/model/Order$State;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/orders/model/Order$Builder;Lokio/ByteString;)V
    .locals 1

    .line 695
    sget-object v0, Lcom/squareup/orders/model/Order;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 696
    iget-object p2, p1, Lcom/squareup/orders/model/Order$Builder;->id:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/orders/model/Order;->id:Ljava/lang/String;

    .line 697
    iget-object p2, p1, Lcom/squareup/orders/model/Order$Builder;->location_id:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/orders/model/Order;->location_id:Ljava/lang/String;

    .line 698
    iget-object p2, p1, Lcom/squareup/orders/model/Order$Builder;->reference_id:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/orders/model/Order;->reference_id:Ljava/lang/String;

    .line 699
    iget-object p2, p1, Lcom/squareup/orders/model/Order$Builder;->creator_app_id:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/orders/model/Order;->creator_app_id:Ljava/lang/String;

    .line 700
    iget-object p2, p1, Lcom/squareup/orders/model/Order$Builder;->source:Lcom/squareup/orders/model/Order$Source;

    iput-object p2, p0, Lcom/squareup/orders/model/Order;->source:Lcom/squareup/orders/model/Order$Source;

    .line 701
    iget-object p2, p1, Lcom/squareup/orders/model/Order$Builder;->customer_id:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/orders/model/Order;->customer_id:Ljava/lang/String;

    .line 702
    iget-object p2, p1, Lcom/squareup/orders/model/Order$Builder;->name:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/orders/model/Order;->name:Ljava/lang/String;

    .line 703
    iget-object p2, p1, Lcom/squareup/orders/model/Order$Builder;->merchant_id:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/orders/model/Order;->merchant_id:Ljava/lang/String;

    .line 704
    iget-object p2, p1, Lcom/squareup/orders/model/Order$Builder;->line_items:Ljava/util/List;

    const-string v0, "line_items"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/orders/model/Order;->line_items:Ljava/util/List;

    .line 705
    iget-object p2, p1, Lcom/squareup/orders/model/Order$Builder;->taxes:Ljava/util/List;

    const-string v0, "taxes"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/orders/model/Order;->taxes:Ljava/util/List;

    .line 706
    iget-object p2, p1, Lcom/squareup/orders/model/Order$Builder;->discounts:Ljava/util/List;

    const-string v0, "discounts"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/orders/model/Order;->discounts:Ljava/util/List;

    .line 707
    iget-object p2, p1, Lcom/squareup/orders/model/Order$Builder;->service_charges:Ljava/util/List;

    const-string v0, "service_charges"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/orders/model/Order;->service_charges:Ljava/util/List;

    .line 708
    iget-object p2, p1, Lcom/squareup/orders/model/Order$Builder;->tips:Ljava/util/List;

    const-string v0, "tips"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/orders/model/Order;->tips:Ljava/util/List;

    .line 709
    iget-object p2, p1, Lcom/squareup/orders/model/Order$Builder;->fulfillments:Ljava/util/List;

    const-string v0, "fulfillments"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/orders/model/Order;->fulfillments:Ljava/util/List;

    .line 710
    iget-object p2, p1, Lcom/squareup/orders/model/Order$Builder;->returns:Ljava/util/List;

    const-string v0, "returns"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/orders/model/Order;->returns:Ljava/util/List;

    .line 711
    iget-object p2, p1, Lcom/squareup/orders/model/Order$Builder;->return_amounts:Lcom/squareup/orders/model/Order$MoneyAmounts;

    iput-object p2, p0, Lcom/squareup/orders/model/Order;->return_amounts:Lcom/squareup/orders/model/Order$MoneyAmounts;

    .line 712
    iget-object p2, p1, Lcom/squareup/orders/model/Order$Builder;->net_amounts:Lcom/squareup/orders/model/Order$MoneyAmounts;

    iput-object p2, p0, Lcom/squareup/orders/model/Order;->net_amounts:Lcom/squareup/orders/model/Order$MoneyAmounts;

    .line 713
    iget-object p2, p1, Lcom/squareup/orders/model/Order$Builder;->rounding_adjustment:Lcom/squareup/orders/model/Order$RoundingAdjustment;

    iput-object p2, p0, Lcom/squareup/orders/model/Order;->rounding_adjustment:Lcom/squareup/orders/model/Order$RoundingAdjustment;

    .line 714
    iget-object p2, p1, Lcom/squareup/orders/model/Order$Builder;->tenders_finalized:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/orders/model/Order;->tenders_finalized:Ljava/lang/Boolean;

    .line 715
    iget-object p2, p1, Lcom/squareup/orders/model/Order$Builder;->tenders:Ljava/util/List;

    const-string v0, "tenders"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/orders/model/Order;->tenders:Ljava/util/List;

    .line 716
    iget-object p2, p1, Lcom/squareup/orders/model/Order$Builder;->payment_groups:Ljava/util/List;

    const-string v0, "payment_groups"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/orders/model/Order;->payment_groups:Ljava/util/List;

    .line 717
    iget-object p2, p1, Lcom/squareup/orders/model/Order$Builder;->refunds:Ljava/util/List;

    const-string v0, "refunds"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/orders/model/Order;->refunds:Ljava/util/List;

    .line 718
    iget-object p2, p1, Lcom/squareup/orders/model/Order$Builder;->refund_groups:Ljava/util/List;

    const-string v0, "refund_groups"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/orders/model/Order;->refund_groups:Ljava/util/List;

    .line 719
    iget-object p2, p1, Lcom/squareup/orders/model/Order$Builder;->metadata:Ljava/util/Map;

    const-string v0, "metadata"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/Map;)Ljava/util/Map;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/orders/model/Order;->metadata:Ljava/util/Map;

    .line 720
    iget-object p2, p1, Lcom/squareup/orders/model/Order$Builder;->old_metadata_map:Ljava/util/Map;

    const-string v0, "old_metadata_map"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/Map;)Ljava/util/Map;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/orders/model/Order;->old_metadata_map:Ljava/util/Map;

    .line 721
    iget-object p2, p1, Lcom/squareup/orders/model/Order$Builder;->created_at:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/orders/model/Order;->created_at:Ljava/lang/String;

    .line 722
    iget-object p2, p1, Lcom/squareup/orders/model/Order$Builder;->updated_at:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/orders/model/Order;->updated_at:Ljava/lang/String;

    .line 723
    iget-object p2, p1, Lcom/squareup/orders/model/Order$Builder;->closed_at:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/orders/model/Order;->closed_at:Ljava/lang/String;

    .line 724
    iget-object p2, p1, Lcom/squareup/orders/model/Order$Builder;->state:Lcom/squareup/orders/model/Order$State;

    iput-object p2, p0, Lcom/squareup/orders/model/Order;->state:Lcom/squareup/orders/model/Order$State;

    .line 725
    iget-object p2, p1, Lcom/squareup/orders/model/Order$Builder;->substatus:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/orders/model/Order;->substatus:Ljava/lang/String;

    .line 726
    iget-object p2, p1, Lcom/squareup/orders/model/Order$Builder;->version:Ljava/lang/Integer;

    iput-object p2, p0, Lcom/squareup/orders/model/Order;->version:Ljava/lang/Integer;

    .line 727
    iget-object p2, p1, Lcom/squareup/orders/model/Order$Builder;->workflow:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/orders/model/Order;->workflow:Ljava/lang/String;

    .line 728
    iget-object p2, p1, Lcom/squareup/orders/model/Order$Builder;->workflow_version:Ljava/lang/Integer;

    iput-object p2, p0, Lcom/squareup/orders/model/Order;->workflow_version:Ljava/lang/Integer;

    .line 729
    iget-object p2, p1, Lcom/squareup/orders/model/Order$Builder;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object p2, p0, Lcom/squareup/orders/model/Order;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 730
    iget-object p2, p1, Lcom/squareup/orders/model/Order$Builder;->total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object p2, p0, Lcom/squareup/orders/model/Order;->total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 731
    iget-object p2, p1, Lcom/squareup/orders/model/Order$Builder;->total_discount_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object p2, p0, Lcom/squareup/orders/model/Order;->total_discount_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 732
    iget-object p2, p1, Lcom/squareup/orders/model/Order$Builder;->total_tip_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object p2, p0, Lcom/squareup/orders/model/Order;->total_tip_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 733
    iget-object p2, p1, Lcom/squareup/orders/model/Order$Builder;->total_service_charge_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object p2, p0, Lcom/squareup/orders/model/Order;->total_service_charge_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 734
    iget-object p2, p1, Lcom/squareup/orders/model/Order$Builder;->short_reference_id:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/orders/model/Order;->short_reference_id:Ljava/lang/String;

    .line 735
    iget-object p2, p1, Lcom/squareup/orders/model/Order$Builder;->pricing_options:Lcom/squareup/orders/model/Order$PricingOptions;

    iput-object p2, p0, Lcom/squareup/orders/model/Order;->pricing_options:Lcom/squareup/orders/model/Order$PricingOptions;

    .line 736
    iget-object p2, p1, Lcom/squareup/orders/model/Order$Builder;->reserved_discount_codes:Ljava/util/List;

    const-string v0, "reserved_discount_codes"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/orders/model/Order;->reserved_discount_codes:Ljava/util/List;

    .line 737
    iget-object p2, p1, Lcom/squareup/orders/model/Order$Builder;->reserved_rewards:Ljava/util/List;

    const-string v0, "reserved_rewards"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/orders/model/Order;->reserved_rewards:Ljava/util/List;

    .line 738
    iget-object p2, p1, Lcom/squareup/orders/model/Order$Builder;->vaulted_data:Lcom/squareup/protos/common/privacyvault/VaultedData;

    iput-object p2, p0, Lcom/squareup/orders/model/Order;->vaulted_data:Lcom/squareup/protos/common/privacyvault/VaultedData;

    .line 739
    iget-object p2, p1, Lcom/squareup/orders/model/Order$Builder;->was_status:Lcom/squareup/orders/model/Order$State;

    iput-object p2, p0, Lcom/squareup/orders/model/Order;->was_status:Lcom/squareup/orders/model/Order$State;

    .line 740
    iget-object p1, p1, Lcom/squareup/orders/model/Order$Builder;->ext_order_client_details:Lcom/squareup/protos/client/orders/OrderClientDetails;

    iput-object p1, p0, Lcom/squareup/orders/model/Order;->ext_order_client_details:Lcom/squareup/protos/client/orders/OrderClientDetails;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 798
    :cond_0
    instance-of v1, p1, Lcom/squareup/orders/model/Order;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 799
    :cond_1
    check-cast p1, Lcom/squareup/orders/model/Order;

    .line 800
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/orders/model/Order;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order;->id:Ljava/lang/String;

    .line 801
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->location_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order;->location_id:Ljava/lang/String;

    .line 802
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->reference_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order;->reference_id:Ljava/lang/String;

    .line 803
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->creator_app_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order;->creator_app_id:Ljava/lang/String;

    .line 804
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->source:Lcom/squareup/orders/model/Order$Source;

    iget-object v3, p1, Lcom/squareup/orders/model/Order;->source:Lcom/squareup/orders/model/Order$Source;

    .line 805
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->customer_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order;->customer_id:Ljava/lang/String;

    .line 806
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order;->name:Ljava/lang/String;

    .line 807
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->merchant_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order;->merchant_id:Ljava/lang/String;

    .line 808
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->line_items:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/orders/model/Order;->line_items:Ljava/util/List;

    .line 809
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->taxes:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/orders/model/Order;->taxes:Ljava/util/List;

    .line 810
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->discounts:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/orders/model/Order;->discounts:Ljava/util/List;

    .line 811
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->service_charges:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/orders/model/Order;->service_charges:Ljava/util/List;

    .line 812
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->tips:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/orders/model/Order;->tips:Ljava/util/List;

    .line 813
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->fulfillments:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/orders/model/Order;->fulfillments:Ljava/util/List;

    .line 814
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->returns:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/orders/model/Order;->returns:Ljava/util/List;

    .line 815
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->return_amounts:Lcom/squareup/orders/model/Order$MoneyAmounts;

    iget-object v3, p1, Lcom/squareup/orders/model/Order;->return_amounts:Lcom/squareup/orders/model/Order$MoneyAmounts;

    .line 816
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->net_amounts:Lcom/squareup/orders/model/Order$MoneyAmounts;

    iget-object v3, p1, Lcom/squareup/orders/model/Order;->net_amounts:Lcom/squareup/orders/model/Order$MoneyAmounts;

    .line 817
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->rounding_adjustment:Lcom/squareup/orders/model/Order$RoundingAdjustment;

    iget-object v3, p1, Lcom/squareup/orders/model/Order;->rounding_adjustment:Lcom/squareup/orders/model/Order$RoundingAdjustment;

    .line 818
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->tenders_finalized:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/orders/model/Order;->tenders_finalized:Ljava/lang/Boolean;

    .line 819
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->tenders:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/orders/model/Order;->tenders:Ljava/util/List;

    .line 820
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->payment_groups:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/orders/model/Order;->payment_groups:Ljava/util/List;

    .line 821
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->refunds:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/orders/model/Order;->refunds:Ljava/util/List;

    .line 822
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->refund_groups:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/orders/model/Order;->refund_groups:Ljava/util/List;

    .line 823
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->metadata:Ljava/util/Map;

    iget-object v3, p1, Lcom/squareup/orders/model/Order;->metadata:Ljava/util/Map;

    .line 824
    invoke-interface {v1, v3}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->old_metadata_map:Ljava/util/Map;

    iget-object v3, p1, Lcom/squareup/orders/model/Order;->old_metadata_map:Ljava/util/Map;

    .line 825
    invoke-interface {v1, v3}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->created_at:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order;->created_at:Ljava/lang/String;

    .line 826
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->updated_at:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order;->updated_at:Ljava/lang/String;

    .line 827
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->closed_at:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order;->closed_at:Ljava/lang/String;

    .line 828
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->state:Lcom/squareup/orders/model/Order$State;

    iget-object v3, p1, Lcom/squareup/orders/model/Order;->state:Lcom/squareup/orders/model/Order$State;

    .line 829
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->substatus:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order;->substatus:Ljava/lang/String;

    .line 830
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->version:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/orders/model/Order;->version:Ljava/lang/Integer;

    .line 831
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->workflow:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order;->workflow:Ljava/lang/String;

    .line 832
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->workflow_version:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/squareup/orders/model/Order;->workflow_version:Ljava/lang/Integer;

    .line 833
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v3, p1, Lcom/squareup/orders/model/Order;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 834
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v3, p1, Lcom/squareup/orders/model/Order;->total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 835
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->total_discount_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v3, p1, Lcom/squareup/orders/model/Order;->total_discount_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 836
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->total_tip_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v3, p1, Lcom/squareup/orders/model/Order;->total_tip_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 837
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->total_service_charge_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v3, p1, Lcom/squareup/orders/model/Order;->total_service_charge_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 838
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->short_reference_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order;->short_reference_id:Ljava/lang/String;

    .line 839
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->pricing_options:Lcom/squareup/orders/model/Order$PricingOptions;

    iget-object v3, p1, Lcom/squareup/orders/model/Order;->pricing_options:Lcom/squareup/orders/model/Order$PricingOptions;

    .line 840
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->reserved_discount_codes:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/orders/model/Order;->reserved_discount_codes:Ljava/util/List;

    .line 841
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->reserved_rewards:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/orders/model/Order;->reserved_rewards:Ljava/util/List;

    .line 842
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->vaulted_data:Lcom/squareup/protos/common/privacyvault/VaultedData;

    iget-object v3, p1, Lcom/squareup/orders/model/Order;->vaulted_data:Lcom/squareup/protos/common/privacyvault/VaultedData;

    .line 843
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->was_status:Lcom/squareup/orders/model/Order$State;

    iget-object v3, p1, Lcom/squareup/orders/model/Order;->was_status:Lcom/squareup/orders/model/Order$State;

    .line 844
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->ext_order_client_details:Lcom/squareup/protos/client/orders/OrderClientDetails;

    iget-object p1, p1, Lcom/squareup/orders/model/Order;->ext_order_client_details:Lcom/squareup/protos/client/orders/OrderClientDetails;

    .line 845
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 850
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_1e

    .line 852
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 853
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 854
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->location_id:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 855
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->reference_id:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 856
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->creator_app_id:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 857
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->source:Lcom/squareup/orders/model/Order$Source;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$Source;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 858
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->customer_id:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 859
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->name:Ljava/lang/String;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 860
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->merchant_id:Ljava/lang/String;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 861
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->line_items:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 862
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->taxes:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 863
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->discounts:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 864
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->service_charges:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 865
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->tips:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 866
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->fulfillments:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 867
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->returns:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 868
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->return_amounts:Lcom/squareup/orders/model/Order$MoneyAmounts;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$MoneyAmounts;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 869
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->net_amounts:Lcom/squareup/orders/model/Order$MoneyAmounts;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$MoneyAmounts;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 870
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->rounding_adjustment:Lcom/squareup/orders/model/Order$RoundingAdjustment;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$RoundingAdjustment;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 871
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->tenders_finalized:Ljava/lang/Boolean;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 872
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->tenders:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 873
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->payment_groups:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 874
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->refunds:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 875
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->refund_groups:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 876
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->metadata:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 877
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->old_metadata_map:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 878
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->created_at:Ljava/lang/String;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_c

    :cond_c
    const/4 v1, 0x0

    :goto_c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 879
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->updated_at:Ljava/lang/String;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_d

    :cond_d
    const/4 v1, 0x0

    :goto_d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 880
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->closed_at:Ljava/lang/String;

    if-eqz v1, :cond_e

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_e

    :cond_e
    const/4 v1, 0x0

    :goto_e
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 881
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->state:Lcom/squareup/orders/model/Order$State;

    if-eqz v1, :cond_f

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$State;->hashCode()I

    move-result v1

    goto :goto_f

    :cond_f
    const/4 v1, 0x0

    :goto_f
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 882
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->substatus:Ljava/lang/String;

    if-eqz v1, :cond_10

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_10

    :cond_10
    const/4 v1, 0x0

    :goto_10
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 883
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->version:Ljava/lang/Integer;

    if-eqz v1, :cond_11

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_11

    :cond_11
    const/4 v1, 0x0

    :goto_11
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 884
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->workflow:Ljava/lang/String;

    if-eqz v1, :cond_12

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_12

    :cond_12
    const/4 v1, 0x0

    :goto_12
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 885
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->workflow_version:Ljava/lang/Integer;

    if-eqz v1, :cond_13

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_13

    :cond_13
    const/4 v1, 0x0

    :goto_13
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 886
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_14

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Money;->hashCode()I

    move-result v1

    goto :goto_14

    :cond_14
    const/4 v1, 0x0

    :goto_14
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 887
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_15

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Money;->hashCode()I

    move-result v1

    goto :goto_15

    :cond_15
    const/4 v1, 0x0

    :goto_15
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 888
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->total_discount_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_16

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Money;->hashCode()I

    move-result v1

    goto :goto_16

    :cond_16
    const/4 v1, 0x0

    :goto_16
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 889
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->total_tip_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_17

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Money;->hashCode()I

    move-result v1

    goto :goto_17

    :cond_17
    const/4 v1, 0x0

    :goto_17
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 890
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->total_service_charge_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_18

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Money;->hashCode()I

    move-result v1

    goto :goto_18

    :cond_18
    const/4 v1, 0x0

    :goto_18
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 891
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->short_reference_id:Ljava/lang/String;

    if-eqz v1, :cond_19

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_19

    :cond_19
    const/4 v1, 0x0

    :goto_19
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 892
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->pricing_options:Lcom/squareup/orders/model/Order$PricingOptions;

    if-eqz v1, :cond_1a

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$PricingOptions;->hashCode()I

    move-result v1

    goto :goto_1a

    :cond_1a
    const/4 v1, 0x0

    :goto_1a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 893
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->reserved_discount_codes:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 894
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->reserved_rewards:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 895
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->vaulted_data:Lcom/squareup/protos/common/privacyvault/VaultedData;

    if-eqz v1, :cond_1b

    invoke-virtual {v1}, Lcom/squareup/protos/common/privacyvault/VaultedData;->hashCode()I

    move-result v1

    goto :goto_1b

    :cond_1b
    const/4 v1, 0x0

    :goto_1b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 896
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->was_status:Lcom/squareup/orders/model/Order$State;

    if-eqz v1, :cond_1c

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$State;->hashCode()I

    move-result v1

    goto :goto_1c

    :cond_1c
    const/4 v1, 0x0

    :goto_1c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 897
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->ext_order_client_details:Lcom/squareup/protos/client/orders/OrderClientDetails;

    if-eqz v1, :cond_1d

    invoke-virtual {v1}, Lcom/squareup/protos/client/orders/OrderClientDetails;->hashCode()I

    move-result v2

    :cond_1d
    add-int/2addr v0, v2

    .line 898
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_1e
    return v0
.end method

.method public newBuilder()Lcom/squareup/orders/model/Order$Builder;
    .locals 2

    .line 745
    new-instance v0, Lcom/squareup/orders/model/Order$Builder;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$Builder;-><init>()V

    .line 746
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Builder;->id:Ljava/lang/String;

    .line 747
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->location_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Builder;->location_id:Ljava/lang/String;

    .line 748
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->reference_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Builder;->reference_id:Ljava/lang/String;

    .line 749
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->creator_app_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Builder;->creator_app_id:Ljava/lang/String;

    .line 750
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->source:Lcom/squareup/orders/model/Order$Source;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Builder;->source:Lcom/squareup/orders/model/Order$Source;

    .line 751
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->customer_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Builder;->customer_id:Ljava/lang/String;

    .line 752
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Builder;->name:Ljava/lang/String;

    .line 753
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->merchant_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Builder;->merchant_id:Ljava/lang/String;

    .line 754
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->line_items:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Builder;->line_items:Ljava/util/List;

    .line 755
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->taxes:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Builder;->taxes:Ljava/util/List;

    .line 756
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->discounts:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Builder;->discounts:Ljava/util/List;

    .line 757
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->service_charges:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Builder;->service_charges:Ljava/util/List;

    .line 758
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->tips:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Builder;->tips:Ljava/util/List;

    .line 759
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->fulfillments:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Builder;->fulfillments:Ljava/util/List;

    .line 760
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->returns:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Builder;->returns:Ljava/util/List;

    .line 761
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->return_amounts:Lcom/squareup/orders/model/Order$MoneyAmounts;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Builder;->return_amounts:Lcom/squareup/orders/model/Order$MoneyAmounts;

    .line 762
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->net_amounts:Lcom/squareup/orders/model/Order$MoneyAmounts;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Builder;->net_amounts:Lcom/squareup/orders/model/Order$MoneyAmounts;

    .line 763
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->rounding_adjustment:Lcom/squareup/orders/model/Order$RoundingAdjustment;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Builder;->rounding_adjustment:Lcom/squareup/orders/model/Order$RoundingAdjustment;

    .line 764
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->tenders_finalized:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Builder;->tenders_finalized:Ljava/lang/Boolean;

    .line 765
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->tenders:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Builder;->tenders:Ljava/util/List;

    .line 766
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->payment_groups:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Builder;->payment_groups:Ljava/util/List;

    .line 767
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->refunds:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Builder;->refunds:Ljava/util/List;

    .line 768
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->refund_groups:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Builder;->refund_groups:Ljava/util/List;

    .line 769
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->metadata:Ljava/util/Map;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Builder;->metadata:Ljava/util/Map;

    .line 770
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->old_metadata_map:Ljava/util/Map;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Builder;->old_metadata_map:Ljava/util/Map;

    .line 771
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->created_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Builder;->created_at:Ljava/lang/String;

    .line 772
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->updated_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Builder;->updated_at:Ljava/lang/String;

    .line 773
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->closed_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Builder;->closed_at:Ljava/lang/String;

    .line 774
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->state:Lcom/squareup/orders/model/Order$State;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Builder;->state:Lcom/squareup/orders/model/Order$State;

    .line 775
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->substatus:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Builder;->substatus:Ljava/lang/String;

    .line 776
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->version:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Builder;->version:Ljava/lang/Integer;

    .line 777
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->workflow:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Builder;->workflow:Ljava/lang/String;

    .line 778
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->workflow_version:Ljava/lang/Integer;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Builder;->workflow_version:Ljava/lang/Integer;

    .line 779
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Builder;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 780
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Builder;->total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 781
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->total_discount_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Builder;->total_discount_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 782
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->total_tip_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Builder;->total_tip_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 783
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->total_service_charge_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Builder;->total_service_charge_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 784
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->short_reference_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Builder;->short_reference_id:Ljava/lang/String;

    .line 785
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->pricing_options:Lcom/squareup/orders/model/Order$PricingOptions;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Builder;->pricing_options:Lcom/squareup/orders/model/Order$PricingOptions;

    .line 786
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->reserved_discount_codes:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Builder;->reserved_discount_codes:Ljava/util/List;

    .line 787
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->reserved_rewards:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Builder;->reserved_rewards:Ljava/util/List;

    .line 788
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->vaulted_data:Lcom/squareup/protos/common/privacyvault/VaultedData;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Builder;->vaulted_data:Lcom/squareup/protos/common/privacyvault/VaultedData;

    .line 789
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->was_status:Lcom/squareup/orders/model/Order$State;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Builder;->was_status:Lcom/squareup/orders/model/Order$State;

    .line 790
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->ext_order_client_details:Lcom/squareup/protos/client/orders/OrderClientDetails;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Builder;->ext_order_client_details:Lcom/squareup/protos/client/orders/OrderClientDetails;

    .line 791
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/orders/model/Order$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 47
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order;->newBuilder()Lcom/squareup/orders/model/Order$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 905
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 906
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 907
    :cond_0
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->location_id:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", location_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->location_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 908
    :cond_1
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->reference_id:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", reference_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->reference_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 909
    :cond_2
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->creator_app_id:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", creator_app_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->creator_app_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 910
    :cond_3
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->source:Lcom/squareup/orders/model/Order$Source;

    if-eqz v1, :cond_4

    const-string v1, ", source="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->source:Lcom/squareup/orders/model/Order$Source;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 911
    :cond_4
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->customer_id:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", customer_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->customer_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 912
    :cond_5
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->name:Ljava/lang/String;

    if-eqz v1, :cond_6

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 913
    :cond_6
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->merchant_id:Ljava/lang/String;

    if-eqz v1, :cond_7

    const-string v1, ", merchant_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->merchant_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 914
    :cond_7
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->line_items:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_8

    const-string v1, ", line_items="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->line_items:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 915
    :cond_8
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->taxes:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_9

    const-string v1, ", taxes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->taxes:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 916
    :cond_9
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->discounts:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_a

    const-string v1, ", discounts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->discounts:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 917
    :cond_a
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->service_charges:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_b

    const-string v1, ", service_charges="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->service_charges:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 918
    :cond_b
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->tips:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_c

    const-string v1, ", tips="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->tips:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 919
    :cond_c
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->fulfillments:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_d

    const-string v1, ", fulfillments="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->fulfillments:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 920
    :cond_d
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->returns:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_e

    const-string v1, ", returns="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->returns:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 921
    :cond_e
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->return_amounts:Lcom/squareup/orders/model/Order$MoneyAmounts;

    if-eqz v1, :cond_f

    const-string v1, ", return_amounts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->return_amounts:Lcom/squareup/orders/model/Order$MoneyAmounts;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 922
    :cond_f
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->net_amounts:Lcom/squareup/orders/model/Order$MoneyAmounts;

    if-eqz v1, :cond_10

    const-string v1, ", net_amounts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->net_amounts:Lcom/squareup/orders/model/Order$MoneyAmounts;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 923
    :cond_10
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->rounding_adjustment:Lcom/squareup/orders/model/Order$RoundingAdjustment;

    if-eqz v1, :cond_11

    const-string v1, ", rounding_adjustment="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->rounding_adjustment:Lcom/squareup/orders/model/Order$RoundingAdjustment;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 924
    :cond_11
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->tenders_finalized:Ljava/lang/Boolean;

    if-eqz v1, :cond_12

    const-string v1, ", tenders_finalized="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->tenders_finalized:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 925
    :cond_12
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->tenders:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_13

    const-string v1, ", tenders="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->tenders:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 926
    :cond_13
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->payment_groups:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_14

    const-string v1, ", payment_groups="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->payment_groups:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 927
    :cond_14
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->refunds:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_15

    const-string v1, ", refunds="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->refunds:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 928
    :cond_15
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->refund_groups:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_16

    const-string v1, ", refund_groups="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->refund_groups:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 929
    :cond_16
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->metadata:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_17

    const-string v1, ", metadata=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 930
    :cond_17
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->old_metadata_map:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_18

    const-string v1, ", old_metadata_map=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 931
    :cond_18
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->created_at:Ljava/lang/String;

    if-eqz v1, :cond_19

    const-string v1, ", created_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->created_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 932
    :cond_19
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->updated_at:Ljava/lang/String;

    if-eqz v1, :cond_1a

    const-string v1, ", updated_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->updated_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 933
    :cond_1a
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->closed_at:Ljava/lang/String;

    if-eqz v1, :cond_1b

    const-string v1, ", closed_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->closed_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 934
    :cond_1b
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->state:Lcom/squareup/orders/model/Order$State;

    if-eqz v1, :cond_1c

    const-string v1, ", state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->state:Lcom/squareup/orders/model/Order$State;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 935
    :cond_1c
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->substatus:Ljava/lang/String;

    if-eqz v1, :cond_1d

    const-string v1, ", substatus="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->substatus:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 936
    :cond_1d
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->version:Ljava/lang/Integer;

    if-eqz v1, :cond_1e

    const-string v1, ", version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->version:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 937
    :cond_1e
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->workflow:Ljava/lang/String;

    if-eqz v1, :cond_1f

    const-string v1, ", workflow="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->workflow:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 938
    :cond_1f
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->workflow_version:Ljava/lang/Integer;

    if-eqz v1, :cond_20

    const-string v1, ", workflow_version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->workflow_version:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 939
    :cond_20
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_21

    const-string v1, ", total_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 940
    :cond_21
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_22

    const-string v1, ", total_tax_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 941
    :cond_22
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->total_discount_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_23

    const-string v1, ", total_discount_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->total_discount_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 942
    :cond_23
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->total_tip_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_24

    const-string v1, ", total_tip_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->total_tip_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 943
    :cond_24
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->total_service_charge_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_25

    const-string v1, ", total_service_charge_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->total_service_charge_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 944
    :cond_25
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->short_reference_id:Ljava/lang/String;

    if-eqz v1, :cond_26

    const-string v1, ", short_reference_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->short_reference_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 945
    :cond_26
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->pricing_options:Lcom/squareup/orders/model/Order$PricingOptions;

    if-eqz v1, :cond_27

    const-string v1, ", pricing_options="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->pricing_options:Lcom/squareup/orders/model/Order$PricingOptions;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 946
    :cond_27
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->reserved_discount_codes:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_28

    const-string v1, ", reserved_discount_codes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->reserved_discount_codes:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 947
    :cond_28
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->reserved_rewards:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_29

    const-string v1, ", reserved_rewards="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->reserved_rewards:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 948
    :cond_29
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->vaulted_data:Lcom/squareup/protos/common/privacyvault/VaultedData;

    if-eqz v1, :cond_2a

    const-string v1, ", vaulted_data="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->vaulted_data:Lcom/squareup/protos/common/privacyvault/VaultedData;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 949
    :cond_2a
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->was_status:Lcom/squareup/orders/model/Order$State;

    if-eqz v1, :cond_2b

    const-string v1, ", was_status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->was_status:Lcom/squareup/orders/model/Order$State;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 950
    :cond_2b
    iget-object v1, p0, Lcom/squareup/orders/model/Order;->ext_order_client_details:Lcom/squareup/protos/client/orders/OrderClientDetails;

    if-eqz v1, :cond_2c

    const-string v1, ", ext_order_client_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order;->ext_order_client_details:Lcom/squareup/protos/client/orders/OrderClientDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2c
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Order{"

    .line 951
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
