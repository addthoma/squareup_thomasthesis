.class final Lcom/squareup/orders/model/Order$ReturnTip$ProtoAdapter_ReturnTip;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order$ReturnTip;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ReturnTip"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/orders/model/Order$ReturnTip;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 15079
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/orders/model/Order$ReturnTip;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/orders/model/Order$ReturnTip;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 15100
    new-instance v0, Lcom/squareup/orders/model/Order$ReturnTip$Builder;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$ReturnTip$Builder;-><init>()V

    .line 15101
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 15102
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x4

    if-eq v3, v4, :cond_0

    .line 15108
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 15106
    :cond_0
    sget-object v3, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$ReturnTip$Builder;->applied_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/orders/model/Order$ReturnTip$Builder;

    goto :goto_0

    .line 15105
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$ReturnTip$Builder;->source_tip_uid(Ljava/lang/String;)Lcom/squareup/orders/model/Order$ReturnTip$Builder;

    goto :goto_0

    .line 15104
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$ReturnTip$Builder;->uid(Ljava/lang/String;)Lcom/squareup/orders/model/Order$ReturnTip$Builder;

    goto :goto_0

    .line 15112
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/orders/model/Order$ReturnTip$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 15113
    invoke-virtual {v0}, Lcom/squareup/orders/model/Order$ReturnTip$Builder;->build()Lcom/squareup/orders/model/Order$ReturnTip;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 15077
    invoke-virtual {p0, p1}, Lcom/squareup/orders/model/Order$ReturnTip$ProtoAdapter_ReturnTip;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/orders/model/Order$ReturnTip;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/orders/model/Order$ReturnTip;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 15092
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$ReturnTip;->uid:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 15093
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$ReturnTip;->source_tip_uid:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 15094
    sget-object v0, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$ReturnTip;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 15095
    invoke-virtual {p2}, Lcom/squareup/orders/model/Order$ReturnTip;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 15077
    check-cast p2, Lcom/squareup/orders/model/Order$ReturnTip;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/orders/model/Order$ReturnTip$ProtoAdapter_ReturnTip;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/orders/model/Order$ReturnTip;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/orders/model/Order$ReturnTip;)I
    .locals 4

    .line 15084
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/orders/model/Order$ReturnTip;->uid:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$ReturnTip;->source_tip_uid:Ljava/lang/String;

    const/4 v3, 0x2

    .line 15085
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$ReturnTip;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    const/4 v3, 0x4

    .line 15086
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 15087
    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$ReturnTip;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 15077
    check-cast p1, Lcom/squareup/orders/model/Order$ReturnTip;

    invoke-virtual {p0, p1}, Lcom/squareup/orders/model/Order$ReturnTip$ProtoAdapter_ReturnTip;->encodedSize(Lcom/squareup/orders/model/Order$ReturnTip;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/orders/model/Order$ReturnTip;)Lcom/squareup/orders/model/Order$ReturnTip;
    .locals 2

    .line 15118
    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$ReturnTip;->newBuilder()Lcom/squareup/orders/model/Order$ReturnTip$Builder;

    move-result-object p1

    .line 15119
    iget-object v0, p1, Lcom/squareup/orders/model/Order$ReturnTip$Builder;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/orders/model/Order$ReturnTip$Builder;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v0, p1, Lcom/squareup/orders/model/Order$ReturnTip$Builder;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 15120
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$ReturnTip$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 15121
    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$ReturnTip$Builder;->build()Lcom/squareup/orders/model/Order$ReturnTip;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 15077
    check-cast p1, Lcom/squareup/orders/model/Order$ReturnTip;

    invoke-virtual {p0, p1}, Lcom/squareup/orders/model/Order$ReturnTip$ProtoAdapter_ReturnTip;->redact(Lcom/squareup/orders/model/Order$ReturnTip;)Lcom/squareup/orders/model/Order$ReturnTip;

    move-result-object p1

    return-object p1
.end method
