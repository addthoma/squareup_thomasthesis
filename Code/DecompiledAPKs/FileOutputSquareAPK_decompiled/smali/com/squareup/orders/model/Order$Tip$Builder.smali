.class public final Lcom/squareup/orders/model/Order$Tip$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order$Tip;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/orders/model/Order$Tip;",
        "Lcom/squareup/orders/model/Order$Tip$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public applied_money:Lcom/squareup/protos/connect/v2/common/Money;

.field public uid:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 5752
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public applied_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/orders/model/Order$Tip$Builder;
    .locals 0

    .line 5766
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Tip$Builder;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public build()Lcom/squareup/orders/model/Order$Tip;
    .locals 4

    .line 5772
    new-instance v0, Lcom/squareup/orders/model/Order$Tip;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$Tip$Builder;->uid:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/orders/model/Order$Tip$Builder;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/orders/model/Order$Tip;-><init>(Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Money;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 5747
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$Tip$Builder;->build()Lcom/squareup/orders/model/Order$Tip;

    move-result-object v0

    return-object v0
.end method

.method public uid(Ljava/lang/String;)Lcom/squareup/orders/model/Order$Tip$Builder;
    .locals 0

    .line 5761
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Tip$Builder;->uid:Ljava/lang/String;

    return-object p0
.end method
