.class public final Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount;
.super Lcom/squareup/wire/Message;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order$LineItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AppliedDiscount"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount$ProtoAdapter_AppliedDiscount;,
        Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount;",
        "Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_DISCOUNT_UID:Ljava/lang/String; = ""

.field public static final DEFAULT_QUANTITY:Ljava/lang/String; = "1"

.field public static final DEFAULT_UID:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final applied_money:Lcom/squareup/protos/connect/v2/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Money#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final discount_uid:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final quantity:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final uid:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 5072
    new-instance v0, Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount$ProtoAdapter_AppliedDiscount;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount$ProtoAdapter_AppliedDiscount;-><init>()V

    sput-object v0, Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Money;Ljava/lang/String;)V
    .locals 6

    .line 5136
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Money;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Money;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 5141
    sget-object v0, Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 5142
    iput-object p1, p0, Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount;->uid:Ljava/lang/String;

    .line 5143
    iput-object p2, p0, Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount;->discount_uid:Ljava/lang/String;

    .line 5144
    iput-object p3, p0, Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 5145
    iput-object p4, p0, Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount;->quantity:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 5162
    :cond_0
    instance-of v1, p1, Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 5163
    :cond_1
    check-cast p1, Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount;

    .line 5164
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount;->uid:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount;->uid:Ljava/lang/String;

    .line 5165
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount;->discount_uid:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount;->discount_uid:Ljava/lang/String;

    .line 5166
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 5167
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount;->quantity:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount;->quantity:Ljava/lang/String;

    .line 5168
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 5173
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 5175
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 5176
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount;->uid:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 5177
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount;->discount_uid:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 5178
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Money;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 5179
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount;->quantity:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 5180
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount$Builder;
    .locals 2

    .line 5150
    new-instance v0, Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount$Builder;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount$Builder;-><init>()V

    .line 5151
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount;->uid:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount$Builder;->uid:Ljava/lang/String;

    .line 5152
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount;->discount_uid:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount$Builder;->discount_uid:Ljava/lang/String;

    .line 5153
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount$Builder;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 5154
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount;->quantity:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount$Builder;->quantity:Ljava/lang/String;

    .line 5155
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 5071
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount;->newBuilder()Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 5187
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 5188
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount;->uid:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount;->uid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 5189
    :cond_0
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount;->discount_uid:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", discount_uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount;->discount_uid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 5190
    :cond_1
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_2

    const-string v1, ", applied_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 5191
    :cond_2
    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount;->quantity:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", quantity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount;->quantity:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "AppliedDiscount{"

    .line 5192
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
