.class final Lcom/squareup/orders/model/Order$RoundingAdjustment$ProtoAdapter_RoundingAdjustment;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order$RoundingAdjustment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_RoundingAdjustment"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/orders/model/Order$RoundingAdjustment;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 10769
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/orders/model/Order$RoundingAdjustment;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/orders/model/Order$RoundingAdjustment;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 10790
    new-instance v0, Lcom/squareup/orders/model/Order$RoundingAdjustment$Builder;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$RoundingAdjustment$Builder;-><init>()V

    .line 10791
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 10792
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 10798
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 10796
    :cond_0
    sget-object v3, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$RoundingAdjustment$Builder;->amount_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/orders/model/Order$RoundingAdjustment$Builder;

    goto :goto_0

    .line 10795
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$RoundingAdjustment$Builder;->name(Ljava/lang/String;)Lcom/squareup/orders/model/Order$RoundingAdjustment$Builder;

    goto :goto_0

    .line 10794
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$RoundingAdjustment$Builder;->uid(Ljava/lang/String;)Lcom/squareup/orders/model/Order$RoundingAdjustment$Builder;

    goto :goto_0

    .line 10802
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/orders/model/Order$RoundingAdjustment$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 10803
    invoke-virtual {v0}, Lcom/squareup/orders/model/Order$RoundingAdjustment$Builder;->build()Lcom/squareup/orders/model/Order$RoundingAdjustment;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 10767
    invoke-virtual {p0, p1}, Lcom/squareup/orders/model/Order$RoundingAdjustment$ProtoAdapter_RoundingAdjustment;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/orders/model/Order$RoundingAdjustment;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/orders/model/Order$RoundingAdjustment;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 10782
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$RoundingAdjustment;->uid:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 10783
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$RoundingAdjustment;->name:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 10784
    sget-object v0, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$RoundingAdjustment;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 10785
    invoke-virtual {p2}, Lcom/squareup/orders/model/Order$RoundingAdjustment;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 10767
    check-cast p2, Lcom/squareup/orders/model/Order$RoundingAdjustment;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/orders/model/Order$RoundingAdjustment$ProtoAdapter_RoundingAdjustment;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/orders/model/Order$RoundingAdjustment;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/orders/model/Order$RoundingAdjustment;)I
    .locals 4

    .line 10774
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/orders/model/Order$RoundingAdjustment;->uid:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$RoundingAdjustment;->name:Ljava/lang/String;

    const/4 v3, 0x2

    .line 10775
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$RoundingAdjustment;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    const/4 v3, 0x3

    .line 10776
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10777
    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$RoundingAdjustment;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 10767
    check-cast p1, Lcom/squareup/orders/model/Order$RoundingAdjustment;

    invoke-virtual {p0, p1}, Lcom/squareup/orders/model/Order$RoundingAdjustment$ProtoAdapter_RoundingAdjustment;->encodedSize(Lcom/squareup/orders/model/Order$RoundingAdjustment;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/orders/model/Order$RoundingAdjustment;)Lcom/squareup/orders/model/Order$RoundingAdjustment;
    .locals 2

    .line 10808
    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$RoundingAdjustment;->newBuilder()Lcom/squareup/orders/model/Order$RoundingAdjustment$Builder;

    move-result-object p1

    .line 10809
    iget-object v0, p1, Lcom/squareup/orders/model/Order$RoundingAdjustment$Builder;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/orders/model/Order$RoundingAdjustment$Builder;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v0, p1, Lcom/squareup/orders/model/Order$RoundingAdjustment$Builder;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 10810
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$RoundingAdjustment$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 10811
    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$RoundingAdjustment$Builder;->build()Lcom/squareup/orders/model/Order$RoundingAdjustment;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 10767
    check-cast p1, Lcom/squareup/orders/model/Order$RoundingAdjustment;

    invoke-virtual {p0, p1}, Lcom/squareup/orders/model/Order$RoundingAdjustment$ProtoAdapter_RoundingAdjustment;->redact(Lcom/squareup/orders/model/Order$RoundingAdjustment;)Lcom/squareup/orders/model/Order$RoundingAdjustment;

    move-result-object p1

    return-object p1
.end method
