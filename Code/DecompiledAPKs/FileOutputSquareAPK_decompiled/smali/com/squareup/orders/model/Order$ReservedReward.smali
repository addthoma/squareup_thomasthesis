.class public final Lcom/squareup/orders/model/Order$ReservedReward;
.super Lcom/squareup/wire/Message;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ReservedReward"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orders/model/Order$ReservedReward$ProtoAdapter_ReservedReward;,
        Lcom/squareup/orders/model/Order$ReservedReward$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/orders/model/Order$ReservedReward;",
        "Lcom/squareup/orders/model/Order$ReservedReward$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/orders/model/Order$ReservedReward;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_PRICING_RULE_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_REWARD_TIER_ID:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final pricing_rule_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final reward_tier_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 15450
    new-instance v0, Lcom/squareup/orders/model/Order$ReservedReward$ProtoAdapter_ReservedReward;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$ReservedReward$ProtoAdapter_ReservedReward;-><init>()V

    sput-object v0, Lcom/squareup/orders/model/Order$ReservedReward;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 15488
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/orders/model/Order$ReservedReward;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 15493
    sget-object v0, Lcom/squareup/orders/model/Order$ReservedReward;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 15494
    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReservedReward;->id:Ljava/lang/String;

    .line 15495
    iput-object p2, p0, Lcom/squareup/orders/model/Order$ReservedReward;->reward_tier_id:Ljava/lang/String;

    .line 15496
    iput-object p3, p0, Lcom/squareup/orders/model/Order$ReservedReward;->pricing_rule_id:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 15512
    :cond_0
    instance-of v1, p1, Lcom/squareup/orders/model/Order$ReservedReward;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 15513
    :cond_1
    check-cast p1, Lcom/squareup/orders/model/Order$ReservedReward;

    .line 15514
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$ReservedReward;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$ReservedReward;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReservedReward;->id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ReservedReward;->id:Ljava/lang/String;

    .line 15515
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReservedReward;->reward_tier_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ReservedReward;->reward_tier_id:Ljava/lang/String;

    .line 15516
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReservedReward;->pricing_rule_id:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/orders/model/Order$ReservedReward;->pricing_rule_id:Ljava/lang/String;

    .line 15517
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 15522
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 15524
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$ReservedReward;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 15525
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReservedReward;->id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 15526
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReservedReward;->reward_tier_id:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 15527
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReservedReward;->pricing_rule_id:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 15528
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/orders/model/Order$ReservedReward$Builder;
    .locals 2

    .line 15501
    new-instance v0, Lcom/squareup/orders/model/Order$ReservedReward$Builder;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$ReservedReward$Builder;-><init>()V

    .line 15502
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReservedReward;->id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReservedReward$Builder;->id:Ljava/lang/String;

    .line 15503
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReservedReward;->reward_tier_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReservedReward$Builder;->reward_tier_id:Ljava/lang/String;

    .line 15504
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReservedReward;->pricing_rule_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReservedReward$Builder;->pricing_rule_id:Ljava/lang/String;

    .line 15505
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$ReservedReward;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/orders/model/Order$ReservedReward$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 15449
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$ReservedReward;->newBuilder()Lcom/squareup/orders/model/Order$ReservedReward$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 15535
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 15536
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReservedReward;->id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReservedReward;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 15537
    :cond_0
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReservedReward;->reward_tier_id:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", reward_tier_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReservedReward;->reward_tier_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 15538
    :cond_1
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReservedReward;->pricing_rule_id:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", pricing_rule_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReservedReward;->pricing_rule_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ReservedReward{"

    .line 15539
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
