.class public final enum Lcom/squareup/orders/model/Order$FulfillmentType;
.super Ljava/lang/Enum;
.source "Order.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "FulfillmentType"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orders/model/Order$FulfillmentType$ProtoAdapter_FulfillmentType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/orders/model/Order$FulfillmentType;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/orders/model/Order$FulfillmentType;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/orders/model/Order$FulfillmentType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum CUSTOM:Lcom/squareup/orders/model/Order$FulfillmentType;

.field public static final enum DELIVERY:Lcom/squareup/orders/model/Order$FulfillmentType;

.field public static final enum DIGITAL:Lcom/squareup/orders/model/Order$FulfillmentType;

.field public static final enum FULFILLMENT_TYPE_DO_NOT_USE:Lcom/squareup/orders/model/Order$FulfillmentType;

.field public static final enum MANAGED_DELIVERY:Lcom/squareup/orders/model/Order$FulfillmentType;

.field public static final enum PICKUP:Lcom/squareup/orders/model/Order$FulfillmentType;

.field public static final enum SHIPMENT:Lcom/squareup/orders/model/Order$FulfillmentType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .line 7656
    new-instance v0, Lcom/squareup/orders/model/Order$FulfillmentType;

    const/4 v1, 0x0

    const-string v2, "FULFILLMENT_TYPE_DO_NOT_USE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/orders/model/Order$FulfillmentType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/orders/model/Order$FulfillmentType;->FULFILLMENT_TYPE_DO_NOT_USE:Lcom/squareup/orders/model/Order$FulfillmentType;

    .line 7658
    new-instance v0, Lcom/squareup/orders/model/Order$FulfillmentType;

    const/4 v2, 0x1

    const-string v3, "CUSTOM"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/orders/model/Order$FulfillmentType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/orders/model/Order$FulfillmentType;->CUSTOM:Lcom/squareup/orders/model/Order$FulfillmentType;

    .line 7666
    new-instance v0, Lcom/squareup/orders/model/Order$FulfillmentType;

    const/4 v3, 0x2

    const-string v4, "PICKUP"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/orders/model/Order$FulfillmentType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/orders/model/Order$FulfillmentType;->PICKUP:Lcom/squareup/orders/model/Order$FulfillmentType;

    .line 7668
    new-instance v0, Lcom/squareup/orders/model/Order$FulfillmentType;

    const/4 v4, 0x3

    const-string v5, "MANAGED_DELIVERY"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/orders/model/Order$FulfillmentType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/orders/model/Order$FulfillmentType;->MANAGED_DELIVERY:Lcom/squareup/orders/model/Order$FulfillmentType;

    .line 7675
    new-instance v0, Lcom/squareup/orders/model/Order$FulfillmentType;

    const/4 v5, 0x4

    const-string v6, "SHIPMENT"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/orders/model/Order$FulfillmentType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/orders/model/Order$FulfillmentType;->SHIPMENT:Lcom/squareup/orders/model/Order$FulfillmentType;

    .line 7677
    new-instance v0, Lcom/squareup/orders/model/Order$FulfillmentType;

    const/4 v6, 0x5

    const-string v7, "DIGITAL"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/orders/model/Order$FulfillmentType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/orders/model/Order$FulfillmentType;->DIGITAL:Lcom/squareup/orders/model/Order$FulfillmentType;

    .line 7679
    new-instance v0, Lcom/squareup/orders/model/Order$FulfillmentType;

    const/4 v7, 0x6

    const-string v8, "DELIVERY"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/orders/model/Order$FulfillmentType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/orders/model/Order$FulfillmentType;->DELIVERY:Lcom/squareup/orders/model/Order$FulfillmentType;

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/squareup/orders/model/Order$FulfillmentType;

    .line 7655
    sget-object v8, Lcom/squareup/orders/model/Order$FulfillmentType;->FULFILLMENT_TYPE_DO_NOT_USE:Lcom/squareup/orders/model/Order$FulfillmentType;

    aput-object v8, v0, v1

    sget-object v1, Lcom/squareup/orders/model/Order$FulfillmentType;->CUSTOM:Lcom/squareup/orders/model/Order$FulfillmentType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/orders/model/Order$FulfillmentType;->PICKUP:Lcom/squareup/orders/model/Order$FulfillmentType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/orders/model/Order$FulfillmentType;->MANAGED_DELIVERY:Lcom/squareup/orders/model/Order$FulfillmentType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/orders/model/Order$FulfillmentType;->SHIPMENT:Lcom/squareup/orders/model/Order$FulfillmentType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/orders/model/Order$FulfillmentType;->DIGITAL:Lcom/squareup/orders/model/Order$FulfillmentType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/orders/model/Order$FulfillmentType;->DELIVERY:Lcom/squareup/orders/model/Order$FulfillmentType;

    aput-object v1, v0, v7

    sput-object v0, Lcom/squareup/orders/model/Order$FulfillmentType;->$VALUES:[Lcom/squareup/orders/model/Order$FulfillmentType;

    .line 7681
    new-instance v0, Lcom/squareup/orders/model/Order$FulfillmentType$ProtoAdapter_FulfillmentType;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$FulfillmentType$ProtoAdapter_FulfillmentType;-><init>()V

    sput-object v0, Lcom/squareup/orders/model/Order$FulfillmentType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 7685
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 7686
    iput p3, p0, Lcom/squareup/orders/model/Order$FulfillmentType;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/orders/model/Order$FulfillmentType;
    .locals 0

    packed-switch p0, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    .line 7700
    :pswitch_0
    sget-object p0, Lcom/squareup/orders/model/Order$FulfillmentType;->DELIVERY:Lcom/squareup/orders/model/Order$FulfillmentType;

    return-object p0

    .line 7699
    :pswitch_1
    sget-object p0, Lcom/squareup/orders/model/Order$FulfillmentType;->DIGITAL:Lcom/squareup/orders/model/Order$FulfillmentType;

    return-object p0

    .line 7698
    :pswitch_2
    sget-object p0, Lcom/squareup/orders/model/Order$FulfillmentType;->SHIPMENT:Lcom/squareup/orders/model/Order$FulfillmentType;

    return-object p0

    .line 7697
    :pswitch_3
    sget-object p0, Lcom/squareup/orders/model/Order$FulfillmentType;->MANAGED_DELIVERY:Lcom/squareup/orders/model/Order$FulfillmentType;

    return-object p0

    .line 7696
    :pswitch_4
    sget-object p0, Lcom/squareup/orders/model/Order$FulfillmentType;->PICKUP:Lcom/squareup/orders/model/Order$FulfillmentType;

    return-object p0

    .line 7695
    :pswitch_5
    sget-object p0, Lcom/squareup/orders/model/Order$FulfillmentType;->CUSTOM:Lcom/squareup/orders/model/Order$FulfillmentType;

    return-object p0

    .line 7694
    :pswitch_6
    sget-object p0, Lcom/squareup/orders/model/Order$FulfillmentType;->FULFILLMENT_TYPE_DO_NOT_USE:Lcom/squareup/orders/model/Order$FulfillmentType;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentType;
    .locals 1

    .line 7655
    const-class v0, Lcom/squareup/orders/model/Order$FulfillmentType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/orders/model/Order$FulfillmentType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/orders/model/Order$FulfillmentType;
    .locals 1

    .line 7655
    sget-object v0, Lcom/squareup/orders/model/Order$FulfillmentType;->$VALUES:[Lcom/squareup/orders/model/Order$FulfillmentType;

    invoke-virtual {v0}, [Lcom/squareup/orders/model/Order$FulfillmentType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/orders/model/Order$FulfillmentType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 7707
    iget v0, p0, Lcom/squareup/orders/model/Order$FulfillmentType;->value:I

    return v0
.end method
