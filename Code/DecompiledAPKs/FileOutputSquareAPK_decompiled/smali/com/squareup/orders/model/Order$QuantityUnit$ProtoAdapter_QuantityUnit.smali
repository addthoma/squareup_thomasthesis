.class final Lcom/squareup/orders/model/Order$QuantityUnit$ProtoAdapter_QuantityUnit;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order$QuantityUnit;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_QuantityUnit"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/orders/model/Order$QuantityUnit;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 2111
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/orders/model/Order$QuantityUnit;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/orders/model/Order$QuantityUnit;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2134
    new-instance v0, Lcom/squareup/orders/model/Order$QuantityUnit$Builder;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$QuantityUnit$Builder;-><init>()V

    .line 2135
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 2136
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x2

    if-eq v3, v4, :cond_2

    const/4 v4, 0x3

    if-eq v3, v4, :cond_1

    const/4 v4, 0x4

    if-eq v3, v4, :cond_0

    .line 2143
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 2141
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$QuantityUnit$Builder;->catalog_object_version(Ljava/lang/Long;)Lcom/squareup/orders/model/Order$QuantityUnit$Builder;

    goto :goto_0

    .line 2140
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$QuantityUnit$Builder;->catalog_object_id(Ljava/lang/String;)Lcom/squareup/orders/model/Order$QuantityUnit$Builder;

    goto :goto_0

    .line 2139
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$QuantityUnit$Builder;->precision(Ljava/lang/Integer;)Lcom/squareup/orders/model/Order$QuantityUnit$Builder;

    goto :goto_0

    .line 2138
    :cond_3
    sget-object v3, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$QuantityUnit$Builder;->measurement_unit(Lcom/squareup/protos/connect/v2/common/MeasurementUnit;)Lcom/squareup/orders/model/Order$QuantityUnit$Builder;

    goto :goto_0

    .line 2147
    :cond_4
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/orders/model/Order$QuantityUnit$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 2148
    invoke-virtual {v0}, Lcom/squareup/orders/model/Order$QuantityUnit$Builder;->build()Lcom/squareup/orders/model/Order$QuantityUnit;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2109
    invoke-virtual {p0, p1}, Lcom/squareup/orders/model/Order$QuantityUnit$ProtoAdapter_QuantityUnit;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/orders/model/Order$QuantityUnit;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/orders/model/Order$QuantityUnit;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2125
    sget-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$QuantityUnit;->measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2126
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$QuantityUnit;->precision:Ljava/lang/Integer;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2127
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$QuantityUnit;->catalog_object_id:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2128
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$QuantityUnit;->catalog_object_version:Ljava/lang/Long;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 2129
    invoke-virtual {p2}, Lcom/squareup/orders/model/Order$QuantityUnit;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 2109
    check-cast p2, Lcom/squareup/orders/model/Order$QuantityUnit;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/orders/model/Order$QuantityUnit$ProtoAdapter_QuantityUnit;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/orders/model/Order$QuantityUnit;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/orders/model/Order$QuantityUnit;)I
    .locals 4

    .line 2116
    sget-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/orders/model/Order$QuantityUnit;->measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$QuantityUnit;->precision:Ljava/lang/Integer;

    const/4 v3, 0x2

    .line 2117
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$QuantityUnit;->catalog_object_id:Ljava/lang/String;

    const/4 v3, 0x3

    .line 2118
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$QuantityUnit;->catalog_object_version:Ljava/lang/Long;

    const/4 v3, 0x4

    .line 2119
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2120
    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$QuantityUnit;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 2109
    check-cast p1, Lcom/squareup/orders/model/Order$QuantityUnit;

    invoke-virtual {p0, p1}, Lcom/squareup/orders/model/Order$QuantityUnit$ProtoAdapter_QuantityUnit;->encodedSize(Lcom/squareup/orders/model/Order$QuantityUnit;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/orders/model/Order$QuantityUnit;)Lcom/squareup/orders/model/Order$QuantityUnit;
    .locals 2

    .line 2153
    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$QuantityUnit;->newBuilder()Lcom/squareup/orders/model/Order$QuantityUnit$Builder;

    move-result-object p1

    .line 2154
    iget-object v0, p1, Lcom/squareup/orders/model/Order$QuantityUnit$Builder;->measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/orders/model/Order$QuantityUnit$Builder;->measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    iput-object v0, p1, Lcom/squareup/orders/model/Order$QuantityUnit$Builder;->measurement_unit:Lcom/squareup/protos/connect/v2/common/MeasurementUnit;

    .line 2155
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$QuantityUnit$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 2156
    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$QuantityUnit$Builder;->build()Lcom/squareup/orders/model/Order$QuantityUnit;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 2109
    check-cast p1, Lcom/squareup/orders/model/Order$QuantityUnit;

    invoke-virtual {p0, p1}, Lcom/squareup/orders/model/Order$QuantityUnit$ProtoAdapter_QuantityUnit;->redact(Lcom/squareup/orders/model/Order$QuantityUnit;)Lcom/squareup/orders/model/Order$QuantityUnit;

    move-result-object p1

    return-object p1
.end method
