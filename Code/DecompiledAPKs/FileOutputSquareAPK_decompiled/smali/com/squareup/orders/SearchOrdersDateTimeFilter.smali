.class public final Lcom/squareup/orders/SearchOrdersDateTimeFilter;
.super Lcom/squareup/wire/Message;
.source "SearchOrdersDateTimeFilter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orders/SearchOrdersDateTimeFilter$ProtoAdapter_SearchOrdersDateTimeFilter;,
        Lcom/squareup/orders/SearchOrdersDateTimeFilter$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/orders/SearchOrdersDateTimeFilter;",
        "Lcom/squareup/orders/SearchOrdersDateTimeFilter$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/orders/SearchOrdersDateTimeFilter;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final closed_at:Lcom/squareup/protos/connect/v2/common/TimeRange;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.TimeRange#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final created_at:Lcom/squareup/protos/connect/v2/common/TimeRange;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.TimeRange#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final updated_at:Lcom/squareup/protos/connect/v2/common/TimeRange;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.TimeRange#ADAPTER"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 39
    new-instance v0, Lcom/squareup/orders/SearchOrdersDateTimeFilter$ProtoAdapter_SearchOrdersDateTimeFilter;

    invoke-direct {v0}, Lcom/squareup/orders/SearchOrdersDateTimeFilter$ProtoAdapter_SearchOrdersDateTimeFilter;-><init>()V

    sput-object v0, Lcom/squareup/orders/SearchOrdersDateTimeFilter;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/connect/v2/common/TimeRange;Lcom/squareup/protos/connect/v2/common/TimeRange;Lcom/squareup/protos/connect/v2/common/TimeRange;)V
    .locals 1

    .line 86
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/orders/SearchOrdersDateTimeFilter;-><init>(Lcom/squareup/protos/connect/v2/common/TimeRange;Lcom/squareup/protos/connect/v2/common/TimeRange;Lcom/squareup/protos/connect/v2/common/TimeRange;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/protos/connect/v2/common/TimeRange;Lcom/squareup/protos/connect/v2/common/TimeRange;Lcom/squareup/protos/connect/v2/common/TimeRange;Lokio/ByteString;)V
    .locals 1

    .line 91
    sget-object v0, Lcom/squareup/orders/SearchOrdersDateTimeFilter;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 92
    iput-object p1, p0, Lcom/squareup/orders/SearchOrdersDateTimeFilter;->created_at:Lcom/squareup/protos/connect/v2/common/TimeRange;

    .line 93
    iput-object p2, p0, Lcom/squareup/orders/SearchOrdersDateTimeFilter;->updated_at:Lcom/squareup/protos/connect/v2/common/TimeRange;

    .line 94
    iput-object p3, p0, Lcom/squareup/orders/SearchOrdersDateTimeFilter;->closed_at:Lcom/squareup/protos/connect/v2/common/TimeRange;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 110
    :cond_0
    instance-of v1, p1, Lcom/squareup/orders/SearchOrdersDateTimeFilter;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 111
    :cond_1
    check-cast p1, Lcom/squareup/orders/SearchOrdersDateTimeFilter;

    .line 112
    invoke-virtual {p0}, Lcom/squareup/orders/SearchOrdersDateTimeFilter;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/orders/SearchOrdersDateTimeFilter;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/SearchOrdersDateTimeFilter;->created_at:Lcom/squareup/protos/connect/v2/common/TimeRange;

    iget-object v3, p1, Lcom/squareup/orders/SearchOrdersDateTimeFilter;->created_at:Lcom/squareup/protos/connect/v2/common/TimeRange;

    .line 113
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/SearchOrdersDateTimeFilter;->updated_at:Lcom/squareup/protos/connect/v2/common/TimeRange;

    iget-object v3, p1, Lcom/squareup/orders/SearchOrdersDateTimeFilter;->updated_at:Lcom/squareup/protos/connect/v2/common/TimeRange;

    .line 114
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/SearchOrdersDateTimeFilter;->closed_at:Lcom/squareup/protos/connect/v2/common/TimeRange;

    iget-object p1, p1, Lcom/squareup/orders/SearchOrdersDateTimeFilter;->closed_at:Lcom/squareup/protos/connect/v2/common/TimeRange;

    .line 115
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 120
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 122
    invoke-virtual {p0}, Lcom/squareup/orders/SearchOrdersDateTimeFilter;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 123
    iget-object v1, p0, Lcom/squareup/orders/SearchOrdersDateTimeFilter;->created_at:Lcom/squareup/protos/connect/v2/common/TimeRange;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/TimeRange;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 124
    iget-object v1, p0, Lcom/squareup/orders/SearchOrdersDateTimeFilter;->updated_at:Lcom/squareup/protos/connect/v2/common/TimeRange;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/TimeRange;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 125
    iget-object v1, p0, Lcom/squareup/orders/SearchOrdersDateTimeFilter;->closed_at:Lcom/squareup/protos/connect/v2/common/TimeRange;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/TimeRange;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 126
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/orders/SearchOrdersDateTimeFilter$Builder;
    .locals 2

    .line 99
    new-instance v0, Lcom/squareup/orders/SearchOrdersDateTimeFilter$Builder;

    invoke-direct {v0}, Lcom/squareup/orders/SearchOrdersDateTimeFilter$Builder;-><init>()V

    .line 100
    iget-object v1, p0, Lcom/squareup/orders/SearchOrdersDateTimeFilter;->created_at:Lcom/squareup/protos/connect/v2/common/TimeRange;

    iput-object v1, v0, Lcom/squareup/orders/SearchOrdersDateTimeFilter$Builder;->created_at:Lcom/squareup/protos/connect/v2/common/TimeRange;

    .line 101
    iget-object v1, p0, Lcom/squareup/orders/SearchOrdersDateTimeFilter;->updated_at:Lcom/squareup/protos/connect/v2/common/TimeRange;

    iput-object v1, v0, Lcom/squareup/orders/SearchOrdersDateTimeFilter$Builder;->updated_at:Lcom/squareup/protos/connect/v2/common/TimeRange;

    .line 102
    iget-object v1, p0, Lcom/squareup/orders/SearchOrdersDateTimeFilter;->closed_at:Lcom/squareup/protos/connect/v2/common/TimeRange;

    iput-object v1, v0, Lcom/squareup/orders/SearchOrdersDateTimeFilter$Builder;->closed_at:Lcom/squareup/protos/connect/v2/common/TimeRange;

    .line 103
    invoke-virtual {p0}, Lcom/squareup/orders/SearchOrdersDateTimeFilter;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/orders/SearchOrdersDateTimeFilter$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 38
    invoke-virtual {p0}, Lcom/squareup/orders/SearchOrdersDateTimeFilter;->newBuilder()Lcom/squareup/orders/SearchOrdersDateTimeFilter$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 133
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 134
    iget-object v1, p0, Lcom/squareup/orders/SearchOrdersDateTimeFilter;->created_at:Lcom/squareup/protos/connect/v2/common/TimeRange;

    if-eqz v1, :cond_0

    const-string v1, ", created_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/SearchOrdersDateTimeFilter;->created_at:Lcom/squareup/protos/connect/v2/common/TimeRange;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 135
    :cond_0
    iget-object v1, p0, Lcom/squareup/orders/SearchOrdersDateTimeFilter;->updated_at:Lcom/squareup/protos/connect/v2/common/TimeRange;

    if-eqz v1, :cond_1

    const-string v1, ", updated_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/SearchOrdersDateTimeFilter;->updated_at:Lcom/squareup/protos/connect/v2/common/TimeRange;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 136
    :cond_1
    iget-object v1, p0, Lcom/squareup/orders/SearchOrdersDateTimeFilter;->closed_at:Lcom/squareup/protos/connect/v2/common/TimeRange;

    if-eqz v1, :cond_2

    const-string v1, ", closed_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/SearchOrdersDateTimeFilter;->closed_at:Lcom/squareup/protos/connect/v2/common/TimeRange;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "SearchOrdersDateTimeFilter{"

    .line 137
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
