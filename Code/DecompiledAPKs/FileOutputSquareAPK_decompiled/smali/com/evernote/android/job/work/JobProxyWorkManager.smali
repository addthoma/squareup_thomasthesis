.class public Lcom/evernote/android/job/work/JobProxyWorkManager;
.super Ljava/lang/Object;
.source "JobProxyWorkManager.java"

# interfaces
.implements Lcom/evernote/android/job/JobProxy;


# static fields
.field private static final CAT:Lcom/evernote/android/job/util/JobCat;

.field private static final PREFIX:Ljava/lang/String; = "android-job-"


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 34
    new-instance v0, Lcom/evernote/android/job/util/JobCat;

    const-string v1, "JobProxyWork"

    invoke-direct {v0, v1}, Lcom/evernote/android/job/util/JobCat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/evernote/android/job/work/JobProxyWorkManager;->CAT:Lcom/evernote/android/job/util/JobCat;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/evernote/android/job/work/JobProxyWorkManager;->mContext:Landroid/content/Context;

    return-void
.end method

.method private static buildConstraints(Lcom/evernote/android/job/JobRequest;)Landroidx/work/Constraints;
    .locals 3

    .line 123
    new-instance v0, Landroidx/work/Constraints$Builder;

    invoke-direct {v0}, Landroidx/work/Constraints$Builder;-><init>()V

    .line 124
    invoke-virtual {p0}, Lcom/evernote/android/job/JobRequest;->requiresBatteryNotLow()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroidx/work/Constraints$Builder;->setRequiresBatteryNotLow(Z)Landroidx/work/Constraints$Builder;

    move-result-object v0

    .line 125
    invoke-virtual {p0}, Lcom/evernote/android/job/JobRequest;->requiresCharging()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroidx/work/Constraints$Builder;->setRequiresCharging(Z)Landroidx/work/Constraints$Builder;

    move-result-object v0

    .line 126
    invoke-virtual {p0}, Lcom/evernote/android/job/JobRequest;->requiresStorageNotLow()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroidx/work/Constraints$Builder;->setRequiresStorageNotLow(Z)Landroidx/work/Constraints$Builder;

    move-result-object v0

    .line 127
    invoke-virtual {p0}, Lcom/evernote/android/job/JobRequest;->requiredNetworkType()Lcom/evernote/android/job/JobRequest$NetworkType;

    move-result-object v1

    invoke-static {v1}, Lcom/evernote/android/job/work/JobProxyWorkManager;->mapNetworkType(Lcom/evernote/android/job/JobRequest$NetworkType;)Landroidx/work/NetworkType;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroidx/work/Constraints$Builder;->setRequiredNetworkType(Landroidx/work/NetworkType;)Landroidx/work/Constraints$Builder;

    move-result-object v0

    .line 129
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x17

    if-lt v1, v2, :cond_0

    .line 130
    invoke-virtual {p0}, Lcom/evernote/android/job/JobRequest;->requiresDeviceIdle()Z

    move-result p0

    invoke-virtual {v0, p0}, Landroidx/work/Constraints$Builder;->setRequiresDeviceIdle(Z)Landroidx/work/Constraints$Builder;

    .line 133
    :cond_0
    invoke-virtual {v0}, Landroidx/work/Constraints$Builder;->build()Landroidx/work/Constraints;

    move-result-object p0

    return-object p0
.end method

.method static createTag(I)Ljava/lang/String;
    .locals 2

    .line 110
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "android-job-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method static getJobIdFromTags(Ljava/util/Collection;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Ljava/lang/String;",
            ">;)I"
        }
    .end annotation

    .line 114
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "android-job-"

    .line 115
    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 p0, 0xc

    .line 116
    invoke-virtual {v0, p0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p0

    return p0

    :cond_1
    const/4 p0, -0x1

    return p0
.end method

.method private getWorkManager()Landroidx/work/WorkManager;
    .locals 4

    .line 158
    :try_start_0
    iget-object v0, p0, Lcom/evernote/android/job/work/JobProxyWorkManager;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroidx/work/WorkManager;->getInstance(Landroid/content/Context;)Landroidx/work/WorkManager;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_0

    .line 164
    :try_start_1
    iget-object v1, p0, Lcom/evernote/android/job/work/JobProxyWorkManager;->mContext:Landroid/content/Context;

    new-instance v2, Landroidx/work/Configuration$Builder;

    invoke-direct {v2}, Landroidx/work/Configuration$Builder;-><init>()V

    invoke-virtual {v2}, Landroidx/work/Configuration$Builder;->build()Landroidx/work/Configuration;

    move-result-object v2

    invoke-static {v1, v2}, Landroidx/work/WorkManager;->initialize(Landroid/content/Context;Landroidx/work/Configuration;)V

    .line 165
    iget-object v1, p0, Lcom/evernote/android/job/work/JobProxyWorkManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroidx/work/WorkManager;->getInstance(Landroid/content/Context;)Landroidx/work/WorkManager;

    move-result-object v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 168
    :catchall_1
    sget-object v1, Lcom/evernote/android/job/work/JobProxyWorkManager;->CAT:Lcom/evernote/android/job/util/JobCat;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    const-string v3, "WorkManager getInstance() returned null, now: %s"

    invoke-virtual {v1, v3, v2}, Lcom/evernote/android/job/util/JobCat;->w(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    return-object v0
.end method

.method private getWorkStatusBlocking(Ljava/lang/String;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Landroidx/work/WorkInfo;",
            ">;"
        }
    .end annotation

    .line 175
    invoke-direct {p0}, Lcom/evernote/android/job/work/JobProxyWorkManager;->getWorkManager()Landroidx/work/WorkManager;

    move-result-object v0

    if-nez v0, :cond_0

    .line 177
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    return-object p1

    .line 181
    :cond_0
    :try_start_0
    invoke-virtual {v0, p1}, Landroidx/work/WorkManager;->getWorkInfosByTag(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object p1

    const-wide/16 v0, 0x5

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {p1, v0, v1, v2}, Lcom/google/common/util/concurrent/ListenableFuture;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    .line 183
    :catch_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method private static mapNetworkType(Lcom/evernote/android/job/JobRequest$NetworkType;)Landroidx/work/NetworkType;
    .locals 1

    .line 138
    sget-object v0, Lcom/evernote/android/job/work/JobProxyWorkManager$1;->$SwitchMap$com$evernote$android$job$JobRequest$NetworkType:[I

    invoke-virtual {p0}, Lcom/evernote/android/job/JobRequest$NetworkType;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x1

    if-eq p0, v0, :cond_4

    const/4 v0, 0x2

    if-eq p0, v0, :cond_3

    const/4 v0, 0x3

    if-eq p0, v0, :cond_2

    const/4 v0, 0x4

    if-eq p0, v0, :cond_1

    const/4 v0, 0x5

    if-ne p0, v0, :cond_0

    .line 148
    sget-object p0, Landroidx/work/NetworkType;->NOT_ROAMING:Landroidx/work/NetworkType;

    return-object p0

    .line 150
    :cond_0
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string v0, "Not implemented"

    invoke-direct {p0, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 146
    :cond_1
    sget-object p0, Landroidx/work/NetworkType;->UNMETERED:Landroidx/work/NetworkType;

    return-object p0

    .line 144
    :cond_2
    sget-object p0, Landroidx/work/NetworkType;->CONNECTED:Landroidx/work/NetworkType;

    return-object p0

    .line 142
    :cond_3
    sget-object p0, Landroidx/work/NetworkType;->METERED:Landroidx/work/NetworkType;

    return-object p0

    .line 140
    :cond_4
    sget-object p0, Landroidx/work/NetworkType;->NOT_REQUIRED:Landroidx/work/NetworkType;

    return-object p0
.end method


# virtual methods
.method public cancel(I)V
    .locals 2

    .line 89
    invoke-direct {p0}, Lcom/evernote/android/job/work/JobProxyWorkManager;->getWorkManager()Landroidx/work/WorkManager;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 94
    :cond_0
    invoke-static {p1}, Lcom/evernote/android/job/work/JobProxyWorkManager;->createTag(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroidx/work/WorkManager;->cancelAllWorkByTag(Ljava/lang/String;)Landroidx/work/Operation;

    .line 95
    invoke-static {p1}, Lcom/evernote/android/job/work/TransientBundleHolder;->cleanUpBundle(I)V

    return-void
.end method

.method public isPlatformJobScheduled(Lcom/evernote/android/job/JobRequest;)Z
    .locals 2

    .line 100
    invoke-virtual {p1}, Lcom/evernote/android/job/JobRequest;->getJobId()I

    move-result p1

    invoke-static {p1}, Lcom/evernote/android/job/work/JobProxyWorkManager;->createTag(I)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/evernote/android/job/work/JobProxyWorkManager;->getWorkStatusBlocking(Ljava/lang/String;)Ljava/util/List;

    move-result-object p1

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    .line 101
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    .line 105
    :cond_0
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroidx/work/WorkInfo;

    invoke-virtual {p1}, Landroidx/work/WorkInfo;->getState()Landroidx/work/WorkInfo$State;

    move-result-object p1

    .line 106
    sget-object v1, Landroidx/work/WorkInfo$State;->ENQUEUED:Landroidx/work/WorkInfo$State;

    if-ne p1, v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    :goto_0
    return v0
.end method

.method public plantOneOff(Lcom/evernote/android/job/JobRequest;)V
    .locals 4

    .line 45
    invoke-virtual {p1}, Lcom/evernote/android/job/JobRequest;->isTransient()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 46
    invoke-virtual {p1}, Lcom/evernote/android/job/JobRequest;->getJobId()I

    move-result v0

    invoke-virtual {p1}, Lcom/evernote/android/job/JobRequest;->getTransientExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/evernote/android/job/work/TransientBundleHolder;->putBundle(ILandroid/os/Bundle;)V

    .line 49
    :cond_0
    new-instance v0, Landroidx/work/OneTimeWorkRequest$Builder;

    const-class v1, Lcom/evernote/android/job/work/PlatformWorker;

    invoke-direct {v0, v1}, Landroidx/work/OneTimeWorkRequest$Builder;-><init>(Ljava/lang/Class;)V

    .line 50
    invoke-virtual {p1}, Lcom/evernote/android/job/JobRequest;->getStartMs()J

    move-result-wide v1

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1, v2, v3}, Landroidx/work/OneTimeWorkRequest$Builder;->setInitialDelay(JLjava/util/concurrent/TimeUnit;)Landroidx/work/WorkRequest$Builder;

    move-result-object v0

    check-cast v0, Landroidx/work/OneTimeWorkRequest$Builder;

    .line 51
    invoke-static {p1}, Lcom/evernote/android/job/work/JobProxyWorkManager;->buildConstraints(Lcom/evernote/android/job/JobRequest;)Landroidx/work/Constraints;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroidx/work/OneTimeWorkRequest$Builder;->setConstraints(Landroidx/work/Constraints;)Landroidx/work/WorkRequest$Builder;

    move-result-object v0

    check-cast v0, Landroidx/work/OneTimeWorkRequest$Builder;

    .line 52
    invoke-virtual {p1}, Lcom/evernote/android/job/JobRequest;->getJobId()I

    move-result p1

    invoke-static {p1}, Lcom/evernote/android/job/work/JobProxyWorkManager;->createTag(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroidx/work/OneTimeWorkRequest$Builder;->addTag(Ljava/lang/String;)Landroidx/work/WorkRequest$Builder;

    move-result-object p1

    check-cast p1, Landroidx/work/OneTimeWorkRequest$Builder;

    .line 53
    invoke-virtual {p1}, Landroidx/work/OneTimeWorkRequest$Builder;->build()Landroidx/work/WorkRequest;

    move-result-object p1

    check-cast p1, Landroidx/work/OneTimeWorkRequest;

    .line 57
    invoke-direct {p0}, Lcom/evernote/android/job/work/JobProxyWorkManager;->getWorkManager()Landroidx/work/WorkManager;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 62
    invoke-virtual {v0, p1}, Landroidx/work/WorkManager;->enqueue(Landroidx/work/WorkRequest;)Landroidx/work/Operation;

    return-void

    .line 59
    :cond_1
    new-instance p1, Lcom/evernote/android/job/JobProxyIllegalStateException;

    const-string v0, "WorkManager is null"

    invoke-direct {p1, v0}, Lcom/evernote/android/job/JobProxyIllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public plantPeriodic(Lcom/evernote/android/job/JobRequest;)V
    .locals 9

    .line 67
    new-instance v8, Landroidx/work/PeriodicWorkRequest$Builder;

    const-class v1, Lcom/evernote/android/job/work/PlatformWorker;

    invoke-virtual {p1}, Lcom/evernote/android/job/JobRequest;->getIntervalMs()J

    move-result-wide v2

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 68
    invoke-virtual {p1}, Lcom/evernote/android/job/JobRequest;->getFlexMs()J

    move-result-wide v5

    sget-object v7, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Landroidx/work/PeriodicWorkRequest$Builder;-><init>(Ljava/lang/Class;JLjava/util/concurrent/TimeUnit;JLjava/util/concurrent/TimeUnit;)V

    .line 69
    invoke-static {p1}, Lcom/evernote/android/job/work/JobProxyWorkManager;->buildConstraints(Lcom/evernote/android/job/JobRequest;)Landroidx/work/Constraints;

    move-result-object v0

    invoke-virtual {v8, v0}, Landroidx/work/PeriodicWorkRequest$Builder;->setConstraints(Landroidx/work/Constraints;)Landroidx/work/WorkRequest$Builder;

    move-result-object v0

    check-cast v0, Landroidx/work/PeriodicWorkRequest$Builder;

    .line 70
    invoke-virtual {p1}, Lcom/evernote/android/job/JobRequest;->getJobId()I

    move-result p1

    invoke-static {p1}, Lcom/evernote/android/job/work/JobProxyWorkManager;->createTag(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroidx/work/PeriodicWorkRequest$Builder;->addTag(Ljava/lang/String;)Landroidx/work/WorkRequest$Builder;

    move-result-object p1

    check-cast p1, Landroidx/work/PeriodicWorkRequest$Builder;

    .line 71
    invoke-virtual {p1}, Landroidx/work/PeriodicWorkRequest$Builder;->build()Landroidx/work/WorkRequest;

    move-result-object p1

    check-cast p1, Landroidx/work/PeriodicWorkRequest;

    .line 73
    invoke-direct {p0}, Lcom/evernote/android/job/work/JobProxyWorkManager;->getWorkManager()Landroidx/work/WorkManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 78
    invoke-virtual {v0, p1}, Landroidx/work/WorkManager;->enqueue(Landroidx/work/WorkRequest;)Landroidx/work/Operation;

    return-void

    .line 75
    :cond_0
    new-instance p1, Lcom/evernote/android/job/JobProxyIllegalStateException;

    const-string v0, "WorkManager is null"

    invoke-direct {p1, v0}, Lcom/evernote/android/job/JobProxyIllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public plantPeriodicFlexSupport(Lcom/evernote/android/job/JobRequest;)V
    .locals 2

    .line 83
    sget-object v0, Lcom/evernote/android/job/work/JobProxyWorkManager;->CAT:Lcom/evernote/android/job/util/JobCat;

    const-string v1, "plantPeriodicFlexSupport called although flex is supported"

    invoke-virtual {v0, v1}, Lcom/evernote/android/job/util/JobCat;->w(Ljava/lang/String;)V

    .line 84
    invoke-virtual {p0, p1}, Lcom/evernote/android/job/work/JobProxyWorkManager;->plantPeriodic(Lcom/evernote/android/job/JobRequest;)V

    return-void
.end method
