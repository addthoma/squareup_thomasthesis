.class public final Lcom/mattprecious/telescope/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mattprecious/telescope/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final telescope_TelescopeLayout:[I

.field public static final telescope_TelescopeLayout_telescope_pointerCount:I = 0x0

.field public static final telescope_TelescopeLayout_telescope_progressColor:I = 0x1

.field public static final telescope_TelescopeLayout_telescope_screenshotChildrenOnly:I = 0x2

.field public static final telescope_TelescopeLayout_telescope_screenshotMode:I = 0x3

.field public static final telescope_TelescopeLayout_telescope_vibrate:I = 0x4


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x5

    new-array v0, v0, [I

    .line 31
    fill-array-data v0, :array_0

    sput-object v0, Lcom/mattprecious/telescope/R$styleable;->telescope_TelescopeLayout:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f04042a
        0x7f04042b
        0x7f04042c
        0x7f04042d
        0x7f04042e
    .end array-data
.end method

.method private constructor <init>()V
    .locals 0

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
