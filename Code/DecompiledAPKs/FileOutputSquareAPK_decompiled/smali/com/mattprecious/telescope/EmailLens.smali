.class public Lcom/mattprecious/telescope/EmailLens;
.super Lcom/mattprecious/telescope/Lens;
.source "EmailLens.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mattprecious/telescope/EmailLens$CreateIntentTask;
    }
.end annotation


# instance fields
.field private final addresses:[Ljava/lang/String;

.field private final context:Landroid/content/Context;

.field private final subject:Ljava/lang/String;


# direct methods
.method public varargs constructor <init>(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 0

    .line 32
    invoke-direct {p0}, Lcom/mattprecious/telescope/Lens;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/mattprecious/telescope/EmailLens;->context:Landroid/content/Context;

    if-nez p3, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    .line 34
    :cond_0
    invoke-virtual {p3}, [Ljava/lang/String;->clone()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Ljava/lang/String;

    :goto_0
    iput-object p1, p0, Lcom/mattprecious/telescope/EmailLens;->addresses:[Ljava/lang/String;

    .line 35
    iput-object p2, p0, Lcom/mattprecious/telescope/EmailLens;->subject:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 29
    invoke-direct {p0, p1, p3, p2}, Lcom/mattprecious/telescope/EmailLens;-><init>(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$000(Lcom/mattprecious/telescope/EmailLens;)Ljava/lang/String;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/mattprecious/telescope/EmailLens;->subject:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$100(Lcom/mattprecious/telescope/EmailLens;)[Ljava/lang/String;
    .locals 0

    .line 21
    iget-object p0, p0, Lcom/mattprecious/telescope/EmailLens;->addresses:[Ljava/lang/String;

    return-object p0
.end method


# virtual methods
.method protected getAdditionalAttachments()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .line 48
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method protected getBody()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCapture(Ljava/io/File;)V
    .locals 2

    .line 44
    new-instance v0, Lcom/mattprecious/telescope/EmailLens$CreateIntentTask;

    iget-object v1, p0, Lcom/mattprecious/telescope/EmailLens;->context:Landroid/content/Context;

    invoke-direct {v0, p0, v1, p1}, Lcom/mattprecious/telescope/EmailLens$CreateIntentTask;-><init>(Lcom/mattprecious/telescope/EmailLens;Landroid/content/Context;Ljava/io/File;)V

    const/4 p1, 0x0

    new-array p1, p1, [Ljava/lang/Void;

    invoke-virtual {v0, p1}, Lcom/mattprecious/telescope/EmailLens$CreateIntentTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method
