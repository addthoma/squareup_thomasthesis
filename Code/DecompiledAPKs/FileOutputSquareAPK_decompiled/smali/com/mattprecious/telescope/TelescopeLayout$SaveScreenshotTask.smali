.class Lcom/mattprecious/telescope/TelescopeLayout$SaveScreenshotTask;
.super Landroid/os/AsyncTask;
.source "TelescopeLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mattprecious/telescope/TelescopeLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SaveScreenshotTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/io/File;",
        ">;"
    }
.end annotation


# instance fields
.field private final context:Landroid/content/Context;

.field private final screenshot:Landroid/graphics/Bitmap;

.field final synthetic this$0:Lcom/mattprecious/telescope/TelescopeLayout;


# direct methods
.method constructor <init>(Lcom/mattprecious/telescope/TelescopeLayout;Landroid/graphics/Bitmap;)V
    .locals 0

    .line 544
    iput-object p1, p0, Lcom/mattprecious/telescope/TelescopeLayout$SaveScreenshotTask;->this$0:Lcom/mattprecious/telescope/TelescopeLayout;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 545
    invoke-virtual {p1}, Lcom/mattprecious/telescope/TelescopeLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lcom/mattprecious/telescope/TelescopeLayout$SaveScreenshotTask;->context:Landroid/content/Context;

    .line 546
    iput-object p2, p0, Lcom/mattprecious/telescope/TelescopeLayout$SaveScreenshotTask;->screenshot:Landroid/graphics/Bitmap;

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/io/File;
    .locals 5

    .line 554
    iget-object p1, p0, Lcom/mattprecious/telescope/TelescopeLayout$SaveScreenshotTask;->screenshot:Landroid/graphics/Bitmap;

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return-object v0

    .line 559
    :cond_0
    :try_start_0
    iget-object p1, p0, Lcom/mattprecious/telescope/TelescopeLayout$SaveScreenshotTask;->context:Landroid/content/Context;

    invoke-static {p1}, Lcom/mattprecious/telescope/TelescopeLayout;->access$1200(Landroid/content/Context;)Ljava/io/File;

    move-result-object p1

    .line 560
    invoke-virtual {p1}, Ljava/io/File;->mkdirs()Z

    .line 562
    new-instance v1, Ljava/io/File;

    invoke-static {}, Lcom/mattprecious/telescope/TelescopeLayout;->access$1300()Ljava/text/SimpleDateFormat;

    move-result-object v2

    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    invoke-virtual {v2, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 563
    new-instance p1, Ljava/io/FileOutputStream;

    invoke-direct {p1, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 565
    iget-object v2, p0, Lcom/mattprecious/telescope/TelescopeLayout$SaveScreenshotTask;->screenshot:Landroid/graphics/Bitmap;

    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v4, 0x64

    invoke-virtual {v2, v3, v4, p1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 566
    invoke-virtual {p1}, Ljava/io/FileOutputStream;->flush()V

    .line 567
    invoke-virtual {p1}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    :catch_0
    const-string p1, "Telescope"

    const-string v1, "Failed to save screenshot. Is the WRITE_EXTERNAL_STORAGE permission requested?"

    .line 571
    invoke-static {p1, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 540
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/mattprecious/telescope/TelescopeLayout$SaveScreenshotTask;->doInBackground([Ljava/lang/Void;)Ljava/io/File;

    move-result-object p1

    return-object p1
.end method

.method protected onPostExecute(Ljava/io/File;)V
    .locals 2

    .line 579
    iget-object v0, p0, Lcom/mattprecious/telescope/TelescopeLayout$SaveScreenshotTask;->this$0:Lcom/mattprecious/telescope/TelescopeLayout;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/mattprecious/telescope/TelescopeLayout;->access$1102(Lcom/mattprecious/telescope/TelescopeLayout;Z)Z

    .line 581
    iget-object v0, p0, Lcom/mattprecious/telescope/TelescopeLayout$SaveScreenshotTask;->this$0:Lcom/mattprecious/telescope/TelescopeLayout;

    invoke-static {v0}, Lcom/mattprecious/telescope/TelescopeLayout;->access$900(Lcom/mattprecious/telescope/TelescopeLayout;)V

    .line 582
    iget-object v0, p0, Lcom/mattprecious/telescope/TelescopeLayout$SaveScreenshotTask;->this$0:Lcom/mattprecious/telescope/TelescopeLayout;

    invoke-static {v0}, Lcom/mattprecious/telescope/TelescopeLayout;->access$1000(Lcom/mattprecious/telescope/TelescopeLayout;)Lcom/mattprecious/telescope/Lens;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/mattprecious/telescope/Lens;->onCapture(Ljava/io/File;)V

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 540
    check-cast p1, Ljava/io/File;

    invoke-virtual {p0, p1}, Lcom/mattprecious/telescope/TelescopeLayout$SaveScreenshotTask;->onPostExecute(Ljava/io/File;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .line 550
    iget-object v0, p0, Lcom/mattprecious/telescope/TelescopeLayout$SaveScreenshotTask;->this$0:Lcom/mattprecious/telescope/TelescopeLayout;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/mattprecious/telescope/TelescopeLayout;->access$1102(Lcom/mattprecious/telescope/TelescopeLayout;Z)Z

    return-void
.end method
