.class final Lcom/f2prateek/rx/preferences2/LongAdapter;
.super Ljava/lang/Object;
.source "LongAdapter.java"

# interfaces
.implements Lcom/f2prateek/rx/preferences2/RealPreference$Adapter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/f2prateek/rx/preferences2/RealPreference$Adapter<",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/f2prateek/rx/preferences2/LongAdapter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 7
    new-instance v0, Lcom/f2prateek/rx/preferences2/LongAdapter;

    invoke-direct {v0}, Lcom/f2prateek/rx/preferences2/LongAdapter;-><init>()V

    sput-object v0, Lcom/f2prateek/rx/preferences2/LongAdapter;->INSTANCE:Lcom/f2prateek/rx/preferences2/LongAdapter;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public get(Ljava/lang/String;Landroid/content/SharedPreferences;)Ljava/lang/Long;
    .locals 2

    const-wide/16 v0, 0x0

    .line 10
    invoke-interface {p2, p1, v0, v1}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide p1

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic get(Ljava/lang/String;Landroid/content/SharedPreferences;)Ljava/lang/Object;
    .locals 0

    .line 6
    invoke-virtual {p0, p1, p2}, Lcom/f2prateek/rx/preferences2/LongAdapter;->get(Ljava/lang/String;Landroid/content/SharedPreferences;)Ljava/lang/Long;

    move-result-object p1

    return-object p1
.end method

.method public set(Ljava/lang/String;Ljava/lang/Long;Landroid/content/SharedPreferences$Editor;)V
    .locals 2

    .line 15
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-interface {p3, p1, v0, v1}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    return-void
.end method

.method public bridge synthetic set(Ljava/lang/String;Ljava/lang/Object;Landroid/content/SharedPreferences$Editor;)V
    .locals 0

    .line 6
    check-cast p2, Ljava/lang/Long;

    invoke-virtual {p0, p1, p2, p3}, Lcom/f2prateek/rx/preferences2/LongAdapter;->set(Ljava/lang/String;Ljava/lang/Long;Landroid/content/SharedPreferences$Editor;)V

    return-void
.end method
