.class public Lcom/github/mikephil/charting/charts/BarLineChartBase$DefaultFillFormatter;
.super Ljava/lang/Object;
.source "BarLineChartBase.java"

# interfaces
.implements Lcom/github/mikephil/charting/utils/FillFormatter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/github/mikephil/charting/charts/BarLineChartBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "DefaultFillFormatter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/github/mikephil/charting/charts/BarLineChartBase;


# direct methods
.method protected constructor <init>(Lcom/github/mikephil/charting/charts/BarLineChartBase;)V
    .locals 0

    .line 1408
    iput-object p1, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase$DefaultFillFormatter;->this$0:Lcom/github/mikephil/charting/charts/BarLineChartBase;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getFillLinePosition(Lcom/github/mikephil/charting/data/LineDataSet;Lcom/github/mikephil/charting/data/LineData;FF)F
    .locals 3

    .line 1416
    invoke-virtual {p1}, Lcom/github/mikephil/charting/data/LineDataSet;->getYMax()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    invoke-virtual {p1}, Lcom/github/mikephil/charting/data/LineDataSet;->getYMin()F

    move-result v0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    goto :goto_0

    .line 1420
    :cond_0
    iget-object v0, p0, Lcom/github/mikephil/charting/charts/BarLineChartBase$DefaultFillFormatter;->this$0:Lcom/github/mikephil/charting/charts/BarLineChartBase;

    invoke-virtual {p1}, Lcom/github/mikephil/charting/data/LineDataSet;->getAxisDependency()Lcom/github/mikephil/charting/components/YAxis$AxisDependency;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/github/mikephil/charting/charts/BarLineChartBase;->getAxis(Lcom/github/mikephil/charting/components/YAxis$AxisDependency;)Lcom/github/mikephil/charting/components/YAxis;

    move-result-object v0

    invoke-virtual {v0}, Lcom/github/mikephil/charting/components/YAxis;->isStartAtZeroEnabled()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1424
    invoke-virtual {p2}, Lcom/github/mikephil/charting/data/LineData;->getYMax()F

    move-result v0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    const/4 p3, 0x0

    .line 1428
    :cond_1
    invoke-virtual {p2}, Lcom/github/mikephil/charting/data/LineData;->getYMin()F

    move-result p2

    cmpg-float p2, p2, v1

    if-gez p2, :cond_2

    const/4 p4, 0x0

    .line 1433
    :cond_2
    invoke-virtual {p1}, Lcom/github/mikephil/charting/data/LineDataSet;->getYMin()F

    move-result p1

    cmpl-float p1, p1, v1

    if-ltz p1, :cond_3

    move v1, p4

    goto :goto_0

    :cond_3
    move v1, p3

    :cond_4
    :goto_0
    return v1
.end method
