.class public abstract Lorg/sqlite/core/CorePreparedStatement;
.super Lorg/sqlite/jdbc4/JDBC4Statement;
.source "CorePreparedStatement.java"


# instance fields
.field protected batchQueryCount:I

.field protected columnCount:I

.field protected paramCount:I


# direct methods
.method protected constructor <init>(Lorg/sqlite/SQLiteConnection;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/sql/SQLException;
        }
    .end annotation

    .line 38
    invoke-direct {p0, p1}, Lorg/sqlite/jdbc4/JDBC4Statement;-><init>(Lorg/sqlite/SQLiteConnection;)V

    .line 40
    iput-object p2, p0, Lorg/sqlite/core/CorePreparedStatement;->sql:Ljava/lang/String;

    .line 41
    iget-object p1, p0, Lorg/sqlite/core/CorePreparedStatement;->db:Lorg/sqlite/core/DB;

    invoke-virtual {p1, p0}, Lorg/sqlite/core/DB;->prepare(Lorg/sqlite/core/CoreStatement;)V

    .line 42
    iget-object p1, p0, Lorg/sqlite/core/CorePreparedStatement;->rs:Lorg/sqlite/core/CoreResultSet;

    iget-object p2, p0, Lorg/sqlite/core/CorePreparedStatement;->db:Lorg/sqlite/core/DB;

    iget-wide v0, p0, Lorg/sqlite/core/CorePreparedStatement;->pointer:J

    invoke-virtual {p2, v0, v1}, Lorg/sqlite/core/DB;->column_names(J)[Ljava/lang/String;

    move-result-object p2

    iput-object p2, p1, Lorg/sqlite/core/CoreResultSet;->colsMeta:[Ljava/lang/String;

    .line 43
    iget-object p1, p0, Lorg/sqlite/core/CorePreparedStatement;->db:Lorg/sqlite/core/DB;

    iget-wide v0, p0, Lorg/sqlite/core/CorePreparedStatement;->pointer:J

    invoke-virtual {p1, v0, v1}, Lorg/sqlite/core/DB;->column_count(J)I

    move-result p1

    iput p1, p0, Lorg/sqlite/core/CorePreparedStatement;->columnCount:I

    .line 44
    iget-object p1, p0, Lorg/sqlite/core/CorePreparedStatement;->db:Lorg/sqlite/core/DB;

    iget-wide v0, p0, Lorg/sqlite/core/CorePreparedStatement;->pointer:J

    invoke-virtual {p1, v0, v1}, Lorg/sqlite/core/DB;->bind_parameter_count(J)I

    move-result p1

    iput p1, p0, Lorg/sqlite/core/CorePreparedStatement;->paramCount:I

    const/4 p1, 0x0

    .line 45
    iput p1, p0, Lorg/sqlite/core/CorePreparedStatement;->batchQueryCount:I

    const/4 p2, 0x0

    .line 46
    iput-object p2, p0, Lorg/sqlite/core/CorePreparedStatement;->batch:[Ljava/lang/Object;

    .line 47
    iput p1, p0, Lorg/sqlite/core/CorePreparedStatement;->batchPos:I

    return-void
.end method


# virtual methods
.method protected batch(ILjava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/sql/SQLException;
        }
    .end annotation

    .line 117
    invoke-virtual {p0}, Lorg/sqlite/core/CorePreparedStatement;->checkOpen()V

    .line 118
    iget-object v0, p0, Lorg/sqlite/core/CorePreparedStatement;->batch:[Ljava/lang/Object;

    if-nez v0, :cond_0

    .line 119
    iget v0, p0, Lorg/sqlite/core/CorePreparedStatement;->paramCount:I

    new-array v0, v0, [Ljava/lang/Object;

    iput-object v0, p0, Lorg/sqlite/core/CorePreparedStatement;->batch:[Ljava/lang/Object;

    .line 121
    :cond_0
    iget-object v0, p0, Lorg/sqlite/core/CorePreparedStatement;->batch:[Ljava/lang/Object;

    iget v1, p0, Lorg/sqlite/core/CorePreparedStatement;->batchPos:I

    add-int/2addr v1, p1

    add-int/lit8 v1, v1, -0x1

    aput-object p2, v0, v1

    return-void
.end method

.method protected checkParameters()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/sql/SQLException;
        }
    .end annotation

    .line 63
    iget-object v0, p0, Lorg/sqlite/core/CorePreparedStatement;->batch:[Ljava/lang/Object;

    if-nez v0, :cond_1

    iget v0, p0, Lorg/sqlite/core/CorePreparedStatement;->paramCount:I

    if-gtz v0, :cond_0

    goto :goto_0

    .line 64
    :cond_0
    new-instance v0, Ljava/sql/SQLException;

    const-string v1, "Values not bound to statement"

    invoke-direct {v0, v1}, Ljava/sql/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    :goto_0
    return-void
.end method

.method public clearBatch()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/sql/SQLException;
        }
    .end annotation

    .line 91
    invoke-super {p0}, Lorg/sqlite/jdbc4/JDBC4Statement;->clearBatch()V

    const/4 v0, 0x0

    .line 92
    iput v0, p0, Lorg/sqlite/core/CorePreparedStatement;->batchQueryCount:I

    return-void
.end method

.method public executeBatch()[I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/sql/SQLException;
        }
    .end annotation

    .line 72
    iget v0, p0, Lorg/sqlite/core/CorePreparedStatement;->batchQueryCount:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [I

    return-object v0

    .line 76
    :cond_0
    invoke-virtual {p0}, Lorg/sqlite/core/CorePreparedStatement;->checkParameters()V

    .line 79
    :try_start_0
    iget-object v0, p0, Lorg/sqlite/core/CorePreparedStatement;->db:Lorg/sqlite/core/DB;

    iget-wide v1, p0, Lorg/sqlite/core/CorePreparedStatement;->pointer:J

    iget v3, p0, Lorg/sqlite/core/CorePreparedStatement;->batchQueryCount:I

    iget-object v4, p0, Lorg/sqlite/core/CorePreparedStatement;->batch:[Ljava/lang/Object;

    invoke-virtual {v0, v1, v2, v3, v4}, Lorg/sqlite/core/DB;->executeBatch(JI[Ljava/lang/Object;)[I

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 82
    invoke-virtual {p0}, Lorg/sqlite/core/CorePreparedStatement;->clearBatch()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lorg/sqlite/core/CorePreparedStatement;->clearBatch()V

    throw v0
.end method

.method protected finalize()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/sql/SQLException;
        }
    .end annotation

    .line 55
    invoke-virtual {p0}, Lorg/sqlite/core/CorePreparedStatement;->close()V

    return-void
.end method

.method public getUpdateCount()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/sql/SQLException;
        }
    .end annotation

    .line 100
    iget-wide v0, p0, Lorg/sqlite/core/CorePreparedStatement;->pointer:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-eqz v4, :cond_1

    iget-boolean v0, p0, Lorg/sqlite/core/CorePreparedStatement;->resultsWaiting:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/sqlite/core/CorePreparedStatement;->rs:Lorg/sqlite/core/CoreResultSet;

    invoke-virtual {v0}, Lorg/sqlite/core/CoreResultSet;->isOpen()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 104
    :cond_0
    iget-object v0, p0, Lorg/sqlite/core/CorePreparedStatement;->db:Lorg/sqlite/core/DB;

    invoke-virtual {v0}, Lorg/sqlite/core/DB;->changes()I

    move-result v0

    return v0

    :cond_1
    :goto_0
    const/4 v0, -0x1

    return v0
.end method

.method protected setDateByMilliseconds(ILjava/lang/Long;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/sql/SQLException;
        }
    .end annotation

    .line 129
    sget-object v0, Lorg/sqlite/core/CorePreparedStatement$1;->$SwitchMap$org$sqlite$SQLiteConfig$DateClass:[I

    iget-object v1, p0, Lorg/sqlite/core/CorePreparedStatement;->conn:Lorg/sqlite/SQLiteConnection;

    iget-object v1, v1, Lorg/sqlite/SQLiteConnection;->dateClass:Lorg/sqlite/SQLiteConfig$DateClass;

    invoke-virtual {v1}, Lorg/sqlite/SQLiteConfig$DateClass;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 140
    new-instance v0, Ljava/lang/Long;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    iget-object p2, p0, Lorg/sqlite/core/CorePreparedStatement;->conn:Lorg/sqlite/SQLiteConnection;

    iget-wide v3, p2, Lorg/sqlite/SQLiteConnection;->dateMultiplier:J

    div-long/2addr v1, v3

    invoke-direct {v0, v1, v2}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {p0, p1, v0}, Lorg/sqlite/core/CorePreparedStatement;->batch(ILjava/lang/Object;)V

    goto :goto_0

    .line 136
    :cond_0
    new-instance v0, Ljava/lang/Double;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    long-to-double v1, v1

    const-wide v3, 0x4194997000000000L    # 8.64E7

    div-double/2addr v1, v3

    const-wide v3, 0x41429ec5c0000000L    # 2440587.5

    add-double/2addr v1, v3

    invoke-direct {v0, v1, v2}, Ljava/lang/Double;-><init>(D)V

    invoke-virtual {p0, p1, v0}, Lorg/sqlite/core/CorePreparedStatement;->batch(ILjava/lang/Object;)V

    goto :goto_0

    .line 131
    :cond_1
    iget-object v0, p0, Lorg/sqlite/core/CorePreparedStatement;->conn:Lorg/sqlite/SQLiteConnection;

    iget-object v0, v0, Lorg/sqlite/SQLiteConnection;->dateFormat:Lorg/sqlite/date/FastDateFormat;

    new-instance v1, Ljava/sql/Date;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Ljava/sql/Date;-><init>(J)V

    invoke-virtual {v0, v1}, Lorg/sqlite/date/FastDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Lorg/sqlite/core/CorePreparedStatement;->batch(ILjava/lang/Object;)V

    :goto_0
    return-void
.end method
