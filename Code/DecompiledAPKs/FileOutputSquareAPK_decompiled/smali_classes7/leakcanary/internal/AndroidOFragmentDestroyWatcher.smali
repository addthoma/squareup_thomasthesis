.class public final Lleakcanary/internal/AndroidOFragmentDestroyWatcher;
.super Ljava/lang/Object;
.source "AndroidOFragmentDestroyWatcher.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lkotlin/jvm/functions/Function1<",
        "Landroid/app/Activity;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000+\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0005*\u0001\u000b\u0008\u0001\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\u001b\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u00a2\u0006\u0002\u0010\tJ\u0011\u0010\r\u001a\u00020\u00032\u0006\u0010\u000e\u001a\u00020\u0002H\u0096\u0002R\u0014\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010\u000cR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000f"
    }
    d2 = {
        "Lleakcanary/internal/AndroidOFragmentDestroyWatcher;",
        "Lkotlin/Function1;",
        "Landroid/app/Activity;",
        "",
        "objectWatcher",
        "Lleakcanary/ObjectWatcher;",
        "configProvider",
        "Lkotlin/Function0;",
        "Lleakcanary/AppWatcher$Config;",
        "(Lleakcanary/ObjectWatcher;Lkotlin/jvm/functions/Function0;)V",
        "fragmentLifecycleCallbacks",
        "leakcanary/internal/AndroidOFragmentDestroyWatcher$fragmentLifecycleCallbacks$1",
        "Lleakcanary/internal/AndroidOFragmentDestroyWatcher$fragmentLifecycleCallbacks$1;",
        "invoke",
        "activity",
        "leakcanary-object-watcher-android_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final configProvider:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lleakcanary/AppWatcher$Config;",
            ">;"
        }
    .end annotation
.end field

.field private final fragmentLifecycleCallbacks:Lleakcanary/internal/AndroidOFragmentDestroyWatcher$fragmentLifecycleCallbacks$1;

.field private final objectWatcher:Lleakcanary/ObjectWatcher;


# direct methods
.method public constructor <init>(Lleakcanary/ObjectWatcher;Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lleakcanary/ObjectWatcher;",
            "Lkotlin/jvm/functions/Function0<",
            "Lleakcanary/AppWatcher$Config;",
            ">;)V"
        }
    .end annotation

    const-string v0, "objectWatcher"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "configProvider"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lleakcanary/internal/AndroidOFragmentDestroyWatcher;->objectWatcher:Lleakcanary/ObjectWatcher;

    iput-object p2, p0, Lleakcanary/internal/AndroidOFragmentDestroyWatcher;->configProvider:Lkotlin/jvm/functions/Function0;

    .line 32
    new-instance p1, Lleakcanary/internal/AndroidOFragmentDestroyWatcher$fragmentLifecycleCallbacks$1;

    invoke-direct {p1, p0}, Lleakcanary/internal/AndroidOFragmentDestroyWatcher$fragmentLifecycleCallbacks$1;-><init>(Lleakcanary/internal/AndroidOFragmentDestroyWatcher;)V

    iput-object p1, p0, Lleakcanary/internal/AndroidOFragmentDestroyWatcher;->fragmentLifecycleCallbacks:Lleakcanary/internal/AndroidOFragmentDestroyWatcher$fragmentLifecycleCallbacks$1;

    return-void
.end method

.method public static final synthetic access$getConfigProvider$p(Lleakcanary/internal/AndroidOFragmentDestroyWatcher;)Lkotlin/jvm/functions/Function0;
    .locals 0

    .line 28
    iget-object p0, p0, Lleakcanary/internal/AndroidOFragmentDestroyWatcher;->configProvider:Lkotlin/jvm/functions/Function0;

    return-object p0
.end method

.method public static final synthetic access$getObjectWatcher$p(Lleakcanary/internal/AndroidOFragmentDestroyWatcher;)Lleakcanary/ObjectWatcher;
    .locals 0

    .line 28
    iget-object p0, p0, Lleakcanary/internal/AndroidOFragmentDestroyWatcher;->objectWatcher:Lleakcanary/ObjectWatcher;

    return-object p0
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 28
    check-cast p1, Landroid/app/Activity;

    invoke-virtual {p0, p1}, Lleakcanary/internal/AndroidOFragmentDestroyWatcher;->invoke(Landroid/app/Activity;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public invoke(Landroid/app/Activity;)V
    .locals 2

    const-string v0, "activity"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    invoke-virtual {p1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object p1

    .line 61
    iget-object v0, p0, Lleakcanary/internal/AndroidOFragmentDestroyWatcher;->fragmentLifecycleCallbacks:Lleakcanary/internal/AndroidOFragmentDestroyWatcher$fragmentLifecycleCallbacks$1;

    check-cast v0, Landroid/app/FragmentManager$FragmentLifecycleCallbacks;

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/app/FragmentManager;->registerFragmentLifecycleCallbacks(Landroid/app/FragmentManager$FragmentLifecycleCallbacks;Z)V

    return-void
.end method
