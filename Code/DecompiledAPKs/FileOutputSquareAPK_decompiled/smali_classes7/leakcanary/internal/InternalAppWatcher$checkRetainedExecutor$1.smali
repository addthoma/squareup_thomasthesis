.class final Lleakcanary/internal/InternalAppWatcher$checkRetainedExecutor$1;
.super Ljava/lang/Object;
.source "InternalAppWatcher.kt"

# interfaces
.implements Ljava/util/concurrent/Executor;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lleakcanary/internal/InternalAppWatcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Ljava/lang/Runnable;",
        "kotlin.jvm.PlatformType",
        "execute"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lleakcanary/internal/InternalAppWatcher$checkRetainedExecutor$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lleakcanary/internal/InternalAppWatcher$checkRetainedExecutor$1;

    invoke-direct {v0}, Lleakcanary/internal/InternalAppWatcher$checkRetainedExecutor$1;-><init>()V

    sput-object v0, Lleakcanary/internal/InternalAppWatcher$checkRetainedExecutor$1;->INSTANCE:Lleakcanary/internal/InternalAppWatcher$checkRetainedExecutor$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final execute(Ljava/lang/Runnable;)V
    .locals 3

    .line 56
    sget-object v0, Lleakcanary/internal/InternalAppWatcher;->INSTANCE:Lleakcanary/internal/InternalAppWatcher;

    invoke-static {v0}, Lleakcanary/internal/InternalAppWatcher;->access$getMainHandler$p(Lleakcanary/internal/InternalAppWatcher;)Landroid/os/Handler;

    move-result-object v0

    invoke-static {}, Lleakcanary/AppWatcher;->getConfig()Lleakcanary/AppWatcher$Config;

    move-result-object v1

    invoke-virtual {v1}, Lleakcanary/AppWatcher$Config;->getWatchDurationMillis()J

    move-result-wide v1

    invoke-virtual {v0, p1, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method
