.class public final Lleakcanary/AppWatcher$Config$Builder;
.super Ljava/lang/Object;
.source "AppWatcher.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lleakcanary/AppWatcher$Config;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAppWatcher.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AppWatcher.kt\nleakcanary/AppWatcher$Config$Builder\n*L\n1#1,204:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0002\u0008\u0005\u0018\u00002\u00020\u0001B\u000f\u0008\u0000\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0006\u0010\r\u001a\u00020\u0003J\u000e\u0010\u0005\u001a\u00020\u00002\u0006\u0010\u0005\u001a\u00020\u0006J\u000e\u0010\u0007\u001a\u00020\u00002\u0006\u0010\u0007\u001a\u00020\u0006J\u000e\u0010\u0008\u001a\u00020\u00002\u0006\u0010\u0008\u001a\u00020\tJ\u000e\u0010\n\u001a\u00020\u00002\u0006\u0010\n\u001a\u00020\u0006J\u000e\u0010\u000b\u001a\u00020\u00002\u0006\u0010\u000b\u001a\u00020\u0006J\u000e\u0010\u000c\u001a\u00020\u00002\u0006\u0010\u000c\u001a\u00020\u0006R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000e"
    }
    d2 = {
        "Lleakcanary/AppWatcher$Config$Builder;",
        "",
        "config",
        "Lleakcanary/AppWatcher$Config;",
        "(Lleakcanary/AppWatcher$Config;)V",
        "enabled",
        "",
        "watchActivities",
        "watchDurationMillis",
        "",
        "watchFragmentViews",
        "watchFragments",
        "watchViewModels",
        "build",
        "leakcanary-object-watcher-android_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private enabled:Z

.field private watchActivities:Z

.field private watchDurationMillis:J

.field private watchFragmentViews:Z

.field private watchFragments:Z

.field private watchViewModels:Z


# direct methods
.method public constructor <init>(Lleakcanary/AppWatcher$Config;)V
    .locals 2

    const-string v0, "config"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    invoke-virtual {p1}, Lleakcanary/AppWatcher$Config;->getEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lleakcanary/AppWatcher$Config$Builder;->enabled:Z

    .line 95
    invoke-virtual {p1}, Lleakcanary/AppWatcher$Config;->getWatchActivities()Z

    move-result v0

    iput-boolean v0, p0, Lleakcanary/AppWatcher$Config$Builder;->watchActivities:Z

    .line 96
    invoke-virtual {p1}, Lleakcanary/AppWatcher$Config;->getWatchFragments()Z

    move-result v0

    iput-boolean v0, p0, Lleakcanary/AppWatcher$Config$Builder;->watchFragments:Z

    .line 97
    invoke-virtual {p1}, Lleakcanary/AppWatcher$Config;->getWatchFragmentViews()Z

    move-result v0

    iput-boolean v0, p0, Lleakcanary/AppWatcher$Config$Builder;->watchFragmentViews:Z

    .line 98
    invoke-virtual {p1}, Lleakcanary/AppWatcher$Config;->getWatchViewModels()Z

    move-result v0

    iput-boolean v0, p0, Lleakcanary/AppWatcher$Config$Builder;->watchViewModels:Z

    .line 99
    invoke-virtual {p1}, Lleakcanary/AppWatcher$Config;->getWatchDurationMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lleakcanary/AppWatcher$Config$Builder;->watchDurationMillis:J

    return-void
.end method


# virtual methods
.method public final build()Lleakcanary/AppWatcher$Config;
    .locals 8

    .line 125
    invoke-static {}, Lleakcanary/AppWatcher;->getConfig()Lleakcanary/AppWatcher$Config;

    move-result-object v0

    .line 126
    iget-boolean v1, p0, Lleakcanary/AppWatcher$Config$Builder;->enabled:Z

    .line 127
    iget-boolean v2, p0, Lleakcanary/AppWatcher$Config$Builder;->watchActivities:Z

    .line 128
    iget-boolean v3, p0, Lleakcanary/AppWatcher$Config$Builder;->watchFragments:Z

    .line 129
    iget-boolean v4, p0, Lleakcanary/AppWatcher$Config$Builder;->watchFragmentViews:Z

    .line 130
    iget-boolean v5, p0, Lleakcanary/AppWatcher$Config$Builder;->watchViewModels:Z

    .line 131
    iget-wide v6, p0, Lleakcanary/AppWatcher$Config$Builder;->watchDurationMillis:J

    .line 125
    invoke-virtual/range {v0 .. v7}, Lleakcanary/AppWatcher$Config;->copy(ZZZZZJ)Lleakcanary/AppWatcher$Config;

    move-result-object v0

    return-object v0
.end method

.method public final enabled(Z)Lleakcanary/AppWatcher$Config$Builder;
    .locals 1

    .line 103
    move-object v0, p0

    check-cast v0, Lleakcanary/AppWatcher$Config$Builder;

    iput-boolean p1, v0, Lleakcanary/AppWatcher$Config$Builder;->enabled:Z

    return-object v0
.end method

.method public final watchActivities(Z)Lleakcanary/AppWatcher$Config$Builder;
    .locals 1

    .line 107
    move-object v0, p0

    check-cast v0, Lleakcanary/AppWatcher$Config$Builder;

    iput-boolean p1, v0, Lleakcanary/AppWatcher$Config$Builder;->watchActivities:Z

    return-object v0
.end method

.method public final watchDurationMillis(J)Lleakcanary/AppWatcher$Config$Builder;
    .locals 1

    .line 123
    move-object v0, p0

    check-cast v0, Lleakcanary/AppWatcher$Config$Builder;

    iput-wide p1, v0, Lleakcanary/AppWatcher$Config$Builder;->watchDurationMillis:J

    return-object v0
.end method

.method public final watchFragmentViews(Z)Lleakcanary/AppWatcher$Config$Builder;
    .locals 1

    .line 115
    move-object v0, p0

    check-cast v0, Lleakcanary/AppWatcher$Config$Builder;

    iput-boolean p1, v0, Lleakcanary/AppWatcher$Config$Builder;->watchFragmentViews:Z

    return-object v0
.end method

.method public final watchFragments(Z)Lleakcanary/AppWatcher$Config$Builder;
    .locals 1

    .line 111
    move-object v0, p0

    check-cast v0, Lleakcanary/AppWatcher$Config$Builder;

    iput-boolean p1, v0, Lleakcanary/AppWatcher$Config$Builder;->watchFragments:Z

    return-object v0
.end method

.method public final watchViewModels(Z)Lleakcanary/AppWatcher$Config$Builder;
    .locals 1

    .line 119
    move-object v0, p0

    check-cast v0, Lleakcanary/AppWatcher$Config$Builder;

    iput-boolean p1, v0, Lleakcanary/AppWatcher$Config$Builder;->watchViewModels:Z

    return-object v0
.end method
