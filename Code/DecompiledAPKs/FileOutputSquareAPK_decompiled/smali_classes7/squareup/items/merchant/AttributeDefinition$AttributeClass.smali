.class public final enum Lsquareup/items/merchant/AttributeDefinition$AttributeClass;
.super Ljava/lang/Enum;
.source "AttributeDefinition.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/items/merchant/AttributeDefinition;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AttributeClass"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsquareup/items/merchant/AttributeDefinition$AttributeClass$ProtoAdapter_AttributeClass;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lsquareup/items/merchant/AttributeDefinition$AttributeClass;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lsquareup/items/merchant/AttributeDefinition$AttributeClass;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lsquareup/items/merchant/AttributeDefinition$AttributeClass;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum ATTRIBUTE_CLASS_DO_NOT_USE:Lsquareup/items/merchant/AttributeDefinition$AttributeClass;

.field public static final enum CUSTOM:Lsquareup/items/merchant/AttributeDefinition$AttributeClass;

.field public static final enum INTERNAL:Lsquareup/items/merchant/AttributeDefinition$AttributeClass;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 411
    new-instance v0, Lsquareup/items/merchant/AttributeDefinition$AttributeClass;

    const/4 v1, 0x0

    const-string v2, "ATTRIBUTE_CLASS_DO_NOT_USE"

    invoke-direct {v0, v2, v1, v1}, Lsquareup/items/merchant/AttributeDefinition$AttributeClass;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/AttributeDefinition$AttributeClass;->ATTRIBUTE_CLASS_DO_NOT_USE:Lsquareup/items/merchant/AttributeDefinition$AttributeClass;

    .line 413
    new-instance v0, Lsquareup/items/merchant/AttributeDefinition$AttributeClass;

    const/4 v2, 0x1

    const-string v3, "INTERNAL"

    invoke-direct {v0, v3, v2, v2}, Lsquareup/items/merchant/AttributeDefinition$AttributeClass;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/AttributeDefinition$AttributeClass;->INTERNAL:Lsquareup/items/merchant/AttributeDefinition$AttributeClass;

    .line 415
    new-instance v0, Lsquareup/items/merchant/AttributeDefinition$AttributeClass;

    const/4 v3, 0x2

    const-string v4, "CUSTOM"

    invoke-direct {v0, v4, v3, v3}, Lsquareup/items/merchant/AttributeDefinition$AttributeClass;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/items/merchant/AttributeDefinition$AttributeClass;->CUSTOM:Lsquareup/items/merchant/AttributeDefinition$AttributeClass;

    const/4 v0, 0x3

    new-array v0, v0, [Lsquareup/items/merchant/AttributeDefinition$AttributeClass;

    .line 410
    sget-object v4, Lsquareup/items/merchant/AttributeDefinition$AttributeClass;->ATTRIBUTE_CLASS_DO_NOT_USE:Lsquareup/items/merchant/AttributeDefinition$AttributeClass;

    aput-object v4, v0, v1

    sget-object v1, Lsquareup/items/merchant/AttributeDefinition$AttributeClass;->INTERNAL:Lsquareup/items/merchant/AttributeDefinition$AttributeClass;

    aput-object v1, v0, v2

    sget-object v1, Lsquareup/items/merchant/AttributeDefinition$AttributeClass;->CUSTOM:Lsquareup/items/merchant/AttributeDefinition$AttributeClass;

    aput-object v1, v0, v3

    sput-object v0, Lsquareup/items/merchant/AttributeDefinition$AttributeClass;->$VALUES:[Lsquareup/items/merchant/AttributeDefinition$AttributeClass;

    .line 417
    new-instance v0, Lsquareup/items/merchant/AttributeDefinition$AttributeClass$ProtoAdapter_AttributeClass;

    invoke-direct {v0}, Lsquareup/items/merchant/AttributeDefinition$AttributeClass$ProtoAdapter_AttributeClass;-><init>()V

    sput-object v0, Lsquareup/items/merchant/AttributeDefinition$AttributeClass;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 421
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 422
    iput p3, p0, Lsquareup/items/merchant/AttributeDefinition$AttributeClass;->value:I

    return-void
.end method

.method public static fromValue(I)Lsquareup/items/merchant/AttributeDefinition$AttributeClass;
    .locals 1

    if-eqz p0, :cond_2

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 432
    :cond_0
    sget-object p0, Lsquareup/items/merchant/AttributeDefinition$AttributeClass;->CUSTOM:Lsquareup/items/merchant/AttributeDefinition$AttributeClass;

    return-object p0

    .line 431
    :cond_1
    sget-object p0, Lsquareup/items/merchant/AttributeDefinition$AttributeClass;->INTERNAL:Lsquareup/items/merchant/AttributeDefinition$AttributeClass;

    return-object p0

    .line 430
    :cond_2
    sget-object p0, Lsquareup/items/merchant/AttributeDefinition$AttributeClass;->ATTRIBUTE_CLASS_DO_NOT_USE:Lsquareup/items/merchant/AttributeDefinition$AttributeClass;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lsquareup/items/merchant/AttributeDefinition$AttributeClass;
    .locals 1

    .line 410
    const-class v0, Lsquareup/items/merchant/AttributeDefinition$AttributeClass;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lsquareup/items/merchant/AttributeDefinition$AttributeClass;

    return-object p0
.end method

.method public static values()[Lsquareup/items/merchant/AttributeDefinition$AttributeClass;
    .locals 1

    .line 410
    sget-object v0, Lsquareup/items/merchant/AttributeDefinition$AttributeClass;->$VALUES:[Lsquareup/items/merchant/AttributeDefinition$AttributeClass;

    invoke-virtual {v0}, [Lsquareup/items/merchant/AttributeDefinition$AttributeClass;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lsquareup/items/merchant/AttributeDefinition$AttributeClass;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 439
    iget v0, p0, Lsquareup/items/merchant/AttributeDefinition$AttributeClass;->value:I

    return v0
.end method
