.class final Lsquareup/items/merchant/Response$ProtoAdapter_Response;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Response.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/items/merchant/Response;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Response"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lsquareup/items/merchant/Response;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 134
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lsquareup/items/merchant/Response;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 132
    invoke-virtual {p0, p1}, Lsquareup/items/merchant/Response$ProtoAdapter_Response;->decode(Lcom/squareup/wire/ProtoReader;)Lsquareup/items/merchant/Response;

    move-result-object p1

    return-object p1
.end method

.method public decode(Lcom/squareup/wire/ProtoReader;)Lsquareup/items/merchant/Response;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 155
    new-instance v0, Lsquareup/items/merchant/Response$Builder;

    invoke-direct {v0}, Lsquareup/items/merchant/Response$Builder;-><init>()V

    .line 156
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 157
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 163
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 161
    :cond_0
    sget-object v3, Lsquareup/items/merchant/DeleteResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lsquareup/items/merchant/DeleteResponse;

    invoke-virtual {v0, v3}, Lsquareup/items/merchant/Response$Builder;->delete_response(Lsquareup/items/merchant/DeleteResponse;)Lsquareup/items/merchant/Response$Builder;

    goto :goto_0

    .line 160
    :cond_1
    sget-object v3, Lsquareup/items/merchant/PutResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lsquareup/items/merchant/PutResponse;

    invoke-virtual {v0, v3}, Lsquareup/items/merchant/Response$Builder;->put_response(Lsquareup/items/merchant/PutResponse;)Lsquareup/items/merchant/Response$Builder;

    goto :goto_0

    .line 159
    :cond_2
    sget-object v3, Lsquareup/items/merchant/GetResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lsquareup/items/merchant/GetResponse;

    invoke-virtual {v0, v3}, Lsquareup/items/merchant/Response$Builder;->get_response(Lsquareup/items/merchant/GetResponse;)Lsquareup/items/merchant/Response$Builder;

    goto :goto_0

    .line 167
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lsquareup/items/merchant/Response$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 168
    invoke-virtual {v0}, Lsquareup/items/merchant/Response$Builder;->build()Lsquareup/items/merchant/Response;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 132
    check-cast p2, Lsquareup/items/merchant/Response;

    invoke-virtual {p0, p1, p2}, Lsquareup/items/merchant/Response$ProtoAdapter_Response;->encode(Lcom/squareup/wire/ProtoWriter;Lsquareup/items/merchant/Response;)V

    return-void
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lsquareup/items/merchant/Response;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 147
    sget-object v0, Lsquareup/items/merchant/GetResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/items/merchant/Response;->get_response:Lsquareup/items/merchant/GetResponse;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 148
    sget-object v0, Lsquareup/items/merchant/PutResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/items/merchant/Response;->put_response:Lsquareup/items/merchant/PutResponse;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 149
    sget-object v0, Lsquareup/items/merchant/DeleteResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/items/merchant/Response;->delete_response:Lsquareup/items/merchant/DeleteResponse;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 150
    invoke-virtual {p2}, Lsquareup/items/merchant/Response;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 132
    check-cast p1, Lsquareup/items/merchant/Response;

    invoke-virtual {p0, p1}, Lsquareup/items/merchant/Response$ProtoAdapter_Response;->encodedSize(Lsquareup/items/merchant/Response;)I

    move-result p1

    return p1
.end method

.method public encodedSize(Lsquareup/items/merchant/Response;)I
    .locals 4

    .line 139
    sget-object v0, Lsquareup/items/merchant/GetResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lsquareup/items/merchant/Response;->get_response:Lsquareup/items/merchant/GetResponse;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lsquareup/items/merchant/PutResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/items/merchant/Response;->put_response:Lsquareup/items/merchant/PutResponse;

    const/4 v3, 0x2

    .line 140
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lsquareup/items/merchant/DeleteResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/items/merchant/Response;->delete_response:Lsquareup/items/merchant/DeleteResponse;

    const/4 v3, 0x3

    .line 141
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 142
    invoke-virtual {p1}, Lsquareup/items/merchant/Response;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 132
    check-cast p1, Lsquareup/items/merchant/Response;

    invoke-virtual {p0, p1}, Lsquareup/items/merchant/Response$ProtoAdapter_Response;->redact(Lsquareup/items/merchant/Response;)Lsquareup/items/merchant/Response;

    move-result-object p1

    return-object p1
.end method

.method public redact(Lsquareup/items/merchant/Response;)Lsquareup/items/merchant/Response;
    .locals 2

    .line 173
    invoke-virtual {p1}, Lsquareup/items/merchant/Response;->newBuilder()Lsquareup/items/merchant/Response$Builder;

    move-result-object p1

    .line 174
    iget-object v0, p1, Lsquareup/items/merchant/Response$Builder;->get_response:Lsquareup/items/merchant/GetResponse;

    if-eqz v0, :cond_0

    sget-object v0, Lsquareup/items/merchant/GetResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lsquareup/items/merchant/Response$Builder;->get_response:Lsquareup/items/merchant/GetResponse;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsquareup/items/merchant/GetResponse;

    iput-object v0, p1, Lsquareup/items/merchant/Response$Builder;->get_response:Lsquareup/items/merchant/GetResponse;

    .line 175
    :cond_0
    iget-object v0, p1, Lsquareup/items/merchant/Response$Builder;->put_response:Lsquareup/items/merchant/PutResponse;

    if-eqz v0, :cond_1

    sget-object v0, Lsquareup/items/merchant/PutResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lsquareup/items/merchant/Response$Builder;->put_response:Lsquareup/items/merchant/PutResponse;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsquareup/items/merchant/PutResponse;

    iput-object v0, p1, Lsquareup/items/merchant/Response$Builder;->put_response:Lsquareup/items/merchant/PutResponse;

    .line 176
    :cond_1
    iget-object v0, p1, Lsquareup/items/merchant/Response$Builder;->delete_response:Lsquareup/items/merchant/DeleteResponse;

    if-eqz v0, :cond_2

    sget-object v0, Lsquareup/items/merchant/DeleteResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lsquareup/items/merchant/Response$Builder;->delete_response:Lsquareup/items/merchant/DeleteResponse;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsquareup/items/merchant/DeleteResponse;

    iput-object v0, p1, Lsquareup/items/merchant/Response$Builder;->delete_response:Lsquareup/items/merchant/DeleteResponse;

    .line 177
    :cond_2
    invoke-virtual {p1}, Lsquareup/items/merchant/Response$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 178
    invoke-virtual {p1}, Lsquareup/items/merchant/Response$Builder;->build()Lsquareup/items/merchant/Response;

    move-result-object p1

    return-object p1
.end method
