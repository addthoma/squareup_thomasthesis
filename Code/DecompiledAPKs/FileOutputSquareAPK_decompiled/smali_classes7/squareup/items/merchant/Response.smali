.class public final Lsquareup/items/merchant/Response;
.super Lcom/squareup/wire/Message;
.source "Response.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsquareup/items/merchant/Response$ProtoAdapter_Response;,
        Lsquareup/items/merchant/Response$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lsquareup/items/merchant/Response;",
        "Lsquareup/items/merchant/Response$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lsquareup/items/merchant/Response;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final delete_response:Lsquareup/items/merchant/DeleteResponse;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.items.merchant.DeleteResponse#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final get_response:Lsquareup/items/merchant/GetResponse;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.items.merchant.GetResponse#ADAPTER"
        tag = 0x1
    .end annotation
.end field

.field public final put_response:Lsquareup/items/merchant/PutResponse;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.items.merchant.PutResponse#ADAPTER"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    new-instance v0, Lsquareup/items/merchant/Response$ProtoAdapter_Response;

    invoke-direct {v0}, Lsquareup/items/merchant/Response$ProtoAdapter_Response;-><init>()V

    sput-object v0, Lsquareup/items/merchant/Response;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lsquareup/items/merchant/GetResponse;Lsquareup/items/merchant/PutResponse;Lsquareup/items/merchant/DeleteResponse;)V
    .locals 1

    .line 47
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lsquareup/items/merchant/Response;-><init>(Lsquareup/items/merchant/GetResponse;Lsquareup/items/merchant/PutResponse;Lsquareup/items/merchant/DeleteResponse;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lsquareup/items/merchant/GetResponse;Lsquareup/items/merchant/PutResponse;Lsquareup/items/merchant/DeleteResponse;Lokio/ByteString;)V
    .locals 1

    .line 52
    sget-object v0, Lsquareup/items/merchant/Response;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 53
    iput-object p1, p0, Lsquareup/items/merchant/Response;->get_response:Lsquareup/items/merchant/GetResponse;

    .line 54
    iput-object p2, p0, Lsquareup/items/merchant/Response;->put_response:Lsquareup/items/merchant/PutResponse;

    .line 55
    iput-object p3, p0, Lsquareup/items/merchant/Response;->delete_response:Lsquareup/items/merchant/DeleteResponse;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 71
    :cond_0
    instance-of v1, p1, Lsquareup/items/merchant/Response;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 72
    :cond_1
    check-cast p1, Lsquareup/items/merchant/Response;

    .line 73
    invoke-virtual {p0}, Lsquareup/items/merchant/Response;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lsquareup/items/merchant/Response;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/items/merchant/Response;->get_response:Lsquareup/items/merchant/GetResponse;

    iget-object v3, p1, Lsquareup/items/merchant/Response;->get_response:Lsquareup/items/merchant/GetResponse;

    .line 74
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/items/merchant/Response;->put_response:Lsquareup/items/merchant/PutResponse;

    iget-object v3, p1, Lsquareup/items/merchant/Response;->put_response:Lsquareup/items/merchant/PutResponse;

    .line 75
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/items/merchant/Response;->delete_response:Lsquareup/items/merchant/DeleteResponse;

    iget-object p1, p1, Lsquareup/items/merchant/Response;->delete_response:Lsquareup/items/merchant/DeleteResponse;

    .line 76
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 81
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 83
    invoke-virtual {p0}, Lsquareup/items/merchant/Response;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 84
    iget-object v1, p0, Lsquareup/items/merchant/Response;->get_response:Lsquareup/items/merchant/GetResponse;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lsquareup/items/merchant/GetResponse;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 85
    iget-object v1, p0, Lsquareup/items/merchant/Response;->put_response:Lsquareup/items/merchant/PutResponse;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lsquareup/items/merchant/PutResponse;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 86
    iget-object v1, p0, Lsquareup/items/merchant/Response;->delete_response:Lsquareup/items/merchant/DeleteResponse;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lsquareup/items/merchant/DeleteResponse;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 87
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lsquareup/items/merchant/Response;->newBuilder()Lsquareup/items/merchant/Response$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilder()Lsquareup/items/merchant/Response$Builder;
    .locals 2

    .line 60
    new-instance v0, Lsquareup/items/merchant/Response$Builder;

    invoke-direct {v0}, Lsquareup/items/merchant/Response$Builder;-><init>()V

    .line 61
    iget-object v1, p0, Lsquareup/items/merchant/Response;->get_response:Lsquareup/items/merchant/GetResponse;

    iput-object v1, v0, Lsquareup/items/merchant/Response$Builder;->get_response:Lsquareup/items/merchant/GetResponse;

    .line 62
    iget-object v1, p0, Lsquareup/items/merchant/Response;->put_response:Lsquareup/items/merchant/PutResponse;

    iput-object v1, v0, Lsquareup/items/merchant/Response$Builder;->put_response:Lsquareup/items/merchant/PutResponse;

    .line 63
    iget-object v1, p0, Lsquareup/items/merchant/Response;->delete_response:Lsquareup/items/merchant/DeleteResponse;

    iput-object v1, v0, Lsquareup/items/merchant/Response$Builder;->delete_response:Lsquareup/items/merchant/DeleteResponse;

    .line 64
    invoke-virtual {p0}, Lsquareup/items/merchant/Response;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lsquareup/items/merchant/Response$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 94
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 95
    iget-object v1, p0, Lsquareup/items/merchant/Response;->get_response:Lsquareup/items/merchant/GetResponse;

    if-eqz v1, :cond_0

    const-string v1, ", get_response="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/items/merchant/Response;->get_response:Lsquareup/items/merchant/GetResponse;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 96
    :cond_0
    iget-object v1, p0, Lsquareup/items/merchant/Response;->put_response:Lsquareup/items/merchant/PutResponse;

    if-eqz v1, :cond_1

    const-string v1, ", put_response="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/items/merchant/Response;->put_response:Lsquareup/items/merchant/PutResponse;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 97
    :cond_1
    iget-object v1, p0, Lsquareup/items/merchant/Response;->delete_response:Lsquareup/items/merchant/DeleteResponse;

    if-eqz v1, :cond_2

    const-string v1, ", delete_response="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/items/merchant/Response;->delete_response:Lsquareup/items/merchant/DeleteResponse;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Response{"

    .line 98
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
