.class final Lsquareup/items/merchant/Request$ProtoAdapter_Request;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Request.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/items/merchant/Request;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Request"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lsquareup/items/merchant/Request;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 133
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lsquareup/items/merchant/Request;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 131
    invoke-virtual {p0, p1}, Lsquareup/items/merchant/Request$ProtoAdapter_Request;->decode(Lcom/squareup/wire/ProtoReader;)Lsquareup/items/merchant/Request;

    move-result-object p1

    return-object p1
.end method

.method public decode(Lcom/squareup/wire/ProtoReader;)Lsquareup/items/merchant/Request;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 154
    new-instance v0, Lsquareup/items/merchant/Request$Builder;

    invoke-direct {v0}, Lsquareup/items/merchant/Request$Builder;-><init>()V

    .line 155
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 156
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 162
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 160
    :cond_0
    sget-object v3, Lsquareup/items/merchant/DeleteRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lsquareup/items/merchant/DeleteRequest;

    invoke-virtual {v0, v3}, Lsquareup/items/merchant/Request$Builder;->delete_request(Lsquareup/items/merchant/DeleteRequest;)Lsquareup/items/merchant/Request$Builder;

    goto :goto_0

    .line 159
    :cond_1
    sget-object v3, Lsquareup/items/merchant/PutRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lsquareup/items/merchant/PutRequest;

    invoke-virtual {v0, v3}, Lsquareup/items/merchant/Request$Builder;->put_request(Lsquareup/items/merchant/PutRequest;)Lsquareup/items/merchant/Request$Builder;

    goto :goto_0

    .line 158
    :cond_2
    sget-object v3, Lsquareup/items/merchant/GetRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lsquareup/items/merchant/GetRequest;

    invoke-virtual {v0, v3}, Lsquareup/items/merchant/Request$Builder;->get_request(Lsquareup/items/merchant/GetRequest;)Lsquareup/items/merchant/Request$Builder;

    goto :goto_0

    .line 166
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lsquareup/items/merchant/Request$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 167
    invoke-virtual {v0}, Lsquareup/items/merchant/Request$Builder;->build()Lsquareup/items/merchant/Request;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 131
    check-cast p2, Lsquareup/items/merchant/Request;

    invoke-virtual {p0, p1, p2}, Lsquareup/items/merchant/Request$ProtoAdapter_Request;->encode(Lcom/squareup/wire/ProtoWriter;Lsquareup/items/merchant/Request;)V

    return-void
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lsquareup/items/merchant/Request;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 146
    sget-object v0, Lsquareup/items/merchant/GetRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/items/merchant/Request;->get_request:Lsquareup/items/merchant/GetRequest;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 147
    sget-object v0, Lsquareup/items/merchant/PutRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/items/merchant/Request;->put_request:Lsquareup/items/merchant/PutRequest;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 148
    sget-object v0, Lsquareup/items/merchant/DeleteRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/items/merchant/Request;->delete_request:Lsquareup/items/merchant/DeleteRequest;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 149
    invoke-virtual {p2}, Lsquareup/items/merchant/Request;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 131
    check-cast p1, Lsquareup/items/merchant/Request;

    invoke-virtual {p0, p1}, Lsquareup/items/merchant/Request$ProtoAdapter_Request;->encodedSize(Lsquareup/items/merchant/Request;)I

    move-result p1

    return p1
.end method

.method public encodedSize(Lsquareup/items/merchant/Request;)I
    .locals 4

    .line 138
    sget-object v0, Lsquareup/items/merchant/GetRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lsquareup/items/merchant/Request;->get_request:Lsquareup/items/merchant/GetRequest;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lsquareup/items/merchant/PutRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/items/merchant/Request;->put_request:Lsquareup/items/merchant/PutRequest;

    const/4 v3, 0x2

    .line 139
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lsquareup/items/merchant/DeleteRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/items/merchant/Request;->delete_request:Lsquareup/items/merchant/DeleteRequest;

    const/4 v3, 0x3

    .line 140
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 141
    invoke-virtual {p1}, Lsquareup/items/merchant/Request;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 131
    check-cast p1, Lsquareup/items/merchant/Request;

    invoke-virtual {p0, p1}, Lsquareup/items/merchant/Request$ProtoAdapter_Request;->redact(Lsquareup/items/merchant/Request;)Lsquareup/items/merchant/Request;

    move-result-object p1

    return-object p1
.end method

.method public redact(Lsquareup/items/merchant/Request;)Lsquareup/items/merchant/Request;
    .locals 2

    .line 172
    invoke-virtual {p1}, Lsquareup/items/merchant/Request;->newBuilder()Lsquareup/items/merchant/Request$Builder;

    move-result-object p1

    .line 173
    iget-object v0, p1, Lsquareup/items/merchant/Request$Builder;->get_request:Lsquareup/items/merchant/GetRequest;

    if-eqz v0, :cond_0

    sget-object v0, Lsquareup/items/merchant/GetRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lsquareup/items/merchant/Request$Builder;->get_request:Lsquareup/items/merchant/GetRequest;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsquareup/items/merchant/GetRequest;

    iput-object v0, p1, Lsquareup/items/merchant/Request$Builder;->get_request:Lsquareup/items/merchant/GetRequest;

    .line 174
    :cond_0
    iget-object v0, p1, Lsquareup/items/merchant/Request$Builder;->put_request:Lsquareup/items/merchant/PutRequest;

    if-eqz v0, :cond_1

    sget-object v0, Lsquareup/items/merchant/PutRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lsquareup/items/merchant/Request$Builder;->put_request:Lsquareup/items/merchant/PutRequest;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsquareup/items/merchant/PutRequest;

    iput-object v0, p1, Lsquareup/items/merchant/Request$Builder;->put_request:Lsquareup/items/merchant/PutRequest;

    .line 175
    :cond_1
    iget-object v0, p1, Lsquareup/items/merchant/Request$Builder;->delete_request:Lsquareup/items/merchant/DeleteRequest;

    if-eqz v0, :cond_2

    sget-object v0, Lsquareup/items/merchant/DeleteRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lsquareup/items/merchant/Request$Builder;->delete_request:Lsquareup/items/merchant/DeleteRequest;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsquareup/items/merchant/DeleteRequest;

    iput-object v0, p1, Lsquareup/items/merchant/Request$Builder;->delete_request:Lsquareup/items/merchant/DeleteRequest;

    .line 176
    :cond_2
    invoke-virtual {p1}, Lsquareup/items/merchant/Request$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 177
    invoke-virtual {p1}, Lsquareup/items/merchant/Request$Builder;->build()Lsquareup/items/merchant/Request;

    move-result-object p1

    return-object p1
.end method
