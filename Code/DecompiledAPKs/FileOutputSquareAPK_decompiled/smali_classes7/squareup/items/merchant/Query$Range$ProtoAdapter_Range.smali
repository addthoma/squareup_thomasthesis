.class final Lsquareup/items/merchant/Query$Range$ProtoAdapter_Range;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Query.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/items/merchant/Query$Range;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Range"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lsquareup/items/merchant/Query$Range;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 517
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lsquareup/items/merchant/Query$Range;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 515
    invoke-virtual {p0, p1}, Lsquareup/items/merchant/Query$Range$ProtoAdapter_Range;->decode(Lcom/squareup/wire/ProtoReader;)Lsquareup/items/merchant/Query$Range;

    move-result-object p1

    return-object p1
.end method

.method public decode(Lcom/squareup/wire/ProtoReader;)Lsquareup/items/merchant/Query$Range;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 536
    new-instance v0, Lsquareup/items/merchant/Query$Range$Builder;

    invoke-direct {v0}, Lsquareup/items/merchant/Query$Range$Builder;-><init>()V

    .line 537
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 538
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 543
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 541
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v0, v3}, Lsquareup/items/merchant/Query$Range$Builder;->end(Ljava/lang/Long;)Lsquareup/items/merchant/Query$Range$Builder;

    goto :goto_0

    .line 540
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v0, v3}, Lsquareup/items/merchant/Query$Range$Builder;->begin(Ljava/lang/Long;)Lsquareup/items/merchant/Query$Range$Builder;

    goto :goto_0

    .line 547
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lsquareup/items/merchant/Query$Range$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 548
    invoke-virtual {v0}, Lsquareup/items/merchant/Query$Range$Builder;->build()Lsquareup/items/merchant/Query$Range;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 515
    check-cast p2, Lsquareup/items/merchant/Query$Range;

    invoke-virtual {p0, p1, p2}, Lsquareup/items/merchant/Query$Range$ProtoAdapter_Range;->encode(Lcom/squareup/wire/ProtoWriter;Lsquareup/items/merchant/Query$Range;)V

    return-void
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lsquareup/items/merchant/Query$Range;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 529
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/items/merchant/Query$Range;->begin:Ljava/lang/Long;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 530
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/items/merchant/Query$Range;->end:Ljava/lang/Long;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 531
    invoke-virtual {p2}, Lsquareup/items/merchant/Query$Range;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 515
    check-cast p1, Lsquareup/items/merchant/Query$Range;

    invoke-virtual {p0, p1}, Lsquareup/items/merchant/Query$Range$ProtoAdapter_Range;->encodedSize(Lsquareup/items/merchant/Query$Range;)I

    move-result p1

    return p1
.end method

.method public encodedSize(Lsquareup/items/merchant/Query$Range;)I
    .locals 4

    .line 522
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lsquareup/items/merchant/Query$Range;->begin:Ljava/lang/Long;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/items/merchant/Query$Range;->end:Ljava/lang/Long;

    const/4 v3, 0x2

    .line 523
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 524
    invoke-virtual {p1}, Lsquareup/items/merchant/Query$Range;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 515
    check-cast p1, Lsquareup/items/merchant/Query$Range;

    invoke-virtual {p0, p1}, Lsquareup/items/merchant/Query$Range$ProtoAdapter_Range;->redact(Lsquareup/items/merchant/Query$Range;)Lsquareup/items/merchant/Query$Range;

    move-result-object p1

    return-object p1
.end method

.method public redact(Lsquareup/items/merchant/Query$Range;)Lsquareup/items/merchant/Query$Range;
    .locals 0

    .line 553
    invoke-virtual {p1}, Lsquareup/items/merchant/Query$Range;->newBuilder()Lsquareup/items/merchant/Query$Range$Builder;

    move-result-object p1

    .line 554
    invoke-virtual {p1}, Lsquareup/items/merchant/Query$Range$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 555
    invoke-virtual {p1}, Lsquareup/items/merchant/Query$Range$Builder;->build()Lsquareup/items/merchant/Query$Range;

    move-result-object p1

    return-object p1
.end method
