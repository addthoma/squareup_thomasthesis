.class public final Lsquareup/items/merchant/AttributeDefinition$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "AttributeDefinition.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/items/merchant/AttributeDefinition;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lsquareup/items/merchant/AttributeDefinition;",
        "Lsquareup/items/merchant/AttributeDefinition$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public attribute_class:Lsquareup/items/merchant/AttributeDefinition$AttributeClass;

.field public display_name:Ljava/lang/String;

.field public legacy_scope:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public scope:Lsquareup/items/merchant/AttributeDefinition$Scope;

.field public searchable:Ljava/lang/Boolean;

.field public token:Ljava/lang/String;

.field public type:Lsquareup/items/merchant/AttributeDefinition$Type;

.field public visibility:Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 208
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public attribute_class(Lsquareup/items/merchant/AttributeDefinition$AttributeClass;)Lsquareup/items/merchant/AttributeDefinition$Builder;
    .locals 0

    .line 250
    iput-object p1, p0, Lsquareup/items/merchant/AttributeDefinition$Builder;->attribute_class:Lsquareup/items/merchant/AttributeDefinition$AttributeClass;

    return-object p0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 189
    invoke-virtual {p0}, Lsquareup/items/merchant/AttributeDefinition$Builder;->build()Lsquareup/items/merchant/AttributeDefinition;

    move-result-object v0

    return-object v0
.end method

.method public build()Lsquareup/items/merchant/AttributeDefinition;
    .locals 12

    .line 261
    new-instance v11, Lsquareup/items/merchant/AttributeDefinition;

    iget-object v1, p0, Lsquareup/items/merchant/AttributeDefinition$Builder;->token:Ljava/lang/String;

    iget-object v2, p0, Lsquareup/items/merchant/AttributeDefinition$Builder;->name:Ljava/lang/String;

    iget-object v3, p0, Lsquareup/items/merchant/AttributeDefinition$Builder;->type:Lsquareup/items/merchant/AttributeDefinition$Type;

    iget-object v4, p0, Lsquareup/items/merchant/AttributeDefinition$Builder;->legacy_scope:Ljava/lang/String;

    iget-object v5, p0, Lsquareup/items/merchant/AttributeDefinition$Builder;->scope:Lsquareup/items/merchant/AttributeDefinition$Scope;

    iget-object v6, p0, Lsquareup/items/merchant/AttributeDefinition$Builder;->searchable:Ljava/lang/Boolean;

    iget-object v7, p0, Lsquareup/items/merchant/AttributeDefinition$Builder;->display_name:Ljava/lang/String;

    iget-object v8, p0, Lsquareup/items/merchant/AttributeDefinition$Builder;->attribute_class:Lsquareup/items/merchant/AttributeDefinition$AttributeClass;

    iget-object v9, p0, Lsquareup/items/merchant/AttributeDefinition$Builder;->visibility:Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v10

    move-object v0, v11

    invoke-direct/range {v0 .. v10}, Lsquareup/items/merchant/AttributeDefinition;-><init>(Ljava/lang/String;Ljava/lang/String;Lsquareup/items/merchant/AttributeDefinition$Type;Ljava/lang/String;Lsquareup/items/merchant/AttributeDefinition$Scope;Ljava/lang/Boolean;Ljava/lang/String;Lsquareup/items/merchant/AttributeDefinition$AttributeClass;Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;Lokio/ByteString;)V

    return-object v11
.end method

.method public display_name(Ljava/lang/String;)Lsquareup/items/merchant/AttributeDefinition$Builder;
    .locals 0

    .line 245
    iput-object p1, p0, Lsquareup/items/merchant/AttributeDefinition$Builder;->display_name:Ljava/lang/String;

    return-object p0
.end method

.method public legacy_scope(Ljava/lang/String;)Lsquareup/items/merchant/AttributeDefinition$Builder;
    .locals 0

    .line 230
    iput-object p1, p0, Lsquareup/items/merchant/AttributeDefinition$Builder;->legacy_scope:Ljava/lang/String;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lsquareup/items/merchant/AttributeDefinition$Builder;
    .locals 0

    .line 217
    iput-object p1, p0, Lsquareup/items/merchant/AttributeDefinition$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public scope(Lsquareup/items/merchant/AttributeDefinition$Scope;)Lsquareup/items/merchant/AttributeDefinition$Builder;
    .locals 0

    .line 235
    iput-object p1, p0, Lsquareup/items/merchant/AttributeDefinition$Builder;->scope:Lsquareup/items/merchant/AttributeDefinition$Scope;

    return-object p0
.end method

.method public searchable(Ljava/lang/Boolean;)Lsquareup/items/merchant/AttributeDefinition$Builder;
    .locals 0

    .line 240
    iput-object p1, p0, Lsquareup/items/merchant/AttributeDefinition$Builder;->searchable:Ljava/lang/Boolean;

    return-object p0
.end method

.method public token(Ljava/lang/String;)Lsquareup/items/merchant/AttributeDefinition$Builder;
    .locals 0

    .line 212
    iput-object p1, p0, Lsquareup/items/merchant/AttributeDefinition$Builder;->token:Ljava/lang/String;

    return-object p0
.end method

.method public type(Lsquareup/items/merchant/AttributeDefinition$Type;)Lsquareup/items/merchant/AttributeDefinition$Builder;
    .locals 0

    .line 222
    iput-object p1, p0, Lsquareup/items/merchant/AttributeDefinition$Builder;->type:Lsquareup/items/merchant/AttributeDefinition$Type;

    return-object p0
.end method

.method public visibility(Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;)Lsquareup/items/merchant/AttributeDefinition$Builder;
    .locals 0

    .line 255
    iput-object p1, p0, Lsquareup/items/merchant/AttributeDefinition$Builder;->visibility:Lsquareup/items/merchant/AttributeDefinition$AttributeVisibility;

    return-object p0
.end method
