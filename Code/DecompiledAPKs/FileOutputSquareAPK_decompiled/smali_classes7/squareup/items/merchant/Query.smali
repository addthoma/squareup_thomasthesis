.class public final Lsquareup/items/merchant/Query;
.super Lcom/squareup/wire/Message;
.source "Query.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsquareup/items/merchant/Query$ProtoAdapter_Query;,
        Lsquareup/items/merchant/Query$NeighborType;,
        Lsquareup/items/merchant/Query$AttributeValues;,
        Lsquareup/items/merchant/Query$SortedAttribute;,
        Lsquareup/items/merchant/Query$Range;,
        Lsquareup/items/merchant/Query$Type;,
        Lsquareup/items/merchant/Query$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lsquareup/items/merchant/Query;",
        "Lsquareup/items/merchant/Query$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lsquareup/items/merchant/Query;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_KEY:Ljava/lang/String; = ""

.field public static final DEFAULT_TEXT_TERM:Ljava/lang/String; = ""

.field public static final DEFAULT_TYPE:Lsquareup/items/merchant/Query$Type;

.field private static final serialVersionUID:J


# instance fields
.field public final attribute_values:Lsquareup/items/merchant/Query$AttributeValues;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.items.merchant.Query$AttributeValues#ADAPTER"
        tag = 0x9
    .end annotation
.end field

.field public final cogs_id:Lsquareup/items/merchant/CogsId;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.items.merchant.CogsId#ADAPTER"
        tag = 0x5
    .end annotation
.end field

.field public final key:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final neighbor_types:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.items.merchant.Query$NeighborType#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x6
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lsquareup/items/merchant/Query$NeighborType;",
            ">;"
        }
    .end annotation
.end field

.field public final range:Lsquareup/items/merchant/Query$Range;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.items.merchant.Query$Range#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final set_query_values:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x8
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final sorted_attribute:Lsquareup/items/merchant/Query$SortedAttribute;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.items.merchant.Query$SortedAttribute#ADAPTER"
        tag = 0x7
    .end annotation
.end field

.field public final text_term:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final type:Lsquareup/items/merchant/Query$Type;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.items.merchant.Query$Type#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 24
    new-instance v0, Lsquareup/items/merchant/Query$ProtoAdapter_Query;

    invoke-direct {v0}, Lsquareup/items/merchant/Query$ProtoAdapter_Query;-><init>()V

    sput-object v0, Lsquareup/items/merchant/Query;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 28
    sget-object v0, Lsquareup/items/merchant/Query$Type;->DO_NOT_USE:Lsquareup/items/merchant/Query$Type;

    sput-object v0, Lsquareup/items/merchant/Query;->DEFAULT_TYPE:Lsquareup/items/merchant/Query$Type;

    return-void
.end method

.method public constructor <init>(Lsquareup/items/merchant/Query$Type;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Lsquareup/items/merchant/Query$Range;Lsquareup/items/merchant/CogsId;Lsquareup/items/merchant/Query$SortedAttribute;Lsquareup/items/merchant/Query$AttributeValues;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lsquareup/items/merchant/Query$Type;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lsquareup/items/merchant/Query$NeighborType;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Lsquareup/items/merchant/Query$Range;",
            "Lsquareup/items/merchant/CogsId;",
            "Lsquareup/items/merchant/Query$SortedAttribute;",
            "Lsquareup/items/merchant/Query$AttributeValues;",
            ")V"
        }
    .end annotation

    .line 110
    sget-object v10, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    invoke-direct/range {v0 .. v10}, Lsquareup/items/merchant/Query;-><init>(Lsquareup/items/merchant/Query$Type;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Lsquareup/items/merchant/Query$Range;Lsquareup/items/merchant/CogsId;Lsquareup/items/merchant/Query$SortedAttribute;Lsquareup/items/merchant/Query$AttributeValues;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lsquareup/items/merchant/Query$Type;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Lsquareup/items/merchant/Query$Range;Lsquareup/items/merchant/CogsId;Lsquareup/items/merchant/Query$SortedAttribute;Lsquareup/items/merchant/Query$AttributeValues;Lokio/ByteString;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lsquareup/items/merchant/Query$Type;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lsquareup/items/merchant/Query$NeighborType;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Lsquareup/items/merchant/Query$Range;",
            "Lsquareup/items/merchant/CogsId;",
            "Lsquareup/items/merchant/Query$SortedAttribute;",
            "Lsquareup/items/merchant/Query$AttributeValues;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 117
    sget-object v0, Lsquareup/items/merchant/Query;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p10}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    const/4 p10, 0x1

    new-array v0, p10, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p9, v0, v1

    .line 118
    invoke-static {p5, p6, p7, p8, v0}, Lcom/squareup/wire/internal/Internal;->countNonNull(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)I

    move-result v0

    if-gt v0, p10, :cond_0

    .line 121
    iput-object p1, p0, Lsquareup/items/merchant/Query;->type:Lsquareup/items/merchant/Query$Type;

    .line 122
    iput-object p2, p0, Lsquareup/items/merchant/Query;->key:Ljava/lang/String;

    const-string p1, "neighbor_types"

    .line 123
    invoke-static {p1, p3}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lsquareup/items/merchant/Query;->neighbor_types:Ljava/util/List;

    const-string p1, "set_query_values"

    .line 124
    invoke-static {p1, p4}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lsquareup/items/merchant/Query;->set_query_values:Ljava/util/List;

    .line 125
    iput-object p5, p0, Lsquareup/items/merchant/Query;->text_term:Ljava/lang/String;

    .line 126
    iput-object p6, p0, Lsquareup/items/merchant/Query;->range:Lsquareup/items/merchant/Query$Range;

    .line 127
    iput-object p7, p0, Lsquareup/items/merchant/Query;->cogs_id:Lsquareup/items/merchant/CogsId;

    .line 128
    iput-object p8, p0, Lsquareup/items/merchant/Query;->sorted_attribute:Lsquareup/items/merchant/Query$SortedAttribute;

    .line 129
    iput-object p9, p0, Lsquareup/items/merchant/Query;->attribute_values:Lsquareup/items/merchant/Query$AttributeValues;

    return-void

    .line 119
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "at most one of text_term, range, cogs_id, sorted_attribute, attribute_values may be non-null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 151
    :cond_0
    instance-of v1, p1, Lsquareup/items/merchant/Query;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 152
    :cond_1
    check-cast p1, Lsquareup/items/merchant/Query;

    .line 153
    invoke-virtual {p0}, Lsquareup/items/merchant/Query;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lsquareup/items/merchant/Query;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/items/merchant/Query;->type:Lsquareup/items/merchant/Query$Type;

    iget-object v3, p1, Lsquareup/items/merchant/Query;->type:Lsquareup/items/merchant/Query$Type;

    .line 154
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/items/merchant/Query;->key:Ljava/lang/String;

    iget-object v3, p1, Lsquareup/items/merchant/Query;->key:Ljava/lang/String;

    .line 155
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/items/merchant/Query;->neighbor_types:Ljava/util/List;

    iget-object v3, p1, Lsquareup/items/merchant/Query;->neighbor_types:Ljava/util/List;

    .line 156
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/items/merchant/Query;->set_query_values:Ljava/util/List;

    iget-object v3, p1, Lsquareup/items/merchant/Query;->set_query_values:Ljava/util/List;

    .line 157
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/items/merchant/Query;->text_term:Ljava/lang/String;

    iget-object v3, p1, Lsquareup/items/merchant/Query;->text_term:Ljava/lang/String;

    .line 158
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/items/merchant/Query;->range:Lsquareup/items/merchant/Query$Range;

    iget-object v3, p1, Lsquareup/items/merchant/Query;->range:Lsquareup/items/merchant/Query$Range;

    .line 159
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/items/merchant/Query;->cogs_id:Lsquareup/items/merchant/CogsId;

    iget-object v3, p1, Lsquareup/items/merchant/Query;->cogs_id:Lsquareup/items/merchant/CogsId;

    .line 160
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/items/merchant/Query;->sorted_attribute:Lsquareup/items/merchant/Query$SortedAttribute;

    iget-object v3, p1, Lsquareup/items/merchant/Query;->sorted_attribute:Lsquareup/items/merchant/Query$SortedAttribute;

    .line 161
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/items/merchant/Query;->attribute_values:Lsquareup/items/merchant/Query$AttributeValues;

    iget-object p1, p1, Lsquareup/items/merchant/Query;->attribute_values:Lsquareup/items/merchant/Query$AttributeValues;

    .line 162
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 167
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_7

    .line 169
    invoke-virtual {p0}, Lsquareup/items/merchant/Query;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 170
    iget-object v1, p0, Lsquareup/items/merchant/Query;->type:Lsquareup/items/merchant/Query$Type;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lsquareup/items/merchant/Query$Type;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 171
    iget-object v1, p0, Lsquareup/items/merchant/Query;->key:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 172
    iget-object v1, p0, Lsquareup/items/merchant/Query;->neighbor_types:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 173
    iget-object v1, p0, Lsquareup/items/merchant/Query;->set_query_values:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 174
    iget-object v1, p0, Lsquareup/items/merchant/Query;->text_term:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 175
    iget-object v1, p0, Lsquareup/items/merchant/Query;->range:Lsquareup/items/merchant/Query$Range;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lsquareup/items/merchant/Query$Range;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 176
    iget-object v1, p0, Lsquareup/items/merchant/Query;->cogs_id:Lsquareup/items/merchant/CogsId;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lsquareup/items/merchant/CogsId;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 177
    iget-object v1, p0, Lsquareup/items/merchant/Query;->sorted_attribute:Lsquareup/items/merchant/Query$SortedAttribute;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lsquareup/items/merchant/Query$SortedAttribute;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 178
    iget-object v1, p0, Lsquareup/items/merchant/Query;->attribute_values:Lsquareup/items/merchant/Query$AttributeValues;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lsquareup/items/merchant/Query$AttributeValues;->hashCode()I

    move-result v2

    :cond_6
    add-int/2addr v0, v2

    .line 179
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_7
    return v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 23
    invoke-virtual {p0}, Lsquareup/items/merchant/Query;->newBuilder()Lsquareup/items/merchant/Query$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilder()Lsquareup/items/merchant/Query$Builder;
    .locals 2

    .line 134
    new-instance v0, Lsquareup/items/merchant/Query$Builder;

    invoke-direct {v0}, Lsquareup/items/merchant/Query$Builder;-><init>()V

    .line 135
    iget-object v1, p0, Lsquareup/items/merchant/Query;->type:Lsquareup/items/merchant/Query$Type;

    iput-object v1, v0, Lsquareup/items/merchant/Query$Builder;->type:Lsquareup/items/merchant/Query$Type;

    .line 136
    iget-object v1, p0, Lsquareup/items/merchant/Query;->key:Ljava/lang/String;

    iput-object v1, v0, Lsquareup/items/merchant/Query$Builder;->key:Ljava/lang/String;

    .line 137
    iget-object v1, p0, Lsquareup/items/merchant/Query;->neighbor_types:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lsquareup/items/merchant/Query$Builder;->neighbor_types:Ljava/util/List;

    .line 138
    iget-object v1, p0, Lsquareup/items/merchant/Query;->set_query_values:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lsquareup/items/merchant/Query$Builder;->set_query_values:Ljava/util/List;

    .line 139
    iget-object v1, p0, Lsquareup/items/merchant/Query;->text_term:Ljava/lang/String;

    iput-object v1, v0, Lsquareup/items/merchant/Query$Builder;->text_term:Ljava/lang/String;

    .line 140
    iget-object v1, p0, Lsquareup/items/merchant/Query;->range:Lsquareup/items/merchant/Query$Range;

    iput-object v1, v0, Lsquareup/items/merchant/Query$Builder;->range:Lsquareup/items/merchant/Query$Range;

    .line 141
    iget-object v1, p0, Lsquareup/items/merchant/Query;->cogs_id:Lsquareup/items/merchant/CogsId;

    iput-object v1, v0, Lsquareup/items/merchant/Query$Builder;->cogs_id:Lsquareup/items/merchant/CogsId;

    .line 142
    iget-object v1, p0, Lsquareup/items/merchant/Query;->sorted_attribute:Lsquareup/items/merchant/Query$SortedAttribute;

    iput-object v1, v0, Lsquareup/items/merchant/Query$Builder;->sorted_attribute:Lsquareup/items/merchant/Query$SortedAttribute;

    .line 143
    iget-object v1, p0, Lsquareup/items/merchant/Query;->attribute_values:Lsquareup/items/merchant/Query$AttributeValues;

    iput-object v1, v0, Lsquareup/items/merchant/Query$Builder;->attribute_values:Lsquareup/items/merchant/Query$AttributeValues;

    .line 144
    invoke-virtual {p0}, Lsquareup/items/merchant/Query;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lsquareup/items/merchant/Query$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 186
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 187
    iget-object v1, p0, Lsquareup/items/merchant/Query;->type:Lsquareup/items/merchant/Query$Type;

    if-eqz v1, :cond_0

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/items/merchant/Query;->type:Lsquareup/items/merchant/Query$Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 188
    :cond_0
    iget-object v1, p0, Lsquareup/items/merchant/Query;->key:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", key="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/items/merchant/Query;->key:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 189
    :cond_1
    iget-object v1, p0, Lsquareup/items/merchant/Query;->neighbor_types:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, ", neighbor_types="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/items/merchant/Query;->neighbor_types:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 190
    :cond_2
    iget-object v1, p0, Lsquareup/items/merchant/Query;->set_query_values:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, ", set_query_values="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/items/merchant/Query;->set_query_values:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 191
    :cond_3
    iget-object v1, p0, Lsquareup/items/merchant/Query;->text_term:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", text_term="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/items/merchant/Query;->text_term:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 192
    :cond_4
    iget-object v1, p0, Lsquareup/items/merchant/Query;->range:Lsquareup/items/merchant/Query$Range;

    if-eqz v1, :cond_5

    const-string v1, ", range="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/items/merchant/Query;->range:Lsquareup/items/merchant/Query$Range;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 193
    :cond_5
    iget-object v1, p0, Lsquareup/items/merchant/Query;->cogs_id:Lsquareup/items/merchant/CogsId;

    if-eqz v1, :cond_6

    const-string v1, ", cogs_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/items/merchant/Query;->cogs_id:Lsquareup/items/merchant/CogsId;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 194
    :cond_6
    iget-object v1, p0, Lsquareup/items/merchant/Query;->sorted_attribute:Lsquareup/items/merchant/Query$SortedAttribute;

    if-eqz v1, :cond_7

    const-string v1, ", sorted_attribute="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/items/merchant/Query;->sorted_attribute:Lsquareup/items/merchant/Query$SortedAttribute;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 195
    :cond_7
    iget-object v1, p0, Lsquareup/items/merchant/Query;->attribute_values:Lsquareup/items/merchant/Query$AttributeValues;

    if-eqz v1, :cond_8

    const-string v1, ", attribute_values="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/items/merchant/Query;->attribute_values:Lsquareup/items/merchant/Query$AttributeValues;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_8
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Query{"

    .line 196
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
