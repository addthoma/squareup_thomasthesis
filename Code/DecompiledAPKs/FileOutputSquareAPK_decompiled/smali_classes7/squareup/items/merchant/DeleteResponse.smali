.class public final Lsquareup/items/merchant/DeleteResponse;
.super Lcom/squareup/wire/Message;
.source "DeleteResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsquareup/items/merchant/DeleteResponse$ProtoAdapter_DeleteResponse;,
        Lsquareup/items/merchant/DeleteResponse$DeletedObject;,
        Lsquareup/items/merchant/DeleteResponse$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lsquareup/items/merchant/DeleteResponse;",
        "Lsquareup/items/merchant/DeleteResponse$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lsquareup/items/merchant/DeleteResponse;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_COUNT:Ljava/lang/Long;

.field public static final DEFAULT_LOCK_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_MODIFICATION_TIMESTAMP:Ljava/lang/Long;

.field private static final serialVersionUID:J


# instance fields
.field public final count:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x1
    .end annotation
.end field

.field public final deleted_objects:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.items.merchant.DeleteResponse$DeletedObject#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x3
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lsquareup/items/merchant/DeleteResponse$DeletedObject;",
            ">;"
        }
    .end annotation
.end field

.field public final lock_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final modification_timestamp:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 22
    new-instance v0, Lsquareup/items/merchant/DeleteResponse$ProtoAdapter_DeleteResponse;

    invoke-direct {v0}, Lsquareup/items/merchant/DeleteResponse$ProtoAdapter_DeleteResponse;-><init>()V

    sput-object v0, Lsquareup/items/merchant/DeleteResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-wide/16 v0, 0x0

    .line 26
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lsquareup/items/merchant/DeleteResponse;->DEFAULT_COUNT:Ljava/lang/Long;

    .line 28
    sput-object v0, Lsquareup/items/merchant/DeleteResponse;->DEFAULT_MODIFICATION_TIMESTAMP:Ljava/lang/Long;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Ljava/lang/Long;Ljava/util/List;Ljava/lang/String;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            "Ljava/util/List<",
            "Lsquareup/items/merchant/DeleteResponse$DeletedObject;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 72
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lsquareup/items/merchant/DeleteResponse;-><init>(Ljava/lang/Long;Ljava/lang/Long;Ljava/util/List;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Ljava/lang/Long;Ljava/util/List;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            "Ljava/util/List<",
            "Lsquareup/items/merchant/DeleteResponse$DeletedObject;",
            ">;",
            "Ljava/lang/String;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 77
    sget-object v0, Lsquareup/items/merchant/DeleteResponse;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 78
    iput-object p1, p0, Lsquareup/items/merchant/DeleteResponse;->count:Ljava/lang/Long;

    .line 79
    iput-object p2, p0, Lsquareup/items/merchant/DeleteResponse;->modification_timestamp:Ljava/lang/Long;

    const-string p1, "deleted_objects"

    .line 80
    invoke-static {p1, p3}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lsquareup/items/merchant/DeleteResponse;->deleted_objects:Ljava/util/List;

    .line 81
    iput-object p4, p0, Lsquareup/items/merchant/DeleteResponse;->lock_token:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 98
    :cond_0
    instance-of v1, p1, Lsquareup/items/merchant/DeleteResponse;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 99
    :cond_1
    check-cast p1, Lsquareup/items/merchant/DeleteResponse;

    .line 100
    invoke-virtual {p0}, Lsquareup/items/merchant/DeleteResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lsquareup/items/merchant/DeleteResponse;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/items/merchant/DeleteResponse;->count:Ljava/lang/Long;

    iget-object v3, p1, Lsquareup/items/merchant/DeleteResponse;->count:Ljava/lang/Long;

    .line 101
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/items/merchant/DeleteResponse;->modification_timestamp:Ljava/lang/Long;

    iget-object v3, p1, Lsquareup/items/merchant/DeleteResponse;->modification_timestamp:Ljava/lang/Long;

    .line 102
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/items/merchant/DeleteResponse;->deleted_objects:Ljava/util/List;

    iget-object v3, p1, Lsquareup/items/merchant/DeleteResponse;->deleted_objects:Ljava/util/List;

    .line 103
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/items/merchant/DeleteResponse;->lock_token:Ljava/lang/String;

    iget-object p1, p1, Lsquareup/items/merchant/DeleteResponse;->lock_token:Ljava/lang/String;

    .line 104
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 109
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 111
    invoke-virtual {p0}, Lsquareup/items/merchant/DeleteResponse;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 112
    iget-object v1, p0, Lsquareup/items/merchant/DeleteResponse;->count:Ljava/lang/Long;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 113
    iget-object v1, p0, Lsquareup/items/merchant/DeleteResponse;->modification_timestamp:Ljava/lang/Long;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 114
    iget-object v1, p0, Lsquareup/items/merchant/DeleteResponse;->deleted_objects:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 115
    iget-object v1, p0, Lsquareup/items/merchant/DeleteResponse;->lock_token:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 116
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 21
    invoke-virtual {p0}, Lsquareup/items/merchant/DeleteResponse;->newBuilder()Lsquareup/items/merchant/DeleteResponse$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilder()Lsquareup/items/merchant/DeleteResponse$Builder;
    .locals 2

    .line 86
    new-instance v0, Lsquareup/items/merchant/DeleteResponse$Builder;

    invoke-direct {v0}, Lsquareup/items/merchant/DeleteResponse$Builder;-><init>()V

    .line 87
    iget-object v1, p0, Lsquareup/items/merchant/DeleteResponse;->count:Ljava/lang/Long;

    iput-object v1, v0, Lsquareup/items/merchant/DeleteResponse$Builder;->count:Ljava/lang/Long;

    .line 88
    iget-object v1, p0, Lsquareup/items/merchant/DeleteResponse;->modification_timestamp:Ljava/lang/Long;

    iput-object v1, v0, Lsquareup/items/merchant/DeleteResponse$Builder;->modification_timestamp:Ljava/lang/Long;

    .line 89
    iget-object v1, p0, Lsquareup/items/merchant/DeleteResponse;->deleted_objects:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lsquareup/items/merchant/DeleteResponse$Builder;->deleted_objects:Ljava/util/List;

    .line 90
    iget-object v1, p0, Lsquareup/items/merchant/DeleteResponse;->lock_token:Ljava/lang/String;

    iput-object v1, v0, Lsquareup/items/merchant/DeleteResponse$Builder;->lock_token:Ljava/lang/String;

    .line 91
    invoke-virtual {p0}, Lsquareup/items/merchant/DeleteResponse;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lsquareup/items/merchant/DeleteResponse$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 123
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 124
    iget-object v1, p0, Lsquareup/items/merchant/DeleteResponse;->count:Ljava/lang/Long;

    if-eqz v1, :cond_0

    const-string v1, ", count="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/items/merchant/DeleteResponse;->count:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 125
    :cond_0
    iget-object v1, p0, Lsquareup/items/merchant/DeleteResponse;->modification_timestamp:Ljava/lang/Long;

    if-eqz v1, :cond_1

    const-string v1, ", modification_timestamp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/items/merchant/DeleteResponse;->modification_timestamp:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 126
    :cond_1
    iget-object v1, p0, Lsquareup/items/merchant/DeleteResponse;->deleted_objects:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, ", deleted_objects="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/items/merchant/DeleteResponse;->deleted_objects:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 127
    :cond_2
    iget-object v1, p0, Lsquareup/items/merchant/DeleteResponse;->lock_token:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", lock_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/items/merchant/DeleteResponse;->lock_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "DeleteResponse{"

    .line 128
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
