.class final Lsquareup/spe/K450Manifest$ProtoAdapter_K450Manifest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "K450Manifest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/spe/K450Manifest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_K450Manifest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lsquareup/spe/K450Manifest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 373
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lsquareup/spe/K450Manifest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 371
    invoke-virtual {p0, p1}, Lsquareup/spe/K450Manifest$ProtoAdapter_K450Manifest;->decode(Lcom/squareup/wire/ProtoReader;)Lsquareup/spe/K450Manifest;

    move-result-object p1

    return-object p1
.end method

.method public decode(Lcom/squareup/wire/ProtoReader;)Lsquareup/spe/K450Manifest;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 418
    new-instance v0, Lsquareup/spe/K450Manifest$Builder;

    invoke-direct {v0}, Lsquareup/spe/K450Manifest$Builder;-><init>()V

    .line 419
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 420
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 473
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 471
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lokio/ByteString;

    invoke-virtual {v0, v3}, Lsquareup/spe/K450Manifest$Builder;->chip_revision(Lokio/ByteString;)Lsquareup/spe/K450Manifest$Builder;

    goto :goto_0

    .line 465
    :pswitch_1
    :try_start_0
    sget-object v4, Lsquareup/spe/RAKSelect;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lsquareup/spe/RAKSelect;

    invoke-virtual {v0, v4}, Lsquareup/spe/K450Manifest$Builder;->rak_select(Lsquareup/spe/RAKSelect;)Lsquareup/spe/K450Manifest$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 467
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lsquareup/spe/K450Manifest$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 462
    :pswitch_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lokio/ByteString;

    invoke-virtual {v0, v3}, Lsquareup/spe/K450Manifest$Builder;->reader_authority_fingerprint(Lokio/ByteString;)Lsquareup/spe/K450Manifest$Builder;

    goto :goto_0

    .line 456
    :pswitch_3
    :try_start_1
    sget-object v4, Lsquareup/spe/UnitConfiguration;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lsquareup/spe/UnitConfiguration;

    invoke-virtual {v0, v4}, Lsquareup/spe/K450Manifest$Builder;->configuration(Lsquareup/spe/UnitConfiguration;)Lsquareup/spe/K450Manifest$Builder;
    :try_end_1
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v4

    .line 458
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lsquareup/spe/K450Manifest$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 453
    :pswitch_4
    sget-object v3, Lsquareup/spe/AssetManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lsquareup/spe/AssetManifest;

    invoke-virtual {v0, v3}, Lsquareup/spe/K450Manifest$Builder;->tms_capk_b(Lsquareup/spe/AssetManifest;)Lsquareup/spe/K450Manifest$Builder;

    goto :goto_0

    .line 452
    :pswitch_5
    sget-object v3, Lsquareup/spe/AssetManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lsquareup/spe/AssetManifest;

    invoke-virtual {v0, v3}, Lsquareup/spe/K450Manifest$Builder;->tms_capk_a(Lsquareup/spe/AssetManifest;)Lsquareup/spe/K450Manifest$Builder;

    goto :goto_0

    .line 451
    :pswitch_6
    sget-object v3, Lsquareup/spe/AssetManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lsquareup/spe/AssetManifest;

    invoke-virtual {v0, v3}, Lsquareup/spe/K450Manifest$Builder;->cpu1_firmware_b(Lsquareup/spe/AssetManifest;)Lsquareup/spe/K450Manifest$Builder;

    goto :goto_0

    .line 450
    :pswitch_7
    sget-object v3, Lsquareup/spe/AssetManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lsquareup/spe/AssetManifest;

    invoke-virtual {v0, v3}, Lsquareup/spe/K450Manifest$Builder;->cpu1_firmware_a(Lsquareup/spe/AssetManifest;)Lsquareup/spe/K450Manifest$Builder;

    goto/16 :goto_0

    .line 449
    :pswitch_8
    sget-object v3, Lsquareup/spe/AssetManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lsquareup/spe/AssetManifest;

    invoke-virtual {v0, v3}, Lsquareup/spe/K450Manifest$Builder;->cpu0_firmware_b(Lsquareup/spe/AssetManifest;)Lsquareup/spe/K450Manifest$Builder;

    goto/16 :goto_0

    .line 448
    :pswitch_9
    sget-object v3, Lsquareup/spe/AssetManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lsquareup/spe/AssetManifest;

    invoke-virtual {v0, v3}, Lsquareup/spe/K450Manifest$Builder;->cpu0_firmware_a(Lsquareup/spe/AssetManifest;)Lsquareup/spe/K450Manifest$Builder;

    goto/16 :goto_0

    .line 447
    :pswitch_a
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lokio/ByteString;

    invoke-virtual {v0, v3}, Lsquareup/spe/K450Manifest$Builder;->signer_fingerprint(Lokio/ByteString;)Lsquareup/spe/K450Manifest$Builder;

    goto/16 :goto_0

    .line 446
    :pswitch_b
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lokio/ByteString;

    invoke-virtual {v0, v3}, Lsquareup/spe/K450Manifest$Builder;->chipid(Lokio/ByteString;)Lsquareup/spe/K450Manifest$Builder;

    goto/16 :goto_0

    .line 440
    :pswitch_c
    :try_start_2
    sget-object v4, Lsquareup/spe/AssetTypeV2;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lsquareup/spe/AssetTypeV2;

    invoke-virtual {v0, v4}, Lsquareup/spe/K450Manifest$Builder;->tms_capk_running_slot(Lsquareup/spe/AssetTypeV2;)Lsquareup/spe/K450Manifest$Builder;
    :try_end_2
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_0

    :catch_2
    move-exception v4

    .line 442
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lsquareup/spe/K450Manifest$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto/16 :goto_0

    .line 432
    :pswitch_d
    :try_start_3
    sget-object v4, Lsquareup/spe/AssetTypeV2;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lsquareup/spe/AssetTypeV2;

    invoke-virtual {v0, v4}, Lsquareup/spe/K450Manifest$Builder;->cpu1_running_slot(Lsquareup/spe/AssetTypeV2;)Lsquareup/spe/K450Manifest$Builder;
    :try_end_3
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_3 .. :try_end_3} :catch_3

    goto/16 :goto_0

    :catch_3
    move-exception v4

    .line 434
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lsquareup/spe/K450Manifest$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto/16 :goto_0

    .line 424
    :pswitch_e
    :try_start_4
    sget-object v4, Lsquareup/spe/AssetTypeV2;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lsquareup/spe/AssetTypeV2;

    invoke-virtual {v0, v4}, Lsquareup/spe/K450Manifest$Builder;->cpu0_running_slot(Lsquareup/spe/AssetTypeV2;)Lsquareup/spe/K450Manifest$Builder;
    :try_end_4
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_4 .. :try_end_4} :catch_4

    goto/16 :goto_0

    :catch_4
    move-exception v4

    .line 426
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lsquareup/spe/K450Manifest$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto/16 :goto_0

    .line 477
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lsquareup/spe/K450Manifest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 478
    invoke-virtual {v0}, Lsquareup/spe/K450Manifest$Builder;->build()Lsquareup/spe/K450Manifest;

    move-result-object p1

    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 371
    check-cast p2, Lsquareup/spe/K450Manifest;

    invoke-virtual {p0, p1, p2}, Lsquareup/spe/K450Manifest$ProtoAdapter_K450Manifest;->encode(Lcom/squareup/wire/ProtoWriter;Lsquareup/spe/K450Manifest;)V

    return-void
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lsquareup/spe/K450Manifest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 398
    sget-object v0, Lsquareup/spe/AssetTypeV2;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/spe/K450Manifest;->cpu0_running_slot:Lsquareup/spe/AssetTypeV2;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 399
    sget-object v0, Lsquareup/spe/AssetTypeV2;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/spe/K450Manifest;->cpu1_running_slot:Lsquareup/spe/AssetTypeV2;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 400
    sget-object v0, Lsquareup/spe/AssetTypeV2;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/spe/K450Manifest;->tms_capk_running_slot:Lsquareup/spe/AssetTypeV2;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 401
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/spe/K450Manifest;->chipid:Lokio/ByteString;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 402
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/spe/K450Manifest;->signer_fingerprint:Lokio/ByteString;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 403
    sget-object v0, Lsquareup/spe/AssetManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/spe/K450Manifest;->cpu0_firmware_a:Lsquareup/spe/AssetManifest;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 404
    sget-object v0, Lsquareup/spe/AssetManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/spe/K450Manifest;->cpu0_firmware_b:Lsquareup/spe/AssetManifest;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 405
    sget-object v0, Lsquareup/spe/AssetManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/spe/K450Manifest;->cpu1_firmware_a:Lsquareup/spe/AssetManifest;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 406
    sget-object v0, Lsquareup/spe/AssetManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/spe/K450Manifest;->cpu1_firmware_b:Lsquareup/spe/AssetManifest;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 407
    sget-object v0, Lsquareup/spe/AssetManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/spe/K450Manifest;->tms_capk_a:Lsquareup/spe/AssetManifest;

    const/16 v2, 0xa

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 408
    sget-object v0, Lsquareup/spe/AssetManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/spe/K450Manifest;->tms_capk_b:Lsquareup/spe/AssetManifest;

    const/16 v2, 0xb

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 409
    sget-object v0, Lsquareup/spe/UnitConfiguration;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/spe/K450Manifest;->configuration:Lsquareup/spe/UnitConfiguration;

    const/16 v2, 0xc

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 410
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/spe/K450Manifest;->reader_authority_fingerprint:Lokio/ByteString;

    const/16 v2, 0xd

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 411
    sget-object v0, Lsquareup/spe/RAKSelect;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/spe/K450Manifest;->rak_select:Lsquareup/spe/RAKSelect;

    const/16 v2, 0xe

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 412
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/spe/K450Manifest;->chip_revision:Lokio/ByteString;

    const/16 v2, 0xf

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 413
    invoke-virtual {p2}, Lsquareup/spe/K450Manifest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 371
    check-cast p1, Lsquareup/spe/K450Manifest;

    invoke-virtual {p0, p1}, Lsquareup/spe/K450Manifest$ProtoAdapter_K450Manifest;->encodedSize(Lsquareup/spe/K450Manifest;)I

    move-result p1

    return p1
.end method

.method public encodedSize(Lsquareup/spe/K450Manifest;)I
    .locals 4

    .line 378
    sget-object v0, Lsquareup/spe/AssetTypeV2;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lsquareup/spe/K450Manifest;->cpu0_running_slot:Lsquareup/spe/AssetTypeV2;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lsquareup/spe/AssetTypeV2;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/spe/K450Manifest;->cpu1_running_slot:Lsquareup/spe/AssetTypeV2;

    const/4 v3, 0x2

    .line 379
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lsquareup/spe/AssetTypeV2;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/spe/K450Manifest;->tms_capk_running_slot:Lsquareup/spe/AssetTypeV2;

    const/4 v3, 0x3

    .line 380
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/spe/K450Manifest;->chipid:Lokio/ByteString;

    const/4 v3, 0x4

    .line 381
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/spe/K450Manifest;->signer_fingerprint:Lokio/ByteString;

    const/4 v3, 0x5

    .line 382
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lsquareup/spe/AssetManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/spe/K450Manifest;->cpu0_firmware_a:Lsquareup/spe/AssetManifest;

    const/4 v3, 0x6

    .line 383
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lsquareup/spe/AssetManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/spe/K450Manifest;->cpu0_firmware_b:Lsquareup/spe/AssetManifest;

    const/4 v3, 0x7

    .line 384
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lsquareup/spe/AssetManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/spe/K450Manifest;->cpu1_firmware_a:Lsquareup/spe/AssetManifest;

    const/16 v3, 0x8

    .line 385
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lsquareup/spe/AssetManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/spe/K450Manifest;->cpu1_firmware_b:Lsquareup/spe/AssetManifest;

    const/16 v3, 0x9

    .line 386
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lsquareup/spe/AssetManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/spe/K450Manifest;->tms_capk_a:Lsquareup/spe/AssetManifest;

    const/16 v3, 0xa

    .line 387
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lsquareup/spe/AssetManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/spe/K450Manifest;->tms_capk_b:Lsquareup/spe/AssetManifest;

    const/16 v3, 0xb

    .line 388
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lsquareup/spe/UnitConfiguration;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/spe/K450Manifest;->configuration:Lsquareup/spe/UnitConfiguration;

    const/16 v3, 0xc

    .line 389
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/spe/K450Manifest;->reader_authority_fingerprint:Lokio/ByteString;

    const/16 v3, 0xd

    .line 390
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lsquareup/spe/RAKSelect;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/spe/K450Manifest;->rak_select:Lsquareup/spe/RAKSelect;

    const/16 v3, 0xe

    .line 391
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/spe/K450Manifest;->chip_revision:Lokio/ByteString;

    const/16 v3, 0xf

    .line 392
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 393
    invoke-virtual {p1}, Lsquareup/spe/K450Manifest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 371
    check-cast p1, Lsquareup/spe/K450Manifest;

    invoke-virtual {p0, p1}, Lsquareup/spe/K450Manifest$ProtoAdapter_K450Manifest;->redact(Lsquareup/spe/K450Manifest;)Lsquareup/spe/K450Manifest;

    move-result-object p1

    return-object p1
.end method

.method public redact(Lsquareup/spe/K450Manifest;)Lsquareup/spe/K450Manifest;
    .locals 2

    .line 483
    invoke-virtual {p1}, Lsquareup/spe/K450Manifest;->newBuilder()Lsquareup/spe/K450Manifest$Builder;

    move-result-object p1

    .line 484
    iget-object v0, p1, Lsquareup/spe/K450Manifest$Builder;->cpu0_firmware_a:Lsquareup/spe/AssetManifest;

    if-eqz v0, :cond_0

    sget-object v0, Lsquareup/spe/AssetManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lsquareup/spe/K450Manifest$Builder;->cpu0_firmware_a:Lsquareup/spe/AssetManifest;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsquareup/spe/AssetManifest;

    iput-object v0, p1, Lsquareup/spe/K450Manifest$Builder;->cpu0_firmware_a:Lsquareup/spe/AssetManifest;

    .line 485
    :cond_0
    iget-object v0, p1, Lsquareup/spe/K450Manifest$Builder;->cpu0_firmware_b:Lsquareup/spe/AssetManifest;

    if-eqz v0, :cond_1

    sget-object v0, Lsquareup/spe/AssetManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lsquareup/spe/K450Manifest$Builder;->cpu0_firmware_b:Lsquareup/spe/AssetManifest;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsquareup/spe/AssetManifest;

    iput-object v0, p1, Lsquareup/spe/K450Manifest$Builder;->cpu0_firmware_b:Lsquareup/spe/AssetManifest;

    .line 486
    :cond_1
    iget-object v0, p1, Lsquareup/spe/K450Manifest$Builder;->cpu1_firmware_a:Lsquareup/spe/AssetManifest;

    if-eqz v0, :cond_2

    sget-object v0, Lsquareup/spe/AssetManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lsquareup/spe/K450Manifest$Builder;->cpu1_firmware_a:Lsquareup/spe/AssetManifest;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsquareup/spe/AssetManifest;

    iput-object v0, p1, Lsquareup/spe/K450Manifest$Builder;->cpu1_firmware_a:Lsquareup/spe/AssetManifest;

    .line 487
    :cond_2
    iget-object v0, p1, Lsquareup/spe/K450Manifest$Builder;->cpu1_firmware_b:Lsquareup/spe/AssetManifest;

    if-eqz v0, :cond_3

    sget-object v0, Lsquareup/spe/AssetManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lsquareup/spe/K450Manifest$Builder;->cpu1_firmware_b:Lsquareup/spe/AssetManifest;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsquareup/spe/AssetManifest;

    iput-object v0, p1, Lsquareup/spe/K450Manifest$Builder;->cpu1_firmware_b:Lsquareup/spe/AssetManifest;

    .line 488
    :cond_3
    iget-object v0, p1, Lsquareup/spe/K450Manifest$Builder;->tms_capk_a:Lsquareup/spe/AssetManifest;

    if-eqz v0, :cond_4

    sget-object v0, Lsquareup/spe/AssetManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lsquareup/spe/K450Manifest$Builder;->tms_capk_a:Lsquareup/spe/AssetManifest;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsquareup/spe/AssetManifest;

    iput-object v0, p1, Lsquareup/spe/K450Manifest$Builder;->tms_capk_a:Lsquareup/spe/AssetManifest;

    .line 489
    :cond_4
    iget-object v0, p1, Lsquareup/spe/K450Manifest$Builder;->tms_capk_b:Lsquareup/spe/AssetManifest;

    if-eqz v0, :cond_5

    sget-object v0, Lsquareup/spe/AssetManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lsquareup/spe/K450Manifest$Builder;->tms_capk_b:Lsquareup/spe/AssetManifest;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsquareup/spe/AssetManifest;

    iput-object v0, p1, Lsquareup/spe/K450Manifest$Builder;->tms_capk_b:Lsquareup/spe/AssetManifest;

    .line 490
    :cond_5
    invoke-virtual {p1}, Lsquareup/spe/K450Manifest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 491
    invoke-virtual {p1}, Lsquareup/spe/K450Manifest$Builder;->build()Lsquareup/spe/K450Manifest;

    move-result-object p1

    return-object p1
.end method
