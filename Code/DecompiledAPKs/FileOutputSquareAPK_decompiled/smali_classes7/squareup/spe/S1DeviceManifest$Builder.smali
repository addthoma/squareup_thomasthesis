.class public final Lsquareup/spe/S1DeviceManifest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "S1DeviceManifest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/spe/S1DeviceManifest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lsquareup/spe/S1DeviceManifest;",
        "Lsquareup/spe/S1DeviceManifest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public comms_protocol_version:Lsquareup/spe/CommsProtocolVersion;

.field public max_compression_version:Ljava/lang/Integer;

.field public pts_version:Ljava/lang/Integer;

.field public stm32f2_manifest:Lsquareup/spe/STM32F2Manifest;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 139
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 130
    invoke-virtual {p0}, Lsquareup/spe/S1DeviceManifest$Builder;->build()Lsquareup/spe/S1DeviceManifest;

    move-result-object v0

    return-object v0
.end method

.method public build()Lsquareup/spe/S1DeviceManifest;
    .locals 7

    .line 176
    new-instance v6, Lsquareup/spe/S1DeviceManifest;

    iget-object v1, p0, Lsquareup/spe/S1DeviceManifest$Builder;->comms_protocol_version:Lsquareup/spe/CommsProtocolVersion;

    iget-object v2, p0, Lsquareup/spe/S1DeviceManifest$Builder;->stm32f2_manifest:Lsquareup/spe/STM32F2Manifest;

    iget-object v3, p0, Lsquareup/spe/S1DeviceManifest$Builder;->max_compression_version:Ljava/lang/Integer;

    iget-object v4, p0, Lsquareup/spe/S1DeviceManifest$Builder;->pts_version:Ljava/lang/Integer;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lsquareup/spe/S1DeviceManifest;-><init>(Lsquareup/spe/CommsProtocolVersion;Lsquareup/spe/STM32F2Manifest;Ljava/lang/Integer;Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v6
.end method

.method public comms_protocol_version(Lsquareup/spe/CommsProtocolVersion;)Lsquareup/spe/S1DeviceManifest$Builder;
    .locals 0

    .line 146
    iput-object p1, p0, Lsquareup/spe/S1DeviceManifest$Builder;->comms_protocol_version:Lsquareup/spe/CommsProtocolVersion;

    return-object p0
.end method

.method public max_compression_version(Ljava/lang/Integer;)Lsquareup/spe/S1DeviceManifest$Builder;
    .locals 0

    .line 162
    iput-object p1, p0, Lsquareup/spe/S1DeviceManifest$Builder;->max_compression_version:Ljava/lang/Integer;

    return-object p0
.end method

.method public pts_version(Ljava/lang/Integer;)Lsquareup/spe/S1DeviceManifest$Builder;
    .locals 0

    .line 170
    iput-object p1, p0, Lsquareup/spe/S1DeviceManifest$Builder;->pts_version:Ljava/lang/Integer;

    return-object p0
.end method

.method public stm32f2_manifest(Lsquareup/spe/STM32F2Manifest;)Lsquareup/spe/S1DeviceManifest$Builder;
    .locals 0

    .line 154
    iput-object p1, p0, Lsquareup/spe/S1DeviceManifest$Builder;->stm32f2_manifest:Lsquareup/spe/STM32F2Manifest;

    return-object p0
.end method
