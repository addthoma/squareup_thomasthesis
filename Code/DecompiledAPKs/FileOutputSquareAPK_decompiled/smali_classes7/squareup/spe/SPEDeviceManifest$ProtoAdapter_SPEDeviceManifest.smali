.class final Lsquareup/spe/SPEDeviceManifest$ProtoAdapter_SPEDeviceManifest;
.super Lcom/squareup/wire/ProtoAdapter;
.source "SPEDeviceManifest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/spe/SPEDeviceManifest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_SPEDeviceManifest"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lsquareup/spe/SPEDeviceManifest;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 205
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lsquareup/spe/SPEDeviceManifest;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 203
    invoke-virtual {p0, p1}, Lsquareup/spe/SPEDeviceManifest$ProtoAdapter_SPEDeviceManifest;->decode(Lcom/squareup/wire/ProtoReader;)Lsquareup/spe/SPEDeviceManifest;

    move-result-object p1

    return-object p1
.end method

.method public decode(Lcom/squareup/wire/ProtoReader;)Lsquareup/spe/SPEDeviceManifest;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 230
    new-instance v0, Lsquareup/spe/SPEDeviceManifest$Builder;

    invoke-direct {v0}, Lsquareup/spe/SPEDeviceManifest$Builder;-><init>()V

    .line 231
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 232
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 240
    :pswitch_0
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 238
    :pswitch_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lsquareup/spe/SPEDeviceManifest$Builder;->mlb_serial_number(Ljava/lang/String;)Lsquareup/spe/SPEDeviceManifest$Builder;

    goto :goto_0

    .line 237
    :pswitch_2
    sget-object v3, Lsquareup/spe/R12CManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lsquareup/spe/R12CManifest;

    invoke-virtual {v0, v3}, Lsquareup/spe/SPEDeviceManifest$Builder;->r12c_manifest(Lsquareup/spe/R12CManifest;)Lsquareup/spe/SPEDeviceManifest$Builder;

    goto :goto_0

    .line 236
    :pswitch_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lsquareup/spe/SPEDeviceManifest$Builder;->serial_number(Ljava/lang/String;)Lsquareup/spe/SPEDeviceManifest$Builder;

    goto :goto_0

    .line 235
    :pswitch_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lokio/ByteString;

    invoke-virtual {v0, v3}, Lsquareup/spe/SPEDeviceManifest$Builder;->hwid(Lokio/ByteString;)Lsquareup/spe/SPEDeviceManifest$Builder;

    goto :goto_0

    .line 234
    :pswitch_5
    sget-object v3, Lsquareup/spe/S1DeviceManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lsquareup/spe/S1DeviceManifest;

    invoke-virtual {v0, v3}, Lsquareup/spe/SPEDeviceManifest$Builder;->s1_manifest(Lsquareup/spe/S1DeviceManifest;)Lsquareup/spe/SPEDeviceManifest$Builder;

    goto :goto_0

    .line 244
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lsquareup/spe/SPEDeviceManifest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 245
    invoke-virtual {v0}, Lsquareup/spe/SPEDeviceManifest$Builder;->build()Lsquareup/spe/SPEDeviceManifest;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x12b
        :pswitch_5
        :pswitch_0
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 203
    check-cast p2, Lsquareup/spe/SPEDeviceManifest;

    invoke-virtual {p0, p1, p2}, Lsquareup/spe/SPEDeviceManifest$ProtoAdapter_SPEDeviceManifest;->encode(Lcom/squareup/wire/ProtoWriter;Lsquareup/spe/SPEDeviceManifest;)V

    return-void
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lsquareup/spe/SPEDeviceManifest;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 220
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/spe/SPEDeviceManifest;->hwid:Lokio/ByteString;

    const/16 v2, 0x12d

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 221
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/spe/SPEDeviceManifest;->serial_number:Ljava/lang/String;

    const/16 v2, 0x12e

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 222
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/spe/SPEDeviceManifest;->mlb_serial_number:Ljava/lang/String;

    const/16 v2, 0x130

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 223
    sget-object v0, Lsquareup/spe/R12CManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/spe/SPEDeviceManifest;->r12c_manifest:Lsquareup/spe/R12CManifest;

    const/16 v2, 0x12f

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 224
    sget-object v0, Lsquareup/spe/S1DeviceManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/spe/SPEDeviceManifest;->s1_manifest:Lsquareup/spe/S1DeviceManifest;

    const/16 v2, 0x12b

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 225
    invoke-virtual {p2}, Lsquareup/spe/SPEDeviceManifest;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 203
    check-cast p1, Lsquareup/spe/SPEDeviceManifest;

    invoke-virtual {p0, p1}, Lsquareup/spe/SPEDeviceManifest$ProtoAdapter_SPEDeviceManifest;->encodedSize(Lsquareup/spe/SPEDeviceManifest;)I

    move-result p1

    return p1
.end method

.method public encodedSize(Lsquareup/spe/SPEDeviceManifest;)I
    .locals 4

    .line 210
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BYTES:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lsquareup/spe/SPEDeviceManifest;->hwid:Lokio/ByteString;

    const/16 v2, 0x12d

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/spe/SPEDeviceManifest;->serial_number:Ljava/lang/String;

    const/16 v3, 0x12e

    .line 211
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/spe/SPEDeviceManifest;->mlb_serial_number:Ljava/lang/String;

    const/16 v3, 0x130

    .line 212
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lsquareup/spe/R12CManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/spe/SPEDeviceManifest;->r12c_manifest:Lsquareup/spe/R12CManifest;

    const/16 v3, 0x12f

    .line 213
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lsquareup/spe/S1DeviceManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/spe/SPEDeviceManifest;->s1_manifest:Lsquareup/spe/S1DeviceManifest;

    const/16 v3, 0x12b

    .line 214
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 215
    invoke-virtual {p1}, Lsquareup/spe/SPEDeviceManifest;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 203
    check-cast p1, Lsquareup/spe/SPEDeviceManifest;

    invoke-virtual {p0, p1}, Lsquareup/spe/SPEDeviceManifest$ProtoAdapter_SPEDeviceManifest;->redact(Lsquareup/spe/SPEDeviceManifest;)Lsquareup/spe/SPEDeviceManifest;

    move-result-object p1

    return-object p1
.end method

.method public redact(Lsquareup/spe/SPEDeviceManifest;)Lsquareup/spe/SPEDeviceManifest;
    .locals 2

    .line 250
    invoke-virtual {p1}, Lsquareup/spe/SPEDeviceManifest;->newBuilder()Lsquareup/spe/SPEDeviceManifest$Builder;

    move-result-object p1

    .line 251
    iget-object v0, p1, Lsquareup/spe/SPEDeviceManifest$Builder;->r12c_manifest:Lsquareup/spe/R12CManifest;

    if-eqz v0, :cond_0

    sget-object v0, Lsquareup/spe/R12CManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lsquareup/spe/SPEDeviceManifest$Builder;->r12c_manifest:Lsquareup/spe/R12CManifest;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsquareup/spe/R12CManifest;

    iput-object v0, p1, Lsquareup/spe/SPEDeviceManifest$Builder;->r12c_manifest:Lsquareup/spe/R12CManifest;

    .line 252
    :cond_0
    iget-object v0, p1, Lsquareup/spe/SPEDeviceManifest$Builder;->s1_manifest:Lsquareup/spe/S1DeviceManifest;

    if-eqz v0, :cond_1

    sget-object v0, Lsquareup/spe/S1DeviceManifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lsquareup/spe/SPEDeviceManifest$Builder;->s1_manifest:Lsquareup/spe/S1DeviceManifest;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lsquareup/spe/S1DeviceManifest;

    iput-object v0, p1, Lsquareup/spe/SPEDeviceManifest$Builder;->s1_manifest:Lsquareup/spe/S1DeviceManifest;

    .line 253
    :cond_1
    invoke-virtual {p1}, Lsquareup/spe/SPEDeviceManifest$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 254
    invoke-virtual {p1}, Lsquareup/spe/SPEDeviceManifest$Builder;->build()Lsquareup/spe/SPEDeviceManifest;

    move-result-object p1

    return-object p1
.end method
