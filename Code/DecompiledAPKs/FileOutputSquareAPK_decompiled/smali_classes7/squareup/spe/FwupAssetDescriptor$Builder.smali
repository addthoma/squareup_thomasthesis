.class public final Lsquareup/spe/FwupAssetDescriptor$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FwupAssetDescriptor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/spe/FwupAssetDescriptor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lsquareup/spe/FwupAssetDescriptor;",
        "Lsquareup/spe/FwupAssetDescriptor$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public country_code:Ljava/lang/String;

.field public filename:Ljava/lang/String;

.field public version:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 117
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 110
    invoke-virtual {p0}, Lsquareup/spe/FwupAssetDescriptor$Builder;->build()Lsquareup/spe/FwupAssetDescriptor;

    move-result-object v0

    return-object v0
.end method

.method public build()Lsquareup/spe/FwupAssetDescriptor;
    .locals 5

    .line 143
    new-instance v0, Lsquareup/spe/FwupAssetDescriptor;

    iget-object v1, p0, Lsquareup/spe/FwupAssetDescriptor$Builder;->filename:Ljava/lang/String;

    iget-object v2, p0, Lsquareup/spe/FwupAssetDescriptor$Builder;->version:Ljava/lang/Integer;

    iget-object v3, p0, Lsquareup/spe/FwupAssetDescriptor$Builder;->country_code:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lsquareup/spe/FwupAssetDescriptor;-><init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Lokio/ByteString;)V

    return-object v0
.end method

.method public country_code(Ljava/lang/String;)Lsquareup/spe/FwupAssetDescriptor$Builder;
    .locals 0

    .line 137
    iput-object p1, p0, Lsquareup/spe/FwupAssetDescriptor$Builder;->country_code:Ljava/lang/String;

    return-object p0
.end method

.method public filename(Ljava/lang/String;)Lsquareup/spe/FwupAssetDescriptor$Builder;
    .locals 0

    .line 121
    iput-object p1, p0, Lsquareup/spe/FwupAssetDescriptor$Builder;->filename:Ljava/lang/String;

    return-object p0
.end method

.method public version(Ljava/lang/Integer;)Lsquareup/spe/FwupAssetDescriptor$Builder;
    .locals 0

    .line 129
    iput-object p1, p0, Lsquareup/spe/FwupAssetDescriptor$Builder;->version:Ljava/lang/Integer;

    return-object p0
.end method
