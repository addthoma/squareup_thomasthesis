.class public final Lsquareup/spe/CRQGT4Manifest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CRQGT4Manifest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/spe/CRQGT4Manifest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lsquareup/spe/CRQGT4Manifest;",
        "Lsquareup/spe/CRQGT4Manifest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public firmware:Lsquareup/spe/AssetManifestV2;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 77
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 74
    invoke-virtual {p0}, Lsquareup/spe/CRQGT4Manifest$Builder;->build()Lsquareup/spe/CRQGT4Manifest;

    move-result-object v0

    return-object v0
.end method

.method public build()Lsquareup/spe/CRQGT4Manifest;
    .locals 3

    .line 87
    new-instance v0, Lsquareup/spe/CRQGT4Manifest;

    iget-object v1, p0, Lsquareup/spe/CRQGT4Manifest$Builder;->firmware:Lsquareup/spe/AssetManifestV2;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lsquareup/spe/CRQGT4Manifest;-><init>(Lsquareup/spe/AssetManifestV2;Lokio/ByteString;)V

    return-object v0
.end method

.method public firmware(Lsquareup/spe/AssetManifestV2;)Lsquareup/spe/CRQGT4Manifest$Builder;
    .locals 0

    .line 81
    iput-object p1, p0, Lsquareup/spe/CRQGT4Manifest$Builder;->firmware:Lsquareup/spe/AssetManifestV2;

    return-object p0
.end method
