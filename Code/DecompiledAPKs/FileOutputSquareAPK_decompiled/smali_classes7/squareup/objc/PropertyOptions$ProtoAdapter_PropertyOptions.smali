.class final Lsquareup/objc/PropertyOptions$ProtoAdapter_PropertyOptions;
.super Lcom/squareup/wire/ProtoAdapter;
.source "PropertyOptions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/objc/PropertyOptions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_PropertyOptions"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lsquareup/objc/PropertyOptions;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 134
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lsquareup/objc/PropertyOptions;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 132
    invoke-virtual {p0, p1}, Lsquareup/objc/PropertyOptions$ProtoAdapter_PropertyOptions;->decode(Lcom/squareup/wire/ProtoReader;)Lsquareup/objc/PropertyOptions;

    move-result-object p1

    return-object p1
.end method

.method public decode(Lcom/squareup/wire/ProtoReader;)Lsquareup/objc/PropertyOptions;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 153
    new-instance v0, Lsquareup/objc/PropertyOptions$Builder;

    invoke-direct {v0}, Lsquareup/objc/PropertyOptions$Builder;-><init>()V

    .line 154
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 155
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 160
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 158
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lsquareup/objc/PropertyOptions$Builder;->as_primitive(Ljava/lang/Boolean;)Lsquareup/objc/PropertyOptions$Builder;

    goto :goto_0

    .line 157
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lsquareup/objc/PropertyOptions$Builder;->name(Ljava/lang/String;)Lsquareup/objc/PropertyOptions$Builder;

    goto :goto_0

    .line 164
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lsquareup/objc/PropertyOptions$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 165
    invoke-virtual {v0}, Lsquareup/objc/PropertyOptions$Builder;->build()Lsquareup/objc/PropertyOptions;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 132
    check-cast p2, Lsquareup/objc/PropertyOptions;

    invoke-virtual {p0, p1, p2}, Lsquareup/objc/PropertyOptions$ProtoAdapter_PropertyOptions;->encode(Lcom/squareup/wire/ProtoWriter;Lsquareup/objc/PropertyOptions;)V

    return-void
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lsquareup/objc/PropertyOptions;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 146
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/objc/PropertyOptions;->name:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 147
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lsquareup/objc/PropertyOptions;->as_primitive:Ljava/lang/Boolean;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 148
    invoke-virtual {p2}, Lsquareup/objc/PropertyOptions;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 132
    check-cast p1, Lsquareup/objc/PropertyOptions;

    invoke-virtual {p0, p1}, Lsquareup/objc/PropertyOptions$ProtoAdapter_PropertyOptions;->encodedSize(Lsquareup/objc/PropertyOptions;)I

    move-result p1

    return p1
.end method

.method public encodedSize(Lsquareup/objc/PropertyOptions;)I
    .locals 4

    .line 139
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lsquareup/objc/PropertyOptions;->name:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lsquareup/objc/PropertyOptions;->as_primitive:Ljava/lang/Boolean;

    const/4 v3, 0x2

    .line 140
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 141
    invoke-virtual {p1}, Lsquareup/objc/PropertyOptions;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 132
    check-cast p1, Lsquareup/objc/PropertyOptions;

    invoke-virtual {p0, p1}, Lsquareup/objc/PropertyOptions$ProtoAdapter_PropertyOptions;->redact(Lsquareup/objc/PropertyOptions;)Lsquareup/objc/PropertyOptions;

    move-result-object p1

    return-object p1
.end method

.method public redact(Lsquareup/objc/PropertyOptions;)Lsquareup/objc/PropertyOptions;
    .locals 0

    .line 170
    invoke-virtual {p1}, Lsquareup/objc/PropertyOptions;->newBuilder()Lsquareup/objc/PropertyOptions$Builder;

    move-result-object p1

    .line 171
    invoke-virtual {p1}, Lsquareup/objc/PropertyOptions$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 172
    invoke-virtual {p1}, Lsquareup/objc/PropertyOptions$Builder;->build()Lsquareup/objc/PropertyOptions;

    move-result-object p1

    return-object p1
.end method
