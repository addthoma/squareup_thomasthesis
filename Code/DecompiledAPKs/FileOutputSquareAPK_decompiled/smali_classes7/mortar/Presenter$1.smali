.class Lmortar/Presenter$1;
.super Ljava/lang/Object;
.source "Presenter.java"

# interfaces
.implements Lmortar/bundler/Bundler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmortar/Presenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lmortar/Presenter;


# direct methods
.method constructor <init>(Lmortar/Presenter;)V
    .locals 0

    .line 28
    iput-object p1, p0, Lmortar/Presenter$1;->this$0:Lmortar/Presenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getMortarBundleKey()Ljava/lang/String;
    .locals 1

    .line 30
    iget-object v0, p0, Lmortar/Presenter$1;->this$0:Lmortar/Presenter;

    invoke-virtual {v0}, Lmortar/Presenter;->getMortarBundleKey()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    .line 45
    iget-object v0, p0, Lmortar/Presenter$1;->this$0:Lmortar/Presenter;

    invoke-virtual {v0, p1}, Lmortar/Presenter;->onEnterScope(Lmortar/MortarScope;)V

    return-void
.end method

.method public onExitScope()V
    .locals 1

    .line 49
    iget-object v0, p0, Lmortar/Presenter$1;->this$0:Lmortar/Presenter;

    invoke-virtual {v0}, Lmortar/Presenter;->onExitScope()V

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 2

    .line 34
    iget-object v0, p0, Lmortar/Presenter$1;->this$0:Lmortar/Presenter;

    invoke-virtual {v0}, Lmortar/Presenter;->hasView()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmortar/Presenter$1;->this$0:Lmortar/Presenter;

    invoke-static {v0}, Lmortar/Presenter;->access$000(Lmortar/Presenter;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 35
    iget-object v0, p0, Lmortar/Presenter$1;->this$0:Lmortar/Presenter;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lmortar/Presenter;->access$002(Lmortar/Presenter;Z)Z

    .line 36
    iget-object v0, p0, Lmortar/Presenter$1;->this$0:Lmortar/Presenter;

    invoke-virtual {v0, p1}, Lmortar/Presenter;->onLoad(Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method public onSave(Landroid/os/Bundle;)V
    .locals 1

    .line 41
    iget-object v0, p0, Lmortar/Presenter$1;->this$0:Lmortar/Presenter;

    invoke-virtual {v0, p1}, Lmortar/Presenter;->onSave(Landroid/os/Bundle;)V

    return-void
.end method
