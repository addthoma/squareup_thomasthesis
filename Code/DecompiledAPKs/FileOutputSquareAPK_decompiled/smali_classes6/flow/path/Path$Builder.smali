.class public final Lflow/path/Path$Builder;
.super Ljava/lang/Object;
.source "Path.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lflow/path/Path;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private final elements:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lflow/path/Path;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 2

    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lflow/path/Path$Builder;->elements:Ljava/util/List;

    .line 71
    iget-object v0, p0, Lflow/path/Path$Builder;->elements:Ljava/util/List;

    sget-object v1, Lflow/path/Path;->ROOT:Lflow/path/Path;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method static synthetic access$000(Lflow/path/Path$Builder;Lflow/path/Path;)Z
    .locals 0

    .line 67
    invoke-direct {p0, p1}, Lflow/path/Path$Builder;->isNotTail(Lflow/path/Path;)Z

    move-result p0

    return p0
.end method

.method static synthetic access$100(Lflow/path/Path$Builder;)Ljava/util/List;
    .locals 0

    .line 67
    iget-object p0, p0, Lflow/path/Path$Builder;->elements:Ljava/util/List;

    return-object p0
.end method

.method private isNotTail(Lflow/path/Path;)Z
    .locals 2

    .line 79
    iget-object v0, p0, Lflow/path/Path$Builder;->elements:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    return p1
.end method


# virtual methods
.method public append(Lflow/path/Path;)V
    .locals 1

    .line 75
    iget-object v0, p0, Lflow/path/Path$Builder;->elements:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method
