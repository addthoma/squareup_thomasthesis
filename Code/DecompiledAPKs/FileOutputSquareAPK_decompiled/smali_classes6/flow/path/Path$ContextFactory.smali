.class final Lflow/path/Path$ContextFactory;
.super Ljava/lang/Object;
.source "Path.java"

# interfaces
.implements Lflow/path/PathContextFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lflow/path/Path;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ContextFactory"
.end annotation


# instance fields
.field private final delegate:Lflow/path/PathContextFactory;


# direct methods
.method constructor <init>()V
    .locals 1

    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 88
    iput-object v0, p0, Lflow/path/Path$ContextFactory;->delegate:Lflow/path/PathContextFactory;

    return-void
.end method

.method constructor <init>(Lflow/path/PathContextFactory;)V
    .locals 0

    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 92
    iput-object p1, p0, Lflow/path/Path$ContextFactory;->delegate:Lflow/path/PathContextFactory;

    return-void
.end method


# virtual methods
.method public setUpContext(Lflow/path/Path;Landroid/content/Context;)Landroid/content/Context;
    .locals 1

    .line 96
    iget-object v0, p0, Lflow/path/Path$ContextFactory;->delegate:Lflow/path/PathContextFactory;

    if-eqz v0, :cond_0

    .line 97
    invoke-interface {v0, p1, p2}, Lflow/path/PathContextFactory;->setUpContext(Lflow/path/Path;Landroid/content/Context;)Landroid/content/Context;

    move-result-object p2

    .line 99
    :cond_0
    new-instance v0, Lflow/path/FlowPathContextWrapper;

    invoke-direct {v0, p2, p1}, Lflow/path/FlowPathContextWrapper;-><init>(Landroid/content/Context;Ljava/lang/Object;)V

    return-object v0
.end method

.method public tearDownContext(Landroid/content/Context;)V
    .locals 1

    .line 103
    iget-object v0, p0, Lflow/path/Path$ContextFactory;->delegate:Lflow/path/PathContextFactory;

    if-eqz v0, :cond_0

    .line 104
    invoke-interface {v0, p1}, Lflow/path/PathContextFactory;->tearDownContext(Landroid/content/Context;)V

    :cond_0
    return-void
.end method
