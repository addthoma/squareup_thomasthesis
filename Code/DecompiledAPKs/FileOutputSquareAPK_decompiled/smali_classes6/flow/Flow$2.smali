.class Lflow/Flow$2;
.super Lflow/Flow$PendingTraversal;
.source "Flow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lflow/Flow;->set(Ljava/lang/Object;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lflow/Flow;

.field final synthetic val$newTop:Ljava/lang/Object;


# direct methods
.method constructor <init>(Lflow/Flow;Ljava/lang/Object;)V
    .locals 0

    .line 134
    iput-object p1, p0, Lflow/Flow$2;->this$0:Lflow/Flow;

    iput-object p2, p0, Lflow/Flow$2;->val$newTop:Ljava/lang/Object;

    const/4 p2, 0x0

    invoke-direct {p0, p1, p2}, Lflow/Flow$PendingTraversal;-><init>(Lflow/Flow;Lflow/Flow$1;)V

    return-void
.end method


# virtual methods
.method doExecute()V
    .locals 7

    .line 136
    iget-object v0, p0, Lflow/Flow$2;->val$newTop:Ljava/lang/Object;

    iget-object v1, p0, Lflow/Flow$2;->this$0:Lflow/Flow;

    invoke-static {v1}, Lflow/Flow;->access$200(Lflow/Flow;)Lflow/History;

    move-result-object v1

    invoke-virtual {v1}, Lflow/History;->top()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 137
    iget-object v0, p0, Lflow/Flow$2;->this$0:Lflow/Flow;

    invoke-static {v0}, Lflow/Flow;->access$200(Lflow/Flow;)Lflow/History;

    move-result-object v0

    sget-object v1, Lflow/Direction;->REPLACE:Lflow/Direction;

    invoke-virtual {p0, v0, v1}, Lflow/Flow$2;->dispatch(Lflow/History;Lflow/Direction;)V

    return-void

    .line 141
    :cond_0
    iget-object v0, p0, Lflow/Flow$2;->this$0:Lflow/Flow;

    invoke-static {v0}, Lflow/Flow;->access$200(Lflow/Flow;)Lflow/History;

    move-result-object v0

    invoke-virtual {v0}, Lflow/History;->buildUpon()Lflow/History$Builder;

    move-result-object v0

    const/4 v1, 0x0

    .line 145
    iget-object v2, p0, Lflow/Flow$2;->this$0:Lflow/Flow;

    invoke-static {v2}, Lflow/Flow;->access$200(Lflow/Flow;)Lflow/History;

    move-result-object v2

    invoke-virtual {v2}, Lflow/History;->framesFromBottom()Ljava/lang/Iterable;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    .line 148
    iget-object v6, p0, Lflow/Flow$2;->val$newTop:Ljava/lang/Object;

    invoke-virtual {v5, v6}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 149
    :goto_1
    iget-object v2, p0, Lflow/Flow$2;->this$0:Lflow/Flow;

    invoke-static {v2}, Lflow/Flow;->access$200(Lflow/Flow;)Lflow/History;

    move-result-object v2

    invoke-virtual {v2}, Lflow/History;->size()I

    move-result v2

    sub-int/2addr v2, v4

    if-ge v3, v2, :cond_2

    .line 150
    invoke-virtual {v0}, Lflow/History$Builder;->pop()Ljava/lang/Object;

    move-result-object v1

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_2
    if-eqz v1, :cond_3

    .line 161
    invoke-virtual {v0, v1}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    .line 162
    invoke-virtual {v0}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object v0

    .line 163
    sget-object v1, Lflow/Direction;->BACKWARD:Lflow/Direction;

    invoke-virtual {p0, v0, v1}, Lflow/Flow$2;->dispatch(Lflow/History;Lflow/Direction;)V

    goto :goto_2

    .line 166
    :cond_3
    iget-object v1, p0, Lflow/Flow$2;->val$newTop:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    .line 167
    invoke-virtual {v0}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object v0

    .line 168
    sget-object v1, Lflow/Direction;->FORWARD:Lflow/Direction;

    invoke-virtual {p0, v0, v1}, Lflow/Flow$2;->dispatch(Lflow/History;Lflow/Direction;)V

    :goto_2
    return-void
.end method
