.class public final Lhu/akarnokd/rxjava/interop/RxJavaInterop;
.super Ljava/lang/Object;
.source "RxJavaInterop.java"


# direct methods
.method private constructor <init>()V
    .locals 2

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No instances!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static toV1Completable(Lio/reactivex/CompletableSource;)Lrx/Completable;
    .locals 1
    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "source is null"

    .line 421
    invoke-static {p0, v0}, Lio/reactivex/internal/functions/ObjectHelper;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 422
    new-instance v0, Lhu/akarnokd/rxjava/interop/CompletableV2ToCompletableV1;

    invoke-direct {v0, p0}, Lhu/akarnokd/rxjava/interop/CompletableV2ToCompletableV1;-><init>(Lio/reactivex/CompletableSource;)V

    invoke-static {v0}, Lrx/Completable;->create(Lrx/Completable$OnSubscribe;)Lrx/Completable;

    move-result-object p0

    return-object p0
.end method

.method public static toV1Completable(Lio/reactivex/MaybeSource;)Lrx/Completable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/MaybeSource<",
            "TT;>;)",
            "Lrx/Completable;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "source is null"

    .line 459
    invoke-static {p0, v0}, Lio/reactivex/internal/functions/ObjectHelper;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 460
    new-instance v0, Lhu/akarnokd/rxjava/interop/MaybeV2ToCompletableV1;

    invoke-direct {v0, p0}, Lhu/akarnokd/rxjava/interop/MaybeV2ToCompletableV1;-><init>(Lio/reactivex/MaybeSource;)V

    invoke-static {v0}, Lrx/Completable;->create(Lrx/Completable$OnSubscribe;)Lrx/Completable;

    move-result-object p0

    return-object p0
.end method

.method public static toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/ObservableSource<",
            "TT;>;",
            "Lio/reactivex/BackpressureStrategy;",
            ")",
            "Lrx/Observable<",
            "TT;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "source is null"

    .line 385
    invoke-static {p0, v0}, Lio/reactivex/internal/functions/ObjectHelper;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "strategy is null"

    .line 386
    invoke-static {p1, v0}, Lio/reactivex/internal/functions/ObjectHelper;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 387
    invoke-static {p0}, Lio/reactivex/Observable;->wrap(Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object p0

    invoke-virtual {p0, p1}, Lio/reactivex/Observable;->toFlowable(Lio/reactivex/BackpressureStrategy;)Lio/reactivex/Flowable;

    move-result-object p0

    invoke-static {p0}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Observable(Lorg/reactivestreams/Publisher;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method public static toV1Observable(Lorg/reactivestreams/Publisher;)Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/reactivestreams/Publisher<",
            "TT;>;)",
            "Lrx/Observable<",
            "TT;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "source is null"

    .line 363
    invoke-static {p0, v0}, Lio/reactivex/internal/functions/ObjectHelper;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 364
    new-instance v0, Lhu/akarnokd/rxjava/interop/FlowableV2ToObservableV1;

    invoke-direct {v0, p0}, Lhu/akarnokd/rxjava/interop/FlowableV2ToObservableV1;-><init>(Lorg/reactivestreams/Publisher;)V

    invoke-static {v0}, Lrx/Observable;->unsafeCreate(Lrx/Observable$OnSubscribe;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method public static toV1Operator(Lio/reactivex/FlowableOperator;)Lrx/Observable$Operator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/FlowableOperator<",
            "TR;TT;>;)",
            "Lrx/Observable$Operator<",
            "TR;TT;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "operator is null"

    .line 616
    invoke-static {p0, v0}, Lio/reactivex/internal/functions/ObjectHelper;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 617
    new-instance v0, Lhu/akarnokd/rxjava/interop/RxJavaInterop$10;

    invoke-direct {v0, p0}, Lhu/akarnokd/rxjava/interop/RxJavaInterop$10;-><init>(Lio/reactivex/FlowableOperator;)V

    return-object v0
.end method

.method public static toV1Scheduler(Lio/reactivex/Scheduler;)Lrx/Scheduler;
    .locals 1

    const-string v0, "scheduler is null"

    .line 665
    invoke-static {p0, v0}, Lio/reactivex/internal/functions/ObjectHelper;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 666
    new-instance v0, Lhu/akarnokd/rxjava/interop/SchedulerV2ToSchedulerV1;

    invoke-direct {v0, p0}, Lhu/akarnokd/rxjava/interop/SchedulerV2ToSchedulerV1;-><init>(Lio/reactivex/Scheduler;)V

    return-object v0
.end method

.method public static toV1Single(Lio/reactivex/MaybeSource;)Lrx/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/MaybeSource<",
            "TT;>;)",
            "Lrx/Single<",
            "TT;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "source is null"

    .line 440
    invoke-static {p0, v0}, Lio/reactivex/internal/functions/ObjectHelper;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 441
    new-instance v0, Lhu/akarnokd/rxjava/interop/MaybeV2ToSingleV1;

    invoke-direct {v0, p0}, Lhu/akarnokd/rxjava/interop/MaybeV2ToSingleV1;-><init>(Lio/reactivex/MaybeSource;)V

    invoke-static {v0}, Lrx/Single;->create(Lrx/Single$OnSubscribe;)Lrx/Single;

    move-result-object p0

    return-object p0
.end method

.method public static toV1Single(Lio/reactivex/SingleSource;)Lrx/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/SingleSource<",
            "TT;>;)",
            "Lrx/Single<",
            "TT;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "source is null"

    .line 404
    invoke-static {p0, v0}, Lio/reactivex/internal/functions/ObjectHelper;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 405
    new-instance v0, Lhu/akarnokd/rxjava/interop/SingleV2ToSingleV1;

    invoke-direct {v0, p0}, Lhu/akarnokd/rxjava/interop/SingleV2ToSingleV1;-><init>(Lio/reactivex/SingleSource;)V

    invoke-static {v0}, Lrx/Single;->create(Lrx/Single$OnSubscribe;)Lrx/Single;

    move-result-object p0

    return-object p0
.end method

.method public static toV1Subject(Lio/reactivex/processors/FlowableProcessor;)Lrx/subjects/Subject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/processors/FlowableProcessor<",
            "TT;>;)",
            "Lrx/subjects/Subject<",
            "TT;TT;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "processor is null"

    .line 500
    invoke-static {p0, v0}, Lio/reactivex/internal/functions/ObjectHelper;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 501
    invoke-static {p0}, Lhu/akarnokd/rxjava/interop/ProcessorV2ToSubjectV1;->createWith(Lio/reactivex/processors/FlowableProcessor;)Lrx/subjects/Subject;

    move-result-object p0

    return-object p0
.end method

.method public static toV1Subject(Lio/reactivex/subjects/Subject;)Lrx/subjects/Subject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/subjects/Subject<",
            "TT;>;)",
            "Lrx/subjects/Subject<",
            "TT;TT;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "subject is null"

    .line 478
    invoke-static {p0, v0}, Lio/reactivex/internal/functions/ObjectHelper;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 479
    invoke-static {p0}, Lhu/akarnokd/rxjava/interop/SubjectV2ToSubjectV1;->createWith(Lio/reactivex/subjects/Subject;)Lrx/subjects/Subject;

    move-result-object p0

    return-object p0
.end method

.method public static toV1Subscription(Lio/reactivex/disposables/Disposable;)Lrx/Subscription;
    .locals 1

    const-string v0, "disposable is null"

    .line 654
    invoke-static {p0, v0}, Lio/reactivex/internal/functions/ObjectHelper;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 655
    new-instance v0, Lhu/akarnokd/rxjava/interop/DisposableV2ToSubscriptionV1;

    invoke-direct {v0, p0}, Lhu/akarnokd/rxjava/interop/DisposableV2ToSubscriptionV1;-><init>(Lio/reactivex/disposables/Disposable;)V

    return-object v0
.end method

.method public static toV1Transformer(Lio/reactivex/CompletableTransformer;)Lrx/Completable$Transformer;
    .locals 1
    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "transformer is null"

    .line 590
    invoke-static {p0, v0}, Lio/reactivex/internal/functions/ObjectHelper;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 591
    new-instance v0, Lhu/akarnokd/rxjava/interop/RxJavaInterop$9;

    invoke-direct {v0, p0}, Lhu/akarnokd/rxjava/interop/RxJavaInterop$9;-><init>(Lio/reactivex/CompletableTransformer;)V

    return-object v0
.end method

.method public static toV1Transformer(Lio/reactivex/FlowableTransformer;)Lrx/Observable$Transformer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/FlowableTransformer<",
            "TT;TR;>;)",
            "Lrx/Observable$Transformer<",
            "TT;TR;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "transformer is null"

    .line 521
    invoke-static {p0, v0}, Lio/reactivex/internal/functions/ObjectHelper;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 522
    new-instance v0, Lhu/akarnokd/rxjava/interop/RxJavaInterop$6;

    invoke-direct {v0, p0}, Lhu/akarnokd/rxjava/interop/RxJavaInterop$6;-><init>(Lio/reactivex/FlowableTransformer;)V

    return-object v0
.end method

.method public static toV1Transformer(Lio/reactivex/ObservableTransformer;Lio/reactivex/BackpressureStrategy;)Lrx/Observable$Transformer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/ObservableTransformer<",
            "TT;TR;>;",
            "Lio/reactivex/BackpressureStrategy;",
            ")",
            "Lrx/Observable$Transformer<",
            "TT;TR;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "transformer is null"

    .line 546
    invoke-static {p0, v0}, Lio/reactivex/internal/functions/ObjectHelper;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 547
    new-instance v0, Lhu/akarnokd/rxjava/interop/RxJavaInterop$7;

    invoke-direct {v0, p0, p1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop$7;-><init>(Lio/reactivex/ObservableTransformer;Lio/reactivex/BackpressureStrategy;)V

    return-object v0
.end method

.method public static toV1Transformer(Lio/reactivex/SingleTransformer;)Lrx/Single$Transformer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/SingleTransformer<",
            "TT;TR;>;)",
            "Lrx/Single$Transformer<",
            "TT;TR;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "transformer is null"

    .line 569
    invoke-static {p0, v0}, Lio/reactivex/internal/functions/ObjectHelper;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 570
    new-instance v0, Lhu/akarnokd/rxjava/interop/RxJavaInterop$8;

    invoke-direct {v0, p0}, Lhu/akarnokd/rxjava/interop/RxJavaInterop$8;-><init>(Lio/reactivex/SingleTransformer;)V

    return-object v0
.end method

.method public static toV2Completable(Lrx/Completable;)Lio/reactivex/Completable;
    .locals 1
    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "source is null"

    .line 140
    invoke-static {p0, v0}, Lio/reactivex/internal/functions/ObjectHelper;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 141
    new-instance v0, Lhu/akarnokd/rxjava/interop/CompletableV1ToCompletableV2;

    invoke-direct {v0, p0}, Lhu/akarnokd/rxjava/interop/CompletableV1ToCompletableV2;-><init>(Lrx/Completable;)V

    return-object v0
.end method

.method public static toV2Disposable(Lrx/Subscription;)Lio/reactivex/disposables/Disposable;
    .locals 1

    const-string v0, "subscription is null"

    .line 334
    invoke-static {p0, v0}, Lio/reactivex/internal/functions/ObjectHelper;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 335
    new-instance v0, Lhu/akarnokd/rxjava/interop/SubscriptionV1ToDisposableV2;

    invoke-direct {v0, p0}, Lhu/akarnokd/rxjava/interop/SubscriptionV1ToDisposableV2;-><init>(Lrx/Subscription;)V

    return-object v0
.end method

.method public static toV2Flowable(Lrx/Observable;)Lio/reactivex/Flowable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/Observable<",
            "TT;>;)",
            "Lio/reactivex/Flowable<",
            "TT;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "source is null"

    .line 53
    invoke-static {p0, v0}, Lio/reactivex/internal/functions/ObjectHelper;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 54
    new-instance v0, Lhu/akarnokd/rxjava/interop/ObservableV1ToFlowableV2;

    invoke-direct {v0, p0}, Lhu/akarnokd/rxjava/interop/ObservableV1ToFlowableV2;-><init>(Lrx/Observable;)V

    return-object v0
.end method

.method public static toV2Maybe(Lrx/Completable;)Lio/reactivex/Maybe;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/Completable;",
            ")",
            "Lio/reactivex/Maybe<",
            "TT;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "source is null"

    .line 90
    invoke-static {p0, v0}, Lio/reactivex/internal/functions/ObjectHelper;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 91
    new-instance v0, Lhu/akarnokd/rxjava/interop/CompletableV1ToMaybeV2;

    invoke-direct {v0, p0}, Lhu/akarnokd/rxjava/interop/CompletableV1ToMaybeV2;-><init>(Lrx/Completable;)V

    return-object v0
.end method

.method public static toV2Maybe(Lrx/Single;)Lio/reactivex/Maybe;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/Single<",
            "TT;>;)",
            "Lio/reactivex/Maybe<",
            "TT;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "source is null"

    .line 107
    invoke-static {p0, v0}, Lio/reactivex/internal/functions/ObjectHelper;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 108
    new-instance v0, Lhu/akarnokd/rxjava/interop/SingleV1ToMaybeV2;

    invoke-direct {v0, p0}, Lhu/akarnokd/rxjava/interop/SingleV1ToMaybeV2;-><init>(Lrx/Single;)V

    return-object v0
.end method

.method public static toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/Observable<",
            "TT;>;)",
            "Lio/reactivex/Observable<",
            "TT;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "source is null"

    .line 73
    invoke-static {p0, v0}, Lio/reactivex/internal/functions/ObjectHelper;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 74
    new-instance v0, Lhu/akarnokd/rxjava/interop/ObservableV1ToObservableV2;

    invoke-direct {v0, p0}, Lhu/akarnokd/rxjava/interop/ObservableV1ToObservableV2;-><init>(Lrx/Observable;)V

    return-object v0
.end method

.method public static toV2Operator(Lrx/Observable$Operator;)Lio/reactivex/FlowableOperator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/Observable$Operator<",
            "TR;TT;>;)",
            "Lio/reactivex/FlowableOperator<",
            "TR;TT;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "operator is null"

    .line 297
    invoke-static {p0, v0}, Lio/reactivex/internal/functions/ObjectHelper;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 298
    new-instance v0, Lhu/akarnokd/rxjava/interop/RxJavaInterop$5;

    invoke-direct {v0, p0}, Lhu/akarnokd/rxjava/interop/RxJavaInterop$5;-><init>(Lrx/Observable$Operator;)V

    return-object v0
.end method

.method public static toV2Processor(Lrx/subjects/Subject;)Lio/reactivex/processors/FlowableProcessor;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/subjects/Subject<",
            "TT;TT;>;)",
            "Lio/reactivex/processors/FlowableProcessor<",
            "TT;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "subject is null"

    .line 181
    invoke-static {p0, v0}, Lio/reactivex/internal/functions/ObjectHelper;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 182
    new-instance v0, Lhu/akarnokd/rxjava/interop/SubjectV1ToProcessorV2;

    invoke-direct {v0, p0}, Lhu/akarnokd/rxjava/interop/SubjectV1ToProcessorV2;-><init>(Lrx/subjects/Subject;)V

    return-object v0
.end method

.method public static toV2Scheduler(Lrx/Scheduler;)Lio/reactivex/Scheduler;
    .locals 1

    const-string v0, "scheduler is null"

    .line 676
    invoke-static {p0, v0}, Lio/reactivex/internal/functions/ObjectHelper;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 677
    new-instance v0, Lhu/akarnokd/rxjava/interop/SchedulerV1ToSchedulerV2;

    invoke-direct {v0, p0}, Lhu/akarnokd/rxjava/interop/SchedulerV1ToSchedulerV2;-><init>(Lrx/Scheduler;)V

    return-object v0
.end method

.method public static toV2Single(Lrx/Single;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/Single<",
            "TT;>;)",
            "Lio/reactivex/Single<",
            "TT;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "source is null"

    .line 124
    invoke-static {p0, v0}, Lio/reactivex/internal/functions/ObjectHelper;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 125
    new-instance v0, Lhu/akarnokd/rxjava/interop/SingleV1ToSingleV2;

    invoke-direct {v0, p0}, Lhu/akarnokd/rxjava/interop/SingleV1ToSingleV2;-><init>(Lrx/Single;)V

    return-object v0
.end method

.method public static toV2Subject(Lrx/subjects/Subject;)Lio/reactivex/subjects/Subject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/subjects/Subject<",
            "TT;TT;>;)",
            "Lio/reactivex/subjects/Subject<",
            "TT;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "subject is null"

    .line 159
    invoke-static {p0, v0}, Lio/reactivex/internal/functions/ObjectHelper;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 160
    new-instance v0, Lhu/akarnokd/rxjava/interop/SubjectV1ToSubjectV2;

    invoke-direct {v0, p0}, Lhu/akarnokd/rxjava/interop/SubjectV1ToSubjectV2;-><init>(Lrx/subjects/Subject;)V

    return-object v0
.end method

.method public static toV2Transformer(Lrx/Completable$Transformer;)Lio/reactivex/CompletableTransformer;
    .locals 1
    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "transformer is null"

    .line 271
    invoke-static {p0, v0}, Lio/reactivex/internal/functions/ObjectHelper;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 272
    new-instance v0, Lhu/akarnokd/rxjava/interop/RxJavaInterop$4;

    invoke-direct {v0, p0}, Lhu/akarnokd/rxjava/interop/RxJavaInterop$4;-><init>(Lrx/Completable$Transformer;)V

    return-object v0
.end method

.method public static toV2Transformer(Lrx/Observable$Transformer;)Lio/reactivex/FlowableTransformer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/Observable$Transformer<",
            "TT;TR;>;)",
            "Lio/reactivex/FlowableTransformer<",
            "TT;TR;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "transformer is null"

    .line 202
    invoke-static {p0, v0}, Lio/reactivex/internal/functions/ObjectHelper;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 203
    new-instance v0, Lhu/akarnokd/rxjava/interop/RxJavaInterop$1;

    invoke-direct {v0, p0}, Lhu/akarnokd/rxjava/interop/RxJavaInterop$1;-><init>(Lrx/Observable$Transformer;)V

    return-object v0
.end method

.method public static toV2Transformer(Lrx/Observable$Transformer;Lio/reactivex/BackpressureStrategy;)Lio/reactivex/ObservableTransformer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/Observable$Transformer<",
            "TT;TR;>;",
            "Lio/reactivex/BackpressureStrategy;",
            ")",
            "Lio/reactivex/ObservableTransformer<",
            "TT;TR;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "transformer is null"

    .line 227
    invoke-static {p0, v0}, Lio/reactivex/internal/functions/ObjectHelper;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 228
    new-instance v0, Lhu/akarnokd/rxjava/interop/RxJavaInterop$2;

    invoke-direct {v0, p0, p1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop$2;-><init>(Lrx/Observable$Transformer;Lio/reactivex/BackpressureStrategy;)V

    return-object v0
.end method

.method public static toV2Transformer(Lrx/Single$Transformer;)Lio/reactivex/SingleTransformer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/Single$Transformer<",
            "TT;TR;>;)",
            "Lio/reactivex/SingleTransformer<",
            "TT;TR;>;"
        }
    .end annotation

    .annotation runtime Lio/reactivex/annotations/SchedulerSupport;
        value = "none"
    .end annotation

    const-string v0, "transformer is null"

    .line 250
    invoke-static {p0, v0}, Lio/reactivex/internal/functions/ObjectHelper;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 251
    new-instance v0, Lhu/akarnokd/rxjava/interop/RxJavaInterop$3;

    invoke-direct {v0, p0}, Lhu/akarnokd/rxjava/interop/RxJavaInterop$3;-><init>(Lrx/Single$Transformer;)V

    return-object v0
.end method
