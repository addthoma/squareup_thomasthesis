.class final Lhu/akarnokd/rxjava/interop/MaybeV2ToSingleV1;
.super Ljava/lang/Object;
.source "MaybeV2ToSingleV1.java"

# interfaces
.implements Lrx/Single$OnSubscribe;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lhu/akarnokd/rxjava/interop/MaybeV2ToSingleV1$MaybeV2Observer;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/Single$OnSubscribe<",
        "TT;>;"
    }
.end annotation


# instance fields
.field final source:Lio/reactivex/MaybeSource;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/MaybeSource<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lio/reactivex/MaybeSource;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/MaybeSource<",
            "TT;>;)V"
        }
    .end annotation

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lhu/akarnokd/rxjava/interop/MaybeV2ToSingleV1;->source:Lio/reactivex/MaybeSource;

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    .line 27
    check-cast p1, Lrx/SingleSubscriber;

    invoke-virtual {p0, p1}, Lhu/akarnokd/rxjava/interop/MaybeV2ToSingleV1;->call(Lrx/SingleSubscriber;)V

    return-void
.end method

.method public call(Lrx/SingleSubscriber;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/SingleSubscriber<",
            "-TT;>;)V"
        }
    .end annotation

    .line 37
    new-instance v0, Lhu/akarnokd/rxjava/interop/MaybeV2ToSingleV1$MaybeV2Observer;

    invoke-direct {v0, p1}, Lhu/akarnokd/rxjava/interop/MaybeV2ToSingleV1$MaybeV2Observer;-><init>(Lrx/SingleSubscriber;)V

    .line 38
    invoke-virtual {p1, v0}, Lrx/SingleSubscriber;->add(Lrx/Subscription;)V

    .line 39
    iget-object p1, p0, Lhu/akarnokd/rxjava/interop/MaybeV2ToSingleV1;->source:Lio/reactivex/MaybeSource;

    invoke-interface {p1, v0}, Lio/reactivex/MaybeSource;->subscribe(Lio/reactivex/MaybeObserver;)V

    return-void
.end method
