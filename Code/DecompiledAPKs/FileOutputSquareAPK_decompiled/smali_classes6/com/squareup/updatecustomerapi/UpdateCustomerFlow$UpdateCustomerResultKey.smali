.class public final enum Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;
.super Ljava/lang/Enum;
.source "UpdateCustomerFlow.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/updatecustomerapi/UpdateCustomerFlow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "UpdateCustomerResultKey"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0008\t\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\u0008\u0003j\u0002\u0008\u0004j\u0002\u0008\u0005j\u0002\u0008\u0006j\u0002\u0008\u0007j\u0002\u0008\u0008j\u0002\u0008\t\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;",
        "",
        "(Ljava/lang/String;I)V",
        "ADD_TO_SALE",
        "CHOOSE_CUSTOMER",
        "CREATE_FOR_INVOICE",
        "CUSTOMERS_APPLET",
        "EDIT_ESTIMATE",
        "ORDER_ENTRY",
        "VIEW_CUSTOMER",
        "update-customer_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;

.field public static final enum ADD_TO_SALE:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;

.field public static final enum CHOOSE_CUSTOMER:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;

.field public static final enum CREATE_FOR_INVOICE:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;

.field public static final enum CUSTOMERS_APPLET:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;

.field public static final enum EDIT_ESTIMATE:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;

.field public static final enum ORDER_ENTRY:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;

.field public static final enum VIEW_CUSTOMER:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;

    new-instance v1, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;

    const/4 v2, 0x0

    const-string v3, "ADD_TO_SALE"

    invoke-direct {v1, v3, v2}, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;->ADD_TO_SALE:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;

    const/4 v2, 0x1

    const-string v3, "CHOOSE_CUSTOMER"

    invoke-direct {v1, v3, v2}, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;->CHOOSE_CUSTOMER:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;

    const/4 v2, 0x2

    const-string v3, "CREATE_FOR_INVOICE"

    invoke-direct {v1, v3, v2}, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;->CREATE_FOR_INVOICE:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;

    const/4 v2, 0x3

    const-string v3, "CUSTOMERS_APPLET"

    invoke-direct {v1, v3, v2}, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;->CUSTOMERS_APPLET:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;

    const/4 v2, 0x4

    const-string v3, "EDIT_ESTIMATE"

    invoke-direct {v1, v3, v2}, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;->EDIT_ESTIMATE:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;

    const/4 v2, 0x5

    const-string v3, "ORDER_ENTRY"

    invoke-direct {v1, v3, v2}, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;->ORDER_ENTRY:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;

    const/4 v2, 0x6

    const-string v3, "VIEW_CUSTOMER"

    invoke-direct {v1, v3, v2}, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;->VIEW_CUSTOMER:Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;->$VALUES:[Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 80
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;
    .locals 1

    const-class v0, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;

    return-object p0
.end method

.method public static values()[Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;
    .locals 1

    sget-object v0, Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;->$VALUES:[Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;

    invoke-virtual {v0}, [Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/updatecustomerapi/UpdateCustomerFlow$UpdateCustomerResultKey;

    return-object v0
.end method
