.class public final enum Lcom/squareup/x2/settings/ScreenType;
.super Ljava/lang/Enum;
.source "ScreenType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/x2/settings/ScreenType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/x2/settings/ScreenType;

.field public static final enum ABOUT_REGISTER_SCREEN:Lcom/squareup/x2/settings/ScreenType;

.field public static final enum ACCESSIBILITY_SCREEN:Lcom/squareup/x2/settings/ScreenType;

.field public static final enum BUYER_DISPLAY_SCREEN:Lcom/squareup/x2/settings/ScreenType;

.field public static final enum CONFIGURE_ETHERNET_SCREEN:Lcom/squareup/x2/settings/ScreenType;

.field public static final enum CONNECTED_WIFI_SCREEN:Lcom/squareup/x2/settings/ScreenType;

.field public static final enum DEVELOPMENT_SCREEN:Lcom/squareup/x2/settings/ScreenType;

.field public static final enum DISPLAY_SCREEN:Lcom/squareup/x2/settings/ScreenType;

.field public static final enum ETHERNET_INFO_SCREEN:Lcom/squareup/x2/settings/ScreenType;

.field public static final enum GENERAL_DATE_SCREEN:Lcom/squareup/x2/settings/ScreenType;

.field public static final enum GENERAL_LOCALE_SCREEN:Lcom/squareup/x2/settings/ScreenType;

.field public static final enum GENERAL_NIGHTLY_REBOOT_SCREEN:Lcom/squareup/x2/settings/ScreenType;

.field public static final enum GENERAL_SCREEN:Lcom/squareup/x2/settings/ScreenType;

.field public static final enum GENERAL_TIME_SCREEN:Lcom/squareup/x2/settings/ScreenType;

.field public static final enum GENERAL_TIME_ZONE_SCREEN:Lcom/squareup/x2/settings/ScreenType;

.field public static final enum HIDDEN_NETWORK_SCREEN:Lcom/squareup/x2/settings/ScreenType;

.field public static final enum NETWORK_SCREEN:Lcom/squareup/x2/settings/ScreenType;

.field public static final enum REGULATORY_SCREEN:Lcom/squareup/x2/settings/ScreenType;

.field public static final enum SECURITY_OPTIONS_SCREEN:Lcom/squareup/x2/settings/ScreenType;

.field public static final enum SOUND_FAMILIES_SCREEN:Lcom/squareup/x2/settings/ScreenType;

.field public static final enum SOUND_SCREEN:Lcom/squareup/x2/settings/ScreenType;

.field public static final enum WIFI_AUTH_SCREEN:Lcom/squareup/x2/settings/ScreenType;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 7
    new-instance v0, Lcom/squareup/x2/settings/ScreenType;

    const/4 v1, 0x0

    const-string v2, "ABOUT_REGISTER_SCREEN"

    invoke-direct {v0, v2, v1}, Lcom/squareup/x2/settings/ScreenType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/x2/settings/ScreenType;->ABOUT_REGISTER_SCREEN:Lcom/squareup/x2/settings/ScreenType;

    .line 8
    new-instance v0, Lcom/squareup/x2/settings/ScreenType;

    const/4 v2, 0x1

    const-string v3, "ACCESSIBILITY_SCREEN"

    invoke-direct {v0, v3, v2}, Lcom/squareup/x2/settings/ScreenType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/x2/settings/ScreenType;->ACCESSIBILITY_SCREEN:Lcom/squareup/x2/settings/ScreenType;

    .line 9
    new-instance v0, Lcom/squareup/x2/settings/ScreenType;

    const/4 v3, 0x2

    const-string v4, "BUYER_DISPLAY_SCREEN"

    invoke-direct {v0, v4, v3}, Lcom/squareup/x2/settings/ScreenType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/x2/settings/ScreenType;->BUYER_DISPLAY_SCREEN:Lcom/squareup/x2/settings/ScreenType;

    .line 10
    new-instance v0, Lcom/squareup/x2/settings/ScreenType;

    const/4 v4, 0x3

    const-string v5, "DEVELOPMENT_SCREEN"

    invoke-direct {v0, v5, v4}, Lcom/squareup/x2/settings/ScreenType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/x2/settings/ScreenType;->DEVELOPMENT_SCREEN:Lcom/squareup/x2/settings/ScreenType;

    .line 11
    new-instance v0, Lcom/squareup/x2/settings/ScreenType;

    const/4 v5, 0x4

    const-string v6, "DISPLAY_SCREEN"

    invoke-direct {v0, v6, v5}, Lcom/squareup/x2/settings/ScreenType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/x2/settings/ScreenType;->DISPLAY_SCREEN:Lcom/squareup/x2/settings/ScreenType;

    .line 12
    new-instance v0, Lcom/squareup/x2/settings/ScreenType;

    const/4 v6, 0x5

    const-string v7, "GENERAL_SCREEN"

    invoke-direct {v0, v7, v6}, Lcom/squareup/x2/settings/ScreenType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/x2/settings/ScreenType;->GENERAL_SCREEN:Lcom/squareup/x2/settings/ScreenType;

    .line 13
    new-instance v0, Lcom/squareup/x2/settings/ScreenType;

    const/4 v7, 0x6

    const-string v8, "NETWORK_SCREEN"

    invoke-direct {v0, v8, v7}, Lcom/squareup/x2/settings/ScreenType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/x2/settings/ScreenType;->NETWORK_SCREEN:Lcom/squareup/x2/settings/ScreenType;

    .line 14
    new-instance v0, Lcom/squareup/x2/settings/ScreenType;

    const/4 v8, 0x7

    const-string v9, "REGULATORY_SCREEN"

    invoke-direct {v0, v9, v8}, Lcom/squareup/x2/settings/ScreenType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/x2/settings/ScreenType;->REGULATORY_SCREEN:Lcom/squareup/x2/settings/ScreenType;

    .line 15
    new-instance v0, Lcom/squareup/x2/settings/ScreenType;

    const/16 v9, 0x8

    const-string v10, "SOUND_FAMILIES_SCREEN"

    invoke-direct {v0, v10, v9}, Lcom/squareup/x2/settings/ScreenType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/x2/settings/ScreenType;->SOUND_FAMILIES_SCREEN:Lcom/squareup/x2/settings/ScreenType;

    .line 16
    new-instance v0, Lcom/squareup/x2/settings/ScreenType;

    const/16 v10, 0x9

    const-string v11, "SOUND_SCREEN"

    invoke-direct {v0, v11, v10}, Lcom/squareup/x2/settings/ScreenType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/x2/settings/ScreenType;->SOUND_SCREEN:Lcom/squareup/x2/settings/ScreenType;

    .line 17
    new-instance v0, Lcom/squareup/x2/settings/ScreenType;

    const/16 v11, 0xa

    const-string v12, "GENERAL_DATE_SCREEN"

    invoke-direct {v0, v12, v11}, Lcom/squareup/x2/settings/ScreenType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/x2/settings/ScreenType;->GENERAL_DATE_SCREEN:Lcom/squareup/x2/settings/ScreenType;

    .line 18
    new-instance v0, Lcom/squareup/x2/settings/ScreenType;

    const/16 v12, 0xb

    const-string v13, "GENERAL_TIME_SCREEN"

    invoke-direct {v0, v13, v12}, Lcom/squareup/x2/settings/ScreenType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/x2/settings/ScreenType;->GENERAL_TIME_SCREEN:Lcom/squareup/x2/settings/ScreenType;

    .line 19
    new-instance v0, Lcom/squareup/x2/settings/ScreenType;

    const/16 v13, 0xc

    const-string v14, "GENERAL_TIME_ZONE_SCREEN"

    invoke-direct {v0, v14, v13}, Lcom/squareup/x2/settings/ScreenType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/x2/settings/ScreenType;->GENERAL_TIME_ZONE_SCREEN:Lcom/squareup/x2/settings/ScreenType;

    .line 20
    new-instance v0, Lcom/squareup/x2/settings/ScreenType;

    const/16 v14, 0xd

    const-string v15, "GENERAL_LOCALE_SCREEN"

    invoke-direct {v0, v15, v14}, Lcom/squareup/x2/settings/ScreenType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/x2/settings/ScreenType;->GENERAL_LOCALE_SCREEN:Lcom/squareup/x2/settings/ScreenType;

    .line 21
    new-instance v0, Lcom/squareup/x2/settings/ScreenType;

    const/16 v15, 0xe

    const-string v14, "GENERAL_NIGHTLY_REBOOT_SCREEN"

    invoke-direct {v0, v14, v15}, Lcom/squareup/x2/settings/ScreenType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/x2/settings/ScreenType;->GENERAL_NIGHTLY_REBOOT_SCREEN:Lcom/squareup/x2/settings/ScreenType;

    .line 22
    new-instance v0, Lcom/squareup/x2/settings/ScreenType;

    const-string v14, "WIFI_AUTH_SCREEN"

    const/16 v15, 0xf

    invoke-direct {v0, v14, v15}, Lcom/squareup/x2/settings/ScreenType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/x2/settings/ScreenType;->WIFI_AUTH_SCREEN:Lcom/squareup/x2/settings/ScreenType;

    .line 23
    new-instance v0, Lcom/squareup/x2/settings/ScreenType;

    const-string v14, "CONNECTED_WIFI_SCREEN"

    const/16 v15, 0x10

    invoke-direct {v0, v14, v15}, Lcom/squareup/x2/settings/ScreenType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/x2/settings/ScreenType;->CONNECTED_WIFI_SCREEN:Lcom/squareup/x2/settings/ScreenType;

    .line 24
    new-instance v0, Lcom/squareup/x2/settings/ScreenType;

    const-string v14, "ETHERNET_INFO_SCREEN"

    const/16 v15, 0x11

    invoke-direct {v0, v14, v15}, Lcom/squareup/x2/settings/ScreenType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/x2/settings/ScreenType;->ETHERNET_INFO_SCREEN:Lcom/squareup/x2/settings/ScreenType;

    .line 25
    new-instance v0, Lcom/squareup/x2/settings/ScreenType;

    const-string v14, "CONFIGURE_ETHERNET_SCREEN"

    const/16 v15, 0x12

    invoke-direct {v0, v14, v15}, Lcom/squareup/x2/settings/ScreenType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/x2/settings/ScreenType;->CONFIGURE_ETHERNET_SCREEN:Lcom/squareup/x2/settings/ScreenType;

    .line 26
    new-instance v0, Lcom/squareup/x2/settings/ScreenType;

    const-string v14, "HIDDEN_NETWORK_SCREEN"

    const/16 v15, 0x13

    invoke-direct {v0, v14, v15}, Lcom/squareup/x2/settings/ScreenType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/x2/settings/ScreenType;->HIDDEN_NETWORK_SCREEN:Lcom/squareup/x2/settings/ScreenType;

    .line 27
    new-instance v0, Lcom/squareup/x2/settings/ScreenType;

    const-string v14, "SECURITY_OPTIONS_SCREEN"

    const/16 v15, 0x14

    invoke-direct {v0, v14, v15}, Lcom/squareup/x2/settings/ScreenType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/x2/settings/ScreenType;->SECURITY_OPTIONS_SCREEN:Lcom/squareup/x2/settings/ScreenType;

    const/16 v0, 0x15

    new-array v0, v0, [Lcom/squareup/x2/settings/ScreenType;

    .line 6
    sget-object v14, Lcom/squareup/x2/settings/ScreenType;->ABOUT_REGISTER_SCREEN:Lcom/squareup/x2/settings/ScreenType;

    aput-object v14, v0, v1

    sget-object v1, Lcom/squareup/x2/settings/ScreenType;->ACCESSIBILITY_SCREEN:Lcom/squareup/x2/settings/ScreenType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/x2/settings/ScreenType;->BUYER_DISPLAY_SCREEN:Lcom/squareup/x2/settings/ScreenType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/x2/settings/ScreenType;->DEVELOPMENT_SCREEN:Lcom/squareup/x2/settings/ScreenType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/x2/settings/ScreenType;->DISPLAY_SCREEN:Lcom/squareup/x2/settings/ScreenType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/x2/settings/ScreenType;->GENERAL_SCREEN:Lcom/squareup/x2/settings/ScreenType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/x2/settings/ScreenType;->NETWORK_SCREEN:Lcom/squareup/x2/settings/ScreenType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/x2/settings/ScreenType;->REGULATORY_SCREEN:Lcom/squareup/x2/settings/ScreenType;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/x2/settings/ScreenType;->SOUND_FAMILIES_SCREEN:Lcom/squareup/x2/settings/ScreenType;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/x2/settings/ScreenType;->SOUND_SCREEN:Lcom/squareup/x2/settings/ScreenType;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/x2/settings/ScreenType;->GENERAL_DATE_SCREEN:Lcom/squareup/x2/settings/ScreenType;

    aput-object v1, v0, v11

    sget-object v1, Lcom/squareup/x2/settings/ScreenType;->GENERAL_TIME_SCREEN:Lcom/squareup/x2/settings/ScreenType;

    aput-object v1, v0, v12

    sget-object v1, Lcom/squareup/x2/settings/ScreenType;->GENERAL_TIME_ZONE_SCREEN:Lcom/squareup/x2/settings/ScreenType;

    aput-object v1, v0, v13

    sget-object v1, Lcom/squareup/x2/settings/ScreenType;->GENERAL_LOCALE_SCREEN:Lcom/squareup/x2/settings/ScreenType;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/x2/settings/ScreenType;->GENERAL_NIGHTLY_REBOOT_SCREEN:Lcom/squareup/x2/settings/ScreenType;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/x2/settings/ScreenType;->WIFI_AUTH_SCREEN:Lcom/squareup/x2/settings/ScreenType;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/x2/settings/ScreenType;->CONNECTED_WIFI_SCREEN:Lcom/squareup/x2/settings/ScreenType;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/x2/settings/ScreenType;->ETHERNET_INFO_SCREEN:Lcom/squareup/x2/settings/ScreenType;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/x2/settings/ScreenType;->CONFIGURE_ETHERNET_SCREEN:Lcom/squareup/x2/settings/ScreenType;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/x2/settings/ScreenType;->HIDDEN_NETWORK_SCREEN:Lcom/squareup/x2/settings/ScreenType;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/x2/settings/ScreenType;->SECURITY_OPTIONS_SCREEN:Lcom/squareup/x2/settings/ScreenType;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/x2/settings/ScreenType;->$VALUES:[Lcom/squareup/x2/settings/ScreenType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 6
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/x2/settings/ScreenType;
    .locals 1

    .line 6
    const-class v0, Lcom/squareup/x2/settings/ScreenType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/x2/settings/ScreenType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/x2/settings/ScreenType;
    .locals 1

    .line 6
    sget-object v0, Lcom/squareup/x2/settings/ScreenType;->$VALUES:[Lcom/squareup/x2/settings/ScreenType;

    invoke-virtual {v0}, [Lcom/squareup/x2/settings/ScreenType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/x2/settings/ScreenType;

    return-object v0
.end method
