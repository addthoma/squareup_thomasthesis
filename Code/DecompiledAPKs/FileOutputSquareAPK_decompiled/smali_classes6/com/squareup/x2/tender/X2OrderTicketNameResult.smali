.class public Lcom/squareup/x2/tender/X2OrderTicketNameResult;
.super Ljava/lang/Object;
.source "X2OrderTicketNameResult.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;
    }
.end annotation


# instance fields
.field private final name:Ljava/lang/String;

.field private final state:Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;


# direct methods
.method public constructor <init>(Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;)V
    .locals 1

    const-string v0, ""

    .line 25
    invoke-direct {p0, p1, v0}, Lcom/squareup/x2/tender/X2OrderTicketNameResult;-><init>(Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;Ljava/lang/String;)V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/x2/tender/X2OrderTicketNameResult;->state:Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;

    .line 21
    iput-object p2, p0, Lcom/squareup/x2/tender/X2OrderTicketNameResult;->name:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/squareup/x2/tender/X2OrderTicketNameResult;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getState()Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/x2/tender/X2OrderTicketNameResult;->state:Lcom/squareup/x2/tender/X2OrderTicketNameResult$State;

    return-object v0
.end method
