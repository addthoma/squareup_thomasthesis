.class public abstract Lcom/squareup/x2/NoX2AppModule;
.super Ljava/lang/Object;
.source "NoX2AppModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideMaybeSquareDevice()Lcom/squareup/x2/BadMaybeSquareDeviceCheck;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 30
    new-instance v0, Lcom/squareup/x2/BadMaybeSquareDeviceCheck$NonSquareDevice;

    invoke-direct {v0}, Lcom/squareup/x2/BadMaybeSquareDeviceCheck$NonSquareDevice;-><init>()V

    return-object v0
.end method

.method static provideRemoteCardReader(Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/CardReaderId;)Lcom/squareup/cardreader/BranRemoteCardReader;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 26
    new-instance v0, Lcom/squareup/cardreader/BranRemoteCardReader;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p0, p1}, Lcom/squareup/cardreader/BranRemoteCardReader;-><init>(Lcom/squareup/cardreader/RemoteCardReaderBus;Lcom/squareup/cardreader/CardReaderInfo;Lcom/squareup/cardreader/CardReaderId;)V

    return-object v0
.end method
