.class public Lcom/squareup/widgets/CenteredFrameLayout;
.super Landroid/widget/FrameLayout;
.source "CenteredFrameLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/widgets/CenteredFrameLayout$LayoutParams;
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 93
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method static calculateChildLeft(IIII)I
    .locals 1

    add-int/2addr p3, p0

    .line 132
    div-int/lit8 v0, p2, 0x2

    sub-int/2addr p3, v0

    .line 135
    invoke-static {p0, p3}, Ljava/lang/Math;->max(II)I

    move-result p0

    sub-int/2addr p1, p2

    .line 136
    invoke-static {p1, p0}, Ljava/lang/Math;->min(II)I

    move-result p0

    return p0
.end method


# virtual methods
.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 0

    .line 109
    instance-of p1, p1, Lcom/squareup/widgets/CenteredFrameLayout$LayoutParams;

    return p1
.end method

.method protected bridge synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .line 70
    invoke-virtual {p0}, Lcom/squareup/widgets/CenteredFrameLayout;->generateDefaultLayoutParams()Landroid/widget/FrameLayout$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method protected generateDefaultLayoutParams()Landroid/widget/FrameLayout$LayoutParams;
    .locals 2

    .line 105
    new-instance v0, Lcom/squareup/widgets/CenteredFrameLayout$LayoutParams;

    invoke-super {p0}, Landroid/widget/FrameLayout;->generateDefaultLayoutParams()Landroid/widget/FrameLayout$LayoutParams;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/widgets/CenteredFrameLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method public bridge synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 0

    .line 70
    invoke-virtual {p0, p1}, Lcom/squareup/widgets/CenteredFrameLayout;->generateLayoutParams(Landroid/util/AttributeSet;)Landroid/widget/FrameLayout$LayoutParams;

    move-result-object p1

    return-object p1
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .line 97
    new-instance v0, Lcom/squareup/widgets/CenteredFrameLayout$LayoutParams;

    invoke-direct {v0, p1}, Lcom/squareup/widgets/CenteredFrameLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/widget/FrameLayout$LayoutParams;
    .locals 2

    .line 101
    new-instance v0, Lcom/squareup/widgets/CenteredFrameLayout$LayoutParams;

    invoke-virtual {p0}, Lcom/squareup/widgets/CenteredFrameLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/squareup/widgets/CenteredFrameLayout$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected onLayout(ZIIII)V
    .locals 4

    .line 114
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    .line 116
    invoke-virtual {p0}, Lcom/squareup/widgets/CenteredFrameLayout;->getChildCount()I

    move-result p1

    const/4 p3, 0x0

    :goto_0
    if-ge p3, p1, :cond_1

    .line 118
    invoke-virtual {p0, p3}, Lcom/squareup/widgets/CenteredFrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object p5

    .line 119
    invoke-virtual {p5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/CenteredFrameLayout$LayoutParams;

    .line 122
    iget v1, v0, Lcom/squareup/widgets/CenteredFrameLayout$LayoutParams;->leftOffset:I

    if-ltz v1, :cond_0

    .line 123
    invoke-virtual {p5}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    .line 124
    iget v0, v0, Lcom/squareup/widgets/CenteredFrameLayout$LayoutParams;->leftOffset:I

    invoke-static {p2, p4, v1, v0}, Lcom/squareup/widgets/CenteredFrameLayout;->calculateChildLeft(IIII)I

    move-result v0

    .line 125
    invoke-virtual {p5}, Landroid/view/View;->getTop()I

    move-result v2

    add-int/2addr v1, v0

    invoke-virtual {p5}, Landroid/view/View;->getBottom()I

    move-result v3

    invoke-virtual {p5, v0, v2, v1, v3}, Landroid/view/View;->layout(IIII)V

    :cond_0
    add-int/lit8 p3, p3, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method
