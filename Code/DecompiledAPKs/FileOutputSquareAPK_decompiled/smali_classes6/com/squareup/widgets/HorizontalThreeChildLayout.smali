.class public Lcom/squareup/widgets/HorizontalThreeChildLayout;
.super Landroid/view/ViewGroup;
.source "HorizontalThreeChildLayout.java"


# static fields
.field private static final UNSET:I = -0x1


# instance fields
.field private firstChild:Landroid/view/View;

.field private final firstChildPercentage:I

.field private secondChild:Landroid/view/View;

.field private final secondChildPercentage:I

.field private thirdChild:Landroid/view/View;

.field private final thirdChildPercentage:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .line 31
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    sget-object v0, Lcom/squareup/widgets/R$styleable;->HorizontalThreeChildLayout:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 33
    sget p2, Lcom/squareup/widgets/R$styleable;->HorizontalThreeChildLayout_firstChildPercentage:I

    const/4 v0, -0x1

    .line 34
    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result p2

    iput p2, p0, Lcom/squareup/widgets/HorizontalThreeChildLayout;->firstChildPercentage:I

    .line 35
    sget p2, Lcom/squareup/widgets/R$styleable;->HorizontalThreeChildLayout_secondChildPercentage:I

    .line 36
    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result p2

    iput p2, p0, Lcom/squareup/widgets/HorizontalThreeChildLayout;->secondChildPercentage:I

    .line 37
    sget p2, Lcom/squareup/widgets/R$styleable;->HorizontalThreeChildLayout_thirdChildPercentage:I

    .line 38
    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result p2

    iput p2, p0, Lcom/squareup/widgets/HorizontalThreeChildLayout;->thirdChildPercentage:I

    .line 40
    iget p2, p0, Lcom/squareup/widgets/HorizontalThreeChildLayout;->firstChildPercentage:I

    if-eq p2, v0, :cond_1

    iget v1, p0, Lcom/squareup/widgets/HorizontalThreeChildLayout;->secondChildPercentage:I

    if-eq v1, v0, :cond_1

    iget v2, p0, Lcom/squareup/widgets/HorizontalThreeChildLayout;->thirdChildPercentage:I

    if-eq v2, v0, :cond_1

    add-int/2addr p2, v1

    add-int/2addr p2, v2

    const/16 v0, 0x64

    if-ne p2, v0, :cond_0

    .line 51
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    return-void

    .line 48
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Percentages must sum to 100 but sums to "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 43
    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Must set child percentage"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 3

    .line 55
    invoke-super {p0}, Landroid/view/ViewGroup;->onFinishInflate()V

    .line 56
    invoke-virtual {p0}, Lcom/squareup/widgets/HorizontalThreeChildLayout;->getChildCount()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    .line 62
    invoke-virtual {p0, v0}, Lcom/squareup/widgets/HorizontalThreeChildLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/widgets/HorizontalThreeChildLayout;->firstChild:Landroid/view/View;

    const/4 v0, 0x1

    .line 63
    invoke-virtual {p0, v0}, Lcom/squareup/widgets/HorizontalThreeChildLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/widgets/HorizontalThreeChildLayout;->secondChild:Landroid/view/View;

    const/4 v0, 0x2

    .line 64
    invoke-virtual {p0, v0}, Lcom/squareup/widgets/HorizontalThreeChildLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/widgets/HorizontalThreeChildLayout;->thirdChild:Landroid/view/View;

    return-void

    .line 57
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "HorizontalThreeChildLayout requires exactly trhee children, but currently has "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 59
    invoke-virtual {p0}, Lcom/squareup/widgets/HorizontalThreeChildLayout;->getChildCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected onLayout(ZIIII)V
    .locals 2

    .line 92
    invoke-virtual {p0}, Lcom/squareup/widgets/HorizontalThreeChildLayout;->getPaddingLeft()I

    move-result p1

    .line 93
    invoke-virtual {p0}, Lcom/squareup/widgets/HorizontalThreeChildLayout;->getChildCount()I

    move-result p2

    const/4 p3, 0x0

    move p4, p1

    const/4 p1, 0x0

    :goto_0
    if-ge p1, p2, :cond_0

    .line 95
    invoke-virtual {p0, p1}, Lcom/squareup/widgets/HorizontalThreeChildLayout;->getChildAt(I)Landroid/view/View;

    move-result-object p5

    .line 96
    invoke-virtual {p5}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    add-int/2addr v0, p4

    .line 97
    invoke-virtual {p5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p5, p4, p3, v0, v1}, Landroid/view/View;->layout(IIII)V

    add-int/lit8 p1, p1, 0x1

    move p4, v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 5

    .line 68
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p1

    .line 70
    iget v0, p0, Lcom/squareup/widgets/HorizontalThreeChildLayout;->firstChildPercentage:I

    mul-int v0, v0, p1

    int-to-float v0, v0

    const/high16 v1, 0x42c80000    # 100.0f

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 74
    invoke-virtual {p0}, Lcom/squareup/widgets/HorizontalThreeChildLayout;->getPaddingLeft()I

    move-result v2

    sub-int v2, v0, v2

    .line 75
    iget-object v3, p0, Lcom/squareup/widgets/HorizontalThreeChildLayout;->firstChild:Landroid/view/View;

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v3, v2, p2}, Landroid/view/View;->measure(II)V

    .line 77
    iget v2, p0, Lcom/squareup/widgets/HorizontalThreeChildLayout;->secondChildPercentage:I

    mul-int v2, v2, p1

    int-to-float v2, v2

    div-float/2addr v2, v1

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 78
    iget-object v2, p0, Lcom/squareup/widgets/HorizontalThreeChildLayout;->secondChild:Landroid/view/View;

    invoke-static {v1, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v2, v3, p2}, Landroid/view/View;->measure(II)V

    sub-int v1, p1, v1

    sub-int/2addr v1, v0

    .line 81
    iget-object v0, p0, Lcom/squareup/widgets/HorizontalThreeChildLayout;->thirdChild:Landroid/view/View;

    invoke-static {v1, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {v0, v1, p2}, Landroid/view/View;->measure(II)V

    .line 83
    iget-object p2, p0, Lcom/squareup/widgets/HorizontalThreeChildLayout;->firstChild:Landroid/view/View;

    .line 84
    invoke-virtual {p2}, Landroid/view/View;->getMeasuredHeight()I

    move-result p2

    iget-object v0, p0, Lcom/squareup/widgets/HorizontalThreeChildLayout;->secondChild:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    invoke-static {p2, v0}, Ljava/lang/Math;->max(II)I

    move-result p2

    iget-object v0, p0, Lcom/squareup/widgets/HorizontalThreeChildLayout;->thirdChild:Landroid/view/View;

    .line 85
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    .line 84
    invoke-static {p2, v0}, Ljava/lang/Math;->max(II)I

    move-result p2

    .line 88
    invoke-virtual {p0, p1, p2}, Lcom/squareup/widgets/HorizontalThreeChildLayout;->setMeasuredDimension(II)V

    return-void
.end method
