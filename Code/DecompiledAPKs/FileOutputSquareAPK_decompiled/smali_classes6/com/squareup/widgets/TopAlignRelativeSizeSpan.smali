.class public Lcom/squareup/widgets/TopAlignRelativeSizeSpan;
.super Landroid/text/style/MetricAffectingSpan;
.source "TopAlignRelativeSizeSpan.java"


# instance fields
.field private fontScale:F


# direct methods
.method public constructor <init>(F)V
    .locals 0

    .line 13
    invoke-direct {p0}, Landroid/text/style/MetricAffectingSpan;-><init>()V

    .line 14
    iput p1, p0, Lcom/squareup/widgets/TopAlignRelativeSizeSpan;->fontScale:F

    return-void
.end method


# virtual methods
.method public updateDrawState(Landroid/text/TextPaint;)V
    .locals 3

    .line 20
    invoke-virtual {p1}, Landroid/text/TextPaint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Paint$FontMetrics;->top:F

    invoke-virtual {p1}, Landroid/text/TextPaint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Paint$FontMetrics;->bottom:F

    add-float/2addr v0, v1

    .line 23
    invoke-virtual {p1}, Landroid/text/TextPaint;->getTextSize()F

    move-result v1

    iget v2, p0, Lcom/squareup/widgets/TopAlignRelativeSizeSpan;->fontScale:F

    mul-float v1, v1, v2

    invoke-virtual {p1, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 26
    invoke-virtual {p1}, Landroid/text/TextPaint;->ascent()F

    move-result v1

    sub-float/2addr v0, v1

    .line 28
    iget v1, p1, Landroid/text/TextPaint;->baselineShift:I

    float-to-int v0, v0

    add-int/2addr v1, v0

    iput v1, p1, Landroid/text/TextPaint;->baselineShift:I

    return-void
.end method

.method public updateMeasureState(Landroid/text/TextPaint;)V
    .locals 0

    .line 33
    invoke-virtual {p0, p1}, Lcom/squareup/widgets/TopAlignRelativeSizeSpan;->updateDrawState(Landroid/text/TextPaint;)V

    return-void
.end method
