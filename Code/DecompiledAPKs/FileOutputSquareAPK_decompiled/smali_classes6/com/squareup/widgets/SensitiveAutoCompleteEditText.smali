.class public Lcom/squareup/widgets/SensitiveAutoCompleteEditText;
.super Lcom/squareup/widgets/OnScreenRectangleAutoCompleteEditText;
.source "SensitiveAutoCompleteEditText.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/widgets/SensitiveAutoCompleteEditText$SensitiveState;
    }
.end annotation


# static fields
.field private static final KEY_SENSITIVE_STATE:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 14
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/squareup/widgets/SensitiveAutoCompleteEditText;

    .line 15
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ".state"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/squareup/widgets/SensitiveAutoCompleteEditText;->KEY_SENSITIVE_STATE:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 18
    invoke-direct {p0, p1, p2}, Lcom/squareup/widgets/OnScreenRectangleAutoCompleteEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    .line 23
    move-object v0, p1

    check-cast v0, Landroid/os/Bundle;

    sget-object v1, Lcom/squareup/widgets/SensitiveAutoCompleteEditText;->KEY_SENSITIVE_STATE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 25
    check-cast v0, Lcom/squareup/widgets/SensitiveAutoCompleteEditText$SensitiveState;

    invoke-static {v0}, Lcom/squareup/widgets/SensitiveAutoCompleteEditText$SensitiveState;->access$000(Lcom/squareup/widgets/SensitiveAutoCompleteEditText$SensitiveState;)Landroid/os/Parcelable;

    move-result-object p1

    .line 27
    :cond_0
    invoke-super {p0, p1}, Lcom/squareup/widgets/OnScreenRectangleAutoCompleteEditText;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 4

    .line 31
    invoke-super {p0}, Lcom/squareup/widgets/OnScreenRectangleAutoCompleteEditText;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 36
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 37
    sget-object v2, Lcom/squareup/widgets/SensitiveAutoCompleteEditText;->KEY_SENSITIVE_STATE:Ljava/lang/String;

    new-instance v3, Lcom/squareup/widgets/SensitiveAutoCompleteEditText$SensitiveState;

    invoke-direct {v3, v0}, Lcom/squareup/widgets/SensitiveAutoCompleteEditText$SensitiveState;-><init>(Landroid/os/Parcelable;)V

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    return-object v1
.end method
