.class public Lcom/squareup/widgets/list/CheckableTextView;
.super Lcom/squareup/marketfont/MarketTextView;
.source "CheckableTextView.java"

# interfaces
.implements Landroid/widget/Checkable;


# instance fields
.field private isChecked:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 14
    invoke-direct {p0, p1, p2}, Lcom/squareup/marketfont/MarketTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public isChecked()Z
    .locals 1

    .line 23
    iget-boolean v0, p0, Lcom/squareup/widgets/list/CheckableTextView;->isChecked:Z

    return v0
.end method

.method public setChecked(Z)V
    .locals 0

    .line 18
    iput-boolean p1, p0, Lcom/squareup/widgets/list/CheckableTextView;->isChecked:Z

    .line 19
    invoke-virtual {p0, p1}, Lcom/squareup/widgets/list/CheckableTextView;->setSelected(Z)V

    return-void
.end method

.method public toggle()V
    .locals 1

    .line 27
    iget-boolean v0, p0, Lcom/squareup/widgets/list/CheckableTextView;->isChecked:Z

    xor-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lcom/squareup/widgets/list/CheckableTextView;->setChecked(Z)V

    return-void
.end method
