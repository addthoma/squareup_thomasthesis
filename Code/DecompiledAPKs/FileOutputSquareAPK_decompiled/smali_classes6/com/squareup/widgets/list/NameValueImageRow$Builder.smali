.class public Lcom/squareup/widgets/list/NameValueImageRow$Builder;
.super Ljava/lang/Object;
.source "NameValueImageRow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/widgets/list/NameValueImageRow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private final context:Landroid/content/Context;

.field private name:Ljava/lang/CharSequence;

.field private valueResourceId:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/squareup/widgets/list/NameValueImageRow$Builder;->context:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/widgets/list/NameValueImageRow;
    .locals 3

    .line 47
    new-instance v0, Lcom/squareup/widgets/list/NameValueImageRow;

    iget-object v1, p0, Lcom/squareup/widgets/list/NameValueImageRow$Builder;->context:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/squareup/widgets/list/NameValueImageRow;-><init>(Landroid/content/Context;)V

    .line 48
    iget-object v1, p0, Lcom/squareup/widgets/list/NameValueImageRow$Builder;->name:Ljava/lang/CharSequence;

    iget v2, p0, Lcom/squareup/widgets/list/NameValueImageRow$Builder;->valueResourceId:I

    invoke-virtual {v0, v1, v2}, Lcom/squareup/widgets/list/NameValueImageRow;->update(Ljava/lang/CharSequence;I)V

    return-object v0
.end method

.method public name(I)Lcom/squareup/widgets/list/NameValueImageRow$Builder;
    .locals 1

    .line 38
    iget-object v0, p0, Lcom/squareup/widgets/list/NameValueImageRow$Builder;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/widgets/list/NameValueImageRow$Builder;->name(Ljava/lang/CharSequence;)Lcom/squareup/widgets/list/NameValueImageRow$Builder;

    move-result-object p1

    return-object p1
.end method

.method public name(Ljava/lang/CharSequence;)Lcom/squareup/widgets/list/NameValueImageRow$Builder;
    .locals 0

    .line 33
    iput-object p1, p0, Lcom/squareup/widgets/list/NameValueImageRow$Builder;->name:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public value(I)Lcom/squareup/widgets/list/NameValueImageRow$Builder;
    .locals 0

    .line 42
    iput p1, p0, Lcom/squareup/widgets/list/NameValueImageRow$Builder;->valueResourceId:I

    return-object p0
.end method
