.class public Lcom/squareup/widgets/TagPercentMarginLinearLayout;
.super Landroid/widget/LinearLayout;
.source "TagPercentMarginLinearLayout.java"


# static fields
.field public static final KEY:Ljava/lang/String; = "PERCENT_MARGIN_HERE"


# instance fields
.field private final paddingPercentage:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 23
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 25
    sget-object v0, Lcom/squareup/widgets/R$styleable;->TagPercentMarginLinearLayout:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 26
    sget p2, Lcom/squareup/widgets/R$styleable;->TagPercentMarginLinearLayout_marginPercentage:I

    const/high16 v0, 0x3f000000    # 0.5f

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result p2

    iput p2, p0, Lcom/squareup/widgets/TagPercentMarginLinearLayout;->paddingPercentage:F

    .line 27
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 5

    .line 31
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    int-to-float v0, v0

    .line 32
    iget v1, p0, Lcom/squareup/widgets/TagPercentMarginLinearLayout;->paddingPercentage:F

    mul-float v0, v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    float-to-int v0, v0

    const/4 v1, 0x0

    .line 33
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/widgets/TagPercentMarginLinearLayout;->getChildCount()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 34
    invoke-virtual {p0, v1}, Lcom/squareup/widgets/TagPercentMarginLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 35
    invoke-virtual {v2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    .line 36
    instance-of v4, v3, Ljava/lang/String;

    if-eqz v4, :cond_0

    const-string v4, "PERCENT_MARGIN_HERE"

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 37
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout$LayoutParams;

    .line 38
    iput v0, v2, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 39
    iput v0, v2, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 42
    :cond_1
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    return-void
.end method
