.class Lcom/squareup/widgets/dialog/internal/AlertController$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "AlertController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/widgets/dialog/internal/AlertController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/widgets/dialog/internal/AlertController;


# direct methods
.method constructor <init>(Lcom/squareup/widgets/dialog/internal/AlertController;)V
    .locals 0

    .line 86
    iput-object p1, p0, Lcom/squareup/widgets/dialog/internal/AlertController$1;->this$0:Lcom/squareup/widgets/dialog/internal/AlertController;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 2

    .line 89
    iget-object v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController$1;->this$0:Lcom/squareup/widgets/dialog/internal/AlertController;

    invoke-static {v0}, Lcom/squareup/widgets/dialog/internal/AlertController;->access$000(Lcom/squareup/widgets/dialog/internal/AlertController;)Landroid/widget/Button;

    move-result-object v0

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController$1;->this$0:Lcom/squareup/widgets/dialog/internal/AlertController;

    invoke-static {v0}, Lcom/squareup/widgets/dialog/internal/AlertController;->access$100(Lcom/squareup/widgets/dialog/internal/AlertController;)Landroid/os/Message;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 90
    iget-object p1, p0, Lcom/squareup/widgets/dialog/internal/AlertController$1;->this$0:Lcom/squareup/widgets/dialog/internal/AlertController;

    invoke-static {p1}, Lcom/squareup/widgets/dialog/internal/AlertController;->access$100(Lcom/squareup/widgets/dialog/internal/AlertController;)Landroid/os/Message;

    move-result-object p1

    invoke-static {p1}, Landroid/os/Message;->obtain(Landroid/os/Message;)Landroid/os/Message;

    move-result-object p1

    goto :goto_0

    .line 91
    :cond_0
    iget-object v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController$1;->this$0:Lcom/squareup/widgets/dialog/internal/AlertController;

    invoke-static {v0}, Lcom/squareup/widgets/dialog/internal/AlertController;->access$200(Lcom/squareup/widgets/dialog/internal/AlertController;)Landroid/widget/Button;

    move-result-object v0

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController$1;->this$0:Lcom/squareup/widgets/dialog/internal/AlertController;

    invoke-static {v0}, Lcom/squareup/widgets/dialog/internal/AlertController;->access$300(Lcom/squareup/widgets/dialog/internal/AlertController;)Landroid/os/Message;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 92
    iget-object p1, p0, Lcom/squareup/widgets/dialog/internal/AlertController$1;->this$0:Lcom/squareup/widgets/dialog/internal/AlertController;

    invoke-static {p1}, Lcom/squareup/widgets/dialog/internal/AlertController;->access$300(Lcom/squareup/widgets/dialog/internal/AlertController;)Landroid/os/Message;

    move-result-object p1

    invoke-static {p1}, Landroid/os/Message;->obtain(Landroid/os/Message;)Landroid/os/Message;

    move-result-object p1

    goto :goto_0

    .line 93
    :cond_1
    iget-object v0, p0, Lcom/squareup/widgets/dialog/internal/AlertController$1;->this$0:Lcom/squareup/widgets/dialog/internal/AlertController;

    invoke-static {v0}, Lcom/squareup/widgets/dialog/internal/AlertController;->access$400(Lcom/squareup/widgets/dialog/internal/AlertController;)Landroid/widget/Button;

    move-result-object v0

    if-ne p1, v0, :cond_2

    iget-object p1, p0, Lcom/squareup/widgets/dialog/internal/AlertController$1;->this$0:Lcom/squareup/widgets/dialog/internal/AlertController;

    invoke-static {p1}, Lcom/squareup/widgets/dialog/internal/AlertController;->access$500(Lcom/squareup/widgets/dialog/internal/AlertController;)Landroid/os/Message;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 94
    iget-object p1, p0, Lcom/squareup/widgets/dialog/internal/AlertController$1;->this$0:Lcom/squareup/widgets/dialog/internal/AlertController;

    invoke-static {p1}, Lcom/squareup/widgets/dialog/internal/AlertController;->access$500(Lcom/squareup/widgets/dialog/internal/AlertController;)Landroid/os/Message;

    move-result-object p1

    invoke-static {p1}, Landroid/os/Message;->obtain(Landroid/os/Message;)Landroid/os/Message;

    move-result-object p1

    goto :goto_0

    :cond_2
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_3

    .line 97
    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    .line 101
    :cond_3
    iget-object p1, p0, Lcom/squareup/widgets/dialog/internal/AlertController$1;->this$0:Lcom/squareup/widgets/dialog/internal/AlertController;

    invoke-static {p1}, Lcom/squareup/widgets/dialog/internal/AlertController;->access$700(Lcom/squareup/widgets/dialog/internal/AlertController;)Landroid/os/Handler;

    move-result-object p1

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/squareup/widgets/dialog/internal/AlertController$1;->this$0:Lcom/squareup/widgets/dialog/internal/AlertController;

    invoke-static {v1}, Lcom/squareup/widgets/dialog/internal/AlertController;->access$600(Lcom/squareup/widgets/dialog/internal/AlertController;)Landroid/content/DialogInterface;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object p1

    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    return-void
.end method
