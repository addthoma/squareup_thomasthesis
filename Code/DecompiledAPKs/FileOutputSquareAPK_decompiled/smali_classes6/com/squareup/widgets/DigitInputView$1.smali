.class Lcom/squareup/widgets/DigitInputView$1;
.super Lcom/squareup/text/EmptyTextWatcher;
.source "DigitInputView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/widgets/DigitInputView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/widgets/DigitInputView;

.field final synthetic val$isRightmost:Z

.field final synthetic val$position:I


# direct methods
.method constructor <init>(Lcom/squareup/widgets/DigitInputView;ZI)V
    .locals 0

    .line 82
    iput-object p1, p0, Lcom/squareup/widgets/DigitInputView$1;->this$0:Lcom/squareup/widgets/DigitInputView;

    iput-boolean p2, p0, Lcom/squareup/widgets/DigitInputView$1;->val$isRightmost:Z

    iput p3, p0, Lcom/squareup/widgets/DigitInputView$1;->val$position:I

    invoke-direct {p0}, Lcom/squareup/text/EmptyTextWatcher;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 3

    .line 84
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    const/4 v0, 0x0

    .line 86
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v2

    sub-int/2addr v2, v1

    invoke-interface {p1, v0, v2}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    goto :goto_0

    .line 87
    :cond_0
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result p1

    if-ne p1, v1, :cond_3

    .line 89
    iget-boolean p1, p0, Lcom/squareup/widgets/DigitInputView$1;->val$isRightmost:Z

    if-eqz p1, :cond_1

    .line 90
    iget-object p1, p0, Lcom/squareup/widgets/DigitInputView$1;->this$0:Lcom/squareup/widgets/DigitInputView;

    invoke-static {p1}, Lcom/squareup/widgets/DigitInputView;->access$000(Lcom/squareup/widgets/DigitInputView;)Lcom/squareup/widgets/DigitInputView$OnDigitsEnteredListener;

    move-result-object p1

    if-eqz p1, :cond_3

    .line 91
    iget-object p1, p0, Lcom/squareup/widgets/DigitInputView$1;->this$0:Lcom/squareup/widgets/DigitInputView;

    invoke-static {p1}, Lcom/squareup/widgets/DigitInputView;->access$000(Lcom/squareup/widgets/DigitInputView;)Lcom/squareup/widgets/DigitInputView$OnDigitsEnteredListener;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/widgets/DigitInputView$1;->this$0:Lcom/squareup/widgets/DigitInputView;

    invoke-virtual {v0}, Lcom/squareup/widgets/DigitInputView;->getDigits()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/widgets/DigitInputView$OnDigitsEnteredListener;->onDigitsEntered(Ljava/lang/String;)V

    goto :goto_0

    .line 94
    :cond_1
    iget-object p1, p0, Lcom/squareup/widgets/DigitInputView$1;->this$0:Lcom/squareup/widgets/DigitInputView;

    invoke-static {p1}, Lcom/squareup/widgets/DigitInputView;->access$000(Lcom/squareup/widgets/DigitInputView;)Lcom/squareup/widgets/DigitInputView$OnDigitsEnteredListener;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 95
    iget-object p1, p0, Lcom/squareup/widgets/DigitInputView$1;->this$0:Lcom/squareup/widgets/DigitInputView;

    invoke-static {p1}, Lcom/squareup/widgets/DigitInputView;->access$000(Lcom/squareup/widgets/DigitInputView;)Lcom/squareup/widgets/DigitInputView$OnDigitsEnteredListener;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/widgets/DigitInputView$OnDigitsEnteredListener;->onDigitEntered()V

    .line 97
    :cond_2
    iget-object p1, p0, Lcom/squareup/widgets/DigitInputView$1;->this$0:Lcom/squareup/widgets/DigitInputView;

    invoke-static {p1}, Lcom/squareup/widgets/DigitInputView;->access$100(Lcom/squareup/widgets/DigitInputView;)[Lcom/squareup/widgets/OnDeleteEditText;

    move-result-object p1

    iget v0, p0, Lcom/squareup/widgets/DigitInputView$1;->val$position:I

    add-int/2addr v0, v1

    aget-object p1, p1, v0

    invoke-virtual {p1}, Lcom/squareup/widgets/OnDeleteEditText;->requestFocus()Z

    :cond_3
    :goto_0
    return-void
.end method
