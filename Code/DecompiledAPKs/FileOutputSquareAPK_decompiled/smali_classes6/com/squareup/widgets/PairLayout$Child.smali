.class public final enum Lcom/squareup/widgets/PairLayout$Child;
.super Ljava/lang/Enum;
.source "PairLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/widgets/PairLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Child"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/widgets/PairLayout$Child;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/widgets/PairLayout$Child;

.field public static final enum FIRST:Lcom/squareup/widgets/PairLayout$Child;

.field public static final enum SECOND:Lcom/squareup/widgets/PairLayout$Child;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 86
    new-instance v0, Lcom/squareup/widgets/PairLayout$Child;

    const/4 v1, 0x0

    const-string v2, "FIRST"

    invoke-direct {v0, v2, v1}, Lcom/squareup/widgets/PairLayout$Child;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/widgets/PairLayout$Child;->FIRST:Lcom/squareup/widgets/PairLayout$Child;

    new-instance v0, Lcom/squareup/widgets/PairLayout$Child;

    const/4 v2, 0x1

    const-string v3, "SECOND"

    invoke-direct {v0, v3, v2}, Lcom/squareup/widgets/PairLayout$Child;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/widgets/PairLayout$Child;->SECOND:Lcom/squareup/widgets/PairLayout$Child;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/widgets/PairLayout$Child;

    sget-object v3, Lcom/squareup/widgets/PairLayout$Child;->FIRST:Lcom/squareup/widgets/PairLayout$Child;

    aput-object v3, v0, v1

    sget-object v1, Lcom/squareup/widgets/PairLayout$Child;->SECOND:Lcom/squareup/widgets/PairLayout$Child;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/widgets/PairLayout$Child;->$VALUES:[Lcom/squareup/widgets/PairLayout$Child;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 86
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/widgets/PairLayout$Child;
    .locals 1

    .line 86
    const-class v0, Lcom/squareup/widgets/PairLayout$Child;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/widgets/PairLayout$Child;

    return-object p0
.end method

.method public static values()[Lcom/squareup/widgets/PairLayout$Child;
    .locals 1

    .line 86
    sget-object v0, Lcom/squareup/widgets/PairLayout$Child;->$VALUES:[Lcom/squareup/widgets/PairLayout$Child;

    invoke-virtual {v0}, [Lcom/squareup/widgets/PairLayout$Child;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/widgets/PairLayout$Child;

    return-object v0
.end method
