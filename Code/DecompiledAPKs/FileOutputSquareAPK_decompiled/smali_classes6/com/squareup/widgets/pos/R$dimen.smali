.class public final Lcom/squareup/widgets/pos/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/widgets/pos/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final badge_margin_right:I = 0x7f070072

.field public static final color_picker_height:I = 0x7f0700cc

.field public static final duration_number_picker_margin:I = 0x7f070144

.field public static final grid_delete_button_circle_stroke:I = 0x7f070180

.field public static final grid_delete_button_x_stroke:I = 0x7f070183

.field public static final grid_plus_button_size:I = 0x7f070186

.field public static final grid_plus_button_stroke:I = 0x7f070187

.field public static final hud_corner_radius:I = 0x7f0701b9

.field public static final hud_edge:I = 0x7f0701ba

.field public static final hud_glyph_text_gap:I = 0x7f0701bb

.field public static final hud_message_text_margin_top:I = 0x7f0701bc

.field public static final hud_text_line_spacing_extra:I = 0x7f0701bd

.field public static final keypad_backspace_half_width:I = 0x7f0701d9

.field public static final pin_radius:I = 0x7f070434

.field public static final pin_star_gap:I = 0x7f070435

.field public static final pin_star_stroke:I = 0x7f070436

.field public static final progress_popup_title_line_spacing_extra:I = 0x7f070440

.field public static final quantity_button_width:I = 0x7f070442

.field public static final smart_line_row_badge_margin_bottom:I = 0x7f0704be


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
