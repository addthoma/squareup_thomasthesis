.class public final Lcom/squareup/widgets/pos/R$attr;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/widgets/pos/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "attr"
.end annotation


# static fields
.field public static final buttonGlyph:I = 0x7f04007e

.field public static final buttonGlyphBackground:I = 0x7f04007f

.field public static final buttonGlyphColor:I = 0x7f040080

.field public static final buttonGlyphHint:I = 0x7f040081

.field public static final chevronVisibility:I = 0x7f0400a9

.field public static final columnCount:I = 0x7f0400e9

.field public static final compact:I = 0x7f0400eb

.field public static final dismissOnClick:I = 0x7f040129

.field public static final displayValueAsPercent:I = 0x7f04012b

.field public static final dominantMeasurement:I = 0x7f040132

.field public static final errorRowHeight:I = 0x7f040164

.field public static final gap:I = 0x7f04019d

.field public static final glyph:I = 0x7f0401a0

.field public static final glyphSpacing:I = 0x7f0401a9

.field public static final hideBackground:I = 0x7f0401b7

.field public static final hideGlyph:I = 0x7f0401b9

.field public static final landscapePhoneAsCard:I = 0x7f040236

.field public static final lineRowPadding:I = 0x7f04027f

.field public static final minCellWidth:I = 0x7f0402c1

.field public static final preserveValueText:I = 0x7f04031b

.field public static final rowHeight:I = 0x7f040331

.field public static final shortText:I = 0x7f040347

.field public static final showXOnFocus:I = 0x7f040351

.field public static final showXOnNonEmptyText:I = 0x7f040352

.field public static final sqInnerShadowColor:I = 0x7f040388

.field public static final sqInnerShadowHeight:I = 0x7f040389

.field public static final sq_aspectRatio:I = 0x7f0403c2

.field public static final sq_aspectRatioEnabled:I = 0x7f0403c3

.field public static final sq_glyphColor:I = 0x7f0403c4

.field public static final sq_labelText:I = 0x7f0403c5

.field public static final sq_noteText:I = 0x7f0403c7

.field public static final sq_subtitleTextAppearance:I = 0x7f0403c8

.field public static final sq_titleTextAppearance:I = 0x7f0403c9

.field public static final sq_valueColor:I = 0x7f0403ca

.field public static final sq_valueText:I = 0x7f0403cb

.field public static final sq_valueTextAppearance:I = 0x7f0403cc

.field public static final starFailColor:I = 0x7f0403da

.field public static final starGap:I = 0x7f0403db

.field public static final starRadius:I = 0x7f0403dc

.field public static final starSuccessColor:I = 0x7f0403dd

.field public static final subtitleText:I = 0x7f040403

.field public static final titleText:I = 0x7f040466

.field public static final titleTextColor:I = 0x7f040468

.field public static final titleTextSize:I = 0x7f040469

.field public static final valueWeight:I = 0x7f040492

.field public static final weight:I = 0x7f040497

.field public static final xSelector:I = 0x7f0404a8


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
