.class public final Lcom/squareup/widgets/pos/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/widgets/pos/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final button_minus_content_description:I = 0x7f1201ce

.field public static final button_plus_content_description:I = 0x7f1201d0

.field public static final ce_card_number_hint:I = 0x7f1203e2

.field public static final ce_card_number_hint_focused:I = 0x7f1203e3

.field public static final ce_card_number_last_four_content_description:I = 0x7f1203e8

.field public static final clear_text_box_content_description:I = 0x7f120427

.field public static final color_blue:I = 0x7f120436

.field public static final color_brown:I = 0x7f120437

.field public static final color_cyan:I = 0x7f120438

.field public static final color_gray:I = 0x7f120439

.field public static final color_green:I = 0x7f12043a

.field public static final color_light_green:I = 0x7f12043b

.field public static final color_pink:I = 0x7f12043c

.field public static final color_purple:I = 0x7f12043d

.field public static final color_red:I = 0x7f12043e

.field public static final color_yellow:I = 0x7f12043f

.field public static final confirmation_popup_resume:I = 0x7f120488

.field public static final cvv:I = 0x7f1207bd

.field public static final cvv_hint:I = 0x7f1207be

.field public static final date_picker_month_format:I = 0x7f1207cc

.field public static final date_picker_next_month:I = 0x7f1207cd

.field public static final date_picker_previous_month:I = 0x7f1207ce

.field public static final date_picker_provide_year:I = 0x7f1207cf

.field public static final date_picker_remove:I = 0x7f1207d0

.field public static final date_selected:I = 0x7f1207d2

.field public static final delete_button_confirm_stage_text:I = 0x7f1207e6

.field public static final duration_cancel:I = 0x7f1208f6

.field public static final duration_save:I = 0x7f1208f7

.field public static final duration_title:I = 0x7f1208f8

.field public static final edit_item_confirm_discard_changes_dialog_message:I = 0x7f12090e

.field public static final edit_item_confirm_discard_changes_dialog_title:I = 0x7f12090f

.field public static final expiration_content_description:I = 0x7f120a98

.field public static final expiration_date_content_description:I = 0x7f120a99

.field public static final expiration_date_hint:I = 0x7f120a9a

.field public static final expiration_date_label:I = 0x7f120a9b

.field public static final expiration_hint:I = 0x7f120a9c

.field public static final hud:I = 0x7f120bdf

.field public static final latin_digit_one:I = 0x7f120eb3

.field public static final new_item:I = 0x7f121068

.field public static final payment_types_settings_empty:I = 0x7f12140d

.field public static final payment_types_settings_empty_disabled:I = 0x7f12140e

.field public static final postal_keyboard_switch:I = 0x7f121459

.field public static final tip_row_format:I = 0x7f1219c3


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 279
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
