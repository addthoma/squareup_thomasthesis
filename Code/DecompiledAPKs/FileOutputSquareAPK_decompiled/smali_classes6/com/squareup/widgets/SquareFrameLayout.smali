.class public Lcom/squareup/widgets/SquareFrameLayout;
.super Landroid/widget/FrameLayout;
.source "SquareFrameLayout.java"


# static fields
.field private static generatedInPortait:Z = false

.field private static screenHeight:I = -0x1

.field private static screenWidth:I = -0x1


# instance fields
.field private final isLandscape:Z

.field private final panelHeight:I

.field private final panelWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 61
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 63
    invoke-virtual {p0}, Lcom/squareup/widgets/SquareFrameLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/widgets/SquareFrameLayout;->getScreenDimens(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object p1

    .line 64
    iget p2, p1, Landroid/graphics/Point;->x:I

    iput p2, p0, Lcom/squareup/widgets/SquareFrameLayout;->panelWidth:I

    .line 65
    iget p1, p1, Landroid/graphics/Point;->y:I

    iput p1, p0, Lcom/squareup/widgets/SquareFrameLayout;->panelHeight:I

    .line 66
    iget p1, p0, Lcom/squareup/widgets/SquareFrameLayout;->panelWidth:I

    iget p2, p0, Lcom/squareup/widgets/SquareFrameLayout;->panelHeight:I

    if-le p1, p2, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput-boolean p1, p0, Lcom/squareup/widgets/SquareFrameLayout;->isLandscape:Z

    return-void
.end method

.method public static getScreenDimens(Landroid/content/Context;)Landroid/graphics/Point;
    .locals 2

    .line 30
    sget v0, Lcom/squareup/widgets/SquareFrameLayout;->screenWidth:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    sget v0, Lcom/squareup/widgets/SquareFrameLayout;->screenHeight:I

    if-ne v0, v1, :cond_1

    .line 31
    :cond_0
    invoke-static {p0}, Lcom/squareup/widgets/SquareFrameLayout;->initDimens(Landroid/content/Context;)V

    .line 34
    :cond_1
    sget-boolean v0, Lcom/squareup/widgets/SquareFrameLayout;->generatedInPortait:Z

    invoke-static {p0}, Lcom/squareup/widgets/SquareFrameLayout;->isPortrait(Landroid/content/Context;)Z

    move-result p0

    if-ne v0, p0, :cond_2

    .line 35
    new-instance p0, Landroid/graphics/Point;

    sget v0, Lcom/squareup/widgets/SquareFrameLayout;->screenWidth:I

    sget v1, Lcom/squareup/widgets/SquareFrameLayout;->screenHeight:I

    invoke-direct {p0, v0, v1}, Landroid/graphics/Point;-><init>(II)V

    return-object p0

    .line 37
    :cond_2
    new-instance p0, Landroid/graphics/Point;

    sget v0, Lcom/squareup/widgets/SquareFrameLayout;->screenHeight:I

    sget v1, Lcom/squareup/widgets/SquareFrameLayout;->screenWidth:I

    invoke-direct {p0, v0, v1}, Landroid/graphics/Point;-><init>(II)V

    return-object p0
.end method

.method private static initDimens(Landroid/content/Context;)V
    .locals 2

    const-string v0, "window"

    .line 42
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 43
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 44
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 45
    invoke-virtual {v0, v1}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    .line 46
    iget v0, v1, Landroid/graphics/Point;->x:I

    sput v0, Lcom/squareup/widgets/SquareFrameLayout;->screenWidth:I

    .line 47
    iget v0, v1, Landroid/graphics/Point;->y:I

    sput v0, Lcom/squareup/widgets/SquareFrameLayout;->screenHeight:I

    .line 48
    invoke-static {p0}, Lcom/squareup/widgets/SquareFrameLayout;->isPortrait(Landroid/content/Context;)Z

    move-result p0

    sput-boolean p0, Lcom/squareup/widgets/SquareFrameLayout;->generatedInPortait:Z

    return-void
.end method

.method public static isPortrait(Landroid/content/Context;)Z
    .locals 1

    .line 52
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    invoke-virtual {p0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object p0

    .line 53
    iget p0, p0, Landroid/content/res/Configuration;->orientation:I

    const/4 v0, 0x1

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 4

    .line 70
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 71
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 75
    iget-boolean v2, p0, Lcom/squareup/widgets/SquareFrameLayout;->isLandscape:Z

    const/4 v3, 0x0

    if-eqz v2, :cond_0

    .line 76
    iget v1, p0, Lcom/squareup/widgets/SquareFrameLayout;->panelHeight:I

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    goto :goto_0

    .line 80
    :cond_0
    iget v0, p0, Lcom/squareup/widgets/SquareFrameLayout;->panelWidth:I

    sub-int/2addr v1, v0

    div-int/lit8 v0, v1, 0x2

    move v3, v0

    const/4 v0, 0x0

    .line 83
    :goto_0
    invoke-virtual {p0, v0, v3, v0, v3}, Lcom/squareup/widgets/SquareFrameLayout;->setPadding(IIII)V

    .line 86
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    return-void
.end method
