.class public Lcom/squareup/widgets/CheckableGroup;
.super Landroid/widget/LinearLayout;
.source "CheckableGroup.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/widgets/CheckableGroup$PassThroughHierarchyChangeListener;,
        Lcom/squareup/widgets/CheckableGroup$CheckedStateTracker;,
        Lcom/squareup/widgets/CheckableGroup$OnCheckedClickListener;,
        Lcom/squareup/widgets/CheckableGroup$OnCheckedChangeListener;,
        Lcom/squareup/widgets/CheckableGroup$LayoutParams;
    }
.end annotation


# static fields
.field private static final CHECKED_IDS:Ljava/lang/String; = "checkedId"

.field private static final SUPER_STATE:Ljava/lang/String; = "super"


# instance fields
.field private addingColumns:Z

.field private final checkedIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private leftColumnLayout:Landroid/widget/LinearLayout;

.field private mChildOnCheckedChangeListener:Lcom/squareup/widgets/CheckableGroup$CheckedStateTracker;

.field private mOnCheckedChangeListener:Lcom/squareup/widgets/CheckableGroup$OnCheckedChangeListener;

.field private mOnCheckedClickListener:Lcom/squareup/widgets/CheckableGroup$OnCheckedClickListener;

.field private mPassThroughListener:Lcom/squareup/widgets/CheckableGroup$PassThroughHierarchyChangeListener;

.field private mProtectFromCheckedChange:Z

.field private restoringState:Z

.field private rightColumnLayout:Landroid/widget/LinearLayout;

.field private singleChoice:Z

.field private final twoColumns:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7

    .line 118
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    .line 103
    iput-boolean v0, p0, Lcom/squareup/widgets/CheckableGroup;->mProtectFromCheckedChange:Z

    .line 112
    iput-boolean v0, p0, Lcom/squareup/widgets/CheckableGroup;->addingColumns:Z

    .line 122
    sget-object v1, Lcom/squareup/widgets/R$styleable;->CheckableGroup:[I

    const v2, 0x101007e

    invoke-virtual {p1, p2, v1, v2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p2

    .line 125
    sget v1, Lcom/squareup/widgets/R$styleable;->CheckableGroup_singleChoice:I

    const/4 v2, 0x1

    invoke-virtual {p2, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/squareup/widgets/CheckableGroup;->singleChoice:Z

    .line 126
    sget v1, Lcom/squareup/widgets/R$styleable;->CheckableGroup_twoColumns:I

    invoke-virtual {p2, v1, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/squareup/widgets/CheckableGroup;->twoColumns:Z

    .line 127
    sget v1, Lcom/squareup/widgets/R$styleable;->CheckableGroup_sqCheckedButton:I

    const/4 v3, -0x1

    invoke-virtual {p2, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 128
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    iput-object v4, p0, Lcom/squareup/widgets/CheckableGroup;->checkedIds:Ljava/util/Set;

    if-eq v1, v3, :cond_0

    .line 130
    iget-object v3, p0, Lcom/squareup/widgets/CheckableGroup;->checkedIds:Ljava/util/Set;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v3, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 133
    :cond_0
    new-instance v1, Lcom/squareup/widgets/CheckableGroup$CheckedStateTracker;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v3}, Lcom/squareup/widgets/CheckableGroup$CheckedStateTracker;-><init>(Lcom/squareup/widgets/CheckableGroup;Lcom/squareup/widgets/CheckableGroup$1;)V

    iput-object v1, p0, Lcom/squareup/widgets/CheckableGroup;->mChildOnCheckedChangeListener:Lcom/squareup/widgets/CheckableGroup$CheckedStateTracker;

    .line 134
    new-instance v1, Lcom/squareup/widgets/CheckableGroup$PassThroughHierarchyChangeListener;

    invoke-direct {v1, p0, v3}, Lcom/squareup/widgets/CheckableGroup$PassThroughHierarchyChangeListener;-><init>(Lcom/squareup/widgets/CheckableGroup;Lcom/squareup/widgets/CheckableGroup$1;)V

    iput-object v1, p0, Lcom/squareup/widgets/CheckableGroup;->mPassThroughListener:Lcom/squareup/widgets/CheckableGroup$PassThroughHierarchyChangeListener;

    .line 136
    iget-boolean v1, p0, Lcom/squareup/widgets/CheckableGroup;->twoColumns:Z

    if-eqz v1, :cond_2

    .line 137
    invoke-virtual {p0, v0}, Lcom/squareup/widgets/CheckableGroup;->setOrientation(I)V

    .line 139
    invoke-virtual {p0}, Lcom/squareup/widgets/CheckableGroup;->getShowDividers()I

    move-result v1

    .line 140
    sget v4, Lcom/squareup/widgets/R$styleable;->CheckableGroup_android_divider:I

    invoke-virtual {p2, v4}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 142
    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    goto :goto_0

    :cond_1
    const/4 v5, 0x0

    .line 145
    :goto_0
    new-instance v6, Landroid/widget/LinearLayout;

    invoke-direct {v6, p1, v3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v6, p0, Lcom/squareup/widgets/CheckableGroup;->leftColumnLayout:Landroid/widget/LinearLayout;

    .line 146
    iget-object v6, p0, Lcom/squareup/widgets/CheckableGroup;->leftColumnLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v6, v2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 147
    iget-object v6, p0, Lcom/squareup/widgets/CheckableGroup;->leftColumnLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v6, v4}, Landroid/widget/LinearLayout;->setDividerDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 148
    iget-object v6, p0, Lcom/squareup/widgets/CheckableGroup;->leftColumnLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v6, v1}, Landroid/widget/LinearLayout;->setShowDividers(I)V

    .line 149
    new-instance v6, Landroid/widget/LinearLayout;

    invoke-direct {v6, p1, v3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v6, p0, Lcom/squareup/widgets/CheckableGroup;->rightColumnLayout:Landroid/widget/LinearLayout;

    .line 150
    iget-object p1, p0, Lcom/squareup/widgets/CheckableGroup;->rightColumnLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 151
    iget-object p1, p0, Lcom/squareup/widgets/CheckableGroup;->rightColumnLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v4}, Landroid/widget/LinearLayout;->setDividerDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 152
    iget-object p1, p0, Lcom/squareup/widgets/CheckableGroup;->rightColumnLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v1}, Landroid/widget/LinearLayout;->setShowDividers(I)V

    .line 154
    new-instance p1, Lcom/squareup/widgets/CheckableGroup$LayoutParams;

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v3, -0x2

    invoke-direct {p1, v0, v3, v1}, Lcom/squareup/widgets/CheckableGroup$LayoutParams;-><init>(IIF)V

    .line 155
    new-instance v4, Lcom/squareup/widgets/CheckableGroup$LayoutParams;

    invoke-direct {v4, v0, v3, v1}, Lcom/squareup/widgets/CheckableGroup$LayoutParams;-><init>(IIF)V

    .line 156
    invoke-virtual {v4, v5, v0, v0, v0}, Lcom/squareup/widgets/CheckableGroup$LayoutParams;->setMargins(IIII)V

    .line 157
    iput-boolean v2, p0, Lcom/squareup/widgets/CheckableGroup;->addingColumns:Z

    .line 158
    iget-object v1, p0, Lcom/squareup/widgets/CheckableGroup;->leftColumnLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v1, p1}, Lcom/squareup/widgets/CheckableGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 159
    iget-object p1, p0, Lcom/squareup/widgets/CheckableGroup;->rightColumnLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p0, p1, v4}, Lcom/squareup/widgets/CheckableGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 160
    iput-boolean v0, p0, Lcom/squareup/widgets/CheckableGroup;->addingColumns:Z

    .line 162
    iget-object p1, p0, Lcom/squareup/widgets/CheckableGroup;->leftColumnLayout:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/squareup/widgets/CheckableGroup;->mPassThroughListener:Lcom/squareup/widgets/CheckableGroup$PassThroughHierarchyChangeListener;

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setOnHierarchyChangeListener(Landroid/view/ViewGroup$OnHierarchyChangeListener;)V

    .line 163
    iget-object p1, p0, Lcom/squareup/widgets/CheckableGroup;->rightColumnLayout:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/squareup/widgets/CheckableGroup;->mPassThroughListener:Lcom/squareup/widgets/CheckableGroup$PassThroughHierarchyChangeListener;

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setOnHierarchyChangeListener(Landroid/view/ViewGroup$OnHierarchyChangeListener;)V

    goto :goto_1

    .line 165
    :cond_2
    sget p1, Lcom/squareup/widgets/R$styleable;->CheckableGroup_isHorizontal:I

    invoke-virtual {p2, p1, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p1

    xor-int/2addr p1, v2

    .line 166
    invoke-virtual {p0, p1}, Lcom/squareup/widgets/CheckableGroup;->setOrientation(I)V

    .line 167
    iget-object p1, p0, Lcom/squareup/widgets/CheckableGroup;->mPassThroughListener:Lcom/squareup/widgets/CheckableGroup$PassThroughHierarchyChangeListener;

    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setOnHierarchyChangeListener(Landroid/view/ViewGroup$OnHierarchyChangeListener;)V

    .line 170
    :goto_1
    invoke-virtual {p2}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method static synthetic access$1000(Lcom/squareup/widgets/CheckableGroup;)Landroid/widget/LinearLayout;
    .locals 0

    .line 93
    iget-object p0, p0, Lcom/squareup/widgets/CheckableGroup;->rightColumnLayout:Landroid/widget/LinearLayout;

    return-object p0
.end method

.method static synthetic access$1100(Lcom/squareup/widgets/CheckableGroup;)Lcom/squareup/widgets/CheckableGroup$CheckedStateTracker;
    .locals 0

    .line 93
    iget-object p0, p0, Lcom/squareup/widgets/CheckableGroup;->mChildOnCheckedChangeListener:Lcom/squareup/widgets/CheckableGroup$CheckedStateTracker;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/widgets/CheckableGroup;)Z
    .locals 0

    .line 93
    iget-boolean p0, p0, Lcom/squareup/widgets/CheckableGroup;->mProtectFromCheckedChange:Z

    return p0
.end method

.method static synthetic access$302(Lcom/squareup/widgets/CheckableGroup;Z)Z
    .locals 0

    .line 93
    iput-boolean p1, p0, Lcom/squareup/widgets/CheckableGroup;->mProtectFromCheckedChange:Z

    return p1
.end method

.method static synthetic access$400(Lcom/squareup/widgets/CheckableGroup;)Ljava/util/Set;
    .locals 0

    .line 93
    iget-object p0, p0, Lcom/squareup/widgets/CheckableGroup;->checkedIds:Ljava/util/Set;

    return-object p0
.end method

.method static synthetic access$500(Lcom/squareup/widgets/CheckableGroup;)Z
    .locals 0

    .line 93
    iget-boolean p0, p0, Lcom/squareup/widgets/CheckableGroup;->singleChoice:Z

    return p0
.end method

.method static synthetic access$600(Lcom/squareup/widgets/CheckableGroup;IZ)Z
    .locals 0

    .line 93
    invoke-direct {p0, p1, p2}, Lcom/squareup/widgets/CheckableGroup;->setCheckedStateForView(IZ)Z

    move-result p0

    return p0
.end method

.method static synthetic access$700(Lcom/squareup/widgets/CheckableGroup;I)V
    .locals 0

    .line 93
    invoke-direct {p0, p1}, Lcom/squareup/widgets/CheckableGroup;->setCheckedId(I)V

    return-void
.end method

.method static synthetic access$800(Lcom/squareup/widgets/CheckableGroup;)Lcom/squareup/widgets/CheckableGroup$OnCheckedClickListener;
    .locals 0

    .line 93
    iget-object p0, p0, Lcom/squareup/widgets/CheckableGroup;->mOnCheckedClickListener:Lcom/squareup/widgets/CheckableGroup$OnCheckedClickListener;

    return-object p0
.end method

.method static synthetic access$900(Lcom/squareup/widgets/CheckableGroup;)Landroid/widget/LinearLayout;
    .locals 0

    .line 93
    iget-object p0, p0, Lcom/squareup/widgets/CheckableGroup;->leftColumnLayout:Landroid/widget/LinearLayout;

    return-object p0
.end method

.method private setCheckedId(I)V
    .locals 3

    .line 293
    iget-boolean v0, p0, Lcom/squareup/widgets/CheckableGroup;->singleChoice:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/widgets/CheckableGroup;->checkedIds:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 294
    iget-object v0, p0, Lcom/squareup/widgets/CheckableGroup;->checkedIds:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 295
    iget-object v1, p0, Lcom/squareup/widgets/CheckableGroup;->checkedIds:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    const/4 v0, -0x1

    .line 299
    :goto_0
    iget-object v1, p0, Lcom/squareup/widgets/CheckableGroup;->checkedIds:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 302
    iget-object v1, p0, Lcom/squareup/widgets/CheckableGroup;->mOnCheckedChangeListener:Lcom/squareup/widgets/CheckableGroup$OnCheckedChangeListener;

    if-eqz v1, :cond_1

    iget-boolean v2, p0, Lcom/squareup/widgets/CheckableGroup;->restoringState:Z

    if-nez v2, :cond_1

    .line 303
    invoke-interface {v1, p0, p1, v0}, Lcom/squareup/widgets/CheckableGroup$OnCheckedChangeListener;->onCheckedChanged(Lcom/squareup/widgets/CheckableGroup;II)V

    :cond_1
    return-void
.end method

.method private setCheckedStateForView(IZ)Z
    .locals 1

    .line 309
    invoke-virtual {p0, p1}, Lcom/squareup/widgets/CheckableGroup;->findViewById(I)Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 310
    instance-of v0, p1, Landroid/widget/Checkable;

    if-eqz v0, :cond_0

    .line 311
    check-cast p1, Landroid/widget/Checkable;

    invoke-interface {p1, p2}, Landroid/widget/Checkable;->setChecked(Z)V

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method


# virtual methods
.method public addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .locals 2

    .line 192
    iget-boolean v0, p0, Lcom/squareup/widgets/CheckableGroup;->addingColumns:Z

    if-eqz v0, :cond_0

    .line 193
    invoke-super {p0, p1, p2, p3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    return-void

    .line 196
    :cond_0
    instance-of v0, p1, Landroid/widget/Checkable;

    if-eqz v0, :cond_2

    .line 197
    move-object v0, p1

    check-cast v0, Landroid/widget/Checkable;

    .line 198
    invoke-interface {v0}, Landroid/widget/Checkable;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    .line 199
    iput-boolean v0, p0, Lcom/squareup/widgets/CheckableGroup;->mProtectFromCheckedChange:Z

    .line 201
    iget-object v0, p0, Lcom/squareup/widgets/CheckableGroup;->checkedIds:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/squareup/widgets/CheckableGroup;->singleChoice:Z

    if-eqz v0, :cond_1

    .line 202
    iget-object v0, p0, Lcom/squareup/widgets/CheckableGroup;->checkedIds:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 203
    invoke-direct {p0, v0, v1}, Lcom/squareup/widgets/CheckableGroup;->setCheckedStateForView(IZ)Z

    .line 206
    :cond_1
    iput-boolean v1, p0, Lcom/squareup/widgets/CheckableGroup;->mProtectFromCheckedChange:Z

    .line 207
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/squareup/widgets/CheckableGroup;->setCheckedId(I)V

    .line 211
    :cond_2
    iget-boolean v0, p0, Lcom/squareup/widgets/CheckableGroup;->twoColumns:Z

    if-eqz v0, :cond_4

    .line 212
    iget-object p2, p0, Lcom/squareup/widgets/CheckableGroup;->leftColumnLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p2}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result p2

    iget-object v0, p0, Lcom/squareup/widgets/CheckableGroup;->rightColumnLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    if-ne p2, v0, :cond_3

    .line 213
    iget-object p2, p0, Lcom/squareup/widgets/CheckableGroup;->leftColumnLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p2, p1, p3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 215
    :cond_3
    iget-object p2, p0, Lcom/squareup/widgets/CheckableGroup;->rightColumnLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p2, p1, p3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 218
    :cond_4
    invoke-super {p0, p1, p2, p3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    :goto_0
    return-void
.end method

.method public check(I)V
    .locals 3

    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .line 245
    iget-object v1, p0, Lcom/squareup/widgets/CheckableGroup;->checkedIds:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    return-void

    .line 250
    :cond_0
    iget-object v1, p0, Lcom/squareup/widgets/CheckableGroup;->checkedIds:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/squareup/widgets/CheckableGroup;->singleChoice:Z

    if-eqz v1, :cond_1

    .line 251
    iget-object v1, p0, Lcom/squareup/widgets/CheckableGroup;->checkedIds:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x0

    .line 252
    invoke-direct {p0, v1, v2}, Lcom/squareup/widgets/CheckableGroup;->setCheckedStateForView(IZ)Z

    :cond_1
    if-eq p1, v0, :cond_2

    const/4 v0, 0x1

    .line 256
    invoke-direct {p0, p1, v0}, Lcom/squareup/widgets/CheckableGroup;->setCheckedStateForView(IZ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 257
    invoke-direct {p0, p1}, Lcom/squareup/widgets/CheckableGroup;->setCheckedId(I)V

    goto :goto_0

    .line 259
    :cond_2
    invoke-virtual {p0}, Lcom/squareup/widgets/CheckableGroup;->clearChecked()V

    :goto_0
    return-void
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 0

    .line 397
    instance-of p1, p1, Lcom/squareup/widgets/CheckableGroup$LayoutParams;

    return p1
.end method

.method public clearChecked()V
    .locals 3

    .line 361
    iget-object v0, p0, Lcom/squareup/widgets/CheckableGroup;->checkedIds:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x0

    .line 362
    invoke-direct {p0, v1, v2}, Lcom/squareup/widgets/CheckableGroup;->setCheckedStateForView(IZ)Z

    goto :goto_0

    .line 364
    :cond_0
    iget-object v0, p0, Lcom/squareup/widgets/CheckableGroup;->checkedIds:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    return-void
.end method

.method protected bridge synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .line 93
    invoke-virtual {p0}, Lcom/squareup/widgets/CheckableGroup;->generateDefaultLayoutParams()Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method protected generateDefaultLayoutParams()Landroid/widget/LinearLayout$LayoutParams;
    .locals 2

    .line 401
    new-instance v0, Lcom/squareup/widgets/CheckableGroup$LayoutParams;

    const/4 v1, -0x2

    invoke-direct {v0, v1, v1}, Lcom/squareup/widgets/CheckableGroup$LayoutParams;-><init>(II)V

    return-object v0
.end method

.method public bridge synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 0

    .line 93
    invoke-virtual {p0, p1}, Lcom/squareup/widgets/CheckableGroup;->generateLayoutParams(Landroid/util/AttributeSet;)Lcom/squareup/widgets/CheckableGroup$LayoutParams;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/widget/LinearLayout$LayoutParams;
    .locals 0

    .line 93
    invoke-virtual {p0, p1}, Lcom/squareup/widgets/CheckableGroup;->generateLayoutParams(Landroid/util/AttributeSet;)Lcom/squareup/widgets/CheckableGroup$LayoutParams;

    move-result-object p1

    return-object p1
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Lcom/squareup/widgets/CheckableGroup$LayoutParams;
    .locals 2

    .line 392
    new-instance v0, Lcom/squareup/widgets/CheckableGroup$LayoutParams;

    invoke-virtual {p0}, Lcom/squareup/widgets/CheckableGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/squareup/widgets/CheckableGroup$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method public getCheckedId()I
    .locals 2

    .line 341
    iget-boolean v0, p0, Lcom/squareup/widgets/CheckableGroup;->singleChoice:Z

    if-eqz v0, :cond_1

    .line 344
    iget-object v0, p0, Lcom/squareup/widgets/CheckableGroup;->checkedIds:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    return v0

    .line 345
    :cond_0
    iget-object v0, p0, Lcom/squareup/widgets/CheckableGroup;->checkedIds:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0

    .line 342
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t get single checked Id in multiChoice mode!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getCheckedIds()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 327
    iget-object v0, p0, Lcom/squareup/widgets/CheckableGroup;->checkedIds:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    return-object v0

    .line 328
    :cond_0
    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, Lcom/squareup/widgets/CheckableGroup;->checkedIds:Ljava/util/Set;

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public getOnCheckedChangeListener()Lcom/squareup/widgets/CheckableGroup$OnCheckedChangeListener;
    .locals 1

    .line 377
    iget-object v0, p0, Lcom/squareup/widgets/CheckableGroup;->mOnCheckedChangeListener:Lcom/squareup/widgets/CheckableGroup$OnCheckedChangeListener;

    return-object v0
.end method

.method public isChecked()Z
    .locals 1

    .line 350
    iget-object v0, p0, Lcom/squareup/widgets/CheckableGroup;->checkedIds:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method protected onFinishInflate()V
    .locals 3

    .line 181
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    const/4 v0, 0x1

    .line 184
    iput-boolean v0, p0, Lcom/squareup/widgets/CheckableGroup;->mProtectFromCheckedChange:Z

    .line 185
    iget-object v1, p0, Lcom/squareup/widgets/CheckableGroup;->checkedIds:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 186
    invoke-direct {p0, v2, v0}, Lcom/squareup/widgets/CheckableGroup;->setCheckedStateForView(IZ)Z

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 188
    iput-boolean v0, p0, Lcom/squareup/widgets/CheckableGroup;->mProtectFromCheckedChange:Z

    return-void
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    .line 405
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 406
    const-class v0, Lcom/squareup/widgets/CheckableGroup;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1

    .line 410
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 411
    const-class v0, Lcom/squareup/widgets/CheckableGroup;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 4

    .line 428
    check-cast p1, Landroid/os/Bundle;

    const-string v0, "super"

    .line 429
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/LinearLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    const-string v0, "checkedId"

    .line 430
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object p1

    const/4 v0, 0x1

    .line 431
    iput-boolean v0, p0, Lcom/squareup/widgets/CheckableGroup;->restoringState:Z

    .line 432
    array-length v0, p1

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    aget v3, p1, v2

    .line 433
    invoke-virtual {p0, v3}, Lcom/squareup/widgets/CheckableGroup;->check(I)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 435
    :cond_0
    iput-boolean v1, p0, Lcom/squareup/widgets/CheckableGroup;->restoringState:Z

    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 6

    .line 415
    invoke-super {p0}, Landroid/widget/LinearLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 416
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "super"

    .line 417
    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 418
    iget-object v0, p0, Lcom/squareup/widgets/CheckableGroup;->checkedIds:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    new-array v0, v0, [I

    .line 420
    iget-object v2, p0, Lcom/squareup/widgets/CheckableGroup;->checkedIds:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    const/4 v3, 0x0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    add-int/lit8 v5, v3, 0x1

    .line 421
    aput v4, v0, v3

    move v3, v5

    goto :goto_0

    :cond_0
    const-string v2, "checkedId"

    .line 423
    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    return-object v1
.end method

.method public setOnCheckedChangeListener(Lcom/squareup/widgets/CheckableGroup$OnCheckedChangeListener;)V
    .locals 0

    .line 373
    iput-object p1, p0, Lcom/squareup/widgets/CheckableGroup;->mOnCheckedChangeListener:Lcom/squareup/widgets/CheckableGroup$OnCheckedChangeListener;

    return-void
.end method

.method public setOnCheckedClickListener(Lcom/squareup/widgets/CheckableGroup$OnCheckedClickListener;)V
    .locals 0

    .line 387
    iput-object p1, p0, Lcom/squareup/widgets/CheckableGroup;->mOnCheckedClickListener:Lcom/squareup/widgets/CheckableGroup$OnCheckedClickListener;

    return-void
.end method

.method public setOnHierarchyChangeListener(Landroid/view/ViewGroup$OnHierarchyChangeListener;)V
    .locals 1

    .line 176
    iget-object v0, p0, Lcom/squareup/widgets/CheckableGroup;->mPassThroughListener:Lcom/squareup/widgets/CheckableGroup$PassThroughHierarchyChangeListener;

    invoke-static {v0, p1}, Lcom/squareup/widgets/CheckableGroup$PassThroughHierarchyChangeListener;->access$202(Lcom/squareup/widgets/CheckableGroup$PassThroughHierarchyChangeListener;Landroid/view/ViewGroup$OnHierarchyChangeListener;)Landroid/view/ViewGroup$OnHierarchyChangeListener;

    return-void
.end method

.method public setSingleChoiceMode(Z)V
    .locals 0

    .line 230
    iput-boolean p1, p0, Lcom/squareup/widgets/CheckableGroup;->singleChoice:Z

    if-eqz p1, :cond_0

    .line 231
    invoke-virtual {p0}, Lcom/squareup/widgets/CheckableGroup;->clearChecked()V

    :cond_0
    return-void
.end method

.method public startAnimationOnCheckable(ILandroid/view/animation/Animation;)V
    .locals 0

    .line 283
    invoke-virtual {p0, p1}, Lcom/squareup/widgets/CheckableGroup;->findViewById(I)Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 287
    invoke-virtual {p1, p2}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    return-void

    .line 285
    :cond_0
    new-instance p1, Ljava/security/InvalidParameterException;

    const-string p2, "The checkable doesn\'t exist."

    invoke-direct {p1, p2}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public toggle(I)V
    .locals 2

    .line 275
    iget-object v0, p0, Lcom/squareup/widgets/CheckableGroup;->checkedIds:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 276
    invoke-virtual {p0, p1}, Lcom/squareup/widgets/CheckableGroup;->uncheck(I)V

    goto :goto_0

    .line 278
    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/widgets/CheckableGroup;->check(I)V

    :goto_0
    return-void
.end method

.method public uncheck(I)V
    .locals 2

    const/4 v0, 0x0

    .line 264
    invoke-direct {p0, p1, v0}, Lcom/squareup/widgets/CheckableGroup;->setCheckedStateForView(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 265
    iget-object v0, p0, Lcom/squareup/widgets/CheckableGroup;->checkedIds:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 268
    iget-object v0, p0, Lcom/squareup/widgets/CheckableGroup;->mOnCheckedChangeListener:Lcom/squareup/widgets/CheckableGroup$OnCheckedChangeListener;

    if-eqz v0, :cond_0

    const/4 v1, -0x1

    .line 269
    invoke-interface {v0, p0, p1, v1}, Lcom/squareup/widgets/CheckableGroup$OnCheckedChangeListener;->onCheckedChanged(Lcom/squareup/widgets/CheckableGroup;II)V

    :cond_0
    return-void
.end method
