.class Lcom/squareup/widgets/DigitInputView$2;
.super Ljava/lang/Object;
.source "DigitInputView.java"

# interfaces
.implements Lcom/squareup/widgets/DigitInputView$OnDigitsEnteredListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/widgets/DigitInputView;->lambda$digits$3(Lio/reactivex/ObservableEmitter;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/widgets/DigitInputView;

.field final synthetic val$emitter:Lio/reactivex/ObservableEmitter;


# direct methods
.method constructor <init>(Lcom/squareup/widgets/DigitInputView;Lio/reactivex/ObservableEmitter;)V
    .locals 0

    .line 184
    iput-object p1, p0, Lcom/squareup/widgets/DigitInputView$2;->this$0:Lcom/squareup/widgets/DigitInputView;

    iput-object p2, p0, Lcom/squareup/widgets/DigitInputView$2;->val$emitter:Lio/reactivex/ObservableEmitter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDigitEntered()V
    .locals 2

    .line 190
    iget-object v0, p0, Lcom/squareup/widgets/DigitInputView$2;->val$emitter:Lio/reactivex/ObservableEmitter;

    iget-object v1, p0, Lcom/squareup/widgets/DigitInputView$2;->this$0:Lcom/squareup/widgets/DigitInputView;

    invoke-virtual {v1}, Lcom/squareup/widgets/DigitInputView;->getDigits()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/reactivex/ObservableEmitter;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method public onDigitsEntered(Ljava/lang/String;)V
    .locals 1

    .line 186
    iget-object v0, p0, Lcom/squareup/widgets/DigitInputView$2;->val$emitter:Lio/reactivex/ObservableEmitter;

    invoke-interface {v0, p1}, Lio/reactivex/ObservableEmitter;->onNext(Ljava/lang/Object;)V

    return-void
.end method
