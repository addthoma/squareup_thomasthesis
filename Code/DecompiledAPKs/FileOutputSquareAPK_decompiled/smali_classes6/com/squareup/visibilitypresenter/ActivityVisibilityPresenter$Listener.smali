.class public interface abstract Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter$Listener;
.super Ljava/lang/Object;
.source "ActivityVisibilityPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Listener"
.end annotation


# virtual methods
.method public abstract activityNoLongerVisible()V
.end method

.method public abstract activityVisible()V
.end method
