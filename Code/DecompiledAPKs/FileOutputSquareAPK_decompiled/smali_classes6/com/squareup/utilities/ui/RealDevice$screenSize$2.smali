.class final Lcom/squareup/utilities/ui/RealDevice$screenSize$2;
.super Lkotlin/jvm/internal/Lambda;
.source "RealDevice.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/utilities/ui/RealDevice;-><init>(Lcom/squareup/util/AndroidConfigurationChangeMonitor;Lkotlin/jvm/functions/Function0;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lcom/squareup/util/DeviceScreenSizeInfo;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/util/DeviceScreenSizeInfo;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/utilities/ui/RealDevice;


# direct methods
.method constructor <init>(Lcom/squareup/utilities/ui/RealDevice;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/utilities/ui/RealDevice$screenSize$2;->this$0:Lcom/squareup/utilities/ui/RealDevice;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke()Lcom/squareup/util/DeviceScreenSizeInfo;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/utilities/ui/RealDevice$screenSize$2;->this$0:Lcom/squareup/utilities/ui/RealDevice;

    invoke-static {v0}, Lcom/squareup/utilities/ui/RealDevice;->access$getScreenInfoProvider$p(Lcom/squareup/utilities/ui/RealDevice;)Lkotlin/jvm/functions/Function0;

    move-result-object v0

    invoke-interface {v0}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/DeviceScreenSizeInfo;

    return-object v0
.end method

.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/utilities/ui/RealDevice$screenSize$2;->invoke()Lcom/squareup/util/DeviceScreenSizeInfo;

    move-result-object v0

    return-object v0
.end method
