.class final Lcom/squareup/utilities/ui/RealDevice$screenSize$1;
.super Ljava/lang/Object;
.source "RealDevice.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/utilities/ui/RealDevice;-><init>(Lcom/squareup/util/AndroidConfigurationChangeMonitor;Lkotlin/jvm/functions/Function0;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0004\u0008\u0004\u0010\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/util/DeviceScreenSizeInfo;",
        "it",
        "",
        "apply",
        "(Lkotlin/Unit;)Lcom/squareup/util/DeviceScreenSizeInfo;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/utilities/ui/RealDevice;


# direct methods
.method constructor <init>(Lcom/squareup/utilities/ui/RealDevice;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/utilities/ui/RealDevice$screenSize$1;->this$0:Lcom/squareup/utilities/ui/RealDevice;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lkotlin/Unit;)Lcom/squareup/util/DeviceScreenSizeInfo;
    .locals 1

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    iget-object p1, p0, Lcom/squareup/utilities/ui/RealDevice$screenSize$1;->this$0:Lcom/squareup/utilities/ui/RealDevice;

    invoke-static {p1}, Lcom/squareup/utilities/ui/RealDevice;->access$getScreenInfoProvider$p(Lcom/squareup/utilities/ui/RealDevice;)Lkotlin/jvm/functions/Function0;

    move-result-object p1

    invoke-interface {p1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/util/DeviceScreenSizeInfo;

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 13
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1}, Lcom/squareup/utilities/ui/RealDevice$screenSize$1;->apply(Lkotlin/Unit;)Lcom/squareup/util/DeviceScreenSizeInfo;

    move-result-object p1

    return-object p1
.end method
