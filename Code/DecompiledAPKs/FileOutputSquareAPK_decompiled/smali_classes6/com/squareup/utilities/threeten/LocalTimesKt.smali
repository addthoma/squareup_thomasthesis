.class public final Lcom/squareup/utilities/threeten/LocalTimesKt;
.super Ljava/lang/Object;
.source "LocalTimes.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0002\u001a\u0012\u0010\u0003\u001a\u00020\u0004*\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0001\u00a8\u0006\u0007"
    }
    d2 = {
        "readLocalTime",
        "Lorg/threeten/bp/LocalTime;",
        "Lokio/BufferedSource;",
        "writeLocalTime",
        "",
        "Lokio/BufferedSink;",
        "localTime",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final readLocalTime(Lokio/BufferedSource;)Lorg/threeten/bp/LocalTime;
    .locals 3

    const-string v0, "$this$readLocalTime"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-interface {p0}, Lokio/BufferedSource;->readInt()I

    move-result v0

    .line 17
    invoke-interface {p0}, Lokio/BufferedSource;->readInt()I

    move-result v1

    .line 18
    invoke-interface {p0}, Lokio/BufferedSource;->readInt()I

    move-result v2

    .line 19
    invoke-interface {p0}, Lokio/BufferedSource;->readInt()I

    move-result p0

    .line 15
    invoke-static {v0, v1, v2, p0}, Lorg/threeten/bp/LocalTime;->of(IIII)Lorg/threeten/bp/LocalTime;

    move-result-object p0

    const-string v0, "LocalTime.of(\n      read\u2026nt(),\n      readInt()\n  )"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final writeLocalTime(Lokio/BufferedSink;Lorg/threeten/bp/LocalTime;)V
    .locals 1

    const-string v0, "$this$writeLocalTime"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "localTime"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    invoke-virtual {p1}, Lorg/threeten/bp/LocalTime;->getHour()I

    move-result v0

    invoke-interface {p0, v0}, Lokio/BufferedSink;->writeInt(I)Lokio/BufferedSink;

    .line 9
    invoke-virtual {p1}, Lorg/threeten/bp/LocalTime;->getMinute()I

    move-result v0

    invoke-interface {p0, v0}, Lokio/BufferedSink;->writeInt(I)Lokio/BufferedSink;

    .line 10
    invoke-virtual {p1}, Lorg/threeten/bp/LocalTime;->getSecond()I

    move-result v0

    invoke-interface {p0, v0}, Lokio/BufferedSink;->writeInt(I)Lokio/BufferedSink;

    .line 11
    invoke-virtual {p1}, Lorg/threeten/bp/LocalTime;->getNano()I

    move-result p1

    invoke-interface {p0, p1}, Lokio/BufferedSink;->writeInt(I)Lokio/BufferedSink;

    return-void
.end method
