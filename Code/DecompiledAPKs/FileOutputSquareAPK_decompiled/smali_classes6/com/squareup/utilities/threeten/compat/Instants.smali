.class public final Lcom/squareup/utilities/threeten/compat/Instants;
.super Ljava/lang/Object;
.source "Instants.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001a\u0012\u0010\u0000\u001a\u00020\u0001*\u00020\u00022\u0006\u0010\u0003\u001a\u00020\u0004\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0005\u001a\n\u0010\u0006\u001a\u00020\u0002*\u00020\u0001\u00a8\u0006\u0007"
    }
    d2 = {
        "toCalendar",
        "Ljava/util/Calendar;",
        "Lorg/threeten/bp/Instant;",
        "zoneId",
        "Lorg/threeten/bp/ZoneId;",
        "Lorg/threeten/bp/ZonedDateTime;",
        "toThreeTenInstant",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final toCalendar(Lorg/threeten/bp/Instant;Lorg/threeten/bp/ZoneId;)Ljava/util/Calendar;
    .locals 1

    const-string v0, "$this$toCalendar"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "zoneId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    invoke-static {p0, p1}, Lorg/threeten/bp/ZonedDateTime;->ofInstant(Lorg/threeten/bp/Instant;Lorg/threeten/bp/ZoneId;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object p0

    const-string p1, "ZonedDateTime.ofInstant(this, zoneId)"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    invoke-static {p0}, Lcom/squareup/utilities/threeten/compat/Instants;->toCalendar(Lorg/threeten/bp/ZonedDateTime;)Ljava/util/Calendar;

    move-result-object p0

    return-object p0
.end method

.method public static final toCalendar(Lorg/threeten/bp/ZonedDateTime;)Ljava/util/Calendar;
    .locals 1

    const-string v0, "$this$toCalendar"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    invoke-static {p0}, Lorg/threeten/bp/DateTimeUtils;->toGregorianCalendar(Lorg/threeten/bp/ZonedDateTime;)Ljava/util/GregorianCalendar;

    move-result-object p0

    const-string v0, "DateTimeUtils.toGregorianCalendar(this)"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p0, Ljava/util/Calendar;

    return-object p0
.end method

.method public static final toThreeTenInstant(Ljava/util/Calendar;)Lorg/threeten/bp/Instant;
    .locals 1

    const-string v0, "$this$toThreeTenInstant"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    invoke-static {p0}, Lorg/threeten/bp/DateTimeUtils;->toInstant(Ljava/util/Calendar;)Lorg/threeten/bp/Instant;

    move-result-object p0

    if-nez p0, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    return-object p0
.end method
