.class public Lcom/squareup/wavpool/swipe/AudioBackendLegacy;
.super Ljava/lang/Object;
.source "AudioBackendLegacy.java"

# interfaces
.implements Lcom/squareup/squarewave/gum/SampleProcessor;
.implements Lcom/squareup/squarewave/m1/R4Decoder;
.implements Lcom/squareup/cardreader/LcrBackend;
.implements Lcom/squareup/wavpool/swipe/AudioBackend;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/wavpool/swipe/AudioBackendLegacy$Session;
    }
.end annotation


# instance fields
.field private audioBackend:Lcom/squareup/wavpool/swipe/AudioBackendLegacy$Session;

.field private final audioBackendBridge:Lcom/squareup/wavpool/swipe/AudioBackendBridge;

.field private final audioPlayer:Lcom/squareup/wavpool/swipe/AudioPlayer;

.field private final cardReaderBridge:Lcom/squareup/wavpool/swipe/CardReaderBridge;

.field private final eventDataListener:Lcom/squareup/squarewave/EventDataListener;

.field private featureListener:Lcom/squareup/cardreader/CardReaderFeatureLegacy$CardReaderFeatureListener;

.field private final inputSampleRate:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final lcrExecutor:Ljava/util/concurrent/ExecutorService;

.field private final outputSampleRate:Ljava/lang/Integer;

.field private final session:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljava/lang/Integer;Lcom/squareup/wavpool/swipe/AudioPlayer;Ljava/util/concurrent/ExecutorService;Ljavax/inject/Provider;Lcom/squareup/squarewave/EventDataListener;Lcom/squareup/wavpool/swipe/AudioBackendBridge;Lcom/squareup/wavpool/swipe/CardReaderBridge;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/lang/Integer;",
            "Lcom/squareup/wavpool/swipe/AudioPlayer;",
            "Ljava/util/concurrent/ExecutorService;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPointer;",
            ">;",
            "Lcom/squareup/squarewave/EventDataListener;",
            "Lcom/squareup/wavpool/swipe/AudioBackendBridge;",
            "Lcom/squareup/wavpool/swipe/CardReaderBridge;",
            ")V"
        }
    .end annotation

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, Lcom/squareup/wavpool/swipe/AudioBackendLegacy;->inputSampleRate:Ljavax/inject/Provider;

    .line 47
    iput-object p2, p0, Lcom/squareup/wavpool/swipe/AudioBackendLegacy;->outputSampleRate:Ljava/lang/Integer;

    .line 48
    iput-object p3, p0, Lcom/squareup/wavpool/swipe/AudioBackendLegacy;->audioPlayer:Lcom/squareup/wavpool/swipe/AudioPlayer;

    .line 49
    iput-object p4, p0, Lcom/squareup/wavpool/swipe/AudioBackendLegacy;->lcrExecutor:Ljava/util/concurrent/ExecutorService;

    .line 50
    iput-object p5, p0, Lcom/squareup/wavpool/swipe/AudioBackendLegacy;->session:Ljavax/inject/Provider;

    .line 51
    iput-object p6, p0, Lcom/squareup/wavpool/swipe/AudioBackendLegacy;->eventDataListener:Lcom/squareup/squarewave/EventDataListener;

    .line 52
    iput-object p7, p0, Lcom/squareup/wavpool/swipe/AudioBackendLegacy;->audioBackendBridge:Lcom/squareup/wavpool/swipe/AudioBackendBridge;

    .line 53
    iput-object p8, p0, Lcom/squareup/wavpool/swipe/AudioBackendLegacy;->cardReaderBridge:Lcom/squareup/wavpool/swipe/CardReaderBridge;

    return-void
.end method

.method private enableTransmission()V
    .locals 1

    .line 132
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioBackendLegacy;->audioPlayer:Lcom/squareup/wavpool/swipe/AudioPlayer;

    invoke-interface {v0}, Lcom/squareup/wavpool/swipe/AudioPlayer;->enableTransmission()V

    return-void
.end method

.method private notifyTransmissionComplete()V
    .locals 1

    .line 136
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioBackendLegacy;->audioPlayer:Lcom/squareup/wavpool/swipe/AudioPlayer;

    invoke-interface {v0}, Lcom/squareup/wavpool/swipe/AudioPlayer;->notifyTransmissionComplete()V

    return-void
.end method


# virtual methods
.method public cardReaderInitialized()V
    .locals 2

    .line 76
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioBackendLegacy;->session:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    return-void

    .line 77
    :cond_0
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioBackendLegacy;->cardReaderBridge:Lcom/squareup/wavpool/swipe/CardReaderBridge;

    iget-object v1, p0, Lcom/squareup/wavpool/swipe/AudioBackendLegacy;->session:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/CardReaderPointer;

    invoke-virtual {v1}, Lcom/squareup/cardreader/CardReaderPointer;->getId()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/wavpool/swipe/CardReaderBridge;->cr_cardreader_notify_reader_plugged(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;)Lcom/squareup/cardreader/lcr/CrCardreaderResult;

    return-void
.end method

.method public decodeR4Packet(Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;[S)Lcom/squareup/squarewave/gum/StatsAndMaybePacket;
    .locals 2

    .line 107
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioBackendLegacy;->session:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 108
    new-instance p1, Lcom/squareup/squarewave/gum/StatsAndMaybePacket;

    const/4 p2, 0x0

    new-instance v0, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;-><init>()V

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;->INCOMPLETE:Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

    .line 109
    invoke-virtual {v0, v1}, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->result(Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;)Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Builder;->build()Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;

    move-result-object v0

    invoke-direct {p1, p2, v0}, Lcom/squareup/squarewave/gum/StatsAndMaybePacket;-><init>(Lcom/squareup/squarewave/gum/CardDataPacket;Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo;)V

    return-object p1

    .line 112
    :cond_0
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioBackendLegacy;->audioBackendBridge:Lcom/squareup/wavpool/swipe/AudioBackendBridge;

    iget-object v1, p0, Lcom/squareup/wavpool/swipe/AudioBackendLegacy;->session:Ljavax/inject/Provider;

    .line 113
    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/cardreader/CardReaderPointer;

    invoke-virtual {v1}, Lcom/squareup/cardreader/CardReaderPointer;->getId()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;

    move-result-object v1

    invoke-static {p1}, Lcom/squareup/squarewave/gum/Mapping;->linkTypeToLcrLinkType(Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;)I

    move-result p1

    invoke-interface {v0, v1, p1, p2}, Lcom/squareup/wavpool/swipe/AudioBackendBridge;->decode_r4_packet(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;I[S)Ljava/lang/Object;

    move-result-object p1

    .line 116
    check-cast p1, Lcom/squareup/squarewave/gum/StatsAndMaybePacket;

    return-object p1
.end method

.method public feedSamples(Ljava/nio/ByteBuffer;I)V
    .locals 2

    .line 98
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioBackendLegacy;->lcrExecutor:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/squareup/wavpool/swipe/-$$Lambda$AudioBackendLegacy$Mjh37KifVXhRVFPQVixLIvftltI;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/wavpool/swipe/-$$Lambda$AudioBackendLegacy$Mjh37KifVXhRVFPQVixLIvftltI;-><init>(Lcom/squareup/wavpool/swipe/AudioBackendLegacy;Ljava/nio/ByteBuffer;I)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public getSession()Lcom/squareup/wavpool/swipe/AudioBackendLegacy$Session;
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioBackendLegacy;->audioBackend:Lcom/squareup/wavpool/swipe/AudioBackendLegacy$Session;

    return-object v0
.end method

.method public initialize(Lcom/squareup/cardreader/TimerApiLegacy$TimerConfig;Lcom/squareup/cardreader/CardReaderFeatureLegacy$CardReaderFeatureListener;Lcom/squareup/cardreader/CardReaderFeatureLegacy;)Lcom/squareup/cardreader/CardReaderPointer;
    .locals 6

    .line 62
    iput-object p2, p0, Lcom/squareup/wavpool/swipe/AudioBackendLegacy;->featureListener:Lcom/squareup/cardreader/CardReaderFeatureLegacy$CardReaderFeatureListener;

    .line 64
    iget-object p2, p0, Lcom/squareup/wavpool/swipe/AudioBackendLegacy;->audioBackend:Lcom/squareup/wavpool/swipe/AudioBackendLegacy$Session;

    if-nez p2, :cond_0

    .line 65
    new-instance p2, Lcom/squareup/wavpool/swipe/AudioBackendLegacy$Session;

    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioBackendLegacy;->audioBackendBridge:Lcom/squareup/wavpool/swipe/AudioBackendBridge;

    invoke-interface {v0}, Lcom/squareup/wavpool/swipe/AudioBackendBridge;->cr_comms_backend_audio_alloc()Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;

    move-result-object v0

    invoke-direct {p2, p0, v0}, Lcom/squareup/wavpool/swipe/AudioBackendLegacy$Session;-><init>(Lcom/squareup/wavpool/swipe/AudioBackendLegacy;Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;)V

    iput-object p2, p0, Lcom/squareup/wavpool/swipe/AudioBackendLegacy;->audioBackend:Lcom/squareup/wavpool/swipe/AudioBackendLegacy$Session;

    .line 67
    new-instance p2, Lcom/squareup/cardreader/LcrBackend$BackendSession;

    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioBackendLegacy;->audioBackendBridge:Lcom/squareup/wavpool/swipe/AudioBackendBridge;

    iget-object v1, p0, Lcom/squareup/wavpool/swipe/AudioBackendLegacy;->inputSampleRate:Ljavax/inject/Provider;

    .line 68
    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v2, p0, Lcom/squareup/wavpool/swipe/AudioBackendLegacy;->outputSampleRate:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v3, p0, Lcom/squareup/wavpool/swipe/AudioBackendLegacy;->audioBackend:Lcom/squareup/wavpool/swipe/AudioBackendLegacy$Session;

    iget-object v3, v3, Lcom/squareup/wavpool/swipe/AudioBackendLegacy$Session;->backend:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;

    iget-object v4, p1, Lcom/squareup/cardreader/TimerApiLegacy$TimerConfig;->timer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_crs_timer_api_t;

    move-object v5, p0

    invoke-interface/range {v0 .. v5}, Lcom/squareup/wavpool/swipe/AudioBackendBridge;->initialize_backend_audio(IILcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;Lcom/squareup/cardreader/lcr/SWIGTYPE_p_crs_timer_api_t;Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_api_t;

    move-result-object v0

    invoke-direct {p2, v0}, Lcom/squareup/cardreader/LcrBackend$BackendSession;-><init>(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_api_t;)V

    .line 70
    new-instance v0, Lcom/squareup/cardreader/CardReaderPointer;

    iget-object v1, p0, Lcom/squareup/wavpool/swipe/AudioBackendLegacy;->cardReaderBridge:Lcom/squareup/wavpool/swipe/CardReaderBridge;

    iget-object p2, p2, Lcom/squareup/cardreader/LcrBackend$BackendSession;->api:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_api_t;

    iget-object p1, p1, Lcom/squareup/cardreader/TimerApiLegacy$TimerConfig;->timer:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_crs_timer_api_t;

    .line 71
    invoke-interface {v1, p2, p1, p3}, Lcom/squareup/wavpool/swipe/CardReaderBridge;->cardreader_initialize(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_api_t;Lcom/squareup/cardreader/lcr/SWIGTYPE_p_crs_timer_api_t;Ljava/lang/Object;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/squareup/cardreader/CardReaderPointer;-><init>(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_cardreader_t;)V

    return-object v0

    .line 64
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Audio backend already exists!"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public synthetic lambda$feedSamples$0$AudioBackendLegacy(Ljava/nio/ByteBuffer;I)V
    .locals 3

    .line 99
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioBackendLegacy;->audioBackend:Lcom/squareup/wavpool/swipe/AudioBackendLegacy$Session;

    if-nez v0, :cond_0

    return-void

    .line 100
    :cond_0
    iget-object v1, p0, Lcom/squareup/wavpool/swipe/AudioBackendLegacy;->audioBackendBridge:Lcom/squareup/wavpool/swipe/AudioBackendBridge;

    iget-object v0, v0, Lcom/squareup/wavpool/swipe/AudioBackendLegacy$Session;->backend:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;

    const/4 v2, 0x0

    invoke-interface {v1, v0, p1, v2, p2}, Lcom/squareup/wavpool/swipe/AudioBackendBridge;->feed_audio_samples(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;Ljava/lang/Object;II)V

    return-void
.end method

.method public synthetic lambda$setLegacyReaderType$1$AudioBackendLegacy(Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;)V
    .locals 2

    .line 121
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioBackendLegacy;->audioBackend:Lcom/squareup/wavpool/swipe/AudioBackendLegacy$Session;

    if-nez v0, :cond_0

    return-void

    .line 122
    :cond_0
    iget-object v1, p0, Lcom/squareup/wavpool/swipe/AudioBackendLegacy;->audioBackendBridge:Lcom/squareup/wavpool/swipe/AudioBackendBridge;

    iget-object v0, v0, Lcom/squareup/wavpool/swipe/AudioBackendLegacy$Session;->backend:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;

    .line 123
    invoke-static {p1}, Lcom/squareup/squarewave/gum/Mapping;->readerTypeToLcrReaderType(Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;)I

    move-result p1

    .line 122
    invoke-interface {v1, v0, p1}, Lcom/squareup/wavpool/swipe/AudioBackendBridge;->set_legacy_reader_type(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;I)V

    return-void
.end method

.method public onCarrierDetectEvent(Lcom/squareup/squarewave/gum/EventData;)V
    .locals 1

    .line 149
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioBackendLegacy;->eventDataListener:Lcom/squareup/squarewave/EventDataListener;

    if-nez v0, :cond_0

    return-void

    .line 150
    :cond_0
    invoke-interface {v0, p1}, Lcom/squareup/squarewave/EventDataListener;->onReceiveEventData(Lcom/squareup/squarewave/gum/EventData;)V

    return-void
.end method

.method public onCommsRateUpdated(ILjava/lang/String;ILjava/lang/String;)V
    .locals 2

    .line 167
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioBackendLegacy;->featureListener:Lcom/squareup/cardreader/CardReaderFeatureLegacy$CardReaderFeatureListener;

    new-instance v1, Lcom/squareup/cardreader/ReaderEventLogger$CommsRate;

    .line 168
    invoke-static {p1}, Lcom/squareup/cardreader/lcr/CrCommsRate;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrCommsRate;

    move-result-object p1

    .line 169
    invoke-static {p3}, Lcom/squareup/cardreader/lcr/CrCommsRate;->swigToEnum(I)Lcom/squareup/cardreader/lcr/CrCommsRate;

    move-result-object p3

    invoke-direct {v1, p1, p2, p3, p4}, Lcom/squareup/cardreader/ReaderEventLogger$CommsRate;-><init>(Lcom/squareup/cardreader/lcr/CrCommsRate;Ljava/lang/String;Lcom/squareup/cardreader/lcr/CrCommsRate;Ljava/lang/String;)V

    .line 167
    invoke-interface {v0, v1}, Lcom/squareup/cardreader/CardReaderFeatureLegacy$CardReaderFeatureListener;->onCommsRateUpdated(Lcom/squareup/cardreader/ReaderEventLogger$CommsRate;)V

    return-void
.end method

.method public onConnectionTimeout()V
    .locals 2

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Reader connection timeout"

    .line 160
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 161
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioBackendLegacy;->featureListener:Lcom/squareup/cardreader/CardReaderFeatureLegacy$CardReaderFeatureListener;

    invoke-interface {v0}, Lcom/squareup/cardreader/CardReaderFeatureLegacy$CardReaderFeatureListener;->onAudioConnectionTimeout()V

    return-void
.end method

.method public onResume()V
    .locals 0

    .line 93
    invoke-direct {p0}, Lcom/squareup/wavpool/swipe/AudioBackendLegacy;->notifyTransmissionComplete()V

    return-void
.end method

.method public powerOnReader()V
    .locals 0

    .line 88
    invoke-direct {p0}, Lcom/squareup/wavpool/swipe/AudioBackendLegacy;->enableTransmission()V

    .line 89
    invoke-direct {p0}, Lcom/squareup/wavpool/swipe/AudioBackendLegacy;->notifyTransmissionComplete()V

    return-void
.end method

.method public declared-synchronized resetBackend()V
    .locals 2

    monitor-enter p0

    .line 81
    :try_start_0
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioBackendLegacy;->audioBackendBridge:Lcom/squareup/wavpool/swipe/AudioBackendBridge;

    iget-object v1, p0, Lcom/squareup/wavpool/swipe/AudioBackendLegacy;->audioBackend:Lcom/squareup/wavpool/swipe/AudioBackendLegacy$Session;

    iget-object v1, v1, Lcom/squareup/wavpool/swipe/AudioBackendLegacy$Session;->backend:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;

    invoke-interface {v0, v1}, Lcom/squareup/wavpool/swipe/AudioBackendBridge;->cr_comms_backend_audio_shutdown(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;)V

    .line 82
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioBackendLegacy;->audioBackendBridge:Lcom/squareup/wavpool/swipe/AudioBackendBridge;

    iget-object v1, p0, Lcom/squareup/wavpool/swipe/AudioBackendLegacy;->audioBackend:Lcom/squareup/wavpool/swipe/AudioBackendLegacy$Session;

    iget-object v1, v1, Lcom/squareup/wavpool/swipe/AudioBackendLegacy$Session;->backend:Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;

    invoke-interface {v0, v1}, Lcom/squareup/wavpool/swipe/AudioBackendBridge;->cr_comms_backend_audio_free(Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_audio_t;)Lcom/squareup/cardreader/lcr/SWIGTYPE_p_cr_comms_backend_result_t;

    const/4 v0, 0x0

    .line 83
    iput-object v0, p0, Lcom/squareup/wavpool/swipe/AudioBackendLegacy;->audioBackend:Lcom/squareup/wavpool/swipe/AudioBackendLegacy$Session;

    .line 84
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioBackendLegacy;->audioPlayer:Lcom/squareup/wavpool/swipe/AudioPlayer;

    invoke-interface {v0}, Lcom/squareup/wavpool/swipe/AudioPlayer;->reset()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 85
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public sendToReader(Z[S)V
    .locals 1

    .line 155
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioBackendLegacy;->audioPlayer:Lcom/squareup/wavpool/swipe/AudioPlayer;

    invoke-interface {v0, p1, p2}, Lcom/squareup/wavpool/swipe/AudioPlayer;->forwardToReader(Z[S)V

    return-void
.end method

.method public setLegacyReaderType(Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;)V
    .locals 2

    .line 120
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioBackendLegacy;->lcrExecutor:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/squareup/wavpool/swipe/-$$Lambda$AudioBackendLegacy$JE6Nc30ynJh6pnMNQjSvsv87428;

    invoke-direct {v1, p0, p1}, Lcom/squareup/wavpool/swipe/-$$Lambda$AudioBackendLegacy$JE6Nc30ynJh6pnMNQjSvsv87428;-><init>(Lcom/squareup/wavpool/swipe/AudioBackendLegacy;Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public stopSendingToReader()V
    .locals 1

    .line 144
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioBackendLegacy;->audioPlayer:Lcom/squareup/wavpool/swipe/AudioPlayer;

    invoke-interface {v0}, Lcom/squareup/wavpool/swipe/AudioPlayer;->stopSendingToReader()V

    return-void
.end method

.method public triggerOnCarrierDetectEvent(Lcom/squareup/squarewave/gum/EventData;)V
    .locals 0

    .line 128
    invoke-virtual {p0, p1}, Lcom/squareup/wavpool/swipe/AudioBackendLegacy;->onCarrierDetectEvent(Lcom/squareup/squarewave/gum/EventData;)V

    return-void
.end method
