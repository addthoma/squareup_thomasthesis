.class public final Lcom/squareup/wavpool/swipe/LegacyLcrAudioModule_Real_ProvideAudioBackendInterfaceFactory;
.super Ljava/lang/Object;
.source "LegacyLcrAudioModule_Real_ProvideAudioBackendInterfaceFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/wavpool/swipe/AudioBackendBridge;",
        ">;"
    }
.end annotation


# instance fields
.field private final audioBackendNativeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/AudioBackendNativeInterface;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/AudioBackendNativeInterface;",
            ">;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/wavpool/swipe/LegacyLcrAudioModule_Real_ProvideAudioBackendInterfaceFactory;->audioBackendNativeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/wavpool/swipe/LegacyLcrAudioModule_Real_ProvideAudioBackendInterfaceFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/lcr/AudioBackendNativeInterface;",
            ">;)",
            "Lcom/squareup/wavpool/swipe/LegacyLcrAudioModule_Real_ProvideAudioBackendInterfaceFactory;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/wavpool/swipe/LegacyLcrAudioModule_Real_ProvideAudioBackendInterfaceFactory;

    invoke-direct {v0, p0}, Lcom/squareup/wavpool/swipe/LegacyLcrAudioModule_Real_ProvideAudioBackendInterfaceFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideAudioBackendInterface(Lcom/squareup/cardreader/lcr/AudioBackendNativeInterface;)Lcom/squareup/wavpool/swipe/AudioBackendBridge;
    .locals 1

    .line 37
    invoke-static {p0}, Lcom/squareup/wavpool/swipe/LegacyLcrAudioModule$Real;->provideAudioBackendInterface(Lcom/squareup/cardreader/lcr/AudioBackendNativeInterface;)Lcom/squareup/wavpool/swipe/AudioBackendBridge;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/wavpool/swipe/AudioBackendBridge;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/wavpool/swipe/AudioBackendBridge;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/LegacyLcrAudioModule_Real_ProvideAudioBackendInterfaceFactory;->audioBackendNativeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/lcr/AudioBackendNativeInterface;

    invoke-static {v0}, Lcom/squareup/wavpool/swipe/LegacyLcrAudioModule_Real_ProvideAudioBackendInterfaceFactory;->provideAudioBackendInterface(Lcom/squareup/cardreader/lcr/AudioBackendNativeInterface;)Lcom/squareup/wavpool/swipe/AudioBackendBridge;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/wavpool/swipe/LegacyLcrAudioModule_Real_ProvideAudioBackendInterfaceFactory;->get()Lcom/squareup/wavpool/swipe/AudioBackendBridge;

    move-result-object v0

    return-object v0
.end method
