.class public Lcom/squareup/wavpool/swipe/AudioView;
.super Landroid/view/View;
.source "AudioView.java"


# instance fields
.field private final detectedSignalPaint:Landroid/graphics/Paint;

.field private drawEnabled:Z

.field private indexSampleCounts:[I

.field private lastEndSample:J

.field private lastStartSample:J

.field private maxPixels:[S

.field private minPixels:[S

.field private final positionPaint:Landroid/graphics/Paint;

.field private ringBuffer:Lcom/squareup/squarewave/wav/AudioRingBuffer;

.field private final signalPaint:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 29
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const-wide/16 p1, -0x1

    .line 24
    iput-wide p1, p0, Lcom/squareup/wavpool/swipe/AudioView;->lastStartSample:J

    .line 25
    iput-wide p1, p0, Lcom/squareup/wavpool/swipe/AudioView;->lastEndSample:J

    .line 31
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/squareup/wavpool/swipe/AudioView;->signalPaint:Landroid/graphics/Paint;

    .line 32
    iget-object p1, p0, Lcom/squareup/wavpool/swipe/AudioView;->signalPaint:Landroid/graphics/Paint;

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 33
    iget-object p1, p0, Lcom/squareup/wavpool/swipe/AudioView;->signalPaint:Landroid/graphics/Paint;

    const v0, -0xdd55de

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 35
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/squareup/wavpool/swipe/AudioView;->detectedSignalPaint:Landroid/graphics/Paint;

    .line 36
    iget-object p1, p0, Lcom/squareup/wavpool/swipe/AudioView;->detectedSignalPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 37
    iget-object p1, p0, Lcom/squareup/wavpool/swipe/AudioView;->detectedSignalPaint:Landroid/graphics/Paint;

    const/high16 v0, -0x10000

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 39
    new-instance p1, Landroid/graphics/Paint;

    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    iput-object p1, p0, Lcom/squareup/wavpool/swipe/AudioView;->positionPaint:Landroid/graphics/Paint;

    .line 40
    iget-object p1, p0, Lcom/squareup/wavpool/swipe/AudioView;->positionPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 41
    iget-object p1, p0, Lcom/squareup/wavpool/swipe/AudioView;->positionPaint:Landroid/graphics/Paint;

    const v0, 0x551513ff

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 42
    invoke-virtual {p0}, Lcom/squareup/wavpool/swipe/AudioView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    .line 43
    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p1

    const/high16 v0, 0x40800000    # 4.0f

    invoke-static {p2, v0, p1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result p1

    .line 44
    iget-object p2, p0, Lcom/squareup/wavpool/swipe/AudioView;->positionPaint:Landroid/graphics/Paint;

    invoke-virtual {p2, p1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    return-void
.end method


# virtual methods
.method public isDrawEnabled()Z
    .locals 1

    .line 56
    iget-boolean v0, p0, Lcom/squareup/wavpool/swipe/AudioView;->drawEnabled:Z

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 18

    move-object/from16 v0, p0

    .line 65
    invoke-super/range {p0 .. p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 67
    iget-boolean v1, v0, Lcom/squareup/wavpool/swipe/AudioView;->drawEnabled:Z

    if-eqz v1, :cond_5

    iget-object v1, v0, Lcom/squareup/wavpool/swipe/AudioView;->ringBuffer:Lcom/squareup/squarewave/wav/AudioRingBuffer;

    if-eqz v1, :cond_5

    .line 68
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/wavpool/swipe/AudioView;->getHeight()I

    move-result v1

    .line 69
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/wavpool/swipe/AudioView;->getWidth()I

    move-result v2

    int-to-float v7, v1

    const v3, 0x477fff00    # 65535.0f

    div-float v3, v7, v3

    .line 71
    div-int/lit8 v1, v1, 0x2

    .line 73
    iget-object v4, v0, Lcom/squareup/wavpool/swipe/AudioView;->minPixels:[S

    if-eqz v4, :cond_0

    array-length v4, v4

    if-eq v4, v2, :cond_1

    .line 74
    :cond_0
    new-array v4, v2, [S

    iput-object v4, v0, Lcom/squareup/wavpool/swipe/AudioView;->minPixels:[S

    .line 75
    new-array v4, v2, [S

    iput-object v4, v0, Lcom/squareup/wavpool/swipe/AudioView;->maxPixels:[S

    .line 76
    new-array v4, v2, [I

    iput-object v4, v0, Lcom/squareup/wavpool/swipe/AudioView;->indexSampleCounts:[I

    .line 79
    :cond_1
    iget-object v4, v0, Lcom/squareup/wavpool/swipe/AudioView;->ringBuffer:Lcom/squareup/squarewave/wav/AudioRingBuffer;

    iget-object v5, v0, Lcom/squareup/wavpool/swipe/AudioView;->minPixels:[S

    iget-object v6, v0, Lcom/squareup/wavpool/swipe/AudioView;->maxPixels:[S

    iget-object v8, v0, Lcom/squareup/wavpool/swipe/AudioView;->indexSampleCounts:[I

    invoke-virtual {v4, v5, v6, v8}, Lcom/squareup/squarewave/wav/AudioRingBuffer;->extractMinMax([S[S[I)F

    move-result v4

    .line 81
    iget-wide v5, v0, Lcom/squareup/wavpool/swipe/AudioView;->lastStartSample:J

    .line 82
    iget-wide v8, v0, Lcom/squareup/wavpool/swipe/AudioView;->lastEndSample:J

    const/4 v10, 0x0

    :goto_0
    if-ge v10, v2, :cond_4

    .line 85
    iget-object v11, v0, Lcom/squareup/wavpool/swipe/AudioView;->signalPaint:Landroid/graphics/Paint;

    const-wide/16 v12, -0x1

    cmp-long v14, v5, v12

    if-eqz v14, :cond_2

    .line 88
    iget-object v12, v0, Lcom/squareup/wavpool/swipe/AudioView;->indexSampleCounts:[I

    aget v12, v12, v10

    int-to-long v12, v12

    cmp-long v14, v12, v5

    if-lez v14, :cond_2

    cmp-long v14, v12, v8

    if-gez v14, :cond_2

    .line 90
    iget-object v11, v0, Lcom/squareup/wavpool/swipe/AudioView;->detectedSignalPaint:Landroid/graphics/Paint;

    .line 94
    :cond_2
    iget-object v12, v0, Lcom/squareup/wavpool/swipe/AudioView;->minPixels:[S

    aget-short v12, v12, v10

    .line 95
    iget-object v13, v0, Lcom/squareup/wavpool/swipe/AudioView;->maxPixels:[S

    aget-short v13, v13, v10

    int-to-float v14, v1

    int-to-float v12, v12

    mul-float v12, v12, v3

    sub-float v12, v14, v12

    float-to-int v12, v12

    int-to-float v13, v13

    mul-float v13, v13, v3

    sub-float/2addr v14, v13

    float-to-int v13, v14

    if-eq v12, v13, :cond_3

    int-to-float v15, v10

    int-to-float v14, v12

    int-to-float v13, v13

    move-object/from16 v12, p1

    move/from16 v16, v13

    move v13, v15

    move-object/from16 v17, v11

    .line 99
    invoke-virtual/range {v12 .. v17}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    move-object/from16 v14, p1

    goto :goto_1

    :cond_3
    int-to-float v13, v10

    int-to-float v12, v12

    move-object/from16 v14, p1

    .line 101
    invoke-virtual {v14, v13, v12, v11}, Landroid/graphics/Canvas;->drawPoint(FFLandroid/graphics/Paint;)V

    :goto_1
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    :cond_4
    move-object/from16 v14, p1

    int-to-float v1, v2

    mul-float v6, v1, v4

    const/4 v5, 0x0

    .line 106
    iget-object v8, v0, Lcom/squareup/wavpool/swipe/AudioView;->positionPaint:Landroid/graphics/Paint;

    move-object/from16 v3, p1

    move v4, v6

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    :cond_5
    const-wide/16 v1, 0x64

    .line 110
    invoke-virtual {v0, v1, v2}, Lcom/squareup/wavpool/swipe/AudioView;->postInvalidateDelayed(J)V

    return-void
.end method

.method public setDrawEnabled(Z)V
    .locals 0

    .line 52
    iput-boolean p1, p0, Lcom/squareup/wavpool/swipe/AudioView;->drawEnabled:Z

    return-void
.end method

.method public setRingBuffer(Lcom/squareup/squarewave/wav/AudioRingBuffer;)V
    .locals 0

    .line 48
    iput-object p1, p0, Lcom/squareup/wavpool/swipe/AudioView;->ringBuffer:Lcom/squareup/squarewave/wav/AudioRingBuffer;

    return-void
.end method

.method public updateLastDemodResult(JJ)V
    .locals 0

    .line 60
    iput-wide p1, p0, Lcom/squareup/wavpool/swipe/AudioView;->lastStartSample:J

    .line 61
    iput-wide p3, p0, Lcom/squareup/wavpool/swipe/AudioView;->lastEndSample:J

    return-void
.end method
