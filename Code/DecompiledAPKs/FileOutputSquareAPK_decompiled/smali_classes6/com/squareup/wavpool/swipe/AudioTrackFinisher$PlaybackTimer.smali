.class Lcom/squareup/wavpool/swipe/AudioTrackFinisher$PlaybackTimer;
.super Ljava/lang/Object;
.source "AudioTrackFinisher.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/wavpool/swipe/AudioTrackFinisher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PlaybackTimer"
.end annotation


# instance fields
.field private final audioTrack:Landroid/media/AudioTrack;

.field private final listener:Lcom/squareup/wavpool/swipe/AndroidAudioPlayer$PlaybackCompleteListener;

.field final synthetic this$0:Lcom/squareup/wavpool/swipe/AudioTrackFinisher;


# direct methods
.method constructor <init>(Lcom/squareup/wavpool/swipe/AudioTrackFinisher;Lcom/squareup/wavpool/swipe/AndroidAudioPlayer$PlaybackCompleteListener;Landroid/media/AudioTrack;)V
    .locals 0

    .line 74
    iput-object p1, p0, Lcom/squareup/wavpool/swipe/AudioTrackFinisher$PlaybackTimer;->this$0:Lcom/squareup/wavpool/swipe/AudioTrackFinisher;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    iput-object p2, p0, Lcom/squareup/wavpool/swipe/AudioTrackFinisher$PlaybackTimer;->listener:Lcom/squareup/wavpool/swipe/AndroidAudioPlayer$PlaybackCompleteListener;

    .line 76
    iput-object p3, p0, Lcom/squareup/wavpool/swipe/AudioTrackFinisher$PlaybackTimer;->audioTrack:Landroid/media/AudioTrack;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .line 80
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioTrackFinisher$PlaybackTimer;->this$0:Lcom/squareup/wavpool/swipe/AudioTrackFinisher;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/squareup/wavpool/swipe/AudioTrackFinisher;->access$002(Lcom/squareup/wavpool/swipe/AudioTrackFinisher;I)I

    .line 81
    iget-object v0, p0, Lcom/squareup/wavpool/swipe/AudioTrackFinisher$PlaybackTimer;->listener:Lcom/squareup/wavpool/swipe/AndroidAudioPlayer$PlaybackCompleteListener;

    iget-object v1, p0, Lcom/squareup/wavpool/swipe/AudioTrackFinisher$PlaybackTimer;->audioTrack:Landroid/media/AudioTrack;

    invoke-virtual {v0, v1}, Lcom/squareup/wavpool/swipe/AndroidAudioPlayer$PlaybackCompleteListener;->onMarkerReached(Landroid/media/AudioTrack;)V

    return-void
.end method
