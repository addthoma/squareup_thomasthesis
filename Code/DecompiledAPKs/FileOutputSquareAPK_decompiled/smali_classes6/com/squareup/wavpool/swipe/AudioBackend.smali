.class public interface abstract Lcom/squareup/wavpool/swipe/AudioBackend;
.super Ljava/lang/Object;
.source "AudioBackend.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0005\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0017\n\u0002\u0008\u0002\u0008f\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J(\u0010\u0006\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u00082\u0006\u0010\u000c\u001a\u00020\nH&J\u0008\u0010\r\u001a\u00020\u0003H&J\u0018\u0010\u000e\u001a\u00020\u00032\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012H&J\u0008\u0010\u0013\u001a\u00020\u0003H&\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/wavpool/swipe/AudioBackend;",
        "",
        "onCarrierDetectEvent",
        "",
        "eventData",
        "Lcom/squareup/squarewave/gum/EventData;",
        "onCommsRateUpdated",
        "inCommsRate",
        "",
        "inCommsRateValue",
        "",
        "outCommsRate",
        "outCommsRateValue",
        "onConnectionTimeout",
        "sendToReader",
        "loop",
        "",
        "samples",
        "",
        "stopSendingToReader",
        "cardreader-features_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract onCarrierDetectEvent(Lcom/squareup/squarewave/gum/EventData;)V
.end method

.method public abstract onCommsRateUpdated(ILjava/lang/String;ILjava/lang/String;)V
.end method

.method public abstract onConnectionTimeout()V
.end method

.method public abstract sendToReader(Z[S)V
.end method

.method public abstract stopSendingToReader()V
.end method
