.class final Lcom/squareup/workflow/legacy/Screen$Key$filter$1;
.super Ljava/lang/Object;
.source "Screen.kt"

# interfaces
.implements Lio/reactivex/ObservableTransformer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/legacy/Screen$Key;->filter()Lio/reactivex/ObservableTransformer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Upstream:",
        "Ljava/lang/Object;",
        "Downstream:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/ObservableTransformer<",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;",
        "Lcom/squareup/workflow/legacy/Screen<",
        "TD;TE;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00040\u00020\u0001\"\u0008\u0008\u0000\u0010\u0003*\u00020\u0005\"\u0008\u0008\u0001\u0010\u0004*\u00020\u0005\"\u0008\u0008\u0002\u0010\u0003*\u00020\u0005\"\u0008\u0008\u0003\u0010\u0004*\u00020\u00052.\u0010\u0006\u001a*\u0012&\u0012$\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u0003 \u0008*\u0012\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u0003\u0018\u00010\u0002j\u0004\u0018\u0001`\u00070\u0002j\u0002`\u00070\u0001H\n\u00a2\u0006\u0002\u0008\t"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "D",
        "E",
        "",
        "observable",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "kotlin.jvm.PlatformType",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/workflow/legacy/Screen$Key;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/legacy/Screen$Key;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/workflow/legacy/Screen$Key$filter$1;->this$0:Lcom/squareup/workflow/legacy/Screen$Key;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lio/reactivex/Observable;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;)",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "TD;TE;>;>;"
        }
    .end annotation

    const-string v0, "observable"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    iget-object v0, p0, Lcom/squareup/workflow/legacy/Screen$Key$filter$1;->this$0:Lcom/squareup/workflow/legacy/Screen$Key;

    invoke-static {p1, v0}, Lcom/squareup/workflow/legacy/ScreenKt;->ofKeyType(Lio/reactivex/Observable;Lcom/squareup/workflow/legacy/Screen$Key;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 0

    .line 42
    invoke-virtual {p0, p1}, Lcom/squareup/workflow/legacy/Screen$Key$filter$1;->apply(Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object p1

    check-cast p1, Lio/reactivex/ObservableSource;

    return-object p1
.end method
