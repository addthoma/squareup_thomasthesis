.class public final Lcom/squareup/workflow/legacy/WorkflowInput$Companion;
.super Ljava/lang/Object;
.source "WorkflowInput.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/workflow/legacy/WorkflowInput;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u0002H\u00050\u0004\"\u0004\u0008\u0001\u0010\u0005\u00a8\u0006\u0006"
    }
    d2 = {
        "Lcom/squareup/workflow/legacy/WorkflowInput$Companion;",
        "",
        "()V",
        "disabled",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "E",
        "legacy-workflow-core"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field static final synthetic $$INSTANCE:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 37
    new-instance v0, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-direct {v0}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;-><init>()V

    sput-object v0, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->$$INSTANCE:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final disabled()Lcom/squareup/workflow/legacy/WorkflowInput;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "TE;>;"
        }
    .end annotation

    .line 38
    sget-object v0, Lcom/squareup/workflow/legacy/NoOp;->INSTANCE:Lcom/squareup/workflow/legacy/NoOp;

    check-cast v0, Lcom/squareup/workflow/legacy/WorkflowInput;

    sget-object v1, Lcom/squareup/workflow/legacy/WorkflowInput$Companion$disabled$1;->INSTANCE:Lcom/squareup/workflow/legacy/WorkflowInput$Companion$disabled$1;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, v1}, Lcom/squareup/workflow/legacy/WorkflowInputKt;->adaptEvents(Lcom/squareup/workflow/legacy/WorkflowInput;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v0

    return-object v0
.end method
