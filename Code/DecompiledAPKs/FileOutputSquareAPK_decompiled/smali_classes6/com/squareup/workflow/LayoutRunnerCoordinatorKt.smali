.class public final Lcom/squareup/workflow/LayoutRunnerCoordinatorKt;
.super Ljava/lang/Object;
.source "LayoutRunnerCoordinator.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u001a\u001e\u0010\u0008\u001a\n\u0012\u0004\u0012\u0002H\n\u0018\u00010\t\"\u0008\u0008\u0000\u0010\n*\u00020\u000b*\u00020\u0003H\u0002\u001a&\u0010\u000c\u001a\u00020\r\"\u0008\u0008\u0000\u0010\n*\u00020\u000b*\u00020\u00032\u000e\u0010\u000e\u001a\n\u0012\u0004\u0012\u0002H\n\u0018\u00010\tH\u0002\",\u0010\u0002\u001a\u0004\u0018\u00010\u0001*\u00020\u00032\u0008\u0010\u0000\u001a\u0004\u0018\u00010\u00018F@BX\u0086\u000e\u00a2\u0006\u000c\u001a\u0004\u0008\u0004\u0010\u0005\"\u0004\u0008\u0006\u0010\u0007\u00a8\u0006\u000f"
    }
    d2 = {
        "value",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "posHints",
        "Landroid/view/View;",
        "getPosHints",
        "(Landroid/view/View;)Lcom/squareup/workflow/ui/ContainerHints;",
        "setPosHints",
        "(Landroid/view/View;Lcom/squareup/workflow/ui/ContainerHints;)V",
        "getRunner",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "D",
        "Lcom/squareup/workflow/legacy/V2Screen;",
        "setRunner",
        "",
        "runner",
        "android_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic access$getPosHints$p(Landroid/view/View;)Lcom/squareup/workflow/ui/ContainerHints;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/workflow/LayoutRunnerCoordinatorKt;->getPosHints(Landroid/view/View;)Lcom/squareup/workflow/ui/ContainerHints;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getRunner(Landroid/view/View;)Lcom/squareup/workflow/ui/LayoutRunner;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/squareup/workflow/LayoutRunnerCoordinatorKt;->getRunner(Landroid/view/View;)Lcom/squareup/workflow/ui/LayoutRunner;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$setPosHints$p(Landroid/view/View;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/squareup/workflow/LayoutRunnerCoordinatorKt;->setPosHints(Landroid/view/View;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method

.method public static final synthetic access$setRunner(Landroid/view/View;Lcom/squareup/workflow/ui/LayoutRunner;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/squareup/workflow/LayoutRunnerCoordinatorKt;->setRunner(Landroid/view/View;Lcom/squareup/workflow/ui/LayoutRunner;)V

    return-void
.end method

.method public static final getPosHints(Landroid/view/View;)Lcom/squareup/workflow/ui/ContainerHints;
    .locals 1

    const-string v0, "$this$posHints"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    invoke-static {p0}, Lcom/squareup/workflow/ui/ViewShowRenderingKt;->getHints(Landroid/view/View;)Lcom/squareup/workflow/ui/ContainerHints;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    sget v0, Lcom/squareup/workflow/R$id;->layout_runner_hints:I

    invoke-virtual {p0, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object p0

    move-object v0, p0

    check-cast v0, Lcom/squareup/workflow/ui/ContainerHints;

    :goto_0
    return-object v0
.end method

.method private static final getRunner(Landroid/view/View;)Lcom/squareup/workflow/ui/LayoutRunner;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<D::",
            "Lcom/squareup/workflow/legacy/V2Screen;",
            ">(",
            "Landroid/view/View;",
            ")",
            "Lcom/squareup/workflow/ui/LayoutRunner<",
            "TD;>;"
        }
    .end annotation

    .line 57
    sget v0, Lcom/squareup/workflow/R$id;->layout_runner_coordinator_binding:I

    invoke-virtual {p0, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/workflow/ui/LayoutRunner;

    return-object p0
.end method

.method private static final setPosHints(Landroid/view/View;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 1

    .line 52
    sget v0, Lcom/squareup/workflow/R$id;->layout_runner_hints:I

    invoke-virtual {p0, v0, p1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    return-void
.end method

.method private static final setRunner(Landroid/view/View;Lcom/squareup/workflow/ui/LayoutRunner;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<D::",
            "Lcom/squareup/workflow/legacy/V2Screen;",
            ">(",
            "Landroid/view/View;",
            "Lcom/squareup/workflow/ui/LayoutRunner<",
            "TD;>;)V"
        }
    .end annotation

    .line 61
    sget v0, Lcom/squareup/workflow/R$id;->layout_runner_coordinator_binding:I

    invoke-virtual {p0, v0, p1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    return-void
.end method
