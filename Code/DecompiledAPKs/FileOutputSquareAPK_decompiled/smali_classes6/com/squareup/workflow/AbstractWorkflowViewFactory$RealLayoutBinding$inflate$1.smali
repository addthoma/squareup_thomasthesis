.class final Lcom/squareup/workflow/AbstractWorkflowViewFactory$RealLayoutBinding$inflate$1;
.super Ljava/lang/Object;
.source "AbstractWorkflowViewFactory.kt"

# interfaces
.implements Lcom/squareup/coordinators/CoordinatorProvider;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/AbstractWorkflowViewFactory$RealLayoutBinding;->inflate(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/squareup/workflow/legacy/Screen$Key;Lio/reactivex/Observable;Lcom/squareup/workflow/ui/ContainerHints;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\u0008\u0008\u0001\u0010\u0004*\u00020\u00032\u0006\u0010\u0005\u001a\u00020\u0006H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/coordinators/Coordinator;",
        "D",
        "",
        "E",
        "it",
        "Landroid/view/View;",
        "provideCoordinator"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $containerHints:Lcom/squareup/workflow/ui/ContainerHints;

.field final synthetic $screenKey:Lcom/squareup/workflow/legacy/Screen$Key;

.field final synthetic $screens:Lio/reactivex/Observable;

.field final synthetic this$0:Lcom/squareup/workflow/AbstractWorkflowViewFactory$RealLayoutBinding;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/AbstractWorkflowViewFactory$RealLayoutBinding;Lio/reactivex/Observable;Lcom/squareup/workflow/legacy/Screen$Key;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/workflow/AbstractWorkflowViewFactory$RealLayoutBinding$inflate$1;->this$0:Lcom/squareup/workflow/AbstractWorkflowViewFactory$RealLayoutBinding;

    iput-object p2, p0, Lcom/squareup/workflow/AbstractWorkflowViewFactory$RealLayoutBinding$inflate$1;->$screens:Lio/reactivex/Observable;

    iput-object p3, p0, Lcom/squareup/workflow/AbstractWorkflowViewFactory$RealLayoutBinding$inflate$1;->$screenKey:Lcom/squareup/workflow/legacy/Screen$Key;

    iput-object p4, p0, Lcom/squareup/workflow/AbstractWorkflowViewFactory$RealLayoutBinding$inflate$1;->$containerHints:Lcom/squareup/workflow/ui/ContainerHints;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 3

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 177
    iget-object p1, p0, Lcom/squareup/workflow/AbstractWorkflowViewFactory$RealLayoutBinding$inflate$1;->this$0:Lcom/squareup/workflow/AbstractWorkflowViewFactory$RealLayoutBinding;

    invoke-static {p1}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$RealLayoutBinding;->access$getCoordinatorForScreens$p(Lcom/squareup/workflow/AbstractWorkflowViewFactory$RealLayoutBinding;)Lkotlin/jvm/functions/Function2;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/workflow/AbstractWorkflowViewFactory$RealLayoutBinding$inflate$1;->$screens:Lio/reactivex/Observable;

    iget-object v1, p0, Lcom/squareup/workflow/AbstractWorkflowViewFactory$RealLayoutBinding$inflate$1;->this$0:Lcom/squareup/workflow/AbstractWorkflowViewFactory$RealLayoutBinding;

    invoke-virtual {v1}, Lcom/squareup/workflow/AbstractWorkflowViewFactory$RealLayoutBinding;->getKey()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/workflow/AbstractWorkflowViewFactory$RealLayoutBinding$inflate$1;->$screenKey:Lcom/squareup/workflow/legacy/Screen$Key;

    invoke-virtual {v1, v2}, Lcom/squareup/workflow/legacy/Screen$Key;->cast(Lcom/squareup/workflow/legacy/Screen$Key;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/workflow/legacy/ScreenKt;->ofKeyType(Lio/reactivex/Observable;Lcom/squareup/workflow/legacy/Screen$Key;)Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/workflow/AbstractWorkflowViewFactory$RealLayoutBinding$inflate$1;->$containerHints:Lcom/squareup/workflow/ui/ContainerHints;

    invoke-interface {p1, v0, v1}, Lkotlin/jvm/functions/Function2;->invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/coordinators/Coordinator;

    return-object p1
.end method
