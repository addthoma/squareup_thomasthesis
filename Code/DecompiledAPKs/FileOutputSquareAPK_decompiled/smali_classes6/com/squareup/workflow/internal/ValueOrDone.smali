.class public final Lcom/squareup/workflow/internal/ValueOrDone;
.super Ljava/lang/Object;
.source "Workers.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/workflow/internal/ValueOrDone$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nWorkers.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Workers.kt\ncom/squareup/workflow/internal/ValueOrDone\n*L\n1#1,142:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0008\u0008\u0000\u0018\u0000 \r*\u0006\u0008\u0000\u0010\u0001 \u00012\u00020\u0002:\u0001\rB\u0011\u0008\u0002\u0012\u0008\u0010\u0003\u001a\u0004\u0018\u00010\u0002\u00a2\u0006\u0002\u0010\u0004R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0002X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0005\u001a\u00020\u00068F\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0007R\u0017\u0010\u0008\u001a\u00028\u00008F\u00a2\u0006\u000c\u0012\u0004\u0008\t\u0010\n\u001a\u0004\u0008\u000b\u0010\u000c\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/workflow/internal/ValueOrDone;",
        "T",
        "",
        "_value",
        "(Ljava/lang/Object;)V",
        "isDone",
        "",
        "()Z",
        "value",
        "value$annotations",
        "()V",
        "getValue",
        "()Ljava/lang/Object;",
        "Companion",
        "workflow-runtime"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/workflow/internal/ValueOrDone$Companion;

.field private static final Done:Lcom/squareup/workflow/internal/ValueOrDone;


# instance fields
.field private final _value:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/workflow/internal/ValueOrDone$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/workflow/internal/ValueOrDone$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/workflow/internal/ValueOrDone;->Companion:Lcom/squareup/workflow/internal/ValueOrDone$Companion;

    .line 120
    new-instance v0, Lcom/squareup/workflow/internal/ValueOrDone;

    invoke-direct {v0, v1}, Lcom/squareup/workflow/internal/ValueOrDone;-><init>(Ljava/lang/Object;)V

    sput-object v0, Lcom/squareup/workflow/internal/ValueOrDone;->Done:Lcom/squareup/workflow/internal/ValueOrDone;

    return-void
.end method

.method private constructor <init>(Ljava/lang/Object;)V
    .locals 0

    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/workflow/internal/ValueOrDone;->_value:Ljava/lang/Object;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/Object;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 108
    invoke-direct {p0, p1}, Lcom/squareup/workflow/internal/ValueOrDone;-><init>(Ljava/lang/Object;)V

    return-void
.end method

.method public static final synthetic access$getDone$cp()Lcom/squareup/workflow/internal/ValueOrDone;
    .locals 1

    .line 108
    sget-object v0, Lcom/squareup/workflow/internal/ValueOrDone;->Done:Lcom/squareup/workflow/internal/ValueOrDone;

    return-object v0
.end method

.method public static synthetic value$annotations()V
    .locals 0

    return-void
.end method


# virtual methods
.method public final getValue()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .line 115
    invoke-virtual {p0}, Lcom/squareup/workflow/internal/ValueOrDone;->isDone()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/squareup/workflow/internal/ValueOrDone;->_value:Ljava/lang/Object;

    return-object v0

    .line 115
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Check failed."

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public final isDone()Z
    .locals 2

    .line 110
    move-object v0, p0

    check-cast v0, Lcom/squareup/workflow/internal/ValueOrDone;

    sget-object v1, Lcom/squareup/workflow/internal/ValueOrDone;->Done:Lcom/squareup/workflow/internal/ValueOrDone;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
