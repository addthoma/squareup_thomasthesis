.class public final Lcom/squareup/workflow/internal/TreeSnapshotsKt;
.super Ljava/lang/Object;
.source "TreeSnapshots.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nTreeSnapshots.kt\nKotlin\n*S Kotlin\n*F\n+ 1 TreeSnapshots.kt\ncom/squareup/workflow/internal/TreeSnapshotsKt\n+ 2 Snapshot.kt\ncom/squareup/workflow/SnapshotKt\n*L\n1#1,68:1\n180#2:69\n165#2:70\n*E\n*S KotlinDebug\n*F\n+ 1 TreeSnapshots.kt\ncom/squareup/workflow/internal/TreeSnapshotsKt\n*L\n57#1:69\n57#1:70\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001a:\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00012(\u0010\u0003\u001a$\u0012 \u0012\u001e\u0012\u0014\u0012\u0012\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\u00010\u00050\u0004H\u0000\u001a@\u0010\u0008\u001a2\u0012\u0006\u0012\u0004\u0018\u00010\t\u0012&\u0012$\u0012 \u0012\u001e\u0012\u0014\u0012\u0012\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\t0\u00050\u00040\u00052\u0006\u0010\n\u001a\u00020\tH\u0000\u00a8\u0006\u000b"
    }
    d2 = {
        "createTreeSnapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "rootSnapshot",
        "childSnapshots",
        "",
        "Lkotlin/Pair;",
        "Lcom/squareup/workflow/internal/WorkflowId;",
        "Lcom/squareup/workflow/internal/AnyId;",
        "parseTreeSnapshot",
        "Lokio/ByteString;",
        "treeSnapshotBytes",
        "workflow-runtime"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final createTreeSnapshot(Lcom/squareup/workflow/Snapshot;Ljava/util/List;)Lcom/squareup/workflow/Snapshot;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/Snapshot;",
            "Ljava/util/List<",
            "+",
            "Lkotlin/Pair<",
            "+",
            "Lcom/squareup/workflow/internal/WorkflowId<",
            "***>;",
            "Lcom/squareup/workflow/Snapshot;",
            ">;>;)",
            "Lcom/squareup/workflow/Snapshot;"
        }
    .end annotation

    const-string v0, "rootSnapshot"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "childSnapshots"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    sget-object v0, Lcom/squareup/workflow/Snapshot;->Companion:Lcom/squareup/workflow/Snapshot$Companion;

    new-instance v1, Lcom/squareup/workflow/internal/TreeSnapshotsKt$createTreeSnapshot$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/workflow/internal/TreeSnapshotsKt$createTreeSnapshot$1;-><init>(Lcom/squareup/workflow/Snapshot;Ljava/util/List;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-virtual {v0, v1}, Lcom/squareup/workflow/Snapshot$Companion;->write(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Snapshot;

    move-result-object p0

    return-object p0
.end method

.method public static final parseTreeSnapshot(Lokio/ByteString;)Lkotlin/Pair;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lokio/ByteString;",
            ")",
            "Lkotlin/Pair<",
            "Lokio/ByteString;",
            "Ljava/util/List<",
            "Lkotlin/Pair<",
            "Lcom/squareup/workflow/internal/WorkflowId<",
            "***>;",
            "Lokio/ByteString;",
            ">;>;>;"
        }
    .end annotation

    const-string v0, "treeSnapshotBytes"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    new-instance v0, Lokio/Buffer;

    invoke-direct {v0}, Lokio/Buffer;-><init>()V

    invoke-virtual {v0, p0}, Lokio/Buffer;->write(Lokio/ByteString;)Lokio/Buffer;

    move-result-object p0

    check-cast p0, Lokio/BufferedSource;

    .line 58
    invoke-static {p0}, Lcom/squareup/workflow/SnapshotKt;->readByteStringWithLength(Lokio/BufferedSource;)Lokio/ByteString;

    move-result-object v0

    .line 70
    invoke-interface {p0}, Lokio/BufferedSource;->readInt()I

    move-result v1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v1, :cond_0

    .line 60
    invoke-static {p0}, Lcom/squareup/workflow/SnapshotKt;->readByteStringWithLength(Lokio/BufferedSource;)Lokio/ByteString;

    move-result-object v5

    invoke-static {v5}, Lcom/squareup/workflow/internal/WorkflowIdKt;->restoreId(Lokio/ByteString;)Lcom/squareup/workflow/internal/WorkflowId;

    move-result-object v5

    .line 61
    invoke-static {p0}, Lcom/squareup/workflow/SnapshotKt;->readByteStringWithLength(Lokio/BufferedSource;)Lokio/ByteString;

    move-result-object v6

    .line 62
    new-instance v7, Lkotlin/Pair;

    invoke-direct {v7, v5, v6}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 70
    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_0
    check-cast v2, Ljava/util/List;

    .line 65
    invoke-virtual {v0}, Lokio/ByteString;->size()I

    move-result p0

    if-lez p0, :cond_1

    const/4 v3, 0x1

    :cond_1
    if-eqz v3, :cond_2

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    .line 66
    :goto_1
    new-instance p0, Lkotlin/Pair;

    invoke-direct {p0, v0, v2}, Lkotlin/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object p0
.end method
