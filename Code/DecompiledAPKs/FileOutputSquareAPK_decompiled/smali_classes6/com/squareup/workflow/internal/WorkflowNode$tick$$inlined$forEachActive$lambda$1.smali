.class final Lcom/squareup/workflow/internal/WorkflowNode$tick$$inlined$forEachActive$lambda$1;
.super Lkotlin/coroutines/jvm/internal/SuspendLambda;
.source "WorkflowNode.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/internal/WorkflowNode;->tick(Lkotlinx/coroutines/selects/SelectBuilder;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/coroutines/jvm/internal/SuspendLambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Lcom/squareup/workflow/internal/ValueOrDone<",
        "*>;",
        "Lkotlin/coroutines/Continuation<",
        "-TT;>;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0090\u0001\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0003\n\u0002\u0008\u0003\n\u0002\u0008\u0004\u0010\u0000\u001a\u0004\u0018\u0001H\u0001\"\u0008\u0008\u0000\u0010\u0001*\u00020\u0002\"\u0004\u0008\u0001\u0010\u0003\"\u0004\u0008\u0002\u0010\u0004\"\u0008\u0008\u0003\u0010\u0005*\u00020\u0002\"\u0004\u0008\u0004\u0010\u00062\n\u0010\u0007\u001a\u0006\u0012\u0002\u0008\u00030\u0008H\u008a@\u00a2\u0006\u0004\u0008\t\u0010\n\u00a8\u0006\u000c"
    }
    d2 = {
        "<anonymous>",
        "T",
        "",
        "PropsT",
        "StateT",
        "OutputT",
        "RenderingT",
        "valueOrDone",
        "Lcom/squareup/workflow/internal/ValueOrDone;",
        "invoke",
        "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;",
        "com/squareup/workflow/internal/WorkflowNode$tick$1$1$1",
        "com/squareup/workflow/internal/WorkflowNode$$special$$inlined$with$lambda$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $child$inlined:Lcom/squareup/workflow/internal/WorkerChildNode;

.field final synthetic $selector$inlined:Lkotlinx/coroutines/selects/SelectBuilder;

.field label:I

.field private p$0:Lcom/squareup/workflow/internal/ValueOrDone;

.field final synthetic this$0:Lcom/squareup/workflow/internal/WorkflowNode;


# direct methods
.method constructor <init>(Lkotlin/coroutines/Continuation;Lcom/squareup/workflow/internal/WorkerChildNode;Lcom/squareup/workflow/internal/WorkflowNode;Lkotlinx/coroutines/selects/SelectBuilder;)V
    .locals 0

    iput-object p2, p0, Lcom/squareup/workflow/internal/WorkflowNode$tick$$inlined$forEachActive$lambda$1;->$child$inlined:Lcom/squareup/workflow/internal/WorkerChildNode;

    iput-object p3, p0, Lcom/squareup/workflow/internal/WorkflowNode$tick$$inlined$forEachActive$lambda$1;->this$0:Lcom/squareup/workflow/internal/WorkflowNode;

    iput-object p4, p0, Lcom/squareup/workflow/internal/WorkflowNode$tick$$inlined$forEachActive$lambda$1;->$selector$inlined:Lkotlinx/coroutines/selects/SelectBuilder;

    const/4 p2, 0x2

    invoke-direct {p0, p2, p1}, Lkotlin/coroutines/jvm/internal/SuspendLambda;-><init>(ILkotlin/coroutines/Continuation;)V

    return-void
.end method


# virtual methods
.method public final create(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Lkotlin/coroutines/Continuation<",
            "*>;)",
            "Lkotlin/coroutines/Continuation<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    const-string v0, "completion"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/workflow/internal/WorkflowNode$tick$$inlined$forEachActive$lambda$1;

    iget-object v1, p0, Lcom/squareup/workflow/internal/WorkflowNode$tick$$inlined$forEachActive$lambda$1;->$child$inlined:Lcom/squareup/workflow/internal/WorkerChildNode;

    iget-object v2, p0, Lcom/squareup/workflow/internal/WorkflowNode$tick$$inlined$forEachActive$lambda$1;->this$0:Lcom/squareup/workflow/internal/WorkflowNode;

    iget-object v3, p0, Lcom/squareup/workflow/internal/WorkflowNode$tick$$inlined$forEachActive$lambda$1;->$selector$inlined:Lkotlinx/coroutines/selects/SelectBuilder;

    invoke-direct {v0, p2, v1, v2, v3}, Lcom/squareup/workflow/internal/WorkflowNode$tick$$inlined$forEachActive$lambda$1;-><init>(Lkotlin/coroutines/Continuation;Lcom/squareup/workflow/internal/WorkerChildNode;Lcom/squareup/workflow/internal/WorkflowNode;Lkotlinx/coroutines/selects/SelectBuilder;)V

    check-cast p1, Lcom/squareup/workflow/internal/ValueOrDone;

    iput-object p1, v0, Lcom/squareup/workflow/internal/WorkflowNode$tick$$inlined$forEachActive$lambda$1;->p$0:Lcom/squareup/workflow/internal/ValueOrDone;

    return-object v0
.end method

.method public final invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p2, Lkotlin/coroutines/Continuation;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/workflow/internal/WorkflowNode$tick$$inlined$forEachActive$lambda$1;->create(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/internal/WorkflowNode$tick$$inlined$forEachActive$lambda$1;

    sget-object p2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, p2}, Lcom/squareup/workflow/internal/WorkflowNode$tick$$inlined$forEachActive$lambda$1;->invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->getCOROUTINE_SUSPENDED()Ljava/lang/Object;

    .line 180
    iget v0, p0, Lcom/squareup/workflow/internal/WorkflowNode$tick$$inlined$forEachActive$lambda$1;->label:I

    if-nez v0, :cond_2

    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    iget-object p1, p0, Lcom/squareup/workflow/internal/WorkflowNode$tick$$inlined$forEachActive$lambda$1;->p$0:Lcom/squareup/workflow/internal/ValueOrDone;

    .line 181
    invoke-virtual {p1}, Lcom/squareup/workflow/internal/ValueOrDone;->isDone()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 183
    iget-object p1, p0, Lcom/squareup/workflow/internal/WorkflowNode$tick$$inlined$forEachActive$lambda$1;->$child$inlined:Lcom/squareup/workflow/internal/WorkerChildNode;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/internal/WorkerChildNode;->setTombstone(Z)V

    const/4 p1, 0x0

    return-object p1

    .line 187
    :cond_0
    iget-object v0, p0, Lcom/squareup/workflow/internal/WorkflowNode$tick$$inlined$forEachActive$lambda$1;->$child$inlined:Lcom/squareup/workflow/internal/WorkerChildNode;

    invoke-virtual {p1}, Lcom/squareup/workflow/internal/ValueOrDone;->getValue()Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/workflow/internal/WorkerChildNode;->acceptUpdate(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    .line 189
    iget-object v0, p0, Lcom/squareup/workflow/internal/WorkflowNode$tick$$inlined$forEachActive$lambda$1;->this$0:Lcom/squareup/workflow/internal/WorkflowNode;

    if-eqz p1, :cond_1

    invoke-static {v0, p1}, Lcom/squareup/workflow/internal/WorkflowNode;->access$applyAction(Lcom/squareup/workflow/internal/WorkflowNode;Lcom/squareup/workflow/WorkflowAction;)Ljava/lang/Object;

    move-result-object p1

    return-object p1

    :cond_1
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.squareup.workflow.WorkflowAction<StateT, OutputT>"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 190
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
