.class public final Lcom/squareup/workflow/RenderingAndSnapshot;
.super Ljava/lang/Object;
.source "RenderingAndSnapshot.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<RenderingT:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u000b\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u0000*\u0006\u0008\u0000\u0010\u0001 \u00012\u00020\u0002B\u0015\u0012\u0006\u0010\u0003\u001a\u00028\u0000\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u000e\u0010\u000c\u001a\u00028\u0000H\u00c6\u0003\u00a2\u0006\u0002\u0010\u0008J\t\u0010\r\u001a\u00020\u0005H\u00c6\u0003J(\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u00002\u0008\u0008\u0002\u0010\u0003\u001a\u00028\u00002\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001\u00a2\u0006\u0002\u0010\u000fJ\u0013\u0010\u0010\u001a\u00020\u00112\u0008\u0010\u0012\u001a\u0004\u0018\u00010\u0002H\u00d6\u0003J\t\u0010\u0013\u001a\u00020\u0014H\u00d6\u0001J\t\u0010\u0015\u001a\u00020\u0016H\u00d6\u0001R\u0013\u0010\u0003\u001a\u00028\u0000\u00a2\u0006\n\n\u0002\u0010\t\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000b\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/workflow/RenderingAndSnapshot;",
        "RenderingT",
        "",
        "rendering",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)V",
        "getRendering",
        "()Ljava/lang/Object;",
        "Ljava/lang/Object;",
        "getSnapshot",
        "()Lcom/squareup/workflow/Snapshot;",
        "component1",
        "component2",
        "copy",
        "(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/workflow/RenderingAndSnapshot;",
        "equals",
        "",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "workflow-runtime"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final rendering:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TRenderingT;"
        }
    .end annotation
.end field

.field private final snapshot:Lcom/squareup/workflow/Snapshot;


# direct methods
.method public constructor <init>(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TRenderingT;",
            "Lcom/squareup/workflow/Snapshot;",
            ")V"
        }
    .end annotation

    const-string v0, "snapshot"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/workflow/RenderingAndSnapshot;->rendering:Ljava/lang/Object;

    iput-object p2, p0, Lcom/squareup/workflow/RenderingAndSnapshot;->snapshot:Lcom/squareup/workflow/Snapshot;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/workflow/RenderingAndSnapshot;Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;ILjava/lang/Object;)Lcom/squareup/workflow/RenderingAndSnapshot;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/squareup/workflow/RenderingAndSnapshot;->rendering:Ljava/lang/Object;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/squareup/workflow/RenderingAndSnapshot;->snapshot:Lcom/squareup/workflow/Snapshot;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/workflow/RenderingAndSnapshot;->copy(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/workflow/RenderingAndSnapshot;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TRenderingT;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/workflow/RenderingAndSnapshot;->rendering:Ljava/lang/Object;

    return-object v0
.end method

.method public final component2()Lcom/squareup/workflow/Snapshot;
    .locals 1

    iget-object v0, p0, Lcom/squareup/workflow/RenderingAndSnapshot;->snapshot:Lcom/squareup/workflow/Snapshot;

    return-object v0
.end method

.method public final copy(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/workflow/RenderingAndSnapshot;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TRenderingT;",
            "Lcom/squareup/workflow/Snapshot;",
            ")",
            "Lcom/squareup/workflow/RenderingAndSnapshot<",
            "TRenderingT;>;"
        }
    .end annotation

    const-string v0, "snapshot"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/workflow/RenderingAndSnapshot;

    invoke-direct {v0, p1, p2}, Lcom/squareup/workflow/RenderingAndSnapshot;-><init>(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/workflow/RenderingAndSnapshot;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/workflow/RenderingAndSnapshot;

    iget-object v0, p0, Lcom/squareup/workflow/RenderingAndSnapshot;->rendering:Ljava/lang/Object;

    iget-object v1, p1, Lcom/squareup/workflow/RenderingAndSnapshot;->rendering:Ljava/lang/Object;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/workflow/RenderingAndSnapshot;->snapshot:Lcom/squareup/workflow/Snapshot;

    iget-object p1, p1, Lcom/squareup/workflow/RenderingAndSnapshot;->snapshot:Lcom/squareup/workflow/Snapshot;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getRendering()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TRenderingT;"
        }
    .end annotation

    .line 22
    iget-object v0, p0, Lcom/squareup/workflow/RenderingAndSnapshot;->rendering:Ljava/lang/Object;

    return-object v0
.end method

.method public final getSnapshot()Lcom/squareup/workflow/Snapshot;
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/squareup/workflow/RenderingAndSnapshot;->snapshot:Lcom/squareup/workflow/Snapshot;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/workflow/RenderingAndSnapshot;->rendering:Ljava/lang/Object;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/workflow/RenderingAndSnapshot;->snapshot:Lcom/squareup/workflow/Snapshot;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "RenderingAndSnapshot(rendering="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/workflow/RenderingAndSnapshot;->rendering:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", snapshot="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/workflow/RenderingAndSnapshot;->snapshot:Lcom/squareup/workflow/Snapshot;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
