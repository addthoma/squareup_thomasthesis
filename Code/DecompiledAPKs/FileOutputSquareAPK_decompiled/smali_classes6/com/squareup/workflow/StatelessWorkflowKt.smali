.class public final Lcom/squareup/workflow/StatelessWorkflowKt;
.super Ljava/lang/Object;
.source "StatelessWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nStatelessWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 StatelessWorkflow.kt\ncom/squareup/workflow/StatelessWorkflowKt\n*L\n1#1,147:1\n86#1,6:148\n86#1,6:154\n*E\n*S KotlinDebug\n*F\n+ 1 StatelessWorkflow.kt\ncom/squareup/workflow/StatelessWorkflowKt\n*L\n99#1,6:148\n109#1,6:154\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000V\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001aq\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u0002H\u00030\u0001\"\u0004\u0008\u0000\u0010\u0004\"\u0008\u0008\u0001\u0010\u0003*\u00020\u0005\"\u0004\u0008\u0002\u0010\u0006*\u0014\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00060\u00072\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t2#\u0010\u000b\u001a\u001f\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u0002H\u00030\r\u0012\u0004\u0012\u00020\u000e0\u000c\u00a2\u0006\u0002\u0008\u000f\u001am\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u0002H\u00030\u0001\"\u0004\u0008\u0000\u0010\u0004\"\u0008\u0008\u0001\u0010\u0003*\u00020\u0005\"\u0004\u0008\u0002\u0010\u0006*\u0014\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00060\u00072\u0008\u0008\u0002\u0010\u0008\u001a\u00020\n2#\u0010\u000b\u001a\u001f\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u0002H\u00030\r\u0012\u0004\u0012\u00020\u000e0\u000c\u00a2\u0006\u0002\u0008\u000f\u001a^\u0010\u0010\u001a\u0014\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00120\u0011\"\u0004\u0008\u0000\u0010\u0004\"\u0008\u0008\u0001\u0010\u0003*\u00020\u0005\"\u0004\u0008\u0002\u0010\u0013\"\u0004\u0008\u0003\u0010\u0012*\u0014\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00130\u00112\u0012\u0010\u0014\u001a\u000e\u0012\u0004\u0012\u0002H\u0013\u0012\u0004\u0012\u0002H\u00120\u000c\u001a9\u0010\u0015\u001a\u0014\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00060\u0011\"\u0008\u0008\u0000\u0010\u0003*\u00020\u0005\"\u0004\u0008\u0001\u0010\u0006*\u00020\u00162\u0006\u0010\u0015\u001a\u0002H\u0006\u00a2\u0006\u0002\u0010\u0017\u001aq\u0010\u0018\u001a\u0014\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00060\u0011\"\u0004\u0008\u0000\u0010\u0004\"\u0008\u0008\u0001\u0010\u0003*\u00020\u0005\"\u0004\u0008\u0002\u0010\u0006*\u00020\u00162:\u0008\u0004\u0010\u0019\u001a4\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u0002H\u00030\u001b\u0012\u0013\u0012\u0011H\u0004\u00a2\u0006\u000c\u0008\u001c\u0012\u0008\u0008\u0008\u0012\u0004\u0008\u0008(\u001d\u0012\u0004\u0012\u0002H\u00060\u001a\u00a2\u0006\u0002\u0008\u000fH\u0086\u0008\u00a8\u0006\u001e"
    }
    d2 = {
        "action",
        "Lcom/squareup/workflow/WorkflowAction;",
        "",
        "OutputT",
        "PropsT",
        "",
        "RenderingT",
        "Lcom/squareup/workflow/StatelessWorkflow;",
        "name",
        "Lkotlin/Function0;",
        "",
        "update",
        "Lkotlin/Function1;",
        "Lcom/squareup/workflow/WorkflowAction$Updater;",
        "",
        "Lkotlin/ExtensionFunctionType;",
        "mapRendering",
        "Lcom/squareup/workflow/Workflow;",
        "ToRenderingT",
        "FromRenderingT",
        "transform",
        "rendering",
        "Lcom/squareup/workflow/Workflow$Companion;",
        "(Lcom/squareup/workflow/Workflow$Companion;Ljava/lang/Object;)Lcom/squareup/workflow/Workflow;",
        "stateless",
        "render",
        "Lkotlin/Function2;",
        "Lcom/squareup/workflow/RenderContext;",
        "Lkotlin/ParameterName;",
        "props",
        "workflow-core"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final action(Lcom/squareup/workflow/StatelessWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<PropsT:",
            "Ljava/lang/Object;",
            "OutputT:",
            "Ljava/lang/Object;",
            "RenderingT:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/StatelessWorkflow<",
            "-TPropsT;+TOutputT;+TRenderingT;>;",
            "Ljava/lang/String;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/workflow/WorkflowAction$Updater;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/workflow/WorkflowAction;"
        }
    .end annotation

    const-string v0, "$this$action"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "update"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 128
    new-instance v0, Lcom/squareup/workflow/StatelessWorkflowKt$action$1;

    invoke-direct {v0, p1}, Lcom/squareup/workflow/StatelessWorkflowKt$action$1;-><init>(Ljava/lang/String;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p0, v0, p2}, Lcom/squareup/workflow/StatelessWorkflowKt;->action(Lcom/squareup/workflow/StatelessWorkflow;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final action(Lcom/squareup/workflow/StatelessWorkflow;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<PropsT:",
            "Ljava/lang/Object;",
            "OutputT:",
            "Ljava/lang/Object;",
            "RenderingT:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/StatelessWorkflow<",
            "-TPropsT;+TOutputT;+TRenderingT;>;",
            "Lkotlin/jvm/functions/Function0<",
            "Ljava/lang/String;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/workflow/WorkflowAction$Updater;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/workflow/WorkflowAction;"
        }
    .end annotation

    const-string v0, "$this$action"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "name"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "update"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 143
    new-instance v0, Lcom/squareup/workflow/StatelessWorkflowKt$action$2;

    invoke-direct {v0, p0, p2, p1}, Lcom/squareup/workflow/StatelessWorkflowKt$action$2;-><init>(Lcom/squareup/workflow/StatelessWorkflow;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V

    check-cast v0, Lcom/squareup/workflow/WorkflowAction;

    return-object v0
.end method

.method public static synthetic action$default(Lcom/squareup/workflow/StatelessWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;
    .locals 0

    and-int/lit8 p3, p3, 0x1

    if-eqz p3, :cond_0

    const-string p1, ""

    .line 126
    :cond_0
    invoke-static {p0, p1, p2}, Lcom/squareup/workflow/StatelessWorkflowKt;->action(Lcom/squareup/workflow/StatelessWorkflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p0

    return-object p0
.end method

.method public static final mapRendering(Lcom/squareup/workflow/Workflow;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Workflow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<PropsT:",
            "Ljava/lang/Object;",
            "OutputT:",
            "Ljava/lang/Object;",
            "FromRenderingT:",
            "Ljava/lang/Object;",
            "ToRenderingT:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/Workflow<",
            "-TPropsT;+TOutputT;+TFromRenderingT;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-TFromRenderingT;+TToRenderingT;>;)",
            "Lcom/squareup/workflow/Workflow<",
            "TPropsT;TOutputT;TToRenderingT;>;"
        }
    .end annotation

    const-string v0, "$this$mapRendering"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transform"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 109
    sget-object v0, Lcom/squareup/workflow/Workflow;->Companion:Lcom/squareup/workflow/Workflow$Companion;

    .line 154
    new-instance v0, Lcom/squareup/workflow/StatelessWorkflowKt$mapRendering$$inlined$stateless$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/workflow/StatelessWorkflowKt$mapRendering$$inlined$stateless$1;-><init>(Lcom/squareup/workflow/Workflow;Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Lcom/squareup/workflow/Workflow;

    return-object v0
.end method

.method public static final rendering(Lcom/squareup/workflow/Workflow$Companion;Ljava/lang/Object;)Lcom/squareup/workflow/Workflow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<OutputT:",
            "Ljava/lang/Object;",
            "RenderingT:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/Workflow$Companion;",
            "TRenderingT;)",
            "Lcom/squareup/workflow/Workflow<",
            "Lkotlin/Unit;",
            "TOutputT;TRenderingT;>;"
        }
    .end annotation

    const-string v0, "$this$rendering"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 148
    new-instance p0, Lcom/squareup/workflow/StatelessWorkflowKt$rendering$$inlined$stateless$1;

    invoke-direct {p0, p1}, Lcom/squareup/workflow/StatelessWorkflowKt$rendering$$inlined$stateless$1;-><init>(Ljava/lang/Object;)V

    check-cast p0, Lcom/squareup/workflow/Workflow;

    return-object p0
.end method

.method public static final stateless(Lcom/squareup/workflow/Workflow$Companion;Lkotlin/jvm/functions/Function2;)Lcom/squareup/workflow/Workflow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<PropsT:",
            "Ljava/lang/Object;",
            "OutputT:",
            "Ljava/lang/Object;",
            "RenderingT:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/Workflow$Companion;",
            "Lkotlin/jvm/functions/Function2<",
            "-",
            "Lcom/squareup/workflow/RenderContext;",
            "-TPropsT;+TRenderingT;>;)",
            "Lcom/squareup/workflow/Workflow<",
            "TPropsT;TOutputT;TRenderingT;>;"
        }
    .end annotation

    const-string v0, "$this$stateless"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p0, "render"

    invoke-static {p1, p0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    new-instance p0, Lcom/squareup/workflow/StatelessWorkflowKt$stateless$1;

    invoke-direct {p0, p1}, Lcom/squareup/workflow/StatelessWorkflowKt$stateless$1;-><init>(Lkotlin/jvm/functions/Function2;)V

    check-cast p0, Lcom/squareup/workflow/Workflow;

    return-object p0
.end method
