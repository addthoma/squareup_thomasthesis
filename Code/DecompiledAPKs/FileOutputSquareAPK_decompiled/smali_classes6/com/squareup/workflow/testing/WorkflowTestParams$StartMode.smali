.class public abstract Lcom/squareup/workflow/testing/WorkflowTestParams$StartMode;
.super Ljava/lang/Object;
.source "WorkflowTestParams.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/workflow/testing/WorkflowTestParams;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "StartMode"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/workflow/testing/WorkflowTestParams$StartMode$StartFresh;,
        Lcom/squareup/workflow/testing/WorkflowTestParams$StartMode$StartFromWorkflowSnapshot;,
        Lcom/squareup/workflow/testing/WorkflowTestParams$StartMode$StartFromCompleteSnapshot;,
        Lcom/squareup/workflow/testing/WorkflowTestParams$StartMode$StartFromState;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<StateT:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u0000*\u0006\u0008\u0001\u0010\u0001 \u00012\u00020\u0002:\u0004\u0004\u0005\u0006\u0007B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0003\u0082\u0001\u0004\u0008\t\n\u000b\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/workflow/testing/WorkflowTestParams$StartMode;",
        "StateT",
        "",
        "()V",
        "StartFresh",
        "StartFromCompleteSnapshot",
        "StartFromState",
        "StartFromWorkflowSnapshot",
        "Lcom/squareup/workflow/testing/WorkflowTestParams$StartMode$StartFresh;",
        "Lcom/squareup/workflow/testing/WorkflowTestParams$StartMode$StartFromWorkflowSnapshot;",
        "Lcom/squareup/workflow/testing/WorkflowTestParams$StartMode$StartFromCompleteSnapshot;",
        "Lcom/squareup/workflow/testing/WorkflowTestParams$StartMode$StartFromState;",
        "workflow-runtime"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 52
    invoke-direct {p0}, Lcom/squareup/workflow/testing/WorkflowTestParams$StartMode;-><init>()V

    return-void
.end method
