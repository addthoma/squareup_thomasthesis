.class public final Lcom/squareup/workflow/rx1/EventChannelKt$asRx1EventChannel$1;
.super Ljava/lang/Object;
.source "EventChannel.kt"

# interfaces
.implements Lcom/squareup/workflow/rx1/EventChannel;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/rx1/EventChannelKt;->asRx1EventChannel(Lcom/squareup/workflow/legacy/rx2/EventChannel;)Lcom/squareup/workflow/rx1/EventChannel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/rx1/EventChannel<",
        "TE;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000)\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u0008\u0012\u0004\u0012\u00028\u00000\u0001J?\u0010\u0002\u001a\n\u0012\u0006\u0008\u0001\u0012\u0002H\u00040\u0003\"\u0008\u0008\u0001\u0010\u0004*\u00020\u00052#\u0010\u0006\u001a\u001f\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u0002H\u00040\u0008\u0012\u0004\u0012\u00020\t0\u0007\u00a2\u0006\u0002\u0008\nH\u0016\u00a8\u0006\u000b"
    }
    d2 = {
        "com/squareup/workflow/rx1/EventChannelKt$asRx1EventChannel$1",
        "Lcom/squareup/workflow/rx1/EventChannel;",
        "select",
        "Lrx/Single;",
        "R",
        "",
        "block",
        "Lkotlin/Function1;",
        "Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder;",
        "",
        "Lkotlin/ExtensionFunctionType;",
        "pure-rx1"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $this_asRx1EventChannel:Lcom/squareup/workflow/legacy/rx2/EventChannel;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/legacy/rx2/EventChannel;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/rx2/EventChannel<",
            "TE;>;)V"
        }
    .end annotation

    .line 33
    iput-object p1, p0, Lcom/squareup/workflow/rx1/EventChannelKt$asRx1EventChannel$1;->$this_asRx1EventChannel:Lcom/squareup/workflow/legacy/rx2/EventChannel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public select(Lkotlin/jvm/functions/Function1;)Lrx/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/workflow/legacy/rx2/EventSelectBuilder<",
            "TE;TR;>;",
            "Lkotlin/Unit;",
            ">;)",
            "Lrx/Single<",
            "+TR;>;"
        }
    .end annotation

    const-string v0, "block"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    iget-object v0, p0, Lcom/squareup/workflow/rx1/EventChannelKt$asRx1EventChannel$1;->$this_asRx1EventChannel:Lcom/squareup/workflow/legacy/rx2/EventChannel;

    invoke-interface {v0, p1}, Lcom/squareup/workflow/legacy/rx2/EventChannel;->select(Lkotlin/jvm/functions/Function1;)Lio/reactivex/Single;

    move-result-object p1

    check-cast p1, Lio/reactivex/SingleSource;

    invoke-static {p1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Single(Lio/reactivex/SingleSource;)Lrx/Single;

    move-result-object p1

    .line 39
    new-instance v0, Lcom/squareup/workflow/rx1/HandleUncaughtExceptionOperator;

    invoke-direct {v0}, Lcom/squareup/workflow/rx1/HandleUncaughtExceptionOperator;-><init>()V

    check-cast v0, Lrx/Observable$Operator;

    invoke-virtual {p1, v0}, Lrx/Single;->lift(Lrx/Observable$Operator;)Lrx/Single;

    move-result-object p1

    const-string v0, "toV1Single(this@asRx1Eve\u2026aughtExceptionOperator())"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
