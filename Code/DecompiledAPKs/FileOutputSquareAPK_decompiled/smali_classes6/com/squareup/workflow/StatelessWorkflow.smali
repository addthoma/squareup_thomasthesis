.class public abstract Lcom/squareup/workflow/StatelessWorkflow;
.super Ljava/lang/Object;
.source "StatelessWorkflow.kt"

# interfaces
.implements Lcom/squareup/workflow/Workflow;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<PropsT:",
        "Ljava/lang/Object;",
        "OutputT:",
        "Ljava/lang/Object;",
        "RenderingT:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/Workflow<",
        "TPropsT;TOutputT;TRenderingT;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nStatelessWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 StatelessWorkflow.kt\ncom/squareup/workflow/StatelessWorkflow\n+ 2 StatefulWorkflow.kt\ncom/squareup/workflow/StatefulWorkflowKt\n*L\n1#1,147:1\n213#2,6:148\n167#2:154\n186#2:155\n219#2:156\n*E\n*S KotlinDebug\n*F\n+ 1 StatelessWorkflow.kt\ncom/squareup/workflow/StatelessWorkflow\n*L\n41#1,6:148\n41#1:154\n41#1:155\n41#1:156\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00006\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0008\u0002\u0008&\u0018\u0000*\u0006\u0008\u0000\u0010\u0001 \u0000*\n\u0008\u0001\u0010\u0002 \u0001*\u00020\u0003*\u0006\u0008\u0002\u0010\u0004 \u00012\u0014\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00040\u0005B\u0005\u00a2\u0006\u0002\u0010\u0006J\u001c\u0010\u000e\u001a\u0018\u0012\u0004\u0012\u00028\u0000\u0012\u0002\u0008\u0003\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\u0008J)\u0010\u000f\u001a\u00028\u00022\u0006\u0010\u000b\u001a\u00028\u00002\u0012\u0010\u0010\u001a\u000e\u0012\u0004\u0012\u00020\u0012\u0012\u0004\u0012\u00028\u00010\u0011H&\u00a2\u0006\u0002\u0010\u0013R;\u0010\u0007\u001a)\u0012\u0013\u0012\u00118\u0000\u00a2\u0006\u000c\u0008\t\u0012\u0008\u0008\n\u0012\u0004\u0008\u0008(\u000b\u0012\u0004\u0012\u00020\u000c\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u00020\u0008X\u0082\u0004\u00a2\u0006\u0008\n\u0000\u0012\u0004\u0008\r\u0010\u0006\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/workflow/StatelessWorkflow;",
        "PropsT",
        "OutputT",
        "",
        "RenderingT",
        "Lcom/squareup/workflow/Workflow;",
        "()V",
        "statefulWorkflow",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lkotlin/ParameterName;",
        "name",
        "props",
        "",
        "statefulWorkflow$annotations",
        "asStatefulWorkflow",
        "render",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "",
        "(Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;",
        "workflow-core"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final statefulWorkflow:Lcom/squareup/workflow/StatefulWorkflow;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/StatefulWorkflow<",
            "TPropsT;",
            "Lkotlin/Unit;",
            "TOutputT;TRenderingT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    sget-object v0, Lcom/squareup/workflow/Workflow;->Companion:Lcom/squareup/workflow/Workflow$Companion;

    .line 154
    new-instance v0, Lcom/squareup/workflow/StatelessWorkflow$$special$$inlined$stateful$1;

    invoke-direct {v0, p0}, Lcom/squareup/workflow/StatelessWorkflow$$special$$inlined$stateful$1;-><init>(Lcom/squareup/workflow/StatelessWorkflow;)V

    check-cast v0, Lcom/squareup/workflow/StatefulWorkflow;

    .line 156
    iput-object v0, p0, Lcom/squareup/workflow/StatelessWorkflow;->statefulWorkflow:Lcom/squareup/workflow/StatefulWorkflow;

    return-void
.end method

.method private static synthetic statefulWorkflow$annotations()V
    .locals 0

    return-void
.end method


# virtual methods
.method public final asStatefulWorkflow()Lcom/squareup/workflow/StatefulWorkflow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/StatefulWorkflow<",
            "TPropsT;*TOutputT;TRenderingT;>;"
        }
    .end annotation

    .line 73
    iget-object v0, p0, Lcom/squareup/workflow/StatelessWorkflow;->statefulWorkflow:Lcom/squareup/workflow/StatefulWorkflow;

    return-object v0
.end method

.method public abstract render(Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TPropsT;",
            "Lcom/squareup/workflow/RenderContext;",
            ")TRenderingT;"
        }
    .end annotation
.end method
