.class public interface abstract Lcom/squareup/workflow/AbstractWorkflowViewFactory$LayoutBinding;
.super Ljava/lang/Object;
.source "AbstractWorkflowViewFactory.kt"

# interfaces
.implements Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/workflow/AbstractWorkflowViewFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "LayoutBinding"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<D:",
        "Ljava/lang/Object;",
        "E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding<",
        "TD;TE;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000B\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u0002*\u0008\u0008\u0001\u0010\u0003*\u00020\u00022\u000e\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u00030\u0004JP\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0008\u0010\t\u001a\u0004\u0018\u00010\n2\u0012\u0010\u000b\u001a\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u000cj\u0002`\r2\u0018\u0010\u000e\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0010j\u0002`\u00110\u000f2\u0006\u0010\u0012\u001a\u00020\u0013H&\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory$LayoutBinding;",
        "D",
        "",
        "E",
        "Lcom/squareup/workflow/AbstractWorkflowViewFactory$Binding;",
        "inflate",
        "Landroid/view/View;",
        "contextForNewView",
        "Landroid/content/Context;",
        "container",
        "Landroid/view/ViewGroup;",
        "screenKey",
        "Lcom/squareup/workflow/legacy/Screen$Key;",
        "Lcom/squareup/workflow/legacy/AnyScreenKey;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "android_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract inflate(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/squareup/workflow/legacy/Screen$Key;Lio/reactivex/Observable;Lcom/squareup/workflow/ui/ContainerHints;)Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/view/ViewGroup;",
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "**>;",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;",
            "Lcom/squareup/workflow/ui/ContainerHints;",
            ")",
            "Landroid/view/View;"
        }
    .end annotation
.end method
