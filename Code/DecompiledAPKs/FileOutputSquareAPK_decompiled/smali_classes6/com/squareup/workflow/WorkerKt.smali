.class public final Lcom/squareup/workflow/WorkerKt;
.super Ljava/lang/Object;
.source "Worker.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nWorker.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Worker.kt\ncom/squareup/workflow/WorkerKt\n+ 2 Worker.kt\ncom/squareup/workflow/Worker$Companion\n*L\n1#1,422:1\n276#1:424\n276#1:425\n276#1:427\n319#1:428\n276#1:430\n328#1:431\n240#2:423\n203#2:426\n203#2:429\n*E\n*S KotlinDebug\n*F\n+ 1 Worker.kt\ncom/squareup/workflow/WorkerKt\n*L\n289#1:424\n299#1:425\n319#1:427\n289#1:423\n319#1:426\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\u001a!\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0006\u0008\u0000\u0010\u0002\u0018\u0001*\u0008\u0012\u0004\u0012\u0002H\u00020\u0003H\u0086\u0008\u001a!\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0006\u0008\u0000\u0010\u0002\u0018\u0001*\u0008\u0012\u0004\u0012\u0002H\u00020\u0004H\u0086\u0008\u001a+\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0006\u0008\u0000\u0010\u0002\u0018\u0001*\u0008\u0012\u0004\u0012\u0002H\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007H\u0086\u0008\u001a!\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0006\u0008\u0000\u0010\u0002\u0018\u0001*\u0008\u0012\u0004\u0012\u0002H\u00020\u0008H\u0086\u0008\u001aB\u0010\t\u001a\u0008\u0012\u0004\u0012\u0002H\n0\u0001\"\u0004\u0008\u0000\u0010\u000b\"\u0004\u0008\u0001\u0010\n*\u0008\u0012\u0004\u0012\u0002H\u000b0\u00012\u001e\u0010\t\u001a\u001a\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H\u000b0\u0008\u0012\n\u0012\u0008\u0012\u0004\u0012\u0002H\n0\u00080\u000c\u00a8\u0006\r"
    }
    d2 = {
        "asWorker",
        "Lcom/squareup/workflow/Worker;",
        "OutputT",
        "Lkotlinx/coroutines/Deferred;",
        "Lkotlinx/coroutines/channels/BroadcastChannel;",
        "Lkotlinx/coroutines/channels/ReceiveChannel;",
        "closeOnCancel",
        "",
        "Lkotlinx/coroutines/flow/Flow;",
        "transform",
        "R",
        "T",
        "Lkotlin/Function1;",
        "workflow-core"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final synthetic asWorker(Lkotlinx/coroutines/Deferred;)Lcom/squareup/workflow/Worker;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<OutputT:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlinx/coroutines/Deferred<",
            "+TOutputT;>;)",
            "Lcom/squareup/workflow/Worker<",
            "TOutputT;>;"
        }
    .end annotation

    const-string v0, "$this$asWorker"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 289
    sget-object v0, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v0, Lcom/squareup/workflow/WorkerKt$asWorker$1;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/workflow/WorkerKt$asWorker$1;-><init>(Lkotlinx/coroutines/Deferred;Lkotlin/coroutines/Continuation;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    .line 423
    invoke-static {v0}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p0

    const/4 v0, 0x6

    const-string v2, "OutputT"

    .line 424
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    new-instance v0, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {v0, v1, p0}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v0, Lcom/squareup/workflow/Worker;

    return-object v0
.end method

.method public static final synthetic asWorker(Lkotlinx/coroutines/channels/BroadcastChannel;)Lcom/squareup/workflow/Worker;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<OutputT:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlinx/coroutines/channels/BroadcastChannel<",
            "TOutputT;>;)",
            "Lcom/squareup/workflow/Worker<",
            "TOutputT;>;"
        }
    .end annotation

    const-string v0, "$this$asWorker"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 299
    invoke-static {p0}, Lkotlinx/coroutines/flow/FlowKt;->asFlow(Lkotlinx/coroutines/channels/BroadcastChannel;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p0

    const/4 v0, 0x6

    const-string v1, "OutputT"

    .line 425
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    new-instance v0, Lcom/squareup/workflow/TypedWorker;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p0}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v0, Lcom/squareup/workflow/Worker;

    return-object v0
.end method

.method public static final synthetic asWorker(Lkotlinx/coroutines/channels/ReceiveChannel;Z)Lcom/squareup/workflow/Worker;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<OutputT:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlinx/coroutines/channels/ReceiveChannel<",
            "+TOutputT;>;Z)",
            "Lcom/squareup/workflow/Worker<",
            "TOutputT;>;"
        }
    .end annotation

    const-string v0, "$this$asWorker"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 319
    sget-object v0, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance v0, Lcom/squareup/workflow/WorkerKt$asWorker$2;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/squareup/workflow/WorkerKt$asWorker$2;-><init>(Lkotlinx/coroutines/channels/ReceiveChannel;ZLkotlin/coroutines/Continuation;)V

    check-cast v0, Lkotlin/jvm/functions/Function2;

    .line 426
    invoke-static {v0}, Lkotlinx/coroutines/flow/FlowKt;->flow(Lkotlin/jvm/functions/Function2;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p0

    const/4 p1, 0x6

    const-string v0, "OutputT"

    .line 427
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    new-instance p1, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {p1, v1, p0}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast p1, Lcom/squareup/workflow/Worker;

    return-object p1
.end method

.method public static final synthetic asWorker(Lkotlinx/coroutines/flow/Flow;)Lcom/squareup/workflow/Worker;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<OutputT:",
            "Ljava/lang/Object;",
            ">(",
            "Lkotlinx/coroutines/flow/Flow<",
            "+TOutputT;>;)",
            "Lcom/squareup/workflow/Worker<",
            "TOutputT;>;"
        }
    .end annotation

    const-string v0, "$this$asWorker"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x6

    const-string v1, "OutputT"

    .line 276
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    new-instance v0, Lcom/squareup/workflow/TypedWorker;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p0}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v0, Lcom/squareup/workflow/Worker;

    return-object v0
.end method

.method public static synthetic asWorker$default(Lkotlinx/coroutines/channels/ReceiveChannel;ZILjava/lang/Object;)Lcom/squareup/workflow/Worker;
    .locals 0

    const/4 p3, 0x1

    and-int/2addr p2, p3

    if-eqz p2, :cond_0

    const/4 p1, 0x1

    :cond_0
    const-string p2, "$this$asWorker"

    .line 318
    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 428
    sget-object p2, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    new-instance p2, Lcom/squareup/workflow/WorkerKt$asWorker$2;

    const/4 p3, 0x0

    invoke-direct {p2, p0, p1, p3}, Lcom/squareup/workflow/WorkerKt$asWorker$2;-><init>(Lkotlinx/coroutines/channels/ReceiveChannel;ZLkotlin/coroutines/Continuation;)V

    check-cast p2, Lkotlin/jvm/functions/Function2;

    .line 429
    invoke-static {p2}, Lkotlinx/coroutines/flow/FlowKt;->flow(Lkotlin/jvm/functions/Function2;)Lkotlinx/coroutines/flow/Flow;

    move-result-object p0

    const/4 p1, 0x6

    const-string p2, "OutputT"

    .line 430
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->reifiedOperationMarker(ILjava/lang/String;)V

    new-instance p1, Lcom/squareup/workflow/TypedWorker;

    invoke-direct {p1, p3, p0}, Lcom/squareup/workflow/TypedWorker;-><init>(Lkotlin/reflect/KType;Lkotlinx/coroutines/flow/Flow;)V

    check-cast p1, Lcom/squareup/workflow/Worker;

    return-object p1
.end method

.method public static final transform(Lcom/squareup/workflow/Worker;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/Worker;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/Worker<",
            "+TT;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lkotlinx/coroutines/flow/Flow<",
            "+TT;>;+",
            "Lkotlinx/coroutines/flow/Flow<",
            "+TR;>;>;)",
            "Lcom/squareup/workflow/Worker<",
            "TR;>;"
        }
    .end annotation

    const-string v0, "$this$transform"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transform"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 366
    new-instance v0, Lcom/squareup/workflow/WorkerWrapper;

    .line 368
    invoke-interface {p0}, Lcom/squareup/workflow/Worker;->run()Lkotlinx/coroutines/flow/Flow;

    move-result-object v1

    invoke-interface {p1, v1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lkotlinx/coroutines/flow/Flow;

    .line 366
    invoke-direct {v0, p0, p1}, Lcom/squareup/workflow/WorkerWrapper;-><init>(Lcom/squareup/workflow/Worker;Lkotlinx/coroutines/flow/Flow;)V

    check-cast v0, Lcom/squareup/workflow/Worker;

    return-object v0
.end method
