.class public final Lcom/squareup/workflow/pos/WorkflowsV2PosWorkflowStarterKt$asPosWorkflowStarter$1;
.super Ljava/lang/Object;
.source "WorkflowsV2PosWorkflowStarter.kt"

# interfaces
.implements Lcom/squareup/container/PosWorkflowStarter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/pos/WorkflowsV2PosWorkflowStarterKt;->asPosWorkflowStarter(Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;Ljava/lang/Object;)Lcom/squareup/container/PosWorkflowStarter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/container/PosWorkflowStarter<",
        "TI;TO;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00009\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0001J`\u0010\u0002\u001aP\u00120\u0012.\u0012*\u0012(\u0012\u0006\u0008\u0001\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\n\u0012\u0006\u0008\u0001\u0012\u00020\u0006`\t0\u0004\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0003j\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001`\n2\u0008\u0010\u000b\u001a\u0004\u0018\u00010\u000cH\u0016Jl\u0010\r\u001a4\u00120\u0012.\u0012*\u0012(\u0012\u0006\u0008\u0001\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\n\u0012\u0006\u0008\u0001\u0012\u00020\u0006`\t0\u00040\u000e*(\u0012\u0006\u0008\u0001\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\n\u0012\u0006\u0008\u0001\u0012\u00020\u0006`\t2\u0006\u0010\u000b\u001a\u00020\u000cH\u0002\u00a8\u0006\u000f"
    }
    d2 = {
        "com/squareup/workflow/pos/WorkflowsV2PosWorkflowStarterKt$asPosWorkflowStarter$1",
        "Lcom/squareup/container/PosWorkflowStarter;",
        "start",
        "Lcom/squareup/workflow/legacy/Workflow;",
        "Lcom/squareup/workflow/ScreenState;",
        "",
        "Lcom/squareup/container/ContainerLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "Lcom/squareup/container/PosWorkflow;",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "toScreenState",
        "Lio/reactivex/Observable;",
        "v2-pos-integration"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $input:Ljava/lang/Object;

.field final synthetic $this_asPosWorkflowStarter:Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Launcher<",
            "Lcom/squareup/workflow/legacyintegration/LegacyState<",
            "TI;",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;-TI;+TO;>;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .line 52
    iput-object p1, p0, Lcom/squareup/workflow/pos/WorkflowsV2PosWorkflowStarterKt$asPosWorkflowStarter$1;->$this_asPosWorkflowStarter:Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;

    iput-object p2, p0, Lcom/squareup/workflow/pos/WorkflowsV2PosWorkflowStarterKt$asPosWorkflowStarter$1;->$input:Ljava/lang/Object;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final synthetic access$toScreenState(Lcom/squareup/workflow/pos/WorkflowsV2PosWorkflowStarterKt$asPosWorkflowStarter$1;Ljava/util/Map;Lcom/squareup/workflow/Snapshot;)Lio/reactivex/Observable;
    .locals 0

    .line 52
    invoke-direct {p0, p1, p2}, Lcom/squareup/workflow/pos/WorkflowsV2PosWorkflowStarterKt$asPosWorkflowStarter$1;->toScreenState(Ljava/util/Map;Lcom/squareup/workflow/Snapshot;)Lio/reactivex/Observable;

    move-result-object p0

    return-object p0
.end method

.method private final toScreenState(Ljava/util/Map;Lcom/squareup/workflow/Snapshot;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "+",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;",
            "Lcom/squareup/workflow/Snapshot;",
            ")",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/ScreenState<",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;>;"
        }
    .end annotation

    .line 68
    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lio/reactivex/Observable;->never()Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "never()"

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/squareup/workflow/ScreenState;

    invoke-direct {v0, p1, p2}, Lcom/squareup/workflow/ScreenState;-><init>(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)V

    invoke-static {v0}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "just(ScreenState(this, snapshot))"

    :goto_0
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method


# virtual methods
.method public start(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/workflow/legacy/Workflow;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/Snapshot;",
            ")",
            "Lcom/squareup/workflow/legacy/Workflow<",
            "Lcom/squareup/workflow/ScreenState<",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;TI;TO;>;"
        }
    .end annotation

    .line 55
    new-instance v0, Lcom/squareup/workflow/legacy/WorkflowPool;

    invoke-direct {v0}, Lcom/squareup/workflow/legacy/WorkflowPool;-><init>()V

    .line 57
    new-instance v7, Lcom/squareup/workflow/legacyintegration/LegacyState;

    .line 58
    iget-object v2, p0, Lcom/squareup/workflow/pos/WorkflowsV2PosWorkflowStarterKt$asPosWorkflowStarter$1;->$input:Ljava/lang/Object;

    const/4 v3, 0x0

    const/4 v5, 0x2

    const/4 v6, 0x0

    move-object v1, v7

    move-object v4, p1

    .line 57
    invoke-direct/range {v1 .. v6}, Lcom/squareup/workflow/legacyintegration/LegacyState;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 61
    iget-object p1, p0, Lcom/squareup/workflow/pos/WorkflowsV2PosWorkflowStarterKt$asPosWorkflowStarter$1;->$this_asPosWorkflowStarter:Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;

    invoke-interface {p1, v7, v0}, Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;->launch(Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p1

    .line 62
    new-instance v0, Lcom/squareup/workflow/pos/WorkflowsV2PosWorkflowStarterKt$asPosWorkflowStarter$1$start$1;

    invoke-direct {v0, p0}, Lcom/squareup/workflow/pos/WorkflowsV2PosWorkflowStarterKt$asPosWorkflowStarter$1$start$1;-><init>(Lcom/squareup/workflow/pos/WorkflowsV2PosWorkflowStarterKt$asPosWorkflowStarter$1;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v0}, Lcom/squareup/workflow/legacy/rx2/WorkflowOperatorsKt;->switchMapState(Lcom/squareup/workflow/legacy/Workflow;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p1

    return-object p1
.end method
