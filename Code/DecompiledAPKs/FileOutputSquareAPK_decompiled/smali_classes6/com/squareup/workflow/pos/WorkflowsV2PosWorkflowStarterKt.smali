.class public final Lcom/squareup/workflow/pos/WorkflowsV2PosWorkflowStarterKt;
.super Ljava/lang/Object;
.source "WorkflowsV2PosWorkflowStarter.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000J\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001a{\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u00030\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0004\"\u0008\u0008\u0001\u0010\u0003*\u00020\u00042>\u0010\u0005\u001a:\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0003\u0012*\u0012(\u0012\u0006\u0008\u0001\u0012\u00020\u0008\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\tj\u0002`\n0\u0007j\n\u0012\u0006\u0008\u0001\u0012\u00020\u0008`\u000b0\u00062\u0006\u0010\u000c\u001a\u0002H\u00022\u0006\u0010\r\u001a\u00020\u000e\u00a2\u0006\u0002\u0010\u000f\u001ad\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u0002H\u00030\u0001\"\u0008\u0008\u0000\u0010\u0003*\u00020\u00042>\u0010\u0005\u001a:\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u0002H\u0003\u0012*\u0012(\u0012\u0006\u0008\u0001\u0012\u00020\u0008\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\tj\u0002`\n0\u0007j\n\u0012\u0006\u0008\u0001\u0012\u00020\u0008`\u000b0\u00062\u0006\u0010\r\u001a\u00020\u000e\u001a\u00ad\u0001\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u0002H\u0012\u0012\u0004\u0012\u0002H\u00130\u0001\"\u0008\u0008\u0000\u0010\u0012*\u00020\u0004\"\u0008\u0008\u0001\u0010\u0013*\u00020\u0004*v\u0012*\u0012(\u0012\u0004\u0012\u0002H\u0012\u0012\u001e\u0012\u001c\u0012\u0006\u0008\u0001\u0012\u00020\u0008\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\tj\u0002`\n0\u00070\u0015\u0012\u0004\u0012\u0002H\u0012\u0012\u0004\u0012\u0002H\u00130\u0014j:\u0012\u0004\u0012\u0002H\u0012\u0012\u0004\u0012\u0002H\u0013\u0012*\u0012(\u0012\u0006\u0008\u0001\u0012\u00020\u0008\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\tj\u0002`\n0\u0007j\n\u0012\u0006\u0008\u0001\u0012\u00020\u0008`\u000b`\u00162\u0006\u0010\u000c\u001a\u0002H\u0012H\u0002\u00a2\u0006\u0002\u0010\u0017\u00a8\u0006\u0018"
    }
    d2 = {
        "createPosWorkflowStarter",
        "Lcom/squareup/container/PosWorkflowStarter;",
        "InputT",
        "OutputT",
        "",
        "workflow",
        "Lcom/squareup/workflow/Workflow;",
        "",
        "Lcom/squareup/container/ContainerLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "input",
        "mainDispatcher",
        "Lkotlinx/coroutines/CoroutineDispatcher;",
        "(Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Lkotlinx/coroutines/CoroutineDispatcher;)Lcom/squareup/container/PosWorkflowStarter;",
        "",
        "asPosWorkflowStarter",
        "I",
        "O",
        "Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;",
        "Lcom/squareup/workflow/legacyintegration/LegacyState;",
        "Lcom/squareup/workflow/legacyintegration/LegacyLauncher;",
        "(Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;Ljava/lang/Object;)Lcom/squareup/container/PosWorkflowStarter;",
        "v2-pos-integration"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private static final asPosWorkflowStarter(Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;Ljava/lang/Object;)Lcom/squareup/container/PosWorkflowStarter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<I:",
            "Ljava/lang/Object;",
            "O:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Launcher<",
            "Lcom/squareup/workflow/legacyintegration/LegacyState<",
            "TI;",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;-TI;+TO;>;TI;)",
            "Lcom/squareup/container/PosWorkflowStarter<",
            "TI;TO;>;"
        }
    .end annotation

    .line 52
    new-instance v0, Lcom/squareup/workflow/pos/WorkflowsV2PosWorkflowStarterKt$asPosWorkflowStarter$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/workflow/pos/WorkflowsV2PosWorkflowStarterKt$asPosWorkflowStarter$1;-><init>(Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;Ljava/lang/Object;)V

    check-cast v0, Lcom/squareup/container/PosWorkflowStarter;

    return-object v0
.end method

.method public static final createPosWorkflowStarter(Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Lkotlinx/coroutines/CoroutineDispatcher;)Lcom/squareup/container/PosWorkflowStarter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<InputT:",
            "Ljava/lang/Object;",
            "OutputT:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/Workflow<",
            "-TInputT;+TOutputT;+",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "+",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;TInputT;",
            "Lkotlinx/coroutines/CoroutineDispatcher;",
            ")",
            "Lcom/squareup/container/PosWorkflowStarter<",
            "TInputT;TOutputT;>;"
        }
    .end annotation

    const-string v0, "workflow"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainDispatcher"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    invoke-static {p0, p2}, Lcom/squareup/workflow/legacyintegration/LegacyLauncherKt;->createLegacyLauncher(Lcom/squareup/workflow/Workflow;Lkotlinx/coroutines/CoroutineDispatcher;)Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;

    move-result-object p0

    .line 34
    invoke-static {p0, p1}, Lcom/squareup/workflow/pos/WorkflowsV2PosWorkflowStarterKt;->asPosWorkflowStarter(Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;Ljava/lang/Object;)Lcom/squareup/container/PosWorkflowStarter;

    move-result-object p0

    return-object p0
.end method

.method public static final createPosWorkflowStarter(Lcom/squareup/workflow/Workflow;Lkotlinx/coroutines/CoroutineDispatcher;)Lcom/squareup/container/PosWorkflowStarter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<OutputT:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/Workflow<",
            "-",
            "Lkotlin/Unit;",
            "+TOutputT;+",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "+",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;",
            "Lkotlinx/coroutines/CoroutineDispatcher;",
            ")",
            "Lcom/squareup/container/PosWorkflowStarter<",
            "Lkotlin/Unit;",
            "TOutputT;>;"
        }
    .end annotation

    const-string v0, "workflow"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainDispatcher"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-static {p0, v0, p1}, Lcom/squareup/workflow/pos/WorkflowsV2PosWorkflowStarterKt;->createPosWorkflowStarter(Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Lkotlinx/coroutines/CoroutineDispatcher;)Lcom/squareup/container/PosWorkflowStarter;

    move-result-object p0

    return-object p0
.end method
