.class public final Lcom/squareup/workflow/legacyintegration/RenderContextsKt;
.super Ljava/lang/Object;
.source "RenderContexts.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u001aZ\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\u0008\u0008\u0001\u0010\u0004*\u00020\u0003\"\u0008\u0008\u0002\u0010\u0005*\u00020\u0003*\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u00062\u001e\u0010\u0007\u001a\u001a\u0012\u0004\u0012\u0002H\u0002\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\t0\u0008\u00a8\u0006\n"
    }
    d2 = {
        "makeLegacyWorkflowInput",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "EventT",
        "",
        "StateT",
        "OutputT",
        "Lcom/squareup/workflow/RenderContext;",
        "handler",
        "Lkotlin/Function1;",
        "Lcom/squareup/workflow/WorkflowAction;",
        "v2-legacy-integration"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final makeLegacyWorkflowInput(Lcom/squareup/workflow/RenderContext;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/WorkflowInput;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<EventT:",
            "Ljava/lang/Object;",
            "StateT:",
            "Ljava/lang/Object;",
            "OutputT:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/RenderContext<",
            "TStateT;-TOutputT;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-TEventT;+",
            "Lcom/squareup/workflow/WorkflowAction<",
            "TStateT;+TOutputT;>;>;)",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "TEventT;>;"
        }
    .end annotation

    const-string v0, "$this$makeLegacyWorkflowInput"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "handler"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-interface {p0, p1}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/workflow/legacy/WorkflowInputKt;->WorkflowInput(Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p0

    return-object p0
.end method
