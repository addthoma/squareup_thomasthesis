.class final Lcom/squareup/workflow/legacyintegration/LegacyStateKt$readLegacyState$1;
.super Lkotlin/jvm/internal/Lambda;
.source "LegacyState.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/legacyintegration/LegacyStateKt;->readLegacyState(Lokio/BufferedSource;Ljava/lang/Object;)Lcom/squareup/workflow/legacyintegration/LegacyState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lokio/BufferedSource;",
        "Lcom/squareup/workflow/Snapshot;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0003\"\u0008\u0008\u0001\u0010\u0004*\u00020\u0003*\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/Snapshot;",
        "InputT",
        "",
        "RenderingT",
        "Lokio/BufferedSource;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/workflow/legacyintegration/LegacyStateKt$readLegacyState$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/workflow/legacyintegration/LegacyStateKt$readLegacyState$1;

    invoke-direct {v0}, Lcom/squareup/workflow/legacyintegration/LegacyStateKt$readLegacyState$1;-><init>()V

    sput-object v0, Lcom/squareup/workflow/legacyintegration/LegacyStateKt$readLegacyState$1;->INSTANCE:Lcom/squareup/workflow/legacyintegration/LegacyStateKt$readLegacyState$1;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lokio/BufferedSource;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 143
    sget-object v0, Lcom/squareup/workflow/Snapshot;->Companion:Lcom/squareup/workflow/Snapshot$Companion;

    invoke-static {p1}, Lcom/squareup/workflow/SnapshotKt;->readByteStringWithLength(Lokio/BufferedSource;)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/workflow/Snapshot$Companion;->of(Lokio/ByteString;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lokio/BufferedSource;

    invoke-virtual {p0, p1}, Lcom/squareup/workflow/legacyintegration/LegacyStateKt$readLegacyState$1;->invoke(Lokio/BufferedSource;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
