.class final Lcom/squareup/workflow/legacyintegration/RealLegacyLauncher$launch$1;
.super Lkotlin/jvm/internal/Lambda;
.source "LegacyLauncher.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/legacyintegration/RealLegacyLauncher;->launch(Lcom/squareup/workflow/legacyintegration/LegacyState;Lcom/squareup/workflow/legacy/WorkflowPool;)Lcom/squareup/workflow/legacy/Workflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Lkotlinx/coroutines/CoroutineScope;",
        "Lcom/squareup/workflow/WorkflowSession<",
        "+TOutputT;+TRenderingT;>;",
        "Lcom/squareup/workflow/legacyintegration/RealLegacyLauncher$launch$1$2;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nLegacyLauncher.kt\nKotlin\n*S Kotlin\n*F\n+ 1 LegacyLauncher.kt\ncom/squareup/workflow/legacyintegration/RealLegacyLauncher$launch$1\n+ 2 Transform.kt\nkotlinx/coroutines/flow/FlowKt__TransformKt\n+ 3 Emitters.kt\nkotlinx/coroutines/flow/FlowKt__EmittersKt\n+ 4 SafeCollector.common.kt\nkotlinx/coroutines/flow/internal/SafeCollector_commonKt\n*L\n1#1,138:1\n47#2:139\n49#2:143\n51#3:140\n56#3:142\n106#4:141\n*E\n*S KotlinDebug\n*F\n+ 1 LegacyLauncher.kt\ncom/squareup/workflow/legacyintegration/RealLegacyLauncher$launch$1\n*L\n109#1:139\n109#1:143\n109#1:140\n109#1:142\n109#1:141\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001b\n\u0000\n\u0002\u0008\u0004\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002*\u0001\u0001\u0010\u0000\u001a\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00040\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0005\"\u0008\u0008\u0001\u0010\u0003*\u00020\u0005\"\u0008\u0008\u0002\u0010\u0004*\u00020\u0005*\u00020\u00062\u0012\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00040\u0008H\n\u00a2\u0006\u0004\u0008\t\u0010\n"
    }
    d2 = {
        "<anonymous>",
        "com/squareup/workflow/legacyintegration/RealLegacyLauncher$launch$1$2",
        "InputT",
        "OutputT",
        "RenderingT",
        "",
        "Lkotlinx/coroutines/CoroutineScope;",
        "session",
        "Lcom/squareup/workflow/WorkflowSession;",
        "invoke",
        "(Lkotlinx/coroutines/CoroutineScope;Lcom/squareup/workflow/WorkflowSession;)Lcom/squareup/workflow/legacyintegration/RealLegacyLauncher$launch$1$2;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $initialInput:Ljava/lang/Object;

.field final synthetic $inputEvents:Lkotlinx/coroutines/channels/Channel;

.field final synthetic $scope:Lkotlinx/coroutines/CoroutineScope;


# direct methods
.method constructor <init>(Ljava/lang/Object;Lkotlinx/coroutines/CoroutineScope;Lkotlinx/coroutines/channels/Channel;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/workflow/legacyintegration/RealLegacyLauncher$launch$1;->$initialInput:Ljava/lang/Object;

    iput-object p2, p0, Lcom/squareup/workflow/legacyintegration/RealLegacyLauncher$launch$1;->$scope:Lkotlinx/coroutines/CoroutineScope;

    iput-object p3, p0, Lcom/squareup/workflow/legacyintegration/RealLegacyLauncher$launch$1;->$inputEvents:Lkotlinx/coroutines/channels/Channel;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lkotlinx/coroutines/CoroutineScope;Lcom/squareup/workflow/WorkflowSession;)Lcom/squareup/workflow/legacyintegration/RealLegacyLauncher$launch$1$2;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlinx/coroutines/CoroutineScope;",
            "Lcom/squareup/workflow/WorkflowSession<",
            "+TOutputT;+TRenderingT;>;)",
            "Lcom/squareup/workflow/legacyintegration/RealLegacyLauncher$launch$1$2;"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "session"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 109
    invoke-virtual {p2}, Lcom/squareup/workflow/WorkflowSession;->getRenderingsAndSnapshots()Lkotlinx/coroutines/flow/Flow;

    move-result-object v0

    .line 141
    new-instance v1, Lcom/squareup/workflow/legacyintegration/RealLegacyLauncher$launch$1$$special$$inlined$map$1;

    invoke-direct {v1, v0, p0}, Lcom/squareup/workflow/legacyintegration/RealLegacyLauncher$launch$1$$special$$inlined$map$1;-><init>(Lkotlinx/coroutines/flow/Flow;Lcom/squareup/workflow/legacyintegration/RealLegacyLauncher$launch$1;)V

    check-cast v1, Lkotlinx/coroutines/flow/Flow;

    .line 113
    sget-object v4, Lkotlinx/coroutines/CoroutineStart;->UNDISPATCHED:Lkotlinx/coroutines/CoroutineStart;

    new-instance v0, Lcom/squareup/workflow/legacyintegration/RealLegacyLauncher$launch$1$result$1;

    const/4 v2, 0x0

    invoke-direct {v0, p2, v2}, Lcom/squareup/workflow/legacyintegration/RealLegacyLauncher$launch$1$result$1;-><init>(Lcom/squareup/workflow/WorkflowSession;Lkotlin/coroutines/Continuation;)V

    move-object v5, v0

    check-cast v5, Lkotlin/jvm/functions/Function2;

    const/4 v3, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    move-object v2, p1

    invoke-static/range {v2 .. v7}, Lkotlinx/coroutines/BuildersKt;->async$default(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Deferred;

    move-result-object p2

    .line 114
    new-instance v0, Lcom/squareup/workflow/legacyintegration/RealLegacyLauncher$launch$1$1;

    invoke-direct {v0, p1}, Lcom/squareup/workflow/legacyintegration/RealLegacyLauncher$launch$1$1;-><init>(Lkotlinx/coroutines/CoroutineScope;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-interface {p2, v0}, Lkotlinx/coroutines/Deferred;->invokeOnCompletion(Lkotlin/jvm/functions/Function1;)Lkotlinx/coroutines/DisposableHandle;

    .line 122
    new-instance p1, Lcom/squareup/workflow/legacyintegration/RealLegacyLauncher$launch$1$2;

    invoke-direct {p1, p0, v1, p2}, Lcom/squareup/workflow/legacyintegration/RealLegacyLauncher$launch$1$2;-><init>(Lcom/squareup/workflow/legacyintegration/RealLegacyLauncher$launch$1;Lkotlinx/coroutines/flow/Flow;Lkotlinx/coroutines/Deferred;)V

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 83
    check-cast p1, Lkotlinx/coroutines/CoroutineScope;

    check-cast p2, Lcom/squareup/workflow/WorkflowSession;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/workflow/legacyintegration/RealLegacyLauncher$launch$1;->invoke(Lkotlinx/coroutines/CoroutineScope;Lcom/squareup/workflow/WorkflowSession;)Lcom/squareup/workflow/legacyintegration/RealLegacyLauncher$launch$1$2;

    move-result-object p1

    return-object p1
.end method
