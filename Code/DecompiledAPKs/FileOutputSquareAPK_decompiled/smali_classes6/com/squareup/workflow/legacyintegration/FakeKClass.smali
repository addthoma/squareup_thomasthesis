.class public final Lcom/squareup/workflow/legacyintegration/FakeKClass;
.super Ljava/lang/Object;
.source "LegacyState.kt"

# interfaces
.implements Lkotlin/reflect/KClass;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<InputT:",
        "Ljava/lang/Object;",
        "OutputT:",
        "Ljava/lang/Object;",
        "RenderingT:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lkotlin/reflect/KClass<",
        "Lcom/squareup/workflow/legacy/WorkflowPool$Launcher<",
        "Lcom/squareup/workflow/legacyintegration/LegacyState<",
        "TInputT;TRenderingT;>;-TInputT;+TOutputT;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000r\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0010\u001b\n\u0002\u0008\u0003\n\u0002\u0010\u001e\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0010\u000e\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u0008\n\u0002\u0008\u0003\u0008\u0001\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u0002*\u0008\u0008\u0001\u0010\u0003*\u00020\u0002*\u0008\u0008\u0002\u0010\u0004*\u00020\u00022<\u00128\u00126\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u00040\u0007\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u00030\u0006j\u0014\u0012\u0004\u0012\u0002H\u0001\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u0004`\u00080\u0005B\u0005\u00a2\u0006\u0002\u0010\tJ\u0013\u00107\u001a\u00020\u00152\u0008\u00108\u001a\u0004\u0018\u00010\u0002H\u0096\u0002J\u0008\u00109\u001a\u00020:H\u0016J\u0012\u0010;\u001a\u00020\u00152\u0008\u0010<\u001a\u0004\u0018\u00010\u0002H\u0016R\u001a\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000bX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eRT\u0010\u000f\u001aB\u0012>\u0012<\u00128\u00126\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00020\u0007\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0006j\u0014\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u0002`\u00080\u00110\u0010X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013R\u0014\u0010\u0014\u001a\u00020\u0015X\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0014\u0010\u0016R\u0014\u0010\u0017\u001a\u00020\u0015X\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\u0016R\u0014\u0010\u0018\u001a\u00020\u0015X\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u0016R\u0014\u0010\u0019\u001a\u00020\u0015X\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0019\u0010\u0016R\u0014\u0010\u001a\u001a\u00020\u0015X\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001a\u0010\u0016R\u0014\u0010\u001b\u001a\u00020\u0015X\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001b\u0010\u0016R\u0014\u0010\u001c\u001a\u00020\u0015X\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001c\u0010\u0016R\u001e\u0010\u001d\u001a\u000c\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030\u001e0\u0010X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u001f\u0010\u0013R\u001e\u0010 \u001a\u000c\u0012\u0008\u0012\u0006\u0012\u0002\u0008\u00030\u00050\u0010X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008!\u0010\u0013RL\u0010\"\u001a:\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00020\u0007\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0018\u00010\u0006j\u0016\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u0002\u0018\u0001`\u0008X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008#\u0010$R\u0016\u0010%\u001a\u0004\u0018\u00010&X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\'\u0010(RV\u0010)\u001aD\u0012@\u0012>\u0012:\u0008\u0001\u00126\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00020\u0007\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0006j\u0014\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00028\u0002`\u00080\u00050\u000bX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008*\u0010\u000eR\u0016\u0010+\u001a\u0004\u0018\u00010&X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008,\u0010(R\u001a\u0010-\u001a\u0008\u0012\u0004\u0012\u00020.0\u000bX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008/\u0010\u000eR\u001a\u00100\u001a\u0008\u0012\u0004\u0012\u0002010\u000bX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u00082\u0010\u000eR\u0016\u00103\u001a\u0004\u0018\u000104X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u00085\u00106\u00a8\u0006="
    }
    d2 = {
        "Lcom/squareup/workflow/legacyintegration/FakeKClass;",
        "InputT",
        "",
        "OutputT",
        "RenderingT",
        "Lkotlin/reflect/KClass;",
        "Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;",
        "Lcom/squareup/workflow/legacyintegration/LegacyState;",
        "Lcom/squareup/workflow/legacyintegration/LegacyLauncher;",
        "()V",
        "annotations",
        "",
        "",
        "getAnnotations",
        "()Ljava/util/List;",
        "constructors",
        "",
        "Lkotlin/reflect/KFunction;",
        "getConstructors",
        "()Ljava/util/Collection;",
        "isAbstract",
        "",
        "()Z",
        "isCompanion",
        "isData",
        "isFinal",
        "isInner",
        "isOpen",
        "isSealed",
        "members",
        "Lkotlin/reflect/KCallable;",
        "getMembers",
        "nestedClasses",
        "getNestedClasses",
        "objectInstance",
        "getObjectInstance",
        "()Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;",
        "qualifiedName",
        "",
        "getQualifiedName",
        "()Ljava/lang/String;",
        "sealedSubclasses",
        "getSealedSubclasses",
        "simpleName",
        "getSimpleName",
        "supertypes",
        "Lkotlin/reflect/KType;",
        "getSupertypes",
        "typeParameters",
        "Lkotlin/reflect/KTypeParameter;",
        "getTypeParameters",
        "visibility",
        "Lkotlin/reflect/KVisibility;",
        "getVisibility",
        "()Lkotlin/reflect/KVisibility;",
        "equals",
        "other",
        "hashCode",
        "",
        "isInstance",
        "value",
        "v2-legacy-integration"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final annotations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/annotation/Annotation;",
            ">;"
        }
    .end annotation
.end field

.field private final constructors:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection<",
            "Lkotlin/reflect/KFunction<",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Launcher<",
            "Lcom/squareup/workflow/legacyintegration/LegacyState<",
            "TInputT;TRenderingT;>;TInputT;TOutputT;>;>;>;"
        }
    .end annotation
.end field

.field private final isAbstract:Z

.field private final isCompanion:Z

.field private final isData:Z

.field private final isFinal:Z

.field private final isInner:Z

.field private final isOpen:Z

.field private final isSealed:Z

.field private final members:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection<",
            "Lkotlin/reflect/KCallable<",
            "*>;>;"
        }
    .end annotation
.end field

.field private final nestedClasses:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection<",
            "Lkotlin/reflect/KClass<",
            "*>;>;"
        }
    .end annotation
.end field

.field private final objectInstance:Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/WorkflowPool$Launcher<",
            "Lcom/squareup/workflow/legacyintegration/LegacyState<",
            "TInputT;TRenderingT;>;TInputT;TOutputT;>;"
        }
    .end annotation
.end field

.field private final qualifiedName:Ljava/lang/String;

.field private final sealedSubclasses:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lkotlin/reflect/KClass<",
            "+",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Launcher<",
            "Lcom/squareup/workflow/legacyintegration/LegacyState<",
            "TInputT;TRenderingT;>;TInputT;TOutputT;>;>;>;"
        }
    .end annotation
.end field

.field private final simpleName:Ljava/lang/String;

.field private final supertypes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lkotlin/reflect/KType;",
            ">;"
        }
    .end annotation
.end field

.field private final typeParameters:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lkotlin/reflect/KTypeParameter;",
            ">;"
        }
    .end annotation
.end field

.field private final visibility:Lkotlin/reflect/KVisibility;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/workflow/legacyintegration/FakeKClass;->annotations:Ljava/util/List;

    .line 105
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    iput-object v0, p0, Lcom/squareup/workflow/legacyintegration/FakeKClass;->constructors:Ljava/util/Collection;

    .line 113
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    iput-object v0, p0, Lcom/squareup/workflow/legacyintegration/FakeKClass;->members:Ljava/util/Collection;

    .line 114
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    iput-object v0, p0, Lcom/squareup/workflow/legacyintegration/FakeKClass;->nestedClasses:Ljava/util/Collection;

    .line 118
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/workflow/legacyintegration/FakeKClass;->sealedSubclasses:Ljava/util/List;

    .line 120
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/workflow/legacyintegration/FakeKClass;->supertypes:Ljava/util/List;

    .line 121
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/workflow/legacyintegration/FakeKClass;->typeParameters:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public getAnnotations()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/annotation/Annotation;",
            ">;"
        }
    .end annotation

    .line 103
    iget-object v0, p0, Lcom/squareup/workflow/legacyintegration/FakeKClass;->annotations:Ljava/util/List;

    return-object v0
.end method

.method public getConstructors()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection<",
            "Lkotlin/reflect/KFunction<",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Launcher<",
            "Lcom/squareup/workflow/legacyintegration/LegacyState<",
            "TInputT;TRenderingT;>;TInputT;TOutputT;>;>;>;"
        }
    .end annotation

    .line 104
    iget-object v0, p0, Lcom/squareup/workflow/legacyintegration/FakeKClass;->constructors:Ljava/util/Collection;

    return-object v0
.end method

.method public getMembers()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection<",
            "Lkotlin/reflect/KCallable<",
            "*>;>;"
        }
    .end annotation

    .line 113
    iget-object v0, p0, Lcom/squareup/workflow/legacyintegration/FakeKClass;->members:Ljava/util/Collection;

    return-object v0
.end method

.method public getNestedClasses()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection<",
            "Lkotlin/reflect/KClass<",
            "*>;>;"
        }
    .end annotation

    .line 114
    iget-object v0, p0, Lcom/squareup/workflow/legacyintegration/FakeKClass;->nestedClasses:Ljava/util/Collection;

    return-object v0
.end method

.method public getObjectInstance()Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Launcher<",
            "Lcom/squareup/workflow/legacyintegration/LegacyState<",
            "TInputT;TRenderingT;>;TInputT;TOutputT;>;"
        }
    .end annotation

    .line 115
    iget-object v0, p0, Lcom/squareup/workflow/legacyintegration/FakeKClass;->objectInstance:Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;

    return-object v0
.end method

.method public bridge synthetic getObjectInstance()Ljava/lang/Object;
    .locals 1

    .line 100
    invoke-virtual {p0}, Lcom/squareup/workflow/legacyintegration/FakeKClass;->getObjectInstance()Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;

    move-result-object v0

    return-object v0
.end method

.method public getQualifiedName()Ljava/lang/String;
    .locals 1

    .line 116
    iget-object v0, p0, Lcom/squareup/workflow/legacyintegration/FakeKClass;->qualifiedName:Ljava/lang/String;

    return-object v0
.end method

.method public getSealedSubclasses()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lkotlin/reflect/KClass<",
            "+",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Launcher<",
            "Lcom/squareup/workflow/legacyintegration/LegacyState<",
            "TInputT;TRenderingT;>;TInputT;TOutputT;>;>;>;"
        }
    .end annotation

    .line 117
    iget-object v0, p0, Lcom/squareup/workflow/legacyintegration/FakeKClass;->sealedSubclasses:Ljava/util/List;

    return-object v0
.end method

.method public getSimpleName()Ljava/lang/String;
    .locals 1

    .line 119
    iget-object v0, p0, Lcom/squareup/workflow/legacyintegration/FakeKClass;->simpleName:Ljava/lang/String;

    return-object v0
.end method

.method public getSupertypes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lkotlin/reflect/KType;",
            ">;"
        }
    .end annotation

    .line 120
    iget-object v0, p0, Lcom/squareup/workflow/legacyintegration/FakeKClass;->supertypes:Ljava/util/List;

    return-object v0
.end method

.method public getTypeParameters()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lkotlin/reflect/KTypeParameter;",
            ">;"
        }
    .end annotation

    .line 121
    iget-object v0, p0, Lcom/squareup/workflow/legacyintegration/FakeKClass;->typeParameters:Ljava/util/List;

    return-object v0
.end method

.method public getVisibility()Lkotlin/reflect/KVisibility;
    .locals 1

    .line 122
    iget-object v0, p0, Lcom/squareup/workflow/legacyintegration/FakeKClass;->visibility:Lkotlin/reflect/KVisibility;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    const/4 v0, -0x1

    return v0
.end method

.method public isAbstract()Z
    .locals 1

    .line 106
    iget-boolean v0, p0, Lcom/squareup/workflow/legacyintegration/FakeKClass;->isAbstract:Z

    return v0
.end method

.method public isCompanion()Z
    .locals 1

    .line 107
    iget-boolean v0, p0, Lcom/squareup/workflow/legacyintegration/FakeKClass;->isCompanion:Z

    return v0
.end method

.method public isData()Z
    .locals 1

    .line 108
    iget-boolean v0, p0, Lcom/squareup/workflow/legacyintegration/FakeKClass;->isData:Z

    return v0
.end method

.method public isFinal()Z
    .locals 1

    .line 109
    iget-boolean v0, p0, Lcom/squareup/workflow/legacyintegration/FakeKClass;->isFinal:Z

    return v0
.end method

.method public isInner()Z
    .locals 1

    .line 110
    iget-boolean v0, p0, Lcom/squareup/workflow/legacyintegration/FakeKClass;->isInner:Z

    return v0
.end method

.method public isInstance(Ljava/lang/Object;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public isOpen()Z
    .locals 1

    .line 111
    iget-boolean v0, p0, Lcom/squareup/workflow/legacyintegration/FakeKClass;->isOpen:Z

    return v0
.end method

.method public isSealed()Z
    .locals 1

    .line 112
    iget-boolean v0, p0, Lcom/squareup/workflow/legacyintegration/FakeKClass;->isSealed:Z

    return v0
.end method
