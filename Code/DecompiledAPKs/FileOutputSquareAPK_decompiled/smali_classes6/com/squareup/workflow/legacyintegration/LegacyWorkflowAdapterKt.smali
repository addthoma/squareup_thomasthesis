.class public final Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapterKt;
.super Ljava/lang/Object;
.source "LegacyWorkflowAdapter.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001a\u00a4\u0001\u0010\u0000\u001a \u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u0002H\u00050\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u0006\"\u0008\u0008\u0001\u0010\u0003*\u00020\u0006\"\u0008\u0008\u0002\u0010\u0004*\u00020\u0006\"\u0008\u0008\u0003\u0010\u0005*\u00020\u0006*\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00040\u00072\u0018\u0010\u0008\u001a\u0014\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00050\t2\u0012\u0010\n\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u00020\u000c0\u000b2\u0012\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\u000c\u0012\u0004\u0012\u0002H\u00020\u000b\u00a8\u0006\u000e"
    }
    d2 = {
        "asV2Workflow",
        "Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter;",
        "StateT",
        "EventT",
        "OutputT",
        "RenderingT",
        "",
        "Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;",
        "renderer",
        "Lcom/squareup/workflow/legacy/Renderer;",
        "snapshotter",
        "Lkotlin/Function1;",
        "Lcom/squareup/workflow/Snapshot;",
        "restorer",
        "v2-legacy-integration"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final asV2Workflow(Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;Lcom/squareup/workflow/legacy/Renderer;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<StateT:",
            "Ljava/lang/Object;",
            "EventT:",
            "Ljava/lang/Object;",
            "OutputT:",
            "Ljava/lang/Object;",
            "RenderingT:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Launcher<",
            "TStateT;-TEventT;+TOutputT;>;",
            "Lcom/squareup/workflow/legacy/Renderer<",
            "TStateT;TEventT;TRenderingT;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-TStateT;",
            "Lcom/squareup/workflow/Snapshot;",
            ">;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/workflow/Snapshot;",
            "+TStateT;>;)",
            "Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter<",
            "TStateT;TStateT;TEventT;TOutputT;TRenderingT;>;"
        }
    .end annotation

    const-string v0, "$this$asV2Workflow"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "renderer"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "snapshotter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "restorer"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    new-instance v0, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapterKt$asV2Workflow$1;

    invoke-direct {v0, p0, p3, p1, p2}, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapterKt$asV2Workflow$1;-><init>(Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;Lkotlin/jvm/functions/Function1;Lcom/squareup/workflow/legacy/Renderer;Lkotlin/jvm/functions/Function1;)V

    check-cast v0, Lcom/squareup/workflow/legacyintegration/LegacyWorkflowAdapter;

    return-object v0
.end method
