.class final Lcom/squareup/workflow/ui/WorkflowRunnerViewModel$Factory$create$1;
.super Lkotlin/jvm/internal/Lambda;
.source "WorkflowRunnerViewModel.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/ui/WorkflowRunnerViewModel$Factory;->create(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Lkotlinx/coroutines/CoroutineScope;",
        "Lcom/squareup/workflow/WorkflowSession<",
        "+TOutputT;+",
        "Ljava/lang/Object;",
        ">;TT;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nWorkflowRunnerViewModel.kt\nKotlin\n*S Kotlin\n*F\n+ 1 WorkflowRunnerViewModel.kt\ncom/squareup/workflow/ui/WorkflowRunnerViewModel$Factory$create$1\n*L\n1#1,113:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u0002H\u0001\"\u0008\u0008\u0000\u0010\u0001*\u00020\u0002\"\u0004\u0008\u0001\u0010\u0003\"\u0008\u0008\u0002\u0010\u0004*\u00020\u0005\"\u0008\u0008\u0003\u0010\u0004*\u00020\u0005*\u00020\u00062\u0012\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u0002H\u0004\u0012\u0004\u0012\u00020\u00050\u0008H\n\u00a2\u0006\u0004\u0008\t\u0010\n"
    }
    d2 = {
        "<anonymous>",
        "T",
        "Landroidx/lifecycle/ViewModel;",
        "PropsT",
        "OutputT",
        "",
        "Lkotlinx/coroutines/CoroutineScope;",
        "session",
        "Lcom/squareup/workflow/WorkflowSession;",
        "invoke",
        "(Lkotlinx/coroutines/CoroutineScope;Lcom/squareup/workflow/WorkflowSession;)Landroidx/lifecycle/ViewModel;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $config:Lcom/squareup/workflow/ui/WorkflowRunner$Config;

.field final synthetic this$0:Lcom/squareup/workflow/ui/WorkflowRunnerViewModel$Factory;


# direct methods
.method constructor <init>(Lcom/squareup/workflow/ui/WorkflowRunnerViewModel$Factory;Lcom/squareup/workflow/ui/WorkflowRunner$Config;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/workflow/ui/WorkflowRunnerViewModel$Factory$create$1;->this$0:Lcom/squareup/workflow/ui/WorkflowRunnerViewModel$Factory;

    iput-object p2, p0, Lcom/squareup/workflow/ui/WorkflowRunnerViewModel$Factory$create$1;->$config:Lcom/squareup/workflow/ui/WorkflowRunner$Config;

    const/4 p1, 0x2

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lkotlinx/coroutines/CoroutineScope;Lcom/squareup/workflow/WorkflowSession;)Landroidx/lifecycle/ViewModel;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlinx/coroutines/CoroutineScope;",
            "Lcom/squareup/workflow/WorkflowSession<",
            "+TOutputT;+",
            "Ljava/lang/Object;",
            ">;)TT;"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "session"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    iget-object v0, p0, Lcom/squareup/workflow/ui/WorkflowRunnerViewModel$Factory$create$1;->$config:Lcom/squareup/workflow/ui/WorkflowRunner$Config;

    invoke-virtual {v0}, Lcom/squareup/workflow/ui/WorkflowRunner$Config;->getDiagnosticListener()Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/squareup/workflow/WorkflowSession;->setDiagnosticListener(Lcom/squareup/workflow/diagnostic/WorkflowDiagnosticListener;)V

    .line 60
    new-instance v0, Lcom/squareup/workflow/ui/WorkflowRunnerViewModel;

    invoke-direct {v0, p1, p2}, Lcom/squareup/workflow/ui/WorkflowRunnerViewModel;-><init>(Lkotlinx/coroutines/CoroutineScope;Lcom/squareup/workflow/WorkflowSession;)V

    .line 61
    iget-object p1, p0, Lcom/squareup/workflow/ui/WorkflowRunnerViewModel$Factory$create$1;->this$0:Lcom/squareup/workflow/ui/WorkflowRunnerViewModel$Factory;

    invoke-static {p1}, Lcom/squareup/workflow/ui/WorkflowRunnerViewModel$Factory;->access$getSavedStateRegistry$p(Lcom/squareup/workflow/ui/WorkflowRunnerViewModel$Factory;)Landroidx/savedstate/SavedStateRegistry;

    move-result-object p1

    invoke-static {}, Lcom/squareup/workflow/ui/WorkflowRunnerViewModel;->access$Companion()Lcom/squareup/workflow/ui/WorkflowRunnerViewModel$Companion;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/workflow/ui/WorkflowRunnerViewModel$Companion;->getBUNDLE_KEY()Ljava/lang/String;

    move-result-object p2

    move-object v1, v0

    check-cast v1, Landroidx/savedstate/SavedStateRegistry$SavedStateProvider;

    invoke-virtual {p1, p2, v1}, Landroidx/savedstate/SavedStateRegistry;->registerSavedStateProvider(Ljava/lang/String;Landroidx/savedstate/SavedStateRegistry$SavedStateProvider;)V

    .line 60
    check-cast v0, Landroidx/lifecycle/ViewModel;

    return-object v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 44
    check-cast p1, Lkotlinx/coroutines/CoroutineScope;

    check-cast p2, Lcom/squareup/workflow/WorkflowSession;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/workflow/ui/WorkflowRunnerViewModel$Factory$create$1;->invoke(Lkotlinx/coroutines/CoroutineScope;Lcom/squareup/workflow/WorkflowSession;)Landroidx/lifecycle/ViewModel;

    move-result-object p1

    return-object p1
.end method
