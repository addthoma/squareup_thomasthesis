.class public final Lcom/squareup/workflow/ui/WorkflowRunnerKt;
.super Ljava/lang/Object;
.source "WorkflowRunner.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nWorkflowRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 WorkflowRunner.kt\ncom/squareup/workflow/ui/WorkflowRunnerKt\n+ 2 LiveDataReactiveSteams.kt\nandroidx/lifecycle/LiveDataReactiveSteamsKt\n*L\n1#1,225:1\n37#2:226\n*E\n*S KotlinDebug\n*F\n+ 1 WorkflowRunner.kt\ncom/squareup/workflow/ui/WorkflowRunnerKt\n*L\n165#1:226\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0010\u0001\n\u0000\n\u0002\u0018\u0002\n\u0000\u001aV\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0004\u0008\u0000\u0010\u0003\"\u0008\u0008\u0001\u0010\u0002*\u00020\u0004*\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0018\u0010\u0008\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00020\n0\t2\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u00020\r0\u000c\u001a8\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\u0001\"\u0004\u0008\u0000\u0010\u0003*\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0018\u0010\u0008\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u00020\u000e0\n0\t\u001aV\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u0002H\u00020\u0001\"\u0004\u0008\u0000\u0010\u0003\"\u0008\u0008\u0001\u0010\u0002*\u00020\u0004*\u00020\u00052\u0006\u0010\u000f\u001a\u00020\u00102\u0018\u0010\u0008\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00020\n0\t2\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u0002H\u0002\u0012\u0004\u0012\u00020\r0\u000c\u001a8\u0010\u0000\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\u0001\"\u0004\u0008\u0000\u0010\u0003*\u00020\u00052\u0006\u0010\u000f\u001a\u00020\u00102\u0018\u0010\u0008\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u00020\u000e0\n0\t\u00a8\u0006\u0011"
    }
    d2 = {
        "setContentWorkflow",
        "Lcom/squareup/workflow/ui/WorkflowRunner;",
        "OutputT",
        "PropsT",
        "",
        "Landroidx/fragment/app/FragmentActivity;",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "configure",
        "Lkotlin/Function0;",
        "Lcom/squareup/workflow/ui/WorkflowRunner$Config;",
        "onResult",
        "Lkotlin/Function1;",
        "",
        "",
        "registry",
        "Lcom/squareup/workflow/ui/ViewRegistry;",
        "core-android_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final setContentWorkflow(Landroidx/fragment/app/FragmentActivity;Lcom/squareup/workflow/ui/ContainerHints;Lkotlin/jvm/functions/Function0;)Lcom/squareup/workflow/ui/WorkflowRunner;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<PropsT:",
            "Ljava/lang/Object;",
            ">(",
            "Landroidx/fragment/app/FragmentActivity;",
            "Lcom/squareup/workflow/ui/ContainerHints;",
            "Lkotlin/jvm/functions/Function0<",
            "Lcom/squareup/workflow/ui/WorkflowRunner$Config;",
            ">;)",
            "Lcom/squareup/workflow/ui/WorkflowRunner;"
        }
    .end annotation

    const-string v0, "$this$setContentWorkflow"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "configure"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 208
    sget-object v0, Lcom/squareup/workflow/ui/WorkflowRunnerKt$setContentWorkflow$2;->INSTANCE:Lcom/squareup/workflow/ui/WorkflowRunnerKt$setContentWorkflow$2;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p0, p1, p2, v0}, Lcom/squareup/workflow/ui/WorkflowRunnerKt;->setContentWorkflow(Landroidx/fragment/app/FragmentActivity;Lcom/squareup/workflow/ui/ContainerHints;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/ui/WorkflowRunner;

    move-result-object p0

    return-object p0
.end method

.method public static final setContentWorkflow(Landroidx/fragment/app/FragmentActivity;Lcom/squareup/workflow/ui/ContainerHints;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/ui/WorkflowRunner;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<PropsT:",
            "Ljava/lang/Object;",
            "OutputT:",
            "Ljava/lang/Object;",
            ">(",
            "Landroidx/fragment/app/FragmentActivity;",
            "Lcom/squareup/workflow/ui/ContainerHints;",
            "Lkotlin/jvm/functions/Function0<",
            "Lcom/squareup/workflow/ui/WorkflowRunner$Config<",
            "TPropsT;TOutputT;>;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-TOutputT;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/workflow/ui/WorkflowRunner<",
            "TOutputT;>;"
        }
    .end annotation

    const-string v0, "$this$setContentWorkflow"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "configure"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onResult"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 158
    sget-object v0, Lcom/squareup/workflow/ui/WorkflowRunner;->Companion:Lcom/squareup/workflow/ui/WorkflowRunner$Companion;

    invoke-virtual {v0, p0, p2}, Lcom/squareup/workflow/ui/WorkflowRunner$Companion;->startWorkflow(Landroidx/fragment/app/FragmentActivity;Lkotlin/jvm/functions/Function0;)Lcom/squareup/workflow/ui/WorkflowRunner;

    move-result-object p2

    .line 159
    new-instance v0, Lcom/squareup/workflow/ui/WorkflowLayout;

    move-object v1, p0

    check-cast v1, Landroid/content/Context;

    const/4 v2, 0x0

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3, v2}, Lcom/squareup/workflow/ui/WorkflowLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 160
    sget v1, Lcom/squareup/workflow/ui/R$id;->workflow_layout:I

    invoke-virtual {v0, v1}, Lcom/squareup/workflow/ui/WorkflowLayout;->setId(I)V

    .line 161
    invoke-interface {p2}, Lcom/squareup/workflow/ui/WorkflowRunner;->getRenderings()Lio/reactivex/Observable;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/squareup/workflow/ui/WorkflowLayout;->start(Lio/reactivex/Observable;Lcom/squareup/workflow/ui/ContainerHints;)V

    .line 164
    invoke-interface {p2}, Lcom/squareup/workflow/ui/WorkflowRunner;->getResult()Lio/reactivex/Maybe;

    move-result-object p1

    invoke-virtual {p1}, Lio/reactivex/Maybe;->toFlowable()Lio/reactivex/Flowable;

    move-result-object p1

    const-string v1, "runner.result.toFlowable()"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lorg/reactivestreams/Publisher;

    .line 226
    invoke-static {p1}, Landroidx/lifecycle/LiveDataReactiveStreams;->fromPublisher(Lorg/reactivestreams/Publisher;)Landroidx/lifecycle/LiveData;

    move-result-object p1

    const-string v1, "LiveDataReactiveStreams.fromPublisher(this)"

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 166
    move-object v1, p0

    check-cast v1, Landroidx/lifecycle/LifecycleOwner;

    new-instance v2, Lcom/squareup/workflow/ui/WorkflowRunnerKt$setContentWorkflow$1;

    invoke-direct {v2, p3}, Lcom/squareup/workflow/ui/WorkflowRunnerKt$setContentWorkflow$1;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v2, Landroidx/lifecycle/Observer;

    invoke-virtual {p1, v1, v2}, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 168
    check-cast v0, Landroid/view/View;

    invoke-virtual {p0, v0}, Landroidx/fragment/app/FragmentActivity;->setContentView(Landroid/view/View;)V

    return-object p2
.end method

.method public static final setContentWorkflow(Landroidx/fragment/app/FragmentActivity;Lcom/squareup/workflow/ui/ViewRegistry;Lkotlin/jvm/functions/Function0;)Lcom/squareup/workflow/ui/WorkflowRunner;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<PropsT:",
            "Ljava/lang/Object;",
            ">(",
            "Landroidx/fragment/app/FragmentActivity;",
            "Lcom/squareup/workflow/ui/ViewRegistry;",
            "Lkotlin/jvm/functions/Function0<",
            "Lcom/squareup/workflow/ui/WorkflowRunner$Config;",
            ">;)",
            "Lcom/squareup/workflow/ui/WorkflowRunner;"
        }
    .end annotation

    const-string v0, "$this$setContentWorkflow"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "registry"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "configure"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 224
    new-instance v0, Lcom/squareup/workflow/ui/ContainerHints;

    invoke-direct {v0, p1}, Lcom/squareup/workflow/ui/ContainerHints;-><init>(Lcom/squareup/workflow/ui/ViewRegistry;)V

    sget-object p1, Lcom/squareup/workflow/ui/WorkflowRunnerKt$setContentWorkflow$3;->INSTANCE:Lcom/squareup/workflow/ui/WorkflowRunnerKt$setContentWorkflow$3;

    check-cast p1, Lkotlin/jvm/functions/Function1;

    invoke-static {p0, v0, p2, p1}, Lcom/squareup/workflow/ui/WorkflowRunnerKt;->setContentWorkflow(Landroidx/fragment/app/FragmentActivity;Lcom/squareup/workflow/ui/ContainerHints;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/ui/WorkflowRunner;

    move-result-object p0

    return-object p0
.end method

.method public static final setContentWorkflow(Landroidx/fragment/app/FragmentActivity;Lcom/squareup/workflow/ui/ViewRegistry;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/ui/WorkflowRunner;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<PropsT:",
            "Ljava/lang/Object;",
            "OutputT:",
            "Ljava/lang/Object;",
            ">(",
            "Landroidx/fragment/app/FragmentActivity;",
            "Lcom/squareup/workflow/ui/ViewRegistry;",
            "Lkotlin/jvm/functions/Function0<",
            "Lcom/squareup/workflow/ui/WorkflowRunner$Config<",
            "TPropsT;TOutputT;>;>;",
            "Lkotlin/jvm/functions/Function1<",
            "-TOutputT;",
            "Lkotlin/Unit;",
            ">;)",
            "Lcom/squareup/workflow/ui/WorkflowRunner<",
            "TOutputT;>;"
        }
    .end annotation

    const-string v0, "$this$setContentWorkflow"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "registry"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "configure"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onResult"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 192
    new-instance v0, Lcom/squareup/workflow/ui/ContainerHints;

    invoke-direct {v0, p1}, Lcom/squareup/workflow/ui/ContainerHints;-><init>(Lcom/squareup/workflow/ui/ViewRegistry;)V

    invoke-static {p0, v0, p2, p3}, Lcom/squareup/workflow/ui/WorkflowRunnerKt;->setContentWorkflow(Landroidx/fragment/app/FragmentActivity;Lcom/squareup/workflow/ui/ContainerHints;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/ui/WorkflowRunner;

    move-result-object p0

    return-object p0
.end method
