.class public final Lcom/squareup/workflow/ui/WorkflowViewStub;
.super Landroid/view/View;
.source "WorkflowViewStub.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nWorkflowViewStub.kt\nKotlin\n*S Kotlin\n*F\n+ 1 WorkflowViewStub.kt\ncom/squareup/workflow/ui/WorkflowViewStub\n*L\n1#1,113:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0008\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B/\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\u0008\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007\u0012\u0008\u0008\u0002\u0010\u0008\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\tJ\u0018\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0001H\u0002J\u0016\u0010\u0014\u001a\u00020\u00012\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0018R\u001a\u0010\n\u001a\u00020\u0001X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000b\u0010\u000c\"\u0004\u0008\r\u0010\u000e\u00a8\u0006\u0019"
    }
    d2 = {
        "Lcom/squareup/workflow/ui/WorkflowViewStub;",
        "Landroid/view/View;",
        "context",
        "Landroid/content/Context;",
        "attributeSet",
        "Landroid/util/AttributeSet;",
        "defStyle",
        "",
        "defStyleRes",
        "(Landroid/content/Context;Landroid/util/AttributeSet;II)V",
        "actual",
        "getActual",
        "()Landroid/view/View;",
        "setActual",
        "(Landroid/view/View;)V",
        "buildNewViewAndReplaceOldView",
        "",
        "parent",
        "Landroid/view/ViewGroup;",
        "newView",
        "update",
        "rendering",
        "",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "core-android_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actual:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 7

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xe

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/squareup/workflow/ui/WorkflowViewStub;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0xc

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v6}, Lcom/squareup/workflow/ui/WorkflowViewStub;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 7

    const/4 v4, 0x0

    const/16 v5, 0x8

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v6}, Lcom/squareup/workflow/ui/WorkflowViewStub;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    const/4 p1, 0x1

    .line 63
    invoke-virtual {p0, p1}, Lcom/squareup/workflow/ui/WorkflowViewStub;->setWillNotDraw(Z)V

    .line 70
    move-object p1, p0

    check-cast p1, Landroid/view/View;

    iput-object p1, p0, Lcom/squareup/workflow/ui/WorkflowViewStub;->actual:Landroid/view/View;

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IIILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 1

    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_0

    const/4 p2, 0x0

    .line 58
    check-cast p2, Landroid/util/AttributeSet;

    :cond_0
    and-int/lit8 p6, p5, 0x4

    const/4 v0, 0x0

    if-eqz p6, :cond_1

    const/4 p3, 0x0

    :cond_1
    and-int/lit8 p5, p5, 0x8

    if-eqz p5, :cond_2

    const/4 p4, 0x0

    .line 60
    :cond_2
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/workflow/ui/WorkflowViewStub;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    return-void
.end method

.method private final buildNewViewAndReplaceOldView(Landroid/view/ViewGroup;Landroid/view/View;)V
    .locals 2

    .line 105
    iget-object v0, p0, Lcom/squareup/workflow/ui/WorkflowViewStub;->actual:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v0

    .line 106
    iget-object v1, p0, Lcom/squareup/workflow/ui/WorkflowViewStub;->actual:Landroid/view/View;

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 109
    iget-object v1, p0, Lcom/squareup/workflow/ui/WorkflowViewStub;->actual:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1, p2, v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 110
    :cond_0
    move-object v1, p0

    check-cast v1, Lcom/squareup/workflow/ui/WorkflowViewStub;

    invoke-virtual {p1, p2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    :goto_0
    return-void
.end method


# virtual methods
.method public final getActual()Landroid/view/View;
    .locals 1

    .line 70
    iget-object v0, p0, Lcom/squareup/workflow/ui/WorkflowViewStub;->actual:Landroid/view/View;

    return-object v0
.end method

.method public final setActual(Landroid/view/View;)V
    .locals 1

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    iput-object p1, p0, Lcom/squareup/workflow/ui/WorkflowViewStub;->actual:Landroid/view/View;

    return-void
.end method

.method public final update(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)Landroid/view/View;
    .locals 8

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 88
    iget-object v0, p0, Lcom/squareup/workflow/ui/WorkflowViewStub;->actual:Landroid/view/View;

    invoke-static {v0, p1}, Lcom/squareup/workflow/ui/ViewShowRenderingKt;->canShowRendering(Landroid/view/View;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 90
    invoke-static {v0, p1, p2}, Lcom/squareup/workflow/ui/ViewShowRenderingKt;->showRendering(Landroid/view/View;Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-object v0

    .line 94
    :cond_1
    iget-object v0, p0, Lcom/squareup/workflow/ui/WorkflowViewStub;->actual:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 95
    instance-of v1, v0, Landroid/view/ViewGroup;

    if-eqz v1, :cond_2

    sget-object v1, Lcom/squareup/workflow/ui/ViewRegistry;->Companion:Lcom/squareup/workflow/ui/ViewRegistry$Companion;

    check-cast v1, Lcom/squareup/workflow/ui/ContainerHintKey;

    invoke-virtual {p2, v1}, Lcom/squareup/workflow/ui/ContainerHints;->get(Lcom/squareup/workflow/ui/ContainerHintKey;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/workflow/ui/ViewRegistry;

    check-cast v0, Landroid/view/ViewGroup;

    invoke-static {v1, p1, p2, v0}, Lcom/squareup/workflow/ui/ViewRegistryKt;->buildView(Lcom/squareup/workflow/ui/ViewRegistry;Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    .line 96
    invoke-direct {p0, v0, p1}, Lcom/squareup/workflow/ui/WorkflowViewStub;->buildNewViewAndReplaceOldView(Landroid/view/ViewGroup;Landroid/view/View;)V

    goto :goto_1

    .line 97
    :cond_2
    sget-object v0, Lcom/squareup/workflow/ui/ViewRegistry;->Companion:Lcom/squareup/workflow/ui/ViewRegistry$Companion;

    check-cast v0, Lcom/squareup/workflow/ui/ContainerHintKey;

    invoke-virtual {p2, v0}, Lcom/squareup/workflow/ui/ContainerHints;->get(Lcom/squareup/workflow/ui/ContainerHintKey;)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/workflow/ui/ViewRegistry;

    iget-object v0, p0, Lcom/squareup/workflow/ui/WorkflowViewStub;->actual:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string v0, "actual.context"

    invoke-static {v4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v5, 0x0

    const/16 v6, 0x8

    const/4 v7, 0x0

    move-object v2, p1

    move-object v3, p2

    invoke-static/range {v1 .. v7}, Lcom/squareup/workflow/ui/ViewRegistry$DefaultImpls;->buildView$default(Lcom/squareup/workflow/ui/ViewRegistry;Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;Landroid/content/Context;Landroid/view/ViewGroup;ILjava/lang/Object;)Landroid/view/View;

    move-result-object p1

    .line 98
    :goto_1
    iput-object p1, p0, Lcom/squareup/workflow/ui/WorkflowViewStub;->actual:Landroid/view/View;

    return-object p1
.end method
