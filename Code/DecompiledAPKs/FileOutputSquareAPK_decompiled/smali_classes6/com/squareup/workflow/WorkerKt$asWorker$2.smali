.class public final Lcom/squareup/workflow/WorkerKt$asWorker$2;
.super Lkotlin/coroutines/jvm/internal/SuspendLambda;
.source "Worker.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function2;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/workflow/WorkerKt;->asWorker(Lkotlinx/coroutines/channels/ReceiveChannel;Z)Lcom/squareup/workflow/Worker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/coroutines/jvm/internal/SuspendLambda;",
        "Lkotlin/jvm/functions/Function2<",
        "Lkotlinx/coroutines/flow/FlowCollector<",
        "-TOutputT;>;",
        "Lkotlin/coroutines/Continuation<",
        "-",
        "Lkotlin/Unit;",
        ">;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nWorker.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Worker.kt\ncom/squareup/workflow/WorkerKt$asWorker$2\n+ 2 Channels.common.kt\nkotlinx/coroutines/channels/ChannelsKt__Channels_commonKt\n*L\n1#1,422:1\n179#2:423\n158#2,3:424\n180#2,2:427\n165#2:429\n161#2,3:430\n*E\n*S KotlinDebug\n*F\n+ 1 Worker.kt\ncom/squareup/workflow/WorkerKt$asWorker$2\n*L\n322#1:423\n322#1,3:424\n322#1,2:427\n322#1:429\n322#1,3:430\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u0001\"\u0006\u0008\u0000\u0010\u0002\u0018\u0001*\u0008\u0012\u0004\u0012\u0002H\u00020\u0003H\u008a@\u00a2\u0006\u0004\u0008\u0004\u0010\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "OutputT",
        "Lkotlinx/coroutines/flow/FlowCollector;",
        "invoke",
        "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation

.annotation runtime Lkotlin/coroutines/jvm/internal/DebugMetadata;
    c = "com.squareup.workflow.WorkerKt$asWorker$2"
    f = "Worker.kt"
    i = {
        0x0,
        0x0,
        0x0,
        0x0,
        0x0,
        0x1,
        0x1,
        0x1,
        0x1,
        0x1,
        0x1,
        0x1,
        0x2,
        0x3,
        0x3
    }
    l = {
        0x1ab,
        0x142,
        0x144,
        0x145
    }
    m = "invokeSuspend"
    n = {
        "$this$create",
        "$this$consumeEach$iv",
        "$this$consume$iv$iv",
        "cause$iv$iv",
        "$this$consume$iv",
        "$this$create",
        "$this$consumeEach$iv",
        "$this$consume$iv$iv",
        "cause$iv$iv",
        "$this$consume$iv",
        "e$iv",
        "it",
        "$this$create",
        "$this$create",
        "value"
    }
    s = {
        "L$0",
        "L$1",
        "L$3",
        "L$4",
        "L$5",
        "L$0",
        "L$1",
        "L$3",
        "L$4",
        "L$5",
        "L$7",
        "L$8",
        "L$0",
        "L$0",
        "L$1"
    }
.end annotation


# instance fields
.field final synthetic $closeOnCancel:Z

.field final synthetic $this_asWorker:Lkotlinx/coroutines/channels/ReceiveChannel;

.field L$0:Ljava/lang/Object;

.field L$1:Ljava/lang/Object;

.field L$2:Ljava/lang/Object;

.field L$3:Ljava/lang/Object;

.field L$4:Ljava/lang/Object;

.field L$5:Ljava/lang/Object;

.field L$6:Ljava/lang/Object;

.field L$7:Ljava/lang/Object;

.field L$8:Ljava/lang/Object;

.field label:I

.field private p$:Lkotlinx/coroutines/flow/FlowCollector;


# direct methods
.method public constructor <init>(Lkotlinx/coroutines/channels/ReceiveChannel;ZLkotlin/coroutines/Continuation;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/workflow/WorkerKt$asWorker$2;->$this_asWorker:Lkotlinx/coroutines/channels/ReceiveChannel;

    iput-boolean p2, p0, Lcom/squareup/workflow/WorkerKt$asWorker$2;->$closeOnCancel:Z

    const/4 p1, 0x2

    invoke-direct {p0, p1, p3}, Lkotlin/coroutines/jvm/internal/SuspendLambda;-><init>(ILkotlin/coroutines/Continuation;)V

    return-void
.end method


# virtual methods
.method public final create(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Lkotlin/coroutines/Continuation<",
            "*>;)",
            "Lkotlin/coroutines/Continuation<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    const-string v0, "completion"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/workflow/WorkerKt$asWorker$2;

    iget-object v1, p0, Lcom/squareup/workflow/WorkerKt$asWorker$2;->$this_asWorker:Lkotlinx/coroutines/channels/ReceiveChannel;

    iget-boolean v2, p0, Lcom/squareup/workflow/WorkerKt$asWorker$2;->$closeOnCancel:Z

    invoke-direct {v0, v1, v2, p2}, Lcom/squareup/workflow/WorkerKt$asWorker$2;-><init>(Lkotlinx/coroutines/channels/ReceiveChannel;ZLkotlin/coroutines/Continuation;)V

    check-cast p1, Lkotlinx/coroutines/flow/FlowCollector;

    iput-object p1, v0, Lcom/squareup/workflow/WorkerKt$asWorker$2;->p$:Lkotlinx/coroutines/flow/FlowCollector;

    return-object v0
.end method

.method public final invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p2, Lkotlin/coroutines/Continuation;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/workflow/WorkerKt$asWorker$2;->create(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Lkotlin/coroutines/Continuation;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/WorkerKt$asWorker$2;

    sget-object p2, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {p1, p2}, Lcom/squareup/workflow/WorkerKt$asWorker$2;->invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final invokeSuspend(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 13

    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->getCOROUTINE_SUSPENDED()Ljava/lang/Object;

    move-result-object v0

    .line 319
    iget v1, p0, Lcom/squareup/workflow/WorkerKt$asWorker$2;->label:I

    const/4 v2, 0x4

    const/4 v3, 0x3

    const/4 v4, 0x2

    const/4 v5, 0x1

    if-eqz v1, :cond_4

    if-eq v1, v5, :cond_3

    if-eq v1, v4, :cond_2

    if-eq v1, v3, :cond_1

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/squareup/workflow/WorkerKt$asWorker$2;->L$2:Ljava/lang/Object;

    check-cast v1, Lkotlinx/coroutines/channels/ChannelIterator;

    iget-object v4, p0, Lcom/squareup/workflow/WorkerKt$asWorker$2;->L$1:Ljava/lang/Object;

    iget-object v4, p0, Lcom/squareup/workflow/WorkerKt$asWorker$2;->L$0:Ljava/lang/Object;

    check-cast v4, Lkotlinx/coroutines/flow/FlowCollector;

    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    move-object p1, p0

    move-object v11, v4

    move-object v4, v0

    move-object v0, v11

    goto/16 :goto_3

    .line 328
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "call to \'resume\' before \'invoke\' with coroutine"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 319
    :cond_1
    iget-object v1, p0, Lcom/squareup/workflow/WorkerKt$asWorker$2;->L$1:Ljava/lang/Object;

    check-cast v1, Lkotlinx/coroutines/channels/ChannelIterator;

    iget-object v4, p0, Lcom/squareup/workflow/WorkerKt$asWorker$2;->L$0:Ljava/lang/Object;

    check-cast v4, Lkotlinx/coroutines/flow/FlowCollector;

    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    move-object v5, v0

    move-object v0, p0

    goto/16 :goto_4

    :cond_2
    iget-object v1, p0, Lcom/squareup/workflow/WorkerKt$asWorker$2;->L$8:Ljava/lang/Object;

    iget-object v1, p0, Lcom/squareup/workflow/WorkerKt$asWorker$2;->L$7:Ljava/lang/Object;

    iget-object v1, p0, Lcom/squareup/workflow/WorkerKt$asWorker$2;->L$6:Ljava/lang/Object;

    check-cast v1, Lkotlinx/coroutines/channels/ChannelIterator;

    iget-object v2, p0, Lcom/squareup/workflow/WorkerKt$asWorker$2;->L$5:Ljava/lang/Object;

    check-cast v2, Lkotlinx/coroutines/channels/ReceiveChannel;

    iget-object v3, p0, Lcom/squareup/workflow/WorkerKt$asWorker$2;->L$4:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Throwable;

    iget-object v6, p0, Lcom/squareup/workflow/WorkerKt$asWorker$2;->L$3:Ljava/lang/Object;

    check-cast v6, Lkotlinx/coroutines/channels/ReceiveChannel;

    iget-object v7, p0, Lcom/squareup/workflow/WorkerKt$asWorker$2;->L$2:Ljava/lang/Object;

    check-cast v7, Lcom/squareup/workflow/WorkerKt$asWorker$2;

    iget-object v8, p0, Lcom/squareup/workflow/WorkerKt$asWorker$2;->L$1:Ljava/lang/Object;

    check-cast v8, Lkotlinx/coroutines/channels/ReceiveChannel;

    iget-object v9, p0, Lcom/squareup/workflow/WorkerKt$asWorker$2;->L$0:Ljava/lang/Object;

    check-cast v9, Lkotlinx/coroutines/flow/FlowCollector;

    :try_start_0
    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object p1, p0

    move-object v11, v9

    move-object v9, v0

    move-object v0, v11

    move-object v12, v2

    move-object v2, v1

    move-object v1, v6

    move-object v6, v8

    move-object v8, v12

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/squareup/workflow/WorkerKt$asWorker$2;->L$6:Ljava/lang/Object;

    check-cast v1, Lkotlinx/coroutines/channels/ChannelIterator;

    iget-object v2, p0, Lcom/squareup/workflow/WorkerKt$asWorker$2;->L$5:Ljava/lang/Object;

    check-cast v2, Lkotlinx/coroutines/channels/ReceiveChannel;

    iget-object v3, p0, Lcom/squareup/workflow/WorkerKt$asWorker$2;->L$4:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Throwable;

    iget-object v6, p0, Lcom/squareup/workflow/WorkerKt$asWorker$2;->L$3:Ljava/lang/Object;

    check-cast v6, Lkotlinx/coroutines/channels/ReceiveChannel;

    iget-object v7, p0, Lcom/squareup/workflow/WorkerKt$asWorker$2;->L$2:Ljava/lang/Object;

    check-cast v7, Lcom/squareup/workflow/WorkerKt$asWorker$2;

    iget-object v8, p0, Lcom/squareup/workflow/WorkerKt$asWorker$2;->L$1:Ljava/lang/Object;

    check-cast v8, Lkotlinx/coroutines/channels/ReceiveChannel;

    iget-object v9, p0, Lcom/squareup/workflow/WorkerKt$asWorker$2;->L$0:Ljava/lang/Object;

    check-cast v9, Lkotlinx/coroutines/flow/FlowCollector;

    :try_start_1
    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v10, v0

    move-object v0, p0

    goto :goto_1

    :catchall_0
    move-exception p1

    goto/16 :goto_2

    :cond_4
    invoke-static {p1}, Lkotlin/ResultKt;->throwOnFailure(Ljava/lang/Object;)V

    iget-object p1, p0, Lcom/squareup/workflow/WorkerKt$asWorker$2;->p$:Lkotlinx/coroutines/flow/FlowCollector;

    .line 320
    iget-boolean v1, p0, Lcom/squareup/workflow/WorkerKt$asWorker$2;->$closeOnCancel:Z

    if-eqz v1, :cond_8

    .line 322
    iget-object v6, p0, Lcom/squareup/workflow/WorkerKt$asWorker$2;->$this_asWorker:Lkotlinx/coroutines/channels/ReceiveChannel;

    const/4 v1, 0x0

    .line 424
    check-cast v1, Ljava/lang/Throwable;

    .line 427
    :try_start_2
    invoke-interface {v6}, Lkotlinx/coroutines/channels/ReceiveChannel;->iterator()Lkotlinx/coroutines/channels/ChannelIterator;

    move-result-object v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object v7, p0

    move-object v9, v0

    move-object v3, v1

    move-object v1, v6

    move-object v8, v1

    move-object v0, p1

    move-object p1, v7

    :goto_0
    :try_start_3
    iput-object v0, p1, Lcom/squareup/workflow/WorkerKt$asWorker$2;->L$0:Ljava/lang/Object;

    iput-object v6, p1, Lcom/squareup/workflow/WorkerKt$asWorker$2;->L$1:Ljava/lang/Object;

    iput-object v7, p1, Lcom/squareup/workflow/WorkerKt$asWorker$2;->L$2:Ljava/lang/Object;

    iput-object v1, p1, Lcom/squareup/workflow/WorkerKt$asWorker$2;->L$3:Ljava/lang/Object;

    iput-object v3, p1, Lcom/squareup/workflow/WorkerKt$asWorker$2;->L$4:Ljava/lang/Object;

    iput-object v8, p1, Lcom/squareup/workflow/WorkerKt$asWorker$2;->L$5:Ljava/lang/Object;

    iput-object v2, p1, Lcom/squareup/workflow/WorkerKt$asWorker$2;->L$6:Ljava/lang/Object;

    iput v5, p1, Lcom/squareup/workflow/WorkerKt$asWorker$2;->label:I

    invoke-interface {v2, v7}, Lkotlinx/coroutines/channels/ChannelIterator;->hasNext(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object v10
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-ne v10, v9, :cond_5

    return-object v9

    :cond_5
    move-object v11, v0

    move-object v0, p1

    move-object p1, v10

    move-object v10, v9

    move-object v9, v11

    move-object v12, v6

    move-object v6, v1

    move-object v1, v2

    move-object v2, v8

    move-object v8, v12

    .line 319
    :goto_1
    :try_start_4
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_7

    invoke-interface {v1}, Lkotlinx/coroutines/channels/ChannelIterator;->next()Ljava/lang/Object;

    move-result-object p1

    .line 322
    iput-object v9, v0, Lcom/squareup/workflow/WorkerKt$asWorker$2;->L$0:Ljava/lang/Object;

    iput-object v8, v0, Lcom/squareup/workflow/WorkerKt$asWorker$2;->L$1:Ljava/lang/Object;

    iput-object v7, v0, Lcom/squareup/workflow/WorkerKt$asWorker$2;->L$2:Ljava/lang/Object;

    iput-object v6, v0, Lcom/squareup/workflow/WorkerKt$asWorker$2;->L$3:Ljava/lang/Object;

    iput-object v3, v0, Lcom/squareup/workflow/WorkerKt$asWorker$2;->L$4:Ljava/lang/Object;

    iput-object v2, v0, Lcom/squareup/workflow/WorkerKt$asWorker$2;->L$5:Ljava/lang/Object;

    iput-object v1, v0, Lcom/squareup/workflow/WorkerKt$asWorker$2;->L$6:Ljava/lang/Object;

    iput-object p1, v0, Lcom/squareup/workflow/WorkerKt$asWorker$2;->L$7:Ljava/lang/Object;

    iput-object p1, v0, Lcom/squareup/workflow/WorkerKt$asWorker$2;->L$8:Ljava/lang/Object;

    iput v4, v0, Lcom/squareup/workflow/WorkerKt$asWorker$2;->label:I

    invoke-interface {v9, p1, v0}, Lkotlinx/coroutines/flow/FlowCollector;->emit(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object p1

    if-ne p1, v10, :cond_6

    return-object v10

    :cond_6
    move-object p1, v0

    move-object v0, v9

    move-object v9, v10

    move-object v11, v2

    move-object v2, v1

    move-object v1, v6

    move-object v6, v8

    move-object v8, v11

    goto :goto_0

    .line 428
    :cond_7
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 429
    invoke-static {v6, v3}, Lkotlinx/coroutines/channels/ChannelsKt;->cancelConsumed(Lkotlinx/coroutines/channels/ReceiveChannel;Ljava/lang/Throwable;)V

    goto :goto_5

    :catchall_1
    move-exception p1

    move-object v6, v1

    .line 432
    :goto_2
    :try_start_5
    throw p1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :catchall_2
    move-exception v0

    .line 429
    invoke-static {v6, p1}, Lkotlinx/coroutines/channels/ChannelsKt;->cancelConsumed(Lkotlinx/coroutines/channels/ReceiveChannel;Ljava/lang/Throwable;)V

    throw v0

    .line 324
    :cond_8
    iget-object v1, p0, Lcom/squareup/workflow/WorkerKt$asWorker$2;->$this_asWorker:Lkotlinx/coroutines/channels/ReceiveChannel;

    invoke-interface {v1}, Lkotlinx/coroutines/channels/ReceiveChannel;->iterator()Lkotlinx/coroutines/channels/ChannelIterator;

    move-result-object v1

    move-object v4, v0

    move-object v0, p1

    move-object p1, p0

    :goto_3
    iput-object v0, p1, Lcom/squareup/workflow/WorkerKt$asWorker$2;->L$0:Ljava/lang/Object;

    iput-object v1, p1, Lcom/squareup/workflow/WorkerKt$asWorker$2;->L$1:Ljava/lang/Object;

    iput v3, p1, Lcom/squareup/workflow/WorkerKt$asWorker$2;->label:I

    invoke-interface {v1, p1}, Lkotlinx/coroutines/channels/ChannelIterator;->hasNext(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object v5

    if-ne v5, v4, :cond_9

    return-object v4

    :cond_9
    move-object v11, v0

    move-object v0, p1

    move-object p1, v5

    move-object v5, v4

    move-object v4, v11

    .line 319
    :goto_4
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_b

    invoke-interface {v1}, Lkotlinx/coroutines/channels/ChannelIterator;->next()Ljava/lang/Object;

    move-result-object p1

    .line 325
    iput-object v4, v0, Lcom/squareup/workflow/WorkerKt$asWorker$2;->L$0:Ljava/lang/Object;

    iput-object p1, v0, Lcom/squareup/workflow/WorkerKt$asWorker$2;->L$1:Ljava/lang/Object;

    iput-object v1, v0, Lcom/squareup/workflow/WorkerKt$asWorker$2;->L$2:Ljava/lang/Object;

    iput v2, v0, Lcom/squareup/workflow/WorkerKt$asWorker$2;->label:I

    invoke-interface {v4, p1, v0}, Lkotlinx/coroutines/flow/FlowCollector;->emit(Ljava/lang/Object;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    move-result-object p1

    if-ne p1, v5, :cond_a

    return-object v5

    :cond_a
    move-object p1, v0

    move-object v0, v4

    move-object v4, v5

    goto :goto_3

    .line 328
    :cond_b
    :goto_5
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method
