.class public Lcom/squareup/ui/main/r12education/R12EducationPanelVideoView;
.super Landroid/widget/LinearLayout;
.source "R12EducationPanelVideoView.java"


# instance fields
.field presenter:Lcom/squareup/ui/main/r12education/R12EducationScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 19
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    const-class p2, Lcom/squareup/ui/main/r12education/R12EducationScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/main/r12education/R12EducationScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/main/r12education/R12EducationScreen$Component;->inject(Lcom/squareup/ui/main/r12education/R12EducationPanelVideoView;)V

    return-void
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 2

    .line 24
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 25
    sget v0, Lcom/squareup/readertutorial/R$id;->r12_education_video_play:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/main/r12education/R12EducationPanelVideoView$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/main/r12education/R12EducationPanelVideoView$1;-><init>(Lcom/squareup/ui/main/r12education/R12EducationPanelVideoView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
