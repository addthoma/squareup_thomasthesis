.class synthetic Lcom/squareup/ui/main/errors/PaymentTakingWarningWorkflow$2;
.super Ljava/lang/Object;
.source "PaymentTakingWarningWorkflow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/main/errors/PaymentTakingWarningWorkflow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$squareup$ui$main$errors$WarningScreenButtonConfig$ButtonBehaviorType:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 71
    invoke-static {}, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;->values()[Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/ui/main/errors/PaymentTakingWarningWorkflow$2;->$SwitchMap$com$squareup$ui$main$errors$WarningScreenButtonConfig$ButtonBehaviorType:[I

    :try_start_0
    sget-object v0, Lcom/squareup/ui/main/errors/PaymentTakingWarningWorkflow$2;->$SwitchMap$com$squareup$ui$main$errors$WarningScreenButtonConfig$ButtonBehaviorType:[I

    sget-object v1, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;->DO_NOTHING:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

    invoke-virtual {v1}, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :try_start_1
    sget-object v0, Lcom/squareup/ui/main/errors/PaymentTakingWarningWorkflow$2;->$SwitchMap$com$squareup$ui$main$errors$WarningScreenButtonConfig$ButtonBehaviorType:[I

    sget-object v1, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;->KILL_TENDER_INITIATED_BY_READER_AND_TURN_OFF_NFC_FIELD:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

    invoke-virtual {v1}, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    return-void
.end method
