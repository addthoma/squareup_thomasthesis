.class public interface abstract Lcom/squareup/ui/main/errors/RootReaderWarningScreen$Component;
.super Ljava/lang/Object;
.source "RootReaderWarningScreen.java"

# interfaces
.implements Lcom/squareup/ui/main/errors/ReaderWarningView$Component;


# annotations
.annotation runtime Ldagger/Subcomponent;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/main/errors/RootReaderWarningScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# virtual methods
.method public abstract deviceUnsupported()Lcom/squareup/ui/main/errors/RootScreenHandler$DeviceUnsupportedScreenHandler;
.end method

.method public abstract dipRequired()Lcom/squareup/ui/main/errors/RootScreenHandler$DipRequiredRootScreenHandler;
.end method

.method public abstract disableNfcWarning()Lcom/squareup/ui/main/errors/RootScreenHandler$DisableNfcWarningScreenHandler;
.end method

.method public abstract firmwareUpdateError()Lcom/squareup/ui/main/errors/RootScreenHandler$FirmwareUpdateErrorScreenHandler;
.end method

.method public abstract genericFailed()Lcom/squareup/ui/main/errors/RootScreenHandler$GenericFailedScreenHandler;
.end method

.method public abstract genericReaderWarning()Lcom/squareup/ui/main/errors/RootScreenHandler$GenericReaderWarningScreenHandler;
.end method

.method public abstract libraryLoadingError()Lcom/squareup/ui/main/errors/RootScreenHandler$LibraryLoadingErrorHandler;
.end method

.method public abstract paymentDeclined()Lcom/squareup/ui/main/errors/RootScreenHandler$PaymentDeclinedScreenHandler;
.end method

.method public abstract postFwupDisconnect()Lcom/squareup/ui/main/errors/RootScreenHandler$PostFwupDisconnectScreenHandler;
.end method

.method public abstract r12BlockingUpdate()Lcom/squareup/ui/main/errors/RootScreenHandler$R12BlockingUpdateScreenHandler;
.end method

.method public abstract r12LowBattery()Lcom/squareup/ui/main/errors/RootScreenHandler$R12LowBatteryScreenHandler;
.end method

.method public abstract r6LowBattery()Lcom/squareup/ui/main/errors/RootScreenHandler$R6LowBatteryScreenHandler;
.end method

.method public abstract secureSessionFailed()Lcom/squareup/ui/main/errors/RootScreenHandler$SecureSessionFailedHandler;
.end method

.method public abstract talkbackEnabled()Lcom/squareup/ui/main/errors/RootScreenHandler$TalkBackEnabledScreenHandler;
.end method

.method public abstract tamperError()Lcom/squareup/ui/main/errors/RootScreenHandler$TamperErrorScreenHandler;
.end method

.method public abstract updateRegister()Lcom/squareup/ui/main/errors/RootScreenHandler$UpdateRegisterScreenHandler;
.end method
