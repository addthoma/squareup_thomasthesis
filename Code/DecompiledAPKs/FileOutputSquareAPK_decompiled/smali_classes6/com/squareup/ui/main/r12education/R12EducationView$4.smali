.class Lcom/squareup/ui/main/r12education/R12EducationView$4;
.super Ljava/lang/Object;
.source "R12EducationView.java"

# interfaces
.implements Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/main/r12education/R12EducationView;->onAttachedToWindow()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/main/r12education/R12EducationView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/main/r12education/R12EducationView;)V
    .locals 0

    .line 499
    iput-object p1, p0, Lcom/squareup/ui/main/r12education/R12EducationView$4;->this$0:Lcom/squareup/ui/main/r12education/R12EducationView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public synthetic lambda$onPageScrollStateChanged$0$R12EducationView$4(Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;Landroid/animation/ValueAnimator;)V
    .locals 5

    .line 544
    invoke-virtual {p2}, Landroid/animation/ValueAnimator;->getAnimatedFraction()F

    move-result p2

    .line 545
    invoke-static {}, Lcom/squareup/ui/main/r12education/R12EducationView$Element;->values()[Lcom/squareup/ui/main/r12education/R12EducationView$Element;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    .line 546
    iget-object v4, p0, Lcom/squareup/ui/main/r12education/R12EducationView$4;->this$0:Lcom/squareup/ui/main/r12education/R12EducationView;

    invoke-static {v4}, Lcom/squareup/ui/main/r12education/R12EducationView;->access$600(Lcom/squareup/ui/main/r12education/R12EducationView;)Ljava/util/LinkedHashMap;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    instance-of v4, v4, Lcom/squareup/ui/main/r12education/TransitionView;

    if-nez v4, :cond_0

    goto :goto_1

    .line 550
    :cond_0
    iget-object v4, p0, Lcom/squareup/ui/main/r12education/R12EducationView$4;->this$0:Lcom/squareup/ui/main/r12education/R12EducationView;

    invoke-static {v4}, Lcom/squareup/ui/main/r12education/R12EducationView;->access$600(Lcom/squareup/ui/main/r12education/R12EducationView;)Ljava/util/LinkedHashMap;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/ui/main/r12education/TransitionView;

    invoke-virtual {p1, v3, v4, p2}, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->overTweenElement(Lcom/squareup/ui/main/r12education/R12EducationView$Element;Lcom/squareup/ui/main/r12education/TransitionView;F)V

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public onPageScrollStateChanged(I)V
    .locals 3

    if-nez p1, :cond_1

    .line 536
    iget-object p1, p0, Lcom/squareup/ui/main/r12education/R12EducationView$4;->this$0:Lcom/squareup/ui/main/r12education/R12EducationView;

    invoke-static {p1}, Lcom/squareup/ui/main/r12education/R12EducationView;->access$200(Lcom/squareup/ui/main/r12education/R12EducationView;)Lcom/squareup/ui/main/r12education/R12EducationView$PanelList;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationView$4;->this$0:Lcom/squareup/ui/main/r12education/R12EducationView;

    invoke-static {v0}, Lcom/squareup/ui/main/r12education/R12EducationView;->access$300(Lcom/squareup/ui/main/r12education/R12EducationView;)Landroidx/viewpager/widget/ViewPager;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/viewpager/widget/ViewPager;->getCurrentItem()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList;->panelAt(I)Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    move-result-object p1

    .line 537
    invoke-virtual {p1}, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->shouldOverTween()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 541
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationView$4;->this$0:Lcom/squareup/ui/main/r12education/R12EducationView;

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/ui/main/r12education/R12EducationView;->access$802(Lcom/squareup/ui/main/r12education/R12EducationView;Landroid/animation/ValueAnimator;)Landroid/animation/ValueAnimator;

    .line 542
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationView$4;->this$0:Lcom/squareup/ui/main/r12education/R12EducationView;

    invoke-static {v0}, Lcom/squareup/ui/main/r12education/R12EducationView;->access$800(Lcom/squareup/ui/main/r12education/R12EducationView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    const-wide/16 v1, 0x2ee

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 543
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationView$4;->this$0:Lcom/squareup/ui/main/r12education/R12EducationView;

    invoke-static {v0}, Lcom/squareup/ui/main/r12education/R12EducationView;->access$800(Lcom/squareup/ui/main/r12education/R12EducationView;)Landroid/animation/ValueAnimator;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/main/r12education/-$$Lambda$R12EducationView$4$B8NRFlj0nqmabqleEsu8YkZrlnc;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/main/r12education/-$$Lambda$R12EducationView$4$B8NRFlj0nqmabqleEsu8YkZrlnc;-><init>(Lcom/squareup/ui/main/r12education/R12EducationView$4;Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 554
    iget-object p1, p0, Lcom/squareup/ui/main/r12education/R12EducationView$4;->this$0:Lcom/squareup/ui/main/r12education/R12EducationView;

    invoke-static {p1}, Lcom/squareup/ui/main/r12education/R12EducationView;->access$800(Lcom/squareup/ui/main/r12education/R12EducationView;)Landroid/animation/ValueAnimator;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/main/r12education/R12EducationView$4$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/main/r12education/R12EducationView$4$1;-><init>(Lcom/squareup/ui/main/r12education/R12EducationView$4;)V

    invoke-virtual {p1, v0}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 571
    iget-object p1, p0, Lcom/squareup/ui/main/r12education/R12EducationView$4;->this$0:Lcom/squareup/ui/main/r12education/R12EducationView;

    invoke-static {p1}, Lcom/squareup/ui/main/r12education/R12EducationView;->access$800(Lcom/squareup/ui/main/r12education/R12EducationView;)Landroid/animation/ValueAnimator;

    move-result-object p1

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_0

    .line 573
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/main/r12education/R12EducationView$4;->this$0:Lcom/squareup/ui/main/r12education/R12EducationView;

    invoke-static {p1}, Lcom/squareup/ui/main/r12education/R12EducationView;->access$800(Lcom/squareup/ui/main/r12education/R12EducationView;)Landroid/animation/ValueAnimator;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 574
    iget-object p1, p0, Lcom/squareup/ui/main/r12education/R12EducationView$4;->this$0:Lcom/squareup/ui/main/r12education/R12EducationView;

    invoke-static {p1}, Lcom/squareup/ui/main/r12education/R12EducationView;->access$800(Lcom/squareup/ui/main/r12education/R12EducationView;)Landroid/animation/ValueAnimator;

    move-result-object p1

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->cancel()V

    .line 575
    iget-object p1, p0, Lcom/squareup/ui/main/r12education/R12EducationView$4;->this$0:Lcom/squareup/ui/main/r12education/R12EducationView;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/squareup/ui/main/r12education/R12EducationView;->access$802(Lcom/squareup/ui/main/r12education/R12EducationView;Landroid/animation/ValueAnimator;)Landroid/animation/ValueAnimator;

    :cond_2
    :goto_0
    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public onPageScrolled(IFI)V
    .locals 12

    .line 501
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationView$4;->this$0:Lcom/squareup/ui/main/r12education/R12EducationView;

    invoke-static {v0}, Lcom/squareup/ui/main/r12education/R12EducationView;->access$200(Lcom/squareup/ui/main/r12education/R12EducationView;)Lcom/squareup/ui/main/r12education/R12EducationView$PanelList;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList;->panelAt(I)Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    move-result-object v0

    .line 502
    iget-object v1, p0, Lcom/squareup/ui/main/r12education/R12EducationView$4;->this$0:Lcom/squareup/ui/main/r12education/R12EducationView;

    invoke-static {v1}, Lcom/squareup/ui/main/r12education/R12EducationView;->access$300(Lcom/squareup/ui/main/r12education/R12EducationView;)Landroidx/viewpager/widget/ViewPager;

    move-result-object v1

    iget v2, v0, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->panelViewId:I

    invoke-static {v1, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 503
    sget v2, Lcom/squareup/readertutorial/R$id;->r12_education_matte:I

    .line 504
    invoke-static {v1, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v10

    const/4 v2, 0x1

    if-ltz p3, :cond_0

    const/4 p3, 0x1

    goto :goto_0

    :cond_0
    const/4 p3, 0x0

    :goto_0
    if-eqz p3, :cond_1

    add-int/lit8 v3, p1, 0x1

    .line 508
    iget-object v4, p0, Lcom/squareup/ui/main/r12education/R12EducationView$4;->this$0:Lcom/squareup/ui/main/r12education/R12EducationView;

    invoke-static {v4}, Lcom/squareup/ui/main/r12education/R12EducationView;->access$200(Lcom/squareup/ui/main/r12education/R12EducationView;)Lcom/squareup/ui/main/r12education/R12EducationView$PanelList;

    move-result-object v4

    invoke-virtual {v4}, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList;->getPanelCount()I

    move-result v4

    if-ge v3, v4, :cond_1

    .line 509
    iget-object p1, p0, Lcom/squareup/ui/main/r12education/R12EducationView$4;->this$0:Lcom/squareup/ui/main/r12education/R12EducationView;

    invoke-static {p1}, Lcom/squareup/ui/main/r12education/R12EducationView;->access$200(Lcom/squareup/ui/main/r12education/R12EducationView;)Lcom/squareup/ui/main/r12education/R12EducationView$PanelList;

    move-result-object p1

    invoke-virtual {p1, v3}, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList;->panelAt(I)Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    move-result-object p1

    goto :goto_1

    :cond_1
    if-nez p3, :cond_2

    if-lez p1, :cond_2

    .line 511
    iget-object p3, p0, Lcom/squareup/ui/main/r12education/R12EducationView$4;->this$0:Lcom/squareup/ui/main/r12education/R12EducationView;

    invoke-static {p3}, Lcom/squareup/ui/main/r12education/R12EducationView;->access$200(Lcom/squareup/ui/main/r12education/R12EducationView;)Lcom/squareup/ui/main/r12education/R12EducationView$PanelList;

    move-result-object p3

    sub-int/2addr p1, v2

    invoke-virtual {p3, p1}, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList;->panelAt(I)Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    move-result-object p1

    .line 517
    :goto_1
    iget-object p3, p0, Lcom/squareup/ui/main/r12education/R12EducationView$4;->this$0:Lcom/squareup/ui/main/r12education/R12EducationView;

    invoke-static {p3}, Lcom/squareup/ui/main/r12education/R12EducationView;->access$300(Lcom/squareup/ui/main/r12education/R12EducationView;)Landroidx/viewpager/widget/ViewPager;

    move-result-object p3

    iget v2, p1, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->panelViewId:I

    invoke-static {p3, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Landroid/view/ViewGroup;

    .line 518
    sget v2, Lcom/squareup/readertutorial/R$id;->r12_education_matte:I

    .line 519
    invoke-static {p3, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v11

    .line 521
    iget-object v2, p0, Lcom/squareup/ui/main/r12education/R12EducationView$4;->this$0:Lcom/squareup/ui/main/r12education/R12EducationView;

    invoke-static {v2}, Lcom/squareup/ui/main/r12education/R12EducationView;->access$500(Lcom/squareup/ui/main/r12education/R12EducationView;)Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;

    move-result-object v2

    invoke-virtual {v2, v0, p1}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->getTransition(Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;)Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;

    move-result-object p1

    .line 522
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationView$4;->this$0:Lcom/squareup/ui/main/r12education/R12EducationView;

    invoke-static {v0}, Lcom/squareup/ui/main/r12education/R12EducationView;->access$600(Lcom/squareup/ui/main/r12education/R12EducationView;)Ljava/util/LinkedHashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 523
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/ui/main/r12education/R12EducationView$Element;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    move-object v4, v2

    check-cast v4, Landroid/view/View;

    move-object v2, p1

    move v5, p2

    move-object v6, v1

    move-object v7, v10

    move-object v8, p3

    move-object v9, v11

    invoke-virtual/range {v2 .. v9}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;->tweenElement(Lcom/squareup/ui/main/r12education/R12EducationView$Element;Landroid/view/View;FLandroid/view/ViewGroup;Landroid/view/View;Landroid/view/ViewGroup;Landroid/view/View;)V

    goto :goto_2

    :cond_2
    return-void
.end method

.method public onPageSelected(I)V
    .locals 1

    .line 529
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationView$4;->this$0:Lcom/squareup/ui/main/r12education/R12EducationView;

    invoke-static {v0}, Lcom/squareup/ui/main/r12education/R12EducationView;->access$200(Lcom/squareup/ui/main/r12education/R12EducationView;)Lcom/squareup/ui/main/r12education/R12EducationView$PanelList;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/ui/main/r12education/R12EducationView$PanelList;->panelAt(I)Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    move-result-object p1

    .line 530
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationView$4;->this$0:Lcom/squareup/ui/main/r12education/R12EducationView;

    iget-object v0, v0, Lcom/squareup/ui/main/r12education/R12EducationView;->presenter:Lcom/squareup/ui/main/r12education/R12EducationScreen$Presenter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/main/r12education/R12EducationScreen$Presenter;->logPanelPresented(Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;)V

    .line 531
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/R12EducationView$4;->this$0:Lcom/squareup/ui/main/r12education/R12EducationView;

    invoke-static {v0}, Lcom/squareup/ui/main/r12education/R12EducationView;->access$700(Lcom/squareup/ui/main/r12education/R12EducationView;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->getButtonText()I

    move-result p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    return-void
.end method
