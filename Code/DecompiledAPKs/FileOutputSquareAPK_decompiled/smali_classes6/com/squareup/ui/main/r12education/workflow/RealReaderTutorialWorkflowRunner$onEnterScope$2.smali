.class final Lcom/squareup/ui/main/r12education/workflow/RealReaderTutorialWorkflowRunner$onEnterScope$2;
.super Ljava/lang/Object;
.source "RealReaderTutorialWorkflowRunner.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/main/r12education/workflow/RealReaderTutorialWorkflowRunner;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/ui/main/r12education/workflow/SpeedTestOutput;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealReaderTutorialWorkflowRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealReaderTutorialWorkflowRunner.kt\ncom/squareup/ui/main/r12education/workflow/RealReaderTutorialWorkflowRunner$onEnterScope$2\n+ 2 Flows.kt\ncom/squareup/container/Flows\n*L\n1#1,56:1\n75#2:57\n*E\n*S KotlinDebug\n*F\n+ 1 RealReaderTutorialWorkflowRunner.kt\ncom/squareup/ui/main/r12education/workflow/RealReaderTutorialWorkflowRunner$onEnterScope$2\n*L\n48#1:57\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/ui/main/r12education/workflow/SpeedTestOutput;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/main/r12education/workflow/RealReaderTutorialWorkflowRunner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/main/r12education/workflow/RealReaderTutorialWorkflowRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/main/r12education/workflow/RealReaderTutorialWorkflowRunner$onEnterScope$2;->this$0:Lcom/squareup/ui/main/r12education/workflow/RealReaderTutorialWorkflowRunner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/ui/main/r12education/workflow/SpeedTestOutput;)V
    .locals 3

    .line 48
    instance-of v0, p1, Lcom/squareup/ui/main/r12education/workflow/SpeedTestOutput$Cancel;

    const-string v1, "flow.get()"

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/main/r12education/workflow/RealReaderTutorialWorkflowRunner$onEnterScope$2;->this$0:Lcom/squareup/ui/main/r12education/workflow/RealReaderTutorialWorkflowRunner;

    invoke-static {p1}, Lcom/squareup/ui/main/r12education/workflow/RealReaderTutorialWorkflowRunner;->access$getFlow$p(Lcom/squareup/ui/main/r12education/workflow/RealReaderTutorialWorkflowRunner;)Ldagger/Lazy;

    move-result-object p1

    invoke-interface {p1}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object p1

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lflow/Flow;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Class;

    const/4 v1, 0x0

    .line 57
    const-class v2, Lcom/squareup/container/WorkflowTreeKey;

    aput-object v2, v0, v1

    invoke-static {p1, v0}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    goto :goto_0

    .line 49
    :cond_0
    instance-of v0, p1, Lcom/squareup/ui/main/r12education/workflow/SpeedTestOutput$Continue;

    if-eqz v0, :cond_1

    iget-object p1, p0, Lcom/squareup/ui/main/r12education/workflow/RealReaderTutorialWorkflowRunner$onEnterScope$2;->this$0:Lcom/squareup/ui/main/r12education/workflow/RealReaderTutorialWorkflowRunner;

    invoke-static {p1}, Lcom/squareup/ui/main/r12education/workflow/RealReaderTutorialWorkflowRunner;->access$getFlow$p(Lcom/squareup/ui/main/r12education/workflow/RealReaderTutorialWorkflowRunner;)Ldagger/Lazy;

    move-result-object p1

    invoke-interface {p1}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object p1

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lflow/Flow;

    new-instance v0, Lcom/squareup/ui/main/r12education/R12EducationScreen;

    iget-object v1, p0, Lcom/squareup/ui/main/r12education/workflow/RealReaderTutorialWorkflowRunner$onEnterScope$2;->this$0:Lcom/squareup/ui/main/r12education/workflow/RealReaderTutorialWorkflowRunner;

    invoke-static {v1}, Lcom/squareup/ui/main/r12education/workflow/RealReaderTutorialWorkflowRunner;->access$getUseUpdatingMessaging$p(Lcom/squareup/ui/main/r12education/workflow/RealReaderTutorialWorkflowRunner;)Z

    move-result v1

    invoke-direct {v0, v1}, Lcom/squareup/ui/main/r12education/R12EducationScreen;-><init>(Z)V

    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    invoke-static {p1, v0}, Lcom/squareup/container/Flows;->replaceTop(Lflow/Flow;Lcom/squareup/container/ContainerTreeKey;)V

    goto :goto_0

    .line 50
    :cond_1
    instance-of p1, p1, Lcom/squareup/ui/main/r12education/workflow/SpeedTestOutput$Retry;

    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/squareup/ui/main/r12education/workflow/RealReaderTutorialWorkflowRunner$onEnterScope$2;->this$0:Lcom/squareup/ui/main/r12education/workflow/RealReaderTutorialWorkflowRunner;

    invoke-static {p1}, Lcom/squareup/ui/main/r12education/workflow/RealReaderTutorialWorkflowRunner;->access$startOrRestart(Lcom/squareup/ui/main/r12education/workflow/RealReaderTutorialWorkflowRunner;)V

    :cond_2
    :goto_0
    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 21
    check-cast p1, Lcom/squareup/ui/main/r12education/workflow/SpeedTestOutput;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/main/r12education/workflow/RealReaderTutorialWorkflowRunner$onEnterScope$2;->accept(Lcom/squareup/ui/main/r12education/workflow/SpeedTestOutput;)V

    return-void
.end method
