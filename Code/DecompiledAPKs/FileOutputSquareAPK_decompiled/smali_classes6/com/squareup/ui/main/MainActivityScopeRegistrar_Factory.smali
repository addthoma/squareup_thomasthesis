.class public final Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;
.super Ljava/lang/Object;
.source "MainActivityScopeRegistrar_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/main/MainActivityScopeRegistrar;",
        ">;"
    }
.end annotation


# instance fields
.field private final activityVisibilityPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final additionalMainActivityBundlersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lmortar/bundler/Bundler;",
            ">;>;"
        }
    .end annotation
.end field

.field private final additionalMainActivityScopeServicesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lmortar/Scoped;",
            ">;>;"
        }
    .end annotation
.end field

.field private final apiReaderSettingsControllerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiReaderSettingsController;",
            ">;"
        }
    .end annotation
.end field

.field private final apiRequestControllerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiRequestController;",
            ">;"
        }
    .end annotation
.end field

.field private final apiTransactionControllerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiTransactionController;",
            ">;"
        }
    .end annotation
.end field

.field private final badBusProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;"
        }
    .end annotation
.end field

.field private final bankAccountSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/banklinking/BankAccountSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final buyerFlowStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerFlowStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderHubProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;"
        }
    .end annotation
.end field

.field private final cashDrawerShiftManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;",
            ">;"
        }
    .end annotation
.end field

.field private final clockProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;"
        }
    .end annotation
.end field

.field private final connectivityMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final dipperUiEventsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderUiEventSink;",
            ">;"
        }
    .end annotation
.end field

.field private final employeeCacheUpdaterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeCacheUpdater;",
            ">;"
        }
    .end annotation
.end field

.field private final emvSwipePassthroughEnablerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;",
            ">;"
        }
    .end annotation
.end field

.field private final helpBadgeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/HelpBadge;",
            ">;"
        }
    .end annotation
.end field

.field private final jailKeeperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/jailkeeper/JailKeeper;",
            ">;"
        }
    .end annotation
.end field

.field private final lockScreenMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/ui/LockScreenMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final nfcProcessorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/NfcProcessor;",
            ">;"
        }
    .end annotation
.end field

.field private final onboardingDiverterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/OnboardingDiverter;",
            ">;"
        }
    .end annotation
.end field

.field private final openTicketsSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final orderEntryScreenStateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            ">;"
        }
    .end annotation
.end field

.field private final passcodeEmployeeManagementProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;"
        }
    .end annotation
.end field

.field private final pauseAndResumeRegistrarProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pauses/PauseAndResumeRegistrar;",
            ">;"
        }
    .end annotation
.end field

.field private final paymentCompletionMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PaymentCompletionMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final printSpoolerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrintSpooler;",
            ">;"
        }
    .end annotation
.end field

.field private final printerScoutSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterScoutScheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final readerBatteryStatusHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final recorderStateMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/ReaderStatusMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final sessionExpiredListenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/SessionExpiredListener;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/tender/TenderStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/Tickets;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketsSweeperManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/opentickets/TicketsSweeperManager;",
            ">;"
        }
    .end annotation
.end field

.field private final tileAppearanceSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final topScreenCheckerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/TopScreenChecker;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialCoreProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;"
        }
    .end annotation
.end field

.field private final x2ScreenRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/jailkeeper/JailKeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PaymentCompletionMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pauses/PauseAndResumeRegistrar;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/NfcProcessor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterScoutScheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/Tickets;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/opentickets/TicketsSweeperManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrintSpooler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeCacheUpdater;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiTransactionController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiRequestController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiReaderSettingsController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/TopScreenChecker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/HelpBadge;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/OnboardingDiverter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderUiEventSink;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerFlowStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/tender/TenderStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/ui/LockScreenMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lmortar/Scoped;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lmortar/bundler/Bundler;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/ReaderStatusMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/banklinking/BankAccountSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/SessionExpiredListener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 166
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 167
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->badBusProvider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 168
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->x2ScreenRunnerProvider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 169
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->transactionProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 170
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->emvSwipePassthroughEnablerProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 171
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->orderEntryScreenStateProvider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 172
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->jailKeeperProvider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 173
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->openTicketsSettingsProvider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 174
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->paymentCompletionMonitorProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 175
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->pauseAndResumeRegistrarProvider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 176
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->nfcProcessorProvider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 177
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->printerScoutSchedulerProvider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 178
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->ticketsProvider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 179
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->ticketsSweeperManagerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 180
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->printSpoolerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 181
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->clockProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 182
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->cashDrawerShiftManagerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p17

    .line 183
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->cardReaderHubProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p18

    .line 184
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->passcodeEmployeeManagementProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p19

    .line 185
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->employeeCacheUpdaterProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p20

    .line 186
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->apiTransactionControllerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p21

    .line 187
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->apiRequestControllerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p22

    .line 188
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->apiReaderSettingsControllerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p23

    .line 189
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->topScreenCheckerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p24

    .line 190
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->helpBadgeProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p25

    .line 191
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->tileAppearanceSettingsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p26

    .line 192
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->connectivityMonitorProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p27

    .line 193
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->readerBatteryStatusHandlerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p28

    .line 194
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->onboardingDiverterProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p29

    .line 195
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->tutorialCoreProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p30

    .line 196
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->dipperUiEventsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p31

    .line 197
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->buyerFlowStarterProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p32

    .line 198
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->tenderStarterProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p33

    .line 199
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->lockScreenMonitorProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p34

    .line 200
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->additionalMainActivityScopeServicesProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p35

    .line 201
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->additionalMainActivityBundlersProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p36

    .line 202
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->recorderStateMonitorProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p37

    .line 203
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->bankAccountSettingsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p38

    .line 204
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->sessionExpiredListenerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p39

    .line 205
    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->activityVisibilityPresenterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;
    .locals 41
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/badbus/BadBus;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/jailkeeper/JailKeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PaymentCompletionMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pauses/PauseAndResumeRegistrar;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/NfcProcessor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterScoutScheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/Tickets;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/opentickets/TicketsSweeperManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrintSpooler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeCacheUpdater;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiTransactionController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiRequestController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiReaderSettingsController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/TopScreenChecker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/HelpBadge;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/onboarding/OnboardingDiverter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/dipper/ReaderUiEventSink;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerFlowStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/tender/TenderStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/ui/LockScreenMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lmortar/Scoped;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lmortar/bundler/Bundler;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/ReaderStatusMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/banklinking/BankAccountSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/SessionExpiredListener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;",
            ">;)",
            "Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    move-object/from16 v23, p22

    move-object/from16 v24, p23

    move-object/from16 v25, p24

    move-object/from16 v26, p25

    move-object/from16 v27, p26

    move-object/from16 v28, p27

    move-object/from16 v29, p28

    move-object/from16 v30, p29

    move-object/from16 v31, p30

    move-object/from16 v32, p31

    move-object/from16 v33, p32

    move-object/from16 v34, p33

    move-object/from16 v35, p34

    move-object/from16 v36, p35

    move-object/from16 v37, p36

    move-object/from16 v38, p37

    move-object/from16 v39, p38

    .line 250
    new-instance v40, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;

    move-object/from16 v0, v40

    invoke-direct/range {v0 .. v39}, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v40
.end method

.method public static newInstance(Lcom/squareup/badbus/BadBus;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/jailkeeper/JailKeeper;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/ui/main/PaymentCompletionMonitor;Lcom/squareup/pauses/PauseAndResumeRegistrar;Lcom/squareup/ui/NfcProcessor;Lcom/squareup/print/PrinterScoutScheduler;Lcom/squareup/tickets/Tickets;Lcom/squareup/opentickets/TicketsSweeperManager;Lcom/squareup/print/PrintSpooler;Lcom/squareup/util/Clock;Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/permissions/EmployeeCacheUpdater;Lcom/squareup/api/ApiTransactionController;Lcom/squareup/api/ApiRequestController;Lcom/squareup/api/ApiReaderSettingsController;Lcom/squareup/ui/main/TopScreenChecker;Lcom/squareup/ui/help/HelpBadge;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;Lcom/squareup/onboarding/OnboardingDiverter;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/cardreader/dipper/ReaderUiEventSink;Lcom/squareup/ui/buyer/BuyerFlowStarter;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/permissions/ui/LockScreenMonitor;Ljava/util/Set;Ljava/util/Set;Lcom/squareup/ui/main/ReaderStatusMonitor;Lcom/squareup/banklinking/BankAccountSettings;Lcom/squareup/account/SessionExpiredListener;Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;)Lcom/squareup/ui/main/MainActivityScopeRegistrar;
    .locals 41
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/badbus/BadBus;",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            "Lcom/squareup/jailkeeper/JailKeeper;",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            "Lcom/squareup/ui/main/PaymentCompletionMonitor;",
            "Lcom/squareup/pauses/PauseAndResumeRegistrar;",
            "Lcom/squareup/ui/NfcProcessor;",
            "Lcom/squareup/print/PrinterScoutScheduler;",
            "Lcom/squareup/tickets/Tickets;",
            "Lcom/squareup/opentickets/TicketsSweeperManager;",
            "Lcom/squareup/print/PrintSpooler;",
            "Lcom/squareup/util/Clock;",
            "Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;",
            "Lcom/squareup/cardreader/CardReaderHub;",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            "Lcom/squareup/permissions/EmployeeCacheUpdater;",
            "Lcom/squareup/api/ApiTransactionController;",
            "Lcom/squareup/api/ApiRequestController;",
            "Lcom/squareup/api/ApiReaderSettingsController;",
            "Lcom/squareup/ui/main/TopScreenChecker;",
            "Lcom/squareup/ui/help/HelpBadge;",
            "Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            "Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;",
            "Lcom/squareup/onboarding/OnboardingDiverter;",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            "Lcom/squareup/cardreader/dipper/ReaderUiEventSink;",
            "Lcom/squareup/ui/buyer/BuyerFlowStarter;",
            "Lcom/squareup/ui/tender/TenderStarter;",
            "Lcom/squareup/permissions/ui/LockScreenMonitor;",
            "Ljava/util/Set<",
            "Lmortar/Scoped;",
            ">;",
            "Ljava/util/Set<",
            "Lmortar/bundler/Bundler;",
            ">;",
            "Lcom/squareup/ui/main/ReaderStatusMonitor;",
            "Lcom/squareup/banklinking/BankAccountSettings;",
            "Lcom/squareup/account/SessionExpiredListener;",
            "Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;",
            ")",
            "Lcom/squareup/ui/main/MainActivityScopeRegistrar;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    move-object/from16 v23, p22

    move-object/from16 v24, p23

    move-object/from16 v25, p24

    move-object/from16 v26, p25

    move-object/from16 v27, p26

    move-object/from16 v28, p27

    move-object/from16 v29, p28

    move-object/from16 v30, p29

    move-object/from16 v31, p30

    move-object/from16 v32, p31

    move-object/from16 v33, p32

    move-object/from16 v34, p33

    move-object/from16 v35, p34

    move-object/from16 v36, p35

    move-object/from16 v37, p36

    move-object/from16 v38, p37

    move-object/from16 v39, p38

    .line 275
    new-instance v40, Lcom/squareup/ui/main/MainActivityScopeRegistrar;

    move-object/from16 v0, v40

    invoke-direct/range {v0 .. v39}, Lcom/squareup/ui/main/MainActivityScopeRegistrar;-><init>(Lcom/squareup/badbus/BadBus;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/jailkeeper/JailKeeper;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/ui/main/PaymentCompletionMonitor;Lcom/squareup/pauses/PauseAndResumeRegistrar;Lcom/squareup/ui/NfcProcessor;Lcom/squareup/print/PrinterScoutScheduler;Lcom/squareup/tickets/Tickets;Lcom/squareup/opentickets/TicketsSweeperManager;Lcom/squareup/print/PrintSpooler;Lcom/squareup/util/Clock;Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/permissions/EmployeeCacheUpdater;Lcom/squareup/api/ApiTransactionController;Lcom/squareup/api/ApiRequestController;Lcom/squareup/api/ApiReaderSettingsController;Lcom/squareup/ui/main/TopScreenChecker;Lcom/squareup/ui/help/HelpBadge;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;Lcom/squareup/onboarding/OnboardingDiverter;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/cardreader/dipper/ReaderUiEventSink;Lcom/squareup/ui/buyer/BuyerFlowStarter;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/permissions/ui/LockScreenMonitor;Ljava/util/Set;Ljava/util/Set;Lcom/squareup/ui/main/ReaderStatusMonitor;Lcom/squareup/banklinking/BankAccountSettings;Lcom/squareup/account/SessionExpiredListener;Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;)V

    return-object v40
.end method


# virtual methods
.method public get()Lcom/squareup/ui/main/MainActivityScopeRegistrar;
    .locals 41

    move-object/from16 v0, p0

    .line 210
    iget-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->badBusProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/badbus/BadBus;

    iget-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->x2ScreenRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    iget-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/payment/Transaction;

    iget-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->emvSwipePassthroughEnablerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;

    iget-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->orderEntryScreenStateProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/squareup/orderentry/OrderEntryScreenState;

    iget-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->jailKeeperProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/squareup/jailkeeper/JailKeeper;

    iget-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->openTicketsSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/squareup/tickets/OpenTicketsSettings;

    iget-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->paymentCompletionMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/squareup/ui/main/PaymentCompletionMonitor;

    iget-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->pauseAndResumeRegistrarProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/squareup/pauses/PauseAndResumeRegistrar;

    iget-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->nfcProcessorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/squareup/ui/NfcProcessor;

    iget-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->printerScoutSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lcom/squareup/print/PrinterScoutScheduler;

    iget-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->ticketsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/squareup/tickets/Tickets;

    iget-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->ticketsSweeperManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/squareup/opentickets/TicketsSweeperManager;

    iget-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->printSpoolerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/squareup/print/PrintSpooler;

    iget-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->clockProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/util/Clock;

    iget-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->cashDrawerShiftManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v17, v1

    check-cast v17, Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;

    iget-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->cardReaderHubProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v18, v1

    check-cast v18, Lcom/squareup/cardreader/CardReaderHub;

    iget-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->passcodeEmployeeManagementProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v19, v1

    check-cast v19, Lcom/squareup/permissions/PasscodeEmployeeManagement;

    iget-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->employeeCacheUpdaterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v20, v1

    check-cast v20, Lcom/squareup/permissions/EmployeeCacheUpdater;

    iget-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->apiTransactionControllerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v21, v1

    check-cast v21, Lcom/squareup/api/ApiTransactionController;

    iget-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->apiRequestControllerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v22, v1

    check-cast v22, Lcom/squareup/api/ApiRequestController;

    iget-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->apiReaderSettingsControllerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v23, v1

    check-cast v23, Lcom/squareup/api/ApiReaderSettingsController;

    iget-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->topScreenCheckerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v24, v1

    check-cast v24, Lcom/squareup/ui/main/TopScreenChecker;

    iget-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->helpBadgeProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v25, v1

    check-cast v25, Lcom/squareup/ui/help/HelpBadge;

    iget-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->tileAppearanceSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v26, v1

    check-cast v26, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    iget-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->connectivityMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v27, v1

    check-cast v27, Lcom/squareup/connectivity/ConnectivityMonitor;

    iget-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->readerBatteryStatusHandlerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v28, v1

    check-cast v28, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;

    iget-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->onboardingDiverterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v29, v1

    check-cast v29, Lcom/squareup/onboarding/OnboardingDiverter;

    iget-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->tutorialCoreProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v30, v1

    check-cast v30, Lcom/squareup/tutorialv2/TutorialCore;

    iget-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->dipperUiEventsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v31, v1

    check-cast v31, Lcom/squareup/cardreader/dipper/ReaderUiEventSink;

    iget-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->buyerFlowStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v32, v1

    check-cast v32, Lcom/squareup/ui/buyer/BuyerFlowStarter;

    iget-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->tenderStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v33, v1

    check-cast v33, Lcom/squareup/ui/tender/TenderStarter;

    iget-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->lockScreenMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v34, v1

    check-cast v34, Lcom/squareup/permissions/ui/LockScreenMonitor;

    iget-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->additionalMainActivityScopeServicesProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v35, v1

    check-cast v35, Ljava/util/Set;

    iget-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->additionalMainActivityBundlersProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v36, v1

    check-cast v36, Ljava/util/Set;

    iget-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->recorderStateMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v37, v1

    check-cast v37, Lcom/squareup/ui/main/ReaderStatusMonitor;

    iget-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->bankAccountSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v38, v1

    check-cast v38, Lcom/squareup/banklinking/BankAccountSettings;

    iget-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->sessionExpiredListenerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v39, v1

    check-cast v39, Lcom/squareup/account/SessionExpiredListener;

    iget-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->activityVisibilityPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v40, v1

    check-cast v40, Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;

    invoke-static/range {v2 .. v40}, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->newInstance(Lcom/squareup/badbus/BadBus;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/jailkeeper/JailKeeper;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/ui/main/PaymentCompletionMonitor;Lcom/squareup/pauses/PauseAndResumeRegistrar;Lcom/squareup/ui/NfcProcessor;Lcom/squareup/print/PrinterScoutScheduler;Lcom/squareup/tickets/Tickets;Lcom/squareup/opentickets/TicketsSweeperManager;Lcom/squareup/print/PrintSpooler;Lcom/squareup/util/Clock;Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/permissions/EmployeeCacheUpdater;Lcom/squareup/api/ApiTransactionController;Lcom/squareup/api/ApiRequestController;Lcom/squareup/api/ApiReaderSettingsController;Lcom/squareup/ui/main/TopScreenChecker;Lcom/squareup/ui/help/HelpBadge;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;Lcom/squareup/onboarding/OnboardingDiverter;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/cardreader/dipper/ReaderUiEventSink;Lcom/squareup/ui/buyer/BuyerFlowStarter;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/permissions/ui/LockScreenMonitor;Ljava/util/Set;Ljava/util/Set;Lcom/squareup/ui/main/ReaderStatusMonitor;Lcom/squareup/banklinking/BankAccountSettings;Lcom/squareup/account/SessionExpiredListener;Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;)Lcom/squareup/ui/main/MainActivityScopeRegistrar;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 43
    invoke-virtual {p0}, Lcom/squareup/ui/main/MainActivityScopeRegistrar_Factory;->get()Lcom/squareup/ui/main/MainActivityScopeRegistrar;

    move-result-object v0

    return-object v0
.end method
