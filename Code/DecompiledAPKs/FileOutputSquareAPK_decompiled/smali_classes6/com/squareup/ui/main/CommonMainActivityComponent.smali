.class public interface abstract Lcom/squareup/ui/main/CommonMainActivityComponent;
.super Ljava/lang/Object;
.source "CommonMainActivityComponent.java"

# interfaces
.implements Lcom/squareup/ui/systempermissions/AboutDeviceSettingsScreen$ParentComponent;
.implements Lcom/squareup/ui/activity/ActivityAppletStarter$ParentComponent;
.implements Lcom/squareup/ui/main/ApiMainActivity$Component;
.implements Lcom/squareup/api/ApiTransactionBootstrapScreen$ParentComponent;
.implements Lcom/squareup/applet/AppletsDrawerLayout$ParentComponent;
.implements Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$ParentComponent;
.implements Lcom/squareup/banklinking/widgets/BankAccountFieldsView$ParentComponent;
.implements Lcom/squareup/register/widgets/card/CardEditor$ParentComponent;
.implements Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen$ParentComponent;
.implements Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$ParentComponent;
.implements Lcom/squareup/ui/systempermissions/EnableDeviceSettingsScreen$ParentComponent;
.implements Lcom/squareup/gen2/Gen2DenialDialog$ParentComponent;
.implements Lcom/squareup/invoicesappletapi/InvoicesAppletRunner$ParentComponent;
.implements Lcom/squareup/ui/LinkSpan$Component;
.implements Lcom/squareup/ui/main/MainActivity$Component;
.implements Lcom/squareup/register/widgets/LegacyNohoDatePickerDialogScreen$Component;
.implements Lcom/squareup/register/widgets/NohoDurationPickerDialogScreen$Component;
.implements Lcom/squareup/ui/main/errors/NoPaymentWarningScreen$ParentComponent;
.implements Lcom/squareup/ui/reader_deprecation/O1ReminderDialogScreen$ParentComponent;
.implements Lcom/squareup/onboarding/OnboardingWorkflowRunner$ParentComponent;
.implements Lcom/squareup/ui/settings/paymentdevices/pairing/PairingScope$ParentComponent;
.implements Lcom/squareup/register/widgets/card/PanEditor$Component;
.implements Lcom/squareup/ui/permissions/PermissionDeniedScreen$ParentComponent;
.implements Lcom/squareup/ui/main/CheckoutWorkflowRunner$ParentComponent;
.implements Lcom/squareup/caller/ProgressPopup$Component;
.implements Lcom/squareup/ui/main/errors/RootReaderWarningScreen$ParentComponent;
.implements Lcom/squareup/rootview/RootView$Component;
.implements Lcom/squareup/address/StatePickerScreen$Component;
.implements Lcom/squareup/ui/main/errors/SwipeDipTapWarningScreen$ParentComponent;
.implements Lcom/squareup/ui/systempermissions/SystemPermissionRevokedScreen$ParentComponent;
.implements Lcom/squareup/register/tutorial/TutorialsComponent;
.implements Lcom/squareup/x2/X2MonitorWorkflowRunner$ParentComponent;


# virtual methods
.method public abstract inject(Lcom/squareup/rootview/RootView;)V
.end method
