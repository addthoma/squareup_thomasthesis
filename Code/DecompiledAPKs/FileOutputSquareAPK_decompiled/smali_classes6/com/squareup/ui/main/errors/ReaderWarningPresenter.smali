.class public Lcom/squareup/ui/main/errors/ReaderWarningPresenter;
.super Lmortar/ViewPresenter;
.source "ReaderWarningPresenter.java"

# interfaces
.implements Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;
.implements Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;
.implements Lcom/squareup/pauses/PausesAndResumes;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/main/errors/ReaderWarningPresenter$SharedScope;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/main/errors/ReaderWarningView;",
        ">;",
        "Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;",
        "Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;",
        "Lcom/squareup/pauses/PausesAndResumes;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final badBus:Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;

.field private final cardReaderFactory:Lcom/squareup/cardreader/CardReaderFactory;

.field private final cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

.field private final cardReaderHubScoper:Lcom/squareup/cardreader/dipper/CardReaderHubScoper;

.field private final defaultEmvCardInsertRemoveProcessor:Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor;

.field private final emvDipScreenHandler:Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

.field private handler:Lcom/squareup/ui/main/errors/ReaderWarningScreenHandler;

.field private final nfcProcessor:Lcom/squareup/ui/NfcProcessorInterface;

.field private final pauseAndResumeRegistrar:Lcom/squareup/pauses/PauseAndResumeRegistrar;

.field private final res:Lcom/squareup/util/Res;

.field private screen:Lcom/squareup/ui/main/errors/ReaderWarningScreen;


# direct methods
.method public constructor <init>(Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;Lcom/squareup/util/Res;Lcom/squareup/cardreader/CardReaderFactory;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/cardreader/dipper/CardReaderHubScoper;Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;Lcom/squareup/ui/NfcProcessorInterface;Lcom/squareup/pauses/PauseAndResumeRegistrar;Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 60
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 61
    iput-object p1, p0, Lcom/squareup/ui/main/errors/ReaderWarningPresenter;->badBus:Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;

    .line 62
    iput-object p2, p0, Lcom/squareup/ui/main/errors/ReaderWarningPresenter;->res:Lcom/squareup/util/Res;

    .line 63
    iput-object p3, p0, Lcom/squareup/ui/main/errors/ReaderWarningPresenter;->cardReaderFactory:Lcom/squareup/cardreader/CardReaderFactory;

    .line 64
    iput-object p4, p0, Lcom/squareup/ui/main/errors/ReaderWarningPresenter;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    .line 65
    iput-object p5, p0, Lcom/squareup/ui/main/errors/ReaderWarningPresenter;->cardReaderHubScoper:Lcom/squareup/cardreader/dipper/CardReaderHubScoper;

    .line 66
    iput-object p6, p0, Lcom/squareup/ui/main/errors/ReaderWarningPresenter;->emvDipScreenHandler:Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

    .line 67
    iput-object p7, p0, Lcom/squareup/ui/main/errors/ReaderWarningPresenter;->nfcProcessor:Lcom/squareup/ui/NfcProcessorInterface;

    .line 68
    iput-object p8, p0, Lcom/squareup/ui/main/errors/ReaderWarningPresenter;->pauseAndResumeRegistrar:Lcom/squareup/pauses/PauseAndResumeRegistrar;

    .line 69
    iput-object p9, p0, Lcom/squareup/ui/main/errors/ReaderWarningPresenter;->defaultEmvCardInsertRemoveProcessor:Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor;

    return-void
.end method


# virtual methods
.method public getWarningScreenDescription()Ljava/lang/String;
    .locals 4

    .line 121
    iget-object v0, p0, Lcom/squareup/ui/main/errors/ReaderWarningPresenter;->screen:Lcom/squareup/ui/main/errors/ReaderWarningScreen;

    invoke-interface {v0}, Lcom/squareup/ui/main/errors/ReaderWarningScreen;->getName()Ljava/lang/String;

    move-result-object v0

    .line 122
    iget-object v1, p0, Lcom/squareup/ui/main/errors/ReaderWarningPresenter;->screen:Lcom/squareup/ui/main/errors/ReaderWarningScreen;

    invoke-interface {v1}, Lcom/squareup/ui/main/errors/ReaderWarningScreen;->getInitialParameters()Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->localizedTitle:Ljava/lang/String;

    const-string v2, "\""

    const-string v3, " \""

    if-eqz v1, :cond_0

    .line 123
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/squareup/ui/main/errors/ReaderWarningPresenter;->screen:Lcom/squareup/ui/main/errors/ReaderWarningScreen;

    invoke-interface {v0}, Lcom/squareup/ui/main/errors/ReaderWarningScreen;->getInitialParameters()Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->localizedTitle:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 124
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/main/errors/ReaderWarningPresenter;->screen:Lcom/squareup/ui/main/errors/ReaderWarningScreen;

    invoke-interface {v1}, Lcom/squareup/ui/main/errors/ReaderWarningScreen;->getInitialParameters()Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    move-result-object v1

    iget v1, v1, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->titleId:I

    if-eqz v1, :cond_1

    .line 125
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/squareup/ui/main/errors/ReaderWarningPresenter;->res:Lcom/squareup/util/Res;

    iget-object v3, p0, Lcom/squareup/ui/main/errors/ReaderWarningPresenter;->screen:Lcom/squareup/ui/main/errors/ReaderWarningScreen;

    invoke-interface {v3}, Lcom/squareup/ui/main/errors/ReaderWarningScreen;->getInitialParameters()Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    move-result-object v3

    iget v3, v3, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->titleId:I

    invoke-interface {v0, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_1
    return-object v0
.end method

.method handleBackPressed()Z
    .locals 1

    .line 211
    iget-object v0, p0, Lcom/squareup/ui/main/errors/ReaderWarningPresenter;->handler:Lcom/squareup/ui/main/errors/ReaderWarningScreenHandler;

    invoke-virtual {v0}, Lcom/squareup/ui/main/errors/ReaderWarningScreenHandler;->handleBackPressed()Z

    move-result v0

    return v0
.end method

.method public onCardReaderAdded(Lcom/squareup/cardreader/CardReader;)V
    .locals 0

    return-void
.end method

.method public onCardReaderRemoved(Lcom/squareup/cardreader/CardReader;)V
    .locals 2

    .line 113
    iget-object v0, p0, Lcom/squareup/ui/main/errors/ReaderWarningPresenter;->screen:Lcom/squareup/ui/main/errors/ReaderWarningScreen;

    const-class v1, Lcom/squareup/ui/main/errors/RequiresReaderDisconnection;

    invoke-static {v0, v1}, Lcom/squareup/util/Objects;->isAnnotated(Ljava/lang/Object;Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 114
    invoke-interface {p1}, Lcom/squareup/cardreader/CardReader;->getId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/main/errors/ReaderWarningPresenter;->screen:Lcom/squareup/ui/main/errors/ReaderWarningScreen;

    invoke-interface {v0}, Lcom/squareup/ui/main/errors/ReaderWarningScreen;->getInitialParameters()Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->cardReaderId:Lcom/squareup/cardreader/CardReaderId;

    if-ne p1, v0, :cond_0

    .line 115
    invoke-virtual {p0}, Lcom/squareup/ui/main/errors/ReaderWarningPresenter;->hasView()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 116
    invoke-virtual {p0}, Lcom/squareup/ui/main/errors/ReaderWarningPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/main/errors/ReaderWarningView;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/squareup/ui/main/errors/ReaderWarningView;->showCancelButton(Z)V

    :cond_0
    return-void
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    .line 74
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/main/errors/ReaderWarningScreen;

    iput-object v0, p0, Lcom/squareup/ui/main/errors/ReaderWarningPresenter;->screen:Lcom/squareup/ui/main/errors/ReaderWarningScreen;

    .line 76
    iget-object v0, p0, Lcom/squareup/ui/main/errors/ReaderWarningPresenter;->screen:Lcom/squareup/ui/main/errors/ReaderWarningScreen;

    invoke-interface {v0}, Lcom/squareup/ui/main/errors/ReaderWarningScreen;->getInitialParameters()Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->warningType:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    .line 77
    invoke-static {v0, p1}, Lcom/squareup/ui/main/errors/ReaderWarningTypeHandlerFactory;->handlerForType(Lcom/squareup/cardreader/ui/api/ReaderWarningType;Lmortar/MortarScope;)Lcom/squareup/ui/main/errors/ReaderWarningScreenHandler;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/main/errors/ReaderWarningPresenter;->handler:Lcom/squareup/ui/main/errors/ReaderWarningScreenHandler;

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/main/errors/ReaderWarningPresenter;->handler:Lcom/squareup/ui/main/errors/ReaderWarningScreenHandler;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/main/errors/ReaderWarningScreenHandler;->setPresenter(Lcom/squareup/ui/main/errors/ReaderWarningPresenter;)V

    .line 79
    iget-object v0, p0, Lcom/squareup/ui/main/errors/ReaderWarningPresenter;->handler:Lcom/squareup/ui/main/errors/ReaderWarningScreenHandler;

    iget-object v1, p0, Lcom/squareup/ui/main/errors/ReaderWarningPresenter;->defaultEmvCardInsertRemoveProcessor:Lcom/squareup/cardreader/dipper/DefaultEmvCardInsertRemoveProcessor;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/main/errors/ReaderWarningScreenHandler;->setDefaultEmvCardInsertRemoveProcessor(Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;)V

    .line 80
    iget-object v0, p0, Lcom/squareup/ui/main/errors/ReaderWarningPresenter;->handler:Lcom/squareup/ui/main/errors/ReaderWarningScreenHandler;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 82
    iget-object v0, p0, Lcom/squareup/ui/main/errors/ReaderWarningPresenter;->cardReaderHubScoper:Lcom/squareup/cardreader/dipper/CardReaderHubScoper;

    invoke-virtual {v0, p1, p0}, Lcom/squareup/cardreader/dipper/CardReaderHubScoper;->scopeAttachListener(Lmortar/MortarScope;Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;)V

    .line 83
    iget-object v0, p0, Lcom/squareup/ui/main/errors/ReaderWarningPresenter;->cardReaderHubScoper:Lcom/squareup/cardreader/dipper/CardReaderHubScoper;

    iget-object v1, p0, Lcom/squareup/ui/main/errors/ReaderWarningPresenter;->handler:Lcom/squareup/ui/main/errors/ReaderWarningScreenHandler;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/cardreader/dipper/CardReaderHubScoper;->scopeAttachListener(Lmortar/MortarScope;Lcom/squareup/cardreader/CardReaderHub$CardReaderAttachListener;)V

    .line 84
    iget-object v0, p0, Lcom/squareup/ui/main/errors/ReaderWarningPresenter;->emvDipScreenHandler:Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;

    invoke-interface {v0, p1, p0}, Lcom/squareup/cardreader/ui/api/EmvDipScreenHandler;->registerEmvCardInsertRemoveProcessor(Lmortar/MortarScope;Lcom/squareup/cardreader/dipper/EmvCardInsertRemoveProcessor;)V

    .line 85
    iget-object v0, p0, Lcom/squareup/ui/main/errors/ReaderWarningPresenter;->pauseAndResumeRegistrar:Lcom/squareup/pauses/PauseAndResumeRegistrar;

    invoke-interface {v0, p1, p0}, Lcom/squareup/pauses/PauseAndResumeRegistrar;->register(Lmortar/MortarScope;Lcom/squareup/pauses/PausesAndResumes;)V

    .line 87
    iget-object v0, p0, Lcom/squareup/ui/main/errors/ReaderWarningPresenter;->badBus:Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;

    invoke-virtual {v0}, Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;->successfulSwipes()Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/main/errors/ReaderWarningPresenter;->handler:Lcom/squareup/ui/main/errors/ReaderWarningScreenHandler;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/main/errors/-$$Lambda$eU47_nIidM3xpPOjPSu1yn7fZh4;

    invoke-direct {v2, v1}, Lcom/squareup/ui/main/errors/-$$Lambda$eU47_nIidM3xpPOjPSu1yn7fZh4;-><init>(Lcom/squareup/ui/main/errors/ReaderWarningScreenHandler;)V

    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 88
    iget-object v0, p0, Lcom/squareup/ui/main/errors/ReaderWarningPresenter;->badBus:Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;

    invoke-virtual {v0}, Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;->failedSwipes()Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/main/errors/ReaderWarningPresenter;->handler:Lcom/squareup/ui/main/errors/ReaderWarningScreenHandler;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/main/errors/-$$Lambda$xTMBIJ2ohoya1jhgkLyf50e5cmM;

    invoke-direct {v2, v1}, Lcom/squareup/ui/main/errors/-$$Lambda$xTMBIJ2ohoya1jhgkLyf50e5cmM;-><init>(Lcom/squareup/ui/main/errors/ReaderWarningScreenHandler;)V

    invoke-virtual {v0, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 90
    iget-object p1, p0, Lcom/squareup/ui/main/errors/ReaderWarningPresenter;->nfcProcessor:Lcom/squareup/ui/NfcProcessorInterface;

    invoke-interface {p1}, Lcom/squareup/ui/NfcProcessorInterface;->continueMonitoring()V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 94
    invoke-virtual {p0}, Lcom/squareup/ui/main/errors/ReaderWarningPresenter;->refreshParameters()V

    .line 96
    invoke-virtual {p0}, Lcom/squareup/ui/main/errors/ReaderWarningPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/main/errors/ReaderWarningView;

    .line 97
    new-instance v0, Lcom/squareup/ui/main/errors/-$$Lambda$FUSkOPVzpFYDYoYbWIEL7UMr2Mc;

    invoke-direct {v0, p0}, Lcom/squareup/ui/main/errors/-$$Lambda$FUSkOPVzpFYDYoYbWIEL7UMr2Mc;-><init>(Lcom/squareup/ui/main/errors/ReaderWarningPresenter;)V

    invoke-static {p1, v0}, Lcom/squareup/workflow/ui/HandlesBack$Helper;->setConditionalBackHandler(Landroid/view/View;Lcom/squareup/workflow/ui/HandlesBack;)V

    return-void
.end method

.method public onPause()V
    .locals 1

    .line 101
    iget-object v0, p0, Lcom/squareup/ui/main/errors/ReaderWarningPresenter;->handler:Lcom/squareup/ui/main/errors/ReaderWarningScreenHandler;

    invoke-virtual {v0}, Lcom/squareup/ui/main/errors/ReaderWarningScreenHandler;->onPause()V

    return-void
.end method

.method public onResume()V
    .locals 1

    .line 105
    iget-object v0, p0, Lcom/squareup/ui/main/errors/ReaderWarningPresenter;->handler:Lcom/squareup/ui/main/errors/ReaderWarningScreenHandler;

    invoke-virtual {v0}, Lcom/squareup/ui/main/errors/ReaderWarningScreenHandler;->onResume()V

    return-void
.end method

.method public processEmvCardInserted(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 1

    .line 203
    iget-object v0, p0, Lcom/squareup/ui/main/errors/ReaderWarningPresenter;->handler:Lcom/squareup/ui/main/errors/ReaderWarningScreenHandler;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/main/errors/ReaderWarningScreenHandler;->processEmvCardInserted(Lcom/squareup/cardreader/CardReaderInfo;)V

    return-void
.end method

.method public processEmvCardRemoved(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 1

    .line 207
    iget-object v0, p0, Lcom/squareup/ui/main/errors/ReaderWarningPresenter;->handler:Lcom/squareup/ui/main/errors/ReaderWarningScreenHandler;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/main/errors/ReaderWarningScreenHandler;->processEmvCardRemoved(Lcom/squareup/cardreader/CardReaderInfo;)V

    return-void
.end method

.method public refreshParameters()V
    .locals 7

    .line 131
    invoke-virtual {p0}, Lcom/squareup/ui/main/errors/ReaderWarningPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/main/errors/ReaderWarningView;

    if-nez v0, :cond_0

    return-void

    .line 134
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/main/errors/ReaderWarningPresenter;->handler:Lcom/squareup/ui/main/errors/ReaderWarningScreenHandler;

    iget-object v2, p0, Lcom/squareup/ui/main/errors/ReaderWarningPresenter;->screen:Lcom/squareup/ui/main/errors/ReaderWarningScreen;

    invoke-interface {v2}, Lcom/squareup/ui/main/errors/ReaderWarningScreen;->getInitialParameters()Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/ui/main/errors/ReaderWarningScreenHandler;->from(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    move-result-object v1

    .line 136
    iget-object v2, v1, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    if-eqz v2, :cond_1

    .line 137
    iget-object v2, v1, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->glyph:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {v0, v2}, Lcom/squareup/ui/main/errors/ReaderWarningView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    goto :goto_0

    .line 139
    :cond_1
    iget-object v2, v1, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->vectorId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/squareup/ui/main/errors/ReaderWarningView;->setVector(I)V

    .line 142
    :goto_0
    iget v2, v1, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->titleId:I

    const/4 v3, 0x1

    if-lez v2, :cond_2

    .line 143
    iget-object v2, p0, Lcom/squareup/ui/main/errors/ReaderWarningPresenter;->res:Lcom/squareup/util/Res;

    iget v4, v1, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->titleId:I

    invoke-interface {v2, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/ui/main/errors/ReaderWarningView;->setTitle(Ljava/lang/String;)V

    goto :goto_1

    .line 144
    :cond_2
    iget-object v2, v1, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->localizedTitle:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 145
    iget-object v2, v1, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->localizedTitle:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/squareup/ui/main/errors/ReaderWarningView;->setTitle(Ljava/lang/String;)V

    goto :goto_1

    .line 148
    :cond_3
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "No title given for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/squareup/ui/main/errors/ReaderWarningPresenter;->screen:Lcom/squareup/ui/main/errors/ReaderWarningScreen;

    .line 149
    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 148
    invoke-static {v2}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;)V

    .line 152
    iget-object v2, p0, Lcom/squareup/ui/main/errors/ReaderWarningPresenter;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/cardreader/ui/R$string;->emv_unknown_error:I

    invoke-interface {v2, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/ui/main/errors/ReaderWarningView;->setTitle(Ljava/lang/String;)V

    .line 153
    iget v2, v1, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->messageId:I

    if-ge v2, v3, :cond_4

    iget-object v2, v1, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->localizedMessage:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 154
    iget-object v2, p0, Lcom/squareup/ui/main/errors/ReaderWarningPresenter;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/cardreader/ui/R$string;->please_try_again_or_contact_support:I

    invoke-interface {v2, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/ui/main/errors/ReaderWarningView;->setMessage(Ljava/lang/String;)V

    .line 158
    :cond_4
    :goto_1
    iget v2, v1, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->messageId:I

    if-lez v2, :cond_5

    .line 159
    iget-object v2, p0, Lcom/squareup/ui/main/errors/ReaderWarningPresenter;->res:Lcom/squareup/util/Res;

    iget v4, v1, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->messageId:I

    invoke-interface {v2, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/ui/main/errors/ReaderWarningView;->setMessage(Ljava/lang/String;)V

    goto :goto_2

    .line 160
    :cond_5
    iget-object v2, v1, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->localizedMessage:Ljava/lang/String;

    if-eqz v2, :cond_6

    .line 161
    iget-object v2, v1, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->localizedMessage:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/squareup/ui/main/errors/ReaderWarningView;->setMessage(Ljava/lang/String;)V

    .line 165
    :cond_6
    :goto_2
    iget v2, v1, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->importantMessageId:I

    if-lez v2, :cond_7

    .line 166
    iget v2, v1, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->importantMessageId:I

    invoke-virtual {v0, v2}, Lcom/squareup/ui/main/errors/ReaderWarningView;->showImportantMessage(I)V

    goto :goto_3

    .line 168
    :cond_7
    invoke-virtual {v0}, Lcom/squareup/ui/main/errors/ReaderWarningView;->hideImportantMessage()V

    .line 171
    :goto_3
    iget-object v2, v1, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->defaultButton:Lcom/squareup/cardreader/ui/api/ButtonDescriptor;

    const/4 v4, 0x0

    if-eqz v2, :cond_8

    .line 172
    iget-object v2, v1, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->defaultButton:Lcom/squareup/cardreader/ui/api/ButtonDescriptor;

    invoke-virtual {v0, v2}, Lcom/squareup/ui/main/errors/ReaderWarningView;->showBottomDefaultButton(Lcom/squareup/cardreader/ui/api/ButtonDescriptor;)V

    goto :goto_4

    .line 174
    :cond_8
    new-instance v2, Ljava/lang/IllegalStateException;

    new-array v5, v3, [Ljava/lang/Object;

    iget-object v6, v1, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->warningType:Lcom/squareup/cardreader/ui/api/ReaderWarningType;

    aput-object v6, v5, v4

    const-string v6, "No default button given for %s."

    .line 175
    invoke-static {v6, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 174
    invoke-static {v2}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;)V

    .line 178
    :goto_4
    iget-object v2, v1, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->secondButton:Lcom/squareup/cardreader/ui/api/ButtonDescriptor;

    if-eqz v2, :cond_9

    .line 179
    iget-object v1, v1, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->secondButton:Lcom/squareup/cardreader/ui/api/ButtonDescriptor;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/main/errors/ReaderWarningView;->showTopAlternativeButton(Lcom/squareup/cardreader/ui/api/ButtonDescriptor;)V

    .line 183
    :cond_9
    iget-object v1, p0, Lcom/squareup/ui/main/errors/ReaderWarningPresenter;->screen:Lcom/squareup/ui/main/errors/ReaderWarningScreen;

    const-class v2, Lcom/squareup/ui/main/errors/RequiresReaderDisconnection;

    invoke-static {v1, v2}, Lcom/squareup/util/Objects;->isAnnotated(Ljava/lang/Object;Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 184
    iget-object v1, p0, Lcom/squareup/ui/main/errors/ReaderWarningPresenter;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    iget-object v2, p0, Lcom/squareup/ui/main/errors/ReaderWarningPresenter;->screen:Lcom/squareup/ui/main/errors/ReaderWarningScreen;

    .line 185
    invoke-interface {v2}, Lcom/squareup/ui/main/errors/ReaderWarningScreen;->getInitialParameters()Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;->cardReaderId:Lcom/squareup/cardreader/CardReaderId;

    invoke-virtual {v1, v2}, Lcom/squareup/cardreader/CardReaderHub;->getCardReader(Lcom/squareup/cardreader/CardReaderId;)Lcom/squareup/cardreader/CardReader;

    move-result-object v1

    if-eqz v1, :cond_b

    .line 188
    invoke-interface {v1}, Lcom/squareup/cardreader/CardReader;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/cardreader/CardReaderInfo;->isWireless()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 190
    iget-object v2, p0, Lcom/squareup/ui/main/errors/ReaderWarningPresenter;->cardReaderFactory:Lcom/squareup/cardreader/CardReaderFactory;

    invoke-interface {v1}, Lcom/squareup/cardreader/CardReader;->getId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v1

    invoke-interface {v2, v1}, Lcom/squareup/cardreader/CardReaderFactory;->destroy(Lcom/squareup/cardreader/CardReaderId;)V

    .line 191
    invoke-virtual {v0, v3}, Lcom/squareup/ui/main/errors/ReaderWarningView;->showCancelButton(Z)V

    goto :goto_5

    .line 194
    :cond_a
    invoke-virtual {v0, v4}, Lcom/squareup/ui/main/errors/ReaderWarningView;->showCancelButton(Z)V

    goto :goto_5

    .line 197
    :cond_b
    invoke-virtual {v0, v3}, Lcom/squareup/ui/main/errors/ReaderWarningView;->showCancelButton(Z)V

    :cond_c
    :goto_5
    return-void
.end method
