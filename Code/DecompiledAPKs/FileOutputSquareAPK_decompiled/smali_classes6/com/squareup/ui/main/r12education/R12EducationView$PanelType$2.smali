.class final enum Lcom/squareup/ui/main/r12education/R12EducationView$PanelType$2;
.super Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;
.source "R12EducationView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4008
    name = null
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;II)V
    .locals 1

    const/4 v0, 0x0

    .line 99
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;-><init>(Ljava/lang/String;IILcom/squareup/ui/main/r12education/R12EducationView$1;)V

    return-void
.end method


# virtual methods
.method buildContent(Landroid/view/ViewGroup;Lcom/squareup/CountryCode;)Landroid/view/View;
    .locals 2

    .line 101
    sget p2, Lcom/squareup/readertutorial/R$layout;->r12_education_panel_welcome:I

    invoke-static {p2, p1}, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->access$100(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    .line 104
    sget p2, Lcom/squareup/readertutorial/R$id;->r12_education_title:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    .line 105
    sget v0, Lcom/squareup/readertutorial/R$id;->r12_education_message:I

    .line 106
    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    .line 107
    sget v1, Lcom/squareup/readertutorial/R$string;->r12_education_welcome_title_updating:I

    invoke-virtual {p2, v1}, Landroid/widget/TextView;->setText(I)V

    .line 108
    sget p2, Lcom/squareup/readertutorial/R$string;->r12_education_welcome_message_updating:I

    invoke-virtual {v0, p2}, Lcom/squareup/widgets/MessageView;->setText(I)V

    return-object p1
.end method

.method getButtonText()I
    .locals 1

    .line 114
    sget v0, Lcom/squareup/readertutorial/R$string;->r12_education_welcome_button:I

    return v0
.end method

.method getLogDetailFromPanel()Ljava/lang/String;
    .locals 1

    const-string v0, "First Page (Reader Connected)"

    return-object v0
.end method
