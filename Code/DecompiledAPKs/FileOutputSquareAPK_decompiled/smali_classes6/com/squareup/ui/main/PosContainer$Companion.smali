.class public final Lcom/squareup/ui/main/PosContainer$Companion;
.super Ljava/lang/Object;
.source "PosContainer.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/main/PosContainer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPosContainer.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PosContainer.kt\ncom/squareup/ui/main/PosContainer$Companion\n*L\n1#1,153:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u00062\u0006\u0010\u0007\u001a\u00020\u0008J\u001a\u0010\t\u001a\u0004\u0018\u00010\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0006\u0010\n\u001a\u00020\u0004H\u0002J\u0010\u0010\u000b\u001a\u0004\u0018\u00010\u00062\u0006\u0010\u0007\u001a\u00020\u0008R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/ui/main/PosContainer$Companion;",
        "",
        "()V",
        "NULL_DRAWABLE",
        "",
        "getThemeCardBackgroundDrawable",
        "Landroid/graphics/drawable/Drawable;",
        "context",
        "Landroid/content/Context;",
        "getThemeDrawable",
        "attrId",
        "getThemeWindowBackgroundDrawable",
        "pos-container_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field static final synthetic $$INSTANCE:Lcom/squareup/ui/main/PosContainer$Companion;

.field private static final NULL_DRAWABLE:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 119
    new-instance v0, Lcom/squareup/ui/main/PosContainer$Companion;

    invoke-direct {v0}, Lcom/squareup/ui/main/PosContainer$Companion;-><init>()V

    sput-object v0, Lcom/squareup/ui/main/PosContainer$Companion;->$$INSTANCE:Lcom/squareup/ui/main/PosContainer$Companion;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private final getThemeDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
    .locals 3

    .line 134
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, p2, v0, v2}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 136
    iget p2, v0, Landroid/util/TypedValue;->type:I

    const/16 v1, 0x1c

    if-le v1, p2, :cond_0

    goto :goto_0

    :cond_0
    const/16 v1, 0x1f

    if-lt v1, p2, :cond_1

    new-instance p1, Landroid/graphics/drawable/ColorDrawable;

    iget p2, v0, Landroid/util/TypedValue;->data:I

    invoke-direct {p1, p2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    check-cast p1, Landroid/graphics/drawable/Drawable;

    goto :goto_1

    .line 138
    :cond_1
    :goto_0
    iget p2, v0, Landroid/util/TypedValue;->resourceId:I

    if-nez p2, :cond_2

    const/4 p1, 0x0

    goto :goto_1

    .line 139
    :cond_2
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    iget p2, v0, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    :goto_1
    return-object p1
.end method


# virtual methods
.method public final getThemeCardBackgroundDrawable(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;
    .locals 2

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 123
    move-object v0, p0

    check-cast v0, Lcom/squareup/ui/main/PosContainer$Companion;

    sget v1, Lcom/squareup/widgets/R$attr;->marinCardBackground:I

    invoke-direct {v0, p1, v1}, Lcom/squareup/ui/main/PosContainer$Companion;->getThemeDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    return-object p1
.end method

.method public final getThemeWindowBackgroundDrawable(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;
    .locals 2

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 127
    move-object v0, p0

    check-cast v0, Lcom/squareup/ui/main/PosContainer$Companion;

    sget v1, Lcom/squareup/widgets/R$attr;->marinWindowBackground:I

    invoke-direct {v0, p1, v1}, Lcom/squareup/ui/main/PosContainer$Companion;->getThemeDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    return-object p1
.end method
