.class public final Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;
.super Ljava/lang/Object;
.source "ApiMainActivity_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/main/ApiMainActivity;",
        ">;"
    }
.end annotation


# instance fields
.field private final activityResultHandlerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ActivityResultHandler;",
            ">;"
        }
    .end annotation
.end field

.field private final activityVisibilityPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final additionalActivityDelegatesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/squareup/ui/ActivityDelegate;",
            ">;>;"
        }
    .end annotation
.end field

.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final apiAddCardOnFileControllerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiAddCardOnFileController;",
            ">;"
        }
    .end annotation
.end field

.field private final apiReaderSettingsControllerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiReaderSettingsController;",
            ">;"
        }
    .end annotation
.end field

.field private final apiRequestControllerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiRequestController;",
            ">;"
        }
    .end annotation
.end field

.field private final apiTransactionControllerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiTransactionController;",
            ">;"
        }
    .end annotation
.end field

.field private final badKeyboardHiderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/BadKeyboardHider;",
            ">;"
        }
    .end annotation
.end field

.field private final browserLauncherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;"
        }
    .end annotation
.end field

.field private final cameraHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/camerahelper/CameraHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderPauseAndResumerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPauseAndResumer;",
            ">;"
        }
    .end annotation
.end field

.field private final configurationChangeMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/AndroidConfigurationChangeMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final containerActivityDelegateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/container/ContainerActivityDelegate;",
            ">;"
        }
    .end annotation
.end field

.field private final contentViewInitializerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/development/drawer/ContentViewInitializer;",
            ">;"
        }
    .end annotation
.end field

.field private final deepLinkHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/DeepLinkHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider2:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final focusedActivityScannerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/radiography/FocusedActivityScanner;",
            ">;"
        }
    .end annotation
.end field

.field private final internetStatusMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/internet/InternetStatusMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final invoiceShareUrlLauncherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/url/InvoiceShareUrlLauncher;",
            ">;"
        }
    .end annotation
.end field

.field private final locationMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final locationPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/LocationPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final mainContainerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;"
        }
    .end annotation
.end field

.field private final mediaButtonDisablerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/MediaButtonDisabler;",
            ">;"
        }
    .end annotation
.end field

.field private final minesweeperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/Minesweeper;",
            ">;"
        }
    .end annotation
.end field

.field private final nfcStateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/AndroidNfcState;",
            ">;"
        }
    .end annotation
.end field

.field private final offlineModeMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final passcodeEmployeeManagementProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;"
        }
    .end annotation
.end field

.field private final pauseNarcPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pauses/PauseAndResumePresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final permissionsPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final persistentBundleManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/persistentbundle/PersistentBundleManager;",
            ">;"
        }
    .end annotation
.end field

.field private final rootViewSetupProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/rootview/RootViewSetup;",
            ">;"
        }
    .end annotation
.end field

.field private final secureScopeManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/secure/SecureScopeManager;",
            ">;"
        }
    .end annotation
.end field

.field private final softInputPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/SoftInputPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final statusBarHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/StatusBarHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final toastFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/ToastFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final usbDiscovererProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/usb/UsbDiscoverer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/development/drawer/ContentViewInitializer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/radiography/FocusedActivityScanner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPauseAndResumer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/MediaButtonDisabler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/Minesweeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/internet/InternetStatusMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/persistentbundle/PersistentBundleManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ActivityResultHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/AndroidConfigurationChangeMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/container/ContainerActivityDelegate;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/camerahelper/CameraHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/LocationPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pauses/PauseAndResumePresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/SoftInputPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/StatusBarHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/ToastFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiRequestController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/DeepLinkHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/AndroidNfcState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/rootview/RootViewSetup;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/secure/SecureScopeManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/url/InvoiceShareUrlLauncher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/usb/UsbDiscoverer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/squareup/ui/ActivityDelegate;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/BadKeyboardHider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiTransactionController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiAddCardOnFileController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiReaderSettingsController;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 168
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 169
    iput-object v1, v0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->analyticsProvider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 170
    iput-object v1, v0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->contentViewInitializerProvider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 171
    iput-object v1, v0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->locationMonitorProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 172
    iput-object v1, v0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->focusedActivityScannerProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 173
    iput-object v1, v0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->cardReaderPauseAndResumerProvider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 174
    iput-object v1, v0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->mediaButtonDisablerProvider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 175
    iput-object v1, v0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->minesweeperProvider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 176
    iput-object v1, v0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->internetStatusMonitorProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 177
    iput-object v1, v0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->featuresProvider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 178
    iput-object v1, v0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->persistentBundleManagerProvider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 179
    iput-object v1, v0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->deviceProvider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 180
    iput-object v1, v0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->activityResultHandlerProvider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 181
    iput-object v1, v0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->configurationChangeMonitorProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 182
    iput-object v1, v0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->containerActivityDelegateProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 183
    iput-object v1, v0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->cameraHelperProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 184
    iput-object v1, v0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->locationPresenterProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p17

    .line 185
    iput-object v1, v0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->pauseNarcPresenterProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p18

    .line 186
    iput-object v1, v0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->passcodeEmployeeManagementProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p19

    .line 187
    iput-object v1, v0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->activityVisibilityPresenterProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p20

    .line 188
    iput-object v1, v0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->offlineModeMonitorProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p21

    .line 189
    iput-object v1, v0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->permissionsPresenterProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p22

    .line 190
    iput-object v1, v0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->mainContainerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p23

    .line 191
    iput-object v1, v0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->softInputPresenterProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p24

    .line 192
    iput-object v1, v0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->featuresProvider2:Ljavax/inject/Provider;

    move-object/from16 v1, p25

    .line 193
    iput-object v1, v0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->statusBarHelperProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p26

    .line 194
    iput-object v1, v0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->toastFactoryProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p27

    .line 195
    iput-object v1, v0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->apiRequestControllerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p28

    .line 196
    iput-object v1, v0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->deepLinkHelperProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p29

    .line 197
    iput-object v1, v0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->nfcStateProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p30

    .line 198
    iput-object v1, v0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->browserLauncherProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p31

    .line 199
    iput-object v1, v0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->rootViewSetupProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p32

    .line 200
    iput-object v1, v0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->secureScopeManagerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p33

    .line 201
    iput-object v1, v0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->invoiceShareUrlLauncherProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p34

    .line 202
    iput-object v1, v0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->usbDiscovererProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p35

    .line 203
    iput-object v1, v0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->additionalActivityDelegatesProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p36

    .line 204
    iput-object v1, v0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->badKeyboardHiderProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p37

    .line 205
    iput-object v1, v0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->apiTransactionControllerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p38

    .line 206
    iput-object v1, v0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->apiAddCardOnFileControllerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p39

    .line 207
    iput-object v1, v0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->apiReaderSettingsControllerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 41
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/development/drawer/ContentViewInitializer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/radiography/FocusedActivityScanner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderPauseAndResumer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/MediaButtonDisabler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ms/Minesweeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/internet/InternetStatusMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/persistentbundle/PersistentBundleManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ActivityResultHandler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/AndroidConfigurationChangeMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/container/ContainerActivityDelegate;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/camerahelper/CameraHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/LocationPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pauses/PauseAndResumePresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/SoftInputPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/StatusBarHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/ToastFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiRequestController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/DeepLinkHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/AndroidNfcState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/BrowserLauncher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/rootview/RootViewSetup;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/secure/SecureScopeManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/url/InvoiceShareUrlLauncher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/usb/UsbDiscoverer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Set<",
            "Lcom/squareup/ui/ActivityDelegate;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/BadKeyboardHider;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiTransactionController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiAddCardOnFileController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiReaderSettingsController;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/main/ApiMainActivity;",
            ">;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    move-object/from16 v23, p22

    move-object/from16 v24, p23

    move-object/from16 v25, p24

    move-object/from16 v26, p25

    move-object/from16 v27, p26

    move-object/from16 v28, p27

    move-object/from16 v29, p28

    move-object/from16 v30, p29

    move-object/from16 v31, p30

    move-object/from16 v32, p31

    move-object/from16 v33, p32

    move-object/from16 v34, p33

    move-object/from16 v35, p34

    move-object/from16 v36, p35

    move-object/from16 v37, p36

    move-object/from16 v38, p37

    move-object/from16 v39, p38

    .line 247
    new-instance v40, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;

    move-object/from16 v0, v40

    invoke-direct/range {v0 .. v39}, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v40
.end method

.method public static injectApiAddCardOnFileController(Lcom/squareup/ui/main/ApiMainActivity;Lcom/squareup/api/ApiAddCardOnFileController;)V
    .locals 0

    .line 301
    iput-object p1, p0, Lcom/squareup/ui/main/ApiMainActivity;->apiAddCardOnFileController:Lcom/squareup/api/ApiAddCardOnFileController;

    return-void
.end method

.method public static injectApiReaderSettingsController(Lcom/squareup/ui/main/ApiMainActivity;Lcom/squareup/api/ApiReaderSettingsController;)V
    .locals 0

    .line 307
    iput-object p1, p0, Lcom/squareup/ui/main/ApiMainActivity;->apiReaderSettingsController:Lcom/squareup/api/ApiReaderSettingsController;

    return-void
.end method

.method public static injectApiTransactionController(Lcom/squareup/ui/main/ApiMainActivity;Lcom/squareup/api/ApiTransactionController;)V
    .locals 0

    .line 295
    iput-object p1, p0, Lcom/squareup/ui/main/ApiMainActivity;->apiTransactionController:Lcom/squareup/api/ApiTransactionController;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/main/ApiMainActivity;)V
    .locals 1

    .line 251
    iget-object v0, p0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/analytics/Analytics;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectAnalytics(Lcom/squareup/ui/SquareActivity;Lcom/squareup/analytics/Analytics;)V

    .line 252
    iget-object v0, p0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->contentViewInitializerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/development/drawer/ContentViewInitializer;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectContentViewInitializer(Lcom/squareup/ui/SquareActivity;Lcom/squareup/development/drawer/ContentViewInitializer;)V

    .line 253
    iget-object v0, p0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->locationMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectLocationMonitor(Lcom/squareup/ui/SquareActivity;Lcom/squareup/core/location/monitors/ContinuousLocationMonitor;)V

    .line 254
    iget-object v0, p0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->focusedActivityScannerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/radiography/FocusedActivityScanner;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectFocusedActivityScanner(Lcom/squareup/ui/SquareActivity;Lcom/squareup/radiography/FocusedActivityScanner;)V

    .line 255
    iget-object v0, p0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->cardReaderPauseAndResumerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/cardreader/CardReaderPauseAndResumer;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectCardReaderPauseAndResumer(Lcom/squareup/ui/SquareActivity;Lcom/squareup/cardreader/CardReaderPauseAndResumer;)V

    .line 256
    iget-object v0, p0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->mediaButtonDisablerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/MediaButtonDisabler;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectMediaButtonDisabler(Lcom/squareup/ui/SquareActivity;Lcom/squareup/ui/MediaButtonDisabler;)V

    .line 257
    iget-object v0, p0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->minesweeperProvider:Ljavax/inject/Provider;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectMinesweeperProvider(Lcom/squareup/ui/SquareActivity;Ljavax/inject/Provider;)V

    .line 258
    iget-object v0, p0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->internetStatusMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/internet/InternetStatusMonitor;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectInternetStatusMonitor(Lcom/squareup/ui/SquareActivity;Lcom/squareup/internet/InternetStatusMonitor;)V

    .line 259
    iget-object v0, p0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/Features;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectFeatures(Lcom/squareup/ui/SquareActivity;Lcom/squareup/settings/server/Features;)V

    .line 260
    iget-object v0, p0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->persistentBundleManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/persistentbundle/PersistentBundleManager;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectPersistentBundleManager(Lcom/squareup/ui/SquareActivity;Lcom/squareup/persistentbundle/PersistentBundleManager;)V

    .line 261
    iget-object v0, p0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Device;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectDevice(Lcom/squareup/ui/SquareActivity;Lcom/squareup/util/Device;)V

    .line 262
    iget-object v0, p0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->activityResultHandlerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ActivityResultHandler;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectActivityResultHandler(Lcom/squareup/ui/SquareActivity;Lcom/squareup/ui/ActivityResultHandler;)V

    .line 263
    iget-object v0, p0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->configurationChangeMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/AndroidConfigurationChangeMonitor;

    invoke-static {p1, v0}, Lcom/squareup/ui/SquareActivity_MembersInjector;->injectConfigurationChangeMonitor(Lcom/squareup/ui/SquareActivity;Lcom/squareup/util/AndroidConfigurationChangeMonitor;)V

    .line 264
    iget-object v0, p0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->containerActivityDelegateProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/container/ContainerActivityDelegate;

    invoke-static {p1, v0}, Lcom/squareup/ui/main/MainActivity_MembersInjector;->injectContainerActivityDelegate(Lcom/squareup/ui/main/MainActivity;Lcom/squareup/container/ContainerActivityDelegate;)V

    .line 265
    iget-object v0, p0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->cameraHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/camerahelper/CameraHelper;

    invoke-static {p1, v0}, Lcom/squareup/ui/main/MainActivity_MembersInjector;->injectCameraHelper(Lcom/squareup/ui/main/MainActivity;Lcom/squareup/camerahelper/CameraHelper;)V

    .line 266
    iget-object v0, p0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->locationPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/main/LocationPresenter;

    invoke-static {p1, v0}, Lcom/squareup/ui/main/MainActivity_MembersInjector;->injectLocationPresenter(Lcom/squareup/ui/main/MainActivity;Lcom/squareup/ui/main/LocationPresenter;)V

    .line 267
    iget-object v0, p0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->pauseNarcPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/pauses/PauseAndResumePresenter;

    invoke-static {p1, v0}, Lcom/squareup/ui/main/MainActivity_MembersInjector;->injectPauseNarcPresenter(Lcom/squareup/ui/main/MainActivity;Lcom/squareup/pauses/PauseAndResumePresenter;)V

    .line 268
    iget-object v0, p0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->passcodeEmployeeManagementProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-static {p1, v0}, Lcom/squareup/ui/main/MainActivity_MembersInjector;->injectPasscodeEmployeeManagement(Lcom/squareup/ui/main/MainActivity;Lcom/squareup/permissions/PasscodeEmployeeManagement;)V

    .line 269
    iget-object v0, p0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->activityVisibilityPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;

    invoke-static {p1, v0}, Lcom/squareup/ui/main/MainActivity_MembersInjector;->injectActivityVisibilityPresenter(Lcom/squareup/ui/main/MainActivity;Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;)V

    .line 270
    iget-object v0, p0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->offlineModeMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/payment/OfflineModeMonitor;

    invoke-static {p1, v0}, Lcom/squareup/ui/main/MainActivity_MembersInjector;->injectOfflineModeMonitor(Lcom/squareup/ui/main/MainActivity;Lcom/squareup/payment/OfflineModeMonitor;)V

    .line 271
    iget-object v0, p0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->permissionsPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;

    invoke-static {p1, v0}, Lcom/squareup/ui/main/MainActivity_MembersInjector;->injectPermissionsPresenter(Lcom/squareup/ui/main/MainActivity;Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;)V

    .line 272
    iget-object v0, p0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->mainContainerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/main/PosContainer;

    invoke-static {p1, v0}, Lcom/squareup/ui/main/MainActivity_MembersInjector;->injectMainContainer(Lcom/squareup/ui/main/MainActivity;Lcom/squareup/ui/main/PosContainer;)V

    .line 273
    iget-object v0, p0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->softInputPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/SoftInputPresenter;

    invoke-static {p1, v0}, Lcom/squareup/ui/main/MainActivity_MembersInjector;->injectSoftInputPresenter(Lcom/squareup/ui/main/MainActivity;Lcom/squareup/ui/SoftInputPresenter;)V

    .line 274
    iget-object v0, p0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->featuresProvider2:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/Features;

    invoke-static {p1, v0}, Lcom/squareup/ui/main/MainActivity_MembersInjector;->injectFeatures(Lcom/squareup/ui/main/MainActivity;Lcom/squareup/settings/server/Features;)V

    .line 275
    iget-object v0, p0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->statusBarHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/StatusBarHelper;

    invoke-static {p1, v0}, Lcom/squareup/ui/main/MainActivity_MembersInjector;->injectStatusBarHelper(Lcom/squareup/ui/main/MainActivity;Lcom/squareup/ui/StatusBarHelper;)V

    .line 276
    iget-object v0, p0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->toastFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/ToastFactory;

    invoke-static {p1, v0}, Lcom/squareup/ui/main/MainActivity_MembersInjector;->injectToastFactory(Lcom/squareup/ui/main/MainActivity;Lcom/squareup/util/ToastFactory;)V

    .line 277
    iget-object v0, p0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->apiRequestControllerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/ApiRequestController;

    invoke-static {p1, v0}, Lcom/squareup/ui/main/MainActivity_MembersInjector;->injectApiRequestController(Lcom/squareup/ui/main/MainActivity;Lcom/squareup/api/ApiRequestController;)V

    .line 278
    iget-object v0, p0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->deepLinkHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/analytics/DeepLinkHelper;

    invoke-static {p1, v0}, Lcom/squareup/ui/main/MainActivity_MembersInjector;->injectDeepLinkHelper(Lcom/squareup/ui/main/MainActivity;Lcom/squareup/analytics/DeepLinkHelper;)V

    .line 279
    iget-object v0, p0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->nfcStateProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/AndroidNfcState;

    invoke-static {p1, v0}, Lcom/squareup/ui/main/MainActivity_MembersInjector;->injectNfcState(Lcom/squareup/ui/main/MainActivity;Lcom/squareup/ui/AndroidNfcState;)V

    .line 280
    iget-object v0, p0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->browserLauncherProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/BrowserLauncher;

    invoke-static {p1, v0}, Lcom/squareup/ui/main/MainActivity_MembersInjector;->injectBrowserLauncher(Lcom/squareup/ui/main/MainActivity;Lcom/squareup/util/BrowserLauncher;)V

    .line 281
    iget-object v0, p0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->rootViewSetupProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/rootview/RootViewSetup;

    invoke-static {p1, v0}, Lcom/squareup/ui/main/MainActivity_MembersInjector;->injectRootViewSetup(Lcom/squareup/ui/main/MainActivity;Lcom/squareup/rootview/RootViewSetup;)V

    .line 282
    iget-object v0, p0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->secureScopeManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/secure/SecureScopeManager;

    invoke-static {p1, v0}, Lcom/squareup/ui/main/MainActivity_MembersInjector;->injectSecureScopeManager(Lcom/squareup/ui/main/MainActivity;Lcom/squareup/secure/SecureScopeManager;)V

    .line 283
    iget-object v0, p0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->invoiceShareUrlLauncherProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/url/InvoiceShareUrlLauncher;

    invoke-static {p1, v0}, Lcom/squareup/ui/main/MainActivity_MembersInjector;->injectInvoiceShareUrlLauncher(Lcom/squareup/ui/main/MainActivity;Lcom/squareup/url/InvoiceShareUrlLauncher;)V

    .line 284
    iget-object v0, p0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->usbDiscovererProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/usb/UsbDiscoverer;

    invoke-static {p1, v0}, Lcom/squareup/ui/main/MainActivity_MembersInjector;->injectUsbDiscoverer(Lcom/squareup/ui/main/MainActivity;Lcom/squareup/usb/UsbDiscoverer;)V

    .line 285
    iget-object v0, p0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->additionalActivityDelegatesProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-static {p1, v0}, Lcom/squareup/ui/main/MainActivity_MembersInjector;->injectAdditionalActivityDelegates(Lcom/squareup/ui/main/MainActivity;Ljava/util/Set;)V

    .line 286
    iget-object v0, p0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->badKeyboardHiderProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/main/BadKeyboardHider;

    invoke-static {p1, v0}, Lcom/squareup/ui/main/MainActivity_MembersInjector;->injectBadKeyboardHider(Lcom/squareup/ui/main/MainActivity;Lcom/squareup/ui/main/BadKeyboardHider;)V

    .line 287
    iget-object v0, p0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->apiTransactionControllerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/ApiTransactionController;

    invoke-static {p1, v0}, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->injectApiTransactionController(Lcom/squareup/ui/main/ApiMainActivity;Lcom/squareup/api/ApiTransactionController;)V

    .line 288
    iget-object v0, p0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->apiAddCardOnFileControllerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/ApiAddCardOnFileController;

    invoke-static {p1, v0}, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->injectApiAddCardOnFileController(Lcom/squareup/ui/main/ApiMainActivity;Lcom/squareup/api/ApiAddCardOnFileController;)V

    .line 289
    iget-object v0, p0, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->apiReaderSettingsControllerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/ApiReaderSettingsController;

    invoke-static {p1, v0}, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->injectApiReaderSettingsController(Lcom/squareup/ui/main/ApiMainActivity;Lcom/squareup/api/ApiReaderSettingsController;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 45
    check-cast p1, Lcom/squareup/ui/main/ApiMainActivity;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/main/ApiMainActivity_MembersInjector;->injectMembers(Lcom/squareup/ui/main/ApiMainActivity;)V

    return-void
.end method
