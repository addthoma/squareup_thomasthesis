.class public Lcom/squareup/ui/main/SquarePaddedFrameLayout;
.super Landroid/widget/FrameLayout;
.source "SquarePaddedFrameLayout.java"


# instance fields
.field private final device:Lcom/squareup/util/Device;

.field private final paddingBottom:I

.field private final paddingTop:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 22
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    invoke-virtual {p0}, Lcom/squareup/ui/main/SquarePaddedFrameLayout;->getPaddingBottom()I

    move-result p2

    iput p2, p0, Lcom/squareup/ui/main/SquarePaddedFrameLayout;->paddingBottom:I

    .line 24
    invoke-virtual {p0}, Lcom/squareup/ui/main/SquarePaddedFrameLayout;->getPaddingTop()I

    move-result p2

    iput p2, p0, Lcom/squareup/ui/main/SquarePaddedFrameLayout;->paddingTop:I

    .line 26
    invoke-static {p1}, Lcom/squareup/container/ContainerKt;->getDevice(Landroid/content/Context;)Lcom/squareup/util/Device;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/main/SquarePaddedFrameLayout;->device:Lcom/squareup/util/Device;

    return-void
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 4

    .line 30
    iget-object v0, p0, Lcom/squareup/ui/main/SquarePaddedFrameLayout;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isLandscapeLongTablet()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 31
    invoke-virtual {p0, v0, v0, v0, v0}, Lcom/squareup/ui/main/SquarePaddedFrameLayout;->setPadding(IIII)V

    goto :goto_0

    .line 33
    :cond_0
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 34
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    sub-int/2addr v0, v1

    .line 36
    div-int/lit8 v0, v0, 0x2

    .line 37
    iget v1, p0, Lcom/squareup/ui/main/SquarePaddedFrameLayout;->paddingTop:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 38
    iget v2, p0, Lcom/squareup/ui/main/SquarePaddedFrameLayout;->paddingBottom:I

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 39
    invoke-virtual {p0}, Lcom/squareup/ui/main/SquarePaddedFrameLayout;->getPaddingLeft()I

    move-result v2

    invoke-virtual {p0}, Lcom/squareup/ui/main/SquarePaddedFrameLayout;->getPaddingRight()I

    move-result v3

    invoke-virtual {p0, v2, v1, v3, v0}, Lcom/squareup/ui/main/SquarePaddedFrameLayout;->setPadding(IIII)V

    .line 42
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    return-void
.end method
