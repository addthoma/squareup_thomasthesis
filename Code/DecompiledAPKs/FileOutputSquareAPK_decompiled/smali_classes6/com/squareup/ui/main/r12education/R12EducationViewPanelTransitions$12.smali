.class Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$12;
.super Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;
.source "R12EducationViewPanelTransitions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->populateCountryPrefersContactlessCardsTransitions()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;

.field final synthetic val$chargeToTapHand:Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;


# direct methods
.method constructor <init>(Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;)V
    .locals 0

    .line 552
    iput-object p1, p0, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$12;->this$0:Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;

    iput-object p2, p0, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$12;->val$chargeToTapHand:Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;

    invoke-direct {p0}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;-><init>()V

    return-void
.end method


# virtual methods
.method tweenElement(Lcom/squareup/ui/main/r12education/R12EducationView$Element;Landroid/view/View;FLandroid/view/ViewGroup;Landroid/view/View;Landroid/view/ViewGroup;Landroid/view/View;)V
    .locals 9

    move-object v0, p0

    move-object v3, p2

    move-object/from16 v8, p7

    .line 556
    sget-object v1, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$14;->$SwitchMap$com$squareup$ui$main$r12education$R12EducationView$Element:[I

    invoke-virtual {p1}, Lcom/squareup/ui/main/r12education/R12EducationView$Element;->ordinal()I

    move-result v2

    aget v1, v1, v2

    const/4 v2, 0x5

    if-eq v1, v2, :cond_1

    const/4 v2, 0x7

    if-eq v1, v2, :cond_0

    .line 576
    iget-object v1, v0, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$12;->val$chargeToTapHand:Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    move-object/from16 v8, p7

    invoke-virtual/range {v1 .. v8}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$PanelTransition;->tweenElement(Lcom/squareup/ui/main/r12education/R12EducationView$Element;Landroid/view/View;FLandroid/view/ViewGroup;Landroid/view/View;Landroid/view/ViewGroup;Landroid/view/View;)V

    goto :goto_0

    .line 560
    :cond_0
    invoke-static {p2}, Lcom/squareup/ui/main/r12education/Tweens;->hide(Landroid/view/View;)V

    goto :goto_0

    :cond_1
    const/high16 v1, 0x3f000000    # 0.5f

    const/high16 v2, 0x3f800000    # 1.0f

    .line 565
    invoke-static {}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->access$200()Landroid/view/animation/Interpolator;

    move-result-object v4

    move v5, p3

    invoke-static {p3, v1, v2, v4}, Lcom/squareup/ui/main/r12education/Tweens;->clampMap(FFFLandroid/view/animation/Interpolator;)F

    move-result v1

    .line 567
    invoke-static {p2}, Lcom/squareup/ui/main/r12education/Tweens;->show(Landroid/view/View;)V

    .line 568
    iget-object v2, v0, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$12;->this$0:Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;

    invoke-static {v2}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->access$300(Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;)Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$ElementValueHolder;

    move-result-object v2

    iget v2, v2, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$ElementValueHolder;->phoneApplePayBottomMargin:I

    neg-int v2, v2

    invoke-static {p2, v8, v2}, Lcom/squareup/ui/main/r12education/Tweens;->alignBottomEdgeToCenterY(Landroid/view/View;Landroid/view/View;I)V

    .line 569
    iget-object v2, v0, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$12;->this$0:Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;

    .line 570
    invoke-static {v2}, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;->access$300(Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions;)Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$ElementValueHolder;

    move-result-object v2

    iget v2, v2, Lcom/squareup/ui/main/r12education/R12EducationViewPanelTransitions$ElementValueHolder;->phoneApplePayLeftMargin:I

    neg-int v2, v2

    move-object v4, p6

    .line 569
    invoke-static {p2, v8, p6, v2, v1}, Lcom/squareup/ui/main/r12education/Tweens;->slideInRightToAlignEdgeToCenter(Landroid/view/View;Landroid/view/View;Landroid/view/ViewGroup;IF)V

    :goto_0
    return-void
.end method
