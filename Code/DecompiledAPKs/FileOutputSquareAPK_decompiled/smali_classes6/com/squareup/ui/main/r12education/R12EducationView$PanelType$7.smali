.class final enum Lcom/squareup/ui/main/r12education/R12EducationView$PanelType$7;
.super Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;
.source "R12EducationView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4008
    name = null
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;II)V
    .locals 1

    const/4 v0, 0x0

    .line 200
    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;-><init>(Ljava/lang/String;IILcom/squareup/ui/main/r12education/R12EducationView$1;)V

    return-void
.end method


# virtual methods
.method buildContent(Landroid/view/ViewGroup;Lcom/squareup/CountryCode;)Landroid/view/View;
    .locals 1

    .line 202
    sget v0, Lcom/squareup/readertutorial/R$layout;->r12_education_panel_swipe:I

    invoke-static {v0, p1}, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->access$100(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    .line 203
    sget-object v0, Lcom/squareup/ui/main/r12education/R12EducationView$5;->$SwitchMap$com$squareup$CountryCode:[I

    invoke-virtual {p2}, Lcom/squareup/CountryCode;->ordinal()I

    move-result p2

    aget p2, v0, p2

    const/4 v0, 0x1

    if-eq p2, v0, :cond_0

    goto :goto_0

    .line 205
    :cond_0
    sget p2, Lcom/squareup/readertutorial/R$id;->r12_education_message:I

    .line 206
    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/squareup/widgets/MessageView;

    .line 207
    sget v0, Lcom/squareup/readertutorial/R$string;->r12_education_swipe_message_au:I

    invoke-virtual {p2, v0}, Lcom/squareup/widgets/MessageView;->setText(I)V

    .line 208
    sget p2, Lcom/squareup/readertutorial/R$id;->r12_education_title:I

    .line 209
    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/squareup/marketfont/MarketTextView;

    .line 210
    sget v0, Lcom/squareup/readertutorial/R$string;->r12_education_swipe_title_au:I

    invoke-virtual {p2, v0}, Lcom/squareup/marketfont/MarketTextView;->setText(I)V

    :goto_0
    return-object p1
.end method

.method getLogDetailFromPanel()Ljava/lang/String;
    .locals 1

    const-string v0, "Fifth Page (Swipe a Card)"

    return-object v0
.end method
