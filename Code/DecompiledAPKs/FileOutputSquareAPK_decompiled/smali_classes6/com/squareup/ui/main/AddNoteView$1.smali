.class Lcom/squareup/ui/main/AddNoteView$1;
.super Lcom/squareup/debounce/DebouncedOnEditorActionListener;
.source "AddNoteView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/main/AddNoteView;->onAttachedToWindow()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/main/AddNoteView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/main/AddNoteView;)V
    .locals 0

    .line 43
    iput-object p1, p0, Lcom/squareup/ui/main/AddNoteView$1;->this$0:Lcom/squareup/ui/main/AddNoteView;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnEditorActionListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doOnEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 0

    const/4 p1, 0x6

    if-ne p2, p1, :cond_0

    .line 46
    iget-object p1, p0, Lcom/squareup/ui/main/AddNoteView$1;->this$0:Lcom/squareup/ui/main/AddNoteView;

    iget-object p1, p1, Lcom/squareup/ui/main/AddNoteView;->presenter:Lcom/squareup/ui/main/AddNoteScreen$Presenter;

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Lcom/squareup/ui/main/AddNoteScreen$Presenter;->done(Z)V

    return p2

    :cond_0
    const/4 p1, 0x0

    return p1
.end method
