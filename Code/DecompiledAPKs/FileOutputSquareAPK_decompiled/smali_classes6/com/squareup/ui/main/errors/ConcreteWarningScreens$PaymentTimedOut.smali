.class public final Lcom/squareup/ui/main/errors/ConcreteWarningScreens$PaymentTimedOut;
.super Lcom/squareup/ui/main/errors/NoPaymentWarningScreen;
.source "ConcreteWarningScreens.java"


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent$FromFactory;
    value = Lcom/squareup/ui/main/errors/NoPaymentWarningScreen$ComponentFactory;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/main/errors/ConcreteWarningScreens;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PaymentTimedOut"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/main/errors/ConcreteWarningScreens$PaymentTimedOut;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/main/errors/ConcreteWarningScreens$PaymentTimedOut;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 313
    new-instance v0, Lcom/squareup/ui/main/errors/ConcreteWarningScreens$PaymentTimedOut;

    invoke-direct {v0}, Lcom/squareup/ui/main/errors/ConcreteWarningScreens$PaymentTimedOut;-><init>()V

    sput-object v0, Lcom/squareup/ui/main/errors/ConcreteWarningScreens$PaymentTimedOut;->INSTANCE:Lcom/squareup/ui/main/errors/ConcreteWarningScreens$PaymentTimedOut;

    .line 319
    sget-object v0, Lcom/squareup/ui/main/errors/ConcreteWarningScreens$PaymentTimedOut;->INSTANCE:Lcom/squareup/ui/main/errors/ConcreteWarningScreens$PaymentTimedOut;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/main/errors/ConcreteWarningScreens$PaymentTimedOut;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 316
    invoke-direct {p0}, Lcom/squareup/ui/main/errors/NoPaymentWarningScreen;-><init>()V

    return-void
.end method


# virtual methods
.method protected getInitialViewData(Lcom/squareup/util/Res;)Lcom/squareup/ui/main/errors/WarningScreenData;
    .locals 2

    .line 322
    new-instance p1, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;

    invoke-direct {p1}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;-><init>()V

    sget-object v0, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;->DISMISS:Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;

    sget v1, Lcom/squareup/common/strings/R$string;->ok:I

    .line 323
    invoke-virtual {p1, v0, v1}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->defaultButton(Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;I)Lcom/squareup/ui/main/errors/WarningScreenData$Builder;

    move-result-object p1

    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 324
    invoke-virtual {p1, v0}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->glyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/ui/main/errors/WarningScreenData$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/cardreader/ui/R$string;->reader_timed_out_waiting_for_card_title:I

    .line 325
    invoke-virtual {p1, v0}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->titleId(I)Lcom/squareup/ui/main/errors/WarningScreenData$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/cardreader/ui/R$string;->reader_timed_out_waiting_for_card_message:I

    .line 326
    invoke-virtual {p1, v0}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->messageId(I)Lcom/squareup/ui/main/errors/WarningScreenData$Builder;

    move-result-object p1

    .line 327
    invoke-virtual {p1}, Lcom/squareup/ui/main/errors/WarningScreenData$Builder;->build()Lcom/squareup/ui/main/errors/WarningScreenData;

    move-result-object p1

    return-object p1
.end method
