.class public final Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "RealSpeedTestWorkflow.kt"

# interfaces
.implements Lcom/squareup/ui/main/r12education/workflow/SpeedTestWorkflow;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow$SpeedTestWorker;,
        Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lkotlin/Unit;",
        "Lcom/squareup/ui/main/r12education/workflow/SpeedTestState;",
        "Lcom/squareup/ui/main/r12education/workflow/SpeedTestOutput;",
        "Ljava/util/Map<",
        "+",
        "Lcom/squareup/container/ContainerLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/ui/main/r12education/workflow/SpeedTestWorkflow;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealSpeedTestWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealSpeedTestWorkflow.kt\ncom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow\n+ 2 Screen.kt\ncom/squareup/workflow/legacy/ScreenKt\n*L\n1#1,145:1\n149#2,5:146\n*E\n*S KotlinDebug\n*F\n+ 1 RealSpeedTestWorkflow.kt\ncom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow\n*L\n124#1,5:146\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000`\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0010 \n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0018\u0000 !2@\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012*\u0012(\u0012\u0006\u0008\u0001\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\n\u0012\u0006\u0008\u0001\u0012\u00020\u0006`\t0\u00012\u00020\n:\u0002!\"B\u001f\u0008\u0007\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u00a2\u0006\u0002\u0010\u0011J\u0016\u0010\u0012\u001a\u00020\u00132\u000c\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00130\u0015H\u0002J\u001f\u0010\u0016\u001a\u00020\u00032\u0006\u0010\u0017\u001a\u00020\u00022\u0008\u0010\u0018\u001a\u0004\u0018\u00010\u0019H\u0016\u00a2\u0006\u0002\u0010\u001aJW\u0010\u001b\u001a(\u0012\u0006\u0008\u0001\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\n\u0012\u0006\u0008\u0001\u0012\u00020\u0006`\t2\u0006\u0010\u0017\u001a\u00020\u00022\u0006\u0010\u001c\u001a\u00020\u00032\u0012\u0010\u001d\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u001eH\u0016\u00a2\u0006\u0002\u0010\u001fJ\u0010\u0010 \u001a\u00020\u00192\u0006\u0010\u001c\u001a\u00020\u0003H\u0016R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006#"
    }
    d2 = {
        "Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "",
        "Lcom/squareup/ui/main/r12education/workflow/SpeedTestState;",
        "Lcom/squareup/ui/main/r12education/workflow/SpeedTestOutput;",
        "",
        "Lcom/squareup/container/ContainerLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "Lcom/squareup/ui/main/r12education/workflow/SpeedTestWorkflow;",
        "felicaSpeedTestService",
        "Lcom/squareup/server/felica/FelicaSpeedTestService;",
        "speedTestLogger",
        "Lcom/squareup/ui/main/r12education/workflow/SpeedTestLogger;",
        "clock",
        "Lcom/squareup/util/Clock;",
        "(Lcom/squareup/server/felica/FelicaSpeedTestService;Lcom/squareup/ui/main/r12education/workflow/SpeedTestLogger;Lcom/squareup/util/Clock;)V",
        "getAverageDuration",
        "",
        "durations",
        "",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "(Lkotlin/Unit;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/ui/main/r12education/workflow/SpeedTestState;",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "(Lkotlin/Unit;Lcom/squareup/ui/main/r12education/workflow/SpeedTestState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;",
        "snapshotState",
        "Companion",
        "SpeedTestWorker",
        "reader-tutorial_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow$Companion;

.field private static final PING_COUNT:I = 0x5

.field private static final TEST_DURATION_THRESHOLD_MS:J = 0xa0L

.field private static final TEST_TIMEOUT_DURATION_MS:J = 0x7d0L


# instance fields
.field private final clock:Lcom/squareup/util/Clock;

.field private final felicaSpeedTestService:Lcom/squareup/server/felica/FelicaSpeedTestService;

.field private final speedTestLogger:Lcom/squareup/ui/main/r12education/workflow/SpeedTestLogger;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow;->Companion:Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/server/felica/FelicaSpeedTestService;Lcom/squareup/ui/main/r12education/workflow/SpeedTestLogger;Lcom/squareup/util/Clock;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "felicaSpeedTestService"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "speedTestLogger"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clock"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow;->felicaSpeedTestService:Lcom/squareup/server/felica/FelicaSpeedTestService;

    iput-object p2, p0, Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow;->speedTestLogger:Lcom/squareup/ui/main/r12education/workflow/SpeedTestLogger;

    iput-object p3, p0, Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow;->clock:Lcom/squareup/util/Clock;

    return-void
.end method

.method public static final synthetic access$getAverageDuration(Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow;Ljava/util/List;)J
    .locals 0

    .line 40
    invoke-direct {p0, p1}, Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow;->getAverageDuration(Ljava/util/List;)J

    move-result-wide p0

    return-wide p0
.end method

.method public static final synthetic access$getClock$p(Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow;)Lcom/squareup/util/Clock;
    .locals 0

    .line 40
    iget-object p0, p0, Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow;->clock:Lcom/squareup/util/Clock;

    return-object p0
.end method

.method public static final synthetic access$getFelicaSpeedTestService$p(Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow;)Lcom/squareup/server/felica/FelicaSpeedTestService;
    .locals 0

    .line 40
    iget-object p0, p0, Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow;->felicaSpeedTestService:Lcom/squareup/server/felica/FelicaSpeedTestService;

    return-object p0
.end method

.method public static final synthetic access$getSpeedTestLogger$p(Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow;)Lcom/squareup/ui/main/r12education/workflow/SpeedTestLogger;
    .locals 0

    .line 40
    iget-object p0, p0, Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow;->speedTestLogger:Lcom/squareup/ui/main/r12education/workflow/SpeedTestLogger;

    return-object p0
.end method

.method private final getAverageDuration(Ljava/util/List;)J
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;)J"
        }
    .end annotation

    .line 65
    check-cast p1, Ljava/lang/Iterable;

    const/4 v0, 0x1

    invoke-static {p1, v0}, Lkotlin/collections/CollectionsKt;->drop(Ljava/lang/Iterable;I)Ljava/util/List;

    move-result-object p1

    check-cast p1, Ljava/lang/Iterable;

    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->averageOfLong(Ljava/lang/Iterable;)D

    move-result-wide v0

    double-to-long v0, v0

    return-wide v0
.end method


# virtual methods
.method public initialState(Lkotlin/Unit;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/ui/main/r12education/workflow/SpeedTestState;
    .locals 2

    const-string p2, "props"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p1

    new-instance p2, Lcom/squareup/ui/main/r12education/workflow/SpeedTestState$Starting;

    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-direct {p2, p1, v0, v1, v0}, Lcom/squareup/ui/main/r12education/workflow/SpeedTestState$Starting;-><init>(Ljava/util/List;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast p2, Lcom/squareup/ui/main/r12education/workflow/SpeedTestState;

    return-object p2
.end method

.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 40
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow;->initialState(Lkotlin/Unit;Lcom/squareup/workflow/Snapshot;)Lcom/squareup/ui/main/r12education/workflow/SpeedTestState;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 40
    check-cast p1, Lkotlin/Unit;

    check-cast p2, Lcom/squareup/ui/main/r12education/workflow/SpeedTestState;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow;->render(Lkotlin/Unit;Lcom/squareup/ui/main/r12education/workflow/SpeedTestState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lkotlin/Unit;Lcom/squareup/ui/main/r12education/workflow/SpeedTestState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Unit;",
            "Lcom/squareup/ui/main/r12education/workflow/SpeedTestState;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lcom/squareup/ui/main/r12education/workflow/SpeedTestState;",
            "-",
            "Lcom/squareup/ui/main/r12education/workflow/SpeedTestOutput;",
            ">;)",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v8, p3

    const-string v2, "props"

    move-object/from16 v3, p1

    invoke-static {v3, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "state"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "context"

    invoke-static {v8, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    new-instance v9, Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen;

    .line 73
    instance-of v2, v1, Lcom/squareup/ui/main/r12education/workflow/SpeedTestState$Starting;

    if-eqz v2, :cond_0

    .line 74
    sget-object v10, Lcom/squareup/workflow/Worker;->Companion:Lcom/squareup/workflow/Worker$Companion;

    const-wide/16 v11, 0x7d0

    const/4 v13, 0x0

    const/4 v14, 0x2

    const/4 v15, 0x0

    invoke-static/range {v10 .. v15}, Lcom/squareup/workflow/Worker$Companion;->timer$default(Lcom/squareup/workflow/Worker$Companion;JLjava/lang/String;ILjava/lang/Object;)Lcom/squareup/workflow/Worker;

    move-result-object v3

    const/4 v4, 0x0

    sget-object v2, Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow$render$1;->INSTANCE:Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow$render$1;

    move-object v5, v2

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v6, 0x2

    const/4 v7, 0x0

    move-object/from16 v2, p3

    invoke-static/range {v2 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 79
    new-instance v2, Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow$SpeedTestWorker;

    move-object v3, v1

    check-cast v3, Lcom/squareup/ui/main/r12education/workflow/SpeedTestState$Starting;

    invoke-virtual {v3}, Lcom/squareup/ui/main/r12education/workflow/SpeedTestState$Starting;->getWorkerUuid()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow$SpeedTestWorker;-><init>(Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow;Ljava/lang/String;)V

    check-cast v2, Lcom/squareup/workflow/Worker;

    const/4 v3, 0x0

    .line 80
    new-instance v4, Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow$render$2;

    invoke-direct {v4, v0, v1}, Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow$render$2;-><init>(Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow;Lcom/squareup/ui/main/r12education/workflow/SpeedTestState;)V

    check-cast v4, Lkotlin/jvm/functions/Function1;

    const/4 v5, 0x2

    const/4 v6, 0x0

    move-object/from16 v1, p3

    .line 78
    invoke-static/range {v1 .. v6}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)V

    .line 93
    new-instance v1, Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen$ScreenState$StartingSpeedTest;

    .line 94
    sget-object v2, Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow$render$3;->INSTANCE:Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow$render$3;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-interface {v8, v2}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object v2

    .line 93
    invoke-direct {v1, v2}, Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen$ScreenState$StartingSpeedTest;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v1, Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen$ScreenState;

    goto :goto_0

    .line 99
    :cond_0
    instance-of v2, v1, Lcom/squareup/ui/main/r12education/workflow/SpeedTestState$Finished;

    if-eqz v2, :cond_2

    .line 100
    iget-object v2, v0, Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow;->speedTestLogger:Lcom/squareup/ui/main/r12education/workflow/SpeedTestLogger;

    invoke-virtual {v2}, Lcom/squareup/ui/main/r12education/workflow/SpeedTestLogger;->clear()V

    .line 101
    check-cast v1, Lcom/squareup/ui/main/r12education/workflow/SpeedTestState$Finished;

    invoke-virtual {v1}, Lcom/squareup/ui/main/r12education/workflow/SpeedTestState$Finished;->isSuccessful()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 102
    new-instance v1, Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen$ScreenState$Success;

    .line 103
    sget-object v2, Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow$render$4;->INSTANCE:Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow$render$4;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-interface {v8, v2}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object v2

    .line 106
    sget-object v3, Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow$render$5;->INSTANCE:Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow$render$5;

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-interface {v8, v3}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object v3

    .line 102
    invoke-direct {v1, v2, v3}, Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen$ScreenState$Success;-><init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    check-cast v1, Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen$ScreenState;

    goto :goto_0

    .line 111
    :cond_1
    new-instance v1, Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen$ScreenState$Failure;

    .line 112
    sget-object v2, Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow$render$6;->INSTANCE:Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow$render$6;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-interface {v8, v2}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object v2

    .line 115
    sget-object v3, Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow$render$7;->INSTANCE:Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow$render$7;

    check-cast v3, Lkotlin/jvm/functions/Function1;

    invoke-interface {v8, v3}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object v3

    .line 118
    sget-object v4, Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow$render$8;->INSTANCE:Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow$render$8;

    check-cast v4, Lkotlin/jvm/functions/Function1;

    invoke-interface {v8, v4}, Lcom/squareup/workflow/RenderContext;->onEvent(Lkotlin/jvm/functions/Function1;)Lkotlin/jvm/functions/Function1;

    move-result-object v4

    .line 111
    invoke-direct {v1, v2, v3, v4}, Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen$ScreenState$Failure;-><init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    check-cast v1, Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen$ScreenState;

    .line 72
    :goto_0
    invoke-direct {v9, v1}, Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen;-><init>(Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen$ScreenState;)V

    check-cast v9, Lcom/squareup/workflow/legacy/V2Screen;

    .line 147
    new-instance v1, Lcom/squareup/workflow/legacy/Screen;

    .line 148
    const-class v2, Lcom/squareup/ui/main/r12education/workflow/SpeedTestScreen;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    const-string v3, ""

    invoke-static {v2, v3}, Lcom/squareup/workflow/legacy/ScreenKt;->asLegacyScreenKey(Lkotlin/reflect/KClass;Ljava/lang/String;)Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v2

    .line 149
    sget-object v3, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {v3}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object v3

    .line 147
    invoke-direct {v1, v2, v9, v3}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 125
    sget-object v2, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {v1, v2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object v1

    const/4 v2, 0x0

    .line 126
    invoke-static {v1, v2}, Lcom/squareup/workflow/LayeredScreenKt;->withPersistence(Ljava/util/Map;Z)Ljava/util/Map;

    move-result-object v1

    return-object v1

    .line 101
    :cond_2
    new-instance v1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v1
.end method

.method public snapshotState(Lcom/squareup/ui/main/r12education/workflow/SpeedTestState;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 129
    sget-object p1, Lcom/squareup/workflow/Snapshot;->EMPTY:Lcom/squareup/workflow/Snapshot;

    return-object p1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 40
    check-cast p1, Lcom/squareup/ui/main/r12education/workflow/SpeedTestState;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow;->snapshotState(Lcom/squareup/ui/main/r12education/workflow/SpeedTestState;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method
