.class Lcom/squareup/ui/main/CheckoutEntryHandler$2;
.super Lcom/squareup/permissions/PermissionGatekeeper$When;
.source "CheckoutEntryHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/main/CheckoutEntryHandler;->itemSuggestionClicked(Lcom/squareup/librarylist/LibraryListConfiguration$ItemSuggestion;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/main/CheckoutEntryHandler;

.field final synthetic val$itemSuggestion:Lcom/squareup/librarylist/LibraryListConfiguration$ItemSuggestion;


# direct methods
.method constructor <init>(Lcom/squareup/ui/main/CheckoutEntryHandler;Lcom/squareup/librarylist/LibraryListConfiguration$ItemSuggestion;)V
    .locals 0

    .line 600
    iput-object p1, p0, Lcom/squareup/ui/main/CheckoutEntryHandler$2;->this$0:Lcom/squareup/ui/main/CheckoutEntryHandler;

    iput-object p2, p0, Lcom/squareup/ui/main/CheckoutEntryHandler$2;->val$itemSuggestion:Lcom/squareup/librarylist/LibraryListConfiguration$ItemSuggestion;

    invoke-direct {p0}, Lcom/squareup/permissions/PermissionGatekeeper$When;-><init>()V

    return-void
.end method


# virtual methods
.method public success()V
    .locals 5

    .line 602
    iget-object v0, p0, Lcom/squareup/ui/main/CheckoutEntryHandler$2;->this$0:Lcom/squareup/ui/main/CheckoutEntryHandler;

    invoke-static {v0}, Lcom/squareup/ui/main/CheckoutEntryHandler;->access$000(Lcom/squareup/ui/main/CheckoutEntryHandler;)Lcom/squareup/ui/items/EditItemGateway;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/main/CheckoutEntryHandler$2;->val$itemSuggestion:Lcom/squareup/librarylist/LibraryListConfiguration$ItemSuggestion;

    .line 603
    invoke-virtual {v1}, Lcom/squareup/librarylist/LibraryListConfiguration$ItemSuggestion;->getIconText()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/main/CheckoutEntryHandler$2;->val$itemSuggestion:Lcom/squareup/librarylist/LibraryListConfiguration$ItemSuggestion;

    .line 604
    invoke-virtual {v2}, Lcom/squareup/librarylist/LibraryListConfiguration$ItemSuggestion;->getName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/main/CheckoutEntryHandler$2;->val$itemSuggestion:Lcom/squareup/librarylist/LibraryListConfiguration$ItemSuggestion;

    .line 605
    invoke-virtual {v3}, Lcom/squareup/librarylist/LibraryListConfiguration$ItemSuggestion;->getAmount()J

    move-result-wide v3

    .line 602
    invoke-interface {v0, v1, v2, v3, v4}, Lcom/squareup/ui/items/EditItemGateway;->startEditNewItemFlowWithNameAndAmount(Ljava/lang/String;Ljava/lang/String;J)V

    return-void
.end method
