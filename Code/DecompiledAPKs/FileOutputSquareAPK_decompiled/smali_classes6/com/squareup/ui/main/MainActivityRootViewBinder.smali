.class Lcom/squareup/ui/main/MainActivityRootViewBinder;
.super Ljava/lang/Object;
.source "MainActivityRootViewBinder.java"

# interfaces
.implements Lcom/squareup/rootview/RootViewBinder;


# instance fields
.field private final impersonatingBanner:Lcom/squareup/ui/main/ImpersonatingBanner;

.field final mainContainer:Lcom/squareup/ui/main/MainActivityContainer;

.field private final printErrorPopupViewBinder:Lcom/squareup/ui/main/PrintErrorPopupViewBinder;

.field private storeAndForwardPopup:Lcom/squareup/caller/ProgressPopup;

.field private final storedPaymentRunner:Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/main/ImpersonatingBanner;Lcom/squareup/ui/main/PrintErrorPopupViewBinder;Lcom/squareup/ui/main/MainActivityContainer;Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/ui/main/MainActivityRootViewBinder;->impersonatingBanner:Lcom/squareup/ui/main/ImpersonatingBanner;

    .line 28
    iput-object p2, p0, Lcom/squareup/ui/main/MainActivityRootViewBinder;->printErrorPopupViewBinder:Lcom/squareup/ui/main/PrintErrorPopupViewBinder;

    .line 29
    iput-object p3, p0, Lcom/squareup/ui/main/MainActivityRootViewBinder;->mainContainer:Lcom/squareup/ui/main/MainActivityContainer;

    .line 30
    iput-object p4, p0, Lcom/squareup/ui/main/MainActivityRootViewBinder;->storedPaymentRunner:Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner;

    return-void
.end method

.method private attachPopups(Landroid/content/Context;)V
    .locals 2

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityRootViewBinder;->impersonatingBanner:Lcom/squareup/ui/main/ImpersonatingBanner;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/main/ImpersonatingBanner;->onAttached(Landroid/content/Context;)V

    .line 45
    new-instance v0, Lcom/squareup/caller/ProgressPopup;

    invoke-direct {v0, p1}, Lcom/squareup/caller/ProgressPopup;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/squareup/ui/main/MainActivityRootViewBinder;->storeAndForwardPopup:Lcom/squareup/caller/ProgressPopup;

    .line 47
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityRootViewBinder;->storedPaymentRunner:Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner;

    iget-object v0, v0, Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner;->showsProgress:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    iget-object v1, p0, Lcom/squareup/ui/main/MainActivityRootViewBinder;->storeAndForwardPopup:Lcom/squareup/caller/ProgressPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->takeView(Ljava/lang/Object;)V

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityRootViewBinder;->printErrorPopupViewBinder:Lcom/squareup/ui/main/PrintErrorPopupViewBinder;

    invoke-interface {v0, p1}, Lcom/squareup/ui/main/PrintErrorPopupViewBinder;->attachPrintErrorPopup(Landroid/content/Context;)V

    return-void
.end method

.method private detachPopups()V
    .locals 2

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityRootViewBinder;->storedPaymentRunner:Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner;

    iget-object v0, v0, Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner;->showsProgress:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    iget-object v1, p0, Lcom/squareup/ui/main/MainActivityRootViewBinder;->storeAndForwardPopup:Lcom/squareup/caller/ProgressPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->dropView(Lcom/squareup/mortar/Popup;)V

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityRootViewBinder;->printErrorPopupViewBinder:Lcom/squareup/ui/main/PrintErrorPopupViewBinder;

    invoke-interface {v0}, Lcom/squareup/ui/main/PrintErrorPopupViewBinder;->detachPrintErrorPopup()V

    return-void
.end method


# virtual methods
.method public onAttached(Lcom/squareup/rootview/RootView;)V
    .locals 1

    .line 34
    invoke-virtual {p1}, Lcom/squareup/rootview/RootView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/ui/main/MainActivityRootViewBinder;->attachPopups(Landroid/content/Context;)V

    .line 35
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityRootViewBinder;->storedPaymentRunner:Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner;

    invoke-virtual {v0, p1}, Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public onDetached(Lcom/squareup/rootview/RootView;)V
    .locals 1

    .line 39
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityRootViewBinder;->storedPaymentRunner:Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner;

    invoke-virtual {v0, p1}, Lcom/squareup/payment/offline/EnqueueStoredPaymentRunner;->dropView(Ljava/lang/Object;)V

    .line 40
    invoke-direct {p0}, Lcom/squareup/ui/main/MainActivityRootViewBinder;->detachPopups()V

    return-void
.end method
