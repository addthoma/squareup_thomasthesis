.class public Lcom/squareup/ui/main/r12education/Tweens;
.super Ljava/lang/Object;
.source "Tweens.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static alignBottomEdgeToBottomY(Landroid/view/View;Landroid/view/View;)V
    .locals 1

    .line 65
    invoke-virtual {p1}, Landroid/view/View;->getBottom()I

    move-result p1

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v0

    sub-int/2addr p1, v0

    int-to-float p1, p1

    invoke-virtual {p0, p1}, Landroid/view/View;->setY(F)V

    return-void
.end method

.method static alignBottomEdgeToCenterY(Landroid/view/View;Landroid/view/View;I)V
    .locals 1

    .line 59
    invoke-virtual {p1}, Landroid/view/View;->getBottom()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result p1

    div-int/lit8 p1, p1, 0x2

    sub-int/2addr v0, p1

    int-to-float p1, v0

    .line 60
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-float v0, v0

    sub-float/2addr p1, v0

    int-to-float p2, p2

    sub-float/2addr p1, p2

    .line 61
    invoke-virtual {p0, p1}, Landroid/view/View;->setY(F)V

    return-void
.end method

.method static centerX(Landroid/view/View;Landroid/view/View;)V
    .locals 1

    .line 69
    invoke-virtual {p1}, Landroid/view/View;->getRight()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result p1

    div-int/lit8 p1, p1, 0x2

    sub-int/2addr v0, p1

    int-to-float p1, v0

    .line 70
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    sub-float/2addr p1, v0

    .line 71
    invoke-virtual {p0, p1}, Landroid/view/View;->setX(F)V

    return-void
.end method

.method static centerY(Landroid/view/View;Landroid/view/View;)V
    .locals 1

    .line 53
    invoke-virtual {p1}, Landroid/view/View;->getBottom()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result p1

    div-int/lit8 p1, p1, 0x2

    sub-int/2addr v0, p1

    int-to-float p1, v0

    .line 54
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    sub-float/2addr p1, v0

    .line 55
    invoke-virtual {p0, p1}, Landroid/view/View;->setY(F)V

    return-void
.end method

.method static clampMap(FFF)F
    .locals 2

    cmpg-float v0, p0, p1

    if-gez v0, :cond_0

    const/4 p0, 0x0

    return p0

    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float v1, p0, p2

    if-lez v1, :cond_1

    return v0

    :cond_1
    sub-float/2addr p2, p1

    div-float/2addr v0, p2

    mul-float p0, p0, v0

    mul-float v0, v0, p1

    sub-float/2addr p0, v0

    return p0
.end method

.method static clampMap(FFFLandroid/view/animation/Interpolator;)F
    .locals 0

    .line 33
    invoke-static {p0, p1, p2}, Lcom/squareup/ui/main/r12education/Tweens;->clampMap(FFF)F

    move-result p0

    invoke-interface {p3, p0}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result p0

    return p0
.end method

.method static fadeIn(Landroid/view/View;F)V
    .locals 0

    .line 45
    invoke-virtual {p0, p1}, Landroid/view/View;->setAlpha(F)V

    return-void
.end method

.method static fadeOut(Landroid/view/View;F)V
    .locals 1

    const/high16 v0, 0x3f800000    # 1.0f

    sub-float/2addr v0, p1

    .line 49
    invoke-virtual {p0, v0}, Landroid/view/View;->setAlpha(F)V

    return-void
.end method

.method static hide(Landroid/view/View;)V
    .locals 1

    const/4 v0, 0x0

    .line 41
    invoke-virtual {p0, v0}, Landroid/view/View;->setAlpha(F)V

    return-void
.end method

.method static rotate90(Landroid/view/View;F)V
    .locals 1

    const/high16 v0, 0x42b40000    # 90.0f

    mul-float p1, p1, v0

    .line 93
    invoke-virtual {p0, p1}, Landroid/view/View;->setRotation(F)V

    return-void
.end method

.method static show(Landroid/view/View;)V
    .locals 1

    const/high16 v0, 0x3f800000    # 1.0f

    .line 37
    invoke-virtual {p0, v0}, Landroid/view/View;->setAlpha(F)V

    return-void
.end method

.method static slideInLeftToAlignEdgeToCenter(Landroid/view/View;Landroid/view/View;IF)V
    .locals 1

    .line 138
    invoke-virtual {p1}, Landroid/view/View;->getRight()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result p1

    div-int/lit8 p1, p1, 0x2

    sub-int/2addr v0, p1

    int-to-float p1, v0

    .line 139
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v0

    neg-int v0, v0

    int-to-float v0, v0

    int-to-float p2, p2

    sub-float/2addr p1, p2

    mul-float p1, p1, p3

    add-float/2addr v0, p1

    invoke-virtual {p0, v0}, Landroid/view/View;->setX(F)V

    return-void
.end method

.method static slideInRightToAlignCenter(Landroid/view/View;Landroid/view/View;Landroid/view/ViewGroup;IF)V
    .locals 1

    const/high16 v0, 0x3f800000    # 1.0f

    sub-float/2addr v0, p4

    .line 160
    invoke-static {p0, p1, p2, p3, v0}, Lcom/squareup/ui/main/r12education/Tweens;->slideOutRightFromAlignCenter(Landroid/view/View;Landroid/view/View;Landroid/view/ViewGroup;IF)V

    return-void
.end method

.method static slideInRightToAlignEdgeToCenter(Landroid/view/View;Landroid/view/View;Landroid/view/ViewGroup;IF)V
    .locals 1

    const/high16 v0, 0x3f800000    # 1.0f

    sub-float/2addr v0, p4

    .line 116
    invoke-static {p0, p1, p2, p3, v0}, Lcom/squareup/ui/main/r12education/Tweens;->slideOutRightFromAlignEdgeToCenter(Landroid/view/View;Landroid/view/View;Landroid/view/ViewGroup;IF)V

    return-void
.end method

.method static slideInToAlignBottom(Landroid/view/View;Landroid/view/View;IF)V
    .locals 1

    .line 98
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v0

    neg-int v0, v0

    int-to-float v0, v0

    .line 99
    invoke-virtual {p1}, Landroid/view/View;->getBottom()I

    move-result p1

    sub-int/2addr p1, p2

    int-to-float p1, p1

    mul-float p1, p1, p3

    add-float/2addr v0, p1

    invoke-virtual {p0, v0}, Landroid/view/View;->setY(F)V

    return-void
.end method

.method static slideOutFromAlignBottom(Landroid/view/View;Landroid/view/View;IF)V
    .locals 1

    const/high16 v0, 0x3f800000    # 1.0f

    sub-float/2addr v0, p3

    .line 104
    invoke-static {p0, p1, p2, v0}, Lcom/squareup/ui/main/r12education/Tweens;->slideInToAlignBottom(Landroid/view/View;Landroid/view/View;IF)V

    return-void
.end method

.method static slideOutLeftFromAlignCenter(Landroid/view/View;Landroid/view/View;IF)V
    .locals 1

    .line 186
    invoke-virtual {p1}, Landroid/view/View;->getRight()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result p1

    div-int/lit8 p1, p1, 0x2

    sub-int/2addr v0, p1

    int-to-float p1, v0

    .line 187
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    sub-float/2addr p1, v0

    int-to-float p2, p2

    sub-float/2addr p1, p2

    .line 189
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result p2

    int-to-float p2, p2

    add-float/2addr p2, p1

    mul-float p2, p2, p3

    sub-float/2addr p1, p2

    invoke-virtual {p0, p1}, Landroid/view/View;->setX(F)V

    return-void
.end method

.method static slideOutLeftFromAlignEdgeToCenter(Landroid/view/View;Landroid/view/View;IF)V
    .locals 1

    const/high16 v0, 0x3f800000    # 1.0f

    sub-float/2addr v0, p3

    .line 150
    invoke-static {p0, p1, p2, v0}, Lcom/squareup/ui/main/r12education/Tweens;->slideInLeftToAlignEdgeToCenter(Landroid/view/View;Landroid/view/View;IF)V

    return-void
.end method

.method static slideOutRightFromAlignCenter(Landroid/view/View;Landroid/view/View;Landroid/view/ViewGroup;IF)V
    .locals 1

    .line 171
    invoke-virtual {p1}, Landroid/view/View;->getRight()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result p1

    div-int/lit8 p1, p1, 0x2

    sub-int/2addr v0, p1

    int-to-float p1, v0

    .line 172
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    sub-float/2addr p1, v0

    int-to-float p3, p3

    add-float/2addr p1, p3

    .line 174
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getWidth()I

    move-result p2

    int-to-float p2, p2

    sub-float/2addr p2, p1

    mul-float p2, p2, p4

    add-float/2addr p1, p2

    .line 175
    invoke-virtual {p0, p1}, Landroid/view/View;->setX(F)V

    return-void
.end method

.method static slideOutRightFromAlignEdgeToCenter(Landroid/view/View;Landroid/view/View;Landroid/view/ViewGroup;IF)V
    .locals 1

    .line 126
    invoke-virtual {p1}, Landroid/view/View;->getRight()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result p1

    div-int/lit8 p1, p1, 0x2

    sub-int/2addr v0, p1

    int-to-float p1, v0

    .line 127
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getWidth()I

    move-result p2

    int-to-float p3, p3

    add-float/2addr p1, p3

    int-to-float p2, p2

    sub-float/2addr p2, p1

    mul-float p2, p2, p4

    add-float/2addr p1, p2

    .line 129
    invoke-virtual {p0, p1}, Landroid/view/View;->setX(F)V

    return-void
.end method

.method static translateCenterX(Landroid/view/View;Landroid/view/View;Landroid/view/View;F)V
    .locals 1

    .line 85
    invoke-virtual {p1}, Landroid/view/View;->getRight()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result p1

    div-int/lit8 p1, p1, 0x2

    sub-int/2addr v0, p1

    int-to-float p1, v0

    .line 86
    invoke-virtual {p2}, Landroid/view/View;->getRight()I

    move-result v0

    invoke-virtual {p2}, Landroid/view/View;->getWidth()I

    move-result p2

    div-int/lit8 p2, p2, 0x2

    sub-int/2addr v0, p2

    int-to-float p2, v0

    .line 87
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    sub-float/2addr p2, p1

    int-to-float v0, v0

    sub-float/2addr p1, v0

    mul-float p2, p2, p3

    add-float/2addr p1, p2

    .line 89
    invoke-virtual {p0, p1}, Landroid/view/View;->setX(F)V

    return-void
.end method

.method static translateCenterY(Landroid/view/View;Landroid/view/View;Landroid/view/View;F)V
    .locals 1

    .line 76
    invoke-virtual {p1}, Landroid/view/View;->getBottom()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result p1

    div-int/lit8 p1, p1, 0x2

    sub-int/2addr v0, p1

    int-to-float p1, v0

    .line 77
    invoke-virtual {p2}, Landroid/view/View;->getBottom()I

    move-result v0

    invoke-virtual {p2}, Landroid/view/View;->getHeight()I

    move-result p2

    div-int/lit8 p2, p2, 0x2

    sub-int/2addr v0, p2

    int-to-float p2, v0

    .line 78
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    sub-float/2addr p2, p1

    int-to-float v0, v0

    sub-float/2addr p1, v0

    mul-float p2, p2, p3

    add-float/2addr p1, p2

    .line 80
    invoke-virtual {p0, p1}, Landroid/view/View;->setY(F)V

    return-void
.end method
