.class public Lcom/squareup/ui/main/CurrencyTileDrawable;
.super Landroid/graphics/drawable/Drawable;
.source "CurrencyTileDrawable.java"


# instance fields
.field private final background:Landroid/graphics/drawable/NinePatchDrawable;

.field private final currencySymbol:Ljava/lang/String;

.field private final currencySymbolPaint:Landroid/text/TextPaint;

.field private final dimension:I

.field private final textSize:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 2

    .line 28
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 29
    iput-object p2, p0, Lcom/squareup/ui/main/CurrencyTileDrawable;->currencySymbol:Ljava/lang/String;

    .line 30
    iput p3, p0, Lcom/squareup/ui/main/CurrencyTileDrawable;->dimension:I

    int-to-float p2, p3

    const v0, 0x3f333333    # 0.7f

    mul-float p2, p2, v0

    .line 31
    iput p2, p0, Lcom/squareup/ui/main/CurrencyTileDrawable;->textSize:F

    .line 32
    new-instance p2, Landroid/text/TextPaint;

    const/16 v0, 0x81

    invoke-direct {p2, v0}, Landroid/text/TextPaint;-><init>(I)V

    iput-object p2, p0, Lcom/squareup/ui/main/CurrencyTileDrawable;->currencySymbolPaint:Landroid/text/TextPaint;

    .line 34
    iget-object p2, p0, Lcom/squareup/ui/main/CurrencyTileDrawable;->currencySymbolPaint:Landroid/text/TextPaint;

    sget-object v0, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    const/4 v1, 0x0

    invoke-static {p1, v0, v1, v1}, Lcom/squareup/marketfont/MarketTypeface;->getTypeface(Landroid/content/Context;Lcom/squareup/marketfont/MarketFont$Weight;ZZ)Landroid/graphics/Typeface;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 35
    iget-object p2, p0, Lcom/squareup/ui/main/CurrencyTileDrawable;->currencySymbolPaint:Landroid/text/TextPaint;

    iget v0, p0, Lcom/squareup/ui/main/CurrencyTileDrawable;->textSize:F

    invoke-virtual {p2, v0}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 36
    iget-object p2, p0, Lcom/squareup/ui/main/CurrencyTileDrawable;->currencySymbolPaint:Landroid/text/TextPaint;

    sget-object v0, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {p2, v0}, Landroid/text/TextPaint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 37
    iget-object p2, p0, Lcom/squareup/ui/main/CurrencyTileDrawable;->currencySymbolPaint:Landroid/text/TextPaint;

    .line 38
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 37
    invoke-virtual {p2, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 39
    iget-object p2, p0, Lcom/squareup/ui/main/CurrencyTileDrawable;->currencySymbolPaint:Landroid/text/TextPaint;

    invoke-static {p2}, Lcom/squareup/marketfont/MarketUtils;->configureOptimalPaintFlags(Landroid/graphics/Paint;)V

    .line 42
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget p2, Lcom/squareup/marin/R$drawable;->marin_custom_tile:I

    invoke-static {p1, p2, p3}, Lcom/squareup/util/Views;->get9PatchSized(Landroid/content/res/Resources;II)Landroid/graphics/drawable/NinePatchDrawable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/main/CurrencyTileDrawable;->background:Landroid/graphics/drawable/NinePatchDrawable;

    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 6

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/main/CurrencyTileDrawable;->background:Landroid/graphics/drawable/NinePatchDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 50
    iget v0, p0, Lcom/squareup/ui/main/CurrencyTileDrawable;->dimension:I

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    .line 52
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 53
    iget-object v2, p0, Lcom/squareup/ui/main/CurrencyTileDrawable;->currencySymbolPaint:Landroid/text/TextPaint;

    iget-object v3, p0, Lcom/squareup/ui/main/CurrencyTileDrawable;->currencySymbol:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v5, v4, v1}, Landroid/text/TextPaint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 54
    iget v1, p0, Lcom/squareup/ui/main/CurrencyTileDrawable;->dimension:I

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    iget-object v2, p0, Lcom/squareup/ui/main/CurrencyTileDrawable;->currencySymbolPaint:Landroid/text/TextPaint;

    .line 56
    invoke-virtual {v2}, Landroid/text/TextPaint;->descent()F

    move-result v2

    iget-object v3, p0, Lcom/squareup/ui/main/CurrencyTileDrawable;->currencySymbolPaint:Landroid/text/TextPaint;

    invoke-virtual {v3}, Landroid/text/TextPaint;->ascent()F

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    add-float/2addr v2, v3

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/squareup/ui/main/CurrencyTileDrawable;->textSize:F

    const v3, 0x3d7ae148    # 0.06125f

    mul-float v2, v2, v3

    add-float/2addr v1, v2

    .line 61
    iget-object v2, p0, Lcom/squareup/ui/main/CurrencyTileDrawable;->currencySymbol:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/ui/main/CurrencyTileDrawable;->currencySymbolPaint:Landroid/text/TextPaint;

    invoke-virtual {p1, v2, v0, v1, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    return-void
.end method

.method public getIntrinsicHeight()I
    .locals 1

    .line 78
    iget v0, p0, Lcom/squareup/ui/main/CurrencyTileDrawable;->dimension:I

    return v0
.end method

.method public getIntrinsicWidth()I
    .locals 1

    .line 74
    iget v0, p0, Lcom/squareup/ui/main/CurrencyTileDrawable;->dimension:I

    return v0
.end method

.method public getOpacity()I
    .locals 1

    const/4 v0, -0x3

    return v0
.end method

.method public setAlpha(I)V
    .locals 0

    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/main/CurrencyTileDrawable;->currencySymbolPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, p1}, Landroid/text/TextPaint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/main/CurrencyTileDrawable;->background:Landroid/graphics/drawable/NinePatchDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/NinePatchDrawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    return-void
.end method
