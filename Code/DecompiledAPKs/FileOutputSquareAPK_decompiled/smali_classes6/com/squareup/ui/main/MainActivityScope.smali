.class public final Lcom/squareup/ui/main/MainActivityScope;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "MainActivityScope.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/main/MainActivityScope$View;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/main/MainActivityScope;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/main/MainActivityScope;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 6
    new-instance v0, Lcom/squareup/ui/main/MainActivityScope;

    invoke-direct {v0}, Lcom/squareup/ui/main/MainActivityScope;-><init>()V

    sput-object v0, Lcom/squareup/ui/main/MainActivityScope;->INSTANCE:Lcom/squareup/ui/main/MainActivityScope;

    .line 22
    sget-object v0, Lcom/squareup/ui/main/MainActivityScope;->INSTANCE:Lcom/squareup/ui/main/MainActivityScope;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/main/MainActivityScope;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    return-void
.end method
