.class public abstract Lcom/squareup/ui/main/errors/NfcWarningScreenHandler;
.super Lcom/squareup/ui/main/errors/ReaderWarningScreenHandler;
.source "NfcWarningScreenHandler.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final apiTransactionState:Lcom/squareup/api/ApiTransactionState;

.field private final hudToaster:Lcom/squareup/hudtoaster/HudToaster;

.field private final nfcProcessor:Lcom/squareup/ui/NfcProcessorInterface;

.field private final orderEntryAppletGateway:Lcom/squareup/orderentry/OrderEntryAppletGateway;

.field private final tenderStarter:Lcom/squareup/ui/tender/TenderStarter;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method public constructor <init>(Lcom/squareup/orderentry/OrderEntryAppletGateway;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/ui/NfcProcessorInterface;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/payment/Transaction;Lcom/squareup/api/ApiTransactionState;)V
    .locals 0

    .line 31
    invoke-direct {p0}, Lcom/squareup/ui/main/errors/ReaderWarningScreenHandler;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/squareup/ui/main/errors/NfcWarningScreenHandler;->orderEntryAppletGateway:Lcom/squareup/orderentry/OrderEntryAppletGateway;

    .line 33
    iput-object p2, p0, Lcom/squareup/ui/main/errors/NfcWarningScreenHandler;->tenderStarter:Lcom/squareup/ui/tender/TenderStarter;

    .line 34
    iput-object p3, p0, Lcom/squareup/ui/main/errors/NfcWarningScreenHandler;->nfcProcessor:Lcom/squareup/ui/NfcProcessorInterface;

    .line 35
    iput-object p4, p0, Lcom/squareup/ui/main/errors/NfcWarningScreenHandler;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    .line 36
    iput-object p5, p0, Lcom/squareup/ui/main/errors/NfcWarningScreenHandler;->transaction:Lcom/squareup/payment/Transaction;

    .line 37
    iput-object p6, p0, Lcom/squareup/ui/main/errors/NfcWarningScreenHandler;->apiTransactionState:Lcom/squareup/api/ApiTransactionState;

    return-void
.end method


# virtual methods
.method public from(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method protected goBackButton()Lcom/squareup/cardreader/ui/api/ButtonDescriptor;
    .locals 3

    .line 56
    new-instance v0, Lcom/squareup/cardreader/ui/api/ButtonDescriptor;

    sget v1, Lcom/squareup/common/strings/R$string;->ok:I

    new-instance v2, Lcom/squareup/ui/main/errors/NfcWarningScreenHandler$1;

    invoke-direct {v2, p0}, Lcom/squareup/ui/main/errors/NfcWarningScreenHandler$1;-><init>(Lcom/squareup/ui/main/errors/NfcWarningScreenHandler;)V

    invoke-direct {v0, v1, v2}, Lcom/squareup/cardreader/ui/api/ButtonDescriptor;-><init>(ILcom/squareup/debounce/DebouncedOnClickListener;)V

    return-object v0
.end method

.method public handleBackPressed()Z
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/main/errors/NfcWarningScreenHandler;->nfcProcessor:Lcom/squareup/ui/NfcProcessorInterface;

    invoke-interface {v0}, Lcom/squareup/ui/NfcProcessorInterface;->cancelPaymentOnAllContactlessReaders()V

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/main/errors/NfcWarningScreenHandler;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->isTakingPaymentFromInvoice()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/main/errors/NfcWarningScreenHandler;->apiTransactionState:Lcom/squareup/api/ApiTransactionState;

    .line 47
    invoke-virtual {v0}, Lcom/squareup/api/ApiTransactionState;->isApiTransactionRequest()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 50
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/main/errors/NfcWarningScreenHandler;->orderEntryAppletGateway:Lcom/squareup/orderentry/OrderEntryAppletGateway;

    invoke-interface {v0}, Lcom/squareup/orderentry/OrderEntryAppletGateway;->activateApplet()V

    goto :goto_1

    .line 48
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/main/errors/NfcWarningScreenHandler;->tenderStarter:Lcom/squareup/ui/tender/TenderStarter;

    invoke-interface {v0}, Lcom/squareup/ui/tender/TenderStarter;->startTenderFlow()V

    :goto_1
    const/4 v0, 0x1

    return v0
.end method
