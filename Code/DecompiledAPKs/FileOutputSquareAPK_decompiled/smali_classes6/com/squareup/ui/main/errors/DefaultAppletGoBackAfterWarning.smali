.class public final Lcom/squareup/ui/main/errors/DefaultAppletGoBackAfterWarning;
.super Lcom/squareup/ui/main/errors/AbstractGoBackAfterWarning;
.source "DefaultAppletGoBackAfterWarning.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u00020\u0001B?\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u00a2\u0006\u0002\u0010\u0010J\u0008\u0010\u0011\u001a\u00020\u0012H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/ui/main/errors/DefaultAppletGoBackAfterWarning;",
        "Lcom/squareup/ui/main/errors/AbstractGoBackAfterWarning;",
        "container",
        "Lcom/squareup/ui/main/PosContainer;",
        "transaction",
        "Lcom/squareup/payment/Transaction;",
        "paymentProcessingEventSink",
        "Lcom/squareup/checkoutflow/PaymentProcessingEventSink;",
        "apiTransactionState",
        "Lcom/squareup/api/ApiTransactionState;",
        "tenderStarter",
        "Lcom/squareup/ui/tender/TenderStarter;",
        "apiReaderSettingsController",
        "Lcom/squareup/api/ApiReaderSettingsController;",
        "readerStatusMonitor",
        "Lcom/squareup/ui/main/ReaderStatusMonitor;",
        "(Lcom/squareup/ui/main/PosContainer;Lcom/squareup/payment/Transaction;Lcom/squareup/checkoutflow/PaymentProcessingEventSink;Lcom/squareup/api/ApiTransactionState;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/api/ApiReaderSettingsController;Lcom/squareup/ui/main/ReaderStatusMonitor;)V",
        "goBack",
        "",
        "cardreader-ui_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final container:Lcom/squareup/ui/main/PosContainer;

.field private final paymentProcessingEventSink:Lcom/squareup/checkoutflow/PaymentProcessingEventSink;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/main/PosContainer;Lcom/squareup/payment/Transaction;Lcom/squareup/checkoutflow/PaymentProcessingEventSink;Lcom/squareup/api/ApiTransactionState;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/api/ApiReaderSettingsController;Lcom/squareup/ui/main/ReaderStatusMonitor;)V
    .locals 7
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "container"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transaction"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentProcessingEventSink"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "apiTransactionState"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tenderStarter"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "apiReaderSettingsController"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "readerStatusMonitor"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v0, p0

    move-object v1, p4

    move-object v2, p2

    move-object v3, p5

    move-object v4, p6

    move-object v5, p7

    move-object v6, p3

    .line 28
    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/main/errors/AbstractGoBackAfterWarning;-><init>(Lcom/squareup/api/ApiTransactionState;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/api/ApiReaderSettingsController;Lcom/squareup/ui/main/ReaderStatusMonitor;Lcom/squareup/checkoutflow/PaymentProcessingEventSink;)V

    iput-object p1, p0, Lcom/squareup/ui/main/errors/DefaultAppletGoBackAfterWarning;->container:Lcom/squareup/ui/main/PosContainer;

    iput-object p2, p0, Lcom/squareup/ui/main/errors/DefaultAppletGoBackAfterWarning;->transaction:Lcom/squareup/payment/Transaction;

    iput-object p3, p0, Lcom/squareup/ui/main/errors/DefaultAppletGoBackAfterWarning;->paymentProcessingEventSink:Lcom/squareup/checkoutflow/PaymentProcessingEventSink;

    return-void
.end method


# virtual methods
.method public goBack()V
    .locals 2

    .line 34
    invoke-virtual {p0}, Lcom/squareup/ui/main/errors/DefaultAppletGoBackAfterWarning;->doCommonRedirections()Z

    move-result v0

    if-nez v0, :cond_0

    .line 36
    iget-object v0, p0, Lcom/squareup/ui/main/errors/DefaultAppletGoBackAfterWarning;->transaction:Lcom/squareup/payment/Transaction;

    sget-object v1, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->CANCEL_BILL_HUMAN_INITIATED:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Transaction;->dropPayment(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V

    .line 37
    iget-object v0, p0, Lcom/squareup/ui/main/errors/DefaultAppletGoBackAfterWarning;->paymentProcessingEventSink:Lcom/squareup/checkoutflow/PaymentProcessingEventSink;

    sget-object v1, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->CANCEL_BILL_HUMAN_INITIATED:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    invoke-interface {v0, v1}, Lcom/squareup/checkoutflow/PaymentProcessingEventSink;->paymentCanceled(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V

    .line 38
    iget-object v0, p0, Lcom/squareup/ui/main/errors/DefaultAppletGoBackAfterWarning;->container:Lcom/squareup/ui/main/PosContainer;

    invoke-interface {v0}, Lcom/squareup/ui/main/PosContainer;->resetHistory()V

    :cond_0
    return-void
.end method
