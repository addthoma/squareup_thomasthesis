.class public abstract Lcom/squareup/ui/main/DailyContentLauncher;
.super Ljava/lang/Object;
.source "DailyContentLauncher.java"

# interfaces
.implements Lcom/squareup/ui/main/ContentLauncher;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/ui/main/ContentLauncher<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final dayNumLocalSetting:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final isAppropriateContext:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final x2ScreenRunner:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Lcom/squareup/settings/LocalSetting;Lcom/squareup/x2/BadMaybeSquareDeviceCheck;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/squareup/x2/BadMaybeSquareDeviceCheck;",
            ")V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/ui/main/DailyContentLauncher;->isAppropriateContext:Ljavax/inject/Provider;

    .line 23
    iput-object p2, p0, Lcom/squareup/ui/main/DailyContentLauncher;->dayNumLocalSetting:Lcom/squareup/settings/LocalSetting;

    .line 24
    iput-object p3, p0, Lcom/squareup/ui/main/DailyContentLauncher;->x2ScreenRunner:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    return-void
.end method

.method private static getUniqueDayNumber()J
    .locals 5

    .line 54
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v0

    const/4 v1, 0x1

    .line 55
    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    int-to-long v1, v1

    const/4 v3, 0x6

    .line 56
    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v0

    const/16 v3, 0x9

    shl-long/2addr v1, v3

    int-to-long v3, v0

    add-long/2addr v1, v3

    return-wide v1
.end method


# virtual methods
.method public bridge synthetic attemptToShowContent(Ljava/lang/Object;)Z
    .locals 0

    .line 15
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/main/DailyContentLauncher;->attemptToShowContent(Ljava/lang/Void;)Z

    move-result p1

    return p1
.end method

.method public attemptToShowContent(Ljava/lang/Void;)Z
    .locals 0

    .line 29
    iget-object p1, p0, Lcom/squareup/ui/main/DailyContentLauncher;->x2ScreenRunner:Lcom/squareup/x2/BadMaybeSquareDeviceCheck;

    invoke-interface {p1}, Lcom/squareup/x2/BadMaybeSquareDeviceCheck;->badIsHodorCheck()Z

    move-result p1

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/main/DailyContentLauncher;->isAppropriateContext:Ljavax/inject/Provider;

    invoke-interface {p1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/squareup/ui/main/DailyContentLauncher;->check()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 30
    invoke-virtual {p0}, Lcom/squareup/ui/main/DailyContentLauncher;->showContent()V

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public check()Z
    .locals 5

    .line 38
    iget-object v0, p0, Lcom/squareup/ui/main/DailyContentLauncher;->dayNumLocalSetting:Lcom/squareup/settings/LocalSetting;

    invoke-interface {v0}, Lcom/squareup/settings/LocalSetting;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 39
    invoke-static {}, Lcom/squareup/ui/main/DailyContentLauncher;->getUniqueDayNumber()J

    move-result-wide v1

    .line 40
    iget-object v3, p0, Lcom/squareup/ui/main/DailyContentLauncher;->dayNumLocalSetting:Lcom/squareup/settings/LocalSetting;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    if-eqz v0, :cond_1

    .line 41
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    cmp-long v0, v1, v3

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method protected abstract showContent()V
.end method
