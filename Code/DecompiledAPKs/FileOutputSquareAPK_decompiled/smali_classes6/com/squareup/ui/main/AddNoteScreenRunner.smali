.class public Lcom/squareup/ui/main/AddNoteScreenRunner;
.super Ljava/lang/Object;
.source "AddNoteScreenRunner.java"


# instance fields
.field private final flow:Lflow/Flow;


# direct methods
.method constructor <init>(Lflow/Flow;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/squareup/ui/main/AddNoteScreenRunner;->flow:Lflow/Flow;

    return-void
.end method


# virtual methods
.method public finishNoteScreen()V
    .locals 4

    .line 21
    iget-object v0, p0, Lcom/squareup/ui/main/AddNoteScreenRunner;->flow:Lflow/Flow;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const-class v2, Lcom/squareup/ui/main/AddNoteScreen;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    return-void
.end method

.method public goToNoteScreen()V
    .locals 2

    .line 17
    iget-object v0, p0, Lcom/squareup/ui/main/AddNoteScreenRunner;->flow:Lflow/Flow;

    sget-object v1, Lcom/squareup/ui/main/AddNoteScreen;->INSTANCE:Lcom/squareup/ui/main/AddNoteScreen;

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method
