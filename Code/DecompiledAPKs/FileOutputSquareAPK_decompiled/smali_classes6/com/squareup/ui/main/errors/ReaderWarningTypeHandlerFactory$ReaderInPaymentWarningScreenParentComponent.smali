.class public interface abstract Lcom/squareup/ui/main/errors/ReaderWarningTypeHandlerFactory$ReaderInPaymentWarningScreenParentComponent;
.super Ljava/lang/Object;
.source "ReaderWarningTypeHandlerFactory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/main/errors/ReaderWarningTypeHandlerFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ReaderInPaymentWarningScreenParentComponent"
.end annotation


# virtual methods
.method public abstract dipRequiredFallback()Lcom/squareup/ui/main/errors/ReaderWarningScreenHandler;
.end method

.method public abstract emvSchemeFallback()Lcom/squareup/ui/main/errors/ReaderWarningScreenHandler;
.end method

.method public abstract emvTechnicalFallback()Lcom/squareup/ui/main/errors/ReaderWarningScreenHandler;
.end method
