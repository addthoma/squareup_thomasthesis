.class abstract enum Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;
.super Ljava/lang/Enum;
.source "R12EducationView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/main/r12education/R12EducationView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4408
    name = "PanelType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

.field public static final enum CHARGE:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

.field public static final enum DIP:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

.field public static final enum START_SELLING:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

.field public static final enum SWIPE:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

.field public static final enum TAP_HAND:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

.field public static final enum TAP_PHONE:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

.field public static final enum VIDEO:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

.field public static final enum WELCOME:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

.field public static final enum WELCOME_DURING_BLOCKING_FWUP:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;


# instance fields
.field final panelViewId:I


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .line 86
    new-instance v0, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType$1;

    sget v1, Lcom/squareup/readertutorial/R$id;->r12_education_welcome:I

    const/4 v2, 0x0

    const-string v3, "WELCOME"

    invoke-direct {v0, v3, v2, v1}, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType$1;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->WELCOME:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    .line 99
    new-instance v0, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType$2;

    sget v1, Lcom/squareup/readertutorial/R$id;->r12_education_welcome:I

    const/4 v3, 0x1

    const-string v4, "WELCOME_DURING_BLOCKING_FWUP"

    invoke-direct {v0, v4, v3, v1}, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType$2;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->WELCOME_DURING_BLOCKING_FWUP:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    .line 121
    new-instance v0, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType$3;

    sget v1, Lcom/squareup/readertutorial/R$id;->r12_education_charge:I

    const/4 v4, 0x2

    const-string v5, "CHARGE"

    invoke-direct {v0, v5, v4, v1}, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType$3;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->CHARGE:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    .line 141
    new-instance v0, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType$4;

    sget v1, Lcom/squareup/readertutorial/R$id;->r12_education_dip:I

    const/4 v5, 0x3

    const-string v6, "DIP"

    invoke-direct {v0, v6, v5, v1}, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType$4;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->DIP:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    .line 160
    new-instance v0, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType$5;

    sget v1, Lcom/squareup/readertutorial/R$id;->r12_education_tap:I

    const/4 v6, 0x4

    const-string v7, "TAP_PHONE"

    invoke-direct {v0, v7, v6, v1}, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType$5;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->TAP_PHONE:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    .line 180
    new-instance v0, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType$6;

    sget v1, Lcom/squareup/readertutorial/R$id;->r12_education_tap:I

    const/4 v7, 0x5

    const-string v8, "TAP_HAND"

    invoke-direct {v0, v8, v7, v1}, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType$6;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->TAP_HAND:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    .line 200
    new-instance v0, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType$7;

    sget v1, Lcom/squareup/readertutorial/R$id;->r12_education_swipe:I

    const/4 v8, 0x6

    const-string v9, "SWIPE"

    invoke-direct {v0, v9, v8, v1}, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType$7;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->SWIPE:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    .line 220
    new-instance v0, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType$8;

    sget v1, Lcom/squareup/readertutorial/R$id;->r12_education_learn_even_more:I

    const/4 v9, 0x7

    const-string v10, "VIDEO"

    invoke-direct {v0, v10, v9, v1}, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType$8;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->VIDEO:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    .line 229
    new-instance v0, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType$9;

    sget v1, Lcom/squareup/readertutorial/R$id;->r12_education_start_selling:I

    const/16 v10, 0x8

    const-string v11, "START_SELLING"

    invoke-direct {v0, v11, v10, v1}, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType$9;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->START_SELLING:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    const/16 v0, 0x9

    new-array v0, v0, [Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    .line 85
    sget-object v1, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->WELCOME:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->WELCOME_DURING_BLOCKING_FWUP:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->CHARGE:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->DIP:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->TAP_PHONE:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->TAP_HAND:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->SWIPE:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->VIDEO:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->START_SELLING:Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    aput-object v1, v0, v10

    sput-object v0, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->$VALUES:[Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 246
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 247
    iput p3, p0, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->panelViewId:I

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IILcom/squareup/ui/main/r12education/R12EducationView$1;)V
    .locals 0

    .line 85
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;-><init>(Ljava/lang/String;II)V

    return-void
.end method

.method static synthetic access$100(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 0

    .line 85
    invoke-static {p0, p1}, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->inflateAndAdjust(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method private static inflateAndAdjust(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .line 274
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 275
    sget v1, Lcom/squareup/marin/R$dimen;->marin_page_indicator_default_diameter:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 277
    sget v2, Lcom/squareup/marin/R$dimen;->marin_gap_big:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 278
    sget v3, Lcom/squareup/marin/R$dimen;->marin_min_height:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 279
    sget v4, Lcom/squareup/marin/R$dimen;->marin_gutter_half:I

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v2

    add-int/2addr v0, v1

    .line 282
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p0, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p0

    .line 283
    sget p1, Lcom/squareup/readertutorial/R$id;->r12_education_matte:I

    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 286
    invoke-virtual {p0}, Landroid/view/View;->getPaddingLeft()I

    move-result p1

    invoke-virtual {p0}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    .line 287
    invoke-virtual {p0}, Landroid/view/View;->getPaddingRight()I

    move-result v2

    .line 286
    invoke-virtual {p0, p1, v1, v2, v0}, Landroid/view/View;->setPadding(IIII)V

    return-object p0

    .line 284
    :cond_0
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string p1, "Must have a matte on each panel!"

    invoke-direct {p0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;
    .locals 1

    .line 85
    const-class v0, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;
    .locals 1

    .line 85
    sget-object v0, Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->$VALUES:[Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    invoke-virtual {v0}, [Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/main/r12education/R12EducationView$PanelType;

    return-object v0
.end method


# virtual methods
.method abstract buildContent(Landroid/view/ViewGroup;Lcom/squareup/CountryCode;)Landroid/view/View;
.end method

.method getButtonText()I
    .locals 1

    .line 253
    sget v0, Lcom/squareup/common/strings/R$string;->next:I

    return v0
.end method

.method abstract getLogDetailFromPanel()Ljava/lang/String;
.end method

.method overTweenElement(Lcom/squareup/ui/main/r12education/R12EducationView$Element;Lcom/squareup/ui/main/r12education/TransitionView;F)V
    .locals 0

    return-void
.end method

.method shouldOverTween()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
