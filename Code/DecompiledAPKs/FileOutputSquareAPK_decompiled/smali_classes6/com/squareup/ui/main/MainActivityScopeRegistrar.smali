.class public final Lcom/squareup/ui/main/MainActivityScopeRegistrar;
.super Ljava/lang/Object;
.source "MainActivityScopeRegistrar.kt"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation runtime Lcom/squareup/ForMainActivity;
.end annotation

.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/main/MainActivityScopeRegistrar$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nMainActivityScopeRegistrar.kt\nKotlin\n*S Kotlin\n*F\n+ 1 MainActivityScopeRegistrar.kt\ncom/squareup/ui/main/MainActivityScopeRegistrar\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,212:1\n1642#2,2:213\n1642#2,2:215\n*E\n*S KotlinDebug\n*F\n+ 1 MainActivityScopeRegistrar.kt\ncom/squareup/ui/main/MainActivityScopeRegistrar\n*L\n118#1,2:213\n158#1,2:215\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u009a\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0008\u0007\u0018\u0000 _2\u00020\u0001:\u0001_B\u00d9\u0002\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u0012\u0006\u0010\u0014\u001a\u00020\u0015\u0012\u0006\u0010\u0016\u001a\u00020\u0017\u0012\u0006\u0010\u0018\u001a\u00020\u0019\u0012\u0006\u0010\u001a\u001a\u00020\u001b\u0012\u0006\u0010\u001c\u001a\u00020\u001d\u0012\u0006\u0010\u001e\u001a\u00020\u001f\u0012\u0006\u0010 \u001a\u00020!\u0012\u0006\u0010\"\u001a\u00020#\u0012\u0006\u0010$\u001a\u00020%\u0012\u0006\u0010&\u001a\u00020\'\u0012\u0006\u0010(\u001a\u00020)\u0012\u0006\u0010*\u001a\u00020+\u0012\u0006\u0010,\u001a\u00020-\u0012\u0006\u0010.\u001a\u00020/\u0012\u0006\u00100\u001a\u000201\u0012\u0006\u00102\u001a\u000203\u0012\u0006\u00104\u001a\u000205\u0012\u0006\u00106\u001a\u000207\u0012\u0006\u00108\u001a\u000209\u0012\u0006\u0010:\u001a\u00020;\u0012\u0006\u0010<\u001a\u00020=\u0012\u0006\u0010>\u001a\u00020?\u0012\u0006\u0010@\u001a\u00020A\u0012\u0006\u0010B\u001a\u00020C\u0012\u0013\u0008\u0001\u0010D\u001a\r\u0012\t\u0012\u00070\u0001\u00a2\u0006\u0002\u0008F0E\u0012\u0013\u0008\u0001\u0010G\u001a\r\u0012\t\u0012\u00070H\u00a2\u0006\u0002\u0008F0E\u0012\u0006\u0010I\u001a\u00020J\u0012\u0006\u0010K\u001a\u00020L\u0012\u0006\u0010M\u001a\u00020N\u0012\u0006\u0010O\u001a\u00020P\u00a2\u0006\u0002\u0010QJ\u0010\u0010X\u001a\u00020Y2\u0006\u0010Z\u001a\u00020[H\u0016J\u0008\u0010\\\u001a\u00020YH\u0016J\u0008\u0010]\u001a\u00020YH\u0002J\u0008\u0010^\u001a\u00020YH\u0002R\u000e\u0010O\u001a\u00020PX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0019\u0010G\u001a\r\u0012\t\u0012\u00070H\u00a2\u0006\u0002\u0008F0EX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0019\u0010D\u001a\r\u0012\t\u0012\u00070\u0001\u00a2\u0006\u0002\u0008F0EX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010,\u001a\u00020-X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010*\u001a\u00020+X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010(\u001a\u00020)X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010K\u001a\u00020LX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010>\u001a\u00020?X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\"\u001a\u00020#X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010 \u001a\u00020!X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001e\u001a\u00020\u001fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010<\u001a\u00020=X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010&\u001a\u00020\'X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u00100\u001a\u000201X\u0082\u0004\u00a2\u0006\u0002\n\u0000R2\u0010R\u001a&\u0012\u000c\u0012\n U*\u0004\u0018\u00010T0T U*\u0012\u0012\u000c\u0012\n U*\u0004\u0018\u00010T0T\u0018\u00010S0SX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010V\u001a\u00020WX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010B\u001a\u00020CX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u00108\u001a\u000209X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010$\u001a\u00020%X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u001dX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u00106\u001a\u000207X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010I\u001a\u00020JX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010M\u001a\u00020NX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010@\u001a\u00020AX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u00102\u001a\u000203X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010.\u001a\u00020/X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010:\u001a\u00020;X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006`"
    }
    d2 = {
        "Lcom/squareup/ui/main/MainActivityScopeRegistrar;",
        "Lmortar/Scoped;",
        "badBus",
        "Lcom/squareup/badbus/BadBus;",
        "x2ScreenRunner",
        "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
        "transaction",
        "Lcom/squareup/payment/Transaction;",
        "emvSwipePassthroughEnabler",
        "Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;",
        "orderEntryScreenState",
        "Lcom/squareup/orderentry/OrderEntryScreenState;",
        "jailKeeper",
        "Lcom/squareup/jailkeeper/JailKeeper;",
        "openTicketsSettings",
        "Lcom/squareup/tickets/OpenTicketsSettings;",
        "paymentCompletionMonitor",
        "Lcom/squareup/ui/main/PaymentCompletionMonitor;",
        "pauseAndResumeRegistrar",
        "Lcom/squareup/pauses/PauseAndResumeRegistrar;",
        "nfcProcessor",
        "Lcom/squareup/ui/NfcProcessor;",
        "printerScoutScheduler",
        "Lcom/squareup/print/PrinterScoutScheduler;",
        "tickets",
        "Lcom/squareup/tickets/Tickets;",
        "ticketsSweeperManager",
        "Lcom/squareup/opentickets/TicketsSweeperManager;",
        "printSpooler",
        "Lcom/squareup/print/PrintSpooler;",
        "clock",
        "Lcom/squareup/util/Clock;",
        "cashDrawerShiftManager",
        "Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;",
        "cardReaderHub",
        "Lcom/squareup/cardreader/CardReaderHub;",
        "passcodeEmployeeManagement",
        "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
        "employeeCacheUpdater",
        "Lcom/squareup/permissions/EmployeeCacheUpdater;",
        "apiTransactionController",
        "Lcom/squareup/api/ApiTransactionController;",
        "apiRequestController",
        "Lcom/squareup/api/ApiRequestController;",
        "apiReaderSettingsController",
        "Lcom/squareup/api/ApiReaderSettingsController;",
        "topScreenChecker",
        "Lcom/squareup/ui/main/TopScreenChecker;",
        "helpBadge",
        "Lcom/squareup/ui/help/HelpBadge;",
        "tileAppearanceSettings",
        "Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;",
        "connectivityMonitor",
        "Lcom/squareup/connectivity/ConnectivityMonitor;",
        "readerBatteryStatusHandler",
        "Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;",
        "onboardingDiverter",
        "Lcom/squareup/onboarding/OnboardingDiverter;",
        "tutorialCore",
        "Lcom/squareup/tutorialv2/TutorialCore;",
        "dipperUiEvents",
        "Lcom/squareup/cardreader/dipper/ReaderUiEventSink;",
        "buyerFlowStarter",
        "Lcom/squareup/ui/buyer/BuyerFlowStarter;",
        "tenderStarter",
        "Lcom/squareup/ui/tender/TenderStarter;",
        "lockScreenMonitor",
        "Lcom/squareup/permissions/ui/LockScreenMonitor;",
        "additionalMainActivityScopeServices",
        "",
        "Lkotlin/jvm/JvmSuppressWildcards;",
        "additionalMainActivityBundlers",
        "Lmortar/bundler/Bundler;",
        "recorderStateMonitor",
        "Lcom/squareup/ui/main/ReaderStatusMonitor;",
        "bankAccountSettings",
        "Lcom/squareup/banklinking/BankAccountSettings;",
        "sessionExpiredListener",
        "Lcom/squareup/account/SessionExpiredListener;",
        "activityVisibilityPresenter",
        "Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;",
        "(Lcom/squareup/badbus/BadBus;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/jailkeeper/JailKeeper;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/ui/main/PaymentCompletionMonitor;Lcom/squareup/pauses/PauseAndResumeRegistrar;Lcom/squareup/ui/NfcProcessor;Lcom/squareup/print/PrinterScoutScheduler;Lcom/squareup/tickets/Tickets;Lcom/squareup/opentickets/TicketsSweeperManager;Lcom/squareup/print/PrintSpooler;Lcom/squareup/util/Clock;Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/permissions/EmployeeCacheUpdater;Lcom/squareup/api/ApiTransactionController;Lcom/squareup/api/ApiRequestController;Lcom/squareup/api/ApiReaderSettingsController;Lcom/squareup/ui/main/TopScreenChecker;Lcom/squareup/ui/help/HelpBadge;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;Lcom/squareup/onboarding/OnboardingDiverter;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/cardreader/dipper/ReaderUiEventSink;Lcom/squareup/ui/buyer/BuyerFlowStarter;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/permissions/ui/LockScreenMonitor;Ljava/util/Set;Ljava/util/Set;Lcom/squareup/ui/main/ReaderStatusMonitor;Lcom/squareup/banklinking/BankAccountSettings;Lcom/squareup/account/SessionExpiredListener;Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;)V",
        "internetState",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/connectivity/InternetState;",
        "kotlin.jvm.PlatformType",
        "lastPauseTime",
        "",
        "onEnterScope",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "onPause",
        "onResume",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ui/main/MainActivityScopeRegistrar$Companion;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final MIN_PAUSE_RESUME_INTERVAL_TO_SYNC_MS:J = 0x1388L


# instance fields
.field private final activityVisibilityPresenter:Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;

.field private final additionalMainActivityBundlers:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lmortar/bundler/Bundler;",
            ">;"
        }
    .end annotation
.end field

.field private final additionalMainActivityScopeServices:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lmortar/Scoped;",
            ">;"
        }
    .end annotation
.end field

.field private final apiReaderSettingsController:Lcom/squareup/api/ApiReaderSettingsController;

.field private final apiRequestController:Lcom/squareup/api/ApiRequestController;

.field private final apiTransactionController:Lcom/squareup/api/ApiTransactionController;

.field private final badBus:Lcom/squareup/badbus/BadBus;

.field private final bankAccountSettings:Lcom/squareup/banklinking/BankAccountSettings;

.field private final buyerFlowStarter:Lcom/squareup/ui/buyer/BuyerFlowStarter;

.field private final cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

.field private final cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;

.field private final clock:Lcom/squareup/util/Clock;

.field private final dipperUiEvents:Lcom/squareup/cardreader/dipper/ReaderUiEventSink;

.field private final employeeCacheUpdater:Lcom/squareup/permissions/EmployeeCacheUpdater;

.field private final emvSwipePassthroughEnabler:Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;

.field private final helpBadge:Lcom/squareup/ui/help/HelpBadge;

.field private final internetState:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/connectivity/InternetState;",
            ">;"
        }
    .end annotation
.end field

.field private final jailKeeper:Lcom/squareup/jailkeeper/JailKeeper;

.field private lastPauseTime:J

.field private final lockScreenMonitor:Lcom/squareup/permissions/ui/LockScreenMonitor;

.field private final nfcProcessor:Lcom/squareup/ui/NfcProcessor;

.field private final onboardingDiverter:Lcom/squareup/onboarding/OnboardingDiverter;

.field private final openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

.field private final orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

.field private final passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

.field private final pauseAndResumeRegistrar:Lcom/squareup/pauses/PauseAndResumeRegistrar;

.field private final paymentCompletionMonitor:Lcom/squareup/ui/main/PaymentCompletionMonitor;

.field private final printSpooler:Lcom/squareup/print/PrintSpooler;

.field private final printerScoutScheduler:Lcom/squareup/print/PrinterScoutScheduler;

.field private final readerBatteryStatusHandler:Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;

.field private final recorderStateMonitor:Lcom/squareup/ui/main/ReaderStatusMonitor;

.field private final sessionExpiredListener:Lcom/squareup/account/SessionExpiredListener;

.field private final tenderStarter:Lcom/squareup/ui/tender/TenderStarter;

.field private final tickets:Lcom/squareup/tickets/Tickets;

.field private final ticketsSweeperManager:Lcom/squareup/opentickets/TicketsSweeperManager;

.field private final tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

.field private final topScreenChecker:Lcom/squareup/ui/main/TopScreenChecker;

.field private final transaction:Lcom/squareup/payment/Transaction;

.field private final tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

.field private final x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/main/MainActivityScopeRegistrar$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->Companion:Lcom/squareup/ui/main/MainActivityScopeRegistrar$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/badbus/BadBus;Lcom/squareup/x2/MaybeX2SellerScreenRunner;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;Lcom/squareup/orderentry/OrderEntryScreenState;Lcom/squareup/jailkeeper/JailKeeper;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/ui/main/PaymentCompletionMonitor;Lcom/squareup/pauses/PauseAndResumeRegistrar;Lcom/squareup/ui/NfcProcessor;Lcom/squareup/print/PrinterScoutScheduler;Lcom/squareup/tickets/Tickets;Lcom/squareup/opentickets/TicketsSweeperManager;Lcom/squareup/print/PrintSpooler;Lcom/squareup/util/Clock;Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/permissions/EmployeeCacheUpdater;Lcom/squareup/api/ApiTransactionController;Lcom/squareup/api/ApiRequestController;Lcom/squareup/api/ApiReaderSettingsController;Lcom/squareup/ui/main/TopScreenChecker;Lcom/squareup/ui/help/HelpBadge;Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;Lcom/squareup/connectivity/ConnectivityMonitor;Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;Lcom/squareup/onboarding/OnboardingDiverter;Lcom/squareup/tutorialv2/TutorialCore;Lcom/squareup/cardreader/dipper/ReaderUiEventSink;Lcom/squareup/ui/buyer/BuyerFlowStarter;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/permissions/ui/LockScreenMonitor;Ljava/util/Set;Ljava/util/Set;Lcom/squareup/ui/main/ReaderStatusMonitor;Lcom/squareup/banklinking/BankAccountSettings;Lcom/squareup/account/SessionExpiredListener;Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;)V
    .locals 16
    .param p34    # Ljava/util/Set;
        .annotation runtime Lcom/squareup/ForMainActivity;
        .end annotation
    .end param
    .param p35    # Ljava/util/Set;
        .annotation runtime Lcom/squareup/ForMainActivity;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/badbus/BadBus;",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;",
            "Lcom/squareup/orderentry/OrderEntryScreenState;",
            "Lcom/squareup/jailkeeper/JailKeeper;",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            "Lcom/squareup/ui/main/PaymentCompletionMonitor;",
            "Lcom/squareup/pauses/PauseAndResumeRegistrar;",
            "Lcom/squareup/ui/NfcProcessor;",
            "Lcom/squareup/print/PrinterScoutScheduler;",
            "Lcom/squareup/tickets/Tickets;",
            "Lcom/squareup/opentickets/TicketsSweeperManager;",
            "Lcom/squareup/print/PrintSpooler;",
            "Lcom/squareup/util/Clock;",
            "Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;",
            "Lcom/squareup/cardreader/CardReaderHub;",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            "Lcom/squareup/permissions/EmployeeCacheUpdater;",
            "Lcom/squareup/api/ApiTransactionController;",
            "Lcom/squareup/api/ApiRequestController;",
            "Lcom/squareup/api/ApiReaderSettingsController;",
            "Lcom/squareup/ui/main/TopScreenChecker;",
            "Lcom/squareup/ui/help/HelpBadge;",
            "Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            "Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;",
            "Lcom/squareup/onboarding/OnboardingDiverter;",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            "Lcom/squareup/cardreader/dipper/ReaderUiEventSink;",
            "Lcom/squareup/ui/buyer/BuyerFlowStarter;",
            "Lcom/squareup/ui/tender/TenderStarter;",
            "Lcom/squareup/permissions/ui/LockScreenMonitor;",
            "Ljava/util/Set<",
            "Lmortar/Scoped;",
            ">;",
            "Ljava/util/Set<",
            "Lmortar/bundler/Bundler;",
            ">;",
            "Lcom/squareup/ui/main/ReaderStatusMonitor;",
            "Lcom/squareup/banklinking/BankAccountSettings;",
            "Lcom/squareup/account/SessionExpiredListener;",
            "Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    move-object/from16 v14, p14

    move-object/from16 v15, p15

    move-object/from16 v0, p16

    const-string v0, "badBus"

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "x2ScreenRunner"

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "transaction"

    invoke-static {v3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "emvSwipePassthroughEnabler"

    invoke-static {v4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderEntryScreenState"

    invoke-static {v5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "jailKeeper"

    invoke-static {v6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "openTicketsSettings"

    invoke-static {v7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentCompletionMonitor"

    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "pauseAndResumeRegistrar"

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nfcProcessor"

    invoke-static {v10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "printerScoutScheduler"

    invoke-static {v11, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tickets"

    invoke-static {v12, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "ticketsSweeperManager"

    invoke-static {v13, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "printSpooler"

    invoke-static {v14, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clock"

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cashDrawerShiftManager"

    move-object/from16 v15, p16

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardReaderHub"

    move-object/from16 v15, p17

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "passcodeEmployeeManagement"

    move-object/from16 v15, p18

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "employeeCacheUpdater"

    move-object/from16 v15, p19

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "apiTransactionController"

    move-object/from16 v15, p20

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "apiRequestController"

    move-object/from16 v15, p21

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "apiReaderSettingsController"

    move-object/from16 v15, p22

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "topScreenChecker"

    move-object/from16 v15, p23

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "helpBadge"

    move-object/from16 v15, p24

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tileAppearanceSettings"

    move-object/from16 v15, p25

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "connectivityMonitor"

    move-object/from16 v15, p26

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "readerBatteryStatusHandler"

    move-object/from16 v15, p27

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onboardingDiverter"

    move-object/from16 v15, p28

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tutorialCore"

    move-object/from16 v15, p29

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dipperUiEvents"

    move-object/from16 v15, p30

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "buyerFlowStarter"

    move-object/from16 v15, p31

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tenderStarter"

    move-object/from16 v15, p32

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "lockScreenMonitor"

    move-object/from16 v15, p33

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "additionalMainActivityScopeServices"

    move-object/from16 v15, p34

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "additionalMainActivityBundlers"

    move-object/from16 v15, p35

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "recorderStateMonitor"

    move-object/from16 v15, p36

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "bankAccountSettings"

    move-object/from16 v15, p37

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "sessionExpiredListener"

    move-object/from16 v15, p38

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "activityVisibilityPresenter"

    move-object/from16 v15, p39

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    move-object/from16 v0, p0

    move-object/from16 v15, p16

    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->badBus:Lcom/squareup/badbus/BadBus;

    iput-object v2, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    iput-object v3, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->transaction:Lcom/squareup/payment/Transaction;

    iput-object v4, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->emvSwipePassthroughEnabler:Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;

    iput-object v5, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    iput-object v6, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->jailKeeper:Lcom/squareup/jailkeeper/JailKeeper;

    iput-object v7, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    iput-object v8, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->paymentCompletionMonitor:Lcom/squareup/ui/main/PaymentCompletionMonitor;

    iput-object v9, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->pauseAndResumeRegistrar:Lcom/squareup/pauses/PauseAndResumeRegistrar;

    iput-object v10, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->nfcProcessor:Lcom/squareup/ui/NfcProcessor;

    iput-object v11, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->printerScoutScheduler:Lcom/squareup/print/PrinterScoutScheduler;

    iput-object v12, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->tickets:Lcom/squareup/tickets/Tickets;

    iput-object v13, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->ticketsSweeperManager:Lcom/squareup/opentickets/TicketsSweeperManager;

    iput-object v14, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->printSpooler:Lcom/squareup/print/PrintSpooler;

    move-object/from16 v1, p15

    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->clock:Lcom/squareup/util/Clock;

    iput-object v15, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;

    move-object/from16 v1, p17

    move-object/from16 v2, p18

    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    iput-object v2, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    move-object/from16 v1, p19

    move-object/from16 v2, p20

    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->employeeCacheUpdater:Lcom/squareup/permissions/EmployeeCacheUpdater;

    iput-object v2, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->apiTransactionController:Lcom/squareup/api/ApiTransactionController;

    move-object/from16 v1, p21

    move-object/from16 v2, p22

    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->apiRequestController:Lcom/squareup/api/ApiRequestController;

    iput-object v2, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->apiReaderSettingsController:Lcom/squareup/api/ApiReaderSettingsController;

    move-object/from16 v1, p23

    move-object/from16 v2, p24

    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->topScreenChecker:Lcom/squareup/ui/main/TopScreenChecker;

    iput-object v2, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->helpBadge:Lcom/squareup/ui/help/HelpBadge;

    move-object/from16 v1, p25

    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    move-object/from16 v1, p27

    move-object/from16 v2, p28

    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->readerBatteryStatusHandler:Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;

    iput-object v2, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->onboardingDiverter:Lcom/squareup/onboarding/OnboardingDiverter;

    move-object/from16 v1, p29

    move-object/from16 v2, p30

    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    iput-object v2, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->dipperUiEvents:Lcom/squareup/cardreader/dipper/ReaderUiEventSink;

    move-object/from16 v1, p31

    move-object/from16 v2, p32

    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->buyerFlowStarter:Lcom/squareup/ui/buyer/BuyerFlowStarter;

    iput-object v2, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->tenderStarter:Lcom/squareup/ui/tender/TenderStarter;

    move-object/from16 v1, p33

    move-object/from16 v2, p34

    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->lockScreenMonitor:Lcom/squareup/permissions/ui/LockScreenMonitor;

    iput-object v2, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->additionalMainActivityScopeServices:Ljava/util/Set;

    move-object/from16 v1, p35

    move-object/from16 v2, p36

    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->additionalMainActivityBundlers:Ljava/util/Set;

    iput-object v2, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->recorderStateMonitor:Lcom/squareup/ui/main/ReaderStatusMonitor;

    move-object/from16 v1, p37

    move-object/from16 v2, p38

    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->bankAccountSettings:Lcom/squareup/banklinking/BankAccountSettings;

    iput-object v2, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->sessionExpiredListener:Lcom/squareup/account/SessionExpiredListener;

    move-object/from16 v1, p39

    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->activityVisibilityPresenter:Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;

    .line 97
    invoke-interface/range {p26 .. p26}, Lcom/squareup/connectivity/ConnectivityMonitor;->internetState()Lio/reactivex/Observable;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->internetState:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$getCardReaderHub$p(Lcom/squareup/ui/main/MainActivityScopeRegistrar;)Lcom/squareup/cardreader/CardReaderHub;
    .locals 0

    .line 56
    iget-object p0, p0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->cardReaderHub:Lcom/squareup/cardreader/CardReaderHub;

    return-object p0
.end method

.method public static final synthetic access$getJailKeeper$p(Lcom/squareup/ui/main/MainActivityScopeRegistrar;)Lcom/squareup/jailkeeper/JailKeeper;
    .locals 0

    .line 56
    iget-object p0, p0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->jailKeeper:Lcom/squareup/jailkeeper/JailKeeper;

    return-object p0
.end method

.method public static final synthetic access$getSessionExpiredListener$p(Lcom/squareup/ui/main/MainActivityScopeRegistrar;)Lcom/squareup/account/SessionExpiredListener;
    .locals 0

    .line 56
    iget-object p0, p0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->sessionExpiredListener:Lcom/squareup/account/SessionExpiredListener;

    return-object p0
.end method

.method public static final synthetic access$onPause(Lcom/squareup/ui/main/MainActivityScopeRegistrar;)V
    .locals 0

    .line 56
    invoke-direct {p0}, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->onPause()V

    return-void
.end method

.method public static final synthetic access$onResume(Lcom/squareup/ui/main/MainActivityScopeRegistrar;)V
    .locals 0

    .line 56
    invoke-direct {p0}, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->onResume()V

    return-void
.end method

.method private final onPause()V
    .locals 2

    .line 202
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->printerScoutScheduler:Lcom/squareup/print/PrinterScoutScheduler;

    invoke-virtual {v0}, Lcom/squareup/print/PrinterScoutScheduler;->onAppPause()V

    .line 203
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->onAppPause()V

    .line 205
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v0}, Lcom/squareup/util/Clock;->getElapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->lastPauseTime:J

    return-void
.end method

.method private final onResume()V
    .locals 5

    .line 188
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->printerScoutScheduler:Lcom/squareup/print/PrinterScoutScheduler;

    invoke-virtual {v0}, Lcom/squareup/print/PrinterScoutScheduler;->onAppResume()V

    .line 189
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->passcodeEmployeeManagement:Lcom/squareup/permissions/PasscodeEmployeeManagement;

    invoke-virtual {v0}, Lcom/squareup/permissions/PasscodeEmployeeManagement;->onAppResume()V

    .line 190
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->jailKeeper:Lcom/squareup/jailkeeper/JailKeeper;

    invoke-interface {v0}, Lcom/squareup/jailkeeper/JailKeeper;->getState()Lcom/squareup/jailkeeper/JailKeeper$State;

    move-result-object v0

    sget-object v1, Lcom/squareup/jailkeeper/JailKeeper$State;->READY:Lcom/squareup/jailkeeper/JailKeeper$State;

    if-ne v0, v1, :cond_0

    .line 191
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->employeeCacheUpdater:Lcom/squareup/permissions/EmployeeCacheUpdater;

    invoke-virtual {v0}, Lcom/squareup/permissions/EmployeeCacheUpdater;->refresh()V

    .line 196
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->clock:Lcom/squareup/util/Clock;

    invoke-interface {v0}, Lcom/squareup/util/Clock;->getElapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->lastPauseTime:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x1388

    cmp-long v4, v0, v2

    if-lez v4, :cond_1

    .line 197
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->jailKeeper:Lcom/squareup/jailkeeper/JailKeeper;

    invoke-interface {v0}, Lcom/squareup/jailkeeper/JailKeeper;->backgroundSync()V

    :cond_1
    return-void
.end method


# virtual methods
.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 104
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->buyerFlowStarter:Lcom/squareup/ui/buyer/BuyerFlowStarter;

    check-cast v0, Lmortar/Scoped;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 105
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->dipperUiEvents:Lcom/squareup/cardreader/dipper/ReaderUiEventSink;

    check-cast v0, Lmortar/Scoped;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 106
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->tenderStarter:Lcom/squareup/ui/tender/TenderStarter;

    check-cast v0, Lmortar/Scoped;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 109
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->recorderStateMonitor:Lcom/squareup/ui/main/ReaderStatusMonitor;

    check-cast v0, Lmortar/Scoped;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 111
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->badBus:Lcom/squareup/badbus/BadBus;

    const-class v1, Lcom/squareup/account/AccountEvents$SessionExpired;

    invoke-virtual {v0, v1}, Lcom/squareup/badbus/BadBus;->events(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "badBus.events(AccountEve\u2026ssionExpired::class.java)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 112
    new-instance v1, Lcom/squareup/ui/main/MainActivityScopeRegistrar$onEnterScope$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/main/MainActivityScopeRegistrar$onEnterScope$2;-><init>(Lcom/squareup/ui/main/MainActivityScopeRegistrar;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 114
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->x2ScreenRunner:Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    check-cast v0, Lmortar/Scoped;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 115
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->nfcProcessor:Lcom/squareup/ui/NfcProcessor;

    check-cast v0, Lmortar/Scoped;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 117
    invoke-static {p1}, Lmortar/bundler/BundleService;->getBundleService(Lmortar/MortarScope;)Lmortar/bundler/BundleService;

    move-result-object v0

    .line 119
    iget-object v1, p0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->paymentCompletionMonitor:Lcom/squareup/ui/main/PaymentCompletionMonitor;

    check-cast v1, Lmortar/bundler/Bundler;

    invoke-virtual {v0, v1}, Lmortar/bundler/BundleService;->register(Lmortar/bundler/Bundler;)V

    .line 120
    iget-object v1, p0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->getBundler()Lmortar/bundler/Bundler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmortar/bundler/BundleService;->register(Lmortar/bundler/Bundler;)V

    .line 121
    iget-object v1, p0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->tickets:Lcom/squareup/tickets/Tickets;

    invoke-interface {v1}, Lcom/squareup/tickets/Tickets;->getBundler()Lmortar/bundler/Bundler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmortar/bundler/BundleService;->register(Lmortar/bundler/Bundler;)V

    .line 122
    iget-object v1, p0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->orderEntryScreenState:Lcom/squareup/orderentry/OrderEntryScreenState;

    invoke-virtual {v1}, Lcom/squareup/orderentry/OrderEntryScreenState;->getBundler()Lcom/squareup/orderentry/OrderEntryScreenState$OrderEntryScreenStateBundler;

    move-result-object v1

    check-cast v1, Lmortar/bundler/Bundler;

    invoke-virtual {v0, v1}, Lmortar/bundler/BundleService;->register(Lmortar/bundler/Bundler;)V

    .line 123
    iget-object v1, p0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->cashDrawerShiftManager:Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;

    invoke-interface {v1}, Lcom/squareup/cashmanagement/CashDrawerShiftManagerForPayments;->getBundler()Lmortar/bundler/Bundler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmortar/bundler/BundleService;->register(Lmortar/bundler/Bundler;)V

    .line 124
    iget-object v1, p0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->tileAppearanceSettings:Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/tiles/TileAppearanceSettings;->getBundler()Lmortar/bundler/Bundler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmortar/bundler/BundleService;->register(Lmortar/bundler/Bundler;)V

    .line 125
    iget-object v1, p0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->apiTransactionController:Lcom/squareup/api/ApiTransactionController;

    invoke-virtual {v1}, Lcom/squareup/api/ApiTransactionController;->getBundler()Lmortar/bundler/Bundler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmortar/bundler/BundleService;->register(Lmortar/bundler/Bundler;)V

    .line 126
    iget-object v1, p0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->apiReaderSettingsController:Lcom/squareup/api/ApiReaderSettingsController;

    invoke-virtual {v1}, Lcom/squareup/api/ApiReaderSettingsController;->getBundler()Lmortar/bundler/Bundler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmortar/bundler/BundleService;->register(Lmortar/bundler/Bundler;)V

    .line 127
    iget-object v1, p0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->apiRequestController:Lcom/squareup/api/ApiRequestController;

    invoke-virtual {v1}, Lcom/squareup/api/ApiRequestController;->getBundler()Lmortar/bundler/Bundler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lmortar/bundler/BundleService;->register(Lmortar/bundler/Bundler;)V

    .line 128
    iget-object v1, p0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->additionalMainActivityBundlers:Ljava/util/Set;

    check-cast v1, Ljava/lang/Iterable;

    .line 213
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lmortar/bundler/Bundler;

    .line 128
    invoke-virtual {v0, v2}, Lmortar/bundler/BundleService;->register(Lmortar/bundler/Bundler;)V

    goto :goto_0

    .line 131
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->pauseAndResumeRegistrar:Lcom/squareup/pauses/PauseAndResumeRegistrar;

    invoke-interface {v0}, Lcom/squareup/pauses/PauseAndResumeRegistrar;->isRunningState()Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "pauseAndResumeRegistrar.isRunningState"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v1, Lcom/squareup/ui/main/MainActivityScopeRegistrar$onEnterScope$4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/main/MainActivityScopeRegistrar$onEnterScope$4;-><init>(Lcom/squareup/ui/main/MainActivityScopeRegistrar;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 138
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->pauseAndResumeRegistrar:Lcom/squareup/pauses/PauseAndResumeRegistrar;

    iget-object v1, p0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->createPauseResumeHandler()Lcom/squareup/pauses/PausesAndResumes;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/squareup/pauses/PauseAndResumeRegistrar;->register(Lmortar/MortarScope;Lcom/squareup/pauses/PausesAndResumes;)V

    .line 139
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->readerBatteryStatusHandler:Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;

    .line 140
    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/dipper/ReaderBatteryStatusHandler;->initialize(Lmortar/MortarScope;)V

    .line 142
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->emvSwipePassthroughEnabler:Lcom/squareup/ui/main/EmvSwipePassthroughEnabler;

    check-cast v0, Lmortar/Scoped;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 143
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->topScreenChecker:Lcom/squareup/ui/main/TopScreenChecker;

    check-cast v0, Lmortar/Scoped;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 144
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->helpBadge:Lcom/squareup/ui/help/HelpBadge;

    check-cast v0, Lmortar/Scoped;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 145
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->onboardingDiverter:Lcom/squareup/onboarding/OnboardingDiverter;

    check-cast v0, Lmortar/Scoped;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 146
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->tutorialCore:Lcom/squareup/tutorialv2/TutorialCore;

    check-cast v0, Lmortar/Scoped;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 148
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->printSpooler:Lcom/squareup/print/PrintSpooler;

    invoke-virtual {v0}, Lcom/squareup/print/PrintSpooler;->checkQueues()V

    .line 150
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v0}, Lcom/squareup/tickets/OpenTicketsSettings;->isOpenTicketsEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 151
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->ticketsSweeperManager:Lcom/squareup/opentickets/TicketsSweeperManager;

    invoke-interface {v0}, Lcom/squareup/opentickets/TicketsSweeperManager;->schedulePeriodicSync()V

    .line 154
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->lockScreenMonitor:Lcom/squareup/permissions/ui/LockScreenMonitor;

    check-cast v0, Lmortar/Scoped;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 155
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->bankAccountSettings:Lcom/squareup/banklinking/BankAccountSettings;

    check-cast v0, Lmortar/Scoped;

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 157
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->additionalMainActivityScopeServices:Ljava/util/Set;

    check-cast v0, Ljava/lang/Iterable;

    .line 215
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmortar/Scoped;

    .line 158
    invoke-virtual {p1, v1}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    goto :goto_1

    .line 160
    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->internetState:Lio/reactivex/Observable;

    const-wide/16 v1, 0x1

    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->skip(J)Lio/reactivex/Observable;

    move-result-object v0

    .line 161
    sget-object v1, Lcom/squareup/ui/main/MainActivityScopeRegistrar$onEnterScope$6;->INSTANCE:Lcom/squareup/ui/main/MainActivityScopeRegistrar$onEnterScope$6;

    check-cast v1, Lio/reactivex/functions/Predicate;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "internetState.skip(1)\n  \u2026e -> state == CONNECTED }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 162
    new-instance v1, Lcom/squareup/ui/main/MainActivityScopeRegistrar$onEnterScope$7;

    invoke-direct {v1, p0}, Lcom/squareup/ui/main/MainActivityScopeRegistrar$onEnterScope$7;-><init>(Lcom/squareup/ui/main/MainActivityScopeRegistrar;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 171
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->internetState:Lio/reactivex/Observable;

    sget-object v1, Lcom/squareup/ui/main/MainActivityScopeRegistrar$onEnterScope$8;->INSTANCE:Lcom/squareup/ui/main/MainActivityScopeRegistrar$onEnterScope$8;

    check-cast v1, Lio/reactivex/functions/Predicate;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "internetState.filter { s\u2026e -> state == CONNECTED }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 172
    new-instance v1, Lcom/squareup/ui/main/MainActivityScopeRegistrar$onEnterScope$9;

    invoke-direct {v1, p0}, Lcom/squareup/ui/main/MainActivityScopeRegistrar$onEnterScope$9;-><init>(Lcom/squareup/ui/main/MainActivityScopeRegistrar;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 175
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->activityVisibilityPresenter:Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;

    new-instance v1, Lcom/squareup/ui/main/MainActivityScopeRegistrar$onEnterScope$10;

    invoke-direct {v1, p0}, Lcom/squareup/ui/main/MainActivityScopeRegistrar$onEnterScope$10;-><init>(Lcom/squareup/ui/main/MainActivityScopeRegistrar;)V

    check-cast v1, Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter$Listener;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter;->register(Lmortar/MortarScope;Lcom/squareup/visibilitypresenter/ActivityVisibilityPresenter$Listener;)V

    return-void
.end method

.method public onExitScope()V
    .locals 1

    .line 183
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->ticketsSweeperManager:Lcom/squareup/opentickets/TicketsSweeperManager;

    invoke-interface {v0}, Lcom/squareup/opentickets/TicketsSweeperManager;->stopSyncing()V

    .line 184
    iget-object v0, p0, Lcom/squareup/ui/main/MainActivityScopeRegistrar;->printerScoutScheduler:Lcom/squareup/print/PrinterScoutScheduler;

    invoke-virtual {v0}, Lcom/squareup/print/PrinterScoutScheduler;->stop()V

    return-void
.end method
