.class public final Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow_Factory;
.super Ljava/lang/Object;
.source "RealSpeedTestWorkflow_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow;",
        ">;"
    }
.end annotation


# instance fields
.field private final clockProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;"
        }
    .end annotation
.end field

.field private final felicaSpeedTestServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/felica/FelicaSpeedTestService;",
            ">;"
        }
    .end annotation
.end field

.field private final speedTestLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/r12education/workflow/SpeedTestLogger;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/felica/FelicaSpeedTestService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/r12education/workflow/SpeedTestLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow_Factory;->felicaSpeedTestServiceProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p2, p0, Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow_Factory;->speedTestLoggerProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p3, p0, Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow_Factory;->clockProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/felica/FelicaSpeedTestService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/r12education/workflow/SpeedTestLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;)",
            "Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow_Factory;"
        }
    .end annotation

    .line 40
    new-instance v0, Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/server/felica/FelicaSpeedTestService;Lcom/squareup/ui/main/r12education/workflow/SpeedTestLogger;Lcom/squareup/util/Clock;)Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow;
    .locals 1

    .line 45
    new-instance v0, Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow;-><init>(Lcom/squareup/server/felica/FelicaSpeedTestService;Lcom/squareup/ui/main/r12education/workflow/SpeedTestLogger;Lcom/squareup/util/Clock;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow;
    .locals 3

    .line 34
    iget-object v0, p0, Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow_Factory;->felicaSpeedTestServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/felica/FelicaSpeedTestService;

    iget-object v1, p0, Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow_Factory;->speedTestLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/main/r12education/workflow/SpeedTestLogger;

    iget-object v2, p0, Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow_Factory;->clockProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/util/Clock;

    invoke-static {v0, v1, v2}, Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow_Factory;->newInstance(Lcom/squareup/server/felica/FelicaSpeedTestService;Lcom/squareup/ui/main/r12education/workflow/SpeedTestLogger;Lcom/squareup/util/Clock;)Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow_Factory;->get()Lcom/squareup/ui/main/r12education/workflow/RealSpeedTestWorkflow;

    move-result-object v0

    return-object v0
.end method
