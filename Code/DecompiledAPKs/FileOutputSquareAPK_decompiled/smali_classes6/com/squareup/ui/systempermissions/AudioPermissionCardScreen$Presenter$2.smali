.class Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter$2;
.super Ljava/lang/Object;
.source "AudioPermissionCardScreen.java"

# interfaces
.implements Lrx/Observer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lrx/Observer<",
        "Lcom/squareup/ui/settings/paymentdevices/ReaderState;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;)V
    .locals 0

    .line 105
    iput-object p1, p0, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter$2;->this$0:Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompleted()V
    .locals 1

    .line 108
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter$2;->this$0:Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;

    invoke-static {v0}, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;->access$000(Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;)Lflow/Flow;

    move-result-object v0

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    return-void
.end method

.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    .line 112
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onNext(Lcom/squareup/ui/settings/paymentdevices/ReaderState;)V
    .locals 0

    return-void
.end method

.method public bridge synthetic onNext(Ljava/lang/Object;)V
    .locals 0

    .line 105
    check-cast p1, Lcom/squareup/ui/settings/paymentdevices/ReaderState;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter$2;->onNext(Lcom/squareup/ui/settings/paymentdevices/ReaderState;)V

    return-void
.end method
