.class public Lcom/squareup/ui/systempermissions/AudioPermissionScreen;
.super Lcom/squareup/ui/settings/InSettingsAppletScope;
.source "AudioPermissionScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/systempermissions/AudioPermissionScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/systempermissions/AudioPermissionScreen$Component;,
        Lcom/squareup/ui/systempermissions/AudioPermissionScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/systempermissions/AudioPermissionScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static INSTANCE:Lcom/squareup/ui/systempermissions/AudioPermissionScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 36
    new-instance v0, Lcom/squareup/ui/systempermissions/AudioPermissionScreen;

    invoke-direct {v0}, Lcom/squareup/ui/systempermissions/AudioPermissionScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/systempermissions/AudioPermissionScreen;->INSTANCE:Lcom/squareup/ui/systempermissions/AudioPermissionScreen;

    .line 132
    sget-object v0, Lcom/squareup/ui/systempermissions/AudioPermissionScreen;->INSTANCE:Lcom/squareup/ui/systempermissions/AudioPermissionScreen;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/systempermissions/AudioPermissionScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 38
    invoke-direct {p0}, Lcom/squareup/ui/settings/InSettingsAppletScope;-><init>()V

    return-void
.end method


# virtual methods
.method public screenLayout()I
    .locals 1

    .line 135
    sget v0, Lcom/squareup/cardreader/ui/R$layout;->audio_permission_screen_view:I

    return v0
.end method
