.class public Lcom/squareup/ui/systempermissions/SystemPermissionRevokedView;
.super Lcom/squareup/ui/StickyNoOverscrollScrollView;
.source "SystemPermissionRevokedView.java"

# interfaces
.implements Lcom/squareup/workflow/ui/HandlesBack;


# instance fields
.field appNameFormatter:Lcom/squareup/util/AppNameFormatter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private bodyView:Lcom/squareup/widgets/MessageView;

.field private enableSettingButton:Lcom/squareup/marketfont/MarketButton;

.field private glyphView:Lcom/squareup/glyph/SquareGlyphView;

.field presenter:Lcom/squareup/ui/systempermissions/SystemPermissionRevokedPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private titleView:Lcom/squareup/widgets/MessageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 34
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/StickyNoOverscrollScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 35
    const-class p2, Lcom/squareup/ui/systempermissions/SystemPermissionRevokedScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/systempermissions/SystemPermissionRevokedScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/systempermissions/SystemPermissionRevokedScreen$Component;->inject(Lcom/squareup/ui/systempermissions/SystemPermissionRevokedView;)V

    return-void
.end method

.method private bindViews()V
    .locals 1

    .line 69
    sget v0, Lcom/squareup/common/bootstrap/R$id;->permission_denied_glyph:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/glyph/SquareGlyphView;

    iput-object v0, p0, Lcom/squareup/ui/systempermissions/SystemPermissionRevokedView;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    .line 70
    sget v0, Lcom/squareup/common/bootstrap/R$id;->permission_denied_title:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/systempermissions/SystemPermissionRevokedView;->titleView:Lcom/squareup/widgets/MessageView;

    .line 71
    sget v0, Lcom/squareup/common/bootstrap/R$id;->permission_denied_body:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/systempermissions/SystemPermissionRevokedView;->bodyView:Lcom/squareup/widgets/MessageView;

    .line 72
    sget v0, Lcom/squareup/common/bootstrap/R$id;->permission_denied_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketButton;

    iput-object v0, p0, Lcom/squareup/ui/systempermissions/SystemPermissionRevokedView;->enableSettingButton:Lcom/squareup/marketfont/MarketButton;

    return-void
.end method


# virtual methods
.method public onBackPressed()Z
    .locals 1

    .line 39
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/SystemPermissionRevokedView;->presenter:Lcom/squareup/ui/systempermissions/SystemPermissionRevokedPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/systempermissions/SystemPermissionRevokedPresenter;->onBackPressed()Z

    move-result v0

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/SystemPermissionRevokedView;->presenter:Lcom/squareup/ui/systempermissions/SystemPermissionRevokedPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/systempermissions/SystemPermissionRevokedPresenter;->dropView(Ljava/lang/Object;)V

    .line 56
    invoke-super {p0}, Lcom/squareup/ui/StickyNoOverscrollScrollView;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 43
    invoke-super {p0}, Lcom/squareup/ui/StickyNoOverscrollScrollView;->onFinishInflate()V

    .line 44
    invoke-direct {p0}, Lcom/squareup/ui/systempermissions/SystemPermissionRevokedView;->bindViews()V

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/SystemPermissionRevokedView;->enableSettingButton:Lcom/squareup/marketfont/MarketButton;

    new-instance v1, Lcom/squareup/ui/systempermissions/SystemPermissionRevokedView$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/systempermissions/SystemPermissionRevokedView$1;-><init>(Lcom/squareup/ui/systempermissions/SystemPermissionRevokedView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 51
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/SystemPermissionRevokedView;->presenter:Lcom/squareup/ui/systempermissions/SystemPermissionRevokedPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/systempermissions/SystemPermissionRevokedPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public setPermission(Lcom/squareup/systempermissions/SystemPermission;I)V
    .locals 3

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/SystemPermissionRevokedView;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    invoke-static {p1}, Lcom/squareup/glyph/GlyphTypeface$Glyph;->permission(Lcom/squareup/systempermissions/SystemPermission;)Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/SystemPermissionRevokedView;->titleView:Lcom/squareup/widgets/MessageView;

    iget-object v1, p0, Lcom/squareup/ui/systempermissions/SystemPermissionRevokedView;->appNameFormatter:Lcom/squareup/util/AppNameFormatter;

    .line 62
    invoke-static {p1}, Lcom/squareup/ui/systempermissions/PermissionMessages;->getExplanationTitle(Lcom/squareup/systempermissions/SystemPermission;)I

    move-result v2

    invoke-interface {v1, v2}, Lcom/squareup/util/AppNameFormatter;->getStringWithAppName(I)Ljava/lang/CharSequence;

    move-result-object v1

    .line 61
    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/SystemPermissionRevokedView;->bodyView:Lcom/squareup/widgets/MessageView;

    iget-object v1, p0, Lcom/squareup/ui/systempermissions/SystemPermissionRevokedView;->appNameFormatter:Lcom/squareup/util/AppNameFormatter;

    .line 64
    invoke-static {p1}, Lcom/squareup/ui/systempermissions/PermissionMessages;->getExplanationBody(Lcom/squareup/systempermissions/SystemPermission;)I

    move-result p1

    invoke-interface {v1, p1}, Lcom/squareup/util/AppNameFormatter;->getStringWithAppName(I)Ljava/lang/CharSequence;

    move-result-object p1

    .line 63
    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 65
    iget-object p1, p0, Lcom/squareup/ui/systempermissions/SystemPermissionRevokedView;->enableSettingButton:Lcom/squareup/marketfont/MarketButton;

    invoke-virtual {p1, p2}, Lcom/squareup/marketfont/MarketButton;->setText(I)V

    return-void
.end method
