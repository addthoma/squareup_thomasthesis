.class public final Lcom/squareup/ui/systempermissions/SystemPermissionRevokedView_MembersInjector;
.super Ljava/lang/Object;
.source "SystemPermissionRevokedView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/systempermissions/SystemPermissionRevokedView;",
        ">;"
    }
.end annotation


# instance fields
.field private final appNameFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/AppNameFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/systempermissions/SystemPermissionRevokedPresenter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/systempermissions/SystemPermissionRevokedPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/AppNameFormatter;",
            ">;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/ui/systempermissions/SystemPermissionRevokedView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    .line 26
    iput-object p2, p0, Lcom/squareup/ui/systempermissions/SystemPermissionRevokedView_MembersInjector;->appNameFormatterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/systempermissions/SystemPermissionRevokedPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/AppNameFormatter;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/systempermissions/SystemPermissionRevokedView;",
            ">;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/ui/systempermissions/SystemPermissionRevokedView_MembersInjector;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/systempermissions/SystemPermissionRevokedView_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectAppNameFormatter(Lcom/squareup/ui/systempermissions/SystemPermissionRevokedView;Lcom/squareup/util/AppNameFormatter;)V
    .locals 0

    .line 49
    iput-object p1, p0, Lcom/squareup/ui/systempermissions/SystemPermissionRevokedView;->appNameFormatter:Lcom/squareup/util/AppNameFormatter;

    return-void
.end method

.method public static injectPresenter(Lcom/squareup/ui/systempermissions/SystemPermissionRevokedView;Lcom/squareup/ui/systempermissions/SystemPermissionRevokedPresenter;)V
    .locals 0

    .line 43
    iput-object p1, p0, Lcom/squareup/ui/systempermissions/SystemPermissionRevokedView;->presenter:Lcom/squareup/ui/systempermissions/SystemPermissionRevokedPresenter;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/systempermissions/SystemPermissionRevokedView;)V
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/SystemPermissionRevokedView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/systempermissions/SystemPermissionRevokedPresenter;

    invoke-static {p1, v0}, Lcom/squareup/ui/systempermissions/SystemPermissionRevokedView_MembersInjector;->injectPresenter(Lcom/squareup/ui/systempermissions/SystemPermissionRevokedView;Lcom/squareup/ui/systempermissions/SystemPermissionRevokedPresenter;)V

    .line 37
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/SystemPermissionRevokedView_MembersInjector;->appNameFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/AppNameFormatter;

    invoke-static {p1, v0}, Lcom/squareup/ui/systempermissions/SystemPermissionRevokedView_MembersInjector;->injectAppNameFormatter(Lcom/squareup/ui/systempermissions/SystemPermissionRevokedView;Lcom/squareup/util/AppNameFormatter;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 9
    check-cast p1, Lcom/squareup/ui/systempermissions/SystemPermissionRevokedView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/systempermissions/SystemPermissionRevokedView_MembersInjector;->injectMembers(Lcom/squareup/ui/systempermissions/SystemPermissionRevokedView;)V

    return-void
.end method
