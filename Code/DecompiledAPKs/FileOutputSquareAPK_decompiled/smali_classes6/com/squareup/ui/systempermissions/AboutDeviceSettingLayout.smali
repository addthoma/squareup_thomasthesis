.class public Lcom/squareup/ui/systempermissions/AboutDeviceSettingLayout;
.super Landroid/widget/LinearLayout;
.source "AboutDeviceSettingLayout.java"


# instance fields
.field private bodyView:Lcom/squareup/widgets/MessageView;

.field private glyphView:Lcom/squareup/glyph/SquareGlyphView;

.field private titleView:Lcom/squareup/widgets/MessageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    .line 21
    invoke-direct {p0, p1, v0}, Lcom/squareup/ui/systempermissions/AboutDeviceSettingLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 25
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    sget p2, Lcom/squareup/widgets/pos/R$layout;->about_device_setting_layout:I

    invoke-static {p1, p2, p0}, Lcom/squareup/ui/systempermissions/AboutDeviceSettingLayout;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 27
    invoke-direct {p0}, Lcom/squareup/ui/systempermissions/AboutDeviceSettingLayout;->bindViews()V

    return-void
.end method

.method private bindViews()V
    .locals 1

    .line 61
    sget v0, Lcom/squareup/widgets/pos/R$id;->about_setting_glyph:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/glyph/SquareGlyphView;

    iput-object v0, p0, Lcom/squareup/ui/systempermissions/AboutDeviceSettingLayout;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    .line 62
    sget v0, Lcom/squareup/widgets/pos/R$id;->about_setting_title:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/systempermissions/AboutDeviceSettingLayout;->titleView:Lcom/squareup/widgets/MessageView;

    .line 63
    sget v0, Lcom/squareup/widgets/pos/R$id;->about_setting_body:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/systempermissions/AboutDeviceSettingLayout;->bodyView:Lcom/squareup/widgets/MessageView;

    return-void
.end method


# virtual methods
.method public setAllGravities(I)V
    .locals 1

    .line 55
    invoke-virtual {p0, p1}, Lcom/squareup/ui/systempermissions/AboutDeviceSettingLayout;->setGravity(I)V

    .line 56
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/AboutDeviceSettingLayout;->titleView:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setGravity(I)V

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/AboutDeviceSettingLayout;->bodyView:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setGravity(I)V

    return-void
.end method

.method public setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V
    .locals 1

    .line 31
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/AboutDeviceSettingLayout;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, p1}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    return-void
.end method

.method public setSubtitle(I)V
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/AboutDeviceSettingLayout;->bodyView:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(I)V

    .line 51
    iget-object p1, p0, Lcom/squareup/ui/systempermissions/AboutDeviceSettingLayout;->bodyView:Lcom/squareup/widgets/MessageView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    return-void
.end method

.method public setSubtitle(Ljava/lang/CharSequence;)V
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/AboutDeviceSettingLayout;->bodyView:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 46
    iget-object p1, p0, Lcom/squareup/ui/systempermissions/AboutDeviceSettingLayout;->bodyView:Lcom/squareup/widgets/MessageView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    return-void
.end method

.method public setTitle(I)V
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/AboutDeviceSettingLayout;->titleView:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(I)V

    .line 41
    iget-object p1, p0, Lcom/squareup/ui/systempermissions/AboutDeviceSettingLayout;->titleView:Lcom/squareup/widgets/MessageView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/AboutDeviceSettingLayout;->titleView:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 36
    iget-object p1, p0, Lcom/squareup/ui/systempermissions/AboutDeviceSettingLayout;->titleView:Lcom/squareup/widgets/MessageView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/MessageView;->setVisibility(I)V

    return-void
.end method
