.class public final Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen_Presenter_Factory;
.super Ljava/lang/Object;
.source "AudioPermissionCardScreen_Presenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final cardReaderHubUtilsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHubUtils;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderOracleProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final systemPermissionsPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialCoreProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHubUtils;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;)V"
        }
    .end annotation

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen_Presenter_Factory;->cardReaderHubUtilsProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p2, p0, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen_Presenter_Factory;->cardReaderOracleProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p3, p0, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen_Presenter_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p4, p0, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p5, p0, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen_Presenter_Factory;->systemPermissionsPresenterProvider:Ljavax/inject/Provider;

    .line 44
    iput-object p6, p0, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen_Presenter_Factory;->tutorialCoreProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen_Presenter_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHubUtils;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tutorialv2/TutorialCore;",
            ">;)",
            "Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen_Presenter_Factory;"
        }
    .end annotation

    .line 58
    new-instance v7, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen_Presenter_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen_Presenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Lcom/squareup/cardreader/CardReaderHubUtils;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lflow/Flow;Lcom/squareup/util/Res;Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;Lcom/squareup/tutorialv2/TutorialCore;)Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;
    .locals 8

    .line 64
    new-instance v7, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;-><init>(Lcom/squareup/cardreader/CardReaderHubUtils;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lflow/Flow;Lcom/squareup/util/Res;Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;Lcom/squareup/tutorialv2/TutorialCore;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;
    .locals 7

    .line 49
    iget-object v0, p0, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen_Presenter_Factory;->cardReaderHubUtilsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/cardreader/CardReaderHubUtils;

    iget-object v0, p0, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen_Presenter_Factory;->cardReaderOracleProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    iget-object v0, p0, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen_Presenter_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lflow/Flow;

    iget-object v0, p0, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen_Presenter_Factory;->systemPermissionsPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;

    iget-object v0, p0, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen_Presenter_Factory;->tutorialCoreProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/tutorialv2/TutorialCore;

    invoke-static/range {v1 .. v6}, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen_Presenter_Factory;->newInstance(Lcom/squareup/cardreader/CardReaderHubUtils;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lflow/Flow;Lcom/squareup/util/Res;Lcom/squareup/ui/systempermissions/SystemPermissionsPresenter;Lcom/squareup/tutorialv2/TutorialCore;)Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen_Presenter_Factory;->get()Lcom/squareup/ui/systempermissions/AudioPermissionCardScreen$Presenter;

    move-result-object v0

    return-object v0
.end method
