.class final Lcom/squareup/ui/photo/ItemPhoto$LibraryItemPhoto;
.super Lcom/squareup/ui/photo/ItemPhoto;
.source "ItemPhoto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/photo/ItemPhoto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "LibraryItemPhoto"
.end annotation


# instance fields
.field private final fromServer:Z

.field private final itemId:Ljava/lang/String;

.field private final loader:Lcom/squareup/ui/photo/ItemPhoto$Loader;

.field private final overrides:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private final uri:Landroid/net/Uri;


# direct methods
.method private constructor <init>(Ljava/lang/String;Landroid/net/Uri;ZLjava/util/Map;Lcom/squareup/ui/photo/ItemPhoto$Loader;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/net/Uri;",
            "Z",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Landroid/net/Uri;",
            ">;",
            "Lcom/squareup/ui/photo/ItemPhoto$Loader;",
            ")V"
        }
    .end annotation

    const/4 v0, 0x0

    .line 180
    invoke-direct {p0, v0}, Lcom/squareup/ui/photo/ItemPhoto;-><init>(Lcom/squareup/ui/photo/ItemPhoto$1;)V

    .line 181
    iput-object p1, p0, Lcom/squareup/ui/photo/ItemPhoto$LibraryItemPhoto;->itemId:Ljava/lang/String;

    .line 182
    iput-object p2, p0, Lcom/squareup/ui/photo/ItemPhoto$LibraryItemPhoto;->uri:Landroid/net/Uri;

    .line 183
    iput-boolean p3, p0, Lcom/squareup/ui/photo/ItemPhoto$LibraryItemPhoto;->fromServer:Z

    .line 184
    iput-object p4, p0, Lcom/squareup/ui/photo/ItemPhoto$LibraryItemPhoto;->overrides:Ljava/util/Map;

    .line 185
    iput-object p5, p0, Lcom/squareup/ui/photo/ItemPhoto$LibraryItemPhoto;->loader:Lcom/squareup/ui/photo/ItemPhoto$Loader;

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Landroid/net/Uri;ZLjava/util/Map;Lcom/squareup/ui/photo/ItemPhoto$Loader;Lcom/squareup/ui/photo/ItemPhoto$1;)V
    .locals 0

    .line 172
    invoke-direct/range {p0 .. p5}, Lcom/squareup/ui/photo/ItemPhoto$LibraryItemPhoto;-><init>(Ljava/lang/String;Landroid/net/Uri;ZLjava/util/Map;Lcom/squareup/ui/photo/ItemPhoto$Loader;)V

    return-void
.end method


# virtual methods
.method public cancel(Lcom/squareup/picasso/Target;)V
    .locals 1

    .line 213
    iget-object v0, p0, Lcom/squareup/ui/photo/ItemPhoto$LibraryItemPhoto;->loader:Lcom/squareup/ui/photo/ItemPhoto$Loader;

    invoke-static {v0}, Lcom/squareup/ui/photo/ItemPhoto$Loader;->access$400(Lcom/squareup/ui/photo/ItemPhoto$Loader;)Lcom/squareup/picasso/Picasso;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/picasso/Picasso;->cancelRequest(Lcom/squareup/picasso/Target;)V

    return-void
.end method

.method public getItemId()Ljava/lang/String;
    .locals 1

    .line 189
    iget-object v0, p0, Lcom/squareup/ui/photo/ItemPhoto$LibraryItemPhoto;->itemId:Ljava/lang/String;

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 2

    .line 201
    iget-object v0, p0, Lcom/squareup/ui/photo/ItemPhoto$LibraryItemPhoto;->overrides:Ljava/util/Map;

    iget-object v1, p0, Lcom/squareup/ui/photo/ItemPhoto$LibraryItemPhoto;->itemId:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    if-eqz v0, :cond_0

    .line 203
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 205
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/photo/ItemPhoto$LibraryItemPhoto;->uri:Landroid/net/Uri;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public hasUrl()Z
    .locals 2

    .line 209
    iget-object v0, p0, Lcom/squareup/ui/photo/ItemPhoto$LibraryItemPhoto;->overrides:Ljava/util/Map;

    iget-object v1, p0, Lcom/squareup/ui/photo/ItemPhoto$LibraryItemPhoto;->itemId:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/photo/ItemPhoto$LibraryItemPhoto;->uri:Landroid/net/Uri;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public into(ILandroid/widget/ImageView;Landroid/graphics/drawable/Drawable;)V
    .locals 6

    .line 217
    iget-object v0, p0, Lcom/squareup/ui/photo/ItemPhoto$LibraryItemPhoto;->overrides:Ljava/util/Map;

    iget-object v1, p0, Lcom/squareup/ui/photo/ItemPhoto$LibraryItemPhoto;->itemId:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 218
    iget-object v0, p0, Lcom/squareup/ui/photo/ItemPhoto$LibraryItemPhoto;->loader:Lcom/squareup/ui/photo/ItemPhoto$Loader;

    iget-object v1, p0, Lcom/squareup/ui/photo/ItemPhoto$LibraryItemPhoto;->overrides:Ljava/util/Map;

    iget-object v2, p0, Lcom/squareup/ui/photo/ItemPhoto$LibraryItemPhoto;->itemId:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2, p1, p3}, Lcom/squareup/ui/photo/ItemPhoto$Loader;->access$500(Lcom/squareup/ui/photo/ItemPhoto$Loader;Landroid/net/Uri;ZILandroid/graphics/drawable/Drawable;)Lcom/squareup/picasso/RequestCreator;

    move-result-object p1

    invoke-virtual {p1, p2}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;)V

    goto :goto_0

    .line 220
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/photo/ItemPhoto$LibraryItemPhoto;->loader:Lcom/squareup/ui/photo/ItemPhoto$Loader;

    iget-object v1, p0, Lcom/squareup/ui/photo/ItemPhoto$LibraryItemPhoto;->uri:Landroid/net/Uri;

    iget-boolean v2, p0, Lcom/squareup/ui/photo/ItemPhoto$LibraryItemPhoto;->fromServer:Z

    move v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/squareup/ui/photo/ItemPhoto$Loader;->load(Landroid/net/Uri;ZILandroid/widget/ImageView;Landroid/graphics/drawable/Drawable;)V

    :goto_0
    return-void
.end method

.method public into(ILcom/squareup/picasso/Target;)V
    .locals 3

    .line 193
    iget-object v0, p0, Lcom/squareup/ui/photo/ItemPhoto$LibraryItemPhoto;->overrides:Ljava/util/Map;

    iget-object v1, p0, Lcom/squareup/ui/photo/ItemPhoto$LibraryItemPhoto;->itemId:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 194
    iget-object v0, p0, Lcom/squareup/ui/photo/ItemPhoto$LibraryItemPhoto;->loader:Lcom/squareup/ui/photo/ItemPhoto$Loader;

    iget-object v1, p0, Lcom/squareup/ui/photo/ItemPhoto$LibraryItemPhoto;->overrides:Ljava/util/Map;

    iget-object v2, p0, Lcom/squareup/ui/photo/ItemPhoto$LibraryItemPhoto;->itemId:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1, p2}, Lcom/squareup/ui/photo/ItemPhoto$Loader;->load(Landroid/net/Uri;ZILcom/squareup/picasso/Target;)V

    goto :goto_0

    .line 196
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/photo/ItemPhoto$LibraryItemPhoto;->loader:Lcom/squareup/ui/photo/ItemPhoto$Loader;

    iget-object v1, p0, Lcom/squareup/ui/photo/ItemPhoto$LibraryItemPhoto;->uri:Landroid/net/Uri;

    iget-boolean v2, p0, Lcom/squareup/ui/photo/ItemPhoto$LibraryItemPhoto;->fromServer:Z

    invoke-virtual {v0, v1, v2, p1, p2}, Lcom/squareup/ui/photo/ItemPhoto$Loader;->load(Landroid/net/Uri;ZILcom/squareup/picasso/Target;)V

    :goto_0
    return-void
.end method

.method public withNewUri(Landroid/net/Uri;)Lcom/squareup/ui/photo/ItemPhoto;
    .locals 7

    .line 225
    new-instance v6, Lcom/squareup/ui/photo/ItemPhoto$LibraryItemPhoto;

    iget-object v1, p0, Lcom/squareup/ui/photo/ItemPhoto$LibraryItemPhoto;->itemId:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/squareup/ui/photo/ItemPhoto$LibraryItemPhoto;->fromServer:Z

    iget-object v4, p0, Lcom/squareup/ui/photo/ItemPhoto$LibraryItemPhoto;->overrides:Ljava/util/Map;

    iget-object v5, p0, Lcom/squareup/ui/photo/ItemPhoto$LibraryItemPhoto;->loader:Lcom/squareup/ui/photo/ItemPhoto$Loader;

    move-object v0, v6

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/photo/ItemPhoto$LibraryItemPhoto;-><init>(Ljava/lang/String;Landroid/net/Uri;ZLjava/util/Map;Lcom/squareup/ui/photo/ItemPhoto$Loader;)V

    return-object v6
.end method
