.class final Lcom/squareup/ui/photo/ItemPhoto$CustomItemPhoto;
.super Lcom/squareup/ui/photo/ItemPhoto;
.source "ItemPhoto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/photo/ItemPhoto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "CustomItemPhoto"
.end annotation


# instance fields
.field private final fromServer:Z

.field private final imageUri:Landroid/net/Uri;

.field private final loader:Lcom/squareup/ui/photo/ItemPhoto$Loader;


# direct methods
.method private constructor <init>(Landroid/net/Uri;ZLcom/squareup/ui/photo/ItemPhoto$Loader;)V
    .locals 1

    const/4 v0, 0x0

    .line 235
    invoke-direct {p0, v0}, Lcom/squareup/ui/photo/ItemPhoto;-><init>(Lcom/squareup/ui/photo/ItemPhoto$1;)V

    .line 236
    iput-object p1, p0, Lcom/squareup/ui/photo/ItemPhoto$CustomItemPhoto;->imageUri:Landroid/net/Uri;

    .line 237
    iput-boolean p2, p0, Lcom/squareup/ui/photo/ItemPhoto$CustomItemPhoto;->fromServer:Z

    .line 238
    iput-object p3, p0, Lcom/squareup/ui/photo/ItemPhoto$CustomItemPhoto;->loader:Lcom/squareup/ui/photo/ItemPhoto$Loader;

    return-void
.end method

.method synthetic constructor <init>(Landroid/net/Uri;ZLcom/squareup/ui/photo/ItemPhoto$Loader;Lcom/squareup/ui/photo/ItemPhoto$1;)V
    .locals 0

    .line 229
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/ui/photo/ItemPhoto$CustomItemPhoto;-><init>(Landroid/net/Uri;ZLcom/squareup/ui/photo/ItemPhoto$Loader;)V

    return-void
.end method


# virtual methods
.method public cancel(Lcom/squareup/picasso/Target;)V
    .locals 1

    .line 254
    iget-object v0, p0, Lcom/squareup/ui/photo/ItemPhoto$CustomItemPhoto;->loader:Lcom/squareup/ui/photo/ItemPhoto$Loader;

    invoke-static {v0}, Lcom/squareup/ui/photo/ItemPhoto$Loader;->access$400(Lcom/squareup/ui/photo/ItemPhoto$Loader;)Lcom/squareup/picasso/Picasso;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/picasso/Picasso;->cancelRequest(Lcom/squareup/picasso/Target;)V

    return-void
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .line 246
    iget-object v0, p0, Lcom/squareup/ui/photo/ItemPhoto$CustomItemPhoto;->imageUri:Landroid/net/Uri;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public hasUrl()Z
    .locals 1

    .line 250
    iget-object v0, p0, Lcom/squareup/ui/photo/ItemPhoto$CustomItemPhoto;->imageUri:Landroid/net/Uri;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public into(ILandroid/widget/ImageView;Landroid/graphics/drawable/Drawable;)V
    .locals 6

    .line 258
    iget-object v0, p0, Lcom/squareup/ui/photo/ItemPhoto$CustomItemPhoto;->loader:Lcom/squareup/ui/photo/ItemPhoto$Loader;

    iget-object v1, p0, Lcom/squareup/ui/photo/ItemPhoto$CustomItemPhoto;->imageUri:Landroid/net/Uri;

    iget-boolean v2, p0, Lcom/squareup/ui/photo/ItemPhoto$CustomItemPhoto;->fromServer:Z

    move v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/squareup/ui/photo/ItemPhoto$Loader;->load(Landroid/net/Uri;ZILandroid/widget/ImageView;Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public into(ILcom/squareup/picasso/Target;)V
    .locals 3

    .line 242
    iget-object v0, p0, Lcom/squareup/ui/photo/ItemPhoto$CustomItemPhoto;->loader:Lcom/squareup/ui/photo/ItemPhoto$Loader;

    iget-object v1, p0, Lcom/squareup/ui/photo/ItemPhoto$CustomItemPhoto;->imageUri:Landroid/net/Uri;

    iget-boolean v2, p0, Lcom/squareup/ui/photo/ItemPhoto$CustomItemPhoto;->fromServer:Z

    invoke-virtual {v0, v1, v2, p1, p2}, Lcom/squareup/ui/photo/ItemPhoto$Loader;->load(Landroid/net/Uri;ZILcom/squareup/picasso/Target;)V

    return-void
.end method

.method public withNewUri(Landroid/net/Uri;)Lcom/squareup/ui/photo/ItemPhoto;
    .locals 3

    .line 262
    new-instance v0, Lcom/squareup/ui/photo/ItemPhoto$CustomItemPhoto;

    iget-boolean v1, p0, Lcom/squareup/ui/photo/ItemPhoto$CustomItemPhoto;->fromServer:Z

    iget-object v2, p0, Lcom/squareup/ui/photo/ItemPhoto$CustomItemPhoto;->loader:Lcom/squareup/ui/photo/ItemPhoto$Loader;

    invoke-direct {v0, p1, v1, v2}, Lcom/squareup/ui/photo/ItemPhoto$CustomItemPhoto;-><init>(Landroid/net/Uri;ZLcom/squareup/ui/photo/ItemPhoto$Loader;)V

    return-object v0
.end method
