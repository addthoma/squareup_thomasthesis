.class public Lcom/squareup/ui/tender/LegacyCancelSplitTenderDialogScreen;
.super Lcom/squareup/ui/tender/InTenderScope;
.source "LegacyCancelSplitTenderDialogScreen.java"


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/cancelsplit/CancelSplitTenderTransactionDialogFactory;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/tender/LegacyCancelSplitTenderDialogScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/tender/LegacyCancelSplitTenderDialogScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 15
    new-instance v0, Lcom/squareup/ui/tender/LegacyCancelSplitTenderDialogScreen;

    invoke-direct {v0}, Lcom/squareup/ui/tender/LegacyCancelSplitTenderDialogScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/tender/LegacyCancelSplitTenderDialogScreen;->INSTANCE:Lcom/squareup/ui/tender/LegacyCancelSplitTenderDialogScreen;

    .line 17
    sget-object v0, Lcom/squareup/ui/tender/LegacyCancelSplitTenderDialogScreen;->INSTANCE:Lcom/squareup/ui/tender/LegacyCancelSplitTenderDialogScreen;

    .line 18
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/tender/LegacyCancelSplitTenderDialogScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .line 21
    sget-object v0, Lcom/squareup/ui/tender/TenderScope;->INSTANCE:Lcom/squareup/ui/tender/TenderScope;

    invoke-direct {p0, v0}, Lcom/squareup/ui/tender/InTenderScope;-><init>(Lcom/squareup/ui/tender/TenderScopeTreeKey;)V

    return-void
.end method
