.class public final Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;
.super Ljava/lang/Object;
.source "RealTenderPaymentResultHandler.kt"

# interfaces
.implements Lcom/squareup/tenderpayment/TenderPaymentResultHandler;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0096\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u0097\u0001\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u0012\u0006\u0010\u0014\u001a\u00020\u0015\u0012\u0006\u0010\u0016\u001a\u00020\u0017\u0012\u0006\u0010\u0018\u001a\u00020\u0019\u0012\u0006\u0010\u001a\u001a\u00020\u001b\u0012\u0006\u0010\u001c\u001a\u00020\u001d\u0012\u0006\u0010\u001e\u001a\u00020\u001f\u0012\u0006\u0010 \u001a\u00020!\u0012\u0006\u0010\"\u001a\u00020#\u0012\u0006\u0010$\u001a\u00020%\u00a2\u0006\u0002\u0010&J\u000e\u0010\'\u001a\u00020(2\u0006\u0010)\u001a\u00020*J\u0008\u0010+\u001a\u00020(H\u0002J\u0008\u0010,\u001a\u00020(H\u0002J\u0010\u0010-\u001a\u00020(2\u0006\u0010.\u001a\u00020/H\u0016J\u0008\u00100\u001a\u00020(H\u0002J\u0012\u00101\u001a\u00020(2\u0008\u00102\u001a\u0004\u0018\u000103H\u0002J\u0008\u00104\u001a\u00020(H\u0002R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010 \u001a\u00020!X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001e\u001a\u00020\u001fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u001dX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010$\u001a\u00020%X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\"\u001a\u00020#X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00065"
    }
    d2 = {
        "Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;",
        "Lcom/squareup/tenderpayment/TenderPaymentResultHandler;",
        "transaction",
        "Lcom/squareup/payment/Transaction;",
        "checkoutWorkflowRunner",
        "Lcom/squareup/ui/main/CheckoutWorkflowRunner;",
        "orderEntryApplet",
        "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
        "tenderStarter",
        "Lcom/squareup/ui/tender/TenderStarter;",
        "editInvoiceInTenderRunner",
        "Lcom/squareup/invoicesappletapi/EditInvoiceInTenderRunner;",
        "flow",
        "Lflow/Flow;",
        "cardSellerWorkflow",
        "Lcom/squareup/tenderpayment/CardSellerWorkflow;",
        "settings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "buyerFlowStarter",
        "Lcom/squareup/ui/buyer/BuyerFlowStarter;",
        "swipeHandler",
        "Lcom/squareup/ui/payment/SwipeHandler;",
        "smartPaymentFlowStarter",
        "Lcom/squareup/ui/main/SmartPaymentFlowStarter;",
        "paymentInputHandler",
        "Lcom/squareup/ui/main/errors/PaymentInputHandler;",
        "invoicesAppletRunner",
        "Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;",
        "tenderCompleter",
        "Lcom/squareup/tenderpayment/TenderCompleter;",
        "posContainer",
        "Lcom/squareup/ui/main/PosContainer;",
        "paymentProcessingEventSink",
        "Lcom/squareup/checkoutflow/PaymentProcessingEventSink;",
        "tenderInEdit",
        "Lcom/squareup/payment/TenderInEdit;",
        "tenderFactory",
        "Lcom/squareup/payment/tender/TenderFactory;",
        "(Lcom/squareup/payment/Transaction;Lcom/squareup/ui/main/CheckoutWorkflowRunner;Lcom/squareup/orderentry/OrderEntryAppletGateway;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/invoicesappletapi/EditInvoiceInTenderRunner;Lflow/Flow;Lcom/squareup/tenderpayment/CardSellerWorkflow;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/buyer/BuyerFlowStarter;Lcom/squareup/ui/payment/SwipeHandler;Lcom/squareup/ui/main/SmartPaymentFlowStarter;Lcom/squareup/ui/main/errors/PaymentInputHandler;Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;Lcom/squareup/tenderpayment/TenderCompleter;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/checkoutflow/PaymentProcessingEventSink;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/payment/tender/TenderFactory;)V",
        "cancelPaymentAndExit",
        "",
        "reason",
        "Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;",
        "cancelPaymentAndShowTimeoutError",
        "goBackPastPaymentWorkflow",
        "handlePaymentResult",
        "result",
        "Lcom/squareup/tenderpayment/TenderPaymentResult;",
        "resetTransactionAndGoToInvoiceDetail",
        "startCardNotPresent",
        "manualCardEntryScreenData",
        "Lcom/squareup/checkoutflow/selecttender/tenderoption/ManualCardEntryScreenData;",
        "startGiftCard",
        "tender-payment-legacy_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final buyerFlowStarter:Lcom/squareup/ui/buyer/BuyerFlowStarter;

.field private final cardSellerWorkflow:Lcom/squareup/tenderpayment/CardSellerWorkflow;

.field private final checkoutWorkflowRunner:Lcom/squareup/ui/main/CheckoutWorkflowRunner;

.field private final editInvoiceInTenderRunner:Lcom/squareup/invoicesappletapi/EditInvoiceInTenderRunner;

.field private final flow:Lflow/Flow;

.field private final invoicesAppletRunner:Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;

.field private final orderEntryApplet:Lcom/squareup/orderentry/OrderEntryAppletGateway;

.field private final paymentInputHandler:Lcom/squareup/ui/main/errors/PaymentInputHandler;

.field private final paymentProcessingEventSink:Lcom/squareup/checkoutflow/PaymentProcessingEventSink;

.field private final posContainer:Lcom/squareup/ui/main/PosContainer;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final smartPaymentFlowStarter:Lcom/squareup/ui/main/SmartPaymentFlowStarter;

.field private final swipeHandler:Lcom/squareup/ui/payment/SwipeHandler;

.field private final tenderCompleter:Lcom/squareup/tenderpayment/TenderCompleter;

.field private final tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

.field private final tenderInEdit:Lcom/squareup/payment/TenderInEdit;

.field private final tenderStarter:Lcom/squareup/ui/tender/TenderStarter;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method public constructor <init>(Lcom/squareup/payment/Transaction;Lcom/squareup/ui/main/CheckoutWorkflowRunner;Lcom/squareup/orderentry/OrderEntryAppletGateway;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/invoicesappletapi/EditInvoiceInTenderRunner;Lflow/Flow;Lcom/squareup/tenderpayment/CardSellerWorkflow;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/buyer/BuyerFlowStarter;Lcom/squareup/ui/payment/SwipeHandler;Lcom/squareup/ui/main/SmartPaymentFlowStarter;Lcom/squareup/ui/main/errors/PaymentInputHandler;Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;Lcom/squareup/tenderpayment/TenderCompleter;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/checkoutflow/PaymentProcessingEventSink;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/payment/tender/TenderFactory;)V
    .locals 16
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    move-object/from16 v14, p14

    move-object/from16 v15, p15

    move-object/from16 v0, p16

    const-string v0, "transaction"

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "checkoutWorkflowRunner"

    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderEntryApplet"

    invoke-static {v3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tenderStarter"

    invoke-static {v4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "editInvoiceInTenderRunner"

    invoke-static {v5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "flow"

    invoke-static {v6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cardSellerWorkflow"

    invoke-static {v7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "settings"

    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "buyerFlowStarter"

    invoke-static {v9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "swipeHandler"

    invoke-static {v10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "smartPaymentFlowStarter"

    invoke-static {v11, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentInputHandler"

    invoke-static {v12, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "invoicesAppletRunner"

    invoke-static {v13, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tenderCompleter"

    invoke-static {v14, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "posContainer"

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "paymentProcessingEventSink"

    move-object/from16 v15, p16

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tenderInEdit"

    move-object/from16 v15, p17

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tenderFactory"

    move-object/from16 v15, p18

    invoke-static {v15, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    move-object/from16 v0, p0

    move-object/from16 v15, p16

    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->transaction:Lcom/squareup/payment/Transaction;

    iput-object v2, v0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->checkoutWorkflowRunner:Lcom/squareup/ui/main/CheckoutWorkflowRunner;

    iput-object v3, v0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->orderEntryApplet:Lcom/squareup/orderentry/OrderEntryAppletGateway;

    iput-object v4, v0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->tenderStarter:Lcom/squareup/ui/tender/TenderStarter;

    iput-object v5, v0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->editInvoiceInTenderRunner:Lcom/squareup/invoicesappletapi/EditInvoiceInTenderRunner;

    iput-object v6, v0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->flow:Lflow/Flow;

    iput-object v7, v0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->cardSellerWorkflow:Lcom/squareup/tenderpayment/CardSellerWorkflow;

    iput-object v8, v0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iput-object v9, v0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->buyerFlowStarter:Lcom/squareup/ui/buyer/BuyerFlowStarter;

    iput-object v10, v0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->swipeHandler:Lcom/squareup/ui/payment/SwipeHandler;

    iput-object v11, v0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->smartPaymentFlowStarter:Lcom/squareup/ui/main/SmartPaymentFlowStarter;

    iput-object v12, v0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->paymentInputHandler:Lcom/squareup/ui/main/errors/PaymentInputHandler;

    iput-object v13, v0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->invoicesAppletRunner:Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;

    iput-object v14, v0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->tenderCompleter:Lcom/squareup/tenderpayment/TenderCompleter;

    move-object/from16 v1, p15

    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->posContainer:Lcom/squareup/ui/main/PosContainer;

    iput-object v15, v0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->paymentProcessingEventSink:Lcom/squareup/checkoutflow/PaymentProcessingEventSink;

    move-object/from16 v1, p17

    move-object/from16 v2, p18

    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    iput-object v2, v0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

    return-void
.end method

.method public static final synthetic access$getCheckoutWorkflowRunner$p(Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;)Lcom/squareup/ui/main/CheckoutWorkflowRunner;
    .locals 0

    .line 71
    iget-object p0, p0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->checkoutWorkflowRunner:Lcom/squareup/ui/main/CheckoutWorkflowRunner;

    return-object p0
.end method

.method private final cancelPaymentAndShowTimeoutError()V
    .locals 4

    .line 256
    iget-object v0, p0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->tenderCompleter:Lcom/squareup/tenderpayment/TenderCompleter;

    sget-object v1, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->CANCEL_BILL_READER_INITIATED:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    invoke-interface {v0, v1}, Lcom/squareup/tenderpayment/TenderCompleter;->cancelPaymentFlow(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;

    .line 257
    iget-object v0, p0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->paymentInputHandler:Lcom/squareup/ui/main/errors/PaymentInputHandler;

    new-instance v1, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowNfcWarningScreen;

    sget-object v2, Lcom/squareup/ui/main/errors/ConcreteWarningScreens$PaymentTimedOut;->INSTANCE:Lcom/squareup/ui/main/errors/ConcreteWarningScreens$PaymentTimedOut;

    const-string v3, "PaymentTimedOut.INSTANCE"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lcom/squareup/container/ContainerTreeKey;

    invoke-direct {v1, v2}, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest$ShowNfcWarningScreen;-><init>(Lcom/squareup/container/ContainerTreeKey;)V

    check-cast v1, Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/main/errors/PaymentInputHandler;->navigateForReaderIssueScreen(Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;)V

    return-void
.end method

.method private final goBackPastPaymentWorkflow()V
    .locals 3

    .line 205
    iget-object v0, p0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/container/CalculatedKey;

    new-instance v2, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler$goBackPastPaymentWorkflow$1;

    invoke-direct {v2, p0}, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler$goBackPastPaymentWorkflow$1;-><init>(Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;)V

    check-cast v2, Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;

    invoke-direct {v1, v2}, Lcom/squareup/container/CalculatedKey;-><init>(Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method private final resetTransactionAndGoToInvoiceDetail()V
    .locals 2

    .line 244
    iget-object v0, p0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requireInvoiceId()Ljava/lang/String;

    move-result-object v0

    .line 245
    iget-object v1, p0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->reset()V

    .line 246
    iget-object v1, p0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->invoicesAppletRunner:Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;

    invoke-interface {v1, v0}, Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;->invoicePaymentCanceled(Ljava/lang/String;)V

    return-void
.end method

.method private final startCardNotPresent(Lcom/squareup/checkoutflow/selecttender/tenderoption/ManualCardEntryScreenData;)V
    .locals 3

    .line 218
    iget-object v0, p0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->cardSellerWorkflow:Lcom/squareup/tenderpayment/CardSellerWorkflow;

    invoke-interface {v0}, Lcom/squareup/tenderpayment/CardSellerWorkflow;->startCardNotPresent()Lcom/squareup/tenderpayment/CardSellerWorkflow$Result;

    move-result-object v0

    .line 219
    sget-object v1, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/tenderpayment/CardSellerWorkflow$Result;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 225
    iget-object v0, p0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->flow:Lflow/Flow;

    .line 226
    new-instance v1, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreen;

    sget-object v2, Lcom/squareup/ui/tender/TenderScope;->INSTANCE:Lcom/squareup/ui/tender/TenderScope;

    check-cast v2, Lcom/squareup/ui/tender/TenderScopeTreeKey;

    invoke-direct {v1, v2, p1}, Lcom/squareup/checkoutflow/manualcardentry/PayCreditCardScreen;-><init>(Lcom/squareup/ui/tender/TenderScopeTreeKey;Lcom/squareup/checkoutflow/selecttender/tenderoption/ManualCardEntryScreenData;)V

    .line 225
    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 228
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Unexpected result from CardSellerWorkflow."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 224
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->flow:Lflow/Flow;

    sget-object v0, Lcom/squareup/ui/tender/PayCardSwipeOnlyScreen;->INSTANCE:Lcom/squareup/ui/tender/PayCardSwipeOnlyScreen;

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 223
    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->flow:Lflow/Flow;

    sget-object v0, Lcom/squareup/ui/tender/PayCardCnpDisabledScreen;->INSTANCE:Lcom/squareup/ui/tender/PayCardCnpDisabledScreen;

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    :cond_3
    :goto_0
    return-void
.end method

.method private final startGiftCard()V
    .locals 3

    .line 233
    iget-object v0, p0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->cardSellerWorkflow:Lcom/squareup/tenderpayment/CardSellerWorkflow;

    invoke-interface {v0}, Lcom/squareup/tenderpayment/CardSellerWorkflow;->startGiftCard()Lcom/squareup/tenderpayment/CardSellerWorkflow$Result;

    move-result-object v0

    .line 234
    sget-object v1, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {v0}, Lcom/squareup/tenderpayment/CardSellerWorkflow$Result;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 238
    iget-object v0, p0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreen;

    sget-object v2, Lcom/squareup/ui/tender/TenderScope;->INSTANCE:Lcom/squareup/ui/tender/TenderScope;

    check-cast v2, Lcom/squareup/ui/tender/TenderScopeTreeKey;

    invoke-direct {v1, v2}, Lcom/squareup/checkoutflow/manualcardentry/PayGiftCardScreen;-><init>(Lcom/squareup/ui/tender/TenderScopeTreeKey;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 239
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unexpected result from CardSellerWorkflow."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_1
    :goto_0
    return-void
.end method


# virtual methods
.method public final cancelPaymentAndExit(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V
    .locals 1

    const-string v0, "reason"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 250
    iget-object v0, p0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->tenderCompleter:Lcom/squareup/tenderpayment/TenderCompleter;

    invoke-interface {v0, p1}, Lcom/squareup/tenderpayment/TenderCompleter;->cancelPaymentFlow(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;

    move-result-object p1

    sget-object v0, Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;->ENDED_API_TRANSACTION:Lcom/squareup/tenderpayment/TenderCompleter$CancelPaymentResult;

    if-eq p1, v0, :cond_0

    .line 251
    iget-object p1, p0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->orderEntryApplet:Lcom/squareup/orderentry/OrderEntryAppletGateway;

    invoke-interface {p1}, Lcom/squareup/orderentry/OrderEntryAppletGateway;->activateApplet()V

    :cond_0
    return-void
.end method

.method public handlePaymentResult(Lcom/squareup/tenderpayment/TenderPaymentResult;)V
    .locals 5

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 93
    instance-of v0, p1, Lcom/squareup/tenderpayment/TenderPaymentResult$CreateInvoice;

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->editInvoiceInTenderRunner:Lcom/squareup/invoicesappletapi/EditInvoiceInTenderRunner;

    invoke-interface {p1}, Lcom/squareup/invoicesappletapi/EditInvoiceInTenderRunner;->start()V

    goto/16 :goto_0

    .line 94
    :cond_0
    instance-of v0, p1, Lcom/squareup/tenderpayment/TenderPaymentResult$PayCashCustomAmount;

    if-eqz v0, :cond_1

    iget-object p1, p0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->flow:Lflow/Flow;

    sget-object v0, Lcom/squareup/ui/tender/PayCashScreen;->INSTANCE:Lcom/squareup/ui/tender/PayCashScreen;

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 95
    :cond_1
    instance-of v0, p1, Lcom/squareup/tenderpayment/TenderPaymentResult$PayCard;

    if-eqz v0, :cond_2

    check-cast p1, Lcom/squareup/tenderpayment/TenderPaymentResult$PayCard;

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/TenderPaymentResult$PayCard;->getScreenData()Lcom/squareup/checkoutflow/selecttender/tenderoption/ManualCardEntryScreenData;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->startCardNotPresent(Lcom/squareup/checkoutflow/selecttender/tenderoption/ManualCardEntryScreenData;)V

    goto/16 :goto_0

    .line 96
    :cond_2
    instance-of v0, p1, Lcom/squareup/tenderpayment/TenderPaymentResult$PayCardOnFileWithCurrentCustomer;

    if-eqz v0, :cond_3

    .line 97
    iget-object p1, p0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->flow:Lflow/Flow;

    .line 98
    sget-object v0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$CardType;->ALL:Lcom/squareup/ui/tender/ChooseCardOnFileScreen$CardType;

    invoke-static {v0}, Lcom/squareup/ui/tender/ChooseCardOnFileScreen;->forCustomerInTransaction(Lcom/squareup/ui/tender/ChooseCardOnFileScreen$CardType;)Lcom/squareup/ui/tender/ChooseCardOnFileScreen;

    move-result-object v0

    .line 97
    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 100
    :cond_3
    instance-of v0, p1, Lcom/squareup/tenderpayment/TenderPaymentResult$SelectCustomerAndPayCardOnFile;

    if-eqz v0, :cond_4

    .line 101
    iget-object p1, p0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->flow:Lflow/Flow;

    .line 102
    sget-object v0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$CardType;->ALL:Lcom/squareup/ui/tender/ChooseCardOnFileScreen$CardType;

    invoke-static {v0}, Lcom/squareup/ui/tender/ChooseCardOnFileCustomerScreenV2;->create(Lcom/squareup/ui/tender/ChooseCardOnFileScreen$CardType;)Lcom/squareup/ui/tender/InTenderScope;

    move-result-object v0

    .line 101
    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 104
    :cond_4
    instance-of v0, p1, Lcom/squareup/tenderpayment/TenderPaymentResult$PayGiftCard;

    if-eqz v0, :cond_5

    invoke-direct {p0}, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->startGiftCard()V

    goto/16 :goto_0

    .line 105
    :cond_5
    instance-of v0, p1, Lcom/squareup/tenderpayment/TenderPaymentResult$PayGiftCardOnFileWithCurrentCustomer;

    if-eqz v0, :cond_6

    .line 106
    iget-object p1, p0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->flow:Lflow/Flow;

    .line 107
    sget-object v0, Lcom/squareup/ui/tender/ChooseCardOnFileScreen$CardType;->GIFT_CARD:Lcom/squareup/ui/tender/ChooseCardOnFileScreen$CardType;

    invoke-static {v0}, Lcom/squareup/ui/tender/ChooseCardOnFileScreen;->forCustomerInTransaction(Lcom/squareup/ui/tender/ChooseCardOnFileScreen$CardType;)Lcom/squareup/ui/tender/ChooseCardOnFileScreen;

    move-result-object v0

    .line 106
    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 109
    :cond_6
    instance-of v0, p1, Lcom/squareup/tenderpayment/TenderPaymentResult$PayContactless;

    if-eqz v0, :cond_8

    .line 110
    iget-object p1, p0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->hasSplitTenderBillPayment()Z

    move-result p1

    if-eqz p1, :cond_7

    .line 111
    iget-object p1, p0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->tenderStarter:Lcom/squareup/ui/tender/TenderStarter;

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Lcom/squareup/ui/tender/TenderStarter;->completeTenderAndAdvance(Z)Z

    goto/16 :goto_0

    .line 114
    :cond_7
    iget-object p1, p0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->smartPaymentFlowStarter:Lcom/squareup/ui/main/SmartPaymentFlowStarter;

    invoke-virtual {p1}, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->getPreAuthContactlessSingleTenderInBuyerFlowResult()Lcom/squareup/ui/main/SmartPaymentResult;

    move-result-object p1

    .line 115
    iget-object v0, p0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->smartPaymentFlowStarter:Lcom/squareup/ui/main/SmartPaymentFlowStarter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->navigateForSmartPaymentResult(Lcom/squareup/ui/main/SmartPaymentResult;)V

    goto/16 :goto_0

    .line 117
    :cond_8
    instance-of v0, p1, Lcom/squareup/tenderpayment/TenderPaymentResult$RecordCardPayment;

    const-string v1, ""

    if-eqz v0, :cond_9

    .line 118
    iget-object p1, p0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->flow:Lflow/Flow;

    .line 119
    new-instance v0, Lcom/squareup/ui/tender/PayThirdPartyCardScreen;

    .line 120
    iget-object v2, p0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v2}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object v2

    const-string v3, "settings.paymentSettings"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/squareup/settings/server/PaymentSettings;->getThirdPartyOtherTenderType()Lcom/squareup/server/account/protos/OtherTenderType;

    move-result-object v2

    .line 119
    invoke-direct {v0, v2, v1}, Lcom/squareup/ui/tender/PayThirdPartyCardScreen;-><init>(Lcom/squareup/server/account/protos/OtherTenderType;Ljava/lang/String;)V

    .line 118
    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 123
    :cond_9
    instance-of v0, p1, Lcom/squareup/tenderpayment/TenderPaymentResult$RecordCardPaymentDebitCredit;

    const/4 v2, 0x1

    if-eqz v0, :cond_a

    .line 124
    iget-object p1, p0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->flow:Lflow/Flow;

    .line 125
    new-instance v0, Lcom/squareup/ui/tender/PayThirdPartyCardScreen;

    .line 126
    new-instance v3, Lcom/squareup/server/account/protos/OtherTenderType$Builder;

    invoke-direct {v3}, Lcom/squareup/server/account/protos/OtherTenderType$Builder;-><init>()V

    .line 127
    sget-object v4, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->DEBIT_OR_CREDIT:Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;

    invoke-virtual {v4}, Lcom/squareup/protos/client/bills/OtherTender$OtherTenderType;->getValue()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/squareup/server/account/protos/OtherTenderType$Builder;->tender_type(Ljava/lang/Integer;)Lcom/squareup/server/account/protos/OtherTenderType$Builder;

    move-result-object v3

    .line 128
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/squareup/server/account/protos/OtherTenderType$Builder;->accepts_tips(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/OtherTenderType$Builder;

    move-result-object v2

    .line 129
    invoke-virtual {v2}, Lcom/squareup/server/account/protos/OtherTenderType$Builder;->build()Lcom/squareup/server/account/protos/OtherTenderType;

    move-result-object v2

    .line 125
    invoke-direct {v0, v2, v1}, Lcom/squareup/ui/tender/PayThirdPartyCardScreen;-><init>(Lcom/squareup/server/account/protos/OtherTenderType;Ljava/lang/String;)V

    .line 124
    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 133
    :cond_a
    instance-of v0, p1, Lcom/squareup/tenderpayment/TenderPaymentResult$Paid;

    if-eqz v0, :cond_b

    iget-object p1, p0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->buyerFlowStarter:Lcom/squareup/ui/buyer/BuyerFlowStarter;

    invoke-interface {p1}, Lcom/squareup/ui/buyer/BuyerFlowStarter;->startBuyerFlow()V

    goto/16 :goto_0

    .line 134
    :cond_b
    instance-of v0, p1, Lcom/squareup/tenderpayment/TenderPaymentResult$PaidNeedToAuthorize;

    if-eqz v0, :cond_c

    iget-object p1, p0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->buyerFlowStarter:Lcom/squareup/ui/buyer/BuyerFlowStarter;

    invoke-interface {p1, v2}, Lcom/squareup/ui/buyer/BuyerFlowStarter;->startBuyerFlowRecreatingSellerFlow(Z)V

    goto/16 :goto_0

    .line 135
    :cond_c
    instance-of v0, p1, Lcom/squareup/tenderpayment/TenderPaymentResult$CanceledApiPayment;

    if-eqz v0, :cond_d

    .line 137
    iget-object p1, p0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->paymentProcessingEventSink:Lcom/squareup/checkoutflow/PaymentProcessingEventSink;

    sget-object v0, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->CANCEL_BILL_HUMAN_INITIATED:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    invoke-interface {p1, v0}, Lcom/squareup/checkoutflow/PaymentProcessingEventSink;->paymentCanceled(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V

    goto/16 :goto_0

    .line 139
    :cond_d
    instance-of v0, p1, Lcom/squareup/tenderpayment/TenderPaymentResult$CanceledBillPayment;

    if-eqz v0, :cond_e

    .line 140
    iget-object p1, p0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->paymentProcessingEventSink:Lcom/squareup/checkoutflow/PaymentProcessingEventSink;

    sget-object v0, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->CANCEL_BILL_HUMAN_INITIATED:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    invoke-interface {p1, v0}, Lcom/squareup/checkoutflow/PaymentProcessingEventSink;->paymentCanceled(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V

    .line 141
    invoke-direct {p0}, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->goBackPastPaymentWorkflow()V

    goto/16 :goto_0

    .line 143
    :cond_e
    instance-of v0, p1, Lcom/squareup/tenderpayment/TenderPaymentResult$CanceledInvoicePayment;

    if-eqz v0, :cond_f

    .line 144
    iget-object p1, p0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->paymentProcessingEventSink:Lcom/squareup/checkoutflow/PaymentProcessingEventSink;

    sget-object v0, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->CANCEL_BILL_HUMAN_INITIATED:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    invoke-interface {p1, v0}, Lcom/squareup/checkoutflow/PaymentProcessingEventSink;->paymentCanceled(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V

    .line 145
    invoke-direct {p0}, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->resetTransactionAndGoToInvoiceDetail()V

    goto/16 :goto_0

    .line 147
    :cond_f
    instance-of v0, p1, Lcom/squareup/tenderpayment/TenderPaymentResult$SuccessfulSwipe;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->swipeHandler:Lcom/squareup/ui/payment/SwipeHandler;

    check-cast p1, Lcom/squareup/tenderpayment/TenderPaymentResult$SuccessfulSwipe;

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/TenderPaymentResult$SuccessfulSwipe;->getSwipe()Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/payment/SwipeHandler;->onSuccessfulSwipe(Lcom/squareup/wavpool/swipe/SwipeEvents$SuccessfulSwipe;)V

    goto/16 :goto_0

    .line 148
    :cond_10
    instance-of v0, p1, Lcom/squareup/tenderpayment/TenderPaymentResult$ShowBuyerPip;

    if-eqz v0, :cond_11

    iget-object p1, p0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->tenderStarter:Lcom/squareup/ui/tender/TenderStarter;

    invoke-interface {p1}, Lcom/squareup/ui/tender/TenderStarter;->ensurePipTenderFlowIsShowing()V

    goto/16 :goto_0

    .line 149
    :cond_11
    instance-of v0, p1, Lcom/squareup/tenderpayment/TenderPaymentResult$SplitTenderAutoCaptureVoid;

    if-eqz v0, :cond_12

    .line 150
    sget-object p1, Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;->CANCEL_BILL_CLIENT_INITIATED_TIMEOUT:Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;

    .line 149
    invoke-virtual {p0, p1}, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->cancelPaymentAndExit(Lcom/squareup/protos/client/bills/CancelBillRequest$CancelBillType;)V

    goto/16 :goto_0

    .line 152
    :cond_12
    instance-of v0, p1, Lcom/squareup/tenderpayment/TenderPaymentResult$SplitTenderRecordCardPayment;

    if-eqz v0, :cond_13

    iget-object p1, p0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->flow:Lflow/Flow;

    .line 153
    sget-object v0, Lcom/squareup/ui/tender/ThirdPartyCardChargedScreen;->INSTANCE:Lcom/squareup/ui/tender/ThirdPartyCardChargedScreen;

    .line 152
    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 155
    :cond_13
    instance-of v0, p1, Lcom/squareup/tenderpayment/TenderPaymentResult$ShowPosApplet;

    if-eqz v0, :cond_14

    .line 156
    iget-object p1, p0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->paymentProcessingEventSink:Lcom/squareup/checkoutflow/PaymentProcessingEventSink;

    invoke-interface {p1}, Lcom/squareup/checkoutflow/PaymentProcessingEventSink;->paymentCompleted()V

    .line 157
    iget-object p1, p0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->orderEntryApplet:Lcom/squareup/orderentry/OrderEntryAppletGateway;

    invoke-interface {p1}, Lcom/squareup/orderentry/OrderEntryAppletGateway;->activateApplet()V

    goto/16 :goto_0

    .line 159
    :cond_14
    instance-of v0, p1, Lcom/squareup/tenderpayment/TenderPaymentResult$DoNothing;

    if-eqz v0, :cond_15

    goto/16 :goto_0

    .line 162
    :cond_15
    instance-of v0, p1, Lcom/squareup/tenderpayment/TenderPaymentResult$ProcessSingleTenderEmvDip;

    if-eqz v0, :cond_16

    iget-object v0, p0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->paymentInputHandler:Lcom/squareup/ui/main/errors/PaymentInputHandler;

    .line 163
    check-cast p1, Lcom/squareup/tenderpayment/TenderPaymentResult$ProcessSingleTenderEmvDip;

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/TenderPaymentResult$ProcessSingleTenderEmvDip;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object p1

    .line 162
    invoke-virtual {v0, p1}, Lcom/squareup/ui/main/errors/PaymentInputHandler;->navigateForEmvPayment(Lcom/squareup/cardreader/CardReaderInfo;)V

    goto/16 :goto_0

    .line 165
    :cond_16
    instance-of v0, p1, Lcom/squareup/tenderpayment/TenderPaymentResult$ProcessSplitTenderEmvDip;

    if-eqz v0, :cond_17

    iget-object p1, p0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->paymentInputHandler:Lcom/squareup/ui/main/errors/PaymentInputHandler;

    invoke-virtual {p1}, Lcom/squareup/ui/main/errors/PaymentInputHandler;->navigateForSplitTenderEmvPayment()V

    goto/16 :goto_0

    .line 166
    :cond_17
    instance-of v0, p1, Lcom/squareup/tenderpayment/TenderPaymentResult$ProcessSplitTenderEmvDipWithTipApplied;

    if-eqz v0, :cond_18

    .line 167
    iget-object v0, p0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->paymentInputHandler:Lcom/squareup/ui/main/errors/PaymentInputHandler;

    check-cast p1, Lcom/squareup/tenderpayment/TenderPaymentResult$ProcessSplitTenderEmvDipWithTipApplied;

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/TenderPaymentResult$ProcessSplitTenderEmvDipWithTipApplied;->getCardReaderInfo()Lcom/squareup/cardreader/CardReaderInfo;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/main/errors/PaymentInputHandler;->navigateForEmvPayment(Lcom/squareup/cardreader/CardReaderInfo;)V

    goto/16 :goto_0

    .line 168
    :cond_18
    instance-of v0, p1, Lcom/squareup/tenderpayment/TenderPaymentResult$ProcessContactless;

    if-eqz v0, :cond_19

    iget-object v0, p0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->smartPaymentFlowStarter:Lcom/squareup/ui/main/SmartPaymentFlowStarter;

    check-cast p1, Lcom/squareup/tenderpayment/TenderPaymentResult$ProcessContactless;

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/TenderPaymentResult$ProcessContactless;->getResult()Lcom/squareup/ui/main/SmartPaymentResult;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/main/SmartPaymentFlowStarter;->navigateForSmartPaymentResult(Lcom/squareup/ui/main/SmartPaymentResult;)V

    goto/16 :goto_0

    .line 169
    :cond_19
    instance-of v0, p1, Lcom/squareup/tenderpayment/TenderPaymentResult$ExitWithReaderIssue;

    if-eqz v0, :cond_1a

    iget-object v0, p0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->paymentInputHandler:Lcom/squareup/ui/main/errors/PaymentInputHandler;

    check-cast p1, Lcom/squareup/tenderpayment/TenderPaymentResult$ExitWithReaderIssue;

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/TenderPaymentResult$ExitWithReaderIssue;->getRequest()Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/main/errors/PaymentInputHandler;->navigateForReaderIssueScreen(Lcom/squareup/cardreader/ui/api/ReaderIssueScreenRequest;)V

    goto :goto_0

    .line 170
    :cond_1a
    instance-of v0, p1, Lcom/squareup/tenderpayment/TenderPaymentResult$EmoneyBrandSelected;

    if-eqz v0, :cond_1d

    .line 171
    iget-object v0, p0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasSplitTenderBillPayment()Z

    move-result v0

    if-nez v0, :cond_1b

    .line 172
    iget-object v0, p0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->startSingleTenderBillPayment()Lcom/squareup/payment/BillPayment;

    .line 175
    :cond_1b
    iget-object v0, p0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getTenderAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 177
    iget-object v1, p0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v1}, Lcom/squareup/payment/TenderInEdit;->isEditingTender()Z

    move-result v1

    if-eqz v1, :cond_1c

    .line 178
    iget-object v1, p0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v1}, Lcom/squareup/payment/TenderInEdit;->clearBaseTender()Lcom/squareup/payment/tender/BaseTender$Builder;

    .line 181
    :cond_1c
    iget-object v1, p0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

    invoke-virtual {v1}, Lcom/squareup/payment/tender/TenderFactory;->createEmoney()Lcom/squareup/payment/tender/EmoneyTender$Builder;

    move-result-object v1

    const-string v2, "emoney"

    .line 182
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Lcom/squareup/payment/tender/EmoneyTender$Builder;->setAmount(Lcom/squareup/protos/common/Money;)Lcom/squareup/payment/tender/BaseTender$Builder;

    .line 183
    iget-object v2, p0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    check-cast v1, Lcom/squareup/payment/tender/BaseTender$Builder;

    invoke-interface {v2, v1}, Lcom/squareup/payment/TenderInEdit;->editTender(Lcom/squareup/payment/tender/BaseTender$Builder;)V

    .line 185
    iget-object v1, p0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->buyerFlowStarter:Lcom/squareup/ui/buyer/BuyerFlowStarter;

    .line 186
    iget-object v2, p0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v2}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v2

    check-cast p1, Lcom/squareup/tenderpayment/TenderPaymentResult$EmoneyBrandSelected;

    invoke-virtual {p1}, Lcom/squareup/tenderpayment/TenderPaymentResult$EmoneyBrandSelected;->getBrand()Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;

    move-result-object p1

    .line 185
    invoke-interface {v1, v2, v0, p1}, Lcom/squareup/ui/buyer/BuyerFlowStarter;->startEmoneyBuyerFlow(Lcom/squareup/payment/BillPayment;Lcom/squareup/protos/common/Money;Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;)V

    goto :goto_0

    .line 189
    :cond_1d
    instance-of v0, p1, Lcom/squareup/tenderpayment/TenderPaymentResult$UnsuccessfullyComplete;

    if-eqz v0, :cond_1e

    iget-object p1, p0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->posContainer:Lcom/squareup/ui/main/PosContainer;

    invoke-interface {p1}, Lcom/squareup/ui/main/PosContainer;->resetHistory()V

    goto :goto_0

    .line 190
    :cond_1e
    instance-of v0, p1, Lcom/squareup/tenderpayment/TenderPaymentResult$SuccessfullyComplete;

    if-eqz v0, :cond_1f

    .line 191
    iget-object p1, p0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->paymentProcessingEventSink:Lcom/squareup/checkoutflow/PaymentProcessingEventSink;

    invoke-interface {p1}, Lcom/squareup/checkoutflow/PaymentProcessingEventSink;->paymentCompleted()V

    .line 192
    iget-object p1, p0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->reset()V

    .line 193
    iget-object p1, p0, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->posContainer:Lcom/squareup/ui/main/PosContainer;

    invoke-interface {p1}, Lcom/squareup/ui/main/PosContainer;->resetHistory()V

    goto :goto_0

    .line 195
    :cond_1f
    instance-of v0, p1, Lcom/squareup/tenderpayment/TenderPaymentResult$ExitWithReaderNfcTimeout;

    if-eqz v0, :cond_20

    invoke-direct {p0}, Lcom/squareup/ui/tender/RealTenderPaymentResultHandler;->cancelPaymentAndShowTimeoutError()V

    :goto_0
    return-void

    .line 196
    :cond_20
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected TenderPaymentResult: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method
