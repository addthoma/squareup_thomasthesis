.class public final Lcom/squareup/ui/tender/PayCashView_MembersInjector;
.super Ljava/lang/Object;
.source "PayCashView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/tender/PayCashView;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final moneyFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final moneyLocaleHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/MoneyLocaleHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/tender/PayCashPresenter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/MoneyLocaleHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/tender/PayCashPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;)V"
        }
    .end annotation

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/squareup/ui/tender/PayCashView_MembersInjector;->moneyLocaleHelperProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p2, p0, Lcom/squareup/ui/tender/PayCashView_MembersInjector;->moneyFormatterProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p3, p0, Lcom/squareup/ui/tender/PayCashView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p4, p0, Lcom/squareup/ui/tender/PayCashView_MembersInjector;->analyticsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/money/MoneyLocaleHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/tender/PayCashPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/tender/PayCashView;",
            ">;"
        }
    .end annotation

    .line 42
    new-instance v0, Lcom/squareup/ui/tender/PayCashView_MembersInjector;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/tender/PayCashView_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectAnalytics(Lcom/squareup/ui/tender/PayCashView;Lcom/squareup/analytics/Analytics;)V
    .locals 0

    .line 70
    iput-object p1, p0, Lcom/squareup/ui/tender/PayCashView;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method

.method public static injectMoneyFormatter(Lcom/squareup/ui/tender/PayCashView;Lcom/squareup/text/Formatter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/tender/PayCashView;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;)V"
        }
    .end annotation

    .line 60
    iput-object p1, p0, Lcom/squareup/ui/tender/PayCashView;->moneyFormatter:Lcom/squareup/text/Formatter;

    return-void
.end method

.method public static injectMoneyLocaleHelper(Lcom/squareup/ui/tender/PayCashView;Lcom/squareup/money/MoneyLocaleHelper;)V
    .locals 0

    .line 55
    iput-object p1, p0, Lcom/squareup/ui/tender/PayCashView;->moneyLocaleHelper:Lcom/squareup/money/MoneyLocaleHelper;

    return-void
.end method

.method public static injectPresenter(Lcom/squareup/ui/tender/PayCashView;Lcom/squareup/ui/tender/PayCashPresenter;)V
    .locals 0

    .line 65
    iput-object p1, p0, Lcom/squareup/ui/tender/PayCashView;->presenter:Lcom/squareup/ui/tender/PayCashPresenter;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/tender/PayCashView;)V
    .locals 1

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/tender/PayCashView_MembersInjector;->moneyLocaleHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/money/MoneyLocaleHelper;

    invoke-static {p1, v0}, Lcom/squareup/ui/tender/PayCashView_MembersInjector;->injectMoneyLocaleHelper(Lcom/squareup/ui/tender/PayCashView;Lcom/squareup/money/MoneyLocaleHelper;)V

    .line 47
    iget-object v0, p0, Lcom/squareup/ui/tender/PayCashView_MembersInjector;->moneyFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/text/Formatter;

    invoke-static {p1, v0}, Lcom/squareup/ui/tender/PayCashView_MembersInjector;->injectMoneyFormatter(Lcom/squareup/ui/tender/PayCashView;Lcom/squareup/text/Formatter;)V

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/tender/PayCashView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/tender/PayCashPresenter;

    invoke-static {p1, v0}, Lcom/squareup/ui/tender/PayCashView_MembersInjector;->injectPresenter(Lcom/squareup/ui/tender/PayCashView;Lcom/squareup/ui/tender/PayCashPresenter;)V

    .line 49
    iget-object v0, p0, Lcom/squareup/ui/tender/PayCashView_MembersInjector;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/analytics/Analytics;

    invoke-static {p1, v0}, Lcom/squareup/ui/tender/PayCashView_MembersInjector;->injectAnalytics(Lcom/squareup/ui/tender/PayCashView;Lcom/squareup/analytics/Analytics;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 12
    check-cast p1, Lcom/squareup/ui/tender/PayCashView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/tender/PayCashView_MembersInjector;->injectMembers(Lcom/squareup/ui/tender/PayCashView;)V

    return-void
.end method
