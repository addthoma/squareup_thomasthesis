.class public final Lcom/squareup/ui/tender/RealTenderScopeRunner_Factory;
.super Ljava/lang/Object;
.source "RealTenderScopeRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/tender/RealTenderScopeRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final checkoutInformationEventLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/CheckoutInformationEventLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final checkoutWorkflowRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/CheckoutWorkflowRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final contextProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final formatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;"
        }
    .end annotation
.end field

.field private final gatekeeperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;"
        }
    .end annotation
.end field

.field private final invoicesAppletRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final mainContainerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;"
        }
    .end annotation
.end field

.field private final manualCardEntryScreenDataHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/ManualCardEntryScreenDataHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final nfcProcessorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/NfcProcessor;",
            ">;"
        }
    .end annotation
.end field

.field private final orderEntryAppletProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
            ">;"
        }
    .end annotation
.end field

.field private final passcodeEmployeeManagementProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderCompleterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderCompleter;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderStarterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/tender/TenderStarter;",
            ">;"
        }
    .end annotation
.end field

.field private final touchEventMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/TouchEventMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/CheckoutInformationEventLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/NfcProcessor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/TouchEventMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/CheckoutWorkflowRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/tender/TenderStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderCompleter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/ManualCardEntryScreenDataHelper;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 81
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderScopeRunner_Factory;->checkoutInformationEventLoggerProvider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 82
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderScopeRunner_Factory;->nfcProcessorProvider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 83
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderScopeRunner_Factory;->resProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 84
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderScopeRunner_Factory;->mainContainerProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 85
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderScopeRunner_Factory;->transactionProvider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 86
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderScopeRunner_Factory;->touchEventMonitorProvider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 87
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderScopeRunner_Factory;->gatekeeperProvider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 88
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderScopeRunner_Factory;->passcodeEmployeeManagementProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 89
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderScopeRunner_Factory;->checkoutWorkflowRunnerProvider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 90
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderScopeRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 91
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderScopeRunner_Factory;->tenderStarterProvider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 92
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderScopeRunner_Factory;->orderEntryAppletProvider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 93
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderScopeRunner_Factory;->invoicesAppletRunnerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 94
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderScopeRunner_Factory;->tenderCompleterProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 95
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderScopeRunner_Factory;->formatterProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 96
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderScopeRunner_Factory;->contextProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p17

    .line 97
    iput-object v1, v0, Lcom/squareup/ui/tender/RealTenderScopeRunner_Factory;->manualCardEntryScreenDataHelperProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/tender/RealTenderScopeRunner_Factory;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/CheckoutInformationEventLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/NfcProcessor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/TouchEventMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/CheckoutWorkflowRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/tender/TenderStarter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/TenderCompleter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tenderpayment/ManualCardEntryScreenDataHelper;",
            ">;)",
            "Lcom/squareup/ui/tender/RealTenderScopeRunner_Factory;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    .line 119
    new-instance v18, Lcom/squareup/ui/tender/RealTenderScopeRunner_Factory;

    move-object/from16 v0, v18

    invoke-direct/range {v0 .. v17}, Lcom/squareup/ui/tender/RealTenderScopeRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v18
.end method

.method public static newInstance(Lcom/squareup/log/CheckoutInformationEventLogger;Lcom/squareup/ui/NfcProcessor;Lcom/squareup/util/Res;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/TouchEventMonitor;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/ui/main/CheckoutWorkflowRunner;Lflow/Flow;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/orderentry/OrderEntryAppletGateway;Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;Lcom/squareup/tenderpayment/TenderCompleter;Lcom/squareup/text/Formatter;Landroid/app/Application;Lcom/squareup/tenderpayment/ManualCardEntryScreenDataHelper;)Lcom/squareup/ui/tender/RealTenderScopeRunner;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/log/CheckoutInformationEventLogger;",
            "Lcom/squareup/ui/NfcProcessor;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/ui/main/PosContainer;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/ui/TouchEventMonitor;",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            "Lcom/squareup/permissions/PasscodeEmployeeManagement;",
            "Lcom/squareup/ui/main/CheckoutWorkflowRunner;",
            "Lflow/Flow;",
            "Lcom/squareup/ui/tender/TenderStarter;",
            "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
            "Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;",
            "Lcom/squareup/tenderpayment/TenderCompleter;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Landroid/app/Application;",
            "Lcom/squareup/tenderpayment/ManualCardEntryScreenDataHelper;",
            ")",
            "Lcom/squareup/ui/tender/RealTenderScopeRunner;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    .line 131
    new-instance v18, Lcom/squareup/ui/tender/RealTenderScopeRunner;

    move-object/from16 v0, v18

    invoke-direct/range {v0 .. v17}, Lcom/squareup/ui/tender/RealTenderScopeRunner;-><init>(Lcom/squareup/log/CheckoutInformationEventLogger;Lcom/squareup/ui/NfcProcessor;Lcom/squareup/util/Res;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/TouchEventMonitor;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/ui/main/CheckoutWorkflowRunner;Lflow/Flow;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/orderentry/OrderEntryAppletGateway;Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;Lcom/squareup/tenderpayment/TenderCompleter;Lcom/squareup/text/Formatter;Landroid/app/Application;Lcom/squareup/tenderpayment/ManualCardEntryScreenDataHelper;)V

    return-object v18
.end method


# virtual methods
.method public get()Lcom/squareup/ui/tender/RealTenderScopeRunner;
    .locals 19

    move-object/from16 v0, p0

    .line 102
    iget-object v1, v0, Lcom/squareup/ui/tender/RealTenderScopeRunner_Factory;->checkoutInformationEventLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/log/CheckoutInformationEventLogger;

    iget-object v1, v0, Lcom/squareup/ui/tender/RealTenderScopeRunner_Factory;->nfcProcessorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/ui/NfcProcessor;

    iget-object v1, v0, Lcom/squareup/ui/tender/RealTenderScopeRunner_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/util/Res;

    iget-object v1, v0, Lcom/squareup/ui/tender/RealTenderScopeRunner_Factory;->mainContainerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/squareup/ui/main/PosContainer;

    iget-object v1, v0, Lcom/squareup/ui/tender/RealTenderScopeRunner_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/squareup/payment/Transaction;

    iget-object v1, v0, Lcom/squareup/ui/tender/RealTenderScopeRunner_Factory;->touchEventMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/squareup/ui/TouchEventMonitor;

    iget-object v1, v0, Lcom/squareup/ui/tender/RealTenderScopeRunner_Factory;->gatekeeperProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/squareup/permissions/PermissionGatekeeper;

    iget-object v1, v0, Lcom/squareup/ui/tender/RealTenderScopeRunner_Factory;->passcodeEmployeeManagementProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/squareup/permissions/PasscodeEmployeeManagement;

    iget-object v1, v0, Lcom/squareup/ui/tender/RealTenderScopeRunner_Factory;->checkoutWorkflowRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/squareup/ui/main/CheckoutWorkflowRunner;

    iget-object v1, v0, Lcom/squareup/ui/tender/RealTenderScopeRunner_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lflow/Flow;

    iget-object v1, v0, Lcom/squareup/ui/tender/RealTenderScopeRunner_Factory;->tenderStarterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lcom/squareup/ui/tender/TenderStarter;

    iget-object v1, v0, Lcom/squareup/ui/tender/RealTenderScopeRunner_Factory;->orderEntryAppletProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/squareup/orderentry/OrderEntryAppletGateway;

    iget-object v1, v0, Lcom/squareup/ui/tender/RealTenderScopeRunner_Factory;->invoicesAppletRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;

    iget-object v1, v0, Lcom/squareup/ui/tender/RealTenderScopeRunner_Factory;->tenderCompleterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/squareup/tenderpayment/TenderCompleter;

    iget-object v1, v0, Lcom/squareup/ui/tender/RealTenderScopeRunner_Factory;->formatterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/text/Formatter;

    iget-object v1, v0, Lcom/squareup/ui/tender/RealTenderScopeRunner_Factory;->contextProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v17, v1

    check-cast v17, Landroid/app/Application;

    iget-object v1, v0, Lcom/squareup/ui/tender/RealTenderScopeRunner_Factory;->manualCardEntryScreenDataHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v18, v1

    check-cast v18, Lcom/squareup/tenderpayment/ManualCardEntryScreenDataHelper;

    invoke-static/range {v2 .. v18}, Lcom/squareup/ui/tender/RealTenderScopeRunner_Factory;->newInstance(Lcom/squareup/log/CheckoutInformationEventLogger;Lcom/squareup/ui/NfcProcessor;Lcom/squareup/util/Res;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/TouchEventMonitor;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/permissions/PasscodeEmployeeManagement;Lcom/squareup/ui/main/CheckoutWorkflowRunner;Lflow/Flow;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/orderentry/OrderEntryAppletGateway;Lcom/squareup/invoicesappletapi/InvoicesAppletRunner;Lcom/squareup/tenderpayment/TenderCompleter;Lcom/squareup/text/Formatter;Landroid/app/Application;Lcom/squareup/tenderpayment/ManualCardEntryScreenDataHelper;)Lcom/squareup/ui/tender/RealTenderScopeRunner;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/squareup/ui/tender/RealTenderScopeRunner_Factory;->get()Lcom/squareup/ui/tender/RealTenderScopeRunner;

    move-result-object v0

    return-object v0
.end method
