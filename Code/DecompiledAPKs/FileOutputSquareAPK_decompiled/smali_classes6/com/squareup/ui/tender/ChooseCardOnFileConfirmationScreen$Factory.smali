.class public Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen$Factory;
.super Ljava/lang/Object;
.source "ChooseCardOnFileConfirmationScreen.java"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$create$0(Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen$Presenter;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 53
    invoke-virtual {p0}, Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen$Presenter;->onConfirm()V

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    .line 47
    new-instance v0, Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen$Presenter;

    invoke-direct {v0, p1}, Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen$Presenter;-><init>(Landroid/content/Context;)V

    .line 48
    const-class v1, Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen$Component;

    invoke-static {p1, v1}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen$Component;

    invoke-interface {v1, v0}, Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen$Component;->inject(Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen$Presenter;)V

    .line 49
    new-instance v1, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v1, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget p1, Lcom/squareup/payment/R$string;->card_on_file_charge_confirmation_title:I

    .line 50
    invoke-virtual {v1, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 51
    invoke-virtual {v0}, Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen$Presenter;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v1, Lcom/squareup/checkout/R$string;->charge:I

    new-instance v2, Lcom/squareup/ui/tender/-$$Lambda$ChooseCardOnFileConfirmationScreen$Factory$SOzMn5Clz4WHaKpX0a_jraH0tFc;

    invoke-direct {v2, v0}, Lcom/squareup/ui/tender/-$$Lambda$ChooseCardOnFileConfirmationScreen$Factory$SOzMn5Clz4WHaKpX0a_jraH0tFc;-><init>(Lcom/squareup/ui/tender/ChooseCardOnFileConfirmationScreen$Presenter;)V

    .line 52
    invoke-virtual {p1, v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/marin/R$drawable;->marin_selector_blue:I

    .line 54
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButtonBackground(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/marin/R$color;->marin_white:I

    .line 55
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButtonTextColor(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/common/strings/R$string;->cancel:I

    .line 56
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(I)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    const/4 v0, 0x1

    .line 57
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setCancelable(Z)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 58
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    .line 49
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
