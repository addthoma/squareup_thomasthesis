.class public final Lcom/squareup/ui/seller/SeparatedPrintoutsOrderTicketBootstrapScreen;
.super Lcom/squareup/container/BootstrapTreeKey;
.source "SeparatedPrintoutsOrderTicketBootstrapScreen.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H\u0016J\u0010\u0010\t\u001a\n \u000b*\u0004\u0018\u00010\n0\nH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/ui/seller/SeparatedPrintoutsOrderTicketBootstrapScreen;",
        "Lcom/squareup/container/BootstrapTreeKey;",
        "input",
        "Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;",
        "(Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;)V",
        "doRegister",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "getParentKey",
        "Lcom/squareup/ui/seller/SellerScope;",
        "kotlin.jvm.PlatformType",
        "order-entry_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final input:Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;


# direct methods
.method public constructor <init>(Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;)V
    .locals 1

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-direct {p0}, Lcom/squareup/container/BootstrapTreeKey;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/seller/SeparatedPrintoutsOrderTicketBootstrapScreen;->input:Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;

    return-void
.end method


# virtual methods
.method public doRegister(Lmortar/MortarScope;)V
    .locals 3

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    sget-object v0, Lcom/squareup/ui/seller/SellerScopeWorkflowRunner;->Companion:Lcom/squareup/ui/seller/SellerScopeWorkflowRunner$Companion;

    .line 14
    new-instance v1, Lcom/squareup/ui/seller/SellerScopeWorkflowProps$StartSeparatedPrintouts;

    iget-object v2, p0, Lcom/squareup/ui/seller/SeparatedPrintoutsOrderTicketBootstrapScreen;->input:Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;

    invoke-direct {v1, v2}, Lcom/squareup/ui/seller/SellerScopeWorkflowProps$StartSeparatedPrintouts;-><init>(Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;)V

    check-cast v1, Lcom/squareup/ui/seller/SellerScopeWorkflowProps;

    .line 13
    invoke-virtual {v0, p1, v1}, Lcom/squareup/ui/seller/SellerScopeWorkflowRunner$Companion;->startNewWorkflow(Lmortar/MortarScope;Lcom/squareup/ui/seller/SellerScopeWorkflowProps;)V

    return-void
.end method

.method public getParentKey()Lcom/squareup/ui/seller/SellerScope;
    .locals 1

    .line 10
    sget-object v0, Lcom/squareup/ui/seller/SellerScope;->INSTANCE:Lcom/squareup/ui/seller/SellerScope;

    return-object v0
.end method

.method public bridge synthetic getParentKey()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/ui/seller/SeparatedPrintoutsOrderTicketBootstrapScreen;->getParentKey()Lcom/squareup/ui/seller/SellerScope;

    move-result-object v0

    return-object v0
.end method
