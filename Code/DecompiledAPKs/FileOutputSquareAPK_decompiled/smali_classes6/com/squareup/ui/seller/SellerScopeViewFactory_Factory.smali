.class public final Lcom/squareup/ui/seller/SellerScopeViewFactory_Factory;
.super Ljava/lang/Object;
.source "SellerScopeViewFactory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/seller/SellerScopeViewFactory;",
        ">;"
    }
.end annotation


# instance fields
.field private final separatedPrintoutsViewFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsViewFactory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsViewFactory;",
            ">;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/ui/seller/SellerScopeViewFactory_Factory;->separatedPrintoutsViewFactoryProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/ui/seller/SellerScopeViewFactory_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsViewFactory;",
            ">;)",
            "Lcom/squareup/ui/seller/SellerScopeViewFactory_Factory;"
        }
    .end annotation

    .line 31
    new-instance v0, Lcom/squareup/ui/seller/SellerScopeViewFactory_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/ui/seller/SellerScopeViewFactory_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsViewFactory;)Lcom/squareup/ui/seller/SellerScopeViewFactory;
    .locals 1

    .line 36
    new-instance v0, Lcom/squareup/ui/seller/SellerScopeViewFactory;

    invoke-direct {v0, p0}, Lcom/squareup/ui/seller/SellerScopeViewFactory;-><init>(Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsViewFactory;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/seller/SellerScopeViewFactory;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/ui/seller/SellerScopeViewFactory_Factory;->separatedPrintoutsViewFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsViewFactory;

    invoke-static {v0}, Lcom/squareup/ui/seller/SellerScopeViewFactory_Factory;->newInstance(Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsViewFactory;)Lcom/squareup/ui/seller/SellerScopeViewFactory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/ui/seller/SellerScopeViewFactory_Factory;->get()Lcom/squareup/ui/seller/SellerScopeViewFactory;

    move-result-object v0

    return-object v0
.end method
