.class public final Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow_Factory;
.super Ljava/lang/Object;
.source "InstrumentOnFileSellerWorkflow_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final customerSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final expirationHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/card/ExpirationHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final hudToasterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/PaymentHudToaster;",
            ">;"
        }
    .end annotation
.end field

.field private final offlineModeMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final paperSignatureSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/PaperSignatureSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final storeAndForwardAnalyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/StoreAndForwardAnalytics;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/tender/TenderFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final tenderInEditProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;"
        }
    .end annotation
.end field

.field private final tipDeterminerFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TipDeterminerFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field

.field private final x2ScreenRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/tender/TenderFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/PaymentHudToaster;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/StoreAndForwardAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TipDeterminerFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/card/ExpirationHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/PaperSignatureSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;)V"
        }
    .end annotation

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    iput-object p1, p0, Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow_Factory;->tenderFactoryProvider:Ljavax/inject/Provider;

    .line 68
    iput-object p2, p0, Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow_Factory;->transactionProvider:Ljavax/inject/Provider;

    .line 69
    iput-object p3, p0, Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow_Factory;->hudToasterProvider:Ljavax/inject/Provider;

    .line 70
    iput-object p4, p0, Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow_Factory;->storeAndForwardAnalyticsProvider:Ljavax/inject/Provider;

    .line 71
    iput-object p5, p0, Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow_Factory;->settingsProvider:Ljavax/inject/Provider;

    .line 72
    iput-object p6, p0, Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow_Factory;->offlineModeMonitorProvider:Ljavax/inject/Provider;

    .line 73
    iput-object p7, p0, Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow_Factory;->tenderInEditProvider:Ljavax/inject/Provider;

    .line 74
    iput-object p8, p0, Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow_Factory;->tipDeterminerFactoryProvider:Ljavax/inject/Provider;

    .line 75
    iput-object p9, p0, Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow_Factory;->expirationHelperProvider:Ljavax/inject/Provider;

    .line 76
    iput-object p10, p0, Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow_Factory;->customerSettingsProvider:Ljavax/inject/Provider;

    .line 77
    iput-object p11, p0, Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow_Factory;->paperSignatureSettingsProvider:Ljavax/inject/Provider;

    .line 78
    iput-object p12, p0, Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 79
    iput-object p13, p0, Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow_Factory;->x2ScreenRunnerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow_Factory;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/tender/TenderFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/PaymentHudToaster;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/StoreAndForwardAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TenderInEdit;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/TipDeterminerFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/card/ExpirationHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/PaperSignatureSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/x2/MaybeX2SellerScreenRunner;",
            ">;)",
            "Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow_Factory;"
        }
    .end annotation

    .line 100
    new-instance v14, Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow_Factory;

    move-object v0, v14

    move-object v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    invoke-direct/range {v0 .. v13}, Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v14
.end method

.method public static newInstance(Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/payment/Transaction;Lcom/squareup/payment/PaymentHudToaster;Lcom/squareup/analytics/StoreAndForwardAnalytics;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/payment/TipDeterminerFactory;Lcom/squareup/card/ExpirationHelper;Lcom/squareup/crm/CustomerManagementSettings;Lcom/squareup/papersignature/PaperSignatureSettings;Lcom/squareup/analytics/Analytics;Lcom/squareup/x2/MaybeX2SellerScreenRunner;)Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow;
    .locals 15

    .line 110
    new-instance v14, Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow;

    move-object v0, v14

    move-object v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    invoke-direct/range {v0 .. v13}, Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow;-><init>(Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/payment/Transaction;Lcom/squareup/payment/PaymentHudToaster;Lcom/squareup/analytics/StoreAndForwardAnalytics;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/payment/TipDeterminerFactory;Lcom/squareup/card/ExpirationHelper;Lcom/squareup/crm/CustomerManagementSettings;Lcom/squareup/papersignature/PaperSignatureSettings;Lcom/squareup/analytics/Analytics;Lcom/squareup/x2/MaybeX2SellerScreenRunner;)V

    return-object v14
.end method


# virtual methods
.method public get()Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow;
    .locals 14

    .line 84
    iget-object v0, p0, Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow_Factory;->tenderFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/payment/tender/TenderFactory;

    iget-object v0, p0, Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/payment/Transaction;

    iget-object v0, p0, Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow_Factory;->hudToasterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/payment/PaymentHudToaster;

    iget-object v0, p0, Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow_Factory;->storeAndForwardAnalyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/analytics/StoreAndForwardAnalytics;

    iget-object v0, p0, Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow_Factory;->offlineModeMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/payment/OfflineModeMonitor;

    iget-object v0, p0, Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow_Factory;->tenderInEditProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/payment/TenderInEdit;

    iget-object v0, p0, Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow_Factory;->tipDeterminerFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/payment/TipDeterminerFactory;

    iget-object v0, p0, Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow_Factory;->expirationHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/card/ExpirationHelper;

    iget-object v0, p0, Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow_Factory;->customerSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/crm/CustomerManagementSettings;

    iget-object v0, p0, Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow_Factory;->paperSignatureSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/squareup/papersignature/PaperSignatureSettings;

    iget-object v0, p0, Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v12, v0

    check-cast v12, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow_Factory;->x2ScreenRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v13, v0

    check-cast v13, Lcom/squareup/x2/MaybeX2SellerScreenRunner;

    invoke-static/range {v1 .. v13}, Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow_Factory;->newInstance(Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/payment/Transaction;Lcom/squareup/payment/PaymentHudToaster;Lcom/squareup/analytics/StoreAndForwardAnalytics;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/payment/TipDeterminerFactory;Lcom/squareup/card/ExpirationHelper;Lcom/squareup/crm/CustomerManagementSettings;Lcom/squareup/papersignature/PaperSignatureSettings;Lcom/squareup/analytics/Analytics;Lcom/squareup/x2/MaybeX2SellerScreenRunner;)Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 20
    invoke-virtual {p0}, Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow_Factory;->get()Lcom/squareup/ui/seller/InstrumentOnFileSellerWorkflow;

    move-result-object v0

    return-object v0
.end method
