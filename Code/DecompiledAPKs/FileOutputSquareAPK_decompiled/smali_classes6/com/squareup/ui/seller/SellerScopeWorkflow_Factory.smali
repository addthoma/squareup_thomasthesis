.class public final Lcom/squareup/ui/seller/SellerScopeWorkflow_Factory;
.super Ljava/lang/Object;
.source "SellerScopeWorkflow_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/seller/SellerScopeWorkflow;",
        ">;"
    }
.end annotation


# instance fields
.field private final separatedPrintoutsWorkflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsWorkflow;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsWorkflow;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/ui/seller/SellerScopeWorkflow_Factory;->separatedPrintoutsWorkflowProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/ui/seller/SellerScopeWorkflow_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsWorkflow;",
            ">;)",
            "Lcom/squareup/ui/seller/SellerScopeWorkflow_Factory;"
        }
    .end annotation

    .line 33
    new-instance v0, Lcom/squareup/ui/seller/SellerScopeWorkflow_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/ui/seller/SellerScopeWorkflow_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Ldagger/Lazy;)Lcom/squareup/ui/seller/SellerScopeWorkflow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldagger/Lazy<",
            "Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsWorkflow;",
            ">;)",
            "Lcom/squareup/ui/seller/SellerScopeWorkflow;"
        }
    .end annotation

    .line 38
    new-instance v0, Lcom/squareup/ui/seller/SellerScopeWorkflow;

    invoke-direct {v0, p0}, Lcom/squareup/ui/seller/SellerScopeWorkflow;-><init>(Ldagger/Lazy;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/seller/SellerScopeWorkflow;
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/squareup/ui/seller/SellerScopeWorkflow_Factory;->separatedPrintoutsWorkflowProvider:Ljavax/inject/Provider;

    invoke-static {v0}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/ui/seller/SellerScopeWorkflow_Factory;->newInstance(Ldagger/Lazy;)Lcom/squareup/ui/seller/SellerScopeWorkflow;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/ui/seller/SellerScopeWorkflow_Factory;->get()Lcom/squareup/ui/seller/SellerScopeWorkflow;

    move-result-object v0

    return-object v0
.end method
