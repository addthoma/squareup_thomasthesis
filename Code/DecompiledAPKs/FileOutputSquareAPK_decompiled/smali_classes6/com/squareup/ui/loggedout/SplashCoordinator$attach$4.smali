.class public final Lcom/squareup/ui/loggedout/SplashCoordinator$attach$4;
.super Landroidx/viewpager/widget/ViewPager$SimpleOnPageChangeListener;
.source "SplashCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/loggedout/SplashCoordinator;->attach(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0006"
    }
    d2 = {
        "com/squareup/ui/loggedout/SplashCoordinator$attach$4",
        "Landroidx/viewpager/widget/ViewPager$SimpleOnPageChangeListener;",
        "onPageSelected",
        "",
        "position",
        "",
        "loggedout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/loggedout/SplashCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/loggedout/SplashCoordinator;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 84
    iput-object p1, p0, Lcom/squareup/ui/loggedout/SplashCoordinator$attach$4;->this$0:Lcom/squareup/ui/loggedout/SplashCoordinator;

    invoke-direct {p0}, Landroidx/viewpager/widget/ViewPager$SimpleOnPageChangeListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageSelected(I)V
    .locals 1

    .line 86
    invoke-super {p0, p1}, Landroidx/viewpager/widget/ViewPager$SimpleOnPageChangeListener;->onPageSelected(I)V

    .line 87
    iget-object v0, p0, Lcom/squareup/ui/loggedout/SplashCoordinator$attach$4;->this$0:Lcom/squareup/ui/loggedout/SplashCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/loggedout/SplashCoordinator;->access$getPages$p(Lcom/squareup/ui/loggedout/SplashCoordinator;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/loggedout/SplashCoordinator$SplashPage;

    invoke-virtual {p1}, Lcom/squareup/ui/loggedout/SplashCoordinator$SplashPage;->getViewEvent()Lcom/squareup/analytics/RegisterViewName;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 89
    iget-object v0, p0, Lcom/squareup/ui/loggedout/SplashCoordinator$attach$4;->this$0:Lcom/squareup/ui/loggedout/SplashCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/loggedout/SplashCoordinator;->access$getAnalytics$p(Lcom/squareup/ui/loggedout/SplashCoordinator;)Lcom/squareup/analytics/Analytics;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/squareup/analytics/Analytics;->logView(Lcom/squareup/analytics/RegisterViewName;)V

    :cond_0
    return-void
.end method
