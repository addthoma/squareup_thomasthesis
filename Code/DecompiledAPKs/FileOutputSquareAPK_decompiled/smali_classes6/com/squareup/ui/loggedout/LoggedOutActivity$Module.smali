.class public abstract Lcom/squareup/ui/loggedout/LoggedOutActivity$Module;
.super Ljava/lang/Object;
.source "LoggedOutActivity.java"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/payment/ledger/TransactionLedgerModule$LoggedOutUi;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/loggedout/LoggedOutActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Module"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/loggedout/LoggedOutActivity$Module$Prod;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 191
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideContainerActivityDelegate(Lcom/squareup/util/Device;)Lcom/squareup/container/ContainerActivityDelegate;
    .locals 6
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 205
    new-instance v0, Lcom/squareup/ui/loggedout/LoggedOutActivity$Module$1;

    invoke-direct {v0}, Lcom/squareup/ui/loggedout/LoggedOutActivity$Module$1;-><init>()V

    .line 219
    new-instance v1, Lcom/squareup/container/ContainerActivityDelegate;

    new-instance v2, Lcom/squareup/container/WorkflowRunnerViewFactory;

    invoke-direct {v2}, Lcom/squareup/container/WorkflowRunnerViewFactory;-><init>()V

    const/4 v3, 0x1

    new-array v3, v3, [Lcom/squareup/container/ContainerViewFactory;

    new-instance v4, Lcom/squareup/container/ContainerTreeKeyViewFactory;

    invoke-direct {v4}, Lcom/squareup/container/ContainerTreeKeyViewFactory;-><init>()V

    const/4 v5, 0x0

    aput-object v4, v3, v5

    invoke-direct {v1, p0, v0, v2, v3}, Lcom/squareup/container/ContainerActivityDelegate;-><init>(Lcom/squareup/util/Device;Lcom/squareup/container/ContainerBackgroundsProvider;Lcom/squareup/container/ContainerViewFactory;[Lcom/squareup/container/ContainerViewFactory;)V

    return-object v1
.end method

.method static provideFlow(Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;)Lflow/Flow;
    .locals 3
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 224
    invoke-virtual {p0}, Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;->getFlow()Lflow/Flow;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    .line 226
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 228
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p0

    aput-object p0, v1, v2

    const-string p0, "Cannot inject flow before %s loads, try injecting Lazy<Flow>."

    .line 227
    invoke-static {p0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method abstract provideBrowserLauncher(Lcom/squareup/util/PosBrowserLauncher;)Lcom/squareup/util/BrowserLauncher;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideOnboardingStarter(Lcom/squareup/loggedout/LoggedOutOnboardingStarter;)Lcom/squareup/onboarding/OnboardingStarter;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideOnboardingType(Lcom/squareup/onboarding/RealOnboardingType;)Lcom/squareup/onboarding/OnboardingType;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
