.class Lcom/squareup/ui/loggedout/WorldLandingScreen;
.super Lcom/squareup/ui/loggedout/AbstractLandingScreen;
.source "WorldLandingScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/HasSoftInputModeForPhone;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/loggedout/AbstractLandingScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/loggedout/WorldLandingScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/loggedout/WorldLandingScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/loggedout/WorldLandingScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 22
    new-instance v0, Lcom/squareup/ui/loggedout/WorldLandingScreen;

    invoke-direct {v0}, Lcom/squareup/ui/loggedout/WorldLandingScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/loggedout/WorldLandingScreen;->INSTANCE:Lcom/squareup/ui/loggedout/WorldLandingScreen;

    .line 43
    sget-object v0, Lcom/squareup/ui/loggedout/WorldLandingScreen;->INSTANCE:Lcom/squareup/ui/loggedout/WorldLandingScreen;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/loggedout/WorldLandingScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 24
    invoke-direct {p0}, Lcom/squareup/ui/loggedout/AbstractLandingScreen;-><init>()V

    return-void
.end method


# virtual methods
.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 28
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->WELCOME_WORLD_LANDING:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getSoftInputMode()Lcom/squareup/workflow/SoftInputMode;
    .locals 1

    .line 32
    sget-object v0, Lcom/squareup/workflow/SoftInputMode;->PAN:Lcom/squareup/workflow/SoftInputMode;

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 46
    sget v0, Lcom/squareup/loggedout/R$layout;->world_landing_view:I

    return v0
.end method
