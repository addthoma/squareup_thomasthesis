.class public final Lcom/squareup/ui/loggedout/LoggedOutActivity_Module_Prod_ProvideImpersonationHelperFactory;
.super Ljava/lang/Object;
.source "LoggedOutActivity_Module_Prod_ProvideImpersonationHelperFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/loggedout/LoggedOutActivity_Module_Prod_ProvideImpersonationHelperFactory$InstanceHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/account/ImpersonationHelper;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create()Lcom/squareup/ui/loggedout/LoggedOutActivity_Module_Prod_ProvideImpersonationHelperFactory;
    .locals 1

    .line 23
    invoke-static {}, Lcom/squareup/ui/loggedout/LoggedOutActivity_Module_Prod_ProvideImpersonationHelperFactory$InstanceHolder;->access$000()Lcom/squareup/ui/loggedout/LoggedOutActivity_Module_Prod_ProvideImpersonationHelperFactory;

    move-result-object v0

    return-object v0
.end method

.method public static provideImpersonationHelper()Lcom/squareup/account/ImpersonationHelper;
    .locals 2

    .line 27
    invoke-static {}, Lcom/squareup/ui/loggedout/LoggedOutActivity$Module$Prod;->provideImpersonationHelper()Lcom/squareup/account/ImpersonationHelper;

    move-result-object v0

    const-string v1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {v0, v1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/account/ImpersonationHelper;

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/account/ImpersonationHelper;
    .locals 1

    .line 19
    invoke-static {}, Lcom/squareup/ui/loggedout/LoggedOutActivity_Module_Prod_ProvideImpersonationHelperFactory;->provideImpersonationHelper()Lcom/squareup/account/ImpersonationHelper;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/ui/loggedout/LoggedOutActivity_Module_Prod_ProvideImpersonationHelperFactory;->get()Lcom/squareup/account/ImpersonationHelper;

    move-result-object v0

    return-object v0
.end method
