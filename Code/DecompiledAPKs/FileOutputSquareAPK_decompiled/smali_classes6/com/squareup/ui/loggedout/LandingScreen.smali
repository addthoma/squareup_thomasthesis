.class public Lcom/squareup/ui/loggedout/LandingScreen;
.super Lcom/squareup/ui/loggedout/AbstractLandingScreen;
.source "LandingScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/HasSoftInputModeForPhone;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/loggedout/AbstractLandingScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/loggedout/LandingScreen$Presenter;,
        Lcom/squareup/ui/loggedout/LandingScreen$PaymentLandingView;,
        Lcom/squareup/ui/loggedout/LandingScreen$Runner;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/loggedout/LandingScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/loggedout/LandingScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 35
    new-instance v0, Lcom/squareup/ui/loggedout/LandingScreen;

    invoke-direct {v0}, Lcom/squareup/ui/loggedout/LandingScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/loggedout/LandingScreen;->INSTANCE:Lcom/squareup/ui/loggedout/LandingScreen;

    .line 70
    sget-object v0, Lcom/squareup/ui/loggedout/LandingScreen;->INSTANCE:Lcom/squareup/ui/loggedout/LandingScreen;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/loggedout/LandingScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 37
    invoke-direct {p0}, Lcom/squareup/ui/loggedout/AbstractLandingScreen;-><init>()V

    return-void
.end method


# virtual methods
.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 41
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->WELCOME_LANDING:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getSoftInputMode()Lcom/squareup/workflow/SoftInputMode;
    .locals 1

    .line 45
    sget-object v0, Lcom/squareup/workflow/SoftInputMode;->PAN:Lcom/squareup/workflow/SoftInputMode;

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 73
    sget v0, Lcom/squareup/loggedout/R$layout;->landing_view:I

    return v0
.end method
