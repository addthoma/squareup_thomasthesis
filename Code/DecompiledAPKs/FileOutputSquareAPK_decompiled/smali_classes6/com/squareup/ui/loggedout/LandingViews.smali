.class Lcom/squareup/ui/loggedout/LandingViews;
.super Ljava/lang/Object;
.source "LandingViews.java"


# direct methods
.method constructor <init>()V
    .locals 0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$startReaderAnimation$0(Landroid/widget/ImageView;)Z
    .locals 2

    .line 83
    invoke-virtual {p0}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/squareup/loggedout/R$anim;->reader_insert:I

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    const/4 v1, 0x0

    .line 84
    invoke-virtual {p0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 85
    invoke-virtual {p0, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    return v1
.end method

.method static setCallToAction(Landroid/view/View;IZ)V
    .locals 1

    .line 92
    sget v0, Lcom/squareup/loggedout/R$id;->call_to_action:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p0

    check-cast p0, Lcom/squareup/marin/widgets/MarinGlyphTextView;

    .line 93
    invoke-virtual {p0, p1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setText(I)V

    const/4 p1, 0x0

    .line 94
    invoke-virtual {p0, p1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setVisibility(I)V

    if-eqz p2, :cond_0

    .line 99
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget p2, Lcom/squareup/marin/R$color;->marin_text_selector_dark_gray:I

    .line 100
    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object p1

    .line 101
    invoke-virtual {p0, p1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 102
    sget-object p2, Lcom/squareup/glyph/GlyphTypeface$Glyph;->RIGHT_CARET:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    const/4 v0, 0x2

    invoke-virtual {p0, p2, v0, p1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;ILandroid/content/res/ColorStateList;)V

    goto :goto_0

    .line 105
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget p2, Lcom/squareup/marin/R$color;->marin_text_selector_medium_gray:I

    .line 106
    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object p1

    .line 107
    invoke-virtual {p0, p1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 108
    sget-object p1, Lcom/squareup/marin/widgets/ChevronVisibility;->GONE:Lcom/squareup/marin/widgets/ChevronVisibility;

    invoke-virtual {p0, p1}, Lcom/squareup/marin/widgets/MarinGlyphTextView;->setChevronVisibility(Lcom/squareup/marin/widgets/ChevronVisibility;)V

    :goto_0
    return-void
.end method

.method static setupReader(Landroid/view/View;)V
    .locals 3

    .line 59
    sget v0, Lcom/squareup/loggedout/R$id;->reader:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 61
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    sget v1, Lcom/squareup/loggedout/R$dimen;->reader_jack_size:I

    invoke-virtual {p0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p0

    .line 63
    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 64
    invoke-virtual {v0}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/squareup/util/Views;->isPortrait(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    mul-int/lit8 p0, p0, -0x1

    .line 65
    iput p0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    goto :goto_0

    :cond_0
    mul-int/lit8 p0, p0, -0x1

    .line 67
    iput p0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 69
    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method static startReaderAnimation(Landroid/view/View;)V
    .locals 2

    .line 73
    invoke-static {}, Lcom/squareup/util/Views;->noAnimationsForInstrumentation()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 77
    :cond_0
    sget v0, Lcom/squareup/loggedout/R$id;->reader:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p0

    check-cast p0, Landroid/widget/ImageView;

    const/4 v0, 0x4

    .line 78
    invoke-virtual {p0, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 82
    invoke-static {}, Landroid/os/Looper;->myQueue()Landroid/os/MessageQueue;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/loggedout/-$$Lambda$LandingViews$dVfvdmiLuFhoTHBviGwXLswgVa4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/loggedout/-$$Lambda$LandingViews$dVfvdmiLuFhoTHBviGwXLswgVa4;-><init>(Landroid/widget/ImageView;)V

    invoke-virtual {v0, v1}, Landroid/os/MessageQueue;->addIdleHandler(Landroid/os/MessageQueue$IdleHandler;)V

    return-void
.end method

.method static viewFinishedInflate(Landroid/view/View;Lcom/squareup/ui/loggedout/LandingScreen$Presenter;)V
    .locals 0

    .line 27
    invoke-static {p0, p1}, Lcom/squareup/ui/loggedout/LandingViews;->viewFinishedInflateImpl(Landroid/view/View;Lcom/squareup/ui/loggedout/AbstractLandingScreen$AbstractLandingScreenPresenter;)V

    return-void
.end method

.method static viewFinishedInflate(Landroid/view/View;Lcom/squareup/ui/loggedout/WorldLandingScreen$Presenter;)V
    .locals 0

    .line 31
    invoke-static {p0, p1}, Lcom/squareup/ui/loggedout/LandingViews;->viewFinishedInflateImpl(Landroid/view/View;Lcom/squareup/ui/loggedout/AbstractLandingScreen$AbstractLandingScreenPresenter;)V

    return-void
.end method

.method private static viewFinishedInflateImpl(Landroid/view/View;Lcom/squareup/ui/loggedout/AbstractLandingScreen$AbstractLandingScreenPresenter;)V
    .locals 2

    .line 36
    sget v0, Lcom/squareup/loggedout/R$id;->sign_in:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 37
    new-instance v1, Lcom/squareup/ui/loggedout/LandingViews$1;

    invoke-direct {v1, p1}, Lcom/squareup/ui/loggedout/LandingViews$1;-><init>(Lcom/squareup/ui/loggedout/AbstractLandingScreen$AbstractLandingScreenPresenter;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 43
    sget v0, Lcom/squareup/loggedout/R$id;->create_account:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 44
    new-instance v1, Lcom/squareup/ui/loggedout/LandingViews$2;

    invoke-direct {v1, p1}, Lcom/squareup/ui/loggedout/LandingViews$2;-><init>(Lcom/squareup/ui/loggedout/AbstractLandingScreen$AbstractLandingScreenPresenter;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 50
    sget v0, Lcom/squareup/loggedout/R$id;->call_to_action:I

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p0

    .line 51
    new-instance v0, Lcom/squareup/ui/loggedout/LandingViews$3;

    invoke-direct {v0, p1}, Lcom/squareup/ui/loggedout/LandingViews$3;-><init>(Lcom/squareup/ui/loggedout/AbstractLandingScreen$AbstractLandingScreenPresenter;)V

    invoke-virtual {p0, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
