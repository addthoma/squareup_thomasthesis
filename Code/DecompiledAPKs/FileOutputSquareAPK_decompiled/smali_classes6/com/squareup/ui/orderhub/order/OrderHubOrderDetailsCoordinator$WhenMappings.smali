.class public final synthetic Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$WhenMappings;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I

.field public static final synthetic $EnumSwitchMapping$1:[I

.field public static final synthetic $EnumSwitchMapping$2:[I

.field public static final synthetic $EnumSwitchMapping$3:[I

.field public static final synthetic $EnumSwitchMapping$4:[I

.field public static final synthetic $EnumSwitchMapping$5:[I

.field public static final synthetic $EnumSwitchMapping$6:[I


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 5

    invoke-static {}, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;->values()[Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;->ENTRY_LIST:Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1

    invoke-static {}, Lcom/squareup/orders/model/Order$Fulfillment$State;->values()[Lcom/squareup/orders/model/Order$Fulfillment$State;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/orders/model/Order$Fulfillment$State;->COMPLETED:Lcom/squareup/orders/model/Order$Fulfillment$State;

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$Fulfillment$State;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/orders/model/Order$Fulfillment$State;->CANCELED:Lcom/squareup/orders/model/Order$Fulfillment$State;

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$Fulfillment$State;->ordinal()I

    move-result v1

    const/4 v3, 0x2

    aput v3, v0, v1

    invoke-static {}, Lcom/squareup/orders/model/Order$FulfillmentType;->values()[Lcom/squareup/orders/model/Order$FulfillmentType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/orders/model/Order$FulfillmentType;->MANAGED_DELIVERY:Lcom/squareup/orders/model/Order$FulfillmentType;

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$FulfillmentType;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/orders/model/Order$FulfillmentType;->PICKUP:Lcom/squareup/orders/model/Order$FulfillmentType;

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$FulfillmentType;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$WhenMappings;->$EnumSwitchMapping$2:[I

    sget-object v1, Lcom/squareup/orders/model/Order$FulfillmentType;->SHIPMENT:Lcom/squareup/orders/model/Order$FulfillmentType;

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$FulfillmentType;->ordinal()I

    move-result v1

    const/4 v4, 0x3

    aput v4, v0, v1

    invoke-static {}, Lcom/squareup/orders/model/Order$Fulfillment$State;->values()[Lcom/squareup/orders/model/Order$Fulfillment$State;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$WhenMappings;->$EnumSwitchMapping$3:[I

    sget-object v0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$WhenMappings;->$EnumSwitchMapping$3:[I

    sget-object v1, Lcom/squareup/orders/model/Order$Fulfillment$State;->COMPLETED:Lcom/squareup/orders/model/Order$Fulfillment$State;

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$Fulfillment$State;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$WhenMappings;->$EnumSwitchMapping$3:[I

    sget-object v1, Lcom/squareup/orders/model/Order$Fulfillment$State;->CANCELED:Lcom/squareup/orders/model/Order$Fulfillment$State;

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$Fulfillment$State;->ordinal()I

    move-result v1

    aput v3, v0, v1

    invoke-static {}, Lcom/squareup/orders/model/Order$FulfillmentType;->values()[Lcom/squareup/orders/model/Order$FulfillmentType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$WhenMappings;->$EnumSwitchMapping$4:[I

    sget-object v0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$WhenMappings;->$EnumSwitchMapping$4:[I

    sget-object v1, Lcom/squareup/orders/model/Order$FulfillmentType;->PICKUP:Lcom/squareup/orders/model/Order$FulfillmentType;

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$FulfillmentType;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$WhenMappings;->$EnumSwitchMapping$4:[I

    sget-object v1, Lcom/squareup/orders/model/Order$FulfillmentType;->MANAGED_DELIVERY:Lcom/squareup/orders/model/Order$FulfillmentType;

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$FulfillmentType;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$WhenMappings;->$EnumSwitchMapping$4:[I

    sget-object v1, Lcom/squareup/orders/model/Order$FulfillmentType;->DELIVERY:Lcom/squareup/orders/model/Order$FulfillmentType;

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$FulfillmentType;->ordinal()I

    move-result v1

    aput v4, v0, v1

    invoke-static {}, Lcom/squareup/orders/model/Order$FulfillmentType;->values()[Lcom/squareup/orders/model/Order$FulfillmentType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$WhenMappings;->$EnumSwitchMapping$5:[I

    sget-object v0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$WhenMappings;->$EnumSwitchMapping$5:[I

    sget-object v1, Lcom/squareup/orders/model/Order$FulfillmentType;->PICKUP:Lcom/squareup/orders/model/Order$FulfillmentType;

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$FulfillmentType;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$WhenMappings;->$EnumSwitchMapping$5:[I

    sget-object v1, Lcom/squareup/orders/model/Order$FulfillmentType;->MANAGED_DELIVERY:Lcom/squareup/orders/model/Order$FulfillmentType;

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$FulfillmentType;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$WhenMappings;->$EnumSwitchMapping$5:[I

    sget-object v1, Lcom/squareup/orders/model/Order$FulfillmentType;->DELIVERY:Lcom/squareup/orders/model/Order$FulfillmentType;

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$FulfillmentType;->ordinal()I

    move-result v1

    aput v4, v0, v1

    invoke-static {}, Lcom/squareup/orders/model/Order$FulfillmentType;->values()[Lcom/squareup/orders/model/Order$FulfillmentType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$WhenMappings;->$EnumSwitchMapping$6:[I

    sget-object v0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$WhenMappings;->$EnumSwitchMapping$6:[I

    sget-object v1, Lcom/squareup/orders/model/Order$FulfillmentType;->SHIPMENT:Lcom/squareup/orders/model/Order$FulfillmentType;

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$FulfillmentType;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/ui/orderhub/order/OrderHubOrderDetailsCoordinator$WhenMappings;->$EnumSwitchMapping$6:[I

    sget-object v1, Lcom/squareup/orders/model/Order$FulfillmentType;->DELIVERY:Lcom/squareup/orders/model/Order$FulfillmentType;

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$FulfillmentType;->ordinal()I

    move-result v1

    aput v3, v0, v1

    return-void
.end method
