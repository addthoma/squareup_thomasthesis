.class final Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$bindViews$4;
.super Ljava/lang/Object;
.source "OrderHubDetailCoordinator.kt"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->bindViews(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Landroid/view/View;",
        "kotlin.jvm.PlatformType",
        "onClick"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$bindViews$4;->this$0:Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 1

    .line 241
    iget-object p1, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$bindViews$4;->this$0:Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->access$getSearchBar$p(Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;)Lcom/squareup/noho/NohoEditRow;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoEditRow;->setVisibility(I)V

    .line 242
    iget-object p1, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$bindViews$4;->this$0:Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;->access$getSearchBar$p(Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator;)Lcom/squareup/noho/NohoEditRow;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/noho/NohoEditRow;->focusAndShowKeyboard()V

    return-void
.end method
