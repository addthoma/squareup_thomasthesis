.class public final Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator_Factory_Factory;
.super Ljava/lang/Object;
.source "OrderHubDetailCoordinator_Factory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$Factory;",
        ">;"
    }
.end annotation


# instance fields
.field private final actionBarNavigationHelperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/ActionBarNavigationHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final appletSelectionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/AppletSelection;",
            ">;"
        }
    .end annotation
.end field

.field private final badgePresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/BadgePresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final currentTimeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/time/CurrentTime;",
            ">;"
        }
    .end annotation
.end field

.field private final dateAndTimeFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final recyclerFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/recycler/RecyclerFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final relativeDateAndTimeFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/time/CurrentTime;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/BadgePresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/ActionBarNavigationHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/AppletSelection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/recycler/RecyclerFactory;",
            ">;)V"
        }
    .end annotation

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object p1, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator_Factory_Factory;->resProvider:Ljavax/inject/Provider;

    .line 55
    iput-object p2, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator_Factory_Factory;->relativeDateAndTimeFormatterProvider:Ljavax/inject/Provider;

    .line 56
    iput-object p3, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator_Factory_Factory;->currentTimeProvider:Ljavax/inject/Provider;

    .line 57
    iput-object p4, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator_Factory_Factory;->localeProvider:Ljavax/inject/Provider;

    .line 58
    iput-object p5, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator_Factory_Factory;->dateAndTimeFormatterProvider:Ljavax/inject/Provider;

    .line 59
    iput-object p6, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator_Factory_Factory;->badgePresenterProvider:Ljavax/inject/Provider;

    .line 60
    iput-object p7, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator_Factory_Factory;->actionBarNavigationHelperProvider:Ljavax/inject/Provider;

    .line 61
    iput-object p8, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator_Factory_Factory;->appletSelectionProvider:Ljavax/inject/Provider;

    .line 62
    iput-object p9, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator_Factory_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    .line 63
    iput-object p10, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator_Factory_Factory;->recyclerFactoryProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator_Factory_Factory;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/time/CurrentTime;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/BadgePresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/ActionBarNavigationHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/AppletSelection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/recycler/RecyclerFactory;",
            ">;)",
            "Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator_Factory_Factory;"
        }
    .end annotation

    .line 79
    new-instance v11, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator_Factory_Factory;

    move-object v0, v11

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator_Factory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v11
.end method

.method public static newInstance(Lcom/squareup/util/Res;Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;Lcom/squareup/time/CurrentTime;Ljava/util/Locale;Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;Lcom/squareup/applet/BadgePresenter;Lcom/squareup/applet/ActionBarNavigationHelper;Lcom/squareup/applet/AppletSelection;Lio/reactivex/Scheduler;Lcom/squareup/recycler/RecyclerFactory;)Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$Factory;
    .locals 12

    .line 87
    new-instance v11, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$Factory;

    move-object v0, v11

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$Factory;-><init>(Lcom/squareup/util/Res;Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;Lcom/squareup/time/CurrentTime;Ljava/util/Locale;Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;Lcom/squareup/applet/BadgePresenter;Lcom/squareup/applet/ActionBarNavigationHelper;Lcom/squareup/applet/AppletSelection;Lio/reactivex/Scheduler;Lcom/squareup/recycler/RecyclerFactory;)V

    return-object v11
.end method


# virtual methods
.method public get()Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$Factory;
    .locals 11

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator_Factory_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator_Factory_Factory;->relativeDateAndTimeFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator_Factory_Factory;->currentTimeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/time/CurrentTime;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator_Factory_Factory;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Ljava/util/Locale;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator_Factory_Factory;->dateAndTimeFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator_Factory_Factory;->badgePresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/applet/BadgePresenter;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator_Factory_Factory;->actionBarNavigationHelperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/applet/ActionBarNavigationHelper;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator_Factory_Factory;->appletSelectionProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/applet/AppletSelection;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator_Factory_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lio/reactivex/Scheduler;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator_Factory_Factory;->recyclerFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/recycler/RecyclerFactory;

    invoke-static/range {v1 .. v10}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator_Factory_Factory;->newInstance(Lcom/squareup/util/Res;Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;Lcom/squareup/time/CurrentTime;Ljava/util/Locale;Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;Lcom/squareup/applet/BadgePresenter;Lcom/squareup/applet/ActionBarNavigationHelper;Lcom/squareup/applet/AppletSelection;Lio/reactivex/Scheduler;Lcom/squareup/recycler/RecyclerFactory;)Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$Factory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 17
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator_Factory_Factory;->get()Lcom/squareup/ui/orderhub/detail/OrderHubDetailCoordinator$Factory;

    move-result-object v0

    return-object v0
.end method
