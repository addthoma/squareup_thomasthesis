.class public final Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;
.super Ljava/lang/Object;
.source "OrderHubPrintingMonitor.kt"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOrderHubPrintingMonitor.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OrderHubPrintingMonitor.kt\ncom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor\n+ 2 Rx2.kt\ncom/squareup/util/rx2/Rx2Kt\n*L\n1#1,142:1\n19#2:143\n*E\n*S KotlinDebug\n*F\n+ 1 OrderHubPrintingMonitor.kt\ncom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor\n*L\n73#1:143\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0007\u0018\u00002\u00020\u0001Bg\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u000e\u0008\u0001\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u000e\u0008\u0001\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u0012\u0006\u0010\u0014\u001a\u00020\u0015\u00a2\u0006\u0002\u0010\u0016J\u0010\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u001fH\u0016J\u0008\u0010 \u001a\u00020\u001dH\u0016J\u0014\u0010!\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u001d0#0\"H\u0002J\u0014\u0010$\u001a\u00020\u0008*\u00020\u00192\u0006\u0010%\u001a\u00020&H\u0002R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0017\u001a\u0004\u0018\u00010\u0018*\u00020\u00198BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u001a\u0010\u001b\u00a8\u0006\'"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;",
        "Lmortar/Scoped;",
        "orderRepository",
        "Lcom/squareup/ordermanagerdata/OrderRepository;",
        "orderPrintingDispatcher",
        "Lcom/squareup/print/OrderPrintingDispatcher;",
        "orderPrintingEnabledPreference",
        "Lcom/f2prateek/rx/preferences2/Preference;",
        "",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "res",
        "Lcom/squareup/util/Res;",
        "ordersPrintedBeforePreference",
        "orderHubAnalytics",
        "Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;",
        "accountStatusSettings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "dateAndTimeFormatter",
        "Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;",
        "currentTime",
        "Lcom/squareup/time/CurrentTime;",
        "(Lcom/squareup/ordermanagerdata/OrderRepository;Lcom/squareup/print/OrderPrintingDispatcher;Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/settings/server/Features;Lcom/squareup/util/Res;Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;Lcom/squareup/time/CurrentTime;)V",
        "shouldPrintByDate",
        "",
        "Lcom/squareup/orders/model/Order;",
        "getShouldPrintByDate",
        "(Lcom/squareup/orders/model/Order;)Ljava/lang/String;",
        "onEnterScope",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "printOrders",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/ordermanagerdata/ResultState;",
        "shouldAutoPrint",
        "now",
        "Lorg/threeten/bp/ZonedDateTime;",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final currentTime:Lcom/squareup/time/CurrentTime;

.field private final dateAndTimeFormatter:Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final orderHubAnalytics:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;

.field private final orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

.field private final orderPrintingEnabledPreference:Lcom/f2prateek/rx/preferences2/Preference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final orderRepository:Lcom/squareup/ordermanagerdata/OrderRepository;

.field private final ordersPrintedBeforePreference:Lcom/f2prateek/rx/preferences2/Preference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/ordermanagerdata/OrderRepository;Lcom/squareup/print/OrderPrintingDispatcher;Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/settings/server/Features;Lcom/squareup/util/Res;Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;Lcom/squareup/time/CurrentTime;)V
    .locals 1
    .param p3    # Lcom/f2prateek/rx/preferences2/Preference;
        .annotation runtime Lcom/squareup/orderhub/settings/OrderHubPrintingEnabledPreference;
        .end annotation
    .end param
    .param p6    # Lcom/f2prateek/rx/preferences2/Preference;
        .annotation runtime Lcom/squareup/orderhub/alerts/OrderHubOrdersPrintedBeforePref;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ordermanagerdata/OrderRepository;",
            "Lcom/squareup/print/OrderPrintingDispatcher;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/util/Res;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;",
            "Lcom/squareup/time/CurrentTime;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "orderRepository"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderPrintingDispatcher"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderPrintingEnabledPreference"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "ordersPrintedBeforePreference"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderHubAnalytics"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "accountStatusSettings"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "dateAndTimeFormatter"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currentTime"

    invoke-static {p10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;->orderRepository:Lcom/squareup/ordermanagerdata/OrderRepository;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;->orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

    iput-object p3, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;->orderPrintingEnabledPreference:Lcom/f2prateek/rx/preferences2/Preference;

    iput-object p4, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;->features:Lcom/squareup/settings/server/Features;

    iput-object p5, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;->res:Lcom/squareup/util/Res;

    iput-object p6, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;->ordersPrintedBeforePreference:Lcom/f2prateek/rx/preferences2/Preference;

    iput-object p7, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;->orderHubAnalytics:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;

    iput-object p8, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    iput-object p9, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;->dateAndTimeFormatter:Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;

    iput-object p10, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;->currentTime:Lcom/squareup/time/CurrentTime;

    return-void
.end method

.method public static final synthetic access$getCurrentTime$p(Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;)Lcom/squareup/time/CurrentTime;
    .locals 0

    .line 44
    iget-object p0, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;->currentTime:Lcom/squareup/time/CurrentTime;

    return-object p0
.end method

.method public static final synthetic access$getDateAndTimeFormatter$p(Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;)Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;
    .locals 0

    .line 44
    iget-object p0, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;->dateAndTimeFormatter:Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;

    return-object p0
.end method

.method public static final synthetic access$getOrderHubAnalytics$p(Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;)Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;
    .locals 0

    .line 44
    iget-object p0, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;->orderHubAnalytics:Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;

    return-object p0
.end method

.method public static final synthetic access$getOrderPrintingDispatcher$p(Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;)Lcom/squareup/print/OrderPrintingDispatcher;
    .locals 0

    .line 44
    iget-object p0, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;->orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

    return-object p0
.end method

.method public static final synthetic access$getOrderRepository$p(Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;)Lcom/squareup/ordermanagerdata/OrderRepository;
    .locals 0

    .line 44
    iget-object p0, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;->orderRepository:Lcom/squareup/ordermanagerdata/OrderRepository;

    return-object p0
.end method

.method public static final synthetic access$getOrdersPrintedBeforePreference$p(Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;)Lcom/f2prateek/rx/preferences2/Preference;
    .locals 0

    .line 44
    iget-object p0, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;->ordersPrintedBeforePreference:Lcom/f2prateek/rx/preferences2/Preference;

    return-object p0
.end method

.method public static final synthetic access$getRes$p(Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;)Lcom/squareup/util/Res;
    .locals 0

    .line 44
    iget-object p0, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;->res:Lcom/squareup/util/Res;

    return-object p0
.end method

.method public static final synthetic access$shouldAutoPrint(Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;Lcom/squareup/orders/model/Order;Lorg/threeten/bp/ZonedDateTime;)Z
    .locals 0

    .line 44
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;->shouldAutoPrint(Lcom/squareup/orders/model/Order;Lorg/threeten/bp/ZonedDateTime;)Z

    move-result p0

    return p0
.end method

.method private final getShouldPrintByDate(Lcom/squareup/orders/model/Order;)Ljava/lang/String;
    .locals 2

    .line 126
    invoke-static {p1}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getPrimaryFulfillment(Lcom/squareup/orders/model/Order;)Lcom/squareup/orders/model/Order$Fulfillment;

    move-result-object v0

    iget-object v0, v0, Lcom/squareup/orders/model/Order$Fulfillment;->type:Lcom/squareup/orders/model/Order$FulfillmentType;

    if-eqz v0, :cond_0

    sget-object v1, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/orders/model/Order$FulfillmentType;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 136
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 135
    :pswitch_0
    invoke-static {p1}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getPrimaryFulfillment(Lcom/squareup/orders/model/Order;)Lcom/squareup/orders/model/Order$Fulfillment;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/orders/model/Order$Fulfillment;->delivery_details:Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;

    iget-object p1, p1, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->deliver_at:Ljava/lang/String;

    goto :goto_0

    .line 134
    :pswitch_1
    invoke-static {p1}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getPrimaryFulfillment(Lcom/squareup/orders/model/Order;)Lcom/squareup/orders/model/Order$Fulfillment;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/orders/model/Order$Fulfillment;->pickup_details:Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;

    iget-object p1, p1, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->pickup_at:Ljava/lang/String;

    goto :goto_0

    .line 129
    :pswitch_2
    invoke-static {p1}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getPrimaryFulfillment(Lcom/squareup/orders/model/Order;)Lcom/squareup/orders/model/Order$Fulfillment;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/ordermanagerdata/proto/FulfillmentsKt;->getPlacedAt(Lcom/squareup/orders/model/Order$Fulfillment;)Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1

    .line 137
    :cond_0
    :pswitch_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unexpected fulfillment type "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p1}, Lcom/squareup/ordermanagerdata/proto/OrdersKt;->getPrimaryFulfillment(Lcom/squareup/orders/model/Order;)Lcom/squareup/orders/model/Order$Fulfillment;

    move-result-object p1

    iget-object p1, p1, Lcom/squareup/orders/model/Order$Fulfillment;->type:Lcom/squareup/orders/model/Order$FulfillmentType;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 136
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private final printOrders()Lio/reactivex/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ordermanagerdata/ResultState<",
            "Lkotlin/Unit;",
            ">;>;"
        }
    .end annotation

    .line 71
    iget-object v0, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;->orderRepository:Lcom/squareup/ordermanagerdata/OrderRepository;

    .line 72
    invoke-interface {v0}, Lcom/squareup/ordermanagerdata/OrderRepository;->unprintedNewOrders()Lio/reactivex/Observable;

    move-result-object v0

    .line 143
    const-class v1, Lcom/squareup/ordermanagerdata/ResultState$Success;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->ofType(Ljava/lang/Class;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "ofType(T::class.java)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    sget-object v1, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor$printOrders$unprintedNewOrders$1;->INSTANCE:Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor$printOrders$unprintedNewOrders$1;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "unprintedNewOrders"

    .line 76
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    iget-object v1, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->ORDERHUB_APPLET_ROLLOUT:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->featureEnabled(Lcom/squareup/settings/server/Features$Feature;)Lio/reactivex/Observable;

    move-result-object v1

    const-string v2, "features.featureEnabled(\u2026.ORDERHUB_APPLET_ROLLOUT)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/squareup/util/rx2/Rx2TransformersKt;->gateBy(Lio/reactivex/Observable;Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object v0

    .line 79
    iget-object v1, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;->orderPrintingEnabledPreference:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {v1}, Lcom/f2prateek/rx/preferences2/Preference;->asObservable()Lio/reactivex/Observable;

    move-result-object v1

    const-string v2, "orderPrintingEnabledPreference.asObservable()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lio/reactivex/ObservableSource;

    .line 80
    iget-object v2, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;->ordersPrintedBeforePreference:Lcom/f2prateek/rx/preferences2/Preference;

    invoke-interface {v2}, Lcom/f2prateek/rx/preferences2/Preference;->asObservable()Lio/reactivex/Observable;

    move-result-object v2

    const-string v3, "ordersPrintedBeforePreference.asObservable()"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v2, Lio/reactivex/ObservableSource;

    .line 78
    invoke-static {v0, v1, v2}, Lcom/squareup/util/rx2/RxKotlinKt;->withLatestFrom(Lio/reactivex/Observable;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object v0

    .line 82
    new-instance v1, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor$printOrders$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor$printOrders$1;-><init>(Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 107
    sget-object v1, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor$printOrders$2;->INSTANCE:Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor$printOrders$2;

    check-cast v1, Lio/reactivex/functions/Predicate;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    .line 108
    new-instance v1, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor$printOrders$3;

    invoke-direct {v1, p0}, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor$printOrders$3;-><init>(Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->switchMapSingle(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "unprintedNewOrders\n     \u2026rsAsPrinted(it)\n        }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final shouldAutoPrint(Lcom/squareup/orders/model/Order;Lorg/threeten/bp/ZonedDateTime;)Z
    .locals 6

    .line 116
    iget-object v0, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getOrderHubSettings()Lcom/squareup/settings/server/OrderHubSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/OrderHubSettings;->getAutomaticActionMaxAgeSeconds()Ljava/lang/Long;

    move-result-object v0

    .line 117
    invoke-direct {p0, p1}, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;->getShouldPrintByDate(Lcom/squareup/orders/model/Order;)Ljava/lang/String;

    move-result-object p1

    if-eqz v0, :cond_1

    .line 121
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long v5, v1, v3

    if-lez v5, :cond_1

    if-eqz p1, :cond_1

    iget-object v1, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;->dateAndTimeFormatter:Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;

    invoke-virtual {v1, p1}, Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;->parse(Ljava/lang/String;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object p1

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p2, v0, v1}, Lorg/threeten/bp/ZonedDateTime;->minusSeconds(J)Lorg/threeten/bp/ZonedDateTime;

    move-result-object p2

    check-cast p2, Lorg/threeten/bp/chrono/ChronoZonedDateTime;

    invoke-virtual {p1, p2}, Lorg/threeten/bp/ZonedDateTime;->isAfter(Lorg/threeten/bp/chrono/ChronoZonedDateTime;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method


# virtual methods
.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 3

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 61
    invoke-direct {p0}, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;->printOrders()Lio/reactivex/Observable;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-static {v0, p1, v1, v2, v1}, Lcom/squareup/mortar/Rx2ObservablesKt;->subscribeWith$default(Lio/reactivex/Observable;Lmortar/MortarScope;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lio/reactivex/disposables/Disposable;

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method
