.class public final Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator$Factory;
.super Ljava/lang/Object;
.source "OrderHubMasterCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000L\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B9\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0008\u0008\u0001\u0010\u000c\u001a\u00020\r\u00a2\u0006\u0002\u0010\u000eJ*\u0010\u000f\u001a\u00020\u00102\"\u0010\u0011\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0014\u0012\u0004\u0012\u00020\u00150\u0013j\u0008\u0012\u0004\u0012\u00020\u0014`\u00160\u0012R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator$Factory;",
        "",
        "res",
        "Lcom/squareup/util/Res;",
        "badgePresenter",
        "Lcom/squareup/applet/BadgePresenter;",
        "orderHubBackButtonConfig",
        "Lcom/squareup/ui/orderhub/util/config/OrderHubBackButtonConfig;",
        "actionBarNavigationHelper",
        "Lcom/squareup/applet/ActionBarNavigationHelper;",
        "appletSelection",
        "Lcom/squareup/applet/AppletSelection;",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "(Lcom/squareup/util/Res;Lcom/squareup/applet/BadgePresenter;Lcom/squareup/ui/orderhub/util/config/OrderHubBackButtonConfig;Lcom/squareup/applet/ActionBarNavigationHelper;Lcom/squareup/applet/AppletSelection;Lio/reactivex/Scheduler;)V",
        "create",
        "Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/ui/orderhub/master/OrderHubMasterScreen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final actionBarNavigationHelper:Lcom/squareup/applet/ActionBarNavigationHelper;

.field private final appletSelection:Lcom/squareup/applet/AppletSelection;

.field private final badgePresenter:Lcom/squareup/applet/BadgePresenter;

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final orderHubBackButtonConfig:Lcom/squareup/ui/orderhub/util/config/OrderHubBackButtonConfig;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/applet/BadgePresenter;Lcom/squareup/ui/orderhub/util/config/OrderHubBackButtonConfig;Lcom/squareup/applet/ActionBarNavigationHelper;Lcom/squareup/applet/AppletSelection;Lio/reactivex/Scheduler;)V
    .locals 1
    .param p6    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "badgePresenter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderHubBackButtonConfig"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "actionBarNavigationHelper"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "appletSelection"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator$Factory;->res:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator$Factory;->badgePresenter:Lcom/squareup/applet/BadgePresenter;

    iput-object p3, p0, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator$Factory;->orderHubBackButtonConfig:Lcom/squareup/ui/orderhub/util/config/OrderHubBackButtonConfig;

    iput-object p4, p0, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator$Factory;->actionBarNavigationHelper:Lcom/squareup/applet/ActionBarNavigationHelper;

    iput-object p5, p0, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator$Factory;->appletSelection:Lcom/squareup/applet/AppletSelection;

    iput-object p6, p0, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator$Factory;->mainScheduler:Lio/reactivex/Scheduler;

    return-void
.end method


# virtual methods
.method public final create(Lio/reactivex/Observable;)Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;)",
            "Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator;"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    new-instance v0, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator;

    .line 52
    iget-object v2, p0, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator$Factory;->res:Lcom/squareup/util/Res;

    .line 53
    iget-object v3, p0, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator$Factory;->badgePresenter:Lcom/squareup/applet/BadgePresenter;

    .line 54
    iget-object v4, p0, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator$Factory;->orderHubBackButtonConfig:Lcom/squareup/ui/orderhub/util/config/OrderHubBackButtonConfig;

    .line 55
    iget-object v5, p0, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator$Factory;->actionBarNavigationHelper:Lcom/squareup/applet/ActionBarNavigationHelper;

    .line 56
    iget-object v6, p0, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator$Factory;->appletSelection:Lcom/squareup/applet/AppletSelection;

    .line 57
    iget-object v7, p0, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator$Factory;->mainScheduler:Lio/reactivex/Scheduler;

    move-object v1, v0

    move-object v8, p1

    .line 51
    invoke-direct/range {v1 .. v8}, Lcom/squareup/ui/orderhub/master/OrderHubMasterCoordinator;-><init>(Lcom/squareup/util/Res;Lcom/squareup/applet/BadgePresenter;Lcom/squareup/ui/orderhub/util/config/OrderHubBackButtonConfig;Lcom/squareup/applet/ActionBarNavigationHelper;Lcom/squareup/applet/AppletSelection;Lio/reactivex/Scheduler;Lio/reactivex/Observable;)V

    return-object v0
.end method
