.class public final Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$FilterViewHolder;
.super Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$ViewHolder;
.source "DetailFiltersAdapter.kt"

# interfaces
.implements Lcom/squareup/noho/NohoEdgeDecoration$ShowsEdges;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "FilterViewHolder"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDetailFiltersAdapter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DetailFiltersAdapter.kt\ncom/squareup/ui/orderhub/detail/DetailFiltersAdapter$FilterViewHolder\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,131:1\n1103#2,7:132\n*E\n*S KotlinDebug\n*F\n+ 1 DetailFiltersAdapter.kt\ncom/squareup/ui/orderhub/detail/DetailFiltersAdapter$FilterViewHolder\n*L\n96#1,7:132\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0008\u0080\u0004\u0018\u00002\u00020\u00012\u00020\u0002B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0010\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\u0007H\u0016R\u0014\u0010\u0006\u001a\u00020\u0007X\u0096D\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\t\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$FilterViewHolder;",
        "Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$ViewHolder;",
        "Lcom/squareup/noho/NohoEdgeDecoration$ShowsEdges;",
        "itemView",
        "Landroid/view/View;",
        "(Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter;Landroid/view/View;)V",
        "edges",
        "",
        "getEdges",
        "()I",
        "bind",
        "",
        "position",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final edges:I

.field final synthetic this$0:Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    const-string v0, "itemView"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 88
    iput-object p1, p0, Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$FilterViewHolder;->this$0:Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter;

    invoke-direct {p0, p2}, Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$ViewHolder;-><init>(Landroid/view/View;)V

    const/16 p1, 0x8

    .line 90
    iput p1, p0, Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$FilterViewHolder;->edges:I

    return-void
.end method


# virtual methods
.method public bind(I)V
    .locals 3

    .line 93
    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$FilterViewHolder;->itemView:Landroid/view/View;

    sget v1, Lcom/squareup/orderhub/applet/R$id;->orderhub_detail_filter_row:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    check-cast v0, Lcom/squareup/noho/NohoCheckableRow;

    .line 94
    iget-object v1, p0, Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$FilterViewHolder;->this$0:Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter;

    invoke-static {v1}, Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter;->access$getFilterList$p(Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    const-string v2, "filterList[position]"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$FilterRow;

    .line 95
    iget-object v2, p0, Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$FilterViewHolder;->this$0:Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter;

    invoke-static {v2}, Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter;->access$getFilterList$p(Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$FilterRow;

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$FilterRow;->getFilter()Lcom/squareup/ui/orderhub/master/Filter;

    move-result-object p1

    iget-object v2, p0, Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$FilterViewHolder;->this$0:Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter;

    invoke-static {v2}, Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter;->access$getRes$p(Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter;)Lcom/squareup/util/Res;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/squareup/ui/orderhub/master/Filter;->getFilterName(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoCheckableRow;->setLabel(Ljava/lang/CharSequence;)V

    .line 96
    move-object p1, v0

    check-cast p1, Landroid/view/View;

    .line 132
    new-instance v2, Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$FilterViewHolder$bind$$inlined$onClickDebounced$1;

    invoke-direct {v2, p0, v1}, Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$FilterViewHolder$bind$$inlined$onClickDebounced$1;-><init>(Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$FilterViewHolder;Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$FilterRow;)V

    check-cast v2, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 99
    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$FilterRow;->isSelected()Z

    move-result p1

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoCheckableRow;->setChecked(Z)V

    .line 100
    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$FilterRow;->getOrderCount()I

    move-result p1

    if-lez p1, :cond_0

    .line 102
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoCheckableRow;->setValue(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 104
    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Lcom/squareup/noho/NohoCheckableRow;->setValue(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    .line 93
    :cond_1
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.squareup.noho.NohoCheckableRow"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public getEdges()I
    .locals 1

    .line 90
    iget v0, p0, Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$FilterViewHolder;->edges:I

    return v0
.end method

.method public getPadding(Landroid/graphics/Rect;)V
    .locals 1

    const-string v0, "outRect"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 88
    invoke-static {p0, p1}, Lcom/squareup/noho/NohoEdgeDecoration$ShowsEdges$DefaultImpls;->getPadding(Lcom/squareup/noho/NohoEdgeDecoration$ShowsEdges;Landroid/graphics/Rect;)V

    return-void
.end method
