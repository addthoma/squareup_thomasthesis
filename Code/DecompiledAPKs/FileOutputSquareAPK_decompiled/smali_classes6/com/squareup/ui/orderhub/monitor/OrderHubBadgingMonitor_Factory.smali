.class public final Lcom/squareup/ui/orderhub/monitor/OrderHubBadgingMonitor_Factory;
.super Ljava/lang/Object;
.source "OrderHubBadgingMonitor_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/orderhub/monitor/OrderHubBadgingMonitor;",
        ">;"
    }
.end annotation


# instance fields
.field private final appletSelectionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/AppletSelection;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final orderHubAppletProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/OrderHubApplet;",
            ">;"
        }
    .end annotation
.end field

.field private final orderRepositoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ordermanagerdata/OrderRepository;",
            ">;"
        }
    .end annotation
.end field

.field private final statusBarEventManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/statusbar/event/StatusBarEventManager;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ordermanagerdata/OrderRepository;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/statusbar/event/StatusBarEventManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/OrderHubApplet;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/AppletSelection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubBadgingMonitor_Factory;->orderRepositoryProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p2, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubBadgingMonitor_Factory;->statusBarEventManagerProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p3, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubBadgingMonitor_Factory;->orderHubAppletProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p4, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubBadgingMonitor_Factory;->appletSelectionProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p5, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubBadgingMonitor_Factory;->featuresProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/orderhub/monitor/OrderHubBadgingMonitor_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ordermanagerdata/OrderRepository;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/statusbar/event/StatusBarEventManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/OrderHubApplet;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/applet/AppletSelection;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Lcom/squareup/ui/orderhub/monitor/OrderHubBadgingMonitor_Factory;"
        }
    .end annotation

    .line 52
    new-instance v6, Lcom/squareup/ui/orderhub/monitor/OrderHubBadgingMonitor_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/orderhub/monitor/OrderHubBadgingMonitor_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Lcom/squareup/ordermanagerdata/OrderRepository;Lcom/squareup/statusbar/event/StatusBarEventManager;Lcom/squareup/ui/orderhub/OrderHubApplet;Lcom/squareup/applet/AppletSelection;Lcom/squareup/settings/server/Features;)Lcom/squareup/ui/orderhub/monitor/OrderHubBadgingMonitor;
    .locals 7

    .line 58
    new-instance v6, Lcom/squareup/ui/orderhub/monitor/OrderHubBadgingMonitor;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/orderhub/monitor/OrderHubBadgingMonitor;-><init>(Lcom/squareup/ordermanagerdata/OrderRepository;Lcom/squareup/statusbar/event/StatusBarEventManager;Lcom/squareup/ui/orderhub/OrderHubApplet;Lcom/squareup/applet/AppletSelection;Lcom/squareup/settings/server/Features;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/ui/orderhub/monitor/OrderHubBadgingMonitor;
    .locals 5

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubBadgingMonitor_Factory;->orderRepositoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ordermanagerdata/OrderRepository;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubBadgingMonitor_Factory;->statusBarEventManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/statusbar/event/StatusBarEventManager;

    iget-object v2, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubBadgingMonitor_Factory;->orderHubAppletProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/orderhub/OrderHubApplet;

    iget-object v3, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubBadgingMonitor_Factory;->appletSelectionProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/applet/AppletSelection;

    iget-object v4, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubBadgingMonitor_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/settings/server/Features;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/ui/orderhub/monitor/OrderHubBadgingMonitor_Factory;->newInstance(Lcom/squareup/ordermanagerdata/OrderRepository;Lcom/squareup/statusbar/event/StatusBarEventManager;Lcom/squareup/ui/orderhub/OrderHubApplet;Lcom/squareup/applet/AppletSelection;Lcom/squareup/settings/server/Features;)Lcom/squareup/ui/orderhub/monitor/OrderHubBadgingMonitor;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/monitor/OrderHubBadgingMonitor_Factory;->get()Lcom/squareup/ui/orderhub/monitor/OrderHubBadgingMonitor;

    move-result-object v0

    return-object v0
.end method
