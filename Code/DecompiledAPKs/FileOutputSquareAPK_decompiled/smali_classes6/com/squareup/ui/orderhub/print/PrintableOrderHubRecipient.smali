.class public final Lcom/squareup/ui/orderhub/print/PrintableOrderHubRecipient;
.super Ljava/lang/Object;
.source "PrintableOrderHubRecipient.kt"

# interfaces
.implements Lcom/squareup/print/PrintableRecipient;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPrintableOrderHubRecipient.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PrintableOrderHubRecipient.kt\ncom/squareup/ui/orderhub/print/PrintableOrderHubRecipient\n*L\n1#1,36:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0000\u0008\u0000\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010\u000f\u001a\u00020\u00082\u0006\u0010\u0010\u001a\u00020\u0011H\u0016R\u0016\u0010\u0007\u001a\u0004\u0018\u00010\u0008X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0016\u0010\u000b\u001a\u0004\u0018\u00010\u0008X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\nR\u0016\u0010\r\u001a\u0004\u0018\u00010\u0008X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\nR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/print/PrintableOrderHubRecipient;",
        "Lcom/squareup/print/PrintableRecipient;",
        "recipient",
        "Lcom/squareup/orders/model/Order$FulfillmentRecipient;",
        "type",
        "Lcom/squareup/orders/model/Order$FulfillmentType;",
        "(Lcom/squareup/orders/model/Order$FulfillmentRecipient;Lcom/squareup/orders/model/Order$FulfillmentType;)V",
        "recipientAddress",
        "",
        "getRecipientAddress",
        "()Ljava/lang/String;",
        "recipientName",
        "getRecipientName",
        "recipientPhoneNumber",
        "getRecipientPhoneNumber",
        "recipientLabel",
        "res",
        "Lcom/squareup/util/Res;",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final recipientAddress:Ljava/lang/String;

.field private final recipientName:Ljava/lang/String;

.field private final recipientPhoneNumber:Ljava/lang/String;

.field private final type:Lcom/squareup/orders/model/Order$FulfillmentType;


# direct methods
.method public constructor <init>(Lcom/squareup/orders/model/Order$FulfillmentRecipient;Lcom/squareup/orders/model/Order$FulfillmentType;)V
    .locals 10

    const-string v0, "recipient"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "type"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubRecipient;->type:Lcom/squareup/orders/model/Order$FulfillmentType;

    .line 17
    iget-object p2, p1, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->display_name:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubRecipient;->recipientName:Ljava/lang/String;

    .line 19
    iget-object p2, p1, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->address:Lcom/squareup/protos/connect/v2/resources/Address;

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    .line 20
    sget-object v1, Lcom/squareup/address/Address;->Companion:Lcom/squareup/address/Address$Companion;

    invoke-virtual {v1, p2}, Lcom/squareup/address/Address$Companion;->fromConnectV2Address(Lcom/squareup/protos/connect/v2/resources/Address;)Lcom/squareup/address/Address;

    move-result-object p2

    .line 21
    invoke-static {p2}, Lcom/squareup/address/AddressFormatter;->formatForShippingDisplay(Lcom/squareup/address/Address;)Ljava/util/List;

    move-result-object p2

    move-object v1, p2

    check-cast v1, Ljava/lang/Iterable;

    const-string p2, "\n"

    .line 22
    move-object v2, p2

    check-cast v2, Ljava/lang/CharSequence;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x3e

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lkotlin/collections/CollectionsKt;->joinToString$default(Ljava/lang/Iterable;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/CharSequence;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    :cond_0
    move-object p2, v0

    .line 19
    :goto_0
    iput-object p2, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubRecipient;->recipientAddress:Ljava/lang/String;

    .line 26
    iget-object p2, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubRecipient;->type:Lcom/squareup/orders/model/Order$FulfillmentType;

    sget-object v1, Lcom/squareup/orders/model/Order$FulfillmentType;->DELIVERY:Lcom/squareup/orders/model/Order$FulfillmentType;

    if-ne p2, v1, :cond_1

    iget-object v0, p1, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->phone_number:Ljava/lang/String;

    :cond_1
    iput-object v0, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubRecipient;->recipientPhoneNumber:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getRecipientAddress()Ljava/lang/String;
    .locals 1

    .line 19
    iget-object v0, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubRecipient;->recipientAddress:Ljava/lang/String;

    return-object v0
.end method

.method public getRecipientName()Ljava/lang/String;
    .locals 1

    .line 17
    iget-object v0, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubRecipient;->recipientName:Ljava/lang/String;

    return-object v0
.end method

.method public getRecipientPhoneNumber()Ljava/lang/String;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubRecipient;->recipientPhoneNumber:Ljava/lang/String;

    return-object v0
.end method

.method public recipientLabel(Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 2

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    iget-object v0, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubRecipient;->type:Lcom/squareup/orders/model/Order$FulfillmentType;

    sget-object v1, Lcom/squareup/ui/orderhub/print/PrintableOrderHubRecipient$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/orders/model/Order$FulfillmentType;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 31
    sget v0, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_shipment_to_header_for_print:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 32
    :cond_0
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Didn\'t expect to retrieve a recipient for type "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/print/PrintableOrderHubRecipient;->type:Lcom/squareup/orders/model/Order$FulfillmentType;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v0, 0x2e

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 30
    :cond_1
    sget v0, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_delivery_deliver_to_header_for_print:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method
