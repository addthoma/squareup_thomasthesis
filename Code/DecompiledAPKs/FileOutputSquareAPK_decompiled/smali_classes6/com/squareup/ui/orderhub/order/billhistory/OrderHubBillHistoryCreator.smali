.class public final Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryCreator;
.super Ljava/lang/Object;
.source "OrderHubBillHistoryCreator.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0007\u0018\u00002\u00020\u0001B\u001f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u0015\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0000\u00a2\u0006\u0002\u0008\rJ\u001d\u0010\u000e\u001a\u00020\n2\u0006\u0010\u000f\u001a\u00020\n2\u0006\u0010\u0010\u001a\u00020\u0011H\u0000\u00a2\u0006\u0002\u0008\u0012R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryCreator;",
        "",
        "res",
        "Lcom/squareup/util/Res;",
        "voidCompSettings",
        "Lcom/squareup/tickets/voidcomp/VoidCompSettings;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "(Lcom/squareup/util/Res;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/settings/server/Features;)V",
        "toBill",
        "Lcom/squareup/billhistory/model/BillHistory;",
        "bill",
        "Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;",
        "toBill$orderhub_applet_release",
        "toRefundBill",
        "billHistory",
        "refundBill",
        "Lcom/squareup/protos/client/bills/Bill;",
        "toRefundBill$orderhub_applet_release",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final features:Lcom/squareup/settings/server/Features;

.field private final res:Lcom/squareup/util/Res;

.field private final voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/tickets/voidcomp/VoidCompSettings;Lcom/squareup/settings/server/Features;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "voidCompSettings"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryCreator;->res:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryCreator;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    iput-object p3, p0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryCreator;->features:Lcom/squareup/settings/server/Features;

    return-void
.end method


# virtual methods
.method public final toBill$orderhub_applet_release(Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;)Lcom/squareup/billhistory/model/BillHistory;
    .locals 4

    const-string v0, "bill"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryCreator;->res:Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryCreator;->voidCompSettings:Lcom/squareup/tickets/voidcomp/VoidCompSettings;

    invoke-virtual {v1}, Lcom/squareup/tickets/voidcomp/VoidCompSettings;->isCompEnabled()Z

    move-result v1

    .line 40
    iget-object v2, p0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryCreator;->features:Lcom/squareup/settings/server/Features;

    sget-object v3, Lcom/squareup/settings/server/Features$Feature;->DISCOUNT_APPLY_MULTIPLE_COUPONS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v2, v3}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v2

    .line 39
    invoke-static {p1, v0, v1, v2}, Lcom/squareup/billhistory/Bills;->toBill(Lcom/squareup/protos/client/bills/GetBillFamiliesResponse$BillFamily;Lcom/squareup/util/Res;ZZ)Lcom/squareup/billhistory/model/BillHistory;

    move-result-object p1

    const-string v0, "toBill(bill, res, voidCo\u2026_APPLY_MULTIPLE_COUPONS))"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final toRefundBill$orderhub_applet_release(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/protos/client/bills/Bill;)Lcom/squareup/billhistory/model/BillHistory;
    .locals 1

    const-string v0, "billHistory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "refundBill"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryCreator;->res:Lcom/squareup/util/Res;

    invoke-static {p1, p2, v0}, Lcom/squareup/billhistory/Bills;->toRefundBill(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/protos/client/bills/Bill;Lcom/squareup/util/Res;)Lcom/squareup/billhistory/model/BillHistory;

    move-result-object p1

    const-string p2, "Bills.toRefundBill(billHistory, refundBill, res)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
