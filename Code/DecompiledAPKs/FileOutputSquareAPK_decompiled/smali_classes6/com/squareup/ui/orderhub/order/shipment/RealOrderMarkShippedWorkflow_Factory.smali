.class public final Lcom/squareup/ui/orderhub/order/shipment/RealOrderMarkShippedWorkflow_Factory;
.super Ljava/lang/Object;
.source "RealOrderMarkShippedWorkflow_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/orderhub/order/shipment/RealOrderMarkShippedWorkflow;",
        ">;"
    }
.end annotation


# instance fields
.field private final orderEditTrackingWorkflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final orderHubAnalyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;",
            ">;"
        }
    .end annotation
.end field

.field private final orderItemSelectionWorkflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final orderRepositoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ordermanagerdata/OrderRepository;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ordermanagerdata/OrderRepository;",
            ">;)V"
        }
    .end annotation

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/shipment/RealOrderMarkShippedWorkflow_Factory;->orderItemSelectionWorkflowProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p2, p0, Lcom/squareup/ui/orderhub/order/shipment/RealOrderMarkShippedWorkflow_Factory;->orderEditTrackingWorkflowProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p3, p0, Lcom/squareup/ui/orderhub/order/shipment/RealOrderMarkShippedWorkflow_Factory;->orderHubAnalyticsProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p4, p0, Lcom/squareup/ui/orderhub/order/shipment/RealOrderMarkShippedWorkflow_Factory;->orderRepositoryProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/orderhub/order/shipment/RealOrderMarkShippedWorkflow_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ordermanagerdata/OrderRepository;",
            ">;)",
            "Lcom/squareup/ui/orderhub/order/shipment/RealOrderMarkShippedWorkflow_Factory;"
        }
    .end annotation

    .line 49
    new-instance v0, Lcom/squareup/ui/orderhub/order/shipment/RealOrderMarkShippedWorkflow_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/orderhub/order/shipment/RealOrderMarkShippedWorkflow_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionWorkflow;Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingWorkflow;Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;Lcom/squareup/ordermanagerdata/OrderRepository;)Lcom/squareup/ui/orderhub/order/shipment/RealOrderMarkShippedWorkflow;
    .locals 1

    .line 56
    new-instance v0, Lcom/squareup/ui/orderhub/order/shipment/RealOrderMarkShippedWorkflow;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/orderhub/order/shipment/RealOrderMarkShippedWorkflow;-><init>(Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionWorkflow;Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingWorkflow;Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;Lcom/squareup/ordermanagerdata/OrderRepository;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/orderhub/order/shipment/RealOrderMarkShippedWorkflow;
    .locals 4

    .line 41
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/shipment/RealOrderMarkShippedWorkflow_Factory;->orderItemSelectionWorkflowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionWorkflow;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/shipment/RealOrderMarkShippedWorkflow_Factory;->orderEditTrackingWorkflowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingWorkflow;

    iget-object v2, p0, Lcom/squareup/ui/orderhub/order/shipment/RealOrderMarkShippedWorkflow_Factory;->orderHubAnalyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;

    iget-object v3, p0, Lcom/squareup/ui/orderhub/order/shipment/RealOrderMarkShippedWorkflow_Factory;->orderRepositoryProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/ordermanagerdata/OrderRepository;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/ui/orderhub/order/shipment/RealOrderMarkShippedWorkflow_Factory;->newInstance(Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionWorkflow;Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingWorkflow;Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;Lcom/squareup/ordermanagerdata/OrderRepository;)Lcom/squareup/ui/orderhub/order/shipment/RealOrderMarkShippedWorkflow;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/shipment/RealOrderMarkShippedWorkflow_Factory;->get()Lcom/squareup/ui/orderhub/order/shipment/RealOrderMarkShippedWorkflow;

    move-result-object v0

    return-object v0
.end method
