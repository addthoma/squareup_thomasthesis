.class public final Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Factory;
.super Ljava/lang/Object;
.source "OrderHubTrackingRunner.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u000e\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Factory;",
        "",
        "res",
        "Lcom/squareup/util/Res;",
        "recyclerFactory",
        "Lcom/squareup/recycler/RecyclerFactory;",
        "(Lcom/squareup/util/Res;Lcom/squareup/recycler/RecyclerFactory;)V",
        "create",
        "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;",
        "view",
        "Landroid/view/View;",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/recycler/RecyclerFactory;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "recyclerFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Factory;->res:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Factory;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    return-void
.end method


# virtual methods
.method public final create(Landroid/view/View;)Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;
    .locals 3

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    new-instance v0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Factory;->res:Lcom/squareup/util/Res;

    iget-object v2, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Factory;->recyclerFactory:Lcom/squareup/recycler/RecyclerFactory;

    invoke-direct {v0, p1, v1, v2}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;-><init>(Landroid/view/View;Lcom/squareup/util/Res;Lcom/squareup/recycler/RecyclerFactory;)V

    return-object v0
.end method
