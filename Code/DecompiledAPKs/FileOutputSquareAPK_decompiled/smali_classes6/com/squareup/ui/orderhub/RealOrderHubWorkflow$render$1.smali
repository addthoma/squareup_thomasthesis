.class final Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$1;
.super Ljava/lang/Object;
.source "RealOrderHubWorkflow.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->render(Lcom/squareup/ui/orderhub/OrderHubWorkflowInput;Lcom/squareup/ui/orderhub/OrderHubState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0004\u0008\u0005\u0010\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "",
        "kotlin.jvm.PlatformType",
        "accept",
        "(Ljava/lang/Boolean;)V"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/ui/orderhub/OrderHubState;

.field final synthetic this$0:Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;Lcom/squareup/ui/orderhub/OrderHubState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$1;->this$0:Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$1;->$state:Lcom/squareup/ui/orderhub/OrderHubState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Ljava/lang/Boolean;)V
    .locals 10

    .line 152
    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 153
    iget-object v0, p0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$1;->$state:Lcom/squareup/ui/orderhub/OrderHubState;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$1;->this$0:Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;

    invoke-static {v1}, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->access$getOrderQuickActionsEnabledPreference$p(Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;)Lcom/f2prateek/rx/preferences2/Preference;

    move-result-object v1

    invoke-interface {v1}, Lcom/f2prateek/rx/preferences2/Preference;->get()Ljava/lang/Object;

    move-result-object v1

    const-string v2, "orderQuickActionsEnabledPreference.get()"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v8, 0x3f

    const/4 v9, 0x0

    invoke-static/range {v0 .. v9}, Lcom/squareup/ui/orderhub/OrderHubState;->copy$default(Lcom/squareup/ui/orderhub/OrderHubState;ZZLjava/util/List;Lcom/squareup/ui/orderhub/MasterState;Lcom/squareup/ui/orderhub/DetailState;Lcom/squareup/ui/orderhub/OrderWorkflowAction;ZILjava/lang/Object;)Lcom/squareup/ui/orderhub/OrderHubState;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x2

    .line 152
    invoke-static {p1, v0, v1, v2, v1}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 77
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$1;->accept(Ljava/lang/Boolean;)V

    return-void
.end method
