.class final Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$3;
.super Lkotlin/jvm/internal/Lambda;
.source "RealOrderHubWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->render(Lcom/squareup/ui/orderhub/OrderHubWorkflowInput;Lcom/squareup/ui/orderhub/OrderHubState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/ordermanagerdata/ResultState<",
        "+",
        "Ljava/util/List<",
        "+",
        "Lcom/squareup/orders/model/Order;",
        ">;>;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/ui/orderhub/OrderHubState;",
        "+",
        "Lcom/squareup/ui/orderhub/OrderHubResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0012\u0010\u0004\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00070\u00060\u0005H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/ui/orderhub/OrderHubState;",
        "Lcom/squareup/ui/orderhub/OrderHubResult;",
        "it",
        "Lcom/squareup/ordermanagerdata/ResultState;",
        "",
        "Lcom/squareup/orders/model/Order;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/ui/orderhub/OrderHubState;

.field final synthetic this$0:Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;Lcom/squareup/ui/orderhub/OrderHubState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$3;->this$0:Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$3;->$state:Lcom/squareup/ui/orderhub/OrderHubState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/ordermanagerdata/ResultState;)Lcom/squareup/workflow/WorkflowAction;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ordermanagerdata/ResultState<",
            "+",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order;",
            ">;>;)",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/orderhub/OrderHubState;",
            "Lcom/squareup/ui/orderhub/OrderHubResult;",
            ">;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    const-string v2, "it"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 206
    instance-of v2, v1, Lcom/squareup/ordermanagerdata/ResultState$Success;

    const/4 v3, 0x2

    const/4 v4, 0x0

    if-eqz v2, :cond_0

    sget-object v2, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    iget-object v5, v0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$3;->this$0:Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;

    iget-object v6, v0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$3;->$state:Lcom/squareup/ui/orderhub/OrderHubState;

    check-cast v1, Lcom/squareup/ordermanagerdata/ResultState$Success;

    invoke-virtual {v1}, Lcom/squareup/ordermanagerdata/ResultState$Success;->getResponse()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-static {v5, v6, v1}, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;->access$getStateWhenOrdersUpdated(Lcom/squareup/ui/orderhub/RealOrderHubWorkflow;Lcom/squareup/ui/orderhub/OrderHubState;Ljava/util/List;)Lcom/squareup/ui/orderhub/OrderHubState;

    move-result-object v1

    invoke-static {v2, v1, v4, v3, v4}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    goto :goto_0

    .line 207
    :cond_0
    instance-of v2, v1, Lcom/squareup/ordermanagerdata/ResultState$Failure;

    if-eqz v2, :cond_1

    sget-object v2, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 208
    iget-object v5, v0, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$3;->$state:Lcom/squareup/ui/orderhub/OrderHubState;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v5}, Lcom/squareup/ui/orderhub/OrderHubState;->getDetailState()Lcom/squareup/ui/orderhub/DetailState;

    move-result-object v10

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-object v15, v1

    check-cast v15, Lcom/squareup/ordermanagerdata/ResultState$Failure;

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x6f

    const/16 v19, 0x0

    invoke-static/range {v10 .. v19}, Lcom/squareup/ui/orderhub/DetailState;->copy$default(Lcom/squareup/ui/orderhub/DetailState;Lcom/squareup/ui/orderhub/master/Filter;Ljava/util/List;ZZLcom/squareup/ordermanagerdata/ResultState$Failure;Lorg/threeten/bp/ZonedDateTime;Ljava/util/Map;ILjava/lang/Object;)Lcom/squareup/ui/orderhub/DetailState;

    move-result-object v10

    const/4 v12, 0x0

    const/16 v13, 0x6f

    const/4 v14, 0x0

    invoke-static/range {v5 .. v14}, Lcom/squareup/ui/orderhub/OrderHubState;->copy$default(Lcom/squareup/ui/orderhub/OrderHubState;ZZLjava/util/List;Lcom/squareup/ui/orderhub/MasterState;Lcom/squareup/ui/orderhub/DetailState;Lcom/squareup/ui/orderhub/OrderWorkflowAction;ZILjava/lang/Object;)Lcom/squareup/ui/orderhub/OrderHubState;

    move-result-object v1

    .line 207
    invoke-static {v2, v1, v4, v3, v4}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_1
    new-instance v1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 77
    check-cast p1, Lcom/squareup/ordermanagerdata/ResultState;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/RealOrderHubWorkflow$render$3;->invoke(Lcom/squareup/ordermanagerdata/ResultState;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
