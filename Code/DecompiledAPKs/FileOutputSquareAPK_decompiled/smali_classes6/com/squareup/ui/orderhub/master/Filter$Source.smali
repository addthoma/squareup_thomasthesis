.class public final Lcom/squareup/ui/orderhub/master/Filter$Source;
.super Lcom/squareup/ui/orderhub/master/Filter;
.source "Filter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/orderhub/master/Filter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Source"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nFilter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Filter.kt\ncom/squareup/ui/orderhub/master/Filter$Source\n*L\n1#1,283:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u000f\u0012\u0008\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0004J\u000b\u0010\u0007\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u0015\u0010\u0008\u001a\u00020\u00002\n\u0008\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003H\u00c6\u0001J\u0013\u0010\t\u001a\u00020\n2\u0008\u0010\u000b\u001a\u0004\u0018\u00010\u000cH\u00d6\u0003J\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\u0018\u0010\u0011\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0012\u001a\u00020\u0013H\u0016J\u0010\u0010\u0014\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\t\u0010\u0015\u001a\u00020\u0016H\u00d6\u0001J\t\u0010\u0017\u001a\u00020\u000eH\u00d6\u0001R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/master/Filter$Source;",
        "Lcom/squareup/ui/orderhub/master/Filter;",
        "channel",
        "Lcom/squareup/protos/client/orders/Channel;",
        "(Lcom/squareup/protos/client/orders/Channel;)V",
        "getChannel",
        "()Lcom/squareup/protos/client/orders/Channel;",
        "component1",
        "copy",
        "equals",
        "",
        "other",
        "",
        "getFilterName",
        "",
        "res",
        "Lcom/squareup/util/Res;",
        "getFilterNameShortLowerCased",
        "locale",
        "Ljava/util/Locale;",
        "getGroupName",
        "hashCode",
        "",
        "toString",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final channel:Lcom/squareup/protos/client/orders/Channel;


# direct methods
.method public constructor <init>(Lcom/squareup/protos/client/orders/Channel;)V
    .locals 1

    const/4 v0, 0x0

    .line 67
    invoke-direct {p0, v0}, Lcom/squareup/ui/orderhub/master/Filter;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/master/Filter$Source;->channel:Lcom/squareup/protos/client/orders/Channel;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/orderhub/master/Filter$Source;Lcom/squareup/protos/client/orders/Channel;ILjava/lang/Object;)Lcom/squareup/ui/orderhub/master/Filter$Source;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/orderhub/master/Filter$Source;->channel:Lcom/squareup/protos/client/orders/Channel;

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/master/Filter$Source;->copy(Lcom/squareup/protos/client/orders/Channel;)Lcom/squareup/ui/orderhub/master/Filter$Source;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/protos/client/orders/Channel;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/orderhub/master/Filter$Source;->channel:Lcom/squareup/protos/client/orders/Channel;

    return-object v0
.end method

.method public final copy(Lcom/squareup/protos/client/orders/Channel;)Lcom/squareup/ui/orderhub/master/Filter$Source;
    .locals 1

    new-instance v0, Lcom/squareup/ui/orderhub/master/Filter$Source;

    invoke-direct {v0, p1}, Lcom/squareup/ui/orderhub/master/Filter$Source;-><init>(Lcom/squareup/protos/client/orders/Channel;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/orderhub/master/Filter$Source;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/orderhub/master/Filter$Source;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/master/Filter$Source;->channel:Lcom/squareup/protos/client/orders/Channel;

    iget-object p1, p1, Lcom/squareup/ui/orderhub/master/Filter$Source;->channel:Lcom/squareup/protos/client/orders/Channel;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getChannel()Lcom/squareup/protos/client/orders/Channel;
    .locals 1

    .line 67
    iget-object v0, p0, Lcom/squareup/ui/orderhub/master/Filter$Source;->channel:Lcom/squareup/protos/client/orders/Channel;

    return-object v0
.end method

.method public getFilterName(Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 1

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    iget-object p1, p0, Lcom/squareup/ui/orderhub/master/Filter$Source;->channel:Lcom/squareup/protos/client/orders/Channel;

    if-eqz p1, :cond_0

    iget-object p1, p1, Lcom/squareup/protos/client/orders/Channel;->display_name:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    const-string p1, ""

    :goto_1
    return-object p1
.end method

.method public getFilterNameShortLowerCased(Lcom/squareup/util/Res;Ljava/util/Locale;)Ljava/lang/String;
    .locals 1

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "locale"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 86
    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/master/Filter$Source;->getFilterName(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getGroupName(Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 1

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 69
    sget v0, Lcom/squareup/orderhub/applet/R$string;->orderhub_order_filter_header_source_uppercase:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/orderhub/master/Filter$Source;->channel:Lcom/squareup/protos/client/orders/Channel;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Source(channel="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/master/Filter$Source;->channel:Lcom/squareup/protos/client/orders/Channel;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
