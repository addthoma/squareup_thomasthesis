.class final Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$createRecycler$$inlined$adopt$lambda$1;
.super Lkotlin/jvm/internal/Lambda;
.source "OrderHubAdjustPickupTimeCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function3;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;->createRecycler(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function3<",
        "Ljava/lang/Integer;",
        "Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$TimeRow;",
        "Lcom/squareup/noho/NohoCheckableRow;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOrderHubAdjustPickupTimeCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OrderHubAdjustPickupTimeCoordinator.kt\ncom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$createRecycler$1$1\n+ 2 Views.kt\ncom/squareup/util/Views\n*L\n1#1,151:1\n1103#2,7:152\n*E\n*S KotlinDebug\n*F\n+ 1 OrderHubAdjustPickupTimeCoordinator.kt\ncom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$createRecycler$1$1\n*L\n89#1,7:152\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\n\u00a2\u0006\u0002\u0008\u0008\u00a8\u0006\t"
    }
    d2 = {
        "<anonymous>",
        "",
        "<anonymous parameter 0>",
        "",
        "timeRow",
        "Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$TimeRow;",
        "row",
        "Lcom/squareup/noho/NohoCheckableRow;",
        "invoke",
        "com/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$createRecycler$1$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$createRecycler$$inlined$adopt$lambda$1;->this$0:Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;

    const/4 p1, 0x3

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 32
    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    check-cast p2, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$TimeRow;

    check-cast p3, Lcom/squareup/noho/NohoCheckableRow;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$createRecycler$$inlined$adopt$lambda$1;->invoke(ILcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$TimeRow;Lcom/squareup/noho/NohoCheckableRow;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(ILcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$TimeRow;Lcom/squareup/noho/NohoCheckableRow;)V
    .locals 3

    const-string p1, "timeRow"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "row"

    invoke-static {p3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 85
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$createRecycler$$inlined$adopt$lambda$1;->this$0:Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;->access$getDateAndTimeFormatter$p(Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;)Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;

    move-result-object p1

    invoke-virtual {p2}, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$TimeRow;->getTime()Lorg/threeten/bp/ZonedDateTime;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;->formatDateAndTime(Lorg/threeten/bp/ZonedDateTime;Z)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {p3, p1}, Lcom/squareup/noho/NohoCheckableRow;->setLabel(Ljava/lang/CharSequence;)V

    .line 87
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$createRecycler$$inlined$adopt$lambda$1;->this$0:Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;->access$getRelativeDateAndTimeFormatter$p(Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;)Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;

    move-result-object p1

    invoke-virtual {p2}, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$TimeRow;->getTime()Lorg/threeten/bp/ZonedDateTime;

    move-result-object v0

    iget-object v2, p0, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$createRecycler$$inlined$adopt$lambda$1;->this$0:Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;

    invoke-static {v2}, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;->access$getNow$p(Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Lcom/squareup/ui/orderhub/util/text/RelativeDateAndTimeFormatter;->getHoursAndMinutesLabel$orderhub_applet_release(Lorg/threeten/bp/ZonedDateTime;Lorg/threeten/bp/ZonedDateTime;)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {p3, p1}, Lcom/squareup/noho/NohoCheckableRow;->setValue(Ljava/lang/CharSequence;)V

    .line 88
    invoke-virtual {p2}, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$TimeRow;->getTime()Lorg/threeten/bp/ZonedDateTime;

    move-result-object p1

    invoke-virtual {p1}, Lorg/threeten/bp/ZonedDateTime;->getMinute()I

    move-result p1

    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$createRecycler$$inlined$adopt$lambda$1;->this$0:Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;->access$getSelectedTime$p(Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator;)Lorg/threeten/bp/ZonedDateTime;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/threeten/bp/ZonedDateTime;->getMinute()I

    move-result v0

    if-ne p1, v0, :cond_0

    const/4 v1, 0x1

    :cond_0
    invoke-virtual {p3, v1}, Lcom/squareup/noho/NohoCheckableRow;->setChecked(Z)V

    .line 89
    check-cast p3, Landroid/view/View;

    .line 152
    new-instance p1, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$createRecycler$$inlined$adopt$lambda$1$1;

    invoke-direct {p1, p0, p2}, Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$createRecycler$$inlined$adopt$lambda$1$1;-><init>(Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$createRecycler$$inlined$adopt$lambda$1;Lcom/squareup/ui/orderhub/order/adjusttime/OrderHubAdjustPickupTimeCoordinator$TimeRow;)V

    check-cast p1, Landroid/view/View$OnClickListener;

    invoke-virtual {p3, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
