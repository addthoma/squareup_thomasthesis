.class public final Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderEventWithId;
.super Lcom/squareup/analytics/event/v1/ActionEvent;
.source "OrderHubAnalytics.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "OrderEventWithId"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008\u0000\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderEventWithId;",
        "Lcom/squareup/analytics/event/v1/ActionEvent;",
        "order_id",
        "",
        "actionName",
        "Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;",
        "(Ljava/lang/String;Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;)V",
        "getOrder_id",
        "()Ljava/lang/String;",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final order_id:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderHubActionName;)V
    .locals 1

    const-string v0, "order_id"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "actionName"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 134
    check-cast p2, Lcom/squareup/analytics/EventNamedAction;

    invoke-direct {p0, p2}, Lcom/squareup/analytics/event/v1/ActionEvent;-><init>(Lcom/squareup/analytics/EventNamedAction;)V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderEventWithId;->order_id:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final getOrder_id()Ljava/lang/String;
    .locals 1

    .line 132
    iget-object v0, p0, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics$OrderEventWithId;->order_id:Ljava/lang/String;

    return-object v0
.end method
