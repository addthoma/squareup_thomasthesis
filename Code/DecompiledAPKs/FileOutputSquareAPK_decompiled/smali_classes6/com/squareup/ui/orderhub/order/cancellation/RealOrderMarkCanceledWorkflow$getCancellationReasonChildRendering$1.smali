.class final Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow$getCancellationReasonChildRendering$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealOrderMarkCanceledWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow;->getCancellationReasonChildRendering(Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonResult;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;",
        "+",
        "Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledResult;",
        ">;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealOrderMarkCanceledWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealOrderMarkCanceledWorkflow.kt\ncom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow$getCancellationReasonChildRendering$1\n*L\n1#1,390:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;",
        "Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledResult;",
        "result",
        "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonResult;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;


# direct methods
.method constructor <init>(Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow$getCancellationReasonChildRendering$1;->$state:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonResult;)Lcom/squareup/workflow/WorkflowAction;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonResult;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;",
            "Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledResult;",
            ">;"
        }
    .end annotation

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 329
    instance-of v0, p1, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonResult$CancellationReasonComplete;

    const/4 v1, 0x2

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    .line 330
    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 331
    iget-object v3, p0, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow$getCancellationReasonChildRendering$1;->$state:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 332
    check-cast p1, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonResult$CancellationReasonComplete;

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonResult$CancellationReasonComplete;->getCancellationReason()Lcom/squareup/ordermanagerdata/CancellationReason;

    move-result-object v8

    const/4 v7, 0x0

    .line 333
    sget-object v6, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;->MARK_CANCELED:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/16 v12, 0xeb

    const/4 v13, 0x0

    .line 331
    invoke-static/range {v3 .. v13}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->copy$default(Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;Lcom/squareup/orders/model/Order;Lcom/squareup/protos/client/orders/Action;Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;Ljava/util/Map;Lcom/squareup/ordermanagerdata/CancellationReason;Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/billhistory/model/BillHistory;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;

    move-result-object p1

    .line 330
    invoke-static {v0, p1, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 336
    :cond_0
    instance-of p1, p1, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonResult$GoBackFromCancellationSelectReason;

    if-eqz p1, :cond_3

    .line 337
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow$getCancellationReasonChildRendering$1;->$state:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->getCancelAction()Lcom/squareup/protos/client/orders/Action;

    move-result-object p1

    if-eqz p1, :cond_2

    iget-object p1, p1, Lcom/squareup/protos/client/orders/Action;->can_apply_to_line_items:Ljava/lang/Boolean;

    const-string v0, "requireNotNull(state.can\u2026).can_apply_to_line_items"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 338
    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 339
    iget-object v3, p0, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow$getCancellationReasonChildRendering$1;->$state:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 340
    sget-object v6, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;->ITEM_SELECTION:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/16 v12, 0xfb

    const/4 v13, 0x0

    .line 339
    invoke-static/range {v3 .. v13}, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;->copy$default(Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;Lcom/squareup/orders/model/Order;Lcom/squareup/protos/client/orders/Action;Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState$Companion$CancelAction;Ljava/util/Map;Lcom/squareup/ordermanagerdata/CancellationReason;Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/billhistory/model/BillHistory;Ljava/lang/String;ILjava/lang/Object;)Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledState;

    move-result-object v0

    .line 338
    invoke-static {p1, v0, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 344
    :cond_1
    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    sget-object v0, Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledResult$GoBackFromMarkCanceled;->INSTANCE:Lcom/squareup/ui/orderhub/order/cancellation/OrderMarkCanceledResult$GoBackFromMarkCanceled;

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1

    .line 337
    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Required value was null."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 83
    check-cast p1, Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonResult;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/order/cancellation/RealOrderMarkCanceledWorkflow$getCancellationReasonChildRendering$1;->invoke(Lcom/squareup/ui/orderhub/order/cancellation/cancellationreason/OrderCancellationReasonResult;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
