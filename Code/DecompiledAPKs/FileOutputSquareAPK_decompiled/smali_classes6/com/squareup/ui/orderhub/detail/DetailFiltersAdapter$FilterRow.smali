.class final Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$FilterRow;
.super Ljava/lang/Object;
.source "DetailFiltersAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "FilterRow"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\t\u0008\u0002\u0018\u00002\u00020\u0001B\'\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0011\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0008\u0010\u000fR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$FilterRow;",
        "",
        "filter",
        "Lcom/squareup/ui/orderhub/master/Filter;",
        "header",
        "",
        "orderCount",
        "",
        "isSelected",
        "",
        "(Lcom/squareup/ui/orderhub/master/Filter;Ljava/lang/String;IZ)V",
        "getFilter",
        "()Lcom/squareup/ui/orderhub/master/Filter;",
        "getHeader",
        "()Ljava/lang/String;",
        "()Z",
        "getOrderCount",
        "()I",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final filter:Lcom/squareup/ui/orderhub/master/Filter;

.field private final header:Ljava/lang/String;

.field private final isSelected:Z

.field private final orderCount:I


# direct methods
.method public constructor <init>(Lcom/squareup/ui/orderhub/master/Filter;Ljava/lang/String;IZ)V
    .locals 1

    const-string v0, "filter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$FilterRow;->filter:Lcom/squareup/ui/orderhub/master/Filter;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$FilterRow;->header:Ljava/lang/String;

    iput p3, p0, Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$FilterRow;->orderCount:I

    iput-boolean p4, p0, Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$FilterRow;->isSelected:Z

    return-void
.end method


# virtual methods
.method public final getFilter()Lcom/squareup/ui/orderhub/master/Filter;
    .locals 1

    .line 120
    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$FilterRow;->filter:Lcom/squareup/ui/orderhub/master/Filter;

    return-object v0
.end method

.method public final getHeader()Ljava/lang/String;
    .locals 1

    .line 121
    iget-object v0, p0, Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$FilterRow;->header:Ljava/lang/String;

    return-object v0
.end method

.method public final getOrderCount()I
    .locals 1

    .line 122
    iget v0, p0, Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$FilterRow;->orderCount:I

    return v0
.end method

.method public final isSelected()Z
    .locals 1

    .line 123
    iget-boolean v0, p0, Lcom/squareup/ui/orderhub/detail/DetailFiltersAdapter$FilterRow;->isSelected:Z

    return v0
.end method
