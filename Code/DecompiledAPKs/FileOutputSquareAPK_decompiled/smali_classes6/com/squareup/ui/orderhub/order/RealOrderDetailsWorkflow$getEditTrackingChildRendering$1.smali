.class final Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$getEditTrackingChildRendering$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealOrderDetailsWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;->getEditTrackingChildRendering(Lcom/squareup/ui/orderhub/order/OrderDetailsState$EditingTrackingState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingResult;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsState;",
        "+",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsState;",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsResult;",
        "result",
        "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingResult;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/ui/orderhub/order/OrderDetailsState$EditingTrackingState;


# direct methods
.method constructor <init>(Lcom/squareup/ui/orderhub/order/OrderDetailsState$EditingTrackingState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$getEditTrackingChildRendering$1;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState$EditingTrackingState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingResult;)Lcom/squareup/workflow/WorkflowAction;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingResult;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/orderhub/order/OrderDetailsState;",
            "Lcom/squareup/ui/orderhub/order/OrderDetailsResult;",
            ">;"
        }
    .end annotation

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 704
    instance-of v0, p1, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingResult$EditTrackingComplete;

    const/4 v1, 0x2

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    .line 705
    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 706
    iget-object v3, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$getEditTrackingChildRendering$1;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState$EditingTrackingState;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    .line 708
    check-cast p1, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingResult$EditTrackingComplete;

    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingResult$EditTrackingComplete;->getTrackingInfo()Lcom/squareup/ordermanagerdata/TrackingInfo;

    move-result-object v11

    const/16 v12, 0x77

    const/4 v13, 0x0

    .line 706
    invoke-static/range {v3 .. v13}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$EditingTrackingState;->copy$default(Lcom/squareup/ui/orderhub/order/OrderDetailsState$EditingTrackingState;ZZLcom/squareup/orders/model/Order;ZLcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/orders/model/Order$Fulfillment;ZLcom/squareup/ordermanagerdata/TrackingInfo;ILjava/lang/Object;)Lcom/squareup/ui/orderhub/order/OrderDetailsState$EditingTrackingState;

    move-result-object p1

    .line 705
    invoke-static {v0, p1, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 712
    :cond_0
    instance-of p1, p1, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingResult$GoBackFromEditTracking;

    if-eqz p1, :cond_1

    .line 713
    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 714
    new-instance v0, Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;

    .line 715
    iget-object v3, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$getEditTrackingChildRendering$1;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState$EditingTrackingState;

    invoke-virtual {v3}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$EditingTrackingState;->isReadOnly()Z

    move-result v4

    .line 716
    iget-object v3, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$getEditTrackingChildRendering$1;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState$EditingTrackingState;

    invoke-virtual {v3}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$EditingTrackingState;->getShowOrderIdInActionBar()Z

    move-result v5

    .line 717
    iget-object v3, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$getEditTrackingChildRendering$1;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState$EditingTrackingState;

    invoke-virtual {v3}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$EditingTrackingState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/16 v11, 0x70

    const/4 v12, 0x0

    move-object v3, v0

    .line 714
    invoke-direct/range {v3 .. v12}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;-><init>(ZZLcom/squareup/orders/model/Order;ZLcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/protos/client/orders/Action;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 713
    invoke-static {p1, v0, v2, v1, v2}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1

    :cond_1
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 112
    check-cast p1, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingResult;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$getEditTrackingChildRendering$1;->invoke(Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingResult;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
