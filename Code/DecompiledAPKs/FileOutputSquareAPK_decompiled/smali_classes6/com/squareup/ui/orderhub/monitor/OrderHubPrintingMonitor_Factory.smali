.class public final Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor_Factory;
.super Ljava/lang/Object;
.source "OrderHubPrintingMonitor_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;",
        ">;"
    }
.end annotation


# instance fields
.field private final accountStatusSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final currentTimeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/time/CurrentTime;",
            ">;"
        }
    .end annotation
.end field

.field private final dateAndTimeFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final orderHubAnalyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;",
            ">;"
        }
    .end annotation
.end field

.field private final orderPrintingDispatcherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/OrderPrintingDispatcher;",
            ">;"
        }
    .end annotation
.end field

.field private final orderPrintingEnabledPreferenceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field private final orderRepositoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ordermanagerdata/OrderRepository;",
            ">;"
        }
    .end annotation
.end field

.field private final ordersPrintedBeforePreferenceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ordermanagerdata/OrderRepository;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/OrderPrintingDispatcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/time/CurrentTime;",
            ">;)V"
        }
    .end annotation

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object p1, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor_Factory;->orderRepositoryProvider:Ljavax/inject/Provider;

    .line 55
    iput-object p2, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor_Factory;->orderPrintingDispatcherProvider:Ljavax/inject/Provider;

    .line 56
    iput-object p3, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor_Factory;->orderPrintingEnabledPreferenceProvider:Ljavax/inject/Provider;

    .line 57
    iput-object p4, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 58
    iput-object p5, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor_Factory;->resProvider:Ljavax/inject/Provider;

    .line 59
    iput-object p6, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor_Factory;->ordersPrintedBeforePreferenceProvider:Ljavax/inject/Provider;

    .line 60
    iput-object p7, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor_Factory;->orderHubAnalyticsProvider:Ljavax/inject/Provider;

    .line 61
    iput-object p8, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor_Factory;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    .line 62
    iput-object p9, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor_Factory;->dateAndTimeFormatterProvider:Ljavax/inject/Provider;

    .line 63
    iput-object p10, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor_Factory;->currentTimeProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor_Factory;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ordermanagerdata/OrderRepository;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/OrderPrintingDispatcher;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/time/CurrentTime;",
            ">;)",
            "Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor_Factory;"
        }
    .end annotation

    .line 81
    new-instance v11, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor_Factory;

    move-object v0, v11

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v11
.end method

.method public static newInstance(Lcom/squareup/ordermanagerdata/OrderRepository;Lcom/squareup/print/OrderPrintingDispatcher;Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/settings/server/Features;Lcom/squareup/util/Res;Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;Lcom/squareup/time/CurrentTime;)Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ordermanagerdata/OrderRepository;",
            "Lcom/squareup/print/OrderPrintingDispatcher;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/util/Res;",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;",
            "Lcom/squareup/time/CurrentTime;",
            ")",
            "Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;"
        }
    .end annotation

    .line 90
    new-instance v11, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;

    move-object v0, v11

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;-><init>(Lcom/squareup/ordermanagerdata/OrderRepository;Lcom/squareup/print/OrderPrintingDispatcher;Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/settings/server/Features;Lcom/squareup/util/Res;Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;Lcom/squareup/time/CurrentTime;)V

    return-object v11
.end method


# virtual methods
.method public get()Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;
    .locals 11

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor_Factory;->orderRepositoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/ordermanagerdata/OrderRepository;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor_Factory;->orderPrintingDispatcherProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/print/OrderPrintingDispatcher;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor_Factory;->orderPrintingEnabledPreferenceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/f2prateek/rx/preferences2/Preference;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor_Factory;->ordersPrintedBeforePreferenceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/f2prateek/rx/preferences2/Preference;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor_Factory;->orderHubAnalyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor_Factory;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor_Factory;->dateAndTimeFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;

    iget-object v0, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor_Factory;->currentTimeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/time/CurrentTime;

    invoke-static/range {v1 .. v10}, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor_Factory;->newInstance(Lcom/squareup/ordermanagerdata/OrderRepository;Lcom/squareup/print/OrderPrintingDispatcher;Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/settings/server/Features;Lcom/squareup/util/Res;Lcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/ui/orderhub/analytics/OrderHubAnalytics;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/orderhub/util/text/DateAndTimeFormatter;Lcom/squareup/time/CurrentTime;)Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 16
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor_Factory;->get()Lcom/squareup/ui/orderhub/monitor/OrderHubPrintingMonitor;

    move-result-object v0

    return-object v0
.end method
