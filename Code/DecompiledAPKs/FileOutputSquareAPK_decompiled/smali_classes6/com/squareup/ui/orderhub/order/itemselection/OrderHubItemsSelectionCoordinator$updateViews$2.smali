.class final Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator$updateViews$2;
.super Lkotlin/jvm/internal/Lambda;
.source "OrderHubItemSelectionCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator;->updateViews(Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionScreen;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/cycler/Update<",
        "Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u0008\u0012\u0004\u0012\u00020\u00030\u0002H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/cycler/Update;",
        "Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $screenData:Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;

.field final synthetic this$0:Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator;Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator$updateViews$2;->this$0:Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator$updateViews$2;->$screenData:Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 49
    check-cast p1, Lcom/squareup/cycler/Update;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator$updateViews$2;->invoke(Lcom/squareup/cycler/Update;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/cycler/Update;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/Update<",
            "Lcom/squareup/ui/orderhub/order/itemselection/OrderLineItemSelection;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 103
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator$updateViews$2;->this$0:Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator;

    .line 104
    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator$updateViews$2;->$screenData:Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;->getAction()Lcom/squareup/protos/client/orders/Action;

    move-result-object v1

    iget-object v1, v1, Lcom/squareup/protos/client/orders/Action;->type:Lcom/squareup/protos/client/orders/Action$Type;

    const-string v2, "screenData.action.type"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 105
    iget-object v2, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator$updateViews$2;->$screenData:Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;

    invoke-virtual {v2}, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;->getSelectedLineItems()Ljava/util/Map;

    move-result-object v2

    .line 106
    iget-object v3, p0, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator$updateViews$2;->$screenData:Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;

    invoke-virtual {v3}, Lcom/squareup/ui/orderhub/order/itemselection/OrderItemSelectionState;->getAvailableLineItems()Ljava/util/Map;

    move-result-object v3

    .line 103
    invoke-static {v0, v1, v2, v3}, Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator;->access$lineItemSelection(Lcom/squareup/ui/orderhub/order/itemselection/OrderHubItemsSelectionCoordinator;Lcom/squareup/protos/client/orders/Action$Type;Ljava/util/Map;Ljava/util/Map;)Lcom/squareup/cycler/DataSource;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/cycler/Update;->setData(Lcom/squareup/cycler/DataSource;)V

    return-void
.end method
