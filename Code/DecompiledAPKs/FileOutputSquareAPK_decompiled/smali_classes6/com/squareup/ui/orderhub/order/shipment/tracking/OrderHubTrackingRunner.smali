.class public final Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;
.super Ljava/lang/Object;
.source "OrderHubTrackingRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/LayoutRunner;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Factory;,
        Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$CarrierRowType;,
        Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Carrier;,
        Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/LayoutRunner<",
        "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOrderHubTrackingRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OrderHubTrackingRunner.kt\ncom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 3 RecyclerFactory.kt\ncom/squareup/recycler/RecyclerFactory\n+ 4 Recycler.kt\ncom/squareup/cycler/Recycler$Companion\n+ 5 Recycler.kt\ncom/squareup/cycler/Recycler$Config\n+ 6 StandardRowSpec.kt\ncom/squareup/cycler/StandardRowSpec\n+ 7 RecyclerNoho.kt\ncom/squareup/noho/dsl/RecyclerNohoKt\n+ 8 RecyclerEdges.kt\ncom/squareup/noho/dsl/RecyclerEdgesKt\n*L\n1#1,328:1\n1370#2:329\n1401#2,4:330\n49#3:334\n50#3,3:340\n53#3:374\n599#4,4:335\n601#4:339\n310#5,3:343\n313#5,3:352\n328#5:360\n342#5,5:361\n344#5,4:366\n329#5:370\n35#6,6:346\n114#7,5:355\n120#7:371\n43#8,2:372\n*E\n*S KotlinDebug\n*F\n+ 1 OrderHubTrackingRunner.kt\ncom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner\n*L\n290#1:329\n290#1,4:330\n72#1:334\n72#1,3:340\n72#1:374\n72#1,4:335\n72#1:339\n72#1,3:343\n72#1,3:352\n72#1:360\n72#1,5:361\n72#1,4:366\n72#1:370\n72#1,6:346\n72#1,5:355\n72#1:371\n72#1,2:372\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000x\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0006\u0018\u0000 72\u0008\u0012\u0004\u0012\u00020\u00020\u0001:\u00045678B\u001d\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0002\u0010\tJ<\u0010\u001a\u001a\u00020\u001b2\u0008\u0010\u0017\u001a\u0004\u0018\u00010\u001c2\u0008\u0010\u001d\u001a\u0004\u0018\u00010\u001c2\u0006\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020\u001f2\u0006\u0010!\u001a\u00020\u001f2\u0006\u0010\"\u001a\u00020\u0002H\u0002J\u0018\u0010#\u001a\u00020\u001b2\u0006\u0010$\u001a\u00020\u001f2\u0006\u0010%\u001a\u00020&H\u0002J \u0010\'\u001a\u00020\u001b2\u0006\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020\u001f2\u0006\u0010\"\u001a\u00020\u0002H\u0002J\"\u0010(\u001a\u00020\u001b2\u0006\u0010$\u001a\u00020\u001f2\u0008\u0010)\u001a\u0004\u0018\u00010\u001c2\u0006\u0010\"\u001a\u00020\u0002H\u0002J\u0010\u0010*\u001a\u00020\u001b2\u0006\u0010+\u001a\u00020&H\u0002J\u0012\u0010,\u001a\u00020-2\u0008\u0010\u001d\u001a\u0004\u0018\u00010\u001cH\u0002J \u0010.\u001a\u0008\u0012\u0004\u0012\u00020\u00100/2\u0008\u00100\u001a\u0004\u0018\u00010\u001c2\u0006\u0010\"\u001a\u00020\u0002H\u0002J\u0018\u00101\u001a\u00020\u001b2\u0006\u0010\"\u001a\u00020\u00022\u0006\u00102\u001a\u000203H\u0016J\u000e\u00104\u001a\u00020\u001f*\u0004\u0018\u00010\u001cH\u0002R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0015\u0010\u0016R\u000e\u0010\u0017\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0018\u0010\u0019\u00a8\u00069"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen;",
        "view",
        "Landroid/view/View;",
        "res",
        "Lcom/squareup/util/Res;",
        "recyclerFactory",
        "Lcom/squareup/recycler/RecyclerFactory;",
        "(Landroid/view/View;Lcom/squareup/util/Res;Lcom/squareup/recycler/RecyclerFactory;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "animator",
        "Lcom/squareup/widgets/SquareViewAnimator;",
        "carriersRecycler",
        "Lcom/squareup/cycler/Recycler;",
        "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$CarrierRowType;",
        "deleteTrackingNumber",
        "Lcom/squareup/noho/NohoButton;",
        "otherCarrier",
        "Lcom/squareup/noho/NohoEditText;",
        "getRes",
        "()Lcom/squareup/util/Res;",
        "trackingNumber",
        "getView",
        "()Landroid/view/View;",
        "configureActionBar",
        "",
        "",
        "carrierName",
        "shouldShowRemoveTracking",
        "",
        "isReadOnly",
        "canGoBack",
        "rendering",
        "configureOtherCarrier",
        "showCustomCarrierOnly",
        "carrierNameEditText",
        "Lcom/squareup/workflow/text/WorkflowEditableText;",
        "configureRemoveTracking",
        "configureSelectedCarrier",
        "selectedCarrierName",
        "configureTrackingNumber",
        "trackingNumberEditText",
        "getCarrierPosition",
        "",
        "getCarriersDataSource",
        "Lcom/squareup/cycler/DataSource;",
        "selectedCarrier",
        "showRendering",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "isNameOfOtherCarrier",
        "Carrier",
        "CarrierRowType",
        "Companion",
        "Factory",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final CARRIERS_LIST:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Carrier;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Companion;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final NO_CARRIER_CHOSEN_POSITION:I = -0x1

.field public static final OTHER_CARRIER_NAME:Ljava/lang/String; = "Other"

.field public static final OTHER_CARRIER_POSITION:I = 0x4

.field private static final TITLE_ROW:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$CarrierRowType$TitleRow;",
            ">;"
        }
    .end annotation
.end field

.field private static final TITLE_ROW_DATA_SOURCE:Lcom/squareup/cycler/DataSource;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cycler/DataSource<",
            "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$CarrierRowType$TitleRow;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final actionBar:Lcom/squareup/noho/NohoActionBar;

.field private final animator:Lcom/squareup/widgets/SquareViewAnimator;

.field private final carriersRecycler:Lcom/squareup/cycler/Recycler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/cycler/Recycler<",
            "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$CarrierRowType;",
            ">;"
        }
    .end annotation
.end field

.field private final deleteTrackingNumber:Lcom/squareup/noho/NohoButton;

.field private final otherCarrier:Lcom/squareup/noho/NohoEditText;

.field private final res:Lcom/squareup/util/Res;

.field private final trackingNumber:Lcom/squareup/noho/NohoEditText;

.field private final view:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->Companion:Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Companion;

    .line 321
    sget-object v0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$CarrierRowType$TitleRow;->INSTANCE:Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$CarrierRowType$TitleRow;

    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->TITLE_ROW:Ljava/util/List;

    .line 322
    sget-object v0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->TITLE_ROW:Ljava/util/List;

    invoke-static {v0}, Lcom/squareup/cycler/DataSourceKt;->toDataSource(Ljava/util/List;)Lcom/squareup/cycler/DataSource;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->TITLE_ROW_DATA_SOURCE:Lcom/squareup/cycler/DataSource;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Carrier;

    .line 324
    sget-object v1, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Carrier$USPS;->INSTANCE:Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Carrier$USPS;

    check-cast v1, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Carrier;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Carrier$FedEx;->INSTANCE:Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Carrier$FedEx;

    check-cast v1, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Carrier;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Carrier$UPS;->INSTANCE:Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Carrier$UPS;

    check-cast v1, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Carrier;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Carrier$DHL;->INSTANCE:Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Carrier$DHL;

    check-cast v1, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Carrier;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Carrier$Other;->INSTANCE:Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Carrier$Other;

    check-cast v1, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Carrier;

    const/4 v2, 0x4

    aput-object v1, v0, v2

    .line 323
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->CARRIERS_LIST:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;Lcom/squareup/util/Res;Lcom/squareup/recycler/RecyclerFactory;)V
    .locals 3

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "recyclerFactory"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->view:Landroid/view/View;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->res:Lcom/squareup/util/Res;

    .line 64
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "view.findViewById(com.sq\u2026s.R.id.stable_action_bar)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/noho/NohoActionBar;

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 65
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/orderhub/applet/R$id;->orderhub_add_tracking_number:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "view.findViewById(R.id.o\u2026rhub_add_tracking_number)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/noho/NohoEditText;

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->trackingNumber:Lcom/squareup/noho/NohoEditText;

    .line 67
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/orderhub/applet/R$id;->orderhub_add_tracking_other_carrier:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "view.findViewById(R.id.o\u2026d_tracking_other_carrier)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/noho/NohoEditText;

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->otherCarrier:Lcom/squareup/noho/NohoEditText;

    .line 69
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/orderhub/applet/R$id;->orderhub_add_tracking_delete_button:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "view.findViewById(R.id.o\u2026d_tracking_delete_button)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/noho/NohoButton;

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->deleteTrackingNumber:Lcom/squareup/noho/NohoButton;

    .line 70
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/orderhub/applet/R$id;->orderhub_add_tracking_animator:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "view.findViewById(R.id.o\u2026ub_add_tracking_animator)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/widgets/SquareViewAnimator;

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->animator:Lcom/squareup/widgets/SquareViewAnimator;

    .line 73
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/orderhub/applet/R$id;->orderhub_add_tracking_carrier_recycler_view:I

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string p2, "view.findViewById(R.id.o\u2026ng_carrier_recycler_view)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView;

    .line 334
    sget-object p2, Lcom/squareup/cycler/Recycler;->Companion:Lcom/squareup/cycler/Recycler$Companion;

    .line 335
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    move-result-object p2

    if-eqz p2, :cond_0

    .line 336
    new-instance p2, Lcom/squareup/cycler/Recycler$Config;

    invoke-direct {p2}, Lcom/squareup/cycler/Recycler$Config;-><init>()V

    .line 340
    invoke-virtual {p3}, Lcom/squareup/recycler/RecyclerFactory;->getMainDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/squareup/cycler/Recycler$Config;->setMainDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 341
    invoke-virtual {p3}, Lcom/squareup/recycler/RecyclerFactory;->getBackgroundDispatcher()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object p3

    invoke-virtual {p2, p3}, Lcom/squareup/cycler/Recycler$Config;->setBackgroundDispatcher(Lkotlinx/coroutines/CoroutineDispatcher;)V

    .line 344
    new-instance p3, Lcom/squareup/cycler/StandardRowSpec;

    sget-object v0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$$special$$inlined$row$1;->INSTANCE:Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$$special$$inlined$row$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-direct {p3, v0}, Lcom/squareup/cycler/StandardRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 76
    sget v0, Lcom/squareup/orderhub/applet/R$layout;->orderhub_recycler_title_row:I

    .line 346
    new-instance v1, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$$special$$inlined$adopt$lambda$1;

    invoke-direct {v1, v0, p0}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$$special$$inlined$adopt$lambda$1;-><init>(ILcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;)V

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-virtual {p3, v1}, Lcom/squareup/cycler/StandardRowSpec;->create(Lkotlin/jvm/functions/Function2;)V

    .line 344
    check-cast p3, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 343
    invoke-virtual {p2, p3}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 85
    sget-object p3, Lcom/squareup/noho/CheckType$RADIO;->INSTANCE:Lcom/squareup/noho/CheckType$RADIO;

    check-cast p3, Lcom/squareup/noho/CheckType;

    .line 86
    new-instance v0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$$special$$inlined$adopt$lambda$2;

    invoke-direct {v0, p0}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$$special$$inlined$adopt$lambda$2;-><init>(Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;)V

    check-cast v0, Lkotlin/jvm/functions/Function3;

    .line 356
    new-instance v1, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$$special$$inlined$nohoRow$1;

    invoke-direct {v1, p3}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$$special$$inlined$nohoRow$1;-><init>(Lcom/squareup/noho/CheckType;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    .line 362
    new-instance p3, Lcom/squareup/cycler/BinderRowSpec;

    .line 366
    sget-object v2, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$$special$$inlined$nohoRow$2;->INSTANCE:Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$$special$$inlined$nohoRow$2;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    .line 362
    invoke-direct {p3, v2, v1}, Lcom/squareup/cycler/BinderRowSpec;-><init>(Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V

    .line 360
    invoke-virtual {p3, v0}, Lcom/squareup/cycler/BinderRowSpec;->bind(Lkotlin/jvm/functions/Function3;)V

    .line 365
    check-cast p3, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 361
    invoke-virtual {p2, p3}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 372
    new-instance p3, Lcom/squareup/noho/dsl/EdgesExtensionSpec;

    invoke-direct {p3}, Lcom/squareup/noho/dsl/EdgesExtensionSpec;-><init>()V

    const/16 v0, 0x8

    .line 108
    invoke-virtual {p3, v0}, Lcom/squareup/noho/dsl/EdgesExtensionSpec;->setDefault(I)V

    .line 109
    check-cast p3, Lcom/squareup/cycler/ExtensionSpec;

    .line 372
    invoke-virtual {p2, p3}, Lcom/squareup/cycler/Recycler$Config;->extension(Lcom/squareup/cycler/ExtensionSpec;)V

    .line 338
    invoke-virtual {p2, p1}, Lcom/squareup/cycler/Recycler$Config;->setUp(Landroidx/recyclerview/widget/RecyclerView;)Lcom/squareup/cycler/Recycler;

    move-result-object p1

    .line 111
    invoke-virtual {p1}, Lcom/squareup/cycler/Recycler;->getView()Landroidx/recyclerview/widget/RecyclerView;

    move-result-object p2

    const/4 p3, 0x0

    check-cast p3, Landroidx/recyclerview/widget/RecyclerView$ItemAnimator;

    invoke-virtual {p2, p3}, Landroidx/recyclerview/widget/RecyclerView;->setItemAnimator(Landroidx/recyclerview/widget/RecyclerView$ItemAnimator;)V

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->carriersRecycler:Lcom/squareup/cycler/Recycler;

    return-void

    .line 335
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "RecyclerView needs a layoutManager assigned."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method

.method public static final synthetic access$Companion()Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Companion;
    .locals 1

    sget-object v0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->Companion:Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Companion;

    return-object v0
.end method

.method public static final synthetic access$getCARRIERS_LIST$cp()Ljava/util/List;
    .locals 1

    .line 46
    sget-object v0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->CARRIERS_LIST:Ljava/util/List;

    return-object v0
.end method

.method public static final synthetic access$getCarriersDataSource(Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;Ljava/lang/String;Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen;)Lcom/squareup/cycler/DataSource;
    .locals 0

    .line 46
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->getCarriersDataSource(Ljava/lang/String;Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen;)Lcom/squareup/cycler/DataSource;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getOtherCarrier$p(Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;)Lcom/squareup/noho/NohoEditText;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->otherCarrier:Lcom/squareup/noho/NohoEditText;

    return-object p0
.end method

.method public static final synthetic access$getTITLE_ROW$cp()Ljava/util/List;
    .locals 1

    .line 46
    sget-object v0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->TITLE_ROW:Ljava/util/List;

    return-object v0
.end method

.method public static final synthetic access$getTITLE_ROW_DATA_SOURCE$cp()Lcom/squareup/cycler/DataSource;
    .locals 1

    .line 46
    sget-object v0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->TITLE_ROW_DATA_SOURCE:Lcom/squareup/cycler/DataSource;

    return-object v0
.end method

.method public static final synthetic access$isNameOfOtherCarrier(Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;Ljava/lang/String;)Z
    .locals 0

    .line 46
    invoke-direct {p0, p1}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->isNameOfOtherCarrier(Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method private final configureActionBar(Ljava/lang/String;Ljava/lang/String;ZZZLcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen;)V
    .locals 15

    move-object v0, p0

    .line 229
    iget-object v1, v0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoActionBar;->setVisibility(I)V

    .line 230
    move-object/from16 v1, p1

    check-cast v1, Ljava/lang/CharSequence;

    const/4 v3, 0x1

    if-eqz v1, :cond_1

    invoke-static {v1}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    :goto_1
    xor-int/2addr v1, v3

    move-object/from16 v9, p2

    .line 231
    invoke-direct {p0, v9}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->getCarrierPosition(Ljava/lang/String;)I

    move-result v4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_4

    .line 233
    iget-object v4, v0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->otherCarrier:Lcom/squareup/noho/NohoEditText;

    invoke-virtual {v4}, Lcom/squareup/noho/NohoEditText;->getText()Landroid/text/Editable;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    if-eqz v4, :cond_3

    invoke-static {v4}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    goto :goto_2

    :cond_2
    const/4 v4, 0x0

    goto :goto_3

    :cond_3
    :goto_2
    const/4 v4, 0x1

    :goto_3
    if-nez v4, :cond_5

    goto :goto_4

    :cond_4
    const/4 v5, -0x1

    if-eq v4, v5, :cond_5

    :goto_4
    const/4 v4, 0x1

    goto :goto_5

    :cond_5
    const/4 v4, 0x0

    :goto_5
    if-nez v1, :cond_6

    if-nez v4, :cond_6

    if-nez p3, :cond_6

    const/4 v5, 0x1

    goto :goto_6

    :cond_6
    const/4 v5, 0x0

    :goto_6
    if-eqz v1, :cond_7

    if-eqz v4, :cond_7

    const/4 v7, 0x1

    goto :goto_7

    :cond_7
    const/4 v7, 0x0

    .line 267
    :goto_7
    iget-object v1, v0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 240
    new-instance v4, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v4}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 241
    new-instance v6, Lcom/squareup/resources/ResourceString;

    sget v8, Lcom/squareup/orderhub/applet/R$string;->orderhub_tracking_action_bar_title:I

    invoke-direct {v6, v8}, Lcom/squareup/resources/ResourceString;-><init>(I)V

    check-cast v6, Lcom/squareup/resources/TextModel;

    invoke-virtual {v4, v6}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v4

    if-eqz p5, :cond_8

    .line 242
    sget-object v6, Lcom/squareup/noho/UpIcon;->BACK_ARROW:Lcom/squareup/noho/UpIcon;

    goto :goto_8

    :cond_8
    sget-object v6, Lcom/squareup/noho/UpIcon;->X:Lcom/squareup/noho/UpIcon;

    :goto_8
    new-instance v8, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$configureActionBar$1;

    move-object/from16 v10, p6

    invoke-direct {v8, v10}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$configureActionBar$1;-><init>(Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen;)V

    check-cast v8, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v4, v6, v8}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v11

    .line 246
    sget-object v12, Lcom/squareup/noho/NohoActionButtonStyle;->PRIMARY:Lcom/squareup/noho/NohoActionButtonStyle;

    if-eqz v5, :cond_9

    .line 247
    new-instance v4, Lcom/squareup/resources/ResourceString;

    .line 248
    sget v6, Lcom/squareup/orderhub/applet/R$string;->orderhub_tracking_action_button_skip_tracking:I

    .line 247
    invoke-direct {v4, v6}, Lcom/squareup/resources/ResourceString;-><init>(I)V

    goto :goto_9

    .line 249
    :cond_9
    new-instance v4, Lcom/squareup/resources/ResourceString;

    sget v6, Lcom/squareup/orderhub/applet/R$string;->orderhub_tracking_action_button_done:I

    invoke-direct {v4, v6}, Lcom/squareup/resources/ResourceString;-><init>(I)V

    .line 247
    :goto_9
    move-object v13, v4

    check-cast v13, Lcom/squareup/resources/TextModel;

    if-nez p4, :cond_b

    if-nez v5, :cond_a

    if-eqz v7, :cond_b

    :cond_a
    const/4 v2, 0x1

    .line 251
    :cond_b
    new-instance v14, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$configureActionBar$2;

    move-object v3, v14

    move/from16 v4, p4

    move-object/from16 v6, p6

    move-object/from16 v8, p1

    move-object/from16 v9, p2

    invoke-direct/range {v3 .. v9}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$configureActionBar$2;-><init>(ZZLcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen;ZLjava/lang/String;Ljava/lang/String;)V

    check-cast v14, Lkotlin/jvm/functions/Function0;

    .line 245
    invoke-virtual {v11, v12, v13, v2, v14}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setActionButton(Lcom/squareup/noho/NohoActionButtonStyle;Lcom/squareup/resources/TextModel;ZLkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v2

    .line 267
    invoke-virtual {v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    return-void
.end method

.method private final configureOtherCarrier(ZLcom/squareup/workflow/text/WorkflowEditableText;)V
    .locals 3

    .line 175
    invoke-virtual {p2}, Lcom/squareup/workflow/text/WorkflowEditableText;->getText()Ljava/lang/String;

    move-result-object v0

    .line 176
    invoke-direct {p0, v0}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->getCarrierPosition(Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x4

    if-eq v1, v2, :cond_1

    if-eqz p1, :cond_0

    goto :goto_0

    .line 190
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->otherCarrier:Lcom/squareup/noho/NohoEditText;

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoEditText;->setVisibility(I)V

    goto :goto_3

    .line 178
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->otherCarrier:Lcom/squareup/noho/NohoEditText;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoEditText;->setVisibility(I)V

    .line 179
    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->otherCarrier:Lcom/squareup/noho/NohoEditText;

    invoke-virtual {v1}, Lcom/squareup/noho/NohoEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    :goto_1
    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_3

    .line 180
    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->otherCarrier:Lcom/squareup/noho/NohoEditText;

    check-cast v1, Landroid/widget/EditText;

    invoke-static {v1, p2}, Lcom/squareup/workflow/text/EditTextsKt;->setWorkflowText(Landroid/widget/EditText;Lcom/squareup/workflow/text/WorkflowEditableText;)V

    .line 182
    :cond_3
    iget-object p2, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->otherCarrier:Lcom/squareup/noho/NohoEditText;

    if-eqz p1, :cond_4

    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->res:Lcom/squareup/util/Res;

    .line 183
    sget v1, Lcom/squareup/orderhub/applet/R$string;->orderhub_tracking_carrier_hint:I

    .line 182
    invoke-interface {p1, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    goto :goto_2

    .line 184
    :cond_4
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/orderhub/applet/R$string;->orderhub_tracking_other:I

    invoke-interface {p1, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    .line 182
    :goto_2
    invoke-virtual {p2, p1}, Lcom/squareup/noho/NohoEditText;->setHint(Ljava/lang/CharSequence;)V

    if-eqz v0, :cond_5

    .line 187
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->otherCarrier:Lcom/squareup/noho/NohoEditText;

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result p2

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoEditText;->setSelection(I)V

    :cond_5
    :goto_3
    return-void
.end method

.method private final configureRemoveTracking(ZZLcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen;)V
    .locals 1

    if-eqz p1, :cond_1

    .line 156
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->deleteTrackingNumber:Lcom/squareup/noho/NohoButton;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoButton;->setVisibility(I)V

    if-eqz p2, :cond_0

    .line 159
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->deleteTrackingNumber:Lcom/squareup/noho/NohoButton;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoButton;->setEnabled(Z)V

    goto :goto_0

    .line 161
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->deleteTrackingNumber:Lcom/squareup/noho/NohoButton;

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoButton;->setEnabled(Z)V

    .line 162
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->deleteTrackingNumber:Lcom/squareup/noho/NohoButton;

    new-instance p2, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$configureRemoveTracking$1;

    invoke-direct {p2, p3}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$configureRemoveTracking$1;-><init>(Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen;)V

    check-cast p2, Landroid/view/View$OnClickListener;

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 167
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->deleteTrackingNumber:Lcom/squareup/noho/NohoButton;

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoButton;->setVisibility(I)V

    :goto_0
    return-void
.end method

.method private final configureSelectedCarrier(ZLjava/lang/String;Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen;)V
    .locals 1

    if-eqz p1, :cond_0

    .line 215
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->carriersRecycler:Lcom/squareup/cycler/Recycler;

    sget-object p2, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$configureSelectedCarrier$1;->INSTANCE:Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$configureSelectedCarrier$1;

    check-cast p2, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, p2}, Lcom/squareup/cycler/Recycler;->update(Lkotlin/jvm/functions/Function1;)V

    goto :goto_0

    .line 217
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->carriersRecycler:Lcom/squareup/cycler/Recycler;

    new-instance v0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$configureSelectedCarrier$2;

    invoke-direct {v0, p0, p2, p3}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$configureSelectedCarrier$2;-><init>(Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;Ljava/lang/String;Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen;)V

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-virtual {p1, v0}, Lcom/squareup/cycler/Recycler;->update(Lkotlin/jvm/functions/Function1;)V

    :goto_0
    return-void
.end method

.method private final configureTrackingNumber(Lcom/squareup/workflow/text/WorkflowEditableText;)V
    .locals 2

    .line 195
    invoke-virtual {p1}, Lcom/squareup/workflow/text/WorkflowEditableText;->getText()Ljava/lang/String;

    move-result-object v0

    .line 196
    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->trackingNumber:Lcom/squareup/noho/NohoEditText;

    invoke-virtual {v1}, Lcom/squareup/noho/NohoEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    .line 197
    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->trackingNumber:Lcom/squareup/noho/NohoEditText;

    check-cast v1, Landroid/widget/EditText;

    invoke-static {v1, p1}, Lcom/squareup/workflow/text/EditTextsKt;->setWorkflowText(Landroid/widget/EditText;Lcom/squareup/workflow/text/WorkflowEditableText;)V

    if-eqz v0, :cond_1

    .line 201
    iget-object p1, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->trackingNumber:Lcom/squareup/noho/NohoEditText;

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoEditText;->setSelection(I)V

    :cond_1
    return-void
.end method

.method private final getCarrierPosition(Ljava/lang/String;)I
    .locals 1

    .line 275
    sget-object v0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Carrier$USPS;->INSTANCE:Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Carrier$USPS;

    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Carrier$USPS;->getCarrierName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    .line 276
    :cond_0
    sget-object v0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Carrier$FedEx;->INSTANCE:Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Carrier$FedEx;

    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Carrier$FedEx;->getCarrierName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 p1, 0x1

    goto :goto_0

    .line 277
    :cond_1
    sget-object v0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Carrier$UPS;->INSTANCE:Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Carrier$UPS;

    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Carrier$UPS;->getCarrierName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 p1, 0x2

    goto :goto_0

    .line 278
    :cond_2
    sget-object v0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Carrier$DHL;->INSTANCE:Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Carrier$DHL;

    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Carrier$DHL;->getCarrierName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 p1, 0x3

    goto :goto_0

    :cond_3
    if-nez p1, :cond_4

    const/4 p1, -0x1

    goto :goto_0

    :cond_4
    const/4 p1, 0x4

    :goto_0
    return p1
.end method

.method private final getCarriersDataSource(Ljava/lang/String;Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen;)Lcom/squareup/cycler/DataSource;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen;",
            ")",
            "Lcom/squareup/cycler/DataSource<",
            "Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$CarrierRowType;",
            ">;"
        }
    .end annotation

    .line 288
    invoke-direct {p0, p1}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->isNameOfOtherCarrier(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x4

    goto :goto_0

    .line 289
    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->getCarrierPosition(Ljava/lang/String;)I

    move-result p1

    .line 290
    :goto_0
    sget-object v0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->TITLE_ROW:Ljava/util/List;

    check-cast v0, Ljava/util/Collection;

    sget-object v1, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->CARRIERS_LIST:Ljava/util/List;

    check-cast v1, Ljava/lang/Iterable;

    .line 329
    new-instance v2, Ljava/util/ArrayList;

    const/16 v3, 0xa

    invoke-static {v1, v3}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v2, Ljava/util/Collection;

    .line 331
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    add-int/lit8 v6, v4, 0x1

    if-gez v4, :cond_1

    .line 332
    invoke-static {}, Lkotlin/collections/CollectionsKt;->throwIndexOverflow()V

    :cond_1
    check-cast v5, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Carrier;

    .line 291
    new-instance v7, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$CarrierRowType$CarrierRow;

    if-ne v4, p1, :cond_2

    const/4 v4, 0x1

    goto :goto_2

    :cond_2
    const/4 v4, 0x0

    :goto_2
    new-instance v8, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$getCarriersDataSource$$inlined$mapIndexed$lambda$1;

    invoke-direct {v8, p1, p2}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$getCarriersDataSource$$inlined$mapIndexed$lambda$1;-><init>(ILcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen;)V

    check-cast v8, Lkotlin/jvm/functions/Function1;

    invoke-direct {v7, v5, v4, v8}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$CarrierRowType$CarrierRow;-><init>(Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$Carrier;ZLkotlin/jvm/functions/Function1;)V

    .line 295
    invoke-interface {v2, v7}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move v4, v6

    goto :goto_1

    .line 333
    :cond_3
    check-cast v2, Ljava/util/List;

    check-cast v2, Ljava/lang/Iterable;

    .line 290
    invoke-static {v0, v2}, Lkotlin/collections/CollectionsKt;->plus(Ljava/util/Collection;Ljava/lang/Iterable;)Ljava/util/List;

    move-result-object p1

    .line 296
    invoke-static {p1}, Lcom/squareup/cycler/DataSourceKt;->toDataSource(Ljava/util/List;)Lcom/squareup/cycler/DataSource;

    move-result-object p1

    return-object p1
.end method

.method private final isNameOfOtherCarrier(Ljava/lang/String;)Z
    .locals 1

    .line 271
    invoke-direct {p0, p1}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->getCarrierPosition(Ljava/lang/String;)I

    move-result p1

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method


# virtual methods
.method public final getRes()Lcom/squareup/util/Res;
    .locals 1

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->res:Lcom/squareup/util/Res;

    return-object v0
.end method

.method public final getView()Landroid/view/View;
    .locals 1

    .line 47
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->view:Landroid/view/View;

    return-object v0
.end method

.method public showRendering(Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 7

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 117
    iget-object p2, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->animator:Lcom/squareup/widgets/SquareViewAnimator;

    sget v0, Lcom/squareup/orderhub/applet/R$id;->orderhub_add_tracking_scrollview:I

    invoke-virtual {p2, v0}, Lcom/squareup/widgets/SquareViewAnimator;->setDisplayedChildById(I)V

    .line 119
    iget-object p2, p0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->view:Landroid/view/View;

    new-instance v0, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$showRendering$1;

    invoke-direct {v0, p1}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner$showRendering$1;-><init>(Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p2, v0}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 124
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen;->getEditTrackingNumber()Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object p2

    .line 123
    invoke-direct {p0, p2}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->configureTrackingNumber(Lcom/squareup/workflow/text/WorkflowEditableText;)V

    .line 127
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen;->getData()Lcom/squareup/ui/orderhub/order/shipment/tracking/TrackingScreenData;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/ui/orderhub/order/shipment/tracking/TrackingScreenData;->getShowCustomCarrierOnly()Z

    move-result p2

    .line 128
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen;->getEditOtherCarrierName()Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/workflow/text/WorkflowEditableText;->getText()Ljava/lang/String;

    move-result-object v0

    .line 126
    invoke-direct {p0, p2, v0, p1}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->configureSelectedCarrier(ZLjava/lang/String;Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen;)V

    .line 132
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen;->getData()Lcom/squareup/ui/orderhub/order/shipment/tracking/TrackingScreenData;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/ui/orderhub/order/shipment/tracking/TrackingScreenData;->getShowCustomCarrierOnly()Z

    move-result p2

    .line 133
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen;->getEditOtherCarrierName()Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object v0

    .line 131
    invoke-direct {p0, p2, v0}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->configureOtherCarrier(ZLcom/squareup/workflow/text/WorkflowEditableText;)V

    .line 136
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen;->getEditTrackingNumber()Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/workflow/text/WorkflowEditableText;->getText()Ljava/lang/String;

    move-result-object v1

    .line 137
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen;->getEditOtherCarrierName()Lcom/squareup/workflow/text/WorkflowEditableText;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/workflow/text/WorkflowEditableText;->getText()Ljava/lang/String;

    move-result-object v2

    .line 138
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen;->getData()Lcom/squareup/ui/orderhub/order/shipment/tracking/TrackingScreenData;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/ui/orderhub/order/shipment/tracking/TrackingScreenData;->getShouldShowRemoveTracking()Z

    move-result v3

    .line 139
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen;->getData()Lcom/squareup/ui/orderhub/order/shipment/tracking/TrackingScreenData;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/ui/orderhub/order/shipment/tracking/TrackingScreenData;->isReadOnly()Z

    move-result v4

    .line 140
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen;->getData()Lcom/squareup/ui/orderhub/order/shipment/tracking/TrackingScreenData;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/ui/orderhub/order/shipment/tracking/TrackingScreenData;->getCanGoBack()Z

    move-result v5

    move-object v0, p0

    move-object v6, p1

    .line 135
    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->configureActionBar(Ljava/lang/String;Ljava/lang/String;ZZZLcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen;)V

    .line 144
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen;->getData()Lcom/squareup/ui/orderhub/order/shipment/tracking/TrackingScreenData;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/ui/orderhub/order/shipment/tracking/TrackingScreenData;->getShouldShowRemoveTracking()Z

    move-result p2

    .line 145
    invoke-virtual {p1}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen;->getData()Lcom/squareup/ui/orderhub/order/shipment/tracking/TrackingScreenData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/orderhub/order/shipment/tracking/TrackingScreenData;->isReadOnly()Z

    move-result v0

    .line 143
    invoke-direct {p0, p2, v0, p1}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->configureRemoveTracking(ZZLcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen;)V

    return-void
.end method

.method public bridge synthetic showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 46
    check-cast p1, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderHubTrackingRunner;->showRendering(Lcom/squareup/ui/orderhub/order/shipment/tracking/OrderEditTrackingScreen;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
