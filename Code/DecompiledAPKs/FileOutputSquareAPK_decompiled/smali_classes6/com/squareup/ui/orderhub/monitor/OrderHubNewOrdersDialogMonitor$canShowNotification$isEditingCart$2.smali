.class final Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor$canShowNotification$isEditingCart$2;
.super Ljava/lang/Object;
.source "OrderHubNewOrdersDialogMonitor.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;->canShowNotification()Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/payment/OrderEntryEvents$CartChanged;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;


# direct methods
.method constructor <init>(Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor$canShowNotification$isEditingCart$2;->this$0:Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 63
    check-cast p1, Lcom/squareup/payment/OrderEntryEvents$CartChanged;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor$canShowNotification$isEditingCart$2;->apply(Lcom/squareup/payment/OrderEntryEvents$CartChanged;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public final apply(Lcom/squareup/payment/OrderEntryEvents$CartChanged;)Z
    .locals 1

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 228
    iget-object p1, p0, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor$canShowNotification$isEditingCart$2;->this$0:Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;

    invoke-static {p1}, Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;->access$getTransaction$p(Lcom/squareup/ui/orderhub/monitor/OrderHubNewOrdersDialogMonitor;)Lcom/squareup/payment/Transaction;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/payment/Transaction;->hasItems()Z

    move-result p1

    return p1
.end method
