.class public final enum Lcom/squareup/ui/orderhub/util/proto/KnownChannel;
.super Ljava/lang/Enum;
.source "Channels.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/orderhub/util/proto/KnownChannel$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/orderhub/util/proto/KnownChannel;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nChannels.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Channels.kt\ncom/squareup/ui/orderhub/util/proto/KnownChannel\n+ 2 _Arrays.kt\nkotlin/collections/ArraysKt___ArraysKt\n*L\n1#1,65:1\n7476#2,2:66\n7736#2,4:68\n*E\n*S KotlinDebug\n*F\n+ 1 Channels.kt\ncom/squareup/ui/orderhub/util/proto/KnownChannel\n*L\n39#1,2:66\n39#1,4:68\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0004\n\u0002\u0010\u000e\n\u0002\u0008\u000f\u0008\u0080\u0001\u0018\u0000 \u00162\u0008\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u0016B\u0011\u0008\u0002\u0012\u0008\u0008\u0001\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\u00088BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\t\u0010\nj\u0002\u0008\u000bj\u0002\u0008\u000cj\u0002\u0008\rj\u0002\u0008\u000ej\u0002\u0008\u000fj\u0002\u0008\u0010j\u0002\u0008\u0011j\u0002\u0008\u0012j\u0002\u0008\u0013j\u0002\u0008\u0014j\u0002\u0008\u0015\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/util/proto/KnownChannel;",
        "",
        "iconRes",
        "",
        "(Ljava/lang/String;II)V",
        "getIconRes",
        "()I",
        "normalizedName",
        "",
        "getNormalizedName",
        "()Ljava/lang/String;",
        "CAVIAR",
        "CHECKOUTLINK",
        "DELIVEROO",
        "DOORDASH",
        "INSTAGRAM",
        "JUSTEAT",
        "OTHER",
        "POSTMATES",
        "SQUARE",
        "UBEREATS",
        "WEEBLY",
        "Companion",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/orderhub/util/proto/KnownChannel;

.field public static final enum CAVIAR:Lcom/squareup/ui/orderhub/util/proto/KnownChannel;

.field public static final enum CHECKOUTLINK:Lcom/squareup/ui/orderhub/util/proto/KnownChannel;

.field public static final Companion:Lcom/squareup/ui/orderhub/util/proto/KnownChannel$Companion;

.field public static final enum DELIVEROO:Lcom/squareup/ui/orderhub/util/proto/KnownChannel;

.field public static final enum DOORDASH:Lcom/squareup/ui/orderhub/util/proto/KnownChannel;

.field public static final enum INSTAGRAM:Lcom/squareup/ui/orderhub/util/proto/KnownChannel;

.field public static final enum JUSTEAT:Lcom/squareup/ui/orderhub/util/proto/KnownChannel;

.field public static final enum OTHER:Lcom/squareup/ui/orderhub/util/proto/KnownChannel;

.field public static final enum POSTMATES:Lcom/squareup/ui/orderhub/util/proto/KnownChannel;

.field public static final enum SQUARE:Lcom/squareup/ui/orderhub/util/proto/KnownChannel;

.field public static final enum UBEREATS:Lcom/squareup/ui/orderhub/util/proto/KnownChannel;

.field public static final enum WEEBLY:Lcom/squareup/ui/orderhub/util/proto/KnownChannel;

.field private static final lookup:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/ui/orderhub/util/proto/KnownChannel;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final iconRes:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/16 v0, 0xb

    new-array v0, v0, [Lcom/squareup/ui/orderhub/util/proto/KnownChannel;

    new-instance v1, Lcom/squareup/ui/orderhub/util/proto/KnownChannel;

    .line 21
    sget v2, Lcom/squareup/vectoricons/R$drawable;->brands_caviar_24:I

    const/4 v3, 0x0

    const-string v4, "CAVIAR"

    invoke-direct {v1, v4, v3, v2}, Lcom/squareup/ui/orderhub/util/proto/KnownChannel;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/ui/orderhub/util/proto/KnownChannel;->CAVIAR:Lcom/squareup/ui/orderhub/util/proto/KnownChannel;

    aput-object v1, v0, v3

    new-instance v1, Lcom/squareup/ui/orderhub/util/proto/KnownChannel;

    .line 22
    sget v2, Lcom/squareup/vectoricons/R$drawable;->ui_link_16:I

    const/4 v4, 0x1

    const-string v5, "CHECKOUTLINK"

    invoke-direct {v1, v5, v4, v2}, Lcom/squareup/ui/orderhub/util/proto/KnownChannel;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/ui/orderhub/util/proto/KnownChannel;->CHECKOUTLINK:Lcom/squareup/ui/orderhub/util/proto/KnownChannel;

    aput-object v1, v0, v4

    new-instance v1, Lcom/squareup/ui/orderhub/util/proto/KnownChannel;

    .line 23
    sget v2, Lcom/squareup/vectoricons/R$drawable;->brands_deliveroo_24:I

    const/4 v4, 0x2

    const-string v5, "DELIVEROO"

    invoke-direct {v1, v5, v4, v2}, Lcom/squareup/ui/orderhub/util/proto/KnownChannel;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/ui/orderhub/util/proto/KnownChannel;->DELIVEROO:Lcom/squareup/ui/orderhub/util/proto/KnownChannel;

    aput-object v1, v0, v4

    new-instance v1, Lcom/squareup/ui/orderhub/util/proto/KnownChannel;

    .line 24
    sget v2, Lcom/squareup/vectoricons/R$drawable;->brands_doordash_24:I

    const/4 v4, 0x3

    const-string v5, "DOORDASH"

    invoke-direct {v1, v5, v4, v2}, Lcom/squareup/ui/orderhub/util/proto/KnownChannel;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/ui/orderhub/util/proto/KnownChannel;->DOORDASH:Lcom/squareup/ui/orderhub/util/proto/KnownChannel;

    aput-object v1, v0, v4

    new-instance v1, Lcom/squareup/ui/orderhub/util/proto/KnownChannel;

    .line 25
    sget v2, Lcom/squareup/vectoricons/R$drawable;->brands_instagram_24:I

    const/4 v4, 0x4

    const-string v5, "INSTAGRAM"

    invoke-direct {v1, v5, v4, v2}, Lcom/squareup/ui/orderhub/util/proto/KnownChannel;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/ui/orderhub/util/proto/KnownChannel;->INSTAGRAM:Lcom/squareup/ui/orderhub/util/proto/KnownChannel;

    aput-object v1, v0, v4

    new-instance v1, Lcom/squareup/ui/orderhub/util/proto/KnownChannel;

    .line 26
    sget v2, Lcom/squareup/vectoricons/R$drawable;->brands_just_eat_24:I

    const/4 v4, 0x5

    const-string v5, "JUSTEAT"

    invoke-direct {v1, v5, v4, v2}, Lcom/squareup/ui/orderhub/util/proto/KnownChannel;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/ui/orderhub/util/proto/KnownChannel;->JUSTEAT:Lcom/squareup/ui/orderhub/util/proto/KnownChannel;

    aput-object v1, v0, v4

    new-instance v1, Lcom/squareup/ui/orderhub/util/proto/KnownChannel;

    .line 29
    sget v2, Lcom/squareup/vectoricons/R$drawable;->brands_square_24:I

    const/4 v4, 0x6

    const-string v5, "OTHER"

    invoke-direct {v1, v5, v4, v2}, Lcom/squareup/ui/orderhub/util/proto/KnownChannel;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/ui/orderhub/util/proto/KnownChannel;->OTHER:Lcom/squareup/ui/orderhub/util/proto/KnownChannel;

    aput-object v1, v0, v4

    new-instance v1, Lcom/squareup/ui/orderhub/util/proto/KnownChannel;

    .line 30
    sget v2, Lcom/squareup/vectoricons/R$drawable;->brands_postmates_24:I

    const/4 v4, 0x7

    const-string v5, "POSTMATES"

    invoke-direct {v1, v5, v4, v2}, Lcom/squareup/ui/orderhub/util/proto/KnownChannel;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/ui/orderhub/util/proto/KnownChannel;->POSTMATES:Lcom/squareup/ui/orderhub/util/proto/KnownChannel;

    aput-object v1, v0, v4

    new-instance v1, Lcom/squareup/ui/orderhub/util/proto/KnownChannel;

    .line 31
    sget v2, Lcom/squareup/vectoricons/R$drawable;->brands_square_24:I

    const/16 v4, 0x8

    const-string v5, "SQUARE"

    invoke-direct {v1, v5, v4, v2}, Lcom/squareup/ui/orderhub/util/proto/KnownChannel;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/ui/orderhub/util/proto/KnownChannel;->SQUARE:Lcom/squareup/ui/orderhub/util/proto/KnownChannel;

    aput-object v1, v0, v4

    new-instance v1, Lcom/squareup/ui/orderhub/util/proto/KnownChannel;

    .line 32
    sget v2, Lcom/squareup/vectoricons/R$drawable;->brands_uber_eats_24:I

    const/16 v4, 0x9

    const-string v5, "UBEREATS"

    invoke-direct {v1, v5, v4, v2}, Lcom/squareup/ui/orderhub/util/proto/KnownChannel;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/ui/orderhub/util/proto/KnownChannel;->UBEREATS:Lcom/squareup/ui/orderhub/util/proto/KnownChannel;

    aput-object v1, v0, v4

    new-instance v1, Lcom/squareup/ui/orderhub/util/proto/KnownChannel;

    .line 33
    sget v2, Lcom/squareup/vectoricons/R$drawable;->brands_weebly_24:I

    const/16 v4, 0xa

    const-string v5, "WEEBLY"

    invoke-direct {v1, v5, v4, v2}, Lcom/squareup/ui/orderhub/util/proto/KnownChannel;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/ui/orderhub/util/proto/KnownChannel;->WEEBLY:Lcom/squareup/ui/orderhub/util/proto/KnownChannel;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/ui/orderhub/util/proto/KnownChannel;->$VALUES:[Lcom/squareup/ui/orderhub/util/proto/KnownChannel;

    new-instance v0, Lcom/squareup/ui/orderhub/util/proto/KnownChannel$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/orderhub/util/proto/KnownChannel$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/orderhub/util/proto/KnownChannel;->Companion:Lcom/squareup/ui/orderhub/util/proto/KnownChannel$Companion;

    .line 39
    invoke-static {}, Lcom/squareup/ui/orderhub/util/proto/KnownChannel;->values()[Lcom/squareup/ui/orderhub/util/proto/KnownChannel;

    move-result-object v0

    .line 66
    array-length v1, v0

    invoke-static {v1}, Lkotlin/collections/MapsKt;->mapCapacity(I)I

    move-result v1

    const/16 v2, 0x10

    invoke-static {v1, v2}, Lkotlin/ranges/RangesKt;->coerceAtLeast(II)I

    move-result v1

    .line 67
    new-instance v2, Ljava/util/LinkedHashMap;

    invoke-direct {v2, v1}, Ljava/util/LinkedHashMap;-><init>(I)V

    check-cast v2, Ljava/util/Map;

    .line 68
    array-length v1, v0

    :goto_0
    if-ge v3, v1, :cond_0

    aget-object v4, v0, v3

    .line 39
    invoke-direct {v4}, Lcom/squareup/ui/orderhub/util/proto/KnownChannel;->getNormalizedName()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 71
    :cond_0
    sput-object v2, Lcom/squareup/ui/orderhub/util/proto/KnownChannel;->lookup:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/squareup/ui/orderhub/util/proto/KnownChannel;->iconRes:I

    return-void
.end method

.method public static final synthetic access$getLookup$cp()Ljava/util/Map;
    .locals 1

    .line 20
    sget-object v0, Lcom/squareup/ui/orderhub/util/proto/KnownChannel;->lookup:Ljava/util/Map;

    return-object v0
.end method

.method private final getNormalizedName()Ljava/lang/String;
    .locals 1

    .line 35
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/util/proto/KnownChannel;->name()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/ui/orderhub/util/proto/ChannelsKt;->access$getNormalized$p(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/orderhub/util/proto/KnownChannel;
    .locals 1

    const-class v0, Lcom/squareup/ui/orderhub/util/proto/KnownChannel;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/orderhub/util/proto/KnownChannel;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/orderhub/util/proto/KnownChannel;
    .locals 1

    sget-object v0, Lcom/squareup/ui/orderhub/util/proto/KnownChannel;->$VALUES:[Lcom/squareup/ui/orderhub/util/proto/KnownChannel;

    invoke-virtual {v0}, [Lcom/squareup/ui/orderhub/util/proto/KnownChannel;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/orderhub/util/proto/KnownChannel;

    return-object v0
.end method


# virtual methods
.method public final getIconRes()I
    .locals 1

    .line 20
    iget v0, p0, Lcom/squareup/ui/orderhub/util/proto/KnownChannel;->iconRes:I

    return v0
.end method
