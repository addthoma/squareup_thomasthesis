.class public final Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter_Factory;
.super Ljava/lang/Object;
.source "OrderHubRefundFlowStarter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter;",
        ">;"
    }
.end annotation


# instance fields
.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final orderHubRefundFlowStateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;",
            ">;"
        }
    .end annotation
.end field

.field private final permissionGatekeeperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;",
            ">;)V"
        }
    .end annotation

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 28
    iput-object p2, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p3, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter_Factory;->orderHubRefundFlowStateProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;",
            ">;)",
            "Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter_Factory;"
        }
    .end annotation

    .line 40
    new-instance v0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lflow/Flow;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;)Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter;
    .locals 1

    .line 45
    new-instance v0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter;-><init>(Lflow/Flow;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter;
    .locals 3

    .line 34
    iget-object v0, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflow/Flow;

    iget-object v1, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/permissions/PermissionGatekeeper;

    iget-object v2, p0, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter_Factory;->orderHubRefundFlowStateProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;

    invoke-static {v0, v1, v2}, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter_Factory;->newInstance(Lflow/Flow;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowState;)Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter_Factory;->get()Lcom/squareup/ui/orderhub/order/cancellation/OrderHubRefundFlowStarter;

    move-result-object v0

    return-object v0
.end method
