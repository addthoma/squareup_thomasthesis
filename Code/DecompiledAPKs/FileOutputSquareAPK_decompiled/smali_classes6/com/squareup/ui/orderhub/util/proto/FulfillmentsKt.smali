.class public final Lcom/squareup/ui/orderhub/util/proto/FulfillmentsKt;
.super Ljava/lang/Object;
.source "Fulfillments.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0000\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0010\u000b\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\u001a\u0012\u0010%\u001a\u00020\u0001*\u00020\u00022\u0006\u0010&\u001a\u00020\'\"\u0017\u0010\u0000\u001a\u0004\u0018\u00010\u0001*\u00020\u00028F\u00a2\u0006\u0006\u001a\u0004\u0008\u0003\u0010\u0004\"\u0017\u0010\u0005\u001a\u0004\u0018\u00010\u0001*\u00020\u00028F\u00a2\u0006\u0006\u001a\u0004\u0008\u0006\u0010\u0004\"\u0017\u0010\u0007\u001a\u0004\u0018\u00010\u0001*\u00020\u00028F\u00a2\u0006\u0006\u001a\u0004\u0008\u0008\u0010\u0004\"\u0018\u0010\t\u001a\u00020\n*\u00020\u00028@X\u0080\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\t\u0010\u000b\"\u0018\u0010\u000c\u001a\u00020\n*\u00020\u00028@X\u0080\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000c\u0010\u000b\"\u0018\u0010\r\u001a\u00020\n*\u00020\u00028@X\u0080\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\r\u0010\u000b\"\u0018\u0010\u000e\u001a\u00020\n*\u00020\u00028@X\u0080\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000e\u0010\u000b\"\u0017\u0010\u000f\u001a\u0004\u0018\u00010\u0001*\u00020\u00028F\u00a2\u0006\u0006\u001a\u0004\u0008\u0010\u0010\u0004\"\u0017\u0010\u0011\u001a\u0004\u0018\u00010\u0001*\u00020\u00028F\u00a2\u0006\u0006\u001a\u0004\u0008\u0012\u0010\u0004\"\u0017\u0010\u0013\u001a\u0004\u0018\u00010\u0014*\u00020\u00028F\u00a2\u0006\u0006\u001a\u0004\u0008\u0015\u0010\u0016\"\u001a\u0010\u0017\u001a\u0004\u0018\u00010\u0001*\u00020\u00028BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0018\u0010\u0004\"\u0017\u0010\u0019\u001a\u0004\u0018\u00010\u0001*\u00020\u00028F\u00a2\u0006\u0006\u001a\u0004\u0008\u001a\u0010\u0004\"\u0017\u0010\u001b\u001a\u0004\u0018\u00010\u001c*\u00020\u00028F\u00a2\u0006\u0006\u001a\u0004\u0008\u001d\u0010\u001e\"\u001a\u0010\u001b\u001a\u0004\u0018\u00010\u001c*\u00020\u001f8BX\u0082\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u001d\u0010 \"\u0015\u0010!\u001a\u00020\"*\u00020\u00028G\u00a2\u0006\u0006\u001a\u0004\u0008#\u0010$\u00a8\u0006("
    }
    d2 = {
        "cancelReason",
        "",
        "Lcom/squareup/orders/model/Order$Fulfillment;",
        "getCancelReason",
        "(Lcom/squareup/orders/model/Order$Fulfillment;)Ljava/lang/String;",
        "canceledAt",
        "getCanceledAt",
        "completedAt",
        "getCompletedAt",
        "isCanceled",
        "",
        "(Lcom/squareup/orders/model/Order$Fulfillment;)Z",
        "isCompleted",
        "isFailed",
        "isFinished",
        "note",
        "getNote",
        "pickupAt",
        "getPickupAt",
        "recipient",
        "Lcom/squareup/orders/model/Order$FulfillmentRecipient;",
        "getRecipient",
        "(Lcom/squareup/orders/model/Order$Fulfillment;)Lcom/squareup/orders/model/Order$FulfillmentRecipient;",
        "recipientName",
        "getRecipientName",
        "rejectedAt",
        "getRejectedAt",
        "trackingInfo",
        "Lcom/squareup/ordermanagerdata/TrackingInfo;",
        "getTrackingInfo",
        "(Lcom/squareup/orders/model/Order$Fulfillment;)Lcom/squareup/ordermanagerdata/TrackingInfo;",
        "Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;",
        "(Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;)Lcom/squareup/ordermanagerdata/TrackingInfo;",
        "typeNameShortRes",
        "",
        "getTypeNameShortRes",
        "(Lcom/squareup/orders/model/Order$Fulfillment;)I",
        "recipientNameOrDefault",
        "res",
        "Lcom/squareup/util/Res;",
        "orderhub-applet_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final getCancelReason(Lcom/squareup/orders/model/Order$Fulfillment;)Ljava/lang/String;
    .locals 3

    const-string v0, "$this$cancelReason"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 179
    iget-object v0, p0, Lcom/squareup/orders/model/Order$Fulfillment;->type:Lcom/squareup/orders/model/Order$FulfillmentType;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v2, Lcom/squareup/ui/orderhub/util/proto/FulfillmentsKt$WhenMappings;->$EnumSwitchMapping$8:[I

    invoke-virtual {v0}, Lcom/squareup/orders/model/Order$FulfillmentType;->ordinal()I

    move-result v0

    aget v0, v2, v0

    const/4 v2, 0x1

    if-eq v0, v2, :cond_4

    const/4 v2, 0x2

    if-eq v0, v2, :cond_3

    const/4 v2, 0x3

    if-eq v0, v2, :cond_2

    const/4 v2, 0x4

    if-eq v0, v2, :cond_1

    goto :goto_0

    .line 183
    :cond_1
    iget-object p0, p0, Lcom/squareup/orders/model/Order$Fulfillment;->delivery_details:Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;

    if-eqz p0, :cond_5

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->cancel_reason:Ljava/lang/String;

    goto :goto_0

    .line 182
    :cond_2
    iget-object p0, p0, Lcom/squareup/orders/model/Order$Fulfillment;->shipment_details:Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;

    if-eqz p0, :cond_5

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->cancel_reason:Ljava/lang/String;

    goto :goto_0

    .line 181
    :cond_3
    iget-object p0, p0, Lcom/squareup/orders/model/Order$Fulfillment;->pickup_details:Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;

    if-eqz p0, :cond_5

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->cancel_reason:Ljava/lang/String;

    goto :goto_0

    .line 180
    :cond_4
    iget-object p0, p0, Lcom/squareup/orders/model/Order$Fulfillment;->digital_details:Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;

    if-eqz p0, :cond_5

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;->cancel_reason:Ljava/lang/String;

    :cond_5
    :goto_0
    return-object v1
.end method

.method public static final getCanceledAt(Lcom/squareup/orders/model/Order$Fulfillment;)Ljava/lang/String;
    .locals 3

    const-string v0, "$this$canceledAt"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 163
    iget-object v0, p0, Lcom/squareup/orders/model/Order$Fulfillment;->type:Lcom/squareup/orders/model/Order$FulfillmentType;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v2, Lcom/squareup/ui/orderhub/util/proto/FulfillmentsKt$WhenMappings;->$EnumSwitchMapping$7:[I

    invoke-virtual {v0}, Lcom/squareup/orders/model/Order$FulfillmentType;->ordinal()I

    move-result v0

    aget v0, v2, v0

    const/4 v2, 0x1

    if-eq v0, v2, :cond_4

    const/4 v2, 0x2

    if-eq v0, v2, :cond_3

    const/4 v2, 0x3

    if-eq v0, v2, :cond_2

    const/4 v2, 0x4

    if-eq v0, v2, :cond_1

    goto :goto_0

    .line 167
    :cond_1
    iget-object p0, p0, Lcom/squareup/orders/model/Order$Fulfillment;->delivery_details:Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;

    if-eqz p0, :cond_5

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->canceled_at:Ljava/lang/String;

    goto :goto_0

    .line 166
    :cond_2
    iget-object p0, p0, Lcom/squareup/orders/model/Order$Fulfillment;->shipment_details:Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;

    if-eqz p0, :cond_5

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->canceled_at:Ljava/lang/String;

    goto :goto_0

    .line 165
    :cond_3
    iget-object p0, p0, Lcom/squareup/orders/model/Order$Fulfillment;->pickup_details:Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;

    if-eqz p0, :cond_5

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->canceled_at:Ljava/lang/String;

    goto :goto_0

    .line 164
    :cond_4
    iget-object p0, p0, Lcom/squareup/orders/model/Order$Fulfillment;->digital_details:Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;

    if-eqz p0, :cond_5

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;->canceled_at:Ljava/lang/String;

    :cond_5
    :goto_0
    return-object v1
.end method

.method public static final getCompletedAt(Lcom/squareup/orders/model/Order$Fulfillment;)Ljava/lang/String;
    .locals 3

    const-string v0, "$this$completedAt"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 146
    iget-object v0, p0, Lcom/squareup/orders/model/Order$Fulfillment;->type:Lcom/squareup/orders/model/Order$FulfillmentType;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v2, Lcom/squareup/ui/orderhub/util/proto/FulfillmentsKt$WhenMappings;->$EnumSwitchMapping$6:[I

    invoke-virtual {v0}, Lcom/squareup/orders/model/Order$FulfillmentType;->ordinal()I

    move-result v0

    aget v0, v2, v0

    const/4 v2, 0x1

    if-eq v0, v2, :cond_5

    const/4 v2, 0x2

    if-eq v0, v2, :cond_4

    const/4 v2, 0x3

    if-eq v0, v2, :cond_3

    const/4 v2, 0x4

    if-eq v0, v2, :cond_2

    const/4 v2, 0x5

    if-eq v0, v2, :cond_1

    goto :goto_0

    .line 151
    :cond_1
    iget-object p0, p0, Lcom/squareup/orders/model/Order$Fulfillment;->delivery_details:Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;

    if-eqz p0, :cond_6

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->completed_at:Ljava/lang/String;

    goto :goto_0

    .line 150
    :cond_2
    iget-object p0, p0, Lcom/squareup/orders/model/Order$Fulfillment;->managed_delivery_details:Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;

    if-eqz p0, :cond_6

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->picked_up_at:Ljava/lang/String;

    goto :goto_0

    .line 149
    :cond_3
    iget-object p0, p0, Lcom/squareup/orders/model/Order$Fulfillment;->shipment_details:Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;

    if-eqz p0, :cond_6

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->shipped_at:Ljava/lang/String;

    goto :goto_0

    .line 148
    :cond_4
    iget-object p0, p0, Lcom/squareup/orders/model/Order$Fulfillment;->pickup_details:Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;

    if-eqz p0, :cond_6

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->picked_up_at:Ljava/lang/String;

    goto :goto_0

    .line 147
    :cond_5
    iget-object p0, p0, Lcom/squareup/orders/model/Order$Fulfillment;->digital_details:Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;

    if-eqz p0, :cond_6

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;->completed_at:Ljava/lang/String;

    :cond_6
    :goto_0
    return-object v1
.end method

.method public static final getNote(Lcom/squareup/orders/model/Order$Fulfillment;)Ljava/lang/String;
    .locals 3

    const-string v0, "$this$note"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 113
    iget-object v0, p0, Lcom/squareup/orders/model/Order$Fulfillment;->type:Lcom/squareup/orders/model/Order$FulfillmentType;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v2, Lcom/squareup/ui/orderhub/util/proto/FulfillmentsKt$WhenMappings;->$EnumSwitchMapping$4:[I

    invoke-virtual {v0}, Lcom/squareup/orders/model/Order$FulfillmentType;->ordinal()I

    move-result v0

    aget v0, v2, v0

    const/4 v2, 0x1

    if-eq v0, v2, :cond_4

    const/4 v2, 0x2

    if-eq v0, v2, :cond_3

    const/4 v2, 0x3

    if-eq v0, v2, :cond_2

    const/4 v2, 0x4

    if-eq v0, v2, :cond_1

    goto :goto_0

    .line 117
    :cond_1
    iget-object p0, p0, Lcom/squareup/orders/model/Order$Fulfillment;->delivery_details:Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;

    if-eqz p0, :cond_5

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->note:Ljava/lang/String;

    goto :goto_0

    .line 116
    :cond_2
    iget-object p0, p0, Lcom/squareup/orders/model/Order$Fulfillment;->managed_delivery_details:Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;

    if-eqz p0, :cond_5

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->note:Ljava/lang/String;

    goto :goto_0

    .line 115
    :cond_3
    iget-object p0, p0, Lcom/squareup/orders/model/Order$Fulfillment;->shipment_details:Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;

    if-eqz p0, :cond_5

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->shipping_note:Ljava/lang/String;

    goto :goto_0

    .line 114
    :cond_4
    iget-object p0, p0, Lcom/squareup/orders/model/Order$Fulfillment;->pickup_details:Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;

    if-eqz p0, :cond_5

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->note:Ljava/lang/String;

    :cond_5
    :goto_0
    return-object v1
.end method

.method public static final getPickupAt(Lcom/squareup/orders/model/Order$Fulfillment;)Ljava/lang/String;
    .locals 3

    const-string v0, "$this$pickupAt"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 127
    iget-object v0, p0, Lcom/squareup/orders/model/Order$Fulfillment;->type:Lcom/squareup/orders/model/Order$FulfillmentType;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v2, Lcom/squareup/ui/orderhub/util/proto/FulfillmentsKt$WhenMappings;->$EnumSwitchMapping$5:[I

    invoke-virtual {v0}, Lcom/squareup/orders/model/Order$FulfillmentType;->ordinal()I

    move-result v0

    aget v0, v2, v0

    const/4 v2, 0x1

    if-eq v0, v2, :cond_2

    const/4 v2, 0x2

    if-eq v0, v2, :cond_1

    goto :goto_0

    .line 129
    :cond_1
    iget-object p0, p0, Lcom/squareup/orders/model/Order$Fulfillment;->managed_delivery_details:Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;

    if-eqz p0, :cond_3

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->pickup_at:Ljava/lang/String;

    goto :goto_0

    .line 128
    :cond_2
    iget-object p0, p0, Lcom/squareup/orders/model/Order$Fulfillment;->pickup_details:Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;

    if-eqz p0, :cond_3

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->pickup_at:Ljava/lang/String;

    :cond_3
    :goto_0
    return-object v1
.end method

.method public static final getRecipient(Lcom/squareup/orders/model/Order$Fulfillment;)Lcom/squareup/orders/model/Order$FulfillmentRecipient;
    .locals 3

    const-string v0, "$this$recipient"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    iget-object v0, p0, Lcom/squareup/orders/model/Order$Fulfillment;->type:Lcom/squareup/orders/model/Order$FulfillmentType;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v2, Lcom/squareup/ui/orderhub/util/proto/FulfillmentsKt$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {v0}, Lcom/squareup/orders/model/Order$FulfillmentType;->ordinal()I

    move-result v0

    aget v0, v2, v0

    const/4 v2, 0x1

    if-eq v0, v2, :cond_4

    const/4 v2, 0x2

    if-eq v0, v2, :cond_3

    const/4 v2, 0x3

    if-eq v0, v2, :cond_2

    const/4 v2, 0x4

    if-eq v0, v2, :cond_1

    goto :goto_0

    .line 33
    :cond_1
    iget-object p0, p0, Lcom/squareup/orders/model/Order$Fulfillment;->delivery_details:Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;

    if-eqz p0, :cond_5

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->recipient:Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    goto :goto_0

    .line 32
    :cond_2
    iget-object p0, p0, Lcom/squareup/orders/model/Order$Fulfillment;->shipment_details:Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;

    if-eqz p0, :cond_5

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->recipient:Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    goto :goto_0

    .line 31
    :cond_3
    iget-object p0, p0, Lcom/squareup/orders/model/Order$Fulfillment;->pickup_details:Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;

    if-eqz p0, :cond_5

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->recipient:Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    goto :goto_0

    .line 30
    :cond_4
    iget-object p0, p0, Lcom/squareup/orders/model/Order$Fulfillment;->digital_details:Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;

    if-eqz p0, :cond_5

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;->recipient:Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    :cond_5
    :goto_0
    return-object v1
.end method

.method private static final getRecipientName(Lcom/squareup/orders/model/Order$Fulfillment;)Ljava/lang/String;
    .locals 3

    .line 46
    iget-object v0, p0, Lcom/squareup/orders/model/Order$Fulfillment;->type:Lcom/squareup/orders/model/Order$FulfillmentType;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v2, Lcom/squareup/ui/orderhub/util/proto/FulfillmentsKt$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {v0}, Lcom/squareup/orders/model/Order$FulfillmentType;->ordinal()I

    move-result v0

    aget v0, v2, v0

    const/4 v2, 0x1

    if-eq v0, v2, :cond_1

    .line 48
    :goto_0
    invoke-static {p0}, Lcom/squareup/ui/orderhub/util/proto/FulfillmentsKt;->getRecipient(Lcom/squareup/orders/model/Order$Fulfillment;)Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    move-result-object p0

    if-eqz p0, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->display_name:Ljava/lang/String;

    goto :goto_1

    .line 47
    :cond_1
    iget-object p0, p0, Lcom/squareup/orders/model/Order$Fulfillment;->ext_fulfillment_client_details:Lcom/squareup/protos/client/orders/FulfillmentClientDetails;

    if-eqz p0, :cond_2

    iget-object p0, p0, Lcom/squareup/protos/client/orders/FulfillmentClientDetails;->courier:Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Courier;

    if-eqz p0, :cond_2

    iget-object v1, p0, Lcom/squareup/protos/client/orders/FulfillmentClientDetails$Courier;->display_name:Ljava/lang/String;

    :cond_2
    :goto_1
    return-object v1
.end method

.method public static final getRejectedAt(Lcom/squareup/orders/model/Order$Fulfillment;)Ljava/lang/String;
    .locals 3

    const-string v0, "$this$rejectedAt"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 195
    iget-object v0, p0, Lcom/squareup/orders/model/Order$Fulfillment;->type:Lcom/squareup/orders/model/Order$FulfillmentType;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v2, Lcom/squareup/ui/orderhub/util/proto/FulfillmentsKt$WhenMappings;->$EnumSwitchMapping$9:[I

    invoke-virtual {v0}, Lcom/squareup/orders/model/Order$FulfillmentType;->ordinal()I

    move-result v0

    aget v0, v2, v0

    const/4 v2, 0x1

    if-eq v0, v2, :cond_2

    const/4 v2, 0x2

    if-eq v0, v2, :cond_1

    goto :goto_0

    .line 197
    :cond_1
    iget-object p0, p0, Lcom/squareup/orders/model/Order$Fulfillment;->delivery_details:Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;

    if-eqz p0, :cond_3

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->rejected_at:Ljava/lang/String;

    goto :goto_0

    .line 196
    :cond_2
    iget-object p0, p0, Lcom/squareup/orders/model/Order$Fulfillment;->pickup_details:Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;

    if-eqz p0, :cond_3

    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->rejected_at:Ljava/lang/String;

    :cond_3
    :goto_0
    return-object v1
.end method

.method public static final getTrackingInfo(Lcom/squareup/orders/model/Order$Fulfillment;)Lcom/squareup/ordermanagerdata/TrackingInfo;
    .locals 3

    const-string v0, "$this$trackingInfo"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    iget-object v0, p0, Lcom/squareup/orders/model/Order$Fulfillment;->type:Lcom/squareup/orders/model/Order$FulfillmentType;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v2, Lcom/squareup/ui/orderhub/util/proto/FulfillmentsKt$WhenMappings;->$EnumSwitchMapping$3:[I

    invoke-virtual {v0}, Lcom/squareup/orders/model/Order$FulfillmentType;->ordinal()I

    move-result v0

    aget v0, v2, v0

    const/4 v2, 0x1

    if-eq v0, v2, :cond_1

    goto :goto_0

    .line 74
    :cond_1
    iget-object p0, p0, Lcom/squareup/orders/model/Order$Fulfillment;->shipment_details:Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;

    if-eqz p0, :cond_2

    invoke-static {p0}, Lcom/squareup/ui/orderhub/util/proto/FulfillmentsKt;->getTrackingInfo(Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;)Lcom/squareup/ordermanagerdata/TrackingInfo;

    move-result-object v1

    :cond_2
    :goto_0
    return-object v1
.end method

.method private static final getTrackingInfo(Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;)Lcom/squareup/ordermanagerdata/TrackingInfo;
    .locals 4

    .line 93
    iget-object v0, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->carrier:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->tracking_number:Ljava/lang/String;

    if-nez v0, :cond_0

    goto :goto_0

    .line 97
    :cond_0
    new-instance v0, Lcom/squareup/ordermanagerdata/TrackingInfo;

    .line 98
    iget-object v1, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->carrier:Ljava/lang/String;

    const-string v2, "carrier"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 99
    iget-object v2, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->tracking_number:Ljava/lang/String;

    const-string v3, "tracking_number"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 100
    iget-object p0, p0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->tracking_url:Ljava/lang/String;

    .line 97
    invoke-direct {v0, v1, v2, p0}, Lcom/squareup/ordermanagerdata/TrackingInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x0

    :goto_1
    return-object v0
.end method

.method public static final getTypeNameShortRes(Lcom/squareup/orders/model/Order$Fulfillment;)I
    .locals 3

    const-string v0, "$this$typeNameShortRes"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 229
    iget-object v0, p0, Lcom/squareup/orders/model/Order$Fulfillment;->type:Lcom/squareup/orders/model/Order$FulfillmentType;

    if-eqz v0, :cond_0

    sget-object v1, Lcom/squareup/ui/orderhub/util/proto/FulfillmentsKt$WhenMappings;->$EnumSwitchMapping$10:[I

    invoke-virtual {v0}, Lcom/squareup/orders/model/Order$FulfillmentType;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 238
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0

    .line 235
    :pswitch_0
    sget p0, Lcom/squareup/orderhub/applet/R$string;->orderhub_fulfillment_type_name_short_managed_delivery:I

    goto :goto_0

    .line 234
    :pswitch_1
    sget p0, Lcom/squareup/orderhub/applet/R$string;->orderhub_fulfillment_type_name_short_delivery:I

    goto :goto_0

    .line 233
    :pswitch_2
    sget p0, Lcom/squareup/orderhub/applet/R$string;->orderhub_fulfillment_type_name_short_shipment:I

    goto :goto_0

    .line 232
    :pswitch_3
    sget p0, Lcom/squareup/orderhub/applet/R$string;->orderhub_fulfillment_type_name_short_pickup:I

    goto :goto_0

    .line 231
    :pswitch_4
    sget p0, Lcom/squareup/orderhub/applet/R$string;->orderhub_fulfillment_type_name_short_digital:I

    goto :goto_0

    .line 230
    :pswitch_5
    sget p0, Lcom/squareup/orderhub/applet/R$string;->orderhub_fulfillment_type_name_short_custom:I

    :goto_0
    return p0

    .line 238
    :cond_0
    :pswitch_6
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_6
    .end packed-switch
.end method

.method public static final isCanceled(Lcom/squareup/orders/model/Order$Fulfillment;)Z
    .locals 1

    const-string v0, "$this$isCanceled"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 212
    iget-object p0, p0, Lcom/squareup/orders/model/Order$Fulfillment;->state:Lcom/squareup/orders/model/Order$Fulfillment$State;

    sget-object v0, Lcom/squareup/orders/model/Order$Fulfillment$State;->CANCELED:Lcom/squareup/orders/model/Order$Fulfillment$State;

    if-ne p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static final isCompleted(Lcom/squareup/orders/model/Order$Fulfillment;)Z
    .locals 1

    const-string v0, "$this$isCompleted"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 206
    iget-object p0, p0, Lcom/squareup/orders/model/Order$Fulfillment;->state:Lcom/squareup/orders/model/Order$Fulfillment$State;

    sget-object v0, Lcom/squareup/orders/model/Order$Fulfillment$State;->COMPLETED:Lcom/squareup/orders/model/Order$Fulfillment$State;

    if-ne p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static final isFailed(Lcom/squareup/orders/model/Order$Fulfillment;)Z
    .locals 1

    const-string v0, "$this$isFailed"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 217
    iget-object p0, p0, Lcom/squareup/orders/model/Order$Fulfillment;->state:Lcom/squareup/orders/model/Order$Fulfillment$State;

    sget-object v0, Lcom/squareup/orders/model/Order$Fulfillment$State;->FAILED:Lcom/squareup/orders/model/Order$Fulfillment$State;

    if-ne p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static final isFinished(Lcom/squareup/orders/model/Order$Fulfillment;)Z
    .locals 1

    const-string v0, "$this$isFinished"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 222
    invoke-static {p0}, Lcom/squareup/ui/orderhub/util/proto/FulfillmentsKt;->isCompleted(Lcom/squareup/orders/model/Order$Fulfillment;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p0}, Lcom/squareup/ui/orderhub/util/proto/FulfillmentsKt;->isCanceled(Lcom/squareup/orders/model/Order$Fulfillment;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p0}, Lcom/squareup/ui/orderhub/util/proto/FulfillmentsKt;->isFailed(Lcom/squareup/orders/model/Order$Fulfillment;)Z

    move-result p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method public static final recipientNameOrDefault(Lcom/squareup/orders/model/Order$Fulfillment;Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 1

    const-string v0, "$this$recipientNameOrDefault"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    invoke-static {p0}, Lcom/squareup/ui/orderhub/util/proto/FulfillmentsKt;->getRecipientName(Lcom/squareup/orders/model/Order$Fulfillment;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_1

    :cond_0
    iget-object p0, p0, Lcom/squareup/orders/model/Order$Fulfillment;->type:Lcom/squareup/orders/model/Order$FulfillmentType;

    if-nez p0, :cond_1

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/squareup/ui/orderhub/util/proto/FulfillmentsKt$WhenMappings;->$EnumSwitchMapping$2:[I

    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$FulfillmentType;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x1

    if-eq p0, v0, :cond_2

    .line 56
    :goto_0
    sget p0, Lcom/squareup/orderhub/applet/R$string;->orderhub_detail_order_missing_recipient:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 55
    :cond_2
    sget p0, Lcom/squareup/orderhub/applet/R$string;->orderhub_detail_order_no_courier_assigned:I

    invoke-interface {p1, p0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    return-object v0
.end method
