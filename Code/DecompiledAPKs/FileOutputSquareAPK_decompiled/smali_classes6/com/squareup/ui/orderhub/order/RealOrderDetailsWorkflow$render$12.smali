.class final Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$12;
.super Lkotlin/jvm/internal/Lambda;
.source "RealOrderDetailsWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;->render(Lcom/squareup/ui/orderhub/order/OrderDetailsInput;Lcom/squareup/ui/orderhub/order/OrderDetailsState;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsState;",
        "+",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsResult;",
        ">;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealOrderDetailsWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealOrderDetailsWorkflow.kt\ncom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$12\n*L\n1#1,885:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsState;",
        "Lcom/squareup/ui/orderhub/order/OrderDetailsResult;",
        "result",
        "Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

.field final synthetic this$0:Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;


# direct methods
.method constructor <init>(Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;Lcom/squareup/ui/orderhub/order/OrderDetailsState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$12;->this$0:Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;

    iput-object p2, p0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$12;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState;)Lcom/squareup/workflow/WorkflowAction;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/orderhub/order/OrderDetailsState;",
            "Lcom/squareup/ui/orderhub/order/OrderDetailsResult;",
            ">;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    const-string v2, "result"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 513
    instance-of v2, v1, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState$Loaded;

    const/4 v3, 0x2

    const/4 v4, 0x0

    if-eqz v2, :cond_1

    .line 514
    iget-object v1, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$12;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    check-cast v1, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->getCancelReason()Lcom/squareup/ordermanagerdata/CancellationReason;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-static {v1}, Lcom/squareup/util/Preconditions;->checkState(Z)V

    .line 516
    iget-object v1, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$12;->this$0:Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;

    invoke-static {v1}, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;->access$getFlow$p(Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow;)Lflow/Flow;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen;

    iget-object v5, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$12;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    invoke-virtual {v5}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v5

    invoke-static {v5}, Lcom/squareup/ui/orderhub/util/proto/OrdersKt;->getBillServerToken(Lcom/squareup/orders/model/Order;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v5}, Lcom/squareup/ui/orderhub/order/billhistory/BillHistoryDetailScreen;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lflow/Flow;->set(Ljava/lang/Object;)V

    .line 520
    sget-object v1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 521
    new-instance v2, Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;

    .line 522
    iget-object v5, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$12;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    invoke-virtual {v5}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->getShowOrderIdInActionBar()Z

    move-result v7

    .line 523
    iget-object v5, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$12;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    invoke-virtual {v5}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->isReadOnly()Z

    move-result v6

    .line 524
    iget-object v5, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$12;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    invoke-virtual {v5}, Lcom/squareup/ui/orderhub/order/OrderDetailsState;->getOrder()Lcom/squareup/orders/model/Order;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/16 v13, 0x78

    const/4 v14, 0x0

    move-object v5, v2

    .line 521
    invoke-direct/range {v5 .. v14}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$DisplayingOrderDetailsState;-><init>(ZZLcom/squareup/orders/model/Order;ZLcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;Lcom/squareup/protos/client/orders/Action;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 520
    invoke-static {v1, v2, v4, v3, v4}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    goto :goto_3

    .line 528
    :cond_1
    instance-of v2, v1, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState$Failed;

    if-eqz v2, :cond_2

    goto :goto_1

    :cond_2
    sget-object v5, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState$BillNotFound;->INSTANCE:Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState$BillNotFound;

    invoke-static {v1, v5}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    :goto_1
    if-eqz v2, :cond_3

    .line 530
    check-cast v1, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState$Failed;

    invoke-virtual {v1}, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState$Failed;->getFailure()Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;->getReceived()Lcom/squareup/receiving/ReceivedResponse;

    move-result-object v1

    sget-object v2, Lcom/squareup/receiving/ReceivedResponse$Error$NetworkError;->INSTANCE:Lcom/squareup/receiving/ReceivedResponse$Error$NetworkError;

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 533
    sget-object v1, Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailure$ConnectionError;->INSTANCE:Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailure$ConnectionError;

    check-cast v1, Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailure;

    goto :goto_2

    .line 535
    :cond_3
    sget-object v1, Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailure$BillRetrievalError;->INSTANCE:Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailure$BillRetrievalError;

    check-cast v1, Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailure;

    .line 537
    :goto_2
    sget-object v2, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 538
    iget-object v5, v0, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$12;->$state:Lcom/squareup/ui/orderhub/order/OrderDetailsState;

    move-object v6, v5

    check-cast v6, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    .line 540
    new-instance v13, Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;

    .line 542
    sget-object v5, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event$RetryBillRetrieval;->INSTANCE:Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event$RetryBillRetrieval;

    check-cast v5, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event;

    .line 543
    sget-object v14, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event$CancelBillRetrieval;->INSTANCE:Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event$CancelBillRetrieval;

    check-cast v14, Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event;

    .line 540
    invoke-direct {v13, v1, v5, v14}, Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;-><init>(Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailure;Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event;Lcom/squareup/ui/orderhub/alerts/OrderHubOrderUpdateFailedDialogScreen$Event;)V

    const/4 v14, 0x0

    const/16 v15, 0x37

    const/16 v16, 0x0

    .line 538
    invoke-static/range {v6 .. v16}, Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;->copy$default(Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;ZZLcom/squareup/orders/model/Order;ZLcom/squareup/ordermanagerdata/CancellationReason;Ljava/util/Map;Lcom/squareup/ui/orderhub/alerts/OrderUpdateFailureState;ZILjava/lang/Object;)Lcom/squareup/ui/orderhub/order/OrderDetailsState$RetrievingBillState;

    move-result-object v1

    .line 537
    invoke-static {v2, v1, v4, v3, v4}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object v1

    :goto_3
    return-object v1

    .line 549
    :cond_4
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "bill loader must return success or failure"

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 112
    check-cast p1, Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/order/RealOrderDetailsWorkflow$render$12;->invoke(Lcom/squareup/ui/orderhub/order/billhistory/OrderHubBillHistoryLoadedState;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
