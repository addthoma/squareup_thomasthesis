.class public abstract Lcom/squareup/ui/orderhub/master/Filter;
.super Ljava/lang/Object;
.source "Filter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/orderhub/master/Filter$Status;,
        Lcom/squareup/ui/orderhub/master/Filter$Type;,
        Lcom/squareup/ui/orderhub/master/Filter$Source;,
        Lcom/squareup/ui/orderhub/master/Filter$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nFilter.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Filter.kt\ncom/squareup/ui/orderhub/master/Filter\n*L\n1#1,283:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u0000 \u000c2\u00020\u0001:\u0004\u000c\r\u000e\u000fB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H&J\u0010\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\u0018\u0010\u0008\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\t\u001a\u00020\nH\u0016J\u0010\u0010\u000b\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H&\u0082\u0001\u0003\u0010\u0011\u0012\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/ui/orderhub/master/Filter;",
        "",
        "()V",
        "getFilterName",
        "",
        "res",
        "Lcom/squareup/util/Res;",
        "getFilterNameShort",
        "getFilterNameShortLowerCased",
        "locale",
        "Ljava/util/Locale;",
        "getGroupName",
        "Companion",
        "Source",
        "Status",
        "Type",
        "Lcom/squareup/ui/orderhub/master/Filter$Status;",
        "Lcom/squareup/ui/orderhub/master/Filter$Type;",
        "Lcom/squareup/ui/orderhub/master/Filter$Source;",
        "orderhub-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ui/orderhub/master/Filter$Companion;

.field private static final DEFAULT$delegate:Lkotlin/Lazy;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/orderhub/master/Filter$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/orderhub/master/Filter$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/orderhub/master/Filter;->Companion:Lcom/squareup/ui/orderhub/master/Filter$Companion;

    .line 121
    sget-object v0, Lcom/squareup/ui/orderhub/master/Filter$Companion$DEFAULT$2;->INSTANCE:Lcom/squareup/ui/orderhub/master/Filter$Companion$DEFAULT$2;

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {v0}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/orderhub/master/Filter;->DEFAULT$delegate:Lkotlin/Lazy;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 37
    invoke-direct {p0}, Lcom/squareup/ui/orderhub/master/Filter;-><init>()V

    return-void
.end method

.method public static final synthetic access$getDEFAULT$cp()Lkotlin/Lazy;
    .locals 1

    .line 37
    sget-object v0, Lcom/squareup/ui/orderhub/master/Filter;->DEFAULT$delegate:Lkotlin/Lazy;

    return-object v0
.end method


# virtual methods
.method public abstract getFilterName(Lcom/squareup/util/Res;)Ljava/lang/String;
.end method

.method public getFilterNameShort(Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 1

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 106
    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/master/Filter;->getFilterName(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getFilterNameShortLowerCased(Lcom/squareup/util/Res;Ljava/util/Locale;)Ljava/lang/String;
    .locals 1

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "locale"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 114
    invoke-virtual {p0, p1}, Lcom/squareup/ui/orderhub/master/Filter;->getFilterNameShort(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p1, p2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p1

    const-string p2, "(this as java.lang.String).toLowerCase(locale)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type java.lang.String"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public abstract getGroupName(Lcom/squareup/util/Res;)Ljava/lang/String;
.end method
