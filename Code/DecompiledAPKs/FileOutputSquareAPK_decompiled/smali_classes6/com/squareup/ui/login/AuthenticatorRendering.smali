.class public final Lcom/squareup/ui/login/AuthenticatorRendering;
.super Ljava/lang/Object;
.source "AuthenticatorRendering.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u000c\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B/\u0012\u000e\u0008\u0002\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u0006\u0012\u000e\u0008\u0002\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008\u00a2\u0006\u0002\u0010\nJ\u000f\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0003J\t\u0010\u0012\u001a\u00020\u0006H\u00c6\u0003J\u000f\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008H\u00c6\u0003J3\u0010\u0014\u001a\u00020\u00002\u000e\u0008\u0002\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0008\u0008\u0002\u0010\u0005\u001a\u00020\u00062\u000e\u0008\u0002\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008H\u00c6\u0001J\u0013\u0010\u0015\u001a\u00020\u00162\u0008\u0010\u0017\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0018\u001a\u00020\u0019H\u00d6\u0001J\t\u0010\u001a\u001a\u00020\u001bH\u00d6\u0001R\u0017\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0017\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u0010\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/ui/login/AuthenticatorRendering;",
        "",
        "screens",
        "",
        "Lcom/squareup/ui/login/AuthenticatorScreenRendering;",
        "operation",
        "Lcom/squareup/ui/login/Operation;",
        "input",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "Lcom/squareup/ui/login/AuthenticatorEvent;",
        "(Ljava/util/List;Lcom/squareup/ui/login/Operation;Lcom/squareup/workflow/legacy/WorkflowInput;)V",
        "getInput",
        "()Lcom/squareup/workflow/legacy/WorkflowInput;",
        "getOperation",
        "()Lcom/squareup/ui/login/Operation;",
        "getScreens",
        "()Ljava/util/List;",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "",
        "other",
        "hashCode",
        "",
        "toString",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final input:Lcom/squareup/workflow/legacy/WorkflowInput;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;"
        }
    .end annotation
.end field

.field private final operation:Lcom/squareup/ui/login/Operation;

.field private final screens:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/ui/login/AuthenticatorScreenRendering;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 6

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x7

    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/login/AuthenticatorRendering;-><init>(Ljava/util/List;Lcom/squareup/ui/login/Operation;Lcom/squareup/workflow/legacy/WorkflowInput;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lcom/squareup/ui/login/Operation;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/ui/login/AuthenticatorScreenRendering;",
            ">;",
            "Lcom/squareup/ui/login/Operation;",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;)V"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "operation"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "input"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/login/AuthenticatorRendering;->screens:Ljava/util/List;

    iput-object p2, p0, Lcom/squareup/ui/login/AuthenticatorRendering;->operation:Lcom/squareup/ui/login/Operation;

    iput-object p3, p0, Lcom/squareup/ui/login/AuthenticatorRendering;->input:Lcom/squareup/workflow/legacy/WorkflowInput;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/util/List;Lcom/squareup/ui/login/Operation;Lcom/squareup/workflow/legacy/WorkflowInput;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    .line 27
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p1

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    .line 28
    sget-object p2, Lcom/squareup/ui/login/Operation;->Companion:Lcom/squareup/ui/login/Operation$Companion;

    invoke-virtual {p2}, Lcom/squareup/ui/login/Operation$Companion;->getNONE()Lcom/squareup/ui/login/Operation;

    move-result-object p2

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    .line 29
    sget-object p3, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {p3}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p3

    :cond_2
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/ui/login/AuthenticatorRendering;-><init>(Ljava/util/List;Lcom/squareup/ui/login/Operation;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/login/AuthenticatorRendering;Ljava/util/List;Lcom/squareup/ui/login/Operation;Lcom/squareup/workflow/legacy/WorkflowInput;ILjava/lang/Object;)Lcom/squareup/ui/login/AuthenticatorRendering;
    .locals 0

    and-int/lit8 p5, p4, 0x1

    if-eqz p5, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/login/AuthenticatorRendering;->screens:Ljava/util/List;

    :cond_0
    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_1

    iget-object p2, p0, Lcom/squareup/ui/login/AuthenticatorRendering;->operation:Lcom/squareup/ui/login/Operation;

    :cond_1
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_2

    iget-object p3, p0, Lcom/squareup/ui/login/AuthenticatorRendering;->input:Lcom/squareup/workflow/legacy/WorkflowInput;

    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/login/AuthenticatorRendering;->copy(Ljava/util/List;Lcom/squareup/ui/login/Operation;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/ui/login/AuthenticatorRendering;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/ui/login/AuthenticatorScreenRendering;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorRendering;->screens:Ljava/util/List;

    return-object v0
.end method

.method public final component2()Lcom/squareup/ui/login/Operation;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorRendering;->operation:Lcom/squareup/ui/login/Operation;

    return-object v0
.end method

.method public final component3()Lcom/squareup/workflow/legacy/WorkflowInput;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorRendering;->input:Lcom/squareup/workflow/legacy/WorkflowInput;

    return-object v0
.end method

.method public final copy(Ljava/util/List;Lcom/squareup/ui/login/Operation;Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/ui/login/AuthenticatorRendering;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/ui/login/AuthenticatorScreenRendering;",
            ">;",
            "Lcom/squareup/ui/login/Operation;",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;)",
            "Lcom/squareup/ui/login/AuthenticatorRendering;"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "operation"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "input"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/login/AuthenticatorRendering;

    invoke-direct {v0, p1, p2, p3}, Lcom/squareup/ui/login/AuthenticatorRendering;-><init>(Ljava/util/List;Lcom/squareup/ui/login/Operation;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/login/AuthenticatorRendering;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/login/AuthenticatorRendering;

    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorRendering;->screens:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/ui/login/AuthenticatorRendering;->screens:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorRendering;->operation:Lcom/squareup/ui/login/Operation;

    iget-object v1, p1, Lcom/squareup/ui/login/AuthenticatorRendering;->operation:Lcom/squareup/ui/login/Operation;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorRendering;->input:Lcom/squareup/workflow/legacy/WorkflowInput;

    iget-object p1, p1, Lcom/squareup/ui/login/AuthenticatorRendering;->input:Lcom/squareup/workflow/legacy/WorkflowInput;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getInput()Lcom/squareup/workflow/legacy/WorkflowInput;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;"
        }
    .end annotation

    .line 29
    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorRendering;->input:Lcom/squareup/workflow/legacy/WorkflowInput;

    return-object v0
.end method

.method public final getOperation()Lcom/squareup/ui/login/Operation;
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorRendering;->operation:Lcom/squareup/ui/login/Operation;

    return-object v0
.end method

.method public final getScreens()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/ui/login/AuthenticatorScreenRendering;",
            ">;"
        }
    .end annotation

    .line 27
    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorRendering;->screens:Ljava/util/List;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorRendering;->screens:Ljava/util/List;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/login/AuthenticatorRendering;->operation:Lcom/squareup/ui/login/Operation;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/login/AuthenticatorRendering;->input:Lcom/squareup/workflow/legacy/WorkflowInput;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "AuthenticatorRendering(screens="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/login/AuthenticatorRendering;->screens:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", operation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/login/AuthenticatorRendering;->operation:Lcom/squareup/ui/login/Operation;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", input="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/login/AuthenticatorRendering;->input:Lcom/squareup/workflow/legacy/WorkflowInput;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
