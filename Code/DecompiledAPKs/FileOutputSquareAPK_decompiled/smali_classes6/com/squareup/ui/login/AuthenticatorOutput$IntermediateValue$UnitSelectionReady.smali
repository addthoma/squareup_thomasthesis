.class public final Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady;
.super Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue;
.source "AuthenticatorOutput.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "UnitSelectionReady"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady$SessionToken;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u00002\u00020\u0001:\u0001\u0016B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000c\u001a\u00020\u0005H\u00c6\u0003J\u001d\u0010\r\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u0005H\u00c6\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\u0008\u0010\u0010\u001a\u0004\u0018\u00010\u0011H\u00d6\u0003J\t\u0010\u0012\u001a\u00020\u0013H\u00d6\u0001J\t\u0010\u0014\u001a\u00020\u0015H\u00d6\u0001R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady;",
        "Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue;",
        "sessionToken",
        "Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady$SessionToken;",
        "resumeState",
        "Lcom/squareup/ui/login/AuthenticatorState;",
        "(Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady$SessionToken;Lcom/squareup/ui/login/AuthenticatorState;)V",
        "getResumeState",
        "()Lcom/squareup/ui/login/AuthenticatorState;",
        "getSessionToken",
        "()Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady$SessionToken;",
        "component1",
        "component2",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "SessionToken",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final resumeState:Lcom/squareup/ui/login/AuthenticatorState;

.field private final sessionToken:Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady$SessionToken;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady$SessionToken;Lcom/squareup/ui/login/AuthenticatorState;)V
    .locals 1

    const-string v0, "sessionToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "resumeState"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 79
    invoke-direct {p0, v0}, Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady;->sessionToken:Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady$SessionToken;

    iput-object p2, p0, Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady;->resumeState:Lcom/squareup/ui/login/AuthenticatorState;

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady;Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady$SessionToken;Lcom/squareup/ui/login/AuthenticatorState;ILjava/lang/Object;)Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady;
    .locals 0

    and-int/lit8 p4, p3, 0x1

    if-eqz p4, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady;->sessionToken:Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady$SessionToken;

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    iget-object p2, p0, Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady;->resumeState:Lcom/squareup/ui/login/AuthenticatorState;

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady;->copy(Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady$SessionToken;Lcom/squareup/ui/login/AuthenticatorState;)Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady$SessionToken;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady;->sessionToken:Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady$SessionToken;

    return-object v0
.end method

.method public final component2()Lcom/squareup/ui/login/AuthenticatorState;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady;->resumeState:Lcom/squareup/ui/login/AuthenticatorState;

    return-object v0
.end method

.method public final copy(Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady$SessionToken;Lcom/squareup/ui/login/AuthenticatorState;)Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady;
    .locals 1

    const-string v0, "sessionToken"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "resumeState"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady;

    invoke-direct {v0, p1, p2}, Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady;-><init>(Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady$SessionToken;Lcom/squareup/ui/login/AuthenticatorState;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady;

    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady;->sessionToken:Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady$SessionToken;

    iget-object v1, p1, Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady;->sessionToken:Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady$SessionToken;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady;->resumeState:Lcom/squareup/ui/login/AuthenticatorState;

    iget-object p1, p1, Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady;->resumeState:Lcom/squareup/ui/login/AuthenticatorState;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getResumeState()Lcom/squareup/ui/login/AuthenticatorState;
    .locals 1

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady;->resumeState:Lcom/squareup/ui/login/AuthenticatorState;

    return-object v0
.end method

.method public final getSessionToken()Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady$SessionToken;
    .locals 1

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady;->sessionToken:Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady$SessionToken;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady;->sessionToken:Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady$SessionToken;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady;->resumeState:Lcom/squareup/ui/login/AuthenticatorState;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "UnitSelectionReady(sessionToken="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady;->sessionToken:Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady$SessionToken;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", resumeState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/login/AuthenticatorOutput$IntermediateValue$UnitSelectionReady;->resumeState:Lcom/squareup/ui/login/AuthenticatorState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
