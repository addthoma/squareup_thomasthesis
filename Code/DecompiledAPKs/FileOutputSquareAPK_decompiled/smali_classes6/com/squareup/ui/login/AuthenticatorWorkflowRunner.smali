.class public final Lcom/squareup/ui/login/AuthenticatorWorkflowRunner;
.super Lcom/squareup/container/DynamicPropsWorkflowV2Runner;
.source "AuthenticatorWorkflowRunner.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/login/AuthenticatorWorkflowRunner$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/container/DynamicPropsWorkflowV2Runner<",
        "Lcom/squareup/ui/login/AuthenticatorInput;",
        "Lcom/squareup/ui/login/AuthenticatorOutput;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\u0018\u0000 \u00122\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001:\u0001\u0012B\u001f\u0008\u0007\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0015\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0000\u00a2\u0006\u0002\u0008\u0011R\u0014\u0010\u0008\u001a\u00020\tX\u0094\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000c\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/ui/login/AuthenticatorWorkflowRunner;",
        "Lcom/squareup/container/DynamicPropsWorkflowV2Runner;",
        "Lcom/squareup/ui/login/AuthenticatorInput;",
        "Lcom/squareup/ui/login/AuthenticatorOutput;",
        "historySource",
        "Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;",
        "viewFactory",
        "Lcom/squareup/ui/login/AuthenticatorViewFactory;",
        "workflow",
        "Lcom/squareup/ui/login/RealAuthenticatorRendererWorkflow;",
        "(Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;Lcom/squareup/ui/login/AuthenticatorViewFactory;Lcom/squareup/ui/login/RealAuthenticatorRendererWorkflow;)V",
        "getWorkflow",
        "()Lcom/squareup/ui/login/RealAuthenticatorRendererWorkflow;",
        "start",
        "",
        "asLandingScreen",
        "",
        "start$loggedout_release",
        "Companion",
        "loggedout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ui/login/AuthenticatorWorkflowRunner$Companion;

.field private static final NAME:Ljava/lang/String;


# instance fields
.field private final workflow:Lcom/squareup/ui/login/RealAuthenticatorRendererWorkflow;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/login/AuthenticatorWorkflowRunner$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/login/AuthenticatorWorkflowRunner$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/login/AuthenticatorWorkflowRunner;->Companion:Lcom/squareup/ui/login/AuthenticatorWorkflowRunner$Companion;

    .line 31
    const-class v0, Lcom/squareup/ui/login/AuthenticatorWorkflowRunner;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AuthenticatorWorkflowRunner::class.java.name"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/ui/login/AuthenticatorWorkflowRunner;->NAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;Lcom/squareup/ui/login/AuthenticatorViewFactory;Lcom/squareup/ui/login/RealAuthenticatorRendererWorkflow;)V
    .locals 9
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "historySource"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "viewFactory"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "workflow"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    sget-object v2, Lcom/squareup/ui/login/AuthenticatorWorkflowRunner;->NAME:Ljava/lang/String;

    .line 17
    invoke-virtual {p1}, Lcom/squareup/ui/loggedout/LoggedOutActivityScope$Container;->getNextHistory()Lio/reactivex/Observable;

    move-result-object v3

    const-string p1, "historySource.nextHistory"

    invoke-static {v3, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    move-object v4, p2

    check-cast v4, Lcom/squareup/workflow/WorkflowViewFactory;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x18

    const/4 v8, 0x0

    move-object v1, p0

    .line 15
    invoke-direct/range {v1 .. v8}, Lcom/squareup/container/DynamicPropsWorkflowV2Runner;-><init>(Ljava/lang/String;Lio/reactivex/Observable;Lcom/squareup/workflow/WorkflowViewFactory;ZLkotlinx/coroutines/CoroutineDispatcher;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p3, p0, Lcom/squareup/ui/login/AuthenticatorWorkflowRunner;->workflow:Lcom/squareup/ui/login/RealAuthenticatorRendererWorkflow;

    return-void
.end method

.method public static final synthetic access$getNAME$cp()Ljava/lang/String;
    .locals 1

    .line 10
    sget-object v0, Lcom/squareup/ui/login/AuthenticatorWorkflowRunner;->NAME:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method protected getWorkflow()Lcom/squareup/ui/login/RealAuthenticatorRendererWorkflow;
    .locals 1

    .line 14
    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorWorkflowRunner;->workflow:Lcom/squareup/ui/login/RealAuthenticatorRendererWorkflow;

    return-object v0
.end method

.method public bridge synthetic getWorkflow()Lcom/squareup/workflow/Workflow;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorWorkflowRunner;->getWorkflow()Lcom/squareup/ui/login/RealAuthenticatorRendererWorkflow;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/Workflow;

    return-object v0
.end method

.method public final start$loggedout_release(Z)V
    .locals 3

    .line 26
    new-instance v0, Lcom/squareup/ui/login/AuthenticatorInput;

    new-instance v1, Lcom/squareup/ui/login/AuthenticatorLaunchMode$Start;

    invoke-direct {v1, p1}, Lcom/squareup/ui/login/AuthenticatorLaunchMode$Start;-><init>(Z)V

    check-cast v1, Lcom/squareup/ui/login/AuthenticatorLaunchMode;

    const/4 p1, 0x0

    const/4 v2, 0x2

    invoke-direct {v0, v1, p1, v2, p1}, Lcom/squareup/ui/login/AuthenticatorInput;-><init>(Lcom/squareup/ui/login/AuthenticatorLaunchMode;Lcom/squareup/ui/login/UnitSelectionFlow;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    invoke-virtual {p0, v0}, Lcom/squareup/ui/login/AuthenticatorWorkflowRunner;->setProps(Ljava/lang/Object;)V

    .line 27
    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorWorkflowRunner;->ensureWorkflow()V

    return-void
.end method
