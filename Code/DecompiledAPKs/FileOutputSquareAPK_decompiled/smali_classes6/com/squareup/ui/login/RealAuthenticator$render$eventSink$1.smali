.class final Lcom/squareup/ui/login/RealAuthenticator$render$eventSink$1;
.super Lkotlin/jvm/internal/Lambda;
.source "RealAuthenticator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/login/RealAuthenticator;->render(Lcom/squareup/ui/login/AuthenticatorInput;Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/ui/login/AuthenticatorRendering;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/ui/login/AuthenticatorEvent;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/ui/login/AuthenticatorState;",
        "+",
        "Lcom/squareup/ui/login/AuthenticatorOutput;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001a\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u0012\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001j\u0002`\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/ui/login/AuthenticatorState;",
        "Lcom/squareup/ui/login/AuthenticatorOutput;",
        "Lcom/squareup/ui/login/AuthUpdate;",
        "event",
        "Lcom/squareup/ui/login/AuthenticatorEvent;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $cleanedState:Lcom/squareup/ui/login/AuthenticatorState;

.field final synthetic $props:Lcom/squareup/ui/login/AuthenticatorInput;

.field final synthetic this$0:Lcom/squareup/ui/login/RealAuthenticator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/login/RealAuthenticator;Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorInput;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/login/RealAuthenticator$render$eventSink$1;->this$0:Lcom/squareup/ui/login/RealAuthenticator;

    iput-object p2, p0, Lcom/squareup/ui/login/RealAuthenticator$render$eventSink$1;->$cleanedState:Lcom/squareup/ui/login/AuthenticatorState;

    iput-object p3, p0, Lcom/squareup/ui/login/RealAuthenticator$render$eventSink$1;->$props:Lcom/squareup/ui/login/AuthenticatorInput;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/ui/login/AuthenticatorEvent;)Lcom/squareup/workflow/WorkflowAction;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/login/AuthenticatorState;",
            "Lcom/squareup/ui/login/AuthenticatorOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "event"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 247
    iget-object v0, p0, Lcom/squareup/ui/login/RealAuthenticator$render$eventSink$1;->this$0:Lcom/squareup/ui/login/RealAuthenticator;

    iget-object v1, p0, Lcom/squareup/ui/login/RealAuthenticator$render$eventSink$1;->$cleanedState:Lcom/squareup/ui/login/AuthenticatorState;

    iget-object v2, p0, Lcom/squareup/ui/login/RealAuthenticator$render$eventSink$1;->$props:Lcom/squareup/ui/login/AuthenticatorInput;

    invoke-virtual {v0, v1, v2, p1}, Lcom/squareup/ui/login/RealAuthenticator;->handleEvent$impl_release(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorInput;Lcom/squareup/ui/login/AuthenticatorEvent;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 157
    check-cast p1, Lcom/squareup/ui/login/AuthenticatorEvent;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/login/RealAuthenticator$render$eventSink$1;->invoke(Lcom/squareup/ui/login/AuthenticatorEvent;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
