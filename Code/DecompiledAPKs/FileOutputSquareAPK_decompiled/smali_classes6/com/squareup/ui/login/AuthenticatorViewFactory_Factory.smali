.class public final Lcom/squareup/ui/login/AuthenticatorViewFactory_Factory;
.super Ljava/lang/Object;
.source "AuthenticatorViewFactory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/login/AuthenticatorViewFactory;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg1Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/login/LoginAlertDialogFactory$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg2Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/login/LoginWarningDialogFactory$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg3Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/login/EnrollSmsCoordinator$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg4Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg5Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$Factory;",
            ">;"
        }
    .end annotation
.end field

.field private final arg6Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/login/AuthenticatorViewFactory$DeviceCodeViewBinding;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/login/LoginAlertDialogFactory$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/login/LoginWarningDialogFactory$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/login/EnrollSmsCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/login/AuthenticatorViewFactory$DeviceCodeViewBinding;",
            ">;)V"
        }
    .end annotation

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/squareup/ui/login/AuthenticatorViewFactory_Factory;->arg0Provider:Ljavax/inject/Provider;

    .line 35
    iput-object p2, p0, Lcom/squareup/ui/login/AuthenticatorViewFactory_Factory;->arg1Provider:Ljavax/inject/Provider;

    .line 36
    iput-object p3, p0, Lcom/squareup/ui/login/AuthenticatorViewFactory_Factory;->arg2Provider:Ljavax/inject/Provider;

    .line 37
    iput-object p4, p0, Lcom/squareup/ui/login/AuthenticatorViewFactory_Factory;->arg3Provider:Ljavax/inject/Provider;

    .line 38
    iput-object p5, p0, Lcom/squareup/ui/login/AuthenticatorViewFactory_Factory;->arg4Provider:Ljavax/inject/Provider;

    .line 39
    iput-object p6, p0, Lcom/squareup/ui/login/AuthenticatorViewFactory_Factory;->arg5Provider:Ljavax/inject/Provider;

    .line 40
    iput-object p7, p0, Lcom/squareup/ui/login/AuthenticatorViewFactory_Factory;->arg6Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/login/AuthenticatorViewFactory_Factory;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/login/LoginAlertDialogFactory$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/login/LoginWarningDialogFactory$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/login/EnrollSmsCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$Factory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/login/AuthenticatorViewFactory$DeviceCodeViewBinding;",
            ">;)",
            "Lcom/squareup/ui/login/AuthenticatorViewFactory_Factory;"
        }
    .end annotation

    .line 56
    new-instance v8, Lcom/squareup/ui/login/AuthenticatorViewFactory_Factory;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/login/AuthenticatorViewFactory_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v8
.end method

.method public static newInstance(Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$Factory;Lcom/squareup/ui/login/LoginAlertDialogFactory$Factory;Lcom/squareup/ui/login/LoginWarningDialogFactory$Factory;Lcom/squareup/ui/login/EnrollSmsCoordinator$Factory;Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator$Factory;Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$Factory;Lcom/squareup/ui/login/AuthenticatorViewFactory$DeviceCodeViewBinding;)Lcom/squareup/ui/login/AuthenticatorViewFactory;
    .locals 9

    .line 64
    new-instance v8, Lcom/squareup/ui/login/AuthenticatorViewFactory;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/login/AuthenticatorViewFactory;-><init>(Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$Factory;Lcom/squareup/ui/login/LoginAlertDialogFactory$Factory;Lcom/squareup/ui/login/LoginWarningDialogFactory$Factory;Lcom/squareup/ui/login/EnrollSmsCoordinator$Factory;Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator$Factory;Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$Factory;Lcom/squareup/ui/login/AuthenticatorViewFactory$DeviceCodeViewBinding;)V

    return-object v8
.end method


# virtual methods
.method public get()Lcom/squareup/ui/login/AuthenticatorViewFactory;
    .locals 8

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorViewFactory_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$Factory;

    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorViewFactory_Factory;->arg1Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/ui/login/LoginAlertDialogFactory$Factory;

    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorViewFactory_Factory;->arg2Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/ui/login/LoginWarningDialogFactory$Factory;

    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorViewFactory_Factory;->arg3Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/ui/login/EnrollSmsCoordinator$Factory;

    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorViewFactory_Factory;->arg4Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator$Factory;

    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorViewFactory_Factory;->arg5Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$Factory;

    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorViewFactory_Factory;->arg6Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/ui/login/AuthenticatorViewFactory$DeviceCodeViewBinding;

    invoke-static/range {v1 .. v7}, Lcom/squareup/ui/login/AuthenticatorViewFactory_Factory;->newInstance(Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$Factory;Lcom/squareup/ui/login/LoginAlertDialogFactory$Factory;Lcom/squareup/ui/login/LoginWarningDialogFactory$Factory;Lcom/squareup/ui/login/EnrollSmsCoordinator$Factory;Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator$Factory;Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$Factory;Lcom/squareup/ui/login/AuthenticatorViewFactory$DeviceCodeViewBinding;)Lcom/squareup/ui/login/AuthenticatorViewFactory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorViewFactory_Factory;->get()Lcom/squareup/ui/login/AuthenticatorViewFactory;

    move-result-object v0

    return-object v0
.end method
