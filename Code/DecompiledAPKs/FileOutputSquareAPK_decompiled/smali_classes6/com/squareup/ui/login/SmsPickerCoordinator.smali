.class public final Lcom/squareup/ui/login/SmsPickerCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "SmsPickerCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/login/SmsPickerCoordinator$TwoFactorDetailsAdapter;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSmsPickerCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SmsPickerCoordinator.kt\ncom/squareup/ui/login/SmsPickerCoordinator\n+ 2 Delegates.kt\nkotlin/properties/Delegates\n*L\n1#1,91:1\n33#2,3:92\n*E\n*S KotlinDebug\n*F\n+ 1 SmsPickerCoordinator.kt\ncom/squareup/ui/login/SmsPickerCoordinator\n*L\n34#1,3:92\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001:\u0001 B\u001f\u0012\u0018\u0010\u0002\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u00040\u0003\u00a2\u0006\u0002\u0010\u0007J\u0010\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001eH\u0016J\u0010\u0010\u001f\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001eH\u0002R\u000e\u0010\u0008\u001a\u00020\tX\u0082.\u00a2\u0006\u0002\n\u0000R\u0012\u0010\n\u001a\u00060\u000bR\u00020\u0000X\u0082\u0004\u00a2\u0006\u0002\n\u0000R \u0010\u0002\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u00040\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R7\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\r2\u000c\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\r8B@BX\u0082\u008e\u0002\u00a2\u0006\u0012\n\u0004\u0008\u0014\u0010\u0015\u001a\u0004\u0008\u0010\u0010\u0011\"\u0004\u0008\u0012\u0010\u0013R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u0019X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006!"
    }
    d2 = {
        "Lcom/squareup/ui/login/SmsPickerCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screenData",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/ui/login/AuthenticatorScreen$PickSms;",
        "Lcom/squareup/ui/login/AuthenticatorEvent;",
        "(Lio/reactivex/Observable;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/ActionBarView;",
        "adapter",
        "Lcom/squareup/ui/login/SmsPickerCoordinator$TwoFactorDetailsAdapter;",
        "<set-?>",
        "",
        "Lcom/squareup/protos/multipass/common/TwoFactorDetails;",
        "smsTwoFactorDetails",
        "getSmsTwoFactorDetails",
        "()Ljava/util/List;",
        "setSmsTwoFactorDetails",
        "(Ljava/util/List;)V",
        "smsTwoFactorDetails$delegate",
        "Lkotlin/properties/ReadWriteProperty;",
        "smsTwoFactorDetailsContainer",
        "Landroid/widget/ListView;",
        "subtitle",
        "Lcom/squareup/widgets/MessageView;",
        "title",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "TwoFactorDetailsAdapter",
        "authenticator-views_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/ActionBarView;

.field private final adapter:Lcom/squareup/ui/login/SmsPickerCoordinator$TwoFactorDetailsAdapter;

.field private final screenData:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/login/AuthenticatorScreen$PickSms;",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;>;"
        }
    .end annotation
.end field

.field private final smsTwoFactorDetails$delegate:Lkotlin/properties/ReadWriteProperty;

.field private smsTwoFactorDetailsContainer:Landroid/widget/ListView;

.field private subtitle:Lcom/squareup/widgets/MessageView;

.field private title:Lcom/squareup/widgets/MessageView;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lkotlin/jvm/internal/MutablePropertyReference1Impl;

    const-class v2, Lcom/squareup/ui/login/SmsPickerCoordinator;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    const-string v3, "smsTwoFactorDetails"

    const-string v4, "getSmsTwoFactorDetails()Ljava/util/List;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/jvm/internal/MutablePropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->mutableProperty1(Lkotlin/jvm/internal/MutablePropertyReference1;)Lkotlin/reflect/KMutableProperty1;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/ui/login/SmsPickerCoordinator;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>(Lio/reactivex/Observable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/login/AuthenticatorScreen$PickSms;",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "screenData"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/login/SmsPickerCoordinator;->screenData:Lio/reactivex/Observable;

    .line 34
    sget-object p1, Lkotlin/properties/Delegates;->INSTANCE:Lkotlin/properties/Delegates;

    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p1

    .line 92
    new-instance v0, Lcom/squareup/ui/login/SmsPickerCoordinator$$special$$inlined$observable$1;

    invoke-direct {v0, p1, p1, p0}, Lcom/squareup/ui/login/SmsPickerCoordinator$$special$$inlined$observable$1;-><init>(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/ui/login/SmsPickerCoordinator;)V

    check-cast v0, Lkotlin/properties/ReadWriteProperty;

    .line 94
    iput-object v0, p0, Lcom/squareup/ui/login/SmsPickerCoordinator;->smsTwoFactorDetails$delegate:Lkotlin/properties/ReadWriteProperty;

    .line 38
    new-instance p1, Lcom/squareup/ui/login/SmsPickerCoordinator$TwoFactorDetailsAdapter;

    invoke-direct {p1, p0}, Lcom/squareup/ui/login/SmsPickerCoordinator$TwoFactorDetailsAdapter;-><init>(Lcom/squareup/ui/login/SmsPickerCoordinator;)V

    iput-object p1, p0, Lcom/squareup/ui/login/SmsPickerCoordinator;->adapter:Lcom/squareup/ui/login/SmsPickerCoordinator$TwoFactorDetailsAdapter;

    return-void
.end method

.method public static final synthetic access$getActionBar$p(Lcom/squareup/ui/login/SmsPickerCoordinator;)Lcom/squareup/marin/widgets/ActionBarView;
    .locals 1

    .line 26
    iget-object p0, p0, Lcom/squareup/ui/login/SmsPickerCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    if-nez p0, :cond_0

    const-string v0, "actionBar"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getAdapter$p(Lcom/squareup/ui/login/SmsPickerCoordinator;)Lcom/squareup/ui/login/SmsPickerCoordinator$TwoFactorDetailsAdapter;
    .locals 0

    .line 26
    iget-object p0, p0, Lcom/squareup/ui/login/SmsPickerCoordinator;->adapter:Lcom/squareup/ui/login/SmsPickerCoordinator$TwoFactorDetailsAdapter;

    return-object p0
.end method

.method public static final synthetic access$getSmsTwoFactorDetails$p(Lcom/squareup/ui/login/SmsPickerCoordinator;)Ljava/util/List;
    .locals 0

    .line 26
    invoke-direct {p0}, Lcom/squareup/ui/login/SmsPickerCoordinator;->getSmsTwoFactorDetails()Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getSmsTwoFactorDetailsContainer$p(Lcom/squareup/ui/login/SmsPickerCoordinator;)Landroid/widget/ListView;
    .locals 1

    .line 26
    iget-object p0, p0, Lcom/squareup/ui/login/SmsPickerCoordinator;->smsTwoFactorDetailsContainer:Landroid/widget/ListView;

    if-nez p0, :cond_0

    const-string v0, "smsTwoFactorDetailsContainer"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$setActionBar$p(Lcom/squareup/ui/login/SmsPickerCoordinator;Lcom/squareup/marin/widgets/ActionBarView;)V
    .locals 0

    .line 26
    iput-object p1, p0, Lcom/squareup/ui/login/SmsPickerCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    return-void
.end method

.method public static final synthetic access$setSmsTwoFactorDetails$p(Lcom/squareup/ui/login/SmsPickerCoordinator;Ljava/util/List;)V
    .locals 0

    .line 26
    invoke-direct {p0, p1}, Lcom/squareup/ui/login/SmsPickerCoordinator;->setSmsTwoFactorDetails(Ljava/util/List;)V

    return-void
.end method

.method public static final synthetic access$setSmsTwoFactorDetailsContainer$p(Lcom/squareup/ui/login/SmsPickerCoordinator;Landroid/widget/ListView;)V
    .locals 0

    .line 26
    iput-object p1, p0, Lcom/squareup/ui/login/SmsPickerCoordinator;->smsTwoFactorDetailsContainer:Landroid/widget/ListView;

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 1

    .line 66
    sget v0, Lcom/squareup/common/authenticatorviews/R$id;->stable_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    iput-object v0, p0, Lcom/squareup/ui/login/SmsPickerCoordinator;->actionBar:Lcom/squareup/marin/widgets/ActionBarView;

    .line 67
    sget v0, Lcom/squareup/common/authenticatorviews/R$id;->title:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/login/SmsPickerCoordinator;->title:Lcom/squareup/widgets/MessageView;

    .line 68
    sget v0, Lcom/squareup/marin/R$id;->subtitle:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/login/SmsPickerCoordinator;->subtitle:Lcom/squareup/widgets/MessageView;

    .line 69
    sget v0, Lcom/squareup/common/authenticatorviews/R$id;->row_container:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ListView;

    iput-object p1, p0, Lcom/squareup/ui/login/SmsPickerCoordinator;->smsTwoFactorDetailsContainer:Landroid/widget/ListView;

    return-void
.end method

.method private final getSmsTwoFactorDetails()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/multipass/common/TwoFactorDetails;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ui/login/SmsPickerCoordinator;->smsTwoFactorDetails$delegate:Lkotlin/properties/ReadWriteProperty;

    sget-object v1, Lcom/squareup/ui/login/SmsPickerCoordinator;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1}, Lkotlin/properties/ReadWriteProperty;->getValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method private final setSmsTwoFactorDetails(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/multipass/common/TwoFactorDetails;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ui/login/SmsPickerCoordinator;->smsTwoFactorDetails$delegate:Lkotlin/properties/ReadWriteProperty;

    sget-object v1, Lcom/squareup/ui/login/SmsPickerCoordinator;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-interface {v0, p0, v1, p1}, Lkotlin/properties/ReadWriteProperty;->setValue(Ljava/lang/Object;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 2

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    invoke-direct {p0, p1}, Lcom/squareup/ui/login/SmsPickerCoordinator;->bindViews(Landroid/view/View;)V

    .line 43
    iget-object v0, p0, Lcom/squareup/ui/login/SmsPickerCoordinator;->title:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_0

    const-string v1, "title"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    sget v1, Lcom/squareup/common/authenticatorviews/R$string;->two_factor_sms_picker_title:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(I)V

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/login/SmsPickerCoordinator;->subtitle:Lcom/squareup/widgets/MessageView;

    if-nez v0, :cond_1

    const-string v1, "subtitle"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    sget v1, Lcom/squareup/common/authenticatorviews/R$string;->two_factor_sms_picker_subtitle:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(I)V

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/login/SmsPickerCoordinator;->smsTwoFactorDetailsContainer:Landroid/widget/ListView;

    if-nez v0, :cond_2

    const-string v1, "smsTwoFactorDetailsContainer"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    iget-object v1, p0, Lcom/squareup/ui/login/SmsPickerCoordinator;->adapter:Lcom/squareup/ui/login/SmsPickerCoordinator$TwoFactorDetailsAdapter;

    check-cast v1, Landroid/widget/ListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/login/SmsPickerCoordinator;->screenData:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/ui/login/SmsPickerCoordinator$attach$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/login/SmsPickerCoordinator$attach$1;-><init>(Lcom/squareup/ui/login/SmsPickerCoordinator;Landroid/view/View;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    return-void
.end method
