.class public final Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$Factory;
.super Ljava/lang/Object;
.source "EmailPasswordLoginCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\'\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ \u0010\u000b\u001a\u00020\u000c2\u0018\u0010\r\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u00110\u000f0\u000eR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$Factory;",
        "",
        "res",
        "Lcom/squareup/util/Res;",
        "device",
        "Lcom/squareup/util/Device;",
        "glassSpinner",
        "Lcom/squareup/register/widgets/GlassSpinner;",
        "postInstallEncryptedEmail",
        "Lcom/squareup/ui/login/PostInstallEncryptedEmail;",
        "(Lcom/squareup/util/Res;Lcom/squareup/util/Device;Lcom/squareup/register/widgets/GlassSpinner;Lcom/squareup/ui/login/PostInstallEncryptedEmail;)V",
        "create",
        "Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;",
        "screenData",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;",
        "Lcom/squareup/ui/login/AuthenticatorEvent;",
        "authenticator-views_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final device:Lcom/squareup/util/Device;

.field private final glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

.field private final postInstallEncryptedEmail:Lcom/squareup/ui/login/PostInstallEncryptedEmail;

.field private final res:Lcom/squareup/util/Res;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/util/Device;Lcom/squareup/register/widgets/GlassSpinner;Lcom/squareup/ui/login/PostInstallEncryptedEmail;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "device"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "glassSpinner"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "postInstallEncryptedEmail"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$Factory;->res:Lcom/squareup/util/Res;

    iput-object p2, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$Factory;->device:Lcom/squareup/util/Device;

    iput-object p3, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$Factory;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    iput-object p4, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$Factory;->postInstallEncryptedEmail:Lcom/squareup/ui/login/PostInstallEncryptedEmail;

    return-void
.end method


# virtual methods
.method public final create(Lio/reactivex/Observable;)Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/login/AuthenticatorScreen$EmailPassword;",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;>;)",
            "Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;"
        }
    .end annotation

    const-string v0, "screenData"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    new-instance v0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;

    .line 64
    iget-object v2, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$Factory;->res:Lcom/squareup/util/Res;

    iget-object v3, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$Factory;->device:Lcom/squareup/util/Device;

    iget-object v4, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$Factory;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    iget-object v5, p0, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$Factory;->postInstallEncryptedEmail:Lcom/squareup/ui/login/PostInstallEncryptedEmail;

    move-object v1, v0

    move-object v6, p1

    .line 63
    invoke-direct/range {v1 .. v6}, Lcom/squareup/ui/login/EmailPasswordLoginCoordinator;-><init>(Lcom/squareup/util/Res;Lcom/squareup/util/Device;Lcom/squareup/register/widgets/GlassSpinner;Lcom/squareup/ui/login/PostInstallEncryptedEmail;Lio/reactivex/Observable;)V

    return-object v0
.end method
