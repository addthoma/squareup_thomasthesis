.class final Lcom/squareup/ui/login/EnrollSmsCoordinator$attach$1$1;
.super Lkotlin/jvm/internal/Lambda;
.source "EnrollSmsCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/login/EnrollSmsCoordinator$attach$1;->invoke(Lcom/squareup/workflow/legacy/Screen;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u000b\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "maybeEnroll",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

.field final synthetic this$0:Lcom/squareup/ui/login/EnrollSmsCoordinator$attach$1;


# direct methods
.method constructor <init>(Lcom/squareup/ui/login/EnrollSmsCoordinator$attach$1;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/login/EnrollSmsCoordinator$attach$1$1;->this$0:Lcom/squareup/ui/login/EnrollSmsCoordinator$attach$1;

    iput-object p2, p0, Lcom/squareup/ui/login/EnrollSmsCoordinator$attach$1$1;->$workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 35
    invoke-virtual {p0}, Lcom/squareup/ui/login/EnrollSmsCoordinator$attach$1$1;->invoke()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final invoke()Z
    .locals 3

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/login/EnrollSmsCoordinator$attach$1$1;->this$0:Lcom/squareup/ui/login/EnrollSmsCoordinator$attach$1;

    iget-object v0, v0, Lcom/squareup/ui/login/EnrollSmsCoordinator$attach$1;->this$0:Lcom/squareup/ui/login/EnrollSmsCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/login/EnrollSmsCoordinator;->access$isPhoneNumberValid$p(Lcom/squareup/ui/login/EnrollSmsCoordinator;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/login/EnrollSmsCoordinator$attach$1$1;->$workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    new-instance v1, Lcom/squareup/ui/login/AuthenticatorEvent$EnrollInSms;

    iget-object v2, p0, Lcom/squareup/ui/login/EnrollSmsCoordinator$attach$1$1;->this$0:Lcom/squareup/ui/login/EnrollSmsCoordinator$attach$1;

    iget-object v2, v2, Lcom/squareup/ui/login/EnrollSmsCoordinator$attach$1;->this$0:Lcom/squareup/ui/login/EnrollSmsCoordinator;

    invoke-static {v2}, Lcom/squareup/ui/login/EnrollSmsCoordinator;->access$getPhoneNumber$p(Lcom/squareup/ui/login/EnrollSmsCoordinator;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/ui/login/AuthenticatorEvent$EnrollInSms;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    const/4 v0, 0x1

    return v0

    .line 74
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/login/EnrollSmsCoordinator$attach$1$1;->this$0:Lcom/squareup/ui/login/EnrollSmsCoordinator$attach$1;

    iget-object v0, v0, Lcom/squareup/ui/login/EnrollSmsCoordinator$attach$1;->this$0:Lcom/squareup/ui/login/EnrollSmsCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/login/EnrollSmsCoordinator;->access$indicatePhoneNumberError(Lcom/squareup/ui/login/EnrollSmsCoordinator;)V

    const/4 v0, 0x0

    return v0
.end method
