.class public final Lcom/squareup/ui/login/CreateAccountPresenter$confirmationPopupPresenter$1;
.super Lcom/squareup/mortar/PopupPresenter;
.source "CreateAccountPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/login/CreateAccountPresenter;-><init>(Lcom/squareup/location/CountryCodeGuesser;Lcom/squareup/ui/login/CreateAccountHelper;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;Landroid/accounts/AccountManager;Lflow/Flow;Lcom/squareup/ui/loggedout/LoggedOutScopeRunner;Lcom/squareup/ui/login/CreateAccountScreen$Runner;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/mortar/PopupPresenter<",
        "Lcom/squareup/register/widgets/Confirmation;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0003*\u0001\u0000\u0008\n\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001J\u0017\u0010\u0004\u001a\u00020\u00052\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0003H\u0014\u00a2\u0006\u0002\u0010\u0007\u00a8\u0006\u0008"
    }
    d2 = {
        "com/squareup/ui/login/CreateAccountPresenter$confirmationPopupPresenter$1",
        "Lcom/squareup/mortar/PopupPresenter;",
        "Lcom/squareup/register/widgets/Confirmation;",
        "",
        "onPopupResult",
        "",
        "result",
        "(Ljava/lang/Boolean;)V",
        "loggedout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/login/CreateAccountPresenter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/login/CreateAccountPresenter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 73
    iput-object p1, p0, Lcom/squareup/ui/login/CreateAccountPresenter$confirmationPopupPresenter$1;->this$0:Lcom/squareup/ui/login/CreateAccountPresenter;

    invoke-direct {p0}, Lcom/squareup/mortar/PopupPresenter;-><init>()V

    return-void
.end method


# virtual methods
.method protected onPopupResult(Ljava/lang/Boolean;)V
    .locals 1

    .line 75
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/login/CreateAccountPresenter$confirmationPopupPresenter$1;->this$0:Lcom/squareup/ui/login/CreateAccountPresenter;

    invoke-static {p1}, Lcom/squareup/ui/login/CreateAccountPresenter;->access$doCreate(Lcom/squareup/ui/login/CreateAccountPresenter;)V

    :cond_0
    return-void
.end method

.method public bridge synthetic onPopupResult(Ljava/lang/Object;)V
    .locals 0

    .line 73
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/login/CreateAccountPresenter$confirmationPopupPresenter$1;->onPopupResult(Ljava/lang/Boolean;)V

    return-void
.end method
