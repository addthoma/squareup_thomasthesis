.class public final Lcom/squareup/ui/login/ForgotPasswordFailedDialogFactory;
.super Ljava/lang/Object;
.source "ForgotPasswordFailedDialogFactory.kt"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001B\u001f\u0012\u0018\u0010\u0002\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u00040\u0003\u00a2\u0006\u0002\u0010\u0007J\u0018\u0010\u0008\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\n0\t2\u0006\u0010\u000b\u001a\u00020\u000cH\u0016J2\u0010\r\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\u000e\u001a\u00020\u00052\u0018\u0010\u000f\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u00040\u0003H\u0002R \u0010\u0002\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u00040\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/ui/login/ForgotPasswordFailedDialogFactory;",
        "Lcom/squareup/workflow/DialogFactory;",
        "screenData",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/ui/login/AuthenticatorScreen$ForgotPasswordFailed;",
        "Lcom/squareup/ui/login/AuthenticatorEvent;",
        "(Lio/reactivex/Observable;)V",
        "create",
        "Lio/reactivex/Single;",
        "Landroid/app/Dialog;",
        "context",
        "Landroid/content/Context;",
        "createDialog",
        "screen",
        "inputs",
        "authenticator-views_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final screenData:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/login/AuthenticatorScreen$ForgotPasswordFailed;",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lio/reactivex/Observable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/login/AuthenticatorScreen$ForgotPasswordFailed;",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "screenData"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/login/ForgotPasswordFailedDialogFactory;->screenData:Lio/reactivex/Observable;

    return-void
.end method

.method public static final synthetic access$createDialog(Lcom/squareup/ui/login/ForgotPasswordFailedDialogFactory;Landroid/content/Context;Lcom/squareup/ui/login/AuthenticatorScreen$ForgotPasswordFailed;Lio/reactivex/Observable;)Landroid/app/Dialog;
    .locals 0

    .line 14
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/ui/login/ForgotPasswordFailedDialogFactory;->createDialog(Landroid/content/Context;Lcom/squareup/ui/login/AuthenticatorScreen$ForgotPasswordFailed;Lio/reactivex/Observable;)Landroid/app/Dialog;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getScreenData$p(Lcom/squareup/ui/login/ForgotPasswordFailedDialogFactory;)Lio/reactivex/Observable;
    .locals 0

    .line 14
    iget-object p0, p0, Lcom/squareup/ui/login/ForgotPasswordFailedDialogFactory;->screenData:Lio/reactivex/Observable;

    return-object p0
.end method

.method private final createDialog(Landroid/content/Context;Lcom/squareup/ui/login/AuthenticatorScreen$ForgotPasswordFailed;Lio/reactivex/Observable;)Landroid/app/Dialog;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/squareup/ui/login/AuthenticatorScreen$ForgotPasswordFailed;",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/login/AuthenticatorScreen$ForgotPasswordFailed;",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;>;)",
            "Landroid/app/Dialog;"
        }
    .end annotation

    .line 27
    invoke-virtual {p2}, Lcom/squareup/ui/login/AuthenticatorScreen$ForgotPasswordFailed;->getTitle()Ljava/lang/String;

    move-result-object v0

    .line 28
    invoke-virtual {p2}, Lcom/squareup/ui/login/AuthenticatorScreen$ForgotPasswordFailed;->getBody()Ljava/lang/String;

    move-result-object p2

    .line 29
    sget v1, Lcom/squareup/common/strings/R$string;->dismiss:I

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 30
    sget v2, Lcom/squareup/common/strings/R$string;->retry:I

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 26
    invoke-static {v0, p2, v1, v2}, Lcom/squareup/register/widgets/FailureAlertDialogFactory;->retry(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/register/widgets/FailureAlertDialogFactory;

    move-result-object p2

    .line 33
    new-instance v0, Lcom/squareup/ui/login/ForgotPasswordFailedDialogFactory$createDialog$1;

    invoke-direct {v0, p3}, Lcom/squareup/ui/login/ForgotPasswordFailedDialogFactory$createDialog$1;-><init>(Lio/reactivex/Observable;)V

    .line 37
    new-instance v1, Lcom/squareup/ui/login/ForgotPasswordFailedDialogFactory$createDialog$2;

    invoke-direct {v1, p3}, Lcom/squareup/ui/login/ForgotPasswordFailedDialogFactory$createDialog$2;-><init>(Lio/reactivex/Observable;)V

    .line 41
    new-instance p3, Lcom/squareup/ui/login/ForgotPasswordFailedDialogFactory$createDialog$3;

    invoke-direct {p3, v0}, Lcom/squareup/ui/login/ForgotPasswordFailedDialogFactory$createDialog$3;-><init>(Lcom/squareup/ui/login/ForgotPasswordFailedDialogFactory$createDialog$1;)V

    check-cast p3, Lkotlin/jvm/functions/Function0;

    new-instance v0, Lcom/squareup/ui/login/ForgotPasswordFailedDialogFactory$sam$java_lang_Runnable$0;

    invoke-direct {v0, p3}, Lcom/squareup/ui/login/ForgotPasswordFailedDialogFactory$sam$java_lang_Runnable$0;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v0, Ljava/lang/Runnable;

    new-instance p3, Lcom/squareup/ui/login/ForgotPasswordFailedDialogFactory$createDialog$4;

    invoke-direct {p3, v1}, Lcom/squareup/ui/login/ForgotPasswordFailedDialogFactory$createDialog$4;-><init>(Lcom/squareup/ui/login/ForgotPasswordFailedDialogFactory$createDialog$2;)V

    check-cast p3, Lkotlin/jvm/functions/Function0;

    new-instance v1, Lcom/squareup/ui/login/ForgotPasswordFailedDialogFactory$sam$java_lang_Runnable$0;

    invoke-direct {v1, p3}, Lcom/squareup/ui/login/ForgotPasswordFailedDialogFactory$sam$java_lang_Runnable$0;-><init>(Lkotlin/jvm/functions/Function0;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-virtual {p2, p1, v0, v1}, Lcom/squareup/register/widgets/FailureAlertDialogFactory;->createFailureAlertDialog(Landroid/content/Context;Ljava/lang/Runnable;Ljava/lang/Runnable;)Landroid/app/AlertDialog;

    move-result-object p1

    const-string p2, "factory.createFailureAle\u2026onDismissed, ::onRetried)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Landroid/app/Dialog;

    return-object p1
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "+",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    iget-object v0, p0, Lcom/squareup/ui/login/ForgotPasswordFailedDialogFactory;->screenData:Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/ui/login/ForgotPasswordFailedDialogFactory$create$1;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/login/ForgotPasswordFailedDialogFactory$create$1;-><init>(Lcom/squareup/ui/login/ForgotPasswordFailedDialogFactory;Landroid/content/Context;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    .line 19
    invoke-virtual {p1}, Lio/reactivex/Observable;->firstOrError()Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "screenData.map { (screen\u2026}\n        .firstOrError()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method
