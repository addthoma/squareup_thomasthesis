.class public final Lcom/squareup/ui/login/AuthenticatorScreenRendering$ProvidedDeviceCodeAuthRendering;
.super Lcom/squareup/ui/login/AuthenticatorScreenRendering;
.source "AuthenticatorRendering.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/login/AuthenticatorScreenRendering;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ProvidedDeviceCodeAuthRendering"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/login/AuthenticatorScreenRendering$ProvidedDeviceCodeAuthRendering$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008\u0086\u0008\u0018\u0000 \u00152\u00020\u0001:\u0001\u0015B\u0015\u0012\u000e\u0008\u0002\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0002\u0010\u0005J\u000f\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0003J\u0019\u0010\t\u001a\u00020\u00002\u000e\u0008\u0002\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0001J\u0013\u0010\n\u001a\u00020\u000b2\u0008\u0010\u000c\u001a\u0004\u0018\u00010\rH\u00d6\u0003J\t\u0010\u000e\u001a\u00020\u000fH\u00d6\u0001J\u0012\u0010\u0010\u001a\u000e\u0012\u0004\u0012\u00020\u0012\u0012\u0004\u0012\u00020\u00040\u0011J\t\u0010\u0013\u001a\u00020\u0014H\u00d6\u0001R\u0017\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/ui/login/AuthenticatorScreenRendering$ProvidedDeviceCodeAuthRendering;",
        "Lcom/squareup/ui/login/AuthenticatorScreenRendering;",
        "input",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "Lcom/squareup/ui/login/AuthenticatorEvent;",
        "(Lcom/squareup/workflow/legacy/WorkflowInput;)V",
        "getInput",
        "()Lcom/squareup/workflow/legacy/WorkflowInput;",
        "component1",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "",
        "toScreen",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/ui/login/AuthenticatorScreen$ProvidedDeviceCodeAuth;",
        "toString",
        "",
        "Companion",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ui/login/AuthenticatorScreenRendering$ProvidedDeviceCodeAuthRendering$Companion;

.field private static final KEY:Lcom/squareup/workflow/legacy/Screen$Key;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/Screen$Key<",
            "Lcom/squareup/ui/login/AuthenticatorScreen$ProvidedDeviceCodeAuth;",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final input:Lcom/squareup/workflow/legacy/WorkflowInput;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$ProvidedDeviceCodeAuthRendering$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/login/AuthenticatorScreenRendering$ProvidedDeviceCodeAuthRendering$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$ProvidedDeviceCodeAuthRendering;->Companion:Lcom/squareup/ui/login/AuthenticatorScreenRendering$ProvidedDeviceCodeAuthRendering$Companion;

    .line 92
    new-instance v0, Lcom/squareup/workflow/legacy/Screen$Key;

    sget-object v1, Lcom/squareup/ui/login/AuthenticatorScreenRendering$ProvidedDeviceCodeAuthRendering;->Companion:Lcom/squareup/ui/login/AuthenticatorScreenRendering$ProvidedDeviceCodeAuthRendering$Companion;

    invoke-direct {v0, v1}, Lcom/squareup/workflow/legacy/Screen$Key;-><init>(Ljava/lang/Object;)V

    sput-object v0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$ProvidedDeviceCodeAuthRendering;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1, v0}, Lcom/squareup/ui/login/AuthenticatorScreenRendering$ProvidedDeviceCodeAuthRendering;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;)V"
        }
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 87
    invoke-direct {p0, v0}, Lcom/squareup/ui/login/AuthenticatorScreenRendering;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$ProvidedDeviceCodeAuthRendering;->input:Lcom/squareup/workflow/legacy/WorkflowInput;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/squareup/workflow/legacy/WorkflowInput;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    .line 86
    sget-object p1, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {p1}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p1

    :cond_0
    invoke-direct {p0, p1}, Lcom/squareup/ui/login/AuthenticatorScreenRendering$ProvidedDeviceCodeAuthRendering;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-void
.end method

.method public static final synthetic access$getKEY$cp()Lcom/squareup/workflow/legacy/Screen$Key;
    .locals 1

    .line 85
    sget-object v0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$ProvidedDeviceCodeAuthRendering;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    return-object v0
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/login/AuthenticatorScreenRendering$ProvidedDeviceCodeAuthRendering;Lcom/squareup/workflow/legacy/WorkflowInput;ILjava/lang/Object;)Lcom/squareup/ui/login/AuthenticatorScreenRendering$ProvidedDeviceCodeAuthRendering;
    .locals 0

    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$ProvidedDeviceCodeAuthRendering;->input:Lcom/squareup/workflow/legacy/WorkflowInput;

    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/ui/login/AuthenticatorScreenRendering$ProvidedDeviceCodeAuthRendering;->copy(Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/ui/login/AuthenticatorScreenRendering$ProvidedDeviceCodeAuthRendering;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Lcom/squareup/workflow/legacy/WorkflowInput;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$ProvidedDeviceCodeAuthRendering;->input:Lcom/squareup/workflow/legacy/WorkflowInput;

    return-object v0
.end method

.method public final copy(Lcom/squareup/workflow/legacy/WorkflowInput;)Lcom/squareup/ui/login/AuthenticatorScreenRendering$ProvidedDeviceCodeAuthRendering;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;)",
            "Lcom/squareup/ui/login/AuthenticatorScreenRendering$ProvidedDeviceCodeAuthRendering;"
        }
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$ProvidedDeviceCodeAuthRendering;

    invoke-direct {v0, p1}, Lcom/squareup/ui/login/AuthenticatorScreenRendering$ProvidedDeviceCodeAuthRendering;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/login/AuthenticatorScreenRendering$ProvidedDeviceCodeAuthRendering;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/login/AuthenticatorScreenRendering$ProvidedDeviceCodeAuthRendering;

    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$ProvidedDeviceCodeAuthRendering;->input:Lcom/squareup/workflow/legacy/WorkflowInput;

    iget-object p1, p1, Lcom/squareup/ui/login/AuthenticatorScreenRendering$ProvidedDeviceCodeAuthRendering;->input:Lcom/squareup/workflow/legacy/WorkflowInput;

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getInput()Lcom/squareup/workflow/legacy/WorkflowInput;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;"
        }
    .end annotation

    .line 86
    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$ProvidedDeviceCodeAuthRendering;->input:Lcom/squareup/workflow/legacy/WorkflowInput;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$ProvidedDeviceCodeAuthRendering;->input:Lcom/squareup/workflow/legacy/WorkflowInput;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final toScreen()Lcom/squareup/workflow/legacy/Screen;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/login/AuthenticatorScreen$ProvidedDeviceCodeAuth;",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;"
        }
    .end annotation

    .line 89
    new-instance v0, Lcom/squareup/workflow/legacy/Screen;

    sget-object v1, Lcom/squareup/ui/login/AuthenticatorScreenRendering$ProvidedDeviceCodeAuthRendering;->KEY:Lcom/squareup/workflow/legacy/Screen$Key;

    sget-object v2, Lcom/squareup/ui/login/AuthenticatorScreen$ProvidedDeviceCodeAuth;->INSTANCE:Lcom/squareup/ui/login/AuthenticatorScreen$ProvidedDeviceCodeAuth;

    iget-object v3, p0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$ProvidedDeviceCodeAuthRendering;->input:Lcom/squareup/workflow/legacy/WorkflowInput;

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ProvidedDeviceCodeAuthRendering(input="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$ProvidedDeviceCodeAuthRendering;->input:Lcom/squareup/workflow/legacy/WorkflowInput;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
