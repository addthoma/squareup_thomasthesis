.class final Lcom/squareup/ui/login/RealAuthenticator$getObservableForCall$3;
.super Ljava/lang/Object;
.source "RealAuthenticator.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/login/RealAuthenticator;->getObservableForCall(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorInput;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u0012\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001j\u0002`\u00042\u001a\u0010\u0005\u001a\u0016\u0012\u0004\u0012\u00020\u0007 \u0008*\n\u0012\u0004\u0012\u00020\u0007\u0018\u00010\u00060\u0006H\n\u00a2\u0006\u0002\u0008\t"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/ui/login/AuthenticatorState;",
        "Lcom/squareup/ui/login/AuthenticatorOutput;",
        "Lcom/squareup/ui/login/AuthUpdate;",
        "it",
        "Lcom/squareup/ui/login/AuthenticationCallResult;",
        "Lcom/squareup/protos/register/api/LoginResponse;",
        "kotlin.jvm.PlatformType",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $operationType:Lcom/squareup/ui/login/OperationType;

.field final synthetic $this_getObservableForCall:Lcom/squareup/ui/login/AuthenticatorState;

.field final synthetic this$0:Lcom/squareup/ui/login/RealAuthenticator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/login/RealAuthenticator;Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/OperationType;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/login/RealAuthenticator$getObservableForCall$3;->this$0:Lcom/squareup/ui/login/RealAuthenticator;

    iput-object p2, p0, Lcom/squareup/ui/login/RealAuthenticator$getObservableForCall$3;->$this_getObservableForCall:Lcom/squareup/ui/login/AuthenticatorState;

    iput-object p3, p0, Lcom/squareup/ui/login/RealAuthenticator$getObservableForCall$3;->$operationType:Lcom/squareup/ui/login/OperationType;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/ui/login/AuthenticationCallResult;)Lcom/squareup/workflow/WorkflowAction;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/login/AuthenticationCallResult<",
            "Lcom/squareup/protos/register/api/LoginResponse;",
            ">;)",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/login/AuthenticatorState;",
            "Lcom/squareup/ui/login/AuthenticatorOutput;",
            ">;"
        }
    .end annotation

    .line 401
    iget-object v0, p0, Lcom/squareup/ui/login/RealAuthenticator$getObservableForCall$3;->this$0:Lcom/squareup/ui/login/RealAuthenticator;

    iget-object v1, p0, Lcom/squareup/ui/login/RealAuthenticator$getObservableForCall$3;->$this_getObservableForCall:Lcom/squareup/ui/login/AuthenticatorState;

    iget-object v2, p0, Lcom/squareup/ui/login/RealAuthenticator$getObservableForCall$3;->$operationType:Lcom/squareup/ui/login/OperationType;

    check-cast v2, Lcom/squareup/ui/login/OperationType$LoginWithDeviceCode;

    invoke-virtual {v2}, Lcom/squareup/ui/login/OperationType$LoginWithDeviceCode;->getRequest()Lcom/squareup/protos/register/api/LoginRequest;

    move-result-object v2

    const-string v3, "it"

    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2, p1}, Lcom/squareup/ui/login/RealAuthenticator;->deviceCodeLoginCallback$impl_release(Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/protos/register/api/LoginRequest;Lcom/squareup/ui/login/AuthenticationCallResult;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 157
    check-cast p1, Lcom/squareup/ui/login/AuthenticationCallResult;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/login/RealAuthenticator$getObservableForCall$3;->apply(Lcom/squareup/ui/login/AuthenticationCallResult;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
