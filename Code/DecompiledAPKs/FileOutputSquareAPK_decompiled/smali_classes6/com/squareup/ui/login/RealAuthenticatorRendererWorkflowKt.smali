.class public final Lcom/squareup/ui/login/RealAuthenticatorRendererWorkflowKt;
.super Ljava/lang/Object;
.source "RealAuthenticatorRendererWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRealAuthenticatorRendererWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RealAuthenticatorRendererWorkflow.kt\ncom/squareup/ui/login/RealAuthenticatorRendererWorkflowKt\n*L\n1#1,113:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u001aP\u0010\u0000\u001a$\u0012\u0004\u0012\u00020\u0002\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0003j\u0002`\u00040\u0001j\u0008\u0012\u0004\u0012\u00020\u0002`\u00052\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u00072\u0008\u0008\u0002\u0010\t\u001a\u00020\n2\u000e\u0008\u0002\u0010\u000b\u001a\u0008\u0012\u0004\u0012\u00020\r0\u000c\u001a.\u0010\u0000\u001a$\u0012\u0004\u0012\u00020\u0002\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0003j\u0002`\u00040\u0001j\u0008\u0012\u0004\u0012\u00020\u0002`\u0005*\u00020\u0008H\u0002\u00a8\u0006\u000e"
    }
    d2 = {
        "toLayeredScreen",
        "",
        "Lcom/squareup/workflow/MainAndModal;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "screens",
        "",
        "Lcom/squareup/ui/login/AuthenticatorScreenRendering;",
        "operation",
        "Lcom/squareup/ui/login/Operation;",
        "workflow",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "Lcom/squareup/ui/login/AuthenticatorEvent;",
        "authenticator-views_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private static final toLayeredScreen(Lcom/squareup/ui/login/AuthenticatorScreenRendering;)Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/login/AuthenticatorScreenRendering;",
            ")",
            "Ljava/util/Map<",
            "Lcom/squareup/workflow/MainAndModal;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    .line 96
    instance-of v0, p0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$EmailPasswordRendering;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/workflow/MainAndModal;->Companion:Lcom/squareup/workflow/MainAndModal$Companion;

    check-cast p0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$EmailPasswordRendering;

    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorScreenRendering$EmailPasswordRendering;->toScreen()Lcom/squareup/workflow/legacy/Screen;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/workflow/MainAndModal$Companion;->onlyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p0

    goto/16 :goto_0

    .line 97
    :cond_0
    instance-of v0, p0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$ForgotPasswordRendering;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/workflow/MainAndModal;->Companion:Lcom/squareup/workflow/MainAndModal$Companion;

    check-cast p0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$ForgotPasswordRendering;

    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorScreenRendering$ForgotPasswordRendering;->toScreen()Lcom/squareup/workflow/legacy/Screen;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/workflow/MainAndModal$Companion;->onlyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p0

    goto/16 :goto_0

    .line 98
    :cond_1
    instance-of v0, p0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$ForgotPasswordFailedRendering;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/workflow/MainAndModal;->Companion:Lcom/squareup/workflow/MainAndModal$Companion;

    check-cast p0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$ForgotPasswordFailedRendering;

    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorScreenRendering$ForgotPasswordFailedRendering;->toScreen()Lcom/squareup/workflow/legacy/Screen;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/workflow/MainAndModal$Companion;->partial(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p0

    goto/16 :goto_0

    .line 99
    :cond_2
    instance-of v0, p0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$DeviceCodeRendering;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/workflow/MainAndModal;->Companion:Lcom/squareup/workflow/MainAndModal$Companion;

    check-cast p0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$DeviceCodeRendering;

    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorScreenRendering$DeviceCodeRendering;->toScreen()Lcom/squareup/workflow/legacy/Screen;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/workflow/MainAndModal$Companion;->onlyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p0

    goto/16 :goto_0

    .line 100
    :cond_3
    instance-of v0, p0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$ProvidedDeviceCodeAuthRendering;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/squareup/workflow/MainAndModal;->Companion:Lcom/squareup/workflow/MainAndModal$Companion;

    check-cast p0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$ProvidedDeviceCodeAuthRendering;

    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorScreenRendering$ProvidedDeviceCodeAuthRendering;->toScreen()Lcom/squareup/workflow/legacy/Screen;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/workflow/MainAndModal$Companion;->onlyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p0

    goto/16 :goto_0

    .line 101
    :cond_4
    instance-of v0, p0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$PickUnitRendering;

    if-eqz v0, :cond_5

    sget-object v0, Lcom/squareup/workflow/MainAndModal;->Companion:Lcom/squareup/workflow/MainAndModal$Companion;

    check-cast p0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$PickUnitRendering;

    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorScreenRendering$PickUnitRendering;->toScreen()Lcom/squareup/workflow/legacy/Screen;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/workflow/MainAndModal$Companion;->onlyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p0

    goto/16 :goto_0

    .line 102
    :cond_5
    instance-of v0, p0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$PickMerchantRendering;

    if-eqz v0, :cond_6

    sget-object v0, Lcom/squareup/workflow/MainAndModal;->Companion:Lcom/squareup/workflow/MainAndModal$Companion;

    check-cast p0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$PickMerchantRendering;

    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorScreenRendering$PickMerchantRendering;->toScreen()Lcom/squareup/workflow/legacy/Screen;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/workflow/MainAndModal$Companion;->onlyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p0

    goto/16 :goto_0

    .line 103
    :cond_6
    instance-of v0, p0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$ShowWarningRendering;

    if-eqz v0, :cond_7

    sget-object v0, Lcom/squareup/workflow/MainAndModal;->Companion:Lcom/squareup/workflow/MainAndModal$Companion;

    check-cast p0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$ShowWarningRendering;

    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorScreenRendering$ShowWarningRendering;->toScreen()Lcom/squareup/workflow/legacy/Screen;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/workflow/MainAndModal$Companion;->partial(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p0

    goto/16 :goto_0

    .line 104
    :cond_7
    instance-of v0, p0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$ShowLoginAlertRendering;

    if-eqz v0, :cond_8

    sget-object v0, Lcom/squareup/workflow/MainAndModal;->Companion:Lcom/squareup/workflow/MainAndModal$Companion;

    check-cast p0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$ShowLoginAlertRendering;

    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorScreenRendering$ShowLoginAlertRendering;->toScreen()Lcom/squareup/workflow/legacy/Screen;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/workflow/MainAndModal$Companion;->partial(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p0

    goto/16 :goto_0

    .line 105
    :cond_8
    instance-of v0, p0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$PickTwoFactorMethodRendering;

    if-eqz v0, :cond_9

    sget-object v0, Lcom/squareup/workflow/MainAndModal;->Companion:Lcom/squareup/workflow/MainAndModal$Companion;

    check-cast p0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$PickTwoFactorMethodRendering;

    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorScreenRendering$PickTwoFactorMethodRendering;->toScreen()Lcom/squareup/workflow/legacy/Screen;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/workflow/MainAndModal$Companion;->onlyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p0

    goto :goto_0

    .line 106
    :cond_9
    instance-of v0, p0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$PickSmsRendering;

    if-eqz v0, :cond_a

    sget-object v0, Lcom/squareup/workflow/MainAndModal;->Companion:Lcom/squareup/workflow/MainAndModal$Companion;

    check-cast p0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$PickSmsRendering;

    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorScreenRendering$PickSmsRendering;->toScreen()Lcom/squareup/workflow/legacy/Screen;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/workflow/MainAndModal$Companion;->onlyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p0

    goto :goto_0

    .line 107
    :cond_a
    instance-of v0, p0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$EnrollSmsRendering;

    if-eqz v0, :cond_b

    sget-object v0, Lcom/squareup/workflow/MainAndModal;->Companion:Lcom/squareup/workflow/MainAndModal$Companion;

    check-cast p0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$EnrollSmsRendering;

    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorScreenRendering$EnrollSmsRendering;->toScreen()Lcom/squareup/workflow/legacy/Screen;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/workflow/MainAndModal$Companion;->onlyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p0

    goto :goto_0

    .line 108
    :cond_b
    instance-of v0, p0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$EnrollGoogleAuthQrRendering;

    if-eqz v0, :cond_c

    sget-object v0, Lcom/squareup/workflow/MainAndModal;->Companion:Lcom/squareup/workflow/MainAndModal$Companion;

    check-cast p0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$EnrollGoogleAuthQrRendering;

    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorScreenRendering$EnrollGoogleAuthQrRendering;->toScreen()Lcom/squareup/workflow/legacy/Screen;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/workflow/MainAndModal$Companion;->onlyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p0

    goto :goto_0

    .line 109
    :cond_c
    instance-of v0, p0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$EnrollGoogleAuthCodeRendering;

    if-eqz v0, :cond_d

    sget-object v0, Lcom/squareup/workflow/MainAndModal;->Companion:Lcom/squareup/workflow/MainAndModal$Companion;

    check-cast p0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$EnrollGoogleAuthCodeRendering;

    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorScreenRendering$EnrollGoogleAuthCodeRendering;->toScreen()Lcom/squareup/workflow/legacy/Screen;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/workflow/MainAndModal$Companion;->onlyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p0

    goto :goto_0

    .line 110
    :cond_d
    instance-of v0, p0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$VerifyCodeSmsRendering;

    if-eqz v0, :cond_e

    sget-object v0, Lcom/squareup/workflow/MainAndModal;->Companion:Lcom/squareup/workflow/MainAndModal$Companion;

    check-cast p0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$VerifyCodeSmsRendering;

    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorScreenRendering$VerifyCodeSmsRendering;->toScreen()Lcom/squareup/workflow/legacy/Screen;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/workflow/MainAndModal$Companion;->onlyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p0

    goto :goto_0

    .line 111
    :cond_e
    instance-of v0, p0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$VerifyCodeGoogleAuthRendering;

    if-eqz v0, :cond_f

    sget-object v0, Lcom/squareup/workflow/MainAndModal;->Companion:Lcom/squareup/workflow/MainAndModal$Companion;

    check-cast p0, Lcom/squareup/ui/login/AuthenticatorScreenRendering$VerifyCodeGoogleAuthRendering;

    invoke-virtual {p0}, Lcom/squareup/ui/login/AuthenticatorScreenRendering$VerifyCodeGoogleAuthRendering;->toScreen()Lcom/squareup/workflow/legacy/Screen;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/workflow/MainAndModal$Companion;->onlyScreen(Lcom/squareup/workflow/legacy/Screen;)Ljava/util/Map;

    move-result-object p0

    :goto_0
    return-object p0

    :cond_f
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0
.end method

.method public static final toLayeredScreen(Ljava/util/List;Lcom/squareup/ui/login/Operation;Lcom/squareup/workflow/legacy/WorkflowInput;)Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/ui/login/AuthenticatorScreenRendering;",
            ">;",
            "Lcom/squareup/ui/login/Operation;",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;)",
            "Ljava/util/Map<",
            "Lcom/squareup/workflow/MainAndModal;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "screens"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "operation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "workflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    invoke-static {p0}, Lkotlin/collections/CollectionsKt;->lastOrNull(Ljava/util/List;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/login/AuthenticatorScreenRendering;

    if-eqz v0, :cond_4

    .line 77
    invoke-static {v0}, Lcom/squareup/ui/login/RealAuthenticatorRendererWorkflowKt;->toLayeredScreen(Lcom/squareup/ui/login/AuthenticatorScreenRendering;)Ljava/util/Map;

    move-result-object v0

    .line 80
    sget-object v1, Lcom/squareup/ui/login/Operation;->Companion:Lcom/squareup/ui/login/Operation$Companion;

    invoke-virtual {v1}, Lcom/squareup/ui/login/Operation$Companion;->getNONE()Lcom/squareup/ui/login/Operation;

    move-result-object v1

    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lcom/squareup/ui/login/Operation;->getShowSpinner()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 81
    sget-object v1, Lcom/squareup/workflow/MainAndModal;->MODAL:Lcom/squareup/workflow/MainAndModal;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    xor-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 82
    sget-object v1, Lcom/squareup/workflow/MainAndModal;->MODAL:Lcom/squareup/workflow/MainAndModal;

    new-instance v2, Lcom/squareup/workflow/legacy/Screen;

    sget-object v3, Lcom/squareup/ui/login/LoginGlassSpinner;->INSTANCE:Lcom/squareup/ui/login/LoginGlassSpinner;

    invoke-virtual {v3}, Lcom/squareup/ui/login/LoginGlassSpinner;->getKEY()Lcom/squareup/workflow/legacy/Screen$Key;

    move-result-object v3

    invoke-direct {v2, v3, p1, p2}, Lcom/squareup/workflow/legacy/Screen;-><init>(Lcom/squareup/workflow/legacy/Screen$Key;Ljava/lang/Object;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    invoke-static {v1, v2}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object p1

    invoke-static {v0, p1}, Lkotlin/collections/MapsKt;->plus(Ljava/util/Map;Lkotlin/Pair;)Ljava/util/Map;

    move-result-object v0

    goto :goto_0

    .line 81
    :cond_0
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string p1, "Expected not to show a glass spinner over a dialog."

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0

    .line 86
    :cond_1
    :goto_0
    sget-object p1, Lcom/squareup/workflow/MainAndModal;->MAIN:Lcom/squareup/workflow/MainAndModal;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_2

    .line 87
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result p1

    add-int/lit8 p1, p1, -0x2

    invoke-interface {p0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/login/AuthenticatorScreenRendering;

    invoke-static {p0}, Lcom/squareup/ui/login/RealAuthenticatorRendererWorkflowKt;->toLayeredScreen(Lcom/squareup/ui/login/AuthenticatorScreenRendering;)Ljava/util/Map;

    move-result-object p0

    invoke-static {v0, p0}, Lkotlin/collections/MapsKt;->plus(Ljava/util/Map;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    .line 91
    :cond_2
    sget-object p0, Lcom/squareup/workflow/MainAndModal;->MAIN:Lcom/squareup/workflow/MainAndModal;

    invoke-interface {v0, p0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    return-object v0

    :cond_3
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string p1, "Expected a main screen to exist in top 2 stack layers: "

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 75
    :cond_4
    invoke-static {}, Lkotlin/collections/MapsKt;->emptyMap()Ljava/util/Map;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic toLayeredScreen$default(Ljava/util/List;Lcom/squareup/ui/login/Operation;Lcom/squareup/workflow/legacy/WorkflowInput;ILjava/lang/Object;)Ljava/util/Map;
    .locals 0

    and-int/lit8 p4, p3, 0x2

    if-eqz p4, :cond_0

    .line 72
    sget-object p1, Lcom/squareup/ui/login/Operation;->Companion:Lcom/squareup/ui/login/Operation$Companion;

    invoke-virtual {p1}, Lcom/squareup/ui/login/Operation$Companion;->getNONE()Lcom/squareup/ui/login/Operation;

    move-result-object p1

    :cond_0
    and-int/lit8 p3, p3, 0x4

    if-eqz p3, :cond_1

    .line 73
    sget-object p2, Lcom/squareup/workflow/legacy/WorkflowInput;->Companion:Lcom/squareup/workflow/legacy/WorkflowInput$Companion;

    invoke-virtual {p2}, Lcom/squareup/workflow/legacy/WorkflowInput$Companion;->disabled()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p2

    :cond_1
    invoke-static {p0, p1, p2}, Lcom/squareup/ui/login/RealAuthenticatorRendererWorkflowKt;->toLayeredScreen(Ljava/util/List;Lcom/squareup/ui/login/Operation;Lcom/squareup/workflow/legacy/WorkflowInput;)Ljava/util/Map;

    move-result-object p0

    return-object p0
.end method
