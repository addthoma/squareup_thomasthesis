.class public final Lcom/squareup/ui/login/DeviceCodeLoginCoordinator$attach$1$3;
.super Lcom/squareup/debounce/DebouncedOnEditorActionListener;
.source "DeviceCodeLoginCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/login/DeviceCodeLoginCoordinator$attach$1;->invoke(Lcom/squareup/workflow/legacy/Screen;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000#\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\"\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0008\u0010\u0008\u001a\u0004\u0018\u00010\tH\u0016\u00a8\u0006\n"
    }
    d2 = {
        "com/squareup/ui/login/DeviceCodeLoginCoordinator$attach$1$3",
        "Lcom/squareup/debounce/DebouncedOnEditorActionListener;",
        "doOnEditorAction",
        "",
        "v",
        "Landroid/widget/TextView;",
        "actionId",
        "",
        "keyEvent",
        "Landroid/view/KeyEvent;",
        "authenticator-views_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

.field final synthetic this$0:Lcom/squareup/ui/login/DeviceCodeLoginCoordinator$attach$1;


# direct methods
.method constructor <init>(Lcom/squareup/ui/login/DeviceCodeLoginCoordinator$attach$1;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/WorkflowInput;",
            ")V"
        }
    .end annotation

    .line 79
    iput-object p1, p0, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator$attach$1$3;->this$0:Lcom/squareup/ui/login/DeviceCodeLoginCoordinator$attach$1;

    iput-object p2, p0, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator$attach$1$3;->$workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnEditorActionListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doOnEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 0

    const-string p3, "v"

    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 p1, 0x2

    if-eq p2, p1, :cond_0

    const/4 p1, 0x5

    if-eq p2, p1, :cond_0

    const/4 p1, 0x6

    if-eq p2, p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    .line 86
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator$attach$1$3;->$workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    new-instance p2, Lcom/squareup/ui/login/AuthenticatorEvent$LoginWithDeviceCode;

    iget-object p3, p0, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator$attach$1$3;->this$0:Lcom/squareup/ui/login/DeviceCodeLoginCoordinator$attach$1;

    iget-object p3, p3, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator$attach$1;->this$0:Lcom/squareup/ui/login/DeviceCodeLoginCoordinator;

    invoke-static {p3}, Lcom/squareup/ui/login/DeviceCodeLoginCoordinator;->access$getDeviceCode$p(Lcom/squareup/ui/login/DeviceCodeLoginCoordinator;)Ljava/lang/String;

    move-result-object p3

    invoke-direct {p2, p3}, Lcom/squareup/ui/login/AuthenticatorEvent$LoginWithDeviceCode;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, p2}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    const/4 p1, 0x1

    :goto_0
    return p1
.end method
