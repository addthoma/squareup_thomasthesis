.class final Lcom/squareup/ui/login/RealAuthenticator$render$2;
.super Lkotlin/jvm/internal/Lambda;
.source "RealAuthenticator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/login/RealAuthenticator;->render(Lcom/squareup/ui/login/AuthenticatorInput;Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/ui/login/AuthenticatorRendering;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/location/CountryGuesser$Result;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/ui/login/AuthenticatorState;",
        "+",
        "Lcom/squareup/ui/login/AuthenticatorOutput;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/ui/login/AuthenticatorState;",
        "Lcom/squareup/ui/login/AuthenticatorOutput;",
        "result",
        "Lcom/squareup/location/CountryGuesser$Result;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $cleanedState:Lcom/squareup/ui/login/AuthenticatorState;


# direct methods
.method constructor <init>(Lcom/squareup/ui/login/AuthenticatorState;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/login/RealAuthenticator$render$2;->$cleanedState:Lcom/squareup/ui/login/AuthenticatorState;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/location/CountryGuesser$Result;)Lcom/squareup/workflow/WorkflowAction;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/location/CountryGuesser$Result;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/login/AuthenticatorState;",
            "Lcom/squareup/ui/login/AuthenticatorOutput;",
            ">;"
        }
    .end annotation

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 226
    instance-of v0, p1, Lcom/squareup/location/CountryGuesser$Result$Country;

    if-nez v0, :cond_0

    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    invoke-virtual {p1}, Lcom/squareup/workflow/WorkflowAction$Companion;->noAction()Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    goto :goto_0

    .line 229
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/login/RealAuthenticator$render$2;->$cleanedState:Lcom/squareup/ui/login/AuthenticatorState;

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 230
    invoke-virtual {p1}, Lcom/squareup/location/CountryGuesser$Result;->getSupportsPayments()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v4, 0x3

    const/4 v5, 0x0

    .line 229
    invoke-static/range {v0 .. v5}, Lcom/squareup/ui/login/RealAuthenticatorKt;->withTemporarySessionData$default(Lcom/squareup/ui/login/AuthenticatorState;Ljava/lang/String;Lcom/squareup/ui/login/UnitsAndMerchants;Ljava/lang/Boolean;ILjava/lang/Object;)Lcom/squareup/ui/login/AuthenticatorState;

    move-result-object p1

    const/4 v0, 0x4

    const/4 v1, 0x0

    const-string v2, "update isWorldEnabled from country guess"

    .line 227
    invoke-static {v2, p1, v1, v0, v1}, Lcom/squareup/ui/login/RealAuthenticatorKt;->enterState$default(Ljava/lang/String;Lcom/squareup/ui/login/AuthenticatorState;Lcom/squareup/ui/login/AuthenticatorOutput;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 157
    check-cast p1, Lcom/squareup/location/CountryGuesser$Result;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/login/RealAuthenticator$render$2;->invoke(Lcom/squareup/location/CountryGuesser$Result;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
