.class public final Lcom/squareup/ui/login/RealAuthenticatorRendererWorkflow;
.super Lcom/squareup/workflow/StatelessWorkflow;
.source "RealAuthenticatorRendererWorkflow.kt"

# interfaces
.implements Lcom/squareup/ui/login/AuthenticatorRendererWorkflow;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatelessWorkflow<",
        "Lcom/squareup/ui/login/AuthenticatorInput;",
        "Lcom/squareup/ui/login/AuthenticatorOutput;",
        "Ljava/util/Map<",
        "Lcom/squareup/container/PosLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;",
        "Lcom/squareup/ui/login/AuthenticatorRendererWorkflow;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0000\u0018\u00002\u00020\u000126\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012&\u0012$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t0\u0002B\u000f\u0008\u0007\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJF\u0010\r\u001a$\u0012\u0004\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\u0008\u0012\u0004\u0012\u00020\u0006`\t2\u0006\u0010\u000e\u001a\u00020\u00032\u0012\u0010\u000f\u001a\u000e\u0012\u0004\u0012\u00020\u0011\u0012\u0004\u0012\u00020\u00040\u0010H\u0016R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/ui/login/RealAuthenticatorRendererWorkflow;",
        "Lcom/squareup/ui/login/AuthenticatorRendererWorkflow;",
        "Lcom/squareup/workflow/StatelessWorkflow;",
        "Lcom/squareup/ui/login/AuthenticatorInput;",
        "Lcom/squareup/ui/login/AuthenticatorOutput;",
        "",
        "Lcom/squareup/container/PosLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "authenticator",
        "Lcom/squareup/ui/login/RealAuthenticator;",
        "(Lcom/squareup/ui/login/RealAuthenticator;)V",
        "render",
        "input",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "",
        "authenticator-views_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final authenticator:Lcom/squareup/ui/login/RealAuthenticator;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/login/RealAuthenticator;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "authenticator"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    invoke-direct {p0}, Lcom/squareup/workflow/StatelessWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/login/RealAuthenticatorRendererWorkflow;->authenticator:Lcom/squareup/ui/login/RealAuthenticator;

    return-void
.end method


# virtual methods
.method public bridge synthetic render(Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 41
    check-cast p1, Lcom/squareup/ui/login/AuthenticatorInput;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/login/RealAuthenticatorRendererWorkflow;->render(Lcom/squareup/ui/login/AuthenticatorInput;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/ui/login/AuthenticatorInput;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/login/AuthenticatorInput;",
            "Lcom/squareup/workflow/RenderContext;",
            ")",
            "Ljava/util/Map<",
            "Lcom/squareup/container/PosLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    const-string v0, "input"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "context"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    iget-object v0, p0, Lcom/squareup/ui/login/RealAuthenticatorRendererWorkflow;->authenticator:Lcom/squareup/ui/login/RealAuthenticator;

    move-object v2, v0

    check-cast v2, Lcom/squareup/workflow/Workflow;

    .line 53
    new-instance v0, Lcom/squareup/ui/login/RealAuthenticatorRendererWorkflow$render$authRendering$1;

    sget-object v1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    invoke-direct {v0, v1}, Lcom/squareup/ui/login/RealAuthenticatorRendererWorkflow$render$authRendering$1;-><init>(Lcom/squareup/workflow/WorkflowAction$Companion;)V

    move-object v5, v0

    check-cast v5, Lkotlin/jvm/functions/Function1;

    const/4 v4, 0x0

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v1, p2

    move-object v3, p1

    .line 50
    invoke-static/range {v1 .. v7}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/login/AuthenticatorRendering;

    .line 56
    invoke-virtual {p1}, Lcom/squareup/ui/login/AuthenticatorRendering;->getScreens()Ljava/util/List;

    move-result-object p2

    .line 57
    invoke-virtual {p1}, Lcom/squareup/ui/login/AuthenticatorRendering;->getOperation()Lcom/squareup/ui/login/Operation;

    move-result-object v0

    .line 58
    invoke-virtual {p1}, Lcom/squareup/ui/login/AuthenticatorRendering;->getInput()Lcom/squareup/workflow/legacy/WorkflowInput;

    move-result-object p1

    .line 55
    invoke-static {p2, v0, p1}, Lcom/squareup/ui/login/RealAuthenticatorRendererWorkflowKt;->toLayeredScreen(Ljava/util/List;Lcom/squareup/ui/login/Operation;Lcom/squareup/workflow/legacy/WorkflowInput;)Ljava/util/Map;

    move-result-object p1

    .line 60
    sget-object p2, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    sget-object v0, Lcom/squareup/container/PosLayering;->DIALOG:Lcom/squareup/container/PosLayering;

    invoke-static {p1, p2, v0}, Lcom/squareup/container/LayeredScreensKt;->toPosScreen(Ljava/util/Map;Lcom/squareup/container/PosLayering;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object p1

    const/4 p2, 0x0

    invoke-static {p1, p2}, Lcom/squareup/workflow/LayeredScreenKt;->withPersistence(Ljava/util/Map;Z)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method
