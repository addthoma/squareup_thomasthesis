.class final Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint$login$1$1;
.super Lkotlin/jvm/internal/Lambda;
.source "AuthenticationServiceEndpoint.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint$login$1;->apply(Lcom/squareup/receiving/ReceivedResponse;)Lcom/squareup/receiving/ReceivedResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/protos/register/api/LoginResponse;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAuthenticationServiceEndpoint.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AuthenticationServiceEndpoint.kt\ncom/squareup/ui/login/RealAuthenticationServiceEndpoint$login$1$1\n*L\n1#1,412:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/protos/register/api/LoginResponse;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint$login$1$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint$login$1$1;

    invoke-direct {v0}, Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint$login$1$1;-><init>()V

    sput-object v0, Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint$login$1$1;->INSTANCE:Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint$login$1$1;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 169
    check-cast p1, Lcom/squareup/protos/register/api/LoginResponse;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/login/RealAuthenticationServiceEndpoint$login$1$1;->invoke(Lcom/squareup/protos/register/api/LoginResponse;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public final invoke(Lcom/squareup/protos/register/api/LoginResponse;)Z
    .locals 3

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 189
    iget-object v0, p1, Lcom/squareup/protos/register/api/LoginResponse;->error_alert:Lcom/squareup/protos/multipass/mobile/Alert;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez v0, :cond_2

    iget-object p1, p1, Lcom/squareup/protos/register/api/LoginResponse;->error_title:Ljava/lang/String;

    check-cast p1, Ljava/lang/CharSequence;

    if-eqz p1, :cond_1

    invoke-static {p1}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    if-eqz p1, :cond_2

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    return v1
.end method
