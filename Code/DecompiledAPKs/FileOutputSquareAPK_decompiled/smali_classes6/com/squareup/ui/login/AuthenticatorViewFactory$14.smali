.class final synthetic Lcom/squareup/ui/login/AuthenticatorViewFactory$14;
.super Lkotlin/jvm/internal/FunctionReference;
.source "AuthenticatorViewFactory.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/login/AuthenticatorViewFactory;-><init>(Lcom/squareup/ui/login/EmailPasswordLoginCoordinator$Factory;Lcom/squareup/ui/login/LoginAlertDialogFactory$Factory;Lcom/squareup/ui/login/LoginWarningDialogFactory$Factory;Lcom/squareup/ui/login/EnrollSmsCoordinator$Factory;Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator$Factory;Lcom/squareup/ui/login/VerificationCodeSmsCoordinator$Factory;Lcom/squareup/ui/login/AuthenticatorViewFactory$DeviceCodeViewBinding;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1018
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/FunctionReference;",
        "Lkotlin/jvm/functions/Function1<",
        "Lio/reactivex/Observable<",
        "Lcom/squareup/workflow/legacy/Screen<",
        "Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuthCode;",
        "Lcom/squareup/ui/login/AuthenticatorEvent;",
        ">;>;",
        "Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a\u00020\u00012\'\u0010\u0002\u001a#\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u00040\u0003\u00a2\u0006\u000c\u0008\u0007\u0012\u0008\u0008\u0008\u0012\u0004\u0008\u0008(\t\u00a2\u0006\u0002\u0008\n"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;",
        "p1",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuthCode;",
        "Lcom/squareup/ui/login/AuthenticatorEvent;",
        "Lkotlin/ParameterName;",
        "name",
        "screenData",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator$Factory;)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lkotlin/jvm/internal/FunctionReference;-><init>(ILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    const-string v0, "create"

    return-object v0
.end method

.method public final getOwner()Lkotlin/reflect/KDeclarationContainer;
    .locals 1

    const-class v0, Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator$Factory;

    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v0

    return-object v0
.end method

.method public final getSignature()Ljava/lang/String;
    .locals 1

    const-string v0, "create(Lio/reactivex/Observable;)Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;"

    return-object v0
.end method

.method public final invoke(Lio/reactivex/Observable;)Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/login/AuthenticatorScreen$EnrollGoogleAuthCode;",
            "Lcom/squareup/ui/login/AuthenticatorEvent;",
            ">;>;)",
            "Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;"
        }
    .end annotation

    const-string v0, "p1"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/ui/login/AuthenticatorViewFactory$14;->receiver:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator$Factory;

    .line 150
    invoke-virtual {v0, p1}, Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator$Factory;->create(Lio/reactivex/Observable;)Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 34
    check-cast p1, Lio/reactivex/Observable;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/login/AuthenticatorViewFactory$14;->invoke(Lio/reactivex/Observable;)Lcom/squareup/ui/login/EnrollGoogleAuthCodeCoordinator;

    move-result-object p1

    return-object p1
.end method
