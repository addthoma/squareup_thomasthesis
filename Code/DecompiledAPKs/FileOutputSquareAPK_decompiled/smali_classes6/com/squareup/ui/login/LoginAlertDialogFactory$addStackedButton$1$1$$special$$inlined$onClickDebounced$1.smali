.class public final Lcom/squareup/ui/login/LoginAlertDialogFactory$addStackedButton$1$1$$special$$inlined$onClickDebounced$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "Views.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/login/LoginAlertDialogFactory$addStackedButton$1$1;->accept(Lcom/squareup/workflow/legacy/WorkflowInput;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nViews.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Views.kt\ncom/squareup/util/Views$onClickDebounced$1\n+ 2 LoginAlertDialogFactory.kt\ncom/squareup/ui/login/LoginAlertDialogFactory$addStackedButton$1$1\n*L\n1#1,1322:1\n121#2:1323\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H\u0016\u00a8\u0006\u0006\u00b8\u0006\u0000"
    }
    d2 = {
        "com/squareup/util/Views$onClickDebounced$1",
        "Lcom/squareup/debounce/DebouncedOnClickListener;",
        "doClick",
        "",
        "view",
        "Landroid/view/View;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $input$inlined:Lcom/squareup/workflow/legacy/WorkflowInput;

.field final synthetic this$0:Lcom/squareup/ui/login/LoginAlertDialogFactory$addStackedButton$1$1;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/login/LoginAlertDialogFactory$addStackedButton$1$1;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/login/LoginAlertDialogFactory$addStackedButton$1$1$$special$$inlined$onClickDebounced$1;->this$0:Lcom/squareup/ui/login/LoginAlertDialogFactory$addStackedButton$1$1;

    iput-object p2, p0, Lcom/squareup/ui/login/LoginAlertDialogFactory$addStackedButton$1$1$$special$$inlined$onClickDebounced$1;->$input$inlined:Lcom/squareup/workflow/legacy/WorkflowInput;

    .line 1103
    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 2

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1106
    check-cast p1, Landroid/widget/TextView;

    .line 1323
    iget-object p1, p0, Lcom/squareup/ui/login/LoginAlertDialogFactory$addStackedButton$1$1$$special$$inlined$onClickDebounced$1;->this$0:Lcom/squareup/ui/login/LoginAlertDialogFactory$addStackedButton$1$1;

    iget-object p1, p1, Lcom/squareup/ui/login/LoginAlertDialogFactory$addStackedButton$1$1;->this$0:Lcom/squareup/ui/login/LoginAlertDialogFactory$addStackedButton$1;

    iget-object p1, p1, Lcom/squareup/ui/login/LoginAlertDialogFactory$addStackedButton$1;->this$0:Lcom/squareup/ui/login/LoginAlertDialogFactory;

    iget-object v0, p0, Lcom/squareup/ui/login/LoginAlertDialogFactory$addStackedButton$1$1$$special$$inlined$onClickDebounced$1;->$input$inlined:Lcom/squareup/workflow/legacy/WorkflowInput;

    const-string v1, "input"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/squareup/ui/login/LoginAlertDialogFactory$addStackedButton$1$1$$special$$inlined$onClickDebounced$1;->this$0:Lcom/squareup/ui/login/LoginAlertDialogFactory$addStackedButton$1$1;

    iget-object v1, v1, Lcom/squareup/ui/login/LoginAlertDialogFactory$addStackedButton$1$1;->this$0:Lcom/squareup/ui/login/LoginAlertDialogFactory$addStackedButton$1;

    iget-object v1, v1, Lcom/squareup/ui/login/LoginAlertDialogFactory$addStackedButton$1;->$alertButton:Lcom/squareup/protos/multipass/mobile/AlertButton;

    invoke-static {p1, v0, v1}, Lcom/squareup/ui/login/LoginAlertDialogFactory;->access$onResult(Lcom/squareup/ui/login/LoginAlertDialogFactory;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/protos/multipass/mobile/AlertButton;)V

    return-void
.end method
