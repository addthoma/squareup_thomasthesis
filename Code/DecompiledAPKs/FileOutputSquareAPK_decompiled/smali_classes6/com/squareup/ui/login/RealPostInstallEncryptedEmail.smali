.class public final Lcom/squareup/ui/login/RealPostInstallEncryptedEmail;
.super Ljava/lang/Object;
.source "RealPostInstallEncryptedEmail.kt"

# interfaces
.implements Lcom/squareup/ui/login/PostInstallEncryptedEmail;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u0001B/\u0008\u0007\u0012\u0008\u0008\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bJ\u0008\u0010\u000c\u001a\u00020\rH\u0016J\u0016\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000f2\u0006\u0010\u0011\u001a\u00020\u0012H\u0016J\n\u0010\u0013\u001a\u0004\u0018\u00010\u0012H\u0016J\u0010\u0010\u0014\u001a\u00020\r2\u0006\u0010\u0015\u001a\u00020\u0016H\u0002J\u000c\u0010\u0015\u001a\u00020\u0016*\u00020\u0010H\u0002R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/ui/login/RealPostInstallEncryptedEmail;",
        "Lcom/squareup/ui/login/PostInstallEncryptedEmail;",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "publicService",
        "Lcom/squareup/server/PublicApiService;",
        "postInstallEncryptedEmailSetting",
        "Lcom/squareup/settings/LocalSetting;",
        "Lcom/squareup/ui/login/PostInstallEncryptedEmailFromReferral;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "(Lio/reactivex/Scheduler;Lcom/squareup/server/PublicApiService;Lcom/squareup/settings/LocalSetting;Lcom/squareup/analytics/Analytics;)V",
        "clearEncryptedEmail",
        "",
        "decryptEmail",
        "Lio/reactivex/Maybe;",
        "Lcom/squareup/protos/client/multipass/DecryptEmailResponse;",
        "encryptedEmail",
        "",
        "getEncryptedEmail",
        "logAnalytics",
        "isValid",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final postInstallEncryptedEmailSetting:Lcom/squareup/settings/LocalSetting;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/ui/login/PostInstallEncryptedEmailFromReferral;",
            ">;"
        }
    .end annotation
.end field

.field private final publicService:Lcom/squareup/server/PublicApiService;


# direct methods
.method public constructor <init>(Lio/reactivex/Scheduler;Lcom/squareup/server/PublicApiService;Lcom/squareup/settings/LocalSetting;Lcom/squareup/analytics/Analytics;)V
    .locals 1
    .param p1    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Scheduler;",
            "Lcom/squareup/server/PublicApiService;",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/ui/login/PostInstallEncryptedEmailFromReferral;",
            ">;",
            "Lcom/squareup/analytics/Analytics;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "mainScheduler"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "publicService"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "postInstallEncryptedEmailSetting"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/login/RealPostInstallEncryptedEmail;->mainScheduler:Lio/reactivex/Scheduler;

    iput-object p2, p0, Lcom/squareup/ui/login/RealPostInstallEncryptedEmail;->publicService:Lcom/squareup/server/PublicApiService;

    iput-object p3, p0, Lcom/squareup/ui/login/RealPostInstallEncryptedEmail;->postInstallEncryptedEmailSetting:Lcom/squareup/settings/LocalSetting;

    iput-object p4, p0, Lcom/squareup/ui/login/RealPostInstallEncryptedEmail;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method

.method public static final synthetic access$isValid(Lcom/squareup/ui/login/RealPostInstallEncryptedEmail;Lcom/squareup/protos/client/multipass/DecryptEmailResponse;)Z
    .locals 0

    .line 21
    invoke-direct {p0, p1}, Lcom/squareup/ui/login/RealPostInstallEncryptedEmail;->isValid(Lcom/squareup/protos/client/multipass/DecryptEmailResponse;)Z

    move-result p0

    return p0
.end method

.method public static final synthetic access$logAnalytics(Lcom/squareup/ui/login/RealPostInstallEncryptedEmail;Z)V
    .locals 0

    .line 21
    invoke-direct {p0, p1}, Lcom/squareup/ui/login/RealPostInstallEncryptedEmail;->logAnalytics(Z)V

    return-void
.end method

.method private final isValid(Lcom/squareup/protos/client/multipass/DecryptEmailResponse;)Z
    .locals 0

    .line 59
    iget-object p1, p1, Lcom/squareup/protos/client/multipass/DecryptEmailResponse;->decrypted_email:Ljava/lang/String;

    check-cast p1, Ljava/lang/CharSequence;

    invoke-static {p1}, Lcom/squareup/text/Emails;->isValid(Ljava/lang/CharSequence;)Z

    move-result p1

    return p1
.end method

.method private final logAnalytics(Z)V
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/login/RealPostInstallEncryptedEmail;->analytics:Lcom/squareup/analytics/Analytics;

    if-eqz p1, :cond_0

    sget-object p1, Lcom/squareup/analytics/RegisterActionName;->SIGN_IN_SHOWED_EMAIL:Lcom/squareup/analytics/RegisterActionName;

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/analytics/RegisterActionName;->SIGN_IN_DECRYPT_FAIL:Lcom/squareup/analytics/RegisterActionName;

    :goto_0
    invoke-interface {v0, p1}, Lcom/squareup/analytics/Analytics;->logAction(Lcom/squareup/analytics/RegisterActionName;)V

    return-void
.end method


# virtual methods
.method public clearEncryptedEmail()V
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/squareup/ui/login/RealPostInstallEncryptedEmail;->postInstallEncryptedEmailSetting:Lcom/squareup/settings/LocalSetting;

    invoke-interface {v0}, Lcom/squareup/settings/LocalSetting;->remove()V

    return-void
.end method

.method public decryptEmail(Ljava/lang/String;)Lio/reactivex/Maybe;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Maybe<",
            "Lcom/squareup/protos/client/multipass/DecryptEmailResponse;",
            ">;"
        }
    .end annotation

    const-string v0, "encryptedEmail"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    new-instance v0, Lcom/squareup/protos/client/multipass/DecryptEmailRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/multipass/DecryptEmailRequest$Builder;-><init>()V

    .line 40
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/multipass/DecryptEmailRequest$Builder;->encrypted_email(Ljava/lang/String;)Lcom/squareup/protos/client/multipass/DecryptEmailRequest$Builder;

    move-result-object p1

    .line 41
    invoke-virtual {p1}, Lcom/squareup/protos/client/multipass/DecryptEmailRequest$Builder;->build()Lcom/squareup/protos/client/multipass/DecryptEmailRequest;

    move-result-object p1

    .line 42
    iget-object v0, p0, Lcom/squareup/ui/login/RealPostInstallEncryptedEmail;->publicService:Lcom/squareup/server/PublicApiService;

    invoke-interface {v0, p1}, Lcom/squareup/server/PublicApiService;->decryptEmail(Lcom/squareup/protos/client/multipass/DecryptEmailRequest;)Lcom/squareup/server/AcceptedResponse;

    move-result-object p1

    .line 43
    invoke-virtual {p1}, Lcom/squareup/server/AcceptedResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    .line 44
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x3

    invoke-virtual {p1, v1, v2, v0}, Lio/reactivex/Single;->timeout(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/Single;

    move-result-object p1

    .line 45
    sget-object v0, Lcom/squareup/ui/login/RealPostInstallEncryptedEmail$decryptEmail$1;->INSTANCE:Lcom/squareup/ui/login/RealPostInstallEncryptedEmail$decryptEmail$1;

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->flatMapMaybe(Lio/reactivex/functions/Function;)Lio/reactivex/Maybe;

    move-result-object p1

    .line 51
    new-instance v0, Lcom/squareup/ui/login/RealPostInstallEncryptedEmail$decryptEmail$2;

    invoke-direct {v0, p0}, Lcom/squareup/ui/login/RealPostInstallEncryptedEmail$decryptEmail$2;-><init>(Lcom/squareup/ui/login/RealPostInstallEncryptedEmail;)V

    check-cast v0, Lio/reactivex/functions/Predicate;

    invoke-virtual {p1, v0}, Lio/reactivex/Maybe;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Maybe;

    move-result-object p1

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/login/RealPostInstallEncryptedEmail;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {p1, v0}, Lio/reactivex/Maybe;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Maybe;

    move-result-object p1

    const-string v0, "publicService.decryptEma\u2026.observeOn(mainScheduler)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public getEncryptedEmail()Ljava/lang/String;
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/squareup/ui/login/RealPostInstallEncryptedEmail;->postInstallEncryptedEmailSetting:Lcom/squareup/settings/LocalSetting;

    invoke-interface {v0}, Lcom/squareup/settings/LocalSetting;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/login/PostInstallEncryptedEmailFromReferral;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/ui/login/PostInstallEncryptedEmailFromReferral;->getEncryptedEmail()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method
