.class public Lcom/squareup/ui/timecards/TimecardsScope;
.super Lcom/squareup/ui/main/InMainActivityScope;
.source "TimecardsScope.java"

# interfaces
.implements Lcom/squareup/container/RegistersInScope;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/timecards/TimecardsScope$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/timecards/TimecardsScope$Component;,
        Lcom/squareup/ui/timecards/TimecardsScope$ParentComponent;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/timecards/TimecardsScope;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/timecards/TimecardsScope;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 18
    new-instance v0, Lcom/squareup/ui/timecards/TimecardsScope;

    invoke-direct {v0}, Lcom/squareup/ui/timecards/TimecardsScope;-><init>()V

    sput-object v0, Lcom/squareup/ui/timecards/TimecardsScope;->INSTANCE:Lcom/squareup/ui/timecards/TimecardsScope;

    .line 83
    sget-object v0, Lcom/squareup/ui/timecards/TimecardsScope;->INSTANCE:Lcom/squareup/ui/timecards/TimecardsScope;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/timecards/TimecardsScope;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 20
    invoke-direct {p0}, Lcom/squareup/ui/main/InMainActivityScope;-><init>()V

    return-void
.end method


# virtual methods
.method public register(Lmortar/MortarScope;)V
    .locals 1

    .line 25
    const-class v0, Lcom/squareup/ui/timecards/TimecardsScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/timecards/TimecardsScope$Component;

    .line 26
    invoke-interface {v0}, Lcom/squareup/ui/timecards/TimecardsScope$Component;->scopeRunner()Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    move-result-object v0

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    return-void
.end method
