.class public Lcom/squareup/ui/timecards/ClockInOutHistoryFactory;
.super Ljava/lang/Object;
.source "ClockInOutHistoryFactory.java"

# interfaces
.implements Lcom/squareup/ui/main/HistoryFactory;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createHistory(Lcom/squareup/ui/main/Home;Lflow/History;)Lflow/History;
    .locals 2

    .line 20
    sget-object v0, Lcom/squareup/ui/timecards/ClockInOutScreen;->NORMAL:Lcom/squareup/ui/timecards/ClockInOutScreen;

    if-nez p2, :cond_0

    .line 22
    invoke-static {v0}, Lflow/History;->single(Ljava/lang/Object;)Lflow/History;

    move-result-object p1

    return-object p1

    .line 24
    :cond_0
    invoke-virtual {p2}, Lflow/History;->top()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    return-object p2

    .line 27
    :cond_1
    invoke-static {p1, p2}, Lcom/squareup/ui/main/HomeKt;->buildUpon(Lcom/squareup/ui/main/Home;Lflow/History;)Lflow/History$Builder;

    move-result-object p1

    invoke-virtual {p1, v0}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object p1

    return-object p1
.end method

.method public getPermissions()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Permission;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method
