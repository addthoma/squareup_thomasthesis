.class public final Lcom/squareup/ui/timecards/TimecardsRunnerLauncher;
.super Ljava/lang/Object;
.source "TimecardsRunnerLauncher.kt"

# interfaces
.implements Lcom/squareup/ui/timecards/api/TimecardsLauncher;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001B%\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u000c\u0010\u0006\u001a\u0008\u0012\u0004\u0012\u00020\u00080\u0007\u00a2\u0006\u0002\u0010\tJ\u0008\u0010\u000f\u001a\u00020\u0010H\u0016J\u000e\u0010\u0011\u001a\u0008\u0012\u0004\u0012\u00020\u00130\u0012H\u0016J\u0008\u0010\u0014\u001a\u00020\u0015H\u0016J\u0008\u0010\u0016\u001a\u00020\u0015H\u0016J\u0008\u0010\u0017\u001a\u00020\u0015H\u0016J\u0008\u0010\u0018\u001a\u00020\u0015H\u0016R\u001b\u0010\n\u001a\u00020\u00088BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\r\u0010\u000e\u001a\u0004\u0008\u000b\u0010\u000cR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0019"
    }
    d2 = {
        "Lcom/squareup/ui/timecards/TimecardsRunnerLauncher;",
        "Lcom/squareup/ui/timecards/api/TimecardsLauncher;",
        "home",
        "Lcom/squareup/ui/main/Home;",
        "mainContainer",
        "Lcom/squareup/ui/main/PosContainer;",
        "lazyFlow",
        "Ldagger/Lazy;",
        "Lflow/Flow;",
        "(Lcom/squareup/ui/main/Home;Lcom/squareup/ui/main/PosContainer;Ldagger/Lazy;)V",
        "flow",
        "getFlow",
        "()Lflow/Flow;",
        "flow$delegate",
        "Ldagger/Lazy;",
        "clockInOutHistoryFactory",
        "Lcom/squareup/ui/main/HistoryFactory;",
        "isClockInOutScreenVisible",
        "Lio/reactivex/Observable;",
        "",
        "showClockInOrContinueScreenFromHome",
        "",
        "showClockInOut",
        "showClockInOutFromPasscode",
        "showTimecardsLoadingScreenFromHome",
        "timecards_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;


# instance fields
.field private final flow$delegate:Ldagger/Lazy;

.field private final home:Lcom/squareup/ui/main/Home;

.field private final mainContainer:Lcom/squareup/ui/main/PosContainer;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Lkotlin/reflect/KProperty;

    new-instance v1, Lkotlin/jvm/internal/PropertyReference1Impl;

    const-class v2, Lcom/squareup/ui/timecards/TimecardsRunnerLauncher;

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v2

    const-string v3, "flow"

    const-string v4, "getFlow()Lflow/Flow;"

    invoke-direct {v1, v2, v3, v4}, Lkotlin/jvm/internal/PropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->property1(Lkotlin/jvm/internal/PropertyReference1;)Lkotlin/reflect/KProperty1;

    move-result-object v1

    check-cast v1, Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/ui/timecards/TimecardsRunnerLauncher;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/main/Home;Lcom/squareup/ui/main/PosContainer;Ldagger/Lazy;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/main/Home;",
            "Lcom/squareup/ui/main/PosContainer;",
            "Ldagger/Lazy<",
            "Lflow/Flow;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "home"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainContainer"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "lazyFlow"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/timecards/TimecardsRunnerLauncher;->home:Lcom/squareup/ui/main/Home;

    iput-object p2, p0, Lcom/squareup/ui/timecards/TimecardsRunnerLauncher;->mainContainer:Lcom/squareup/ui/main/PosContainer;

    .line 23
    iput-object p3, p0, Lcom/squareup/ui/timecards/TimecardsRunnerLauncher;->flow$delegate:Ldagger/Lazy;

    return-void
.end method

.method public static final synthetic access$getHome$p(Lcom/squareup/ui/timecards/TimecardsRunnerLauncher;)Lcom/squareup/ui/main/Home;
    .locals 0

    .line 17
    iget-object p0, p0, Lcom/squareup/ui/timecards/TimecardsRunnerLauncher;->home:Lcom/squareup/ui/main/Home;

    return-object p0
.end method

.method private final getFlow()Lflow/Flow;
    .locals 3

    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsRunnerLauncher;->flow$delegate:Ldagger/Lazy;

    sget-object v1, Lcom/squareup/ui/timecards/TimecardsRunnerLauncher;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-static {v0, p0, v1}, Lcom/squareup/dagger/LazysKt;->getValue(Ldagger/Lazy;Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lflow/Flow;

    return-object v0
.end method


# virtual methods
.method public clockInOutHistoryFactory()Lcom/squareup/ui/main/HistoryFactory;
    .locals 1

    .line 59
    new-instance v0, Lcom/squareup/ui/timecards/ClockInOutHistoryFactory;

    invoke-direct {v0}, Lcom/squareup/ui/timecards/ClockInOutHistoryFactory;-><init>()V

    check-cast v0, Lcom/squareup/ui/main/HistoryFactory;

    return-object v0
.end method

.method public isClockInOutScreenVisible()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsRunnerLauncher;->mainContainer:Lcom/squareup/ui/main/PosContainer;

    invoke-static {v0}, Lcom/squareup/ui/main/PosContainers;->nextScreen(Lcom/squareup/ui/main/PosContainer;)Lio/reactivex/Observable;

    move-result-object v0

    .line 55
    sget-object v1, Lcom/squareup/ui/timecards/TimecardsRunnerLauncher$isClockInOutScreenVisible$1;->INSTANCE:Lcom/squareup/ui/timecards/TimecardsRunnerLauncher$isClockInOutScreenVisible$1;

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 56
    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "mainContainer.nextScreen\u2026  .distinctUntilChanged()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public showClockInOrContinueScreenFromHome()V
    .locals 4

    .line 31
    invoke-direct {p0}, Lcom/squareup/ui/timecards/TimecardsRunnerLauncher;->getFlow()Lflow/Flow;

    move-result-object v0

    new-instance v1, Lcom/squareup/container/CalculatedKey;

    new-instance v2, Lcom/squareup/ui/timecards/TimecardsRunnerLauncher$showClockInOrContinueScreenFromHome$1;

    invoke-direct {v2, p0}, Lcom/squareup/ui/timecards/TimecardsRunnerLauncher$showClockInOrContinueScreenFromHome$1;-><init>(Lcom/squareup/ui/timecards/TimecardsRunnerLauncher;)V

    check-cast v2, Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;

    const-string v3, "showClockInOrContinueScreen"

    invoke-direct {v1, v3, v2}, Lcom/squareup/container/CalculatedKey;-><init>(Ljava/lang/String;Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public showClockInOut()V
    .locals 2

    .line 25
    invoke-direct {p0}, Lcom/squareup/ui/timecards/TimecardsRunnerLauncher;->getFlow()Lflow/Flow;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/timecards/ClockInOutScreen;->NORMAL:Lcom/squareup/ui/timecards/ClockInOutScreen;

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public showClockInOutFromPasscode()V
    .locals 2

    .line 27
    invoke-direct {p0}, Lcom/squareup/ui/timecards/TimecardsRunnerLauncher;->getFlow()Lflow/Flow;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/timecards/ClockInOutScreen;->FROM_PASSCODE:Lcom/squareup/ui/timecards/ClockInOutScreen;

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public showTimecardsLoadingScreenFromHome()V
    .locals 4

    .line 43
    invoke-direct {p0}, Lcom/squareup/ui/timecards/TimecardsRunnerLauncher;->getFlow()Lflow/Flow;

    move-result-object v0

    new-instance v1, Lcom/squareup/container/CalculatedKey;

    new-instance v2, Lcom/squareup/ui/timecards/TimecardsRunnerLauncher$showTimecardsLoadingScreenFromHome$1;

    invoke-direct {v2, p0}, Lcom/squareup/ui/timecards/TimecardsRunnerLauncher$showTimecardsLoadingScreenFromHome$1;-><init>(Lcom/squareup/ui/timecards/TimecardsRunnerLauncher;)V

    check-cast v2, Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;

    const-string v3, "showTimecardsLoadingScreen"

    invoke-direct {v1, v3, v2}, Lcom/squareup/container/CalculatedKey;-><init>(Ljava/lang/String;Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method
