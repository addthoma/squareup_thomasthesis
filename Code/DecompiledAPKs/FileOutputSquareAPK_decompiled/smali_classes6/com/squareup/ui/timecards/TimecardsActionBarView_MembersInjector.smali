.class public final Lcom/squareup/ui/timecards/TimecardsActionBarView_MembersInjector;
.super Ljava/lang/Object;
.source "TimecardsActionBarView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/timecards/TimecardsActionBarView;",
        ">;"
    }
.end annotation


# instance fields
.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView_MembersInjector;->resProvider:Ljavax/inject/Provider;

    .line 26
    iput-object p2, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView_MembersInjector;->deviceProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/timecards/TimecardsActionBarView;",
            ">;"
        }
    .end annotation

    .line 31
    new-instance v0, Lcom/squareup/ui/timecards/TimecardsActionBarView_MembersInjector;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/timecards/TimecardsActionBarView_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectDevice(Lcom/squareup/ui/timecards/TimecardsActionBarView;Lcom/squareup/util/Device;)V
    .locals 0

    .line 46
    iput-object p1, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView;->device:Lcom/squareup/util/Device;

    return-void
.end method

.method public static injectRes(Lcom/squareup/ui/timecards/TimecardsActionBarView;Lcom/squareup/util/Res;)V
    .locals 0

    .line 41
    iput-object p1, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView;->res:Lcom/squareup/util/Res;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/timecards/TimecardsActionBarView;)V
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView_MembersInjector;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Res;

    invoke-static {p1, v0}, Lcom/squareup/ui/timecards/TimecardsActionBarView_MembersInjector;->injectRes(Lcom/squareup/ui/timecards/TimecardsActionBarView;Lcom/squareup/util/Res;)V

    .line 36
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsActionBarView_MembersInjector;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Device;

    invoke-static {p1, v0}, Lcom/squareup/ui/timecards/TimecardsActionBarView_MembersInjector;->injectDevice(Lcom/squareup/ui/timecards/TimecardsActionBarView;Lcom/squareup/util/Device;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 10
    check-cast p1, Lcom/squareup/ui/timecards/TimecardsActionBarView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/timecards/TimecardsActionBarView_MembersInjector;->injectMembers(Lcom/squareup/ui/timecards/TimecardsActionBarView;)V

    return-void
.end method
