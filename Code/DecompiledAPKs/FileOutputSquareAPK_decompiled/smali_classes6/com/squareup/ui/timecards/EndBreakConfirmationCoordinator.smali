.class public Lcom/squareup/ui/timecards/EndBreakConfirmationCoordinator;
.super Lcom/squareup/ui/timecards/TimecardsCoordinator;
.source "EndBreakConfirmationCoordinator.java"


# instance fields
.field private endBreakButton:Lcom/squareup/marketfont/MarketButton;

.field private endBreakContainer:Landroid/widget/LinearLayout;


# direct methods
.method constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/ui/timecards/TimecardsScopeRunner;Lcom/squareup/time/CurrentTime;Lrx/Scheduler;Lcom/squareup/analytics/Analytics;)V
    .locals 0
    .param p4    # Lrx/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 27
    invoke-direct/range {p0 .. p5}, Lcom/squareup/ui/timecards/TimecardsCoordinator;-><init>(Lcom/squareup/util/Res;Lcom/squareup/ui/timecards/TimecardsScopeRunner;Lcom/squareup/time/CurrentTime;Lrx/Scheduler;Lcom/squareup/analytics/Analytics;)V

    return-void
.end method

.method private static breakEndingLate(JJ)Z
    .locals 1

    cmp-long v0, p0, p2

    if-lez v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private getHeader(JJ)Ljava/lang/String;
    .locals 0

    .line 80
    invoke-static {p1, p2, p3, p4}, Lcom/squareup/ui/timecards/EndBreakConfirmationCoordinator;->breakEndingLate(JJ)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/timecards/EndBreakConfirmationCoordinator;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/ui/timecards/R$string;->employee_management_break_tracking_sheet_title_over:I

    .line 81
    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/timecards/EndBreakConfirmationCoordinator;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/ui/timecards/R$string;->employee_management_break_tracking_sheet_title_not_over:I

    .line 82
    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method private getSubHeader(JJ)Ljava/lang/String;
    .locals 1

    .line 86
    invoke-static {p1, p2, p3, p4}, Lcom/squareup/ui/timecards/EndBreakConfirmationCoordinator;->breakEndingLate(JJ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    iget-object p1, p0, Lcom/squareup/ui/timecards/EndBreakConfirmationCoordinator;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/ui/timecards/R$string;->employee_management_break_tracking_end_break_late_message:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/ui/timecards/EndBreakConfirmationCoordinator;->currentTime:Lcom/squareup/time/CurrentTime;

    .line 88
    invoke-interface {p2}, Lcom/squareup/time/CurrentTime;->zonedDateTime()Lorg/threeten/bp/ZonedDateTime;

    move-result-object p2

    invoke-virtual {p2}, Lorg/threeten/bp/ZonedDateTime;->getZone()Lorg/threeten/bp/ZoneId;

    move-result-object p2

    invoke-static {p2, p3, p4}, Lcom/squareup/ui/timecards/TimeFormatter;->getSentenceTime(Lorg/threeten/bp/ZoneId;J)Ljava/lang/String;

    move-result-object p2

    const-string p3, "break_end_time"

    invoke-virtual {p1, p3, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 90
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 91
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 94
    :cond_0
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, p1, p2}, Ljava/util/Date;-><init>(J)V

    new-instance p1, Ljava/util/Date;

    invoke-direct {p1, p3, p4}, Ljava/util/Date;-><init>(J)V

    invoke-static {v0, p1}, Lcom/squareup/ui/timecards/HoursMinutes;->getDiff(Ljava/util/Date;Ljava/util/Date;)Lcom/squareup/ui/timecards/HoursMinutes;

    move-result-object p1

    .line 96
    iget p2, p1, Lcom/squareup/ui/timecards/HoursMinutes;->minutes:I

    iget p1, p1, Lcom/squareup/ui/timecards/HoursMinutes;->hours:I

    mul-int/lit8 p1, p1, 0x3c

    add-int/2addr p2, p1

    const/4 p1, 0x1

    if-ge p2, p1, :cond_1

    .line 99
    iget-object p1, p0, Lcom/squareup/ui/timecards/EndBreakConfirmationCoordinator;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/ui/timecards/R$string;->employee_management_break_tracking_end_break_sub_one_min_message:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_1
    if-ne p2, p1, :cond_2

    .line 104
    iget-object p1, p0, Lcom/squareup/ui/timecards/EndBreakConfirmationCoordinator;->res:Lcom/squareup/util/Res;

    sget p2, Lcom/squareup/ui/timecards/R$string;->employee_management_break_tracking_end_break_one_min_message:I

    invoke-interface {p1, p2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 107
    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/timecards/EndBreakConfirmationCoordinator;->res:Lcom/squareup/util/Res;

    sget p3, Lcom/squareup/ui/timecards/R$string;->employee_management_break_tracking_end_break_message:I

    invoke-interface {p1, p3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    const-string p3, "mins_left"

    .line 108
    invoke-virtual {p1, p3, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 109
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 110
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 1

    .line 31
    invoke-super {p0, p1}, Lcom/squareup/ui/timecards/TimecardsCoordinator;->attach(Landroid/view/View;)V

    .line 32
    new-instance v0, Lcom/squareup/ui/timecards/-$$Lambda$EndBreakConfirmationCoordinator$Q6odcAlfiVYOPkThbYbZR0zNrKo;

    invoke-direct {v0, p0}, Lcom/squareup/ui/timecards/-$$Lambda$EndBreakConfirmationCoordinator$Q6odcAlfiVYOPkThbYbZR0zNrKo;-><init>(Lcom/squareup/ui/timecards/EndBreakConfirmationCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method protected bindViews(Landroid/view/View;)V
    .locals 2

    .line 118
    invoke-super {p0, p1}, Lcom/squareup/ui/timecards/TimecardsCoordinator;->bindViews(Landroid/view/View;)V

    .line 119
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_end_break_confirmation:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/ui/timecards/EndBreakConfirmationCoordinator;->endBreakContainer:Landroid/widget/LinearLayout;

    .line 120
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakConfirmationCoordinator;->endBreakContainer:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 121
    sget v0, Lcom/squareup/ui/timecards/R$id;->timecards_end_break_confirmation_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marketfont/MarketButton;

    iput-object p1, p0, Lcom/squareup/ui/timecards/EndBreakConfirmationCoordinator;->endBreakButton:Lcom/squareup/marketfont/MarketButton;

    return-void
.end method

.method protected getErrorMessage()Ljava/lang/String;
    .locals 2

    .line 47
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakConfirmationCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/ui/timecards/R$string;->employee_management_break_tracking_general_error_message:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getErrorTitle()Ljava/lang/String;
    .locals 2

    .line 43
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakConfirmationCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/ui/timecards/R$string;->employee_management_break_tracking_general_error_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getSuccessViewEvent()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 39
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->TIMECARDS_SELECT_ACTION_END_BREAK:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public synthetic lambda$attach$0$EndBreakConfirmationCoordinator()Lrx/Subscription;
    .locals 3

    .line 32
    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakConfirmationCoordinator;->timecardsScopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->endBreakConfirmationScreenData()Lrx/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/timecards/EndBreakConfirmationCoordinator;->mainScheduler:Lrx/Scheduler;

    .line 33
    invoke-virtual {v0, v1}, Lrx/Observable;->observeOn(Lrx/Scheduler;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/timecards/-$$Lambda$XaTGPoBsywSTjGscHlpbxnkiK7Y;

    invoke-direct {v1, p0}, Lcom/squareup/ui/timecards/-$$Lambda$XaTGPoBsywSTjGscHlpbxnkiK7Y;-><init>(Lcom/squareup/ui/timecards/EndBreakConfirmationCoordinator;)V

    .line 34
    invoke-virtual {v0, v1}, Lrx/Observable;->doOnSubscribe(Lrx/functions/Action0;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/timecards/-$$Lambda$_P4pUv_V0tnV_OfxBJu1zI0x2uQ;

    invoke-direct {v1, p0}, Lcom/squareup/ui/timecards/-$$Lambda$_P4pUv_V0tnV_OfxBJu1zI0x2uQ;-><init>(Lcom/squareup/ui/timecards/EndBreakConfirmationCoordinator;)V

    new-instance v2, Lcom/squareup/ui/timecards/-$$Lambda$5cS7zncGeEZnqKCwo4IpFOqC3qY;

    invoke-direct {v2, p0}, Lcom/squareup/ui/timecards/-$$Lambda$5cS7zncGeEZnqKCwo4IpFOqC3qY;-><init>(Lcom/squareup/ui/timecards/EndBreakConfirmationCoordinator;)V

    .line 35
    invoke-virtual {v0, v1, v2}, Lrx/Observable;->subscribe(Lrx/functions/Action1;Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$showSuccess$1$EndBreakConfirmationCoordinator(Landroid/view/View;)V
    .locals 0

    .line 70
    iget-object p1, p0, Lcom/squareup/ui/timecards/EndBreakConfirmationCoordinator;->timecardsScopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->onEndBreakEarlyConfirmationClicked()V

    return-void
.end method

.method public synthetic lambda$showSuccess$2$EndBreakConfirmationCoordinator(Landroid/view/View;)V
    .locals 0

    .line 75
    iget-object p1, p0, Lcom/squareup/ui/timecards/EndBreakConfirmationCoordinator;->timecardsScopeRunner:Lcom/squareup/ui/timecards/TimecardsScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/timecards/TimecardsScopeRunner;->onEndBreakConfirmationClicked()V

    return-void
.end method

.method protected showSuccess(Lcom/squareup/ui/timecards/TimecardsScreenData;)V
    .locals 6

    .line 51
    invoke-super {p0, p1}, Lcom/squareup/ui/timecards/TimecardsCoordinator;->showSuccess(Lcom/squareup/ui/timecards/TimecardsScreenData;)V

    .line 52
    move-object v0, p1

    check-cast v0, Lcom/squareup/ui/timecards/EndBreakConfirmationScreen$Data;

    .line 55
    iget-wide v1, v0, Lcom/squareup/ui/timecards/EndBreakConfirmationScreen$Data;->currentTimeMillis:J

    iget-object v3, v0, Lcom/squareup/ui/timecards/EndBreakConfirmationScreen$Data;->expectedBreakEndTimeMillis:Ljava/lang/Long;

    .line 56
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    .line 55
    invoke-direct {p0, v1, v2, v3, v4}, Lcom/squareup/ui/timecards/EndBreakConfirmationCoordinator;->getHeader(JJ)Ljava/lang/String;

    move-result-object v1

    .line 57
    iget-wide v2, v0, Lcom/squareup/ui/timecards/EndBreakConfirmationScreen$Data;->currentTimeMillis:J

    iget-object v4, v0, Lcom/squareup/ui/timecards/EndBreakConfirmationScreen$Data;->expectedBreakEndTimeMillis:Ljava/lang/Long;

    .line 58
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 57
    invoke-direct {p0, v2, v3, v4, v5}, Lcom/squareup/ui/timecards/EndBreakConfirmationCoordinator;->getSubHeader(JJ)Ljava/lang/String;

    move-result-object v2

    .line 59
    iget-object v3, p0, Lcom/squareup/ui/timecards/EndBreakConfirmationCoordinator;->timecardHeader:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v3, v1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 60
    iget-object v1, p0, Lcom/squareup/ui/timecards/EndBreakConfirmationCoordinator;->timecardHeader:Lcom/squareup/marketfont/MarketTextView;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    .line 61
    iget-object v1, p0, Lcom/squareup/ui/timecards/EndBreakConfirmationCoordinator;->timecardSubHeader:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v1, v2}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 62
    iget-object v1, p0, Lcom/squareup/ui/timecards/EndBreakConfirmationCoordinator;->timecardSubHeader:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v1, v3}, Lcom/squareup/marketfont/MarketTextView;->setVisibility(I)V

    .line 64
    sget v1, Lcom/squareup/ui/timecards/R$id;->end_break_confirmation_notes_button:I

    invoke-virtual {p0, p1, v1}, Lcom/squareup/ui/timecards/EndBreakConfirmationCoordinator;->showOrHideNotesButton(Lcom/squareup/ui/timecards/TimecardsScreenData;I)V

    .line 66
    iget-boolean p1, v0, Lcom/squareup/ui/timecards/EndBreakConfirmationScreen$Data;->endAuthorizationNeeded:Z

    if-eqz p1, :cond_0

    .line 67
    iget-object p1, p0, Lcom/squareup/ui/timecards/EndBreakConfirmationCoordinator;->endBreakButton:Lcom/squareup/marketfont/MarketButton;

    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakConfirmationCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/ui/timecards/R$string;->employee_management_break_tracking_end_break_early_button_text:I

    .line 68
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 67
    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketButton;->setText(Ljava/lang/CharSequence;)V

    .line 69
    iget-object p1, p0, Lcom/squareup/ui/timecards/EndBreakConfirmationCoordinator;->endBreakButton:Lcom/squareup/marketfont/MarketButton;

    new-instance v0, Lcom/squareup/ui/timecards/-$$Lambda$EndBreakConfirmationCoordinator$PdNh7iBklKgB1YFBoCAPpCmyISM;

    invoke-direct {v0, p0}, Lcom/squareup/ui/timecards/-$$Lambda$EndBreakConfirmationCoordinator$PdNh7iBklKgB1YFBoCAPpCmyISM;-><init>(Lcom/squareup/ui/timecards/EndBreakConfirmationCoordinator;)V

    .line 70
    invoke-static {v0}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v0

    .line 69
    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 72
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/timecards/EndBreakConfirmationCoordinator;->endBreakButton:Lcom/squareup/marketfont/MarketButton;

    iget-object v0, p0, Lcom/squareup/ui/timecards/EndBreakConfirmationCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/ui/timecards/R$string;->employee_management_break_tracking_end_break_button_text:I

    .line 73
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 72
    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketButton;->setText(Ljava/lang/CharSequence;)V

    .line 74
    iget-object p1, p0, Lcom/squareup/ui/timecards/EndBreakConfirmationCoordinator;->endBreakButton:Lcom/squareup/marketfont/MarketButton;

    new-instance v0, Lcom/squareup/ui/timecards/-$$Lambda$EndBreakConfirmationCoordinator$UUDEAUVngvF7W9PFfpTE2Lw3BZ8;

    invoke-direct {v0, p0}, Lcom/squareup/ui/timecards/-$$Lambda$EndBreakConfirmationCoordinator$UUDEAUVngvF7W9PFfpTE2Lw3BZ8;-><init>(Lcom/squareup/ui/timecards/EndBreakConfirmationCoordinator;)V

    .line 75
    invoke-static {v0}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v0

    .line 74
    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_0
    return-void
.end method
