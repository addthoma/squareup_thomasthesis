.class Lcom/squareup/ui/timecards/Timecards$StartTimecardEvent;
.super Lcom/squareup/ui/timecards/Timecards$Event;
.source "Timecards.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/timecards/Timecards;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "StartTimecardEvent"
.end annotation


# instance fields
.field public final jobToken:Ljava/lang/String;

.field final synthetic this$0:Lcom/squareup/ui/timecards/Timecards;

.field public final unitToken:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/squareup/ui/timecards/Timecards;Ljava/lang/String;)V
    .locals 0

    .line 635
    iput-object p1, p0, Lcom/squareup/ui/timecards/Timecards$StartTimecardEvent;->this$0:Lcom/squareup/ui/timecards/Timecards;

    invoke-direct {p0}, Lcom/squareup/ui/timecards/Timecards$Event;-><init>()V

    .line 636
    iput-object p2, p0, Lcom/squareup/ui/timecards/Timecards$StartTimecardEvent;->unitToken:Ljava/lang/String;

    const/4 p1, 0x0

    .line 637
    iput-object p1, p0, Lcom/squareup/ui/timecards/Timecards$StartTimecardEvent;->jobToken:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Lcom/squareup/ui/timecards/Timecards;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 640
    iput-object p1, p0, Lcom/squareup/ui/timecards/Timecards$StartTimecardEvent;->this$0:Lcom/squareup/ui/timecards/Timecards;

    invoke-direct {p0}, Lcom/squareup/ui/timecards/Timecards$Event;-><init>()V

    .line 641
    iput-object p2, p0, Lcom/squareup/ui/timecards/Timecards$StartTimecardEvent;->unitToken:Ljava/lang/String;

    .line 642
    iput-object p3, p0, Lcom/squareup/ui/timecards/Timecards$StartTimecardEvent;->jobToken:Ljava/lang/String;

    return-void
.end method
