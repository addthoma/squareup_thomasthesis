.class public final Lcom/squareup/ui/timecards/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/timecards/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final bottom_error_text:I = 0x7f0a0242

.field public static final clock_in_button:I = 0x7f0a0344

.field public static final clock_in_clock_glyph:I = 0x7f0a0345

.field public static final clock_in_out_back:I = 0x7f0a0346

.field public static final clock_in_pad:I = 0x7f0a0348

.field public static final clock_in_pad_container:I = 0x7f0a0349

.field public static final clock_in_progress_bar:I = 0x7f0a034a

.field public static final clock_in_server_error:I = 0x7f0a034b

.field public static final clock_in_sheet_message:I = 0x7f0a034c

.field public static final clock_in_sheet_progress_bar:I = 0x7f0a034d

.field public static final clock_in_sheet_success:I = 0x7f0a034e

.field public static final clock_in_sheet_title:I = 0x7f0a034f

.field public static final clock_in_star_group:I = 0x7f0a0350

.field public static final clock_in_success:I = 0x7f0a0351

.field public static final clock_in_title:I = 0x7f0a0352

.field public static final clock_out_success_container:I = 0x7f0a0353

.field public static final clock_out_success_hours:I = 0x7f0a0354

.field public static final clock_out_success_minutes:I = 0x7f0a0355

.field public static final clockout_confirmation_notes_button:I = 0x7f0a035a

.field public static final clockout_summary_view_notes_button:I = 0x7f0a035b

.field public static final continue_button:I = 0x7f0a03a9

.field public static final employee_jobs_button:I = 0x7f0a06d3

.field public static final employee_jobs_list:I = 0x7f0a06d4

.field public static final employee_jobs_list_modal:I = 0x7f0a06d5

.field public static final employee_jobs_list_modal_message:I = 0x7f0a06d6

.field public static final employee_jobs_list_modal_scrollable_view:I = 0x7f0a06d7

.field public static final employee_jobs_list_modal_title:I = 0x7f0a06d8

.field public static final employee_jobs_list_scroll_view:I = 0x7f0a06d9

.field public static final end_break_confirmation_notes_button:I = 0x7f0a0708

.field public static final no_internet_glyph:I = 0x7f0a0a2c

.field public static final start_break_or_clockout_notes_button:I = 0x7f0a0f2b

.field public static final success_continue_button:I = 0x7f0a0f4a

.field public static final success_notes_button:I = 0x7f0a0f4c

.field public static final timecards_action_bar:I = 0x7f0a0fe6

.field public static final timecards_action_bar_primary_button:I = 0x7f0a0fe7

.field public static final timecards_add_or_edit_notes:I = 0x7f0a0fe8

.field public static final timecards_add_or_edit_notes_action_bar:I = 0x7f0a0fe9

.field public static final timecards_add_or_edit_notes_text_view:I = 0x7f0a0fea

.field public static final timecards_clocked_in_green_indicator:I = 0x7f0a0feb

.field public static final timecards_clockin_confirmation:I = 0x7f0a0fec

.field public static final timecards_clockin_confirmation_button:I = 0x7f0a0fed

.field public static final timecards_clockout_button:I = 0x7f0a0fee

.field public static final timecards_clockout_confirmation:I = 0x7f0a0fef

.field public static final timecards_clockout_confirmation_button:I = 0x7f0a0ff0

.field public static final timecards_clockout_summary:I = 0x7f0a0ff1

.field public static final timecards_clockout_summary_data_cell:I = 0x7f0a0ff2

.field public static final timecards_clockout_summary_header_cell:I = 0x7f0a0ff3

.field public static final timecards_clockout_summary_line_separator:I = 0x7f0a0ff4

.field public static final timecards_clockout_summary_placeholder:I = 0x7f0a0ff5

.field public static final timecards_clockout_summary_scroll_view:I = 0x7f0a0ff6

.field public static final timecards_clockout_summary_table:I = 0x7f0a0ff7

.field public static final timecards_container:I = 0x7f0a0ff8

.field public static final timecards_current_time:I = 0x7f0a0ff9

.field public static final timecards_end_break_confirmation:I = 0x7f0a0ffa

.field public static final timecards_end_break_confirmation_button:I = 0x7f0a0ffb

.field public static final timecards_error:I = 0x7f0a0ffc

.field public static final timecards_error_glyph:I = 0x7f0a0ffd

.field public static final timecards_error_message:I = 0x7f0a0ffe

.field public static final timecards_error_title:I = 0x7f0a0fff

.field public static final timecards_header:I = 0x7f0a1000

.field public static final timecards_header_container:I = 0x7f0a1001

.field public static final timecards_list_breaks:I = 0x7f0a1002

.field public static final timecards_list_breaks_button:I = 0x7f0a1003

.field public static final timecards_list_breaks_button_break_duration:I = 0x7f0a1004

.field public static final timecards_list_breaks_button_break_name:I = 0x7f0a1005

.field public static final timecards_loading_progress_bar:I = 0x7f0a1006

.field public static final timecards_modal:I = 0x7f0a1007

.field public static final timecards_modal_clock_glyph:I = 0x7f0a1008

.field public static final timecards_modal_failure:I = 0x7f0a1009

.field public static final timecards_modal_message:I = 0x7f0a100a

.field public static final timecards_modal_primary_button:I = 0x7f0a100b

.field public static final timecards_modal_progress_bar:I = 0x7f0a100c

.field public static final timecards_modal_secondary_button:I = 0x7f0a100d

.field public static final timecards_modal_success:I = 0x7f0a100e

.field public static final timecards_modal_title:I = 0x7f0a100f

.field public static final timecards_primary_button:I = 0x7f0a1010

.field public static final timecards_progress_bar:I = 0x7f0a1011

.field public static final timecards_progress_bar_spinner:I = 0x7f0a1012

.field public static final timecards_progress_bar_title:I = 0x7f0a1013

.field public static final timecards_secondary_button:I = 0x7f0a1014

.field public static final timecards_start_break_button:I = 0x7f0a1015

.field public static final timecards_start_break_or_clockout:I = 0x7f0a1016

.field public static final timecards_sub_header:I = 0x7f0a1017

.field public static final timecards_success:I = 0x7f0a1018

.field public static final timecards_success_button_container:I = 0x7f0a1019

.field public static final timecards_success_button_separator:I = 0x7f0a101a

.field public static final timecards_success_buttons_view:I = 0x7f0a101b

.field public static final timecards_success_buttons_view_summary:I = 0x7f0a101c

.field public static final timecards_success_container:I = 0x7f0a101d

.field public static final timecards_success_continue_button_modal:I = 0x7f0a101e

.field public static final timecards_success_glyph:I = 0x7f0a101f

.field public static final timecards_success_glyph_modal:I = 0x7f0a1020

.field public static final timecards_success_message:I = 0x7f0a1021

.field public static final timecards_success_message_modal:I = 0x7f0a1022

.field public static final timecards_success_modal:I = 0x7f0a1023

.field public static final timecards_success_title:I = 0x7f0a1024

.field public static final timecards_success_title_modal:I = 0x7f0a1025

.field public static final timecards_switch_jobs_button:I = 0x7f0a1026

.field public static final timecards_title_text:I = 0x7f0a1027

.field public static final timecards_title_text_primary:I = 0x7f0a1028

.field public static final timecards_title_text_secondary:I = 0x7f0a1029

.field public static final timecards_up_button:I = 0x7f0a102a

.field public static final timecards_up_button_container:I = 0x7f0a102b

.field public static final timecards_up_button_text:I = 0x7f0a102c

.field public static final timecards_view_notes:I = 0x7f0a102d

.field public static final timecards_view_notes_action_bar:I = 0x7f0a102e

.field public static final timecards_view_notes_container:I = 0x7f0a102f

.field public static final view_notes_entry:I = 0x7f0a10f8

.field public static final view_notes_entry_job_title:I = 0x7f0a10f9

.field public static final view_notes_entry_note_content:I = 0x7f0a10fa


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
