.class Lcom/squareup/ui/timecards/ClockInOutView$1;
.super Lcom/squareup/padlock/Padlock$OnKeyPressListenerAdapter;
.source "ClockInOutView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/timecards/ClockInOutView;->onFinishInflate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/timecards/ClockInOutView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/timecards/ClockInOutView;)V
    .locals 0

    .line 64
    iput-object p1, p0, Lcom/squareup/ui/timecards/ClockInOutView$1;->this$0:Lcom/squareup/ui/timecards/ClockInOutView;

    invoke-direct {p0}, Lcom/squareup/padlock/Padlock$OnKeyPressListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onClearClicked()V
    .locals 1

    .line 71
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutView$1;->this$0:Lcom/squareup/ui/timecards/ClockInOutView;

    iget-object v0, v0, Lcom/squareup/ui/timecards/ClockInOutView;->presenter:Lcom/squareup/ui/timecards/ClockInOutPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/timecards/ClockInOutPresenter;->onClearPressed()V

    return-void
.end method

.method public onClearLongpressed()V
    .locals 1

    .line 75
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutView$1;->this$0:Lcom/squareup/ui/timecards/ClockInOutView;

    iget-object v0, v0, Lcom/squareup/ui/timecards/ClockInOutView;->presenter:Lcom/squareup/ui/timecards/ClockInOutPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/timecards/ClockInOutPresenter;->onClearPressed()V

    return-void
.end method

.method public onDigitClicked(I)V
    .locals 2

    .line 66
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutView$1;->this$0:Lcom/squareup/ui/timecards/ClockInOutView;

    invoke-static {v0}, Lcom/squareup/ui/timecards/ClockInOutView;->access$000(Lcom/squareup/ui/timecards/ClockInOutView;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 67
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutView$1;->this$0:Lcom/squareup/ui/timecards/ClockInOutView;

    iget-object v0, v0, Lcom/squareup/ui/timecards/ClockInOutView;->presenter:Lcom/squareup/ui/timecards/ClockInOutPresenter;

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p1

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/timecards/ClockInOutPresenter;->onKeyPressed(C)V

    return-void
.end method
