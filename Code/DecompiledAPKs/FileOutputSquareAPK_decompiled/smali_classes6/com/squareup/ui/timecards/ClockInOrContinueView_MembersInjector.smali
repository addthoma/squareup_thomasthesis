.class public final Lcom/squareup/ui/timecards/ClockInOrContinueView_MembersInjector;
.super Ljava/lang/Object;
.source "ClockInOrContinueView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/timecards/ClockInOrContinueView;",
        ">;"
    }
.end annotation


# instance fields
.field private final appNameFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/AppNameFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/timecards/ClockInOrContinueScreen$Presenter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/timecards/ClockInOrContinueScreen$Presenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/AppNameFormatter;",
            ">;)V"
        }
    .end annotation

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/ui/timecards/ClockInOrContinueView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p2, p0, Lcom/squareup/ui/timecards/ClockInOrContinueView_MembersInjector;->deviceProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p3, p0, Lcom/squareup/ui/timecards/ClockInOrContinueView_MembersInjector;->appNameFormatterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/timecards/ClockInOrContinueScreen$Presenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/AppNameFormatter;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/timecards/ClockInOrContinueView;",
            ">;"
        }
    .end annotation

    .line 36
    new-instance v0, Lcom/squareup/ui/timecards/ClockInOrContinueView_MembersInjector;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/timecards/ClockInOrContinueView_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectAppNameFormatter(Lcom/squareup/ui/timecards/ClockInOrContinueView;Lcom/squareup/util/AppNameFormatter;)V
    .locals 0

    .line 59
    iput-object p1, p0, Lcom/squareup/ui/timecards/ClockInOrContinueView;->appNameFormatter:Lcom/squareup/util/AppNameFormatter;

    return-void
.end method

.method public static injectDevice(Lcom/squareup/ui/timecards/ClockInOrContinueView;Lcom/squareup/util/Device;)V
    .locals 0

    .line 53
    iput-object p1, p0, Lcom/squareup/ui/timecards/ClockInOrContinueView;->device:Lcom/squareup/util/Device;

    return-void
.end method

.method public static injectPresenter(Lcom/squareup/ui/timecards/ClockInOrContinueView;Lcom/squareup/ui/timecards/ClockInOrContinueScreen$Presenter;)V
    .locals 0

    .line 48
    iput-object p1, p0, Lcom/squareup/ui/timecards/ClockInOrContinueView;->presenter:Lcom/squareup/ui/timecards/ClockInOrContinueScreen$Presenter;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/timecards/ClockInOrContinueView;)V
    .locals 1

    .line 40
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOrContinueView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/timecards/ClockInOrContinueScreen$Presenter;

    invoke-static {p1, v0}, Lcom/squareup/ui/timecards/ClockInOrContinueView_MembersInjector;->injectPresenter(Lcom/squareup/ui/timecards/ClockInOrContinueView;Lcom/squareup/ui/timecards/ClockInOrContinueScreen$Presenter;)V

    .line 41
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOrContinueView_MembersInjector;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Device;

    invoke-static {p1, v0}, Lcom/squareup/ui/timecards/ClockInOrContinueView_MembersInjector;->injectDevice(Lcom/squareup/ui/timecards/ClockInOrContinueView;Lcom/squareup/util/Device;)V

    .line 42
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOrContinueView_MembersInjector;->appNameFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/AppNameFormatter;

    invoke-static {p1, v0}, Lcom/squareup/ui/timecards/ClockInOrContinueView_MembersInjector;->injectAppNameFormatter(Lcom/squareup/ui/timecards/ClockInOrContinueView;Lcom/squareup/util/AppNameFormatter;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 10
    check-cast p1, Lcom/squareup/ui/timecards/ClockInOrContinueView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/timecards/ClockInOrContinueView_MembersInjector;->injectMembers(Lcom/squareup/ui/timecards/ClockInOrContinueView;)V

    return-void
.end method
