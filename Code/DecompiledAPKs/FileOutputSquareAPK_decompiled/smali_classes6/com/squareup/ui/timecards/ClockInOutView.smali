.class public Lcom/squareup/ui/timecards/ClockInOutView;
.super Landroid/widget/FrameLayout;
.source "ClockInOutView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/timecards/ClockInOutView$Component;
    }
.end annotation


# instance fields
.field private backButton:Lcom/squareup/glyph/SquareGlyphView;

.field private bottomErrorText:Landroid/widget/TextView;

.field private clockInSuccessContainer:Landroid/view/View;

.field private clockOutSuccessContainer:Landroid/widget/LinearLayout;

.field private clockOutSuccessHours:Landroid/widget/TextView;

.field private clockOutSuccessMinutes:Landroid/widget/TextView;

.field device:Lcom/squareup/util/Device;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private errorContainer:Landroid/view/View;

.field private padContainer:Landroid/view/ViewGroup;

.field private padlock:Lcom/squareup/padlock/Padlock;

.field private passcodeEnabled:Z

.field presenter:Lcom/squareup/ui/timecards/ClockInOutPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private progressBar:Landroid/widget/ProgressBar;

.field private starGroup:Lcom/squareup/ui/StarGroup;

.field private title:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 54
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 p2, 0x1

    .line 51
    iput-boolean p2, p0, Lcom/squareup/ui/timecards/ClockInOutView;->passcodeEnabled:Z

    .line 55
    const-class p2, Lcom/squareup/ui/timecards/ClockInOutView$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/timecards/ClockInOutView$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/timecards/ClockInOutView$Component;->inject(Lcom/squareup/ui/timecards/ClockInOutView;)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/timecards/ClockInOutView;)Z
    .locals 0

    .line 29
    iget-boolean p0, p0, Lcom/squareup/ui/timecards/ClockInOutView;->passcodeEnabled:Z

    return p0
.end method


# virtual methods
.method incorrectPasscode()V
    .locals 1

    const/4 v0, 0x1

    .line 127
    invoke-virtual {p0, v0}, Lcom/squareup/ui/timecards/ClockInOutView;->setPasscodePadEnabled(Z)V

    .line 128
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutView;->starGroup:Lcom/squareup/ui/StarGroup;

    invoke-virtual {v0}, Lcom/squareup/ui/StarGroup;->wiggleClear()V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 105
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutView;->presenter:Lcom/squareup/ui/timecards/ClockInOutPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/timecards/ClockInOutPresenter;->dropView(Ljava/lang/Object;)V

    .line 106
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 4

    .line 59
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 61
    sget v0, Lcom/squareup/ui/timecards/R$id;->clock_in_pad:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/padlock/Padlock;

    iput-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutView;->padlock:Lcom/squareup/padlock/Padlock;

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutView;->padlock:Lcom/squareup/padlock/Padlock;

    invoke-virtual {p0}, Lcom/squareup/ui/timecards/ClockInOutView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/squareup/marketfont/MarketFont$Weight;->LIGHT:Lcom/squareup/marketfont/MarketFont$Weight;

    const/4 v3, 0x0

    invoke-static {v1, v2, v3, v3}, Lcom/squareup/marketfont/MarketTypeface;->getTypeface(Landroid/content/Context;Lcom/squareup/marketfont/MarketFont$Weight;ZZ)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/padlock/Padlock;->setTypeface(Landroid/graphics/Typeface;)V

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutView;->padlock:Lcom/squareup/padlock/Padlock;

    invoke-virtual {v0, v3}, Lcom/squareup/padlock/Padlock;->setClearEnabled(Z)V

    .line 64
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutView;->padlock:Lcom/squareup/padlock/Padlock;

    new-instance v1, Lcom/squareup/ui/timecards/ClockInOutView$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/timecards/ClockInOutView$1;-><init>(Lcom/squareup/ui/timecards/ClockInOutView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/padlock/Padlock;->setOnKeyPressListener(Lcom/squareup/padlock/Padlock$OnKeyPressListener;)V

    .line 78
    sget v0, Lcom/squareup/ui/timecards/R$id;->clock_in_star_group:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/StarGroup;

    iput-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutView;->starGroup:Lcom/squareup/ui/StarGroup;

    .line 79
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutView;->starGroup:Lcom/squareup/ui/StarGroup;

    invoke-virtual {p0}, Lcom/squareup/ui/timecards/ClockInOutView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/squareup/marin/R$color;->marin_white:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/StarGroup;->setCorrectColor(I)V

    .line 81
    sget v0, Lcom/squareup/ui/timecards/R$id;->clock_in_out_back:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/glyph/SquareGlyphView;

    iput-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutView;->backButton:Lcom/squareup/glyph/SquareGlyphView;

    .line 82
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutView;->presenter:Lcom/squareup/ui/timecards/ClockInOutPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/timecards/ClockInOutPresenter;->showBackButton()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 83
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutView;->backButton:Lcom/squareup/glyph/SquareGlyphView;

    new-instance v1, Lcom/squareup/ui/timecards/ClockInOutView$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/timecards/ClockInOutView$2;-><init>(Lcom/squareup/ui/timecards/ClockInOutView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 88
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutView;->backButton:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {v0, v3}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    .line 91
    :cond_0
    sget v0, Lcom/squareup/ui/timecards/R$id;->clock_in_progress_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutView;->progressBar:Landroid/widget/ProgressBar;

    .line 92
    sget v0, Lcom/squareup/ui/timecards/R$id;->clock_in_pad_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutView;->padContainer:Landroid/view/ViewGroup;

    .line 93
    sget v0, Lcom/squareup/ui/timecards/R$id;->clock_in_title:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutView;->title:Landroid/widget/TextView;

    .line 94
    sget v0, Lcom/squareup/ui/timecards/R$id;->clock_in_success:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutView;->clockInSuccessContainer:Landroid/view/View;

    .line 95
    sget v0, Lcom/squareup/ui/timecards/R$id;->clock_out_success_container:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutView;->clockOutSuccessContainer:Landroid/widget/LinearLayout;

    .line 96
    sget v0, Lcom/squareup/ui/timecards/R$id;->clock_out_success_hours:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutView;->clockOutSuccessHours:Landroid/widget/TextView;

    .line 97
    sget v0, Lcom/squareup/ui/timecards/R$id;->clock_out_success_minutes:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutView;->clockOutSuccessMinutes:Landroid/widget/TextView;

    .line 98
    sget v0, Lcom/squareup/ui/timecards/R$id;->clock_in_server_error:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutView;->errorContainer:Landroid/view/View;

    .line 99
    sget v0, Lcom/squareup/ui/timecards/R$id;->bottom_error_text:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutView;->bottomErrorText:Landroid/widget/TextView;

    .line 101
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutView;->presenter:Lcom/squareup/ui/timecards/ClockInOutPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/timecards/ClockInOutPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public resetView()V
    .locals 2

    .line 179
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutView;->padContainer:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 180
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutView;->title:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    const/4 v0, 0x1

    .line 181
    invoke-virtual {p0, v0}, Lcom/squareup/ui/timecards/ClockInOutView;->setPasscodePadEnabled(Z)V

    .line 182
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutView;->errorContainer:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method setClearEnabled(Z)V
    .locals 1

    .line 118
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutView;->padlock:Lcom/squareup/padlock/Padlock;

    invoke-virtual {v0, p1}, Lcom/squareup/padlock/Padlock;->setClearEnabled(Z)V

    return-void
.end method

.method setExpectedStars(I)V
    .locals 1

    .line 110
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutView;->starGroup:Lcom/squareup/ui/StarGroup;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/StarGroup;->setExpectedStarCount(I)V

    return-void
.end method

.method public setGlyphX()V
    .locals 3

    .line 173
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutView;->backButton:Lcom/squareup/glyph/SquareGlyphView;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X_LARGE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    .line 174
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutView;->backButton:Lcom/squareup/glyph/SquareGlyphView;

    invoke-virtual {p0}, Lcom/squareup/ui/timecards/ClockInOutView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/squareup/containerconstants/R$string;->content_description_close:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void
.end method

.method setPasscodePadEnabled(Z)V
    .locals 1

    .line 132
    iput-boolean p1, p0, Lcom/squareup/ui/timecards/ClockInOutView;->passcodeEnabled:Z

    .line 133
    iget-object p1, p0, Lcom/squareup/ui/timecards/ClockInOutView;->padlock:Lcom/squareup/padlock/Padlock;

    iget-boolean v0, p0, Lcom/squareup/ui/timecards/ClockInOutView;->passcodeEnabled:Z

    invoke-virtual {p1, v0}, Lcom/squareup/padlock/Padlock;->setDigitsEnabled(Z)V

    return-void
.end method

.method setStarCount(I)V
    .locals 1

    .line 114
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutView;->starGroup:Lcom/squareup/ui/StarGroup;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/StarGroup;->setStarCount(I)V

    return-void
.end method

.method public showClockInSuccess(Ljava/lang/String;)V
    .locals 1

    .line 143
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutView;->title:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 144
    iget-object p1, p0, Lcom/squareup/ui/timecards/ClockInOutView;->progressBar:Landroid/widget/ProgressBar;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 145
    iget-object p1, p0, Lcom/squareup/ui/timecards/ClockInOutView;->clockInSuccessContainer:Landroid/view/View;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public showClockOutSuccess(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 150
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutView;->title:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 151
    iget-object p1, p0, Lcom/squareup/ui/timecards/ClockInOutView;->progressBar:Landroid/widget/ProgressBar;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 153
    iget-object p1, p0, Lcom/squareup/ui/timecards/ClockInOutView;->device:Lcom/squareup/util/Device;

    invoke-interface {p1}, Lcom/squareup/util/Device;->isPhoneOrPortraitLessThan10Inches()Z

    move-result p1

    .line 154
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutView;->clockOutSuccessContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 155
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutView;->clockOutSuccessContainer:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 156
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutView;->clockOutSuccessHours:Landroid/widget/TextView;

    if-eqz p1, :cond_0

    move-object p1, p2

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " "

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :goto_0
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 157
    iget-object p1, p0, Lcom/squareup/ui/timecards/ClockInOutView;->clockOutSuccessHours:Landroid/widget/TextView;

    invoke-static {p2}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p2

    xor-int/lit8 p2, p2, 0x1

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 158
    iget-object p1, p0, Lcom/squareup/ui/timecards/ClockInOutView;->clockOutSuccessMinutes:Landroid/widget/TextView;

    invoke-virtual {p1, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public showError(Z)V
    .locals 2

    .line 162
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutView;->padContainer:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 163
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutView;->title:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 164
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutView;->progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 165
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutView;->bottomErrorText:Landroid/widget/TextView;

    if-eqz p1, :cond_0

    .line 166
    invoke-virtual {p0}, Lcom/squareup/ui/timecards/ClockInOutView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v1, Lcom/squareup/ui/timecards/R$string;->timecard_error_no_internet_connection:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 167
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/timecards/ClockInOutView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v1, Lcom/squareup/ui/timecards/R$string;->timecard_error_title:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 165
    :goto_0
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 169
    iget-object p1, p0, Lcom/squareup/ui/timecards/ClockInOutView;->errorContainer:Landroid/view/View;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public showProgressBar(Ljava/lang/String;)V
    .locals 2

    .line 137
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutView;->padContainer:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 138
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutView;->progressBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 139
    iget-object v0, p0, Lcom/squareup/ui/timecards/ClockInOutView;->title:Landroid/widget/TextView;

    const-string v1, "message"

    invoke-static {p1, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public updateStars(I)V
    .locals 0

    .line 122
    invoke-virtual {p0, p1}, Lcom/squareup/ui/timecards/ClockInOutView;->setStarCount(I)V

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 123
    :goto_0
    invoke-virtual {p0, p1}, Lcom/squareup/ui/timecards/ClockInOutView;->setClearEnabled(Z)V

    return-void
.end method
