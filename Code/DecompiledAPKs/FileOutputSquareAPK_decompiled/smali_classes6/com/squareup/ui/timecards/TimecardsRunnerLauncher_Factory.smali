.class public final Lcom/squareup/ui/timecards/TimecardsRunnerLauncher_Factory;
.super Ljava/lang/Object;
.source "TimecardsRunnerLauncher_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/timecards/TimecardsRunnerLauncher;",
        ">;"
    }
.end annotation


# instance fields
.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final homeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/Home;",
            ">;"
        }
    .end annotation
.end field

.field private final mainContainerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/Home;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;)V"
        }
    .end annotation

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/squareup/ui/timecards/TimecardsRunnerLauncher_Factory;->homeProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p2, p0, Lcom/squareup/ui/timecards/TimecardsRunnerLauncher_Factory;->mainContainerProvider:Ljavax/inject/Provider;

    .line 31
    iput-object p3, p0, Lcom/squareup/ui/timecards/TimecardsRunnerLauncher_Factory;->flowProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/timecards/TimecardsRunnerLauncher_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/Home;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;)",
            "Lcom/squareup/ui/timecards/TimecardsRunnerLauncher_Factory;"
        }
    .end annotation

    .line 41
    new-instance v0, Lcom/squareup/ui/timecards/TimecardsRunnerLauncher_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/timecards/TimecardsRunnerLauncher_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/ui/main/Home;Lcom/squareup/ui/main/PosContainer;Ldagger/Lazy;)Lcom/squareup/ui/timecards/TimecardsRunnerLauncher;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/main/Home;",
            "Lcom/squareup/ui/main/PosContainer;",
            "Ldagger/Lazy<",
            "Lflow/Flow;",
            ">;)",
            "Lcom/squareup/ui/timecards/TimecardsRunnerLauncher;"
        }
    .end annotation

    .line 46
    new-instance v0, Lcom/squareup/ui/timecards/TimecardsRunnerLauncher;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/timecards/TimecardsRunnerLauncher;-><init>(Lcom/squareup/ui/main/Home;Lcom/squareup/ui/main/PosContainer;Ldagger/Lazy;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/timecards/TimecardsRunnerLauncher;
    .locals 3

    .line 36
    iget-object v0, p0, Lcom/squareup/ui/timecards/TimecardsRunnerLauncher_Factory;->homeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/main/Home;

    iget-object v1, p0, Lcom/squareup/ui/timecards/TimecardsRunnerLauncher_Factory;->mainContainerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/main/PosContainer;

    iget-object v2, p0, Lcom/squareup/ui/timecards/TimecardsRunnerLauncher_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-static {v2}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/squareup/ui/timecards/TimecardsRunnerLauncher_Factory;->newInstance(Lcom/squareup/ui/main/Home;Lcom/squareup/ui/main/PosContainer;Ldagger/Lazy;)Lcom/squareup/ui/timecards/TimecardsRunnerLauncher;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/ui/timecards/TimecardsRunnerLauncher_Factory;->get()Lcom/squareup/ui/timecards/TimecardsRunnerLauncher;

    move-result-object v0

    return-object v0
.end method
