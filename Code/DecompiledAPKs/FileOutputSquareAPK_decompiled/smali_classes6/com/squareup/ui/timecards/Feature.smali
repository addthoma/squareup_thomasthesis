.class final enum Lcom/squareup/ui/timecards/Feature;
.super Ljava/lang/Enum;
.source "FeatureReleaseHelper.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/timecards/Feature;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/timecards/Feature;

.field public static final enum MANDATORY_BREAK_COMPLETION:Lcom/squareup/ui/timecards/Feature;

.field public static final enum MULTIPLE_WAGES_CLOCK_OUT_SUMMARY:Lcom/squareup/ui/timecards/Feature;

.field public static final enum MULTIPLE_WAGES_JOBS_SELECT:Lcom/squareup/ui/timecards/Feature;

.field public static final enum MULTIPLE_WAGES_SWITCH_JOBS:Lcom/squareup/ui/timecards/Feature;

.field public static final enum RECEIPT_SUMMARY:Lcom/squareup/ui/timecards/Feature;

.field public static final enum TIMECARD_NOTES:Lcom/squareup/ui/timecards/Feature;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 81
    new-instance v0, Lcom/squareup/ui/timecards/Feature;

    const/4 v1, 0x0

    const-string v2, "MULTIPLE_WAGES_JOBS_SELECT"

    invoke-direct {v0, v2, v1}, Lcom/squareup/ui/timecards/Feature;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/timecards/Feature;->MULTIPLE_WAGES_JOBS_SELECT:Lcom/squareup/ui/timecards/Feature;

    .line 82
    new-instance v0, Lcom/squareup/ui/timecards/Feature;

    const/4 v2, 0x1

    const-string v3, "MULTIPLE_WAGES_SWITCH_JOBS"

    invoke-direct {v0, v3, v2}, Lcom/squareup/ui/timecards/Feature;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/timecards/Feature;->MULTIPLE_WAGES_SWITCH_JOBS:Lcom/squareup/ui/timecards/Feature;

    .line 83
    new-instance v0, Lcom/squareup/ui/timecards/Feature;

    const/4 v3, 0x2

    const-string v4, "MULTIPLE_WAGES_CLOCK_OUT_SUMMARY"

    invoke-direct {v0, v4, v3}, Lcom/squareup/ui/timecards/Feature;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/timecards/Feature;->MULTIPLE_WAGES_CLOCK_OUT_SUMMARY:Lcom/squareup/ui/timecards/Feature;

    .line 84
    new-instance v0, Lcom/squareup/ui/timecards/Feature;

    const/4 v4, 0x3

    const-string v5, "RECEIPT_SUMMARY"

    invoke-direct {v0, v5, v4}, Lcom/squareup/ui/timecards/Feature;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/timecards/Feature;->RECEIPT_SUMMARY:Lcom/squareup/ui/timecards/Feature;

    .line 85
    new-instance v0, Lcom/squareup/ui/timecards/Feature;

    const/4 v5, 0x4

    const-string v6, "MANDATORY_BREAK_COMPLETION"

    invoke-direct {v0, v6, v5}, Lcom/squareup/ui/timecards/Feature;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/timecards/Feature;->MANDATORY_BREAK_COMPLETION:Lcom/squareup/ui/timecards/Feature;

    .line 86
    new-instance v0, Lcom/squareup/ui/timecards/Feature;

    const/4 v6, 0x5

    const-string v7, "TIMECARD_NOTES"

    invoke-direct {v0, v7, v6}, Lcom/squareup/ui/timecards/Feature;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/timecards/Feature;->TIMECARD_NOTES:Lcom/squareup/ui/timecards/Feature;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/squareup/ui/timecards/Feature;

    .line 80
    sget-object v7, Lcom/squareup/ui/timecards/Feature;->MULTIPLE_WAGES_JOBS_SELECT:Lcom/squareup/ui/timecards/Feature;

    aput-object v7, v0, v1

    sget-object v1, Lcom/squareup/ui/timecards/Feature;->MULTIPLE_WAGES_SWITCH_JOBS:Lcom/squareup/ui/timecards/Feature;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/timecards/Feature;->MULTIPLE_WAGES_CLOCK_OUT_SUMMARY:Lcom/squareup/ui/timecards/Feature;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/ui/timecards/Feature;->RECEIPT_SUMMARY:Lcom/squareup/ui/timecards/Feature;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/ui/timecards/Feature;->MANDATORY_BREAK_COMPLETION:Lcom/squareup/ui/timecards/Feature;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/ui/timecards/Feature;->TIMECARD_NOTES:Lcom/squareup/ui/timecards/Feature;

    aput-object v1, v0, v6

    sput-object v0, Lcom/squareup/ui/timecards/Feature;->$VALUES:[Lcom/squareup/ui/timecards/Feature;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 80
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/timecards/Feature;
    .locals 1

    .line 80
    const-class v0, Lcom/squareup/ui/timecards/Feature;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/timecards/Feature;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/timecards/Feature;
    .locals 1

    .line 80
    sget-object v0, Lcom/squareup/ui/timecards/Feature;->$VALUES:[Lcom/squareup/ui/timecards/Feature;

    invoke-virtual {v0}, [Lcom/squareup/ui/timecards/Feature;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/timecards/Feature;

    return-object v0
.end method
