.class public Lcom/squareup/ui/permissions/PermissionDeniedScreen$Factory;
.super Ljava/lang/Object;
.source "PermissionDeniedScreen.java"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/permissions/PermissionDeniedScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private createContentView(Landroid/content/Context;)Lcom/squareup/ui/permissions/PermissionDeniedView;
    .locals 2

    .line 66
    sget v0, Lcom/squareup/ui/permissions/R$layout;->permission_denied_view:I

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/permissions/PermissionDeniedView;

    return-object p1
.end method

.method static synthetic lambda$create$0(Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;Landroid/content/DialogInterface;)V
    .locals 0

    const/4 p1, 0x0

    .line 51
    invoke-static {p0, p1}, Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;->access$000(Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;Z)V

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    .line 46
    const-class v0, Lcom/squareup/ui/permissions/PermissionDeniedScreen$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/permissions/PermissionDeniedScreen$Component;

    .line 47
    invoke-direct {p0, p1}, Lcom/squareup/ui/permissions/PermissionDeniedScreen$Factory;->createContentView(Landroid/content/Context;)Lcom/squareup/ui/permissions/PermissionDeniedView;

    move-result-object v1

    .line 48
    invoke-interface {v0, v1}, Lcom/squareup/ui/permissions/PermissionDeniedScreen$Component;->inject(Lcom/squareup/ui/permissions/PermissionDeniedView;)V

    .line 49
    invoke-interface {v0}, Lcom/squareup/ui/permissions/PermissionDeniedScreen$Component;->presenter()Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;

    move-result-object v0

    .line 51
    new-instance v2, Lcom/squareup/ui/permissions/-$$Lambda$PermissionDeniedScreen$Factory$LIn1y_LHeaRYhLsLkkrCQhZWna8;

    invoke-direct {v2, v0}, Lcom/squareup/ui/permissions/-$$Lambda$PermissionDeniedScreen$Factory$LIn1y_LHeaRYhLsLkkrCQhZWna8;-><init>(Lcom/squareup/ui/permissions/PermissionDeniedScreen$Presenter;)V

    .line 53
    new-instance v0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v0, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 p1, 0x1

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setCancelable(Z)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 54
    invoke-virtual {p1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 55
    invoke-virtual {p1, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setView(Landroid/view/View;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 56
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    const/4 v0, 0x0

    .line 57
    invoke-virtual {p1, v0}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 58
    invoke-virtual {p1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, -0x2

    .line 59
    invoke-virtual {v0, v1, v1}, Landroid/view/Window;->setLayout(II)V

    .line 62
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
