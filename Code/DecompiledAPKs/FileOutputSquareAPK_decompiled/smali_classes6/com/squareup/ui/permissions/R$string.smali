.class public final Lcom/squareup/ui/permissions/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/permissions/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final access_denied:I = 0x7f12002a

.field public static final access_denied_message:I = 0x7f12002b

.field public static final employee_management_clock_in_out:I = 0x7f120a04

.field public static final employee_management_guest_button:I = 0x7f120a14

.field public static final enter_passcode:I = 0x7f120a7c

.field public static final enter_passcode_account_owner:I = 0x7f120a7d

.field public static final enter_passcode_account_owner_or_admin:I = 0x7f120a7e

.field public static final legacy_guest_mode_expiration_dialog_button_text:I = 0x7f120eba

.field public static final legacy_guest_mode_expiration_dialog_message:I = 0x7f120ebb

.field public static final legacy_guest_mode_expiration_dialog_title:I = 0x7f120ebc


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
