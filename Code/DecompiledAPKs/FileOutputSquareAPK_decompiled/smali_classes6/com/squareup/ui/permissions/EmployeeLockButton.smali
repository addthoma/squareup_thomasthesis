.class public Lcom/squareup/ui/permissions/EmployeeLockButton;
.super Landroid/widget/FrameLayout;
.source "EmployeeLockButton.java"

# interfaces
.implements Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter$LockButtonView;


# instance fields
.field device:Lcom/squareup/util/Device;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private glyphView:Lcom/squareup/glyph/SquareGlyphView;

.field presenter:Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private textColor:I

.field private textView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .line 35
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    const-class v0, Lcom/squareup/orderentry/OrderEntryScreen$BaseComponent;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orderentry/OrderEntryScreen$BaseComponent;

    invoke-interface {v0, p0}, Lcom/squareup/orderentry/OrderEntryScreen$BaseComponent;->inject(Lcom/squareup/ui/permissions/EmployeeLockButton;)V

    .line 38
    sget-object v0, Lcom/squareup/orderentry/R$styleable;->EmployeeLockButton:[I

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object p1

    .line 39
    sget p2, Lcom/squareup/orderentry/R$styleable;->EmployeeLockButton_android_textColor:I

    const/high16 v0, -0x1000000

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result p2

    iput p2, p0, Lcom/squareup/ui/permissions/EmployeeLockButton;->textColor:I

    .line 40
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method


# virtual methods
.method public asView()Landroid/view/View;
    .locals 0

    return-object p0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 60
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/permissions/EmployeeLockButton;->presenter:Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/permissions/EmployeeLockButton;->presenter:Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/permissions/EmployeeLockButtonPresenter;->dropView(Ljava/lang/Object;)V

    .line 66
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 44
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 46
    invoke-virtual {p0}, Lcom/squareup/ui/permissions/EmployeeLockButton;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/squareup/orderentry/R$layout;->employee_lock_button:I

    invoke-static {v0, v1, p0}, Lcom/squareup/ui/permissions/EmployeeLockButton;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 48
    sget v0, Lcom/squareup/orderentry/R$id;->employee_lock_button_text:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/permissions/EmployeeLockButton;->textView:Landroid/widget/TextView;

    .line 49
    iget-object v0, p0, Lcom/squareup/ui/permissions/EmployeeLockButton;->textView:Landroid/widget/TextView;

    iget v1, p0, Lcom/squareup/ui/permissions/EmployeeLockButton;->textColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 50
    sget v0, Lcom/squareup/orderentry/R$id;->employee_lock_button_glyph:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/glyph/SquareGlyphView;

    iput-object v0, p0, Lcom/squareup/ui/permissions/EmployeeLockButton;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    .line 52
    new-instance v0, Lcom/squareup/ui/permissions/EmployeeLockButton$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/permissions/EmployeeLockButton$1;-><init>(Lcom/squareup/ui/permissions/EmployeeLockButton;)V

    invoke-virtual {p0, v0}, Lcom/squareup/ui/permissions/EmployeeLockButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setDescription(Ljava/lang/String;)V
    .locals 0

    .line 78
    invoke-virtual {p0, p1}, Lcom/squareup/ui/permissions/EmployeeLockButton;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public showButton()V
    .locals 1

    const/4 v0, 0x0

    .line 74
    invoke-virtual {p0, v0}, Lcom/squareup/ui/permissions/EmployeeLockButton;->setVisibility(I)V

    return-void
.end method

.method public showClock()V
    .locals 2

    .line 82
    iget-object v0, p0, Lcom/squareup/ui/permissions/EmployeeLockButton;->textView:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 83
    iget-object v0, p0, Lcom/squareup/ui/permissions/EmployeeLockButton;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    .line 84
    iget-object v0, p0, Lcom/squareup/ui/permissions/EmployeeLockButton;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->STOPWATCH:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    return-void
.end method

.method public showLegacyGuest()V
    .locals 2

    .line 95
    iget-object v0, p0, Lcom/squareup/ui/permissions/EmployeeLockButton;->textView:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 96
    iget-object v0, p0, Lcom/squareup/ui/permissions/EmployeeLockButton;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    .line 97
    iget-object v0, p0, Lcom/squareup/ui/permissions/EmployeeLockButton;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->PERSON:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Z

    return-void
.end method

.method public showLegacyLoginOut(Ljava/lang/String;)V
    .locals 2

    .line 101
    iget-object v0, p0, Lcom/squareup/ui/permissions/EmployeeLockButton;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    .line 102
    iget-object v0, p0, Lcom/squareup/ui/permissions/EmployeeLockButton;->textView:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 103
    iget-object v0, p0, Lcom/squareup/ui/permissions/EmployeeLockButton;->textView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 104
    iget-object p1, p0, Lcom/squareup/ui/permissions/EmployeeLockButton;->textView:Landroid/widget/TextView;

    const/16 v0, 0x11

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setGravity(I)V

    return-void
.end method

.method public showLoginOut(Ljava/lang/String;)V
    .locals 2

    .line 88
    iget-object v0, p0, Lcom/squareup/ui/permissions/EmployeeLockButton;->textView:Landroid/widget/TextView;

    const v1, 0x800005

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 89
    iget-object v0, p0, Lcom/squareup/ui/permissions/EmployeeLockButton;->glyphView:Lcom/squareup/glyph/SquareGlyphView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphView;->setVisibility(I)V

    .line 90
    iget-object v0, p0, Lcom/squareup/ui/permissions/EmployeeLockButton;->textView:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 91
    iget-object v0, p0, Lcom/squareup/ui/permissions/EmployeeLockButton;->textView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
