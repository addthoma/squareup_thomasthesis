.class public Lcom/squareup/ui/ticket/EditSplitTicketView;
.super Lcom/squareup/ui/ticket/EditTicketView;
.source "EditSplitTicketView.java"

# interfaces
.implements Lcom/squareup/workflow/ui/HandlesBack;
.implements Lcom/squareup/container/spot/HasSpot;
.implements Lcom/squareup/ui/HasActionBar;


# instance fields
.field presenter:Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 25
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/ticket/EditTicketView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    const-class p2, Lcom/squareup/ui/ticket/EditSplitTicketScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/ticket/EditSplitTicketScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/ticket/EditSplitTicketScreen$Component;->inject(Lcom/squareup/ui/ticket/EditSplitTicketView;)V

    return-void
.end method


# virtual methods
.method public getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 35
    invoke-static {p0}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 39
    invoke-static {p1}, Lcom/squareup/container/ContainerKt;->getDevice(Landroid/content/Context;)Lcom/squareup/util/Device;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/util/Device;->isTablet()Z

    move-result p1

    if-eqz p1, :cond_0

    sget-object p1, Lcom/squareup/container/spot/Spots;->GROW_OVER:Lcom/squareup/container/spot/Spot;

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/squareup/container/spot/Spots;->BELOW:Lcom/squareup/container/spot/Spot;

    :goto_0
    return-object p1
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 43
    invoke-super {p0}, Lcom/squareup/ui/ticket/EditTicketView;->onAttachedToWindow()V

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/ticket/EditSplitTicketView;->presenter:Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/squareup/ui/ticket/EditSplitTicketView;->presenter:Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->onCancelSelected()V

    const/4 v0, 0x1

    return v0
.end method

.method protected onConvertToCustomTicketClicked()V
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/ticket/EditSplitTicketView;->presenter:Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->onConvertToCustomTicketClicked()V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/ticket/EditSplitTicketView;->presenter:Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->dropView(Ljava/lang/Object;)V

    .line 49
    invoke-super {p0}, Lcom/squareup/ui/ticket/EditTicketView;->onDetachedFromWindow()V

    return-void
.end method

.method protected onTicketNameChanged()V
    .locals 1

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/ticket/EditSplitTicketView;->presenter:Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/EditSplitTicketScreen$EditSplitTicketPresenter;->onTicketNameChanged()V

    return-void
.end method
