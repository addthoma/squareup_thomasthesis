.class final enum Lcom/squareup/ui/ticket/GroupListView$GroupViewHolder;
.super Ljava/lang/Enum;
.source "GroupListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/GroupListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "GroupViewHolder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/ticket/GroupListView$GroupViewHolder;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/ticket/GroupListView$GroupViewHolder;

.field public static final enum ALL_TICKETS_HOLDER:Lcom/squareup/ui/ticket/GroupListView$GroupViewHolder;

.field public static final enum CUSTOM_TICKETS_HOLDER:Lcom/squareup/ui/ticket/GroupListView$GroupViewHolder;

.field public static final enum GROUP_HOLDER:Lcom/squareup/ui/ticket/GroupListView$GroupViewHolder;

.field public static final enum SEARCH_HOLDER:Lcom/squareup/ui/ticket/GroupListView$GroupViewHolder;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 65
    new-instance v0, Lcom/squareup/ui/ticket/GroupListView$GroupViewHolder;

    const/4 v1, 0x0

    const-string v2, "SEARCH_HOLDER"

    invoke-direct {v0, v2, v1}, Lcom/squareup/ui/ticket/GroupListView$GroupViewHolder;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/ticket/GroupListView$GroupViewHolder;->SEARCH_HOLDER:Lcom/squareup/ui/ticket/GroupListView$GroupViewHolder;

    .line 66
    new-instance v0, Lcom/squareup/ui/ticket/GroupListView$GroupViewHolder;

    const/4 v2, 0x1

    const-string v3, "ALL_TICKETS_HOLDER"

    invoke-direct {v0, v3, v2}, Lcom/squareup/ui/ticket/GroupListView$GroupViewHolder;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/ticket/GroupListView$GroupViewHolder;->ALL_TICKETS_HOLDER:Lcom/squareup/ui/ticket/GroupListView$GroupViewHolder;

    .line 67
    new-instance v0, Lcom/squareup/ui/ticket/GroupListView$GroupViewHolder;

    const/4 v3, 0x2

    const-string v4, "CUSTOM_TICKETS_HOLDER"

    invoke-direct {v0, v4, v3}, Lcom/squareup/ui/ticket/GroupListView$GroupViewHolder;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/ticket/GroupListView$GroupViewHolder;->CUSTOM_TICKETS_HOLDER:Lcom/squareup/ui/ticket/GroupListView$GroupViewHolder;

    .line 68
    new-instance v0, Lcom/squareup/ui/ticket/GroupListView$GroupViewHolder;

    const/4 v4, 0x3

    const-string v5, "GROUP_HOLDER"

    invoke-direct {v0, v5, v4}, Lcom/squareup/ui/ticket/GroupListView$GroupViewHolder;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/squareup/ui/ticket/GroupListView$GroupViewHolder;->GROUP_HOLDER:Lcom/squareup/ui/ticket/GroupListView$GroupViewHolder;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/ui/ticket/GroupListView$GroupViewHolder;

    .line 64
    sget-object v5, Lcom/squareup/ui/ticket/GroupListView$GroupViewHolder;->SEARCH_HOLDER:Lcom/squareup/ui/ticket/GroupListView$GroupViewHolder;

    aput-object v5, v0, v1

    sget-object v1, Lcom/squareup/ui/ticket/GroupListView$GroupViewHolder;->ALL_TICKETS_HOLDER:Lcom/squareup/ui/ticket/GroupListView$GroupViewHolder;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/ticket/GroupListView$GroupViewHolder;->CUSTOM_TICKETS_HOLDER:Lcom/squareup/ui/ticket/GroupListView$GroupViewHolder;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/ui/ticket/GroupListView$GroupViewHolder;->GROUP_HOLDER:Lcom/squareup/ui/ticket/GroupListView$GroupViewHolder;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/ui/ticket/GroupListView$GroupViewHolder;->$VALUES:[Lcom/squareup/ui/ticket/GroupListView$GroupViewHolder;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 64
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/ticket/GroupListView$GroupViewHolder;
    .locals 1

    .line 64
    const-class v0, Lcom/squareup/ui/ticket/GroupListView$GroupViewHolder;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/ticket/GroupListView$GroupViewHolder;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/ticket/GroupListView$GroupViewHolder;
    .locals 1

    .line 64
    sget-object v0, Lcom/squareup/ui/ticket/GroupListView$GroupViewHolder;->$VALUES:[Lcom/squareup/ui/ticket/GroupListView$GroupViewHolder;

    invoke-virtual {v0}, [Lcom/squareup/ui/ticket/GroupListView$GroupViewHolder;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/ticket/GroupListView$GroupViewHolder;

    return-object v0
.end method
