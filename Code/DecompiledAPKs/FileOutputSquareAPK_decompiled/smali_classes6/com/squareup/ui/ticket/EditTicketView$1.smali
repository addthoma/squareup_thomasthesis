.class synthetic Lcom/squareup/ui/ticket/EditTicketView$1;
.super Ljava/lang/Object;
.source "EditTicketView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/EditTicketView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$squareup$ui$ticket$EditTicketView$HolderType:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 303
    invoke-static {}, Lcom/squareup/ui/ticket/EditTicketView$HolderType;->values()[Lcom/squareup/ui/ticket/EditTicketView$HolderType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/ui/ticket/EditTicketView$1;->$SwitchMap$com$squareup$ui$ticket$EditTicketView$HolderType:[I

    :try_start_0
    sget-object v0, Lcom/squareup/ui/ticket/EditTicketView$1;->$SwitchMap$com$squareup$ui$ticket$EditTicketView$HolderType:[I

    sget-object v1, Lcom/squareup/ui/ticket/EditTicketView$HolderType;->HEADER_ROW_HOLDER:Lcom/squareup/ui/ticket/EditTicketView$HolderType;

    invoke-virtual {v1}, Lcom/squareup/ui/ticket/EditTicketView$HolderType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :try_start_1
    sget-object v0, Lcom/squareup/ui/ticket/EditTicketView$1;->$SwitchMap$com$squareup$ui$ticket$EditTicketView$HolderType:[I

    sget-object v1, Lcom/squareup/ui/ticket/EditTicketView$HolderType;->CONVERT_TO_CUSTOM_TICKET_ROW_BUTTON_HOLDER:Lcom/squareup/ui/ticket/EditTicketView$HolderType;

    invoke-virtual {v1}, Lcom/squareup/ui/ticket/EditTicketView$HolderType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    :try_start_2
    sget-object v0, Lcom/squareup/ui/ticket/EditTicketView$1;->$SwitchMap$com$squareup$ui$ticket$EditTicketView$HolderType:[I

    sget-object v1, Lcom/squareup/ui/ticket/EditTicketView$HolderType;->TICKET_GROUP_HEADER_ROW_HOLDER:Lcom/squareup/ui/ticket/EditTicketView$HolderType;

    invoke-virtual {v1}, Lcom/squareup/ui/ticket/EditTicketView$HolderType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    :try_start_3
    sget-object v0, Lcom/squareup/ui/ticket/EditTicketView$1;->$SwitchMap$com$squareup$ui$ticket$EditTicketView$HolderType:[I

    sget-object v1, Lcom/squareup/ui/ticket/EditTicketView$HolderType;->NO_TICKET_GROUP_ROW_HOLDER:Lcom/squareup/ui/ticket/EditTicketView$HolderType;

    invoke-virtual {v1}, Lcom/squareup/ui/ticket/EditTicketView$HolderType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    :try_start_4
    sget-object v0, Lcom/squareup/ui/ticket/EditTicketView$1;->$SwitchMap$com$squareup$ui$ticket$EditTicketView$HolderType:[I

    sget-object v1, Lcom/squareup/ui/ticket/EditTicketView$HolderType;->TICKET_GROUP_ROW_HOLDER:Lcom/squareup/ui/ticket/EditTicketView$HolderType;

    invoke-virtual {v1}, Lcom/squareup/ui/ticket/EditTicketView$HolderType;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    :catch_4
    :try_start_5
    sget-object v0, Lcom/squareup/ui/ticket/EditTicketView$1;->$SwitchMap$com$squareup$ui$ticket$EditTicketView$HolderType:[I

    sget-object v1, Lcom/squareup/ui/ticket/EditTicketView$HolderType;->BUTTON_GROUP_ROW_HOLDER:Lcom/squareup/ui/ticket/EditTicketView$HolderType;

    invoke-virtual {v1}, Lcom/squareup/ui/ticket/EditTicketView$HolderType;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_5

    :catch_5
    return-void
.end method
