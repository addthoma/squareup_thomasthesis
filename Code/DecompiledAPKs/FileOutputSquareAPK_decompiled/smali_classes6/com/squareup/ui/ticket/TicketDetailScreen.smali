.class public Lcom/squareup/ui/ticket/TicketDetailScreen;
.super Lcom/squareup/ui/ticket/EditTicketScreen;
.source "TicketDetailScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/spot/HasSpot;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/ticket/TicketDetailScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/ticket/TicketDetailScreen$Component;
    }
.end annotation


# static fields
.field public static final CREATOR:Lcom/squareup/container/ContainerTreeKey$PathCreator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/container/ContainerTreeKey$PathCreator<",
            "Lcom/squareup/ui/ticket/TicketDetailScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final cardOwnerName:Ljava/lang/String;

.field final preselectedGroup:Lcom/squareup/api/items/TicketGroup;

.field final screenData:Lcom/squareup/ui/ticket/TicketDetailScreenData;

.field final startedFromSwipe:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 118
    sget-object v0, Lcom/squareup/ui/ticket/-$$Lambda$TicketDetailScreen$heuZvY-sC4C5nOqlLopF6sihPvQ;->INSTANCE:Lcom/squareup/ui/ticket/-$$Lambda$TicketDetailScreen$heuZvY-sC4C5nOqlLopF6sihPvQ;

    .line 119
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/ticket/TicketDetailScreen;->CREATOR:Lcom/squareup/container/ContainerTreeKey$PathCreator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .line 89
    invoke-direct {p0, p1}, Lcom/squareup/ui/ticket/EditTicketScreen;-><init>(Landroid/os/Parcel;)V

    .line 90
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailScreen;->cardOwnerName:Ljava/lang/String;

    .line 91
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    iput-boolean v1, p0, Lcom/squareup/ui/ticket/TicketDetailScreen;->startedFromSwipe:Z

    .line 92
    const-class v0, Lcom/squareup/api/items/TicketGroup;

    invoke-static {p1, v0}, Lcom/squareup/util/Parcels;->readProtoMessage(Landroid/os/Parcel;Ljava/lang/Class;)Lcom/squareup/wire/Message;

    move-result-object v0

    check-cast v0, Lcom/squareup/api/items/TicketGroup;

    iput-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailScreen;->preselectedGroup:Lcom/squareup/api/items/TicketGroup;

    .line 93
    const-class v0, Lcom/squareup/ui/ticket/TicketDetailScreenData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/ticket/TicketDetailScreenData;

    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketDetailScreen;->screenData:Lcom/squareup/ui/ticket/TicketDetailScreenData;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/ticket/EditTicketScreen$Builder;)V
    .locals 1

    .line 81
    invoke-direct {p0, p1}, Lcom/squareup/ui/ticket/EditTicketScreen;-><init>(Lcom/squareup/ui/ticket/EditTicketScreen$Builder;)V

    .line 82
    iget-object v0, p1, Lcom/squareup/ui/ticket/EditTicketScreen$Builder;->cardOwnerName:Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailScreen;->cardOwnerName:Ljava/lang/String;

    .line 83
    iget-boolean v0, p1, Lcom/squareup/ui/ticket/EditTicketScreen$Builder;->startedFromSwipe:Z

    iput-boolean v0, p0, Lcom/squareup/ui/ticket/TicketDetailScreen;->startedFromSwipe:Z

    .line 84
    iget-object p1, p1, Lcom/squareup/ui/ticket/EditTicketScreen$Builder;->preselectedGroup:Lcom/squareup/api/items/TicketGroup;

    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketDetailScreen;->preselectedGroup:Lcom/squareup/api/items/TicketGroup;

    const/4 p1, 0x0

    .line 85
    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketDetailScreen;->screenData:Lcom/squareup/ui/ticket/TicketDetailScreenData;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;Ljava/lang/String;ZLcom/squareup/api/items/TicketGroup;Lcom/squareup/tickets/OpenTicket;)V
    .locals 0

    .line 73
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/ticket/EditTicketScreen;-><init>(Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;)V

    .line 74
    iput-object p3, p0, Lcom/squareup/ui/ticket/TicketDetailScreen;->cardOwnerName:Ljava/lang/String;

    .line 75
    iput-boolean p4, p0, Lcom/squareup/ui/ticket/TicketDetailScreen;->startedFromSwipe:Z

    .line 76
    iput-object p5, p0, Lcom/squareup/ui/ticket/TicketDetailScreen;->preselectedGroup:Lcom/squareup/api/items/TicketGroup;

    if-eqz p6, :cond_0

    .line 77
    new-instance p1, Lcom/squareup/ui/ticket/TicketDetailScreenData;

    invoke-direct {p1, p6}, Lcom/squareup/ui/ticket/TicketDetailScreenData;-><init>(Lcom/squareup/tickets/OpenTicket;)V

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketDetailScreen;->screenData:Lcom/squareup/ui/ticket/TicketDetailScreenData;

    return-void
.end method

.method public static forEditingExistingTicket(Lcom/squareup/tickets/OpenTicket;)Lcom/squareup/ui/ticket/TicketDetailScreen;
    .locals 8

    .line 55
    new-instance v7, Lcom/squareup/ui/ticket/TicketDetailScreen;

    sget-object v1, Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;->EDIT_EXISTING_TICKET:Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

    sget-object v2, Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;->EXIT:Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, v7

    move-object v6, p0

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/ticket/TicketDetailScreen;-><init>(Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;Ljava/lang/String;ZLcom/squareup/api/items/TicketGroup;Lcom/squareup/tickets/OpenTicket;)V

    return-object v7
.end method

.method public static forEditingTransactionTicket()Lcom/squareup/ui/ticket/TicketDetailScreen;
    .locals 8

    .line 46
    new-instance v7, Lcom/squareup/ui/ticket/TicketDetailScreen;

    sget-object v1, Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;->EDIT_TRANSACTION_TICKET:Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;

    sget-object v2, Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;->EXIT:Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/ticket/TicketDetailScreen;-><init>(Lcom/squareup/ui/ticket/EditTicketScreen$TicketAction;Lcom/squareup/ui/ticket/EditTicketScreen$AfterAction;Ljava/lang/String;ZLcom/squareup/api/items/TicketGroup;Lcom/squareup/tickets/OpenTicket;)V

    return-object v7
.end method

.method public static synthetic lambda$heuZvY-sC4C5nOqlLopF6sihPvQ(Landroid/os/Parcel;)Lcom/squareup/ui/ticket/TicketDetailScreen;
    .locals 1

    new-instance v0, Lcom/squareup/ui/ticket/TicketDetailScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/ticket/TicketDetailScreen;-><init>(Landroid/os/Parcel;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 111
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/ticket/EditTicketScreen;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 112
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailScreen;->cardOwnerName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 113
    iget-boolean v0, p0, Lcom/squareup/ui/ticket/TicketDetailScreen;->startedFromSwipe:Z

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 114
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailScreen;->preselectedGroup:Lcom/squareup/api/items/TicketGroup;

    invoke-static {p1, v0}, Lcom/squareup/util/Parcels;->writeProtoMessage(Landroid/os/Parcel;Lcom/squareup/wire/Message;)V

    .line 115
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketDetailScreen;->screenData:Lcom/squareup/ui/ticket/TicketDetailScreenData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 101
    sget-object p1, Lcom/squareup/container/spot/Spots;->GROW_OVER:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 97
    sget v0, Lcom/squareup/orderentry/R$layout;->ticket_detail_view:I

    return v0
.end method
