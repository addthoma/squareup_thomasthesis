.class Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$TicketGroupRowHolder;
.super Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$NoTicketGroupRowHolder;
.source "EditTicketView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TicketGroupRowHolder"
.end annotation


# instance fields
.field final synthetic this$1:Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;

.field protected ticketGroup:Lcom/squareup/api/items/TicketGroup;


# direct methods
.method constructor <init>(Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;Landroid/view/ViewGroup;)V
    .locals 0

    .line 237
    iput-object p1, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$TicketGroupRowHolder;->this$1:Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;

    .line 238
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$NoTicketGroupRowHolder;-><init>(Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;Landroid/view/ViewGroup;)V

    return-void
.end method


# virtual methods
.method protected bindRow(Lcom/squareup/ui/ticket/EditTicketView$RowType;II)V
    .locals 2

    .line 243
    iget-object p1, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$TicketGroupRowHolder;->this$1:Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;

    iget-object p1, p1, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;->this$0:Lcom/squareup/ui/ticket/EditTicketView;

    invoke-static {p1}, Lcom/squareup/ui/ticket/EditTicketView;->access$300(Lcom/squareup/ui/ticket/EditTicketView;)Ljava/util/List;

    move-result-object p1

    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/api/items/TicketGroup;

    iput-object p1, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$TicketGroupRowHolder;->ticketGroup:Lcom/squareup/api/items/TicketGroup;

    .line 244
    iget-object p1, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$TicketGroupRowHolder;->ticketGroup:Lcom/squareup/api/items/TicketGroup;

    iget-object v0, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$TicketGroupRowHolder;->this$1:Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;

    iget-object v0, v0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;->this$0:Lcom/squareup/ui/ticket/EditTicketView;

    invoke-static {v0}, Lcom/squareup/ui/ticket/EditTicketView;->access$100(Lcom/squareup/ui/ticket/EditTicketView;)Lcom/squareup/ui/ticket/EditTicketState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/EditTicketState;->getSelectedGroup()Lcom/squareup/api/items/TicketGroup;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/api/items/TicketGroup;->equals(Ljava/lang/Object;)Z

    move-result p1

    .line 245
    iget-object v0, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$TicketGroupRowHolder;->this$1:Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;

    if-eqz p1, :cond_0

    move v1, p3

    goto :goto_0

    :cond_0
    invoke-static {v0}, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;->access$200(Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;)I

    move-result v1

    :goto_0
    invoke-static {v0, v1}, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;->access$202(Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;I)I

    .line 246
    iget-object v0, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$TicketGroupRowHolder;->checkableTicketGroup:Lcom/squareup/marketfont/MarketCheckedTextView;

    iget-object v1, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$TicketGroupRowHolder;->ticketGroup:Lcom/squareup/api/items/TicketGroup;

    iget-object v1, v1, Lcom/squareup/api/items/TicketGroup;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketCheckedTextView;->setText(Ljava/lang/CharSequence;)V

    .line 247
    iget-object v0, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$TicketGroupRowHolder;->checkableTicketGroup:Lcom/squareup/marketfont/MarketCheckedTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketCheckedTextView;->setChecked(Z)V

    .line 248
    iget-object p1, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$TicketGroupRowHolder;->checkableTicketGroup:Lcom/squareup/marketfont/MarketCheckedTextView;

    new-instance v0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$TicketGroupRowHolder$1;

    invoke-direct {v0, p0, p3}, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$TicketGroupRowHolder$1;-><init>(Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$TicketGroupRowHolder;I)V

    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketCheckedTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 255
    iget-object p1, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$TicketGroupRowHolder;->this$1:Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;

    iget-object p1, p1, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;->this$0:Lcom/squareup/ui/ticket/EditTicketView;

    invoke-static {p1}, Lcom/squareup/ui/ticket/EditTicketView;->access$300(Lcom/squareup/ui/ticket/EditTicketView;)Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    const/4 p3, 0x1

    sub-int/2addr p1, p3

    if-ne p2, p1, :cond_1

    goto :goto_1

    :cond_1
    const/4 p3, 0x0

    :goto_1
    invoke-virtual {p0, p3}, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$TicketGroupRowHolder;->setExtraBottomMargin(Z)V

    return-void
.end method

.method protected bridge synthetic bindRow(Ljava/lang/Enum;II)V
    .locals 0

    .line 233
    check-cast p1, Lcom/squareup/ui/ticket/EditTicketView$RowType;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$TicketGroupRowHolder;->bindRow(Lcom/squareup/ui/ticket/EditTicketView$RowType;II)V

    return-void
.end method
