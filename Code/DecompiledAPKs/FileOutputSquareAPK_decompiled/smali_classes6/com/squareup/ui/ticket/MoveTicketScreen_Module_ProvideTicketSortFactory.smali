.class public final Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketSortFactory;
.super Ljava/lang/Object;
.source "MoveTicketScreen_Module_ProvideTicketSortFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/settings/LocalSetting<",
        "Lcom/squareup/tickets/TicketSort;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final module:Lcom/squareup/ui/ticket/MoveTicketScreen$Module;

.field private final prefsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/content/SharedPreferences;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/ui/ticket/MoveTicketScreen$Module;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/ticket/MoveTicketScreen$Module;",
            "Ljavax/inject/Provider<",
            "Landroid/content/SharedPreferences;",
            ">;)V"
        }
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketSortFactory;->module:Lcom/squareup/ui/ticket/MoveTicketScreen$Module;

    .line 27
    iput-object p2, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketSortFactory;->prefsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Lcom/squareup/ui/ticket/MoveTicketScreen$Module;Ljavax/inject/Provider;)Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketSortFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/ticket/MoveTicketScreen$Module;",
            "Ljavax/inject/Provider<",
            "Landroid/content/SharedPreferences;",
            ">;)",
            "Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketSortFactory;"
        }
    .end annotation

    .line 37
    new-instance v0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketSortFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketSortFactory;-><init>(Lcom/squareup/ui/ticket/MoveTicketScreen$Module;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideTicketSort(Lcom/squareup/ui/ticket/MoveTicketScreen$Module;Landroid/content/SharedPreferences;)Lcom/squareup/settings/LocalSetting;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/ticket/MoveTicketScreen$Module;",
            "Landroid/content/SharedPreferences;",
            ")",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/tickets/TicketSort;",
            ">;"
        }
    .end annotation

    .line 42
    invoke-virtual {p0, p1}, Lcom/squareup/ui/ticket/MoveTicketScreen$Module;->provideTicketSort(Landroid/content/SharedPreferences;)Lcom/squareup/settings/LocalSetting;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/settings/LocalSetting;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/settings/LocalSetting;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/tickets/TicketSort;",
            ">;"
        }
    .end annotation

    .line 32
    iget-object v0, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketSortFactory;->module:Lcom/squareup/ui/ticket/MoveTicketScreen$Module;

    iget-object v1, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketSortFactory;->prefsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/SharedPreferences;

    invoke-static {v0, v1}, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketSortFactory;->provideTicketSort(Lcom/squareup/ui/ticket/MoveTicketScreen$Module;Landroid/content/SharedPreferences;)Lcom/squareup/settings/LocalSetting;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketSortFactory;->get()Lcom/squareup/settings/LocalSetting;

    move-result-object v0

    return-object v0
.end method
