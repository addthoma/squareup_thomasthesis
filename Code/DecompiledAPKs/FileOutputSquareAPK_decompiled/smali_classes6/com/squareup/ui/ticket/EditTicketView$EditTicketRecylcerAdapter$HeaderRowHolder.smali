.class Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder;
.super Lcom/squareup/ui/RangerRecyclerAdapter$RangerHolder;
.source "EditTicketView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "HeaderRowHolder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/RangerRecyclerAdapter<",
        "Lcom/squareup/ui/ticket/EditTicketView$RowType;",
        "Lcom/squareup/ui/ticket/EditTicketView$HolderType;",
        ">.RangerHolder;"
    }
.end annotation


# instance fields
.field private final cardNotStoredMessage:Lcom/squareup/widgets/MessageView;

.field private final editTextBackground:Landroid/graphics/drawable/Drawable;

.field private final editTextBackgroundFocused:Landroid/graphics/drawable/Drawable;

.field private final nameField:Lcom/squareup/ui/XableEditText;

.field private final nameFieldWatcher:Landroid/text/TextWatcher;

.field private final noteField:Landroid/widget/EditText;

.field private final noteFieldWatcher:Landroid/text/TextWatcher;

.field final synthetic this$1:Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;

.field private final ticketTemplateName:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;Landroid/view/ViewGroup;)V
    .locals 2

    .line 106
    iput-object p1, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder;->this$1:Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;

    .line 107
    sget v0, Lcom/squareup/orderentry/R$layout;->ticket_detail_header:I

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/ui/RangerRecyclerAdapter$RangerHolder;-><init>(Lcom/squareup/ui/RangerRecyclerAdapter;Landroid/view/ViewGroup;I)V

    .line 108
    iget-object p2, p1, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;->this$0:Lcom/squareup/ui/ticket/EditTicketView;

    invoke-virtual {p2}, Lcom/squareup/ui/ticket/EditTicketView;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    .line 110
    sget v0, Lcom/squareup/marin/R$drawable;->marin_edit_text_normal:I

    const/4 v1, 0x0

    .line 111
    invoke-static {p2, v0, v1}, Landroidx/core/content/res/ResourcesCompat;->getDrawable(Landroid/content/res/Resources;ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder;->editTextBackground:Landroid/graphics/drawable/Drawable;

    .line 113
    sget v0, Lcom/squareup/marin/R$drawable;->marin_edit_text_focused:I

    .line 114
    invoke-static {p2, v0, v1}, Landroidx/core/content/res/ResourcesCompat;->getDrawable(Landroid/content/res/Resources;ILandroid/content/res/Resources$Theme;)Landroid/graphics/drawable/Drawable;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder;->editTextBackgroundFocused:Landroid/graphics/drawable/Drawable;

    .line 117
    iget-object p2, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder;->itemView:Landroid/view/View;

    sget v0, Lcom/squareup/orderentry/R$id;->ticket_template_name:I

    invoke-static {p2, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    iput-object p2, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder;->ticketTemplateName:Landroid/widget/TextView;

    .line 119
    iget-object p2, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder;->itemView:Landroid/view/View;

    sget v0, Lcom/squareup/orderentry/R$id;->ticket_name:I

    invoke-static {p2, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/squareup/ui/XableEditText;

    iput-object p2, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder;->nameField:Lcom/squareup/ui/XableEditText;

    .line 120
    new-instance p2, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder$1;

    invoke-direct {p2, p0, p1}, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder$1;-><init>(Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder;Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;)V

    iput-object p2, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder;->nameFieldWatcher:Landroid/text/TextWatcher;

    .line 128
    iget-object p2, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder;->nameField:Lcom/squareup/ui/XableEditText;

    new-instance v0, Lcom/squareup/ui/ticket/-$$Lambda$EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder$eaSR9ob6_8Y_ChLJw4RJlRUNcO8;

    invoke-direct {v0, p0}, Lcom/squareup/ui/ticket/-$$Lambda$EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder$eaSR9ob6_8Y_ChLJw4RJlRUNcO8;-><init>(Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder;)V

    invoke-virtual {p2, v0}, Lcom/squareup/ui/XableEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 134
    iget-object p2, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder;->nameField:Lcom/squareup/ui/XableEditText;

    iget-object v0, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder;->nameFieldWatcher:Landroid/text/TextWatcher;

    invoke-virtual {p2, v0}, Lcom/squareup/ui/XableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 136
    iget-object p2, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder;->itemView:Landroid/view/View;

    sget v0, Lcom/squareup/orderentry/R$id;->ticket_card_not_stored_hint:I

    invoke-static {p2, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/squareup/widgets/MessageView;

    iput-object p2, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder;->cardNotStoredMessage:Lcom/squareup/widgets/MessageView;

    .line 138
    iget-object p2, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder;->itemView:Landroid/view/View;

    sget v0, Lcom/squareup/orderentry/R$id;->ticket_note:I

    invoke-static {p2, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/EditText;

    iput-object p2, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder;->noteField:Landroid/widget/EditText;

    .line 139
    new-instance p2, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder$2;

    invoke-direct {p2, p0, p1}, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder$2;-><init>(Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder;Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;)V

    iput-object p2, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder;->noteFieldWatcher:Landroid/text/TextWatcher;

    .line 147
    iget-object p2, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder;->noteField:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder;->noteFieldWatcher:Landroid/text/TextWatcher;

    invoke-virtual {p2, v0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 149
    iget-object p2, p1, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;->this$0:Lcom/squareup/ui/ticket/EditTicketView;

    invoke-static {p2}, Lcom/squareup/ui/ticket/EditTicketView;->access$100(Lcom/squareup/ui/ticket/EditTicketView;)Lcom/squareup/ui/ticket/EditTicketState;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/ui/ticket/EditTicketState;->didShowInitialFocus()Z

    move-result p2

    if-nez p2, :cond_0

    .line 150
    iget-object p2, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder;->nameField:Lcom/squareup/ui/XableEditText;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v0, Lcom/squareup/ui/ticket/-$$Lambda$MI9J-udRRNyPh5cmHHscAZLV2hw;

    invoke-direct {v0, p2}, Lcom/squareup/ui/ticket/-$$Lambda$MI9J-udRRNyPh5cmHHscAZLV2hw;-><init>(Lcom/squareup/ui/XableEditText;)V

    invoke-virtual {p2, v0}, Lcom/squareup/ui/XableEditText;->post(Ljava/lang/Runnable;)Z

    .line 151
    iget-object p1, p1, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;->this$0:Lcom/squareup/ui/ticket/EditTicketView;

    invoke-static {p1}, Lcom/squareup/ui/ticket/EditTicketView;->access$100(Lcom/squareup/ui/ticket/EditTicketView;)Lcom/squareup/ui/ticket/EditTicketState;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/ticket/EditTicketState;->initialFocusShown()V

    :cond_0
    return-void
.end method


# virtual methods
.method protected bindRow(Lcom/squareup/ui/ticket/EditTicketView$RowType;II)V
    .locals 0

    .line 157
    iget-object p1, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder;->ticketTemplateName:Landroid/widget/TextView;

    iget-object p2, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder;->this$1:Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;

    iget-object p2, p2, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;->this$0:Lcom/squareup/ui/ticket/EditTicketView;

    invoke-static {p2}, Lcom/squareup/ui/ticket/EditTicketView;->access$100(Lcom/squareup/ui/ticket/EditTicketView;)Lcom/squareup/ui/ticket/EditTicketState;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/ui/ticket/EditTicketState;->getTemplateName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 159
    iget-object p1, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder;->nameField:Lcom/squareup/ui/XableEditText;

    iget-object p2, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder;->this$1:Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;

    iget-object p2, p2, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;->this$0:Lcom/squareup/ui/ticket/EditTicketView;

    invoke-static {p2}, Lcom/squareup/ui/ticket/EditTicketView;->access$100(Lcom/squareup/ui/ticket/EditTicketView;)Lcom/squareup/ui/ticket/EditTicketState;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/ui/ticket/EditTicketState;->getEditableName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/XableEditText;->setText(Ljava/lang/CharSequence;)V

    .line 160
    iget-object p1, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder;->this$1:Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;

    iget-object p1, p1, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;->this$0:Lcom/squareup/ui/ticket/EditTicketView;

    invoke-static {p1}, Lcom/squareup/ui/ticket/EditTicketView;->access$100(Lcom/squareup/ui/ticket/EditTicketView;)Lcom/squareup/ui/ticket/EditTicketState;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/ticket/EditTicketState;->shouldSelectName()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 161
    iget-object p1, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder;->nameField:Lcom/squareup/ui/XableEditText;

    const/4 p2, 0x0

    iget-object p3, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder;->this$1:Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;

    iget-object p3, p3, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;->this$0:Lcom/squareup/ui/ticket/EditTicketView;

    invoke-static {p3}, Lcom/squareup/ui/ticket/EditTicketView;->access$100(Lcom/squareup/ui/ticket/EditTicketView;)Lcom/squareup/ui/ticket/EditTicketState;

    move-result-object p3

    invoke-virtual {p3}, Lcom/squareup/ui/ticket/EditTicketState;->getName()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result p3

    invoke-virtual {p1, p2, p3}, Lcom/squareup/ui/XableEditText;->setSelection(II)V

    .line 163
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder;->nameField:Lcom/squareup/ui/XableEditText;

    iget-object p2, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder;->this$1:Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;

    iget-object p2, p2, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;->this$0:Lcom/squareup/ui/ticket/EditTicketView;

    invoke-static {p2}, Lcom/squareup/ui/ticket/EditTicketView;->access$100(Lcom/squareup/ui/ticket/EditTicketView;)Lcom/squareup/ui/ticket/EditTicketState;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/ui/ticket/EditTicketState;->getNameHint()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/XableEditText;->setHint(Ljava/lang/String;)V

    .line 165
    iget-object p1, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder;->noteField:Landroid/widget/EditText;

    iget-object p2, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder;->this$1:Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;

    iget-object p2, p2, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;->this$0:Lcom/squareup/ui/ticket/EditTicketView;

    invoke-static {p2}, Lcom/squareup/ui/ticket/EditTicketView;->access$100(Lcom/squareup/ui/ticket/EditTicketView;)Lcom/squareup/ui/ticket/EditTicketState;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/ui/ticket/EditTicketState;->getNote()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 167
    iget-object p1, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder;->ticketTemplateName:Landroid/widget/TextView;

    iget-object p2, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder;->this$1:Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;

    iget-object p2, p2, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;->this$0:Lcom/squareup/ui/ticket/EditTicketView;

    invoke-static {p2}, Lcom/squareup/ui/ticket/EditTicketView;->access$100(Lcom/squareup/ui/ticket/EditTicketView;)Lcom/squareup/ui/ticket/EditTicketState;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/ui/ticket/EditTicketState;->isTicketTemplateNameVisible()Z

    move-result p2

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 168
    iget-object p1, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder;->cardNotStoredMessage:Lcom/squareup/widgets/MessageView;

    iget-object p2, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder;->this$1:Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;

    iget-object p2, p2, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;->this$0:Lcom/squareup/ui/ticket/EditTicketView;

    invoke-static {p2}, Lcom/squareup/ui/ticket/EditTicketView;->access$100(Lcom/squareup/ui/ticket/EditTicketView;)Lcom/squareup/ui/ticket/EditTicketState;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/ui/ticket/EditTicketState;->isCardNotStoredHintVisible()Z

    move-result p2

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method protected bridge synthetic bindRow(Ljava/lang/Enum;II)V
    .locals 0

    .line 95
    check-cast p1, Lcom/squareup/ui/ticket/EditTicketView$RowType;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder;->bindRow(Lcom/squareup/ui/ticket/EditTicketView$RowType;II)V

    return-void
.end method

.method public synthetic lambda$new$0$EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder(Landroid/view/View;Z)V
    .locals 0

    if-eqz p2, :cond_0

    .line 129
    iget-object p1, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder;->editTextBackgroundFocused:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder;->editTextBackground:Landroid/graphics/drawable/Drawable;

    .line 132
    :goto_0
    iget-object p2, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$HeaderRowHolder;->ticketTemplateName:Landroid/widget/TextView;

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method
