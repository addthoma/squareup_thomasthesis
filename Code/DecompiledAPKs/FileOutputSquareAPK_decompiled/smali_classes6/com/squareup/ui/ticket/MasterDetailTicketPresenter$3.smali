.class Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$3;
.super Lcom/squareup/permissions/PermissionGatekeeper$When;
.source "MasterDetailTicketPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->onBulkVoidClicked()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;)V
    .locals 0

    .line 666
    iput-object p1, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$3;->this$0:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;

    invoke-direct {p0}, Lcom/squareup/permissions/PermissionGatekeeper$When;-><init>()V

    return-void
.end method


# virtual methods
.method public success()V
    .locals 2

    .line 668
    iget-object v0, p0, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$3;->this$0:Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;

    invoke-static {v0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;->access$300(Lcom/squareup/ui/ticket/MasterDetailTicketPresenter;)Lflow/Flow;

    move-result-object v0

    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$3;->getAuthorizedEmployeeToken()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen;->forVoid(Ljava/lang/String;)Lcom/squareup/ui/ticket/TicketBulkDeleteDialogScreen;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method
