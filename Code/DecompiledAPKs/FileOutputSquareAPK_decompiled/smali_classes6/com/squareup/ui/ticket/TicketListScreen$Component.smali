.class public interface abstract Lcom/squareup/ui/ticket/TicketListScreen$Component;
.super Ljava/lang/Object;
.source "TicketListScreen.java"

# interfaces
.implements Lcom/squareup/ui/ticket/MasterDetailTicketPresenter$Component;


# annotations
.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/ui/ticket/TicketListScreen$Module;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/TicketListScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation
