.class Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$TaxViewHolder;
.super Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$ViewHolder;
.source "SplitTicketRecyclerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TaxViewHolder"
.end annotation


# instance fields
.field private final taxRow:Lcom/squareup/ui/cart/CartEntryView;

.field final synthetic this$0:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;Landroid/view/View;)V
    .locals 0

    .line 324
    iput-object p1, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$TaxViewHolder;->this$0:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;

    .line 325
    invoke-direct {p0, p2}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$ViewHolder;-><init>(Landroid/view/View;)V

    .line 326
    sget p1, Lcom/squareup/orderentry/R$id;->split_ticket_tax_total:I

    invoke-static {p2, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/cart/CartEntryView;

    iput-object p1, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$TaxViewHolder;->taxRow:Lcom/squareup/ui/cart/CartEntryView;

    return-void
.end method


# virtual methods
.method public bind()V
    .locals 6

    .line 330
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$TaxViewHolder;->this$0:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;

    invoke-static {v0}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->access$200(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;)Lcom/squareup/ui/ticket/SplitTicketPresenter;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$TaxViewHolder;->this$0:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;

    invoke-static {v1}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->access$100(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->getTaxTypeLabelId(Ljava/lang/String;)I

    move-result v0

    .line 332
    iget-object v1, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$TaxViewHolder;->this$0:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;

    invoke-static {v1}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->access$200(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;)Lcom/squareup/ui/ticket/SplitTicketPresenter;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$TaxViewHolder;->this$0:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;

    invoke-static {v2}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->access$100(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->ticketMutable(Ljava/lang/String;)Z

    move-result v1

    const/4 v2, 0x1

    xor-int/2addr v1, v2

    .line 333
    sget v3, Lcom/squareup/common/strings/R$string;->cart_tax_row_included:I

    if-ne v0, v3, :cond_0

    const/4 v2, 0x0

    .line 337
    :cond_0
    iget-object v3, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$TaxViewHolder;->this$0:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;

    .line 338
    invoke-static {v3}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->access$500(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;)Lcom/squareup/ui/cart/CartEntryViewModelFactory;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$TaxViewHolder;->this$0:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;

    invoke-static {v4}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->access$200(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;)Lcom/squareup/ui/ticket/SplitTicketPresenter;

    move-result-object v4

    iget-object v5, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$TaxViewHolder;->this$0:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;

    invoke-static {v5}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->access$100(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/squareup/ui/ticket/SplitTicketPresenter;->getAdditionalTaxAmount(Ljava/lang/String;)Lcom/squareup/protos/common/Money;

    move-result-object v4

    invoke-interface {v3, v0, v4, v1, v2}, Lcom/squareup/ui/cart/CartEntryViewModelFactory;->taxes(ILcom/squareup/protos/common/Money;ZZ)Lcom/squareup/ui/cart/CartEntryViewModel;

    move-result-object v0

    .line 340
    iget-object v2, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$TaxViewHolder;->taxRow:Lcom/squareup/ui/cart/CartEntryView;

    invoke-virtual {v2, v0}, Lcom/squareup/ui/cart/CartEntryView;->present(Lcom/squareup/ui/cart/CartEntryViewModel;)V

    if-nez v1, :cond_1

    .line 345
    iget-object v0, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$TaxViewHolder;->taxRow:Lcom/squareup/ui/cart/CartEntryView;

    iget-object v1, p0, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter$TaxViewHolder;->this$0:Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;

    invoke-static {v1}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->access$800(Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/squareup/ui/ticket/SplitTicketRecyclerAdapter;->access$900(Landroid/view/View;I)V

    :cond_1
    return-void
.end method
