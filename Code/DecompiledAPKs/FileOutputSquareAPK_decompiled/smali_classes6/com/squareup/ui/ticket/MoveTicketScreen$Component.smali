.class public interface abstract Lcom/squareup/ui/ticket/MoveTicketScreen$Component;
.super Ljava/lang/Object;
.source "MoveTicketScreen.java"

# interfaces
.implements Lcom/squareup/ui/ticket/TicketListPresenter$Component;


# annotations
.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/ui/ticket/MoveTicketScreen$Module;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/MoveTicketScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract inject(Lcom/squareup/ui/ticket/MoveTicketView;)V
.end method

.method public abstract ticketActionSession()Lcom/squareup/ui/ticket/TicketActionScopeRunner;
.end method

.method public abstract ticketSelectionSession()Lcom/squareup/ui/ticket/TicketSelection;
.end method

.method public abstract ticketsLoader()Lcom/squareup/ui/ticket/TicketsLoader;
.end method
