.class Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$ButtonGroupRowHolder;
.super Lcom/squareup/ui/RangerRecyclerAdapter$RangerHolder;
.source "EditTicketView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ButtonGroupRowHolder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/RangerRecyclerAdapter<",
        "Lcom/squareup/ui/ticket/EditTicketView$RowType;",
        "Lcom/squareup/ui/ticket/EditTicketView$HolderType;",
        ">.RangerHolder;"
    }
.end annotation


# instance fields
.field private final compButton:Lcom/squareup/marketfont/MarketButton;

.field private final deleteButton:Lcom/squareup/ui/ConfirmButton;

.field final synthetic this$1:Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;

.field private final uncompButton:Lcom/squareup/marketfont/MarketButton;

.field private final voidButton:Lcom/squareup/marketfont/MarketButton;


# direct methods
.method constructor <init>(Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;Landroid/view/ViewGroup;)V
    .locals 2

    .line 266
    iput-object p1, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$ButtonGroupRowHolder;->this$1:Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;

    .line 267
    sget v0, Lcom/squareup/orderentry/R$layout;->ticket_detail_buttons:I

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/ui/RangerRecyclerAdapter$RangerHolder;-><init>(Lcom/squareup/ui/RangerRecyclerAdapter;Landroid/view/ViewGroup;I)V

    .line 268
    iget-object p2, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$ButtonGroupRowHolder;->itemView:Landroid/view/View;

    sget v0, Lcom/squareup/orderentry/R$id;->ticket_delete:I

    invoke-static {p2, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/squareup/ui/ConfirmButton;

    iput-object p2, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$ButtonGroupRowHolder;->deleteButton:Lcom/squareup/ui/ConfirmButton;

    .line 269
    iget-object p2, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$ButtonGroupRowHolder;->deleteButton:Lcom/squareup/ui/ConfirmButton;

    iget-object v0, p1, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;->this$0:Lcom/squareup/ui/ticket/EditTicketView;

    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$h8DJ2mq-crISRz6tGuRO3oJkvg4;

    invoke-direct {v1, v0}, Lcom/squareup/ui/ticket/-$$Lambda$h8DJ2mq-crISRz6tGuRO3oJkvg4;-><init>(Lcom/squareup/ui/ticket/EditTicketView;)V

    invoke-virtual {p2, v1}, Lcom/squareup/ui/ConfirmButton;->setOnConfirmListener(Lcom/squareup/ui/ConfirmableButton$OnConfirmListener;)V

    .line 270
    iget-object p2, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$ButtonGroupRowHolder;->itemView:Landroid/view/View;

    sget v0, Lcom/squareup/orderentry/R$id;->ticket_void:I

    invoke-static {p2, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/squareup/marketfont/MarketButton;

    iput-object p2, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$ButtonGroupRowHolder;->voidButton:Lcom/squareup/marketfont/MarketButton;

    .line 271
    iget-object p2, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$ButtonGroupRowHolder;->voidButton:Lcom/squareup/marketfont/MarketButton;

    new-instance v0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$ButtonGroupRowHolder$1;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$ButtonGroupRowHolder$1;-><init>(Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$ButtonGroupRowHolder;Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;)V

    invoke-virtual {p2, v0}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 276
    iget-object p2, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$ButtonGroupRowHolder;->itemView:Landroid/view/View;

    sget v0, Lcom/squareup/orderentry/R$id;->ticket_comp:I

    invoke-static {p2, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/squareup/marketfont/MarketButton;

    iput-object p2, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$ButtonGroupRowHolder;->compButton:Lcom/squareup/marketfont/MarketButton;

    .line 277
    iget-object p2, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$ButtonGroupRowHolder;->compButton:Lcom/squareup/marketfont/MarketButton;

    new-instance v0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$ButtonGroupRowHolder$2;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$ButtonGroupRowHolder$2;-><init>(Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$ButtonGroupRowHolder;Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;)V

    invoke-virtual {p2, v0}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 282
    iget-object p2, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$ButtonGroupRowHolder;->itemView:Landroid/view/View;

    sget v0, Lcom/squareup/orderentry/R$id;->ticket_uncomp:I

    invoke-static {p2, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/squareup/marketfont/MarketButton;

    iput-object p2, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$ButtonGroupRowHolder;->uncompButton:Lcom/squareup/marketfont/MarketButton;

    .line 283
    iget-object p2, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$ButtonGroupRowHolder;->uncompButton:Lcom/squareup/marketfont/MarketButton;

    new-instance v0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$ButtonGroupRowHolder$3;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$ButtonGroupRowHolder$3;-><init>(Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$ButtonGroupRowHolder;Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;)V

    invoke-virtual {p2, v0}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method protected bindRow(Lcom/squareup/ui/ticket/EditTicketView$RowType;II)V
    .locals 0

    .line 292
    iget-object p1, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$ButtonGroupRowHolder;->deleteButton:Lcom/squareup/ui/ConfirmButton;

    iget-object p2, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$ButtonGroupRowHolder;->this$1:Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;

    iget-object p2, p2, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;->this$0:Lcom/squareup/ui/ticket/EditTicketView;

    invoke-static {p2}, Lcom/squareup/ui/ticket/EditTicketView;->access$100(Lcom/squareup/ui/ticket/EditTicketView;)Lcom/squareup/ui/ticket/EditTicketState;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/ui/ticket/EditTicketState;->isDeleteButtonVisible()Z

    move-result p2

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 293
    iget-object p1, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$ButtonGroupRowHolder;->voidButton:Lcom/squareup/marketfont/MarketButton;

    iget-object p2, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$ButtonGroupRowHolder;->this$1:Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;

    iget-object p2, p2, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;->this$0:Lcom/squareup/ui/ticket/EditTicketView;

    invoke-static {p2}, Lcom/squareup/ui/ticket/EditTicketView;->access$100(Lcom/squareup/ui/ticket/EditTicketView;)Lcom/squareup/ui/ticket/EditTicketState;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/ui/ticket/EditTicketState;->isVoidButtonVisible()Z

    move-result p2

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 294
    iget-object p1, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$ButtonGroupRowHolder;->compButton:Lcom/squareup/marketfont/MarketButton;

    iget-object p2, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$ButtonGroupRowHolder;->this$1:Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;

    iget-object p2, p2, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;->this$0:Lcom/squareup/ui/ticket/EditTicketView;

    invoke-static {p2}, Lcom/squareup/ui/ticket/EditTicketView;->access$100(Lcom/squareup/ui/ticket/EditTicketView;)Lcom/squareup/ui/ticket/EditTicketState;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/ui/ticket/EditTicketState;->isCompButtonVisible()Z

    move-result p2

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 295
    iget-object p1, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$ButtonGroupRowHolder;->uncompButton:Lcom/squareup/marketfont/MarketButton;

    iget-object p2, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$ButtonGroupRowHolder;->this$1:Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;

    iget-object p2, p2, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;->this$0:Lcom/squareup/ui/ticket/EditTicketView;

    invoke-static {p2}, Lcom/squareup/ui/ticket/EditTicketView;->access$100(Lcom/squareup/ui/ticket/EditTicketView;)Lcom/squareup/ui/ticket/EditTicketState;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/ui/ticket/EditTicketState;->isUncompButtonVisible()Z

    move-result p2

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 296
    iget-object p1, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$ButtonGroupRowHolder;->voidButton:Lcom/squareup/marketfont/MarketButton;

    iget-object p2, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$ButtonGroupRowHolder;->this$1:Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;

    invoke-static {p2}, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;->access$400(Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;)Z

    move-result p2

    invoke-virtual {p1, p2}, Lcom/squareup/marketfont/MarketButton;->setEnabled(Z)V

    .line 297
    iget-object p1, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$ButtonGroupRowHolder;->compButton:Lcom/squareup/marketfont/MarketButton;

    iget-object p2, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$ButtonGroupRowHolder;->this$1:Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;

    invoke-static {p2}, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;->access$400(Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;)Z

    move-result p2

    invoke-virtual {p1, p2}, Lcom/squareup/marketfont/MarketButton;->setEnabled(Z)V

    .line 298
    iget-object p1, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$ButtonGroupRowHolder;->uncompButton:Lcom/squareup/marketfont/MarketButton;

    iget-object p2, p0, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$ButtonGroupRowHolder;->this$1:Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;

    invoke-static {p2}, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;->access$400(Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter;)Z

    move-result p2

    invoke-virtual {p1, p2}, Lcom/squareup/marketfont/MarketButton;->setEnabled(Z)V

    return-void
.end method

.method protected bridge synthetic bindRow(Ljava/lang/Enum;II)V
    .locals 0

    .line 259
    check-cast p1, Lcom/squareup/ui/ticket/EditTicketView$RowType;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/ticket/EditTicketView$EditTicketRecylcerAdapter$ButtonGroupRowHolder;->bindRow(Lcom/squareup/ui/ticket/EditTicketView$RowType;II)V

    return-void
.end method
