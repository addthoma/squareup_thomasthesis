.class public final Lcom/squareup/ui/ticket/TicketScope;
.super Lcom/squareup/ui/seller/InSellerScope;
.source "TicketScope.java"

# interfaces
.implements Lcom/squareup/container/RegistersInScope;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/ticket/TicketScope$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/ticket/TicketScope$Module;,
        Lcom/squareup/ui/ticket/TicketScope$Component;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/ticket/TicketScope;",
            ">;"
        }
    .end annotation
.end field

.field static final INSTANCE:Lcom/squareup/ui/ticket/TicketScope;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 26
    new-instance v0, Lcom/squareup/ui/ticket/TicketScope;

    invoke-direct {v0}, Lcom/squareup/ui/ticket/TicketScope;-><init>()V

    sput-object v0, Lcom/squareup/ui/ticket/TicketScope;->INSTANCE:Lcom/squareup/ui/ticket/TicketScope;

    .line 69
    sget-object v0, Lcom/squareup/ui/ticket/TicketScope;->INSTANCE:Lcom/squareup/ui/ticket/TicketScope;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/ticket/TicketScope;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 28
    invoke-direct {p0}, Lcom/squareup/ui/seller/InSellerScope;-><init>()V

    return-void
.end method


# virtual methods
.method public register(Lmortar/MortarScope;)V
    .locals 1

    .line 32
    const-class v0, Lcom/squareup/ui/ticket/TicketScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ticket/TicketScope$Component;

    .line 33
    invoke-static {p1}, Lmortar/bundler/BundleService;->getBundleService(Lmortar/MortarScope;)Lmortar/bundler/BundleService;

    move-result-object p1

    .line 34
    invoke-interface {v0}, Lcom/squareup/ui/ticket/TicketScope$Component;->scopeRunner()Lcom/squareup/ui/ticket/TicketScopeRunner;

    move-result-object v0

    invoke-virtual {p1, v0}, Lmortar/bundler/BundleService;->register(Lmortar/bundler/Bundler;)V

    return-void
.end method
