.class public final Lcom/squareup/ui/ticket/TicketActionScope_Module_ProvideTicketSortFactory;
.super Ljava/lang/Object;
.source "TicketActionScope_Module_ProvideTicketSortFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/settings/LocalSetting<",
        "Lcom/squareup/tickets/TicketSort;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final prefsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/content/SharedPreferences;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/content/SharedPreferences;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketActionScope_Module_ProvideTicketSortFactory;->prefsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/ui/ticket/TicketActionScope_Module_ProvideTicketSortFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/content/SharedPreferences;",
            ">;)",
            "Lcom/squareup/ui/ticket/TicketActionScope_Module_ProvideTicketSortFactory;"
        }
    .end annotation

    .line 34
    new-instance v0, Lcom/squareup/ui/ticket/TicketActionScope_Module_ProvideTicketSortFactory;

    invoke-direct {v0, p0}, Lcom/squareup/ui/ticket/TicketActionScope_Module_ProvideTicketSortFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideTicketSort(Landroid/content/SharedPreferences;)Lcom/squareup/settings/LocalSetting;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            ")",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/tickets/TicketSort;",
            ">;"
        }
    .end annotation

    .line 38
    invoke-static {p0}, Lcom/squareup/ui/ticket/TicketActionScope$Module;->provideTicketSort(Landroid/content/SharedPreferences;)Lcom/squareup/settings/LocalSetting;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/settings/LocalSetting;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/settings/LocalSetting;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/settings/LocalSetting<",
            "Lcom/squareup/tickets/TicketSort;",
            ">;"
        }
    .end annotation

    .line 29
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketActionScope_Module_ProvideTicketSortFactory;->prefsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    invoke-static {v0}, Lcom/squareup/ui/ticket/TicketActionScope_Module_ProvideTicketSortFactory;->provideTicketSort(Landroid/content/SharedPreferences;)Lcom/squareup/settings/LocalSetting;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketActionScope_Module_ProvideTicketSortFactory;->get()Lcom/squareup/settings/LocalSetting;

    move-result-object v0

    return-object v0
.end method
