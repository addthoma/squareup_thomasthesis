.class public final Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketsToMoveFactory;
.super Ljava/lang/Object;
.source "MoveTicketScreen_Module_ProvideTicketsToMoveFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Ljava/util/List<",
        "Lcom/squareup/ui/ticket/TicketInfo;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final module:Lcom/squareup/ui/ticket/MoveTicketScreen$Module;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/ticket/MoveTicketScreen$Module;)V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketsToMoveFactory;->module:Lcom/squareup/ui/ticket/MoveTicketScreen$Module;

    return-void
.end method

.method public static create(Lcom/squareup/ui/ticket/MoveTicketScreen$Module;)Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketsToMoveFactory;
    .locals 1

    .line 30
    new-instance v0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketsToMoveFactory;

    invoke-direct {v0, p0}, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketsToMoveFactory;-><init>(Lcom/squareup/ui/ticket/MoveTicketScreen$Module;)V

    return-object v0
.end method

.method public static provideTicketsToMove(Lcom/squareup/ui/ticket/MoveTicketScreen$Module;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/ticket/MoveTicketScreen$Module;",
            ")",
            "Ljava/util/List<",
            "Lcom/squareup/ui/ticket/TicketInfo;",
            ">;"
        }
    .end annotation

    .line 34
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MoveTicketScreen$Module;->provideTicketsToMove()Ljava/util/List;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/util/List;

    return-object p0
.end method


# virtual methods
.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketsToMoveFactory;->get()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public get()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/ui/ticket/TicketInfo;",
            ">;"
        }
    .end annotation

    .line 25
    iget-object v0, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketsToMoveFactory;->module:Lcom/squareup/ui/ticket/MoveTicketScreen$Module;

    invoke-static {v0}, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketsToMoveFactory;->provideTicketsToMove(Lcom/squareup/ui/ticket/MoveTicketScreen$Module;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
