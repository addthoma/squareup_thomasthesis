.class Lcom/squareup/ui/ticket/TicketTransferEmployeesView$EmployeeRecyclerAdapter;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;
.source "TicketTransferEmployeesView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/TicketTransferEmployeesView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "EmployeeRecyclerAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/ticket/TicketTransferEmployeesView$EmployeeRecyclerAdapter$EmployeeRowHolder;,
        Lcom/squareup/ui/ticket/TicketTransferEmployeesView$EmployeeRecyclerAdapter$NoEmployeesHolder;,
        Lcom/squareup/ui/ticket/TicketTransferEmployeesView$EmployeeRecyclerAdapter$EmployeesAdapterHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "Lcom/squareup/ui/ticket/TicketTransferEmployeesView$EmployeeRecyclerAdapter$EmployeesAdapterHolder;",
        ">;"
    }
.end annotation


# static fields
.field private static final EMPLOYEE_ROW:I = 0x1

.field private static final NO_EMPLOYEES_ROW:I = 0x2


# instance fields
.field employeeList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$SearchableEmployeeInfo;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/squareup/ui/ticket/TicketTransferEmployeesView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/ticket/TicketTransferEmployeesView;)V
    .locals 0

    .line 31
    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesView$EmployeeRecyclerAdapter;->this$0:Lcom/squareup/ui/ticket/TicketTransferEmployeesView;

    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    .line 80
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesView$EmployeeRecyclerAdapter;->employeeList:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public getItemCount()I
    .locals 1

    .line 110
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesView$EmployeeRecyclerAdapter;->employeeList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 111
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesView$EmployeeRecyclerAdapter;->employeeList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public getItemViewType(I)I
    .locals 0

    .line 103
    iget-object p1, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesView$EmployeeRecyclerAdapter;->employeeList:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x2

    return p1
.end method

.method public bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .line 31
    check-cast p1, Lcom/squareup/ui/ticket/TicketTransferEmployeesView$EmployeeRecyclerAdapter$EmployeesAdapterHolder;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/ticket/TicketTransferEmployeesView$EmployeeRecyclerAdapter;->onBindViewHolder(Lcom/squareup/ui/ticket/TicketTransferEmployeesView$EmployeeRecyclerAdapter$EmployeesAdapterHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/squareup/ui/ticket/TicketTransferEmployeesView$EmployeeRecyclerAdapter$EmployeesAdapterHolder;I)V
    .locals 0

    .line 99
    invoke-virtual {p1, p2}, Lcom/squareup/ui/ticket/TicketTransferEmployeesView$EmployeeRecyclerAdapter$EmployeesAdapterHolder;->bindRow(I)V

    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0

    .line 31
    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/ticket/TicketTransferEmployeesView$EmployeeRecyclerAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/ui/ticket/TicketTransferEmployeesView$EmployeeRecyclerAdapter$EmployeesAdapterHolder;

    move-result-object p1

    return-object p1
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/squareup/ui/ticket/TicketTransferEmployeesView$EmployeeRecyclerAdapter$EmployeesAdapterHolder;
    .locals 2

    const/4 v0, 0x1

    if-eq p2, v0, :cond_1

    const/4 v0, 0x2

    if-ne p2, v0, :cond_0

    .line 92
    new-instance p2, Lcom/squareup/ui/ticket/TicketTransferEmployeesView$EmployeeRecyclerAdapter$NoEmployeesHolder;

    invoke-direct {p2, p0, p1}, Lcom/squareup/ui/ticket/TicketTransferEmployeesView$EmployeeRecyclerAdapter$NoEmployeesHolder;-><init>(Lcom/squareup/ui/ticket/TicketTransferEmployeesView$EmployeeRecyclerAdapter;Landroid/view/ViewGroup;)V

    return-object p2

    .line 94
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Cannot understand viewtype "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 90
    :cond_1
    new-instance p2, Lcom/squareup/ui/ticket/TicketTransferEmployeesView$EmployeeRecyclerAdapter$EmployeeRowHolder;

    invoke-direct {p2, p0, p1}, Lcom/squareup/ui/ticket/TicketTransferEmployeesView$EmployeeRecyclerAdapter$EmployeeRowHolder;-><init>(Lcom/squareup/ui/ticket/TicketTransferEmployeesView$EmployeeRecyclerAdapter;Landroid/view/ViewGroup;)V

    return-object p2
.end method

.method public setList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/ui/ticket/TicketTransferEmployeesScreen$SearchableEmployeeInfo;",
            ">;)V"
        }
    .end annotation

    .line 83
    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketTransferEmployeesView$EmployeeRecyclerAdapter;->employeeList:Ljava/util/List;

    .line 84
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketTransferEmployeesView$EmployeeRecyclerAdapter;->notifyDataSetChanged()V

    return-void
.end method
