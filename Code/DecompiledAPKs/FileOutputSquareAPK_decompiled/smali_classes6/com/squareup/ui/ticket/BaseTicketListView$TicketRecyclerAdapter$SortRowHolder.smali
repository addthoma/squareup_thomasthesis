.class Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$SortRowHolder;
.super Lcom/squareup/ui/RangerRecyclerAdapter$RangerHolder;
.source "BaseTicketListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SortRowHolder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/RangerRecyclerAdapter<",
        "Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;",
        "Lcom/squareup/ui/ticket/BaseTicketListView$TicketViewHolder;",
        ">.RangerHolder;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;Landroid/view/ViewGroup;)V
    .locals 1

    .line 317
    iput-object p1, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$SortRowHolder;->this$1:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;

    .line 318
    sget v0, Lcom/squareup/orderentry/R$layout;->ticket_list_sort_row:I

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/ui/RangerRecyclerAdapter$RangerHolder;-><init>(Lcom/squareup/ui/RangerRecyclerAdapter;Landroid/view/ViewGroup;I)V

    return-void
.end method


# virtual methods
.method protected bindRow(Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;II)V
    .locals 0

    .line 322
    iget-object p1, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$SortRowHolder;->this$1:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;

    invoke-static {p1}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;->access$100(Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;)Lcom/squareup/tickets/TicketRowCursorList;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/tickets/TicketRowCursorList;->isClosed()Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    .line 330
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$SortRowHolder;->itemView()Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/ticket/TicketSortGroup;

    .line 331
    iget-object p2, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$SortRowHolder;->this$1:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;

    invoke-static {p2}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;->access$100(Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;)Lcom/squareup/tickets/TicketRowCursorList;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/tickets/TicketRowCursorList;->getSort()Lcom/squareup/tickets/TicketSort;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/ticket/TicketSortGroup;->setSortType(Lcom/squareup/tickets/TicketSort;)V

    .line 332
    iget-object p2, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$SortRowHolder;->this$1:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;

    invoke-static {p2}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;->access$400(Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;)Z

    move-result p2

    invoke-virtual {p1, p2}, Lcom/squareup/ui/ticket/TicketSortGroup;->setEmployeeSortVisible(Z)V

    .line 334
    new-instance p2, Lcom/squareup/ui/ticket/-$$Lambda$BaseTicketListView$TicketRecyclerAdapter$SortRowHolder$avilCJfbgd1AEGxWUXvpN6n_yoM;

    invoke-direct {p2, p0}, Lcom/squareup/ui/ticket/-$$Lambda$BaseTicketListView$TicketRecyclerAdapter$SortRowHolder$avilCJfbgd1AEGxWUXvpN6n_yoM;-><init>(Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$SortRowHolder;)V

    invoke-virtual {p1, p2}, Lcom/squareup/ui/ticket/TicketSortGroup;->setOnSortChangeListener(Lcom/squareup/ui/ticket/TicketSortGroup$OnSortChangeListener;)V

    return-void
.end method

.method protected bridge synthetic bindRow(Ljava/lang/Enum;II)V
    .locals 0

    .line 315
    check-cast p1, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$SortRowHolder;->bindRow(Lcom/squareup/ui/ticket/BaseTicketListView$TicketRow;II)V

    return-void
.end method

.method public synthetic lambda$bindRow$0$BaseTicketListView$TicketRecyclerAdapter$SortRowHolder(Lcom/squareup/tickets/TicketSort;)V
    .locals 1

    .line 334
    iget-object v0, p0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter$SortRowHolder;->this$1:Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;

    iget-object v0, v0, Lcom/squareup/ui/ticket/BaseTicketListView$TicketRecyclerAdapter;->this$0:Lcom/squareup/ui/ticket/BaseTicketListView;

    iget-object v0, v0, Lcom/squareup/ui/ticket/BaseTicketListView;->presenter:Lcom/squareup/ui/ticket/TicketListPresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ticket/TicketListPresenter;->onSortChange(Lcom/squareup/tickets/TicketSort;)V

    return-void
.end method
