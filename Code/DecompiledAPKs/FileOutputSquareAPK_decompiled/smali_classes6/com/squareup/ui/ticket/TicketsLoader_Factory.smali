.class public final Lcom/squareup/ui/ticket/TicketsLoader_Factory;
.super Ljava/lang/Object;
.source "TicketsLoader_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/ticket/TicketsLoader;",
        ">;"
    }
.end annotation


# instance fields
.field private final connectivityMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketsListSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/opentickets/TicketsListScheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketsLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/tickets/OpenTicketsLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/Tickets;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/Tickets;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/opentickets/TicketsListScheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/tickets/OpenTicketsLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;)V"
        }
    .end annotation

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/squareup/ui/ticket/TicketsLoader_Factory;->ticketsProvider:Ljavax/inject/Provider;

    .line 33
    iput-object p2, p0, Lcom/squareup/ui/ticket/TicketsLoader_Factory;->ticketsListSchedulerProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p3, p0, Lcom/squareup/ui/ticket/TicketsLoader_Factory;->ticketsLoggerProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p4, p0, Lcom/squareup/ui/ticket/TicketsLoader_Factory;->connectivityMonitorProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/ticket/TicketsLoader_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/Tickets;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/opentickets/TicketsListScheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/tickets/OpenTicketsLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/connectivity/ConnectivityMonitor;",
            ">;)",
            "Lcom/squareup/ui/ticket/TicketsLoader_Factory;"
        }
    .end annotation

    .line 47
    new-instance v0, Lcom/squareup/ui/ticket/TicketsLoader_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/ticket/TicketsLoader_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/tickets/Tickets;Lcom/squareup/opentickets/TicketsListScheduler;Lcom/squareup/log/tickets/OpenTicketsLogger;Lcom/squareup/connectivity/ConnectivityMonitor;)Lcom/squareup/ui/ticket/TicketsLoader;
    .locals 1

    .line 53
    new-instance v0, Lcom/squareup/ui/ticket/TicketsLoader;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/ticket/TicketsLoader;-><init>(Lcom/squareup/tickets/Tickets;Lcom/squareup/opentickets/TicketsListScheduler;Lcom/squareup/log/tickets/OpenTicketsLogger;Lcom/squareup/connectivity/ConnectivityMonitor;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/ticket/TicketsLoader;
    .locals 4

    .line 40
    iget-object v0, p0, Lcom/squareup/ui/ticket/TicketsLoader_Factory;->ticketsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/tickets/Tickets;

    iget-object v1, p0, Lcom/squareup/ui/ticket/TicketsLoader_Factory;->ticketsListSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/opentickets/TicketsListScheduler;

    iget-object v2, p0, Lcom/squareup/ui/ticket/TicketsLoader_Factory;->ticketsLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/log/tickets/OpenTicketsLogger;

    iget-object v3, p0, Lcom/squareup/ui/ticket/TicketsLoader_Factory;->connectivityMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/connectivity/ConnectivityMonitor;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/ui/ticket/TicketsLoader_Factory;->newInstance(Lcom/squareup/tickets/Tickets;Lcom/squareup/opentickets/TicketsListScheduler;Lcom/squareup/log/tickets/OpenTicketsLogger;Lcom/squareup/connectivity/ConnectivityMonitor;)Lcom/squareup/ui/ticket/TicketsLoader;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/TicketsLoader_Factory;->get()Lcom/squareup/ui/ticket/TicketsLoader;

    move-result-object v0

    return-object v0
.end method
