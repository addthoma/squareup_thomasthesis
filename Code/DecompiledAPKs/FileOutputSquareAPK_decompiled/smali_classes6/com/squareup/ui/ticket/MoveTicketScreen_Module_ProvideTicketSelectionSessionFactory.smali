.class public final Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketSelectionSessionFactory;
.super Ljava/lang/Object;
.source "MoveTicketScreen_Module_ProvideTicketSelectionSessionFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/ticket/TicketSelection;",
        ">;"
    }
.end annotation


# instance fields
.field private final module:Lcom/squareup/ui/ticket/MoveTicketScreen$Module;

.field private final selectedTicketsInfoKeyProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/BundleKey<",
            "Ljava/util/List<",
            "Lcom/squareup/ui/ticket/TicketInfo;",
            ">;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/ui/ticket/MoveTicketScreen$Module;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/ticket/MoveTicketScreen$Module;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/BundleKey<",
            "Ljava/util/List<",
            "Lcom/squareup/ui/ticket/TicketInfo;",
            ">;>;>;)V"
        }
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketSelectionSessionFactory;->module:Lcom/squareup/ui/ticket/MoveTicketScreen$Module;

    .line 27
    iput-object p2, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketSelectionSessionFactory;->selectedTicketsInfoKeyProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Lcom/squareup/ui/ticket/MoveTicketScreen$Module;Ljavax/inject/Provider;)Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketSelectionSessionFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/ticket/MoveTicketScreen$Module;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/BundleKey<",
            "Ljava/util/List<",
            "Lcom/squareup/ui/ticket/TicketInfo;",
            ">;>;>;)",
            "Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketSelectionSessionFactory;"
        }
    .end annotation

    .line 38
    new-instance v0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketSelectionSessionFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketSelectionSessionFactory;-><init>(Lcom/squareup/ui/ticket/MoveTicketScreen$Module;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideTicketSelectionSession(Lcom/squareup/ui/ticket/MoveTicketScreen$Module;Lcom/squareup/BundleKey;)Lcom/squareup/ui/ticket/TicketSelection;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/ticket/MoveTicketScreen$Module;",
            "Lcom/squareup/BundleKey<",
            "Ljava/util/List<",
            "Lcom/squareup/ui/ticket/TicketInfo;",
            ">;>;)",
            "Lcom/squareup/ui/ticket/TicketSelection;"
        }
    .end annotation

    .line 43
    invoke-virtual {p0, p1}, Lcom/squareup/ui/ticket/MoveTicketScreen$Module;->provideTicketSelectionSession(Lcom/squareup/BundleKey;)Lcom/squareup/ui/ticket/TicketSelection;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/ticket/TicketSelection;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/ticket/TicketSelection;
    .locals 2

    .line 32
    iget-object v0, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketSelectionSessionFactory;->module:Lcom/squareup/ui/ticket/MoveTicketScreen$Module;

    iget-object v1, p0, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketSelectionSessionFactory;->selectedTicketsInfoKeyProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/BundleKey;

    invoke-static {v0, v1}, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketSelectionSessionFactory;->provideTicketSelectionSession(Lcom/squareup/ui/ticket/MoveTicketScreen$Module;Lcom/squareup/BundleKey;)Lcom/squareup/ui/ticket/TicketSelection;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MoveTicketScreen_Module_ProvideTicketSelectionSessionFactory;->get()Lcom/squareup/ui/ticket/TicketSelection;

    move-result-object v0

    return-object v0
.end method
