.class final Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;
.super Lmortar/ViewPresenter;
.source "MergeTicketScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/ticket/MergeTicketScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "MergeTicketPresenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/ticket/MergeTicketView;",
        ">;"
    }
.end annotation


# static fields
.field private static final DESTINATION_TICKET_INFO_KEY:Ljava/lang/String; = "merge_ticket_presenter_destination_ticket_info_key"

.field private static final HUD_DELAY_LONG:J = 0x2bcL

.field private static final HUD_DELAY_MEDIUM:J = 0x190L

.field private static final HUD_DELAY_SHORT:J = 0x64L

.field private static final MERGE_REQUEST_IN_PROGRESS_KEY:Ljava/lang/String; = "merge_ticket_presenter_merge_request_in_progress_key"

.field private static final TICKET_COUNT_KEY:Ljava/lang/String; = "merge_ticket_presenter_ticket_count_key"


# instance fields
.field final blockingPresenter:Lcom/squareup/caller/BlockingPopupPresenter;

.field private destinationTicketInfo:Lcom/squareup/ui/ticket/TicketInfo;

.field private final flow:Lflow/Flow;

.field private forTransactionTicket:Z

.field private final hudToaster:Lcom/squareup/hudtoaster/HudToaster;

.field private final mainThread:Lcom/squareup/thread/executor/MainThread;

.field final mergeInFlightPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/flowlegacy/NoResultPopupPresenter<",
            "Lcom/squareup/caller/ProgressPopup$Progress;",
            ">;"
        }
    .end annotation
.end field

.field private mergeRequestInProgress:Z

.field private final mergeTicketListener:Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketListener;

.field private mergingWasSlow:Z

.field private final openTicketsRunner:Lcom/squareup/ui/ticket/OpenTicketsRunner;

.field private final openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

.field private final orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

.field private final possibleDestinationTickets:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/ui/ticket/TicketInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field private final ticketActionScopeRunner:Lcom/squareup/ui/ticket/TicketActionScopeRunner;

.field private ticketCount:I

.field private final tickets:Lcom/squareup/tickets/Tickets;

.field private final ticketsLoader:Lcom/squareup/ui/ticket/TicketsLoader;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method constructor <init>(Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/ui/ticket/OpenTicketsRunner;Lcom/squareup/tickets/Tickets;Lcom/squareup/ui/ticket/TicketsLoader;Lcom/squareup/ui/ticket/TicketActionScopeRunner;Lcom/squareup/payment/Transaction;Lflow/Flow;Lcom/squareup/util/Res;Lcom/squareup/thread/executor/MainThread;Lcom/squareup/print/OrderPrintingDispatcher;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketListener;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 116
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 117
    iput-object p1, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    .line 118
    iput-object p2, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->openTicketsRunner:Lcom/squareup/ui/ticket/OpenTicketsRunner;

    .line 119
    iput-object p3, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->tickets:Lcom/squareup/tickets/Tickets;

    .line 120
    iput-object p4, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->ticketsLoader:Lcom/squareup/ui/ticket/TicketsLoader;

    .line 121
    iput-object p5, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->ticketActionScopeRunner:Lcom/squareup/ui/ticket/TicketActionScopeRunner;

    .line 122
    iput-object p6, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->transaction:Lcom/squareup/payment/Transaction;

    .line 123
    iput-object p7, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->flow:Lflow/Flow;

    .line 124
    iput-object p8, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->res:Lcom/squareup/util/Res;

    .line 125
    iput-object p9, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->mainThread:Lcom/squareup/thread/executor/MainThread;

    .line 126
    iput-object p10, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

    .line 127
    iput-object p11, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    .line 128
    iput-object p12, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->mergeTicketListener:Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketListener;

    .line 129
    new-instance p1, Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    invoke-direct {p1}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->mergeInFlightPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    .line 130
    new-instance p1, Lcom/squareup/caller/BlockingPopupPresenter;

    invoke-direct {p1, p9}, Lcom/squareup/caller/BlockingPopupPresenter;-><init>(Lcom/squareup/thread/executor/MainThread;)V

    iput-object p1, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->blockingPresenter:Lcom/squareup/caller/BlockingPopupPresenter;

    .line 131
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->possibleDestinationTickets:Ljava/util/List;

    return-void
.end method

.method private buildActionBar(Z)V
    .locals 4

    .line 256
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ticket/MergeTicketView;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/MergeTicketView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    .line 257
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar;->getConfig()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config;->buildUpon()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 258
    invoke-direct {p0}, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->getUpButtonGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;

    move-result-object v2

    invoke-direct {p0}, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->getActionBarTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    new-instance v2, Lcom/squareup/ui/ticket/-$$Lambda$ZUzOT4WHM4K0FoT8EIXUT1WFOzk;

    invoke-direct {v2, p0}, Lcom/squareup/ui/ticket/-$$Lambda$ZUzOT4WHM4K0FoT8EIXUT1WFOzk;-><init>(Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;)V

    .line 259
    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/orderentry/R$string;->open_tickets_merge:I

    .line 260
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 261
    invoke-virtual {v1, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$FGO9NAKpqNWU4GaHAz2inCUD3LM;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/-$$Lambda$FGO9NAKpqNWU4GaHAz2inCUD3LM;-><init>(Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;)V

    .line 262
    invoke-virtual {p1, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 263
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method private getActionBarTitle()Ljava/lang/String;
    .locals 3

    .line 274
    iget-boolean v0, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->mergeRequestInProgress:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->ticketCount:I

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->possibleDestinationTickets:Ljava/util/List;

    .line 276
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 277
    :goto_0
    iget-object v1, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/orderentry/R$string;->open_tickets_merge_tickets_prompt:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v1

    .line 278
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    const-string v2, "number"

    invoke-virtual {v1, v2, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    .line 279
    invoke-virtual {v0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v0

    .line 280
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getUpButtonGlyph()Lcom/squareup/glyph/GlyphTypeface$Glyph;
    .locals 1

    .line 267
    iget-object v0, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasTicket()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 268
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    return-object v0

    .line 270
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->showMasterDetailTicketScreen()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    :goto_0
    return-object v0
.end method

.method private listenForTrasactionTicketUpdates(Lmortar/MortarScope;)V
    .locals 2

    .line 345
    iget-object v0, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->tickets:Lcom/squareup/tickets/Tickets;

    invoke-interface {v0}, Lcom/squareup/tickets/Tickets;->onLocalTicketsUpdated()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$MergeTicketScreen$MergeTicketPresenter$NVaEuW0elrIIwYSWakbIbTvCfsM;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/-$$Lambda$MergeTicketScreen$MergeTicketPresenter$NVaEuW0elrIIwYSWakbIbTvCfsM;-><init>(Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    return-void
.end method

.method private mergingTransactionTicket()Z
    .locals 2

    .line 337
    iget-object v0, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasTicket()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    .line 340
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->possibleDestinationTickets:Ljava/util/List;

    invoke-static {v0}, Lcom/squareup/ui/ticket/TicketInfo;->extractIds(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 341
    iget-object v1, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->getTicketId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private showMasterDetailTicketScreen()Z
    .locals 1

    .line 356
    iget-object v0, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v0}, Lcom/squareup/tickets/OpenTicketsSettings;->isOpenTicketsAsHomeScreenEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    .line 357
    invoke-interface {v0}, Lcom/squareup/tickets/OpenTicketsSettings;->isPredefinedTicketsEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method


# virtual methods
.method doMerge()V
    .locals 4

    .line 291
    iget-object v0, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->ticketActionScopeRunner:Lcom/squareup/ui/ticket/TicketActionScopeRunner;

    iget-object v1, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->possibleDestinationTickets:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/ui/ticket/TicketInfo;->extractIds(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->destinationTicketInfo:Lcom/squareup/ui/ticket/TicketInfo;

    iget-object v2, v2, Lcom/squareup/ui/ticket/TicketInfo;->id:Ljava/lang/String;

    new-instance v3, Lcom/squareup/ui/ticket/-$$Lambda$MergeTicketScreen$MergeTicketPresenter$489duEQwEO_TgWIMda_pdUpnbQA;

    invoke-direct {v3, p0}, Lcom/squareup/ui/ticket/-$$Lambda$MergeTicketScreen$MergeTicketPresenter$489duEQwEO_TgWIMda_pdUpnbQA;-><init>(Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;)V

    invoke-virtual {v0, v1, v2, v3}, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->mergeTicketsTo(Ljava/util/List;Ljava/lang/String;Lcom/squareup/tickets/TicketsCallback;)V

    return-void
.end method

.method exit()V
    .locals 2

    .line 246
    iget-object v0, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasTicket()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 247
    iget-object v0, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-static {v0}, Lcom/squareup/ui/ticket/TicketSelection;->ticketInfoFromTransaction(Lcom/squareup/payment/Transaction;)Lcom/squareup/ui/ticket/TicketInfo;

    move-result-object v0

    .line 248
    iget-object v1, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->ticketActionScopeRunner:Lcom/squareup/ui/ticket/TicketActionScopeRunner;

    invoke-virtual {v1, v0}, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->isTicketSelected(Lcom/squareup/ui/ticket/TicketInfo;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 249
    iget-object v1, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->ticketActionScopeRunner:Lcom/squareup/ui/ticket/TicketActionScopeRunner;

    invoke-virtual {v1, v0}, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->toggleSelected(Lcom/squareup/ui/ticket/TicketInfo;)V

    .line 252
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->flow:Lflow/Flow;

    invoke-virtual {v0}, Lflow/Flow;->goBack()Z

    return-void
.end method

.method getToastText(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 284
    iget-object v0, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/orderentry/R$string;->open_tickets_merge_tickets_success:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v0

    const-string v1, "ticket_name"

    .line 285
    invoke-virtual {v0, v1, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 286
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 287
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method isTicketSelected(Lcom/squareup/ui/ticket/TicketInfo;)Z
    .locals 1

    .line 184
    iget-object v0, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->destinationTicketInfo:Lcom/squareup/ui/ticket/TicketInfo;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/squareup/ui/ticket/TicketInfo;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public synthetic lambda$doMerge$2$MergeTicketScreen$MergeTicketPresenter(Lcom/squareup/tickets/TicketsResult;)V
    .locals 4

    .line 294
    iget-object v0, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->blockingPresenter:Lcom/squareup/caller/BlockingPopupPresenter;

    invoke-virtual {v0}, Lcom/squareup/caller/BlockingPopupPresenter;->dismiss()V

    .line 295
    iget-object v0, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->mergeInFlightPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    invoke-virtual {v0}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->dismiss()V

    .line 298
    iget-object v0, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->mergeTicketListener:Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketListener;

    invoke-static {v0}, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketListener;->access$000(Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketListener;)V

    .line 304
    invoke-interface {p1}, Lcom/squareup/tickets/TicketsResult;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/tickets/Tickets$MergeResults;

    .line 305
    iget-boolean v0, p1, Lcom/squareup/tickets/Tickets$MergeResults;->canceledDueToClosedTicket:Z

    if-eqz v0, :cond_1

    .line 306
    iget-boolean p1, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->forTransactionTicket:Z

    if-eqz p1, :cond_0

    .line 307
    iget-object p1, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->flow:Lflow/Flow;

    iget v0, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->ticketCount:I

    invoke-static {v0}, Lcom/squareup/ui/ticket/MergeCanceledDialogScreen;->forMergeWithTransactionticket(I)Lcom/squareup/ui/ticket/MergeCanceledDialogScreen;

    move-result-object v0

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    goto :goto_0

    .line 309
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->flow:Lflow/Flow;

    iget v0, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->ticketCount:I

    invoke-static {v0}, Lcom/squareup/ui/ticket/MergeCanceledDialogScreen;->forBulkMerge(I)Lcom/squareup/ui/ticket/MergeCanceledDialogScreen;

    move-result-object v0

    invoke-virtual {p1, v0}, Lflow/Flow;->set(Ljava/lang/Object;)V

    :goto_0
    return-void

    .line 316
    :cond_1
    iget-boolean v0, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->mergingWasSlow:Z

    if-eqz v0, :cond_2

    const-wide/16 v0, 0x2bc

    goto :goto_1

    .line 319
    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {v0}, Lcom/squareup/tickets/OpenTicketsSettings;->isOpenTicketsAsHomeScreenEnabled()Z

    move-result v0

    if-eqz v0, :cond_3

    const-wide/16 v0, 0x64

    goto :goto_1

    :cond_3
    const-wide/16 v0, 0x190

    .line 323
    :goto_1
    iget-object v2, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->mainThread:Lcom/squareup/thread/executor/MainThread;

    new-instance v3, Lcom/squareup/ui/ticket/-$$Lambda$MergeTicketScreen$MergeTicketPresenter$wd9Six0lymD9--8qyX2ucEj5H-c;

    invoke-direct {v3, p0, p1}, Lcom/squareup/ui/ticket/-$$Lambda$MergeTicketScreen$MergeTicketPresenter$wd9Six0lymD9--8qyX2ucEj5H-c;-><init>(Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;Lcom/squareup/tickets/Tickets$MergeResults;)V

    invoke-interface {v2, v3, v0, v1}, Lcom/squareup/thread/executor/MainThread;->executeDelayed(Ljava/lang/Runnable;J)Z

    .line 328
    iget-boolean p1, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->forTransactionTicket:Z

    if-eqz p1, :cond_4

    .line 329
    iget-object p1, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->openTicketsRunner:Lcom/squareup/ui/ticket/OpenTicketsRunner;

    iget-object v0, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->flow:Lflow/Flow;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/ticket/OpenTicketsRunner;->finishMergeTicketsFromCartMenu(Lflow/Flow;)V

    return-void

    .line 332
    :cond_4
    iget-object p1, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->openTicketsRunner:Lcom/squareup/ui/ticket/OpenTicketsRunner;

    iget-object v0, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->flow:Lflow/Flow;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/ticket/OpenTicketsRunner;->finishMergeTicketsFromEditBar(Lflow/Flow;)V

    return-void
.end method

.method public synthetic lambda$listenForTrasactionTicketUpdates$3$MergeTicketScreen$MergeTicketPresenter(Ljava/util/List;)V
    .locals 1

    .line 346
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/tickets/LocalTicketUpdateEvent;

    .line 347
    invoke-virtual {v0}, Lcom/squareup/tickets/LocalTicketUpdateEvent;->isUnlockedByUpdate()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 348
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->doMerge()V

    :cond_1
    return-void
.end method

.method public synthetic lambda$mergeTickets$0$MergeTicketScreen$MergeTicketPresenter()V
    .locals 4

    const/4 v0, 0x1

    .line 201
    iput-boolean v0, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->mergingWasSlow:Z

    .line 202
    iget-object v0, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->mergeInFlightPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    new-instance v1, Lcom/squareup/caller/ProgressPopup$Progress;

    iget-object v2, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/orderentry/R$string;->open_tickets_merge_in_progress:I

    .line 203
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/caller/ProgressPopup$Progress;-><init>(Ljava/lang/String;)V

    .line 202
    invoke-virtual {v0, v1}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->show(Landroid/os/Parcelable;)V

    return-void
.end method

.method public synthetic lambda$null$1$MergeTicketScreen$MergeTicketPresenter(Lcom/squareup/tickets/Tickets$MergeResults;)V
    .locals 3

    .line 324
    iget-object v0, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_CHECK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object p1, p1, Lcom/squareup/tickets/Tickets$MergeResults;->ticketName:Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->getToastText(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const/4 v2, 0x0

    invoke-interface {v0, v1, p1, v2}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    return-void
.end method

.method mergeTickets()V
    .locals 4

    .line 188
    iget-object v0, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->destinationTicketInfo:Lcom/squareup/ui/ticket/TicketInfo;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/squareup/ui/ticket/TicketInfo;->id:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string v3, "Destination ticket id cannot be null when merging tickets"

    invoke-static {v0, v3}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 191
    iput-boolean v1, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->mergeRequestInProgress:Z

    .line 192
    iget-object v0, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->possibleDestinationTickets:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->ticketCount:I

    .line 193
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ticket/MergeTicketView;

    .line 194
    invoke-virtual {v0, v2}, Lcom/squareup/ui/ticket/MergeTicketView;->setMergeButtonEnabled(Z)V

    .line 200
    iget-object v0, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->blockingPresenter:Lcom/squareup/caller/BlockingPopupPresenter;

    new-instance v1, Lcom/squareup/ui/ticket/-$$Lambda$MergeTicketScreen$MergeTicketPresenter$oPxkQBGRu_p6Z1NCv2Rwx6BPS-o;

    invoke-direct {v1, p0}, Lcom/squareup/ui/ticket/-$$Lambda$MergeTicketScreen$MergeTicketPresenter$oPxkQBGRu_p6Z1NCv2Rwx6BPS-o;-><init>(Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;)V

    invoke-virtual {v0, v1}, Lcom/squareup/caller/BlockingPopupPresenter;->show(Ljava/lang/Runnable;)V

    .line 206
    iget-boolean v0, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->forTransactionTicket:Z

    if-eqz v0, :cond_1

    .line 208
    iget-object v0, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->transaction:Lcom/squareup/payment/Transaction;

    iget-object v1, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->destinationTicketInfo:Lcom/squareup/ui/ticket/TicketInfo;

    iget-object v1, v1, Lcom/squareup/ui/ticket/TicketInfo;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->destinationTicketInfo:Lcom/squareup/ui/ticket/TicketInfo;

    iget-object v2, v2, Lcom/squareup/ui/ticket/TicketInfo;->note:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/payment/Transaction;->setOpenTicketNameAndNote(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    iget-object v0, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->orderPrintingDispatcher:Lcom/squareup/print/OrderPrintingDispatcher;

    iget-object v1, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->getOrder()Lcom/squareup/payment/Order;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->transaction:Lcom/squareup/payment/Transaction;

    .line 211
    invoke-virtual {v2}, Lcom/squareup/payment/Transaction;->getDisplayNameOrDefault()Ljava/lang/String;

    move-result-object v2

    .line 210
    invoke-virtual {v0, v1, v2}, Lcom/squareup/print/OrderPrintingDispatcher;->printStubAndTicket(Lcom/squareup/payment/Order;Ljava/lang/String;)V

    .line 217
    iget-object v0, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->updateCurrentTicketBeforeReset()V

    .line 218
    iget-object v0, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->reset()V

    return-void

    .line 227
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->doMerge()V

    return-void
.end method

.method onBackPressed()Z
    .locals 1

    .line 235
    iget-boolean v0, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->mergeRequestInProgress:Z

    return v0
.end method

.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    .line 135
    invoke-direct {p0, p1}, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->listenForTrasactionTicketUpdates(Lmortar/MortarScope;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 3

    .line 139
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    if-nez p1, :cond_0

    .line 142
    iget-object p1, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->possibleDestinationTickets:Ljava/util/List;

    iget-object v0, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->ticketActionScopeRunner:Lcom/squareup/ui/ticket/TicketActionScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/ticket/TicketActionScopeRunner;->getSelectedTicketsInfo()Ljava/util/List;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 143
    iget-object p1, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->possibleDestinationTickets:Ljava/util/List;

    invoke-static {p1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    goto :goto_0

    :cond_0
    const-string v0, "merge_ticket_presenter_destination_ticket_info_key"

    .line 145
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/ticket/TicketInfo;

    iput-object v0, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->destinationTicketInfo:Lcom/squareup/ui/ticket/TicketInfo;

    const-string v0, "merge_ticket_presenter_merge_request_in_progress_key"

    .line 146
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->mergeRequestInProgress:Z

    const-string v0, "merge_ticket_presenter_ticket_count_key"

    .line 147
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result p1

    iput p1, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->ticketCount:I

    .line 150
    :goto_0
    invoke-direct {p0}, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->mergingTransactionTicket()Z

    move-result p1

    iput-boolean p1, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->forTransactionTicket:Z

    .line 156
    iget-boolean p1, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->forTransactionTicket:Z

    if-nez p1, :cond_1

    iget-object p1, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->openTicketsSettings:Lcom/squareup/tickets/OpenTicketsSettings;

    invoke-interface {p1}, Lcom/squareup/tickets/OpenTicketsSettings;->isOpenTicketsAsHomeScreenEnabled()Z

    move-result p1

    if-nez p1, :cond_2

    .line 157
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->ticketsLoader:Lcom/squareup/ui/ticket/TicketsLoader;

    sget-object v0, Lcom/squareup/tickets/TicketSort;->NAME:Lcom/squareup/tickets/TicketSort;

    sget-object v1, Lcom/squareup/tickets/TicketStore$EmployeeAccess;->IGNORE_EMPLOYEES:Lcom/squareup/tickets/TicketStore$EmployeeAccess;

    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0, v2, v1}, Lcom/squareup/ui/ticket/TicketsLoader;->syncAllTickets(Ljava/util/List;Lcom/squareup/tickets/TicketSort;Ljava/lang/String;Lcom/squareup/tickets/TicketStore$EmployeeAccess;)V

    .line 160
    :cond_2
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/ticket/MergeTicketView;

    .line 161
    iget-object v0, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->destinationTicketInfo:Lcom/squareup/ui/ticket/TicketInfo;

    const/4 v1, 0x0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    :goto_1
    invoke-direct {p0, v0}, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->buildActionBar(Z)V

    .line 162
    iget-object v0, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->possibleDestinationTickets:Ljava/util/List;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/ticket/MergeTicketView;->setTicketList(Ljava/util/List;)V

    .line 164
    iget-boolean v0, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->mergeRequestInProgress:Z

    if-eqz v0, :cond_4

    .line 165
    invoke-virtual {p1, v1}, Lcom/squareup/ui/ticket/MergeTicketView;->setMergeButtonEnabled(Z)V

    :cond_4
    return-void
.end method

.method protected onSave(Landroid/os/Bundle;)V
    .locals 2

    .line 170
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onSave(Landroid/os/Bundle;)V

    .line 171
    iget-object v0, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->destinationTicketInfo:Lcom/squareup/ui/ticket/TicketInfo;

    const-string v1, "merge_ticket_presenter_destination_ticket_info_key"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 172
    iget-boolean v0, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->mergeRequestInProgress:Z

    const-string v1, "merge_ticket_presenter_merge_request_in_progress_key"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 173
    iget v0, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->ticketCount:I

    const-string v1, "merge_ticket_presenter_ticket_count_key"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method

.method onTicketClicked(Lcom/squareup/ui/ticket/TicketInfo;)V
    .locals 1

    .line 177
    iput-object p1, p0, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->destinationTicketInfo:Lcom/squareup/ui/ticket/TicketInfo;

    .line 178
    invoke-virtual {p0}, Lcom/squareup/ui/ticket/MergeTicketScreen$MergeTicketPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/ticket/MergeTicketView;

    const/4 v0, 0x1

    .line 179
    invoke-virtual {p1, v0}, Lcom/squareup/ui/ticket/MergeTicketView;->setMergeButtonEnabled(Z)V

    .line 180
    invoke-virtual {p1}, Lcom/squareup/ui/ticket/MergeTicketView;->refreshTicketList()V

    return-void
.end method
