.class public final Lcom/squareup/ui/settings/taxes/tax/TaxScope;
.super Lcom/squareup/ui/settings/InSettingsAppletScope;
.source "TaxScope.java"

# interfaces
.implements Lcom/squareup/container/RegistersInScope;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/settings/taxes/tax/TaxScope$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/taxes/tax/TaxScope$Component;,
        Lcom/squareup/ui/settings/taxes/tax/TaxScope$Module;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/taxes/tax/TaxScope;",
            ">;"
        }
    .end annotation
.end field

.field public static final NEW_TAX:Ljava/lang/String;


# instance fields
.field final cogsTaxId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 83
    sget-object v0, Lcom/squareup/ui/settings/taxes/tax/-$$Lambda$TaxScope$n9CFTUqcA4Jo8F6y5UPxUf2no8k;->INSTANCE:Lcom/squareup/ui/settings/taxes/tax/-$$Lambda$TaxScope$n9CFTUqcA4Jo8F6y5UPxUf2no8k;

    .line 84
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/settings/taxes/tax/TaxScope;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 29
    invoke-direct {p0}, Lcom/squareup/ui/settings/InSettingsAppletScope;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxScope;->cogsTaxId:Ljava/lang/String;

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/settings/taxes/tax/TaxScope;
    .locals 1

    .line 84
    new-instance v0, Lcom/squareup/ui/settings/taxes/tax/TaxScope;

    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/taxes/tax/TaxScope;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 79
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/settings/InSettingsAppletScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 80
    iget-object p2, p0, Lcom/squareup/ui/settings/taxes/tax/TaxScope;->cogsTaxId:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .line 44
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Lcom/squareup/ui/settings/InSettingsAppletScope;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxScope;->cogsTaxId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isNewTax()Z
    .locals 2

    .line 39
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxScope;->cogsTaxId:Ljava/lang/String;

    sget-object v1, Lcom/squareup/ui/settings/taxes/tax/TaxScope;->NEW_TAX:Ljava/lang/String;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public register(Lmortar/MortarScope;)V
    .locals 1

    .line 34
    const-class v0, Lcom/squareup/ui/settings/taxes/tax/TaxScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/taxes/tax/TaxScope$Component;

    .line 35
    invoke-interface {v0}, Lcom/squareup/ui/settings/taxes/tax/TaxScope$Component;->taxScopeRunner()Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;

    move-result-object v0

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    return-void
.end method
