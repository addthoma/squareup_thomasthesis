.class public Lcom/squareup/ui/settings/merchantprofile/PublicProfileSection$Access;
.super Lcom/squareup/ui/settings/SettingsSectionAccess$Restricted;
.source "PublicProfileSection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/merchantprofile/PublicProfileSection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Access"
.end annotation


# instance fields
.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/AccountStatusSettings;)V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Lcom/squareup/permissions/Permission;

    .line 78
    invoke-direct {p0, v0}, Lcom/squareup/ui/settings/SettingsSectionAccess$Restricted;-><init>([Lcom/squareup/permissions/Permission;)V

    .line 79
    iput-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/PublicProfileSection$Access;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    return-void
.end method


# virtual methods
.method public determineVisibility()Z
    .locals 1

    .line 83
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/PublicProfileSection$Access;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->canPublishMerchantProfile()Z

    move-result v0

    return v0
.end method
