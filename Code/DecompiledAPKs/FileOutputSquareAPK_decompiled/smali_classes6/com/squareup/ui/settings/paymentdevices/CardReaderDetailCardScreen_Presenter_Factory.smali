.class public final Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen_Presenter_Factory;
.super Ljava/lang/Object;
.source "CardReaderDetailCardScreen_Presenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final apiReaderSettingsControllerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiReaderSettingsController;",
            ">;"
        }
    .end annotation
.end field

.field private final bluetoothUtilsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderHubProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;"
        }
    .end annotation
.end field

.field private final cardReaderOracleProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;",
            ">;"
        }
    .end annotation
.end field

.field private final detailDelegateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;",
            ">;"
        }
    .end annotation
.end field

.field private final flowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;"
        }
    .end annotation
.end field

.field private final readerEventLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/ReaderEventLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final storedCardReadersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;",
            ">;"
        }
    .end annotation
.end field

.field private final watchdogProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/RxWatchdog<",
            "Lcom/squareup/ui/settings/paymentdevices/ReaderState;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiReaderSettingsController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/ReaderEventLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/RxWatchdog<",
            "Lcom/squareup/ui/settings/paymentdevices/ReaderState;",
            ">;>;)V"
        }
    .end annotation

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen_Presenter_Factory;->apiReaderSettingsControllerProvider:Ljavax/inject/Provider;

    .line 57
    iput-object p2, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen_Presenter_Factory;->cardReaderFactoryProvider:Ljavax/inject/Provider;

    .line 58
    iput-object p3, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen_Presenter_Factory;->cardReaderHubProvider:Ljavax/inject/Provider;

    .line 59
    iput-object p4, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen_Presenter_Factory;->cardReaderOracleProvider:Ljavax/inject/Provider;

    .line 60
    iput-object p5, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    .line 61
    iput-object p6, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen_Presenter_Factory;->readerEventLoggerProvider:Ljavax/inject/Provider;

    .line 62
    iput-object p7, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen_Presenter_Factory;->detailDelegateProvider:Ljavax/inject/Provider;

    .line 63
    iput-object p8, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen_Presenter_Factory;->storedCardReadersProvider:Ljavax/inject/Provider;

    .line 64
    iput-object p9, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen_Presenter_Factory;->bluetoothUtilsProvider:Ljavax/inject/Provider;

    .line 65
    iput-object p10, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen_Presenter_Factory;->flowProvider:Ljavax/inject/Provider;

    .line 66
    iput-object p11, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen_Presenter_Factory;->watchdogProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen_Presenter_Factory;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiReaderSettingsController;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderFactory;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/CardReaderHub;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/ReaderEventLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lflow/Flow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/RxWatchdog<",
            "Lcom/squareup/ui/settings/paymentdevices/ReaderState;",
            ">;>;)",
            "Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen_Presenter_Factory;"
        }
    .end annotation

    .line 84
    new-instance v12, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen_Presenter_Factory;

    move-object v0, v12

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen_Presenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v12
.end method

.method public static newInstance(Lcom/squareup/api/ApiReaderSettingsController;Lcom/squareup/cardreader/CardReaderFactory;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lcom/squareup/util/Res;Lcom/squareup/log/ReaderEventLogger;Ljava/lang/Object;Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;Lcom/squareup/cardreader/BluetoothUtils;Lflow/Flow;Lcom/squareup/util/RxWatchdog;)Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/api/ApiReaderSettingsController;",
            "Lcom/squareup/cardreader/CardReaderFactory;",
            "Lcom/squareup/cardreader/CardReaderHub;",
            "Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/log/ReaderEventLogger;",
            "Ljava/lang/Object;",
            "Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;",
            "Lcom/squareup/cardreader/BluetoothUtils;",
            "Lflow/Flow;",
            "Lcom/squareup/util/RxWatchdog<",
            "Lcom/squareup/ui/settings/paymentdevices/ReaderState;",
            ">;)",
            "Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;"
        }
    .end annotation

    .line 93
    new-instance v12, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;

    move-object/from16 v7, p6

    check-cast v7, Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;

    move-object v0, v12

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;-><init>(Lcom/squareup/api/ApiReaderSettingsController;Lcom/squareup/cardreader/CardReaderFactory;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lcom/squareup/util/Res;Lcom/squareup/log/ReaderEventLogger;Lcom/squareup/ui/settings/paymentdevices/DetailDelegate;Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;Lcom/squareup/cardreader/BluetoothUtils;Lflow/Flow;Lcom/squareup/util/RxWatchdog;)V

    return-object v12
.end method


# virtual methods
.method public get()Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;
    .locals 12

    .line 71
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen_Presenter_Factory;->apiReaderSettingsControllerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/api/ApiReaderSettingsController;

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen_Presenter_Factory;->cardReaderFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/cardreader/CardReaderFactory;

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen_Presenter_Factory;->cardReaderHubProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/cardreader/CardReaderHub;

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen_Presenter_Factory;->cardReaderOracleProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen_Presenter_Factory;->readerEventLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/log/ReaderEventLogger;

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen_Presenter_Factory;->detailDelegateProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v7

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen_Presenter_Factory;->storedCardReadersProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen_Presenter_Factory;->bluetoothUtilsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/cardreader/BluetoothUtils;

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen_Presenter_Factory;->flowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lflow/Flow;

    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen_Presenter_Factory;->watchdogProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/squareup/util/RxWatchdog;

    invoke-static/range {v1 .. v11}, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen_Presenter_Factory;->newInstance(Lcom/squareup/api/ApiReaderSettingsController;Lcom/squareup/cardreader/CardReaderFactory;Lcom/squareup/cardreader/CardReaderHub;Lcom/squareup/ui/settings/paymentdevices/CardReaderOracle;Lcom/squareup/util/Res;Lcom/squareup/log/ReaderEventLogger;Ljava/lang/Object;Lcom/squareup/ui/settings/paymentdevices/StoredCardReaders;Lcom/squareup/cardreader/BluetoothUtils;Lflow/Flow;Lcom/squareup/util/RxWatchdog;)Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 15
    invoke-virtual {p0}, Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen_Presenter_Factory;->get()Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailCardScreen$Presenter;

    move-result-object v0

    return-object v0
.end method
