.class public final Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScreen;
.super Lcom/squareup/ui/settings/loyalty/InLoyaltySettingsScope;
.source "LoyaltySettingsScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/coordinators/CoordinatorProvider;
.implements Lcom/squareup/container/layer/InSection;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScreen$ScreenData;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 42
    new-instance v0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScreen;

    invoke-direct {v0}, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScreen;->INSTANCE:Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScreen;

    .line 64
    sget-object v0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScreen;->INSTANCE:Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScreen;

    .line 65
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 44
    invoke-direct {p0}, Lcom/squareup/ui/settings/loyalty/InLoyaltySettingsScope;-><init>()V

    return-void
.end method


# virtual methods
.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 56
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_LOYALTY:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getSection()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 48
    const-class v0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsSection;

    return-object v0
.end method

.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 1

    .line 52
    const-class v0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScope$Component;

    invoke-interface {p1}, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScope$Component;->loyaltySettingsCoordinator()Lcom/squareup/ui/settings/loyalty/LoyaltySettingsCoordinator;

    move-result-object p1

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 60
    sget v0, Lcom/squareup/settingsapplet/R$layout;->loyalty_settings_screen:I

    return v0
.end method
