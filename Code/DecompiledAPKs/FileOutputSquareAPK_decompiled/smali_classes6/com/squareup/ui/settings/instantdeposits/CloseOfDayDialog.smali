.class public final Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialog;
.super Lcom/squareup/ui/settings/instantdeposits/InDepositSettingsScope;
.source "CloseOfDayDialog.kt"

# interfaces
.implements Lcom/squareup/container/layer/InSection;


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialog$Factory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialog$ScreenData;,
        Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialog$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCloseOfDayDialog.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CloseOfDayDialog.kt\ncom/squareup/ui/settings/instantdeposits/CloseOfDayDialog\n+ 2 Container.kt\ncom/squareup/container/ContainerKt\n*L\n1#1,86:1\n24#2,4:87\n*E\n*S KotlinDebug\n*F\n+ 1 CloseOfDayDialog.kt\ncom/squareup/ui/settings/instantdeposits/CloseOfDayDialog\n*L\n29#1,4:87\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0008\u00c7\u0002\u0018\u00002\u00020\u00012\u00020\u0002:\u0002\n\u000bB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0003R\u0016\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00000\u00058\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0018\u0010\u0006\u001a\u0006\u0012\u0002\u0008\u00030\u00078VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0008\u0010\t\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialog;",
        "Lcom/squareup/ui/settings/instantdeposits/InDepositSettingsScope;",
        "Lcom/squareup/container/layer/InSection;",
        "()V",
        "CREATOR",
        "Landroid/os/Parcelable$Creator;",
        "section",
        "Ljava/lang/Class;",
        "getSection",
        "()Ljava/lang/Class;",
        "Factory",
        "ScreenData",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialog;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialog;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 25
    new-instance v0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialog;

    invoke-direct {v0}, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialog;-><init>()V

    sput-object v0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialog;->INSTANCE:Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialog;

    .line 87
    new-instance v0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialog$$special$$inlined$pathCreator$1;

    invoke-direct {v0}, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialog$$special$$inlined$pathCreator$1;-><init>()V

    check-cast v0, Landroid/os/Parcelable$Creator;

    .line 90
    sput-object v0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialog;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 25
    invoke-direct {p0}, Lcom/squareup/ui/settings/instantdeposits/InDepositSettingsScope;-><init>()V

    return-void
.end method


# virtual methods
.method public getSection()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 27
    const-class v0, Lcom/squareup/ui/settings/instantdeposits/DepositsSection;

    return-object v0
.end method
