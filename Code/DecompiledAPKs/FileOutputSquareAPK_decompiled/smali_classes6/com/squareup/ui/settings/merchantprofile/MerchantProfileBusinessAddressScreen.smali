.class public final Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressScreen;
.super Lcom/squareup/ui/settings/InSettingsAppletScope;
.source "MerchantProfileBusinessAddressScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/layer/InSection;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressScreen$Component;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 20
    new-instance v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressScreen;

    invoke-direct {v0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressScreen;->INSTANCE:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressScreen;

    .line 39
    sget-object v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressScreen;->INSTANCE:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressScreen;

    .line 40
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 22
    invoke-direct {p0}, Lcom/squareup/ui/settings/InSettingsAppletScope;-><init>()V

    return-void
.end method


# virtual methods
.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 26
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_PUBLIC_PROFILE_ADDRESS:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getSection()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 30
    const-class v0, Lcom/squareup/ui/settings/merchantprofile/PublicProfileSection;

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 43
    sget v0, Lcom/squareup/settingsapplet/R$layout;->merchant_profile_business_address_view:I

    return v0
.end method
