.class Lcom/squareup/ui/settings/SettingsApplet$1;
.super Ljava/lang/Object;
.source "SettingsApplet.java"

# interfaces
.implements Lcom/squareup/ui/main/HistoryFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/SettingsApplet;->getHistoryFactory(Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/applet/AppletSection;)Lcom/squareup/ui/main/HistoryFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/SettingsApplet;

.field final synthetic val$screen:Lcom/squareup/ui/main/RegisterTreeKey;

.field final synthetic val$section:Lcom/squareup/applet/AppletSection;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/SettingsApplet;Lcom/squareup/ui/main/RegisterTreeKey;Lcom/squareup/applet/AppletSection;)V
    .locals 0

    .line 126
    iput-object p1, p0, Lcom/squareup/ui/settings/SettingsApplet$1;->this$0:Lcom/squareup/ui/settings/SettingsApplet;

    iput-object p2, p0, Lcom/squareup/ui/settings/SettingsApplet$1;->val$screen:Lcom/squareup/ui/main/RegisterTreeKey;

    iput-object p3, p0, Lcom/squareup/ui/settings/SettingsApplet$1;->val$section:Lcom/squareup/applet/AppletSection;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createHistory(Lcom/squareup/ui/main/Home;Lflow/History;)Lflow/History;
    .locals 1

    .line 129
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsApplet$1;->this$0:Lcom/squareup/ui/settings/SettingsApplet;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/ui/settings/SettingsApplet;->createHistory(Lcom/squareup/ui/main/Home;Lflow/History;)Lflow/History;

    move-result-object p1

    .line 130
    invoke-virtual {p1}, Lflow/History;->buildUpon()Lflow/History$Builder;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/ui/settings/SettingsApplet$1;->val$screen:Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-virtual {p1, p2}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object p1

    return-object p1
.end method

.method public getPermissions()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Permission;",
            ">;"
        }
    .end annotation

    .line 134
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsApplet$1;->val$section:Lcom/squareup/applet/AppletSection;

    invoke-virtual {v0}, Lcom/squareup/applet/AppletSection;->getAccessControl()Lcom/squareup/applet/SectionAccess;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/applet/SectionAccess;->getPermissions()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
