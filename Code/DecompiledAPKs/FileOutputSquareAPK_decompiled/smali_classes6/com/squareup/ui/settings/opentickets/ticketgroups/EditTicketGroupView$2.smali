.class Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView$2;
.super Lcom/squareup/util/RunnableOnce;
.source "EditTicketGroupView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->hideKeyboardOrAdvanceCursor()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;)V
    .locals 0

    .line 199
    iput-object p1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView$2;->this$0:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;

    invoke-direct {p0}, Lcom/squareup/util/RunnableOnce;-><init>()V

    return-void
.end method


# virtual methods
.method protected runOnce()V
    .locals 2

    .line 201
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView$2;->this$0:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;

    invoke-static {v0}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->access$300(Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;)Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView$2;->this$0:Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;

    invoke-static {v1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;->access$200(Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupView;)Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupRecyclerAdapter;->requestFocusOnNewCustomTicketRow(Landroidx/recyclerview/widget/RecyclerView;)V

    return-void
.end method
