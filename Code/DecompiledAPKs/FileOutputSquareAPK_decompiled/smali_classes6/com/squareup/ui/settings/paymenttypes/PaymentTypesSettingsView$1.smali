.class Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsView$1;
.super Ljava/lang/Object;
.source "PaymentTypesSettingsView.java"

# interfaces
.implements Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsView;->onAttachedToWindow()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsView;)V
    .locals 0

    .line 62
    iput-object p1, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsView$1;->this$0:Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTenderDragFinished(IZ)V
    .locals 1

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsView$1;->this$0:Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsView;

    .line 79
    invoke-static {v0}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsView;->access$000(Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsView;)Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroidx/recyclerview/widget/RecyclerView;->findViewHolderForAdapterPosition(I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$PaymentTypeViewHolder;

    if-eqz p1, :cond_0

    .line 87
    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesAdapter$PaymentTypeViewHolder;->setEnabled(Z)V

    :cond_0
    return-void
.end method

.method public onTenderMovedToCategory(Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;)V
    .locals 1

    .line 66
    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsView$1;->this$0:Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsView;

    iget-object v0, v0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsView;->presenter:Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;

    invoke-virtual {v0, p1, p2, p3}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;->moveTenderToCategory(Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsCategory;)V

    return-void
.end method

.method public onTenderMovedToPosition(Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsIndex;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsIndex;)V
    .locals 1

    .line 72
    iget-object v0, p0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsView$1;->this$0:Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsView;

    iget-object v0, v0, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsView;->presenter:Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;

    invoke-virtual {v0, p1, p2, p3}, Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Presenter;->moveTenderToPosition(Lcom/squareup/protos/client/devicesettings/TenderSettings$Tender;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsIndex;Lcom/squareup/tenderpayment/TenderSettingsManager$TenderSettingsIndex;)V

    return-void
.end method
