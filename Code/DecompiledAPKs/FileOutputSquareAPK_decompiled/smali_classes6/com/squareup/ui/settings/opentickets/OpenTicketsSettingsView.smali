.class public Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;
.super Landroid/widget/LinearLayout;
.source "OpenTicketsSettingsView.java"

# interfaces
.implements Lcom/squareup/ui/HasActionBar;


# instance fields
.field private final blockingPopup:Lcom/squareup/caller/BlockingPopup;

.field private final checkUnsyncedTicketsPopup:Lcom/squareup/caller/ProgressPopup;

.field private createTicketGroupButton:Lcom/squareup/marketfont/MarketButton;

.field private openTicketsAsHomeScreenEnabledRow:Lcom/squareup/widgets/list/ToggleButtonRow;

.field private openTicketsEnabledRow:Lcom/squareup/widgets/list/ToggleButtonRow;

.field private openTicketsHint:Lcom/squareup/widgets/MessageView;

.field private predefinedTicketsEnabledRow:Lcom/squareup/widgets/list/ToggleButtonRow;

.field presenter:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private recyclerView:Landroidx/recyclerview/widget/RecyclerView;

.field private recyclerViewAdapter:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter;

.field res:Lcom/squareup/util/Res;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 46
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 47
    const-class p2, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsScreen$Component;

    invoke-interface {p2, p0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsScreen$Component;->inject(Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;)V

    .line 48
    new-instance p2, Lcom/squareup/caller/ProgressPopup;

    invoke-direct {p2, p1}, Lcom/squareup/caller/ProgressPopup;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->checkUnsyncedTicketsPopup:Lcom/squareup/caller/ProgressPopup;

    .line 49
    new-instance p2, Lcom/squareup/caller/BlockingPopup;

    invoke-direct {p2, p1}, Lcom/squareup/caller/BlockingPopup;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->blockingPopup:Lcom/squareup/caller/BlockingPopup;

    return-void
.end method


# virtual methods
.method public getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 167
    invoke-static {p0}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method protected isDragging()Z
    .locals 1

    .line 152
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->recyclerViewAdapter:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter;->isDragging()Z

    move-result v0

    return v0
.end method

.method public synthetic lambda$onOpenTicketsAsHomeScreenEnabledRowInflated$1$OpenTicketsSettingsView(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 96
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->presenter:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->onOpenTicketsAsHomeScreenEnabledSwitchToggled(Z)V

    return-void
.end method

.method public synthetic lambda$onOpenTicketsEnabledRowInflated$0$OpenTicketsSettingsView(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 86
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->presenter:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->onEnableSwitchChecked(Z)V

    return-void
.end method

.method public synthetic lambda$onPredefinedTicketsEnabledRowInflated$2$OpenTicketsSettingsView(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 108
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->presenter:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->onPredefinedTicketsEnabledSwitchToggled(Z)V

    return-void
.end method

.method protected onCreateTicketGroupButtonRowInflated(Lcom/squareup/marketfont/MarketButton;)V
    .locals 1

    .line 113
    iput-object p1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->createTicketGroupButton:Lcom/squareup/marketfont/MarketButton;

    .line 114
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->createTicketGroupButton:Lcom/squareup/marketfont/MarketButton;

    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->presenter:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->isCreateTicketGroupButtonEnabled()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketButton;->setEnabled(Z)V

    .line 115
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->createTicketGroupButton:Lcom/squareup/marketfont/MarketButton;

    new-instance v0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView$1;-><init>(Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;)V

    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .line 67
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->presenter:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;

    iget-object v0, v0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->blockingPresenter:Lcom/squareup/caller/BlockingPopupPresenter;

    iget-object v1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->blockingPopup:Lcom/squareup/caller/BlockingPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/caller/BlockingPopupPresenter;->dropView(Lcom/squareup/mortar/Popup;)V

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->presenter:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;

    iget-object v0, v0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->unsyncedTicketsPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    iget-object v1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->checkUnsyncedTicketsPopup:Lcom/squareup/caller/ProgressPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->dropView(Lcom/squareup/mortar/Popup;)V

    .line 69
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->presenter:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->dropView(Landroid/view/ViewGroup;)V

    .line 70
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    .line 53
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 55
    sget v0, Lcom/squareup/settingsapplet/R$id;->open_tickets_recycler_view:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    iput-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    .line 56
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    new-instance v1, Landroidx/recyclerview/widget/LinearLayoutManager;

    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 57
    new-instance v0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter;

    iget-object v1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->presenter:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;

    iget-object v2, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->res:Lcom/squareup/util/Res;

    invoke-direct {v0, p0, v1, v2}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter;-><init>(Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;Lcom/squareup/util/Res;)V

    iput-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->recyclerViewAdapter:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter;

    .line 58
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->recyclerView:Landroidx/recyclerview/widget/RecyclerView;

    iget-object v1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->recyclerViewAdapter:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->presenter:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;

    iget-object v1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->recyclerViewAdapter:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter;->onDragStateChanged()Lrx/Observable;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->observeDragState(Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;Lio/reactivex/Observable;)V

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->presenter:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->takeView(Ljava/lang/Object;)V

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->presenter:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;

    iget-object v0, v0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->unsyncedTicketsPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    iget-object v1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->checkUnsyncedTicketsPopup:Lcom/squareup/caller/ProgressPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->takeView(Ljava/lang/Object;)V

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->presenter:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;

    iget-object v0, v0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->blockingPresenter:Lcom/squareup/caller/BlockingPopupPresenter;

    iget-object v1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->blockingPopup:Lcom/squareup/caller/BlockingPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/caller/BlockingPopupPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onOpenTicketsAsHomeScreenEnabledRowInflated(Landroid/view/View;)V
    .locals 1

    .line 91
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->openTicketsAsHomeScreenEnabledRow:Lcom/squareup/widgets/list/ToggleButtonRow;

    if-nez v0, :cond_0

    .line 92
    sget v0, Lcom/squareup/settingsapplet/R$id;->open_tickets_as_home_screen_toggle:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object p1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->openTicketsAsHomeScreenEnabledRow:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 93
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->openTicketsAsHomeScreenEnabledRow:Lcom/squareup/widgets/list/ToggleButtonRow;

    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->presenter:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->isOpenTicketsAsHomeScreenEnabled()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    .line 95
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->openTicketsAsHomeScreenEnabledRow:Lcom/squareup/widgets/list/ToggleButtonRow;

    new-instance v0, Lcom/squareup/ui/settings/opentickets/-$$Lambda$OpenTicketsSettingsView$Wgelw56h16v6iII261lJ6JeA5lA;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/opentickets/-$$Lambda$OpenTicketsSettingsView$Wgelw56h16v6iII261lJ6JeA5lA;-><init>(Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;)V

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    :cond_0
    return-void
.end method

.method protected onOpenTicketsEnabledRowInflated(Landroid/view/View;)V
    .locals 3

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->openTicketsEnabledRow:Lcom/squareup/widgets/list/ToggleButtonRow;

    if-nez v0, :cond_0

    .line 75
    sget v0, Lcom/squareup/settingsapplet/R$id;->open_tickets_toggle:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->openTicketsEnabledRow:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 76
    sget v0, Lcom/squareup/settingsapplet/R$id;->open_tickets_toggle_hint:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/MessageView;

    iput-object p1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->openTicketsHint:Lcom/squareup/widgets/MessageView;

    .line 77
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->openTicketsEnabledRow:Lcom/squareup/widgets/list/ToggleButtonRow;

    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->presenter:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->isOpenTicketsEnabled()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    .line 79
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->openTicketsHint:Lcom/squareup/widgets/MessageView;

    new-instance v0, Lcom/squareup/ui/LinkSpan$Builder;

    invoke-virtual {p0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/squareup/settingsapplet/R$string;->open_tickets_toggle_hint:I

    const-string v2, "support_center"

    .line 80
    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v0

    sget v1, Lcom/squareup/registerlib/R$string;->open_tickets_url:I

    .line 81
    invoke-virtual {v0, v1}, Lcom/squareup/ui/LinkSpan$Builder;->url(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v0

    sget v1, Lcom/squareup/checkout/R$string;->support_center:I

    .line 82
    invoke-virtual {v0, v1}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v0

    .line 83
    invoke-virtual {v0}, Lcom/squareup/ui/LinkSpan$Builder;->asCharSequence()Ljava/lang/CharSequence;

    move-result-object v0

    .line 79
    invoke-virtual {p1, v0}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 85
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->openTicketsEnabledRow:Lcom/squareup/widgets/list/ToggleButtonRow;

    new-instance v0, Lcom/squareup/ui/settings/opentickets/-$$Lambda$OpenTicketsSettingsView$hg6LME8jYNDfC1D58CMyIuS0E3w;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/opentickets/-$$Lambda$OpenTicketsSettingsView$hg6LME8jYNDfC1D58CMyIuS0E3w;-><init>(Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;)V

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    :cond_0
    return-void
.end method

.method protected onPredefinedTicketsEnabledRowInflated(Landroid/view/View;)V
    .locals 1

    .line 102
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->predefinedTicketsEnabledRow:Lcom/squareup/widgets/list/ToggleButtonRow;

    if-nez v0, :cond_0

    .line 103
    sget v0, Lcom/squareup/settingsapplet/R$id;->predefined_tickets_toggle:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object p1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->predefinedTicketsEnabledRow:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 104
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->predefinedTicketsEnabledRow:Lcom/squareup/widgets/list/ToggleButtonRow;

    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->presenter:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->isPredefinedTicketsSwitchEnabled()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/list/ToggleButtonRow;->setEnabled(Z)V

    .line 105
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->predefinedTicketsEnabledRow:Lcom/squareup/widgets/list/ToggleButtonRow;

    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->presenter:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsPresenter;->isPredefinedTicketsEnabled()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    .line 107
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->predefinedTicketsEnabledRow:Lcom/squareup/widgets/list/ToggleButtonRow;

    new-instance v0, Lcom/squareup/ui/settings/opentickets/-$$Lambda$OpenTicketsSettingsView$vlcvL4bJp6fUaqO0f320ZXjBj5k;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/opentickets/-$$Lambda$OpenTicketsSettingsView$vlcvL4bJp6fUaqO0f320ZXjBj5k;-><init>(Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;)V

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    :cond_0
    return-void
.end method

.method setCreateTicketGroupButtonEnabled(Z)V
    .locals 1

    .line 156
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->createTicketGroupButton:Lcom/squareup/marketfont/MarketButton;

    if-eqz v0, :cond_0

    .line 158
    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketButton;->setEnabled(Z)V

    :cond_0
    return-void
.end method

.method protected setEnabledSwitch(ZZ)V
    .locals 1

    .line 127
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->openTicketsEnabledRow:Lcom/squareup/widgets/list/ToggleButtonRow;

    if-eqz v0, :cond_0

    .line 129
    invoke-virtual {v0, p1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setEnabled(Z)V

    .line 130
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->openTicketsEnabledRow:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    :cond_0
    return-void
.end method

.method protected setOpenTicketsAsHomeScreenSwitch(ZZ)V
    .locals 1

    .line 135
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->recyclerViewAdapter:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter;->setAsHomeScreenSwitchVisible(Z)V

    .line 136
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->openTicketsAsHomeScreenEnabledRow:Lcom/squareup/widgets/list/ToggleButtonRow;

    if-eqz p1, :cond_0

    .line 138
    invoke-virtual {p1, p2}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    :cond_0
    return-void
.end method

.method setPredefinedTicketGroupVisible(Z)V
    .locals 1

    .line 163
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->recyclerViewAdapter:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter;->setTicketGroupsVisible(Z)V

    return-void
.end method

.method protected setPredefinedTicketsSwitch(ZZZ)V
    .locals 1

    .line 143
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->recyclerViewAdapter:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter;->setPredefinedTicketsSwitchVisible(Z)V

    .line 144
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->predefinedTicketsEnabledRow:Lcom/squareup/widgets/list/ToggleButtonRow;

    if-eqz p1, :cond_0

    .line 146
    invoke-virtual {p1, p3}, Lcom/squareup/widgets/list/ToggleButtonRow;->setEnabled(Z)V

    .line 147
    iget-object p1, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->predefinedTicketsEnabledRow:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    :cond_0
    return-void
.end method

.method protected setTicketGroupEntries(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;",
            ">;)V"
        }
    .end annotation

    .line 123
    iget-object v0, p0, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsView;->recyclerViewAdapter:Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRecyclerAdapter;->setDraggableItems(Ljava/util/List;)V

    return-void
.end method
