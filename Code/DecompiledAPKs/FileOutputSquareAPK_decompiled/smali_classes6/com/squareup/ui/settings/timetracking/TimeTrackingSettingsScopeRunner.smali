.class public Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScopeRunner;
.super Ljava/lang/Object;
.source "TimeTrackingSettingsScopeRunner.java"

# interfaces
.implements Lmortar/Scoped;


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final device:Lcom/squareup/util/Device;

.field private final flow:Lflow/Flow;

.field private final timeTrackingSettings:Lcom/squareup/permissions/TimeTrackingSettings;


# direct methods
.method constructor <init>(Lcom/squareup/permissions/TimeTrackingSettings;Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Device;Lflow/Flow;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScopeRunner;->timeTrackingSettings:Lcom/squareup/permissions/TimeTrackingSettings;

    .line 27
    iput-object p2, p0, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    .line 28
    iput-object p3, p0, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScopeRunner;->device:Lcom/squareup/util/Device;

    .line 29
    iput-object p4, p0, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScopeRunner;->flow:Lflow/Flow;

    return-void
.end method

.method static synthetic lambda$timeTrackingSettingsScreenData$0(Lcom/squareup/permissions/TimeTrackingSettings$State;Lcom/squareup/util/DeviceScreenSizeInfo;)Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScreen$Data;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 40
    new-instance v0, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScreen$Data;

    .line 41
    invoke-virtual {p1}, Lcom/squareup/util/DeviceScreenSizeInfo;->isPhoneOrPortraitLessThan10Inches()Z

    move-result p1

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScreen$Data;-><init>(Lcom/squareup/permissions/TimeTrackingSettings$State;Z)V

    return-object v0
.end method


# virtual methods
.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public onTimeTrackingSettingsBackClicked()V
    .locals 2

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScopeRunner;->flow:Lflow/Flow;

    const-class v1, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScreen;

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackFrom(Lflow/Flow;Ljava/lang/Class;)V

    return-void
.end method

.method public onTimeTrackingSettingsSwitchChanged(Z)Lkotlin/Unit;
    .locals 2

    .line 49
    iget-object v0, p0, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {p1}, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsEvent;->forTimeTrackingToggle(Z)Lcom/squareup/eventstream/v1/EventStreamEvent;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScopeRunner;->timeTrackingSettings:Lcom/squareup/permissions/TimeTrackingSettings;

    invoke-virtual {v0, p1}, Lcom/squareup/permissions/TimeTrackingSettings;->enableOrDisableTimeTracking(Z)V

    .line 51
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public timeTrackingSettingsScreenData()Lio/reactivex/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScreen$Data;",
            ">;"
        }
    .end annotation

    .line 39
    iget-object v0, p0, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScopeRunner;->timeTrackingSettings:Lcom/squareup/permissions/TimeTrackingSettings;

    invoke-virtual {v0}, Lcom/squareup/permissions/TimeTrackingSettings;->state()Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScopeRunner;->device:Lcom/squareup/util/Device;

    invoke-interface {v1}, Lcom/squareup/util/Device;->getScreenSize()Lio/reactivex/Observable;

    move-result-object v1

    sget-object v2, Lcom/squareup/ui/settings/timetracking/-$$Lambda$TimeTrackingSettingsScopeRunner$wPG6bskSrh60qQnN5xw2G3WRs50;->INSTANCE:Lcom/squareup/ui/settings/timetracking/-$$Lambda$TimeTrackingSettingsScopeRunner$wPG6bskSrh60qQnN5xw2G3WRs50;

    invoke-static {v0, v1, v2}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method
