.class Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView$2;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "SignatureAndReceiptSettingsView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->onAttachedToWindow()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;)V
    .locals 0

    .line 88
    iput-object p1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView$2;->this$0:Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 0

    .line 90
    iget-object p1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView$2;->this$0:Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;

    invoke-static {p1}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->access$100(Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;)Lcom/squareup/widgets/list/ToggleButtonRow;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/widgets/list/ToggleButtonRow;->isChecked()Z

    move-result p1

    if-nez p1, :cond_0

    .line 91
    iget-object p1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView$2;->this$0:Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;

    iget-object p1, p1, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->presenter:Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->onUnderAmountSkipSignature()V

    :cond_0
    return-void
.end method
