.class public final Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsScreen;
.super Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsSection$InGiftCardSettingsScreen;
.source "CropCustomDesignSettingsScreen.kt"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/coordinators/CoordinatorProvider;
.implements Lcom/squareup/container/layer/InSection;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsScreen$Runner;,
        Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsScreen$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCropCustomDesignSettingsScreen.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CropCustomDesignSettingsScreen.kt\ncom/squareup/ui/settings/giftcards/CropCustomDesignSettingsScreen\n+ 2 Components.kt\ncom/squareup/dagger/Components\n*L\n1#1,48:1\n43#2:49\n*E\n*S KotlinDebug\n*F\n+ 1 CropCustomDesignSettingsScreen.kt\ncom/squareup/ui/settings/giftcards/CropCustomDesignSettingsScreen\n*L\n26#1:49\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0003\u0008\u0007\u0018\u0000 \u000e2\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u0004:\u0002\u000e\u000fB\u0005\u00a2\u0006\u0002\u0010\u0005J\u0008\u0010\u0006\u001a\u00020\u0007H\u0016J\u0012\u0010\u0008\u001a\u0004\u0018\u00010\t2\u0006\u0010\n\u001a\u00020\u000bH\u0016J\u0008\u0010\u000c\u001a\u00020\rH\u0016\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsScreen;",
        "Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsSection$InGiftCardSettingsScreen;",
        "Lcom/squareup/container/LayoutScreen;",
        "Lcom/squareup/coordinators/CoordinatorProvider;",
        "Lcom/squareup/container/layer/InSection;",
        "()V",
        "getParentKey",
        "",
        "provideCoordinator",
        "Lcom/squareup/coordinators/Coordinator;",
        "view",
        "Landroid/view/View;",
        "screenLayout",
        "",
        "Companion",
        "Runner",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsScreen$Companion;

.field private static final INSTANCE:Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsScreen$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsScreen$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsScreen;->Companion:Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsScreen$Companion;

    .line 32
    new-instance v0, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsScreen;

    invoke-direct {v0}, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsScreen;->INSTANCE:Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsScreen;

    .line 35
    sget-object v0, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsScreen;->INSTANCE:Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsScreen;

    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    const-string v1, "PathCreator.forSingleton(INSTANCE)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/os/Parcelable$Creator;

    sput-object v0, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 18
    invoke-direct {p0}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsSection$InGiftCardSettingsScreen;-><init>()V

    return-void
.end method

.method public static final synthetic access$getINSTANCE$cp()Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsScreen;
    .locals 1

    .line 17
    sget-object v0, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsScreen;->INSTANCE:Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsScreen;

    return-object v0
.end method


# virtual methods
.method public getParentKey()Ljava/lang/Object;
    .locals 1

    .line 23
    sget-object v0, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen;->Companion:Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$Companion;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen$Companion;->getINSTANCE()Lcom/squareup/ui/settings/giftcards/AddDesignsSettingsScreen;

    move-result-object v0

    return-object v0
.end method

.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 1

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    const-class v0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScope$Component;

    .line 26
    invoke-interface {p1}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScope$Component;->cropCustomDesignSettingsCoordinator()Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;

    move-result-object p1

    check-cast p1, Lcom/squareup/coordinators/Coordinator;

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 29
    sget v0, Lcom/squareup/settingsapplet/R$layout;->egiftcard_crop_custom_design_settings_screen:I

    return v0
.end method
