.class public final Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$policyWatcher$1;
.super Lcom/squareup/text/EmptyTextWatcher;
.source "GiftCardsSettingsCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;-><init>(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;Lcom/squareup/util/Res;Lcom/squareup/ui/ErrorsBarPresenter;Lcom/squareup/text/Formatter;Lcom/squareup/money/PriceLocaleHelper;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000)\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0010\u0002\n\u0002\u0008\u0005\n\u0002\u0010\u000b\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0012\u0010\u0010\u001a\u00020\u00052\u0008\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u0016R&\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007\"\u0004\u0008\u0008\u0010\tR\u001a\u0010\n\u001a\u00020\u000bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\u000c\u0010\r\"\u0004\u0008\u000e\u0010\u000f\u00a8\u0006\u0013"
    }
    d2 = {
        "com/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$policyWatcher$1",
        "Lcom/squareup/text/EmptyTextWatcher;",
        "onChange",
        "Lkotlin/Function1;",
        "",
        "",
        "getOnChange",
        "()Lkotlin/jvm/functions/Function1;",
        "setOnChange",
        "(Lkotlin/jvm/functions/Function1;)V",
        "paused",
        "",
        "getPaused",
        "()Z",
        "setPaused",
        "(Z)V",
        "afterTextChanged",
        "editable",
        "Landroid/text/Editable;",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private onChange:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/String;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private paused:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .line 72
    invoke-direct {p0}, Lcom/squareup/text/EmptyTextWatcher;-><init>()V

    .line 79
    sget-object v0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$policyWatcher$1$onChange$1;->INSTANCE:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$policyWatcher$1$onChange$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    iput-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$policyWatcher$1;->onChange:Lkotlin/jvm/functions/Function1;

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 1

    .line 75
    iget-boolean v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$policyWatcher$1;->paused:Z

    if-eqz v0, :cond_0

    return-void

    .line 76
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$policyWatcher$1;->onChange:Lkotlin/jvm/functions/Function1;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public final getOnChange()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Ljava/lang/String;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 79
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$policyWatcher$1;->onChange:Lkotlin/jvm/functions/Function1;

    return-object v0
.end method

.method public final getPaused()Z
    .locals 1

    .line 73
    iget-boolean v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$policyWatcher$1;->paused:Z

    return v0
.end method

.method public final setOnChange(Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/String;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$policyWatcher$1;->onChange:Lkotlin/jvm/functions/Function1;

    return-void
.end method

.method public final setPaused(Z)V
    .locals 0

    .line 73
    iput-boolean p1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$policyWatcher$1;->paused:Z

    return-void
.end method
