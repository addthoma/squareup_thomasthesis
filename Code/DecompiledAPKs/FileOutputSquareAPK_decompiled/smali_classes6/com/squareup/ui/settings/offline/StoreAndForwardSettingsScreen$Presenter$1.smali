.class Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter$1;
.super Ljava/lang/Object;
.source "StoreAndForwardSettingsScreen.java"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;->lambda$onLoad$0(Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsView;)Lio/reactivex/disposables/Disposable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation


# instance fields
.field private initialized:Z

.field final synthetic this$0:Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;

.field final synthetic val$animate:Ljava/util/concurrent/atomic/AtomicBoolean;

.field final synthetic val$view:Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsView;Ljava/util/concurrent/atomic/AtomicBoolean;)V
    .locals 0

    .line 84
    iput-object p1, p0, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter$1;->this$0:Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;

    iput-object p2, p0, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter$1;->val$view:Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsView;

    iput-object p3, p0, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter$1;->val$animate:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 84
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter$1;->accept(Lkotlin/Unit;)V

    return-void
.end method

.method public accept(Lkotlin/Unit;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 87
    iget-boolean p1, p0, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter$1;->initialized:Z

    if-nez p1, :cond_0

    .line 88
    iget-object p1, p0, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter$1;->val$view:Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsView;

    iget-object v0, p0, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter$1;->this$0:Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;

    invoke-static {v0}, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;->access$000(Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;)Lcom/squareup/settings/server/AccountStatusSettings;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;->access$100(Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;Lcom/squareup/settings/server/AccountStatusSettings;)Lcom/squareup/money/MaxMoneyScrubber;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsView;->initializeMaxAmountScrubber(Lcom/squareup/text/Scrubber;)V

    .line 89
    iget-object p1, p0, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter$1;->val$view:Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsView;

    iget-object v0, p0, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter$1;->this$0:Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;

    invoke-static {v0}, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;->access$300(Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;)Lcom/squareup/text/Formatter;

    move-result-object v0

    const-wide/16 v1, 0x0

    iget-object v3, p0, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter$1;->this$0:Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;

    invoke-static {v3}, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;->access$200(Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;)Lcom/squareup/protos/common/CurrencyCode;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 90
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter$1;->this$0:Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;

    invoke-static {v1}, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;->access$400(Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;)Lcom/squareup/util/Res;

    move-result-object v1

    sget v2, Lcom/squareup/settingsapplet/R$string;->offline_mode_no_limit:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 89
    invoke-virtual {p1, v0, v1}, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsView;->initializeFocusListener(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p1, 0x1

    .line 91
    iput-boolean p1, p0, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter$1;->initialized:Z

    .line 94
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter$1;->this$0:Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;

    invoke-static {p1}, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;->access$000(Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;)Lcom/squareup/settings/server/AccountStatusSettings;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/settings/server/AccountStatusSettings;->getStoreAndForwardSettings()Lcom/squareup/settings/server/StoreAndForwardSettings;

    move-result-object p1

    .line 95
    iget-object v0, p0, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter$1;->this$0:Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;

    .line 96
    invoke-static {v0}, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;->access$300(Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;)Lcom/squareup/text/Formatter;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/settings/server/StoreAndForwardSettings;->getSingleTransactionLimit()Lcom/squareup/protos/common/Money;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 97
    invoke-virtual {p1}, Lcom/squareup/settings/server/StoreAndForwardSettings;->isStoreAndForwardEnabled()Z

    move-result p1

    .line 98
    iget-object v1, p0, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter$1;->this$0:Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;

    invoke-static {v1}, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;->access$000(Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter;)Lcom/squareup/settings/server/AccountStatusSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/settings/server/PaymentSettings;->canEnableTipping()Z

    move-result v1

    .line 99
    iget-object v2, p0, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter$1;->val$view:Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsView;

    iget-object v3, p0, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter$1;->val$animate:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v3

    invoke-virtual {v2, p1, v3}, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsView;->setStoreAndForwardEnabled(ZZ)V

    .line 100
    iget-object v2, p0, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter$1;->val$view:Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsView;

    invoke-virtual {v2, p1}, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsView;->setTransactionLimitSectionVisible(Z)V

    .line 101
    iget-object p1, p0, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter$1;->val$view:Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsView;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsView;->setTransactionLimit(Ljava/lang/String;)V

    .line 102
    iget-object p1, p0, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Presenter$1;->val$view:Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsView;

    if-eqz v1, :cond_1

    sget v0, Lcom/squareup/settingsapplet/R$string;->offline_mode_transaction_limit_hint:I

    goto :goto_0

    :cond_1
    sget v0, Lcom/squareup/settingsapplet/R$string;->offline_mode_transaction_limit_hint_no_tips:I

    :goto_0
    invoke-virtual {p1, v0}, Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsView;->setLimitHint(I)V

    return-void
.end method
