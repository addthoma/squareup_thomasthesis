.class public interface abstract Lcom/squareup/ui/settings/paymentdevices/pairing/ReaderSdkR12PairingScreen$Component;
.super Ljava/lang/Object;
.source "ReaderSdkR12PairingScreen.java"

# interfaces
.implements Lcom/squareup/ui/settings/paymentdevices/pairing/PairingView$ParentComponent;
.implements Lcom/squareup/ui/settings/paymentdevices/PairingConfirmationScreen$ParentComponent;


# annotations
.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/ui/settings/paymentdevices/pairing/R12PairingScreenModule;,
        Lcom/squareup/ui/settings/paymentdevices/pairing/PairingScope$Module;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/paymentdevices/pairing/ReaderSdkR12PairingScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation
