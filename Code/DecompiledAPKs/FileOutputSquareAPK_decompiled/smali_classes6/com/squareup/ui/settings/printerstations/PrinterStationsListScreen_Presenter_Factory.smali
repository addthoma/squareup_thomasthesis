.class public final Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen_Presenter_Factory;
.super Ljava/lang/Object;
.source "PrinterStationsListScreen_Presenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final coreParametersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;",
            ">;"
        }
    .end annotation
.end field

.field private final hardwarePrinterTrackerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/HardwarePrinterTracker;",
            ">;"
        }
    .end annotation
.end field

.field private final printerStationsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final sidebarRefresherProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SidebarRefresher;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/HardwarePrinterTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SidebarRefresher;",
            ">;)V"
        }
    .end annotation

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen_Presenter_Factory;->coreParametersProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p2, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p3, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen_Presenter_Factory;->printerStationsProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p4, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen_Presenter_Factory;->hardwarePrinterTrackerProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p5, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen_Presenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 44
    iput-object p6, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen_Presenter_Factory;->sidebarRefresherProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen_Presenter_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/HardwarePrinterTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/SidebarRefresher;",
            ">;)",
            "Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen_Presenter_Factory;"
        }
    .end annotation

    .line 57
    new-instance v7, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen_Presenter_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen_Presenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;Lcom/squareup/util/Res;Lcom/squareup/print/PrinterStations;Lcom/squareup/print/HardwarePrinterTracker;Lcom/squareup/analytics/Analytics;Lcom/squareup/ui/settings/SidebarRefresher;)Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;
    .locals 8

    .line 64
    new-instance v7, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;-><init>(Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;Lcom/squareup/util/Res;Lcom/squareup/print/PrinterStations;Lcom/squareup/print/HardwarePrinterTracker;Lcom/squareup/analytics/Analytics;Lcom/squareup/ui/settings/SidebarRefresher;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;
    .locals 7

    .line 49
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen_Presenter_Factory;->coreParametersProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;

    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen_Presenter_Factory;->printerStationsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/print/PrinterStations;

    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen_Presenter_Factory;->hardwarePrinterTrackerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/print/HardwarePrinterTracker;

    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen_Presenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen_Presenter_Factory;->sidebarRefresherProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/ui/settings/SidebarRefresher;

    invoke-static/range {v1 .. v6}, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen_Presenter_Factory;->newInstance(Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;Lcom/squareup/util/Res;Lcom/squareup/print/PrinterStations;Lcom/squareup/print/HardwarePrinterTracker;Lcom/squareup/analytics/Analytics;Lcom/squareup/ui/settings/SidebarRefresher;)Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen_Presenter_Factory;->get()Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;

    move-result-object v0

    return-object v0
.end method
