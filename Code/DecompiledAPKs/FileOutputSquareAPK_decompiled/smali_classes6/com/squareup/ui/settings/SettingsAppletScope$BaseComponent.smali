.class public interface abstract Lcom/squareup/ui/settings/SettingsAppletScope$BaseComponent;
.super Ljava/lang/Object;
.source "SettingsAppletScope.java"

# interfaces
.implements Lcom/squareup/ui/settings/PreviewSelectMethodWorkflowRunner$ParentComponent;
.implements Lcom/squareup/feedback/AppFeedbackScope$ParentComponent;
.implements Lcom/squareup/ui/settings/quickamounts/QuickAmountsSettingsScope$ParentComponent;
.implements Lcom/squareup/ui/settings/scales/ScalesSettingsScope$ParentComponent;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/SettingsAppletScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "BaseComponent"
.end annotation


# virtual methods
.method public abstract audioPermissionCard()Lcom/squareup/ui/systempermissions/AudioPermissionScreen$Component;
.end method

.method public abstract bankAccountSettingsScope()Lcom/squareup/ui/settings/bankaccount/BankAccountSettingsScope$Component;
.end method

.method public abstract barcodeScannersSettings()Lcom/squareup/ui/settings/barcodescanners/BarcodeScannersSettingsScreen$Component;
.end method

.method public abstract cardReaderDetail()Lcom/squareup/ui/settings/paymentdevices/CardReaderDetailScreen$Component;
.end method

.method public abstract cardReaders()Lcom/squareup/ui/settings/paymentdevices/CardReadersScreen$Component;
.end method

.method public abstract cashDrawerSettings()Lcom/squareup/ui/settings/cashdrawer/CashDrawerSettingsScreen$Component;
.end method

.method public abstract cashManagementSectionController()Lcom/squareup/ui/settings/cashmanagement/CashManagementSectionController;
.end method

.method public abstract cashManagementSettings()Lcom/squareup/ui/settings/cashmanagement/CashManagementSettingsSectionScreen$Component;
.end method

.method public abstract confirmBusinessAddressDialog()Lcom/squareup/ui/settings/merchantprofile/ConfirmBusinessAddressDialogScreen$Component;
.end method

.method public abstract createOrEditTeamPasscodeCoordinator()Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;
.end method

.method public abstract createOrEditTeamPasscodeSuccessCoordinator()Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeSuccessCoordinator;
.end method

.method public abstract createOwnerPasscodeCoordinator()Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeCoordinator;
.end method

.method public abstract createOwnerPasscodeSuccessCoordinator()Lcom/squareup/ui/settings/passcodes/CreateOwnerPasscodeSuccessCoordinator;
.end method

.method public abstract customerManagementSettings()Lcom/squareup/ui/settings/crm/CustomerManagementSettingsScreen$Component;
.end method

.method public abstract delegateLockout()Lcom/squareup/ui/settings/DelegateLockoutScreen$Component;
.end method

.method public abstract depositSettingsScope()Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScope$Component;
.end method

.method public abstract deviceName()Lcom/squareup/ui/settings/devicename/DeviceNameScreen$Component;
.end method

.method public abstract editTicketGroupFlow()Lcom/squareup/ui/settings/opentickets/ticketgroups/EditTicketGroupScope$Component;
.end method

.method public abstract emailCollectionDialogScreen()Lcom/squareup/ui/settings/crm/EmailCollectionConfirmationDialog$Component;
.end method

.method public abstract emailCollectionSectionController()Lcom/squareup/ui/settings/crm/EmailCollectionSectionController;
.end method

.method public abstract emailCollectionSettings()Lcom/squareup/ui/settings/crm/EmailCollectionSettingsScreen$Component;
.end method

.method public abstract employeeManagementSettings()Lcom/squareup/ui/settings/empmanagement/EmployeeManagementSettingsScreen$Component;
.end method

.method public abstract employeesUpsellScreen()Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$Component;
.end method

.method public abstract giftCardsSettings()Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScope$Component;
.end method

.method public abstract learnMoreReader()Lcom/squareup/ui/settings/paymentdevices/LearnMoreReaderScreen$Component;
.end method

.method public abstract loyaltySettings()Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScope$Component;
.end method

.method public abstract merchantProfile()Lcom/squareup/ui/settings/merchantprofile/MerchantProfileScreen$Component;
.end method

.method public abstract merchantProfileBusinessAddress()Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressScreen$Component;
.end method

.method public abstract merchantProfileEditLogo()Lcom/squareup/ui/settings/merchantprofile/logo/MerchantProfileEditLogoScreen$Component;
.end method

.method public abstract openTicketsSettings()Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsScreen$Component;
.end method

.method public abstract openTicketsSettingsRunner()Lcom/squareup/ui/settings/opentickets/OpenTicketsSettingsRunner;
.end method

.method public abstract orderHubSettings()Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$Component;
.end method

.method public abstract passcodeSettingsCoordinator()Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;
.end method

.method public abstract passcodeSettingsScopeRunner()Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;
.end method

.method public abstract passcodesTimeoutCoordinator()Lcom/squareup/ui/settings/passcodes/PasscodesTimeoutCoordinator;
.end method

.method public abstract paymentTypes()Lcom/squareup/ui/settings/paymenttypes/PaymentTypesSettingsScreen$Component;
.end method

.method public abstract predefinedTicketsOptIn()Lcom/squareup/ui/settings/opentickets/optin/PredefinedTicketsOptInScreen$Component;
.end method

.method public abstract printerStation(Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$Module;)Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$Component;
.end method

.method public abstract printerStationsList()Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Component;
.end method

.method public abstract requestBusinessAddressDialog()Lcom/squareup/ui/settings/merchantprofile/RequestBusinessAddressDialogScreen$Component;
.end method

.method public abstract settingsAppletScopeRunner()Lcom/squareup/ui/settings/SettingsAppletScopeRunner;
.end method

.method public abstract settingsSection()Lcom/squareup/ui/settings/SettingsSectionsScreen$Component;
.end method

.method public abstract sharedSettings()Lcom/squareup/ui/settings/sharedsettings/SharedSettingsScreen$Component;
.end method

.method public abstract signatureAndReceiptSettings()Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Component;
.end method

.method public abstract storeAndForwardSettings()Lcom/squareup/ui/settings/offline/StoreAndForwardSettingsScreen$Component;
.end method

.method public abstract storeAndForwardSettingsEnable()Lcom/squareup/ui/settings/offline/optin/StoreAndForwardSettingsEnableScreen$Component;
.end method

.method public abstract swipeChipCardsSettings()Lcom/squareup/ui/settings/swipechipcards/SwipeChipCardsSettingsScreen$Component;
.end method

.method public abstract swipeChipCardsSettingsEnable()Lcom/squareup/ui/settings/swipechipcards/optin/SwipeChipCardsSettingsEnableScreen$Component;
.end method

.method public abstract tax()Lcom/squareup/ui/settings/taxes/tax/TaxScope$Component;
.end method

.method public abstract taxesList()Lcom/squareup/ui/settings/taxes/TaxesListScreen$Component;
.end method

.method public abstract tileAppearance()Lcom/squareup/ui/settings/tiles/TileAppearanceScreen$Component;
.end method

.method public abstract timeTrackingSettingsCoordinator()Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsCoordinator;
.end method

.method public abstract timeTrackingSettingsScopeRunner()Lcom/squareup/ui/settings/timetracking/TimeTrackingSettingsScopeRunner;
.end method

.method public abstract tipSettings()Lcom/squareup/ui/settings/tipping/TipSettingsScreen$Component;
.end method

.method public abstract x2SettingsCoordinators()Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Lcom/squareup/x2/settings/ScreenType;",
            "Lcom/squareup/coordinators/Coordinator;",
            ">;"
        }
    .end annotation
.end method
