.class final Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$saveChangesErrors$1$2;
.super Ljava/lang/Object;
.source "GiftCardsSettingsScopeRunner.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$saveChangesErrors$1;->apply(Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;)Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0014\u0010\u0002\u001a\u0010\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;",
        "it",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;",
        "kotlin.jvm.PlatformType",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $newSetting:Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$saveChangesErrors$1$2;->$newSetting:Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;",
            ">;)",
            "Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 118
    iget-object v1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$saveChangesErrors$1$2;->$newSetting:Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/16 v7, 0xf

    const/4 v8, 0x0

    invoke-static/range {v1 .. v8}, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;->copy$default(Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;ZZILjava/lang/Object;)Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 47
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$saveChangesErrors$1$2;->apply(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;

    move-result-object p1

    return-object p1
.end method
