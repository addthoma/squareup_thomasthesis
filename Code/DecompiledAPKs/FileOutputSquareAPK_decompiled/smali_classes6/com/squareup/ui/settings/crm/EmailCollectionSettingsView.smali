.class public Lcom/squareup/ui/settings/crm/EmailCollectionSettingsView;
.super Landroid/widget/LinearLayout;
.source "EmailCollectionSettingsView.java"

# interfaces
.implements Lcom/squareup/ui/HasActionBar;


# instance fields
.field private enableLabel:Lcom/squareup/widgets/MessageView;

.field private enableToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

.field presenter:Lcom/squareup/ui/settings/crm/EmailCollectionSettingsScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 28
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    const-class p2, Lcom/squareup/ui/settings/crm/EmailCollectionSettingsScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/crm/EmailCollectionSettingsScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/settings/crm/EmailCollectionSettingsScreen$Component;->inject(Lcom/squareup/ui/settings/crm/EmailCollectionSettingsView;)V

    return-void
.end method


# virtual methods
.method public getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 59
    invoke-static {p0}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$onFinishInflate$0$EmailCollectionSettingsView(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 45
    iget-object p1, p0, Lcom/squareup/ui/settings/crm/EmailCollectionSettingsView;->presenter:Lcom/squareup/ui/settings/crm/EmailCollectionSettingsScreen$Presenter;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/crm/EmailCollectionSettingsScreen$Presenter;->onToggleClicked()V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 49
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/settings/crm/EmailCollectionSettingsView;->presenter:Lcom/squareup/ui/settings/crm/EmailCollectionSettingsScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/crm/EmailCollectionSettingsScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/settings/crm/EmailCollectionSettingsView;->presenter:Lcom/squareup/ui/settings/crm/EmailCollectionSettingsScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/crm/EmailCollectionSettingsScreen$Presenter;->dropView(Landroid/view/ViewGroup;)V

    .line 55
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 4

    .line 33
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 35
    sget v0, Lcom/squareup/settingsapplet/R$id;->crm_email_collection_settings_screen_toggle:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/crm/EmailCollectionSettingsView;->enableToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 36
    sget v0, Lcom/squareup/settingsapplet/R$id;->crm_email_collection_settings_screen_hint:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/settings/crm/EmailCollectionSettingsView;->enableLabel:Lcom/squareup/widgets/MessageView;

    .line 38
    iget-object v0, p0, Lcom/squareup/ui/settings/crm/EmailCollectionSettingsView;->enableLabel:Lcom/squareup/widgets/MessageView;

    new-instance v1, Lcom/squareup/ui/LinkSpan$Builder;

    invoke-virtual {p0}, Lcom/squareup/ui/settings/crm/EmailCollectionSettingsView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/squareup/settingsapplet/R$string;->crm_email_collection_settings_screen_hint:I

    const-string v3, "support_center_link"

    .line 39
    invoke-virtual {v1, v2, v3}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    sget v2, Lcom/squareup/registerlib/R$string;->crm_email_collection_settings_url:I

    .line 41
    invoke-virtual {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->url(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    sget v2, Lcom/squareup/checkout/R$string;->support_center:I

    .line 42
    invoke-virtual {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    .line 43
    invoke-virtual {v1}, Lcom/squareup/ui/LinkSpan$Builder;->asCharSequence()Ljava/lang/CharSequence;

    move-result-object v1

    .line 38
    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/settings/crm/EmailCollectionSettingsView;->enableToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    new-instance v1, Lcom/squareup/ui/settings/crm/-$$Lambda$EmailCollectionSettingsView$mFK4OyNTklOna6ISh-Zv9-46lpU;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/crm/-$$Lambda$EmailCollectionSettingsView$mFK4OyNTklOna6ISh-Zv9-46lpU;-><init>(Lcom/squareup/ui/settings/crm/EmailCollectionSettingsView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    return-void
.end method

.method public setEnableToggleState(Z)V
    .locals 1

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/settings/crm/EmailCollectionSettingsView;->enableToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    return-void
.end method
