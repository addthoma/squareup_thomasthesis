.class final Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$uploadingTheme$3;
.super Ljava/lang/Object;
.source "GiftCardsSettingsScopeRunner.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;-><init>(Lflow/Flow;Lcom/squareup/giftcard/GiftCardServiceHelper;Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsHelper;Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader;Lio/reactivex/Scheduler;Lcom/squareup/util/Device;Lcom/squareup/camerahelper/CameraHelper;Lcom/squareup/register/widgets/GlassSpinner;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/ObservableSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a&\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002 \u0003*\u0012\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002\u0018\u00010\u00010\u00012\u0012\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader$UploadResult;",
        "kotlin.jvm.PlatformType",
        "<name for destructuring parameter 0>",
        "Lkotlin/Pair;",
        "Landroid/graphics/Bitmap;",
        "Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$uploadingTheme$3;->this$0:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lkotlin/Pair;)Lio/reactivex/Observable;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "Landroid/graphics/Bitmap;",
            "Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;",
            ">;)",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader$UploadResult;",
            ">;"
        }
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {p1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    .line 130
    iget-object v1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$uploadingTheme$3;->this$0:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;

    invoke-static {v1}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->access$getEGiftCardImageUploadHelper$p(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;)Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader;->uploadImage(Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;Landroid/graphics/Bitmap;)Lio/reactivex/Single;

    move-result-object p1

    .line 131
    invoke-virtual {p1}, Lio/reactivex/Single;->toObservable()Lio/reactivex/Observable;

    move-result-object p1

    .line 133
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$uploadingTheme$3;->this$0:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;

    invoke-static {v0}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->access$getUploadSpinner$p(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;)Lcom/squareup/register/widgets/GlassSpinner;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const/4 v2, -0x1

    const-wide/16 v3, 0x320

    invoke-virtual {v0, v2, v3, v4, v1}, Lcom/squareup/register/widgets/GlassSpinner;->spinnerTransformRx2(IJLjava/util/concurrent/TimeUnit;)Lio/reactivex/ObservableTransformer;

    move-result-object v0

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 47
    check-cast p1, Lkotlin/Pair;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$uploadingTheme$3;->apply(Lkotlin/Pair;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method
