.class Lcom/squareup/ui/settings/taxes/tax/TaxItemPricingScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "TaxItemPricingScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/taxes/tax/TaxItemPricingScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/settings/taxes/tax/TaxItemPricingView;",
        ">;"
    }
.end annotation


# instance fields
.field private final flow:Lflow/Flow;

.field private final res:Lcom/squareup/util/Res;

.field private final taxState:Lcom/squareup/ui/settings/taxes/tax/TaxState;


# direct methods
.method constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/ui/settings/taxes/tax/TaxState;Lflow/Flow;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 43
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxItemPricingScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 45
    iput-object p2, p0, Lcom/squareup/ui/settings/taxes/tax/TaxItemPricingScreen$Presenter;->taxState:Lcom/squareup/ui/settings/taxes/tax/TaxState;

    .line 46
    iput-object p3, p0, Lcom/squareup/ui/settings/taxes/tax/TaxItemPricingScreen$Presenter;->flow:Lflow/Flow;

    return-void
.end method

.method private refresh()V
    .locals 5

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxItemPricingScreen$Presenter;->taxState:Lcom/squareup/ui/settings/taxes/tax/TaxState;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/taxes/tax/TaxState;->getBuilder()Lcom/squareup/shared/catalog/models/CatalogTax$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->getInclusionType()Lcom/squareup/api/items/Fee$InclusionType;

    move-result-object v0

    .line 71
    invoke-virtual {p0}, Lcom/squareup/ui/settings/taxes/tax/TaxItemPricingScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/settings/taxes/tax/TaxItemPricingView;

    sget-object v2, Lcom/squareup/api/items/Fee$InclusionType;->ADDITIVE:Lcom/squareup/api/items/Fee$InclusionType;

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-ne v0, v2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    invoke-virtual {v1, v2}, Lcom/squareup/ui/settings/taxes/tax/TaxItemPricingView;->setExcludeRowChecked(Z)V

    .line 72
    invoke-virtual {p0}, Lcom/squareup/ui/settings/taxes/tax/TaxItemPricingScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/settings/taxes/tax/TaxItemPricingView;

    sget-object v2, Lcom/squareup/api/items/Fee$InclusionType;->INCLUSIVE:Lcom/squareup/api/items/Fee$InclusionType;

    if-ne v0, v2, :cond_1

    goto :goto_1

    :cond_1
    const/4 v3, 0x0

    :goto_1
    invoke-virtual {v1, v3}, Lcom/squareup/ui/settings/taxes/tax/TaxItemPricingView;->setIncludeRowChecked(Z)V

    return-void
.end method


# virtual methods
.method excludeRowClicked()V
    .locals 2

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxItemPricingScreen$Presenter;->taxState:Lcom/squareup/ui/settings/taxes/tax/TaxState;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/taxes/tax/TaxState;->getBuilder()Lcom/squareup/shared/catalog/models/CatalogTax$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/api/items/Fee$InclusionType;->ADDITIVE:Lcom/squareup/api/items/Fee$InclusionType;

    invoke-virtual {v0, v1}, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->setInclusionType(Lcom/squareup/api/items/Fee$InclusionType;)Lcom/squareup/shared/catalog/models/CatalogTax$Builder;

    .line 61
    invoke-direct {p0}, Lcom/squareup/ui/settings/taxes/tax/TaxItemPricingScreen$Presenter;->refresh()V

    return-void
.end method

.method includeRowClicked()V
    .locals 2

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxItemPricingScreen$Presenter;->taxState:Lcom/squareup/ui/settings/taxes/tax/TaxState;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/taxes/tax/TaxState;->getBuilder()Lcom/squareup/shared/catalog/models/CatalogTax$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/api/items/Fee$InclusionType;->INCLUSIVE:Lcom/squareup/api/items/Fee$InclusionType;

    invoke-virtual {v0, v1}, Lcom/squareup/shared/catalog/models/CatalogTax$Builder;->setInclusionType(Lcom/squareup/api/items/Fee$InclusionType;)Lcom/squareup/shared/catalog/models/CatalogTax$Builder;

    .line 66
    invoke-direct {p0}, Lcom/squareup/ui/settings/taxes/tax/TaxItemPricingScreen$Presenter;->refresh()V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 3

    .line 50
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 51
    invoke-virtual {p0}, Lcom/squareup/ui/settings/taxes/tax/TaxItemPricingScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/taxes/tax/TaxItemPricingView;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/taxes/tax/TaxItemPricingView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object p1

    new-instance v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxItemPricingScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/settingsapplet/R$string;->tax_item_pricing:I

    .line 52
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonTextBackArrow(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxItemPricingScreen$Presenter;->flow:Lflow/Flow;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/settings/taxes/tax/-$$Lambda$sZX4UsMUUPBhpIptxMDlKtBJT0w;

    invoke-direct {v2, v1}, Lcom/squareup/ui/settings/taxes/tax/-$$Lambda$sZX4UsMUUPBhpIptxMDlKtBJT0w;-><init>(Lflow/Flow;)V

    .line 53
    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 54
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->hidePrimaryButton()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    .line 55
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    .line 51
    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 56
    invoke-direct {p0}, Lcom/squareup/ui/settings/taxes/tax/TaxItemPricingScreen$Presenter;->refresh()V

    return-void
.end method
