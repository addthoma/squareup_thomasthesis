.class public Lcom/squareup/ui/settings/checkout/CheckoutSettingsSectionAccess;
.super Lcom/squareup/applet/SectionAccess;
.source "CheckoutSettingsSectionAccess.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCheckoutSettingsSectionAccess.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CheckoutSettingsSectionAccess.kt\ncom/squareup/ui/settings/checkout/CheckoutSettingsSectionAccess\n*L\n1#1,29:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\"\n\u0002\u0008\u0006\u0008\u0016\u0018\u00002\u00020\u0001B!\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0012\u0010\u0004\u001a\n\u0012\u0006\u0008\u0001\u0012\u00020\u00060\u0005\"\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J\u000e\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00060\tH\u0016R \u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\u00060\tX\u0084\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\u0008\n\u0010\u000b\"\u0004\u0008\u000c\u0010\r\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/ui/settings/checkout/CheckoutSettingsSectionAccess;",
        "Lcom/squareup/applet/SectionAccess;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "alternatePermissions",
        "",
        "Lcom/squareup/permissions/Permission;",
        "(Lcom/squareup/settings/server/Features;[Lcom/squareup/permissions/Permission;)V",
        "acceptablePermissions",
        "",
        "getAcceptablePermissions",
        "()Ljava/util/Set;",
        "setAcceptablePermissions",
        "(Ljava/util/Set;)V",
        "getPermissions",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private acceptablePermissions:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "+",
            "Lcom/squareup/permissions/Permission;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public varargs constructor <init>(Lcom/squareup/settings/server/Features;[Lcom/squareup/permissions/Permission;)V
    .locals 2

    const-string v0, "features"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "alternatePermissions"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    invoke-direct {p0}, Lcom/squareup/applet/SectionAccess;-><init>()V

    .line 15
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    check-cast v0, Ljava/util/Set;

    .line 16
    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_SETTINGS_GRANULAR_PERMISSIONS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {p1, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 17
    sget-object p1, Lcom/squareup/permissions/Permission;->MANAGE_CHECKOUT_SETTINGS:Lcom/squareup/permissions/Permission;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 19
    :cond_0
    sget-object p1, Lcom/squareup/permissions/Permission;->SETTINGS:Lcom/squareup/permissions/Permission;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 20
    move-object p1, v0

    check-cast p1, Ljava/util/Collection;

    invoke-static {p1, p2}, Lkotlin/collections/CollectionsKt;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 22
    :goto_0
    iput-object v0, p0, Lcom/squareup/ui/settings/checkout/CheckoutSettingsSectionAccess;->acceptablePermissions:Ljava/util/Set;

    return-void
.end method


# virtual methods
.method protected final getAcceptablePermissions()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Permission;",
            ">;"
        }
    .end annotation

    .line 12
    iget-object v0, p0, Lcom/squareup/ui/settings/checkout/CheckoutSettingsSectionAccess;->acceptablePermissions:Ljava/util/Set;

    return-object v0
.end method

.method public getPermissions()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Permission;",
            ">;"
        }
    .end annotation

    .line 26
    iget-object v0, p0, Lcom/squareup/ui/settings/checkout/CheckoutSettingsSectionAccess;->acceptablePermissions:Ljava/util/Set;

    return-object v0
.end method

.method protected final setAcceptablePermissions(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "+",
            "Lcom/squareup/permissions/Permission;",
            ">;)V"
        }
    .end annotation

    const-string v0, "<set-?>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    iput-object p1, p0, Lcom/squareup/ui/settings/checkout/CheckoutSettingsSectionAccess;->acceptablePermissions:Ljava/util/Set;

    return-void
.end method
