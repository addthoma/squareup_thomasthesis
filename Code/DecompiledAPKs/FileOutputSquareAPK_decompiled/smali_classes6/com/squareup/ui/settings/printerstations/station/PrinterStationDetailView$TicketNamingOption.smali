.class public final enum Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;
.super Ljava/lang/Enum;
.source "PrinterStationDetailView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TicketNamingOption"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;

.field public static final enum AUTO_NUMBER:Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;

.field public static final enum CUSTOM_NAME:Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;


# instance fields
.field private final rowId:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 48
    new-instance v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;

    sget v1, Lcom/squareup/settingsapplet/R$id;->order_ticket_custom_name_switch:I

    const/4 v2, 0x0

    const-string v3, "CUSTOM_NAME"

    invoke-direct {v0, v3, v2, v1}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;->CUSTOM_NAME:Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;

    .line 49
    new-instance v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;

    sget v1, Lcom/squareup/settingsapplet/R$id;->order_ticket_autonumber_switch:I

    const/4 v3, 0x1

    const-string v4, "AUTO_NUMBER"

    invoke-direct {v0, v4, v3, v1}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;->AUTO_NUMBER:Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;

    .line 47
    sget-object v1, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;->CUSTOM_NAME:Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;->AUTO_NUMBER:Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;->$VALUES:[Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 62
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 63
    iput p3, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;->rowId:I

    return-void
.end method

.method public static ofId(I)Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;
    .locals 5

    .line 52
    invoke-static {}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;->values()[Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    .line 53
    invoke-virtual {v3}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;->getRowId()I

    move-result v4

    if-ne v4, p0, :cond_0

    return-object v3

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;
    .locals 1

    .line 47
    const-class v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;
    .locals 1

    .line 47
    sget-object v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;->$VALUES:[Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;

    invoke-virtual {v0}, [Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;

    return-object v0
.end method


# virtual methods
.method public getRowId()I
    .locals 1

    .line 67
    iget v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailView$TicketNamingOption;->rowId:I

    return v0
.end method
