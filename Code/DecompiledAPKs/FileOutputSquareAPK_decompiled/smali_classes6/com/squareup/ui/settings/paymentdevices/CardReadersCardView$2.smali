.class Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView$2;
.super Ljava/lang/Object;
.source "CardReadersCardView.java"

# interfaces
.implements Lcom/squareup/ui/settings/paymentdevices/EnableBluetoothDialog$EnableBluetoothDialogListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView;->showBluetoothRequiredDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView;)V
    .locals 0

    .line 119
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView$2;->this$0:Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 0

    return-void
.end method

.method public enableBle()V
    .locals 1

    .line 121
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView$2;->this$0:Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView;

    iget-object v0, v0, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardView;->presenter:Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/paymentdevices/CardReadersCardScreen$Presenter;->enableBleAndGo()V

    return-void
.end method
