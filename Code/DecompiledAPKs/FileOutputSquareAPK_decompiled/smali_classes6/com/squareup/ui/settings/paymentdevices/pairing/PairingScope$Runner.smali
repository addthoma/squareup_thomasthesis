.class public Lcom/squareup/ui/settings/paymentdevices/pairing/PairingScope$Runner;
.super Ljava/lang/Object;
.source "PairingScope.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/paymentdevices/pairing/PairingScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Runner"
.end annotation


# instance fields
.field private shouldManuallySelectReader:Z


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/ui/settings/paymentdevices/pairing/PairingScope$1;)V
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingScope$Runner;-><init>()V

    return-void
.end method


# virtual methods
.method public onAutomaticPairingAttemptComplete()V
    .locals 1

    const/4 v0, 0x1

    .line 44
    iput-boolean v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingScope$Runner;->shouldManuallySelectReader:Z

    return-void
.end method

.method public shouldManuallySelectReader()Z
    .locals 1

    .line 48
    iget-boolean v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingScope$Runner;->shouldManuallySelectReader:Z

    return v0
.end method
