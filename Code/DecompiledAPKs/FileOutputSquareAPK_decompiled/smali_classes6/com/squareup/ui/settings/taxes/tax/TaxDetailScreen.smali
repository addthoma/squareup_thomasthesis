.class public final Lcom/squareup/ui/settings/taxes/tax/TaxDetailScreen;
.super Lcom/squareup/ui/settings/taxes/tax/InTaxScope;
.source "TaxDetailScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/layer/InSection;


# annotations
.annotation runtime Lcom/squareup/container/layer/CardScreen;
.end annotation

.annotation runtime Lcom/squareup/ui/WithComponent$FromFactory;
    value = Lcom/squareup/ui/settings/taxes/tax/TaxDetailScreen$ComponentFactory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/taxes/tax/TaxDetailScreen$Component;,
        Lcom/squareup/ui/settings/taxes/tax/TaxDetailScreen$ComponentFactory;,
        Lcom/squareup/ui/settings/taxes/tax/TaxDetailScreen$Module;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/taxes/tax/TaxDetailScreen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 77
    sget-object v0, Lcom/squareup/ui/settings/taxes/tax/-$$Lambda$TaxDetailScreen$-S0L8IZj2biaw6bD8TIFQnHIZ5Q;->INSTANCE:Lcom/squareup/ui/settings/taxes/tax/-$$Lambda$TaxDetailScreen$-S0L8IZj2biaw6bD8TIFQnHIZ5Q;

    .line 78
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 32
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/taxes/tax/InTaxScope;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/settings/taxes/tax/TaxDetailScreen;
    .locals 1

    .line 78
    new-instance v0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailScreen;

    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/taxes/tax/TaxDetailScreen;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 65
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/settings/taxes/tax/InTaxScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 66
    iget-object p2, p0, Lcom/squareup/ui/settings/taxes/tax/TaxDetailScreen;->cogsTaxId:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method

.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 74
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_TAX_EDIT:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getSection()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 36
    const-class v0, Lcom/squareup/ui/settings/taxes/TaxesSection;

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 70
    sget v0, Lcom/squareup/settingsapplet/R$layout;->tax_detail_view:I

    return v0
.end method
