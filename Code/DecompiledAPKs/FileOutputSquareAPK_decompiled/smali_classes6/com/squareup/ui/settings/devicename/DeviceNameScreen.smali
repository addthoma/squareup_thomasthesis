.class public final Lcom/squareup/ui/settings/devicename/DeviceNameScreen;
.super Lcom/squareup/ui/settings/InSettingsAppletScope;
.source "DeviceNameScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/layer/InSection;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/settings/devicename/DeviceNameScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/devicename/DeviceNameScreen$Component;,
        Lcom/squareup/ui/settings/devicename/DeviceNameScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/devicename/DeviceNameScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/settings/devicename/DeviceNameScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 27
    new-instance v0, Lcom/squareup/ui/settings/devicename/DeviceNameScreen;

    invoke-direct {v0}, Lcom/squareup/ui/settings/devicename/DeviceNameScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/settings/devicename/DeviceNameScreen;->INSTANCE:Lcom/squareup/ui/settings/devicename/DeviceNameScreen;

    .line 111
    sget-object v0, Lcom/squareup/ui/settings/devicename/DeviceNameScreen;->INSTANCE:Lcom/squareup/ui/settings/devicename/DeviceNameScreen;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/settings/devicename/DeviceNameScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 29
    invoke-direct {p0}, Lcom/squareup/ui/settings/InSettingsAppletScope;-><init>()V

    return-void
.end method


# virtual methods
.method public getSection()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 33
    const-class v0, Lcom/squareup/ui/settings/devicename/DeviceSection;

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 114
    sget v0, Lcom/squareup/settingsapplet/R$layout;->device_name_view:I

    return v0
.end method
