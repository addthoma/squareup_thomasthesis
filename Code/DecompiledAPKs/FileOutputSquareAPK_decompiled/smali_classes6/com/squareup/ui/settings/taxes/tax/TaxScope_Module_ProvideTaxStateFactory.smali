.class public final Lcom/squareup/ui/settings/taxes/tax/TaxScope_Module_ProvideTaxStateFactory;
.super Ljava/lang/Object;
.source "TaxScope_Module_ProvideTaxStateFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/settings/taxes/tax/TaxState;",
        ">;"
    }
.end annotation


# instance fields
.field private final catalogIntegrationControllerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            ">;"
        }
    .end annotation
.end field

.field private final gsonProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            ">;)V"
        }
    .end annotation

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxScope_Module_ProvideTaxStateFactory;->gsonProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p2, p0, Lcom/squareup/ui/settings/taxes/tax/TaxScope_Module_ProvideTaxStateFactory;->settingsProvider:Ljavax/inject/Provider;

    .line 31
    iput-object p3, p0, Lcom/squareup/ui/settings/taxes/tax/TaxScope_Module_ProvideTaxStateFactory;->catalogIntegrationControllerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/settings/taxes/tax/TaxScope_Module_ProvideTaxStateFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/google/gson/Gson;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/catalogapi/CatalogIntegrationController;",
            ">;)",
            "Lcom/squareup/ui/settings/taxes/tax/TaxScope_Module_ProvideTaxStateFactory;"
        }
    .end annotation

    .line 42
    new-instance v0, Lcom/squareup/ui/settings/taxes/tax/TaxScope_Module_ProvideTaxStateFactory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/settings/taxes/tax/TaxScope_Module_ProvideTaxStateFactory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideTaxState(Lcom/google/gson/Gson;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/catalogapi/CatalogIntegrationController;)Lcom/squareup/ui/settings/taxes/tax/TaxState;
    .locals 0

    .line 47
    invoke-static {p0, p1, p2}, Lcom/squareup/ui/settings/taxes/tax/TaxScope$Module;->provideTaxState(Lcom/google/gson/Gson;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/catalogapi/CatalogIntegrationController;)Lcom/squareup/ui/settings/taxes/tax/TaxState;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/settings/taxes/tax/TaxState;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/settings/taxes/tax/TaxState;
    .locals 3

    .line 36
    iget-object v0, p0, Lcom/squareup/ui/settings/taxes/tax/TaxScope_Module_ProvideTaxStateFactory;->gsonProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/gson/Gson;

    iget-object v1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxScope_Module_ProvideTaxStateFactory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v2, p0, Lcom/squareup/ui/settings/taxes/tax/TaxScope_Module_ProvideTaxStateFactory;->catalogIntegrationControllerProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/catalogapi/CatalogIntegrationController;

    invoke-static {v0, v1, v2}, Lcom/squareup/ui/settings/taxes/tax/TaxScope_Module_ProvideTaxStateFactory;->provideTaxState(Lcom/google/gson/Gson;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/catalogapi/CatalogIntegrationController;)Lcom/squareup/ui/settings/taxes/tax/TaxState;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/ui/settings/taxes/tax/TaxScope_Module_ProvideTaxStateFactory;->get()Lcom/squareup/ui/settings/taxes/tax/TaxState;

    move-result-object v0

    return-object v0
.end method
