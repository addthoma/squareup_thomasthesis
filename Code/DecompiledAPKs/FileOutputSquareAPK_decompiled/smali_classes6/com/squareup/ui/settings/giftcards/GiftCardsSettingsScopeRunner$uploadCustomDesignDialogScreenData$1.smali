.class final Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$uploadCustomDesignDialogScreenData$1;
.super Ljava/lang/Object;
.source "GiftCardsSettingsScopeRunner.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;->uploadCustomDesignDialogScreenData()Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen$UploadCustomDesignDialogScreenData;",
        "it",
        "Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader$UploadResult;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$uploadCustomDesignDialogScreenData$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$uploadCustomDesignDialogScreenData$1;

    invoke-direct {v0}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$uploadCustomDesignDialogScreenData$1;-><init>()V

    sput-object v0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$uploadCustomDesignDialogScreenData$1;->INSTANCE:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$uploadCustomDesignDialogScreenData$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader$UploadResult;)Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen$UploadCustomDesignDialogScreenData;
    .locals 1

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 273
    new-instance v0, Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen$UploadCustomDesignDialogScreenData;

    instance-of p1, p1, Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader$UploadResult$Success;

    invoke-direct {v0, p1}, Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen$UploadCustomDesignDialogScreenData;-><init>(Z)V

    return-object v0
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 47
    check-cast p1, Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader$UploadResult;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner$uploadCustomDesignDialogScreenData$1;->apply(Lcom/squareup/ui/settings/giftcards/EGiftCardImageUploader$UploadResult;)Lcom/squareup/ui/settings/giftcards/UploadCustomDesignDialogScreen$UploadCustomDesignDialogScreenData;

    move-result-object p1

    return-object p1
.end method
