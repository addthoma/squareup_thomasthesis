.class public final Lcom/squareup/ui/settings/instantdeposits/DepositsSection$Access;
.super Lcom/squareup/applet/SectionAccess;
.source "DepositsSection.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/instantdeposits/DepositsSection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Access"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0008\u0010\u0007\u001a\u00020\u0008H\u0016J\u000e\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\nH\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/ui/settings/instantdeposits/DepositsSection$Access;",
        "Lcom/squareup/applet/SectionAccess;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "employeeManagement",
        "Lcom/squareup/permissions/EmployeeManagement;",
        "(Lcom/squareup/settings/server/Features;Lcom/squareup/permissions/EmployeeManagement;)V",
        "determineVisibility",
        "",
        "getPermissions",
        "",
        "Lcom/squareup/permissions/Permission;",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

.field private final features:Lcom/squareup/settings/server/Features;


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/Features;Lcom/squareup/permissions/EmployeeManagement;)V
    .locals 1

    const-string v0, "features"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "employeeManagement"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    invoke-direct {p0}, Lcom/squareup/applet/SectionAccess;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/DepositsSection$Access;->features:Lcom/squareup/settings/server/Features;

    iput-object p2, p0, Lcom/squareup/ui/settings/instantdeposits/DepositsSection$Access;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    return-void
.end method


# virtual methods
.method public determineVisibility()Z
    .locals 2

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositsSection$Access;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->DEPOSIT_SETTINGS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 56
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositsSection$Access;->employeeManagement:Lcom/squareup/permissions/EmployeeManagement;

    sget-object v1, Lcom/squareup/permissions/Permission;->USE_INSTANT_DEPOSIT:Lcom/squareup/permissions/Permission;

    invoke-interface {v0, v1}, Lcom/squareup/permissions/EmployeeManagement;->shouldDisplayFeature(Lcom/squareup/permissions/Permission;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public getPermissions()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Permission;",
            ">;"
        }
    .end annotation

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/settings/instantdeposits/DepositsSection$Access;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->USE_SETTINGS_GRANULAR_PERMISSIONS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    sget-object v0, Lcom/squareup/permissions/Permission;->MANAGE_BALANCE_SETTINGS:Lcom/squareup/permissions/Permission;

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/squareup/permissions/Permission;->SETTINGS:Lcom/squareup/permissions/Permission;

    :goto_0
    invoke-static {v0}, Lkotlin/collections/SetsKt;->setOf(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
