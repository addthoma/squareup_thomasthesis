.class final Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner$depositScheduleScreenData$1;
.super Ljava/lang/Object;
.source "DepositSettingsScopeRunner.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->depositScheduleScreenData()Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u001a\u0010\u0002\u001a\u0016\u0012\u000c\u0012\n \u0005*\u0004\u0018\u00010\u00040\u0004\u0012\u0004\u0012\u00020\u00060\u0003H\n\u00a2\u0006\u0002\u0008\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/ui/settings/instantdeposits/DepositScheduleScreen$ScreenData;",
        "<name for destructuring parameter 0>",
        "Lkotlin/Pair;",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "kotlin.jvm.PlatformType",
        "Lcom/squareup/depositschedule/DepositScheduleSettings$State;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner$depositScheduleScreenData$1;->this$0:Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lkotlin/Pair;)Lcom/squareup/ui/settings/instantdeposits/DepositScheduleScreen$ScreenData;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Pair<",
            "+",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/depositschedule/DepositScheduleSettings$State;",
            ">;)",
            "Lcom/squareup/ui/settings/instantdeposits/DepositScheduleScreen$ScreenData;"
        }
    .end annotation

    const-string v0, "<name for destructuring parameter 0>"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Lkotlin/Pair;->component1()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {p1}, Lkotlin/Pair;->component2()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/depositschedule/DepositScheduleSettings$State;

    .line 116
    iget-object v1, p0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner$depositScheduleScreenData$1;->this$0:Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;

    const-string v2, "settings"

    .line 117
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getInstantDepositsSettings()Lcom/squareup/settings/server/InstantDepositsSettings;

    move-result-object v0

    const-string v2, "settings.instantDepositsSettings"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 116
    invoke-static {v1, v0, p1}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;->access$toDepositScheduleScreenData(Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;Lcom/squareup/settings/server/InstantDepositsSettings;Lcom/squareup/depositschedule/DepositScheduleSettings$State;)Lcom/squareup/ui/settings/instantdeposits/DepositScheduleScreen$ScreenData;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 60
    check-cast p1, Lkotlin/Pair;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner$depositScheduleScreenData$1;->apply(Lkotlin/Pair;)Lcom/squareup/ui/settings/instantdeposits/DepositScheduleScreen$ScreenData;

    move-result-object p1

    return-object p1
.end method
