.class public Lcom/squareup/ui/settings/loyalty/LoyaltySettingsSection$ListEntry;
.super Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;
.source "LoyaltySettingsSection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/loyalty/LoyaltySettingsSection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ListEntry"
.end annotation


# instance fields
.field private final loyaltyLoggedInSettings:Lcom/squareup/loyalty/LoyaltySettings;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/settings/loyalty/LoyaltySettingsSection;Lcom/squareup/util/Res;Lcom/squareup/util/Device;Lcom/squareup/loyalty/LoyaltySettings;)V
    .locals 6
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 38
    sget-object v2, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;->CHECKOUT_OPTIONS:Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;

    sget v3, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsSection;->TITLE_ID:I

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;-><init>(Lcom/squareup/applet/AppletSection;Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$SettingsAppletGrouping;ILcom/squareup/util/Res;Lcom/squareup/util/Device;)V

    .line 39
    iput-object p4, p0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsSection$ListEntry;->loyaltyLoggedInSettings:Lcom/squareup/loyalty/LoyaltySettings;

    return-void
.end method


# virtual methods
.method public getValueText()Ljava/lang/String;
    .locals 2

    .line 43
    iget-object v0, p0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsSection$ListEntry;->res:Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsSection$ListEntry;->loyaltyLoggedInSettings:Lcom/squareup/loyalty/LoyaltySettings;

    invoke-interface {v1}, Lcom/squareup/loyalty/LoyaltySettings;->isLoyaltyScreensEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    sget v1, Lcom/squareup/settingsapplet/R$string;->loyalty_settings_on:I

    goto :goto_0

    :cond_0
    sget v1, Lcom/squareup/settingsapplet/R$string;->loyalty_settings_off:I

    :goto_0
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
