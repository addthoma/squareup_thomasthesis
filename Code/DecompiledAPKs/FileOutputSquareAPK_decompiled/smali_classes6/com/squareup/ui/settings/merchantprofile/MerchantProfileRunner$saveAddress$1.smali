.class final Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner$saveAddress$1;
.super Lkotlin/jvm/internal/Lambda;
.source "MerchantProfileRunner.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;->saveAddress(ZLcom/squareup/address/Address;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
        "+",
        "Lcom/squareup/server/account/MerchantProfileResponse;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "result",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "Lcom/squareup/server/account/MerchantProfileResponse;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner$saveAddress$1;->this$0:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 33
    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner$saveAddress$1;->invoke(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "+",
            "Lcom/squareup/server/account/MerchantProfileResponse;",
            ">;)V"
        }
    .end annotation

    const-string v0, "result"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 82
    instance-of p1, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner$saveAddress$1;->this$0:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;

    invoke-static {p1}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;->access$getMonitor$p(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileRunner;)Lcom/squareup/requestingbusinessaddress/RequestBusinessAddressMonitor;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/requestingbusinessaddress/RequestBusinessAddressMonitor;->setBusinessAddressVerified()V

    :cond_0
    return-void
.end method
