.class public final Lcom/squareup/ui/settings/merchantprofile/PublicProfileSection$ReceiptInfoGranularPermissionAccess;
.super Lcom/squareup/ui/settings/merchantprofile/PublicProfileSection$Access;
.source "PublicProfileSection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/merchantprofile/PublicProfileSection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ReceiptInfoGranularPermissionAccess"
.end annotation


# instance fields
.field public final acceptablePermissions:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Permission;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/settings/server/AccountStatusSettings;)V
    .locals 0

    .line 92
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/merchantprofile/PublicProfileSection$Access;-><init>(Lcom/squareup/settings/server/AccountStatusSettings;)V

    .line 93
    sget-object p1, Lcom/squareup/permissions/Permission;->MANAGE_RECEIPT_INFORMATION:Lcom/squareup/permissions/Permission;

    invoke-static {p1}, Lkotlin/collections/SetsKt;->setOf(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/PublicProfileSection$ReceiptInfoGranularPermissionAccess;->acceptablePermissions:Ljava/util/Set;

    return-void
.end method


# virtual methods
.method public getPermissions()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Permission;",
            ">;"
        }
    .end annotation

    .line 98
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/PublicProfileSection$ReceiptInfoGranularPermissionAccess;->acceptablePermissions:Ljava/util/Set;

    return-object v0
.end method
