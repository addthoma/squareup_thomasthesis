.class public Lcom/squareup/ui/settings/loyalty/LoyaltySettingsCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "LoyaltySettingsCoordinator.java"


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private final device:Lcom/squareup/util/Device;

.field private enabledOptionsContainer:Landroid/view/View;

.field private frontOfTransactionsContainer:Landroid/view/View;

.field private frontOfTransactionsToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

.field private loyaltyScreenEnabledToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

.field private final res:Lcom/squareup/util/Res;

.field private final runner:Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScopeRunner;

.field private showNonQualifyingToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

.field private timeout30:Lcom/squareup/widgets/list/ToggleButtonRow;

.field private timeout60:Lcom/squareup/widgets/list/ToggleButtonRow;

.field private timeout90:Lcom/squareup/widgets/list/ToggleButtonRow;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScopeRunner;Lcom/squareup/util/Res;Lcom/squareup/util/Device;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 37
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsCoordinator;->runner:Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScopeRunner;

    .line 39
    iput-object p2, p0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsCoordinator;->res:Lcom/squareup/util/Res;

    .line 40
    iput-object p3, p0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsCoordinator;->device:Lcom/squareup/util/Device;

    return-void
.end method

.method private bindViews(Landroid/view/View;)V
    .locals 1

    .line 93
    invoke-static {p1}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 94
    sget v0, Lcom/squareup/settingsapplet/R$id;->loyalty_settings_enable_loyalty:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsCoordinator;->loyaltyScreenEnabledToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 95
    sget v0, Lcom/squareup/settingsapplet/R$id;->loyalty_settings_enabled_content:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsCoordinator;->enabledOptionsContainer:Landroid/view/View;

    .line 96
    sget v0, Lcom/squareup/settingsapplet/R$id;->loyalty_settings_show_nonqualifying:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsCoordinator;->showNonQualifyingToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 98
    sget v0, Lcom/squareup/settingsapplet/R$id;->loyalty_settings_timeout_30:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsCoordinator;->timeout30:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 99
    sget v0, Lcom/squareup/settingsapplet/R$id;->loyalty_settings_timeout_60:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsCoordinator;->timeout60:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 100
    sget v0, Lcom/squareup/settingsapplet/R$id;->loyalty_settings_timeout_90:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsCoordinator;->timeout90:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 102
    sget v0, Lcom/squareup/settingsapplet/R$id;->loyalty_settings_front_of_transaction_section:I

    .line 103
    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsCoordinator;->frontOfTransactionsContainer:Landroid/view/View;

    .line 104
    sget v0, Lcom/squareup/settingsapplet/R$id;->loyalty_settings_front_of_transaction_toggle:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object p1, p0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsCoordinator;->frontOfTransactionsToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    return-void
.end method

.method public static synthetic lambda$5nCje8UfzKHUmEMU-pt0EynkpI0(Lcom/squareup/ui/settings/loyalty/LoyaltySettingsCoordinator;Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScreen$ScreenData;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsCoordinator;->onScreenData(Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScreen$ScreenData;)V

    return-void
.end method

.method private onScreenData(Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScreen$ScreenData;)V
    .locals 2

    .line 80
    iget-object v0, p0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsCoordinator;->loyaltyScreenEnabledToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    iget-boolean v1, p1, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScreen$ScreenData;->loyaltyScreensEnabled:Z

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsCoordinator;->enabledOptionsContainer:Landroid/view/View;

    iget-boolean v1, p1, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScreen$ScreenData;->loyaltyScreensEnabled:Z

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 82
    iget-object v0, p0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsCoordinator;->showNonQualifyingToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    iget-boolean v1, p1, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScreen$ScreenData;->showNonQualifyingEvents:Z

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    .line 84
    iget-object v0, p0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsCoordinator;->timeout30:Lcom/squareup/widgets/list/ToggleButtonRow;

    iget-boolean v1, p1, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScreen$ScreenData;->timeout30:Z

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    .line 85
    iget-object v0, p0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsCoordinator;->timeout60:Lcom/squareup/widgets/list/ToggleButtonRow;

    iget-boolean v1, p1, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScreen$ScreenData;->timeout60:Z

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    .line 86
    iget-object v0, p0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsCoordinator;->timeout90:Lcom/squareup/widgets/list/ToggleButtonRow;

    iget-boolean v1, p1, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScreen$ScreenData;->timeout90:Z

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    .line 88
    iget-object v0, p0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsCoordinator;->frontOfTransactionsContainer:Landroid/view/View;

    iget-boolean v1, p1, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScreen$ScreenData;->frontOfTransactionToggleDisplayed:Z

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 89
    iget-object v0, p0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsCoordinator;->frontOfTransactionsToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    iget-boolean p1, p1, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScreen$ScreenData;->frontOfTransactionEnabled:Z

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 4

    .line 44
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsCoordinator;->bindViews(Landroid/view/View;)V

    .line 46
    new-instance v0, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    .line 47
    iget-object v1, p0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsCoordinator;->device:Lcom/squareup/util/Device;

    invoke-interface {v1}, Lcom/squareup/util/Device;->isPhoneOrPortraitLessThan10Inches()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 48
    iget-object v1, p0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/settingsapplet/R$string;->loyalty_settings_title:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonTextBackArrow(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    const/4 v2, 0x1

    .line 49
    invoke-virtual {v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsCoordinator;->runner:Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScopeRunner;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v3, Lcom/squareup/ui/settings/loyalty/-$$Lambda$kCMLL6zUc_zfTJJJGx_CgmvPVTw;

    invoke-direct {v3, v2}, Lcom/squareup/ui/settings/loyalty/-$$Lambda$kCMLL6zUc_zfTJJJGx_CgmvPVTw;-><init>(Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScopeRunner;)V

    .line 50
    invoke-virtual {v1, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 51
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 53
    iget-object v2, p0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsCoordinator;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/settingsapplet/R$string;->loyalty_settings_title:I

    .line 54
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v1

    .line 55
    invoke-virtual {v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->hideUpButton()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 57
    :goto_0
    iget-object v1, p0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 59
    new-instance v0, Lcom/squareup/ui/settings/loyalty/-$$Lambda$LoyaltySettingsCoordinator$1WWjBAgXeWaKg0MAIg-h8RrJ7ak;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/loyalty/-$$Lambda$LoyaltySettingsCoordinator$1WWjBAgXeWaKg0MAIg-h8RrJ7ak;-><init>(Lcom/squareup/ui/settings/loyalty/LoyaltySettingsCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/RxViews;->unsubscribeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 62
    iget-object p1, p0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsCoordinator;->loyaltyScreenEnabledToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    new-instance v0, Lcom/squareup/ui/settings/loyalty/-$$Lambda$LoyaltySettingsCoordinator$psP95FkAekd2Na5Mw4CU2EytPsw;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/loyalty/-$$Lambda$LoyaltySettingsCoordinator$psP95FkAekd2Na5Mw4CU2EytPsw;-><init>(Lcom/squareup/ui/settings/loyalty/LoyaltySettingsCoordinator;)V

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 66
    iget-object p1, p0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsCoordinator;->showNonQualifyingToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    new-instance v0, Lcom/squareup/ui/settings/loyalty/-$$Lambda$LoyaltySettingsCoordinator$NxgkTetuU1oGp3H_Qv1JLfw4uqQ;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/loyalty/-$$Lambda$LoyaltySettingsCoordinator$NxgkTetuU1oGp3H_Qv1JLfw4uqQ;-><init>(Lcom/squareup/ui/settings/loyalty/LoyaltySettingsCoordinator;)V

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 70
    iget-object p1, p0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsCoordinator;->timeout30:Lcom/squareup/widgets/list/ToggleButtonRow;

    new-instance v0, Lcom/squareup/ui/settings/loyalty/-$$Lambda$LoyaltySettingsCoordinator$ek04jvNlmfA68l0fxbh9adhAtuY;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/loyalty/-$$Lambda$LoyaltySettingsCoordinator$ek04jvNlmfA68l0fxbh9adhAtuY;-><init>(Lcom/squareup/ui/settings/loyalty/LoyaltySettingsCoordinator;)V

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 71
    iget-object p1, p0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsCoordinator;->timeout60:Lcom/squareup/widgets/list/ToggleButtonRow;

    new-instance v0, Lcom/squareup/ui/settings/loyalty/-$$Lambda$LoyaltySettingsCoordinator$grVNl9D3igRe3Zmpm341Se8zGVg;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/loyalty/-$$Lambda$LoyaltySettingsCoordinator$grVNl9D3igRe3Zmpm341Se8zGVg;-><init>(Lcom/squareup/ui/settings/loyalty/LoyaltySettingsCoordinator;)V

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 72
    iget-object p1, p0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsCoordinator;->timeout90:Lcom/squareup/widgets/list/ToggleButtonRow;

    new-instance v0, Lcom/squareup/ui/settings/loyalty/-$$Lambda$LoyaltySettingsCoordinator$tUWrcJBOwbGEW9N6vjQZfwDjv98;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/loyalty/-$$Lambda$LoyaltySettingsCoordinator$tUWrcJBOwbGEW9N6vjQZfwDjv98;-><init>(Lcom/squareup/ui/settings/loyalty/LoyaltySettingsCoordinator;)V

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 75
    iget-object p1, p0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsCoordinator;->frontOfTransactionsToggle:Lcom/squareup/widgets/list/ToggleButtonRow;

    new-instance v0, Lcom/squareup/ui/settings/loyalty/-$$Lambda$LoyaltySettingsCoordinator$PFlYf2cDuRLm9xf9xyEiknlFO38;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/loyalty/-$$Lambda$LoyaltySettingsCoordinator$PFlYf2cDuRLm9xf9xyEiknlFO38;-><init>(Lcom/squareup/ui/settings/loyalty/LoyaltySettingsCoordinator;)V

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    return-void
.end method

.method public synthetic lambda$attach$0$LoyaltySettingsCoordinator()Lrx/Subscription;
    .locals 2

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsCoordinator;->runner:Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScopeRunner;->getScreenData()Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/settings/loyalty/-$$Lambda$LoyaltySettingsCoordinator$5nCje8UfzKHUmEMU-pt0EynkpI0;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/loyalty/-$$Lambda$LoyaltySettingsCoordinator$5nCje8UfzKHUmEMU-pt0EynkpI0;-><init>(Lcom/squareup/ui/settings/loyalty/LoyaltySettingsCoordinator;)V

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$attach$1$LoyaltySettingsCoordinator(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 63
    iget-object p1, p0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsCoordinator;->runner:Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScopeRunner;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScopeRunner;->setLoyaltyScreenEnabled(Z)V

    return-void
.end method

.method public synthetic lambda$attach$2$LoyaltySettingsCoordinator(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 67
    iget-object p1, p0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsCoordinator;->runner:Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScopeRunner;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScopeRunner;->setShowNonQualifyingLoyaltyEvents(Z)V

    return-void
.end method

.method public synthetic lambda$attach$3$LoyaltySettingsCoordinator(Landroid/widget/CompoundButton;Z)V
    .locals 2

    .line 70
    iget-object p1, p0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsCoordinator;->runner:Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScopeRunner;

    const-wide/16 v0, 0x1e

    invoke-virtual {p1, v0, v1}, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScopeRunner;->setLoyaltyTimeout(J)V

    return-void
.end method

.method public synthetic lambda$attach$4$LoyaltySettingsCoordinator(Landroid/widget/CompoundButton;Z)V
    .locals 2

    .line 71
    iget-object p1, p0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsCoordinator;->runner:Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScopeRunner;

    const-wide/16 v0, 0x3c

    invoke-virtual {p1, v0, v1}, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScopeRunner;->setLoyaltyTimeout(J)V

    return-void
.end method

.method public synthetic lambda$attach$5$LoyaltySettingsCoordinator(Landroid/widget/CompoundButton;Z)V
    .locals 2

    .line 72
    iget-object p1, p0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsCoordinator;->runner:Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScopeRunner;

    const-wide/16 v0, 0x5a

    invoke-virtual {p1, v0, v1}, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScopeRunner;->setLoyaltyTimeout(J)V

    return-void
.end method

.method public synthetic lambda$attach$6$LoyaltySettingsCoordinator(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 76
    iget-object p1, p0, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsCoordinator;->runner:Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScopeRunner;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/loyalty/LoyaltySettingsScopeRunner;->setFrontOfTransaction(Z)V

    return-void
.end method
