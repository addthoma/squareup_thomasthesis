.class final Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder$setupHourPicker$1;
.super Ljava/lang/Object;
.source "CloseOfDayDialogBuilder.kt"

# interfaces
.implements Lcom/squareup/noho/NohoNumberPicker$Formatter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder;->setupHourPicker()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "kotlin.jvm.PlatformType",
        "value",
        "",
        "format"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder$setupHourPicker$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder$setupHourPicker$1;

    invoke-direct {v0}, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder$setupHourPicker$1;-><init>()V

    sput-object v0, Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder$setupHourPicker$1;->INSTANCE:Lcom/squareup/ui/settings/instantdeposits/CloseOfDayDialogBuilder$setupHourPicker$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final format(I)Ljava/lang/String;
    .locals 0

    if-nez p1, :cond_0

    const-string p1, "12"

    goto :goto_0

    .line 154
    :cond_0
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method
