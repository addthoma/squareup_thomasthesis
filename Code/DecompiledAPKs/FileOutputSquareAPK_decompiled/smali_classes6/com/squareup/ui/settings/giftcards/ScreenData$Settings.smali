.class public final Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;
.super Lcom/squareup/ui/settings/giftcards/ScreenData;
.source "GiftCardsSettingsScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/giftcards/ScreenData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Settings"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0011\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B=\u0012\u000c\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u000c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0003\u0012\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0008\u0008\u0002\u0010\u000b\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000cJ\u000f\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0003J\u000f\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0003H\u00c6\u0003J\u000b\u0010\u0016\u001a\u0004\u0018\u00010\u0008H\u00c6\u0003J\t\u0010\u0017\u001a\u00020\nH\u00c6\u0003J\t\u0010\u0018\u001a\u00020\nH\u00c6\u0003JI\u0010\u0019\u001a\u00020\u00002\u000e\u0008\u0002\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u000e\u0008\u0002\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u00032\n\u0008\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u00082\u0008\u0008\u0002\u0010\t\u001a\u00020\n2\u0008\u0008\u0002\u0010\u000b\u001a\u00020\nH\u00c6\u0001J\u0013\u0010\u001a\u001a\u00020\n2\u0008\u0010\u001b\u001a\u0004\u0018\u00010\u001cH\u00d6\u0003J\t\u0010\u001d\u001a\u00020\u001eH\u00d6\u0001J\t\u0010\u001f\u001a\u00020 H\u00d6\u0001R\u0017\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000eR\u0017\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000f\u0010\u000eR\u0011\u0010\u000b\u001a\u00020\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\u0011R\u0013\u0010\u0007\u001a\u0004\u0018\u00010\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013\u00a8\u0006!"
    }
    d2 = {
        "Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;",
        "Lcom/squareup/ui/settings/giftcards/ScreenData;",
        "all_egift_themes",
        "",
        "Lcom/squareup/protos/client/giftcards/EGiftTheme;",
        "denomination_amounts",
        "Lcom/squareup/protos/common/Money;",
        "order_configuration",
        "Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;",
        "isEGiftCardVisible",
        "",
        "failedToSave",
        "(Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;ZZ)V",
        "getAll_egift_themes",
        "()Ljava/util/List;",
        "getDenomination_amounts",
        "getFailedToSave",
        "()Z",
        "getOrder_configuration",
        "()Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;",
        "component1",
        "component2",
        "component3",
        "component4",
        "component5",
        "copy",
        "equals",
        "other",
        "",
        "hashCode",
        "",
        "toString",
        "",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final all_egift_themes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/giftcards/EGiftTheme;",
            ">;"
        }
    .end annotation
.end field

.field private final denomination_amounts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final failedToSave:Z

.field private final isEGiftCardVisible:Z

.field private final order_configuration:Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;ZZ)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/giftcards/EGiftTheme;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;",
            "ZZ)V"
        }
    .end annotation

    const-string v0, "all_egift_themes"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "denomination_amounts"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 80
    invoke-direct {p0, v0}, Lcom/squareup/ui/settings/giftcards/ScreenData;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;->all_egift_themes:Ljava/util/List;

    iput-object p2, p0, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;->denomination_amounts:Ljava/util/List;

    iput-object p3, p0, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;->order_configuration:Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    iput-boolean p4, p0, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;->isEGiftCardVisible:Z

    iput-boolean p5, p0, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;->failedToSave:Z

    return-void
.end method

.method public synthetic constructor <init>(Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;ZZILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 6

    and-int/lit8 p6, p6, 0x10

    if-eqz p6, :cond_0

    const/4 p5, 0x0

    const/4 v5, 0x0

    goto :goto_0

    :cond_0
    move v5, p5

    :goto_0
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    .line 79
    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;-><init>(Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;ZZ)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;ZZILjava/lang/Object;)Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;
    .locals 3

    and-int/lit8 p7, p6, 0x1

    if-eqz p7, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;->all_egift_themes:Ljava/util/List;

    :cond_0
    and-int/lit8 p7, p6, 0x2

    if-eqz p7, :cond_1

    iget-object p2, p0, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;->denomination_amounts:Ljava/util/List;

    :cond_1
    move-object p7, p2

    and-int/lit8 p2, p6, 0x4

    if-eqz p2, :cond_2

    iget-object p3, p0, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;->order_configuration:Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    :cond_2
    move-object v0, p3

    and-int/lit8 p2, p6, 0x8

    if-eqz p2, :cond_3

    iget-boolean p4, p0, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;->isEGiftCardVisible:Z

    :cond_3
    move v1, p4

    and-int/lit8 p2, p6, 0x10

    if-eqz p2, :cond_4

    iget-boolean p5, p0, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;->failedToSave:Z

    :cond_4
    move v2, p5

    move-object p2, p0

    move-object p3, p1

    move-object p4, p7

    move-object p5, v0

    move p6, v1

    move p7, v2

    invoke-virtual/range {p2 .. p7}, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;->copy(Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;ZZ)Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/giftcards/EGiftTheme;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;->all_egift_themes:Ljava/util/List;

    return-object v0
.end method

.method public final component2()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;->denomination_amounts:Ljava/util/List;

    return-object v0
.end method

.method public final component3()Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;->order_configuration:Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    return-object v0
.end method

.method public final component4()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;->isEGiftCardVisible:Z

    return v0
.end method

.method public final component5()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;->failedToSave:Z

    return v0
.end method

.method public final copy(Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;ZZ)Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/giftcards/EGiftTheme;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;",
            "ZZ)",
            "Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;"
        }
    .end annotation

    const-string v0, "all_egift_themes"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "denomination_amounts"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;

    move-object v1, v0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;-><init>(Ljava/util/List;Ljava/util/List;Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;ZZ)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;

    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;->all_egift_themes:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;->all_egift_themes:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;->denomination_amounts:Ljava/util/List;

    iget-object v1, p1, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;->denomination_amounts:Ljava/util/List;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;->order_configuration:Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    iget-object v1, p1, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;->order_configuration:Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;->isEGiftCardVisible:Z

    iget-boolean v1, p1, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;->isEGiftCardVisible:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;->failedToSave:Z

    iget-boolean p1, p1, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;->failedToSave:Z

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getAll_egift_themes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/client/giftcards/EGiftTheme;",
            ">;"
        }
    .end annotation

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;->all_egift_themes:Ljava/util/List;

    return-object v0
.end method

.method public final getDenomination_amounts()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation

    .line 71
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;->denomination_amounts:Ljava/util/List;

    return-object v0
.end method

.method public final getFailedToSave()Z
    .locals 1

    .line 79
    iget-boolean v0, p0, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;->failedToSave:Z

    return v0
.end method

.method public final getOrder_configuration()Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;
    .locals 1

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;->order_configuration:Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;->all_egift_themes:Ljava/util/List;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;->denomination_amounts:Ljava/util/List;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;->order_configuration:Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v1

    :cond_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;->isEGiftCardVisible:Z

    const/4 v2, 0x1

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :cond_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;->failedToSave:Z

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    :cond_4
    add-int/2addr v0, v1

    return v0
.end method

.method public final isEGiftCardVisible()Z
    .locals 1

    .line 78
    iget-boolean v0, p0, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;->isEGiftCardVisible:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Settings(all_egift_themes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;->all_egift_themes:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", denomination_amounts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;->denomination_amounts:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", order_configuration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;->order_configuration:Lcom/squareup/protos/client/giftcards/EGiftOrderConfiguration;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", isEGiftCardVisible="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;->isEGiftCardVisible:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ", failedToSave="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/squareup/ui/settings/giftcards/ScreenData$Settings;->failedToSave:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
