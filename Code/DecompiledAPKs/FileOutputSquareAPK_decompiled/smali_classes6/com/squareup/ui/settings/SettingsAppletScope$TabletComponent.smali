.class public interface abstract Lcom/squareup/ui/settings/SettingsAppletScope$TabletComponent;
.super Ljava/lang/Object;
.source "SettingsAppletScope.java"

# interfaces
.implements Lcom/squareup/ui/settings/SettingsAppletScope$BaseComponent;
.implements Lcom/squareup/ui/settings/SettingsSectionsView$Component;


# annotations
.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/ui/settings/SettingsAppletScope$Module;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/SettingsAppletScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "TabletComponent"
.end annotation
