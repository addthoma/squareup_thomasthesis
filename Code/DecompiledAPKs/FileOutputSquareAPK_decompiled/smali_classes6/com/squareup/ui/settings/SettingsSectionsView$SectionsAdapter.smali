.class Lcom/squareup/ui/settings/SettingsSectionsView$SectionsAdapter;
.super Landroid/widget/BaseAdapter;
.source "SettingsSectionsView.java"

# interfaces
.implements Lcom/squareup/stickylistheaders/StickyListHeadersAdapter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/SettingsSectionsView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SectionsAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/SettingsSectionsView;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/SettingsSectionsView;)V
    .locals 0

    .line 113
    iput-object p1, p0, Lcom/squareup/ui/settings/SettingsSectionsView$SectionsAdapter;->this$0:Lcom/squareup/ui/settings/SettingsSectionsView;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method

.method private bindButtonToAppletBanner(Lcom/squareup/applet/AppletBanner;Lcom/squareup/onboarding/BannerButton;)V
    .locals 3

    .line 337
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsSectionsView$SectionsAdapter;->this$0:Lcom/squareup/ui/settings/SettingsSectionsView;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/SettingsSectionsView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 340
    invoke-virtual {p2}, Lcom/squareup/onboarding/BannerButton;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 341
    new-instance v1, Lcom/squareup/ui/settings/-$$Lambda$SettingsSectionsView$SectionsAdapter$R_p9QaVVrPctOnyPTVMyGTMwlAs;

    invoke-direct {v1, p0, p2, v0}, Lcom/squareup/ui/settings/-$$Lambda$SettingsSectionsView$SectionsAdapter$R_p9QaVVrPctOnyPTVMyGTMwlAs;-><init>(Lcom/squareup/ui/settings/SettingsSectionsView$SectionsAdapter;Lcom/squareup/onboarding/BannerButton;Landroid/content/res/Resources;)V

    invoke-static {v1}, Lcom/squareup/debounce/Debouncers;->debounceRunnable(Ljava/lang/Runnable;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/applet/AppletBanner;->setCallToActionClickListener(Landroid/view/View$OnClickListener;)V

    .line 345
    new-instance v1, Lcom/squareup/applet/BannerVisibility$ShowBanner;

    invoke-virtual {p2, v0}, Lcom/squareup/onboarding/BannerButton;->callToAction(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v0}, Lcom/squareup/onboarding/BannerButton;->subtitle(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object p2

    invoke-direct {v1, v2, p2}, Lcom/squareup/applet/BannerVisibility$ShowBanner;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 347
    :cond_0
    sget-object v1, Lcom/squareup/applet/BannerVisibility$HideBanner;->INSTANCE:Lcom/squareup/applet/BannerVisibility$HideBanner;

    .line 349
    :goto_0
    invoke-virtual {p1, v1}, Lcom/squareup/applet/AppletBanner;->setVisibility(Lcom/squareup/applet/BannerVisibility;)V

    return-void
.end method

.method private buildAppletSection(Landroid/view/ViewGroup;)Lcom/squareup/ui/account/view/SmartLineRow;
    .locals 1

    .line 260
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsSectionsView$SectionsAdapter;->this$0:Lcom/squareup/ui/settings/SettingsSectionsView;

    invoke-static {v0}, Lcom/squareup/ui/settings/SettingsSectionsView;->access$000(Lcom/squareup/ui/settings/SettingsSectionsView;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 261
    sget v0, Lcom/squareup/pos/container/R$layout;->applet_sidebar_row:I

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/account/view/SmartLineRow;

    return-object p1

    .line 263
    :cond_0
    sget v0, Lcom/squareup/settingsapplet/R$layout;->settings_applet_list_row:I

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/account/view/SmartLineRow;

    return-object p1
.end method

.method private buildBanner(Landroid/view/ViewGroup;)Lcom/squareup/applet/AppletBanner;
    .locals 1

    .line 244
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsSectionsView$SectionsAdapter;->this$0:Lcom/squareup/ui/settings/SettingsSectionsView;

    invoke-static {v0}, Lcom/squareup/ui/settings/SettingsSectionsView;->access$000(Lcom/squareup/ui/settings/SettingsSectionsView;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 245
    sget v0, Lcom/squareup/pos/container/R$layout;->applet_banner_sidebar:I

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/applet/AppletBanner;

    return-object p1

    .line 247
    :cond_0
    sget v0, Lcom/squareup/pos/container/R$layout;->applet_banner_list:I

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/applet/AppletBanner;

    return-object p1
.end method

.method private buildGroupingHeader(Landroid/view/ViewGroup;)Landroid/widget/TextView;
    .locals 1

    .line 236
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsSectionsView$SectionsAdapter;->this$0:Lcom/squareup/ui/settings/SettingsSectionsView;

    invoke-static {v0}, Lcom/squareup/ui/settings/SettingsSectionsView;->access$000(Lcom/squareup/ui/settings/SettingsSectionsView;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 237
    sget v0, Lcom/squareup/pos/container/R$layout;->applet_header_sidebar:I

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    return-object p1

    .line 239
    :cond_0
    sget v0, Lcom/squareup/settingsapplet/R$layout;->applet_header_list:I

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    return-object p1
.end method

.method private buildSignOutButton(Landroid/view/ViewGroup;)Landroid/widget/FrameLayout;
    .locals 1

    .line 252
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsSectionsView$SectionsAdapter;->this$0:Lcom/squareup/ui/settings/SettingsSectionsView;

    invoke-static {v0}, Lcom/squareup/ui/settings/SettingsSectionsView;->access$000(Lcom/squareup/ui/settings/SettingsSectionsView;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 253
    sget v0, Lcom/squareup/settingsapplet/R$layout;->sign_out_button_sidebar:I

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/FrameLayout;

    return-object p1

    .line 255
    :cond_0
    sget v0, Lcom/squareup/settingsapplet/R$layout;->sign_out_button_list:I

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/FrameLayout;

    return-object p1
.end method

.method static synthetic lambda$null$2(Lcom/squareup/ui/account/view/SmartLineRow;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 328
    invoke-static {p1}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 329
    invoke-virtual {p0, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueVisible(Z)V

    .line 330
    invoke-virtual {p0, p1}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public areAllItemsEnabled()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method bindAppletSection(Lcom/squareup/ui/account/view/SmartLineRow;I)V
    .locals 1

    .line 304
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsSectionsView$SectionsAdapter;->this$0:Lcom/squareup/ui/settings/SettingsSectionsView;

    invoke-static {v0}, Lcom/squareup/ui/settings/SettingsSectionsView;->access$000(Lcom/squareup/ui/settings/SettingsSectionsView;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 305
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsSectionsView$SectionsAdapter;->this$0:Lcom/squareup/ui/settings/SettingsSectionsView;

    iget-object v0, v0, Lcom/squareup/ui/settings/SettingsSectionsView;->presenter:Lcom/squareup/ui/settings/SettingsSectionsPresenter;

    invoke-virtual {v0, p2}, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->isSectionSelected(I)Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setActivated(Z)V

    .line 308
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/ui/account/view/SmartLineRow;->setDefaultColors()V

    .line 318
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsSectionsView$SectionsAdapter;->this$0:Lcom/squareup/ui/settings/SettingsSectionsView;

    iget-object v0, v0, Lcom/squareup/ui/settings/SettingsSectionsView;->presenter:Lcom/squareup/ui/settings/SettingsSectionsPresenter;

    invoke-virtual {v0, p2}, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->isSectionValueABadge(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 319
    sget v0, Lcom/squareup/marin/R$color;->marin_text_selector_blue:I

    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueColor(I)V

    .line 322
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsSectionsView$SectionsAdapter;->this$0:Lcom/squareup/ui/settings/SettingsSectionsView;

    iget-object v0, v0, Lcom/squareup/ui/settings/SettingsSectionsView;->presenter:Lcom/squareup/ui/settings/SettingsSectionsPresenter;

    invoke-virtual {v0, p2}, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->getSectionName(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleText(Ljava/lang/CharSequence;)V

    .line 323
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsSectionsView$SectionsAdapter;->this$0:Lcom/squareup/ui/settings/SettingsSectionsView;

    iget-object v0, v0, Lcom/squareup/ui/settings/SettingsSectionsView;->presenter:Lcom/squareup/ui/settings/SettingsSectionsPresenter;

    invoke-virtual {v0, p2}, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->getSectionValueShortText(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueShortText(Ljava/lang/CharSequence;)V

    const/4 v0, 0x0

    .line 324
    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueTruncateShortText(Z)V

    .line 325
    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueVisible(Z)V

    .line 326
    new-instance v0, Lcom/squareup/ui/settings/-$$Lambda$SettingsSectionsView$SectionsAdapter$EsCQMKeFlG9t0fU-jaT250ScamM;

    invoke-direct {v0, p0, p2, p1}, Lcom/squareup/ui/settings/-$$Lambda$SettingsSectionsView$SectionsAdapter$EsCQMKeFlG9t0fU-jaT250ScamM;-><init>(Lcom/squareup/ui/settings/SettingsSectionsView$SectionsAdapter;ILcom/squareup/ui/account/view/SmartLineRow;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    const/4 p2, 0x1

    .line 333
    invoke-virtual {p1, p2}, Lcom/squareup/ui/account/view/SmartLineRow;->setEnabled(Z)V

    return-void
.end method

.method bindBanner(Lcom/squareup/applet/AppletBanner;)V
    .locals 1

    .line 287
    new-instance v0, Lcom/squareup/ui/settings/-$$Lambda$SettingsSectionsView$SectionsAdapter$WdEKUPEKThOUjrpvzbGtausBIs8;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/settings/-$$Lambda$SettingsSectionsView$SectionsAdapter$WdEKUPEKThOUjrpvzbGtausBIs8;-><init>(Lcom/squareup/ui/settings/SettingsSectionsView$SectionsAdapter;Lcom/squareup/applet/AppletBanner;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method bindGroupingHeader(Landroid/widget/TextView;I)V
    .locals 3

    .line 268
    invoke-virtual {p0, p2}, Lcom/squareup/ui/settings/SettingsSectionsView$SectionsAdapter;->getItemViewType(I)I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eq v0, v1, :cond_1

    const/4 p2, 0x2

    if-ne v0, p2, :cond_0

    .line 275
    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 276
    iget-object p2, p0, Lcom/squareup/ui/settings/SettingsSectionsView$SectionsAdapter;->this$0:Lcom/squareup/ui/settings/SettingsSectionsView;

    iget-object p2, p2, Lcom/squareup/ui/settings/SettingsSectionsView;->presenter:Lcom/squareup/ui/settings/SettingsSectionsPresenter;

    invoke-virtual {p2}, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->getSignOutHeaderText()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 282
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unexpected row type: "

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 271
    :cond_1
    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 272
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsSectionsView$SectionsAdapter;->this$0:Lcom/squareup/ui/settings/SettingsSectionsView;

    iget-object v0, v0, Lcom/squareup/ui/settings/SettingsSectionsView;->presenter:Lcom/squareup/ui/settings/SettingsSectionsPresenter;

    invoke-virtual {v0, p2}, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->getHeaderForSectionAtPosition(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    :goto_0
    return-void
.end method

.method bindSignOutButton(Landroid/widget/FrameLayout;)V
    .locals 1

    .line 294
    sget v0, Lcom/squareup/settingsapplet/R$id;->sign_out_button:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    .line 295
    new-instance v0, Lcom/squareup/ui/settings/SettingsSectionsView$SectionsAdapter$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/SettingsSectionsView$SectionsAdapter$1;-><init>(Lcom/squareup/ui/settings/SettingsSectionsView$SectionsAdapter;)V

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public getCount()I
    .locals 1

    .line 207
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsSectionsView$SectionsAdapter;->this$0:Lcom/squareup/ui/settings/SettingsSectionsView;

    iget-object v0, v0, Lcom/squareup/ui/settings/SettingsSectionsView;->presenter:Lcom/squareup/ui/settings/SettingsSectionsPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->getSectionCount()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public getHeaderId(I)J
    .locals 3

    .line 219
    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/SettingsSectionsView$SectionsAdapter;->getItemViewType(I)I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 p1, 0x2

    if-ne v0, p1, :cond_0

    const-wide/16 v0, 0x2

    return-wide v0

    .line 231
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected row type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 225
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsSectionsView$SectionsAdapter;->this$0:Lcom/squareup/ui/settings/SettingsSectionsView;

    iget-object v0, v0, Lcom/squareup/ui/settings/SettingsSectionsView;->presenter:Lcom/squareup/ui/settings/SettingsSectionsPresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->getSectionGroupingTitleIdAtPosition(I)J

    move-result-wide v0

    return-wide v0

    :cond_2
    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public getHeaderView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 0

    if-nez p2, :cond_0

    .line 212
    invoke-direct {p0, p3}, Lcom/squareup/ui/settings/SettingsSectionsView$SectionsAdapter;->buildGroupingHeader(Landroid/view/ViewGroup;)Landroid/widget/TextView;

    move-result-object p2

    goto :goto_0

    :cond_0
    check-cast p2, Landroid/widget/TextView;

    .line 214
    :goto_0
    invoke-virtual {p0, p2, p1}, Lcom/squareup/ui/settings/SettingsSectionsView$SectionsAdapter;->bindGroupingHeader(Landroid/widget/TextView;I)V

    return-object p2
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 3

    .line 158
    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/SettingsSectionsView$SectionsAdapter;->getItemViewType(I)I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 p1, 0x2

    if-ne v0, p1, :cond_0

    .line 166
    iget-object p1, p0, Lcom/squareup/ui/settings/SettingsSectionsView$SectionsAdapter;->this$0:Lcom/squareup/ui/settings/SettingsSectionsView;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/SettingsSectionsView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v0, Lcom/squareup/signout/R$string;->sign_out:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 168
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected row type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 164
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsSectionsView$SectionsAdapter;->this$0:Lcom/squareup/ui/settings/SettingsSectionsView;

    iget-object v0, v0, Lcom/squareup/ui/settings/SettingsSectionsView;->presenter:Lcom/squareup/ui/settings/SettingsSectionsPresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->getSectionName(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    .line 162
    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/settings/SettingsSectionsView$SectionsAdapter;->this$0:Lcom/squareup/ui/settings/SettingsSectionsView;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/SettingsSectionsView;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v0, Lcom/squareup/onboarding/common/R$string;->add_bank_account:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 2

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return p1

    .line 146
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsSectionsView$SectionsAdapter;->this$0:Lcom/squareup/ui/settings/SettingsSectionsView;

    iget-object v0, v0, Lcom/squareup/ui/settings/SettingsSectionsView;->presenter:Lcom/squareup/ui/settings/SettingsSectionsPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->getSectionCount()I

    move-result v0

    const/4 v1, 0x1

    add-int/2addr v0, v1

    if-ge p1, v0, :cond_1

    return v1

    .line 150
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/ui/settings/SettingsSectionsView$SectionsAdapter;->getCount()I

    move-result v0

    sub-int/2addr v0, v1

    if-ne p1, v0, :cond_2

    const/4 p1, 0x2

    return p1

    .line 154
    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Should have found the appropriate type by now!"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .line 177
    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/SettingsSectionsView$SectionsAdapter;->getItemViewType(I)I

    move-result v0

    if-eqz v0, :cond_4

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 p1, 0x2

    if-ne v0, p1, :cond_1

    if-nez p2, :cond_0

    .line 195
    invoke-direct {p0, p3}, Lcom/squareup/ui/settings/SettingsSectionsView$SectionsAdapter;->buildSignOutButton(Landroid/view/ViewGroup;)Landroid/widget/FrameLayout;

    move-result-object p1

    goto :goto_0

    :cond_0
    move-object p1, p2

    check-cast p1, Landroid/widget/FrameLayout;

    .line 197
    :goto_0
    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/SettingsSectionsView$SectionsAdapter;->bindSignOutButton(Landroid/widget/FrameLayout;)V

    return-object p1

    .line 201
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "Unexpected row type: "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    if-nez p2, :cond_3

    .line 188
    invoke-direct {p0, p3}, Lcom/squareup/ui/settings/SettingsSectionsView$SectionsAdapter;->buildAppletSection(Landroid/view/ViewGroup;)Lcom/squareup/ui/account/view/SmartLineRow;

    move-result-object p2

    goto :goto_1

    :cond_3
    check-cast p2, Lcom/squareup/ui/account/view/SmartLineRow;

    .line 190
    :goto_1
    invoke-virtual {p0, p2, p1}, Lcom/squareup/ui/settings/SettingsSectionsView$SectionsAdapter;->bindAppletSection(Lcom/squareup/ui/account/view/SmartLineRow;I)V

    return-object p2

    :cond_4
    if-nez p2, :cond_5

    .line 181
    invoke-direct {p0, p3}, Lcom/squareup/ui/settings/SettingsSectionsView$SectionsAdapter;->buildBanner(Landroid/view/ViewGroup;)Lcom/squareup/applet/AppletBanner;

    move-result-object p1

    goto :goto_2

    :cond_5
    move-object p1, p2

    check-cast p1, Lcom/squareup/applet/AppletBanner;

    .line 183
    :goto_2
    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/SettingsSectionsView$SectionsAdapter;->bindBanner(Lcom/squareup/applet/AppletBanner;)V

    return-object p1
.end method

.method public getViewTypeCount()I
    .locals 1

    const/4 v0, 0x3

    return v0
.end method

.method public isEnabled(I)Z
    .locals 3

    .line 121
    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/SettingsSectionsView$SectionsAdapter;->getItemViewType(I)I

    move-result p1

    if-eqz p1, :cond_2

    const/4 v0, 0x1

    if-eq p1, v0, :cond_1

    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    goto :goto_0

    .line 131
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected row type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return v0

    :cond_2
    :goto_0
    const/4 p1, 0x0

    return p1
.end method

.method public synthetic lambda$bindAppletSection$3$SettingsSectionsView$SectionsAdapter(ILcom/squareup/ui/account/view/SmartLineRow;)Lio/reactivex/disposables/Disposable;
    .locals 1

    .line 327
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsSectionsView$SectionsAdapter;->this$0:Lcom/squareup/ui/settings/SettingsSectionsView;

    iget-object v0, v0, Lcom/squareup/ui/settings/SettingsSectionsView;->presenter:Lcom/squareup/ui/settings/SettingsSectionsPresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->getSectionValue(I)Lio/reactivex/Observable;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/settings/-$$Lambda$SettingsSectionsView$SectionsAdapter$niKLjrt8ZdXJlWKmD9SMxDmcuyU;

    invoke-direct {v0, p2}, Lcom/squareup/ui/settings/-$$Lambda$SettingsSectionsView$SectionsAdapter$niKLjrt8ZdXJlWKmD9SMxDmcuyU;-><init>(Lcom/squareup/ui/account/view/SmartLineRow;)V

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$bindBanner$1$SettingsSectionsView$SectionsAdapter(Lcom/squareup/applet/AppletBanner;)Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 288
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsSectionsView$SectionsAdapter;->this$0:Lcom/squareup/ui/settings/SettingsSectionsView;

    iget-object v0, v0, Lcom/squareup/ui/settings/SettingsSectionsView;->presenter:Lcom/squareup/ui/settings/SettingsSectionsPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->getBannerButton()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/settings/-$$Lambda$SettingsSectionsView$SectionsAdapter$3S83TLH8efbq9ohk_U104GonQGI;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/settings/-$$Lambda$SettingsSectionsView$SectionsAdapter$3S83TLH8efbq9ohk_U104GonQGI;-><init>(Lcom/squareup/ui/settings/SettingsSectionsView$SectionsAdapter;Lcom/squareup/applet/AppletBanner;)V

    .line 289
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$bindButtonToAppletBanner$4$SettingsSectionsView$SectionsAdapter(Lcom/squareup/onboarding/BannerButton;Landroid/content/res/Resources;)V
    .locals 1

    .line 342
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsSectionsView$SectionsAdapter;->this$0:Lcom/squareup/ui/settings/SettingsSectionsView;

    iget-object v0, v0, Lcom/squareup/ui/settings/SettingsSectionsView;->presenter:Lcom/squareup/ui/settings/SettingsSectionsPresenter;

    invoke-virtual {p1, p2}, Lcom/squareup/onboarding/BannerButton;->url(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/settings/SettingsSectionsPresenter;->onBannerClicked(Ljava/lang/String;)V

    return-void
.end method

.method public synthetic lambda$null$0$SettingsSectionsView$SectionsAdapter(Lcom/squareup/applet/AppletBanner;Lcom/squareup/onboarding/BannerButton;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 290
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/settings/SettingsSectionsView$SectionsAdapter;->bindButtonToAppletBanner(Lcom/squareup/applet/AppletBanner;Lcom/squareup/onboarding/BannerButton;)V

    return-void
.end method
