.class Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner$1;
.super Lcom/squareup/permissions/PermissionGatekeeper$AccountOwnerOrAdminLoginWhen;
.source "PasscodesSettingsScopeRunner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->onPasscodeSettingsSwitchChanged(Ljava/lang/Boolean;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;)V
    .locals 0

    .line 221
    iput-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner$1;->this$0:Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;

    invoke-direct {p0}, Lcom/squareup/permissions/PermissionGatekeeper$AccountOwnerOrAdminLoginWhen;-><init>()V

    return-void
.end method


# virtual methods
.method public success()V
    .locals 2

    .line 223
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner$1;->this$0:Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;

    invoke-static {v0}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->access$000(Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;)Lcom/squareup/permissions/PasscodesSettings;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/permissions/PasscodesSettings;->enableOrDisablePasscodes(Ljava/lang/Boolean;)V

    return-void
.end method
