.class public final Lcom/squareup/ui/settings/DelegateLockoutScreen;
.super Lcom/squareup/ui/settings/InSettingsAppletScope;
.source "DelegateLockoutScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/settings/DelegateLockoutScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/DelegateLockoutScreen$Component;,
        Lcom/squareup/ui/settings/DelegateLockoutScreen$Presenter;,
        Lcom/squareup/ui/settings/DelegateLockoutScreen$LockoutTitleId;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/DelegateLockoutScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final titleText:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 79
    sget-object v0, Lcom/squareup/ui/settings/-$$Lambda$DelegateLockoutScreen$SD-Rvjlzln6Qb-qAxOq3umFWRyU;->INSTANCE:Lcom/squareup/ui/settings/-$$Lambda$DelegateLockoutScreen$SD-Rvjlzln6Qb-qAxOq3umFWRyU;

    .line 80
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/settings/DelegateLockoutScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 33
    invoke-direct {p0}, Lcom/squareup/ui/settings/InSettingsAppletScope;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/squareup/ui/settings/DelegateLockoutScreen;->titleText:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/settings/DelegateLockoutScreen;)Ljava/lang/String;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/squareup/ui/settings/DelegateLockoutScreen;->titleText:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/settings/DelegateLockoutScreen;
    .locals 1

    .line 80
    new-instance v0, Lcom/squareup/ui/settings/DelegateLockoutScreen;

    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/DelegateLockoutScreen;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 70
    iget-object p2, p0, Lcom/squareup/ui/settings/DelegateLockoutScreen;->titleText:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .line 39
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Lcom/squareup/ui/settings/InSettingsAppletScope;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "{titleText= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/settings/DelegateLockoutScreen;->titleText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 83
    sget v0, Lcom/squareup/settingsapplet/R$layout;->delegate_lockout_view:I

    return v0
.end method
