.class public Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScope;
.super Lcom/squareup/ui/settings/InSettingsAppletScope;
.source "GiftCardsSettingsScope.java"

# interfaces
.implements Lcom/squareup/container/RegistersInScope;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScope$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScope$Component;,
        Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScope$Module;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScope;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScope;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 17
    new-instance v0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScope;

    invoke-direct {v0}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScope;-><init>()V

    sput-object v0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScope;->INSTANCE:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScope;

    .line 60
    sget-object v0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScope;->INSTANCE:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScope;

    .line 61
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScope;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 19
    invoke-direct {p0}, Lcom/squareup/ui/settings/InSettingsAppletScope;-><init>()V

    return-void
.end method


# virtual methods
.method public register(Lmortar/MortarScope;)V
    .locals 1

    .line 23
    const-class v0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScope$Component;

    .line 24
    invoke-interface {v0}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScope$Component;->scopeRunner()Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsScopeRunner;

    move-result-object v0

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    return-void
.end method
