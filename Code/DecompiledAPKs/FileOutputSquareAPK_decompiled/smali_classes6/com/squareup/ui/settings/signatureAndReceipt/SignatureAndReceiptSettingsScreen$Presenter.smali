.class public Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;
.super Lcom/squareup/ui/settings/SettingsSectionPresenter;
.source "SignatureAndReceiptSettingsScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/settings/SettingsSectionPresenter<",
        "Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;",
        ">;"
    }
.end annotation


# instance fields
.field final alwaysSkipSignatureWarningPopup:Lcom/squareup/mortar/PopupPresenter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/mortar/PopupPresenter<",
            "Lcom/squareup/register/widgets/Confirmation;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final paperSignatureSettings:Lcom/squareup/papersignature/PaperSignatureSettings;

.field private final res:Lcom/squareup/util/Res;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final sidebarRefresher:Lcom/squareup/ui/settings/SidebarRefresher;

.field final signOnPrintedReceiptWarningPopup:Lcom/squareup/mortar/PopupPresenter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/mortar/PopupPresenter<",
            "Lcom/squareup/register/widgets/Confirmation;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field final signOnPrintedReceiptWithoutPrinterPopup:Lcom/squareup/mortar/PopupPresenter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/mortar/PopupPresenter<",
            "Lcom/squareup/register/widgets/FailureAlertDialogFactory;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final skipReceiptScreenSettings:Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;

.field final skipSignatureWarningPopup:Lcom/squareup/mortar/PopupPresenter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/mortar/PopupPresenter<",
            "Lcom/squareup/register/widgets/Confirmation;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/settings/SidebarRefresher;Lcom/squareup/papersignature/PaperSignatureSettings;Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/CurrencyCode;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/server/Features;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/ui/settings/SidebarRefresher;",
            "Lcom/squareup/papersignature/PaperSignatureSettings;",
            "Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/protos/common/CurrencyCode;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/settings/server/Features;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 90
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/SettingsSectionPresenter;-><init>(Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;)V

    .line 91
    iput-object p2, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 92
    iput-object p3, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 93
    iput-object p4, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->sidebarRefresher:Lcom/squareup/ui/settings/SidebarRefresher;

    .line 94
    iput-object p5, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->paperSignatureSettings:Lcom/squareup/papersignature/PaperSignatureSettings;

    .line 95
    iput-object p6, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->skipReceiptScreenSettings:Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;

    .line 96
    iput-object p7, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 97
    iput-object p8, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    .line 98
    iput-object p9, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    .line 99
    iput-object p10, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    .line 101
    new-instance p1, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter$1;

    invoke-direct {p1, p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter$1;-><init>(Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;)V

    iput-object p1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->signOnPrintedReceiptWarningPopup:Lcom/squareup/mortar/PopupPresenter;

    .line 111
    new-instance p1, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter$2;

    invoke-direct {p1, p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter$2;-><init>(Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;)V

    iput-object p1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->signOnPrintedReceiptWithoutPrinterPopup:Lcom/squareup/mortar/PopupPresenter;

    .line 118
    new-instance p1, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter$3;

    invoke-direct {p1, p0, p9}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter$3;-><init>(Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;Lcom/squareup/analytics/Analytics;)V

    iput-object p1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->skipSignatureWarningPopup:Lcom/squareup/mortar/PopupPresenter;

    .line 137
    new-instance p1, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter$4;

    invoke-direct {p1, p0, p9}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter$4;-><init>(Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;Lcom/squareup/analytics/Analytics;)V

    iput-object p1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->alwaysSkipSignatureWarningPopup:Lcom/squareup/mortar/PopupPresenter;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;Z)V
    .locals 0

    .line 67
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->setSignOnPrintedReceipt(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;Lcom/squareup/analytics/RegisterActionName;Z)V
    .locals 0

    .line 67
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->logToggle(Lcom/squareup/analytics/RegisterActionName;Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;)Z
    .locals 0

    .line 67
    invoke-direct {p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->isAlwaysSkipSignatureEnabled()Z

    move-result p0

    return p0
.end method

.method static synthetic access$300(Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;)Ljava/lang/Object;
    .locals 0

    .line 67
    invoke-virtual {p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;)V
    .locals 0

    .line 67
    invoke-direct {p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->updateView()V

    return-void
.end method

.method static synthetic access$500(Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;)Ljava/lang/Object;
    .locals 0

    .line 67
    invoke-virtual {p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$600(Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;)Ljava/lang/Object;
    .locals 0

    .line 67
    invoke-virtual {p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method private getMoneyString(I)Ljava/lang/String;
    .locals 4

    .line 375
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->moneyFormatter:Lcom/squareup/text/Formatter;

    iget-object v1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 376
    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/settings/server/PaymentSettings;->getSkipSignatureMaxCents()J

    move-result-wide v1

    iget-object v3, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v1, v2, v3}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v1

    .line 375
    invoke-interface {v0, v1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 377
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 378
    iget-object v1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->res:Lcom/squareup/util/Res;

    invoke-interface {v1, p1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    const-string v1, "amount"

    invoke-virtual {p1, v1, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private getSignatureWarningPopupStringIds()Lcom/squareup/register/widgets/ConfirmationIds;
    .locals 5

    .line 386
    new-instance v0, Lcom/squareup/register/widgets/ConfirmationIds;

    sget v1, Lcom/squareup/settingsapplet/R$string;->paper_signature_always_skip_signature_warning_dialog_title:I

    sget v2, Lcom/squareup/settingsapplet/R$string;->paper_signature_always_skip_signature_warning_dialog_content:I

    sget v3, Lcom/squareup/common/strings/R$string;->okay:I

    sget v4, Lcom/squareup/common/strings/R$string;->cancel:I

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/register/widgets/ConfirmationIds;-><init>(IIII)V

    return-object v0
.end method

.method private isAlwaysSkipSignatureEnabled()Z
    .locals 1

    .line 382
    invoke-virtual {p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->isAlwaysSkipSignatureEnabled()Z

    move-result v0

    return v0
.end method

.method private logToggle(Lcom/squareup/analytics/RegisterActionName;Z)V
    .locals 2

    .line 350
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/analytics/event/v1/BooleanToggleEvent;

    invoke-direct {v1, p1, p2}, Lcom/squareup/analytics/event/v1/BooleanToggleEvent;-><init>(Lcom/squareup/analytics/RegisterActionName;Z)V

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method private setSignOnPrintedReceipt(Z)V
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    .line 344
    iget-object v2, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v2}, Lcom/squareup/settings/server/AccountStatusSettings;->getTipSettings()Lcom/squareup/settings/server/TipSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/settings/server/TipSettings;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    if-eqz p1, :cond_1

    .line 345
    iget-object v3, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->paperSignatureSettings:Lcom/squareup/papersignature/PaperSignatureSettings;

    invoke-virtual {v3}, Lcom/squareup/papersignature/PaperSignatureSettings;->hasReceiptPrinter()Z

    move-result v3

    if-nez v3, :cond_1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    .line 346
    :goto_1
    invoke-virtual {p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;

    invoke-virtual {v1, p1, v2, v0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->setSignOnPrintedReceipt(ZZZ)V

    return-void
.end method

.method private shouldShowSignatureWarningPopup()Z
    .locals 1

    .line 328
    invoke-virtual {p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->isSignOnPrintedReceiptChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 329
    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getTipSettings()Lcom/squareup/settings/server/TipSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/TipSettings;->isPreAuthTippingRequired()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 330
    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/PaymentSettings;->canEnableTipping()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private showSignatureWarningPopup()V
    .locals 6

    .line 334
    sget v0, Lcom/squareup/settingsapplet/R$string;->paper_signature_skip_signature_warning_dialog_content:I

    .line 335
    invoke-direct {p0, v0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->getMoneyString(I)Ljava/lang/String;

    move-result-object v0

    .line 336
    new-instance v1, Lcom/squareup/register/widgets/ConfirmationStrings;

    iget-object v2, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/settingsapplet/R$string;->paper_signature_skip_signature_warning_dialog_title:I

    .line 337
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/common/strings/R$string;->ok:I

    .line 338
    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v5, Lcom/squareup/common/strings/R$string;->cancel:I

    .line 339
    invoke-interface {v4, v5}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v0, v3, v4}, Lcom/squareup/register/widgets/ConfirmationStrings;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 340
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->skipSignatureWarningPopup:Lcom/squareup/mortar/PopupPresenter;

    invoke-virtual {v0, v1}, Lcom/squareup/mortar/PopupPresenter;->show(Landroid/os/Parcelable;)V

    return-void
.end method

.method private updateSignUnderAmount()V
    .locals 3

    .line 354
    invoke-virtual {p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;

    .line 355
    iget-object v1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getSignatureSettings()Lcom/squareup/settings/server/SignatureSettings;

    move-result-object v1

    .line 357
    invoke-direct {p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->isAlwaysSkipSignatureEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 358
    sget v1, Lcom/squareup/settingsapplet/R$string;->signature_collect_over_amount:I

    invoke-direct {p0, v1}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->getMoneyString(I)Ljava/lang/String;

    move-result-object v1

    .line 359
    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->showSkipSignUnderAmount(Ljava/lang/String;)V

    return-void

    .line 364
    :cond_0
    invoke-virtual {v1}, Lcom/squareup/settings/server/SignatureSettings;->merchantOptedInToSkipSignaturesForSmallPayments()Z

    move-result v2

    .line 363
    invoke-virtual {v0, v2}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->setSkipSignUnderAmount(Z)V

    .line 365
    invoke-virtual {v1}, Lcom/squareup/settings/server/SignatureSettings;->merchantIsAllowedToSkipSignaturesForSmallPayments()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 366
    sget v1, Lcom/squareup/settingsapplet/R$string;->sign_skip_under_amount:I

    invoke-direct {p0, v1}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->getMoneyString(I)Ljava/lang/String;

    move-result-object v1

    .line 367
    sget v2, Lcom/squareup/settingsapplet/R$string;->sign_under_amount_hint:I

    invoke-direct {p0, v2}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->getMoneyString(I)Ljava/lang/String;

    move-result-object v2

    .line 368
    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->showSkipSignUnderAmount(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 370
    :cond_1
    invoke-virtual {v0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->hideSkipSignUnderAmount()V

    :goto_0
    return-void
.end method

.method private updateView()V
    .locals 4

    .line 183
    invoke-virtual {p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;

    .line 185
    iget-object v1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->skipReceiptScreenSettings:Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;

    invoke-interface {v1}, Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;->canSkipReceiptScreen()Z

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    .line 186
    invoke-virtual {v0, v2}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->setSkipReceiptScreenEnabled(Z)V

    .line 187
    iget-object v1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->skipReceiptScreenSettings:Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;

    invoke-interface {v1}, Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;->skipReceiptScreenForFastCheckout()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->setSkipReceiptScreen(Z)V

    .line 188
    sget v1, Lcom/squareup/settingsapplet/R$string;->skip_receipt_screen_helper_text:I

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->setSkipReceiptScreenHintText(I)V

    goto :goto_0

    .line 190
    :cond_0
    invoke-virtual {v0, v3}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->setSkipReceiptScreenEnabled(Z)V

    .line 193
    :goto_0
    iget-object v1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-static {v1}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection;->shouldShowSignatureSettings(Lcom/squareup/settings/server/AccountStatusSettings;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 196
    invoke-virtual {v0, v2}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->setSignatureSettingsVisible(Z)V

    .line 198
    invoke-direct {p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->updateSignUnderAmount()V

    .line 200
    invoke-direct {p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->isAlwaysSkipSignatureEnabled()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 201
    iget-object v1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getSignatureSettings()Lcom/squareup/settings/server/SignatureSettings;

    move-result-object v1

    .line 202
    invoke-virtual {v1}, Lcom/squareup/settings/server/SignatureSettings;->merchantAlwaysSkipSignature()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 203
    invoke-virtual {v0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->setAlwaysSkipSignatureChecked()V

    goto :goto_1

    .line 204
    :cond_1
    invoke-virtual {v1}, Lcom/squareup/settings/server/SignatureSettings;->merchantOptedInToSkipSignaturesForSmallPayments()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 205
    invoke-virtual {v0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->setUnderAmountSkipSignatureChecked()V

    goto :goto_1

    .line 207
    :cond_2
    invoke-virtual {v0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->setNeverSkipSignatureChecked()V

    .line 211
    :cond_3
    :goto_1
    invoke-virtual {v0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->isAlwaysSkipSignatureChecked()Z

    move-result v1

    xor-int/lit8 v2, v1, 0x1

    .line 212
    invoke-virtual {v0, v2}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->setSignatureDetailsSettingsVisible(Z)V

    if-nez v1, :cond_7

    .line 214
    iget-object v1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->paperSignatureSettings:Lcom/squareup/papersignature/PaperSignatureSettings;

    invoke-virtual {v1}, Lcom/squareup/papersignature/PaperSignatureSettings;->isSignOnPrintedReceiptEnabled()Z

    move-result v1

    invoke-direct {p0, v1}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->setSignOnPrintedReceipt(Z)V

    .line 215
    iget-object v1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->paperSignatureSettings:Lcom/squareup/papersignature/PaperSignatureSettings;

    invoke-virtual {v1}, Lcom/squareup/papersignature/PaperSignatureSettings;->isQuickTipEnabled()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->setQuickTipEnabled(Z)V

    .line 216
    iget-object v1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->paperSignatureSettings:Lcom/squareup/papersignature/PaperSignatureSettings;

    .line 217
    invoke-virtual {v1}, Lcom/squareup/papersignature/PaperSignatureSettings;->isPrintAdditionalAuthSlipEnabled()Z

    move-result v1

    .line 216
    invoke-virtual {v0, v1}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->setAdditionalAuthSlipEnabled(Z)V

    .line 219
    iget-object v1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/settings/server/PaymentSettings;->canEnableTipping()Z

    move-result v1

    if-nez v1, :cond_4

    .line 220
    invoke-virtual {v0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->hideSignOnDeviceHint()V

    goto :goto_2

    .line 222
    :cond_4
    iget-object v1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/settings/server/UserSettings;->getCountryCode()Lcom/squareup/CountryCode;

    move-result-object v1

    sget-object v2, Lcom/squareup/CountryCode;->AU:Lcom/squareup/CountryCode;

    if-ne v1, v2, :cond_5

    .line 223
    invoke-virtual {v0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->showSignOnDeviceHintAu()V

    goto :goto_2

    .line 225
    :cond_5
    invoke-virtual {v0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->showSignOnDeviceHint()V

    goto :goto_2

    .line 230
    :cond_6
    invoke-virtual {v0, v3}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->setSignatureSettingsVisible(Z)V

    .line 231
    invoke-virtual {v0, v3}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->setSignatureDetailsSettingsVisible(Z)V

    :cond_7
    :goto_2
    return-void
.end method


# virtual methods
.method public getActionbarText()Ljava/lang/String;
    .locals 3

    .line 179
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->res:Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v2, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->skipReceiptScreenSettings:Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;

    invoke-static {v1, v2}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSection;->getSectionNameResId(Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;)I

    move-result v1

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method onAdditionalAuthSlipToggled(Z)V
    .locals 1

    .line 318
    invoke-virtual {p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->saveSettings()V

    .line 319
    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->PAPER_SIGNATURE_PRINT_ADDITIONAL_AUTH_SLIP_TOGGLED:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {p0, v0, p1}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->logToggle(Lcom/squareup/analytics/RegisterActionName;Z)V

    return-void
.end method

.method onAlwaysSkipSignature()V
    .locals 3

    .line 245
    invoke-direct {p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->shouldShowSignatureWarningPopup()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 246
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->alwaysSkipSignatureWarningPopup:Lcom/squareup/mortar/PopupPresenter;

    invoke-direct {p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->getSignatureWarningPopupStringIds()Lcom/squareup/register/widgets/ConfirmationIds;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/mortar/PopupPresenter;->show(Landroid/os/Parcelable;)V

    goto :goto_0

    .line 248
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->setAlwaysSkipSignatureChecked()V

    .line 249
    invoke-virtual {p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->saveSettings()V

    .line 250
    invoke-direct {p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->updateView()V

    .line 251
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/ui/settings/signatureAndReceipt/EnableSignatureOptionEvent;

    sget-object v2, Lcom/squareup/ui/settings/signatureAndReceipt/EnableSignatureOptionEvent$SignatureSettingState;->ALWAYS_SKIP_SIGNATURE:Lcom/squareup/ui/settings/signatureAndReceipt/EnableSignatureOptionEvent$SignatureSettingState;

    invoke-direct {v1, v2}, Lcom/squareup/ui/settings/signatureAndReceipt/EnableSignatureOptionEvent;-><init>(Lcom/squareup/ui/settings/signatureAndReceipt/EnableSignatureOptionEvent$SignatureSettingState;)V

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    :goto_0
    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 0

    .line 150
    invoke-super {p0, p1}, Lcom/squareup/ui/settings/SettingsSectionPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 151
    invoke-direct {p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->updateView()V

    return-void
.end method

.method onNeverSkipSignature()V
    .locals 3

    .line 267
    invoke-virtual {p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->setNeverSkipSignatureChecked()V

    .line 268
    invoke-virtual {p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->saveSettings()V

    .line 269
    invoke-direct {p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->updateView()V

    .line 270
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/ui/settings/signatureAndReceipt/EnableSignatureOptionEvent;

    sget-object v2, Lcom/squareup/ui/settings/signatureAndReceipt/EnableSignatureOptionEvent$SignatureSettingState;->NEVER_SKIP_SIGNATURE:Lcom/squareup/ui/settings/signatureAndReceipt/EnableSignatureOptionEvent$SignatureSettingState;

    invoke-direct {v1, v2}, Lcom/squareup/ui/settings/signatureAndReceipt/EnableSignatureOptionEvent;-><init>(Lcom/squareup/ui/settings/signatureAndReceipt/EnableSignatureOptionEvent$SignatureSettingState;)V

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method onQuickTipToggled(Z)V
    .locals 1

    .line 312
    invoke-virtual {p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->setQuickTipEnabled(Z)V

    .line 313
    invoke-virtual {p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->saveSettings()V

    .line 314
    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->PAPER_SIGNATURE_QUICK_TIP_RECEIPT_TOGGLED:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {p0, v0, p1}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->logToggle(Lcom/squareup/analytics/RegisterActionName;Z)V

    return-void
.end method

.method onSignOnPrintedReceiptToggled(Z)V
    .locals 5

    .line 274
    invoke-virtual {p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;

    if-eqz p1, :cond_1

    .line 275
    iget-object v1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->paperSignatureSettings:Lcom/squareup/papersignature/PaperSignatureSettings;

    invoke-virtual {v1}, Lcom/squareup/papersignature/PaperSignatureSettings;->hasReceiptPrinter()Z

    move-result v1

    if-nez v1, :cond_1

    .line 276
    iget-object p1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/settingsapplet/R$string;->paper_signature_sign_on_printed_receipts_without_printer_dialog_title:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->res:Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 278
    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/settings/server/PaymentSettings;->canEnableTipping()Z

    move-result v1

    if-eqz v1, :cond_0

    sget v1, Lcom/squareup/registerlib/R$string;->paper_signature_sign_and_tip_on_printed_receipts_without_printer_dialog_content:I

    goto :goto_0

    :cond_0
    sget v1, Lcom/squareup/settingsapplet/R$string;->paper_signature_sign_on_printed_receipts_without_printer_dialog_content:I

    :goto_0
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/common/strings/R$string;->ok:I

    .line 282
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 276
    invoke-static {p1, v0, v1}, Lcom/squareup/register/widgets/FailureAlertDialogFactory;->noRetry(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/register/widgets/FailureAlertDialogFactory;

    move-result-object p1

    .line 283
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->signOnPrintedReceiptWithoutPrinterPopup:Lcom/squareup/mortar/PopupPresenter;

    invoke-virtual {v0, p1}, Lcom/squareup/mortar/PopupPresenter;->show(Landroid/os/Parcelable;)V

    return-void

    :cond_1
    if-eqz p1, :cond_3

    .line 288
    invoke-virtual {v0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->isSkipSignUnderAmountChecked()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 289
    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getTipSettings()Lcom/squareup/settings/server/TipSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/TipSettings;->isPreAuthTippingRequired()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 290
    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/PaymentSettings;->canEnableTipping()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 291
    invoke-direct {p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->isAlwaysSkipSignatureEnabled()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 292
    iget-object p1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->signOnPrintedReceiptWarningPopup:Lcom/squareup/mortar/PopupPresenter;

    invoke-direct {p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->getSignatureWarningPopupStringIds()Lcom/squareup/register/widgets/ConfirmationIds;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/mortar/PopupPresenter;->show(Landroid/os/Parcelable;)V

    return-void

    .line 295
    :cond_2
    sget p1, Lcom/squareup/settingsapplet/R$string;->paper_signature_sign_on_printed_receipts_warning_dialog_content:I

    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->getMoneyString(I)Ljava/lang/String;

    move-result-object p1

    .line 297
    new-instance v0, Lcom/squareup/register/widgets/ConfirmationStrings;

    iget-object v1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/settingsapplet/R$string;->paper_signature_sign_on_printed_receipts_warning_dialog_title:I

    .line 298
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/common/strings/R$string;->ok:I

    .line 299
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v4, Lcom/squareup/common/strings/R$string;->cancel:I

    .line 300
    invoke-interface {v3, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, p1, v2, v3}, Lcom/squareup/register/widgets/ConfirmationStrings;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    iget-object p1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->signOnPrintedReceiptWarningPopup:Lcom/squareup/mortar/PopupPresenter;

    invoke-virtual {p1, v0}, Lcom/squareup/mortar/PopupPresenter;->show(Landroid/os/Parcelable;)V

    return-void

    .line 305
    :cond_3
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->setSignOnPrintedReceipt(Z)V

    .line 307
    invoke-virtual {p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->saveSettings()V

    .line 308
    sget-object v0, Lcom/squareup/analytics/RegisterActionName;->PAPER_SIGNATURE_TOGGLED:Lcom/squareup/analytics/RegisterActionName;

    invoke-direct {p0, v0, p1}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->logToggle(Lcom/squareup/analytics/RegisterActionName;Z)V

    return-void
.end method

.method onSkipReceiptScreenToggled(Z)V
    .locals 2

    .line 323
    invoke-virtual {p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->saveSettings()V

    .line 324
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/log/fastcheckout/SkipReceiptScreenToggleEvent;

    invoke-direct {v1, p1}, Lcom/squareup/log/fastcheckout/SkipReceiptScreenToggleEvent;-><init>(Z)V

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    return-void
.end method

.method onSkipSignatureUnderAmountToggled()V
    .locals 3

    .line 236
    invoke-virtual {p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->isSkipSignUnderAmountChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->shouldShowSignatureWarningPopup()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 237
    invoke-direct {p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->showSignatureWarningPopup()V

    goto :goto_0

    .line 239
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->saveSettings()V

    .line 240
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/log/fastcheckout/SkipSigToggleEvent;

    invoke-virtual {p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;

    invoke-virtual {v2}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->isSkipSignUnderAmountChecked()Z

    move-result v2

    invoke-direct {v1, v2}, Lcom/squareup/log/fastcheckout/SkipSigToggleEvent;-><init>(Z)V

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    :goto_0
    return-void
.end method

.method onUnderAmountSkipSignature()V
    .locals 3

    .line 256
    invoke-direct {p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->shouldShowSignatureWarningPopup()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 257
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->skipSignatureWarningPopup:Lcom/squareup/mortar/PopupPresenter;

    invoke-direct {p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->getSignatureWarningPopupStringIds()Lcom/squareup/register/widgets/ConfirmationIds;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/mortar/PopupPresenter;->show(Landroid/os/Parcelable;)V

    goto :goto_0

    .line 259
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->setUnderAmountSkipSignatureChecked()V

    .line 260
    invoke-virtual {p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->saveSettings()V

    .line 261
    invoke-direct {p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->updateView()V

    .line 262
    iget-object v0, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/ui/settings/signatureAndReceipt/EnableSignatureOptionEvent;

    sget-object v2, Lcom/squareup/ui/settings/signatureAndReceipt/EnableSignatureOptionEvent$SignatureSettingState;->UNDER_AMOUNT_SKIP_SIGNATURE:Lcom/squareup/ui/settings/signatureAndReceipt/EnableSignatureOptionEvent$SignatureSettingState;

    invoke-direct {v1, v2}, Lcom/squareup/ui/settings/signatureAndReceipt/EnableSignatureOptionEvent;-><init>(Lcom/squareup/ui/settings/signatureAndReceipt/EnableSignatureOptionEvent$SignatureSettingState;)V

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    :goto_0
    return-void
.end method

.method protected saveSettings()V
    .locals 8

    .line 159
    invoke-virtual {p0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;

    .line 160
    invoke-virtual {v0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->isSignOnPrintedReceiptChecked()Z

    move-result v1

    .line 161
    iget-object v2, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->paperSignatureSettings:Lcom/squareup/papersignature/PaperSignatureSettings;

    invoke-virtual {v2}, Lcom/squareup/papersignature/PaperSignatureSettings;->useDeviceProfile()Z

    move-result v2

    if-nez v2, :cond_0

    .line 162
    iget-object v2, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->paperSignatureSettings:Lcom/squareup/papersignature/PaperSignatureSettings;

    invoke-virtual {v2, v1}, Lcom/squareup/papersignature/PaperSignatureSettings;->setSignOnPrintedReceiptEnabled(Z)V

    .line 163
    iget-object v1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getSignatureSettings()Lcom/squareup/settings/server/SignatureSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/settings/server/SignatureSettings;->usePaperSignature()Z

    move-result v1

    :cond_0
    move v5, v1

    .line 166
    iget-object v2, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 167
    invoke-virtual {v0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->isAlwaysSkipSignatureChecked()Z

    move-result v3

    .line 168
    invoke-virtual {v0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->isSkipSignUnderAmountChecked()Z

    move-result v4

    .line 170
    invoke-virtual {v0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->isAdditionalAuthSlipChecked()Z

    move-result v6

    .line 171
    invoke-virtual {v0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->isQuickTipChecked()Z

    move-result v7

    .line 166
    invoke-virtual/range {v2 .. v7}, Lcom/squareup/settings/server/AccountStatusSettings;->setSignatureSettings(ZZZZZ)V

    .line 174
    iget-object v1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->sidebarRefresher:Lcom/squareup/ui/settings/SidebarRefresher;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/SidebarRefresher;->refresh()V

    .line 175
    iget-object v1, p0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen$Presenter;->skipReceiptScreenSettings:Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsView;->isSkipReceiptScreenChecked()Z

    move-result v0

    invoke-interface {v1, v0}, Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;->updateSkipReceiptScreenSettings(Z)V

    return-void
.end method

.method public screenForAssertion()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "+",
            "Lcom/squareup/ui/main/RegisterTreeKey;",
            ">;"
        }
    .end annotation

    .line 155
    const-class v0, Lcom/squareup/ui/settings/signatureAndReceipt/SignatureAndReceiptSettingsScreen;

    return-object v0
.end method
