.class public final Lcom/squareup/ui/settings/scales/ScalesWorkflowRunner_Factory;
.super Ljava/lang/Object;
.source "ScalesWorkflowRunner_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/settings/scales/ScalesWorkflowRunner;",
        ">;"
    }
.end annotation


# instance fields
.field private final containerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;"
        }
    .end annotation
.end field

.field private final viewFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/scales/ScalesWorkflowViewFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final workflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/scales/ScalesWorkflow;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/scales/ScalesWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/scales/ScalesWorkflowViewFactory;",
            ">;)V"
        }
    .end annotation

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/ui/settings/scales/ScalesWorkflowRunner_Factory;->containerProvider:Ljavax/inject/Provider;

    .line 29
    iput-object p2, p0, Lcom/squareup/ui/settings/scales/ScalesWorkflowRunner_Factory;->workflowProvider:Ljavax/inject/Provider;

    .line 30
    iput-object p3, p0, Lcom/squareup/ui/settings/scales/ScalesWorkflowRunner_Factory;->viewFactoryProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/settings/scales/ScalesWorkflowRunner_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/PosContainer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/scales/ScalesWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/scales/ScalesWorkflowViewFactory;",
            ">;)",
            "Lcom/squareup/ui/settings/scales/ScalesWorkflowRunner_Factory;"
        }
    .end annotation

    .line 41
    new-instance v0, Lcom/squareup/ui/settings/scales/ScalesWorkflowRunner_Factory;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/settings/scales/ScalesWorkflowRunner_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/ui/main/PosContainer;Lcom/squareup/scales/ScalesWorkflow;Lcom/squareup/scales/ScalesWorkflowViewFactory;)Lcom/squareup/ui/settings/scales/ScalesWorkflowRunner;
    .locals 1

    .line 46
    new-instance v0, Lcom/squareup/ui/settings/scales/ScalesWorkflowRunner;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/ui/settings/scales/ScalesWorkflowRunner;-><init>(Lcom/squareup/ui/main/PosContainer;Lcom/squareup/scales/ScalesWorkflow;Lcom/squareup/scales/ScalesWorkflowViewFactory;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/settings/scales/ScalesWorkflowRunner;
    .locals 3

    .line 35
    iget-object v0, p0, Lcom/squareup/ui/settings/scales/ScalesWorkflowRunner_Factory;->containerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/main/PosContainer;

    iget-object v1, p0, Lcom/squareup/ui/settings/scales/ScalesWorkflowRunner_Factory;->workflowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/scales/ScalesWorkflow;

    iget-object v2, p0, Lcom/squareup/ui/settings/scales/ScalesWorkflowRunner_Factory;->viewFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/scales/ScalesWorkflowViewFactory;

    invoke-static {v0, v1, v2}, Lcom/squareup/ui/settings/scales/ScalesWorkflowRunner_Factory;->newInstance(Lcom/squareup/ui/main/PosContainer;Lcom/squareup/scales/ScalesWorkflow;Lcom/squareup/scales/ScalesWorkflowViewFactory;)Lcom/squareup/ui/settings/scales/ScalesWorkflowRunner;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/ui/settings/scales/ScalesWorkflowRunner_Factory;->get()Lcom/squareup/ui/settings/scales/ScalesWorkflowRunner;

    move-result-object v0

    return-object v0
.end method
