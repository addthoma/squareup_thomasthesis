.class synthetic Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$4;
.super Ljava/lang/Object;
.source "PairingPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$squareup$dipper$events$BleErrorType:[I

.field static final synthetic $SwitchMap$com$squareup$ui$settings$paymentdevices$TutorialPopup$ButtonTap:[I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 431
    invoke-static {}, Lcom/squareup/dipper/events/BleErrorType;->values()[Lcom/squareup/dipper/events/BleErrorType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$4;->$SwitchMap$com$squareup$dipper$events$BleErrorType:[I

    const/4 v0, 0x1

    :try_start_0
    sget-object v1, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$4;->$SwitchMap$com$squareup$dipper$events$BleErrorType:[I

    sget-object v2, Lcom/squareup/dipper/events/BleErrorType;->SERVICE_VERSION_INCOMPATIBLE:Lcom/squareup/dipper/events/BleErrorType;

    invoke-virtual {v2}, Lcom/squareup/dipper/events/BleErrorType;->ordinal()I

    move-result v2

    aput v0, v1, v2
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    const/4 v1, 0x2

    :try_start_1
    sget-object v2, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$4;->$SwitchMap$com$squareup$dipper$events$BleErrorType:[I

    sget-object v3, Lcom/squareup/dipper/events/BleErrorType;->OLD_SERVICE_CACHED:Lcom/squareup/dipper/events/BleErrorType;

    invoke-virtual {v3}, Lcom/squareup/dipper/events/BleErrorType;->ordinal()I

    move-result v3

    aput v1, v2, v3
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_1
    :try_start_2
    sget-object v2, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$4;->$SwitchMap$com$squareup$dipper$events$BleErrorType:[I

    sget-object v3, Lcom/squareup/dipper/events/BleErrorType;->TOO_MANY_RECONNECT_ATTEMPTS:Lcom/squareup/dipper/events/BleErrorType;

    invoke-virtual {v3}, Lcom/squareup/dipper/events/BleErrorType;->ordinal()I

    move-result v3

    const/4 v4, 0x3

    aput v4, v2, v3
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    :try_start_3
    sget-object v2, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$4;->$SwitchMap$com$squareup$dipper$events$BleErrorType:[I

    sget-object v3, Lcom/squareup/dipper/events/BleErrorType;->UNABLE_TO_CREATE_BOND:Lcom/squareup/dipper/events/BleErrorType;

    invoke-virtual {v3}, Lcom/squareup/dipper/events/BleErrorType;->ordinal()I

    move-result v3

    const/4 v4, 0x4

    aput v4, v2, v3
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    :try_start_4
    sget-object v2, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$4;->$SwitchMap$com$squareup$dipper$events$BleErrorType:[I

    sget-object v3, Lcom/squareup/dipper/events/BleErrorType;->TIMEOUT:Lcom/squareup/dipper/events/BleErrorType;

    invoke-virtual {v3}, Lcom/squareup/dipper/events/BleErrorType;->ordinal()I

    move-result v3

    const/4 v4, 0x5

    aput v4, v2, v3
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    :catch_4
    :try_start_5
    sget-object v2, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$4;->$SwitchMap$com$squareup$dipper$events$BleErrorType:[I

    sget-object v3, Lcom/squareup/dipper/events/BleErrorType;->UNKNOWN_ERROR_TYPE:Lcom/squareup/dipper/events/BleErrorType;

    invoke-virtual {v3}, Lcom/squareup/dipper/events/BleErrorType;->ordinal()I

    move-result v3

    const/4 v4, 0x6

    aput v4, v2, v3
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_5

    .line 252
    :catch_5
    invoke-static {}, Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$ButtonTap;->values()[Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$ButtonTap;

    move-result-object v2

    array-length v2, v2

    new-array v2, v2, [I

    sput-object v2, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$4;->$SwitchMap$com$squareup$ui$settings$paymentdevices$TutorialPopup$ButtonTap:[I

    :try_start_6
    sget-object v2, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$4;->$SwitchMap$com$squareup$ui$settings$paymentdevices$TutorialPopup$ButtonTap:[I

    sget-object v3, Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$ButtonTap;->PRIMARY:Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$ButtonTap;

    invoke-virtual {v3}, Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$ButtonTap;->ordinal()I

    move-result v3

    aput v0, v2, v3
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_6

    :catch_6
    :try_start_7
    sget-object v0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingPresenter$4;->$SwitchMap$com$squareup$ui$settings$paymentdevices$TutorialPopup$ButtonTap:[I

    sget-object v2, Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$ButtonTap;->SECONDARY:Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$ButtonTap;

    invoke-virtual {v2}, Lcom/squareup/ui/settings/paymentdevices/TutorialPopup$ButtonTap;->ordinal()I

    move-result v2

    aput v1, v0, v2
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_7

    :catch_7
    return-void
.end method
