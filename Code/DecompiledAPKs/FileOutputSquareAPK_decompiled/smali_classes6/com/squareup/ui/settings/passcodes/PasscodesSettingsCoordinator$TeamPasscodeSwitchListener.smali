.class Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator$TeamPasscodeSwitchListener;
.super Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator$ConsumeSwitchCheckedChangeListener;
.source "PasscodesSettingsCoordinator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "TeamPasscodeSwitchListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;)V
    .locals 0

    .line 289
    iput-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator$TeamPasscodeSwitchListener;->this$0:Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;

    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator$ConsumeSwitchCheckedChangeListener;-><init>(Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 289
    check-cast p1, Lcom/squareup/noho/NohoCheckableRow;

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator$TeamPasscodeSwitchListener;->invoke(Lcom/squareup/noho/NohoCheckableRow;Ljava/lang/Boolean;)Lkotlin/Unit;

    move-result-object p1

    return-object p1
.end method

.method public invoke(Lcom/squareup/noho/NohoCheckableRow;Ljava/lang/Boolean;)Lkotlin/Unit;
    .locals 0

    .line 291
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator$ConsumeSwitchCheckedChangeListener;->invoke(Lcom/squareup/noho/NohoCheckableRow;Ljava/lang/Boolean;)Lkotlin/Unit;

    .line 292
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator$TeamPasscodeSwitchListener;->this$0:Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;->access$000(Lcom/squareup/ui/settings/passcodes/PasscodesSettingsCoordinator;)Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;

    move-result-object p1

    invoke-virtual {p1, p2}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->onTeamPasscodeSwitchChanged(Ljava/lang/Boolean;)V

    .line 293
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method
