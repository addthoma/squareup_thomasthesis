.class public Lcom/squareup/ui/settings/taxes/tax/TaxExemptAllRow;
.super Lcom/squareup/ui/ReorientingLinearLayout;
.source "TaxExemptAllRow.java"


# instance fields
.field private final all:Landroid/view/View;

.field private final none:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V
    .locals 2

    .line 19
    invoke-direct {p0, p1}, Lcom/squareup/ui/ReorientingLinearLayout;-><init>(Landroid/content/Context;)V

    .line 21
    invoke-virtual {p0}, Lcom/squareup/ui/settings/taxes/tax/TaxExemptAllRow;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/marin/R$dimen;->marin_gutter_half:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 22
    invoke-virtual {p0, v0, v0, v0, v0}, Lcom/squareup/ui/settings/taxes/tax/TaxExemptAllRow;->setPadding(IIII)V

    .line 23
    sget v0, Lcom/squareup/settingsapplet/R$layout;->edit_tax_tax_exempt_all_row:I

    invoke-static {p1, v0, p0}, Lcom/squareup/ui/settings/taxes/tax/TaxExemptAllRow;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    const/4 p1, 0x2

    .line 25
    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/taxes/tax/TaxExemptAllRow;->setShowDividers(I)V

    .line 26
    invoke-virtual {p0}, Lcom/squareup/ui/settings/taxes/tax/TaxExemptAllRow;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v0, Lcom/squareup/marin/R$drawable;->marin_divider_square_clear_medium:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/taxes/tax/TaxExemptAllRow;->setDividerDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 29
    sget p1, Lcom/squareup/settingsapplet/R$id;->all:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxExemptAllRow;->all:Landroid/view/View;

    .line 30
    sget p1, Lcom/squareup/settingsapplet/R$id;->none:I

    invoke-static {p0, p1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxExemptAllRow;->none:Landroid/view/View;

    .line 32
    iget-object p1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxExemptAllRow;->all:Landroid/view/View;

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 33
    iget-object p1, p0, Lcom/squareup/ui/settings/taxes/tax/TaxExemptAllRow;->none:Landroid/view/View;

    invoke-virtual {p1, p3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
