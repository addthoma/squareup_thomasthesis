.class public final Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope_Module_ProvideNewPrinterStationFactory;
.super Ljava/lang/Object;
.source "PrinterStationScope_Module_ProvideNewPrinterStationFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private final module:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$Module;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$Module;)V
    .locals 0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope_Module_ProvideNewPrinterStationFactory;->module:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$Module;

    return-void
.end method

.method public static create(Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$Module;)Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope_Module_ProvideNewPrinterStationFactory;
    .locals 1

    .line 29
    new-instance v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope_Module_ProvideNewPrinterStationFactory;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope_Module_ProvideNewPrinterStationFactory;-><init>(Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$Module;)V

    return-object v0
.end method

.method public static provideNewPrinterStation(Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$Module;)Z
    .locals 0

    .line 33
    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$Module;->provideNewPrinterStation()Z

    move-result p0

    return p0
.end method


# virtual methods
.method public get()Ljava/lang/Boolean;
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope_Module_ProvideNewPrinterStationFactory;->module:Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$Module;

    invoke-static {v0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope_Module_ProvideNewPrinterStationFactory;->provideNewPrinterStation(Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$Module;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 6
    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope_Module_ProvideNewPrinterStationFactory;->get()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
