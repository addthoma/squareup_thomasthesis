.class Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter$ItemViewHolder;
.super Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
.source "PairingAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ItemViewHolder"
.end annotation


# instance fields
.field private final itemView:Lcom/squareup/ui/account/view/SmartLineRow;


# direct methods
.method constructor <init>(Lcom/squareup/ui/account/view/SmartLineRow;)V
    .locals 0

    .line 222
    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 223
    iput-object p1, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter$ItemViewHolder;->itemView:Lcom/squareup/ui/account/view/SmartLineRow;

    return-void
.end method


# virtual methods
.method bind(Lcom/squareup/cardreader/WirelessConnection;Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;)V
    .locals 2

    .line 227
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter$ItemViewHolder;->itemView:Lcom/squareup/ui/account/view/SmartLineRow;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/account/view/SmartLineRow;->setTag(Ljava/lang/Object;)V

    .line 228
    iget-object v0, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter$ItemViewHolder;->itemView:Lcom/squareup/ui/account/view/SmartLineRow;

    .line 229
    invoke-virtual {v0}, Lcom/squareup/ui/account/view/SmartLineRow;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/cardreader/R$string;->smart_reader_contactless_and_chip_name:I

    .line 230
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 232
    invoke-virtual {p2, p1, v0}, Lcom/squareup/ui/settings/paymentdevices/pairing/CardReaderMessages;->getScanResultRowTitle(Lcom/squareup/cardreader/WirelessConnection;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 233
    iget-object p2, p0, Lcom/squareup/ui/settings/paymentdevices/pairing/PairingAdapter$ItemViewHolder;->itemView:Lcom/squareup/ui/account/view/SmartLineRow;

    invoke-virtual {p2, p1}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleText(Ljava/lang/CharSequence;)V

    return-void
.end method
