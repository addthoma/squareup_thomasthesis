.class public Lcom/squareup/ui/settings/devicename/DeviceNameView;
.super Landroid/widget/LinearLayout;
.source "DeviceNameView.java"

# interfaces
.implements Lcom/squareup/workflow/ui/HandlesBack;
.implements Lcom/squareup/ui/HasActionBar;


# instance fields
.field private deviceNameField:Lcom/squareup/ui/XableEditText;

.field presenter:Lcom/squareup/ui/settings/devicename/DeviceNameScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 29
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    invoke-virtual {p0}, Lcom/squareup/ui/settings/devicename/DeviceNameView;->isInEditMode()Z

    move-result p2

    if-eqz p2, :cond_0

    return-void

    .line 31
    :cond_0
    const-class p2, Lcom/squareup/ui/settings/devicename/DeviceNameScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/devicename/DeviceNameScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/settings/devicename/DeviceNameScreen$Component;->inject(Lcom/squareup/ui/settings/devicename/DeviceNameView;)V

    return-void
.end method


# virtual methods
.method public getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 76
    invoke-static {p0}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method getDeviceNameValue()Ljava/lang/String;
    .locals 1

    .line 72
    iget-object v0, p0, Lcom/squareup/ui/settings/devicename/DeviceNameView;->deviceNameField:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0}, Lcom/squareup/ui/XableEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$requestInitialFocus$0$DeviceNameView()V
    .locals 1

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/settings/devicename/DeviceNameView;->deviceNameField:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0}, Lcom/squareup/ui/XableEditText;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0}, Lcom/squareup/widgets/SelectableEditText;->requestFocus()Z

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/settings/devicename/DeviceNameView;->deviceNameField:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0}, Lcom/squareup/ui/XableEditText;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/util/Views;->showSoftKeyboard(Landroid/view/View;)V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .line 56
    iget-object v0, p0, Lcom/squareup/ui/settings/devicename/DeviceNameView;->presenter:Lcom/squareup/ui/settings/devicename/DeviceNameScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/devicename/DeviceNameScreen$Presenter;->onBackPressed()Z

    move-result v0

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 51
    iget-object v0, p0, Lcom/squareup/ui/settings/devicename/DeviceNameView;->presenter:Lcom/squareup/ui/settings/devicename/DeviceNameScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/devicename/DeviceNameScreen$Presenter;->dropView(Landroid/view/ViewGroup;)V

    .line 52
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 35
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 36
    invoke-virtual {p0}, Lcom/squareup/ui/settings/devicename/DeviceNameView;->isInEditMode()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 37
    :cond_0
    sget v0, Lcom/squareup/settingsapplet/R$id;->device_name_field:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/XableEditText;

    iput-object v0, p0, Lcom/squareup/ui/settings/devicename/DeviceNameView;->deviceNameField:Lcom/squareup/ui/XableEditText;

    .line 38
    iget-object v0, p0, Lcom/squareup/ui/settings/devicename/DeviceNameView;->deviceNameField:Lcom/squareup/ui/XableEditText;

    new-instance v1, Lcom/squareup/ui/settings/devicename/DeviceNameView$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/devicename/DeviceNameView$1;-><init>(Lcom/squareup/ui/settings/devicename/DeviceNameView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/XableEditText;->setOnEditorActionListener(Lcom/squareup/debounce/DebouncedOnEditorActionListener;)V

    .line 47
    iget-object v0, p0, Lcom/squareup/ui/settings/devicename/DeviceNameView;->presenter:Lcom/squareup/ui/settings/devicename/DeviceNameScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/devicename/DeviceNameScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method requestInitialFocus()V
    .locals 2

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/settings/devicename/DeviceNameView;->deviceNameField:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0}, Lcom/squareup/ui/XableEditText;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/SelectableEditText;

    new-instance v1, Lcom/squareup/ui/settings/devicename/-$$Lambda$DeviceNameView$9q5mQ0RRjuQnUZFUgAtMuowTCQA;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/devicename/-$$Lambda$DeviceNameView$9q5mQ0RRjuQnUZFUgAtMuowTCQA;-><init>(Lcom/squareup/ui/settings/devicename/DeviceNameView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method setDeviceNameValue(Ljava/lang/CharSequence;)V
    .locals 1

    .line 67
    iget-object v0, p0, Lcom/squareup/ui/settings/devicename/DeviceNameView;->deviceNameField:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/XableEditText;->setText(Ljava/lang/CharSequence;)V

    .line 68
    iget-object p1, p0, Lcom/squareup/ui/settings/devicename/DeviceNameView;->deviceNameField:Lcom/squareup/ui/XableEditText;

    invoke-virtual {p1}, Lcom/squareup/ui/XableEditText;->getEditText()Landroid/widget/EditText;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {p0}, Lcom/squareup/ui/settings/devicename/DeviceNameView;->getDeviceNameValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/widgets/SelectableEditText;->setSelection(I)V

    return-void
.end method
