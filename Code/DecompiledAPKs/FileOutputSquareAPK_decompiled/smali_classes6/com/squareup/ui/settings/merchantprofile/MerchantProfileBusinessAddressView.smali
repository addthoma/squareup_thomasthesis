.class public Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressView;
.super Landroid/widget/LinearLayout;
.source "MerchantProfileBusinessAddressView.java"

# interfaces
.implements Lcom/squareup/workflow/ui/HandlesBack;
.implements Lcom/squareup/ui/HasActionBar;


# instance fields
.field private addressLayout:Lcom/squareup/address/AddressLayout;

.field private helpText:Lcom/squareup/widgets/MessageView;

.field private mobileBusiness:Lcom/squareup/widgets/list/ToggleButtonRow;

.field presenter:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressPresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private saveButton:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 32
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    const-class p2, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressScreen$Component;->inject(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressView;)V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressView;)Lcom/squareup/widgets/list/ToggleButtonRow;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressView;->mobileBusiness:Lcom/squareup/widgets/list/ToggleButtonRow;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressView;)Lcom/squareup/address/AddressLayout;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressView;->addressLayout:Lcom/squareup/address/AddressLayout;

    return-object p0
.end method


# virtual methods
.method public getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 86
    invoke-static {p0}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method isBusinessAddressSet()Z
    .locals 1

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressView;->addressLayout:Lcom/squareup/address/AddressLayout;

    invoke-virtual {v0}, Lcom/squareup/address/AddressLayout;->isSet()Z

    move-result v0

    return v0
.end method

.method public synthetic lambda$onAttachedToWindow$0$MerchantProfileBusinessAddressView()V
    .locals 1

    .line 41
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressView;->presenter:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressPresenter;->onCloseButtonTapped()V

    return-void
.end method

.method public synthetic lambda$onAttachedToWindow$1$MerchantProfileBusinessAddressView(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 46
    invoke-virtual {p0, p2}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressView;->setMobileBusinessContent(Z)V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 4

    .line 37
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 39
    invoke-virtual {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 40
    invoke-virtual {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/squareup/settingsapplet/R$string;->merchant_profile_business_address_title:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 39
    invoke-virtual {v0, v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    .line 41
    invoke-virtual {p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/settings/merchantprofile/-$$Lambda$MerchantProfileBusinessAddressView$UhkXgOpR-V1-o5wEf6JZVdp5BqE;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/merchantprofile/-$$Lambda$MerchantProfileBusinessAddressView$UhkXgOpR-V1-o5wEf6JZVdp5BqE;-><init>(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->showUpButton(Ljava/lang/Runnable;)V

    .line 43
    sget v0, Lcom/squareup/settingsapplet/R$id;->public_profile_address_help_text:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressView;->helpText:Lcom/squareup/widgets/MessageView;

    .line 44
    sget v0, Lcom/squareup/settingsapplet/R$id;->public_profile_edit_mobile_business:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/ToggleButtonRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressView;->mobileBusiness:Lcom/squareup/widgets/list/ToggleButtonRow;

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressView;->mobileBusiness:Lcom/squareup/widgets/list/ToggleButtonRow;

    new-instance v1, Lcom/squareup/ui/settings/merchantprofile/-$$Lambda$MerchantProfileBusinessAddressView$d2M60xXJOMZXxedz5kpwF6Y5jfk;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/merchantprofile/-$$Lambda$MerchantProfileBusinessAddressView$d2M60xXJOMZXxedz5kpwF6Y5jfk;-><init>(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 47
    sget v0, Lcom/squareup/settingsapplet/R$id;->public_profile_edit_address:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/address/AddressLayout;

    iput-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressView;->addressLayout:Lcom/squareup/address/AddressLayout;

    .line 48
    sget v0, Lcom/squareup/settingsapplet/R$id;->public_profile_address_save_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressView;->saveButton:Landroid/view/View;

    .line 49
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressView;->saveButton:Landroid/view/View;

    new-instance v1, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressView$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressView$1;-><init>(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 56
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressView;->mobileBusiness:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v0}, Lcom/squareup/widgets/list/ToggleButtonRow;->isChecked()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressView;->setMobileBusinessContent(Z)V

    .line 58
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressView;->presenter:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .line 82
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressView;->presenter:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressPresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressPresenter;->onBackPressed()Z

    move-result v0

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressView;->presenter:Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressPresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressPresenter;->dropView(Ljava/lang/Object;)V

    .line 78
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method setAddress(Lcom/squareup/address/Address;)V
    .locals 2

    .line 66
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressView;->addressLayout:Lcom/squareup/address/AddressLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/squareup/address/AddressLayout;->setAddress(Lcom/squareup/address/Address;Z)V

    return-void
.end method

.method setMobileBusinessContent(Z)V
    .locals 2

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressView;->mobileBusiness:Lcom/squareup/widgets/list/ToggleButtonRow;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/list/ToggleButtonRow;->setChecked(Z)V

    .line 71
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressView;->helpText:Lcom/squareup/widgets/MessageView;

    if-eqz p1, :cond_0

    sget v1, Lcom/squareup/settingsapplet/R$string;->merchant_profile_mobile_business_help_text:I

    goto :goto_0

    :cond_0
    sget v1, Lcom/squareup/settingsapplet/R$string;->business_address_subheading:I

    :goto_0
    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(I)V

    .line 73
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileBusinessAddressView;->addressLayout:Lcom/squareup/address/AddressLayout;

    if-eqz p1, :cond_1

    const/16 p1, 0x8

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    :goto_1
    invoke-virtual {v0, p1}, Lcom/squareup/address/AddressLayout;->setVisibility(I)V

    return-void
.end method
