.class public Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "CreateOrEditTeamPasscodeCoordinator.java"


# instance fields
.field private actionBar:Lcom/squareup/noho/NohoActionBar;

.field private description:Lcom/squareup/marketfont/MarketTextView;

.field private digitGroup:Landroid/widget/LinearLayout;

.field private header:Lcom/squareup/marketfont/MarketTextView;

.field private mainScheduler:Lio/reactivex/Scheduler;

.field private pinPad:Lcom/squareup/padlock/Padlock;

.field private res:Lcom/squareup/util/Res;

.field private scopeRunner:Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;

.field private view:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/squareup/util/Res;Lio/reactivex/Scheduler;Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;)V
    .locals 0
    .param p2    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 43
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;->res:Lcom/squareup/util/Res;

    .line 45
    iput-object p2, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;->mainScheduler:Lio/reactivex/Scheduler;

    .line 46
    iput-object p3, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;->scopeRunner:Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;)Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;
    .locals 0

    .line 31
    iget-object p0, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;->scopeRunner:Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;

    return-object p0
.end method

.method private bindViews(Landroid/view/View;)V
    .locals 1

    .line 86
    sget v0, Lcom/squareup/settingsapplet/R$id;->create_passcode_action_bar:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoActionBar;

    iput-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 87
    sget v0, Lcom/squareup/settingsapplet/R$id;->create_passcode_digit_group:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;->digitGroup:Landroid/widget/LinearLayout;

    .line 88
    sget v0, Lcom/squareup/settingsapplet/R$id;->create_passcode_pin_pad:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/padlock/Padlock;

    iput-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;->pinPad:Lcom/squareup/padlock/Padlock;

    .line 89
    sget v0, Lcom/squareup/settingsapplet/R$id;->create_passcode_header:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;->header:Lcom/squareup/marketfont/MarketTextView;

    .line 90
    sget v0, Lcom/squareup/settingsapplet/R$id;->create_passcode_description:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marketfont/MarketTextView;

    iput-object p1, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;->description:Lcom/squareup/marketfont/MarketTextView;

    return-void
.end method

.method private createDigitView(I)Lcom/squareup/marketfont/MarketTextView;
    .locals 5

    .line 143
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/settingsapplet/R$dimen;->passcode_settings_create_passcode_digit_width:I

    .line 144
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getDimensionPixelSize(I)I

    move-result v0

    .line 145
    iget-object v1, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/settingsapplet/R$dimen;->passcode_settings_create_passcode_digit_text_size:I

    .line 146
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getDimensionPixelSize(I)I

    move-result v1

    .line 147
    iget-object v2, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/settingsapplet/R$dimen;->passcode_settings_create_passcode_digit_margin:I

    .line 148
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getDimensionPixelSize(I)I

    move-result v2

    .line 150
    new-instance v3, Lcom/squareup/marketfont/MarketTextView;

    iget-object v4, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;->view:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/squareup/marketfont/MarketTextView;-><init>(Landroid/content/Context;)V

    int-to-float v1, v1

    const/4 v4, 0x0

    .line 151
    invoke-virtual {v3, v4, v1}, Lcom/squareup/marketfont/MarketTextView;->setTextSize(IF)V

    .line 152
    sget v1, Lcom/squareup/settingsapplet/R$drawable;->create_passcode_digit_style:I

    invoke-virtual {v3, v1}, Lcom/squareup/marketfont/MarketTextView;->setBackgroundResource(I)V

    .line 153
    sget-object v1, Lcom/squareup/marketfont/MarketFont$Weight;->REGULAR:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-virtual {v3, v1}, Lcom/squareup/marketfont/MarketTextView;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    const/16 v1, 0x11

    .line 154
    invoke-virtual {v3, v1}, Lcom/squareup/marketfont/MarketTextView;->setGravity(I)V

    .line 156
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v4, -0x2

    invoke-direct {v1, v0, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    if-lez p1, :cond_0

    .line 159
    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout$LayoutParams;->setMarginStart(I)V

    .line 162
    :cond_0
    invoke-virtual {v3, v1}, Lcom/squareup/marketfont/MarketTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v3
.end method

.method private getActionBarConfig(Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeScreen$Data;)Lcom/squareup/noho/NohoActionBar$Config;
    .locals 5

    .line 94
    new-instance v0, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    sget-object v1, Lcom/squareup/noho/UpIcon;->X:Lcom/squareup/noho/UpIcon;

    new-instance v2, Lcom/squareup/ui/settings/passcodes/-$$Lambda$CreateOrEditTeamPasscodeCoordinator$FVUtiZaof5u25HWYPXYUptRvNPM;

    invoke-direct {v2, p0}, Lcom/squareup/ui/settings/passcodes/-$$Lambda$CreateOrEditTeamPasscodeCoordinator$FVUtiZaof5u25HWYPXYUptRvNPM;-><init>(Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;)V

    .line 95
    invoke-virtual {v0, v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    move-result-object v0

    .line 96
    iget-boolean v1, p1, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeScreen$Data;->passcodePreviouslyCreated:Z

    if-nez v1, :cond_0

    .line 97
    new-instance v1, Lcom/squareup/util/ViewString$ResourceString;

    sget v2, Lcom/squareup/settingsapplet/R$string;->passcode_settings_create_team_passcode_title:I

    invoke-direct {v1, v2}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    goto :goto_0

    .line 99
    :cond_0
    new-instance v1, Lcom/squareup/util/ViewString$ResourceString;

    sget v2, Lcom/squareup/settingsapplet/R$string;->passcode_settings_edit_team_passcode_title:I

    invoke-direct {v1, v2}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 101
    :goto_0
    sget-object v1, Lcom/squareup/noho/NohoActionButtonStyle;->PRIMARY:Lcom/squareup/noho/NohoActionButtonStyle;

    new-instance v2, Lcom/squareup/util/ViewString$ResourceString;

    sget v3, Lcom/squareup/settingsapplet/R$string;->passcode_settings_timeout_save_button:I

    invoke-direct {v2, v3}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    const/4 v3, 0x1

    new-instance v4, Lcom/squareup/ui/settings/passcodes/-$$Lambda$CreateOrEditTeamPasscodeCoordinator$Ay3GTA_dORnbXqjxZc-Jl7-xmfk;

    invoke-direct {v4, p0, p1}, Lcom/squareup/ui/settings/passcodes/-$$Lambda$CreateOrEditTeamPasscodeCoordinator$Ay3GTA_dORnbXqjxZc-Jl7-xmfk;-><init>(Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeScreen$Data;)V

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setActionButton(Lcom/squareup/noho/NohoActionButtonStyle;Lcom/squareup/resources/TextModel;ZLkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 110
    invoke-virtual {v0}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object p1

    return-object p1
.end method

.method public static synthetic lambda$J1KzatCyJAZCPNpWVAblN1YOvBk(Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeScreen$Data;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;->showScreenData(Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeScreen$Data;)V

    return-void
.end method

.method private setHeaderText(Lcom/squareup/permissions/PasscodesSettings$RequestState;Z)V
    .locals 1

    .line 168
    sget-object v0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator$2;->$SwitchMap$com$squareup$permissions$PasscodesSettings$RequestState:[I

    invoke-virtual {p1}, Lcom/squareup/permissions/PasscodesSettings$RequestState;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_3

    const/4 v0, 0x2

    if-eq p1, v0, :cond_2

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    goto :goto_1

    :cond_0
    if-eqz p2, :cond_1

    .line 178
    sget p1, Lcom/squareup/settingsapplet/R$string;->passcode_settings_enter_passcode:I

    goto :goto_0

    :cond_1
    sget p1, Lcom/squareup/settingsapplet/R$string;->passcode_settings_enter_new_passcode:I

    .line 181
    :goto_0
    iget-object p2, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;->header:Lcom/squareup/marketfont/MarketTextView;

    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;->res:Lcom/squareup/util/Res;

    invoke-interface {v0, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 182
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;->header:Lcom/squareup/marketfont/MarketTextView;

    iget-object p2, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/settingsapplet/R$color;->create_passcode_body_text:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getColor(I)I

    move-result p2

    invoke-virtual {p1, p2}, Lcom/squareup/marketfont/MarketTextView;->setTextColor(I)V

    goto :goto_1

    .line 174
    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;->header:Lcom/squareup/marketfont/MarketTextView;

    iget-object p2, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/settingsapplet/R$string;->passcode_settings_error:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 175
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;->header:Lcom/squareup/marketfont/MarketTextView;

    iget-object p2, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/settingsapplet/R$color;->create_passcode_error:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getColor(I)I

    move-result p2

    invoke-virtual {p1, p2}, Lcom/squareup/marketfont/MarketTextView;->setTextColor(I)V

    goto :goto_1

    .line 170
    :cond_3
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;->header:Lcom/squareup/marketfont/MarketTextView;

    iget-object p2, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/settingsapplet/R$string;->passcode_settings_passcode_in_use:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 171
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;->header:Lcom/squareup/marketfont/MarketTextView;

    iget-object p2, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/settingsapplet/R$color;->create_passcode_error:I

    invoke-interface {p2, v0}, Lcom/squareup/util/Res;->getColor(I)I

    move-result p2

    invoke-virtual {p1, p2}, Lcom/squareup/marketfont/MarketTextView;->setTextColor(I)V

    :goto_1
    return-void
.end method

.method private showScreenData(Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeScreen$Data;)V
    .locals 4

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;->getActionBarConfig(Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeScreen$Data;)Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    .line 61
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar;->setVisibility(I)V

    .line 63
    iget-object v0, p1, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeScreen$Data;->requestState:Lcom/squareup/permissions/PasscodesSettings$RequestState;

    iget-boolean v2, p1, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeScreen$Data;->passcodePreviouslyCreated:Z

    invoke-direct {p0, v0, v2}, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;->setHeaderText(Lcom/squareup/permissions/PasscodesSettings$RequestState;Z)V

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;->description:Lcom/squareup/marketfont/MarketTextView;

    iget-object v2, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/settingsapplet/R$string;->passcode_settings_create_team_passcode_description:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 66
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;->description:Lcom/squareup/marketfont/MarketTextView;

    iget-object v2, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/settingsapplet/R$color;->create_passcode_body_text:I

    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/squareup/marketfont/MarketTextView;->setTextColor(I)V

    .line 68
    iget-object v0, p1, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeScreen$Data;->unsavedTeamPasscode:Ljava/lang/String;

    iget v2, p1, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeScreen$Data;->expectedPasscodeLength:I

    invoke-direct {p0, v0, v2}, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;->showUnsavedPasscode(Ljava/lang/String;I)V

    .line 70
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;->pinPad:Lcom/squareup/padlock/Padlock;

    iget-object p1, p1, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeScreen$Data;->unsavedTeamPasscode:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    invoke-virtual {v0, p1}, Lcom/squareup/padlock/Padlock;->setBackspaceEnabled(Z)V

    .line 71
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;->pinPad:Lcom/squareup/padlock/Padlock;

    new-instance v0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator$1;-><init>(Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;)V

    invoke-virtual {p1, v0}, Lcom/squareup/padlock/Padlock;->setOnKeyPressListener(Lcom/squareup/padlock/Padlock$OnKeyPressListener;)V

    .line 82
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;->pinPad:Lcom/squareup/padlock/Padlock;

    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;->view:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v2, Lcom/squareup/marketfont/MarketFont$Weight;->REGULAR:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-static {v0, v2, v1, v1}, Lcom/squareup/marketfont/MarketTypeface;->getTypeface(Landroid/content/Context;Lcom/squareup/marketfont/MarketFont$Weight;ZZ)Landroid/graphics/Typeface;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/padlock/Padlock;->setTypeface(Landroid/graphics/Typeface;)V

    return-void
.end method

.method private showUnsavedPasscode(Ljava/lang/String;I)V
    .locals 4

    .line 114
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;->digitGroup:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    if-ge v0, p2, :cond_0

    :goto_0
    if-ge v0, p2, :cond_1

    .line 119
    iget-object v1, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;->digitGroup:Landroid/widget/LinearLayout;

    invoke-direct {p0, v0}, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;->createDigitView(I)Lcom/squareup/marketfont/MarketTextView;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    if-le v0, p2, :cond_1

    add-int/lit8 v1, p2, 0x1

    :goto_1
    if-gt v1, v0, :cond_1

    .line 125
    iget-object v2, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;->digitGroup:Landroid/widget/LinearLayout;

    add-int/lit8 v3, v1, -0x1

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->removeViewAt(I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_2
    if-ge v1, p2, :cond_3

    .line 132
    iget-object v2, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;->digitGroup:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/squareup/marketfont/MarketTextView;

    .line 133
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v1, v3, :cond_2

    add-int/lit8 v3, v1, 0x1

    .line 134
    invoke-virtual {p1, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    :cond_2
    const-string v3, " "

    .line 136
    invoke-virtual {v2, v3}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 139
    :cond_3
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;->digitGroup:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 1

    .line 50
    iput-object p1, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;->view:Landroid/view/View;

    .line 51
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;->bindViews(Landroid/view/View;)V

    .line 54
    new-instance v0, Lcom/squareup/ui/settings/passcodes/-$$Lambda$CreateOrEditTeamPasscodeCoordinator$rWZj_qD6hAv68b9v1arVTQvTjWg;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/passcodes/-$$Lambda$CreateOrEditTeamPasscodeCoordinator$rWZj_qD6hAv68b9v1arVTQvTjWg;-><init>(Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method public synthetic lambda$attach$0$CreateOrEditTeamPasscodeCoordinator()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;->scopeRunner:Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->createOrEditTeamPasscodeScreenData()Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;->mainScheduler:Lio/reactivex/Scheduler;

    .line 55
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/settings/passcodes/-$$Lambda$CreateOrEditTeamPasscodeCoordinator$J1KzatCyJAZCPNpWVAblN1YOvBk;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/passcodes/-$$Lambda$CreateOrEditTeamPasscodeCoordinator$J1KzatCyJAZCPNpWVAblN1YOvBk;-><init>(Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;)V

    .line 56
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$getActionBarConfig$1$CreateOrEditTeamPasscodeCoordinator()Lkotlin/Unit;
    .locals 1

    .line 95
    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;->scopeRunner:Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->onCreateOrEditTeamPasscodeExit()Lkotlin/Unit;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$getActionBarConfig$2$CreateOrEditTeamPasscodeCoordinator(Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeScreen$Data;)Lkotlin/Unit;
    .locals 2

    .line 103
    iget-object p1, p1, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeScreen$Data;->unsavedTeamPasscode:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    const/4 v0, 0x4

    if-ge p1, v0, :cond_0

    .line 104
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;->header:Lcom/squareup/marketfont/MarketTextView;

    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/settingsapplet/R$string;->passcode_settings_incomplete_passcode:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    .line 105
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;->header:Lcom/squareup/marketfont/MarketTextView;

    iget-object v0, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/settingsapplet/R$color;->create_passcode_error:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/squareup/marketfont/MarketTextView;->setTextColor(I)V

    .line 106
    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1

    .line 108
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/settings/passcodes/CreateOrEditTeamPasscodeCoordinator;->scopeRunner:Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/passcodes/PasscodesSettingsScopeRunner;->onSaveTeamPasscode()Lkotlin/Unit;

    move-result-object p1

    return-object p1
.end method
