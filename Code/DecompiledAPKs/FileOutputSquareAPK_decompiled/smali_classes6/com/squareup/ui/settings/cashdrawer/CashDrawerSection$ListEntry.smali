.class public Lcom/squareup/ui/settings/cashdrawer/CashDrawerSection$ListEntry;
.super Lcom/squareup/ui/settings/SettingsAppletConnectedDevicesListEntry;
.source "CashDrawerSection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/cashdrawer/CashDrawerSection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ListEntry"
.end annotation


# instance fields
.field private final cashDrawerTracker:Lcom/squareup/cashdrawer/CashDrawerTracker;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/settings/cashdrawer/CashDrawerSection;Lcom/squareup/util/Res;Lcom/squareup/util/Device;Lcom/squareup/cashdrawer/CashDrawerTracker;)V
    .locals 6
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 34
    sget-object v5, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;->HARDWARE:Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/settings/cashdrawer/CashDrawerSection$ListEntry;-><init>(Lcom/squareup/ui/settings/cashdrawer/CashDrawerSection;Lcom/squareup/util/Res;Lcom/squareup/util/Device;Lcom/squareup/cashdrawer/CashDrawerTracker;Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$SettingsAppletGrouping;)V

    return-void
.end method

.method protected constructor <init>(Lcom/squareup/ui/settings/cashdrawer/CashDrawerSection;Lcom/squareup/util/Res;Lcom/squareup/util/Device;Lcom/squareup/cashdrawer/CashDrawerTracker;Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$SettingsAppletGrouping;)V
    .locals 8

    .line 39
    sget v2, Lcom/squareup/ui/settings/cashdrawer/CashDrawerSection;->TITLE_ID:I

    sget v5, Lcom/squareup/settingsapplet/R$string;->peripheral_connected_one:I

    sget v6, Lcom/squareup/settingsapplet/R$string;->peripheral_connected_many:I

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v7, p5

    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/settings/SettingsAppletConnectedDevicesListEntry;-><init>(Lcom/squareup/applet/AppletSection;ILcom/squareup/util/Res;Lcom/squareup/util/Device;IILcom/squareup/ui/settings/SettingsAppletSectionsListEntry$SettingsAppletGrouping;)V

    .line 41
    iput-object p4, p0, Lcom/squareup/ui/settings/cashdrawer/CashDrawerSection$ListEntry;->cashDrawerTracker:Lcom/squareup/cashdrawer/CashDrawerTracker;

    return-void
.end method


# virtual methods
.method protected connectedCount()I
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/settings/cashdrawer/CashDrawerSection$ListEntry;->cashDrawerTracker:Lcom/squareup/cashdrawer/CashDrawerTracker;

    invoke-virtual {v0}, Lcom/squareup/cashdrawer/CashDrawerTracker;->getAvailableUsbCashDrawerCount()I

    move-result v0

    return v0
.end method
