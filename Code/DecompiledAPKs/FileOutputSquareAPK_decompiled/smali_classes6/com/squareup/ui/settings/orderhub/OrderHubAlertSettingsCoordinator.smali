.class public final Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "OrderHubAlertSettingsCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOrderHubAlertSettingsCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OrderHubAlertSettingsCoordinator.kt\ncom/squareup/ui/settings/orderhub/OrderHubAlertSettingsCoordinator\n*L\n1#1,101:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0017H\u0016J\u0010\u0010\u0018\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0017H\u0002J\u0010\u0010\u0019\u001a\u00020\u00152\u0006\u0010\u001a\u001a\u00020\u001bH\u0002R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u00130\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001c"
    }
    d2 = {
        "Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "runner",
        "Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScopeRunner;",
        "device",
        "Lcom/squareup/util/Device;",
        "(Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScopeRunner;Lcom/squareup/util/Device;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "alertsEnabledRow",
        "Lcom/squareup/noho/NohoCheckableRow;",
        "frequencyGroup",
        "Lcom/squareup/noho/NohoLinearLayout;",
        "frequencyRow1",
        "frequencyRow2",
        "frequencyRow3",
        "frequencyRow4",
        "radioGroup",
        "Lcom/squareup/noho/RadioGroup;",
        "Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "onScreenData",
        "data",
        "Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$ScreenData;",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/noho/NohoActionBar;

.field private alertsEnabledRow:Lcom/squareup/noho/NohoCheckableRow;

.field private final device:Lcom/squareup/util/Device;

.field private frequencyGroup:Lcom/squareup/noho/NohoLinearLayout;

.field private frequencyRow1:Lcom/squareup/noho/NohoCheckableRow;

.field private frequencyRow2:Lcom/squareup/noho/NohoCheckableRow;

.field private frequencyRow3:Lcom/squareup/noho/NohoCheckableRow;

.field private frequencyRow4:Lcom/squareup/noho/NohoCheckableRow;

.field private radioGroup:Lcom/squareup/noho/RadioGroup;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/noho/RadioGroup<",
            "Lcom/squareup/noho/NohoCheckableRow;",
            "Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;",
            ">;"
        }
    .end annotation
.end field

.field private final runner:Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScopeRunner;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScopeRunner;Lcom/squareup/util/Device;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "runner"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "device"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsCoordinator;->runner:Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScopeRunner;

    iput-object p2, p0, Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsCoordinator;->device:Lcom/squareup/util/Device;

    return-void
.end method

.method public static final synthetic access$getRadioGroup$p(Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsCoordinator;)Lcom/squareup/noho/RadioGroup;
    .locals 1

    .line 24
    iget-object p0, p0, Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsCoordinator;->radioGroup:Lcom/squareup/noho/RadioGroup;

    if-nez p0, :cond_0

    const-string v0, "radioGroup"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getRunner$p(Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsCoordinator;)Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScopeRunner;
    .locals 0

    .line 24
    iget-object p0, p0, Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsCoordinator;->runner:Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScopeRunner;

    return-object p0
.end method

.method public static final synthetic access$onScreenData(Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsCoordinator;Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$ScreenData;)V
    .locals 0

    .line 24
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsCoordinator;->onScreenData(Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$ScreenData;)V

    return-void
.end method

.method public static final synthetic access$setRadioGroup$p(Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsCoordinator;Lcom/squareup/noho/RadioGroup;)V
    .locals 0

    .line 24
    iput-object p1, p0, Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsCoordinator;->radioGroup:Lcom/squareup/noho/RadioGroup;

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 5

    .line 74
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "view.findViewById(com.sq\u2026s.R.id.stable_action_bar)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/noho/NohoActionBar;

    iput-object v0, p0, Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    .line 75
    sget v0, Lcom/squareup/settingsapplet/R$id;->orderhub_alert_settings_frequency_container:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "view.findViewById(R.id.o\u2026ings_frequency_container)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/noho/NohoLinearLayout;

    iput-object v0, p0, Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsCoordinator;->frequencyGroup:Lcom/squareup/noho/NohoLinearLayout;

    .line 76
    sget v0, Lcom/squareup/settingsapplet/R$id;->orderhub_alert_settings_enabled:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "view.findViewById(R.id.o\u2026b_alert_settings_enabled)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/noho/NohoCheckableRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsCoordinator;->alertsEnabledRow:Lcom/squareup/noho/NohoCheckableRow;

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsCoordinator;->alertsEnabledRow:Lcom/squareup/noho/NohoCheckableRow;

    if-nez v0, :cond_0

    const-string v1, "alertsEnabledRow"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    new-instance v1, Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsCoordinator$bindViews$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsCoordinator$bindViews$1;-><init>(Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsCoordinator;)V

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoCheckableRow;->onCheckedChange(Lkotlin/jvm/functions/Function2;)V

    .line 82
    sget v0, Lcom/squareup/settingsapplet/R$id;->orderhub_alert_settings_frequency_1:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "view.findViewById(R.id.o\u2026ert_settings_frequency_1)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/noho/NohoCheckableRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsCoordinator;->frequencyRow1:Lcom/squareup/noho/NohoCheckableRow;

    .line 83
    sget v0, Lcom/squareup/settingsapplet/R$id;->orderhub_alert_settings_frequency_2:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "view.findViewById(R.id.o\u2026ert_settings_frequency_2)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/noho/NohoCheckableRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsCoordinator;->frequencyRow2:Lcom/squareup/noho/NohoCheckableRow;

    .line 84
    sget v0, Lcom/squareup/settingsapplet/R$id;->orderhub_alert_settings_frequency_3:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const-string v1, "view.findViewById(R.id.o\u2026ert_settings_frequency_3)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/noho/NohoCheckableRow;

    iput-object v0, p0, Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsCoordinator;->frequencyRow3:Lcom/squareup/noho/NohoCheckableRow;

    .line 85
    sget v0, Lcom/squareup/settingsapplet/R$id;->orderhub_alert_settings_frequency_4:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const-string v0, "view.findViewById(R.id.o\u2026ert_settings_frequency_4)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/noho/NohoCheckableRow;

    iput-object p1, p0, Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsCoordinator;->frequencyRow4:Lcom/squareup/noho/NohoCheckableRow;

    .line 86
    new-instance p1, Lcom/squareup/noho/RadioGroup;

    .line 87
    new-instance v0, Lcom/squareup/noho/ListenerAttacher$ListenableCheckableAttacher;

    invoke-direct {v0}, Lcom/squareup/noho/ListenerAttacher$ListenableCheckableAttacher;-><init>()V

    check-cast v0, Lcom/squareup/noho/ListenerAttacher;

    const/4 v1, 0x4

    new-array v1, v1, [Lkotlin/Pair;

    const/4 v2, 0x0

    .line 88
    iget-object v3, p0, Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsCoordinator;->frequencyRow1:Lcom/squareup/noho/NohoCheckableRow;

    if-nez v3, :cond_1

    const-string v4, "frequencyRow1"

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    sget-object v4, Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;->IMMEDIATELY:Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;

    invoke-static {v3, v4}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    .line 89
    iget-object v3, p0, Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsCoordinator;->frequencyRow2:Lcom/squareup/noho/NohoCheckableRow;

    if-nez v3, :cond_2

    const-string v4, "frequencyRow2"

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    sget-object v4, Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;->MINUTES_5:Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;

    invoke-static {v3, v4}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    .line 90
    iget-object v3, p0, Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsCoordinator;->frequencyRow3:Lcom/squareup/noho/NohoCheckableRow;

    if-nez v3, :cond_3

    const-string v4, "frequencyRow3"

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    sget-object v4, Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;->MINUTES_15:Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;

    invoke-static {v3, v4}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    .line 91
    iget-object v3, p0, Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsCoordinator;->frequencyRow4:Lcom/squareup/noho/NohoCheckableRow;

    if-nez v3, :cond_4

    const-string v4, "frequencyRow4"

    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    sget-object v4, Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;->MINUTES_30:Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;

    invoke-static {v3, v4}, Lkotlin/TuplesKt;->to(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v3

    aput-object v3, v1, v2

    .line 86
    invoke-direct {p1, v0, v1}, Lcom/squareup/noho/RadioGroup;-><init>(Lcom/squareup/noho/ListenerAttacher;[Lkotlin/Pair;)V

    iput-object p1, p0, Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsCoordinator;->radioGroup:Lcom/squareup/noho/RadioGroup;

    .line 94
    iget-object p1, p0, Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsCoordinator;->radioGroup:Lcom/squareup/noho/RadioGroup;

    if-nez p1, :cond_5

    const-string v0, "radioGroup"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_5
    new-instance v0, Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsCoordinator$bindViews$2;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsCoordinator$bindViews$2;-><init>(Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsCoordinator;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-virtual {p1, v0}, Lcom/squareup/noho/RadioGroup;->onChange(Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method private final onScreenData(Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$ScreenData;)V
    .locals 3

    .line 54
    invoke-virtual {p1}, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$ScreenData;->getNotificationsEnabled$settings_applet_release()Z

    move-result v0

    .line 55
    invoke-virtual {p1}, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScope$ScreenData;->getNotificationFrequency$settings_applet_release()Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;

    move-result-object p1

    .line 56
    iget-object v1, p0, Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsCoordinator;->alertsEnabledRow:Lcom/squareup/noho/NohoCheckableRow;

    if-nez v1, :cond_0

    const-string v2, "alertsEnabledRow"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v1, v0}, Lcom/squareup/noho/NohoCheckableRow;->setChecked(Z)V

    const-string v1, "frequencyGroup"

    if-nez v0, :cond_2

    .line 58
    iget-object p1, p0, Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsCoordinator;->frequencyGroup:Lcom/squareup/noho/NohoLinearLayout;

    if-nez p1, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoLinearLayout;->setVisibility(I)V

    goto :goto_1

    .line 60
    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsCoordinator;->frequencyGroup:Lcom/squareup/noho/NohoLinearLayout;

    if-nez v0, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoLinearLayout;->setVisibility(I)V

    .line 61
    sget-object v0, Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsCoordinator$WhenMappings;->$EnumSwitchMapping$0:[I

    invoke-virtual {p1}, Lcom/squareup/orderhub/settings/OrderHubAlertsFrequency;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_7

    const/4 v1, 0x2

    if-eq p1, v1, :cond_6

    const/4 v1, 0x3

    if-eq p1, v1, :cond_5

    const/4 v1, 0x4

    if-ne p1, v1, :cond_4

    .line 65
    iget-object p1, p0, Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsCoordinator;->frequencyRow4:Lcom/squareup/noho/NohoCheckableRow;

    if-nez p1, :cond_8

    const-string v1, "frequencyRow4"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 64
    :cond_5
    iget-object p1, p0, Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsCoordinator;->frequencyRow3:Lcom/squareup/noho/NohoCheckableRow;

    if-nez p1, :cond_8

    const-string v1, "frequencyRow3"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    goto :goto_0

    .line 63
    :cond_6
    iget-object p1, p0, Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsCoordinator;->frequencyRow2:Lcom/squareup/noho/NohoCheckableRow;

    if-nez p1, :cond_8

    const-string v1, "frequencyRow2"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    goto :goto_0

    .line 62
    :cond_7
    iget-object p1, p0, Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsCoordinator;->frequencyRow1:Lcom/squareup/noho/NohoCheckableRow;

    if-nez p1, :cond_8

    const-string v1, "frequencyRow1"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 67
    :cond_8
    :goto_0
    iget-object v1, p0, Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsCoordinator;->radioGroup:Lcom/squareup/noho/RadioGroup;

    if-nez v1, :cond_9

    const-string v2, "radioGroup"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_9
    invoke-virtual {v1}, Lcom/squareup/noho/RadioGroup;->getSelected()Landroid/widget/Checkable;

    move-result-object v1

    check-cast v1, Lcom/squareup/noho/NohoCheckableRow;

    invoke-static {v1, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    xor-int/2addr v1, v0

    if-eqz v1, :cond_a

    .line 68
    invoke-virtual {p1, v0}, Lcom/squareup/noho/NohoCheckableRow;->setChecked(Z)V

    :cond_a
    :goto_1
    return-void
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 5

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsCoordinator;->bindViews(Landroid/view/View;)V

    .line 47
    iget-object v0, p0, Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsCoordinator;->actionBar:Lcom/squareup/noho/NohoActionBar;

    if-nez v0, :cond_0

    const-string v1, "actionBar"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 40
    :cond_0
    new-instance v1, Lcom/squareup/noho/NohoActionBar$Config$Builder;

    invoke-direct {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;-><init>()V

    .line 42
    new-instance v2, Lcom/squareup/util/ViewString$ResourceString;

    sget v3, Lcom/squareup/settingsapplet/R$string;->orderhub_alerts_settings_section_label:I

    invoke-direct {v2, v3}, Lcom/squareup/util/ViewString$ResourceString;-><init>(I)V

    check-cast v2, Lcom/squareup/resources/TextModel;

    invoke-virtual {v1, v2}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setTitle(Lcom/squareup/resources/TextModel;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 43
    iget-object v2, p0, Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsCoordinator;->device:Lcom/squareup/util/Device;

    invoke-interface {v2}, Lcom/squareup/util/Device;->isPhoneOrPortraitLessThan10Inches()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 44
    sget-object v2, Lcom/squareup/noho/UpIcon;->BACK_ARROW:Lcom/squareup/noho/UpIcon;

    new-instance v3, Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsCoordinator$attach$1$1;

    iget-object v4, p0, Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsCoordinator;->runner:Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScopeRunner;

    invoke-direct {v3, v4}, Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsCoordinator$attach$1$1;-><init>(Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScopeRunner;)V

    check-cast v3, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v1, v2, v3}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->setUpButton(Lcom/squareup/noho/UpIcon;Lkotlin/jvm/functions/Function0;)Lcom/squareup/noho/NohoActionBar$Config$Builder;

    .line 47
    :cond_1
    invoke-virtual {v1}, Lcom/squareup/noho/NohoActionBar$Config$Builder;->build()Lcom/squareup/noho/NohoActionBar$Config;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/noho/NohoActionBar;->setConfig(Lcom/squareup/noho/NohoActionBar$Config;)V

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsCoordinator;->runner:Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScopeRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/orderhub/OrderHubSettingsScopeRunner;->getScreenData$settings_applet_release()Lrx/Observable;

    move-result-object v0

    .line 49
    new-instance v1, Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsCoordinator$attach$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsCoordinator$attach$2;-><init>(Lcom/squareup/ui/settings/orderhub/OrderHubAlertSettingsCoordinator;)V

    check-cast v1, Lrx/functions/Action1;

    invoke-virtual {v0, v1}, Lrx/Observable;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    move-result-object v0

    const-string v1, "runner.getScreenData()\n \u2026ribe { onScreenData(it) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    invoke-static {v0, p1}, Lcom/squareup/util/SubscriptionsKt;->unsubscribeOnDetach(Lrx/Subscription;Landroid/view/View;)V

    return-void
.end method
