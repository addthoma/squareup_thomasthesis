.class public Lcom/squareup/ui/settings/taxes/tax/ConfirmDiscardTaxChangesDialogScreen;
.super Lcom/squareup/ui/settings/taxes/tax/InTaxScope;
.source "ConfirmDiscardTaxChangesDialogScreen.java"


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/ui/settings/taxes/tax/ConfirmDiscardTaxChangesDialogScreen$Factory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/taxes/tax/ConfirmDiscardTaxChangesDialogScreen$Factory;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/taxes/tax/ConfirmDiscardTaxChangesDialogScreen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 34
    sget-object v0, Lcom/squareup/ui/settings/taxes/tax/-$$Lambda$ConfirmDiscardTaxChangesDialogScreen$M3CwVgEcY-PmU8sT1BHBuXGNzzQ;->INSTANCE:Lcom/squareup/ui/settings/taxes/tax/-$$Lambda$ConfirmDiscardTaxChangesDialogScreen$M3CwVgEcY-PmU8sT1BHBuXGNzzQ;

    .line 35
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/settings/taxes/tax/ConfirmDiscardTaxChangesDialogScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 18
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/taxes/tax/InTaxScope;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/settings/taxes/tax/ConfirmDiscardTaxChangesDialogScreen;
    .locals 1

    .line 36
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object p0

    .line 37
    new-instance v0, Lcom/squareup/ui/settings/taxes/tax/ConfirmDiscardTaxChangesDialogScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/taxes/tax/ConfirmDiscardTaxChangesDialogScreen;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 31
    iget-object p2, p0, Lcom/squareup/ui/settings/taxes/tax/ConfirmDiscardTaxChangesDialogScreen;->cogsTaxId:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
