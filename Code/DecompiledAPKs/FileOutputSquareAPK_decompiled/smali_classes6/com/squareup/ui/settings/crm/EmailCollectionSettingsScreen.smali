.class public final Lcom/squareup/ui/settings/crm/EmailCollectionSettingsScreen;
.super Lcom/squareup/ui/settings/InSettingsAppletScope;
.source "EmailCollectionSettingsScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/layer/InSection;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/settings/crm/EmailCollectionSettingsScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/crm/EmailCollectionSettingsScreen$Component;,
        Lcom/squareup/ui/settings/crm/EmailCollectionSettingsScreen$Presenter;,
        Lcom/squareup/ui/settings/crm/EmailCollectionSettingsScreen$Controller;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/crm/EmailCollectionSettingsScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/settings/crm/EmailCollectionSettingsScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 25
    new-instance v0, Lcom/squareup/ui/settings/crm/EmailCollectionSettingsScreen;

    invoke-direct {v0}, Lcom/squareup/ui/settings/crm/EmailCollectionSettingsScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/settings/crm/EmailCollectionSettingsScreen;->INSTANCE:Lcom/squareup/ui/settings/crm/EmailCollectionSettingsScreen;

    .line 92
    sget-object v0, Lcom/squareup/ui/settings/crm/EmailCollectionSettingsScreen;->INSTANCE:Lcom/squareup/ui/settings/crm/EmailCollectionSettingsScreen;

    .line 93
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/settings/crm/EmailCollectionSettingsScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 27
    invoke-direct {p0}, Lcom/squareup/ui/settings/InSettingsAppletScope;-><init>()V

    return-void
.end method


# virtual methods
.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 35
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->SETTINGS_EMAIL_COLLECTION:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getSection()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 31
    const-class v0, Lcom/squareup/ui/settings/crm/EmailCollectionSection;

    return-object v0
.end method

.method public screenLayout()I
    .locals 1

    .line 39
    sget v0, Lcom/squareup/settingsapplet/R$layout;->crm_email_collection_settings_view:I

    return v0
.end method
