.class public final enum Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;
.super Ljava/lang/Enum;
.source "SettingsAppletSectionsListEntry.java"

# interfaces
.implements Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$SettingsAppletGrouping;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Grouping"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;",
        ">;",
        "Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$SettingsAppletGrouping;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;

.field public static final enum ACCOUNT:Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;

.field public static final enum CHECKOUT_OPTIONS:Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;

.field public static final enum DEVICE:Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;

.field public static final enum HARDWARE:Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;

.field public static final enum ORDERHUB:Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;

.field public static final enum SECURITY:Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;

.field public static final enum TEAM_MANAGEMENT:Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;


# instance fields
.field private final stringResId:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .line 14
    new-instance v0, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;

    sget v1, Lcom/squareup/settings/applet/R$string;->settings_group_uppercase_device:I

    const/4 v2, 0x0

    const-string v3, "DEVICE"

    invoke-direct {v0, v3, v2, v1}, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;->DEVICE:Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;

    .line 15
    new-instance v0, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;

    sget v1, Lcom/squareup/settings/applet/R$string;->settings_group_uppercase_checkout_options:I

    const/4 v3, 0x1

    const-string v4, "CHECKOUT_OPTIONS"

    invoke-direct {v0, v4, v3, v1}, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;->CHECKOUT_OPTIONS:Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;

    .line 16
    new-instance v0, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;

    sget v1, Lcom/squareup/settings/applet/R$string;->settings_group_uppercase_security:I

    const/4 v4, 0x2

    const-string v5, "SECURITY"

    invoke-direct {v0, v5, v4, v1}, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;->SECURITY:Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;

    .line 17
    new-instance v0, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;

    sget v1, Lcom/squareup/settings/applet/R$string;->settings_group_uppercase_team_management:I

    const/4 v5, 0x3

    const-string v6, "TEAM_MANAGEMENT"

    invoke-direct {v0, v6, v5, v1}, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;->TEAM_MANAGEMENT:Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;

    .line 18
    new-instance v0, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;

    sget v1, Lcom/squareup/settings/applet/R$string;->settings_group_uppercase_hardware:I

    const/4 v6, 0x4

    const-string v7, "HARDWARE"

    invoke-direct {v0, v7, v6, v1}, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;->HARDWARE:Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;

    .line 19
    new-instance v0, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;

    sget v1, Lcom/squareup/settings/applet/R$string;->settings_group_uppercase_orders:I

    const/4 v7, 0x5

    const-string v8, "ORDERHUB"

    invoke-direct {v0, v8, v7, v1}, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;->ORDERHUB:Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;

    .line 20
    new-instance v0, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;

    sget v1, Lcom/squareup/settings/applet/R$string;->settings_group_uppercase_account:I

    const/4 v8, 0x6

    const-string v9, "ACCOUNT"

    invoke-direct {v0, v9, v8, v1}, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;->ACCOUNT:Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;

    .line 13
    sget-object v1, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;->DEVICE:Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;->CHECKOUT_OPTIONS:Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;->SECURITY:Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;->TEAM_MANAGEMENT:Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;->HARDWARE:Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;->ORDERHUB:Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;->ACCOUNT:Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;

    aput-object v1, v0, v8

    sput-object v0, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;->$VALUES:[Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 24
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 25
    iput p3, p0, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;->stringResId:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;
    .locals 1

    .line 13
    const-class v0, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;
    .locals 1

    .line 13
    sget-object v0, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;->$VALUES:[Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;

    invoke-virtual {v0}, [Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;

    return-object v0
.end method


# virtual methods
.method public getStringResId()I
    .locals 1

    .line 29
    iget v0, p0, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;->stringResId:I

    return v0
.end method
