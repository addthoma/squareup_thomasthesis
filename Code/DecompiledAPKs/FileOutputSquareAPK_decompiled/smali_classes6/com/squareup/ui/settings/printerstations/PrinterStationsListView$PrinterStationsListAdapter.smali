.class Lcom/squareup/ui/settings/printerstations/PrinterStationsListView$PrinterStationsListAdapter;
.super Landroid/widget/BaseAdapter;
.source "PrinterStationsListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/printerstations/PrinterStationsListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PrinterStationsListAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/printerstations/PrinterStationsListView;


# direct methods
.method private constructor <init>(Lcom/squareup/ui/settings/printerstations/PrinterStationsListView;)V
    .locals 0

    .line 71
    iput-object p1, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListView$PrinterStationsListAdapter;->this$0:Lcom/squareup/ui/settings/printerstations/PrinterStationsListView;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/ui/settings/printerstations/PrinterStationsListView;Lcom/squareup/ui/settings/printerstations/PrinterStationsListView$1;)V
    .locals 0

    .line 71
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/printerstations/PrinterStationsListView$PrinterStationsListAdapter;-><init>(Lcom/squareup/ui/settings/printerstations/PrinterStationsListView;)V

    return-void
.end method

.method private buildAndBindButtonRow(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    if-nez p1, :cond_0

    .line 116
    sget p1, Lcom/squareup/settingsapplet/R$layout;->printer_station_create_station_row:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    .line 117
    sget p2, Lcom/squareup/settingsapplet/R$id;->new_station:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/Button;

    .line 118
    new-instance v0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListView$PrinterStationsListAdapter$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/printerstations/PrinterStationsListView$PrinterStationsListAdapter$1;-><init>(Lcom/squareup/ui/settings/printerstations/PrinterStationsListView$PrinterStationsListAdapter;)V

    invoke-virtual {p2, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-object p1
.end method

.method private buildAndBindPrinterStationRow(Lcom/squareup/ui/account/view/SmartLineRow;ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    const/4 v0, 0x1

    if-nez p1, :cond_0

    .line 140
    invoke-static {p3}, Lcom/squareup/ui/account/view/SmartLineRow;->inflateForListView(Landroid/view/ViewGroup;)Lcom/squareup/ui/account/view/SmartLineRow;

    move-result-object p1

    .line 141
    sget p3, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    invoke-virtual {p1, p3}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueColor(I)V

    .line 142
    invoke-virtual {p1, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueVisible(Z)V

    .line 145
    :cond_0
    iget-object p3, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListView$PrinterStationsListAdapter;->this$0:Lcom/squareup/ui/settings/printerstations/PrinterStationsListView;

    iget-object p3, p3, Lcom/squareup/ui/settings/printerstations/PrinterStationsListView;->presenter:Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;

    invoke-virtual {p3, p2}, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->getPrinterStationTitleText(I)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p1, p3}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleText(Ljava/lang/CharSequence;)V

    .line 146
    iget-object p3, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListView$PrinterStationsListAdapter;->this$0:Lcom/squareup/ui/settings/printerstations/PrinterStationsListView;

    iget-object p3, p3, Lcom/squareup/ui/settings/printerstations/PrinterStationsListView;->presenter:Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;

    invoke-virtual {p3, p2}, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->getPrinterStationValueText(I)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p1, p3}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueText(Ljava/lang/CharSequence;)V

    .line 148
    iget-object p3, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListView$PrinterStationsListAdapter;->this$0:Lcom/squareup/ui/settings/printerstations/PrinterStationsListView;

    iget-object p3, p3, Lcom/squareup/ui/settings/printerstations/PrinterStationsListView;->device:Lcom/squareup/util/Device;

    invoke-interface {p3}, Lcom/squareup/util/Device;->isPhone()Z

    move-result p3

    if-eqz p3, :cond_1

    .line 149
    iget-object p3, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListView$PrinterStationsListAdapter;->this$0:Lcom/squareup/ui/settings/printerstations/PrinterStationsListView;

    iget-object p3, p3, Lcom/squareup/ui/settings/printerstations/PrinterStationsListView;->presenter:Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;

    invoke-virtual {p3, p2}, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->getPrinterStationSubtitleText(I)Ljava/lang/String;

    move-result-object p2

    .line 150
    invoke-virtual {p1, p2}, Lcom/squareup/ui/account/view/SmartLineRow;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 151
    invoke-static {p2}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p2

    xor-int/2addr p2, v0

    invoke-virtual {p1, p2}, Lcom/squareup/ui/account/view/SmartLineRow;->setSubtitleVisible(Z)V

    :cond_1
    return-object p1
.end method

.method private buildHeaderRow(Lcom/squareup/widgets/list/SectionHeaderRow;I)Landroid/view/View;
    .locals 2

    if-nez p1, :cond_0

    .line 130
    new-instance p1, Lcom/squareup/widgets/list/SectionHeaderRow;

    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListView$PrinterStationsListAdapter;->this$0:Lcom/squareup/ui/settings/printerstations/PrinterStationsListView;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/printerstations/PrinterStationsListView;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p1, v0, v1}, Lcom/squareup/widgets/list/SectionHeaderRow;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    .line 132
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListView$PrinterStationsListAdapter;->this$0:Lcom/squareup/ui/settings/printerstations/PrinterStationsListView;

    iget-object v0, v0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListView;->presenter:Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;

    invoke-virtual {v0, p2}, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->getHeaderText(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/widgets/list/SectionHeaderRow;->setText(Ljava/lang/CharSequence;)V

    return-object p1
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListView$PrinterStationsListAdapter;->this$0:Lcom/squareup/ui/settings/printerstations/PrinterStationsListView;

    iget-object v0, v0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListView;->presenter:Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->getViewCount()I

    move-result v0

    return v0
.end method

.method public getItem(I)Lcom/squareup/print/PrinterStation;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 0

    .line 71
    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/printerstations/PrinterStationsListView$PrinterStationsListAdapter;->getItem(I)Lcom/squareup/print/PrinterStation;

    move-result-object p1

    return-object p1
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListView$PrinterStationsListAdapter;->this$0:Lcom/squareup/ui/settings/printerstations/PrinterStationsListView;

    iget-object v0, v0, Lcom/squareup/ui/settings/printerstations/PrinterStationsListView;->presenter:Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/settings/printerstations/PrinterStationsListScreen$Presenter;->getItemViewType(I)I

    move-result p1

    return p1
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .line 99
    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/printerstations/PrinterStationsListView$PrinterStationsListAdapter;->getItemViewType(I)I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 106
    check-cast p2, Lcom/squareup/ui/account/view/SmartLineRow;

    invoke-direct {p0, p2, p1, p3}, Lcom/squareup/ui/settings/printerstations/PrinterStationsListView$PrinterStationsListAdapter;->buildAndBindPrinterStationRow(Lcom/squareup/ui/account/view/SmartLineRow;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    return-object p1

    .line 108
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "Unknown RowType: "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 104
    :cond_1
    check-cast p2, Lcom/squareup/widgets/list/SectionHeaderRow;

    invoke-direct {p0, p2, p1}, Lcom/squareup/ui/settings/printerstations/PrinterStationsListView$PrinterStationsListAdapter;->buildHeaderRow(Lcom/squareup/widgets/list/SectionHeaderRow;I)Landroid/view/View;

    move-result-object p1

    return-object p1

    .line 102
    :cond_2
    invoke-direct {p0, p2, p3}, Lcom/squareup/ui/settings/printerstations/PrinterStationsListView$PrinterStationsListAdapter;->buildAndBindButtonRow(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public getViewTypeCount()I
    .locals 1

    const/4 v0, 0x3

    return v0
.end method

.method public isEnabled(I)Z
    .locals 1

    .line 82
    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/printerstations/PrinterStationsListView$PrinterStationsListAdapter;->getItemViewType(I)I

    move-result p1

    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method
