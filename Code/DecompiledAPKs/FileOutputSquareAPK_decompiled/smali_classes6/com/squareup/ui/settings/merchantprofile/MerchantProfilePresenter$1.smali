.class Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter$1;
.super Lcom/squareup/mortar/PopupPresenter;
.source "MerchantProfilePresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/mortar/PopupPresenter<",
        "Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Params;",
        "Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Choice;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;


# direct methods
.method constructor <init>(Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;)V
    .locals 0

    .line 82
    iput-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter$1;->this$0:Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;

    invoke-direct {p0}, Lcom/squareup/mortar/PopupPresenter;-><init>()V

    return-void
.end method


# virtual methods
.method protected onPopupResult(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Choice;)V
    .locals 1

    .line 84
    sget-object v0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter$2;->$SwitchMap$com$squareup$ui$settings$merchantprofile$MerchantProfileEditPhotoPopup$Choice:[I

    invoke-virtual {p1}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Choice;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_4

    const/4 v0, 0x2

    if-eq p1, v0, :cond_3

    const/4 v0, 0x3

    if-eq p1, v0, :cond_1

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    goto :goto_0

    .line 104
    :cond_0
    new-instance p1, Ljava/lang/AssertionError;

    const-string v0, "Unexpected choice for editing item."

    invoke-direct {p1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw p1

    .line 92
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter$1;->this$0:Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;

    invoke-static {p1}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->access$100(Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;)Z

    move-result p1

    if-nez p1, :cond_2

    .line 93
    iget-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter$1;->this$0:Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;

    invoke-static {p1}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->access$200(Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;)V

    .line 94
    iget-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter$1;->this$0:Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;

    invoke-static {p1}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->access$300(Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->clearMerchantLogo()V

    goto :goto_0

    .line 96
    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter$1;->this$0:Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;

    invoke-static {p1}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->access$400(Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;)V

    .line 97
    iget-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter$1;->this$0:Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;

    invoke-static {p1}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->access$500(Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileView;->clearFeaturedImage(Z)V

    goto :goto_0

    .line 89
    :cond_3
    iget-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter$1;->this$0:Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;

    invoke-static {p1}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->access$000(Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;)Lcom/squareup/camerahelper/CameraHelper;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/camerahelper/CameraHelper;->startCamera()V

    goto :goto_0

    .line 86
    :cond_4
    iget-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter$1;->this$0:Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;

    invoke-static {p1}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;->access$000(Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter;)Lcom/squareup/camerahelper/CameraHelper;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/camerahelper/CameraHelper;->startGallery()V

    :goto_0
    return-void
.end method

.method protected bridge synthetic onPopupResult(Ljava/lang/Object;)V
    .locals 0

    .line 82
    check-cast p1, Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Choice;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/settings/merchantprofile/MerchantProfilePresenter$1;->onPopupResult(Lcom/squareup/ui/settings/merchantprofile/MerchantProfileEditPhotoPopup$Choice;)V

    return-void
.end method
