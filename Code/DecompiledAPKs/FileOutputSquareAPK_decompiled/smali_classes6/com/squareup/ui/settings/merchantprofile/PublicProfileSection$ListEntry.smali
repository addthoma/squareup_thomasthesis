.class public Lcom/squareup/ui/settings/merchantprofile/PublicProfileSection$ListEntry;
.super Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;
.source "PublicProfileSection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/merchantprofile/PublicProfileSection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ListEntry"
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private badgeTitle:Ljava/lang/String;

.field private final monitor:Lcom/squareup/requestingbusinessaddress/RequestBusinessAddressMonitor;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/settings/merchantprofile/PublicProfileSection;Lcom/squareup/util/Res;Lcom/squareup/util/Device;Lcom/squareup/requestingbusinessaddress/RequestBusinessAddressMonitor;Lcom/squareup/analytics/Analytics;)V
    .locals 6
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 49
    sget-object v2, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;->ACCOUNT:Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$Grouping;

    sget v3, Lcom/squareup/ui/settings/merchantprofile/PublicProfileSection;->TITLE_ID:I

    move-object v0, p0

    move-object v1, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry;-><init>(Lcom/squareup/applet/AppletSection;Lcom/squareup/ui/settings/SettingsAppletSectionsListEntry$SettingsAppletGrouping;ILcom/squareup/util/Res;Lcom/squareup/util/Device;)V

    const-string p1, ""

    .line 45
    iput-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/PublicProfileSection$ListEntry;->badgeTitle:Ljava/lang/String;

    .line 50
    iput-object p4, p0, Lcom/squareup/ui/settings/merchantprofile/PublicProfileSection$ListEntry;->monitor:Lcom/squareup/requestingbusinessaddress/RequestBusinessAddressMonitor;

    .line 51
    iput-object p5, p0, Lcom/squareup/ui/settings/merchantprofile/PublicProfileSection$ListEntry;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method


# virtual methods
.method public getValueText()Ljava/lang/String;
    .locals 1

    .line 55
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/PublicProfileSection$ListEntry;->badgeTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getValueTextObservable()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/settings/merchantprofile/PublicProfileSection$ListEntry;->monitor:Lcom/squareup/requestingbusinessaddress/RequestBusinessAddressMonitor;

    invoke-interface {v0}, Lcom/squareup/requestingbusinessaddress/RequestBusinessAddressMonitor;->addressRequiresValidation()Lio/reactivex/Observable;

    move-result-object v0

    .line 60
    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/settings/merchantprofile/-$$Lambda$PublicProfileSection$ListEntry$z6tSoFEO0I1e41YX9jbw5dTkKl8;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/merchantprofile/-$$Lambda$PublicProfileSection$ListEntry$z6tSoFEO0I1e41YX9jbw5dTkKl8;-><init>(Lcom/squareup/ui/settings/merchantprofile/PublicProfileSection$ListEntry;)V

    .line 61
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public hasBadgeValue()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public synthetic lambda$getValueTextObservable$0$PublicProfileSection$ListEntry(Ljava/lang/Boolean;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 62
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 63
    iget-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/PublicProfileSection$ListEntry;->analytics:Lcom/squareup/analytics/Analytics;

    invoke-static {p1}, Lcom/squareup/ui/settings/merchantprofile/SettingsBusinessAddressAnalytics;->logVerifyBadgeSeen(Lcom/squareup/analytics/Analytics;)V

    .line 64
    iget-object p1, p0, Lcom/squareup/ui/settings/merchantprofile/PublicProfileSection$ListEntry;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/settingsapplet/R$string;->merchant_profile_row_value:I

    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_0
    const-string p1, ""

    return-object p1
.end method
