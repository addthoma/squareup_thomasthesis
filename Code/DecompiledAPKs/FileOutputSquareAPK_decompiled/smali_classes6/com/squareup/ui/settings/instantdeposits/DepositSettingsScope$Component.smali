.class public interface abstract Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScope$Component;
.super Ljava/lang/Object;
.source "DepositSettingsScope.kt"

# interfaces
.implements Lcom/squareup/debitcard/LinkDebitCardScope$ParentComponent;
.implements Lcom/squareup/debitcard/VerifyCardChangeScope$ParentComponent;


# annotations
.annotation runtime Lcom/squareup/dagger/SingleIn;
    value = Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScope;
.end annotation

.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScope$Module;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008g\u0018\u00002\u00020\u00012\u00020\u0002J\u0008\u0010\u0003\u001a\u00020\u0004H&J\u0008\u0010\u0005\u001a\u00020\u0006H&J\u0008\u0010\u0007\u001a\u00020\u0008H&\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScope$Component;",
        "Lcom/squareup/debitcard/LinkDebitCardScope$ParentComponent;",
        "Lcom/squareup/debitcard/VerifyCardChangeScope$ParentComponent;",
        "depositScheduleCoordinator",
        "Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;",
        "instantDepositsCoordinator",
        "Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;",
        "scopeRunner",
        "Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract depositScheduleCoordinator()Lcom/squareup/ui/settings/instantdeposits/DepositScheduleCoordinator;
.end method

.method public abstract instantDepositsCoordinator()Lcom/squareup/ui/settings/instantdeposits/InstantDepositsCoordinator;
.end method

.method public abstract scopeRunner()Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScopeRunner;
.end method
