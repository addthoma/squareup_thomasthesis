.class public final Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$MinMaxTextWatcher;
.super Lcom/squareup/text/EmptyTextWatcher;
.source "GiftCardsSettingsCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "MinMaxTextWatcher"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nGiftCardsSettingsCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 GiftCardsSettingsCoordinator.kt\ncom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$MinMaxTextWatcher\n*L\n1#1,260:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0008\u0086\u0004\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0007J\u0010\u0010\u0011\u001a\u00020\u000c2\u0006\u0010\u0012\u001a\u00020\u0013H\u0016J2\u0010\u0014\u001a\u00020\u000c2\u0006\u0010\u0015\u001a\u00020\u00052\u0006\u0010\u0016\u001a\u00020\u00052\u0006\u0010\u0017\u001a\u00020\u00052\u0012\u0010\n\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u000c0\u000bR\u000e\u0010\u0008\u001a\u00020\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\n\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u000c0\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$MinMaxTextWatcher;",
        "Lcom/squareup/text/EmptyTextWatcher;",
        "view",
        "Lcom/squareup/noho/NohoEditText;",
        "initialMin",
        "Lcom/squareup/protos/common/Money;",
        "initialMax",
        "(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;Lcom/squareup/noho/NohoEditText;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V",
        "max",
        "min",
        "onNewValidValue",
        "Lkotlin/Function1;",
        "",
        "paused",
        "",
        "scrubber",
        "Lcom/squareup/money/MaxMoneyScrubber;",
        "afterTextChanged",
        "s",
        "Landroid/text/Editable;",
        "update",
        "currentValue",
        "newMin",
        "newMax",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private max:Lcom/squareup/protos/common/Money;

.field private min:Lcom/squareup/protos/common/Money;

.field private onNewValidValue:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/protos/common/Money;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private paused:Z

.field private final scrubber:Lcom/squareup/money/MaxMoneyScrubber;

.field final synthetic this$0:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;

.field private final view:Lcom/squareup/noho/NohoEditText;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;Lcom/squareup/noho/NohoEditText;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/noho/NohoEditText;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            ")V"
        }
    .end annotation

    const-string v0, "view"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "initialMin"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "initialMax"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 209
    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$MinMaxTextWatcher;->this$0:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;

    .line 213
    invoke-direct {p0}, Lcom/squareup/text/EmptyTextWatcher;-><init>()V

    iput-object p2, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$MinMaxTextWatcher;->view:Lcom/squareup/noho/NohoEditText;

    .line 215
    new-instance p2, Lcom/squareup/money/MaxMoneyScrubber;

    invoke-static {p1}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->access$getPriceLocaleHelper$p(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;)Lcom/squareup/money/PriceLocaleHelper;

    move-result-object v0

    check-cast v0, Lcom/squareup/money/MoneyExtractor;

    invoke-static {p1}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->access$getMoneyFormatter$p(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;)Lcom/squareup/text/Formatter;

    move-result-object v1

    invoke-direct {p2, v0, v1, p4}, Lcom/squareup/money/MaxMoneyScrubber;-><init>(Lcom/squareup/money/MoneyExtractor;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/Money;)V

    iput-object p2, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$MinMaxTextWatcher;->scrubber:Lcom/squareup/money/MaxMoneyScrubber;

    .line 216
    sget-object p2, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$MinMaxTextWatcher$onNewValidValue$1;->INSTANCE:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$MinMaxTextWatcher$onNewValidValue$1;

    check-cast p2, Lkotlin/jvm/functions/Function1;

    iput-object p2, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$MinMaxTextWatcher;->onNewValidValue:Lkotlin/jvm/functions/Function1;

    .line 217
    iput-object p3, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$MinMaxTextWatcher;->min:Lcom/squareup/protos/common/Money;

    .line 218
    iput-object p4, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$MinMaxTextWatcher;->max:Lcom/squareup/protos/common/Money;

    .line 222
    invoke-static {p1}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->access$getPriceLocaleHelper$p(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;)Lcom/squareup/money/PriceLocaleHelper;

    move-result-object p2

    iget-object p3, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$MinMaxTextWatcher;->view:Lcom/squareup/noho/NohoEditText;

    check-cast p3, Lcom/squareup/text/HasSelectableText;

    invoke-virtual {p2, p3}, Lcom/squareup/money/PriceLocaleHelper;->configure(Lcom/squareup/text/HasSelectableText;)Lcom/squareup/text/ScrubbingTextWatcher;

    move-result-object p2

    iget-object p3, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$MinMaxTextWatcher;->scrubber:Lcom/squareup/money/MaxMoneyScrubber;

    check-cast p3, Lcom/squareup/text/Scrubber;

    invoke-virtual {p2, p3}, Lcom/squareup/text/ScrubbingTextWatcher;->addScrubber(Lcom/squareup/text/Scrubber;)V

    .line 223
    invoke-static {p1}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->access$getPriceLocaleHelper$p(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;)Lcom/squareup/money/PriceLocaleHelper;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$MinMaxTextWatcher;->view:Lcom/squareup/noho/NohoEditText;

    check-cast p2, Landroid/widget/TextView;

    invoke-virtual {p1, p2}, Lcom/squareup/money/PriceLocaleHelper;->setHintToZeroMoney(Landroid/widget/TextView;)V

    .line 224
    iget-object p1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$MinMaxTextWatcher;->view:Lcom/squareup/noho/NohoEditText;

    move-object p2, p0

    check-cast p2, Landroid/text/TextWatcher;

    invoke-virtual {p1, p2}, Lcom/squareup/noho/NohoEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 3

    const-string v0, "s"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 250
    iget-boolean v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$MinMaxTextWatcher;->paused:Z

    if-eqz v0, :cond_0

    return-void

    .line 251
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$MinMaxTextWatcher;->this$0:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->access$getPriceLocaleHelper$p(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;)Lcom/squareup/money/PriceLocaleHelper;

    move-result-object v0

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {v0, p1}, Lcom/squareup/money/PriceLocaleHelper;->extractMoney(Ljava/lang/CharSequence;)Lcom/squareup/protos/common/Money;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 253
    iget-object v1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$MinMaxTextWatcher;->this$0:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;

    invoke-static {v1}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->access$getPriceLocaleHelper$p(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;)Lcom/squareup/money/PriceLocaleHelper;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/squareup/money/PriceLocaleHelper;->extractMoney(Ljava/lang/CharSequence;)Lcom/squareup/protos/common/Money;

    move-result-object p1

    iget-object v1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$MinMaxTextWatcher;->min:Lcom/squareup/protos/common/Money;

    iget-object v2, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$MinMaxTextWatcher;->max:Lcom/squareup/protos/common/Money;

    invoke-static {p1, v1, v2}, Lcom/squareup/money/MoneyMath;->inRange(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 254
    iget-object p1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$MinMaxTextWatcher;->onNewValidValue:Lkotlin/jvm/functions/Function1;

    const-string v1, "it"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-void
.end method

.method public final update(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lkotlin/jvm/functions/Function1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Lcom/squareup/protos/common/Money;",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Lcom/squareup/protos/common/Money;",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "currentValue"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "newMin"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "newMax"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onNewValidValue"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x1

    .line 233
    iput-boolean v0, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$MinMaxTextWatcher;->paused:Z

    .line 235
    iput-object p2, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$MinMaxTextWatcher;->min:Lcom/squareup/protos/common/Money;

    .line 236
    iput-object p3, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$MinMaxTextWatcher;->max:Lcom/squareup/protos/common/Money;

    .line 237
    iget-object p2, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$MinMaxTextWatcher;->scrubber:Lcom/squareup/money/MaxMoneyScrubber;

    invoke-virtual {p2, p3}, Lcom/squareup/money/MaxMoneyScrubber;->setMax(Lcom/squareup/protos/common/Money;)V

    .line 240
    iget-object p2, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$MinMaxTextWatcher;->this$0:Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;

    invoke-static {p2}, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;->access$getMoneyFormatter$p(Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator;)Lcom/squareup/text/Formatter;

    move-result-object p2

    invoke-interface {p2, p1}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    .line 241
    iget-object p2, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$MinMaxTextWatcher;->view:Lcom/squareup/noho/NohoEditText;

    invoke-virtual {p2}, Lcom/squareup/noho/NohoEditText;->getText()Landroid/text/Editable;

    move-result-object p2

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p2

    xor-int/2addr p2, v0

    if-eqz p2, :cond_0

    .line 242
    iget-object p2, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$MinMaxTextWatcher;->view:Lcom/squareup/noho/NohoEditText;

    check-cast p1, Ljava/lang/CharSequence;

    invoke-virtual {p2, p1}, Lcom/squareup/noho/NohoEditText;->setText(Ljava/lang/CharSequence;)V

    .line 244
    :cond_0
    iput-object p4, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$MinMaxTextWatcher;->onNewValidValue:Lkotlin/jvm/functions/Function1;

    const/4 p1, 0x0

    .line 246
    iput-boolean p1, p0, Lcom/squareup/ui/settings/giftcards/GiftCardsSettingsCoordinator$MinMaxTextWatcher;->paused:Z

    return-void
.end method
