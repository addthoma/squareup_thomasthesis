.class public abstract Lcom/squareup/ui/settings/paymentdevices/pairing/R12PairingScreenModule;
.super Ljava/lang/Object;
.source "R12PairingScreenModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideReaderType()Lcom/squareup/protos/client/bills/CardData$ReaderType;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 23
    sget-object v0, Lcom/squareup/protos/client/bills/CardData$ReaderType;->R12:Lcom/squareup/protos/client/bills/CardData$ReaderType;

    return-object v0
.end method

.method static provideWaitForAdditionalReaders()Z
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    const/4 v0, 0x1

    return v0
.end method


# virtual methods
.method abstract provideBleScanner(Lcom/squareup/cardreader/ble/BleScanner;)Lcom/squareup/cardreader/WirelessSearcher;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
