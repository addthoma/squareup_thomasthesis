.class public final Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen_Presenter_Factory;
.super Ljava/lang/Object;
.source "PrinterStationDetailScreen_Presenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final accountStatusSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final appNameFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/AppNameFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final cogsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;"
        }
    .end annotation
.end field

.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final errorsBarPresenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ErrorsBarPresenter;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final hardwarePrinterTrackerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/HardwarePrinterTracker;",
            ">;"
        }
    .end annotation
.end field

.field private final isNewPrinterStationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final openTicketsSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final orderTicketPrintingSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/OrderTicketPrintingSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final paperSignatureSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/PaperSignatureSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final printSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrintSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final printerRoleSupportCheckerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/printerstations/station/PrinterRoleSupportChecker;",
            ">;"
        }
    .end annotation
.end field

.field private final printerStationStateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;",
            ">;"
        }
    .end annotation
.end field

.field private final printerStationsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final runnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final scopeRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final selectedPrinterStationProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStation;",
            ">;"
        }
    .end annotation
.end field

.field private final testPrintProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/printerstations/station/TestPrint;",
            ">;"
        }
    .end annotation
.end field

.field private final ticketAutoNumberingEnabledProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ErrorsBarPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStation;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/HardwarePrinterTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/PaperSignatureSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/printerstations/station/PrinterRoleSupportChecker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/printerstations/station/TestPrint;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrintSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/AppNameFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/OrderTicketPrintingSettings;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 96
    iput-object v1, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen_Presenter_Factory;->errorsBarPresenterProvider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 97
    iput-object v1, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen_Presenter_Factory;->selectedPrinterStationProvider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 98
    iput-object v1, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen_Presenter_Factory;->hardwarePrinterTrackerProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 99
    iput-object v1, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen_Presenter_Factory;->printerStationsProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 100
    iput-object v1, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen_Presenter_Factory;->scopeRunnerProvider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 101
    iput-object v1, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 102
    iput-object v1, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen_Presenter_Factory;->cogsProvider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 103
    iput-object v1, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen_Presenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 104
    iput-object v1, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen_Presenter_Factory;->isNewPrinterStationProvider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 105
    iput-object v1, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen_Presenter_Factory;->ticketAutoNumberingEnabledProvider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 106
    iput-object v1, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen_Presenter_Factory;->printerStationStateProvider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 107
    iput-object v1, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen_Presenter_Factory;->paperSignatureSettingsProvider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 108
    iput-object v1, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen_Presenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 109
    iput-object v1, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen_Presenter_Factory;->printerRoleSupportCheckerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 110
    iput-object v1, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen_Presenter_Factory;->deviceProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 111
    iput-object v1, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen_Presenter_Factory;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p17

    .line 112
    iput-object v1, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen_Presenter_Factory;->runnerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p18

    .line 113
    iput-object v1, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen_Presenter_Factory;->testPrintProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p19

    .line 114
    iput-object v1, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen_Presenter_Factory;->printSettingsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p20

    .line 115
    iput-object v1, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen_Presenter_Factory;->appNameFormatterProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p21

    .line 116
    iput-object v1, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen_Presenter_Factory;->openTicketsSettingsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p22

    .line 117
    iput-object v1, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen_Presenter_Factory;->orderTicketPrintingSettingsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen_Presenter_Factory;
    .locals 24
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/ErrorsBarPresenter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStation;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/HardwarePrinterTracker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrinterStations;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/cogs/Cogs;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;>;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/papersignature/PaperSignatureSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/printerstations/station/PrinterRoleSupportChecker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/settings/printerstations/station/TestPrint;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/PrintSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/AppNameFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/print/OrderTicketPrintingSettings;",
            ">;)",
            "Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen_Presenter_Factory;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    .line 145
    new-instance v23, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen_Presenter_Factory;

    move-object/from16 v0, v23

    invoke-direct/range {v0 .. v22}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen_Presenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v23
.end method

.method public static newInstance(Lcom/squareup/ui/ErrorsBarPresenter;Lcom/squareup/print/PrinterStation;Lcom/squareup/print/HardwarePrinterTracker;Lcom/squareup/print/PrinterStations;Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;Lcom/squareup/util/Res;Lcom/squareup/cogs/Cogs;Lcom/squareup/analytics/Analytics;ZLcom/f2prateek/rx/preferences2/Preference;Ljava/lang/Object;Lcom/squareup/papersignature/PaperSignatureSettings;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/settings/printerstations/station/PrinterRoleSupportChecker;Lcom/squareup/util/Device;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;Lcom/squareup/ui/settings/printerstations/station/TestPrint;Lcom/squareup/print/PrintSettings;Lcom/squareup/util/AppNameFormatter;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/print/OrderTicketPrintingSettings;)Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;
    .locals 24
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/ErrorsBarPresenter;",
            "Lcom/squareup/print/PrinterStation;",
            "Lcom/squareup/print/HardwarePrinterTracker;",
            "Lcom/squareup/print/PrinterStations;",
            "Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/cogs/Cogs;",
            "Lcom/squareup/analytics/Analytics;",
            "Z",
            "Lcom/f2prateek/rx/preferences2/Preference<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljava/lang/Object;",
            "Lcom/squareup/papersignature/PaperSignatureSettings;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/ui/settings/printerstations/station/PrinterRoleSupportChecker;",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;",
            "Lcom/squareup/ui/settings/printerstations/station/TestPrint;",
            "Lcom/squareup/print/PrintSettings;",
            "Lcom/squareup/util/AppNameFormatter;",
            "Lcom/squareup/tickets/OpenTicketsSettings;",
            "Lcom/squareup/print/OrderTicketPrintingSettings;",
            ")",
            "Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    .line 159
    new-instance v23, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;

    move-object/from16 v0, v23

    move-object/from16 v11, p10

    check-cast v11, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;

    invoke-direct/range {v0 .. v22}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;-><init>(Lcom/squareup/ui/ErrorsBarPresenter;Lcom/squareup/print/PrinterStation;Lcom/squareup/print/HardwarePrinterTracker;Lcom/squareup/print/PrinterStations;Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;Lcom/squareup/util/Res;Lcom/squareup/cogs/Cogs;Lcom/squareup/analytics/Analytics;ZLcom/f2prateek/rx/preferences2/Preference;Lcom/squareup/ui/settings/printerstations/station/PrinterStationScope$PrinterStationState;Lcom/squareup/papersignature/PaperSignatureSettings;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/settings/printerstations/station/PrinterRoleSupportChecker;Lcom/squareup/util/Device;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;Lcom/squareup/ui/settings/printerstations/station/TestPrint;Lcom/squareup/print/PrintSettings;Lcom/squareup/util/AppNameFormatter;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/print/OrderTicketPrintingSettings;)V

    return-object v23
.end method


# virtual methods
.method public get()Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;
    .locals 24

    move-object/from16 v0, p0

    .line 122
    iget-object v1, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen_Presenter_Factory;->errorsBarPresenterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/ui/ErrorsBarPresenter;

    iget-object v1, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen_Presenter_Factory;->selectedPrinterStationProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/print/PrinterStation;

    iget-object v1, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen_Presenter_Factory;->hardwarePrinterTrackerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/print/HardwarePrinterTracker;

    iget-object v1, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen_Presenter_Factory;->printerStationsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/squareup/print/PrinterStations;

    iget-object v1, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen_Presenter_Factory;->scopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;

    iget-object v1, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/squareup/util/Res;

    iget-object v1, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen_Presenter_Factory;->cogsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/squareup/cogs/Cogs;

    iget-object v1, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen_Presenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/squareup/analytics/Analytics;

    iget-object v1, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen_Presenter_Factory;->isNewPrinterStationProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v10

    iget-object v1, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen_Presenter_Factory;->ticketAutoNumberingEnabledProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/f2prateek/rx/preferences2/Preference;

    iget-object v1, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen_Presenter_Factory;->printerStationStateProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v12

    iget-object v1, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen_Presenter_Factory;->paperSignatureSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/squareup/papersignature/PaperSignatureSettings;

    iget-object v1, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen_Presenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/squareup/settings/server/Features;

    iget-object v1, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen_Presenter_Factory;->printerRoleSupportCheckerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/squareup/ui/settings/printerstations/station/PrinterRoleSupportChecker;

    iget-object v1, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen_Presenter_Factory;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/util/Device;

    iget-object v1, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen_Presenter_Factory;->accountStatusSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v17, v1

    check-cast v17, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v1, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen_Presenter_Factory;->runnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v18, v1

    check-cast v18, Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;

    iget-object v1, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen_Presenter_Factory;->testPrintProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v19, v1

    check-cast v19, Lcom/squareup/ui/settings/printerstations/station/TestPrint;

    iget-object v1, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen_Presenter_Factory;->printSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v20, v1

    check-cast v20, Lcom/squareup/print/PrintSettings;

    iget-object v1, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen_Presenter_Factory;->appNameFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v21, v1

    check-cast v21, Lcom/squareup/util/AppNameFormatter;

    iget-object v1, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen_Presenter_Factory;->openTicketsSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v22, v1

    check-cast v22, Lcom/squareup/tickets/OpenTicketsSettings;

    iget-object v1, v0, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen_Presenter_Factory;->orderTicketPrintingSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v23, v1

    check-cast v23, Lcom/squareup/print/OrderTicketPrintingSettings;

    invoke-static/range {v2 .. v23}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen_Presenter_Factory;->newInstance(Lcom/squareup/ui/ErrorsBarPresenter;Lcom/squareup/print/PrinterStation;Lcom/squareup/print/HardwarePrinterTracker;Lcom/squareup/print/PrinterStations;Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;Lcom/squareup/util/Res;Lcom/squareup/cogs/Cogs;Lcom/squareup/analytics/Analytics;ZLcom/f2prateek/rx/preferences2/Preference;Ljava/lang/Object;Lcom/squareup/papersignature/PaperSignatureSettings;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/settings/printerstations/station/PrinterRoleSupportChecker;Lcom/squareup/util/Device;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/ui/settings/printerstations/station/PrinterStationScopeRunner;Lcom/squareup/ui/settings/printerstations/station/TestPrint;Lcom/squareup/print/PrintSettings;Lcom/squareup/util/AppNameFormatter;Lcom/squareup/tickets/OpenTicketsSettings;Lcom/squareup/print/OrderTicketPrintingSettings;)Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 23
    invoke-virtual {p0}, Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen_Presenter_Factory;->get()Lcom/squareup/ui/settings/printerstations/station/PrinterStationDetailScreen$Presenter;

    move-result-object v0

    return-object v0
.end method
