.class public Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketNamingMethodPopupScreen$Factory;
.super Ljava/lang/Object;
.source "TicketNamingMethodPopupScreen.java"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketNamingMethodPopupScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    .line 32
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Landroid/content/Context;)Lflow/path/Path;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketNamingMethodPopupScreen;

    .line 33
    new-instance v1, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketNamingMethodPopupScreen$DialogBuilder;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {v1, v0, p1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketNamingMethodPopupScreen$DialogBuilder;-><init>(Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketNamingMethodPopupScreen;Landroid/content/Context;)V

    invoke-virtual {v1}, Lcom/squareup/ui/settings/opentickets/ticketgroups/TicketNamingMethodPopupScreen$DialogBuilder;->build()Landroid/app/Dialog;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
