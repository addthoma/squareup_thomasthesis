.class public final Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;
.super Ljava/lang/Object;
.source "SettingsSectionPresenter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/SettingsSectionPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CoreParameters"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\n\u0018\u00002\u00020\u0001B+\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bR\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0011\u0010\u0007\u001a\u00020\u0008\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\u0011R\u0017\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013\u00a8\u0006\u0014"
    }
    d2 = {
        "Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;",
        "",
        "flow",
        "Lflow/Flow;",
        "onLoggingOut",
        "Lio/reactivex/Observable;",
        "",
        "device",
        "Lcom/squareup/util/Device;",
        "badBus",
        "Lcom/squareup/badbus/BadBus;",
        "(Lflow/Flow;Lio/reactivex/Observable;Lcom/squareup/util/Device;Lcom/squareup/badbus/BadBus;)V",
        "getBadBus",
        "()Lcom/squareup/badbus/BadBus;",
        "getDevice",
        "()Lcom/squareup/util/Device;",
        "getFlow",
        "()Lflow/Flow;",
        "getOnLoggingOut",
        "()Lio/reactivex/Observable;",
        "settings-ui_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final badBus:Lcom/squareup/badbus/BadBus;

.field private final device:Lcom/squareup/util/Device;

.field private final flow:Lflow/Flow;

.field private final onLoggingOut:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lflow/Flow;Lio/reactivex/Observable;Lcom/squareup/util/Device;Lcom/squareup/badbus/BadBus;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lflow/Flow;",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/badbus/BadBus;",
            ")V"
        }
    .end annotation

    const-string v0, "flow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "onLoggingOut"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "device"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "badBus"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;->flow:Lflow/Flow;

    iput-object p2, p0, Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;->onLoggingOut:Lio/reactivex/Observable;

    iput-object p3, p0, Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;->device:Lcom/squareup/util/Device;

    iput-object p4, p0, Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;->badBus:Lcom/squareup/badbus/BadBus;

    return-void
.end method


# virtual methods
.method public final getBadBus()Lcom/squareup/badbus/BadBus;
    .locals 1

    .line 36
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;->badBus:Lcom/squareup/badbus/BadBus;

    return-object v0
.end method

.method public final getDevice()Lcom/squareup/util/Device;
    .locals 1

    .line 35
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;->device:Lcom/squareup/util/Device;

    return-object v0
.end method

.method public final getFlow()Lflow/Flow;
    .locals 1

    .line 33
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;->flow:Lflow/Flow;

    return-object v0
.end method

.method public final getOnLoggingOut()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .line 34
    iget-object v0, p0, Lcom/squareup/ui/settings/SettingsSectionPresenter$CoreParameters;->onLoggingOut:Lio/reactivex/Observable;

    return-object v0
.end method
