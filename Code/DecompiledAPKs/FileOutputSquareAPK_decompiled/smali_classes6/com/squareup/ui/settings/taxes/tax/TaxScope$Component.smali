.class public interface abstract Lcom/squareup/ui/settings/taxes/tax/TaxScope$Component;
.super Ljava/lang/Object;
.source "TaxScope.java"


# annotations
.annotation runtime Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher$SharedScope;
.end annotation

.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/catalog/online/CatalogOnlineModule;,
        Lcom/squareup/ui/settings/taxes/tax/TaxScope$Module;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/taxes/tax/TaxScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract taxApplicableItems()Lcom/squareup/ui/settings/taxes/tax/TaxApplicableScreen$Component;
.end method

.method public abstract taxDetail(Lcom/squareup/ui/settings/taxes/tax/TaxDetailScreen$Module;)Lcom/squareup/ui/settings/taxes/tax/TaxDetailScreen$Component;
.end method

.method public abstract taxFeeType()Lcom/squareup/ui/settings/taxes/tax/TaxFeeTypeScreen$Component;
.end method

.method public abstract taxItemPricing()Lcom/squareup/ui/settings/taxes/tax/TaxItemPricingScreen$Component;
.end method

.method public abstract taxScopeRunner()Lcom/squareup/ui/settings/taxes/tax/TaxScopeRunner;
.end method
