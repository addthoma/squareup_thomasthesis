.class public final Lcom/squareup/ui/settings/instantdeposits/DepositsSection;
.super Lcom/squareup/applet/AppletSection;
.source "DepositsSection.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/instantdeposits/DepositsSection$ListEntry;,
        Lcom/squareup/ui/settings/instantdeposits/DepositsSection$Access;,
        Lcom/squareup/ui/settings/instantdeposits/DepositsSection$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008\u0007\u0018\u0000 \n2\u00020\u0001:\u0003\t\n\u000bB\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0008\u0010\u0007\u001a\u00020\u0008H\u0016\u00a8\u0006\u000c"
    }
    d2 = {
        "Lcom/squareup/ui/settings/instantdeposits/DepositsSection;",
        "Lcom/squareup/applet/AppletSection;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "employeeManagement",
        "Lcom/squareup/permissions/EmployeeManagement;",
        "(Lcom/squareup/settings/server/Features;Lcom/squareup/permissions/EmployeeManagement;)V",
        "getInitialScreen",
        "Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen;",
        "Access",
        "Companion",
        "ListEntry",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ui/settings/instantdeposits/DepositsSection$Companion;

.field public static final TITLE_ID:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/settings/instantdeposits/DepositsSection$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/settings/instantdeposits/DepositsSection$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/settings/instantdeposits/DepositsSection;->Companion:Lcom/squareup/ui/settings/instantdeposits/DepositsSection$Companion;

    .line 67
    sget v0, Lcom/squareup/common/strings/R$string;->instant_deposits_title:I

    sput v0, Lcom/squareup/ui/settings/instantdeposits/DepositsSection;->TITLE_ID:I

    return-void
.end method

.method public constructor <init>(Lcom/squareup/settings/server/Features;Lcom/squareup/permissions/EmployeeManagement;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "features"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "employeeManagement"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    new-instance v0, Lcom/squareup/ui/settings/instantdeposits/DepositsSection$Access;

    invoke-direct {v0, p1, p2}, Lcom/squareup/ui/settings/instantdeposits/DepositsSection$Access;-><init>(Lcom/squareup/settings/server/Features;Lcom/squareup/permissions/EmployeeManagement;)V

    check-cast v0, Lcom/squareup/applet/SectionAccess;

    invoke-direct {p0, v0}, Lcom/squareup/applet/AppletSection;-><init>(Lcom/squareup/applet/SectionAccess;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic getInitialScreen()Lcom/squareup/container/ContainerTreeKey;
    .locals 1

    .line 26
    invoke-virtual {p0}, Lcom/squareup/ui/settings/instantdeposits/DepositsSection;->getInitialScreen()Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen;

    move-result-object v0

    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    return-object v0
.end method

.method public getInitialScreen()Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen;
    .locals 1

    .line 32
    sget-object v0, Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen;->INSTANCE:Lcom/squareup/ui/settings/instantdeposits/DepositSettingsScreen;

    return-object v0
.end method
