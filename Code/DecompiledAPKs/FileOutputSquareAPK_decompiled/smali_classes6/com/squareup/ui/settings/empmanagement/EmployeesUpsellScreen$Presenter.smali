.class Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "EmployeesUpsellScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellView;",
        ">;"
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final flow:Lflow/Flow;

.field private final res:Lcom/squareup/util/Res;

.field private source:Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$EmployeesUpsellSource;


# direct methods
.method constructor <init>(Lcom/squareup/util/Res;Lflow/Flow;Lcom/squareup/analytics/Analytics;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 61
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 62
    iput-object p1, p0, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 63
    iput-object p2, p0, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$Presenter;->flow:Lflow/Flow;

    .line 64
    iput-object p3, p0, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method


# virtual methods
.method protected onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    .line 68
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onEnterScope(Lmortar/MortarScope;)V

    .line 70
    invoke-static {p1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen;

    invoke-static {p1}, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen;->access$000(Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen;)Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$EmployeesUpsellSource;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$Presenter;->source:Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$EmployeesUpsellSource;

    return-void
.end method

.method public onLoad(Landroid/os/Bundle;)V
    .locals 3

    .line 74
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 76
    invoke-virtual {p0}, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellView;

    .line 79
    sget-object v0, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$1;->$SwitchMap$com$squareup$ui$settings$empmanagement$EmployeesUpsellScreen$EmployeesUpsellSource:[I

    iget-object v1, p0, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$Presenter;->source:Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$EmployeesUpsellSource;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$EmployeesUpsellSource;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 88
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/settingsapplet/R$string;->track_time_upsell_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 89
    iget-object v1, p0, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/settingsapplet/R$string;->track_time_upsell_headline:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellView;->setTitle(Ljava/lang/CharSequence;)V

    .line 90
    sget v1, Lcom/squareup/settingsapplet/R$string;->track_time_upsell_description:I

    invoke-virtual {p1, v1}, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellView;->setMessage(I)V

    .line 91
    iget-object v1, p0, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/settingsapplet/R$string;->track_time_upsell_link:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellView;->setButtonText(Ljava/lang/CharSequence;)V

    .line 92
    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_TIMECARDS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {p1, v1}, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    goto :goto_0

    .line 95
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unexpected source for Employees Upsell: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$Presenter;->source:Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$EmployeesUpsellSource;

    .line 96
    invoke-virtual {v1}, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$EmployeesUpsellSource;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 81
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/settingsapplet/R$string;->payroll_upsell_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 82
    iget-object v1, p0, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/settingsapplet/R$string;->payroll_upsell_headline:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellView;->setTitle(Ljava/lang/CharSequence;)V

    .line 83
    sget v1, Lcom/squareup/settingsapplet/R$string;->payroll_upsell_description:I

    invoke-virtual {p1, v1}, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellView;->setMessage(I)V

    .line 84
    iget-object v1, p0, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/settingsapplet/R$string;->payroll_upsell_link:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellView;->setButtonText(Ljava/lang/CharSequence;)V

    .line 85
    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_PAYROLL:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {p1, v1}, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellView;->setGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)V

    .line 100
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellView;

    invoke-virtual {p1}, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object p1

    .line 101
    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    invoke-virtual {p1, v1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    .line 102
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$Presenter;->flow:Lflow/Flow;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v1, Lcom/squareup/ui/settings/empmanagement/-$$Lambda$sZX4UsMUUPBhpIptxMDlKtBJT0w;

    invoke-direct {v1, v0}, Lcom/squareup/ui/settings/empmanagement/-$$Lambda$sZX4UsMUUPBhpIptxMDlKtBJT0w;-><init>(Lflow/Flow;)V

    invoke-virtual {p1, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->showUpButton(Ljava/lang/Runnable;)V

    return-void
.end method

.method onUpsellButtonClicked()V
    .locals 3

    .line 106
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->PAYROLL_UPSELL_BUTTON:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 109
    sget-object v0, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$1;->$SwitchMap$com$squareup$ui$settings$empmanagement$EmployeesUpsellScreen$EmployeesUpsellSource:[I

    iget-object v1, p0, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$Presenter;->source:Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$EmployeesUpsellSource;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$EmployeesUpsellSource;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 114
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/registerlib/R$string;->employee_management_dashboard_url:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 117
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected source for Employees Upsell: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$Presenter;->source:Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$EmployeesUpsellSource;

    .line 118
    invoke-virtual {v2}, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$EmployeesUpsellSource;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 111
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/registerlib/R$string;->payroll_url:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 121
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellView;

    invoke-virtual {v1}, Lcom/squareup/ui/settings/empmanagement/EmployeesUpsellView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/squareup/util/RegisterIntents;->launchBrowser(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method
