.class public final Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "CropCustomDesignSettingsCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nCropCustomDesignSettingsCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 CropCustomDesignSettingsCoordinator.kt\ncom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator\n*L\n1#1,196:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0010\u0010&\u001a\u00020\'2\u0006\u0010%\u001a\u00020\u0012H\u0016J\u0010\u0010(\u001a\u00020\'2\u0006\u0010)\u001a\u00020*H\u0002R\u001b\u0010\u0007\u001a\u00020\u00088BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u000b\u0010\u000c\u001a\u0004\u0008\t\u0010\nR\u001c\u0010\r\u001a\u0010\u0012\u000c\u0012\n \u0010*\u0004\u0018\u00010\u000f0\u000f0\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u0011\u001a\u00020\u00128BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u0015\u0010\u000c\u001a\u0004\u0008\u0013\u0010\u0014R\u001b\u0010\u0016\u001a\u00020\u00178BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u001a\u0010\u000c\u001a\u0004\u0008\u0018\u0010\u0019R\u001b\u0010\u001b\u001a\u00020\u00178BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\u001d\u0010\u000c\u001a\u0004\u0008\u001c\u0010\u0019R\u0010\u0010\u001e\u001a\u0004\u0018\u00010\u001fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001b\u0010 \u001a\u00020\u00128BX\u0082\u0084\u0002\u00a2\u0006\u000c\n\u0004\u0008\"\u0010\u000c\u001a\u0004\u0008!\u0010\u0014R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010#\u001a\u00020$X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010%\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006+"
    }
    d2 = {
        "Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "runner",
        "Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsScreen$Runner;",
        "picasso",
        "Lcom/squareup/picasso/Picasso;",
        "(Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsScreen$Runner;Lcom/squareup/picasso/Picasso;)V",
        "actionBar",
        "Lcom/squareup/noho/NohoActionBar;",
        "getActionBar",
        "()Lcom/squareup/noho/NohoActionBar;",
        "actionBar$delegate",
        "Lkotlin/Lazy;",
        "doneEnabled",
        "Lcom/jakewharton/rxrelay2/BehaviorRelay;",
        "",
        "kotlin.jvm.PlatformType",
        "errorView",
        "Landroid/view/View;",
        "getErrorView",
        "()Landroid/view/View;",
        "errorView$delegate",
        "image",
        "Lcom/squareup/ui/settings/giftcards/EGiftCardRatioImageView;",
        "getImage",
        "()Lcom/squareup/ui/settings/giftcards/EGiftCardRatioImageView;",
        "image$delegate",
        "imageFrame",
        "getImageFrame",
        "imageFrame$delegate",
        "photoViewAttacher",
        "Lcom/github/chrisbanes/photoview/PhotoViewAttacher;",
        "progressBar",
        "getProgressBar",
        "progressBar$delegate",
        "target",
        "Lcom/squareup/picasso/Target;",
        "view",
        "attach",
        "",
        "displayBitmap",
        "bitmap",
        "Landroid/graphics/Bitmap;",
        "settings-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final actionBar$delegate:Lkotlin/Lazy;

.field private final doneEnabled:Lcom/jakewharton/rxrelay2/BehaviorRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/BehaviorRelay<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final errorView$delegate:Lkotlin/Lazy;

.field private final image$delegate:Lkotlin/Lazy;

.field private final imageFrame$delegate:Lkotlin/Lazy;

.field private photoViewAttacher:Lcom/github/chrisbanes/photoview/PhotoViewAttacher;

.field private final picasso:Lcom/squareup/picasso/Picasso;

.field private final progressBar$delegate:Lkotlin/Lazy;

.field private final runner:Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsScreen$Runner;

.field private target:Lcom/squareup/picasso/Target;

.field private view:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsScreen$Runner;Lcom/squareup/picasso/Picasso;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "runner"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "picasso"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;->runner:Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsScreen$Runner;

    iput-object p2, p0, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;->picasso:Lcom/squareup/picasso/Picasso;

    .line 42
    new-instance p1, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator$actionBar$2;

    invoke-direct {p1, p0}, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator$actionBar$2;-><init>(Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;->actionBar$delegate:Lkotlin/Lazy;

    .line 43
    new-instance p1, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator$image$2;

    invoke-direct {p1, p0}, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator$image$2;-><init>(Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;->image$delegate:Lkotlin/Lazy;

    .line 46
    new-instance p1, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator$imageFrame$2;

    invoke-direct {p1, p0}, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator$imageFrame$2;-><init>(Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;->imageFrame$delegate:Lkotlin/Lazy;

    .line 49
    new-instance p1, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator$progressBar$2;

    invoke-direct {p1, p0}, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator$progressBar$2;-><init>(Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;->progressBar$delegate:Lkotlin/Lazy;

    .line 50
    new-instance p1, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator$errorView$2;

    invoke-direct {p1, p0}, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator$errorView$2;-><init>(Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;)V

    check-cast p1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1}, Lkotlin/LazyKt;->lazy(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;->errorView$delegate:Lkotlin/Lazy;

    const/4 p1, 0x0

    .line 53
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-static {p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->createDefault(Ljava/lang/Object;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    const-string p2, "BehaviorRelay.createDefault(false)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;->doneEnabled:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    return-void
.end method

.method public static final synthetic access$displayBitmap(Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;Landroid/graphics/Bitmap;)V
    .locals 0

    .line 36
    invoke-direct {p0, p1}, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;->displayBitmap(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public static final synthetic access$getActionBar$p(Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;)Lcom/squareup/noho/NohoActionBar;
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;->getActionBar()Lcom/squareup/noho/NohoActionBar;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getErrorView$p(Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;)Landroid/view/View;
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;->getErrorView()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getImage$p(Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;)Lcom/squareup/ui/settings/giftcards/EGiftCardRatioImageView;
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;->getImage()Lcom/squareup/ui/settings/giftcards/EGiftCardRatioImageView;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getProgressBar$p(Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;)Landroid/view/View;
    .locals 0

    .line 36
    invoke-direct {p0}, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;->getProgressBar()Landroid/view/View;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$getRunner$p(Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;)Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsScreen$Runner;
    .locals 0

    .line 36
    iget-object p0, p0, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;->runner:Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsScreen$Runner;

    return-object p0
.end method

.method public static final synthetic access$getView$p(Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;)Landroid/view/View;
    .locals 1

    .line 36
    iget-object p0, p0, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;->view:Landroid/view/View;

    if-nez p0, :cond_0

    const-string v0, "view"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$setView$p(Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;Landroid/view/View;)V
    .locals 0

    .line 36
    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;->view:Landroid/view/View;

    return-void
.end method

.method private final displayBitmap(Landroid/graphics/Bitmap;)V
    .locals 2

    .line 103
    invoke-direct {p0}, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;->getImage()Lcom/squareup/ui/settings/giftcards/EGiftCardRatioImageView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/ui/settings/giftcards/EGiftCardRatioImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 104
    invoke-direct {p0}, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;->getImage()Lcom/squareup/ui/settings/giftcards/EGiftCardRatioImageView;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 105
    invoke-direct {p0}, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;->getImageFrame()Lcom/squareup/ui/settings/giftcards/EGiftCardRatioImageView;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 106
    invoke-direct {p0}, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;->getProgressBar()Landroid/view/View;

    move-result-object p1

    const/4 v1, 0x0

    invoke-static {p1, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 107
    iget-object p1, p0, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;->doneEnabled:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    .line 110
    new-instance p1, Lcom/github/chrisbanes/photoview/PhotoViewAttacher;

    invoke-direct {p0}, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;->getImage()Lcom/squareup/ui/settings/giftcards/EGiftCardRatioImageView;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-direct {p1, v0}, Lcom/github/chrisbanes/photoview/PhotoViewAttacher;-><init>(Landroid/widget/ImageView;)V

    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;->photoViewAttacher:Lcom/github/chrisbanes/photoview/PhotoViewAttacher;

    .line 114
    iget-object p1, p0, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;->photoViewAttacher:Lcom/github/chrisbanes/photoview/PhotoViewAttacher;

    if-nez p1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    sget-object v0, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p1, v0}, Lcom/github/chrisbanes/photoview/PhotoViewAttacher;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 115
    iget-object p1, p0, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;->photoViewAttacher:Lcom/github/chrisbanes/photoview/PhotoViewAttacher;

    if-nez p1, :cond_1

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_1
    const v0, 0x3dcccccd    # 0.1f

    invoke-virtual {p1, v0}, Lcom/github/chrisbanes/photoview/PhotoViewAttacher;->setMinimumScale(F)V

    return-void
.end method

.method private final getActionBar()Lcom/squareup/noho/NohoActionBar;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;->actionBar$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/noho/NohoActionBar;

    return-object v0
.end method

.method private final getErrorView()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;->errorView$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method private final getImage()Lcom/squareup/ui/settings/giftcards/EGiftCardRatioImageView;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;->image$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/giftcards/EGiftCardRatioImageView;

    return-object v0
.end method

.method private final getImageFrame()Lcom/squareup/ui/settings/giftcards/EGiftCardRatioImageView;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;->imageFrame$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/settings/giftcards/EGiftCardRatioImageView;

    return-object v0
.end method

.method private final getProgressBar()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;->progressBar$delegate:Lkotlin/Lazy;

    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 5

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    iput-object p1, p0, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;->view:Landroid/view/View;

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;->doneEnabled:Lcom/jakewharton/rxrelay2/BehaviorRelay;

    check-cast v0, Lio/reactivex/Observable;

    new-instance v1, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator$attach$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator$attach$1;-><init>(Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;)V

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-static {v0, p1, v1}, Lcom/squareup/util/Rx2ObservablesKt;->subscribeWith(Lio/reactivex/Observable;Landroid/view/View;Lkotlin/jvm/functions/Function1;)Lio/reactivex/disposables/Disposable;

    .line 74
    invoke-direct {p0}, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;->getImageFrame()Lcom/squareup/ui/settings/giftcards/EGiftCardRatioImageView;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 75
    invoke-direct {p0}, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;->getImage()Lcom/squareup/ui/settings/giftcards/EGiftCardRatioImageView;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 76
    invoke-direct {p0}, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;->getErrorView()Landroid/view/View;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 77
    invoke-direct {p0}, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;->getProgressBar()Landroid/view/View;

    move-result-object v0

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    .line 79
    new-instance v0, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator$attach$2;

    invoke-direct {v0, p0}, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator$attach$2;-><init>(Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;)V

    check-cast v0, Lcom/squareup/picasso/Target;

    iput-object v0, p0, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;->target:Lcom/squareup/picasso/Target;

    .line 93
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;->picasso:Lcom/squareup/picasso/Picasso;

    iget-object v2, p0, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;->runner:Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsScreen$Runner;

    invoke-interface {v2}, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsScreen$Runner;->imageUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/picasso/Picasso;->load(Landroid/net/Uri;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    .line 94
    invoke-direct {p0}, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;->getImage()Lcom/squareup/ui/settings/giftcards/EGiftCardRatioImageView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/ui/settings/giftcards/EGiftCardRatioImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const-string v3, "image.resources"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-static {v2, v1, v3, v4}, Lcom/squareup/egiftcard/activation/EGiftCardConfigKt;->createEGiftCardPlaceholder$default(Landroid/content/res/Resources;IILjava/lang/Object;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Lcom/squareup/picasso/RequestCreator;->placeholder(Landroid/graphics/drawable/Drawable;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    .line 95
    new-instance v1, Lcom/squareup/ui/settings/giftcards/ScaleTransform;

    invoke-direct {v1}, Lcom/squareup/ui/settings/giftcards/ScaleTransform;-><init>()V

    check-cast v1, Lcom/squareup/picasso/Transformation;

    invoke-virtual {v0, v1}, Lcom/squareup/picasso/RequestCreator;->transform(Lcom/squareup/picasso/Transformation;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    .line 96
    iget-object v1, p0, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;->target:Lcom/squareup/picasso/Target;

    if-nez v1, :cond_0

    const-string v2, "target"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {v0, v1}, Lcom/squareup/picasso/RequestCreator;->into(Lcom/squareup/picasso/Target;)V

    .line 99
    iget-object v0, p0, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsCoordinator;->runner:Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsScreen$Runner;

    invoke-interface {v0, p1}, Lcom/squareup/ui/settings/giftcards/CropCustomDesignSettingsScreen$Runner;->showUploadSpinnerEvents(Landroid/view/View;)V

    return-void
.end method
