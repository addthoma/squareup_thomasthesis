.class public final Lcom/squareup/ui/settings/opentickets/PredefinedTicketsDisabledDialogScreen;
.super Lcom/squareup/ui/settings/InSettingsAppletScope;
.source "PredefinedTicketsDisabledDialogScreen.java"


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/ui/settings/opentickets/PredefinedTicketsDisabledDialogScreen$Factory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/settings/opentickets/PredefinedTicketsDisabledDialogScreen$Factory;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/settings/opentickets/PredefinedTicketsDisabledDialogScreen;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 37
    sget-object v0, Lcom/squareup/ui/settings/opentickets/-$$Lambda$PredefinedTicketsDisabledDialogScreen$GmQWHhxLWtlhfYHWX9JCap0R1qE;->INSTANCE:Lcom/squareup/ui/settings/opentickets/-$$Lambda$PredefinedTicketsDisabledDialogScreen$GmQWHhxLWtlhfYHWX9JCap0R1qE;

    .line 38
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/settings/opentickets/PredefinedTicketsDisabledDialogScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 19
    invoke-direct {p0}, Lcom/squareup/ui/settings/InSettingsAppletScope;-><init>()V

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/settings/opentickets/PredefinedTicketsDisabledDialogScreen;
    .locals 0

    .line 38
    new-instance p0, Lcom/squareup/ui/settings/opentickets/PredefinedTicketsDisabledDialogScreen;

    invoke-direct {p0}, Lcom/squareup/ui/settings/opentickets/PredefinedTicketsDisabledDialogScreen;-><init>()V

    return-object p0
.end method
