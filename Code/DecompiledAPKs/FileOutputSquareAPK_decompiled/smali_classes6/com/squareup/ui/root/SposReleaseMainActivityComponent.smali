.class public interface abstract Lcom/squareup/ui/root/SposReleaseMainActivityComponent;
.super Ljava/lang/Object;
.source "SposReleaseMainActivityComponent.java"

# interfaces
.implements Lcom/squareup/SposMainActivityComponent;
.implements Lcom/squareup/ui/root/PosReleaseMainActivityComponent;


# annotations
.annotation runtime Lcom/squareup/banklinking/RealBankAccountSettings$SharedScope;
.end annotation

.annotation runtime Lcom/squareup/camerahelper/CameraHelperScope;
.end annotation

.annotation runtime Lcom/squareup/depositschedule/RealDepositScheduleSettings$SharedScope;
.end annotation

.annotation runtime Lcom/squareup/permissions/PermissionGatekeeperScope;
.end annotation

.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/SposReleaseMainActivityModule;,
        Lcom/squareup/ui/root/PosReleaseMainActivityComponent$Module;
    }
.end annotation
