.class public abstract Lcom/squareup/ui/root/SposReleaseAppletModule;
.super Ljava/lang/Object;
.source "SposReleaseAppletModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract bindApplets(Lcom/squareup/ui/root/SposReleaseApplets;)Lcom/squareup/applet/Applets;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideTutorialCreatorss(Ljava/util/List;)Ljava/util/List;
    .annotation runtime Ldagger/Binds;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/tutorialv2/TutorialCreator;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/tutorialv2/TutorialCreator;",
            ">;"
        }
    .end annotation
.end method
