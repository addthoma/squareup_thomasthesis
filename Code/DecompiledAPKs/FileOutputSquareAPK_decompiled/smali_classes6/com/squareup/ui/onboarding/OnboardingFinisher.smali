.class public Lcom/squareup/ui/onboarding/OnboardingFinisher;
.super Lmortar/Presenter;
.source "OnboardingFinisher.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/onboarding/OnboardingFinisher$View;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/Presenter<",
        "Lcom/squareup/ui/onboarding/OnboardingFinisher$View;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 16
    invoke-direct {p0}, Lmortar/Presenter;-><init>()V

    return-void
.end method


# virtual methods
.method protected extractBundleService(Lcom/squareup/ui/onboarding/OnboardingFinisher$View;)Lmortar/bundler/BundleService;
    .locals 0

    .line 20
    invoke-interface {p1}, Lcom/squareup/ui/onboarding/OnboardingFinisher$View;->getBundleService()Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method

.method protected bridge synthetic extractBundleService(Ljava/lang/Object;)Lmortar/bundler/BundleService;
    .locals 0

    .line 8
    check-cast p1, Lcom/squareup/ui/onboarding/OnboardingFinisher$View;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/onboarding/OnboardingFinisher;->extractBundleService(Lcom/squareup/ui/onboarding/OnboardingFinisher$View;)Lmortar/bundler/BundleService;

    move-result-object p1

    return-object p1
.end method

.method public finish()V
    .locals 1

    .line 24
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/OnboardingFinisher;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/onboarding/OnboardingFinisher$View;

    invoke-interface {v0}, Lcom/squareup/ui/onboarding/OnboardingFinisher$View;->finish()V

    return-void
.end method
