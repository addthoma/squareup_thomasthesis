.class public Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "ConfirmMagstripeAddressScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;",
        ">;"
    }
.end annotation


# instance fields
.field private final activationService:Lcom/squareup/server/activation/ActivationService;

.field private final analytics:Lcom/squareup/analytics/Analytics;

.field final confirmHaveReaderPopupPresenter:Lcom/squareup/mortar/PopupPresenter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/mortar/PopupPresenter<",
            "Lcom/squareup/register/widgets/Confirmation;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private createAddressDisposable:Lio/reactivex/disposables/Disposable;

.field final createAddressProgressPresenter:Lcom/squareup/caller/ProgressAndFailurePresenter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/caller/ProgressAndFailurePresenter<",
            "Lcom/squareup/server/shipping/UpdateAddressResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final features:Lcom/squareup/settings/server/Features;

.field private lastFailedResult:Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Result;

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final messageFactory:Lcom/squareup/receiving/FailureMessageFactory;

.field private final model:Lcom/squareup/ui/onboarding/OnboardingModel;

.field private final res:Lcom/squareup/util/Res;

.field private final runner:Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final shippingAddressService:Lcom/squareup/server/shipping/ShippingAddressService;

.field private final taskQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

.field final warningPopupPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/flowlegacy/NoResultPopupPresenter<",
            "Lcom/squareup/widgets/warning/Warning;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/ui/onboarding/OnboardingActivityRunner;Lcom/squareup/server/activation/ActivationService;Lio/reactivex/Scheduler;Lcom/squareup/ui/onboarding/OnboardingModel;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;Lcom/squareup/server/shipping/ShippingAddressService;Lcom/squareup/settings/server/Features;Lcom/squareup/receiving/FailureMessageFactory;)V
    .locals 1
    .param p3    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 105
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 88
    new-instance v0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter$1;-><init>(Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;)V

    iput-object v0, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->confirmHaveReaderPopupPresenter:Lcom/squareup/mortar/PopupPresenter;

    .line 97
    new-instance v0, Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    invoke-direct {v0}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;-><init>()V

    iput-object v0, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->warningPopupPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    .line 98
    invoke-static {}, Lio/reactivex/disposables/Disposables;->disposed()Lio/reactivex/disposables/Disposable;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->createAddressDisposable:Lio/reactivex/disposables/Disposable;

    .line 106
    iput-object p1, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->runner:Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

    .line 107
    iput-object p2, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->activationService:Lcom/squareup/server/activation/ActivationService;

    .line 108
    iput-object p3, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->mainScheduler:Lio/reactivex/Scheduler;

    .line 109
    iput-object p4, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    .line 110
    iput-object p5, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->taskQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    .line 111
    iput-object p6, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 112
    iput-object p7, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 113
    iput-object p8, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    .line 114
    iput-object p9, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->shippingAddressService:Lcom/squareup/server/shipping/ShippingAddressService;

    .line 115
    iput-object p10, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    .line 116
    iput-object p11, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->messageFactory:Lcom/squareup/receiving/FailureMessageFactory;

    .line 118
    new-instance p2, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$RequestReaderResources;

    invoke-direct {p2, p7}, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$RequestReaderResources;-><init>(Lcom/squareup/util/Res;)V

    .line 119
    new-instance p3, Lcom/squareup/caller/ProgressAndFailurePresenter;

    new-instance p4, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter$2;

    invoke-direct {p4, p0, p1}, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter$2;-><init>(Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;Lcom/squareup/ui/onboarding/OnboardingActivityRunner;)V

    const-string p1, "createShippingCall"

    invoke-direct {p3, p1, p2, p4}, Lcom/squareup/caller/ProgressAndFailurePresenter;-><init>(Ljava/lang/String;Lcom/squareup/request/RequestMessages;Lcom/squareup/caller/ProgressAndFailurePresenter$ViewListener;)V

    iput-object p3, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->createAddressProgressPresenter:Lcom/squareup/caller/ProgressAndFailurePresenter;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;)Lcom/squareup/queue/retrofit/RetrofitQueue;
    .locals 0

    .line 73
    iget-object p0, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->taskQueue:Lcom/squareup/queue/retrofit/RetrofitQueue;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;)Lcom/squareup/ui/onboarding/OnboardingActivityRunner;
    .locals 0

    .line 73
    iget-object p0, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->runner:Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

    return-object p0
.end method

.method private getVerifyRequest()Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;
    .locals 3

    .line 277
    new-instance v0, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    .line 278
    invoke-virtual {v1}, Lcom/squareup/ui/onboarding/OnboardingModel;->getPersonalFirstName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;->first_name(Ljava/lang/String;)Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    .line 279
    invoke-virtual {v1}, Lcom/squareup/ui/onboarding/OnboardingModel;->getPersonalLastName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;->last_name(Ljava/lang/String;)Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    .line 280
    invoke-virtual {v1}, Lcom/squareup/ui/onboarding/OnboardingModel;->getShippingAddress()Lcom/squareup/address/Address;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 281
    invoke-virtual {v2}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/settings/server/UserSettings;->getCountryCode()Lcom/squareup/CountryCode;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/address/Address;->toGlobalAddress(Lcom/squareup/CountryCode;)Lcom/squareup/protos/common/location/GlobalAddress;

    move-result-object v1

    .line 280
    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;->shipping_address(Lcom/squareup/protos/common/location/GlobalAddress;)Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;

    move-result-object v0

    new-instance v1, Lcom/squareup/protos/common/location/Phone$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/common/location/Phone$Builder;-><init>()V

    iget-object v2, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    .line 282
    invoke-virtual {v2}, Lcom/squareup/ui/onboarding/OnboardingModel;->getPhoneNumber()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/protos/common/location/Phone$Builder;->number(Ljava/lang/String;)Lcom/squareup/protos/common/location/Phone$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/protos/common/location/Phone$Builder;->build()Lcom/squareup/protos/common/location/Phone;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;->phone(Lcom/squareup/protos/common/location/Phone;)Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;

    move-result-object v0

    .line 283
    invoke-virtual {v0}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest$Builder;->build()Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;

    move-result-object v0

    return-object v0
.end method

.method static synthetic lambda$null$3(Lcom/squareup/server/shipping/UpdateAddressResponse;)Lcom/squareup/receiving/FailureMessage$Parts;
    .locals 2

    .line 212
    new-instance v0, Lcom/squareup/receiving/FailureMessage$Parts;

    invoke-direct {v0}, Lcom/squareup/receiving/FailureMessage$Parts;-><init>()V

    .line 213
    invoke-virtual {p0}, Lcom/squareup/server/shipping/UpdateAddressResponse;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/receiving/FailureMessage$Parts;->withTitle(Ljava/lang/String;)Lcom/squareup/receiving/FailureMessage$Parts;

    move-result-object v0

    .line 214
    invoke-virtual {p0}, Lcom/squareup/server/shipping/UpdateAddressResponse;->getMessage()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/receiving/FailureMessage$Parts;->withBody(Ljava/lang/String;)Lcom/squareup/receiving/FailureMessage$Parts;

    move-result-object p0

    return-object p0
.end method

.method private sendShippingAddress(Lcom/squareup/server/shipping/ShippingBody;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/shipping/ShippingBody;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Result;",
            ">;"
        }
    .end annotation

    .line 201
    iget-object v0, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->activationService:Lcom/squareup/server/activation/ActivationService;

    invoke-interface {v0, p1}, Lcom/squareup/server/activation/ActivationService;->createShippingAddress(Lcom/squareup/server/shipping/ShippingBody;)Lcom/squareup/server/SimpleStandardResponse;

    move-result-object p1

    .line 202
    invoke-virtual {p1}, Lcom/squareup/server/SimpleStandardResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/onboarding/contactless/-$$Lambda$ConfirmMagstripeAddressScreen$Presenter$9Da4DpKfIJmModakOW72Q4xE-ko;

    invoke-direct {v0, p0}, Lcom/squareup/ui/onboarding/contactless/-$$Lambda$ConfirmMagstripeAddressScreen$Presenter$9Da4DpKfIJmModakOW72Q4xE-ko;-><init>(Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;)V

    .line 203
    invoke-virtual {p1, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public synthetic lambda$sendShippingAddress$4$ConfirmMagstripeAddressScreen$Presenter(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Result;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 204
    instance-of v0, p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$HandleSuccess;

    if-eqz v0, :cond_0

    .line 205
    invoke-static {}, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Result;->success()Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Result;

    move-result-object p1

    return-object p1

    .line 207
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->messageFactory:Lcom/squareup/receiving/FailureMessageFactory;

    check-cast p1, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;

    sget v1, Lcom/squareup/common/strings/R$string;->order_reader_magstripe_validation_system_error_title:I

    sget-object v2, Lcom/squareup/ui/onboarding/contactless/-$$Lambda$ConfirmMagstripeAddressScreen$Presenter$FL8DYgk2yKLH824jlCu6zZd1FRc;->INSTANCE:Lcom/squareup/ui/onboarding/contactless/-$$Lambda$ConfirmMagstripeAddressScreen$Presenter$FL8DYgk2yKLH824jlCu6zZd1FRc;

    invoke-virtual {v0, p1, v1, v2}, Lcom/squareup/receiving/FailureMessageFactory;->get(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure$ShowFailure;ILkotlin/jvm/functions/Function1;)Lcom/squareup/receiving/FailureMessage;

    move-result-object p1

    .line 216
    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getBody()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Result;->error(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Result;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$verifyAndSendShippingAddress$0$ConfirmMagstripeAddressScreen$Presenter(Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;)Lio/reactivex/SingleSource;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 144
    invoke-virtual {p1}, Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;->getOkayResponse()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;

    if-nez p1, :cond_0

    .line 146
    iget-object p1, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->res:Lcom/squareup/util/Res;

    invoke-static {p1}, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Result;->serviceError(Lcom/squareup/util/Res;)Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Result;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1

    .line 149
    :cond_0
    iget-object v0, p1, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;->corrected_field:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 150
    iget-object p1, p1, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;->corrected_address:Lcom/squareup/protos/common/location/GlobalAddress;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->res:Lcom/squareup/util/Res;

    invoke-static {p1, v0}, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Result;->corrected(Lcom/squareup/protos/common/location/GlobalAddress;Lcom/squareup/util/Res;)Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Result;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1

    .line 152
    :cond_1
    sget-object v0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$1;->$SwitchMap$com$squareup$protos$client$solidshop$VerifyShippingAddressResponse$AddressVerificationStatus:[I

    iget-object v1, p1, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;->verification_status:Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;

    invoke-virtual {v1}, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_4

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_3

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 164
    iget-object p1, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->res:Lcom/squareup/util/Res;

    invoke-static {p1}, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Result;->uncorrectable(Lcom/squareup/util/Res;)Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Result;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1

    .line 167
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unknown status: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p1, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;->verification_status:Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse$AddressVerificationStatus;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 160
    :cond_3
    iget-object p1, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->res:Lcom/squareup/util/Res;

    invoke-static {p1}, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Result;->serviceError(Lcom/squareup/util/Res;)Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Result;

    move-result-object p1

    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1

    .line 154
    :cond_4
    iget-object v0, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    .line 155
    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/OnboardingModel;->getShippingName()Ljava/lang/String;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/protos/client/solidshop/VerifyShippingAddressResponse;->corrected_address:Lcom/squareup/protos/common/location/GlobalAddress;

    invoke-static {v0, p1}, Lcom/squareup/server/shipping/ShippingBody;->from(Ljava/lang/String;Lcom/squareup/protos/common/location/GlobalAddress;)Lcom/squareup/server/shipping/ShippingBody;

    move-result-object p1

    .line 154
    invoke-direct {p0, p1}, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->sendShippingAddress(Lcom/squareup/server/shipping/ShippingBody;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$verifyAndSendShippingAddress$1$ConfirmMagstripeAddressScreen$Presenter(Lio/reactivex/disposables/Disposable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 180
    iget-object p1, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->createAddressProgressPresenter:Lcom/squareup/caller/ProgressAndFailurePresenter;

    invoke-virtual {p1}, Lcom/squareup/caller/ProgressAndFailurePresenter;->beginProgress()V

    return-void
.end method

.method public synthetic lambda$verifyAndSendShippingAddress$2$ConfirmMagstripeAddressScreen$Presenter(Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Result;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 182
    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Result;->isSuccessful()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 183
    iget-object p1, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->createAddressProgressPresenter:Lcom/squareup/caller/ProgressAndFailurePresenter;

    invoke-static {}, Lcom/squareup/server/shipping/UpdateAddressResponse;->success()Lcom/squareup/server/shipping/UpdateAddressResponse;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/caller/ProgressAndFailurePresenter;->onSuccess(Ljava/lang/Object;)V

    goto :goto_0

    .line 185
    :cond_0
    iput-object p1, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->lastFailedResult:Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Result;

    .line 186
    iget-object v0, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->createAddressProgressPresenter:Lcom/squareup/caller/ProgressAndFailurePresenter;

    invoke-static {p1}, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Result;->access$200(Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Result;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Result;->access$300(Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Result;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/caller/ProgressAndFailurePresenter;->onFailure(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;

    .line 188
    iget-object v1, p1, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Result;->correction:Lcom/squareup/protos/common/location/GlobalAddress;

    if-eqz v1, :cond_1

    .line 189
    iget-object v1, p1, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Result;->correction:Lcom/squareup/protos/common/location/GlobalAddress;

    invoke-static {v1}, Lcom/squareup/address/Address;->fromGlobalAddress(Lcom/squareup/protos/common/location/GlobalAddress;)Lcom/squareup/address/Address;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;->setAddress(Lcom/squareup/address/Address;)V

    .line 190
    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;->indicateCorrection()V

    .line 193
    :cond_1
    iget-boolean p1, p1, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Result;->uncorrectable:Z

    if-eqz p1, :cond_2

    .line 194
    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;->indicateUncorrectable()V

    :cond_2
    :goto_0
    return-void
.end method

.method protected onExitScope()V
    .locals 1

    .line 242
    iget-object v0, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->createAddressDisposable:Lio/reactivex/disposables/Disposable;

    invoke-interface {v0}, Lio/reactivex/disposables/Disposable;->dispose()V

    return-void
.end method

.method onHasReader()V
    .locals 5

    .line 266
    iget-object v0, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_CONTACTLESS_R4_SKIP_READER:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 268
    new-instance v0, Lcom/squareup/register/widgets/ConfirmationIds;

    sget v1, Lcom/squareup/onboarding/flow/R$string;->onboarding_shipping_confirm_have_reader_title:I

    sget v2, Lcom/squareup/onboarding/flow/R$string;->onboarding_shipping_confirm_have_reader_message:I

    sget v3, Lcom/squareup/common/strings/R$string;->continue_label:I

    sget v4, Lcom/squareup/common/strings/R$string;->cancel:I

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/register/widgets/ConfirmationIds;-><init>(IIII)V

    .line 273
    iget-object v1, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->confirmHaveReaderPopupPresenter:Lcom/squareup/mortar/PopupPresenter;

    invoke-virtual {v1, v0}, Lcom/squareup/mortar/PopupPresenter;->show(Landroid/os/Parcelable;)V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 1

    .line 221
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 223
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;

    .line 224
    iget-object v0, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/OnboardingModel;->getShippingName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;->setName(Ljava/lang/String;)V

    .line 225
    iget-object v0, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/OnboardingModel;->getPhoneNumber()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;->setPhone(Ljava/lang/String;)V

    .line 226
    iget-object v0, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/OnboardingModel;->getShippingAddress()Lcom/squareup/address/Address;

    move-result-object v0

    .line 227
    invoke-virtual {p1, v0}, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;->setAddress(Lcom/squareup/address/Address;)V

    .line 229
    iget-object v0, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->lastFailedResult:Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Result;

    if-eqz v0, :cond_1

    .line 231
    iget-object v0, v0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Result;->correction:Lcom/squareup/protos/common/location/GlobalAddress;

    if-eqz v0, :cond_0

    .line 232
    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;->indicateCorrection()V

    .line 235
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->lastFailedResult:Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Result;

    iget-boolean v0, v0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Result;->uncorrectable:Z

    if-eqz v0, :cond_1

    .line 236
    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;->indicateUncorrectable()V

    :cond_1
    return-void
.end method

.method onMailFreeReader()V
    .locals 3

    .line 246
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;

    .line 247
    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;->validateAddress()Lcom/squareup/ui/onboarding/ValidationError;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 249
    invoke-virtual {v1}, Lcom/squareup/ui/onboarding/ValidationError;->getParentViewId()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;->focusView(I)V

    .line 250
    new-instance v0, Lcom/squareup/widgets/warning/WarningIds;

    .line 251
    invoke-virtual {v1}, Lcom/squareup/ui/onboarding/ValidationError;->getTitleId()I

    move-result v2

    invoke-virtual {v1}, Lcom/squareup/ui/onboarding/ValidationError;->getMessageId()I

    move-result v1

    invoke-direct {v0, v2, v1}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    .line 252
    iget-object v1, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->warningPopupPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    invoke-virtual {v1, v0}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->show(Landroid/os/Parcelable;)V

    return-void

    .line 255
    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;->getShippingName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/ui/onboarding/OnboardingModel;->setShippingName(Ljava/lang/String;)V

    .line 256
    iget-object v1, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;->getPhone()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/ui/onboarding/OnboardingModel;->setPhoneNumber(Ljava/lang/String;)V

    .line 257
    iget-object v1, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;->getAddress()Lcom/squareup/address/Address;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/ui/onboarding/OnboardingModel;->setShippingAddress(Lcom/squareup/address/Address;)V

    .line 258
    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressView;->hideKeyboard()V

    .line 260
    iget-object v0, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_CONTACTLESS_R4_MAIL_READER:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 262
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->verifyAndSendShippingAddress()V

    return-void
.end method

.method verifyAndSendShippingAddress()V
    .locals 2

    .line 139
    iget-object v0, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->VERIFY_ADDRESS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->shippingAddressService:Lcom/squareup/server/shipping/ShippingAddressService;

    invoke-direct {p0}, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->getVerifyRequest()Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/server/shipping/ShippingAddressService;->verify(Lcom/squareup/protos/client/solidshop/VerifyShippingAddressRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object v0

    .line 142
    invoke-virtual {v0}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/onboarding/contactless/-$$Lambda$ConfirmMagstripeAddressScreen$Presenter$_X3ytdhAvcjP2e6JlaiEvhDj7bA;

    invoke-direct {v1, p0}, Lcom/squareup/ui/onboarding/contactless/-$$Lambda$ConfirmMagstripeAddressScreen$Presenter$_X3ytdhAvcjP2e6JlaiEvhDj7bA;-><init>(Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;)V

    .line 143
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    goto :goto_0

    .line 173
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/OnboardingModel;->getShippingAddress()Lcom/squareup/address/Address;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    invoke-virtual {v1}, Lcom/squareup/ui/onboarding/OnboardingModel;->getShippingName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/address/Address;->toShippingBody(Ljava/lang/String;)Lcom/squareup/server/shipping/ShippingBody;

    move-result-object v0

    .line 174
    invoke-direct {p0, v0}, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->sendShippingAddress(Lcom/squareup/server/shipping/ShippingBody;)Lio/reactivex/Single;

    move-result-object v0

    .line 177
    :goto_0
    iget-object v1, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->createAddressDisposable:Lio/reactivex/disposables/Disposable;

    invoke-interface {v1}, Lio/reactivex/disposables/Disposable;->dispose()V

    .line 178
    iget-object v1, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->mainScheduler:Lio/reactivex/Scheduler;

    .line 179
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/onboarding/contactless/-$$Lambda$ConfirmMagstripeAddressScreen$Presenter$2aC3acXdE-icHIUKIPnbrk6Wrpg;

    invoke-direct {v1, p0}, Lcom/squareup/ui/onboarding/contactless/-$$Lambda$ConfirmMagstripeAddressScreen$Presenter$2aC3acXdE-icHIUKIPnbrk6Wrpg;-><init>(Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;)V

    .line 180
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->doOnSubscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/onboarding/contactless/-$$Lambda$ConfirmMagstripeAddressScreen$Presenter$3205p7DpnfSitMdjJfLndRBRwdA;

    invoke-direct {v1, p0}, Lcom/squareup/ui/onboarding/contactless/-$$Lambda$ConfirmMagstripeAddressScreen$Presenter$3205p7DpnfSitMdjJfLndRBRwdA;-><init>(Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;)V

    .line 181
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;->createAddressDisposable:Lio/reactivex/disposables/Disposable;

    return-void
.end method
