.class public Lcom/squareup/ui/onboarding/MerchantCategoryScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "MerchantCategoryScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/onboarding/MerchantCategoryScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/onboarding/MerchantCategoryView;",
        ">;"
    }
.end annotation


# instance fields
.field private final activationResourcesService:Lcom/squareup/onboarding/ActivationResourcesService;

.field private final analytics:Lcom/squareup/analytics/Analytics;

.field final confirmLaterPopupPresenter:Lcom/squareup/mortar/PopupPresenter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/mortar/PopupPresenter<",
            "Lcom/squareup/register/widgets/Confirmation;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final model:Lcom/squareup/ui/onboarding/OnboardingModel;

.field private final res:Lcom/squareup/util/Res;

.field private final runner:Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

.field private final spinner:Lcom/squareup/register/widgets/GlassSpinner;


# direct methods
.method public constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/onboarding/ActivationResourcesService;Lcom/squareup/ui/onboarding/OnboardingModel;Lcom/squareup/ui/onboarding/OnboardingActivityRunner;Lcom/squareup/analytics/Analytics;Lcom/squareup/register/widgets/GlassSpinner;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 68
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 55
    new-instance v0, Lcom/squareup/ui/onboarding/MerchantCategoryScreen$Presenter$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/onboarding/MerchantCategoryScreen$Presenter$1;-><init>(Lcom/squareup/ui/onboarding/MerchantCategoryScreen$Presenter;)V

    iput-object v0, p0, Lcom/squareup/ui/onboarding/MerchantCategoryScreen$Presenter;->confirmLaterPopupPresenter:Lcom/squareup/mortar/PopupPresenter;

    .line 69
    iput-object p1, p0, Lcom/squareup/ui/onboarding/MerchantCategoryScreen$Presenter;->res:Lcom/squareup/util/Res;

    .line 70
    iput-object p2, p0, Lcom/squareup/ui/onboarding/MerchantCategoryScreen$Presenter;->activationResourcesService:Lcom/squareup/onboarding/ActivationResourcesService;

    .line 71
    iput-object p3, p0, Lcom/squareup/ui/onboarding/MerchantCategoryScreen$Presenter;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    .line 72
    iput-object p4, p0, Lcom/squareup/ui/onboarding/MerchantCategoryScreen$Presenter;->runner:Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

    .line 73
    iput-object p5, p0, Lcom/squareup/ui/onboarding/MerchantCategoryScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    .line 74
    iput-object p6, p0, Lcom/squareup/ui/onboarding/MerchantCategoryScreen$Presenter;->spinner:Lcom/squareup/register/widgets/GlassSpinner;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/onboarding/MerchantCategoryScreen$Presenter;)Lcom/squareup/ui/onboarding/OnboardingActivityRunner;
    .locals 0

    .line 46
    iget-object p0, p0, Lcom/squareup/ui/onboarding/MerchantCategoryScreen$Presenter;->runner:Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

    return-object p0
.end method

.method public static synthetic lambda$5Kbm_OhT289Y4SjQTCfouGTqBDw(Lcom/squareup/ui/onboarding/MerchantCategoryScreen$Presenter;)V
    .locals 0

    invoke-direct {p0}, Lcom/squareup/ui/onboarding/MerchantCategoryScreen$Presenter;->onContinueButtonTapped()V

    return-void
.end method

.method public static synthetic lambda$N0IyxJZVlFz14G8MQLgXJice96g(Lcom/squareup/ui/onboarding/MerchantCategoryScreen$Presenter;)V
    .locals 0

    invoke-direct {p0}, Lcom/squareup/ui/onboarding/MerchantCategoryScreen$Presenter;->onSkipButtonTapped()V

    return-void
.end method

.method private onContinueButtonTapped()V
    .locals 3

    .line 135
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/MerchantCategoryScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/onboarding/MerchantCategoryView;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/MerchantCategoryView;->getSelected()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/activation/ActivationResources$BusinessCategory;

    if-eqz v0, :cond_0

    .line 137
    iget-object v1, p0, Lcom/squareup/ui/onboarding/MerchantCategoryScreen$Presenter;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    invoke-virtual {v1, v0}, Lcom/squareup/ui/onboarding/OnboardingModel;->setBusinessCategory(Lcom/squareup/server/activation/ActivationResources$BusinessCategory;)V

    .line 138
    iget-object v0, p0, Lcom/squareup/ui/onboarding/MerchantCategoryScreen$Presenter;->runner:Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->onBusinessCategorySelected()V

    .line 139
    iget-object v0, p0, Lcom/squareup/ui/onboarding/MerchantCategoryScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_MCC_CONTINUE:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    goto :goto_0

    .line 141
    :cond_0
    new-instance v0, Lcom/squareup/widgets/warning/WarningIds;

    sget v1, Lcom/squareup/banklinking/R$string;->missing_required_field:I

    sget v2, Lcom/squareup/onboarding/flow/R$string;->choose_a_category:I

    invoke-direct {v0, v1, v2}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    .line 144
    iget-object v1, p0, Lcom/squareup/ui/onboarding/MerchantCategoryScreen$Presenter;->runner:Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

    invoke-virtual {v1, v0}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->warn(Lcom/squareup/widgets/warning/Warning;)V

    .line 145
    iget-object v0, p0, Lcom/squareup/ui/onboarding/MerchantCategoryScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_MCC_CONTINUE_NON_SELECTED:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    :goto_0
    return-void
.end method

.method private onSkipButtonTapped()V
    .locals 5

    .line 150
    new-instance v0, Lcom/squareup/register/widgets/ConfirmationIds;

    sget v1, Lcom/squareup/onboarding/flow/R$string;->onboarding_shipping_confirm_have_reader_title:I

    sget v2, Lcom/squareup/onboarding/flow/R$string;->onboarding_shipping_confirm_later:I

    sget v3, Lcom/squareup/common/strings/R$string;->continue_label:I

    sget v4, Lcom/squareup/common/strings/R$string;->cancel:I

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/register/widgets/ConfirmationIds;-><init>(IIII)V

    .line 155
    iget-object v1, p0, Lcom/squareup/ui/onboarding/MerchantCategoryScreen$Presenter;->confirmLaterPopupPresenter:Lcom/squareup/mortar/PopupPresenter;

    invoke-virtual {v1, v0}, Lcom/squareup/mortar/PopupPresenter;->show(Landroid/os/Parcelable;)V

    .line 156
    iget-object v0, p0, Lcom/squareup/ui/onboarding/MerchantCategoryScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_MCC_LATER:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    return-void
.end method

.method private setBusinessCategories(Lcom/squareup/ui/onboarding/MerchantCategoryView;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/onboarding/MerchantCategoryView;",
            "Ljava/util/List<",
            "Lcom/squareup/server/activation/ActivationResources$BusinessCategory;",
            ">;)V"
        }
    .end annotation

    .line 113
    invoke-virtual {p1, p2}, Lcom/squareup/ui/onboarding/MerchantCategoryView;->setCategories(Ljava/util/List;)V

    .line 114
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/onboarding/MerchantCategoryScreen$Presenter;->setSelectedCategory(Lcom/squareup/ui/onboarding/MerchantCategoryView;Ljava/util/List;)V

    .line 115
    new-instance p2, Lcom/squareup/ui/onboarding/-$$Lambda$MerchantCategoryScreen$Presenter$crzguOStVrLjrTtUnFRlTGH0eSQ;

    invoke-direct {p2, p0}, Lcom/squareup/ui/onboarding/-$$Lambda$MerchantCategoryScreen$Presenter$crzguOStVrLjrTtUnFRlTGH0eSQ;-><init>(Lcom/squareup/ui/onboarding/MerchantCategoryScreen$Presenter;)V

    invoke-virtual {p1, p2}, Lcom/squareup/ui/onboarding/MerchantCategoryView;->setCategorySelectedListener(Lcom/squareup/widgets/CheckableGroup$OnCheckedChangeListener;)V

    return-void
.end method

.method private setSelectedCategory(Lcom/squareup/ui/onboarding/MerchantCategoryView;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/onboarding/MerchantCategoryView;",
            "Ljava/util/List<",
            "Lcom/squareup/server/activation/ActivationResources$BusinessCategory;",
            ">;)V"
        }
    .end annotation

    .line 122
    iget-object v0, p0, Lcom/squareup/ui/onboarding/MerchantCategoryScreen$Presenter;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/OnboardingModel;->getBusinessCategory()Lcom/squareup/server/activation/ActivationResources$BusinessCategory;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v1, 0x0

    .line 125
    :goto_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 126
    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/server/activation/ActivationResources$BusinessCategory;

    iget-object v2, v2, Lcom/squareup/server/activation/ActivationResources$BusinessCategory;->key:Ljava/lang/String;

    iget-object v3, v0, Lcom/squareup/server/activation/ActivationResources$BusinessCategory;->key:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 127
    invoke-virtual {p1, v1}, Lcom/squareup/ui/onboarding/MerchantCategoryView;->setSelectedIndex(I)V

    goto :goto_1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    return-void
.end method


# virtual methods
.method public synthetic lambda$null$0$MerchantCategoryScreen$Presenter(Lio/reactivex/disposables/Disposable;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 93
    iget-object p1, p0, Lcom/squareup/ui/onboarding/MerchantCategoryScreen$Presenter;->spinner:Lcom/squareup/register/widgets/GlassSpinner;

    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/MerchantCategoryScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/onboarding/MerchantCategoryView;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/MerchantCategoryView;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x1

    .line 94
    invoke-static {v1}, Lcom/squareup/register/widgets/GlassSpinnerState;->showNonDebouncedSpinner(Z)Lcom/squareup/register/widgets/GlassSpinnerState;

    move-result-object v1

    .line 93
    invoke-virtual {p1, v0, v1}, Lcom/squareup/register/widgets/GlassSpinner;->setSpinnerState(Landroid/content/Context;Lcom/squareup/register/widgets/GlassSpinnerState;)V

    return-void
.end method

.method public synthetic lambda$null$1$MerchantCategoryScreen$Presenter(Lcom/squareup/server/activation/ActivationResources;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 97
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/MerchantCategoryScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/onboarding/MerchantCategoryView;

    invoke-virtual {p1}, Lcom/squareup/server/activation/ActivationResources;->getBusinessCategories()Ljava/util/List;

    move-result-object p1

    invoke-direct {p0, v0, p1}, Lcom/squareup/ui/onboarding/MerchantCategoryScreen$Presenter;->setBusinessCategories(Lcom/squareup/ui/onboarding/MerchantCategoryView;Ljava/util/List;)V

    .line 98
    iget-object p1, p0, Lcom/squareup/ui/onboarding/MerchantCategoryScreen$Presenter;->spinner:Lcom/squareup/register/widgets/GlassSpinner;

    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/MerchantCategoryScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/onboarding/MerchantCategoryView;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/MerchantCategoryView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/register/widgets/GlassSpinner;->clearSpinnerState(Landroid/content/Context;)V

    return-void
.end method

.method public synthetic lambda$onLoad$2$MerchantCategoryScreen$Presenter()Lio/reactivex/disposables/Disposable;
    .locals 2

    .line 91
    iget-object v0, p0, Lcom/squareup/ui/onboarding/MerchantCategoryScreen$Presenter;->activationResourcesService:Lcom/squareup/onboarding/ActivationResourcesService;

    .line 92
    invoke-virtual {v0}, Lcom/squareup/onboarding/ActivationResourcesService;->getActivationResources()Lio/reactivex/observables/ConnectableObservable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/onboarding/-$$Lambda$MerchantCategoryScreen$Presenter$gz1lSNif0SYPoTkO-G2STyDlrT4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/onboarding/-$$Lambda$MerchantCategoryScreen$Presenter$gz1lSNif0SYPoTkO-G2STyDlrT4;-><init>(Lcom/squareup/ui/onboarding/MerchantCategoryScreen$Presenter;)V

    .line 93
    invoke-virtual {v0, v1}, Lio/reactivex/observables/ConnectableObservable;->doOnSubscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v0

    .line 95
    invoke-static {}, Lcom/squareup/receiving/StandardReceiver;->filterSuccess()Lio/reactivex/ObservableTransformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/onboarding/-$$Lambda$MerchantCategoryScreen$Presenter$SA8KHrP2aQrM2VNx40vU2GXaKXM;

    invoke-direct {v1, p0}, Lcom/squareup/ui/onboarding/-$$Lambda$MerchantCategoryScreen$Presenter$SA8KHrP2aQrM2VNx40vU2GXaKXM;-><init>(Lcom/squareup/ui/onboarding/MerchantCategoryScreen$Presenter;)V

    .line 96
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    return-object v0
.end method

.method public synthetic lambda$setBusinessCategories$3$MerchantCategoryScreen$Presenter(Lcom/squareup/widgets/CheckableGroup;II)V
    .locals 0

    .line 116
    iget-object p1, p0, Lcom/squareup/ui/onboarding/MerchantCategoryScreen$Presenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object p2, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_MCC_RADIO:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {p1, p2}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .line 160
    invoke-direct {p0}, Lcom/squareup/ui/onboarding/MerchantCategoryScreen$Presenter;->onSkipButtonTapped()V

    const/4 v0, 0x1

    return v0
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 3

    .line 78
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 80
    new-instance p1, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    iget-object v0, p0, Lcom/squareup/ui/onboarding/MerchantCategoryScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/onboarding/flow/R$string;->onboarding_actionbar_continue:I

    .line 82
    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 81
    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/onboarding/-$$Lambda$MerchantCategoryScreen$Presenter$5Kbm_OhT289Y4SjQTCfouGTqBDw;

    invoke-direct {v0, p0}, Lcom/squareup/ui/onboarding/-$$Lambda$MerchantCategoryScreen$Presenter$5Kbm_OhT289Y4SjQTCfouGTqBDw;-><init>(Lcom/squareup/ui/onboarding/MerchantCategoryScreen$Presenter;)V

    .line 83
    invoke-virtual {p1, v0}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 84
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->hideUpButton()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    const/4 v1, 0x1

    .line 85
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setSecondaryButtonEnabled(Z)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/onboarding/MerchantCategoryScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/onboarding/flow/R$string;->onboarding_actionbar_later:I

    .line 86
    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setSecondaryButtonTextNoGlyph(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/onboarding/-$$Lambda$MerchantCategoryScreen$Presenter$N0IyxJZVlFz14G8MQLgXJice96g;

    invoke-direct {v1, p0}, Lcom/squareup/ui/onboarding/-$$Lambda$MerchantCategoryScreen$Presenter$N0IyxJZVlFz14G8MQLgXJice96g;-><init>(Lcom/squareup/ui/onboarding/MerchantCategoryScreen$Presenter;)V

    .line 87
    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showSecondaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 89
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/MerchantCategoryScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/onboarding/MerchantCategoryView;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/MerchantCategoryView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 91
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/MerchantCategoryScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    new-instance v0, Lcom/squareup/ui/onboarding/-$$Lambda$MerchantCategoryScreen$Presenter$xr5c0iieHJpnho8WGfJGHC96Czk;

    invoke-direct {v0, p0}, Lcom/squareup/ui/onboarding/-$$Lambda$MerchantCategoryScreen$Presenter$xr5c0iieHJpnho8WGfJGHC96Czk;-><init>(Lcom/squareup/ui/onboarding/MerchantCategoryScreen$Presenter;)V

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method

.method protected onSave(Landroid/os/Bundle;)V
    .locals 1

    .line 103
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onSave(Landroid/os/Bundle;)V

    .line 105
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/MerchantCategoryScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/onboarding/MerchantCategoryView;

    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/MerchantCategoryView;->getSelected()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/server/activation/ActivationResources$BusinessCategory;

    if-eqz p1, :cond_0

    .line 107
    iget-object v0, p0, Lcom/squareup/ui/onboarding/MerchantCategoryScreen$Presenter;->model:Lcom/squareup/ui/onboarding/OnboardingModel;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/onboarding/OnboardingModel;->setBusinessCategory(Lcom/squareup/server/activation/ActivationResources$BusinessCategory;)V

    :cond_0
    return-void
.end method
