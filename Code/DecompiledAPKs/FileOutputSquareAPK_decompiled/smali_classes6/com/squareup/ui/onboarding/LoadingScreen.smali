.class public Lcom/squareup/ui/onboarding/LoadingScreen;
.super Lcom/squareup/ui/onboarding/InLoggedInOnboardingScope;
.source "LoadingScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/onboarding/LoadingScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/onboarding/LoadingScreen$Component;,
        Lcom/squareup/ui/onboarding/LoadingScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/onboarding/LoadingScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/onboarding/LoadingScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 17
    new-instance v0, Lcom/squareup/ui/onboarding/LoadingScreen;

    invoke-direct {v0}, Lcom/squareup/ui/onboarding/LoadingScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/onboarding/LoadingScreen;->INSTANCE:Lcom/squareup/ui/onboarding/LoadingScreen;

    .line 22
    sget-object v0, Lcom/squareup/ui/onboarding/LoadingScreen;->INSTANCE:Lcom/squareup/ui/onboarding/LoadingScreen;

    .line 23
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/onboarding/LoadingScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 19
    invoke-direct {p0}, Lcom/squareup/ui/onboarding/InLoggedInOnboardingScope;-><init>()V

    return-void
.end method


# virtual methods
.method public screenLayout()I
    .locals 1

    .line 47
    sget v0, Lcom/squareup/onboarding/flow/R$layout;->loading_view:I

    return v0
.end method
