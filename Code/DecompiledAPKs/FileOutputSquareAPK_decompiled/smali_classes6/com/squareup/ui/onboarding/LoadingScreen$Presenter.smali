.class public Lcom/squareup/ui/onboarding/LoadingScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "LoadingScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/onboarding/LoadingScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/onboarding/LoadingView;",
        ">;"
    }
.end annotation


# instance fields
.field final serverCallPresenter:Lcom/squareup/ui/onboarding/LoadingStatusPresenter;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/onboarding/LoadingStatusPresenter;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 31
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/squareup/ui/onboarding/LoadingScreen$Presenter;->serverCallPresenter:Lcom/squareup/ui/onboarding/LoadingStatusPresenter;

    return-void
.end method


# virtual methods
.method onBackPressed()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
