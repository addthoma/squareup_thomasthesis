.class public final Lcom/squareup/ui/onboarding/OnboardingClientActionTranslator;
.super Ljava/lang/Object;
.source "OnboardingClientActionTranslator.kt"

# interfaces
.implements Lcom/squareup/clientactiontranslation/ClientActionTranslator;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nOnboardingClientActionTranslator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 OnboardingClientActionTranslator.kt\ncom/squareup/ui/onboarding/OnboardingClientActionTranslator\n*L\n1#1,36:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/ui/onboarding/OnboardingClientActionTranslator;",
        "Lcom/squareup/clientactiontranslation/ClientActionTranslator;",
        "()V",
        "translate",
        "Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse;",
        "clientAction",
        "Lcom/squareup/protos/client/ClientAction;",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public translate(Lcom/squareup/protos/client/ClientAction;)Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse;
    .locals 2

    const-string v0, "clientAction"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    iget-object v0, p1, Lcom/squareup/protos/client/ClientAction;->activate_account:Lcom/squareup/protos/client/ClientAction$ActivateAccount;

    if-nez v0, :cond_1

    iget-object v0, p1, Lcom/squareup/protos/client/ClientAction;->finish_account_setup:Lcom/squareup/protos/client/ClientAction$FinishAccountSetup;

    if-nez v0, :cond_1

    iget-object p1, p1, Lcom/squareup/protos/client/ClientAction;->link_bank_account:Lcom/squareup/protos/client/ClientAction$LinkBankAccount;

    if-eqz p1, :cond_0

    goto :goto_0

    .line 32
    :cond_0
    sget-object p1, Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse$Unhandled;->INSTANCE:Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse$Unhandled;

    check-cast p1, Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse;

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x3

    new-array p1, p1, [Ljava/lang/Object;

    const/4 v0, 0x0

    const-string v1, "square-register"

    aput-object v1, p1, v0

    const/4 v0, 0x1

    const-string v1, "settings"

    aput-object v1, p1, v0

    const/4 v0, 0x2

    const-string v1, "/activate"

    aput-object v1, p1, v0

    .line 27
    array-length v0, p1

    invoke-static {p1, v0}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object p1

    const-string v0, "%s://%s%s"

    invoke-static {v0, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "java.lang.String.format(this, *args)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    new-instance v0, Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse$Handled;

    invoke-direct {v0, p1}, Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse$Handled;-><init>(Ljava/lang/String;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/clientactiontranslation/ClientActionTranslatorResponse;

    :goto_1
    return-object p1
.end method
