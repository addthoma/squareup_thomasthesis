.class public final Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;
.super Ljava/lang/Object;
.source "DepositOptionsScreenWorkflow.kt"

# interfaces
.implements Lcom/squareup/container/PosWorkflowStarter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$Factory;,
        Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$OnboardingFactory;,
        Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$MainActivityFactory;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u009a\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001:\u0003/01B5\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u00a2\u0006\u0002\u0010\u0010J\u0010\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014H\u0002J\u0010\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0013\u001a\u00020\u0014H\u0002J\u0008\u0010\u0017\u001a\u00020\u0018H\u0002J\u0010\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u0013\u001a\u00020\u0014H\u0002Jd\u0010\u001b\u001aD\u00120\u0012.\u0012*\u0012(\u0012\u0006\u0008\u0001\u0012\u00020\u001f\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030 j\u0002`!0\u001ej\n\u0012\u0006\u0008\u0001\u0012\u00020\u001f`\"0\u001d\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u001cj\u0002`#2\u0018\u0010$\u001a\u0014\u0012\u0004\u0012\u00020\u0014\u0012\u0004\u0012\u00020&\u0012\u0004\u0012\u00020\u00030%H\u0002J\u0012\u0010\'\u001a\u00020\u00072\u0008\u0008\u0001\u0010(\u001a\u00020)H\u0002J\u0010\u0010*\u001a\u00020+2\u0006\u0010\u0013\u001a\u00020\u0014H\u0002JT\u0010,\u001aD\u00120\u0012.\u0012*\u0012(\u0012\u0006\u0008\u0001\u0012\u00020\u001f\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030 j\u0002`!0\u001ej\n\u0012\u0006\u0008\u0001\u0012\u00020\u001f`\"0\u001d\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u001cj\u0002`#2\u0008\u0010-\u001a\u0004\u0018\u00010.H\u0016R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u00062"
    }
    d2 = {
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;",
        "Lcom/squareup/container/PosWorkflowStarter;",
        "",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsResult;",
        "reactor",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;",
        "personalName",
        "",
        "settings",
        "Lcom/squareup/settings/server/AccountStatusSettings;",
        "res",
        "Lcom/squareup/util/Res;",
        "rateFormatter",
        "Lcom/squareup/text/RateFormatter;",
        "config",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowConfig;",
        "(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;Ljava/lang/String;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/util/Res;Lcom/squareup/text/RateFormatter;Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowConfig;)V",
        "depositBankLinkingScreenData",
        "Lcom/squareup/ui/onboarding/bank/DepositBankLinking$ScreenData;",
        "state",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;",
        "depositCardLinkingScreenData",
        "Lcom/squareup/ui/onboarding/bank/DepositCardLinking$ScreenData;",
        "depositSpeedScreenData",
        "Lcom/squareup/ui/onboarding/bank/DepositSpeedScreenData;",
        "depositsSettingsResultScreenData",
        "Lcom/squareup/ui/onboarding/bank/DepositLinkingResult$ScreenData;",
        "doStart",
        "Lcom/squareup/workflow/legacy/Workflow;",
        "Lcom/squareup/workflow/ScreenState;",
        "",
        "Lcom/squareup/container/ContainerLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflow;",
        "workflow",
        "Lcom/squareup/workflow/rx1/Workflow;",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent;",
        "formatSameDayDepositFee",
        "resId",
        "",
        "messageDialogScreenData",
        "Lcom/squareup/widgets/warning/Warning;",
        "start",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "Factory",
        "MainActivityFactory",
        "OnboardingFactory",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final config:Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowConfig;

.field private final personalName:Ljava/lang/String;

.field private final rateFormatter:Lcom/squareup/text/RateFormatter;

.field private final reactor:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;

.field private final res:Lcom/squareup/util/Res;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;Ljava/lang/String;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/util/Res;Lcom/squareup/text/RateFormatter;Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowConfig;)V
    .locals 1

    const-string v0, "reactor"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "personalName"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "settings"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rateFormatter"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "config"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;->reactor:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;

    iput-object p2, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;->personalName:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    iput-object p4, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;->res:Lcom/squareup/util/Res;

    iput-object p5, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;->rateFormatter:Lcom/squareup/text/RateFormatter;

    iput-object p6, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;->config:Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowConfig;

    return-void
.end method

.method public static final synthetic access$depositBankLinkingScreenData(Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;)Lcom/squareup/ui/onboarding/bank/DepositBankLinking$ScreenData;
    .locals 0

    .line 93
    invoke-direct {p0, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;->depositBankLinkingScreenData(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;)Lcom/squareup/ui/onboarding/bank/DepositBankLinking$ScreenData;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$depositCardLinkingScreenData(Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;)Lcom/squareup/ui/onboarding/bank/DepositCardLinking$ScreenData;
    .locals 0

    .line 93
    invoke-direct {p0, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;->depositCardLinkingScreenData(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;)Lcom/squareup/ui/onboarding/bank/DepositCardLinking$ScreenData;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$depositSpeedScreenData(Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;)Lcom/squareup/ui/onboarding/bank/DepositSpeedScreenData;
    .locals 0

    .line 93
    invoke-direct {p0}, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;->depositSpeedScreenData()Lcom/squareup/ui/onboarding/bank/DepositSpeedScreenData;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$depositsSettingsResultScreenData(Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;)Lcom/squareup/ui/onboarding/bank/DepositLinkingResult$ScreenData;
    .locals 0

    .line 93
    invoke-direct {p0, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;->depositsSettingsResultScreenData(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;)Lcom/squareup/ui/onboarding/bank/DepositLinkingResult$ScreenData;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic access$messageDialogScreenData(Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;)Lcom/squareup/widgets/warning/Warning;
    .locals 0

    .line 93
    invoke-direct {p0, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;->messageDialogScreenData(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;)Lcom/squareup/widgets/warning/Warning;

    move-result-object p0

    return-object p0
.end method

.method private final depositBankLinkingScreenData(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;)Lcom/squareup/ui/onboarding/bank/DepositBankLinking$ScreenData;
    .locals 4

    .line 258
    instance-of v0, p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/ui/onboarding/bank/DepositBankLinking$ScreenData;

    .line 259
    iget-object v1, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;->personalName:Ljava/lang/String;

    .line 260
    move-object v2, p1

    check-cast v2, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking;

    invoke-virtual {v2}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking;->getBankAccountType()Lcom/squareup/protos/client/bankaccount/BankAccountType;

    move-result-object v2

    .line 261
    instance-of p1, p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$LinkingBankAccount;

    .line 262
    iget-object v3, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;->config:Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowConfig;

    invoke-virtual {v3}, Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowConfig;->getCanSelectDepositSpeed()Z

    move-result v3

    .line 258
    invoke-direct {v0, v1, v2, p1, v3}, Lcom/squareup/ui/onboarding/bank/DepositBankLinking$ScreenData;-><init>(Ljava/lang/String;Lcom/squareup/protos/client/bankaccount/BankAccountType;ZZ)V

    return-object v0

    .line 264
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 265
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Can\'t convert "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " to DepositBankLinking.ScreenData"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 264
    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method private final depositCardLinkingScreenData(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;)Lcom/squareup/ui/onboarding/bank/DepositCardLinking$ScreenData;
    .locals 3

    .line 272
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v0

    const-string v1, "settings.userSettings"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/settings/server/UserSettings;->getCountryCode()Lcom/squareup/CountryCode;

    move-result-object v0

    .line 274
    instance-of v1, p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$CardLinking$PrepareToLinkDebitCard;

    if-eqz v1, :cond_0

    goto :goto_0

    .line 275
    :cond_0
    instance-of v1, p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult;

    if-eqz v1, :cond_1

    :goto_0
    const/4 p1, 0x0

    goto :goto_2

    .line 276
    :cond_1
    instance-of v1, p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$CardLinking$LinkingDebitCard;

    if-eqz v1, :cond_2

    goto :goto_1

    .line 277
    :cond_2
    instance-of v1, p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$CardLinking$SettingUpSameDayDeposit;

    if-eqz v1, :cond_3

    :goto_1
    const/4 p1, 0x1

    .line 282
    :goto_2
    new-instance v1, Lcom/squareup/ui/onboarding/bank/DepositCardLinking$ScreenData;

    const-string v2, "countryCode"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v1, v0, p1}, Lcom/squareup/ui/onboarding/bank/DepositCardLinking$ScreenData;-><init>(Lcom/squareup/CountryCode;Z)V

    return-object v1

    .line 278
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 279
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Can\'t convert "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " to DepositCardLinking.ScreenData"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 278
    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method private final depositSpeedScreenData()Lcom/squareup/ui/onboarding/bank/DepositSpeedScreenData;
    .locals 3

    .line 249
    new-instance v0, Lcom/squareup/ui/onboarding/bank/DepositSpeedScreenData;

    .line 250
    sget v1, Lcom/squareup/onboarding/flow/R$string;->same_day_deposit_description:I

    invoke-direct {p0, v1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;->formatSameDayDepositFee(I)Ljava/lang/String;

    move-result-object v1

    .line 251
    iget-object v2, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;->config:Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowConfig;

    invoke-virtual {v2}, Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowConfig;->getCanGoBackToExit()Z

    move-result v2

    .line 249
    invoke-direct {v0, v1, v2}, Lcom/squareup/ui/onboarding/bank/DepositSpeedScreenData;-><init>(Ljava/lang/String;Z)V

    return-object v0
.end method

.method private final depositsSettingsResultScreenData(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;)Lcom/squareup/ui/onboarding/bank/DepositLinkingResult$ScreenData;
    .locals 10

    .line 288
    sget-object v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankSuccess;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankSuccess;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance p1, Lcom/squareup/ui/onboarding/bank/DepositLinkingResult$ScreenData;

    .line 289
    sget v2, Lcom/squareup/onboarding/common/R$string;->add_bank_account:I

    .line 290
    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_CHECK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 291
    sget v4, Lcom/squareup/banklinking/R$string;->bank_account_linked:I

    const/4 v5, -0x1

    .line 293
    sget v6, Lcom/squareup/onboarding/flow/R$string;->onboarding_actionbar_continue:I

    const/4 v7, 0x0

    const/16 v8, 0x20

    const/4 v9, 0x0

    move-object v1, p1

    .line 288
    invoke-direct/range {v1 .. v9}, Lcom/squareup/ui/onboarding/bank/DepositLinkingResult$ScreenData;-><init>(ILcom/squareup/glyph/GlyphTypeface$Glyph;IIIIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    goto/16 :goto_0

    .line 295
    :cond_0
    sget-object v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankSuccessCardSkip;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankSuccessCardSkip;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance p1, Lcom/squareup/ui/onboarding/bank/DepositLinkingResult$ScreenData;

    .line 296
    sget v2, Lcom/squareup/debitcard/R$string;->instant_deposits_link_debit_card:I

    .line 297
    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 298
    sget v4, Lcom/squareup/onboarding/flow/R$string;->missing_debit_card_info:I

    .line 299
    sget v5, Lcom/squareup/onboarding/flow/R$string;->missing_debit_card_info_message:I

    .line 300
    sget v6, Lcom/squareup/onboarding/flow/R$string;->skip_for_now:I

    const/4 v7, 0x0

    const/16 v8, 0x20

    const/4 v9, 0x0

    move-object v1, p1

    .line 295
    invoke-direct/range {v1 .. v9}, Lcom/squareup/ui/onboarding/bank/DepositLinkingResult$ScreenData;-><init>(ILcom/squareup/glyph/GlyphTypeface$Glyph;IIIIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    goto/16 :goto_0

    .line 302
    :cond_1
    sget-object v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankSuccessCardFailure;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankSuccessCardFailure;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance p1, Lcom/squareup/ui/onboarding/bank/DepositLinkingResult$ScreenData;

    .line 303
    sget v2, Lcom/squareup/onboarding/flow/R$string;->setup_failed:I

    .line 304
    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 305
    sget v4, Lcom/squareup/onboarding/flow/R$string;->card_not_supported:I

    .line 306
    sget v5, Lcom/squareup/onboarding/flow/R$string;->card_not_supported_bank_success_message:I

    .line 307
    sget v6, Lcom/squareup/onboarding/flow/R$string;->skip_for_now:I

    const/4 v7, 0x0

    const/16 v8, 0x20

    const/4 v9, 0x0

    move-object v1, p1

    .line 302
    invoke-direct/range {v1 .. v9}, Lcom/squareup/ui/onboarding/bank/DepositLinkingResult$ScreenData;-><init>(ILcom/squareup/glyph/GlyphTypeface$Glyph;IIIIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    goto/16 :goto_0

    .line 309
    :cond_2
    sget-object v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankSuccessCardSuccessSameDayDepositSetupSuccess;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankSuccessCardSuccessSameDayDepositSetupSuccess;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance p1, Lcom/squareup/ui/onboarding/bank/DepositLinkingResult$ScreenData;

    .line 310
    sget v2, Lcom/squareup/onboarding/flow/R$string;->you_are_set_up_for_same_day_deposits:I

    .line 311
    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_CHECK:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 312
    sget v4, Lcom/squareup/onboarding/flow/R$string;->you_are_set_up_for_same_day_deposits:I

    const/4 v5, -0x1

    .line 314
    sget v6, Lcom/squareup/onboarding/flow/R$string;->onboarding_actionbar_continue:I

    .line 315
    sget v7, Lcom/squareup/onboarding/flow/R$string;->you_are_set_up_for_same_day_deposits_footer:I

    move-object v1, p1

    .line 309
    invoke-direct/range {v1 .. v7}, Lcom/squareup/ui/onboarding/bank/DepositLinkingResult$ScreenData;-><init>(ILcom/squareup/glyph/GlyphTypeface$Glyph;IIII)V

    goto/16 :goto_0

    .line 317
    :cond_3
    sget-object v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankSuccessCardSuccessSameDayDepositSetupFailure;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankSuccessCardSuccessSameDayDepositSetupFailure;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance p1, Lcom/squareup/ui/onboarding/bank/DepositLinkingResult$ScreenData;

    .line 318
    sget v2, Lcom/squareup/onboarding/flow/R$string;->setup_failed:I

    .line 319
    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 320
    sget v4, Lcom/squareup/onboarding/flow/R$string;->could_not_enable_same_day_deposit:I

    .line 321
    sget v5, Lcom/squareup/onboarding/flow/R$string;->could_not_enable_same_day_deposit_message:I

    .line 322
    sget v6, Lcom/squareup/onboarding/flow/R$string;->onboarding_actionbar_continue:I

    const/4 v7, 0x0

    const/16 v8, 0x20

    const/4 v9, 0x0

    move-object v1, p1

    .line 317
    invoke-direct/range {v1 .. v9}, Lcom/squareup/ui/onboarding/bank/DepositLinkingResult$ScreenData;-><init>(ILcom/squareup/glyph/GlyphTypeface$Glyph;IIIIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    goto/16 :goto_0

    .line 324
    :cond_4
    sget-object v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankPending;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankPending;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    new-instance p1, Lcom/squareup/ui/onboarding/bank/DepositLinkingResult$ScreenData;

    .line 325
    sget v2, Lcom/squareup/onboarding/flow/R$string;->awaiting_verification:I

    .line 326
    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_ENVELOPE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 327
    sget v4, Lcom/squareup/onboarding/flow/R$string;->awaiting_bank_account_verification:I

    .line 328
    sget v5, Lcom/squareup/onboarding/flow/R$string;->awaiting_bank_account_verification_message:I

    .line 329
    sget v6, Lcom/squareup/onboarding/flow/R$string;->onboarding_actionbar_continue:I

    const/4 v7, 0x0

    const/16 v8, 0x20

    const/4 v9, 0x0

    move-object v1, p1

    .line 324
    invoke-direct/range {v1 .. v9}, Lcom/squareup/ui/onboarding/bank/DepositLinkingResult$ScreenData;-><init>(ILcom/squareup/glyph/GlyphTypeface$Glyph;IIIIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    goto/16 :goto_0

    .line 331
    :cond_5
    sget-object v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankPendingCardSkip;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankPendingCardSkip;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    new-instance p1, Lcom/squareup/ui/onboarding/bank/DepositLinkingResult$ScreenData;

    .line 332
    sget v2, Lcom/squareup/onboarding/flow/R$string;->awaiting_verification:I

    .line 333
    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_ENVELOPE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 334
    sget v4, Lcom/squareup/onboarding/flow/R$string;->awaiting_bank_account_verification:I

    .line 335
    sget v5, Lcom/squareup/onboarding/flow/R$string;->awaiting_bank_account_verification_card_skip_message:I

    .line 336
    sget v6, Lcom/squareup/onboarding/flow/R$string;->onboarding_actionbar_continue:I

    const/4 v7, 0x0

    const/16 v8, 0x20

    const/4 v9, 0x0

    move-object v1, p1

    .line 331
    invoke-direct/range {v1 .. v9}, Lcom/squareup/ui/onboarding/bank/DepositLinkingResult$ScreenData;-><init>(ILcom/squareup/glyph/GlyphTypeface$Glyph;IIIIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    goto/16 :goto_0

    .line 338
    :cond_6
    sget-object v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankPendingCardFailure;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankPendingCardFailure;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    new-instance p1, Lcom/squareup/ui/onboarding/bank/DepositLinkingResult$ScreenData;

    .line 339
    sget v2, Lcom/squareup/debitcard/R$string;->instant_deposits_link_debit_card:I

    .line 340
    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 341
    sget v4, Lcom/squareup/onboarding/flow/R$string;->card_not_supported:I

    .line 342
    sget v5, Lcom/squareup/onboarding/flow/R$string;->card_not_supported_bank_pending_message:I

    .line 343
    sget v6, Lcom/squareup/onboarding/flow/R$string;->onboarding_actionbar_continue:I

    const/4 v7, 0x0

    const/16 v8, 0x20

    const/4 v9, 0x0

    move-object v1, p1

    .line 338
    invoke-direct/range {v1 .. v9}, Lcom/squareup/ui/onboarding/bank/DepositLinkingResult$ScreenData;-><init>(ILcom/squareup/glyph/GlyphTypeface$Glyph;IIIIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    goto :goto_0

    .line 345
    :cond_7
    sget-object v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankPendingCardSuccessSameDayDepositSetupSuccess;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankPendingCardSuccessSameDayDepositSetupSuccess;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    new-instance p1, Lcom/squareup/ui/onboarding/bank/DepositLinkingResult$ScreenData;

    .line 346
    sget v2, Lcom/squareup/onboarding/flow/R$string;->awaiting_verification:I

    .line 347
    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_ENVELOPE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 348
    sget v4, Lcom/squareup/onboarding/flow/R$string;->awaiting_bank_account_verification:I

    .line 349
    sget v5, Lcom/squareup/onboarding/flow/R$string;->awaiting_bank_account_verification_schedule_success_message:I

    .line 350
    sget v6, Lcom/squareup/onboarding/flow/R$string;->onboarding_actionbar_continue:I

    const/4 v7, 0x0

    const/16 v8, 0x20

    const/4 v9, 0x0

    move-object v1, p1

    .line 345
    invoke-direct/range {v1 .. v9}, Lcom/squareup/ui/onboarding/bank/DepositLinkingResult$ScreenData;-><init>(ILcom/squareup/glyph/GlyphTypeface$Glyph;IIIIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    goto :goto_0

    .line 352
    :cond_8
    sget-object v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankPendingCardSuccessSameDayDepositSetupFailure;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankPendingCardSuccessSameDayDepositSetupFailure;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    new-instance p1, Lcom/squareup/ui/onboarding/bank/DepositLinkingResult$ScreenData;

    .line 353
    sget v2, Lcom/squareup/onboarding/flow/R$string;->awaiting_verification:I

    .line 354
    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_ENVELOPE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 355
    sget v4, Lcom/squareup/onboarding/flow/R$string;->awaiting_bank_account_verification:I

    .line 356
    sget v5, Lcom/squareup/onboarding/flow/R$string;->awaiting_bank_account_verification_schedule_failure_message:I

    .line 357
    sget v6, Lcom/squareup/onboarding/flow/R$string;->onboarding_actionbar_continue:I

    const/4 v7, 0x0

    const/16 v8, 0x20

    const/4 v9, 0x0

    move-object v1, p1

    .line 352
    invoke-direct/range {v1 .. v9}, Lcom/squareup/ui/onboarding/bank/DepositLinkingResult$ScreenData;-><init>(ILcom/squareup/glyph/GlyphTypeface$Glyph;IIIIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    goto :goto_0

    .line 359
    :cond_9
    sget-object v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankFailure;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankFailure;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    new-instance p1, Lcom/squareup/ui/onboarding/bank/DepositLinkingResult$ScreenData;

    .line 360
    sget v2, Lcom/squareup/onboarding/flow/R$string;->setup_failed:I

    .line 361
    sget-object v3, Lcom/squareup/glyph/GlyphTypeface$Glyph;->CIRCLE_WARNING:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 362
    sget v4, Lcom/squareup/onboarding/flow/R$string;->link_bank_account_failed:I

    const/4 v5, -0x1

    .line 364
    sget v6, Lcom/squareup/onboarding/flow/R$string;->skip_for_now:I

    const/4 v7, 0x0

    const/16 v8, 0x20

    const/4 v9, 0x0

    move-object v1, p1

    .line 359
    invoke-direct/range {v1 .. v9}, Lcom/squareup/ui/onboarding/bank/DepositLinkingResult$ScreenData;-><init>(ILcom/squareup/glyph/GlyphTypeface$Glyph;IIIIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    :goto_0
    return-object p1

    .line 366
    :cond_a
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 367
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Can\'t convert "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " to DepositResult.ScreenData"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 366
    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method private final doStart(Lcom/squareup/workflow/rx1/Workflow;)Lcom/squareup/workflow/legacy/Workflow;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/rx1/Workflow<",
            "+",
            "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;",
            "-",
            "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent;",
            "+",
            "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsResult;",
            ">;)",
            "Lcom/squareup/workflow/legacy/Workflow<",
            "Lcom/squareup/workflow/ScreenState<",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;*",
            "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsResult;",
            ">;"
        }
    .end annotation

    .line 144
    sget-object v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$depositSpeedInput$1;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$depositSpeedInput$1;

    check-cast v0, Lkotlin/jvm/functions/Function1;

    invoke-static {p1, v0}, Lcom/squareup/workflow/rx1/WorkflowKt;->adaptEvents(Lcom/squareup/workflow/rx1/Workflow;Lkotlin/jvm/functions/Function1;)Lcom/squareup/workflow/rx1/Workflow;

    move-result-object v0

    check-cast v0, Lcom/squareup/workflow/legacy/WorkflowInput;

    .line 153
    new-instance v1, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$1;

    invoke-direct {v1, p0, v0, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$1;-><init>(Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;Lcom/squareup/workflow/legacy/WorkflowInput;Lcom/squareup/workflow/rx1/Workflow;)V

    .line 194
    new-instance v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$2;

    invoke-direct {v0, p0, v1, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$2;-><init>(Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$1;Lcom/squareup/workflow/rx1/Workflow;)V

    .line 239
    invoke-static {p1}, Lcom/squareup/workflow/rx1/WorkflowKt;->toCoreWorkflow(Lcom/squareup/workflow/rx1/Workflow;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p1

    .line 240
    new-instance v1, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$3;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$3;-><init>(Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter$doStart$2;Lkotlin/coroutines/Continuation;)V

    check-cast v1, Lkotlin/jvm/functions/Function2;

    invoke-static {p1, v1}, Lcom/squareup/workflow/legacy/WorkflowOperatorsKt;->mapState(Lcom/squareup/workflow/legacy/Workflow;Lkotlin/jvm/functions/Function2;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p1

    return-object p1
.end method

.method private final formatSameDayDepositFee(I)Ljava/lang/String;
    .locals 3

    .line 415
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->getInstantDepositsSettings()Lcom/squareup/settings/server/InstantDepositsSettings;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/settings/server/InstantDepositsSettings;->sameDayDepositFeeAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    .line 416
    iget-object v1, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getInstantDepositsSettings()Lcom/squareup/settings/server/InstantDepositsSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/settings/server/InstantDepositsSettings;->sameDayDepositFeePercentage()Lcom/squareup/util/Percentage;

    move-result-object v1

    .line 417
    iget-object v2, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;->res:Lcom/squareup/util/Res;

    invoke-interface {v2, p1}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 418
    iget-object v2, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;->rateFormatter:Lcom/squareup/text/RateFormatter;

    invoke-virtual {v2, v0, v1}, Lcom/squareup/text/RateFormatter;->format(Lcom/squareup/protos/common/Money;Lcom/squareup/util/Percentage;)Ljava/lang/CharSequence;

    move-result-object v0

    const-string v1, "percent_or_amount"

    invoke-virtual {p1, v1, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p1

    .line 419
    invoke-virtual {p1}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p1

    .line 420
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private final messageDialogScreenData(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState;)Lcom/squareup/widgets/warning/Warning;
    .locals 3

    .line 372
    instance-of v0, p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$UpdatingDepositSpeed$OnPhone$ConfirmingLater;

    if-eqz v0, :cond_0

    goto :goto_0

    .line 373
    :cond_0
    instance-of v0, p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$UpdatingDepositSpeed$OnTablet$ConfirmingLater;

    if-eqz v0, :cond_1

    goto :goto_0

    .line 374
    :cond_1
    instance-of v0, p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$ConfirmingLater;

    if-eqz v0, :cond_2

    :goto_0
    new-instance p1, Lcom/squareup/widgets/warning/WarningIds;

    .line 375
    sget v0, Lcom/squareup/onboarding/flow/R$string;->onboarding_shipping_confirm_have_reader_title:I

    .line 376
    sget v1, Lcom/squareup/onboarding/flow/R$string;->onboarding_bank_account_confirm_later:I

    .line 374
    invoke-direct {p1, v0, v1}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    check-cast p1, Lcom/squareup/widgets/warning/Warning;

    goto/16 :goto_3

    .line 378
    :cond_2
    instance-of v0, p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$UpdatingDepositSpeed$OnPhone$ConfirmingSameDayDepositFee;

    if-eqz v0, :cond_3

    goto :goto_1

    .line 379
    :cond_3
    instance-of v0, p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$UpdatingDepositSpeed$OnTablet$ConfirmingSameDayDepositFee;

    if-eqz v0, :cond_4

    :goto_1
    new-instance p1, Lcom/squareup/widgets/warning/WarningStrings;

    .line 380
    sget v0, Lcom/squareup/onboarding/flow/R$string;->onboarding_confirm_same_day_deposit_fee_title:I

    invoke-direct {p0, v0}, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;->formatSameDayDepositFee(I)Ljava/lang/String;

    move-result-object v0

    .line 381
    sget v1, Lcom/squareup/onboarding/flow/R$string;->onboarding_confirm_same_day_deposit_fee_message:I

    invoke-direct {p0, v1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;->formatSameDayDepositFee(I)Ljava/lang/String;

    move-result-object v1

    .line 379
    invoke-direct {p1, v0, v1}, Lcom/squareup/widgets/warning/WarningStrings;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/widgets/warning/Warning;

    goto/16 :goto_3

    .line 383
    :cond_4
    instance-of v0, p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$HasInvalidInput;

    if-eqz v0, :cond_a

    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$HasInvalidInput;

    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$HasInvalidInput;->getInvalidInput()Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$InvalidInput;

    move-result-object p1

    .line 384
    instance-of v0, p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$InvalidInput$MissingAccountHolder;

    if-eqz v0, :cond_5

    new-instance p1, Lcom/squareup/widgets/warning/WarningIds;

    .line 385
    sget v0, Lcom/squareup/banklinking/R$string;->missing_required_field:I

    .line 386
    sget v1, Lcom/squareup/banklinking/R$string;->missing_account_holder:I

    .line 384
    invoke-direct {p1, v0, v1}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    goto :goto_2

    .line 388
    :cond_5
    instance-of v0, p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$InvalidInput$InvalidRoutingNumber;

    if-eqz v0, :cond_6

    new-instance p1, Lcom/squareup/widgets/warning/WarningIds;

    .line 389
    sget v0, Lcom/squareup/banklinking/R$string;->missing_required_field:I

    .line 390
    sget v1, Lcom/squareup/country/resources/R$string;->invalid_routing_message:I

    .line 388
    invoke-direct {p1, v0, v1}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    goto :goto_2

    .line 392
    :cond_6
    sget-object v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$InvalidInput$MissingAccountNumber;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$InvalidInput$MissingAccountNumber;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    new-instance p1, Lcom/squareup/widgets/warning/WarningIds;

    .line 393
    sget v0, Lcom/squareup/banklinking/R$string;->missing_required_field:I

    .line 394
    sget v1, Lcom/squareup/banklinking/R$string;->missing_account_number:I

    .line 392
    invoke-direct {p1, v0, v1}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    goto :goto_2

    .line 396
    :cond_7
    sget-object v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$InvalidInput$RoutingAndAccountNumbersMatch;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$InvalidInput$RoutingAndAccountNumbersMatch;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    new-instance p1, Lcom/squareup/widgets/warning/WarningIds;

    .line 397
    sget v0, Lcom/squareup/banklinking/R$string;->routing_account_number_match_headline:I

    .line 398
    sget v1, Lcom/squareup/banklinking/R$string;->routing_account_number_match_prompt:I

    .line 396
    invoke-direct {p1, v0, v1}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    goto :goto_2

    .line 400
    :cond_8
    sget-object v0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$InvalidInput$MissingAccountType;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$InvalidInput$MissingAccountType;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_9

    new-instance p1, Lcom/squareup/widgets/warning/WarningIds;

    .line 401
    sget v0, Lcom/squareup/banklinking/R$string;->missing_required_field:I

    .line 402
    sget v1, Lcom/squareup/banklinking/R$string;->missing_account_type:I

    .line 400
    invoke-direct {p1, v0, v1}, Lcom/squareup/widgets/warning/WarningIds;-><init>(II)V

    .line 383
    :goto_2
    check-cast p1, Lcom/squareup/widgets/warning/Warning;

    goto :goto_3

    .line 400
    :cond_9
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p1

    .line 405
    :cond_a
    instance-of v0, p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$BankWarning;

    if-eqz v0, :cond_b

    new-instance v0, Lcom/squareup/widgets/warning/WarningStrings;

    .line 406
    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$BankWarning;

    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$BankWarning;->getFailureMessage()Lcom/squareup/receiving/FailureMessage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/receiving/FailureMessage;->getTitle()Ljava/lang/String;

    move-result-object v1

    .line 407
    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$BankLinking$BankWarning;->getFailureMessage()Lcom/squareup/receiving/FailureMessage;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/receiving/FailureMessage;->getBody()Ljava/lang/String;

    move-result-object p1

    .line 405
    invoke-direct {v0, v1, p1}, Lcom/squareup/widgets/warning/WarningStrings;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object p1, v0

    check-cast p1, Lcom/squareup/widgets/warning/Warning;

    :goto_3
    return-object p1

    .line 409
    :cond_b
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 410
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Can\'t convert "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " to a Warning"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 409
    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method


# virtual methods
.method public start(Lcom/squareup/workflow/Snapshot;)Lcom/squareup/workflow/legacy/Workflow;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/Snapshot;",
            ")",
            "Lcom/squareup/workflow/legacy/Workflow<",
            "Lcom/squareup/workflow/ScreenState<",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;>;*",
            "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsResult;",
            ">;"
        }
    .end annotation

    .line 137
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;->reactor:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;->config:Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowConfig;

    invoke-virtual {v1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowConfig;->getCanSelectDepositSpeed()Z

    move-result v1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;->startWorkflow(Lcom/squareup/workflow/Snapshot;Z)Lcom/squareup/workflow/rx1/Workflow;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsScreenWorkflowStarter;->doStart(Lcom/squareup/workflow/rx1/Workflow;)Lcom/squareup/workflow/legacy/Workflow;

    move-result-object p1

    return-object p1
.end method
