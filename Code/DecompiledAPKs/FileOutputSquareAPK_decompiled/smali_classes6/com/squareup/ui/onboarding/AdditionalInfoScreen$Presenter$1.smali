.class Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter$1;
.super Lcom/squareup/mortar/PopupPresenter;
.source "AdditionalInfoScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter;-><init>(Lcom/squareup/util/Res;Lcom/squareup/ui/onboarding/OnboardingModel;Ljava/text/DateFormat;Lcom/squareup/ui/onboarding/ActivationStatusPresenter;Lcom/squareup/onboarding/ShareableReceivedResponse;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/mortar/PopupPresenter<",
        "Lcom/squareup/ui/SquareDate;",
        "Lcom/squareup/ui/SquareDate;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter;

.field final synthetic val$dateFormat:Ljava/text/DateFormat;

.field final synthetic val$model:Lcom/squareup/ui/onboarding/OnboardingModel;


# direct methods
.method constructor <init>(Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter;Lcom/squareup/ui/onboarding/OnboardingModel;Ljava/text/DateFormat;)V
    .locals 0

    .line 62
    iput-object p1, p0, Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter$1;->this$0:Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter;

    iput-object p2, p0, Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter$1;->val$model:Lcom/squareup/ui/onboarding/OnboardingModel;

    iput-object p3, p0, Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter$1;->val$dateFormat:Ljava/text/DateFormat;

    invoke-direct {p0}, Lcom/squareup/mortar/PopupPresenter;-><init>()V

    return-void
.end method


# virtual methods
.method protected onPopupResult(Lcom/squareup/ui/SquareDate;)V
    .locals 2

    if-eqz p1, :cond_0

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter$1;->val$model:Lcom/squareup/ui/onboarding/OnboardingModel;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/onboarding/OnboardingModel;->setBirthDate(Lcom/squareup/ui/SquareDate;)V

    .line 66
    iget-object v0, p0, Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter$1;->this$0:Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter;

    invoke-static {v0}, Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter;->access$000(Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/onboarding/AdditionalInfoView;

    .line 67
    iget-object v1, p0, Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter$1;->val$dateFormat:Ljava/text/DateFormat;

    invoke-virtual {p1}, Lcom/squareup/ui/SquareDate;->asDate()Ljava/util/Date;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/onboarding/AdditionalInfoView;->setBirthDateText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method protected bridge synthetic onPopupResult(Ljava/lang/Object;)V
    .locals 0

    .line 62
    check-cast p1, Lcom/squareup/ui/SquareDate;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/onboarding/AdditionalInfoScreen$Presenter$1;->onPopupResult(Lcom/squareup/ui/SquareDate;)V

    return-void
.end method
