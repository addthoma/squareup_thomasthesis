.class public final Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "DepositBankLinkingCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDepositBankLinkingCoordinator.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DepositBankLinkingCoordinator.kt\ncom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator\n*L\n1#1,233:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0082\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\u0010\r\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\u0018\u00002\u00020\u0001:\u00011B5\u0008\u0002\u0012\u001c\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u000cJ\u0010\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u001fH\u0016J\u0010\u0010 \u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u001fH\u0002J\u0008\u0010!\u001a\u00020\"H\u0002J\u0016\u0010#\u001a\u00020\u001d2\u000c\u0010$\u001a\u0008\u0012\u0004\u0012\u00020\u00060%H\u0002J\u0016\u0010&\u001a\u00020\u001d2\u000c\u0010$\u001a\u0008\u0012\u0004\u0012\u00020\u00060%H\u0002J(\u0010\'\u001a\u00020\u001d2\u0016\u0010(\u001a\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00072\u0006\u0010\u001e\u001a\u00020\u001fH\u0002J\u0016\u0010)\u001a\u00020\u001d2\u000c\u0010$\u001a\u0008\u0012\u0004\u0012\u00020\u00060%H\u0002J(\u0010*\u001a\u00020\u001d2\u0016\u0010(\u001a\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00072\u0006\u0010\u001e\u001a\u00020\u001fH\u0002J\u0008\u0010+\u001a\u00020\u001dH\u0002J \u0010,\u001a\u00020-2\u0016\u0010(\u001a\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u0007H\u0002J\u0016\u0010.\u001a\u00020/2\u000c\u00100\u001a\u0008\u0012\u0004\u0012\u00020\u00060%H\u0002R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0014\u001a\u0008\u0012\u0004\u0012\u00020\u00160\u0015X\u0082.\u00a2\u0006\u0004\n\u0002\u0010\u0017R\u000e\u0010\u0018\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R$\u0010\u0002\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u00070\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u001aX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u001aX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u00062"
    }
    d2 = {
        "Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "screens",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/ui/onboarding/bank/DepositBankLinking$ScreenData;",
        "Lcom/squareup/ui/onboarding/bank/DepositBankLinking$Event;",
        "Lcom/squareup/ui/onboarding/bank/DepositBankLinkingScreen;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "glassSpinner",
        "Lcom/squareup/register/widgets/GlassSpinner;",
        "(Lio/reactivex/Observable;Lcom/squareup/analytics/Analytics;Lcom/squareup/register/widgets/GlassSpinner;)V",
        "accountHolderField",
        "Landroid/widget/EditText;",
        "accountNumberField",
        "accountTypeField",
        "Landroid/widget/TextView;",
        "actionBar",
        "Lcom/squareup/marin/widgets/MarinActionBar;",
        "localizedNames",
        "",
        "",
        "[Ljava/lang/CharSequence;",
        "routingNumberField",
        "subtitle",
        "Lcom/squareup/widgets/MessageView;",
        "title",
        "attach",
        "",
        "view",
        "Landroid/view/View;",
        "bindViews",
        "getBankAccountDetails",
        "Lcom/squareup/protos/client/bankaccount/BankAccountDetails;",
        "onAdvanced",
        "workflow",
        "Lcom/squareup/workflow/legacy/WorkflowInput;",
        "onLater",
        "onScreen",
        "screen",
        "setUpAccountTypeField",
        "setUpActionBar",
        "setUpRoutingNumberField",
        "spinnerData",
        "Lcom/squareup/register/widgets/GlassSpinnerState;",
        "validateInput",
        "",
        "eventHandler",
        "Factory",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private accountHolderField:Landroid/widget/EditText;

.field private accountNumberField:Landroid/widget/EditText;

.field private accountTypeField:Landroid/widget/TextView;

.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

.field private localizedNames:[Ljava/lang/CharSequence;

.field private routingNumberField:Landroid/widget/EditText;

.field private final screens:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/onboarding/bank/DepositBankLinking$ScreenData;",
            "Lcom/squareup/ui/onboarding/bank/DepositBankLinking$Event;",
            ">;>;"
        }
    .end annotation
.end field

.field private subtitle:Lcom/squareup/widgets/MessageView;

.field private title:Lcom/squareup/widgets/MessageView;


# direct methods
.method private constructor <init>(Lio/reactivex/Observable;Lcom/squareup/analytics/Analytics;Lcom/squareup/register/widgets/GlassSpinner;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/onboarding/bank/DepositBankLinking$ScreenData;",
            "Lcom/squareup/ui/onboarding/bank/DepositBankLinking$Event;",
            ">;>;",
            "Lcom/squareup/analytics/Analytics;",
            "Lcom/squareup/register/widgets/GlassSpinner;",
            ")V"
        }
    .end annotation

    .line 56
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;->screens:Lio/reactivex/Observable;

    iput-object p2, p0, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;->analytics:Lcom/squareup/analytics/Analytics;

    iput-object p3, p0, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    return-void
.end method

.method public synthetic constructor <init>(Lio/reactivex/Observable;Lcom/squareup/analytics/Analytics;Lcom/squareup/register/widgets/GlassSpinner;Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 51
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;-><init>(Lio/reactivex/Observable;Lcom/squareup/analytics/Analytics;Lcom/squareup/register/widgets/GlassSpinner;)V

    return-void
.end method

.method public static final synthetic access$getRoutingNumberField$p(Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;)Landroid/widget/EditText;
    .locals 1

    .line 51
    iget-object p0, p0, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;->routingNumberField:Landroid/widget/EditText;

    if-nez p0, :cond_0

    const-string v0, "routingNumberField"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$onAdvanced(Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 0

    .line 51
    invoke-direct {p0, p1}, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;->onAdvanced(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-void
.end method

.method public static final synthetic access$onLater(Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 0

    .line 51
    invoke-direct {p0, p1}, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;->onLater(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    return-void
.end method

.method public static final synthetic access$onScreen(Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;Lcom/squareup/workflow/legacy/Screen;Landroid/view/View;)V
    .locals 0

    .line 51
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;->onScreen(Lcom/squareup/workflow/legacy/Screen;Landroid/view/View;)V

    return-void
.end method

.method public static final synthetic access$setRoutingNumberField$p(Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;Landroid/widget/EditText;)V
    .locals 0

    .line 51
    iput-object p1, p0, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;->routingNumberField:Landroid/widget/EditText;

    return-void
.end method

.method public static final synthetic access$spinnerData(Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;Lcom/squareup/workflow/legacy/Screen;)Lcom/squareup/register/widgets/GlassSpinnerState;
    .locals 0

    .line 51
    invoke-direct {p0, p1}, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;->spinnerData(Lcom/squareup/workflow/legacy/Screen;)Lcom/squareup/register/widgets/GlassSpinnerState;

    move-result-object p0

    return-object p0
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 2

    .line 224
    invoke-static {p1}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    const-string v1, "ActionBarView.findIn(view)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 225
    sget v0, Lcom/squareup/onboarding/flow/R$id;->title:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;->title:Lcom/squareup/widgets/MessageView;

    .line 226
    sget v0, Lcom/squareup/onboarding/flow/R$id;->subtitle:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    iput-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;->subtitle:Lcom/squareup/widgets/MessageView;

    .line 227
    sget v0, Lcom/squareup/onboarding/flow/R$id;->account_holder_field:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;->accountHolderField:Landroid/widget/EditText;

    .line 228
    sget v0, Lcom/squareup/onboarding/flow/R$id;->routing_number_field:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;->routingNumberField:Landroid/widget/EditText;

    .line 229
    sget v0, Lcom/squareup/onboarding/flow/R$id;->account_number_field:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;->accountNumberField:Landroid/widget/EditText;

    .line 230
    sget v0, Lcom/squareup/onboarding/flow/R$id;->account_type_field:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;->accountTypeField:Landroid/widget/TextView;

    return-void
.end method

.method private final getBankAccountDetails()Lcom/squareup/protos/client/bankaccount/BankAccountDetails;
    .locals 4

    .line 210
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;->localizedNames:[Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    const-string v1, "localizedNames"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    iget-object v1, p0, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;->accountTypeField:Landroid/widget/TextView;

    if-nez v1, :cond_1

    const-string v2, "accountTypeField"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-static {v1}, Lcom/squareup/util/Views;->getValue(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/collections/ArraysKt;->indexOf([Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Lcom/squareup/protos/client/bankaccount/BankAccountType;->fromValue(I)Lcom/squareup/protos/client/bankaccount/BankAccountType;

    move-result-object v0

    .line 211
    new-instance v1, Lcom/squareup/protos/client/bankaccount/BankAccountDetails$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/client/bankaccount/BankAccountDetails$Builder;-><init>()V

    .line 212
    iget-object v2, p0, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;->accountHolderField:Landroid/widget/EditText;

    if-nez v2, :cond_2

    const-string v3, "accountHolderField"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_2
    check-cast v2, Landroid/widget/TextView;

    invoke-static {v2}, Lcom/squareup/util/Views;->getValue(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bankaccount/BankAccountDetails$Builder;->account_name(Ljava/lang/String;)Lcom/squareup/protos/client/bankaccount/BankAccountDetails$Builder;

    move-result-object v1

    .line 213
    iget-object v2, p0, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;->routingNumberField:Landroid/widget/EditText;

    if-nez v2, :cond_3

    const-string v3, "routingNumberField"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    check-cast v2, Landroid/widget/TextView;

    invoke-static {v2}, Lcom/squareup/util/Views;->getValue(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bankaccount/BankAccountDetails$Builder;->primary_institution_number(Ljava/lang/String;)Lcom/squareup/protos/client/bankaccount/BankAccountDetails$Builder;

    move-result-object v1

    .line 214
    iget-object v2, p0, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;->accountNumberField:Landroid/widget/EditText;

    if-nez v2, :cond_4

    const-string v3, "accountNumberField"

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    check-cast v2, Landroid/widget/TextView;

    invoke-static {v2}, Lcom/squareup/util/Views;->getValue(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/protos/client/bankaccount/BankAccountDetails$Builder;->account_number(Ljava/lang/String;)Lcom/squareup/protos/client/bankaccount/BankAccountDetails$Builder;

    move-result-object v1

    .line 215
    invoke-virtual {v1, v0}, Lcom/squareup/protos/client/bankaccount/BankAccountDetails$Builder;->account_type(Lcom/squareup/protos/client/bankaccount/BankAccountType;)Lcom/squareup/protos/client/bankaccount/BankAccountDetails$Builder;

    move-result-object v0

    .line 216
    invoke-virtual {v0}, Lcom/squareup/protos/client/bankaccount/BankAccountDetails$Builder;->build()Lcom/squareup/protos/client/bankaccount/BankAccountDetails;

    move-result-object v0

    const-string v1, "BankAccountDetails.Build\u2026untType)\n        .build()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method private final onAdvanced(Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/ui/onboarding/bank/DepositBankLinking$Event;",
            ">;)V"
        }
    .end annotation

    .line 137
    invoke-direct {p0, p1}, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;->validateInput(Lcom/squareup/workflow/legacy/WorkflowInput;)Z

    move-result v0

    const-string v1, "Onboard: Add Bank Account Attempt"

    if-eqz v0, :cond_0

    .line 138
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v2, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_BANK_CONTINUE:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v2}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 139
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v2, Lcom/squareup/log/deposits/BankLinkingAttemptEvent;

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-direct {v2, v1, v3}, Lcom/squareup/log/deposits/BankLinkingAttemptEvent;-><init>(Ljava/lang/String;Ljava/lang/Boolean;)V

    check-cast v2, Lcom/squareup/eventstream/v2/AppEvent;

    invoke-interface {v0, v2}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    .line 140
    new-instance v0, Lcom/squareup/ui/onboarding/bank/DepositBankLinking$Event$LinkBank;

    invoke-direct {p0}, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;->getBankAccountDetails()Lcom/squareup/protos/client/bankaccount/BankAccountDetails;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/ui/onboarding/bank/DepositBankLinking$Event$LinkBank;-><init>(Lcom/squareup/protos/client/bankaccount/BankAccountDetails;)V

    invoke-interface {p1, v0}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    goto :goto_0

    .line 142
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v0, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_BANK_CONTINUE_INVALID:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 143
    iget-object p1, p0, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v0, Lcom/squareup/log/deposits/BankLinkingAttemptEvent;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/log/deposits/BankLinkingAttemptEvent;-><init>(Ljava/lang/String;Ljava/lang/Boolean;)V

    check-cast v0, Lcom/squareup/eventstream/v2/AppEvent;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    :goto_0
    return-void
.end method

.method private final onLater(Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/ui/onboarding/bank/DepositBankLinking$Event;",
            ">;)V"
        }
    .end annotation

    .line 148
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ONBOARDING_BANK_LATER:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 149
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/analytics/event/ClickEvent;

    const-string v2, "Onboard: Add Bank Account Later"

    invoke-direct {v1, v2}, Lcom/squareup/analytics/event/ClickEvent;-><init>(Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/eventstream/v2/AppEvent;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v2/AppEvent;)V

    .line 150
    sget-object v0, Lcom/squareup/ui/onboarding/bank/DepositBankLinking$Event$LaterClicked;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositBankLinking$Event$LaterClicked;

    invoke-interface {p1, v0}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return-void
.end method

.method private final onScreen(Lcom/squareup/workflow/legacy/Screen;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/onboarding/bank/DepositBankLinking$ScreenData;",
            "Lcom/squareup/ui/onboarding/bank/DepositBankLinking$Event;",
            ">;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .line 97
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;->setUpActionBar(Lcom/squareup/workflow/legacy/Screen;Landroid/view/View;)V

    .line 98
    iget-object p2, p1, Lcom/squareup/workflow/legacy/Screen;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    invoke-direct {p0, p2}, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;->setUpAccountTypeField(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    .line 100
    iget-object p2, p0, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;->accountHolderField:Landroid/widget/EditText;

    const-string v0, "accountHolderField"

    if-nez p2, :cond_0

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast p2, Landroid/widget/TextView;

    invoke-static {p2}, Lcom/squareup/util/Views;->getValue(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    invoke-static {p2}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p2

    if-eqz p2, :cond_2

    .line 101
    iget-object p2, p0, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;->accountHolderField:Landroid/widget/EditText;

    if-nez p2, :cond_1

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    iget-object v0, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/ui/onboarding/bank/DepositBankLinking$ScreenData;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/bank/DepositBankLinking$ScreenData;->getPersonalName()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p2, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 103
    :cond_2
    iget-object p1, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositBankLinking$ScreenData;

    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/bank/DepositBankLinking$ScreenData;->getBankAccountType()Lcom/squareup/protos/client/bankaccount/BankAccountType;

    move-result-object p1

    if-eqz p1, :cond_5

    .line 104
    iget-object p2, p0, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;->accountTypeField:Landroid/widget/TextView;

    if-nez p2, :cond_3

    const-string v0, "accountTypeField"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;->localizedNames:[Ljava/lang/CharSequence;

    if-nez v0, :cond_4

    const-string v1, "localizedNames"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {p1}, Lcom/squareup/protos/client/bankaccount/BankAccountType;->ordinal()I

    move-result p1

    add-int/lit8 p1, p1, -0x1

    aget-object p1, v0, p1

    invoke-virtual {p2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_5
    return-void
.end method

.method private final setUpAccountTypeField(Lcom/squareup/workflow/legacy/WorkflowInput;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/ui/onboarding/bank/DepositBankLinking$Event;",
            ">;)V"
        }
    .end annotation

    .line 167
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;->accountTypeField:Landroid/widget/TextView;

    const-string v1, "accountTypeField"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    new-instance v2, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator$setUpAccountTypeField$1;

    invoke-direct {v2, p1}, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator$setUpAccountTypeField$1;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v2, Ljava/lang/Runnable;

    invoke-static {v2}, Lcom/squareup/debounce/Debouncers;->debounceRunnable(Ljava/lang/Runnable;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v2

    check-cast v2, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 168
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;->accountTypeField:Landroid/widget/TextView;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    new-instance v1, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator$setUpAccountTypeField$2;

    invoke-direct {v1, p1}, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator$setUpAccountTypeField$2;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v1, Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    return-void
.end method

.method private final setUpActionBar(Lcom/squareup/workflow/legacy/Screen;Landroid/view/View;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/onboarding/bank/DepositBankLinking$ScreenData;",
            "Lcom/squareup/ui/onboarding/bank/DepositBankLinking$Event;",
            ">;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .line 112
    iget-object v0, p1, Lcom/squareup/workflow/legacy/Screen;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    .line 113
    iget-object p1, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositBankLinking$ScreenData;

    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/bank/DepositBankLinking$ScreenData;->getCanGoBack()Z

    move-result p1

    .line 115
    iget-object v1, p0, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;->title:Lcom/squareup/widgets/MessageView;

    if-nez v1, :cond_0

    const-string v2, "title"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    sget v2, Lcom/squareup/onboarding/common/R$string;->add_bank_account:I

    invoke-virtual {v1, v2}, Lcom/squareup/widgets/MessageView;->setText(I)V

    .line 116
    iget-object v1, p0, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;->subtitle:Lcom/squareup/widgets/MessageView;

    if-nez v1, :cond_1

    const-string v2, "subtitle"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    sget v2, Lcom/squareup/onboarding/flow/R$string;->link_bank_account_subtitle:I

    invoke-virtual {v1, v2}, Lcom/squareup/widgets/MessageView;->setText(I)V

    .line 118
    new-instance v1, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator$setUpActionBar$1;

    invoke-direct {v1, v0}, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator$setUpActionBar$1;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-static {p2, v1}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 120
    invoke-virtual {p2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    .line 133
    iget-object v1, p0, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    if-nez v1, :cond_2

    const-string v2, "actionBar"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    .line 121
    :cond_2
    new-instance v2, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    invoke-direct {v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;-><init>()V

    if-eqz p1, :cond_3

    .line 124
    new-instance v3, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator$setUpActionBar$$inlined$apply$lambda$1;

    invoke-direct {v3, p1, v0}, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator$setUpActionBar$$inlined$apply$lambda$1;-><init>(ZLcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v3, Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showUpButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    goto :goto_0

    .line 126
    :cond_3
    invoke-virtual {v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->hideUpButton()Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    .line 129
    :goto_0
    sget p1, Lcom/squareup/onboarding/flow/R$string;->onboarding_actionbar_continue:I

    invoke-virtual {p2, p1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {v2, p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setPrimaryButtonText(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 130
    new-instance v2, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator$setUpActionBar$3;

    invoke-direct {v2, p0, v0}, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator$setUpActionBar$3;-><init>(Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v2, Ljava/lang/Runnable;

    invoke-virtual {p1, v2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showPrimaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 131
    sget v2, Lcom/squareup/onboarding/flow/R$string;->onboarding_actionbar_later:I

    invoke-virtual {p2, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->setSecondaryButtonTextNoGlyph(Ljava/lang/CharSequence;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 132
    new-instance p2, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator$setUpActionBar$4;

    invoke-direct {p2, p0, v0}, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator$setUpActionBar$4;-><init>(Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast p2, Ljava/lang/Runnable;

    invoke-virtual {p1, p2}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->showSecondaryButton(Ljava/lang/Runnable;)Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;

    move-result-object p1

    .line 133
    invoke-virtual {p1}, Lcom/squareup/marin/widgets/MarinActionBar$Config$Builder;->build()Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method

.method private final setUpRoutingNumberField()V
    .locals 3

    .line 154
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;->routingNumberField:Landroid/widget/EditText;

    const-string v1, "routingNumberField"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    new-instance v2, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator$setUpRoutingNumberField$1;

    invoke-direct {v2, p0}, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator$setUpRoutingNumberField$1;-><init>(Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;)V

    check-cast v2, Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 159
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;->routingNumberField:Landroid/widget/EditText;

    if-nez v0, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    new-instance v1, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator$setUpRoutingNumberField$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator$setUpRoutingNumberField$2;-><init>(Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;)V

    check-cast v1, Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method

.method private final spinnerData(Lcom/squareup/workflow/legacy/Screen;)Lcom/squareup/register/widgets/GlassSpinnerState;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/ui/onboarding/bank/DepositBankLinking$ScreenData;",
            "Lcom/squareup/ui/onboarding/bank/DepositBankLinking$Event;",
            ">;)",
            "Lcom/squareup/register/widgets/GlassSpinnerState;"
        }
    .end annotation

    .line 220
    sget-object v0, Lcom/squareup/register/widgets/GlassSpinnerState;->Factory:Lcom/squareup/register/widgets/GlassSpinnerState$Factory;

    iget-object p1, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositBankLinking$ScreenData;

    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/bank/DepositBankLinking$ScreenData;->getShowSpinner()Z

    move-result p1

    sget v1, Lcom/squareup/onboarding/flow/R$string;->onboarding_bank_add_progress:I

    invoke-virtual {v0, p1, v1}, Lcom/squareup/register/widgets/GlassSpinnerState$Factory;->showNonDebouncedSpinner(ZI)Lcom/squareup/register/widgets/GlassSpinnerState;

    move-result-object p1

    return-object p1
.end method

.method private final validateInput(Lcom/squareup/workflow/legacy/WorkflowInput;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/WorkflowInput<",
            "-",
            "Lcom/squareup/ui/onboarding/bank/DepositBankLinking$Event;",
            ">;)Z"
        }
    .end annotation

    .line 178
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;->accountHolderField:Landroid/widget/EditText;

    const-string v1, "accountHolderField"

    if-nez v0, :cond_0

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lcom/squareup/util/Views;->getValue(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v2, 0x0

    if-eqz v0, :cond_2

    .line 179
    new-instance v0, Lcom/squareup/ui/onboarding/bank/DepositBankLinking$Event$InvalidInput;

    sget-object v3, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$InvalidInput$MissingAccountHolder;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$InvalidInput$MissingAccountHolder;

    check-cast v3, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$InvalidInput;

    invoke-direct {v0, v3}, Lcom/squareup/ui/onboarding/bank/DepositBankLinking$Event$InvalidInput;-><init>(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$InvalidInput;)V

    invoke-interface {p1, v0}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    .line 180
    iget-object p1, p0, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;->accountHolderField:Landroid/widget/EditText;

    if-nez p1, :cond_1

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p1}, Landroid/widget/EditText;->requestFocus()Z

    return v2

    .line 184
    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;->routingNumberField:Landroid/widget/EditText;

    const-string v1, "routingNumberField"

    if-nez v0, :cond_3

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_3
    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lcom/squareup/util/Views;->getValue(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lcom/squareup/banklinking/RoutingNumberUtil;->isRoutingTransitNumber(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 185
    new-instance v0, Lcom/squareup/ui/onboarding/bank/DepositBankLinking$Event$InvalidInput;

    sget-object v3, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$InvalidInput$InvalidRoutingNumber;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$InvalidInput$InvalidRoutingNumber;

    check-cast v3, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$InvalidInput;

    invoke-direct {v0, v3}, Lcom/squareup/ui/onboarding/bank/DepositBankLinking$Event$InvalidInput;-><init>(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$InvalidInput;)V

    invoke-interface {p1, v0}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    .line 186
    iget-object p1, p0, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;->routingNumberField:Landroid/widget/EditText;

    if-nez p1, :cond_4

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_4
    invoke-virtual {p1}, Landroid/widget/EditText;->requestFocus()Z

    return v2

    .line 190
    :cond_5
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;->accountNumberField:Landroid/widget/EditText;

    const-string v3, "accountNumberField"

    if-nez v0, :cond_6

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_6
    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lcom/squareup/util/Views;->getValue(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 191
    new-instance v0, Lcom/squareup/ui/onboarding/bank/DepositBankLinking$Event$InvalidInput;

    sget-object v1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$InvalidInput$MissingAccountNumber;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$InvalidInput$MissingAccountNumber;

    check-cast v1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$InvalidInput;

    invoke-direct {v0, v1}, Lcom/squareup/ui/onboarding/bank/DepositBankLinking$Event$InvalidInput;-><init>(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$InvalidInput;)V

    invoke-interface {p1, v0}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    .line 192
    iget-object p1, p0, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;->accountNumberField:Landroid/widget/EditText;

    if-nez p1, :cond_7

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_7
    invoke-virtual {p1}, Landroid/widget/EditText;->requestFocus()Z

    return v2

    .line 196
    :cond_8
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;->accountNumberField:Landroid/widget/EditText;

    if-nez v0, :cond_9

    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_9
    check-cast v0, Landroid/widget/TextView;

    invoke-static {v0}, Lcom/squareup/util/Views;->getValue(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;->routingNumberField:Landroid/widget/EditText;

    if-nez v3, :cond_a

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_a
    check-cast v3, Landroid/widget/TextView;

    invoke-static {v3}, Lcom/squareup/util/Views;->getValue(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 197
    new-instance v0, Lcom/squareup/ui/onboarding/bank/DepositBankLinking$Event$InvalidInput;

    sget-object v1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$InvalidInput$RoutingAndAccountNumbersMatch;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$InvalidInput$RoutingAndAccountNumbersMatch;

    check-cast v1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$InvalidInput;

    invoke-direct {v0, v1}, Lcom/squareup/ui/onboarding/bank/DepositBankLinking$Event$InvalidInput;-><init>(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$InvalidInput;)V

    invoke-interface {p1, v0}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return v2

    .line 201
    :cond_b
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;->accountTypeField:Landroid/widget/TextView;

    if-nez v0, :cond_c

    const-string v1, "accountTypeField"

    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_c
    invoke-static {v0}, Lcom/squareup/util/Views;->getValue(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 202
    new-instance v0, Lcom/squareup/ui/onboarding/bank/DepositBankLinking$Event$InvalidInput;

    sget-object v1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$InvalidInput$MissingAccountType;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$InvalidInput$MissingAccountType;

    check-cast v1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$InvalidInput;

    invoke-direct {v0, v1}, Lcom/squareup/ui/onboarding/bank/DepositBankLinking$Event$InvalidInput;-><init>(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsEvent$InvalidInput;)V

    invoke-interface {p1, v0}, Lcom/squareup/workflow/legacy/WorkflowInput;->sendEvent(Ljava/lang/Object;)V

    return v2

    :cond_d
    const/4 p1, 0x1

    return p1
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 4

    const-string v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    invoke-direct {p0, p1}, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;->bindViews(Landroid/view/View;)V

    .line 78
    invoke-direct {p0}, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;->setUpRoutingNumberField()V

    .line 81
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/squareup/banklinking/R$array;->bank_account_types:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v0

    const-string v1, "view.resources.getTextAr\u2026array.bank_account_types)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;->localizedNames:[Ljava/lang/CharSequence;

    .line 83
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/register/widgets/GlassSpinner;->showOrHideSpinner(Landroid/content/Context;)Lrx/Subscription;

    move-result-object v0

    const-string v1, "glassSpinner.showOrHideSpinner(view.context)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 84
    invoke-static {v0}, Lcom/squareup/util/RxJavaInteropExtensionsKt;->toV2Disposable(Lrx/Subscription;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 85
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    .line 87
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;->screens:Lio/reactivex/Observable;

    .line 88
    iget-object v1, p0, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;->glassSpinner:Lcom/squareup/register/widgets/GlassSpinner;

    new-instance v2, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator$attach$1;

    move-object v3, p0

    check-cast v3, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;

    invoke-direct {v2, v3}, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator$attach$1;-><init>(Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;)V

    check-cast v2, Lkotlin/jvm/functions/Function1;

    new-instance v3, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinatorKt$sam$rx_functions_Func1$0;

    invoke-direct {v3, v2}, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinatorKt$sam$rx_functions_Func1$0;-><init>(Lkotlin/jvm/functions/Function1;)V

    check-cast v3, Lrx/functions/Func1;

    invoke-virtual {v1, v3}, Lcom/squareup/register/widgets/GlassSpinner;->spinnerTransformRx2(Lrx/functions/Func1;)Lio/reactivex/ObservableTransformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object v0

    .line 89
    new-instance v1, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator$attach$2;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator$attach$2;-><init>(Lcom/squareup/ui/onboarding/bank/DepositBankLinkingCoordinator;Landroid/view/View;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    const-string v1, "screens\n        .compose\u2026be { onScreen(it, view) }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 90
    invoke-static {v0, p1}, Lcom/squareup/util/DisposablesKt;->disposeOnDetach(Lio/reactivex/disposables/Disposable;Landroid/view/View;)V

    return-void
.end method
