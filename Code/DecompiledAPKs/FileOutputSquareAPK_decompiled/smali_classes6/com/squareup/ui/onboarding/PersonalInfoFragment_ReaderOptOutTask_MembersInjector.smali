.class public final Lcom/squareup/ui/onboarding/PersonalInfoFragment_ReaderOptOutTask_MembersInjector;
.super Ljava/lang/Object;
.source "PersonalInfoFragment_ReaderOptOutTask_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/onboarding/PersonalInfoFragment$ReaderOptOutTask;",
        ">;"
    }
.end annotation


# instance fields
.field private final accountServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/PersistentAccountService;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/PersistentAccountService;",
            ">;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/ui/onboarding/PersonalInfoFragment_ReaderOptOutTask_MembersInjector;->accountServiceProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/account/PersistentAccountService;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/onboarding/PersonalInfoFragment$ReaderOptOutTask;",
            ">;"
        }
    .end annotation

    .line 27
    new-instance v0, Lcom/squareup/ui/onboarding/PersonalInfoFragment_ReaderOptOutTask_MembersInjector;

    invoke-direct {v0, p0}, Lcom/squareup/ui/onboarding/PersonalInfoFragment_ReaderOptOutTask_MembersInjector;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectAccountService(Lcom/squareup/ui/onboarding/PersonalInfoFragment$ReaderOptOutTask;Lcom/squareup/account/PersistentAccountService;)V
    .locals 0

    .line 37
    iput-object p1, p0, Lcom/squareup/ui/onboarding/PersonalInfoFragment$ReaderOptOutTask;->accountService:Lcom/squareup/account/PersistentAccountService;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/onboarding/PersonalInfoFragment$ReaderOptOutTask;)V
    .locals 1

    .line 31
    iget-object v0, p0, Lcom/squareup/ui/onboarding/PersonalInfoFragment_ReaderOptOutTask_MembersInjector;->accountServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/account/PersistentAccountService;

    invoke-static {p1, v0}, Lcom/squareup/ui/onboarding/PersonalInfoFragment_ReaderOptOutTask_MembersInjector;->injectAccountService(Lcom/squareup/ui/onboarding/PersonalInfoFragment$ReaderOptOutTask;Lcom/squareup/account/PersistentAccountService;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 9
    check-cast p1, Lcom/squareup/ui/onboarding/PersonalInfoFragment$ReaderOptOutTask;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/onboarding/PersonalInfoFragment_ReaderOptOutTask_MembersInjector;->injectMembers(Lcom/squareup/ui/onboarding/PersonalInfoFragment$ReaderOptOutTask;)V

    return-void
.end method
