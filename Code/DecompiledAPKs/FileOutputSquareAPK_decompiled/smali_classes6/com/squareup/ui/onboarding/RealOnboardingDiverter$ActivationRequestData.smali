.class Lcom/squareup/ui/onboarding/RealOnboardingDiverter$ActivationRequestData;
.super Ljava/lang/Object;
.source "RealOnboardingDiverter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/onboarding/RealOnboardingDiverter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ActivationRequestData"
.end annotation


# instance fields
.field final eligibleForActivation:Z

.field final launchMode:Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;


# direct methods
.method private constructor <init>(Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;ZZ)V
    .locals 0

    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 114
    iput-object p1, p0, Lcom/squareup/ui/onboarding/RealOnboardingDiverter$ActivationRequestData;->launchMode:Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;

    if-eqz p2, :cond_1

    if-nez p3, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    .line 115
    :goto_1
    iput-boolean p1, p0, Lcom/squareup/ui/onboarding/RealOnboardingDiverter$ActivationRequestData;->eligibleForActivation:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;ZZLcom/squareup/ui/onboarding/RealOnboardingDiverter$1;)V
    .locals 0

    .line 108
    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/ui/onboarding/RealOnboardingDiverter$ActivationRequestData;-><init>(Lcom/squareup/onboarding/OnboardingStarter$ActivationLaunchMode;ZZ)V

    return-void
.end method
