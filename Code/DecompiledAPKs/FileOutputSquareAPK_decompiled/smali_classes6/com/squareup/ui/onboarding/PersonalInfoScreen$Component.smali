.class public interface abstract Lcom/squareup/ui/onboarding/PersonalInfoScreen$Component;
.super Ljava/lang/Object;
.source "PersonalInfoScreen.java"

# interfaces
.implements Lcom/squareup/address/AddressLayout$Component;


# annotations
.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/ui/onboarding/PersonalInfoScreen$Module;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/onboarding/PersonalInfoScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract inject(Lcom/squareup/ui/onboarding/PersonalInfoView;)V
.end method
