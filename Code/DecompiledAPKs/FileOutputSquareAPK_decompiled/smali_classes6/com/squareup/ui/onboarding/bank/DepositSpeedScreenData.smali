.class public final Lcom/squareup/ui/onboarding/bank/DepositSpeedScreenData;
.super Ljava/lang/Object;
.source "DepositSpeedScreenData.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0006\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\n\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/ui/onboarding/bank/DepositSpeedScreenData;",
        "",
        "sameDayDepositDescription",
        "",
        "canGoBack",
        "",
        "(Ljava/lang/String;Z)V",
        "getCanGoBack",
        "()Z",
        "getSameDayDepositDescription",
        "()Ljava/lang/String;",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final canGoBack:Z

.field private final sameDayDepositDescription:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 1

    const-string v0, "sameDayDepositDescription"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/onboarding/bank/DepositSpeedScreenData;->sameDayDepositDescription:Ljava/lang/String;

    iput-boolean p2, p0, Lcom/squareup/ui/onboarding/bank/DepositSpeedScreenData;->canGoBack:Z

    return-void
.end method


# virtual methods
.method public final getCanGoBack()Z
    .locals 1

    .line 6
    iget-boolean v0, p0, Lcom/squareup/ui/onboarding/bank/DepositSpeedScreenData;->canGoBack:Z

    return v0
.end method

.method public final getSameDayDepositDescription()Ljava/lang/String;
    .locals 1

    .line 5
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositSpeedScreenData;->sameDayDepositDescription:Ljava/lang/String;

    return-object v0
.end method
