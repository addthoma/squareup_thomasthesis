.class public Lcom/squareup/ui/onboarding/ActivationRetryScreen$Presenter;
.super Lmortar/ViewPresenter;
.source "ActivationRetryScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/onboarding/ActivationRetryScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lmortar/ViewPresenter<",
        "Lcom/squareup/ui/onboarding/ActivationRetryView;",
        ">;"
    }
.end annotation


# instance fields
.field private final activationService:Lcom/squareup/server/activation/ActivationService;

.field final confirmLaterPopupPresenter:Lcom/squareup/mortar/PopupPresenter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/mortar/PopupPresenter<",
            "Lcom/squareup/register/widgets/Confirmation;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field final progressPresenter:Lcom/squareup/caller/ProgressAndFailurePresenter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/caller/ProgressAndFailurePresenter<",
            "Lcom/squareup/server/SimpleResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final runner:Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

.field private final rxGlue:Lcom/squareup/caller/ProgressAndFailureRxGlue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/caller/ProgressAndFailureRxGlue<",
            "Lcom/squareup/server/SimpleResponse;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/ui/onboarding/OnboardingActivityRunner;Lcom/squareup/util/Res;Lcom/squareup/server/activation/ActivationService;Lcom/squareup/caller/ProgressAndFailureRxGlue$Factory;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 57
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 45
    new-instance v0, Lcom/squareup/ui/onboarding/ActivationRetryScreen$Presenter$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/onboarding/ActivationRetryScreen$Presenter$1;-><init>(Lcom/squareup/ui/onboarding/ActivationRetryScreen$Presenter;)V

    iput-object v0, p0, Lcom/squareup/ui/onboarding/ActivationRetryScreen$Presenter;->confirmLaterPopupPresenter:Lcom/squareup/mortar/PopupPresenter;

    .line 58
    iput-object p1, p0, Lcom/squareup/ui/onboarding/ActivationRetryScreen$Presenter;->runner:Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

    .line 59
    iput-object p3, p0, Lcom/squareup/ui/onboarding/ActivationRetryScreen$Presenter;->activationService:Lcom/squareup/server/activation/ActivationService;

    .line 61
    new-instance p1, Lcom/squareup/request/RequestMessages;

    sget p3, Lcom/squareup/onboarding/flow/R$string;->onboarding_apply_progress_title:I

    sget v0, Lcom/squareup/onboarding/flow/R$string;->onboarding_apply_failure_title:I

    invoke-direct {p1, p2, p3, v0}, Lcom/squareup/request/RequestMessages;-><init>(Lcom/squareup/util/Res;II)V

    .line 64
    new-instance p2, Lcom/squareup/caller/ProgressAndFailurePresenter;

    new-instance p3, Lcom/squareup/ui/onboarding/ActivationRetryScreen$Presenter$2;

    invoke-direct {p3, p0}, Lcom/squareup/ui/onboarding/ActivationRetryScreen$Presenter$2;-><init>(Lcom/squareup/ui/onboarding/ActivationRetryScreen$Presenter;)V

    const-string v0, "retryCall"

    invoke-direct {p2, v0, p1, p3}, Lcom/squareup/caller/ProgressAndFailurePresenter;-><init>(Ljava/lang/String;Lcom/squareup/request/RequestMessages;Lcom/squareup/caller/ProgressAndFailurePresenter$ViewListener;)V

    iput-object p2, p0, Lcom/squareup/ui/onboarding/ActivationRetryScreen$Presenter;->progressPresenter:Lcom/squareup/caller/ProgressAndFailurePresenter;

    .line 73
    iget-object p1, p0, Lcom/squareup/ui/onboarding/ActivationRetryScreen$Presenter;->progressPresenter:Lcom/squareup/caller/ProgressAndFailurePresenter;

    invoke-virtual {p4, p1}, Lcom/squareup/caller/ProgressAndFailureRxGlue$Factory;->forSimpleResponse(Lcom/squareup/caller/ProgressAndFailurePresenter;)Lcom/squareup/caller/ProgressAndFailureRxGlue;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/onboarding/ActivationRetryScreen$Presenter;->rxGlue:Lcom/squareup/caller/ProgressAndFailureRxGlue;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/onboarding/ActivationRetryScreen$Presenter;)Lcom/squareup/ui/onboarding/OnboardingActivityRunner;
    .locals 0

    .line 38
    iget-object p0, p0, Lcom/squareup/ui/onboarding/ActivationRetryScreen$Presenter;->runner:Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/onboarding/ActivationRetryScreen$Presenter;)V
    .locals 0

    .line 38
    invoke-direct {p0}, Lcom/squareup/ui/onboarding/ActivationRetryScreen$Presenter;->retry()V

    return-void
.end method

.method private retry()V
    .locals 4

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/onboarding/ActivationRetryScreen$Presenter;->activationService:Lcom/squareup/server/activation/ActivationService;

    const-string v1, ""

    invoke-interface {v0, v1}, Lcom/squareup/server/activation/ActivationService;->retry(Ljava/lang/String;)Lcom/squareup/server/SimpleStandardResponse;

    move-result-object v0

    .line 78
    invoke-virtual {v0}, Lcom/squareup/server/SimpleStandardResponse;->receivedResponse()Lio/reactivex/Single;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/onboarding/ActivationRetryScreen$Presenter;->rxGlue:Lcom/squareup/caller/ProgressAndFailureRxGlue;

    .line 79
    invoke-virtual {v1}, Lcom/squareup/caller/ProgressAndFailureRxGlue;->showRetrofitProgress()Lio/reactivex/SingleTransformer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->compose(Lio/reactivex/SingleTransformer;)Lio/reactivex/Single;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/onboarding/ActivationRetryScreen$Presenter;->rxGlue:Lcom/squareup/caller/ProgressAndFailureRxGlue;

    new-instance v2, Lcom/squareup/ui/onboarding/-$$Lambda$ActivationRetryScreen$Presenter$hIxvXCH0sYPObpzkhbV0b3-VfSA;

    invoke-direct {v2, p0}, Lcom/squareup/ui/onboarding/-$$Lambda$ActivationRetryScreen$Presenter$hIxvXCH0sYPObpzkhbV0b3-VfSA;-><init>(Lcom/squareup/ui/onboarding/ActivationRetryScreen$Presenter;)V

    new-instance v3, Lcom/squareup/ui/onboarding/-$$Lambda$ActivationRetryScreen$Presenter$mLiuc1XkfnWmmpIkUljoTuZd8OA;

    invoke-direct {v3, p0}, Lcom/squareup/ui/onboarding/-$$Lambda$ActivationRetryScreen$Presenter$mLiuc1XkfnWmmpIkUljoTuZd8OA;-><init>(Lcom/squareup/ui/onboarding/ActivationRetryScreen$Presenter;)V

    .line 80
    invoke-virtual {v1, v2, v3}, Lcom/squareup/caller/ProgressAndFailureRxGlue;->handler(Lio/reactivex/functions/Consumer;Lrx/functions/Action3;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    return-void
.end method


# virtual methods
.method public synthetic lambda$retry$0$ActivationRetryScreen$Presenter(Lcom/squareup/server/SimpleResponse;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 81
    iget-object p1, p0, Lcom/squareup/ui/onboarding/ActivationRetryScreen$Presenter;->runner:Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->onRetrySucceeded()V

    return-void
.end method

.method public synthetic lambda$retry$1$ActivationRetryScreen$Presenter(Lcom/squareup/server/SimpleResponse;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 82
    iget-object p1, p0, Lcom/squareup/ui/onboarding/ActivationRetryScreen$Presenter;->runner:Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->onActivationCallFailed()V

    return-void
.end method

.method onLaterTapped()V
    .locals 5

    .line 91
    new-instance v0, Lcom/squareup/register/widgets/ConfirmationIds;

    sget v1, Lcom/squareup/onboarding/flow/R$string;->onboarding_shipping_confirm_have_reader_title:I

    sget v2, Lcom/squareup/onboarding/flow/R$string;->onboarding_shipping_confirm_later:I

    sget v3, Lcom/squareup/common/strings/R$string;->continue_label:I

    sget v4, Lcom/squareup/common/strings/R$string;->cancel:I

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/register/widgets/ConfirmationIds;-><init>(IIII)V

    .line 96
    iget-object v1, p0, Lcom/squareup/ui/onboarding/ActivationRetryScreen$Presenter;->confirmLaterPopupPresenter:Lcom/squareup/mortar/PopupPresenter;

    invoke-virtual {v1, v0}, Lcom/squareup/mortar/PopupPresenter;->show(Landroid/os/Parcelable;)V

    return-void
.end method

.method onRestartTapped()V
    .locals 0

    .line 87
    invoke-direct {p0}, Lcom/squareup/ui/onboarding/ActivationRetryScreen$Presenter;->retry()V

    return-void
.end method
