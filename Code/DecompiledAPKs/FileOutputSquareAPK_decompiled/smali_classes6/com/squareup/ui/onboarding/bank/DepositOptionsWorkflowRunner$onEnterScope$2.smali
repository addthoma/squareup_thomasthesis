.class final Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner$onEnterScope$2;
.super Ljava/lang/Object;
.source "DepositOptionsWorkflowRunner.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsResult;",
        "Lio/reactivex/CompletableSource;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Completable;",
        "it",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsResult;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $newScope:Lmortar/MortarScope;

.field final synthetic this$0:Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner;Lmortar/MortarScope;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner$onEnterScope$2;->this$0:Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner;

    iput-object p2, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner$onEnterScope$2;->$newScope:Lmortar/MortarScope;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsResult;)Lio/reactivex/Completable;
    .locals 3

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner$onEnterScope$2;->this$0:Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner;

    invoke-static {v0}, Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner;->access$getResultHandler$p(Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner;)Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner$ResultHandler;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner$onEnterScope$2;->$newScope:Lmortar/MortarScope;

    invoke-static {v1}, Lcom/squareup/ui/main/RegisterTreeKey;->get(Lmortar/MortarScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object v1

    const-string v2, "RegisterTreeKey.get(newScope)"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v1, Lcom/squareup/ui/main/RegisterTreeKey;

    invoke-interface {v0, p1, v1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner$ResultHandler;->onDepositOptionsResult(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsResult;Lcom/squareup/ui/main/RegisterTreeKey;)Lio/reactivex/Completable;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 17
    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsResult;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsWorkflowRunner$onEnterScope$2;->apply(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsResult;)Lio/reactivex/Completable;

    move-result-object p1

    return-object p1
.end method
