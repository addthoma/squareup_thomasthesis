.class public final Lcom/squareup/ui/onboarding/bank/DepositBankLinking$ScreenData;
.super Ljava/lang/Object;
.source "DepositBankLinkingScreen.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/onboarding/bank/DepositBankLinking;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ScreenData"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\n\u0018\u00002\u00020\u0001B\'\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\tR\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\n\u0010\u000bR\u0011\u0010\u0008\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000c\u0010\rR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000e\u0010\u000fR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0010\u0010\r\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/ui/onboarding/bank/DepositBankLinking$ScreenData;",
        "",
        "personalName",
        "",
        "bankAccountType",
        "Lcom/squareup/protos/client/bankaccount/BankAccountType;",
        "showSpinner",
        "",
        "canGoBack",
        "(Ljava/lang/String;Lcom/squareup/protos/client/bankaccount/BankAccountType;ZZ)V",
        "getBankAccountType",
        "()Lcom/squareup/protos/client/bankaccount/BankAccountType;",
        "getCanGoBack",
        "()Z",
        "getPersonalName",
        "()Ljava/lang/String;",
        "getShowSpinner",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final bankAccountType:Lcom/squareup/protos/client/bankaccount/BankAccountType;

.field private final canGoBack:Z

.field private final personalName:Ljava/lang/String;

.field private final showSpinner:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/squareup/protos/client/bankaccount/BankAccountType;ZZ)V
    .locals 1

    const-string v0, "personalName"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/onboarding/bank/DepositBankLinking$ScreenData;->personalName:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/ui/onboarding/bank/DepositBankLinking$ScreenData;->bankAccountType:Lcom/squareup/protos/client/bankaccount/BankAccountType;

    iput-boolean p3, p0, Lcom/squareup/ui/onboarding/bank/DepositBankLinking$ScreenData;->showSpinner:Z

    iput-boolean p4, p0, Lcom/squareup/ui/onboarding/bank/DepositBankLinking$ScreenData;->canGoBack:Z

    return-void
.end method


# virtual methods
.method public final getBankAccountType()Lcom/squareup/protos/client/bankaccount/BankAccountType;
    .locals 1

    .line 22
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositBankLinking$ScreenData;->bankAccountType:Lcom/squareup/protos/client/bankaccount/BankAccountType;

    return-object v0
.end method

.method public final getCanGoBack()Z
    .locals 1

    .line 24
    iget-boolean v0, p0, Lcom/squareup/ui/onboarding/bank/DepositBankLinking$ScreenData;->canGoBack:Z

    return v0
.end method

.method public final getPersonalName()Ljava/lang/String;
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositBankLinking$ScreenData;->personalName:Ljava/lang/String;

    return-object v0
.end method

.method public final getShowSpinner()Z
    .locals 1

    .line 23
    iget-boolean v0, p0, Lcom/squareup/ui/onboarding/bank/DepositBankLinking$ScreenData;->showSpinner:Z

    return v0
.end method
