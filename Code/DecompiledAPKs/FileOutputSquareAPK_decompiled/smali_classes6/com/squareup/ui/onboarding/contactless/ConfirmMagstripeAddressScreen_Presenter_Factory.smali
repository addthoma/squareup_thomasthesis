.class public final Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen_Presenter_Factory;
.super Ljava/lang/Object;
.source "ConfirmMagstripeAddressScreen_Presenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final activationServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/activation/ActivationService;",
            ">;"
        }
    .end annotation
.end field

.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final messageFactoryProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/FailureMessageFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final modelProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingModel;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final runnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingActivityRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final shippingAddressServiceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/shipping/ShippingAddressService;",
            ">;"
        }
    .end annotation
.end field

.field private final taskQueueProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingActivityRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/activation/ActivationService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingModel;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/shipping/ShippingAddressService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/FailureMessageFactory;",
            ">;)V"
        }
    .end annotation

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object p1, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen_Presenter_Factory;->runnerProvider:Ljavax/inject/Provider;

    .line 58
    iput-object p2, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen_Presenter_Factory;->activationServiceProvider:Ljavax/inject/Provider;

    .line 59
    iput-object p3, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen_Presenter_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    .line 60
    iput-object p4, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen_Presenter_Factory;->modelProvider:Ljavax/inject/Provider;

    .line 61
    iput-object p5, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen_Presenter_Factory;->taskQueueProvider:Ljavax/inject/Provider;

    .line 62
    iput-object p6, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen_Presenter_Factory;->settingsProvider:Ljavax/inject/Provider;

    .line 63
    iput-object p7, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    .line 64
    iput-object p8, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen_Presenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 65
    iput-object p9, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen_Presenter_Factory;->shippingAddressServiceProvider:Ljavax/inject/Provider;

    .line 66
    iput-object p10, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen_Presenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    .line 67
    iput-object p11, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen_Presenter_Factory;->messageFactoryProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen_Presenter_Factory;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingActivityRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/activation/ActivationService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/onboarding/OnboardingModel;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/shipping/ShippingAddressService;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/receiving/FailureMessageFactory;",
            ">;)",
            "Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen_Presenter_Factory;"
        }
    .end annotation

    .line 83
    new-instance v12, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen_Presenter_Factory;

    move-object v0, v12

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen_Presenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v12
.end method

.method public static newInstance(Lcom/squareup/ui/onboarding/OnboardingActivityRunner;Lcom/squareup/server/activation/ActivationService;Lio/reactivex/Scheduler;Lcom/squareup/ui/onboarding/OnboardingModel;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;Lcom/squareup/server/shipping/ShippingAddressService;Lcom/squareup/settings/server/Features;Lcom/squareup/receiving/FailureMessageFactory;)Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;
    .locals 13

    .line 91
    new-instance v12, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;

    move-object v0, v12

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;-><init>(Lcom/squareup/ui/onboarding/OnboardingActivityRunner;Lcom/squareup/server/activation/ActivationService;Lio/reactivex/Scheduler;Lcom/squareup/ui/onboarding/OnboardingModel;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;Lcom/squareup/server/shipping/ShippingAddressService;Lcom/squareup/settings/server/Features;Lcom/squareup/receiving/FailureMessageFactory;)V

    return-object v12
.end method


# virtual methods
.method public get()Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;
    .locals 12

    .line 72
    iget-object v0, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen_Presenter_Factory;->runnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen_Presenter_Factory;->activationServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/server/activation/ActivationService;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen_Presenter_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lio/reactivex/Scheduler;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen_Presenter_Factory;->modelProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/ui/onboarding/OnboardingModel;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen_Presenter_Factory;->taskQueueProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/queue/retrofit/RetrofitQueue;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen_Presenter_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen_Presenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/analytics/Analytics;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen_Presenter_Factory;->shippingAddressServiceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/squareup/server/shipping/ShippingAddressService;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen_Presenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/squareup/settings/server/Features;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen_Presenter_Factory;->messageFactoryProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v11, v0

    check-cast v11, Lcom/squareup/receiving/FailureMessageFactory;

    invoke-static/range {v1 .. v11}, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen_Presenter_Factory;->newInstance(Lcom/squareup/ui/onboarding/OnboardingActivityRunner;Lcom/squareup/server/activation/ActivationService;Lio/reactivex/Scheduler;Lcom/squareup/ui/onboarding/OnboardingModel;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/util/Res;Lcom/squareup/analytics/Analytics;Lcom/squareup/server/shipping/ShippingAddressService;Lcom/squareup/settings/server/Features;Lcom/squareup/receiving/FailureMessageFactory;)Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 18
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen_Presenter_Factory;->get()Lcom/squareup/ui/onboarding/contactless/ConfirmMagstripeAddressScreen$Presenter;

    move-result-object v0

    return-object v0
.end method
