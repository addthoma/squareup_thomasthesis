.class public Lcom/squareup/ui/onboarding/MerchantSubcategoryView;
.super Lcom/squareup/ui/onboarding/BaseCategoryView;
.source "MerchantSubcategoryView.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/onboarding/BaseCategoryView<",
        "Lcom/squareup/server/activation/ActivationResources$BusinessSubcategory;",
        ">;"
    }
.end annotation


# instance fields
.field presenter:Lcom/squareup/ui/onboarding/MerchantSubcategoryScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 15
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/onboarding/BaseCategoryView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 16
    const-class p2, Lcom/squareup/ui/onboarding/MerchantSubcategoryScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/onboarding/MerchantSubcategoryScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/onboarding/MerchantSubcategoryScreen$Component;->inject(Lcom/squareup/ui/onboarding/MerchantSubcategoryView;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 10
    invoke-super {p0}, Lcom/squareup/ui/onboarding/BaseCategoryView;->getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic hideSubtitle()V
    .locals 0

    .line 10
    invoke-super {p0}, Lcom/squareup/ui/onboarding/BaseCategoryView;->hideSubtitle()V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .line 28
    iget-object v0, p0, Lcom/squareup/ui/onboarding/MerchantSubcategoryView;->presenter:Lcom/squareup/ui/onboarding/MerchantSubcategoryScreen$Presenter;

    iget-object v0, v0, Lcom/squareup/ui/onboarding/MerchantSubcategoryScreen$Presenter;->warningPopupPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/MerchantSubcategoryView;->getWarningPopup()Lcom/squareup/flowlegacy/WarningPopup;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->dropView(Lcom/squareup/mortar/Popup;)V

    .line 29
    iget-object v0, p0, Lcom/squareup/ui/onboarding/MerchantSubcategoryView;->presenter:Lcom/squareup/ui/onboarding/MerchantSubcategoryScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/onboarding/MerchantSubcategoryScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 31
    invoke-super {p0}, Lcom/squareup/ui/onboarding/BaseCategoryView;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 20
    invoke-super {p0}, Lcom/squareup/ui/onboarding/BaseCategoryView;->onFinishInflate()V

    .line 22
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/MerchantSubcategoryView;->hideSubtitle()V

    .line 23
    iget-object v0, p0, Lcom/squareup/ui/onboarding/MerchantSubcategoryView;->presenter:Lcom/squareup/ui/onboarding/MerchantSubcategoryScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/onboarding/MerchantSubcategoryScreen$Presenter;->takeView(Ljava/lang/Object;)V

    .line 24
    iget-object v0, p0, Lcom/squareup/ui/onboarding/MerchantSubcategoryView;->presenter:Lcom/squareup/ui/onboarding/MerchantSubcategoryScreen$Presenter;

    iget-object v0, v0, Lcom/squareup/ui/onboarding/MerchantSubcategoryScreen$Presenter;->warningPopupPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/MerchantSubcategoryView;->getWarningPopup()Lcom/squareup/flowlegacy/WarningPopup;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method
