.class final Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$setUpSameDayDeposit$1;
.super Ljava/lang/Object;
.source "DepositOptionsReactor.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor;->setUpSameDayDeposit(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$CardLinking$SettingUpSameDayDeposit;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult;",
        "it",
        "Lcom/squareup/depositschedule/DepositScheduleSettings$State;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $state:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$CardLinking$SettingUpSameDayDeposit;


# direct methods
.method constructor <init>(Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$CardLinking$SettingUpSameDayDeposit;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$setUpSameDayDeposit$1;->$state:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$CardLinking$SettingUpSameDayDeposit;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/depositschedule/DepositScheduleSettings$State;)Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult;
    .locals 1

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 403
    iget-object p1, p1, Lcom/squareup/depositschedule/DepositScheduleSettings$State;->depositScheduleState:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;

    sget-object v0, Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;->SUCCEEDED:Lcom/squareup/depositschedule/DepositScheduleSettings$DepositScheduleState;

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_1

    .line 405
    iget-object v0, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$setUpSameDayDeposit$1;->$state:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$CardLinking$SettingUpSameDayDeposit;

    invoke-virtual {v0}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$CardLinking$SettingUpSameDayDeposit;->getBankSuccess()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankSuccessCardSuccessSameDayDepositSetupSuccess;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankSuccessCardSuccessSameDayDepositSetupSuccess;

    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult;

    goto :goto_1

    :cond_1
    if-eqz p1, :cond_2

    .line 406
    sget-object p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankPendingCardSuccessSameDayDepositSetupSuccess;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankPendingCardSuccessSameDayDepositSetupSuccess;

    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult;

    goto :goto_1

    .line 407
    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$setUpSameDayDeposit$1;->$state:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$CardLinking$SettingUpSameDayDeposit;

    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$CardLinking$SettingUpSameDayDeposit;->getBankSuccess()Z

    move-result p1

    if-eqz p1, :cond_3

    sget-object p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankSuccessCardSuccessSameDayDepositSetupFailure;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankSuccessCardSuccessSameDayDepositSetupFailure;

    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult;

    goto :goto_1

    .line 408
    :cond_3
    sget-object p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankPendingCardSuccessSameDayDepositSetupFailure;->INSTANCE:Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult$BankPendingCardSuccessSameDayDepositSetupFailure;

    check-cast p1, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult;

    :goto_1
    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 81
    check-cast p1, Lcom/squareup/depositschedule/DepositScheduleSettings$State;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$setUpSameDayDeposit$1;->apply(Lcom/squareup/depositschedule/DepositScheduleSettings$State;)Lcom/squareup/ui/onboarding/bank/DepositOptionsReactor$DepositOptionsState$LinkingResult;

    move-result-object p1

    return-object p1
.end method
