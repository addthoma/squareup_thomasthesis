.class public Lcom/squareup/ui/onboarding/BusinessInfoView;
.super Landroid/widget/LinearLayout;
.source "BusinessInfoView.java"


# instance fields
.field private businessName:Landroid/widget/EditText;

.field private ein:Lcom/squareup/widgets/SelectableEditText;

.field private einForm:Landroid/view/View;

.field private einRadios:Lcom/squareup/widgets/CheckableGroup;

.field private incomeRadios:Lcom/squareup/widgets/CheckableGroup;

.field presenter:Lcom/squareup/ui/onboarding/BusinessInfoScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final warningPopup:Lcom/squareup/flowlegacy/WarningPopup;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 49
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 51
    const-class p2, Lcom/squareup/ui/onboarding/BusinessInfoScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/squareup/ui/onboarding/BusinessInfoScreen$Component;

    invoke-interface {p2, p0}, Lcom/squareup/ui/onboarding/BusinessInfoScreen$Component;->inject(Lcom/squareup/ui/onboarding/BusinessInfoView;)V

    .line 53
    new-instance p2, Lcom/squareup/flowlegacy/WarningPopup;

    invoke-direct {p2, p1}, Lcom/squareup/flowlegacy/WarningPopup;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lcom/squareup/ui/onboarding/BusinessInfoView;->warningPopup:Lcom/squareup/flowlegacy/WarningPopup;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/onboarding/BusinessInfoView;)V
    .locals 0

    .line 37
    invoke-direct {p0}, Lcom/squareup/ui/onboarding/BusinessInfoView;->updateEINVisibility()V

    return-void
.end method

.method private updateEINVisibility()V
    .locals 3

    .line 126
    iget-object v0, p0, Lcom/squareup/ui/onboarding/BusinessInfoView;->einForm:Landroid/view/View;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/BusinessInfoView;->einRadios:Lcom/squareup/widgets/CheckableGroup;

    invoke-virtual {v1}, Lcom/squareup/widgets/CheckableGroup;->getCheckedId()I

    move-result v1

    sget v2, Lcom/squareup/onboarding/flow/R$id;->has_ein:I

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-static {v0, v1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method


# virtual methods
.method protected bindAdvanceOnGo(Landroid/widget/EditText;)V
    .locals 1

    const/4 v0, 0x2

    .line 130
    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setImeOptions(I)V

    .line 131
    new-instance v0, Lcom/squareup/ui/onboarding/BusinessInfoView$4;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/onboarding/BusinessInfoView$4;-><init>(Lcom/squareup/ui/onboarding/BusinessInfoView;Landroid/widget/EditText;)V

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    return-void
.end method

.method public getActionBar()Lcom/squareup/marin/widgets/MarinActionBar;
    .locals 1

    .line 165
    sget v0, Lcom/squareup/containerconstants/R$id;->stable_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marin/widgets/ActionBarView;

    .line 166
    invoke-virtual {v0}, Lcom/squareup/marin/widgets/ActionBarView;->getPresenter()Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    return-object v0
.end method

.method getBusinessName()Ljava/lang/String;
    .locals 1

    .line 153
    iget-object v0, p0, Lcom/squareup/ui/onboarding/BusinessInfoView;->businessName:Landroid/widget/EditText;

    invoke-static {v0}, Lcom/squareup/util/Views;->getValue(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getEin()Ljava/lang/String;
    .locals 1

    .line 161
    iget-object v0, p0, Lcom/squareup/ui/onboarding/BusinessInfoView;->ein:Lcom/squareup/widgets/SelectableEditText;

    invoke-static {v0}, Lcom/squareup/util/Views;->getValue(Landroid/widget/TextView;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getEinCheckedIndex()I
    .locals 1

    .line 157
    iget-object v0, p0, Lcom/squareup/ui/onboarding/BusinessInfoView;->einRadios:Lcom/squareup/widgets/CheckableGroup;

    invoke-virtual {v0}, Lcom/squareup/widgets/CheckableGroup;->getCheckedId()I

    move-result v0

    return v0
.end method

.method public synthetic lambda$onFinishInflate$0$BusinessInfoView()V
    .locals 1

    .line 77
    iget-object v0, p0, Lcom/squareup/ui/onboarding/BusinessInfoView;->businessName:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    return-void
.end method

.method public synthetic lambda$onFinishInflate$1$BusinessInfoView(Lcom/squareup/widgets/CheckableGroup;II)V
    .locals 0

    .line 81
    invoke-direct {p0}, Lcom/squareup/ui/onboarding/BusinessInfoView;->updateEINVisibility()V

    .line 82
    sget p1, Lcom/squareup/onboarding/flow/R$id;->has_ein:I

    if-ne p2, p1, :cond_0

    .line 83
    iget-object p1, p0, Lcom/squareup/ui/onboarding/BusinessInfoView;->ein:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {p1}, Lcom/squareup/widgets/SelectableEditText;->requestFocus()Z

    goto :goto_0

    .line 86
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/onboarding/BusinessInfoView;->ein:Lcom/squareup/widgets/SelectableEditText;

    invoke-static {p1}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    .line 88
    :goto_0
    iget-object p1, p0, Lcom/squareup/ui/onboarding/BusinessInfoView;->presenter:Lcom/squareup/ui/onboarding/BusinessInfoScreen$Presenter;

    invoke-virtual {p1}, Lcom/squareup/ui/onboarding/BusinessInfoScreen$Presenter;->onEinRadioTapped()V

    return-void
.end method

.method public synthetic lambda$onFinishInflate$2$BusinessInfoView(Lcom/squareup/widgets/CheckableGroup;II)V
    .locals 0

    .line 95
    iget-object p1, p0, Lcom/squareup/ui/onboarding/BusinessInfoView;->presenter:Lcom/squareup/ui/onboarding/BusinessInfoScreen$Presenter;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/onboarding/BusinessInfoScreen$Presenter;->onIncomeRangeSelected(I)V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .line 114
    iget-object v0, p0, Lcom/squareup/ui/onboarding/BusinessInfoView;->presenter:Lcom/squareup/ui/onboarding/BusinessInfoScreen$Presenter;

    iget-object v0, v0, Lcom/squareup/ui/onboarding/BusinessInfoScreen$Presenter;->warningPopupPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/BusinessInfoView;->warningPopup:Lcom/squareup/flowlegacy/WarningPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->dropView(Lcom/squareup/mortar/Popup;)V

    .line 115
    iget-object v0, p0, Lcom/squareup/ui/onboarding/BusinessInfoView;->presenter:Lcom/squareup/ui/onboarding/BusinessInfoScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/onboarding/BusinessInfoScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 117
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 4

    .line 57
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 59
    sget v0, Lcom/squareup/onboarding/flow/R$id;->title:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    .line 60
    sget v1, Lcom/squareup/onboarding/flow/R$string;->business_info_heading:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(I)V

    .line 61
    sget v0, Lcom/squareup/onboarding/flow/R$id;->subtitle:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/MessageView;

    .line 62
    sget v1, Lcom/squareup/onboarding/flow/R$string;->business_info_subheading:I

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/MessageView;->setText(I)V

    .line 64
    new-instance v0, Lcom/squareup/ui/onboarding/BusinessInfoView$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/onboarding/BusinessInfoView$1;-><init>(Lcom/squareup/ui/onboarding/BusinessInfoView;)V

    .line 70
    sget v1, Lcom/squareup/onboarding/flow/R$id;->business_name:I

    invoke-static {p0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/squareup/ui/onboarding/BusinessInfoView;->businessName:Landroid/widget/EditText;

    .line 71
    iget-object v1, p0, Lcom/squareup/ui/onboarding/BusinessInfoView;->businessName:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 72
    iget-object v1, p0, Lcom/squareup/ui/onboarding/BusinessInfoView;->businessName:Landroid/widget/EditText;

    new-instance v2, Lcom/squareup/ui/onboarding/BusinessInfoView$2;

    invoke-direct {v2, p0}, Lcom/squareup/ui/onboarding/BusinessInfoView$2;-><init>(Lcom/squareup/ui/onboarding/BusinessInfoView;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 77
    iget-object v1, p0, Lcom/squareup/ui/onboarding/BusinessInfoView;->businessName:Landroid/widget/EditText;

    new-instance v2, Lcom/squareup/ui/onboarding/-$$Lambda$BusinessInfoView$enlJLwYRTNYQKOGQli8LaHIYFNs;

    invoke-direct {v2, p0}, Lcom/squareup/ui/onboarding/-$$Lambda$BusinessInfoView$enlJLwYRTNYQKOGQli8LaHIYFNs;-><init>(Lcom/squareup/ui/onboarding/BusinessInfoView;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->post(Ljava/lang/Runnable;)Z

    .line 79
    sget v1, Lcom/squareup/onboarding/flow/R$id;->ein_radios:I

    invoke-static {p0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/widgets/CheckableGroup;

    iput-object v1, p0, Lcom/squareup/ui/onboarding/BusinessInfoView;->einRadios:Lcom/squareup/widgets/CheckableGroup;

    .line 80
    iget-object v1, p0, Lcom/squareup/ui/onboarding/BusinessInfoView;->einRadios:Lcom/squareup/widgets/CheckableGroup;

    new-instance v2, Lcom/squareup/ui/onboarding/-$$Lambda$BusinessInfoView$t32hRAB-Iww8jlosfw1jpm5LJ6w;

    invoke-direct {v2, p0}, Lcom/squareup/ui/onboarding/-$$Lambda$BusinessInfoView$t32hRAB-Iww8jlosfw1jpm5LJ6w;-><init>(Lcom/squareup/ui/onboarding/BusinessInfoView;)V

    invoke-virtual {v1, v2}, Lcom/squareup/widgets/CheckableGroup;->setOnCheckedChangeListener(Lcom/squareup/widgets/CheckableGroup$OnCheckedChangeListener;)V

    .line 91
    sget v1, Lcom/squareup/onboarding/flow/R$id;->income_radios:I

    invoke-static {p0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/widgets/CheckableGroup;

    iput-object v1, p0, Lcom/squareup/ui/onboarding/BusinessInfoView;->incomeRadios:Lcom/squareup/widgets/CheckableGroup;

    .line 92
    iget-object v1, p0, Lcom/squareup/ui/onboarding/BusinessInfoView;->incomeRadios:Lcom/squareup/widgets/CheckableGroup;

    new-instance v2, Lcom/squareup/ui/onboarding/-$$Lambda$BusinessInfoView$O0GYqfHoRF2VL470UqjY0h0LipQ;

    invoke-direct {v2, p0}, Lcom/squareup/ui/onboarding/-$$Lambda$BusinessInfoView$O0GYqfHoRF2VL470UqjY0h0LipQ;-><init>(Lcom/squareup/ui/onboarding/BusinessInfoView;)V

    invoke-virtual {v1, v2}, Lcom/squareup/widgets/CheckableGroup;->setOnCheckedChangeListener(Lcom/squareup/widgets/CheckableGroup$OnCheckedChangeListener;)V

    .line 97
    sget v1, Lcom/squareup/onboarding/flow/R$id;->ein_text:I

    invoke-static {p0, v1}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/widgets/SelectableEditText;

    iput-object v1, p0, Lcom/squareup/ui/onboarding/BusinessInfoView;->ein:Lcom/squareup/widgets/SelectableEditText;

    .line 98
    iget-object v1, p0, Lcom/squareup/ui/onboarding/BusinessInfoView;->ein:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v1, v0}, Lcom/squareup/widgets/SelectableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 99
    iget-object v0, p0, Lcom/squareup/ui/onboarding/BusinessInfoView;->ein:Lcom/squareup/widgets/SelectableEditText;

    new-instance v1, Lcom/squareup/text/ScrubbingTextWatcher;

    invoke-static {}, Lcom/squareup/text/TinFormatter;->createEinFormatter()Lcom/squareup/text/TinFormatter;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/onboarding/BusinessInfoView;->ein:Lcom/squareup/widgets/SelectableEditText;

    invoke-direct {v1, v2, v3}, Lcom/squareup/text/ScrubbingTextWatcher;-><init>(Lcom/squareup/text/Scrubber;Lcom/squareup/text/HasSelectableText;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 100
    iget-object v0, p0, Lcom/squareup/ui/onboarding/BusinessInfoView;->ein:Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {p0, v0}, Lcom/squareup/ui/onboarding/BusinessInfoView;->bindAdvanceOnGo(Landroid/widget/EditText;)V

    .line 101
    iget-object v0, p0, Lcom/squareup/ui/onboarding/BusinessInfoView;->ein:Lcom/squareup/widgets/SelectableEditText;

    new-instance v1, Lcom/squareup/ui/onboarding/BusinessInfoView$3;

    invoke-direct {v1, p0}, Lcom/squareup/ui/onboarding/BusinessInfoView$3;-><init>(Lcom/squareup/ui/onboarding/BusinessInfoView;)V

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/SelectableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 107
    sget v0, Lcom/squareup/onboarding/flow/R$id;->ein_form:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/onboarding/BusinessInfoView;->einForm:Landroid/view/View;

    .line 109
    iget-object v0, p0, Lcom/squareup/ui/onboarding/BusinessInfoView;->presenter:Lcom/squareup/ui/onboarding/BusinessInfoScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/onboarding/BusinessInfoScreen$Presenter;->takeView(Ljava/lang/Object;)V

    .line 110
    iget-object v0, p0, Lcom/squareup/ui/onboarding/BusinessInfoView;->presenter:Lcom/squareup/ui/onboarding/BusinessInfoScreen$Presenter;

    iget-object v0, v0, Lcom/squareup/ui/onboarding/BusinessInfoScreen$Presenter;->warningPopupPresenter:Lcom/squareup/flowlegacy/NoResultPopupPresenter;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/BusinessInfoView;->warningPopup:Lcom/squareup/flowlegacy/WarningPopup;

    invoke-virtual {v0, v1}, Lcom/squareup/flowlegacy/NoResultPopupPresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public setBusinessName(Ljava/lang/String;)V
    .locals 1

    .line 121
    iget-object v0, p0, Lcom/squareup/ui/onboarding/BusinessInfoView;->businessName:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method setIncomeOptions(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/server/activation/ActivationResources$RevenueEntry;",
            ">;)V"
        }
    .end annotation

    .line 144
    iget-object v0, p0, Lcom/squareup/ui/onboarding/BusinessInfoView;->incomeRadios:Lcom/squareup/widgets/CheckableGroup;

    invoke-virtual {v0}, Lcom/squareup/widgets/CheckableGroup;->removeAllViews()V

    .line 145
    invoke-virtual {p0}, Lcom/squareup/ui/onboarding/BusinessInfoView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/onboarding/BusinessInfoView;->incomeRadios:Lcom/squareup/widgets/CheckableGroup;

    const/4 v2, 0x1

    invoke-static {v0, v1, p1, v2}, Lcom/squareup/ui/CheckableGroups;->addAsRows(Landroid/view/LayoutInflater;Lcom/squareup/widgets/CheckableGroup;Ljava/util/List;Z)V

    return-void
.end method

.method setSelectedIncome(I)V
    .locals 1

    .line 149
    iget-object v0, p0, Lcom/squareup/ui/onboarding/BusinessInfoView;->incomeRadios:Lcom/squareup/widgets/CheckableGroup;

    invoke-virtual {v0, p1}, Lcom/squareup/widgets/CheckableGroup;->check(I)V

    return-void
.end method
