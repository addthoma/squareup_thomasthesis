.class public final synthetic Lcom/squareup/ui/onboarding/-$$Lambda$OnboardingActivityRunner$hAf8FO0OZ2ppwRskFx_gxl05kuY;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lio/reactivex/functions/Function;


# instance fields
.field private final synthetic f$0:Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

.field private final synthetic f$1:Lcom/squareup/ui/onboarding/OnboardingModel;

.field private final synthetic f$2:[Ljava/lang/Class;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/ui/onboarding/OnboardingActivityRunner;Lcom/squareup/ui/onboarding/OnboardingModel;[Ljava/lang/Class;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/onboarding/-$$Lambda$OnboardingActivityRunner$hAf8FO0OZ2ppwRskFx_gxl05kuY;->f$0:Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

    iput-object p2, p0, Lcom/squareup/ui/onboarding/-$$Lambda$OnboardingActivityRunner$hAf8FO0OZ2ppwRskFx_gxl05kuY;->f$1:Lcom/squareup/ui/onboarding/OnboardingModel;

    iput-object p3, p0, Lcom/squareup/ui/onboarding/-$$Lambda$OnboardingActivityRunner$hAf8FO0OZ2ppwRskFx_gxl05kuY;->f$2:[Ljava/lang/Class;

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    iget-object v0, p0, Lcom/squareup/ui/onboarding/-$$Lambda$OnboardingActivityRunner$hAf8FO0OZ2ppwRskFx_gxl05kuY;->f$0:Lcom/squareup/ui/onboarding/OnboardingActivityRunner;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/-$$Lambda$OnboardingActivityRunner$hAf8FO0OZ2ppwRskFx_gxl05kuY;->f$1:Lcom/squareup/ui/onboarding/OnboardingModel;

    iget-object v2, p0, Lcom/squareup/ui/onboarding/-$$Lambda$OnboardingActivityRunner$hAf8FO0OZ2ppwRskFx_gxl05kuY;->f$2:[Ljava/lang/Class;

    check-cast p1, Lcom/squareup/onboarding/OnboardingVerticalSelectionRunner$VerticalSelectionResult;

    invoke-virtual {v0, v1, v2, p1}, Lcom/squareup/ui/onboarding/OnboardingActivityRunner;->lambda$goToNextScreenAfterBusinessAddress$2$OnboardingActivityRunner(Lcom/squareup/ui/onboarding/OnboardingModel;[Ljava/lang/Class;Lcom/squareup/onboarding/OnboardingVerticalSelectionRunner$VerticalSelectionResult;)Lkotlin/Unit;

    move-result-object p1

    return-object p1
.end method
