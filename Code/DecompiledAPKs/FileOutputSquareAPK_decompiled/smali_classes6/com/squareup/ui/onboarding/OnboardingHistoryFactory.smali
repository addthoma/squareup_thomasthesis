.class public final Lcom/squareup/ui/onboarding/OnboardingHistoryFactory;
.super Ljava/lang/Object;
.source "OnboardingHistoryFactory.kt"

# interfaces
.implements Lcom/squareup/ui/main/HistoryFactory;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u001a\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u0008\u0010\t\u001a\u0004\u0018\u00010\u0006H\u0016J\u0010\u0010\n\u001a\n\u0012\u0004\u0012\u00020\u000c\u0018\u00010\u000bH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/ui/onboarding/OnboardingHistoryFactory;",
        "Lcom/squareup/ui/main/HistoryFactory;",
        "onboardingDiverter",
        "Lcom/squareup/onboarding/OnboardingDiverter;",
        "(Lcom/squareup/onboarding/OnboardingDiverter;)V",
        "createHistory",
        "Lflow/History;",
        "home",
        "Lcom/squareup/ui/main/Home;",
        "currentHistory",
        "getPermissions",
        "",
        "Lcom/squareup/permissions/Permission;",
        "onboarding_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final onboardingDiverter:Lcom/squareup/onboarding/OnboardingDiverter;


# direct methods
.method public constructor <init>(Lcom/squareup/onboarding/OnboardingDiverter;)V
    .locals 1

    const-string v0, "onboardingDiverter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/onboarding/OnboardingHistoryFactory;->onboardingDiverter:Lcom/squareup/onboarding/OnboardingDiverter;

    return-void
.end method


# virtual methods
.method public createHistory(Lcom/squareup/ui/main/Home;Lflow/History;)Lflow/History;
    .locals 1

    const-string v0, "home"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    invoke-static {p1, p2}, Lcom/squareup/ui/main/HomeKt;->buildUpon(Lcom/squareup/ui/main/Home;Lflow/History;)Lflow/History$Builder;

    move-result-object p1

    .line 19
    new-instance p2, Lcom/squareup/onboarding/OnboardingDeepLinkBootstrapScreen;

    iget-object v0, p0, Lcom/squareup/ui/onboarding/OnboardingHistoryFactory;->onboardingDiverter:Lcom/squareup/onboarding/OnboardingDiverter;

    invoke-direct {p2, v0}, Lcom/squareup/onboarding/OnboardingDeepLinkBootstrapScreen;-><init>(Lcom/squareup/onboarding/OnboardingDiverter;)V

    invoke-virtual {p1, p2}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    move-result-object p1

    .line 20
    invoke-virtual {p1}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object p1

    const-string p2, "home.buildUpon(currentHi\u2026Diverter))\n      .build()"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public getPermissions()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lcom/squareup/permissions/Permission;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method
