.class public Lcom/squareup/ui/onboarding/OnboardingErrorScreen;
.super Lcom/squareup/ui/onboarding/InLoggedInOnboardingScope;
.source "OnboardingErrorScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/onboarding/OnboardingErrorScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/onboarding/OnboardingErrorScreen$Component;,
        Lcom/squareup/ui/onboarding/OnboardingErrorScreen$Presenter;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/onboarding/OnboardingErrorScreen;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/onboarding/OnboardingErrorScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 27
    new-instance v0, Lcom/squareup/ui/onboarding/OnboardingErrorScreen;

    invoke-direct {v0}, Lcom/squareup/ui/onboarding/OnboardingErrorScreen;-><init>()V

    sput-object v0, Lcom/squareup/ui/onboarding/OnboardingErrorScreen;->INSTANCE:Lcom/squareup/ui/onboarding/OnboardingErrorScreen;

    .line 32
    sget-object v0, Lcom/squareup/ui/onboarding/OnboardingErrorScreen;->INSTANCE:Lcom/squareup/ui/onboarding/OnboardingErrorScreen;

    .line 33
    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/onboarding/OnboardingErrorScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 29
    invoke-direct {p0}, Lcom/squareup/ui/onboarding/InLoggedInOnboardingScope;-><init>()V

    return-void
.end method


# virtual methods
.method public screenLayout()I
    .locals 1

    .line 122
    sget v0, Lcom/squareup/onboarding/flow/R$layout;->onboarding_error_view:I

    return v0
.end method
