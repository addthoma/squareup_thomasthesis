.class final Lcom/squareup/ui/onboarding/bank/DepositOptionsConfirmationDialogFactory$create$1;
.super Ljava/lang/Object;
.source "DepositOptionsConfirmationDialogFactory.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/onboarding/bank/DepositOptionsConfirmationDialogFactory;->create(Landroid/content/Context;)Lio/reactivex/Single;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u0016\u0010\u0003\u001a\u0012\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004j\u0002`\u0007H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "Landroid/app/AlertDialog;",
        "kotlin.jvm.PlatformType",
        "screen",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/widgets/warning/Warning;",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsConfirmationDialog$Event;",
        "Lcom/squareup/ui/onboarding/bank/DepositOptionsConfirmationDialogScreen;",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $context:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsConfirmationDialogFactory$create$1;->$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lcom/squareup/workflow/legacy/Screen;)Landroid/app/AlertDialog;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/workflow/legacy/Screen<",
            "Lcom/squareup/widgets/warning/Warning;",
            "Lcom/squareup/ui/onboarding/bank/DepositOptionsConfirmationDialog$Event;",
            ">;)",
            "Landroid/app/AlertDialog;"
        }
    .end annotation

    const-string v0, "screen"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    iget-object v0, p1, Lcom/squareup/workflow/legacy/Screen;->data:Ljava/lang/Object;

    check-cast v0, Lcom/squareup/widgets/warning/Warning;

    iget-object v1, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsConfirmationDialogFactory$create$1;->$context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/widgets/warning/Warning;->getStrings(Landroid/content/res/Resources;)Lcom/squareup/widgets/warning/Warning$Strings;

    move-result-object v0

    .line 21
    iget-object p1, p1, Lcom/squareup/workflow/legacy/Screen;->workflow:Lcom/squareup/workflow/legacy/WorkflowInput;

    .line 22
    new-instance v1, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    iget-object v2, p0, Lcom/squareup/ui/onboarding/bank/DepositOptionsConfirmationDialogFactory$create$1;->$context:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 23
    iget-object v2, v0, Lcom/squareup/widgets/warning/Warning$Strings;->title:Ljava/lang/String;

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v1

    .line 24
    iget-object v0, v0, Lcom/squareup/widgets/warning/Warning$Strings;->body:Ljava/lang/String;

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    .line 26
    sget v1, Lcom/squareup/common/strings/R$string;->continue_label:I

    .line 27
    new-instance v2, Lcom/squareup/ui/onboarding/bank/DepositOptionsConfirmationDialogFactory$create$1$1;

    invoke-direct {v2, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsConfirmationDialogFactory$create$1$1;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v2, Landroid/content/DialogInterface$OnClickListener;

    .line 25
    invoke-virtual {v0, v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    .line 29
    sget v1, Lcom/squareup/common/strings/R$string;->cancel:I

    .line 30
    new-instance v2, Lcom/squareup/ui/onboarding/bank/DepositOptionsConfirmationDialogFactory$create$1$2;

    invoke-direct {v2, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsConfirmationDialogFactory$create$1$2;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v2, Landroid/content/DialogInterface$OnClickListener;

    .line 28
    invoke-virtual {v0, v1, v2}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    .line 31
    new-instance v1, Lcom/squareup/ui/onboarding/bank/DepositOptionsConfirmationDialogFactory$create$1$3;

    invoke-direct {v1, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsConfirmationDialogFactory$create$1$3;-><init>(Lcom/squareup/workflow/legacy/WorkflowInput;)V

    check-cast v1, Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object p1

    .line 32
    invoke-virtual {p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 13
    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/onboarding/bank/DepositOptionsConfirmationDialogFactory$create$1;->apply(Lcom/squareup/workflow/legacy/Screen;)Landroid/app/AlertDialog;

    move-result-object p1

    return-object p1
.end method
