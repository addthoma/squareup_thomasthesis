.class Lcom/squareup/ui/payment/SwipeHandler$DefaultSwipeHandler;
.super Ljava/lang/Object;
.source "SwipeHandler.java"

# interfaces
.implements Lcom/squareup/ui/seller/SwipedCardHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/payment/SwipeHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DefaultSwipeHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/payment/SwipeHandler;


# direct methods
.method private constructor <init>(Lcom/squareup/ui/payment/SwipeHandler;)V
    .locals 0

    .line 255
    iput-object p1, p0, Lcom/squareup/ui/payment/SwipeHandler$DefaultSwipeHandler;->this$0:Lcom/squareup/ui/payment/SwipeHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/ui/payment/SwipeHandler;Lcom/squareup/ui/payment/SwipeHandler$1;)V
    .locals 0

    .line 255
    invoke-direct {p0, p1}, Lcom/squareup/ui/payment/SwipeHandler$DefaultSwipeHandler;-><init>(Lcom/squareup/ui/payment/SwipeHandler;)V

    return-void
.end method


# virtual methods
.method public ignoreSwipeFor(Lcom/squareup/Card;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public processSwipedCard(Lcom/squareup/cardreader/CardReaderId;Lcom/squareup/Card;)V
    .locals 3

    .line 257
    iget-object v0, p0, Lcom/squareup/ui/payment/SwipeHandler$DefaultSwipeHandler;->this$0:Lcom/squareup/ui/payment/SwipeHandler;

    invoke-static {v0}, Lcom/squareup/ui/payment/SwipeHandler;->access$300(Lcom/squareup/ui/payment/SwipeHandler;)Lcom/squareup/payment/Transaction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->attributeChargeIfPossible()V

    .line 258
    iget-object v0, p0, Lcom/squareup/ui/payment/SwipeHandler$DefaultSwipeHandler;->this$0:Lcom/squareup/ui/payment/SwipeHandler;

    invoke-static {v0}, Lcom/squareup/ui/payment/SwipeHandler;->access$300(Lcom/squareup/ui/payment/SwipeHandler;)Lcom/squareup/payment/Transaction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasItems()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 260
    iget-object v0, p0, Lcom/squareup/ui/payment/SwipeHandler$DefaultSwipeHandler;->this$0:Lcom/squareup/ui/payment/SwipeHandler;

    invoke-static {v0}, Lcom/squareup/ui/payment/SwipeHandler;->access$400(Lcom/squareup/ui/payment/SwipeHandler;)Lcom/squareup/swipe/SwipeValidator;

    move-result-object v0

    iget-object v2, p0, Lcom/squareup/ui/payment/SwipeHandler$DefaultSwipeHandler;->this$0:Lcom/squareup/ui/payment/SwipeHandler;

    invoke-static {v2}, Lcom/squareup/ui/payment/SwipeHandler;->access$300(Lcom/squareup/ui/payment/SwipeHandler;)Lcom/squareup/payment/Transaction;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/payment/Transaction;->getTenderAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v2

    invoke-virtual {v0, p2, v2}, Lcom/squareup/swipe/SwipeValidator;->isInAuthRange(Lcom/squareup/Card;Lcom/squareup/protos/common/Money;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 261
    iget-object v0, p0, Lcom/squareup/ui/payment/SwipeHandler$DefaultSwipeHandler;->this$0:Lcom/squareup/ui/payment/SwipeHandler;

    invoke-static {v0}, Lcom/squareup/ui/payment/SwipeHandler;->access$500(Lcom/squareup/ui/payment/SwipeHandler;)Lcom/squareup/cardreader/PaymentCounter;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/cardreader/PaymentCounter;->onPaymentStarted(Lcom/squareup/cardreader/CardReaderId;)V

    .line 262
    iget-object p1, p0, Lcom/squareup/ui/payment/SwipeHandler$DefaultSwipeHandler;->this$0:Lcom/squareup/ui/payment/SwipeHandler;

    invoke-static {p1}, Lcom/squareup/ui/payment/SwipeHandler;->access$300(Lcom/squareup/ui/payment/SwipeHandler;)Lcom/squareup/payment/Transaction;

    move-result-object p1

    invoke-virtual {p1, p2}, Lcom/squareup/payment/Transaction;->setCard(Lcom/squareup/Card;)V

    .line 263
    iget-object p1, p0, Lcom/squareup/ui/payment/SwipeHandler$DefaultSwipeHandler;->this$0:Lcom/squareup/ui/payment/SwipeHandler;

    invoke-static {p1}, Lcom/squareup/ui/payment/SwipeHandler;->access$600(Lcom/squareup/ui/payment/SwipeHandler;)Lcom/squareup/ui/main/BadKeyboardHider;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/ui/main/BadKeyboardHider;->hideSoftKeyboard()V

    .line 264
    iget-object p1, p0, Lcom/squareup/ui/payment/SwipeHandler$DefaultSwipeHandler;->this$0:Lcom/squareup/ui/payment/SwipeHandler;

    invoke-virtual {p1}, Lcom/squareup/ui/payment/SwipeHandler;->authorize()V

    goto :goto_1

    .line 267
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/payment/SwipeHandler$DefaultSwipeHandler;->this$0:Lcom/squareup/ui/payment/SwipeHandler;

    invoke-static {p1}, Lcom/squareup/ui/payment/SwipeHandler;->access$300(Lcom/squareup/ui/payment/SwipeHandler;)Lcom/squareup/payment/Transaction;

    move-result-object p1

    invoke-virtual {p1, v1}, Lcom/squareup/payment/Transaction;->setCard(Lcom/squareup/Card;)V

    .line 268
    iget-object p1, p0, Lcom/squareup/ui/payment/SwipeHandler$DefaultSwipeHandler;->this$0:Lcom/squareup/ui/payment/SwipeHandler;

    invoke-static {p1, p2}, Lcom/squareup/ui/payment/SwipeHandler;->access$700(Lcom/squareup/ui/payment/SwipeHandler;Lcom/squareup/Card;)V

    goto :goto_1

    .line 270
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/payment/SwipeHandler$DefaultSwipeHandler;->this$0:Lcom/squareup/ui/payment/SwipeHandler;

    invoke-static {p1}, Lcom/squareup/ui/payment/SwipeHandler;->access$800(Lcom/squareup/ui/payment/SwipeHandler;)Z

    move-result p1

    if-eqz p1, :cond_3

    iget-object p1, p0, Lcom/squareup/ui/payment/SwipeHandler$DefaultSwipeHandler;->this$0:Lcom/squareup/ui/payment/SwipeHandler;

    .line 271
    invoke-static {p1}, Lcom/squareup/ui/payment/SwipeHandler;->access$900(Lcom/squareup/ui/payment/SwipeHandler;)Z

    move-result p1

    if-nez p1, :cond_3

    iget-object p1, p0, Lcom/squareup/ui/payment/SwipeHandler$DefaultSwipeHandler;->this$0:Lcom/squareup/ui/payment/SwipeHandler;

    .line 272
    invoke-static {p1}, Lcom/squareup/ui/payment/SwipeHandler;->access$1000(Lcom/squareup/ui/payment/SwipeHandler;)Lcom/squareup/tickets/OpenTicketsSettings;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/tickets/OpenTicketsSettings;->isOpenTicketsEnabled()Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    .line 281
    :cond_2
    iget-object p1, p0, Lcom/squareup/ui/payment/SwipeHandler$DefaultSwipeHandler;->this$0:Lcom/squareup/ui/payment/SwipeHandler;

    invoke-static {p1}, Lcom/squareup/ui/payment/SwipeHandler;->access$300(Lcom/squareup/ui/payment/SwipeHandler;)Lcom/squareup/payment/Transaction;

    move-result-object p1

    invoke-virtual {p1, p2}, Lcom/squareup/payment/Transaction;->setCard(Lcom/squareup/Card;)V

    goto :goto_1

    .line 276
    :cond_3
    :goto_0
    iget-object p1, p0, Lcom/squareup/ui/payment/SwipeHandler$DefaultSwipeHandler;->this$0:Lcom/squareup/ui/payment/SwipeHandler;

    invoke-static {p1}, Lcom/squareup/ui/payment/SwipeHandler;->access$300(Lcom/squareup/ui/payment/SwipeHandler;)Lcom/squareup/payment/Transaction;

    move-result-object p1

    invoke-virtual {p1, v1}, Lcom/squareup/payment/Transaction;->setCard(Lcom/squareup/Card;)V

    .line 277
    iget-object p1, p0, Lcom/squareup/ui/payment/SwipeHandler$DefaultSwipeHandler;->this$0:Lcom/squareup/ui/payment/SwipeHandler;

    invoke-static {p1, p2}, Lcom/squareup/ui/payment/SwipeHandler;->access$700(Lcom/squareup/ui/payment/SwipeHandler;Lcom/squareup/Card;)V

    :goto_1
    return-void
.end method
