.class final enum Lcom/squareup/ui/print/PrintErrorType$5;
.super Lcom/squareup/ui/print/PrintErrorType;
.source "PrintErrorType.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/print/PrintErrorType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4008
    name = null
.end annotation


# direct methods
.method constructor <init>(Ljava/lang/String;I)V
    .locals 1

    const/4 v0, 0x0

    .line 49
    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/ui/print/PrintErrorType;-><init>(Ljava/lang/String;ILcom/squareup/ui/print/PrintErrorType$1;)V

    return-void
.end method


# virtual methods
.method getMessage()I
    .locals 1

    .line 55
    sget v0, Lcom/squareup/print/popup/error/impl/R$string;->kitchen_printing_error_out_of_paper_body:I

    return v0
.end method

.method getTitle()I
    .locals 1

    .line 51
    sget v0, Lcom/squareup/print/popup/error/impl/R$string;->kitchen_printing_error_out_of_paper_title:I

    return v0
.end method
