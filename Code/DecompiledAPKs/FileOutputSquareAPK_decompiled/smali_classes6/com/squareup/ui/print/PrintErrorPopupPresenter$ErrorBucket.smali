.class Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorBucket;
.super Ljava/lang/Object;
.source "PrintErrorPopupPresenter.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/print/PrintErrorPopupPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ErrorBucket"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable<",
        "Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorBucket;",
        ">;"
    }
.end annotation


# instance fields
.field final errorType:Lcom/squareup/ui/print/PrintErrorType;

.field final hardwareInfo:Lcom/squareup/print/HardwarePrinter$HardwareInfo;

.field final hardwarePrinterId:Ljava/lang/String;

.field final hasHadAtLeastOneReprintAttempt:Z


# direct methods
.method constructor <init>(Lcom/squareup/print/HardwarePrinter$HardwareInfo;Lcom/squareup/ui/print/PrintErrorType;I)V
    .locals 0

    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    iput-object p1, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorBucket;->hardwareInfo:Lcom/squareup/print/HardwarePrinter$HardwareInfo;

    .line 88
    invoke-virtual {p1}, Lcom/squareup/print/HardwarePrinter$HardwareInfo;->getId()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorBucket;->hardwarePrinterId:Ljava/lang/String;

    .line 89
    iput-object p2, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorBucket;->errorType:Lcom/squareup/ui/print/PrintErrorType;

    const/4 p1, 0x1

    if-le p3, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 90
    :goto_0
    iput-boolean p1, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorBucket;->hasHadAtLeastOneReprintAttempt:Z

    return-void
.end method


# virtual methods
.method public compareTo(Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorBucket;)I
    .locals 2

    .line 94
    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorBucket;->hardwarePrinterId:Ljava/lang/String;

    iget-object v1, p1, Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorBucket;->hardwarePrinterId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_0

    return v0

    .line 98
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorBucket;->errorType:Lcom/squareup/ui/print/PrintErrorType;

    iget-object p1, p1, Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorBucket;->errorType:Lcom/squareup/ui/print/PrintErrorType;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/print/PrintErrorType;->compareTo(Ljava/lang/Enum;)I

    move-result p1

    if-eqz p1, :cond_1

    return p1

    :cond_1
    const/4 p1, 0x1

    return p1
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    .line 78
    check-cast p1, Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorBucket;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorBucket;->compareTo(Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorBucket;)I

    move-result p1

    return p1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_3

    .line 107
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_1

    .line 108
    :cond_1
    check-cast p1, Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorBucket;

    .line 109
    iget-object v2, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorBucket;->hardwarePrinterId:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorBucket;->hardwarePrinterId:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorBucket;->errorType:Lcom/squareup/ui/print/PrintErrorType;

    iget-object v3, p1, Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorBucket;->errorType:Lcom/squareup/ui/print/PrintErrorType;

    if-ne v2, v3, :cond_2

    iget-boolean v2, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorBucket;->hasHadAtLeastOneReprintAttempt:Z

    iget-boolean p1, p1, Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorBucket;->hasHadAtLeastOneReprintAttempt:Z

    if-ne v2, p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_3
    :goto_1
    return v1
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    .line 115
    iget-object v1, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorBucket;->hardwarePrinterId:Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorBucket;->errorType:Lcom/squareup/ui/print/PrintErrorType;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-boolean v1, p0, Lcom/squareup/ui/print/PrintErrorPopupPresenter$ErrorBucket;->hasHadAtLeastOneReprintAttempt:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const/4 v2, 0x2

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/squareup/util/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
