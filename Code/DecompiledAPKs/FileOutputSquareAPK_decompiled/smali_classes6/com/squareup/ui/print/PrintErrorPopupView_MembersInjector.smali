.class public final Lcom/squareup/ui/print/PrintErrorPopupView_MembersInjector;
.super Ljava/lang/Object;
.source "PrintErrorPopupView_MembersInjector.java"

# interfaces
.implements Ldagger/MembersInjector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/MembersInjector<",
        "Lcom/squareup/ui/print/PrintErrorPopupView;",
        ">;"
    }
.end annotation


# instance fields
.field private final deviceProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final presenterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/print/PrintErrorPopupPresenter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/print/PrintErrorPopupPresenter;",
            ">;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/ui/print/PrintErrorPopupView_MembersInjector;->deviceProvider:Ljavax/inject/Provider;

    .line 21
    iput-object p2, p0, Lcom/squareup/ui/print/PrintErrorPopupView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Ldagger/MembersInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Device;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/print/PrintErrorPopupPresenter;",
            ">;)",
            "Ldagger/MembersInjector<",
            "Lcom/squareup/ui/print/PrintErrorPopupView;",
            ">;"
        }
    .end annotation

    .line 26
    new-instance v0, Lcom/squareup/ui/print/PrintErrorPopupView_MembersInjector;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/print/PrintErrorPopupView_MembersInjector;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static injectDevice(Lcom/squareup/ui/print/PrintErrorPopupView;Lcom/squareup/util/Device;)V
    .locals 0

    .line 36
    iput-object p1, p0, Lcom/squareup/ui/print/PrintErrorPopupView;->device:Lcom/squareup/util/Device;

    return-void
.end method

.method public static injectPresenter(Lcom/squareup/ui/print/PrintErrorPopupView;Lcom/squareup/ui/print/PrintErrorPopupPresenter;)V
    .locals 0

    .line 42
    iput-object p1, p0, Lcom/squareup/ui/print/PrintErrorPopupView;->presenter:Lcom/squareup/ui/print/PrintErrorPopupPresenter;

    return-void
.end method


# virtual methods
.method public injectMembers(Lcom/squareup/ui/print/PrintErrorPopupView;)V
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupView_MembersInjector;->deviceProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/util/Device;

    invoke-static {p1, v0}, Lcom/squareup/ui/print/PrintErrorPopupView_MembersInjector;->injectDevice(Lcom/squareup/ui/print/PrintErrorPopupView;Lcom/squareup/util/Device;)V

    .line 31
    iget-object v0, p0, Lcom/squareup/ui/print/PrintErrorPopupView_MembersInjector;->presenterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/print/PrintErrorPopupPresenter;

    invoke-static {p1, v0}, Lcom/squareup/ui/print/PrintErrorPopupView_MembersInjector;->injectPresenter(Lcom/squareup/ui/print/PrintErrorPopupView;Lcom/squareup/ui/print/PrintErrorPopupPresenter;)V

    return-void
.end method

.method public bridge synthetic injectMembers(Ljava/lang/Object;)V
    .locals 0

    .line 9
    check-cast p1, Lcom/squareup/ui/print/PrintErrorPopupView;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/print/PrintErrorPopupView_MembersInjector;->injectMembers(Lcom/squareup/ui/print/PrintErrorPopupView;)V

    return-void
.end method
