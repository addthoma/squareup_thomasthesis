.class public Lcom/squareup/util/DateTimeFactory;
.super Ljava/lang/Object;
.source "DateTimeFactory.java"


# instance fields
.field private final DATE_TIME_BUILDER:Lcom/squareup/protos/common/time/DateTime$Builder;

.field private previousNowMillis:J

.field private previousNowOrdinal:J


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    new-instance v0, Lcom/squareup/protos/common/time/DateTime$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/common/time/DateTime$Builder;-><init>()V

    iput-object v0, p0, Lcom/squareup/util/DateTimeFactory;->DATE_TIME_BUILDER:Lcom/squareup/protos/common/time/DateTime$Builder;

    return-void
.end method

.method private builder(J)Lcom/squareup/protos/common/time/DateTime$Builder;
    .locals 4

    .line 54
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, p1, p2}, Ljava/util/concurrent/TimeUnit;->toMicros(J)J

    move-result-wide v0

    .line 56
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v2

    .line 58
    iget-object v3, p0, Lcom/squareup/util/DateTimeFactory;->DATE_TIME_BUILDER:Lcom/squareup/protos/common/time/DateTime$Builder;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/squareup/protos/common/time/DateTime$Builder;->instant_usec(Ljava/lang/Long;)Lcom/squareup/protos/common/time/DateTime$Builder;

    move-result-object v0

    .line 59
    invoke-static {v2, p1, p2}, Lcom/squareup/util/DateTimeFactory;->timezoneOffsetMin(Ljava/util/TimeZone;J)I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/protos/common/time/DateTime$Builder;->timezone_offset_min(Ljava/lang/Integer;)Lcom/squareup/protos/common/time/DateTime$Builder;

    move-result-object p1

    .line 60
    invoke-static {v2}, Lcom/squareup/util/DateTimeFactory;->tzName(Ljava/util/TimeZone;)Ljava/lang/String;

    move-result-object p2

    invoke-static {p2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/protos/common/time/DateTime$Builder;->tz_name(Ljava/util/List;)Lcom/squareup/protos/common/time/DateTime$Builder;

    move-result-object p1

    return-object p1
.end method

.method private static timezoneOffsetMin(Ljava/util/TimeZone;J)I
    .locals 1

    .line 16
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p0, p1, p2}, Ljava/util/TimeZone;->getOffset(J)I

    move-result p0

    int-to-long p0, p0

    invoke-virtual {v0, p0, p1}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide p0

    long-to-int p1, p0

    return p1
.end method

.method private static tzName(Ljava/util/TimeZone;)Ljava/lang/String;
    .locals 1

    .line 20
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v0}, Ljava/util/TimeZone;->getDisplayName(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public build(J)Lcom/squareup/protos/common/time/DateTime;
    .locals 2

    .line 33
    invoke-direct {p0, p1, p2}, Lcom/squareup/util/DateTimeFactory;->builder(J)Lcom/squareup/protos/common/time/DateTime$Builder;

    move-result-object p1

    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/protos/common/time/DateTime$Builder;->ordinal(Ljava/lang/Long;)Lcom/squareup/protos/common/time/DateTime$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/protos/common/time/DateTime$Builder;->build()Lcom/squareup/protos/common/time/DateTime;

    move-result-object p1

    return-object p1
.end method

.method public forMillis(J)Lcom/squareup/protos/common/time/DateTime;
    .locals 5

    .line 42
    iget-wide v0, p0, Lcom/squareup/util/DateTimeFactory;->previousNowMillis:J

    const-wide/16 v2, 0x0

    cmp-long v4, p1, v0

    if-nez v4, :cond_0

    .line 43
    iget-wide v0, p0, Lcom/squareup/util/DateTimeFactory;->previousNowOrdinal:J

    const-wide/16 v2, 0x1

    add-long/2addr v2, v0

    iput-wide v2, p0, Lcom/squareup/util/DateTimeFactory;->previousNowOrdinal:J

    goto :goto_0

    .line 45
    :cond_0
    iput-wide p1, p0, Lcom/squareup/util/DateTimeFactory;->previousNowMillis:J

    .line 46
    iput-wide v2, p0, Lcom/squareup/util/DateTimeFactory;->previousNowOrdinal:J

    .line 49
    :goto_0
    invoke-direct {p0, p1, p2}, Lcom/squareup/util/DateTimeFactory;->builder(J)Lcom/squareup/protos/common/time/DateTime$Builder;

    move-result-object p1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/protos/common/time/DateTime$Builder;->ordinal(Ljava/lang/Long;)Lcom/squareup/protos/common/time/DateTime$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/protos/common/time/DateTime$Builder;->build()Lcom/squareup/protos/common/time/DateTime;

    move-result-object p1

    return-object p1
.end method

.method public now()Lcom/squareup/protos/common/time/DateTime;
    .locals 2

    .line 28
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/squareup/util/DateTimeFactory;->forMillis(J)Lcom/squareup/protos/common/time/DateTime;

    move-result-object v0

    return-object v0
.end method
