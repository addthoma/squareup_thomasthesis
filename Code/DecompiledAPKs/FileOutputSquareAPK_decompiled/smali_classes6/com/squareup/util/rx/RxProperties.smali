.class public Lcom/squareup/util/rx/RxProperties;
.super Ljava/lang/Object;
.source "RxProperties.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 2

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Should not instantiate instance of class."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public static clear(Ljava/io/File;)Lrx/Completable;
    .locals 1

    .line 85
    new-instance v0, Ljava/util/Properties;

    invoke-direct {v0}, Ljava/util/Properties;-><init>()V

    invoke-static {p0, v0}, Lcom/squareup/util/rx/RxProperties;->save(Ljava/io/File;Ljava/util/Properties;)Lrx/Completable;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$load$0(Ljava/io/File;)Ljava/util/Properties;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 39
    new-instance v0, Ljava/util/Properties;

    invoke-direct {v0}, Ljava/util/Properties;-><init>()V

    .line 40
    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Ljava/io/File;->canRead()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 41
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 42
    :try_start_0
    invoke-virtual {v0, v1}, Ljava/util/Properties;->load(Ljava/io/InputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 43
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    goto :goto_1

    :catchall_0
    move-exception p0

    .line 41
    :try_start_1
    throw p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :catchall_1
    move-exception v0

    .line 43
    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_0

    :catchall_2
    move-exception v1

    invoke-virtual {p0, v1}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    :goto_0
    throw v0

    :cond_0
    :goto_1
    return-object v0
.end method

.method static synthetic lambda$save$1(Ljava/io/File;Ljava/util/Properties;)V
    .locals 1

    .line 59
    :try_start_0
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, p0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 p0, 0x0

    .line 60
    :try_start_1
    invoke-virtual {p1, v0, p0}, Ljava/util/Properties;->store(Ljava/io/OutputStream;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 61
    :try_start_2
    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    return-void

    :catchall_0
    move-exception p0

    .line 59
    :try_start_3
    throw p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_1
    move-exception p1

    .line 61
    :try_start_4
    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    goto :goto_0

    :catchall_2
    move-exception v0

    :try_start_5
    invoke-virtual {p0, v0}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    :goto_0
    throw p1
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0

    :catch_0
    move-exception p0

    .line 62
    new-instance p1, Ljava/lang/RuntimeException;

    invoke-direct {p1, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw p1
.end method

.method static synthetic lambda$update$2(Lrx/functions/Action1;Ljava/io/File;Ljava/util/Properties;)Lrx/Single;
    .locals 0

    .line 78
    invoke-interface {p0, p2}, Lrx/functions/Action1;->call(Ljava/lang/Object;)V

    .line 79
    invoke-static {p1, p2}, Lcom/squareup/util/rx/RxProperties;->save(Ljava/io/File;Ljava/util/Properties;)Lrx/Completable;

    move-result-object p0

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    .line 80
    invoke-static {p1}, Lrx/Single;->just(Ljava/lang/Object;)Lrx/Single;

    move-result-object p1

    invoke-virtual {p0, p1}, Lrx/Completable;->andThen(Lrx/Single;)Lrx/Single;

    move-result-object p0

    return-object p0
.end method

.method public static load(Ljava/io/File;)Lrx/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Lrx/Single<",
            "Ljava/util/Properties;",
            ">;"
        }
    .end annotation

    .line 38
    new-instance v0, Lcom/squareup/util/rx/-$$Lambda$RxProperties$FS1g0J4snrs4bg7_IcnJQ79sjJ4;

    invoke-direct {v0, p0}, Lcom/squareup/util/rx/-$$Lambda$RxProperties$FS1g0J4snrs4bg7_IcnJQ79sjJ4;-><init>(Ljava/io/File;)V

    invoke-static {v0}, Lrx/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lrx/Single;

    move-result-object p0

    return-object p0
.end method

.method public static save(Ljava/io/File;Ljava/util/Properties;)Lrx/Completable;
    .locals 1

    .line 58
    new-instance v0, Lcom/squareup/util/rx/-$$Lambda$RxProperties$lO6oV-bNemOjLrD0v-OmndypGN0;

    invoke-direct {v0, p0, p1}, Lcom/squareup/util/rx/-$$Lambda$RxProperties$lO6oV-bNemOjLrD0v-OmndypGN0;-><init>(Ljava/io/File;Ljava/util/Properties;)V

    invoke-static {v0}, Lrx/Completable;->fromAction(Lrx/functions/Action0;)Lrx/Completable;

    move-result-object p0

    return-object p0
.end method

.method public static update(Ljava/io/File;Lrx/functions/Action1;)Lrx/Completable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Lrx/functions/Action1<",
            "Ljava/util/Properties;",
            ">;)",
            "Lrx/Completable;"
        }
    .end annotation

    .line 76
    invoke-static {p0}, Lcom/squareup/util/rx/RxProperties;->load(Ljava/io/File;)Lrx/Single;

    move-result-object v0

    new-instance v1, Lcom/squareup/util/rx/-$$Lambda$RxProperties$3Op6LT4Ip2gxjA-iIjwbt-y-dec;

    invoke-direct {v1, p1, p0}, Lcom/squareup/util/rx/-$$Lambda$RxProperties$3Op6LT4Ip2gxjA-iIjwbt-y-dec;-><init>(Lrx/functions/Action1;Ljava/io/File;)V

    .line 77
    invoke-virtual {v0, v1}, Lrx/Single;->flatMap(Lrx/functions/Func1;)Lrx/Single;

    move-result-object p0

    .line 81
    invoke-virtual {p0}, Lrx/Single;->toCompletable()Lrx/Completable;

    move-result-object p0

    return-object p0
.end method
