.class public final synthetic Lcom/squareup/util/rx/-$$Lambda$RxRetryStrategies$RPubeEN5IRUmzCj34gq9gr2587E;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lrx/functions/Func1;


# instance fields
.field private final synthetic f$0:I

.field private final synthetic f$1:Lrx/functions/Func1;

.field private final synthetic f$2:Ljava/util/concurrent/TimeUnit;

.field private final synthetic f$3:Lrx/Scheduler;


# direct methods
.method public synthetic constructor <init>(ILrx/functions/Func1;Ljava/util/concurrent/TimeUnit;Lrx/Scheduler;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/squareup/util/rx/-$$Lambda$RxRetryStrategies$RPubeEN5IRUmzCj34gq9gr2587E;->f$0:I

    iput-object p2, p0, Lcom/squareup/util/rx/-$$Lambda$RxRetryStrategies$RPubeEN5IRUmzCj34gq9gr2587E;->f$1:Lrx/functions/Func1;

    iput-object p3, p0, Lcom/squareup/util/rx/-$$Lambda$RxRetryStrategies$RPubeEN5IRUmzCj34gq9gr2587E;->f$2:Ljava/util/concurrent/TimeUnit;

    iput-object p4, p0, Lcom/squareup/util/rx/-$$Lambda$RxRetryStrategies$RPubeEN5IRUmzCj34gq9gr2587E;->f$3:Lrx/Scheduler;

    return-void
.end method


# virtual methods
.method public final call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    iget v0, p0, Lcom/squareup/util/rx/-$$Lambda$RxRetryStrategies$RPubeEN5IRUmzCj34gq9gr2587E;->f$0:I

    iget-object v1, p0, Lcom/squareup/util/rx/-$$Lambda$RxRetryStrategies$RPubeEN5IRUmzCj34gq9gr2587E;->f$1:Lrx/functions/Func1;

    iget-object v2, p0, Lcom/squareup/util/rx/-$$Lambda$RxRetryStrategies$RPubeEN5IRUmzCj34gq9gr2587E;->f$2:Ljava/util/concurrent/TimeUnit;

    iget-object v3, p0, Lcom/squareup/util/rx/-$$Lambda$RxRetryStrategies$RPubeEN5IRUmzCj34gq9gr2587E;->f$3:Lrx/Scheduler;

    check-cast p1, Lrx/Observable;

    invoke-static {v0, v1, v2, v3, p1}, Lcom/squareup/util/rx/RxRetryStrategies;->lambda$customBackoffThenError$4(ILrx/functions/Func1;Ljava/util/concurrent/TimeUnit;Lrx/Scheduler;Lrx/Observable;)Lrx/Observable;

    move-result-object p1

    return-object p1
.end method
