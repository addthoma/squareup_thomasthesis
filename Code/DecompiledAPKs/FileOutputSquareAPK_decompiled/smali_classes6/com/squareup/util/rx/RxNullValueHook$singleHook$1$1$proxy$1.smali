.class public final Lcom/squareup/util/rx/RxNullValueHook$singleHook$1$1$proxy$1;
.super Lrx/SingleSubscriber;
.source "RxNullValueHook.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/util/rx/RxNullValueHook$singleHook$1$1;->call(Lrx/SingleSubscriber;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lrx/SingleSubscriber<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRxNullValueHook.kt\nKotlin\n*S Kotlin\n*F\n+ 1 RxNullValueHook.kt\ncom/squareup/util/rx/RxNullValueHook$singleHook$1$1$proxy$1\n*L\n1#1,168:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001d\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0003\n\u0002\u0008\u0003*\u0001\u0000\u0008\n\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001J\u0012\u0010\u0003\u001a\u00020\u00042\u0008\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0016J\u0012\u0010\u0007\u001a\u00020\u00042\u0008\u0010\u0008\u001a\u0004\u0018\u00010\u0002H\u0016\u00a8\u0006\t"
    }
    d2 = {
        "com/squareup/util/rx/RxNullValueHook$singleHook$1$1$proxy$1",
        "Lrx/SingleSubscriber;",
        "",
        "onError",
        "",
        "error",
        "",
        "onSuccess",
        "event",
        "pure"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $subscriber:Lrx/SingleSubscriber;


# direct methods
.method constructor <init>(Lrx/SingleSubscriber;)V
    .locals 0

    .line 81
    iput-object p1, p0, Lcom/squareup/util/rx/RxNullValueHook$singleHook$1$1$proxy$1;->$subscriber:Lrx/SingleSubscriber;

    invoke-direct {p0}, Lrx/SingleSubscriber;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Ljava/lang/Throwable;)V
    .locals 1

    .line 100
    iget-object v0, p0, Lcom/squareup/util/rx/RxNullValueHook$singleHook$1$1$proxy$1;->$subscriber:Lrx/SingleSubscriber;

    invoke-virtual {v0, p1}, Lrx/SingleSubscriber;->onError(Ljava/lang/Throwable;)V

    return-void
.end method

.method public onSuccess(Ljava/lang/Object;)V
    .locals 3

    if-nez p1, :cond_2

    .line 84
    new-instance v0, Lcom/squareup/util/rx/RxNullValueHook$NullNotAllowedException;

    invoke-direct {v0}, Lcom/squareup/util/rx/RxNullValueHook$NullNotAllowedException;-><init>()V

    .line 85
    sget-object v1, Lcom/squareup/util/rx/RxNullValueHook;->INSTANCE:Lcom/squareup/util/rx/RxNullValueHook;

    move-object v2, v0

    check-cast v2, Ljava/lang/Exception;

    invoke-static {v1, v2}, Lcom/squareup/util/rx/RxNullValueHook;->access$ignoreException(Lcom/squareup/util/rx/RxNullValueHook;Ljava/lang/Exception;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 86
    sget-object v1, Lcom/squareup/util/rx/RxNullValueHook;->INSTANCE:Lcom/squareup/util/rx/RxNullValueHook;

    iget-object v2, p0, Lcom/squareup/util/rx/RxNullValueHook$singleHook$1$1$proxy$1;->$subscriber:Lrx/SingleSubscriber;

    check-cast v2, Lrx/Subscription;

    invoke-static {v1, v2}, Lcom/squareup/util/rx/RxNullValueHook;->access$getAssemblyStacktrace(Lcom/squareup/util/rx/RxNullValueHook;Lrx/Subscription;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 87
    new-instance v2, Lrx/exceptions/AssemblyStackTraceException;

    invoke-direct {v2, v1}, Lrx/exceptions/AssemblyStackTraceException;-><init>(Ljava/lang/String;)V

    move-object v1, v0

    check-cast v1, Ljava/lang/Throwable;

    invoke-virtual {v2, v1}, Lrx/exceptions/AssemblyStackTraceException;->attachTo(Ljava/lang/Throwable;)V

    .line 89
    :cond_0
    sget-object v1, Lcom/squareup/util/rx/RxNullValueHook;->INSTANCE:Lcom/squareup/util/rx/RxNullValueHook;

    invoke-static {v1}, Lcom/squareup/util/rx/RxNullValueHook;->access$getCrash$p(Lcom/squareup/util/rx/RxNullValueHook;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 90
    check-cast v0, Ljava/lang/Throwable;

    move-object v1, p0

    check-cast v1, Lrx/SingleSubscriber;

    invoke-static {v0, v1}, Lrx/exceptions/Exceptions;->throwOrReport(Ljava/lang/Throwable;Lrx/SingleSubscriber;)V

    goto :goto_0

    .line 92
    :cond_1
    invoke-virtual {v0}, Lcom/squareup/util/rx/RxNullValueHook$NullNotAllowedException;->printStackTrace()V

    .line 96
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/squareup/util/rx/RxNullValueHook$singleHook$1$1$proxy$1;->$subscriber:Lrx/SingleSubscriber;

    invoke-virtual {v0, p1}, Lrx/SingleSubscriber;->onSuccess(Ljava/lang/Object;)V

    return-void
.end method
