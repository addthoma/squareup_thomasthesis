.class public final Lcom/squareup/util/RxJavaInteropExtensionsKt;
.super Ljava/lang/Object;
.source "RxJavaInteropExtensions.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0098\u0001\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0002\u001a\u0016\u0010\u0000\u001a\u00020\u0001\"\u0004\u0008\u0000\u0010\u0003*\u0008\u0012\u0004\u0012\u0002H\u00030\u0004\u001a$\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u0002H\u00030\u0006\"\u0004\u0008\u0000\u0010\u0003*\u0008\u0012\u0004\u0012\u0002H\u00030\u00072\u0006\u0010\u0008\u001a\u00020\t\u001a\u001c\u0010\u0005\u001a\u0008\u0012\u0004\u0012\u0002H\u00030\u0006\"\u0004\u0008\u0000\u0010\u0003*\u0008\u0012\u0004\u0012\u0002H\u00030\n\u001a.\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u0002H\r\u0012\u0004\u0012\u0002H\u00030\u000c\"\u0004\u0008\u0000\u0010\u0003\"\u0004\u0008\u0001\u0010\r*\u000e\u0012\u0004\u0012\u0002H\r\u0012\u0004\u0012\u0002H\u00030\u000e\u001a\u001c\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u0002H\u00030\u0010\"\u0004\u0008\u0000\u0010\u0003*\u0008\u0012\u0004\u0012\u0002H\u00030\u0004\u001a\u001c\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u0002H\u00030\u0010\"\u0004\u0008\u0000\u0010\u0003*\u0008\u0012\u0004\u0012\u0002H\u00030\u0011\u001a\"\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00030\u0013\"\u0004\u0008\u0000\u0010\u0003*\u0008\u0012\u0004\u0012\u0002H\u00030\u0014\u001a\"\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00030\u0013\"\u0004\u0008\u0000\u0010\u0003*\u0008\u0012\u0004\u0012\u0002H\u00030\u0015\u001a\n\u0010\u0016\u001a\u00020\u0017*\u00020\u0018\u001a\n\u0010\u0019\u001a\u00020\u001a*\u00020\u001b\u001a.\u0010\u0019\u001a\u000e\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\r0\u001c\"\u0004\u0008\u0000\u0010\u0003\"\u0004\u0008\u0001\u0010\r*\u000e\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\r0\u001d\u001a.\u0010\u0019\u001a\u000e\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\r0\u001e\"\u0004\u0008\u0000\u0010\u0003\"\u0004\u0008\u0001\u0010\r*\u000e\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\r0\u001f\u001a\n\u0010 \u001a\u00020!*\u00020\u0001\u001a\n\u0010\"\u001a\u00020\u0018*\u00020\u0017\u001a\u001c\u0010#\u001a\u0008\u0012\u0004\u0012\u0002H\u00030$\"\u0004\u0008\u0000\u0010\u0003*\u0008\u0012\u0004\u0012\u0002H\u00030\u0006\u001a\u0016\u0010%\u001a\u0008\u0012\u0004\u0012\u0002H\u00030&\"\u0004\u0008\u0000\u0010\u0003*\u00020\u0001\u001a\u001c\u0010%\u001a\u0008\u0012\u0004\u0012\u0002H\u00030&\"\u0004\u0008\u0000\u0010\u0003*\u0008\u0012\u0004\u0012\u0002H\u00030\u0010\u001a\u001c\u0010\'\u001a\u0008\u0012\u0004\u0012\u0002H\u00030(\"\u0004\u0008\u0000\u0010\u0003*\u0008\u0012\u0004\u0012\u0002H\u00030\u0006\u001a.\u0010)\u001a\u000e\u0012\u0004\u0012\u0002H\r\u0012\u0004\u0012\u0002H\u00030\u000e\"\u0004\u0008\u0000\u0010\u0003\"\u0004\u0008\u0001\u0010\r*\u000e\u0012\u0004\u0012\u0002H\r\u0012\u0004\u0012\u0002H\u00030\u000c\u001a\"\u0010*\u001a\u0008\u0012\u0004\u0012\u0002H\u00030\u0014\"\u0004\u0008\u0000\u0010\u0003*\u000e\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00030\u0013\u001a\u001c\u0010+\u001a\u0008\u0012\u0004\u0012\u0002H\u00030,\"\u0004\u0008\u0000\u0010\u0003*\u0008\u0012\u0004\u0012\u0002H\u00030\u0010\u001a\"\u0010-\u001a\u0008\u0012\u0004\u0012\u0002H\u00030\u0015\"\u0004\u0008\u0000\u0010\u0003*\u000e\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\u00030\u0013\u001a\n\u0010.\u001a\u00020\u001b*\u00020\u001a\u001a.\u0010.\u001a\u000e\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\r0\u001d\"\u0004\u0008\u0000\u0010\u0003\"\u0004\u0008\u0001\u0010\r*\u000e\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\r0\u001c\u001a6\u0010.\u001a\u000e\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\r0/\"\u0004\u0008\u0000\u0010\u0003\"\u0004\u0008\u0001\u0010\r*\u000e\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\r0\u001c2\u0006\u0010\u0008\u001a\u00020\t\u001a.\u0010.\u001a\u000e\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\r0\u001f\"\u0004\u0008\u0000\u0010\u0003\"\u0004\u0008\u0001\u0010\r*\u000e\u0012\u0004\u0012\u0002H\u0003\u0012\u0004\u0012\u0002H\r0\u001e\u00a8\u00060"
    }
    d2 = {
        "toV1Completable",
        "Lrx/Completable;",
        "Lio/reactivex/CompletableSource;",
        "T",
        "Lio/reactivex/MaybeSource;",
        "toV1Observable",
        "Lrx/Observable;",
        "Lio/reactivex/ObservableSource;",
        "strategy",
        "Lio/reactivex/BackpressureStrategy;",
        "Lorg/reactivestreams/Publisher;",
        "toV1Operator",
        "Lrx/Observable$Operator;",
        "R",
        "Lio/reactivex/FlowableOperator;",
        "toV1Single",
        "Lrx/Single;",
        "Lio/reactivex/SingleSource;",
        "toV1Subject",
        "Lrx/subjects/Subject;",
        "Lio/reactivex/processors/FlowableProcessor;",
        "Lio/reactivex/subjects/Subject;",
        "toV1Subscription",
        "Lrx/Subscription;",
        "Lio/reactivex/disposables/Disposable;",
        "toV1Transformer",
        "Lrx/Completable$Transformer;",
        "Lio/reactivex/CompletableTransformer;",
        "Lrx/Observable$Transformer;",
        "Lio/reactivex/FlowableTransformer;",
        "Lrx/Single$Transformer;",
        "Lio/reactivex/SingleTransformer;",
        "toV2Completable",
        "Lio/reactivex/Completable;",
        "toV2Disposable",
        "toV2Flowable",
        "Lio/reactivex/Flowable;",
        "toV2Maybe",
        "Lio/reactivex/Maybe;",
        "toV2Observable",
        "Lio/reactivex/Observable;",
        "toV2Operator",
        "toV2Processor",
        "toV2Single",
        "Lio/reactivex/Single;",
        "toV2Subject",
        "toV2Transformer",
        "Lio/reactivex/ObservableTransformer;",
        "pure"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final toV1Completable(Lio/reactivex/CompletableSource;)Lrx/Completable;
    .locals 1

    const-string v0, "$this$toV1Completable"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 91
    invoke-static {p0}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Completable(Lio/reactivex/CompletableSource;)Lrx/Completable;

    move-result-object p0

    const-string v0, "RxJavaInterop.toV1Completable(this)"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final toV1Completable(Lio/reactivex/MaybeSource;)Lrx/Completable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/MaybeSource<",
            "TT;>;)",
            "Lrx/Completable;"
        }
    .end annotation

    const-string v0, "$this$toV1Completable"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 99
    invoke-static {p0}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Completable(Lio/reactivex/MaybeSource;)Lrx/Completable;

    move-result-object p0

    const-string v0, "RxJavaInterop.toV1Completable(this)"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/ObservableSource<",
            "TT;>;",
            "Lio/reactivex/BackpressureStrategy;",
            ")",
            "Lrx/Observable<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "$this$toV1Observable"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "strategy"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    invoke-static {p0, p1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object p0

    const-string p1, "RxJavaInterop.toV1Observable(this, strategy)"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final toV1Observable(Lorg/reactivestreams/Publisher;)Lrx/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/reactivestreams/Publisher<",
            "TT;>;)",
            "Lrx/Observable<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "$this$toV1Observable"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    invoke-static {p0}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Observable(Lorg/reactivestreams/Publisher;)Lrx/Observable;

    move-result-object p0

    const-string v0, "RxJavaInterop.toV1Observable(this)"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final toV1Operator(Lio/reactivex/FlowableOperator;)Lrx/Observable$Operator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/FlowableOperator<",
            "TR;TT;>;)",
            "Lrx/Observable$Operator<",
            "TR;TT;>;"
        }
    .end annotation

    const-string v0, "$this$toV1Operator"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 123
    invoke-static {p0}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Operator(Lio/reactivex/FlowableOperator;)Lrx/Observable$Operator;

    move-result-object p0

    const-string v0, "RxJavaInterop.toV1Operator(this)"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final toV1Single(Lio/reactivex/MaybeSource;)Lrx/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/MaybeSource<",
            "TT;>;)",
            "Lrx/Single<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "$this$toV1Single"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 95
    invoke-static {p0}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Single(Lio/reactivex/MaybeSource;)Lrx/Single;

    move-result-object p0

    const-string v0, "RxJavaInterop.toV1Single(this)"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final toV1Single(Lio/reactivex/SingleSource;)Lrx/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/SingleSource<",
            "TT;>;)",
            "Lrx/Single<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "$this$toV1Single"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 87
    invoke-static {p0}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Single(Lio/reactivex/SingleSource;)Lrx/Single;

    move-result-object p0

    const-string v0, "RxJavaInterop.toV1Single(this)"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final toV1Subject(Lio/reactivex/processors/FlowableProcessor;)Lrx/subjects/Subject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/processors/FlowableProcessor<",
            "TT;>;)",
            "Lrx/subjects/Subject<",
            "TT;TT;>;"
        }
    .end annotation

    const-string v0, "$this$toV1Subject"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 107
    invoke-static {p0}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Subject(Lio/reactivex/processors/FlowableProcessor;)Lrx/subjects/Subject;

    move-result-object p0

    const-string v0, "RxJavaInterop.toV1Subject(this)"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final toV1Subject(Lio/reactivex/subjects/Subject;)Lrx/subjects/Subject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/subjects/Subject<",
            "TT;>;)",
            "Lrx/subjects/Subject<",
            "TT;TT;>;"
        }
    .end annotation

    const-string v0, "$this$toV1Subject"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 103
    invoke-static {p0}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Subject(Lio/reactivex/subjects/Subject;)Lrx/subjects/Subject;

    move-result-object p0

    const-string v0, "RxJavaInterop.toV1Subject(this)"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final toV1Subscription(Lio/reactivex/disposables/Disposable;)Lrx/Subscription;
    .locals 1

    const-string v0, "$this$toV1Subscription"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    invoke-static {p0}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Subscription(Lio/reactivex/disposables/Disposable;)Lrx/Subscription;

    move-result-object p0

    const-string v0, "RxJavaInterop.toV1Subscription(this)"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final toV1Transformer(Lio/reactivex/CompletableTransformer;)Lrx/Completable$Transformer;
    .locals 1

    const-string v0, "$this$toV1Transformer"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 119
    invoke-static {p0}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Transformer(Lio/reactivex/CompletableTransformer;)Lrx/Completable$Transformer;

    move-result-object p0

    const-string v0, "RxJavaInterop.toV1Transformer(this)"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final toV1Transformer(Lio/reactivex/FlowableTransformer;)Lrx/Observable$Transformer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/FlowableTransformer<",
            "TT;TR;>;)",
            "Lrx/Observable$Transformer<",
            "TT;TR;>;"
        }
    .end annotation

    const-string v0, "$this$toV1Transformer"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 111
    invoke-static {p0}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Transformer(Lio/reactivex/FlowableTransformer;)Lrx/Observable$Transformer;

    move-result-object p0

    const-string v0, "RxJavaInterop.toV1Transformer(this)"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final toV1Transformer(Lio/reactivex/SingleTransformer;)Lrx/Single$Transformer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lio/reactivex/SingleTransformer<",
            "TT;TR;>;)",
            "Lrx/Single$Transformer<",
            "TT;TR;>;"
        }
    .end annotation

    const-string v0, "$this$toV1Transformer"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 115
    invoke-static {p0}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Transformer(Lio/reactivex/SingleTransformer;)Lrx/Single$Transformer;

    move-result-object p0

    const-string v0, "RxJavaInterop.toV1Transformer(this)"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final toV2Completable(Lrx/Completable;)Lio/reactivex/Completable;
    .locals 1

    const-string v0, "$this$toV2Completable"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    invoke-static {p0}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV2Completable(Lrx/Completable;)Lio/reactivex/Completable;

    move-result-object p0

    const-string v0, "RxJavaInterop.toV2Completable(this)"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final toV2Disposable(Lrx/Subscription;)Lio/reactivex/disposables/Disposable;
    .locals 1

    const-string v0, "$this$toV2Disposable"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 13
    invoke-static {p0}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV2Disposable(Lrx/Subscription;)Lio/reactivex/disposables/Disposable;

    move-result-object p0

    const-string v0, "RxJavaInterop.toV2Disposable(this)"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final toV2Flowable(Lrx/Observable;)Lio/reactivex/Flowable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/Observable<",
            "TT;>;)",
            "Lio/reactivex/Flowable<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "$this$toV2Flowable"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    invoke-static {p0}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV2Flowable(Lrx/Observable;)Lio/reactivex/Flowable;

    move-result-object p0

    const-string v0, "RxJavaInterop.toV2Flowable(this)"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final toV2Maybe(Lrx/Completable;)Lio/reactivex/Maybe;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/Completable;",
            ")",
            "Lio/reactivex/Maybe<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "$this$toV2Maybe"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-static {p0}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV2Maybe(Lrx/Completable;)Lio/reactivex/Maybe;

    move-result-object p0

    const-string v0, "RxJavaInterop.toV2Maybe(this)"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final toV2Maybe(Lrx/Single;)Lio/reactivex/Maybe;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/Single<",
            "TT;>;)",
            "Lio/reactivex/Maybe<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "$this$toV2Maybe"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    invoke-static {p0}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV2Maybe(Lrx/Single;)Lio/reactivex/Maybe;

    move-result-object p0

    const-string v0, "RxJavaInterop.toV2Maybe(this)"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/Observable<",
            "TT;>;)",
            "Lio/reactivex/Observable<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "$this$toV2Observable"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-static {p0}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV2Observable(Lrx/Observable;)Lio/reactivex/Observable;

    move-result-object p0

    const-string v0, "RxJavaInterop.toV2Observable(this)"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final toV2Operator(Lrx/Observable$Operator;)Lio/reactivex/FlowableOperator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/Observable$Operator<",
            "TR;TT;>;)",
            "Lio/reactivex/FlowableOperator<",
            "TR;TT;>;"
        }
    .end annotation

    const-string v0, "$this$toV2Operator"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 67
    invoke-static {p0}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV2Operator(Lrx/Observable$Operator;)Lio/reactivex/FlowableOperator;

    move-result-object p0

    const-string v0, "RxJavaInterop.toV2Operator(this)"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final toV2Processor(Lrx/subjects/Subject;)Lio/reactivex/processors/FlowableProcessor;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/subjects/Subject<",
            "TT;TT;>;)",
            "Lio/reactivex/processors/FlowableProcessor<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "$this$toV2Processor"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    invoke-static {p0}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV2Processor(Lrx/subjects/Subject;)Lio/reactivex/processors/FlowableProcessor;

    move-result-object p0

    const-string v0, "RxJavaInterop.toV2Processor(this)"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final toV2Single(Lrx/Single;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/Single<",
            "TT;>;)",
            "Lio/reactivex/Single<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "$this$toV2Single"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    invoke-static {p0}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV2Single(Lrx/Single;)Lio/reactivex/Single;

    move-result-object p0

    const-string v0, "RxJavaInterop.toV2Single(this)"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final toV2Subject(Lrx/subjects/Subject;)Lio/reactivex/subjects/Subject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/subjects/Subject<",
            "TT;TT;>;)",
            "Lio/reactivex/subjects/Subject<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "$this$toV2Subject"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    invoke-static {p0}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV2Subject(Lrx/subjects/Subject;)Lio/reactivex/subjects/Subject;

    move-result-object p0

    const-string v0, "RxJavaInterop.toV2Subject(this)"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final toV2Transformer(Lrx/Completable$Transformer;)Lio/reactivex/CompletableTransformer;
    .locals 1

    const-string v0, "$this$toV2Transformer"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    invoke-static {p0}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV2Transformer(Lrx/Completable$Transformer;)Lio/reactivex/CompletableTransformer;

    move-result-object p0

    const-string v0, "RxJavaInterop.toV2Transformer(this)"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final toV2Transformer(Lrx/Observable$Transformer;)Lio/reactivex/FlowableTransformer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/Observable$Transformer<",
            "TT;TR;>;)",
            "Lio/reactivex/FlowableTransformer<",
            "TT;TR;>;"
        }
    .end annotation

    const-string v0, "$this$toV2Transformer"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    invoke-static {p0}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV2Transformer(Lrx/Observable$Transformer;)Lio/reactivex/FlowableTransformer;

    move-result-object p0

    const-string v0, "RxJavaInterop.toV2Transformer(this)"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final toV2Transformer(Lrx/Observable$Transformer;Lio/reactivex/BackpressureStrategy;)Lio/reactivex/ObservableTransformer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/Observable$Transformer<",
            "TT;TR;>;",
            "Lio/reactivex/BackpressureStrategy;",
            ")",
            "Lio/reactivex/ObservableTransformer<",
            "TT;TR;>;"
        }
    .end annotation

    const-string v0, "$this$toV2Transformer"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "strategy"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    invoke-static {p0, p1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV2Transformer(Lrx/Observable$Transformer;Lio/reactivex/BackpressureStrategy;)Lio/reactivex/ObservableTransformer;

    move-result-object p0

    const-string p1, "RxJavaInterop.toV2Transformer(this, strategy)"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method public static final toV2Transformer(Lrx/Single$Transformer;)Lio/reactivex/SingleTransformer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">(",
            "Lrx/Single$Transformer<",
            "TT;TR;>;)",
            "Lio/reactivex/SingleTransformer<",
            "TT;TR;>;"
        }
    .end annotation

    const-string v0, "$this$toV2Transformer"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    invoke-static {p0}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV2Transformer(Lrx/Single$Transformer;)Lio/reactivex/SingleTransformer;

    move-result-object p0

    const-string v0, "RxJavaInterop.toV2Transformer(this)"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method
