.class public final Lcom/squareup/util/rx2/Rx2Files$watch$1$fileObserver$1;
.super Landroid/os/FileObserver;
.source "Rx2Files.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/util/rx2/Rx2Files$watch$1;->subscribe(Lio/reactivex/ObservableEmitter;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001d\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u001a\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0007H\u0016\u00a8\u0006\u0008"
    }
    d2 = {
        "com/squareup/util/rx2/Rx2Files$watch$1$fileObserver$1",
        "Landroid/os/FileObserver;",
        "onEvent",
        "",
        "event",
        "",
        "path",
        "",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $emitter:Lio/reactivex/ObservableEmitter;

.field final synthetic this$0:Lcom/squareup/util/rx2/Rx2Files$watch$1;


# direct methods
.method constructor <init>(Lcom/squareup/util/rx2/Rx2Files$watch$1;Lio/reactivex/ObservableEmitter;Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/ObservableEmitter;",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .line 25
    iput-object p1, p0, Lcom/squareup/util/rx2/Rx2Files$watch$1$fileObserver$1;->this$0:Lcom/squareup/util/rx2/Rx2Files$watch$1;

    iput-object p2, p0, Lcom/squareup/util/rx2/Rx2Files$watch$1$fileObserver$1;->$emitter:Lio/reactivex/ObservableEmitter;

    invoke-direct {p0, p3, p4}, Landroid/os/FileObserver;-><init>(Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public onEvent(ILjava/lang/String;)V
    .locals 0

    .line 29
    iget-object p1, p0, Lcom/squareup/util/rx2/Rx2Files$watch$1$fileObserver$1;->$emitter:Lio/reactivex/ObservableEmitter;

    iget-object p2, p0, Lcom/squareup/util/rx2/Rx2Files$watch$1$fileObserver$1;->this$0:Lcom/squareup/util/rx2/Rx2Files$watch$1;

    iget-object p2, p2, Lcom/squareup/util/rx2/Rx2Files$watch$1;->$file:Ljava/io/File;

    invoke-interface {p1, p2}, Lio/reactivex/ObservableEmitter;->onNext(Ljava/lang/Object;)V

    return-void
.end method
