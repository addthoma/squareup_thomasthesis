.class public final Lcom/squareup/util/rx2/Rx2TransformersKt$debounceWithHysteresis$1;
.super Ljava/lang/Object;
.source "Rx2Transformers.kt"

# interfaces
.implements Lio/reactivex/ObservableTransformer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/util/rx2/Rx2TransformersKt;->debounceWithHysteresis(ILkotlin/jvm/functions/Function1;)Lio/reactivex/ObservableTransformer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Upstream:",
        "Ljava/lang/Object;",
        "Downstream:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/ObservableTransformer<",
        "TT;TT;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nRx2Transformers.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Rx2Transformers.kt\ncom/squareup/util/rx2/Rx2TransformersKt$debounceWithHysteresis$1\n+ 2 Rx2Transformers.kt\ncom/squareup/util/rx2/Rx2TransformersKt\n*L\n1#1,452:1\n380#2,9:453\n381#2:462\n*E\n*S KotlinDebug\n*F\n+ 1 Rx2Transformers.kt\ncom/squareup/util/rx2/Rx2TransformersKt$debounceWithHysteresis$1\n*L\n362#1,9:453\n362#1:462\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\u0010\u0000\u001a\u0010\u0012\u000c\u0012\n \u0003*\u0004\u0018\u0001H\u0002H\u00020\u0001\"\u0008\u0008\u0000\u0010\u0002*\u00020\u00042\u0014\u0010\u0005\u001a\u0010\u0012\u000c\u0012\n \u0003*\u0004\u0018\u0001H\u0002H\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Observable;",
        "T",
        "kotlin.jvm.PlatformType",
        "",
        "it",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $deltaThreshold:I

.field final synthetic $getValue:Lkotlin/jvm/functions/Function1;


# direct methods
.method public constructor <init>(ILkotlin/jvm/functions/Function1;)V
    .locals 0

    iput p1, p0, Lcom/squareup/util/rx2/Rx2TransformersKt$debounceWithHysteresis$1;->$deltaThreshold:I

    iput-object p2, p0, Lcom/squareup/util/rx2/Rx2TransformersKt$debounceWithHysteresis$1;->$getValue:Lkotlin/jvm/functions/Function1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lio/reactivex/Observable;)Lio/reactivex/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/Observable<",
            "TT;>;)",
            "Lio/reactivex/Observable<",
            "TT;>;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 362
    iget v0, p0, Lcom/squareup/util/rx2/Rx2TransformersKt$debounceWithHysteresis$1;->$deltaThreshold:I

    .line 462
    new-instance v1, Lcom/squareup/util/rx2/Rx2TransformersKt$debounceWithHysteresis$2;

    iget-object v2, p0, Lcom/squareup/util/rx2/Rx2TransformersKt$debounceWithHysteresis$1;->$getValue:Lkotlin/jvm/functions/Function1;

    invoke-direct {v1, v0, v2}, Lcom/squareup/util/rx2/Rx2TransformersKt$debounceWithHysteresis$2;-><init>(ILkotlin/jvm/functions/Function1;)V

    check-cast v1, Lio/reactivex/functions/BiFunction;

    invoke-virtual {p1, v1}, Lio/reactivex/Observable;->scan(Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object p1

    .line 461
    invoke-virtual {p1}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object p1

    const-string v0, "this\n      .scan { lastE\u2026  .distinctUntilChanged()"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public bridge synthetic apply(Lio/reactivex/Observable;)Lio/reactivex/ObservableSource;
    .locals 0

    invoke-virtual {p0, p1}, Lcom/squareup/util/rx2/Rx2TransformersKt$debounceWithHysteresis$1;->apply(Lio/reactivex/Observable;)Lio/reactivex/Observable;

    move-result-object p1

    check-cast p1, Lio/reactivex/ObservableSource;

    return-object p1
.end method
