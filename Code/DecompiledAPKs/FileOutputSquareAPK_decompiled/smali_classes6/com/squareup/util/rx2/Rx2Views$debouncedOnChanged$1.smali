.class final Lcom/squareup/util/rx2/Rx2Views$debouncedOnChanged$1;
.super Ljava/lang/Object;
.source "Rx2Views.kt"

# interfaces
.implements Lio/reactivex/ObservableOnSubscribe;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/util/rx2/Rx2Views;->debouncedOnChanged(Landroid/widget/TextView;)Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/ObservableOnSubscribe<",
        "TT;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u0014\u0010\u0002\u001a\u0010\u0012\u000c\u0012\n \u0004*\u0004\u0018\u00010\u00010\u00010\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "emitter",
        "Lio/reactivex/ObservableEmitter;",
        "kotlin.jvm.PlatformType",
        "subscribe"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $this_debouncedOnChanged:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Landroid/widget/TextView;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/util/rx2/Rx2Views$debouncedOnChanged$1;->$this_debouncedOnChanged:Landroid/widget/TextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final subscribe(Lio/reactivex/ObservableEmitter;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/ObservableEmitter<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    const-string v0, "emitter"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 237
    new-instance v0, Lcom/squareup/util/rx2/Rx2Views$debouncedOnChanged$1$listener$1;

    invoke-direct {v0, p1}, Lcom/squareup/util/rx2/Rx2Views$debouncedOnChanged$1$listener$1;-><init>(Lio/reactivex/ObservableEmitter;)V

    .line 241
    iget-object v1, p0, Lcom/squareup/util/rx2/Rx2Views$debouncedOnChanged$1;->$this_debouncedOnChanged:Landroid/widget/TextView;

    move-object v2, v0

    check-cast v2, Landroid/text/TextWatcher;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 243
    new-instance v1, Lcom/squareup/util/rx2/Rx2Views$debouncedOnChanged$1$1;

    invoke-direct {v1, p0, v0}, Lcom/squareup/util/rx2/Rx2Views$debouncedOnChanged$1$1;-><init>(Lcom/squareup/util/rx2/Rx2Views$debouncedOnChanged$1;Lcom/squareup/util/rx2/Rx2Views$debouncedOnChanged$1$listener$1;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v1}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnMainThread(Lio/reactivex/ObservableEmitter;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
