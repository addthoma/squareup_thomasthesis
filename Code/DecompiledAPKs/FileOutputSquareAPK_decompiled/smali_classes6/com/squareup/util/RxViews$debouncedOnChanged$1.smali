.class final Lcom/squareup/util/RxViews$debouncedOnChanged$1;
.super Ljava/lang/Object;
.source "RxViews.kt"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/util/RxViews;->debouncedOnChanged(Landroid/widget/TextView;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Lrx/Emitter<",
        "TT;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012*\u0010\u0002\u001a&\u0012\u000c\u0012\n \u0004*\u0004\u0018\u00010\u00010\u0001 \u0004*\u0012\u0012\u000c\u0012\n \u0004*\u0004\u0018\u00010\u00010\u0001\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "emitter",
        "Lrx/Emitter;",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $this_debouncedOnChanged:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Landroid/widget/TextView;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/util/RxViews$debouncedOnChanged$1;->$this_debouncedOnChanged:Landroid/widget/TextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lrx/Emitter;

    invoke-virtual {p0, p1}, Lcom/squareup/util/RxViews$debouncedOnChanged$1;->call(Lrx/Emitter;)V

    return-void
.end method

.method public final call(Lrx/Emitter;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lrx/Emitter<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 164
    new-instance v0, Lcom/squareup/util/RxViews$debouncedOnChanged$1$listener$1;

    invoke-direct {v0, p1}, Lcom/squareup/util/RxViews$debouncedOnChanged$1$listener$1;-><init>(Lrx/Emitter;)V

    .line 168
    iget-object v1, p0, Lcom/squareup/util/RxViews$debouncedOnChanged$1;->$this_debouncedOnChanged:Landroid/widget/TextView;

    move-object v2, v0

    check-cast v2, Landroid/text/TextWatcher;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    const-string v1, "emitter"

    .line 170
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v1, Lcom/squareup/util/RxViews$debouncedOnChanged$1$1;

    invoke-direct {v1, p0, v0}, Lcom/squareup/util/RxViews$debouncedOnChanged$1$1;-><init>(Lcom/squareup/util/RxViews$debouncedOnChanged$1;Lcom/squareup/util/RxViews$debouncedOnChanged$1$listener$1;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v1}, Lcom/squareup/util/RxViews;->unsubscribeOnMainThread(Lrx/Emitter;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
