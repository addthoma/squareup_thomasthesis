.class public Lcom/squareup/util/ConditionalTaxesHelper;
.super Ljava/lang/Object;
.source "ConditionalTaxesHelper.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static updateAppliedTaxes(Ljava/util/Map;Ljava/util/Map;Ljava/util/Set;)Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Tax;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Tax;",
            ">;",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Tax;",
            ">;"
        }
    .end annotation

    .line 66
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 67
    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 69
    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/checkout/Tax;

    if-eqz v2, :cond_0

    .line 71
    iget-object v3, v2, Lcom/squareup/checkout/Tax;->id:Ljava/lang/String;

    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 74
    :cond_1
    invoke-interface {p0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_2
    :goto_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result p1

    if-eqz p1, :cond_3

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/checkout/Tax;

    .line 77
    iget-object v1, p1, Lcom/squareup/checkout/Tax;->id:Ljava/lang/String;

    invoke-interface {p2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 78
    iget-object v1, p1, Lcom/squareup/checkout/Tax;->id:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_3
    return-object v0
.end method

.method public static updateRuleBasedTaxes(Ljava/lang/String;ZLjava/util/Map;Ljava/util/List;Lcom/squareup/checkout/DiningOption;)Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Tax;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/payment/OrderTaxRule;",
            ">;",
            "Lcom/squareup/checkout/DiningOption;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/squareup/checkout/Tax;",
            ">;"
        }
    .end annotation

    .line 41
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    .line 42
    invoke-interface {p2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/checkout/Tax;

    const/4 v2, 0x0

    if-eqz p4, :cond_1

    .line 46
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/payment/OrderTaxRule;

    .line 47
    invoke-virtual {v4, p0, p1, v1, p4}, Lcom/squareup/payment/OrderTaxRule;->doesTaxRuleApply(Ljava/lang/String;ZLcom/squareup/checkout/Tax;Lcom/squareup/checkout/DiningOption;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 48
    invoke-virtual {v4}, Lcom/squareup/payment/OrderTaxRule;->getEnablingActionType()Lcom/squareup/api/items/EnablingActionType;

    move-result-object v2

    goto :goto_1

    :cond_1
    if-nez v2, :cond_2

    .line 53
    iget-object v2, v1, Lcom/squareup/checkout/Tax;->id:Ljava/lang/String;

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 54
    :cond_2
    sget-object v1, Lcom/squareup/api/items/EnablingActionType;->DISABLE:Lcom/squareup/api/items/EnablingActionType;

    if-ne v2, v1, :cond_3

    goto :goto_0

    .line 55
    :cond_3
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string p1, "Register only supports DISABLE tax rules."

    invoke-direct {p0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_4
    return-object v0
.end method

.method public static updateTaxesUpon(Lcom/squareup/checkout/CartItem;Ljava/util/List;Lcom/squareup/checkout/DiningOption;)Lcom/squareup/checkout/CartItem$Builder;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/checkout/CartItem;",
            "Ljava/util/List<",
            "Lcom/squareup/payment/OrderTaxRule;",
            ">;",
            "Lcom/squareup/checkout/DiningOption;",
            ")",
            "Lcom/squareup/checkout/CartItem$Builder;"
        }
    .end annotation

    .line 23
    invoke-virtual {p0}, Lcom/squareup/checkout/CartItem;->buildUpon()Lcom/squareup/checkout/CartItem$Builder;

    move-result-object v0

    .line 24
    iget-object v1, p0, Lcom/squareup/checkout/CartItem;->itemId:Ljava/lang/String;

    .line 25
    invoke-virtual {p0}, Lcom/squareup/checkout/CartItem;->isCustomItem()Z

    move-result v2

    iget-object v3, p0, Lcom/squareup/checkout/CartItem;->defaultTaxes:Ljava/util/Map;

    invoke-static {v1, v2, v3, p1, p2}, Lcom/squareup/util/ConditionalTaxesHelper;->updateRuleBasedTaxes(Ljava/lang/String;ZLjava/util/Map;Ljava/util/List;Lcom/squareup/checkout/DiningOption;)Ljava/util/Map;

    move-result-object p1

    .line 27
    iget-object p2, p0, Lcom/squareup/checkout/CartItem;->appliedTaxes:Ljava/util/Map;

    iget-object p0, p0, Lcom/squareup/checkout/CartItem;->userEditedTaxIds:Ljava/util/Set;

    .line 28
    invoke-static {p1, p2, p0}, Lcom/squareup/util/ConditionalTaxesHelper;->updateAppliedTaxes(Ljava/util/Map;Ljava/util/Map;Ljava/util/Set;)Ljava/util/Map;

    move-result-object p0

    .line 29
    invoke-virtual {v0, p1}, Lcom/squareup/checkout/CartItem$Builder;->ruleBasedTaxes(Ljava/util/Map;)Lcom/squareup/checkout/CartItem$Builder;

    .line 30
    invoke-virtual {v0, p0}, Lcom/squareup/checkout/CartItem$Builder;->appliedTaxes(Ljava/util/Map;)Lcom/squareup/checkout/CartItem$Builder;

    return-object v0
.end method
