.class public interface abstract Lcom/squareup/util/Res;
.super Ljava/lang/Object;
.source "Res.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/util/Res$RealRes;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000Z\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0007\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0011\n\u0002\u0008\u0002\n\u0002\u0010\r\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008f\u0018\u00002\u00020\u0001:\u0001!J\u0012\u0010\n\u001a\u00020\u000b2\u0008\u0008\u0001\u0010\u000c\u001a\u00020\rH&J\u0012\u0010\u000e\u001a\u00020\r2\u0008\u0008\u0001\u0010\u000c\u001a\u00020\rH\'J\u0012\u0010\u000f\u001a\u00020\u00102\u0008\u0008\u0001\u0010\u000c\u001a\u00020\rH&J\u0012\u0010\u0011\u001a\u00020\u00122\u0008\u0008\u0001\u0010\u000c\u001a\u00020\rH&J\u0012\u0010\u0013\u001a\u00020\r2\u0008\u0008\u0001\u0010\u000c\u001a\u00020\rH&J\u0012\u0010\u0014\u001a\u00020\u00152\u0008\u0008\u0001\u0010\u000c\u001a\u00020\rH&J\u0012\u0010\u0016\u001a\u00020\r2\u0008\u0008\u0001\u0010\u000c\u001a\u00020\rH&J\u0012\u0010\u0017\u001a\u00020\u00182\u0008\u0008\u0001\u0010\u000c\u001a\u00020\rH&J\u001d\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u00180\u001a2\u0008\u0008\u0001\u0010\u000c\u001a\u00020\rH&\u00a2\u0006\u0002\u0010\u001bJ\u0012\u0010\u001c\u001a\u00020\u001d2\u0008\u0008\u0001\u0010\u000c\u001a\u00020\rH&J\u0012\u0010\u001e\u001a\u00020\u001f2\u0008\u0008\u0001\u0010 \u001a\u00020\rH&R\u0012\u0010\u0002\u001a\u00020\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0004\u0010\u0005R\u0012\u0010\u0006\u001a\u00020\u0007X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0008\u0010\t\u00a8\u0006\""
    }
    d2 = {
        "Lcom/squareup/util/Res;",
        "",
        "assets",
        "Landroid/content/res/AssetManager;",
        "getAssets",
        "()Landroid/content/res/AssetManager;",
        "resources",
        "Landroid/content/res/Resources;",
        "getResources",
        "()Landroid/content/res/Resources;",
        "getBoolean",
        "",
        "id",
        "",
        "getColor",
        "getColorStateList",
        "Landroid/content/res/ColorStateList;",
        "getDimension",
        "",
        "getDimensionPixelSize",
        "getDrawable",
        "Landroid/graphics/drawable/Drawable;",
        "getInteger",
        "getString",
        "",
        "getStringArray",
        "",
        "(I)[Ljava/lang/String;",
        "getText",
        "",
        "phrase",
        "Lcom/squareup/phrase/Phrase;",
        "patternResourceId",
        "RealRes",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract getAssets()Landroid/content/res/AssetManager;
.end method

.method public abstract getBoolean(I)Z
.end method

.method public abstract getColor(I)I
.end method

.method public abstract getColorStateList(I)Landroid/content/res/ColorStateList;
.end method

.method public abstract getDimension(I)F
.end method

.method public abstract getDimensionPixelSize(I)I
.end method

.method public abstract getDrawable(I)Landroid/graphics/drawable/Drawable;
.end method

.method public abstract getInteger(I)I
.end method

.method public abstract getResources()Landroid/content/res/Resources;
.end method

.method public abstract getString(I)Ljava/lang/String;
.end method

.method public abstract getStringArray(I)[Ljava/lang/String;
.end method

.method public abstract getText(I)Ljava/lang/CharSequence;
.end method

.method public abstract phrase(I)Lcom/squareup/phrase/Phrase;
.end method
