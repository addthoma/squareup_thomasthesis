.class public Lcom/squareup/util/ReceiptTenderNameUtils;
.super Ljava/lang/Object;
.source "ReceiptTenderNameUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static getEntryMethodStringOrNull(Lcom/squareup/util/Res;Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)Ljava/lang/String;
    .locals 2

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return-object v0

    .line 41
    :cond_0
    sget-object v1, Lcom/squareup/util/ReceiptTenderNameUtils$1;->$SwitchMap$com$squareup$protos$client$bills$CardTender$Card$EntryMethod:[I

    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->ordinal()I

    move-result p1

    aget p1, v1, p1

    packed-switch p1, :pswitch_data_0

    return-object v0

    :pswitch_0
    const/4 p0, 0x0

    new-array p0, p0, [Ljava/lang/Object;

    const-string p1, "Encountered UNKNOWN_ENTRY_METHOD. Neat."

    .line 53
    invoke-static {p1, p0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-object v0

    .line 51
    :pswitch_1
    sget p1, Lcom/squareup/checkout/R$string;->buyer_printed_receipt_entry_method_on_file:I

    invoke-interface {p0, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 49
    :pswitch_2
    sget p1, Lcom/squareup/checkout/R$string;->buyer_printed_receipt_entry_method_contactless:I

    invoke-interface {p0, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 47
    :pswitch_3
    sget p1, Lcom/squareup/checkout/R$string;->buyer_printed_receipt_entry_method_emv:I

    invoke-interface {p0, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 45
    :pswitch_4
    sget p1, Lcom/squareup/checkout/R$string;->buyer_printed_receipt_entry_method_keyed:I

    invoke-interface {p0, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    .line 43
    :pswitch_5
    sget p1, Lcom/squareup/checkout/R$string;->buyer_printed_receipt_entry_method_swipe:I

    invoke-interface {p0, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static getReceiptCardTenderName(Lcom/squareup/util/Res;ILjava/lang/String;Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)Ljava/lang/String;
    .locals 4

    .line 11
    invoke-interface {p0, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    .line 12
    invoke-static {p0, p3}, Lcom/squareup/util/ReceiptTenderNameUtils;->getEntryMethodStringOrNull(Lcom/squareup/util/Res;Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "entry_method"

    const-string v2, "card_suffix"

    const-string v3, "card_brand"

    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    .line 14
    sget p3, Lcom/squareup/checkout/R$string;->buyer_printed_receipt_tender_card_detail_all:I

    invoke-interface {p0, p3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 15
    invoke-virtual {p0, v3, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 16
    invoke-virtual {p0, v2, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 17
    invoke-virtual {p0, v1, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 18
    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_0
    if-eqz p2, :cond_1

    .line 20
    sget p3, Lcom/squareup/checkout/R$string;->buyer_printed_receipt_tender_card_detail_no_entry_method:I

    invoke-interface {p0, p3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 21
    invoke-virtual {p0, v3, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 22
    invoke-virtual {p0, v2, p2}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 23
    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_1
    if-eqz v0, :cond_2

    .line 25
    sget p2, Lcom/squareup/checkout/R$string;->buyer_printed_receipt_tender_card_detail_no_suffix:I

    invoke-interface {p0, p2}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 26
    invoke-virtual {p0, v3, p1}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 27
    invoke-virtual {p0, v1, v0}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object p0

    .line 28
    invoke-virtual {p0}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object p0

    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_2
    return-object p1
.end method

.method public static getReceiptOtherTenderName(Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 1

    .line 34
    sget v0, Lcom/squareup/checkout/R$string;->buyer_printed_receipt_tender_other:I

    invoke-interface {p0, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method
