.class final Lcom/squareup/util/CountryChecker$Point;
.super Ljava/lang/Object;
.source "CountryChecker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/util/CountryChecker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "Point"
.end annotation


# instance fields
.field private final latitude:D

.field private final longitude:D


# direct methods
.method constructor <init>(DD)V
    .locals 0

    .line 248
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 249
    iput-wide p1, p0, Lcom/squareup/util/CountryChecker$Point;->latitude:D

    .line 250
    iput-wide p3, p0, Lcom/squareup/util/CountryChecker$Point;->longitude:D

    return-void
.end method

.method static synthetic access$200(Lcom/squareup/util/CountryChecker$Point;)D
    .locals 2

    .line 244
    iget-wide v0, p0, Lcom/squareup/util/CountryChecker$Point;->latitude:D

    return-wide v0
.end method

.method static synthetic access$300(Lcom/squareup/util/CountryChecker$Point;)D
    .locals 2

    .line 244
    iget-wide v0, p0, Lcom/squareup/util/CountryChecker$Point;->longitude:D

    return-wide v0
.end method
