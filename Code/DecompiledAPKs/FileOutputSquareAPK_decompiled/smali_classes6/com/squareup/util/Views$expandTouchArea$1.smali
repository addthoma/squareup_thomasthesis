.class final Lcom/squareup/util/Views$expandTouchArea$1;
.super Ljava/lang/Object;
.source "Views.kt"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/util/Views;->expandTouchArea(Landroid/view/View;Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "run"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $bigView:Landroid/view/View;

.field final synthetic $smallView:Landroid/view/View;


# direct methods
.method constructor <init>(Landroid/view/View;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/util/Views$expandTouchArea$1;->$bigView:Landroid/view/View;

    iput-object p2, p0, Lcom/squareup/util/Views$expandTouchArea$1;->$smallView:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .line 603
    iget-object v0, p0, Lcom/squareup/util/Views$expandTouchArea$1;->$bigView:Landroid/view/View;

    new-instance v1, Lcom/squareup/util/SharingTouchDelegate;

    iget-object v2, p0, Lcom/squareup/util/Views$expandTouchArea$1;->$smallView:Landroid/view/View;

    const/4 v3, 0x0

    invoke-direct {v1, v0, v2, v3}, Lcom/squareup/util/SharingTouchDelegate;-><init>(Landroid/view/View;Landroid/view/View;Z)V

    check-cast v1, Landroid/view/TouchDelegate;

    invoke-virtual {v0, v1}, Landroid/view/View;->setTouchDelegate(Landroid/view/TouchDelegate;)V

    return-void
.end method
