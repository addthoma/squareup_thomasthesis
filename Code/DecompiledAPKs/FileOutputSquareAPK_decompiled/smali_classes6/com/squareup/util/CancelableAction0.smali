.class public Lcom/squareup/util/CancelableAction0;
.super Ljava/lang/Object;
.source "CancelableAction0.java"

# interfaces
.implements Lrx/functions/Action0;


# static fields
.field private static final actionsInProgress:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/squareup/util/CancelableAction0;",
            "Ljava/lang/Thread;",
            ">;"
        }
    .end annotation
.end field

.field private static final enableDisableLock:Ljava/lang/Object;

.field private static final phaser:Ljava/util/concurrent/Phaser;

.field private static volatile rxActionsEnabled:Ljava/util/concurrent/atomic/AtomicBoolean;


# instance fields
.field private final createdAt:J

.field private final delegate:Lrx/functions/Action0;

.field private final enabled:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private startedRunningAt:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 24
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/squareup/util/CancelableAction0;->enableDisableLock:Ljava/lang/Object;

    .line 27
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    sput-object v0, Lcom/squareup/util/CancelableAction0;->rxActionsEnabled:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 38
    new-instance v0, Ljava/util/concurrent/Phaser;

    invoke-direct {v0, v1}, Ljava/util/concurrent/Phaser;-><init>(I)V

    sput-object v0, Lcom/squareup/util/CancelableAction0;->phaser:Ljava/util/concurrent/Phaser;

    .line 43
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lcom/squareup/util/CancelableAction0;->actionsInProgress:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>(Lrx/functions/Action0;Ljava/util/concurrent/atomic/AtomicBoolean;)V
    .locals 2

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/squareup/util/CancelableAction0;->createdAt:J

    .line 52
    iput-object p1, p0, Lcom/squareup/util/CancelableAction0;->delegate:Lrx/functions/Action0;

    .line 53
    iput-object p2, p0, Lcom/squareup/util/CancelableAction0;->enabled:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method

.method public static disableAndAwaitTermination(JLjava/util/concurrent/TimeUnit;)V
    .locals 11

    .line 86
    sget-object v0, Lcom/squareup/util/CancelableAction0;->enableDisableLock:Ljava/lang/Object;

    monitor-enter v0

    .line 87
    :try_start_0
    sget-object v1, Lcom/squareup/util/CancelableAction0;->rxActionsEnabled:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    const-string v2, "Already disabled."

    invoke-static {v1, v2}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 88
    sget-object v1, Lcom/squareup/util/CancelableAction0;->phaser:Ljava/util/concurrent/Phaser;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 89
    :try_start_1
    sget-object v2, Lcom/squareup/util/CancelableAction0;->rxActionsEnabled:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 90
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 92
    :try_start_2
    sget-object v1, Lcom/squareup/util/CancelableAction0;->phaser:Ljava/util/concurrent/Phaser;

    sget-object v2, Lcom/squareup/util/CancelableAction0;->phaser:Ljava/util/concurrent/Phaser;

    invoke-virtual {v2}, Ljava/util/concurrent/Phaser;->arrive()I

    move-result v2

    invoke-virtual {v1, v2, p0, p1, p2}, Ljava/util/concurrent/Phaser;->awaitAdvanceInterruptibly(IJLjava/util/concurrent/TimeUnit;)I
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 124
    :try_start_3
    monitor-exit v0

    return-void

    .line 97
    :catch_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide p0

    const/4 p2, 0x0

    .line 99
    sget-object v1, Lcom/squareup/util/CancelableAction0;->actionsInProgress:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 100
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/util/CancelableAction0;

    .line 101
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Thread;

    .line 102
    invoke-virtual {v2}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    .line 103
    iget-wide v5, v3, Lcom/squareup/util/CancelableAction0;->startedRunningAt:J

    sub-long v5, p0, v5

    .line 104
    iget-wide v7, v3, Lcom/squareup/util/CancelableAction0;->startedRunningAt:J

    iget-wide v9, v3, Lcom/squareup/util/CancelableAction0;->createdAt:J

    sub-long/2addr v7, v9

    .line 105
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Action "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, v3, Lcom/squareup/util/CancelableAction0;->delegate:Lrx/functions/Action0;

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v3, " started after "

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v3, "ms and running for "

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v3, "ms on thread ["

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 112
    invoke-virtual {v2}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "]"

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    if-eqz p2, :cond_0

    .line 114
    new-instance v3, Ljava/lang/RuntimeException;

    invoke-direct {v3, v2, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object p2, v3

    goto :goto_1

    .line 116
    :cond_0
    new-instance p2, Ljava/lang/RuntimeException;

    invoke-direct {p2, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 118
    :goto_1
    invoke-virtual {p2, v4}, Ljava/lang/Exception;->setStackTrace([Ljava/lang/StackTraceElement;)V

    goto :goto_0

    .line 120
    :cond_1
    new-instance p0, Ljava/lang/RuntimeException;

    const-string p1, "Actions did not terminate before timeout. Each cause shows where each action was hanging"

    invoke-direct {p0, p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p0

    :catch_1
    move-exception p0

    .line 94
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    .line 95
    new-instance p1, Ljava/lang/RuntimeException;

    const-string p2, "Actions did not terminate before interrupted."

    invoke-direct {p1, p2, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catchall_0
    move-exception p0

    .line 90
    :try_start_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw p0

    :catchall_1
    move-exception p0

    .line 124
    monitor-exit v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw p0
.end method

.method public static enableNewActions()V
    .locals 4

    .line 132
    sget-object v0, Lcom/squareup/util/CancelableAction0;->enableDisableLock:Ljava/lang/Object;

    monitor-enter v0

    .line 133
    :try_start_0
    sget-object v1, Lcom/squareup/util/CancelableAction0;->rxActionsEnabled:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    const/4 v2, 0x1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    const-string v3, "Already enabled."

    invoke-static {v1, v3}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 134
    new-instance v1, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    sput-object v1, Lcom/squareup/util/CancelableAction0;->rxActionsEnabled:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 135
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public static wrap(Lrx/functions/Action0;)Lrx/functions/Action0;
    .locals 2

    .line 139
    new-instance v0, Lcom/squareup/util/CancelableAction0;

    sget-object v1, Lcom/squareup/util/CancelableAction0;->rxActionsEnabled:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, p0, v1}, Lcom/squareup/util/CancelableAction0;-><init>(Lrx/functions/Action0;Ljava/util/concurrent/atomic/AtomicBoolean;)V

    return-object v0
.end method


# virtual methods
.method public call()V
    .locals 2

    .line 64
    sget-object v0, Lcom/squareup/util/CancelableAction0;->phaser:Ljava/util/concurrent/Phaser;

    monitor-enter v0

    .line 65
    :try_start_0
    iget-object v1, p0, Lcom/squareup/util/CancelableAction0;->enabled:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-nez v1, :cond_0

    monitor-exit v0

    return-void

    .line 66
    :cond_0
    sget-object v1, Lcom/squareup/util/CancelableAction0;->phaser:Ljava/util/concurrent/Phaser;

    invoke-virtual {v1}, Ljava/util/concurrent/Phaser;->register()I

    .line 67
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 69
    :try_start_1
    sget-object v0, Lcom/squareup/util/CancelableAction0;->actionsInProgress:Ljava/util/Map;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-interface {v0, p0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/squareup/util/CancelableAction0;->startedRunningAt:J

    .line 71
    iget-object v0, p0, Lcom/squareup/util/CancelableAction0;->delegate:Lrx/functions/Action0;

    invoke-interface {v0}, Lrx/functions/Action0;->call()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 73
    sget-object v0, Lcom/squareup/util/CancelableAction0;->actionsInProgress:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    sget-object v0, Lcom/squareup/util/CancelableAction0;->phaser:Ljava/util/concurrent/Phaser;

    invoke-virtual {v0}, Ljava/util/concurrent/Phaser;->arriveAndDeregister()I

    return-void

    :catchall_0
    move-exception v0

    .line 73
    sget-object v1, Lcom/squareup/util/CancelableAction0;->actionsInProgress:Ljava/util/Map;

    invoke-interface {v1, p0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    sget-object v1, Lcom/squareup/util/CancelableAction0;->phaser:Ljava/util/concurrent/Phaser;

    invoke-virtual {v1}, Ljava/util/concurrent/Phaser;->arriveAndDeregister()I

    .line 75
    throw v0

    :catchall_1
    move-exception v1

    .line 67
    :try_start_2
    monitor-exit v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v1
.end method
