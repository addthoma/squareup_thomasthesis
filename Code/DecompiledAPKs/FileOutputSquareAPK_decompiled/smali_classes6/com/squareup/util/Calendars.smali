.class public final Lcom/squareup/util/Calendars;
.super Ljava/lang/Object;
.source "Calendars.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008$\u001a\n\u0010&\u001a\u00020\u0003*\u00020\u0003\"/\u0010\u0002\u001a\u00020\u0001*\u00020\u00032\u0006\u0010\u0000\u001a\u00020\u00018F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\u0008\u0008\u0010\t\u001a\u0004\u0008\u0004\u0010\u0005\"\u0004\u0008\u0006\u0010\u0007\"/\u0010\n\u001a\u00020\u0001*\u00020\u00032\u0006\u0010\u0000\u001a\u00020\u00018F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\u0008\r\u0010\t\u001a\u0004\u0008\u000b\u0010\u0005\"\u0004\u0008\u000c\u0010\u0007\"/\u0010\u000e\u001a\u00020\u0001*\u00020\u00032\u0006\u0010\u0000\u001a\u00020\u00018F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\u0008\u0011\u0010\t\u001a\u0004\u0008\u000f\u0010\u0005\"\u0004\u0008\u0010\u0010\u0007\"/\u0010\u0012\u001a\u00020\u0001*\u00020\u00032\u0006\u0010\u0000\u001a\u00020\u00018F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\u0008\u0015\u0010\t\u001a\u0004\u0008\u0013\u0010\u0005\"\u0004\u0008\u0014\u0010\u0007\"/\u0010\u0016\u001a\u00020\u0001*\u00020\u00032\u0006\u0010\u0000\u001a\u00020\u00018F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\u0008\u0019\u0010\t\u001a\u0004\u0008\u0017\u0010\u0005\"\u0004\u0008\u0018\u0010\u0007\"/\u0010\u001a\u001a\u00020\u0001*\u00020\u00032\u0006\u0010\u0000\u001a\u00020\u00018F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\u0008\u001d\u0010\t\u001a\u0004\u0008\u001b\u0010\u0005\"\u0004\u0008\u001c\u0010\u0007\"/\u0010\u001e\u001a\u00020\u0001*\u00020\u00032\u0006\u0010\u0000\u001a\u00020\u00018F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\u0008!\u0010\t\u001a\u0004\u0008\u001f\u0010\u0005\"\u0004\u0008 \u0010\u0007\"/\u0010\"\u001a\u00020\u0001*\u00020\u00032\u0006\u0010\u0000\u001a\u00020\u00018F@FX\u0086\u008e\u0002\u00a2\u0006\u0012\n\u0004\u0008%\u0010\t\u001a\u0004\u0008#\u0010\u0005\"\u0004\u0008$\u0010\u0007\u00a8\u0006\'"
    }
    d2 = {
        "<set-?>",
        "",
        "ampm",
        "Ljava/util/Calendar;",
        "getAmpm",
        "(Ljava/util/Calendar;)I",
        "setAmpm",
        "(Ljava/util/Calendar;I)V",
        "ampm$delegate",
        "Lcom/squareup/util/CalendarFieldDelegate;",
        "day",
        "getDay",
        "setDay",
        "day$delegate",
        "hour",
        "getHour",
        "setHour",
        "hour$delegate",
        "millisecond",
        "getMillisecond",
        "setMillisecond",
        "millisecond$delegate",
        "minute",
        "getMinute",
        "setMinute",
        "minute$delegate",
        "month",
        "getMonth",
        "setMonth",
        "month$delegate",
        "second",
        "getSecond",
        "setSecond",
        "second$delegate",
        "year",
        "getYear",
        "setYear",
        "year$delegate",
        "copy",
        "public"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field static final synthetic $$delegatedProperties:[Lkotlin/reflect/KProperty;

.field private static final ampm$delegate:Lcom/squareup/util/CalendarFieldDelegate;

.field private static final day$delegate:Lcom/squareup/util/CalendarFieldDelegate;

.field private static final hour$delegate:Lcom/squareup/util/CalendarFieldDelegate;

.field private static final millisecond$delegate:Lcom/squareup/util/CalendarFieldDelegate;

.field private static final minute$delegate:Lcom/squareup/util/CalendarFieldDelegate;

.field private static final month$delegate:Lcom/squareup/util/CalendarFieldDelegate;

.field private static final second$delegate:Lcom/squareup/util/CalendarFieldDelegate;

.field private static final year$delegate:Lcom/squareup/util/CalendarFieldDelegate;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    const-class v0, Lcom/squareup/util/Calendars;

    const/16 v1, 0x8

    new-array v1, v1, [Lkotlin/reflect/KProperty;

    new-instance v2, Lkotlin/jvm/internal/MutablePropertyReference1Impl;

    const-string v3, "public"

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinPackage(Ljava/lang/Class;Ljava/lang/String;)Lkotlin/reflect/KDeclarationContainer;

    move-result-object v4

    const-string v5, "year"

    const-string v6, "getYear(Ljava/util/Calendar;)I"

    invoke-direct {v2, v4, v5, v6}, Lkotlin/jvm/internal/MutablePropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->mutableProperty1(Lkotlin/jvm/internal/MutablePropertyReference1;)Lkotlin/reflect/KMutableProperty1;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v4, 0x0

    aput-object v2, v1, v4

    new-instance v2, Lkotlin/jvm/internal/MutablePropertyReference1Impl;

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinPackage(Ljava/lang/Class;Ljava/lang/String;)Lkotlin/reflect/KDeclarationContainer;

    move-result-object v4

    const-string v5, "month"

    const-string v6, "getMonth(Ljava/util/Calendar;)I"

    invoke-direct {v2, v4, v5, v6}, Lkotlin/jvm/internal/MutablePropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->mutableProperty1(Lkotlin/jvm/internal/MutablePropertyReference1;)Lkotlin/reflect/KMutableProperty1;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v4, 0x1

    aput-object v2, v1, v4

    new-instance v2, Lkotlin/jvm/internal/MutablePropertyReference1Impl;

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinPackage(Ljava/lang/Class;Ljava/lang/String;)Lkotlin/reflect/KDeclarationContainer;

    move-result-object v5

    const-string v6, "day"

    const-string v7, "getDay(Ljava/util/Calendar;)I"

    invoke-direct {v2, v5, v6, v7}, Lkotlin/jvm/internal/MutablePropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->mutableProperty1(Lkotlin/jvm/internal/MutablePropertyReference1;)Lkotlin/reflect/KMutableProperty1;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v5, 0x2

    aput-object v2, v1, v5

    new-instance v2, Lkotlin/jvm/internal/MutablePropertyReference1Impl;

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinPackage(Ljava/lang/Class;Ljava/lang/String;)Lkotlin/reflect/KDeclarationContainer;

    move-result-object v6

    const-string v7, "hour"

    const-string v8, "getHour(Ljava/util/Calendar;)I"

    invoke-direct {v2, v6, v7, v8}, Lkotlin/jvm/internal/MutablePropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->mutableProperty1(Lkotlin/jvm/internal/MutablePropertyReference1;)Lkotlin/reflect/KMutableProperty1;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v6, 0x3

    aput-object v2, v1, v6

    new-instance v2, Lkotlin/jvm/internal/MutablePropertyReference1Impl;

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinPackage(Ljava/lang/Class;Ljava/lang/String;)Lkotlin/reflect/KDeclarationContainer;

    move-result-object v6

    const-string v7, "ampm"

    const-string v8, "getAmpm(Ljava/util/Calendar;)I"

    invoke-direct {v2, v6, v7, v8}, Lkotlin/jvm/internal/MutablePropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->mutableProperty1(Lkotlin/jvm/internal/MutablePropertyReference1;)Lkotlin/reflect/KMutableProperty1;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v6, 0x4

    aput-object v2, v1, v6

    new-instance v2, Lkotlin/jvm/internal/MutablePropertyReference1Impl;

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinPackage(Ljava/lang/Class;Ljava/lang/String;)Lkotlin/reflect/KDeclarationContainer;

    move-result-object v6

    const-string v7, "minute"

    const-string v8, "getMinute(Ljava/util/Calendar;)I"

    invoke-direct {v2, v6, v7, v8}, Lkotlin/jvm/internal/MutablePropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->mutableProperty1(Lkotlin/jvm/internal/MutablePropertyReference1;)Lkotlin/reflect/KMutableProperty1;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v6, 0x5

    aput-object v2, v1, v6

    new-instance v2, Lkotlin/jvm/internal/MutablePropertyReference1Impl;

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinPackage(Ljava/lang/Class;Ljava/lang/String;)Lkotlin/reflect/KDeclarationContainer;

    move-result-object v7

    const-string v8, "second"

    const-string v9, "getSecond(Ljava/util/Calendar;)I"

    invoke-direct {v2, v7, v8, v9}, Lkotlin/jvm/internal/MutablePropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->mutableProperty1(Lkotlin/jvm/internal/MutablePropertyReference1;)Lkotlin/reflect/KMutableProperty1;

    move-result-object v2

    check-cast v2, Lkotlin/reflect/KProperty;

    const/4 v7, 0x6

    aput-object v2, v1, v7

    new-instance v2, Lkotlin/jvm/internal/MutablePropertyReference1Impl;

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinPackage(Ljava/lang/Class;Ljava/lang/String;)Lkotlin/reflect/KDeclarationContainer;

    move-result-object v0

    const-string v3, "millisecond"

    const-string v7, "getMillisecond(Ljava/util/Calendar;)I"

    invoke-direct {v2, v0, v3, v7}, Lkotlin/jvm/internal/MutablePropertyReference1Impl;-><init>(Lkotlin/reflect/KDeclarationContainer;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->mutableProperty1(Lkotlin/jvm/internal/MutablePropertyReference1;)Lkotlin/reflect/KMutableProperty1;

    move-result-object v0

    check-cast v0, Lkotlin/reflect/KProperty;

    const/4 v2, 0x7

    aput-object v0, v1, v2

    sput-object v1, Lcom/squareup/util/Calendars;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    .line 10
    new-instance v0, Lcom/squareup/util/CalendarFieldDelegate;

    invoke-direct {v0, v4}, Lcom/squareup/util/CalendarFieldDelegate;-><init>(I)V

    sput-object v0, Lcom/squareup/util/Calendars;->year$delegate:Lcom/squareup/util/CalendarFieldDelegate;

    .line 13
    new-instance v0, Lcom/squareup/util/CalendarFieldDelegate;

    invoke-direct {v0, v5}, Lcom/squareup/util/CalendarFieldDelegate;-><init>(I)V

    sput-object v0, Lcom/squareup/util/Calendars;->month$delegate:Lcom/squareup/util/CalendarFieldDelegate;

    .line 16
    new-instance v0, Lcom/squareup/util/CalendarFieldDelegate;

    invoke-direct {v0, v6}, Lcom/squareup/util/CalendarFieldDelegate;-><init>(I)V

    sput-object v0, Lcom/squareup/util/Calendars;->day$delegate:Lcom/squareup/util/CalendarFieldDelegate;

    .line 19
    new-instance v0, Lcom/squareup/util/CalendarFieldDelegate;

    const/16 v1, 0xb

    invoke-direct {v0, v1}, Lcom/squareup/util/CalendarFieldDelegate;-><init>(I)V

    sput-object v0, Lcom/squareup/util/Calendars;->hour$delegate:Lcom/squareup/util/CalendarFieldDelegate;

    .line 22
    new-instance v0, Lcom/squareup/util/CalendarFieldDelegate;

    const/16 v1, 0x9

    invoke-direct {v0, v1}, Lcom/squareup/util/CalendarFieldDelegate;-><init>(I)V

    sput-object v0, Lcom/squareup/util/Calendars;->ampm$delegate:Lcom/squareup/util/CalendarFieldDelegate;

    .line 25
    new-instance v0, Lcom/squareup/util/CalendarFieldDelegate;

    const/16 v1, 0xc

    invoke-direct {v0, v1}, Lcom/squareup/util/CalendarFieldDelegate;-><init>(I)V

    sput-object v0, Lcom/squareup/util/Calendars;->minute$delegate:Lcom/squareup/util/CalendarFieldDelegate;

    .line 28
    new-instance v0, Lcom/squareup/util/CalendarFieldDelegate;

    const/16 v1, 0xd

    invoke-direct {v0, v1}, Lcom/squareup/util/CalendarFieldDelegate;-><init>(I)V

    sput-object v0, Lcom/squareup/util/Calendars;->second$delegate:Lcom/squareup/util/CalendarFieldDelegate;

    .line 31
    new-instance v0, Lcom/squareup/util/CalendarFieldDelegate;

    const/16 v1, 0xe

    invoke-direct {v0, v1}, Lcom/squareup/util/CalendarFieldDelegate;-><init>(I)V

    sput-object v0, Lcom/squareup/util/Calendars;->millisecond$delegate:Lcom/squareup/util/CalendarFieldDelegate;

    return-void
.end method

.method public static final copy(Ljava/util/Calendar;)Ljava/util/Calendar;
    .locals 1

    const-string v0, "$this$copy"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-virtual {p0}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object p0

    if-eqz p0, :cond_0

    check-cast p0, Ljava/util/Calendar;

    return-object p0

    :cond_0
    new-instance p0, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type java.util.Calendar"

    invoke-direct {p0, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static final getAmpm(Ljava/util/Calendar;)I
    .locals 3

    const-string v0, "$this$ampm"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/squareup/util/Calendars;->ampm$delegate:Lcom/squareup/util/CalendarFieldDelegate;

    sget-object v1, Lcom/squareup/util/Calendars;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-virtual {v0, p0, v1}, Lcom/squareup/util/CalendarFieldDelegate;->getValue(Ljava/util/Calendar;Lkotlin/reflect/KProperty;)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    return p0
.end method

.method public static final getDay(Ljava/util/Calendar;)I
    .locals 3

    const-string v0, "$this$day"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/squareup/util/Calendars;->day$delegate:Lcom/squareup/util/CalendarFieldDelegate;

    sget-object v1, Lcom/squareup/util/Calendars;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-virtual {v0, p0, v1}, Lcom/squareup/util/CalendarFieldDelegate;->getValue(Ljava/util/Calendar;Lkotlin/reflect/KProperty;)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    return p0
.end method

.method public static final getHour(Ljava/util/Calendar;)I
    .locals 3

    const-string v0, "$this$hour"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/squareup/util/Calendars;->hour$delegate:Lcom/squareup/util/CalendarFieldDelegate;

    sget-object v1, Lcom/squareup/util/Calendars;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-virtual {v0, p0, v1}, Lcom/squareup/util/CalendarFieldDelegate;->getValue(Ljava/util/Calendar;Lkotlin/reflect/KProperty;)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    return p0
.end method

.method public static final getMillisecond(Ljava/util/Calendar;)I
    .locals 3

    const-string v0, "$this$millisecond"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/squareup/util/Calendars;->millisecond$delegate:Lcom/squareup/util/CalendarFieldDelegate;

    sget-object v1, Lcom/squareup/util/Calendars;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x7

    aget-object v1, v1, v2

    invoke-virtual {v0, p0, v1}, Lcom/squareup/util/CalendarFieldDelegate;->getValue(Ljava/util/Calendar;Lkotlin/reflect/KProperty;)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    return p0
.end method

.method public static final getMinute(Ljava/util/Calendar;)I
    .locals 3

    const-string v0, "$this$minute"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/squareup/util/Calendars;->minute$delegate:Lcom/squareup/util/CalendarFieldDelegate;

    sget-object v1, Lcom/squareup/util/Calendars;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-virtual {v0, p0, v1}, Lcom/squareup/util/CalendarFieldDelegate;->getValue(Ljava/util/Calendar;Lkotlin/reflect/KProperty;)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    return p0
.end method

.method public static final getMonth(Ljava/util/Calendar;)I
    .locals 3

    const-string v0, "$this$month"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/squareup/util/Calendars;->month$delegate:Lcom/squareup/util/CalendarFieldDelegate;

    sget-object v1, Lcom/squareup/util/Calendars;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, p0, v1}, Lcom/squareup/util/CalendarFieldDelegate;->getValue(Ljava/util/Calendar;Lkotlin/reflect/KProperty;)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    return p0
.end method

.method public static final getSecond(Ljava/util/Calendar;)I
    .locals 3

    const-string v0, "$this$second"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/squareup/util/Calendars;->second$delegate:Lcom/squareup/util/CalendarFieldDelegate;

    sget-object v1, Lcom/squareup/util/Calendars;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-virtual {v0, p0, v1}, Lcom/squareup/util/CalendarFieldDelegate;->getValue(Ljava/util/Calendar;Lkotlin/reflect/KProperty;)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    return p0
.end method

.method public static final getYear(Ljava/util/Calendar;)I
    .locals 3

    const-string v0, "$this$year"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/squareup/util/Calendars;->year$delegate:Lcom/squareup/util/CalendarFieldDelegate;

    sget-object v1, Lcom/squareup/util/Calendars;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, p0, v1}, Lcom/squareup/util/CalendarFieldDelegate;->getValue(Ljava/util/Calendar;Lkotlin/reflect/KProperty;)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result p0

    return p0
.end method

.method public static final setAmpm(Ljava/util/Calendar;I)V
    .locals 3

    const-string v0, "$this$ampm"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/squareup/util/Calendars;->ampm$delegate:Lcom/squareup/util/CalendarFieldDelegate;

    sget-object v1, Lcom/squareup/util/Calendars;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    invoke-virtual {v0, p0, v1, p1}, Lcom/squareup/util/CalendarFieldDelegate;->setValue(Ljava/util/Calendar;Lkotlin/reflect/KProperty;I)V

    return-void
.end method

.method public static final setDay(Ljava/util/Calendar;I)V
    .locals 3

    const-string v0, "$this$day"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/squareup/util/Calendars;->day$delegate:Lcom/squareup/util/CalendarFieldDelegate;

    sget-object v1, Lcom/squareup/util/Calendars;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-virtual {v0, p0, v1, p1}, Lcom/squareup/util/CalendarFieldDelegate;->setValue(Ljava/util/Calendar;Lkotlin/reflect/KProperty;I)V

    return-void
.end method

.method public static final setHour(Ljava/util/Calendar;I)V
    .locals 3

    const-string v0, "$this$hour"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/squareup/util/Calendars;->hour$delegate:Lcom/squareup/util/CalendarFieldDelegate;

    sget-object v1, Lcom/squareup/util/Calendars;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    invoke-virtual {v0, p0, v1, p1}, Lcom/squareup/util/CalendarFieldDelegate;->setValue(Ljava/util/Calendar;Lkotlin/reflect/KProperty;I)V

    return-void
.end method

.method public static final setMillisecond(Ljava/util/Calendar;I)V
    .locals 3

    const-string v0, "$this$millisecond"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/squareup/util/Calendars;->millisecond$delegate:Lcom/squareup/util/CalendarFieldDelegate;

    sget-object v1, Lcom/squareup/util/Calendars;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x7

    aget-object v1, v1, v2

    invoke-virtual {v0, p0, v1, p1}, Lcom/squareup/util/CalendarFieldDelegate;->setValue(Ljava/util/Calendar;Lkotlin/reflect/KProperty;I)V

    return-void
.end method

.method public static final setMinute(Ljava/util/Calendar;I)V
    .locals 3

    const-string v0, "$this$minute"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/squareup/util/Calendars;->minute$delegate:Lcom/squareup/util/CalendarFieldDelegate;

    sget-object v1, Lcom/squareup/util/Calendars;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    invoke-virtual {v0, p0, v1, p1}, Lcom/squareup/util/CalendarFieldDelegate;->setValue(Ljava/util/Calendar;Lkotlin/reflect/KProperty;I)V

    return-void
.end method

.method public static final setMonth(Ljava/util/Calendar;I)V
    .locals 3

    const-string v0, "$this$month"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/squareup/util/Calendars;->month$delegate:Lcom/squareup/util/CalendarFieldDelegate;

    sget-object v1, Lcom/squareup/util/Calendars;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, p0, v1, p1}, Lcom/squareup/util/CalendarFieldDelegate;->setValue(Ljava/util/Calendar;Lkotlin/reflect/KProperty;I)V

    return-void
.end method

.method public static final setSecond(Ljava/util/Calendar;I)V
    .locals 3

    const-string v0, "$this$second"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/squareup/util/Calendars;->second$delegate:Lcom/squareup/util/CalendarFieldDelegate;

    sget-object v1, Lcom/squareup/util/Calendars;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    invoke-virtual {v0, p0, v1, p1}, Lcom/squareup/util/CalendarFieldDelegate;->setValue(Ljava/util/Calendar;Lkotlin/reflect/KProperty;I)V

    return-void
.end method

.method public static final setYear(Ljava/util/Calendar;I)V
    .locals 3

    const-string v0, "$this$year"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lcom/squareup/util/Calendars;->year$delegate:Lcom/squareup/util/CalendarFieldDelegate;

    sget-object v1, Lcom/squareup/util/Calendars;->$$delegatedProperties:[Lkotlin/reflect/KProperty;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, p0, v1, p1}, Lcom/squareup/util/CalendarFieldDelegate;->setValue(Ljava/util/Calendar;Lkotlin/reflect/KProperty;I)V

    return-void
.end method
