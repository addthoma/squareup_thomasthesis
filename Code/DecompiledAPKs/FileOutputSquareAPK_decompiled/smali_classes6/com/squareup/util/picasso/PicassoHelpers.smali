.class public final Lcom/squareup/util/picasso/PicassoHelpers;
.super Ljava/lang/Object;
.source "PicassoHelpers.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic lambda$toSingle$0(Lcom/squareup/picasso/RequestCreator;Lrx/SingleSubscriber;)V
    .locals 1

    .line 20
    new-instance v0, Lcom/squareup/util/picasso/PicassoHelpers$1;

    invoke-direct {v0, p1}, Lcom/squareup/util/picasso/PicassoHelpers$1;-><init>(Lrx/SingleSubscriber;)V

    invoke-virtual {p0, v0}, Lcom/squareup/picasso/RequestCreator;->into(Lcom/squareup/picasso/Target;)V

    return-void
.end method

.method public static toSingle(Lcom/squareup/picasso/RequestCreator;Lrx/Scheduler;)Lrx/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/picasso/RequestCreator;",
            "Lrx/Scheduler;",
            ")",
            "Lrx/Single<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .line 19
    new-instance v0, Lcom/squareup/util/picasso/-$$Lambda$PicassoHelpers$SqlBl7NOGC8scJ7z40zOrWxfOgw;

    invoke-direct {v0, p0}, Lcom/squareup/util/picasso/-$$Lambda$PicassoHelpers$SqlBl7NOGC8scJ7z40zOrWxfOgw;-><init>(Lcom/squareup/picasso/RequestCreator;)V

    invoke-static {v0}, Lrx/Single;->create(Lrx/Single$OnSubscribe;)Lrx/Single;

    move-result-object p0

    .line 37
    invoke-virtual {p0, p1}, Lrx/Single;->subscribeOn(Lrx/Scheduler;)Lrx/Single;

    move-result-object p0

    return-object p0
.end method

.method public static twoTone()Ljava/lang/String;
    .locals 1

    const-string v0, "twotone()"

    return-object v0
.end method
