.class public final Lcom/squareup/util/ScreenParameters;
.super Ljava/lang/Object;
.source "ScreenParameters.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u001a\u0010\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000c\u001a\u00020\u0007H\u0002\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082\u000e\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u000e\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0004\u001a\u00020\u0003X\u0082\u000e\u00a2\u0006\u0002\n\u0000\"\u0015\u0010\u0005\u001a\u00020\u0006*\u00020\u00078F\u00a2\u0006\u0006\u001a\u0004\u0008\u0008\u0010\t\u00a8\u0006\r"
    }
    d2 = {
        "generatedInPortait",
        "",
        "screenHeight",
        "",
        "screenWidth",
        "screenDimens",
        "Landroid/graphics/Point;",
        "Landroid/content/Context;",
        "getScreenDimens",
        "(Landroid/content/Context;)Landroid/graphics/Point;",
        "initDimens",
        "",
        "context",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static generatedInPortait:Z = false

.field private static screenHeight:I = -0x1

.field private static screenWidth:I = -0x1


# direct methods
.method static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public static final getScreenDimens(Landroid/content/Context;)Landroid/graphics/Point;
    .locals 2

    const-string v0, "$this$screenDimens"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    sget v0, Lcom/squareup/util/ScreenParameters;->screenWidth:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    sget v0, Lcom/squareup/util/ScreenParameters;->screenHeight:I

    if-ne v0, v1, :cond_1

    .line 24
    :cond_0
    invoke-static {p0}, Lcom/squareup/util/ScreenParameters;->initDimens(Landroid/content/Context;)V

    .line 27
    :cond_1
    sget-boolean v0, Lcom/squareup/util/ScreenParameters;->generatedInPortait:Z

    invoke-static {p0}, Lcom/squareup/util/Views;->isPortrait(Landroid/content/Context;)Z

    move-result p0

    if-ne v0, p0, :cond_2

    .line 28
    new-instance p0, Landroid/graphics/Point;

    sget v0, Lcom/squareup/util/ScreenParameters;->screenWidth:I

    sget v1, Lcom/squareup/util/ScreenParameters;->screenHeight:I

    invoke-direct {p0, v0, v1}, Landroid/graphics/Point;-><init>(II)V

    goto :goto_0

    .line 30
    :cond_2
    new-instance p0, Landroid/graphics/Point;

    sget v0, Lcom/squareup/util/ScreenParameters;->screenHeight:I

    sget v1, Lcom/squareup/util/ScreenParameters;->screenWidth:I

    invoke-direct {p0, v0, v1}, Landroid/graphics/Point;-><init>(II)V

    :goto_0
    return-object p0
.end method

.method private static final initDimens(Landroid/content/Context;)V
    .locals 2

    const-string v0, "window"

    .line 35
    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Landroid/view/WindowManager;

    .line 36
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 37
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 38
    invoke-virtual {v0, v1}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    .line 39
    iget v0, v1, Landroid/graphics/Point;->x:I

    sput v0, Lcom/squareup/util/ScreenParameters;->screenWidth:I

    .line 40
    iget v0, v1, Landroid/graphics/Point;->y:I

    sput v0, Lcom/squareup/util/ScreenParameters;->screenHeight:I

    .line 41
    invoke-static {p0}, Lcom/squareup/util/Views;->isPortrait(Landroid/content/Context;)Z

    move-result p0

    sput-boolean p0, Lcom/squareup/util/ScreenParameters;->generatedInPortait:Z

    return-void

    .line 35
    :cond_0
    new-instance p0, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type android.view.WindowManager"

    invoke-direct {p0, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0
.end method
