.class final Lcom/squareup/util/OptionalExtensionsRx1Kt$mapIfPresent$1;
.super Ljava/lang/Object;
.source "OptionalExtensionsRx1.kt"

# interfaces
.implements Lrx/functions/Func1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/util/OptionalExtensionsRx1Kt;->mapIfPresent(Lrx/Observable;)Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func1<",
        "TT;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0008\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a\u0002H\u0001\"\u0008\u0008\u0000\u0010\u0001*\u00020\u00022\u001a\u0010\u0003\u001a\u0016\u0012\u0004\u0012\u0002H\u0001 \u0005*\n\u0012\u0004\u0012\u0002H\u0001\u0018\u00010\u00040\u0004H\n\u00a2\u0006\u0004\u0008\u0006\u0010\u0007"
    }
    d2 = {
        "<anonymous>",
        "T",
        "",
        "it",
        "Lcom/squareup/util/Optional$Present;",
        "kotlin.jvm.PlatformType",
        "call",
        "(Lcom/squareup/util/Optional$Present;)Ljava/lang/Object;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/util/OptionalExtensionsRx1Kt$mapIfPresent$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/util/OptionalExtensionsRx1Kt$mapIfPresent$1;

    invoke-direct {v0}, Lcom/squareup/util/OptionalExtensionsRx1Kt$mapIfPresent$1;-><init>()V

    sput-object v0, Lcom/squareup/util/OptionalExtensionsRx1Kt$mapIfPresent$1;->INSTANCE:Lcom/squareup/util/OptionalExtensionsRx1Kt$mapIfPresent$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/squareup/util/Optional$Present;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Optional$Present<",
            "TT;>;)TT;"
        }
    .end annotation

    .line 11
    invoke-virtual {p1}, Lcom/squareup/util/Optional$Present;->getValue()Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p1, Lcom/squareup/util/Optional$Present;

    invoke-virtual {p0, p1}, Lcom/squareup/util/OptionalExtensionsRx1Kt$mapIfPresent$1;->call(Lcom/squareup/util/Optional$Present;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method
