.class public Lcom/squareup/util/TaxBreakdown$IndividualTaxBreakdown;
.super Ljava/lang/Object;
.source "TaxBreakdown.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/util/TaxBreakdown;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "IndividualTaxBreakdown"
.end annotation


# instance fields
.field public final afterApplicationTotalAmount:Lcom/squareup/protos/common/Money;

.field public final appliedAmount:Lcom/squareup/protos/common/Money;

.field public final appliedToAmount:Lcom/squareup/protos/common/Money;

.field public final label:Ljava/lang/String;

.field public final taxId:Ljava/lang/String;

.field public final taxPercentage:Lcom/squareup/util/Percentage;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/util/Percentage;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)V
    .locals 0

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object p1, p0, Lcom/squareup/util/TaxBreakdown$IndividualTaxBreakdown;->taxId:Ljava/lang/String;

    .line 57
    iput-object p2, p0, Lcom/squareup/util/TaxBreakdown$IndividualTaxBreakdown;->label:Ljava/lang/String;

    .line 58
    iput-object p3, p0, Lcom/squareup/util/TaxBreakdown$IndividualTaxBreakdown;->taxPercentage:Lcom/squareup/util/Percentage;

    .line 59
    iput-object p4, p0, Lcom/squareup/util/TaxBreakdown$IndividualTaxBreakdown;->appliedToAmount:Lcom/squareup/protos/common/Money;

    .line 60
    iput-object p5, p0, Lcom/squareup/util/TaxBreakdown$IndividualTaxBreakdown;->appliedAmount:Lcom/squareup/protos/common/Money;

    .line 61
    iput-object p6, p0, Lcom/squareup/util/TaxBreakdown$IndividualTaxBreakdown;->afterApplicationTotalAmount:Lcom/squareup/protos/common/Money;

    return-void
.end method
