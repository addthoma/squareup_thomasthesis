.class public Lcom/squareup/util/RxFiles;
.super Ljava/lang/Object;
.source "RxFiles.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/util/RxFiles$RxFileObserver;
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 2

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Should not instantiate instance of RxFiles"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public static watch(Ljava/io/File;)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Lrx/Observable<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .line 28
    new-instance v0, Lcom/squareup/util/RxFiles$RxFileObserver;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/util/RxFiles$RxFileObserver;-><init>(Ljava/io/File;Lcom/squareup/util/RxFiles$1;)V

    invoke-static {v0}, Lcom/squareup/util/RxFiles$RxFileObserver;->access$100(Lcom/squareup/util/RxFiles$RxFileObserver;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method

.method public static watchForModification(Ljava/io/File;)Lrx/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Lrx/Observable<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .line 40
    new-instance v0, Lcom/squareup/util/RxFiles$RxFileObserver;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/util/RxFiles$RxFileObserver;-><init>(Ljava/io/File;Lcom/squareup/util/RxFiles$1;)V

    invoke-static {v0}, Lcom/squareup/util/RxFiles$RxFileObserver;->access$200(Lcom/squareup/util/RxFiles$RxFileObserver;)Lrx/Observable;

    move-result-object p0

    return-object p0
.end method
