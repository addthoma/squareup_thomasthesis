.class final Lcom/squareup/util/CountryChecker$Circle;
.super Ljava/lang/Object;
.source "CountryChecker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/util/CountryChecker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "Circle"
.end annotation


# instance fields
.field private final center:Lcom/squareup/util/CountryChecker$Point;

.field private final radiusSquared:D


# direct methods
.method constructor <init>(Lcom/squareup/util/CountryChecker$Point;D)V
    .locals 0

    .line 258
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 259
    iput-object p1, p0, Lcom/squareup/util/CountryChecker$Circle;->center:Lcom/squareup/util/CountryChecker$Point;

    mul-double p2, p2, p2

    .line 260
    iput-wide p2, p0, Lcom/squareup/util/CountryChecker$Circle;->radiusSquared:D

    return-void
.end method

.method static synthetic access$600(Lcom/squareup/util/CountryChecker$Circle;)Lcom/squareup/util/CountryChecker$Point;
    .locals 0

    .line 254
    iget-object p0, p0, Lcom/squareup/util/CountryChecker$Circle;->center:Lcom/squareup/util/CountryChecker$Point;

    return-object p0
.end method

.method static synthetic access$700(Lcom/squareup/util/CountryChecker$Circle;)D
    .locals 2

    .line 254
    iget-wide v0, p0, Lcom/squareup/util/CountryChecker$Circle;->radiusSquared:D

    return-wide v0
.end method
