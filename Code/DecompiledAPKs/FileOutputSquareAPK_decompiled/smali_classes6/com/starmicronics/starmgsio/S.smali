.class Lcom/starmicronics/starmgsio/S;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Landroid/content/BroadcastReceiver;",
            ">;"
        }
    .end annotation
.end field

.field private static b:J


# instance fields
.field private c:Landroid/content/Context;

.field private d:J

.field private e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/starmicronics/starmgsio/S;->a:Ljava/util/HashMap;

    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/starmicronics/starmgsio/S;->b:J

    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/16 v0, 0x1388

    invoke-direct {p0, p1, v0}, Lcom/starmicronics/starmgsio/S;-><init>(Landroid/content/Context;I)V

    return-void
.end method

.method constructor <init>(Landroid/content/Context;I)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/starmicronics/starmgsio/S;->e:Z

    iput-object p1, p0, Lcom/starmicronics/starmgsio/S;->c:Landroid/content/Context;

    int-to-long p1, p2

    iput-wide p1, p0, Lcom/starmicronics/starmgsio/S;->d:J

    iget-wide p1, p0, Lcom/starmicronics/starmgsio/S;->d:J

    const-wide/16 v0, 0x0

    cmp-long v2, p1, v0

    if-gez v2, :cond_0

    iput-wide v0, p0, Lcom/starmicronics/starmgsio/S;->d:J

    :cond_0
    return-void
.end method


# virtual methods
.method a()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/starmicronics/starmgsio/S;->e:Z

    return-void
.end method

.method a(Landroid/hardware/usb/UsbDevice;)Z
    .locals 13

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/starmicronics/starmgsio/S;->e:Z

    iget-object v1, p0, Lcom/starmicronics/starmgsio/S;->c:Landroid/content/Context;

    const-string v2, "usb"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/usb/UsbManager;

    if-nez v1, :cond_0

    return v0

    :cond_0
    if-eqz p1, :cond_7

    invoke-virtual {v1, p1}, Landroid/hardware/usb/UsbManager;->hasPermission(Landroid/hardware/usb/UsbDevice;)Z

    move-result v2

    if-nez v2, :cond_7

    sget-object v2, Lcom/starmicronics/starmgsio/S;->a:Ljava/util/HashMap;

    monitor-enter v2

    :try_start_0
    iget-object v3, p0, Lcom/starmicronics/starmgsio/S;->c:Landroid/content/Context;

    new-instance v4, Landroid/content/Intent;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const-string v5, "com.starmicronics.starmgsio.USB_PERMISSION"

    :try_start_1
    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {v3, v0, v4, v0}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    new-instance v4, Landroid/content/IntentFilter;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    const-string v5, "com.starmicronics.starmgsio.USB_PERMISSION"

    :try_start_2
    invoke-direct {v4, v5}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    sget-object v4, Lcom/starmicronics/starmgsio/S;->a:Ljava/util/HashMap;

    invoke-virtual {p1}, Landroid/hardware/usb/UsbDevice;->getDeviceName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    const-wide/16 v5, 0x0

    const-wide/16 v7, 0xbb8

    if-nez v4, :cond_1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    sget-wide v11, Lcom/starmicronics/starmgsio/S;->b:J

    sub-long/2addr v9, v11

    cmp-long v4, v7, v9

    if-gez v4, :cond_3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    sput-wide v7, Lcom/starmicronics/starmgsio/S;->b:J

    invoke-virtual {v1, p1, v3}, Landroid/hardware/usb/UsbManager;->requestPermission(Landroid/hardware/usb/UsbDevice;Landroid/app/PendingIntent;)V

    goto :goto_1

    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    sget-wide v9, Lcom/starmicronics/starmgsio/S;->b:J

    sub-long/2addr v3, v9

    cmp-long v0, v7, v3

    if-gez v0, :cond_3

    sput-wide v5, Lcom/starmicronics/starmgsio/S;->b:J

    sget-object v0, Lcom/starmicronics/starmgsio/S;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/BroadcastReceiver;

    iget-object v4, p0, Lcom/starmicronics/starmgsio/S;->c:Landroid/content/Context;

    invoke-virtual {v4, v3}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/starmicronics/starmgsio/S;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    :cond_3
    const/4 v0, 0x1

    :goto_1
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    :cond_4
    invoke-virtual {v1, p1}, Landroid/hardware/usb/UsbManager;->hasPermission(Landroid/hardware/usb/UsbDevice;)Z

    move-result v4

    if-nez v4, :cond_7

    if-nez v0, :cond_7

    const-wide/16 v7, 0x64

    :try_start_3
    invoke-static {v7, v8}, Ljava/lang/Thread;->sleep(J)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_2

    :catch_0
    nop

    :goto_2
    iget-boolean v4, p0, Lcom/starmicronics/starmgsio/S;->e:Z

    if-nez v4, :cond_5

    invoke-virtual {v1, p1}, Landroid/hardware/usb/UsbManager;->hasPermission(Landroid/hardware/usb/UsbDevice;)Z

    move-result v4

    if-nez v4, :cond_5

    iget-wide v7, p0, Lcom/starmicronics/starmgsio/S;->d:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    sub-long/2addr v9, v2

    cmp-long v4, v7, v9

    if-gez v4, :cond_4

    :cond_5
    invoke-virtual {v1, p1}, Landroid/hardware/usb/UsbManager;->hasPermission(Landroid/hardware/usb/UsbDevice;)Z

    move-result v0

    if-eqz v0, :cond_6

    sput-wide v5, Lcom/starmicronics/starmgsio/S;->b:J

    :cond_6
    sget-object v0, Lcom/starmicronics/starmgsio/S;->a:Ljava/util/HashMap;

    monitor-enter v0

    :try_start_4
    sget-object v2, Lcom/starmicronics/starmgsio/S;->a:Ljava/util/HashMap;

    invoke-virtual {p1}, Landroid/hardware/usb/UsbDevice;->getDeviceName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v0

    goto :goto_3

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw p1

    :catchall_1
    move-exception p1

    :try_start_5
    monitor-exit v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw p1

    :cond_7
    :goto_3
    invoke-virtual {v1, p1}, Landroid/hardware/usb/UsbManager;->hasPermission(Landroid/hardware/usb/UsbDevice;)Z

    move-result p1

    return p1
.end method
