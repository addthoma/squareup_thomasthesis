.class Lcom/starmicronics/starmgsio/o$a;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/starmicronics/starmgsio/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "a"
.end annotation


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/starmicronics/starmgsio/o;",
            ">;"
        }
    .end annotation
.end field

.field private b:[B


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/starmicronics/starmgsio/o$a;->a:Ljava/util/List;

    const/4 v0, 0x0

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/starmicronics/starmgsio/o$a;->b:[B

    return-void
.end method

.method private a([BI)Lcom/starmicronics/starmgsio/o;
    .locals 4

    const/4 v0, 0x0

    if-eqz p1, :cond_5

    if-ltz p2, :cond_5

    array-length v1, p1

    if-ge v1, p2, :cond_0

    goto :goto_0

    :cond_0
    array-length v1, p1

    sub-int/2addr v1, p2

    const/16 v2, 0x1a

    if-ge v1, v2, :cond_1

    return-object v0

    :cond_1
    new-array v1, v2, [B

    array-length v2, v1

    const/4 v3, 0x0

    invoke-static {p1, p2, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const/4 p1, 0x2

    aget-byte p1, v1, p1

    const/16 p2, 0x20

    if-eq p1, p2, :cond_2

    return-object v0

    :cond_2
    new-instance p1, Lcom/starmicronics/starmgsio/o;

    invoke-direct {p1}, Lcom/starmicronics/starmgsio/o;-><init>()V

    invoke-virtual {p1, v1}, Lcom/starmicronics/starmgsio/o;->a([B)V

    invoke-direct {p0, v1}, Lcom/starmicronics/starmgsio/o$a;->b([B)Lcom/starmicronics/starmgsio/ScaleData$Status;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/starmicronics/starmgsio/o;->a(Lcom/starmicronics/starmgsio/ScaleData$Status;)V

    invoke-virtual {p1}, Lcom/starmicronics/starmgsio/o;->getStatus()Lcom/starmicronics/starmgsio/ScaleData$Status;

    move-result-object p2

    sget-object v2, Lcom/starmicronics/starmgsio/ScaleData$Status;->ERROR:Lcom/starmicronics/starmgsio/ScaleData$Status;

    if-ne p2, v2, :cond_3

    return-object p1

    :cond_3
    invoke-direct {p0, v1}, Lcom/starmicronics/starmgsio/o$a;->c([B)Lcom/starmicronics/starmgsio/ScaleData$ComparatorResult;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/starmicronics/starmgsio/o;->a(Lcom/starmicronics/starmgsio/ScaleData$ComparatorResult;)V

    invoke-direct {p0, v1}, Lcom/starmicronics/starmgsio/o$a;->d([B)Lcom/starmicronics/starmgsio/ScaleData$DataType;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/starmicronics/starmgsio/o;->a(Lcom/starmicronics/starmgsio/ScaleData$DataType;)V

    invoke-direct {p0, v1}, Lcom/starmicronics/starmgsio/o$a;->e([B)Lcom/starmicronics/starmgsio/K;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/starmicronics/starmgsio/o;->a(Lcom/starmicronics/starmgsio/K;)V

    invoke-direct {p0, v1}, Lcom/starmicronics/starmgsio/o$a;->f([B)Lcom/starmicronics/starmgsio/ScaleData$Unit;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/starmicronics/starmgsio/o;->a(Lcom/starmicronics/starmgsio/ScaleData$Unit;)V

    invoke-virtual {p1}, Lcom/starmicronics/starmgsio/o;->a()Z

    move-result p2

    if-nez p2, :cond_4

    return-object v0

    :cond_4
    return-object p1

    :cond_5
    :goto_0
    return-object v0
.end method

.method private b([B)Lcom/starmicronics/starmgsio/ScaleData$Status;
    .locals 5

    array-length v0, p1

    const/16 v1, 0x1a

    if-ge v0, v1, :cond_0

    sget-object p1, Lcom/starmicronics/starmgsio/ScaleData$Status;->INVALID:Lcom/starmicronics/starmgsio/ScaleData$Status;

    return-object p1

    :cond_0
    sget-object v0, Lcom/starmicronics/starmgsio/ScaleData$Status;->INVALID:Lcom/starmicronics/starmgsio/ScaleData$Status;

    const/4 v2, 0x0

    aget-byte v3, p1, v2

    const/16 v4, 0x20

    if-ne v3, v4, :cond_1

    sget-object v0, Lcom/starmicronics/starmgsio/ScaleData$Status;->STABLE:Lcom/starmicronics/starmgsio/ScaleData$Status;

    goto :goto_0

    :cond_1
    new-array v1, v1, [B

    fill-array-data v1, :array_0

    invoke-static {p1, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v0, Lcom/starmicronics/starmgsio/ScaleData$Status;->ERROR:Lcom/starmicronics/starmgsio/ScaleData$Status;

    goto :goto_0

    :cond_2
    aget-byte p1, p1, v2

    const/16 v1, 0x2a

    if-ne p1, v1, :cond_3

    sget-object v0, Lcom/starmicronics/starmgsio/ScaleData$Status;->UNSTABLE:Lcom/starmicronics/starmgsio/ScaleData$Status;

    :cond_3
    :goto_0
    return-object v0

    nop

    :array_0
    .array-data 1
        0x2at
        0x2at
        0x20t
        0x45t
        0x52t
        0x52t
        0x4ft
        0x52t
        0x20t
        0x2at
        0x2at
        0x2at
        0x2at
        0x2at
        0x2at
        0x2at
        0x2at
        0x2at
        0x2at
        0x2at
        0x2at
        0x2at
        0x2at
        0x20t
        0xdt
        0xat
    .end array-data
.end method

.method private c([B)Lcom/starmicronics/starmgsio/ScaleData$ComparatorResult;
    .locals 4

    array-length v0, p1

    const/16 v1, 0x1a

    if-ge v0, v1, :cond_0

    sget-object p1, Lcom/starmicronics/starmgsio/ScaleData$ComparatorResult;->INVALID:Lcom/starmicronics/starmgsio/ScaleData$ComparatorResult;

    return-object p1

    :cond_0
    sget-object v0, Lcom/starmicronics/starmgsio/ScaleData$ComparatorResult;->INVALID:Lcom/starmicronics/starmgsio/ScaleData$ComparatorResult;

    const/4 v1, 0x1

    aget-byte v2, p1, v1

    const/16 v3, 0x20

    if-ne v2, v3, :cond_1

    sget-object v0, Lcom/starmicronics/starmgsio/ScaleData$ComparatorResult;->OK:Lcom/starmicronics/starmgsio/ScaleData$ComparatorResult;

    goto :goto_0

    :cond_1
    aget-byte v2, p1, v1

    const/16 v3, 0x48

    if-ne v2, v3, :cond_2

    sget-object v0, Lcom/starmicronics/starmgsio/ScaleData$ComparatorResult;->OVER:Lcom/starmicronics/starmgsio/ScaleData$ComparatorResult;

    goto :goto_0

    :cond_2
    aget-byte p1, p1, v1

    const/16 v1, 0x4c

    if-ne p1, v1, :cond_3

    sget-object v0, Lcom/starmicronics/starmgsio/ScaleData$ComparatorResult;->SHORTAGE:Lcom/starmicronics/starmgsio/ScaleData$ComparatorResult;

    :cond_3
    :goto_0
    return-object v0
.end method

.method private d([B)Lcom/starmicronics/starmgsio/ScaleData$DataType;
    .locals 4

    array-length v0, p1

    const/16 v1, 0x1a

    if-ge v0, v1, :cond_0

    sget-object p1, Lcom/starmicronics/starmgsio/ScaleData$DataType;->INVALID:Lcom/starmicronics/starmgsio/ScaleData$DataType;

    return-object p1

    :cond_0
    sget-object v0, Lcom/starmicronics/starmgsio/ScaleData$DataType;->INVALID:Lcom/starmicronics/starmgsio/ScaleData$DataType;

    const/4 v1, 0x6

    new-array v2, v1, [B

    fill-array-data v2, :array_0

    const/4 v3, 0x3

    invoke-static {p1, v3, v2}, Lcom/starmicronics/starmgsio/U;->a([BI[B)Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object v0, Lcom/starmicronics/starmgsio/ScaleData$DataType;->NET_NOT_TARED:Lcom/starmicronics/starmgsio/ScaleData$DataType;

    goto :goto_0

    :cond_1
    new-array v2, v1, [B

    fill-array-data v2, :array_1

    invoke-static {p1, v3, v2}, Lcom/starmicronics/starmgsio/U;->a([BI[B)Z

    move-result v2

    if-eqz v2, :cond_2

    sget-object v0, Lcom/starmicronics/starmgsio/ScaleData$DataType;->NET:Lcom/starmicronics/starmgsio/ScaleData$DataType;

    goto :goto_0

    :cond_2
    new-array v2, v1, [B

    fill-array-data v2, :array_2

    invoke-static {p1, v3, v2}, Lcom/starmicronics/starmgsio/U;->a([BI[B)Z

    move-result v2

    if-eqz v2, :cond_3

    sget-object v0, Lcom/starmicronics/starmgsio/ScaleData$DataType;->PRESET_TARE:Lcom/starmicronics/starmgsio/ScaleData$DataType;

    goto :goto_0

    :cond_3
    new-array v2, v1, [B

    fill-array-data v2, :array_3

    invoke-static {p1, v3, v2}, Lcom/starmicronics/starmgsio/U;->a([BI[B)Z

    move-result v2

    if-eqz v2, :cond_4

    sget-object v0, Lcom/starmicronics/starmgsio/ScaleData$DataType;->TARE:Lcom/starmicronics/starmgsio/ScaleData$DataType;

    goto :goto_0

    :cond_4
    new-array v2, v1, [B

    fill-array-data v2, :array_4

    invoke-static {p1, v3, v2}, Lcom/starmicronics/starmgsio/U;->a([BI[B)Z

    move-result v2

    if-eqz v2, :cond_5

    sget-object v0, Lcom/starmicronics/starmgsio/ScaleData$DataType;->TOTAL:Lcom/starmicronics/starmgsio/ScaleData$DataType;

    goto :goto_0

    :cond_5
    new-array v2, v1, [B

    fill-array-data v2, :array_5

    invoke-static {p1, v3, v2}, Lcom/starmicronics/starmgsio/U;->a([BI[B)Z

    move-result v2

    if-eqz v2, :cond_6

    sget-object v0, Lcom/starmicronics/starmgsio/ScaleData$DataType;->GROSS:Lcom/starmicronics/starmgsio/ScaleData$DataType;

    goto :goto_0

    :cond_6
    new-array v1, v1, [B

    fill-array-data v1, :array_6

    invoke-static {p1, v3, v1}, Lcom/starmicronics/starmgsio/U;->a([BI[B)Z

    move-result p1

    if-eqz p1, :cond_7

    sget-object v0, Lcom/starmicronics/starmgsio/ScaleData$DataType;->UNIT:Lcom/starmicronics/starmgsio/ScaleData$DataType;

    :cond_7
    :goto_0
    return-object v0

    :array_0
    .array-data 1
        0x20t
        0x20t
        0x20t
        0x20t
        0x20t
        0x20t
    .end array-data

    nop

    :array_1
    .array-data 1
        0x4et
        0x20t
        0x20t
        0x20t
        0x20t
        0x20t
    .end array-data

    nop

    :array_2
    .array-data 1
        0x50t
        0x54t
        0x20t
        0x20t
        0x20t
        0x20t
    .end array-data

    nop

    :array_3
    .array-data 1
        0x54t
        0x20t
        0x20t
        0x20t
        0x20t
        0x20t
    .end array-data

    nop

    :array_4
    .array-data 1
        0x54t
        0x4ft
        0x54t
        0x41t
        0x4ct
        0x20t
    .end array-data

    nop

    :array_5
    .array-data 1
        0x47t
        0x20t
        0x20t
        0x20t
        0x20t
        0x20t
    .end array-data

    nop

    :array_6
    .array-data 1
        0x55t
        0x4et
        0x49t
        0x54t
        0x20t
        0x20t
    .end array-data
.end method

.method private e([B)Lcom/starmicronics/starmgsio/K;
    .locals 7

    new-instance v0, Lcom/starmicronics/starmgsio/K$a;

    invoke-direct {v0}, Lcom/starmicronics/starmgsio/K$a;-><init>()V

    array-length v1, p1

    const/16 v2, 0x1a

    if-ge v1, v2, :cond_0

    invoke-virtual {v0}, Lcom/starmicronics/starmgsio/K$a;->a()Lcom/starmicronics/starmgsio/K;

    move-result-object p1

    return-object p1

    :cond_0
    new-instance v1, Ljava/lang/String;

    const/16 v2, 0x9

    const/16 v3, 0x15

    invoke-static {p1, v2, v3}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object p1

    invoke-direct {v1, p1}, Ljava/lang/String;-><init>([B)V

    const-string p1, "-"

    invoke-virtual {v1, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    const-string v3, ""

    const-string v4, "+"

    invoke-virtual {v1, v4, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    const-string v1, " "

    invoke-virtual {p1, v1, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p1

    const/4 v1, 0x0

    const-string v3, "."

    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {p1, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    :cond_1
    :try_start_0
    invoke-static {p1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v2, :cond_2

    const-wide/high16 v5, -0x4010000000000000L    # -1.0

    mul-double v3, v3, v5

    :cond_2
    invoke-virtual {v0, v3, v4}, Lcom/starmicronics/starmgsio/K$a;->a(D)Lcom/starmicronics/starmgsio/K$a;

    invoke-virtual {v0, v1}, Lcom/starmicronics/starmgsio/K$a;->a(I)Lcom/starmicronics/starmgsio/K$a;

    invoke-virtual {v0}, Lcom/starmicronics/starmgsio/K$a;->a()Lcom/starmicronics/starmgsio/K;

    move-result-object p1

    return-object p1

    :catch_0
    invoke-virtual {v0}, Lcom/starmicronics/starmgsio/K$a;->a()Lcom/starmicronics/starmgsio/K;

    move-result-object p1

    return-object p1
.end method

.method private f([B)Lcom/starmicronics/starmgsio/ScaleData$Unit;
    .locals 5

    array-length v0, p1

    const/16 v1, 0x1a

    if-ge v0, v1, :cond_0

    sget-object p1, Lcom/starmicronics/starmgsio/ScaleData$Unit;->INVALID:Lcom/starmicronics/starmgsio/ScaleData$Unit;

    return-object p1

    :cond_0
    sget-object v0, Lcom/starmicronics/starmgsio/ScaleData$Unit;->INVALID:Lcom/starmicronics/starmgsio/ScaleData$Unit;

    const/4 v1, 0x2

    new-array v2, v1, [B

    fill-array-data v2, :array_0

    const/16 v3, 0x15

    invoke-static {p1, v3, v2}, Lcom/starmicronics/starmgsio/U;->a([BI[B)Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object p1, Lcom/starmicronics/starmgsio/ScaleData$Unit;->MG:Lcom/starmicronics/starmgsio/ScaleData$Unit;

    :goto_0
    move-object v0, p1

    goto/16 :goto_2

    :cond_1
    new-array v2, v1, [B

    fill-array-data v2, :array_1

    invoke-static {p1, v3, v2}, Lcom/starmicronics/starmgsio/U;->a([BI[B)Z

    move-result v2

    if-eqz v2, :cond_2

    sget-object p1, Lcom/starmicronics/starmgsio/ScaleData$Unit;->G:Lcom/starmicronics/starmgsio/ScaleData$Unit;

    goto :goto_0

    :cond_2
    new-array v2, v1, [B

    fill-array-data v2, :array_2

    invoke-static {p1, v3, v2}, Lcom/starmicronics/starmgsio/U;->a([BI[B)Z

    move-result v2

    if-nez v2, :cond_12

    new-array v2, v1, [B

    fill-array-data v2, :array_3

    invoke-static {p1, v3, v2}, Lcom/starmicronics/starmgsio/U;->a([BI[B)Z

    move-result v2

    if-eqz v2, :cond_3

    goto/16 :goto_1

    :cond_3
    new-array v2, v1, [B

    fill-array-data v2, :array_4

    invoke-static {p1, v3, v2}, Lcom/starmicronics/starmgsio/U;->a([BI[B)Z

    move-result v2

    if-eqz v2, :cond_4

    sget-object p1, Lcom/starmicronics/starmgsio/ScaleData$Unit;->MOM:Lcom/starmicronics/starmgsio/ScaleData$Unit;

    goto :goto_0

    :cond_4
    new-array v2, v1, [B

    fill-array-data v2, :array_5

    invoke-static {p1, v3, v2}, Lcom/starmicronics/starmgsio/U;->a([BI[B)Z

    move-result v2

    if-eqz v2, :cond_5

    sget-object p1, Lcom/starmicronics/starmgsio/ScaleData$Unit;->OZ:Lcom/starmicronics/starmgsio/ScaleData$Unit;

    goto :goto_0

    :cond_5
    new-array v2, v1, [B

    fill-array-data v2, :array_6

    invoke-static {p1, v3, v2}, Lcom/starmicronics/starmgsio/U;->a([BI[B)Z

    move-result v2

    if-eqz v2, :cond_6

    sget-object p1, Lcom/starmicronics/starmgsio/ScaleData$Unit;->LB:Lcom/starmicronics/starmgsio/ScaleData$Unit;

    goto :goto_0

    :cond_6
    new-array v2, v1, [B

    fill-array-data v2, :array_7

    invoke-static {p1, v3, v2}, Lcom/starmicronics/starmgsio/U;->a([BI[B)Z

    move-result v2

    if-eqz v2, :cond_7

    sget-object p1, Lcom/starmicronics/starmgsio/ScaleData$Unit;->OZT:Lcom/starmicronics/starmgsio/ScaleData$Unit;

    goto :goto_0

    :cond_7
    new-array v2, v1, [B

    fill-array-data v2, :array_8

    invoke-static {p1, v3, v2}, Lcom/starmicronics/starmgsio/U;->a([BI[B)Z

    move-result v2

    if-eqz v2, :cond_8

    sget-object p1, Lcom/starmicronics/starmgsio/ScaleData$Unit;->DWT:Lcom/starmicronics/starmgsio/ScaleData$Unit;

    goto :goto_0

    :cond_8
    new-array v2, v1, [B

    fill-array-data v2, :array_9

    invoke-static {p1, v3, v2}, Lcom/starmicronics/starmgsio/U;->a([BI[B)Z

    move-result v2

    if-eqz v2, :cond_9

    sget-object p1, Lcom/starmicronics/starmgsio/ScaleData$Unit;->GN:Lcom/starmicronics/starmgsio/ScaleData$Unit;

    goto :goto_0

    :cond_9
    new-array v2, v1, [B

    fill-array-data v2, :array_a

    invoke-static {p1, v3, v2}, Lcom/starmicronics/starmgsio/U;->a([BI[B)Z

    move-result v2

    if-eqz v2, :cond_a

    sget-object p1, Lcom/starmicronics/starmgsio/ScaleData$Unit;->TLH:Lcom/starmicronics/starmgsio/ScaleData$Unit;

    goto/16 :goto_0

    :cond_a
    new-array v2, v1, [B

    fill-array-data v2, :array_b

    invoke-static {p1, v3, v2}, Lcom/starmicronics/starmgsio/U;->a([BI[B)Z

    move-result v2

    if-eqz v2, :cond_b

    sget-object p1, Lcom/starmicronics/starmgsio/ScaleData$Unit;->TLS:Lcom/starmicronics/starmgsio/ScaleData$Unit;

    goto/16 :goto_0

    :cond_b
    new-array v2, v1, [B

    fill-array-data v2, :array_c

    invoke-static {p1, v3, v2}, Lcom/starmicronics/starmgsio/U;->a([BI[B)Z

    move-result v2

    if-eqz v2, :cond_c

    sget-object p1, Lcom/starmicronics/starmgsio/ScaleData$Unit;->TLT:Lcom/starmicronics/starmgsio/ScaleData$Unit;

    goto/16 :goto_0

    :cond_c
    new-array v2, v1, [B

    fill-array-data v2, :array_d

    const/4 v4, 0x1

    invoke-static {p1, v4, v2}, Lcom/starmicronics/starmgsio/U;->a([BI[B)Z

    move-result v2

    if-eqz v2, :cond_d

    sget-object p1, Lcom/starmicronics/starmgsio/ScaleData$Unit;->TO:Lcom/starmicronics/starmgsio/ScaleData$Unit;

    goto/16 :goto_0

    :cond_d
    new-array v2, v1, [B

    fill-array-data v2, :array_e

    invoke-static {p1, v3, v2}, Lcom/starmicronics/starmgsio/U;->a([BI[B)Z

    move-result v2

    if-eqz v2, :cond_e

    sget-object p1, Lcom/starmicronics/starmgsio/ScaleData$Unit;->MSG:Lcom/starmicronics/starmgsio/ScaleData$Unit;

    goto/16 :goto_0

    :cond_e
    new-array v2, v1, [B

    fill-array-data v2, :array_f

    invoke-static {p1, v3, v2}, Lcom/starmicronics/starmgsio/U;->a([BI[B)Z

    move-result v2

    if-eqz v2, :cond_f

    sget-object p1, Lcom/starmicronics/starmgsio/ScaleData$Unit;->BAT:Lcom/starmicronics/starmgsio/ScaleData$Unit;

    goto/16 :goto_0

    :cond_f
    new-array v2, v1, [B

    fill-array-data v2, :array_10

    invoke-static {p1, v3, v2}, Lcom/starmicronics/starmgsio/U;->a([BI[B)Z

    move-result v2

    if-eqz v2, :cond_10

    sget-object p1, Lcom/starmicronics/starmgsio/ScaleData$Unit;->PCS:Lcom/starmicronics/starmgsio/ScaleData$Unit;

    goto/16 :goto_0

    :cond_10
    new-array v2, v1, [B

    fill-array-data v2, :array_11

    invoke-static {p1, v3, v2}, Lcom/starmicronics/starmgsio/U;->a([BI[B)Z

    move-result v2

    if-eqz v2, :cond_11

    sget-object p1, Lcom/starmicronics/starmgsio/ScaleData$Unit;->PERCENT:Lcom/starmicronics/starmgsio/ScaleData$Unit;

    goto/16 :goto_0

    :cond_11
    new-array v1, v1, [B

    fill-array-data v1, :array_12

    invoke-static {p1, v3, v1}, Lcom/starmicronics/starmgsio/U;->a([BI[B)Z

    move-result p1

    if-eqz p1, :cond_13

    sget-object p1, Lcom/starmicronics/starmgsio/ScaleData$Unit;->COEFFICIENT:Lcom/starmicronics/starmgsio/ScaleData$Unit;

    goto/16 :goto_0

    :cond_12
    :goto_1
    sget-object p1, Lcom/starmicronics/starmgsio/ScaleData$Unit;->CT:Lcom/starmicronics/starmgsio/ScaleData$Unit;

    goto/16 :goto_0

    :cond_13
    :goto_2
    return-object v0

    :array_0
    .array-data 1
        0x6dt
        0x67t
    .end array-data

    nop

    :array_1
    .array-data 1
        0x20t
        0x67t
    .end array-data

    nop

    :array_2
    .array-data 1
        0x63t
        0x74t
    .end array-data

    nop

    :array_3
    .array-data 1
        0x20t
        0x63t
    .end array-data

    nop

    :array_4
    .array-data 1
        0x6dt
        0x6ft
    .end array-data

    nop

    :array_5
    .array-data 1
        0x6ft
        0x7at
    .end array-data

    nop

    :array_6
    .array-data 1
        0x6ct
        0x62t
    .end array-data

    nop

    :array_7
    .array-data 1
        0x4ft
        0x54t
    .end array-data

    nop

    :array_8
    .array-data 1
        0x64t
        0x77t
    .end array-data

    nop

    :array_9
    .array-data 1
        0x47t
        0x52t
    .end array-data

    nop

    :array_a
    .array-data 1
        0x74t
        0x6ct
    .end array-data

    nop

    :array_b
    .array-data 1
        0x74t
        0x6ct
    .end array-data

    nop

    :array_c
    .array-data 1
        0x74t
        0x6ct
    .end array-data

    nop

    :array_d
    .array-data 1
        0x74t
        0x6ft
    .end array-data

    nop

    :array_e
    .array-data 1
        0x4dt
        0x53t
    .end array-data

    nop

    :array_f
    .array-data 1
        0x42t
        0x41t
    .end array-data

    nop

    :array_10
    .array-data 1
        0x50t
        0x43t
    .end array-data

    nop

    :array_11
    .array-data 1
        0x20t
        0x25t
    .end array-data

    nop

    :array_12
    .array-data 1
        0x20t
        0x23t
    .end array-data
.end method


# virtual methods
.method a()Lcom/starmicronics/starmgsio/o;
    .locals 2

    iget-object v0, p0, Lcom/starmicronics/starmgsio/o$a;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/starmicronics/starmgsio/o$a;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/starmicronics/starmgsio/o;

    return-object v0
.end method

.method a([B)V
    .locals 3

    iget-object v0, p0, Lcom/starmicronics/starmgsio/o$a;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    const/4 v0, 0x0

    new-array v1, v0, [B

    iput-object v1, p0, Lcom/starmicronics/starmgsio/o$a;->b:[B

    array-length v1, p1

    const/16 v2, 0x1a

    if-ge v1, v2, :cond_0

    iput-object p1, p0, Lcom/starmicronics/starmgsio/o$a;->b:[B

    return-void

    :cond_0
    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_2

    invoke-direct {p0, p1, v0}, Lcom/starmicronics/starmgsio/o$a;->a([BI)Lcom/starmicronics/starmgsio/o;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v2, p0, Lcom/starmicronics/starmgsio/o$a;->a:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1a

    goto :goto_1

    :cond_1
    iget-object v1, p0, Lcom/starmicronics/starmgsio/o$a;->b:[B

    aget-byte v2, p1, v0

    invoke-static {v1, v2}, Lcom/starmicronics/starmgsio/U;->a([BB)[B

    move-result-object v1

    iput-object v1, p0, Lcom/starmicronics/starmgsio/o$a;->b:[B

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method b()[B
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starmgsio/o$a;->b:[B

    return-object v0
.end method
