.class Lcom/starmicronics/starmgsio/u;
.super Lcom/starmicronics/starmgsio/Scale;
.source ""


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/starmicronics/starmgsio/u$b;,
        Lcom/starmicronics/starmgsio/u$a;
    }
.end annotation


# instance fields
.field private a:Z

.field private b:Lcom/starmicronics/starmgsio/ScaleCallback;

.field private c:Lcom/starmicronics/starmgsio/n;

.field private d:Lcom/starmicronics/starmgsio/n;

.field private final e:Lcom/starmicronics/starmgsio/p;

.field private f:Lcom/starmicronics/starmgsio/p$a;


# direct methods
.method constructor <init>(Lcom/starmicronics/starmgsio/p;)V
    .locals 1

    invoke-direct {p0}, Lcom/starmicronics/starmgsio/Scale;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/starmicronics/starmgsio/u;->a:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/starmicronics/starmgsio/u;->b:Lcom/starmicronics/starmgsio/ScaleCallback;

    iput-object v0, p0, Lcom/starmicronics/starmgsio/u;->c:Lcom/starmicronics/starmgsio/n;

    iput-object v0, p0, Lcom/starmicronics/starmgsio/u;->d:Lcom/starmicronics/starmgsio/n;

    new-instance v0, Lcom/starmicronics/starmgsio/r;

    invoke-direct {v0, p0}, Lcom/starmicronics/starmgsio/r;-><init>(Lcom/starmicronics/starmgsio/u;)V

    iput-object v0, p0, Lcom/starmicronics/starmgsio/u;->f:Lcom/starmicronics/starmgsio/p$a;

    iput-object p1, p0, Lcom/starmicronics/starmgsio/u;->e:Lcom/starmicronics/starmgsio/p;

    return-void
.end method

.method static synthetic a(Lcom/starmicronics/starmgsio/u;)Lcom/starmicronics/starmgsio/p;
    .locals 0

    iget-object p0, p0, Lcom/starmicronics/starmgsio/u;->e:Lcom/starmicronics/starmgsio/p;

    return-object p0
.end method

.method private a()V
    .locals 1

    iget-object v0, p0, Lcom/starmicronics/starmgsio/u;->c:Lcom/starmicronics/starmgsio/n;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/starmicronics/starmgsio/n;->a()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/starmicronics/starmgsio/u;->c:Lcom/starmicronics/starmgsio/n;

    :cond_0
    new-instance v0, Lcom/starmicronics/starmgsio/u$a;

    invoke-direct {v0, p0}, Lcom/starmicronics/starmgsio/u$a;-><init>(Lcom/starmicronics/starmgsio/u;)V

    iput-object v0, p0, Lcom/starmicronics/starmgsio/u;->c:Lcom/starmicronics/starmgsio/n;

    iget-object v0, p0, Lcom/starmicronics/starmgsio/u;->c:Lcom/starmicronics/starmgsio/n;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method static synthetic a(Lcom/starmicronics/starmgsio/u;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/starmicronics/starmgsio/u;->a:Z

    return p1
.end method

.method static synthetic b(Lcom/starmicronics/starmgsio/u;)Lcom/starmicronics/starmgsio/ScaleCallback;
    .locals 0

    iget-object p0, p0, Lcom/starmicronics/starmgsio/u;->b:Lcom/starmicronics/starmgsio/ScaleCallback;

    return-object p0
.end method

.method static synthetic c(Lcom/starmicronics/starmgsio/u;)V
    .locals 0

    invoke-direct {p0}, Lcom/starmicronics/starmgsio/u;->a()V

    return-void
.end method


# virtual methods
.method public connect(Lcom/starmicronics/starmgsio/ScaleCallback;)V
    .locals 2

    iget-boolean v0, p0, Lcom/starmicronics/starmgsio/u;->a:Z

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/starmicronics/starmgsio/u;->f:Lcom/starmicronics/starmgsio/p$a;

    iget-object v0, p0, Lcom/starmicronics/starmgsio/u;->e:Lcom/starmicronics/starmgsio/p;

    sget-object v1, Lcom/starmicronics/starmgsio/p$b;->b:Lcom/starmicronics/starmgsio/p$b;

    invoke-interface {p1, v0, v1}, Lcom/starmicronics/starmgsio/p$a;->a(Lcom/starmicronics/starmgsio/p;Lcom/starmicronics/starmgsio/p$b;)V

    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/starmicronics/starmgsio/u;->a:Z

    iput-object p1, p0, Lcom/starmicronics/starmgsio/u;->b:Lcom/starmicronics/starmgsio/ScaleCallback;

    iget-object p1, p0, Lcom/starmicronics/starmgsio/u;->e:Lcom/starmicronics/starmgsio/p;

    iget-object v0, p0, Lcom/starmicronics/starmgsio/u;->f:Lcom/starmicronics/starmgsio/p$a;

    invoke-interface {p1, v0}, Lcom/starmicronics/starmgsio/p;->a(Lcom/starmicronics/starmgsio/p$a;)V

    return-void
.end method

.method public disconnect()V
    .locals 2

    iget-object v0, p0, Lcom/starmicronics/starmgsio/u;->c:Lcom/starmicronics/starmgsio/n;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/starmicronics/starmgsio/n;->a()V

    iput-object v1, p0, Lcom/starmicronics/starmgsio/u;->c:Lcom/starmicronics/starmgsio/n;

    :cond_0
    iget-object v0, p0, Lcom/starmicronics/starmgsio/u;->d:Lcom/starmicronics/starmgsio/n;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/starmicronics/starmgsio/n;->a()V

    iput-object v1, p0, Lcom/starmicronics/starmgsio/u;->d:Lcom/starmicronics/starmgsio/n;

    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/starmicronics/starmgsio/u;->a:Z

    iget-object v0, p0, Lcom/starmicronics/starmgsio/u;->e:Lcom/starmicronics/starmgsio/p;

    invoke-interface {v0}, Lcom/starmicronics/starmgsio/p;->a()V

    return-void
.end method

.method public updateSetting(Lcom/starmicronics/starmgsio/ScaleSetting;)V
    .locals 2

    :goto_0
    iget-object v0, p0, Lcom/starmicronics/starmgsio/u;->d:Lcom/starmicronics/starmgsio/n;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x64

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    nop

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/starmicronics/starmgsio/u$b;

    invoke-direct {v0, p0, p1}, Lcom/starmicronics/starmgsio/u$b;-><init>(Lcom/starmicronics/starmgsio/u;Lcom/starmicronics/starmgsio/ScaleSetting;)V

    iput-object v0, p0, Lcom/starmicronics/starmgsio/u;->d:Lcom/starmicronics/starmgsio/n;

    iget-object p1, p0, Lcom/starmicronics/starmgsio/u;->d:Lcom/starmicronics/starmgsio/n;

    invoke-virtual {p1}, Ljava/lang/Thread;->start()V

    return-void
.end method
