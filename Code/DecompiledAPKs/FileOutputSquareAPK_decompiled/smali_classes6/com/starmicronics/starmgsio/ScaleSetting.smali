.class public final enum Lcom/starmicronics/starmgsio/ScaleSetting;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/starmicronics/starmgsio/ScaleSetting;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/starmicronics/starmgsio/ScaleSetting;

.field public static final enum ZeroPointAdjustment:Lcom/starmicronics/starmgsio/ScaleSetting;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/starmicronics/starmgsio/ScaleSetting;

    const/4 v1, 0x0

    const-string v2, "ZeroPointAdjustment"

    invoke-direct {v0, v2, v1}, Lcom/starmicronics/starmgsio/ScaleSetting;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starmgsio/ScaleSetting;->ZeroPointAdjustment:Lcom/starmicronics/starmgsio/ScaleSetting;

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/starmicronics/starmgsio/ScaleSetting;

    sget-object v2, Lcom/starmicronics/starmgsio/ScaleSetting;->ZeroPointAdjustment:Lcom/starmicronics/starmgsio/ScaleSetting;

    aput-object v2, v0, v1

    sput-object v0, Lcom/starmicronics/starmgsio/ScaleSetting;->$VALUES:[Lcom/starmicronics/starmgsio/ScaleSetting;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/starmicronics/starmgsio/ScaleSetting;
    .locals 1

    const-class v0, Lcom/starmicronics/starmgsio/ScaleSetting;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/starmicronics/starmgsio/ScaleSetting;

    return-object p0
.end method

.method public static values()[Lcom/starmicronics/starmgsio/ScaleSetting;
    .locals 1

    sget-object v0, Lcom/starmicronics/starmgsio/ScaleSetting;->$VALUES:[Lcom/starmicronics/starmgsio/ScaleSetting;

    invoke-virtual {v0}, [Lcom/starmicronics/starmgsio/ScaleSetting;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/starmicronics/starmgsio/ScaleSetting;

    return-object v0
.end method
