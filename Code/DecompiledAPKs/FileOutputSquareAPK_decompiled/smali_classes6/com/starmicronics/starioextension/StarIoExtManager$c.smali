.class Lcom/starmicronics/starioextension/StarIoExtManager$c;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/starmicronics/starioextension/StarIoExtManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "c"
.end annotation


# instance fields
.field final synthetic a:Lcom/starmicronics/starioextension/StarIoExtManager;

.field private b:Lcom/starmicronics/starioextension/StarIoExtManager$a;

.field private c:[B

.field private d:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/starmicronics/starioextension/StarIoExtManager;Lcom/starmicronics/starioextension/StarIoExtManager$a;)V
    .locals 0

    iput-object p1, p0, Lcom/starmicronics/starioextension/StarIoExtManager$c;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p1, 0x0

    iput-object p1, p0, Lcom/starmicronics/starioextension/StarIoExtManager$c;->c:[B

    iput-object p2, p0, Lcom/starmicronics/starioextension/StarIoExtManager$c;->b:Lcom/starmicronics/starioextension/StarIoExtManager$a;

    return-void
.end method


# virtual methods
.method declared-synchronized a(Ljava/lang/String;)V
    .locals 0

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/starmicronics/starioextension/StarIoExtManager$c;->d:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method declared-synchronized a([B)V
    .locals 0

    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, [B->clone()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [B

    iput-object p1, p0, Lcom/starmicronics/starioextension/StarIoExtManager$c;->c:[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method declared-synchronized a()[B
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$c;->c:[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized b()Ljava/lang/String;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$c;->d:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public run()V
    .locals 2

    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$c;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v0}, Lcom/starmicronics/starioextension/StarIoExtManager;->n(Lcom/starmicronics/starioextension/StarIoExtManager;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$c;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v0}, Lcom/starmicronics/starioextension/StarIoExtManager;->p(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/starioextension/ai;

    move-result-object v0

    if-nez v0, :cond_1

    return-void

    :cond_1
    sget-object v0, Lcom/starmicronics/starioextension/StarIoExtManager$5;->a:[I

    iget-object v1, p0, Lcom/starmicronics/starioextension/StarIoExtManager$c;->b:Lcom/starmicronics/starioextension/StarIoExtManager$a;

    invoke-virtual {v1}, Lcom/starmicronics/starioextension/StarIoExtManager$a;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_0

    :pswitch_0
    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$c;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v0}, Lcom/starmicronics/starioextension/StarIoExtManager;->p(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/starioextension/ai;

    move-result-object v0

    invoke-interface {v0}, Lcom/starmicronics/starioextension/ai;->onAccessoryDisconnect()V

    goto/16 :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$c;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v0}, Lcom/starmicronics/starioextension/StarIoExtManager;->p(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/starioextension/ai;

    move-result-object v0

    invoke-interface {v0}, Lcom/starmicronics/starioextension/ai;->onAccessoryConnectFailure()V

    goto/16 :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$c;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v0}, Lcom/starmicronics/starioextension/StarIoExtManager;->p(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/starioextension/ai;

    move-result-object v0

    invoke-interface {v0}, Lcom/starmicronics/starioextension/ai;->onAccessoryConnectSuccess()V

    goto/16 :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$c;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$c;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v0}, Lcom/starmicronics/starioextension/StarIoExtManager;->p(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/starioextension/ai;

    move-result-object v0

    invoke-virtual {p0}, Lcom/starmicronics/starioextension/StarIoExtManager$c;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/starmicronics/starioextension/ai;->onStatusUpdate(Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$c;->c:[B

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$c;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v0}, Lcom/starmicronics/starioextension/StarIoExtManager;->p(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/starioextension/ai;

    move-result-object v0

    invoke-virtual {p0}, Lcom/starmicronics/starioextension/StarIoExtManager$c;->a()[B

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/starmicronics/starioextension/ai;->onBarcodeDataReceive([B)V

    goto/16 :goto_0

    :pswitch_5
    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$c;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v0}, Lcom/starmicronics/starioextension/StarIoExtManager;->p(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/starioextension/ai;

    move-result-object v0

    invoke-interface {v0}, Lcom/starmicronics/starioextension/ai;->onBarcodeReaderDisconnect()V

    goto/16 :goto_0

    :pswitch_6
    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$c;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v0}, Lcom/starmicronics/starioextension/StarIoExtManager;->p(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/starioextension/ai;

    move-result-object v0

    invoke-interface {v0}, Lcom/starmicronics/starioextension/ai;->onBarcodeReaderConnect()V

    goto/16 :goto_0

    :pswitch_7
    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$c;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v0}, Lcom/starmicronics/starioextension/StarIoExtManager;->p(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/starioextension/ai;

    move-result-object v0

    invoke-interface {v0}, Lcom/starmicronics/starioextension/ai;->onBarcodeReaderImpossible()V

    goto :goto_0

    :pswitch_8
    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$c;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v0}, Lcom/starmicronics/starioextension/StarIoExtManager;->p(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/starioextension/ai;

    move-result-object v0

    invoke-interface {v0}, Lcom/starmicronics/starioextension/ai;->onCashDrawerClose()V

    goto :goto_0

    :pswitch_9
    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$c;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v0}, Lcom/starmicronics/starioextension/StarIoExtManager;->p(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/starioextension/ai;

    move-result-object v0

    invoke-interface {v0}, Lcom/starmicronics/starioextension/ai;->onCashDrawerOpen()V

    goto :goto_0

    :pswitch_a
    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$c;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v0}, Lcom/starmicronics/starioextension/StarIoExtManager;->p(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/starioextension/ai;

    move-result-object v0

    invoke-interface {v0}, Lcom/starmicronics/starioextension/ai;->onPrinterCoverClose()V

    goto :goto_0

    :pswitch_b
    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$c;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v0}, Lcom/starmicronics/starioextension/StarIoExtManager;->p(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/starioextension/ai;

    move-result-object v0

    invoke-interface {v0}, Lcom/starmicronics/starioextension/ai;->onPrinterCoverOpen()V

    goto :goto_0

    :pswitch_c
    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$c;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v0}, Lcom/starmicronics/starioextension/StarIoExtManager;->p(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/starioextension/ai;

    move-result-object v0

    invoke-interface {v0}, Lcom/starmicronics/starioextension/ai;->onPrinterPaperEmpty()V

    goto :goto_0

    :pswitch_d
    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$c;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v0}, Lcom/starmicronics/starioextension/StarIoExtManager;->p(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/starioextension/ai;

    move-result-object v0

    invoke-interface {v0}, Lcom/starmicronics/starioextension/ai;->onPrinterPaperNearEmpty()V

    goto :goto_0

    :pswitch_e
    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$c;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v0}, Lcom/starmicronics/starioextension/StarIoExtManager;->p(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/starioextension/ai;

    move-result-object v0

    invoke-interface {v0}, Lcom/starmicronics/starioextension/ai;->onPrinterPaperReady()V

    goto :goto_0

    :pswitch_f
    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$c;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v0}, Lcom/starmicronics/starioextension/StarIoExtManager;->p(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/starioextension/ai;

    move-result-object v0

    invoke-interface {v0}, Lcom/starmicronics/starioextension/ai;->onPrinterOffline()V

    goto :goto_0

    :pswitch_10
    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$c;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v0}, Lcom/starmicronics/starioextension/StarIoExtManager;->p(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/starioextension/ai;

    move-result-object v0

    invoke-interface {v0}, Lcom/starmicronics/starioextension/ai;->onPrinterOnline()V

    goto :goto_0

    :pswitch_11
    iget-object v0, p0, Lcom/starmicronics/starioextension/StarIoExtManager$c;->a:Lcom/starmicronics/starioextension/StarIoExtManager;

    invoke-static {v0}, Lcom/starmicronics/starioextension/StarIoExtManager;->p(Lcom/starmicronics/starioextension/StarIoExtManager;)Lcom/starmicronics/starioextension/ai;

    move-result-object v0

    invoke-interface {v0}, Lcom/starmicronics/starioextension/ai;->onPrinterImpossible()V

    :cond_2
    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
