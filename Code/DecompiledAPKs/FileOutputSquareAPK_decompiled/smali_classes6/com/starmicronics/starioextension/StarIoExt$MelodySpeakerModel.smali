.class public final enum Lcom/starmicronics/starioextension/StarIoExt$MelodySpeakerModel;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/starmicronics/starioextension/StarIoExt;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MelodySpeakerModel"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/starmicronics/starioextension/StarIoExt$MelodySpeakerModel;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/starmicronics/starioextension/StarIoExt$MelodySpeakerModel;

.field public static final enum FVP10:Lcom/starmicronics/starioextension/StarIoExt$MelodySpeakerModel;

.field public static final enum MCS10:Lcom/starmicronics/starioextension/StarIoExt$MelodySpeakerModel;

.field public static final enum None:Lcom/starmicronics/starioextension/StarIoExt$MelodySpeakerModel;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    new-instance v0, Lcom/starmicronics/starioextension/StarIoExt$MelodySpeakerModel;

    const/4 v1, 0x0

    const-string v2, "None"

    invoke-direct {v0, v2, v1}, Lcom/starmicronics/starioextension/StarIoExt$MelodySpeakerModel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/StarIoExt$MelodySpeakerModel;->None:Lcom/starmicronics/starioextension/StarIoExt$MelodySpeakerModel;

    new-instance v0, Lcom/starmicronics/starioextension/StarIoExt$MelodySpeakerModel;

    const/4 v2, 0x1

    const-string v3, "MCS10"

    invoke-direct {v0, v3, v2}, Lcom/starmicronics/starioextension/StarIoExt$MelodySpeakerModel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/StarIoExt$MelodySpeakerModel;->MCS10:Lcom/starmicronics/starioextension/StarIoExt$MelodySpeakerModel;

    new-instance v0, Lcom/starmicronics/starioextension/StarIoExt$MelodySpeakerModel;

    const/4 v3, 0x2

    const-string v4, "FVP10"

    invoke-direct {v0, v4, v3}, Lcom/starmicronics/starioextension/StarIoExt$MelodySpeakerModel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/StarIoExt$MelodySpeakerModel;->FVP10:Lcom/starmicronics/starioextension/StarIoExt$MelodySpeakerModel;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/starmicronics/starioextension/StarIoExt$MelodySpeakerModel;

    sget-object v4, Lcom/starmicronics/starioextension/StarIoExt$MelodySpeakerModel;->None:Lcom/starmicronics/starioextension/StarIoExt$MelodySpeakerModel;

    aput-object v4, v0, v1

    sget-object v1, Lcom/starmicronics/starioextension/StarIoExt$MelodySpeakerModel;->MCS10:Lcom/starmicronics/starioextension/StarIoExt$MelodySpeakerModel;

    aput-object v1, v0, v2

    sget-object v1, Lcom/starmicronics/starioextension/StarIoExt$MelodySpeakerModel;->FVP10:Lcom/starmicronics/starioextension/StarIoExt$MelodySpeakerModel;

    aput-object v1, v0, v3

    sput-object v0, Lcom/starmicronics/starioextension/StarIoExt$MelodySpeakerModel;->$VALUES:[Lcom/starmicronics/starioextension/StarIoExt$MelodySpeakerModel;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/starmicronics/starioextension/StarIoExt$MelodySpeakerModel;
    .locals 1

    const-class v0, Lcom/starmicronics/starioextension/StarIoExt$MelodySpeakerModel;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/starmicronics/starioextension/StarIoExt$MelodySpeakerModel;

    return-object p0
.end method

.method public static values()[Lcom/starmicronics/starioextension/StarIoExt$MelodySpeakerModel;
    .locals 1

    sget-object v0, Lcom/starmicronics/starioextension/StarIoExt$MelodySpeakerModel;->$VALUES:[Lcom/starmicronics/starioextension/StarIoExt$MelodySpeakerModel;

    invoke-virtual {v0}, [Lcom/starmicronics/starioextension/StarIoExt$MelodySpeakerModel;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/starmicronics/starioextension/StarIoExt$MelodySpeakerModel;

    return-object v0
.end method
