.class public final enum Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/starmicronics/starioextension/ICommandBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CodePageType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

.field public static final enum Blank:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

.field public static final enum CP1001:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

.field public static final enum CP1250:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

.field public static final enum CP1251:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

.field public static final enum CP1252:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

.field public static final enum CP2001:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

.field public static final enum CP3001:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

.field public static final enum CP3002:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

.field public static final enum CP3011:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

.field public static final enum CP3012:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

.field public static final enum CP3021:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

.field public static final enum CP3041:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

.field public static final enum CP3840:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

.field public static final enum CP3841:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

.field public static final enum CP3843:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

.field public static final enum CP3844:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

.field public static final enum CP3845:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

.field public static final enum CP3846:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

.field public static final enum CP3847:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

.field public static final enum CP3848:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

.field public static final enum CP437:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

.field public static final enum CP737:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

.field public static final enum CP772:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

.field public static final enum CP774:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

.field public static final enum CP851:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

.field public static final enum CP852:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

.field public static final enum CP855:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

.field public static final enum CP857:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

.field public static final enum CP858:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

.field public static final enum CP860:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

.field public static final enum CP861:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

.field public static final enum CP862:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

.field public static final enum CP863:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

.field public static final enum CP864:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

.field public static final enum CP865:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

.field public static final enum CP866:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

.field public static final enum CP869:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

.field public static final enum CP874:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

.field public static final enum CP928:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

.field public static final enum CP932:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

.field public static final enum CP998:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

.field public static final enum CP999:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

.field public static final enum UTF8:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const/4 v1, 0x0

    const-string v2, "CP437"

    invoke-direct {v0, v2, v1}, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP437:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const/4 v2, 0x1

    const-string v3, "CP737"

    invoke-direct {v0, v3, v2}, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP737:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const/4 v3, 0x2

    const-string v4, "CP772"

    invoke-direct {v0, v4, v3}, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP772:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const/4 v4, 0x3

    const-string v5, "CP774"

    invoke-direct {v0, v5, v4}, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP774:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const/4 v5, 0x4

    const-string v6, "CP851"

    invoke-direct {v0, v6, v5}, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP851:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const/4 v6, 0x5

    const-string v7, "CP852"

    invoke-direct {v0, v7, v6}, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP852:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const/4 v7, 0x6

    const-string v8, "CP855"

    invoke-direct {v0, v8, v7}, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP855:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const/4 v8, 0x7

    const-string v9, "CP857"

    invoke-direct {v0, v9, v8}, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP857:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const/16 v9, 0x8

    const-string v10, "CP858"

    invoke-direct {v0, v10, v9}, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP858:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const/16 v10, 0x9

    const-string v11, "CP860"

    invoke-direct {v0, v11, v10}, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP860:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const/16 v11, 0xa

    const-string v12, "CP861"

    invoke-direct {v0, v12, v11}, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP861:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const/16 v12, 0xb

    const-string v13, "CP862"

    invoke-direct {v0, v13, v12}, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP862:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const/16 v13, 0xc

    const-string v14, "CP863"

    invoke-direct {v0, v14, v13}, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP863:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const/16 v14, 0xd

    const-string v15, "CP864"

    invoke-direct {v0, v15, v14}, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP864:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const/16 v15, 0xe

    const-string v14, "CP865"

    invoke-direct {v0, v14, v15}, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP865:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const-string v14, "CP866"

    const/16 v15, 0xf

    invoke-direct {v0, v14, v15}, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP866:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const-string v14, "CP869"

    const/16 v15, 0x10

    invoke-direct {v0, v14, v15}, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP869:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const-string v14, "CP874"

    const/16 v15, 0x11

    invoke-direct {v0, v14, v15}, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP874:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const-string v14, "CP928"

    const/16 v15, 0x12

    invoke-direct {v0, v14, v15}, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP928:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const-string v14, "CP932"

    const/16 v15, 0x13

    invoke-direct {v0, v14, v15}, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP932:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const-string v14, "CP998"

    const/16 v15, 0x14

    invoke-direct {v0, v14, v15}, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP998:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const-string v14, "CP999"

    const/16 v15, 0x15

    invoke-direct {v0, v14, v15}, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP999:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const-string v14, "CP1001"

    const/16 v15, 0x16

    invoke-direct {v0, v14, v15}, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP1001:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const-string v14, "CP1250"

    const/16 v15, 0x17

    invoke-direct {v0, v14, v15}, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP1250:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const-string v14, "CP1251"

    const/16 v15, 0x18

    invoke-direct {v0, v14, v15}, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP1251:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const-string v14, "CP1252"

    const/16 v15, 0x19

    invoke-direct {v0, v14, v15}, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP1252:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const-string v14, "CP2001"

    const/16 v15, 0x1a

    invoke-direct {v0, v14, v15}, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP2001:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const-string v14, "CP3001"

    const/16 v15, 0x1b

    invoke-direct {v0, v14, v15}, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP3001:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const-string v14, "CP3002"

    const/16 v15, 0x1c

    invoke-direct {v0, v14, v15}, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP3002:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const-string v14, "CP3011"

    const/16 v15, 0x1d

    invoke-direct {v0, v14, v15}, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP3011:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const-string v14, "CP3012"

    const/16 v15, 0x1e

    invoke-direct {v0, v14, v15}, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP3012:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const-string v14, "CP3021"

    const/16 v15, 0x1f

    invoke-direct {v0, v14, v15}, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP3021:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const-string v14, "CP3041"

    const/16 v15, 0x20

    invoke-direct {v0, v14, v15}, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP3041:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const-string v14, "CP3840"

    const/16 v15, 0x21

    invoke-direct {v0, v14, v15}, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP3840:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const-string v14, "CP3841"

    const/16 v15, 0x22

    invoke-direct {v0, v14, v15}, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP3841:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const-string v14, "CP3843"

    const/16 v15, 0x23

    invoke-direct {v0, v14, v15}, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP3843:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const-string v14, "CP3844"

    const/16 v15, 0x24

    invoke-direct {v0, v14, v15}, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP3844:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const-string v14, "CP3845"

    const/16 v15, 0x25

    invoke-direct {v0, v14, v15}, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP3845:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const-string v14, "CP3846"

    const/16 v15, 0x26

    invoke-direct {v0, v14, v15}, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP3846:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const-string v14, "CP3847"

    const/16 v15, 0x27

    invoke-direct {v0, v14, v15}, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP3847:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const-string v14, "CP3848"

    const/16 v15, 0x28

    invoke-direct {v0, v14, v15}, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP3848:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const-string v14, "UTF8"

    const/16 v15, 0x29

    invoke-direct {v0, v14, v15}, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->UTF8:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    new-instance v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const-string v14, "Blank"

    const/16 v15, 0x2a

    invoke-direct {v0, v14, v15}, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->Blank:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const/16 v0, 0x2b

    new-array v0, v0, [Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    sget-object v14, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP437:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    aput-object v14, v0, v1

    sget-object v1, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP737:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP772:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP774:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP851:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP852:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP855:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP857:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    aput-object v1, v0, v8

    sget-object v1, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP858:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    aput-object v1, v0, v9

    sget-object v1, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP860:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    aput-object v1, v0, v10

    sget-object v1, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP861:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    aput-object v1, v0, v11

    sget-object v1, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP862:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    aput-object v1, v0, v12

    sget-object v1, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP863:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    aput-object v1, v0, v13

    sget-object v1, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP864:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const/16 v2, 0xd

    aput-object v1, v0, v2

    sget-object v1, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP865:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const/16 v2, 0xe

    aput-object v1, v0, v2

    sget-object v1, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP866:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const/16 v2, 0xf

    aput-object v1, v0, v2

    sget-object v1, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP869:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const/16 v2, 0x10

    aput-object v1, v0, v2

    sget-object v1, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP874:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const/16 v2, 0x11

    aput-object v1, v0, v2

    sget-object v1, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP928:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const/16 v2, 0x12

    aput-object v1, v0, v2

    sget-object v1, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP932:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const/16 v2, 0x13

    aput-object v1, v0, v2

    sget-object v1, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP998:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const/16 v2, 0x14

    aput-object v1, v0, v2

    sget-object v1, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP999:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const/16 v2, 0x15

    aput-object v1, v0, v2

    sget-object v1, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP1001:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const/16 v2, 0x16

    aput-object v1, v0, v2

    sget-object v1, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP1250:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const/16 v2, 0x17

    aput-object v1, v0, v2

    sget-object v1, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP1251:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const/16 v2, 0x18

    aput-object v1, v0, v2

    sget-object v1, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP1252:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const/16 v2, 0x19

    aput-object v1, v0, v2

    sget-object v1, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP2001:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const/16 v2, 0x1a

    aput-object v1, v0, v2

    sget-object v1, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP3001:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const/16 v2, 0x1b

    aput-object v1, v0, v2

    sget-object v1, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP3002:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const/16 v2, 0x1c

    aput-object v1, v0, v2

    sget-object v1, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP3011:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const/16 v2, 0x1d

    aput-object v1, v0, v2

    sget-object v1, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP3012:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const/16 v2, 0x1e

    aput-object v1, v0, v2

    sget-object v1, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP3021:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const/16 v2, 0x1f

    aput-object v1, v0, v2

    sget-object v1, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP3041:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const/16 v2, 0x20

    aput-object v1, v0, v2

    sget-object v1, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP3840:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const/16 v2, 0x21

    aput-object v1, v0, v2

    sget-object v1, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP3841:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const/16 v2, 0x22

    aput-object v1, v0, v2

    sget-object v1, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP3843:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const/16 v2, 0x23

    aput-object v1, v0, v2

    sget-object v1, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP3844:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const/16 v2, 0x24

    aput-object v1, v0, v2

    sget-object v1, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP3845:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const/16 v2, 0x25

    aput-object v1, v0, v2

    sget-object v1, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP3846:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const/16 v2, 0x26

    aput-object v1, v0, v2

    sget-object v1, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP3847:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const/16 v2, 0x27

    aput-object v1, v0, v2

    sget-object v1, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->CP3848:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const/16 v2, 0x28

    aput-object v1, v0, v2

    sget-object v1, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->UTF8:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const/16 v2, 0x29

    aput-object v1, v0, v2

    sget-object v1, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->Blank:Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    const/16 v2, 0x2a

    aput-object v1, v0, v2

    sput-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->$VALUES:[Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;
    .locals 1

    const-class v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    return-object p0
.end method

.method public static values()[Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;
    .locals 1

    sget-object v0, Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->$VALUES:[Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    invoke-virtual {v0}, [Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/starmicronics/starioextension/ICommandBuilder$CodePageType;

    return-object v0
.end method
