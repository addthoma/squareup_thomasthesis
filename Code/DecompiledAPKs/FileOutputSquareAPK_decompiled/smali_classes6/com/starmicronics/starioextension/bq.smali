.class Lcom/starmicronics/starioextension/bq;
.super Lcom/starmicronics/starioextension/ah;


# direct methods
.method constructor <init>(I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/starmicronics/starioextension/ah;-><init>(I)V

    return-void
.end method

.method private static a([B)Ljava/lang/String;
    .locals 7

    new-instance v0, Lcom/starmicronics/starioextension/bq$2;

    invoke-direct {v0}, Lcom/starmicronics/starioextension/bq$2;-><init>()V

    new-instance v1, Lcom/starmicronics/starioextension/bq$3;

    invoke-direct {v1}, Lcom/starmicronics/starioextension/bq$3;-><init>()V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "101"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v4, 0x0

    :goto_0
    const/4 v5, 0x0

    const/4 v6, 0x6

    if-ge v4, v6, :cond_1

    aget-byte v6, p0, v4

    invoke-virtual {v0, v6}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    if-nez v6, :cond_0

    return-object v5

    :cond_0
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_1
    const-string v0, "01010"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_1
    const/16 v0, 0xc

    if-ge v6, v0, :cond_3

    aget-byte v0, p0, v6

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-nez v0, :cond_2

    return-object v5

    :cond_2
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    :cond_3
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public a([BLcom/starmicronics/starioextension/ICommandBuilder$BarcodeWidth;)V
    .locals 4

    if-nez p1, :cond_0

    return-void

    :cond_0
    array-length v0, p1

    const/16 v1, 0xc

    const/16 v2, 0xb

    if-eq v0, v2, :cond_1

    array-length v0, p1

    if-eq v0, v1, :cond_1

    return-void

    :cond_1
    new-array v0, v1, [B

    const/4 v1, 0x0

    invoke-static {p1, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-static {p1, v2}, Lcom/starmicronics/starioextension/n;->a([BI)B

    move-result p1

    int-to-byte p1, p1

    aput-byte p1, v0, v2

    invoke-static {v0}, Lcom/starmicronics/starioextension/bq;->a([B)Ljava/lang/String;

    move-result-object p1

    if-nez p1, :cond_2

    return-void

    :cond_2
    new-instance v3, Lcom/starmicronics/starioextension/bq$1;

    invoke-direct {v3, p0}, Lcom/starmicronics/starioextension/bq$1;-><init>(Lcom/starmicronics/starioextension/bq;)V

    invoke-interface {v3, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p2

    invoke-virtual {p0, p1, p2}, Lcom/starmicronics/starioextension/bq;->a(Ljava/lang/String;I)V

    new-array p1, v2, [I

    :goto_0
    array-length p2, p1

    if-ge v1, p2, :cond_3

    aget-byte p2, v0, v1

    aput p2, p1, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    invoke-virtual {p0, p1}, Lcom/starmicronics/starioextension/bq;->a([I)V

    return-void
.end method
