.class Lcom/starmicronics/starioextension/av;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/starmicronics/starioextension/IPeripheralConnectParser;


# static fields
.field private static final a:Ljava/lang/String; = "Mdl="

.field private static final b:Ljava/lang/String; = "MCS10"


# instance fields
.field private c:Z


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/starmicronics/starioextension/av;->c:Z

    return-void
.end method


# virtual methods
.method public createSendCommands()[B
    .locals 1

    const/16 v0, 0x9

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    return-object v0

    :array_0
    .array-data 1
        0x1bt
        0x1dt
        0x29t
        0x49t
        0x3t
        0x0t
        0x34t
        0x0t
        0x0t
    .end array-data
.end method

.method public isConnected()Z
    .locals 1

    iget-boolean v0, p0, Lcom/starmicronics/starioextension/av;->c:Z

    return v0
.end method

.method public parse([BI)Lcom/starmicronics/starioextension/IPeripheralCommandParser$ParseResult;
    .locals 4

    const/16 v0, 0x9

    if-ge p2, v0, :cond_0

    sget-object p1, Lcom/starmicronics/starioextension/IPeripheralCommandParser$ParseResult;->Invalid:Lcom/starmicronics/starioextension/IPeripheralCommandParser$ParseResult;

    return-object p1

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/lit8 v2, p2, -0xb

    if-gt v1, v2, :cond_3

    aget-byte v2, p1, v1

    const/16 v3, 0x1b

    if-ne v2, v3, :cond_2

    add-int/lit8 v2, v1, 0x1

    aget-byte v2, p1, v2

    const/16 v3, 0x1d

    if-ne v2, v3, :cond_2

    add-int/lit8 v2, v1, 0x2

    aget-byte v2, p1, v2

    const/16 v3, 0x29

    if-ne v2, v3, :cond_2

    add-int/lit8 v2, v1, 0x3

    aget-byte v2, p1, v2

    const/16 v3, 0x49

    if-ne v2, v3, :cond_2

    add-int/lit8 v2, v1, 0x4

    aget-byte v2, p1, v2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_2

    add-int/lit8 v2, v1, 0x5

    aget-byte v2, p1, v2

    if-nez v2, :cond_2

    add-int/lit8 v2, v1, 0x6

    aget-byte v2, p1, v2

    const/16 v3, 0x34

    if-ne v2, v3, :cond_2

    add-int/lit8 v2, v1, 0x7

    aget-byte v2, p1, v2

    add-int/lit8 v3, v1, 0x8

    aget-byte v3, p1, v3

    mul-int/lit16 v3, v3, 0x100

    add-int/2addr v2, v3

    add-int/2addr v1, v0

    add-int v0, v1, v2

    if-ge p2, v0, :cond_1

    sget-object p1, Lcom/starmicronics/starioextension/IPeripheralCommandParser$ParseResult;->Invalid:Lcom/starmicronics/starioextension/IPeripheralCommandParser$ParseResult;

    return-object p1

    :cond_1
    new-instance p2, Ljava/lang/String;

    invoke-direct {p2, p1, v1, v2}, Ljava/lang/String;-><init>([BII)V

    const-string p1, "Mdl=MCS10"

    invoke-virtual {p2, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/starmicronics/starioextension/av;->c:Z

    sget-object p1, Lcom/starmicronics/starioextension/IPeripheralCommandParser$ParseResult;->Success:Lcom/starmicronics/starioextension/IPeripheralCommandParser$ParseResult;

    return-object p1

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    sget-object p1, Lcom/starmicronics/starioextension/IPeripheralCommandParser$ParseResult;->Invalid:Lcom/starmicronics/starioextension/IPeripheralCommandParser$ParseResult;

    return-object p1
.end method
