.class public final enum Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/starmicronics/starioextension/StarIoExtManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CashDrawerStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;

.field public static final enum Close:Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;

.field public static final enum Impossible:Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;

.field public static final enum Invalid:Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;

.field public static final enum Open:Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    new-instance v0, Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;

    const/4 v1, 0x0

    const-string v2, "Invalid"

    invoke-direct {v0, v2, v1}, Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;->Invalid:Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;

    new-instance v0, Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;

    const/4 v2, 0x1

    const-string v3, "Impossible"

    invoke-direct {v0, v3, v2}, Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;->Impossible:Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;

    new-instance v0, Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;

    const/4 v3, 0x2

    const-string v4, "Open"

    invoke-direct {v0, v4, v3}, Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;->Open:Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;

    new-instance v0, Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;

    const/4 v4, 0x3

    const-string v5, "Close"

    invoke-direct {v0, v5, v4}, Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;->Close:Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;

    sget-object v5, Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;->Invalid:Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;

    aput-object v5, v0, v1

    sget-object v1, Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;->Impossible:Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;->Open:Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;->Close:Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;

    aput-object v1, v0, v4

    sput-object v0, Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;->$VALUES:[Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;
    .locals 1

    const-class v0, Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;

    return-object p0
.end method

.method public static values()[Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;
    .locals 1

    sget-object v0, Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;->$VALUES:[Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;

    invoke-virtual {v0}, [Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/starmicronics/starioextension/StarIoExtManager$CashDrawerStatus;

    return-object v0
.end method
