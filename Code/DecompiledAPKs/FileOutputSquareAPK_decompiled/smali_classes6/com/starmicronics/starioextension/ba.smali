.class Lcom/starmicronics/starioextension/ba;
.super Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;",
            "Lcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;",
            ")V"
        }
    .end annotation

    new-instance v0, Lcom/starmicronics/starioextension/ba$1;

    invoke-direct {v0}, Lcom/starmicronics/starioextension/ba$1;-><init>()V

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Byte;

    invoke-virtual {p1}, Ljava/lang/Byte;->byteValue()B

    move-result p1

    const/4 v0, 0x5

    new-array v0, v0, [B

    const/4 v1, 0x0

    const/16 v2, 0x1b

    aput-byte v2, v0, v1

    const/4 v1, 0x1

    const/16 v2, 0x1d

    aput-byte v2, v0, v1

    const/4 v1, 0x2

    const/16 v2, 0x50

    aput-byte v2, v0, v1

    const/4 v1, 0x3

    const/16 v2, 0x32

    aput-byte v2, v0, v1

    const/4 v1, 0x4

    aput-byte p1, v0, v1

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method static b(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;",
            "Lcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;",
            ")V"
        }
    .end annotation

    return-void
.end method

.method static c(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;",
            "Lcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;",
            ")V"
        }
    .end annotation

    new-instance v0, Lcom/starmicronics/starioextension/ba$2;

    invoke-direct {v0}, Lcom/starmicronics/starioextension/ba$2;-><init>()V

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Byte;

    invoke-virtual {p1}, Ljava/lang/Byte;->byteValue()B

    move-result p1

    const/4 v0, 0x3

    new-array v0, v0, [B

    const/4 v1, 0x0

    const/16 v2, 0x1b

    aput-byte v2, v0, v1

    const/4 v1, 0x1

    const/16 v2, 0x54

    aput-byte v2, v0, v1

    const/4 v1, 0x2

    aput-byte p1, v0, v1

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method static d(Ljava/util/List;Lcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;",
            "Lcom/starmicronics/starioextension/ICommandBuilder$BitmapConverterRotation;",
            ")V"
        }
    .end annotation

    return-void
.end method
