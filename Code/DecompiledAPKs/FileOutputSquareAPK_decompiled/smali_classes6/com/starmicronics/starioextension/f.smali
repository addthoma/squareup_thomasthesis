.class Lcom/starmicronics/starioextension/f;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/starmicronics/starioextension/IPeripheralConnectParser;


# instance fields
.field private a:Z


# direct methods
.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/starmicronics/starioextension/f;->a:Z

    return-void
.end method


# virtual methods
.method public createSendCommands()[B
    .locals 1

    const/4 v0, 0x4

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    return-object v0

    nop

    :array_0
    .array-data 1
        0x1bt
        0x1et
        0x42t
        0x31t
    .end array-data
.end method

.method public isConnected()Z
    .locals 1

    iget-boolean v0, p0, Lcom/starmicronics/starioextension/f;->a:Z

    return v0
.end method

.method public parse([BI)Lcom/starmicronics/starioextension/IPeripheralCommandParser$ParseResult;
    .locals 5

    const/4 v0, 0x5

    if-ge p2, v0, :cond_0

    sget-object p1, Lcom/starmicronics/starioextension/IPeripheralCommandParser$ParseResult;->Invalid:Lcom/starmicronics/starioextension/IPeripheralCommandParser$ParseResult;

    return-object p1

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    add-int/lit8 v3, p2, -0x5

    if-gt v2, v3, :cond_3

    aget-byte v3, p1, v2

    const/16 v4, 0x1b

    if-ne v3, v4, :cond_2

    add-int/lit8 v3, v2, 0x1

    aget-byte v3, p1, v3

    const/16 v4, 0x1e

    if-ne v3, v4, :cond_2

    add-int/lit8 v3, v2, 0x2

    aget-byte v3, p1, v3

    const/16 v4, 0x42

    if-ne v3, v4, :cond_2

    add-int/lit8 v3, v2, 0x3

    aget-byte v3, p1, v3

    const/16 v4, 0x31

    if-ne v3, v4, :cond_2

    add-int/lit8 v2, v2, 0x4

    aget-byte p1, p1, v2

    const/4 p2, 0x2

    and-int/2addr p1, p2

    if-ne p1, p2, :cond_1

    const/4 v1, 0x1

    :cond_1
    iput-boolean v1, p0, Lcom/starmicronics/starioextension/f;->a:Z

    sget-object p1, Lcom/starmicronics/starioextension/IPeripheralCommandParser$ParseResult;->Success:Lcom/starmicronics/starioextension/IPeripheralCommandParser$ParseResult;

    return-object p1

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    sget-object p1, Lcom/starmicronics/starioextension/IPeripheralCommandParser$ParseResult;->Invalid:Lcom/starmicronics/starioextension/IPeripheralCommandParser$ParseResult;

    return-object p1
.end method
