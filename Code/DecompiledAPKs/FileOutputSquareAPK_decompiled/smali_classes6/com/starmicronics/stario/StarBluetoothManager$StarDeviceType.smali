.class public final enum Lcom/starmicronics/stario/StarBluetoothManager$StarDeviceType;
.super Ljava/lang/Enum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/starmicronics/stario/StarBluetoothManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "StarDeviceType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/starmicronics/stario/StarBluetoothManager$StarDeviceType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/starmicronics/stario/StarBluetoothManager$StarDeviceType;

.field public static final enum StarDeviceTypeDesktopPrinter:Lcom/starmicronics/stario/StarBluetoothManager$StarDeviceType;

.field public static final enum StarDeviceTypePortablePrinter:Lcom/starmicronics/stario/StarBluetoothManager$StarDeviceType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Lcom/starmicronics/stario/StarBluetoothManager$StarDeviceType;

    const/4 v1, 0x0

    const-string v2, "StarDeviceTypeDesktopPrinter"

    invoke-direct {v0, v2, v1}, Lcom/starmicronics/stario/StarBluetoothManager$StarDeviceType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/stario/StarBluetoothManager$StarDeviceType;->StarDeviceTypeDesktopPrinter:Lcom/starmicronics/stario/StarBluetoothManager$StarDeviceType;

    new-instance v0, Lcom/starmicronics/stario/StarBluetoothManager$StarDeviceType;

    const/4 v2, 0x1

    const-string v3, "StarDeviceTypePortablePrinter"

    invoke-direct {v0, v3, v2}, Lcom/starmicronics/stario/StarBluetoothManager$StarDeviceType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/starmicronics/stario/StarBluetoothManager$StarDeviceType;->StarDeviceTypePortablePrinter:Lcom/starmicronics/stario/StarBluetoothManager$StarDeviceType;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/starmicronics/stario/StarBluetoothManager$StarDeviceType;

    sget-object v3, Lcom/starmicronics/stario/StarBluetoothManager$StarDeviceType;->StarDeviceTypeDesktopPrinter:Lcom/starmicronics/stario/StarBluetoothManager$StarDeviceType;

    aput-object v3, v0, v1

    sget-object v1, Lcom/starmicronics/stario/StarBluetoothManager$StarDeviceType;->StarDeviceTypePortablePrinter:Lcom/starmicronics/stario/StarBluetoothManager$StarDeviceType;

    aput-object v1, v0, v2

    sput-object v0, Lcom/starmicronics/stario/StarBluetoothManager$StarDeviceType;->$VALUES:[Lcom/starmicronics/stario/StarBluetoothManager$StarDeviceType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/starmicronics/stario/StarBluetoothManager$StarDeviceType;
    .locals 1

    const-class v0, Lcom/starmicronics/stario/StarBluetoothManager$StarDeviceType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/starmicronics/stario/StarBluetoothManager$StarDeviceType;

    return-object p0
.end method

.method public static values()[Lcom/starmicronics/stario/StarBluetoothManager$StarDeviceType;
    .locals 1

    sget-object v0, Lcom/starmicronics/stario/StarBluetoothManager$StarDeviceType;->$VALUES:[Lcom/starmicronics/stario/StarBluetoothManager$StarDeviceType;

    invoke-virtual {v0}, [Lcom/starmicronics/stario/StarBluetoothManager$StarDeviceType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/starmicronics/stario/StarBluetoothManager$StarDeviceType;

    return-object v0
.end method
