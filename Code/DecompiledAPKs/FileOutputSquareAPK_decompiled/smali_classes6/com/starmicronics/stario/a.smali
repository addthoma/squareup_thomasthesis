.class Lcom/starmicronics/stario/a;
.super Lcom/starmicronics/stario/StarIOPort;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/starmicronics/stario/a$a;
    }
.end annotation


# static fields
.field private static j:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector<",
            "Landroid/bluetooth/BluetoothSocket;",
            ">;"
        }
    .end annotation
.end field

.field private static m:[B


# instance fields
.field private a:Z

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:I

.field private e:I

.field private f:Landroid/bluetooth/BluetoothSocket;

.field private g:Ljava/io/InputStream;

.field private h:Ljava/io/OutputStream;

.field private final i:Ljava/lang/String;

.field private k:Z

.field private l:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;

.field private p:Z

.field private q:I

.field private r:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    sput-object v0, Lcom/starmicronics/stario/a;->j:Ljava/util/Vector;

    const/4 v0, 0x0

    sput-object v0, Lcom/starmicronics/stario/a;->m:[B

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/starmicronics/stario/StarIOPort;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/starmicronics/stario/a;->f:Landroid/bluetooth/BluetoothSocket;

    const-string v0, "00001101-0000-1000-8000-00805F9B34FB"

    iput-object v0, p0, Lcom/starmicronics/stario/a;->i:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/starmicronics/stario/a;->k:Z

    const-string v1, "StarLine"

    iput-object v1, p0, Lcom/starmicronics/stario/a;->l:Ljava/lang/String;

    const-string v1, ""

    iput-object v1, p0, Lcom/starmicronics/stario/a;->n:Ljava/lang/String;

    iput-object v1, p0, Lcom/starmicronics/stario/a;->o:Ljava/lang/String;

    iput-boolean v0, p0, Lcom/starmicronics/stario/a;->p:Z

    iput v0, p0, Lcom/starmicronics/stario/a;->q:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/starmicronics/stario/a;->r:I

    iput-object p1, p0, Lcom/starmicronics/stario/a;->b:Ljava/lang/String;

    iput-object p2, p0, Lcom/starmicronics/stario/a;->c:Ljava/lang/String;

    iput p3, p0, Lcom/starmicronics/stario/a;->d:I

    iput p3, p0, Lcom/starmicronics/stario/a;->e:I

    invoke-direct {p0}, Lcom/starmicronics/stario/a;->f()V

    return-void
.end method

.method static a(Ljava/lang/String;)Z
    .locals 3

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x3

    const/4 v2, 0x0

    if-ge v0, v1, :cond_0

    return v2

    :cond_0
    invoke-virtual {p0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    const-string v0, "BT:"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_1

    const/4 p0, 0x1

    return p0

    :cond_1
    return v2
.end method

.method private static a(Ljava/util/List;)[B
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;)[B"
        }
    .end annotation

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v3

    if-ge v1, v3, :cond_0

    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [B

    array-length v3, v3

    add-int/2addr v2, v3

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    new-array v1, v2, [B

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_1
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v4

    if-ge v2, v4, :cond_1

    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [B

    array-length v5, v5

    invoke-static {v4, v0, v1, v3, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [B

    array-length v4, v4

    add-int/2addr v3, v4

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    return-object v1
.end method

.method private a([B[B)[B
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    array-length v0, p1

    array-length v1, p2

    add-int v2, v0, v1

    new-array v2, v2, [B

    const/4 v3, 0x0

    invoke-static {p1, v3, v2, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-static {p2, v3, v2, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :try_start_0
    array-length v0, v2

    invoke-virtual {p0, v2, v3, v0}, Lcom/starmicronics/stario/a;->writePort([BII)V
    :try_end_0
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_0 .. :try_end_0} :catch_3

    iget v0, p0, Lcom/starmicronics/stario/a;->d:I

    const/16 v1, 0x2710

    if-le v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/16 v0, 0x2710

    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    const/16 v4, 0x80

    new-array v4, v4, [B

    const/4 v5, 0x0

    move-object v6, v5

    const/4 v5, 0x0

    :goto_1
    :try_start_1
    array-length v7, v4

    if-ge v5, v7, :cond_4

    array-length v7, v4

    sub-int/2addr v7, v5

    invoke-virtual {p0, v4, v5, v7}, Lcom/starmicronics/stario/a;->readPort([BII)I

    move-result v7

    add-int/2addr v5, v7

    array-length v7, p1

    array-length v8, p2

    add-int/2addr v7, v8

    if-gt v7, v5, :cond_1

    new-array v6, v5, [B

    invoke-static {v4, v3, v6, v3, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-static {v6, p1, p2}, Lcom/starmicronics/stario/f;->a([B[B[B)[B

    move-result-object v6
    :try_end_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    if-eqz v6, :cond_2

    goto :goto_2

    :cond_2
    const-wide/16 v7, 0x32

    :try_start_2
    invoke-static {v7, v8}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_0
    :try_start_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    sub-long/2addr v7, v1

    int-to-long v9, v0

    cmp-long v11, v7, v9

    if-gtz v11, :cond_3

    goto :goto_1

    :cond_3
    new-instance p1, Ljava/util/concurrent/TimeoutException;

    const-string p2, "There was no response of the device within the timeout period."

    invoke-direct {p1, p2}, Ljava/util/concurrent/TimeoutException;-><init>(Ljava/lang/String;)V

    throw p1
    :try_end_3
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_3 .. :try_end_3} :catch_1

    :cond_4
    :goto_2
    if-eqz v6, :cond_5

    return-object v6

    :cond_5
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string p2, "Failed to parse dip-switch1 condition."

    invoke-direct {p1, p2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1

    :catch_1
    move-exception p1

    new-instance p2, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {p1}, Lcom/starmicronics/stario/StarIOPortException;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p2

    :catch_2
    move-exception p1

    new-instance p2, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {p1}, Ljava/util/concurrent/TimeoutException;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p2

    :catch_3
    move-exception p1

    new-instance p2, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {p1}, Lcom/starmicronics/stario/StarIOPortException;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method public static b(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList<",
            "Lcom/starmicronics/stario/PortInfo;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v1

    const-string v2, "BT:"

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->getBondedDevices()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v3

    if-lez v3, :cond_1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/starmicronics/stario/a;->c(Ljava/lang/String;)Z

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_0

    new-instance v4, Lcom/starmicronics/stario/PortInfo;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v3

    const-string v6, ""

    invoke-direct {v4, v5, v3, v6, v6}, Lcom/starmicronics/stario/PortInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x3

    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/starmicronics/stario/PortInfo;

    invoke-virtual {v3}, Lcom/starmicronics/stario/PortInfo;->getPortName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    return-object v1

    :cond_4
    return-object v0
.end method

.method private b()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    :try_start_0
    invoke-direct {p0}, Lcom/starmicronics/stario/a;->c()V

    iget-object v0, p0, Lcom/starmicronics/stario/a;->n:Ljava/lang/String;

    const-string v1, "DK-AirCash"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0
    :try_end_0
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v0, :cond_0

    :try_start_1
    iget-object v0, p0, Lcom/starmicronics/stario/a;->o:Ljava/lang/String;

    const/4 v1, 0x0

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    invoke-direct {p0}, Lcom/starmicronics/stario/a;->d()V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    new-instance v1, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_2
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_2 .. :try_end_2} :catch_1

    :cond_0
    :goto_0
    return-void

    :catch_1
    move-exception v0

    new-instance v1, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {v0}, Lcom/starmicronics/stario/StarIOPortException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private c()V
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    const/4 v0, 0x5

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    :try_start_0
    array-length v1, v0

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2, v1}, Lcom/starmicronics/stario/a;->writePort([BII)V
    :try_end_0
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_0 .. :try_end_0} :catch_3

    iget v0, p0, Lcom/starmicronics/stario/a;->d:I

    const/16 v1, 0x2710

    if-le v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/16 v0, 0x2710

    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    const/16 v1, 0x80

    new-array v1, v1, [B

    const/4 v5, 0x4

    new-array v5, v5, [B

    fill-array-data v5, :array_1

    const/4 v6, 0x2

    new-array v6, v6, [B

    fill-array-data v6, :array_2

    const/4 v7, 0x0

    move-object v8, v7

    const/4 v7, 0x0

    :goto_1
    :try_start_1
    array-length v9, v1

    if-ge v7, v9, :cond_4

    array-length v9, v1

    sub-int/2addr v9, v7

    invoke-virtual {p0, v1, v7, v9}, Lcom/starmicronics/stario/a;->readPort([BII)I

    move-result v9

    add-int/2addr v7, v9

    array-length v9, v5

    array-length v10, v6

    add-int/2addr v9, v10

    if-gt v9, v7, :cond_1

    new-array v8, v7, [B

    invoke-static {v1, v2, v8, v2, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-static {v8, v5, v6}, Lcom/starmicronics/stario/f;->a([B[B[B)[B

    move-result-object v8
    :try_end_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    if-eqz v8, :cond_2

    goto :goto_2

    :cond_2
    const-wide/16 v9, 0x32

    :try_start_2
    invoke-static {v9, v10}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_0
    :try_start_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    sub-long/2addr v9, v3

    int-to-long v11, v0

    cmp-long v13, v9, v11

    if-gtz v13, :cond_3

    goto :goto_1

    :cond_3
    new-instance v0, Ljava/util/concurrent/TimeoutException;

    const-string v1, "There was no response of the device within the timeout period."

    invoke-direct {v0, v1}, Ljava/util/concurrent/TimeoutException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_3
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_3 .. :try_end_3} :catch_1

    :cond_4
    :goto_2
    if-eqz v8, :cond_5

    invoke-static {v8}, Lcom/starmicronics/stario/f;->b([B)Ljava/util/Map;

    move-result-object v0

    sget-object v1, Lcom/starmicronics/stario/f$a;->a:Lcom/starmicronics/stario/f$a;

    invoke-virtual {v1}, Lcom/starmicronics/stario/f$a;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/starmicronics/stario/a;->n:Ljava/lang/String;

    sget-object v1, Lcom/starmicronics/stario/f$a;->b:Lcom/starmicronics/stario/f$a;

    invoke-virtual {v1}, Lcom/starmicronics/stario/f$a;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/starmicronics/stario/a;->o:Ljava/lang/String;

    return-void

    :cond_5
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v1, "Failed to parse model and version"

    invoke-direct {v0, v1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_1
    move-exception v0

    new-instance v1, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {v0}, Lcom/starmicronics/stario/StarIOPortException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v1

    :catch_2
    move-exception v0

    new-instance v1, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {v0}, Ljava/util/concurrent/TimeoutException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v1

    :catch_3
    move-exception v0

    new-instance v1, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {v0}, Lcom/starmicronics/stario/StarIOPortException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v1

    :array_0
    .array-data 1
        0x1bt
        0x23t
        0x2at
        0xat
        0x0t
    .end array-data

    nop

    :array_1
    .array-data 1
        0x1bt
        0x23t
        0x2at
        0x2ct
    .end array-data

    :array_2
    .array-data 1
        0xat
        0x0t
    .end array-data
.end method

.method private static c(Ljava/lang/String;)Z
    .locals 1

    invoke-virtual {p0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object p0

    const-string v0, "00:12:F3"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "00:15:0E"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "8C:DE:52"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "34:81:F4"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "00:11:62"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method private static d(Ljava/lang/String;)I
    .locals 1

    const-string v0, "d[0-9]+"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object p0

    invoke-virtual {p0}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object p0

    const-string v0, "[0-9]+"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object p0

    invoke-virtual {p0}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p0

    const/16 v0, 0xff

    if-lt v0, p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, -0x1

    :goto_0
    return p0
.end method

.method private d()V
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    const/4 v0, 0x6

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    :try_start_0
    array-length v1, v0

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2, v1}, Lcom/starmicronics/stario/a;->writePort([BII)V
    :try_end_0
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_0 .. :try_end_0} :catch_3

    iget v0, p0, Lcom/starmicronics/stario/a;->d:I

    const/16 v1, 0x2710

    if-le v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/16 v0, 0x2710

    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    const/16 v1, 0x64

    new-array v1, v1, [B

    const/4 v5, 0x0

    :catch_0
    :goto_1
    :try_start_1
    iget-object v6, p0, Lcom/starmicronics/stario/a;->g:Ljava/io/InputStream;

    invoke-virtual {v6}, Ljava/io/InputStream;->available()I

    move-result v6

    if-lez v6, :cond_6

    iget-object v6, p0, Lcom/starmicronics/stario/a;->g:Ljava/io/InputStream;

    array-length v7, v1

    sub-int/2addr v7, v5

    invoke-virtual {v6, v1, v5, v7}, Ljava/io/InputStream;->read([BII)I

    move-result v6
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_1 .. :try_end_1} :catch_1

    const/4 v7, 0x7

    if-lt v6, v7, :cond_5

    const/4 v0, 0x0

    :goto_2
    const/4 v3, 0x1

    if-lt v6, v7, :cond_2

    aget-byte v4, v1, v0

    const/16 v5, 0x1b

    if-ne v4, v5, :cond_1

    add-int/lit8 v4, v0, 0x1

    aget-byte v4, v1, v4

    const/16 v5, 0x23

    if-ne v4, v5, :cond_1

    add-int/lit8 v4, v0, 0x2

    aget-byte v4, v1, v4

    const/16 v5, 0x2c

    if-ne v4, v5, :cond_1

    add-int/lit8 v4, v0, 0x3

    aget-byte v4, v1, v4

    const/16 v5, 0x31

    if-ne v4, v5, :cond_1

    add-int/lit8 v4, v0, 0x5

    aget-byte v4, v1, v4

    const/16 v5, 0xa

    if-ne v4, v5, :cond_1

    add-int/lit8 v4, v0, 0x6

    aget-byte v4, v1, v4

    if-nez v4, :cond_1

    const/4 v4, 0x1

    goto :goto_3

    :cond_1
    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v6, v6, -0x1

    goto :goto_2

    :cond_2
    const/4 v4, 0x0

    :goto_3
    if-ne v4, v3, :cond_4

    new-array v3, v3, [B

    add-int/lit8 v0, v0, 0x4

    aget-byte v0, v1, v0

    aput-byte v0, v3, v2

    iget v0, p0, Lcom/starmicronics/stario/a;->d:I

    invoke-static {v3, v0}, Lcom/starmicronics/stario/f;->a([BI)I

    move-result v0

    if-eqz v0, :cond_3

    iput v0, p0, Lcom/starmicronics/stario/a;->e:I

    :cond_3
    return-void

    :cond_4
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v1, "Failed to parse dip-switch1 condition."

    invoke-direct {v0, v1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    add-int/2addr v5, v6

    :cond_6
    :try_start_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_2 .. :try_end_2} :catch_1

    sub-long/2addr v6, v3

    int-to-long v8, v0

    cmp-long v10, v6, v8

    if-gtz v10, :cond_7

    const-wide/16 v6, 0x64

    :try_start_3
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    :cond_7
    :try_start_4
    new-instance v0, Ljava/util/concurrent/TimeoutException;

    const-string v1, "There was no response of the device within the timeout period."

    invoke-direct {v0, v1}, Ljava/util/concurrent/TimeoutException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_4 .. :try_end_4} :catch_1

    :catch_1
    move-exception v0

    new-instance v1, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {v0}, Ljava/util/concurrent/TimeoutException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v1

    :catch_2
    move-exception v0

    new-instance v1, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v1

    :catch_3
    move-exception v0

    new-instance v1, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {v0}, Lcom/starmicronics/stario/StarIOPortException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v1

    nop

    :array_0
    .array-data 1
        0x1bt
        0x23t
        0x2ct
        0x31t
        0xat
        0x0t
    .end array-data
.end method

.method private e()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getBondedDevices()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->toArray()[Ljava/lang/Object;

    move-result-object v1

    array-length v1, v1

    new-array v1, v1, [Landroid/bluetooth/BluetoothDevice;

    invoke-interface {v0, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/bluetooth/BluetoothDevice;

    const/4 v1, 0x0

    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_1

    aget-object v2, v0, v1

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/starmicronics/stario/a;->c(Ljava/lang/String;)Z

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "BT:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/starmicronics/stario/a;->b:Ljava/lang/String;

    return-void

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v1, "Cannot find bluetooth printer"

    invoke-direct {v0, v1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private f()V
    .locals 18
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    move-object/from16 v1, p0

    iget-object v2, v1, Lcom/starmicronics/stario/a;->f:Landroid/bluetooth/BluetoothSocket;

    if-eqz v2, :cond_0

    return-void

    :cond_0
    :try_start_0
    invoke-direct/range {p0 .. p0}, Lcom/starmicronics/stario/a;->g()V
    :try_end_0
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_0 .. :try_end_0} :catch_1b
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1a
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_19
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_18
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_17
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_16
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_15
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_14

    :try_start_1
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v3

    if-eqz v3, :cond_16

    iget-boolean v4, v1, Lcom/starmicronics/stario/a;->p:Z
    :try_end_1
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_1 .. :try_end_1} :catch_1b
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_13
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_12
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_18
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_11
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_10
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_15
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_14

    const-wide/16 v5, 0x64

    const-string v7, "createInsecureRfcommSocketToServiceRecord"

    const-string v8, "Cannot find printer"

    const/16 v9, 0xc

    const-string v10, "00001101-0000-1000-8000-00805F9B34FB"

    const/4 v11, 0x0

    const/4 v12, 0x3

    const/4 v13, 0x1

    if-nez v4, :cond_a

    :try_start_2
    invoke-virtual {v3}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    move-result v4

    if-ne v4, v9, :cond_9

    iget-object v4, v1, Lcom/starmicronics/stario/a;->b:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-ne v4, v12, :cond_1

    invoke-direct/range {p0 .. p0}, Lcom/starmicronics/stario/a;->e()V

    :cond_1
    invoke-virtual {v3}, Landroid/bluetooth/BluetoothAdapter;->getBondedDevices()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->toArray()[Ljava/lang/Object;

    move-result-object v9

    array-length v9, v9

    new-array v9, v9, [Landroid/bluetooth/BluetoothDevice;

    invoke-interface {v4, v9}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Landroid/bluetooth/BluetoothDevice;

    const/4 v9, 0x0

    :goto_0
    array-length v14, v4

    if-ge v9, v14, :cond_3

    aget-object v14, v4, v9

    invoke-virtual {v14}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v14

    iget-object v15, v1, Lcom/starmicronics/stario/a;->b:Ljava/lang/String;

    invoke-virtual {v15, v12}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_2

    aget-object v14, v4, v9

    invoke-virtual {v14}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Lcom/starmicronics/stario/a;->c(Ljava/lang/String;)Z

    move-result v14

    if-ne v14, v13, :cond_2

    aget-object v9, v4, v9

    goto :goto_1

    :cond_2
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    :cond_3
    const/4 v9, 0x0

    :goto_1
    if-nez v9, :cond_5

    iget-object v14, v1, Lcom/starmicronics/stario/a;->b:Ljava/lang/String;

    invoke-virtual {v14, v12}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Landroid/bluetooth/BluetoothAdapter;->checkBluetoothAddress(Ljava/lang/String;)Z

    move-result v14

    if-ne v14, v13, :cond_5

    const/4 v14, 0x0

    :goto_2
    array-length v15, v4

    if-ge v14, v15, :cond_5

    aget-object v15, v4, v14

    invoke-virtual {v15}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v15

    iget-object v2, v1, Lcom/starmicronics/stario/a;->b:Ljava/lang/String;

    invoke-virtual {v2, v12}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v15, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    aget-object v2, v4, v14

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/starmicronics/stario/a;->c(Ljava/lang/String;)Z

    move-result v2

    if-ne v2, v13, :cond_4

    aget-object v9, v4, v14

    goto :goto_3

    :cond_4
    add-int/lit8 v14, v14, 0x1

    goto :goto_2

    :cond_5
    :goto_3
    if-eqz v9, :cond_8

    iget-boolean v2, v1, Lcom/starmicronics/stario/a;->k:Z

    if-eqz v2, :cond_6

    invoke-static {v10}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v9, v2}, Landroid/bluetooth/BluetoothDevice;->createRfcommSocketToServiceRecord(Ljava/util/UUID;)Landroid/bluetooth/BluetoothSocket;

    move-result-object v2

    :goto_4
    iput-object v2, v1, Lcom/starmicronics/stario/a;->f:Landroid/bluetooth/BluetoothSocket;

    goto :goto_5

    :cond_6
    invoke-virtual {v9}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    new-array v4, v13, [Ljava/lang/Class;

    const-class v8, Ljava/util/UUID;

    aput-object v8, v4, v11

    invoke-virtual {v2, v7, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    new-array v4, v13, [Ljava/lang/Object;

    invoke-static {v10}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v7

    aput-object v7, v4, v11

    invoke-virtual {v2, v9, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/bluetooth/BluetoothSocket;

    goto :goto_4

    :goto_5
    invoke-virtual {v3}, Landroid/bluetooth/BluetoothAdapter;->isDiscovering()Z

    move-result v2

    if-ne v2, v13, :cond_7

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothAdapter;->cancelDiscovery()Z

    :cond_7
    iget-object v2, v1, Lcom/starmicronics/stario/a;->f:Landroid/bluetooth/BluetoothSocket;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothSocket;->connect()V

    invoke-static {v5, v6}, Ljava/lang/Thread;->sleep(J)V

    iget-object v2, v1, Lcom/starmicronics/stario/a;->f:Landroid/bluetooth/BluetoothSocket;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothSocket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v2

    iput-object v2, v1, Lcom/starmicronics/stario/a;->h:Ljava/io/OutputStream;

    iget-object v2, v1, Lcom/starmicronics/stario/a;->f:Landroid/bluetooth/BluetoothSocket;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothSocket;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    iput-object v2, v1, Lcom/starmicronics/stario/a;->g:Ljava/io/InputStream;

    goto/16 :goto_12

    :cond_8
    new-instance v2, Lcom/starmicronics/stario/StarIOPortException;

    invoke-direct {v2, v8}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_9
    new-instance v2, Lcom/starmicronics/stario/StarIOPortException;

    const-string v3, "bluetooth adapter is off"

    invoke-direct {v2, v3}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_a
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14
    :try_end_2
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_2 .. :try_end_2} :catch_1b
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_13
    .catch Ljava/lang/SecurityException; {:try_start_2 .. :try_end_2} :catch_12
    .catch Ljava/lang/NoSuchMethodException; {:try_start_2 .. :try_end_2} :catch_18
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_11
    .catch Ljava/lang/IllegalAccessException; {:try_start_2 .. :try_end_2} :catch_10
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_2 .. :try_end_2} :catch_15
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_14

    :goto_6
    :try_start_3
    invoke-virtual {v3}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    move-result v2

    if-ne v2, v9, :cond_14

    invoke-virtual {v3}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_13

    iget-object v2, v1, Lcom/starmicronics/stario/a;->b:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2
    :try_end_3
    .catch Lcom/starmicronics/stario/a$a; {:try_start_3 .. :try_end_3} :catch_f
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_8
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_3 .. :try_end_3} :catch_1b
    .catch Ljava/lang/SecurityException; {:try_start_3 .. :try_end_3} :catch_12
    .catch Ljava/lang/NoSuchMethodException; {:try_start_3 .. :try_end_3} :catch_18
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_11
    .catch Ljava/lang/IllegalAccessException; {:try_start_3 .. :try_end_3} :catch_10
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_3 .. :try_end_3} :catch_15
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_14

    if-ne v2, v12, :cond_b

    :try_start_4
    invoke-direct/range {p0 .. p0}, Lcom/starmicronics/stario/a;->e()V
    :try_end_4
    .catch Lcom/starmicronics/stario/a$a; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_4 .. :try_end_4} :catch_1b
    .catch Ljava/lang/SecurityException; {:try_start_4 .. :try_end_4} :catch_12
    .catch Ljava/lang/NoSuchMethodException; {:try_start_4 .. :try_end_4} :catch_18
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_11
    .catch Ljava/lang/IllegalAccessException; {:try_start_4 .. :try_end_4} :catch_10
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_4 .. :try_end_4} :catch_15
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_14

    goto :goto_7

    :catch_0
    move-exception v0

    move-object v2, v0

    move-wide v4, v5

    goto/16 :goto_e

    :catch_1
    move-exception v0

    move-object v2, v0

    move-wide v4, v5

    goto/16 :goto_10

    :cond_b
    :goto_7
    :try_start_5
    invoke-virtual {v3}, Landroid/bluetooth/BluetoothAdapter;->getBondedDevices()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->toArray()[Ljava/lang/Object;

    move-result-object v4

    array-length v4, v4

    new-array v4, v4, [Landroid/bluetooth/BluetoothDevice;

    invoke-interface {v2, v4}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Landroid/bluetooth/BluetoothDevice;

    const/4 v4, 0x0

    :goto_8
    array-length v9, v2
    :try_end_5
    .catch Lcom/starmicronics/stario/a$a; {:try_start_5 .. :try_end_5} :catch_f
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_8
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_5 .. :try_end_5} :catch_1b
    .catch Ljava/lang/SecurityException; {:try_start_5 .. :try_end_5} :catch_12
    .catch Ljava/lang/NoSuchMethodException; {:try_start_5 .. :try_end_5} :catch_18
    .catch Ljava/lang/IllegalArgumentException; {:try_start_5 .. :try_end_5} :catch_11
    .catch Ljava/lang/IllegalAccessException; {:try_start_5 .. :try_end_5} :catch_10
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_5 .. :try_end_5} :catch_15
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_14

    if-ge v4, v9, :cond_d

    :try_start_6
    aget-object v9, v2, v4

    invoke-virtual {v9}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v9
    :try_end_6
    .catch Lcom/starmicronics/stario/a$a; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_6 .. :try_end_6} :catch_1b
    .catch Ljava/lang/SecurityException; {:try_start_6 .. :try_end_6} :catch_12
    .catch Ljava/lang/NoSuchMethodException; {:try_start_6 .. :try_end_6} :catch_18
    .catch Ljava/lang/IllegalArgumentException; {:try_start_6 .. :try_end_6} :catch_11
    .catch Ljava/lang/IllegalAccessException; {:try_start_6 .. :try_end_6} :catch_10
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_6 .. :try_end_6} :catch_15
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_14

    :try_start_7
    iget-object v5, v1, Lcom/starmicronics/stario/a;->b:Ljava/lang/String;

    invoke-virtual {v5, v12}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v9, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_c

    aget-object v5, v2, v4

    invoke-virtual {v5}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/starmicronics/stario/a;->c(Ljava/lang/String;)Z

    move-result v5

    if-ne v5, v13, :cond_c

    aget-object v4, v2, v4

    goto :goto_9

    :cond_c
    add-int/lit8 v4, v4, 0x1

    const-wide/16 v5, 0x64

    goto :goto_8

    :cond_d
    const/4 v4, 0x0

    :goto_9
    if-nez v4, :cond_f

    iget-object v5, v1, Lcom/starmicronics/stario/a;->b:Ljava/lang/String;

    invoke-virtual {v5, v12}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/bluetooth/BluetoothAdapter;->checkBluetoothAddress(Ljava/lang/String;)Z

    move-result v5

    if-ne v5, v13, :cond_f

    const/4 v5, 0x0

    :goto_a
    array-length v6, v2

    if-ge v5, v6, :cond_f

    aget-object v6, v2, v5

    invoke-virtual {v6}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v6

    iget-object v9, v1, Lcom/starmicronics/stario/a;->b:Ljava/lang/String;

    invoke-virtual {v9, v12}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_e

    aget-object v6, v2, v5

    invoke-virtual {v6}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/starmicronics/stario/a;->c(Ljava/lang/String;)Z

    move-result v6

    if-ne v6, v13, :cond_e

    aget-object v4, v2, v5
    :try_end_7
    .catch Lcom/starmicronics/stario/a$a; {:try_start_7 .. :try_end_7} :catch_3
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_7 .. :try_end_7} :catch_1b
    .catch Ljava/lang/SecurityException; {:try_start_7 .. :try_end_7} :catch_12
    .catch Ljava/lang/NoSuchMethodException; {:try_start_7 .. :try_end_7} :catch_18
    .catch Ljava/lang/IllegalArgumentException; {:try_start_7 .. :try_end_7} :catch_11
    .catch Ljava/lang/IllegalAccessException; {:try_start_7 .. :try_end_7} :catch_10
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_7 .. :try_end_7} :catch_15
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_7} :catch_14

    goto :goto_b

    :cond_e
    add-int/lit8 v5, v5, 0x1

    goto :goto_a

    :catch_2
    move-exception v0

    move-object v2, v0

    const-wide/16 v4, 0x64

    goto/16 :goto_e

    :catch_3
    move-exception v0

    move-object v2, v0

    const-wide/16 v4, 0x64

    goto/16 :goto_10

    :cond_f
    :goto_b
    if-eqz v4, :cond_12

    :try_start_8
    iget-boolean v2, v1, Lcom/starmicronics/stario/a;->k:Z
    :try_end_8
    .catch Lcom/starmicronics/stario/a$a; {:try_start_8 .. :try_end_8} :catch_5
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_8 .. :try_end_8} :catch_1b
    .catch Ljava/lang/SecurityException; {:try_start_8 .. :try_end_8} :catch_12
    .catch Ljava/lang/NoSuchMethodException; {:try_start_8 .. :try_end_8} :catch_18
    .catch Ljava/lang/IllegalArgumentException; {:try_start_8 .. :try_end_8} :catch_11
    .catch Ljava/lang/IllegalAccessException; {:try_start_8 .. :try_end_8} :catch_10
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_8 .. :try_end_8} :catch_15
    .catch Ljava/lang/InterruptedException; {:try_start_8 .. :try_end_8} :catch_14

    if-eqz v2, :cond_10

    :try_start_9
    invoke-static {v10}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/bluetooth/BluetoothDevice;->createRfcommSocketToServiceRecord(Ljava/util/UUID;)Landroid/bluetooth/BluetoothSocket;

    move-result-object v2

    iput-object v2, v1, Lcom/starmicronics/stario/a;->f:Landroid/bluetooth/BluetoothSocket;
    :try_end_9
    .catch Lcom/starmicronics/stario/a$a; {:try_start_9 .. :try_end_9} :catch_3
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_2
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_9 .. :try_end_9} :catch_1b
    .catch Ljava/lang/SecurityException; {:try_start_9 .. :try_end_9} :catch_12
    .catch Ljava/lang/NoSuchMethodException; {:try_start_9 .. :try_end_9} :catch_18
    .catch Ljava/lang/IllegalArgumentException; {:try_start_9 .. :try_end_9} :catch_11
    .catch Ljava/lang/IllegalAccessException; {:try_start_9 .. :try_end_9} :catch_10
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_9 .. :try_end_9} :catch_15
    .catch Ljava/lang/InterruptedException; {:try_start_9 .. :try_end_9} :catch_14

    goto :goto_c

    :cond_10
    :try_start_a
    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    new-array v5, v13, [Ljava/lang/Class;

    const-class v6, Ljava/util/UUID;

    aput-object v6, v5, v11

    invoke-virtual {v2, v7, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    new-array v5, v13, [Ljava/lang/Object;

    invoke-static {v10}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v6

    aput-object v6, v5, v11

    invoke-virtual {v2, v4, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/bluetooth/BluetoothSocket;

    iput-object v2, v1, Lcom/starmicronics/stario/a;->f:Landroid/bluetooth/BluetoothSocket;

    :goto_c
    invoke-virtual {v3}, Landroid/bluetooth/BluetoothAdapter;->isDiscovering()Z

    move-result v2
    :try_end_a
    .catch Lcom/starmicronics/stario/a$a; {:try_start_a .. :try_end_a} :catch_5
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_4
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_a .. :try_end_a} :catch_1b
    .catch Ljava/lang/SecurityException; {:try_start_a .. :try_end_a} :catch_12
    .catch Ljava/lang/NoSuchMethodException; {:try_start_a .. :try_end_a} :catch_18
    .catch Ljava/lang/IllegalArgumentException; {:try_start_a .. :try_end_a} :catch_11
    .catch Ljava/lang/IllegalAccessException; {:try_start_a .. :try_end_a} :catch_10
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_a .. :try_end_a} :catch_15
    .catch Ljava/lang/InterruptedException; {:try_start_a .. :try_end_a} :catch_14

    if-ne v2, v13, :cond_11

    :try_start_b
    invoke-virtual {v3}, Landroid/bluetooth/BluetoothAdapter;->cancelDiscovery()Z
    :try_end_b
    .catch Lcom/starmicronics/stario/a$a; {:try_start_b .. :try_end_b} :catch_3
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_2
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_b .. :try_end_b} :catch_1b
    .catch Ljava/lang/SecurityException; {:try_start_b .. :try_end_b} :catch_12
    .catch Ljava/lang/NoSuchMethodException; {:try_start_b .. :try_end_b} :catch_18
    .catch Ljava/lang/IllegalArgumentException; {:try_start_b .. :try_end_b} :catch_11
    .catch Ljava/lang/IllegalAccessException; {:try_start_b .. :try_end_b} :catch_10
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_b .. :try_end_b} :catch_15
    .catch Ljava/lang/InterruptedException; {:try_start_b .. :try_end_b} :catch_14

    :cond_11
    :try_start_c
    iget-object v2, v1, Lcom/starmicronics/stario/a;->f:Landroid/bluetooth/BluetoothSocket;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothSocket;->connect()V
    :try_end_c
    .catch Lcom/starmicronics/stario/a$a; {:try_start_c .. :try_end_c} :catch_5
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_4
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_c .. :try_end_c} :catch_1b
    .catch Ljava/lang/SecurityException; {:try_start_c .. :try_end_c} :catch_12
    .catch Ljava/lang/NoSuchMethodException; {:try_start_c .. :try_end_c} :catch_18
    .catch Ljava/lang/IllegalArgumentException; {:try_start_c .. :try_end_c} :catch_11
    .catch Ljava/lang/IllegalAccessException; {:try_start_c .. :try_end_c} :catch_10
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_c .. :try_end_c} :catch_15
    .catch Ljava/lang/InterruptedException; {:try_start_c .. :try_end_c} :catch_14

    const-wide/16 v4, 0x64

    :try_start_d
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V

    iget-object v2, v1, Lcom/starmicronics/stario/a;->f:Landroid/bluetooth/BluetoothSocket;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothSocket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v2

    iput-object v2, v1, Lcom/starmicronics/stario/a;->h:Ljava/io/OutputStream;

    iget-object v2, v1, Lcom/starmicronics/stario/a;->f:Landroid/bluetooth/BluetoothSocket;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothSocket;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    iput-object v2, v1, Lcom/starmicronics/stario/a;->g:Ljava/io/InputStream;

    goto/16 :goto_12

    :catch_4
    move-exception v0

    const-wide/16 v4, 0x64

    goto :goto_d

    :catch_5
    move-exception v0

    const-wide/16 v4, 0x64

    goto/16 :goto_f

    :cond_12
    const-wide/16 v4, 0x64

    new-instance v2, Lcom/starmicronics/stario/StarIOPortException;

    invoke-direct {v2, v8}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_13
    move-wide v4, v5

    new-instance v2, Lcom/starmicronics/stario/a$a;

    const-string v6, "Adapter failed: code 002"

    invoke-direct {v2, v1, v6}, Lcom/starmicronics/stario/a$a;-><init>(Lcom/starmicronics/stario/a;Ljava/lang/String;)V

    throw v2

    :cond_14
    move-wide v4, v5

    new-instance v2, Lcom/starmicronics/stario/a$a;

    const-string v6, "Adapter failed: code 001"

    invoke-direct {v2, v1, v6}, Lcom/starmicronics/stario/a$a;-><init>(Lcom/starmicronics/stario/a;Ljava/lang/String;)V

    throw v2
    :try_end_d
    .catch Lcom/starmicronics/stario/a$a; {:try_start_d .. :try_end_d} :catch_7
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_6
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_d .. :try_end_d} :catch_1b
    .catch Ljava/lang/SecurityException; {:try_start_d .. :try_end_d} :catch_12
    .catch Ljava/lang/NoSuchMethodException; {:try_start_d .. :try_end_d} :catch_18
    .catch Ljava/lang/IllegalArgumentException; {:try_start_d .. :try_end_d} :catch_11
    .catch Ljava/lang/IllegalAccessException; {:try_start_d .. :try_end_d} :catch_10
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_d .. :try_end_d} :catch_15
    .catch Ljava/lang/InterruptedException; {:try_start_d .. :try_end_d} :catch_14

    :catch_6
    move-exception v0

    goto :goto_d

    :catch_7
    move-exception v0

    goto :goto_f

    :catch_8
    move-exception v0

    move-wide v4, v5

    :goto_d
    move-object v2, v0

    :goto_e
    :try_start_e
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    iget-object v9, v1, Lcom/starmicronics/stario/a;->f:Landroid/bluetooth/BluetoothSocket;

    invoke-virtual {v9}, Landroid/bluetooth/BluetoothSocket;->close()V
    :try_end_e
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_e .. :try_end_e} :catch_1b
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_13
    .catch Ljava/lang/SecurityException; {:try_start_e .. :try_end_e} :catch_12
    .catch Ljava/lang/NoSuchMethodException; {:try_start_e .. :try_end_e} :catch_18
    .catch Ljava/lang/IllegalArgumentException; {:try_start_e .. :try_end_e} :catch_11
    .catch Ljava/lang/IllegalAccessException; {:try_start_e .. :try_end_e} :catch_10
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_e .. :try_end_e} :catch_15
    .catch Ljava/lang/InterruptedException; {:try_start_e .. :try_end_e} :catch_14

    const/4 v9, 0x0

    :try_start_f
    iput-object v9, v1, Lcom/starmicronics/stario/a;->f:Landroid/bluetooth/BluetoothSocket;

    iput-object v9, v1, Lcom/starmicronics/stario/a;->h:Ljava/io/OutputStream;

    iput-object v9, v1, Lcom/starmicronics/stario/a;->g:Ljava/io/InputStream;
    :try_end_f
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_f .. :try_end_f} :catch_1b
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_e
    .catch Ljava/lang/SecurityException; {:try_start_f .. :try_end_f} :catch_d
    .catch Ljava/lang/NoSuchMethodException; {:try_start_f .. :try_end_f} :catch_c
    .catch Ljava/lang/IllegalArgumentException; {:try_start_f .. :try_end_f} :catch_b
    .catch Ljava/lang/IllegalAccessException; {:try_start_f .. :try_end_f} :catch_a
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_f .. :try_end_f} :catch_9
    .catch Ljava/lang/InterruptedException; {:try_start_f .. :try_end_f} :catch_14

    goto :goto_11

    :catch_9
    move-exception v0

    move-object v2, v0

    move-object v3, v9

    goto/16 :goto_13

    :catch_a
    move-exception v0

    move-object v2, v0

    move-object v3, v9

    goto/16 :goto_14

    :catch_b
    move-exception v0

    move-object v2, v0

    move-object v3, v9

    goto/16 :goto_15

    :catch_c
    move-object v3, v9

    goto/16 :goto_16

    :catch_d
    move-exception v0

    move-object v2, v0

    move-object v3, v9

    goto/16 :goto_17

    :catch_e
    move-exception v0

    move-object v2, v0

    move-object v3, v9

    goto/16 :goto_18

    :catch_f
    move-exception v0

    move-wide v4, v5

    :goto_f
    move-object v2, v0

    :goto_10
    :try_start_10
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2}, Lcom/starmicronics/stario/a$a;->getMessage()Ljava/lang/String;

    move-result-object v2

    :goto_11
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    sub-long v16, v16, v14

    iget v9, v1, Lcom/starmicronics/stario/a;->q:I

    int-to-long v4, v9

    cmp-long v9, v16, v4

    if-gtz v9, :cond_15

    const-wide/16 v4, 0x1f4

    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V

    const-wide/16 v5, 0x64

    const/16 v9, 0xc

    goto/16 :goto_6

    :cond_15
    new-instance v3, Lcom/starmicronics/stario/StarIOPortException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Failed to connect port within the timeout period."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_16
    new-instance v2, Lcom/starmicronics/stario/StarIOPortException;

    const-string v3, "No bluetooth adapter found"

    invoke-direct {v2, v3}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_10
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_10 .. :try_end_10} :catch_1b
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_13
    .catch Ljava/lang/SecurityException; {:try_start_10 .. :try_end_10} :catch_12
    .catch Ljava/lang/NoSuchMethodException; {:try_start_10 .. :try_end_10} :catch_18
    .catch Ljava/lang/IllegalArgumentException; {:try_start_10 .. :try_end_10} :catch_11
    .catch Ljava/lang/IllegalAccessException; {:try_start_10 .. :try_end_10} :catch_10
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_10 .. :try_end_10} :catch_15
    .catch Ljava/lang/InterruptedException; {:try_start_10 .. :try_end_10} :catch_14

    :catch_10
    move-exception v0

    move-object v2, v0

    const/4 v3, 0x0

    goto :goto_14

    :catch_11
    move-exception v0

    move-object v2, v0

    const/4 v3, 0x0

    goto :goto_15

    :catch_12
    move-exception v0

    move-object v2, v0

    const/4 v3, 0x0

    goto :goto_17

    :catch_13
    move-exception v0

    move-object v2, v0

    const/4 v3, 0x0

    goto :goto_18

    :catch_14
    :goto_12
    return-void

    :catch_15
    move-exception v0

    move-object v2, v0

    const/4 v3, 0x0

    :goto_13
    iput-object v3, v1, Lcom/starmicronics/stario/a;->f:Landroid/bluetooth/BluetoothSocket;

    new-instance v3, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {v2}, Ljava/lang/reflect/InvocationTargetException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v3

    :catch_16
    move-exception v0

    const/4 v3, 0x0

    move-object v2, v0

    :goto_14
    iput-object v3, v1, Lcom/starmicronics/stario/a;->f:Landroid/bluetooth/BluetoothSocket;

    new-instance v3, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {v2}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v3

    :catch_17
    move-exception v0

    const/4 v3, 0x0

    move-object v2, v0

    :goto_15
    iput-object v3, v1, Lcom/starmicronics/stario/a;->f:Landroid/bluetooth/BluetoothSocket;

    new-instance v3, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {v2}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v3

    :catch_18
    const/4 v3, 0x0

    :goto_16
    iput-object v3, v1, Lcom/starmicronics/stario/a;->f:Landroid/bluetooth/BluetoothSocket;

    new-instance v2, Lcom/starmicronics/stario/StarIOPortException;

    const-string v3, "Need android version 3.1 to use unsecure method"

    invoke-direct {v2, v3}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v2

    :catch_19
    move-exception v0

    const/4 v3, 0x0

    move-object v2, v0

    :goto_17
    iput-object v3, v1, Lcom/starmicronics/stario/a;->f:Landroid/bluetooth/BluetoothSocket;

    new-instance v3, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {v2}, Ljava/lang/SecurityException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v3

    :catch_1a
    move-exception v0

    const/4 v3, 0x0

    move-object v2, v0

    :goto_18
    iput-object v3, v1, Lcom/starmicronics/stario/a;->f:Landroid/bluetooth/BluetoothSocket;

    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    new-instance v3, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v3

    :catch_1b
    move-exception v0

    move-object v2, v0

    throw v2
.end method

.method private g()V
    .locals 8

    iget-object v0, p0, Lcom/starmicronics/stario/a;->c:Ljava/lang/String;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "PORTABLE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    const-string v1, "f"

    const-string v2, ";"

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-eqz v0, :cond_2

    iput-boolean v4, p0, Lcom/starmicronics/stario/a;->k:Z

    iget-object v0, p0, Lcom/starmicronics/stario/a;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    array-length v2, v0

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v2, :cond_5

    aget-object v6, v0, v5

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v7

    if-ne v7, v4, :cond_1

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-ne v7, v4, :cond_0

    iput-boolean v4, p0, Lcom/starmicronics/stario/a;->a:Z

    goto :goto_1

    :cond_0
    const-string v7, "u"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-ne v6, v4, :cond_1

    iput-boolean v3, p0, Lcom/starmicronics/stario/a;->k:Z

    :cond_1
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/starmicronics/stario/a;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-ne v0, v4, :cond_5

    iget-object v0, p0, Lcom/starmicronics/stario/a;->c:Ljava/lang/String;

    const/16 v2, 0x3b

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    add-int/2addr v0, v4

    iget-object v2, p0, Lcom/starmicronics/stario/a;->c:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-ne v1, v4, :cond_3

    iput-boolean v4, p0, Lcom/starmicronics/stario/a;->a:Z

    :cond_3
    const-string v1, "p"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-ne v1, v4, :cond_4

    iput-boolean v4, p0, Lcom/starmicronics/stario/a;->k:Z

    :cond_4
    const-string v1, "d"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-ne v1, v4, :cond_5

    invoke-static {v0}, Lcom/starmicronics/stario/a;->d(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/starmicronics/stario/a;->r:I

    :cond_5
    iget v0, p0, Lcom/starmicronics/stario/a;->d:I

    const/16 v1, 0x2710

    if-le v0, v1, :cond_6

    goto :goto_2

    :cond_6
    const/16 v0, 0x2710

    :goto_2
    iget-object v1, p0, Lcom/starmicronics/stario/a;->c:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/starmicronics/stario/f;->a(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/starmicronics/stario/a;->q:I

    iget v0, p0, Lcom/starmicronics/stario/a;->q:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_7

    const/4 v3, 0x1

    :cond_7
    iput-boolean v3, p0, Lcom/starmicronics/stario/a;->p:Z

    return-void
.end method

.method private h()Lcom/starmicronics/stario/StarPrinterStatus;
    .locals 20
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    move-object/from16 v1, p0

    :try_start_0
    invoke-direct/range {p0 .. p0}, Lcom/starmicronics/stario/a;->f()V

    const-string v0, "StarLine"

    iget-object v2, v1, Lcom/starmicronics/stario/a;->l:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const-wide/16 v2, 0x64

    const/16 v4, 0x8

    const/4 v5, 0x5

    const/16 v6, 0x1d

    const/16 v7, 0x9

    const/4 v8, 0x7

    const/4 v9, 0x6

    const/4 v10, 0x2

    const/16 v11, 0x1b

    const/4 v12, 0x3

    const/4 v13, 0x4

    const/4 v14, 0x1

    const/4 v15, 0x0

    if-eqz v0, :cond_9

    new-array v0, v7, [B

    aput-byte v11, v0, v15

    aput-byte v6, v0, v14

    aput-byte v12, v0, v10

    aput-byte v13, v0, v12

    aput-byte v15, v0, v13

    aput-byte v15, v0, v5

    aput-byte v11, v0, v9

    aput-byte v9, v0, v8

    aput-byte v14, v0, v4

    array-length v4, v0

    invoke-virtual {v1, v0, v15, v4}, Lcom/starmicronics/stario/a;->writePort([BII)V

    iget v0, v1, Lcom/starmicronics/stario/a;->d:I

    const/16 v4, 0x2710

    if-le v0, v4, :cond_0

    iget v0, v1, Lcom/starmicronics/stario/a;->d:I

    int-to-long v4, v0

    goto :goto_0

    :cond_0
    const-wide/16 v4, 0x2710

    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    new-instance v0, Lcom/starmicronics/stario/StarPrinterStatus;

    invoke-direct {v0}, Lcom/starmicronics/stario/StarPrinterStatus;-><init>()V

    iget-object v9, v0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    array-length v9, v9

    new-array v9, v9, [B

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    :goto_1
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V

    array-length v13, v9

    sub-int/2addr v13, v10

    invoke-virtual {v1, v9, v10, v13}, Lcom/starmicronics/stario/a;->readPort([BII)I

    move-result v13

    add-int/2addr v13, v10

    if-eq v10, v13, :cond_1

    const/4 v10, 0x1

    goto :goto_2

    :cond_1
    const/4 v10, 0x0

    :goto_2
    if-eqz v13, :cond_2

    aget-byte v12, v9, v15

    invoke-static {v12}, Lcom/starmicronics/stario/f;->a(B)I

    move-result v12

    :cond_2
    const/16 v2, 0xf

    if-nez v12, :cond_3

    if-ge v2, v13, :cond_3

    add-int/lit8 v13, v13, -0xf

    invoke-static {v9, v13, v9, v15, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const/16 v13, 0xf

    :cond_3
    if-nez v12, :cond_6

    if-nez v10, :cond_6

    if-eqz v11, :cond_6

    :goto_3
    if-lez v2, :cond_6

    sub-int v3, v13, v2

    if-gez v3, :cond_4

    goto :goto_4

    :cond_4
    aget-byte v11, v9, v3

    invoke-static {v11}, Lcom/starmicronics/stario/f;->a(B)I

    move-result v11

    if-ne v2, v11, :cond_5

    invoke-static {v9, v3, v9, v15, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move v12, v2

    goto :goto_5

    :cond_5
    :goto_4
    add-int/lit8 v2, v2, -0x1

    goto :goto_3

    :cond_6
    move v2, v13

    :goto_5
    if-eqz v12, :cond_7

    if-gt v12, v2, :cond_7

    iget-object v2, v0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    invoke-static {v9, v15, v2, v15, v12}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput v12, v0, Lcom/starmicronics/stario/StarPrinterStatus;->rawLength:I

    iget v2, v0, Lcom/starmicronics/stario/StarPrinterStatus;->rawLength:I

    if-lt v2, v8, :cond_12

    invoke-static {v0}, Lcom/starmicronics/stario/f;->c(Lcom/starmicronics/stario/StarPrinterStatus;)V

    goto/16 :goto_b

    :cond_7
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    sub-long v18, v18, v6

    cmp-long v3, v18, v4

    if-gtz v3, :cond_8

    move v11, v10

    move v10, v2

    const-wide/16 v2, 0x64

    goto :goto_1

    :cond_8
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v2, "There was no response of the device within the timeout period."

    invoke-direct {v0, v2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_9
    const-string v0, "ESCPOS"

    iget-object v2, v1, Lcom/starmicronics/stario/a;->l:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    new-array v0, v7, [B

    aput-byte v11, v0, v15

    aput-byte v6, v0, v14

    aput-byte v12, v0, v10

    aput-byte v13, v0, v12

    aput-byte v15, v0, v13

    aput-byte v15, v0, v5

    const/16 v2, 0x10

    aput-byte v2, v0, v9

    aput-byte v13, v0, v8

    aput-byte v10, v0, v4

    array-length v3, v0

    invoke-virtual {v1, v0, v15, v3}, Lcom/starmicronics/stario/a;->writePort([BII)V

    invoke-direct/range {p0 .. p0}, Lcom/starmicronics/stario/a;->i()Lcom/starmicronics/stario/StarPrinterStatus;

    move-result-object v0

    iget v3, v0, Lcom/starmicronics/stario/StarPrinterStatus;->rawLength:I

    if-lt v3, v14, :cond_a

    const-string v3, "CoverOpen"

    invoke-static {v0, v3}, Lcom/starmicronics/stario/f;->a(Lcom/starmicronics/stario/StarPrinterStatus;Ljava/lang/String;)V

    iget-boolean v0, v0, Lcom/starmicronics/stario/StarPrinterStatus;->coverOpen:Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_6

    :cond_a
    const/4 v0, 0x0

    :goto_6
    const-wide/16 v16, 0x64

    :try_start_1
    invoke-static/range {v16 .. v17}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_1 .. :try_end_1} :catch_2

    :catch_0
    :try_start_2
    new-array v3, v7, [B

    aput-byte v11, v3, v15

    aput-byte v6, v3, v14

    aput-byte v12, v3, v10

    aput-byte v13, v3, v12

    aput-byte v15, v3, v13

    aput-byte v15, v3, v5

    aput-byte v2, v3, v9

    aput-byte v13, v3, v8

    aput-byte v13, v3, v4

    array-length v4, v3

    invoke-virtual {v1, v3, v15, v4}, Lcom/starmicronics/stario/a;->writePort([BII)V

    invoke-direct/range {p0 .. p0}, Lcom/starmicronics/stario/a;->i()Lcom/starmicronics/stario/StarPrinterStatus;

    move-result-object v3

    iget v4, v3, Lcom/starmicronics/stario/StarPrinterStatus;->rawLength:I

    if-lt v4, v14, :cond_b

    const-string v4, "PaperEmpty"

    invoke-static {v3, v4}, Lcom/starmicronics/stario/f;->a(Lcom/starmicronics/stario/StarPrinterStatus;Ljava/lang/String;)V

    iget-boolean v4, v3, Lcom/starmicronics/stario/StarPrinterStatus;->receiptPaperEmpty:Z

    if-ne v0, v14, :cond_c

    iput-boolean v14, v3, Lcom/starmicronics/stario/StarPrinterStatus;->coverOpen:Z
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_7

    :cond_b
    const/4 v4, 0x0

    :cond_c
    :goto_7
    const-wide/16 v16, 0x64

    :try_start_3
    invoke-static/range {v16 .. v17}, Ljava/lang/Thread;->sleep(J)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_3 .. :try_end_3} :catch_2

    :catch_1
    :try_start_4
    new-array v3, v7, [B

    aput-byte v11, v3, v15

    aput-byte v6, v3, v14

    aput-byte v12, v3, v10

    aput-byte v13, v3, v12

    aput-byte v15, v3, v13

    aput-byte v15, v3, v5

    aput-byte v2, v3, v9

    aput-byte v13, v3, v8

    const/16 v2, 0x8

    aput-byte v14, v3, v2

    array-length v2, v3

    invoke-virtual {v1, v3, v15, v2}, Lcom/starmicronics/stario/a;->writePort([BII)V

    invoke-direct/range {p0 .. p0}, Lcom/starmicronics/stario/a;->i()Lcom/starmicronics/stario/StarPrinterStatus;

    move-result-object v2

    iget v3, v2, Lcom/starmicronics/stario/StarPrinterStatus;->rawLength:I

    if-lt v3, v14, :cond_10

    const-string v3, "Online/CashDrawer"

    invoke-static {v2, v3}, Lcom/starmicronics/stario/f;->a(Lcom/starmicronics/stario/StarPrinterStatus;Ljava/lang/String;)V

    if-ne v0, v14, :cond_d

    if-ne v4, v14, :cond_d

    iput-boolean v14, v2, Lcom/starmicronics/stario/StarPrinterStatus;->coverOpen:Z

    :goto_8
    iput-boolean v14, v2, Lcom/starmicronics/stario/StarPrinterStatus;->receiptPaperEmpty:Z

    goto :goto_a

    :cond_d
    if-nez v0, :cond_e

    if-ne v4, v14, :cond_e

    iput-boolean v15, v2, Lcom/starmicronics/stario/StarPrinterStatus;->coverOpen:Z

    goto :goto_8

    :cond_e
    if-ne v0, v14, :cond_f

    if-nez v4, :cond_f

    iput-boolean v14, v2, Lcom/starmicronics/stario/StarPrinterStatus;->coverOpen:Z

    :goto_9
    iput-boolean v15, v2, Lcom/starmicronics/stario/StarPrinterStatus;->receiptPaperEmpty:Z

    goto :goto_a

    :cond_f
    iput-boolean v15, v2, Lcom/starmicronics/stario/StarPrinterStatus;->coverOpen:Z
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_9

    :cond_10
    :goto_a
    move-object v0, v2

    goto :goto_b

    :cond_11
    const/4 v0, 0x0

    :cond_12
    :goto_b
    return-object v0

    :catch_2
    move-exception v0

    new-instance v2, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {v0}, Lcom/starmicronics/stario/StarIOPortException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v2

    :catch_3
    move-exception v0

    new-instance v2, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method private i()Lcom/starmicronics/stario/StarPrinterStatus;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    new-instance v0, Lcom/starmicronics/stario/StarPrinterStatus;

    invoke-direct {v0}, Lcom/starmicronics/stario/StarPrinterStatus;-><init>()V

    const-wide/16 v1, 0x64

    :try_start_0
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    nop

    :goto_0
    iget-object v1, v0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    iget-object v2, v0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    array-length v2, v2

    const/4 v3, 0x0

    invoke-virtual {p0, v1, v3, v2}, Lcom/starmicronics/stario/a;->readPort([BII)I

    move-result v1

    iput v1, v0, Lcom/starmicronics/stario/StarPrinterStatus;->rawLength:I

    iget v1, v0, Lcom/starmicronics/stario/StarPrinterStatus;->rawLength:I

    const/16 v2, 0x8

    if-lt v1, v2, :cond_4

    const/4 v1, 0x0

    :goto_1
    iget v4, v0, Lcom/starmicronics/stario/StarPrinterStatus;->rawLength:I

    const/16 v5, 0x1b

    if-ge v1, v4, :cond_1

    iget-object v4, v0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    aget-byte v4, v4, v1

    if-ne v5, v4, :cond_0

    iget-object v1, v0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    sput-object v1, Lcom/starmicronics/stario/a;->m:[B

    goto :goto_2

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    :goto_2
    iget v1, v0, Lcom/starmicronics/stario/StarPrinterStatus;->rawLength:I

    if-le v1, v2, :cond_4

    const/4 v1, 0x0

    :goto_3
    iget v2, v0, Lcom/starmicronics/stario/StarPrinterStatus;->rawLength:I

    if-ge v1, v2, :cond_4

    iget-object v2, v0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    aget-byte v2, v2, v1

    const/4 v4, 0x1

    if-ne v5, v2, :cond_2

    add-int/lit8 v1, v1, 0x7

    goto :goto_4

    :cond_2
    iget-object v2, v0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    aget-byte v2, v2, v1

    const/16 v6, 0x12

    and-int/2addr v2, v6

    if-ne v6, v2, :cond_3

    new-array v2, v4, [B

    iget-object v4, v0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    aget-byte v1, v4, v1

    aput-byte v1, v2, v3

    iput-object v2, v0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    iget-object v1, v0, Lcom/starmicronics/stario/StarPrinterStatus;->raw:[B

    array-length v1, v1

    iput v1, v0, Lcom/starmicronics/stario/StarPrinterStatus;->rawLength:I

    goto :goto_5

    :cond_3
    :goto_4
    add-int/2addr v1, v4

    goto :goto_3

    :cond_4
    :goto_5
    return-object v0
.end method


# virtual methods
.method protected a()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    const/4 v0, 0x0

    :try_start_0
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    const/16 v2, 0x13

    const/4 v3, 0x1

    if-ne v1, v2, :cond_0

    :try_start_1
    const-class v1, Landroid/bluetooth/BluetoothSocket;

    const-string v2, "mPfd"

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    iget-object v2, p0, Lcom/starmicronics/stario/a;->f:Landroid/bluetooth/BluetoothSocket;

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/ParcelFileDescriptor;
    :try_end_1
    .catch Ljava/lang/NoSuchFieldException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    if-eqz v1, :cond_0

    const/4 v2, 0x0

    :try_start_2
    invoke-virtual {v1}, Landroid/os/ParcelFileDescriptor;->close()V

    iget-object v1, p0, Lcom/starmicronics/stario/a;->f:Landroid/bluetooth/BluetoothSocket;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothSocket;->close()V
    :try_end_2
    .catch Ljava/lang/NoSuchFieldException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    const/4 v3, 0x0

    goto :goto_0

    :catch_1
    nop

    :cond_0
    :goto_0
    if-eqz v3, :cond_1

    :try_start_3
    iget-object v1, p0, Lcom/starmicronics/stario/a;->h:Ljava/io/OutputStream;

    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    iget-object v1, p0, Lcom/starmicronics/stario/a;->g:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    iget-object v1, p0, Lcom/starmicronics/stario/a;->f:Landroid/bluetooth/BluetoothSocket;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothSocket;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    :cond_1
    const-wide/16 v1, 0x1f4

    :try_start_4
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V

    iput-object v0, p0, Lcom/starmicronics/stario/a;->f:Landroid/bluetooth/BluetoothSocket;
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_2

    :catch_2
    return-void

    :catch_3
    move-exception v1

    iput-object v0, p0, Lcom/starmicronics/stario/a;->f:Landroid/bluetooth/BluetoothSocket;

    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public beginCheckedBlock()Lcom/starmicronics/stario/StarPrinterStatus;
    .locals 23
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    move-object/from16 v1, p0

    const-string v0, "StarLine"

    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/starmicronics/stario/a;->retreiveStatus()Lcom/starmicronics/stario/StarPrinterStatus;

    move-result-object v2

    iget-boolean v3, v2, Lcom/starmicronics/stario/StarPrinterStatus;->offline:Z
    :try_end_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_3

    const-string v4, "Printer is offline"

    const/4 v5, 0x1

    if-eq v3, v5, :cond_10

    :try_start_1
    iget-object v3, v1, Lcom/starmicronics/stario/a;->l:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-boolean v2, v2, Lcom/starmicronics/stario/StarPrinterStatus;->etbAvailable:Z

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v2, "Checked block is not available for this printer"

    invoke-direct {v0, v2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_1 .. :try_end_1} :catch_3

    :cond_1
    :goto_0
    :try_start_2
    invoke-direct/range {p0 .. p0}, Lcom/starmicronics/stario/a;->b()V
    :try_end_2
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_2 .. :try_end_2} :catch_3

    :try_start_3
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget v3, v1, Lcom/starmicronics/stario/a;->r:I

    const/4 v6, -0x1

    const/16 v7, 0x1d

    const/4 v8, 0x6

    const/4 v9, 0x5

    const/4 v10, 0x2

    const/16 v11, 0x1b

    const/4 v12, 0x4

    const/4 v13, 0x3

    const/4 v14, 0x0

    if-eq v3, v6, :cond_2

    new-array v3, v8, [B

    aput-byte v11, v3, v14

    aput-byte v7, v3, v5

    aput-byte v13, v3, v10

    aput-byte v9, v3, v13

    aput-byte v5, v3, v12

    iget v6, v1, Lcom/starmicronics/stario/a;->r:I

    int-to-byte v6, v6

    aput-byte v6, v3, v9

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    iget-object v3, v1, Lcom/starmicronics/stario/a;->l:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    const/16 v6, 0x8

    const/16 v15, 0xa

    if-eqz v3, :cond_3

    new-array v3, v15, [B

    aput-byte v11, v3, v14

    const/16 v16, 0x2a

    aput-byte v16, v3, v5

    const/16 v16, 0x72

    aput-byte v16, v3, v10

    const/16 v16, 0x42

    aput-byte v16, v3, v13

    aput-byte v11, v3, v12

    aput-byte v7, v3, v9

    aput-byte v13, v3, v8

    const/16 v16, 0x7

    aput-byte v12, v3, v16

    aput-byte v14, v3, v6

    const/16 v16, 0x9

    aput-byte v14, v3, v16

    goto :goto_1

    :cond_3
    new-array v3, v8, [B

    aput-byte v11, v3, v14

    aput-byte v7, v3, v5

    aput-byte v13, v3, v10

    aput-byte v12, v3, v13

    aput-byte v14, v3, v12

    aput-byte v14, v3, v9

    :goto_1
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v3, v1, Lcom/starmicronics/stario/a;->l:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    const/16 v16, 0x0

    if-eqz v3, :cond_4

    new-array v3, v12, [B

    aput-byte v11, v3, v14

    const/16 v17, 0x1e

    aput-byte v17, v3, v5

    const/16 v17, 0x45

    aput-byte v17, v3, v10

    aput-byte v14, v3, v13

    goto :goto_2

    :cond_4
    const-string v3, "ESCPOS"

    iget-object v6, v1, Lcom/starmicronics/stario/a;->l:Ljava/lang/String;

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    new-array v3, v8, [B

    aput-byte v11, v3, v14

    aput-byte v7, v3, v5

    aput-byte v13, v3, v10

    aput-byte v10, v3, v13

    aput-byte v14, v3, v12

    aput-byte v14, v3, v9

    goto :goto_2

    :cond_5
    move-object/from16 v3, v16

    :goto_2
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget v3, v1, Lcom/starmicronics/stario/a;->d:I

    const/16 v6, 0x2710

    if-le v3, v6, :cond_6

    iget v6, v1, Lcom/starmicronics/stario/a;->d:I

    :cond_6
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    iget-object v3, v1, Lcom/starmicronics/stario/a;->l:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    new-array v0, v5, [B

    const/16 v3, 0x17

    aput-byte v3, v0, v14

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {v2}, Lcom/starmicronics/stario/a;->a(Ljava/util/List;)[B

    move-result-object v0

    array-length v2, v0

    invoke-virtual {v1, v0, v14, v2}, Lcom/starmicronics/stario/a;->writePort([BII)V

    :goto_3
    invoke-virtual/range {p0 .. p0}, Lcom/starmicronics/stario/a;->retreiveStatus()Lcom/starmicronics/stario/StarPrinterStatus;

    move-result-object v0

    iget-boolean v2, v0, Lcom/starmicronics/stario/StarPrinterStatus;->offline:Z

    if-eq v2, v5, :cond_9

    iget v2, v0, Lcom/starmicronics/stario/StarPrinterStatus;->etbCounter:I
    :try_end_3
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_3 .. :try_end_3} :catch_3

    if-ne v2, v5, :cond_7

    goto/16 :goto_8

    :cond_7
    const-wide/16 v2, 0xc8

    :try_start_4
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_4 .. :try_end_4} :catch_3

    goto :goto_4

    :catch_0
    move-exception v0

    move-object v2, v0

    :try_start_5
    invoke-virtual {v2}, Ljava/lang/InterruptedException;->printStackTrace()V

    :goto_4
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long v2, v2, v18

    int-to-long v9, v6

    cmp-long v0, v2, v9

    if-gtz v0, :cond_8

    const/4 v9, 0x5

    const/4 v10, 0x2

    goto :goto_3

    :cond_8
    new-instance v0, Ljava/util/concurrent/TimeoutException;

    const-string v2, "There was no response of the printer within the timeout period."

    invoke-direct {v0, v2}, Ljava/util/concurrent/TimeoutException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_9
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    invoke-direct {v0, v4}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_a
    new-array v0, v8, [B

    aput-byte v11, v0, v14

    aput-byte v7, v0, v5

    const/4 v3, 0x2

    aput-byte v13, v0, v3

    aput-byte v14, v0, v13

    aput-byte v14, v0, v12

    const/4 v3, 0x5

    aput-byte v14, v0, v3

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-array v0, v15, [B

    invoke-static {v2}, Lcom/starmicronics/stario/a;->a(Ljava/util/List;)[B

    move-result-object v2

    array-length v3, v2

    invoke-virtual {v1, v2, v14, v3}, Lcom/starmicronics/stario/a;->writePort([BII)V

    move-object v2, v0

    :goto_5
    invoke-virtual/range {p0 .. p0}, Lcom/starmicronics/stario/a;->retreiveStatus()Lcom/starmicronics/stario/StarPrinterStatus;

    move-result-object v3

    iget-boolean v0, v3, Lcom/starmicronics/stario/StarPrinterStatus;->offline:Z
    :try_end_5
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_5 .. :try_end_5} :catch_3

    if-eq v0, v5, :cond_f

    const-wide/16 v9, 0x64

    :try_start_6
    invoke-static {v9, v10}, Ljava/lang/Thread;->sleep(J)V
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_6 .. :try_end_6} :catch_3

    goto :goto_6

    :catch_1
    move-exception v0

    move-object v9, v0

    :try_start_7
    invoke-virtual {v9}, Ljava/lang/InterruptedException;->printStackTrace()V

    :goto_6
    sget-object v0, Lcom/starmicronics/stario/a;->m:[B

    if-eqz v0, :cond_b

    sget-object v0, Lcom/starmicronics/stario/a;->m:[B

    array-length v2, v0

    const/16 v9, 0x8

    move/from16 v22, v2

    move-object v2, v0

    move/from16 v0, v22

    goto :goto_7

    :cond_b
    array-length v0, v2

    invoke-virtual {v1, v2, v14, v0}, Lcom/starmicronics/stario/a;->readPort([BII)I

    move-result v0

    const/16 v9, 0x8

    :goto_7
    if-lt v0, v9, :cond_d

    invoke-static {v2}, Lcom/starmicronics/stario/f;->a([B)I

    move-result v0

    sput-object v16, Lcom/starmicronics/stario/a;->m:[B

    if-nez v0, :cond_c

    move-object v0, v3

    :goto_8
    new-array v2, v8, [B

    aput-byte v11, v2, v14

    aput-byte v7, v2, v5

    const/4 v3, 0x2

    aput-byte v13, v2, v3

    aput-byte v13, v2, v13

    aput-byte v14, v2, v12

    const/4 v3, 0x5

    aput-byte v14, v2, v3

    array-length v3, v2

    invoke-virtual {v1, v2, v14, v3}, Lcom/starmicronics/stario/a;->writePort([BII)V

    return-object v0

    :cond_c
    new-array v0, v8, [B

    aput-byte v11, v0, v14

    aput-byte v7, v0, v5

    const/4 v3, 0x2

    aput-byte v13, v0, v3

    aput-byte v14, v0, v13

    aput-byte v14, v0, v12

    const/4 v10, 0x5

    aput-byte v14, v0, v10

    array-length v15, v0

    invoke-virtual {v1, v0, v14, v15}, Lcom/starmicronics/stario/a;->writePort([BII)V

    goto :goto_9

    :cond_d
    const/4 v3, 0x2

    const/4 v10, 0x5

    :goto_9
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v20

    sub-long v20, v20, v18

    int-to-long v7, v6

    cmp-long v0, v20, v7

    if-gtz v0, :cond_e

    const/16 v7, 0x1d

    const/4 v8, 0x6

    goto :goto_5

    :cond_e
    new-instance v0, Ljava/util/concurrent/TimeoutException;

    const-string v2, "There was no response of the printer within the timeout period."

    invoke-direct {v0, v2}, Ljava/util/concurrent/TimeoutException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_f
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    invoke-direct {v0, v4}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_2
    move-exception v0

    move-object v2, v0

    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {v2}, Lcom/starmicronics/stario/StarIOPortException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_10
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    invoke-direct {v0, v4}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_7
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_7 .. :try_end_7} :catch_3

    :catch_3
    move-exception v0

    new-instance v2, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {v0}, Ljava/util/concurrent/TimeoutException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public endCheckedBlock()Lcom/starmicronics/stario/StarPrinterStatus;
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    const-string v0, "StarLine"

    :try_start_0
    iget-object v1, p0, Lcom/starmicronics/stario/a;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x5

    const/16 v4, 0x1d

    const/16 v5, 0x1b

    const/4 v6, 0x6

    const/4 v7, 0x4

    const/4 v8, 0x2

    const/4 v9, 0x3

    const/4 v10, 0x1

    const/4 v11, 0x0

    if-eqz v1, :cond_0

    new-array v1, v10, [B

    const/16 v12, 0x17

    aput-byte v12, v1, v11

    goto :goto_0

    :cond_0
    const-string v1, "ESCPOS"

    iget-object v12, p0, Lcom/starmicronics/stario/a;->l:Ljava/lang/String;

    invoke-virtual {v1, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    new-array v1, v6, [B

    aput-byte v5, v1, v11

    aput-byte v4, v1, v10

    aput-byte v9, v1, v8

    aput-byte v10, v1, v9

    aput-byte v11, v1, v7

    aput-byte v11, v1, v3

    goto :goto_0

    :cond_1
    move-object v1, v2

    :goto_0
    array-length v12, v1

    invoke-virtual {p0, v1, v11, v12}, Lcom/starmicronics/stario/a;->writePort([BII)V

    new-array v1, v6, [B

    aput-byte v5, v1, v11

    aput-byte v4, v1, v10

    aput-byte v9, v1, v8

    aput-byte v7, v1, v9

    aput-byte v11, v1, v7

    aput-byte v11, v1, v3

    array-length v12, v1

    invoke-virtual {p0, v1, v11, v12}, Lcom/starmicronics/stario/a;->writePort([BII)V

    iget v1, p0, Lcom/starmicronics/stario/a;->e:I

    const/16 v12, 0x2710

    if-le v1, v12, :cond_2

    iget v12, p0, Lcom/starmicronics/stario/a;->e:I

    :cond_2
    iget v1, p0, Lcom/starmicronics/stario/a;->r:I

    add-int/2addr v1, v9

    mul-int/lit16 v1, v1, 0x3e8

    if-le v1, v12, :cond_3

    iget v1, p0, Lcom/starmicronics/stario/a;->r:I

    add-int/2addr v1, v9

    mul-int/lit16 v12, v1, 0x3e8

    :cond_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v13

    invoke-virtual {p0}, Lcom/starmicronics/stario/a;->retreiveStatus()Lcom/starmicronics/stario/StarPrinterStatus;

    iget-object v1, p0, Lcom/starmicronics/stario/a;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0
    :try_end_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_0 .. :try_end_0} :catch_2

    const-string v1, "There was no response of the printer within the timeout period."

    if-eqz v0, :cond_7

    :goto_1
    :try_start_1
    invoke-virtual {p0}, Lcom/starmicronics/stario/a;->retreiveStatus()Lcom/starmicronics/stario/StarPrinterStatus;

    move-result-object v0

    iget-boolean v2, v0, Lcom/starmicronics/stario/StarPrinterStatus;->offline:Z

    if-ne v2, v10, :cond_4

    return-object v0

    :cond_4
    iget v2, v0, Lcom/starmicronics/stario/StarPrinterStatus;->etbCounter:I
    :try_end_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_1 .. :try_end_1} :catch_2

    if-ne v2, v8, :cond_5

    return-object v0

    :cond_5
    const-wide/16 v2, 0xc8

    :try_start_2
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_2

    :catch_0
    move-exception v0

    :try_start_3
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    :goto_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, v13

    int-to-long v4, v12

    cmp-long v0, v2, v4

    if-gtz v0, :cond_6

    goto :goto_1

    :cond_6
    new-instance v0, Ljava/util/concurrent/TimeoutException;

    invoke-direct {v0, v1}, Ljava/util/concurrent/TimeoutException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_7
    new-array v0, v6, [B

    aput-byte v5, v0, v11

    aput-byte v4, v0, v10

    aput-byte v9, v0, v8

    aput-byte v11, v0, v9

    aput-byte v11, v0, v7

    aput-byte v11, v0, v3

    const/16 v3, 0xa

    new-array v3, v3, [B

    array-length v4, v0

    invoke-virtual {p0, v0, v11, v4}, Lcom/starmicronics/stario/a;->writePort([BII)V

    :goto_3
    invoke-virtual {p0}, Lcom/starmicronics/stario/a;->retreiveStatus()Lcom/starmicronics/stario/StarPrinterStatus;

    move-result-object v4

    iget-boolean v5, v4, Lcom/starmicronics/stario/StarPrinterStatus;->offline:Z
    :try_end_3
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_3 .. :try_end_3} :catch_2

    if-eq v5, v10, :cond_c

    const-wide/16 v5, 0x64

    :try_start_4
    invoke-static {v5, v6}, Ljava/lang/Thread;->sleep(J)V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_4

    :catch_1
    move-exception v5

    :try_start_5
    invoke-virtual {v5}, Ljava/lang/InterruptedException;->printStackTrace()V

    :goto_4
    sget-object v5, Lcom/starmicronics/stario/a;->m:[B

    if-eqz v5, :cond_8

    sget-object v3, Lcom/starmicronics/stario/a;->m:[B

    array-length v5, v3

    goto :goto_5

    :cond_8
    array-length v5, v3

    invoke-virtual {p0, v3, v11, v5}, Lcom/starmicronics/stario/a;->readPort([BII)I

    move-result v5

    :goto_5
    const/16 v6, 0x8

    if-lt v5, v6, :cond_a

    invoke-static {v3}, Lcom/starmicronics/stario/f;->a([B)I

    move-result v5

    sput-object v2, Lcom/starmicronics/stario/a;->m:[B

    if-ne v5, v10, :cond_9

    return-object v4

    :cond_9
    array-length v4, v0

    invoke-virtual {p0, v0, v11, v4}, Lcom/starmicronics/stario/a;->writePort([BII)V

    :cond_a
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v13

    int-to-long v6, v12

    cmp-long v8, v4, v6

    if-gtz v8, :cond_b

    goto :goto_3

    :cond_b
    new-instance v0, Ljava/util/concurrent/TimeoutException;

    invoke-direct {v0, v1}, Ljava/util/concurrent/TimeoutException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_c
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v1, "Printer is offline"

    invoke-direct {v0, v1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_5
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_5 .. :try_end_5} :catch_3
    .catch Lcom/starmicronics/stario/StarIOPortException; {:try_start_5 .. :try_end_5} :catch_2

    :catch_2
    move-exception v0

    goto :goto_6

    :catch_3
    move-exception v0

    goto :goto_7

    :goto_6
    new-instance v1, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {v0}, Lcom/starmicronics/stario/StarIOPortException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v1

    :goto_7
    new-instance v1, Lcom/starmicronics/stario/StarIOPortException;

    invoke-virtual {v0}, Ljava/util/concurrent/TimeoutException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public getDipSwitchInformation()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/starmicronics/stario/a;->h()Lcom/starmicronics/stario/StarPrinterStatus;

    move-result-object v0

    iget-boolean v0, v0, Lcom/starmicronics/stario/StarPrinterStatus;->offline:Z

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    invoke-direct {p0}, Lcom/starmicronics/stario/a;->c()V

    iget-object v0, p0, Lcom/starmicronics/stario/a;->n:Ljava/lang/String;

    const-string v1, "DK-AirCash"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/starmicronics/stario/a;->o:Ljava/lang/String;

    const-string v1, "1.0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x2

    const/4 v2, 0x4

    if-eqz v0, :cond_0

    new-array v0, v2, [B

    fill-array-data v0, :array_0

    new-array v2, v2, [B

    fill-array-data v2, :array_1

    goto :goto_0

    :cond_0
    new-array v0, v2, [B

    fill-array-data v0, :array_2

    new-array v2, v2, [B

    fill-array-data v2, :array_3

    :goto_0
    new-array v1, v1, [B

    fill-array-data v1, :array_4

    invoke-direct {p0, v0, v1}, Lcom/starmicronics/stario/a;->a([B[B)[B

    move-result-object v0

    invoke-direct {p0, v2, v1}, Lcom/starmicronics/stario/a;->a([B[B)[B

    move-result-object v1

    invoke-static {v0, v1}, Lcom/starmicronics/stario/f;->a([B[B)Ljava/util/Map;

    move-result-object v0

    return-object v0

    :cond_1
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v1, "This model is not supported this method."

    invoke-direct {v0, v1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v1, "printer is offline."

    invoke-direct {v0, v1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0

    nop

    :array_0
    .array-data 1
        0x1bt
        0x3ft
        0x21t
        0x31t
    .end array-data

    :array_1
    .array-data 1
        0x1bt
        0x3ft
        0x21t
        0x32t
    .end array-data

    :array_2
    .array-data 1
        0x1bt
        0x23t
        0x2ct
        0x31t
    .end array-data

    :array_3
    .array-data 1
        0x1bt
        0x23t
        0x2ct
        0x32t
    .end array-data

    :array_4
    .array-data 1
        0xat
        0x0t
    .end array-data
.end method

.method public getFirmwareInformation()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/starmicronics/stario/a;->h()Lcom/starmicronics/stario/StarPrinterStatus;

    move-result-object v0

    iget-boolean v0, v0, Lcom/starmicronics/stario/StarPrinterStatus;->offline:Z

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-direct {p0}, Lcom/starmicronics/stario/a;->c()V

    iget-object v1, p0, Lcom/starmicronics/stario/a;->n:Ljava/lang/String;

    const-string v2, "ModelName"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/starmicronics/stario/a;->o:Ljava/lang/String;

    const-string v2, "FirmwareVersion"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0

    :cond_0
    new-instance v0, Lcom/starmicronics/stario/StarIOPortException;

    const-string v1, "printer is offline."

    invoke-direct {v0, v1}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public readPort([BII)I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    :try_start_0
    invoke-direct {p0}, Lcom/starmicronics/stario/a;->f()V

    iget-object v0, p0, Lcom/starmicronics/stario/a;->g:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->available()I

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    iget-object v0, p0, Lcom/starmicronics/stario/a;->g:Ljava/io/InputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/InputStream;->read([BII)I

    move-result p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 p2, -0x1

    if-ne p1, p2, :cond_1

    const/4 p1, 0x0

    :cond_1
    return p1

    :catch_0
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string p2, "Failed to read"

    invoke-direct {p1, p2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public retreiveStatus()Lcom/starmicronics/stario/StarPrinterStatus;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/starmicronics/stario/a;->h()Lcom/starmicronics/stario/StarPrinterStatus;

    move-result-object v0

    return-object v0
.end method

.method public setEndCheckedBlockTimeoutMillis(I)V
    .locals 0

    iput p1, p0, Lcom/starmicronics/stario/a;->e:I

    return-void
.end method

.method public writePort([BII)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/starmicronics/stario/StarIOPortException;
        }
    .end annotation

    :try_start_0
    invoke-direct {p0}, Lcom/starmicronics/stario/a;->f()V

    const/4 v0, 0x0

    const/16 v1, 0x400

    if-ge v1, p3, :cond_1

    const/16 v2, 0x400

    :goto_0
    if-ge v0, p3, :cond_2

    iget-object v3, p0, Lcom/starmicronics/stario/a;->h:Ljava/io/OutputStream;

    invoke-virtual {v3, p1, p2, v2}, Ljava/io/OutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    add-int p2, v0, v2

    sub-int v0, p3, p2

    if-ge v0, v1, :cond_0

    move v2, v0

    :cond_0
    const-wide/16 v3, 0x0

    :try_start_1
    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_0
    move-exception v0

    :try_start_2
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    :goto_1
    move v0, p2

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/starmicronics/stario/a;->h:Ljava/io/OutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V

    :cond_2
    iget-boolean p1, p0, Lcom/starmicronics/stario/a;->a:Z

    const/4 p2, 0x1

    if-ne p1, p2, :cond_3

    iget-object p1, p0, Lcom/starmicronics/stario/a;->h:Ljava/io/OutputStream;

    invoke-virtual {p1}, Ljava/io/OutputStream;->flush()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    :cond_3
    return-void

    :catch_1
    new-instance p1, Lcom/starmicronics/stario/StarIOPortException;

    const-string p2, "failed to write"

    invoke-direct {p1, p2}, Lcom/starmicronics/stario/StarIOPortException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
