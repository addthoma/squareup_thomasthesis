.class Lcom/starmicronics/stario/e$1;
.super Landroid/content/BroadcastReceiver;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/starmicronics/stario/e;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/starmicronics/stario/e;


# direct methods
.method constructor <init>(Lcom/starmicronics/stario/e;)V
    .locals 0

    iput-object p1, p0, Lcom/starmicronics/stario/e$1;->a:Lcom/starmicronics/stario/e;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object p1

    const-string v0, "com.StarMicronics.StarIO.USB_PERMISSION"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    const/4 v0, 0x1

    if-eqz p1, :cond_2

    const-string p1, "device"

    invoke-virtual {p2, p1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Landroid/hardware/usb/UsbDevice;

    const/4 v1, 0x0

    const-string v2, "permission"

    invoke-virtual {p2, v2, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result p2

    if-eqz p2, :cond_0

    if-eqz p1, :cond_1

    iget-object p1, p0, Lcom/starmicronics/stario/e$1;->a:Lcom/starmicronics/stario/e;

    invoke-static {p1, v0}, Lcom/starmicronics/stario/e;->a(Lcom/starmicronics/stario/e;Z)Z

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/starmicronics/stario/e$1;->a:Lcom/starmicronics/stario/e;

    invoke-static {p1, v1}, Lcom/starmicronics/stario/e;->a(Lcom/starmicronics/stario/e;Z)Z

    :cond_1
    :goto_0
    invoke-static {}, Lcom/starmicronics/stario/e;->b()Landroid/content/Context;

    move-result-object p1

    iget-object p2, p0, Lcom/starmicronics/stario/e$1;->a:Lcom/starmicronics/stario/e;

    invoke-static {p2}, Lcom/starmicronics/stario/e;->a(Lcom/starmicronics/stario/e;)Landroid/content/BroadcastReceiver;

    move-result-object p2

    invoke-virtual {p1, p2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    :cond_2
    iget-object p1, p0, Lcom/starmicronics/stario/e$1;->a:Lcom/starmicronics/stario/e;

    invoke-static {p1, v0}, Lcom/starmicronics/stario/e;->b(Lcom/starmicronics/stario/e;Z)Z

    return-void
.end method
