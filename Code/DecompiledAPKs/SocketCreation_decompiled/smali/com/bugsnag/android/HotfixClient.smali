.class public final Lcom/bugsnag/android/HotfixClient;
.super Lcom/bugsnag/android/Client;
.source "HotfixClient.java"


# static fields
.field private static final BUGSNAG_METADATA_API_KEY:Ljava/lang/String; = "com.bugsnag.android.square.API_KEY"

.field private static final PATCH_BUGSNAG_CLIENT:Ljava/lang/String; = "com.squareup.PATCH_BUGSNAG_CLIENT"

.field private static volatile STORE_DIRECTORY_FIELD:Ljava/lang/reflect/Field;


# instance fields
.field private final directorySuffix:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x1

    .line 84
    invoke-direct {p0, p1, p2, v0}, Lcom/bugsnag/android/Client;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 85
    iput-object p3, p0, Lcom/bugsnag/android/HotfixClient;->directorySuffix:Ljava/lang/String;

    return-void
.end method

.method public static createBugsnagClient(Landroid/app/Application;)Lcom/bugsnag/android/Client;
    .locals 4

    .line 40
    invoke-virtual {p0}, Landroid/app/Application;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 41
    invoke-virtual {p0}, Landroid/app/Application;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x80

    .line 44
    :try_start_0
    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 50
    iget-object v1, v0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    const-string v2, "com.bugsnag.android.square.API_KEY"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 56
    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    const-string v2, "com.squareup.PATCH_BUGSNAG_CLIENT"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    .line 60
    new-instance v0, Lcom/bugsnag/android/HotfixClient;

    const-string v3, "-square"

    invoke-direct {v0, p0, v1, v3}, Lcom/bugsnag/android/HotfixClient;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 62
    :cond_0
    new-instance v0, Lcom/bugsnag/android/Client;

    invoke-direct {v0, p0, v1, v2}, Lcom/bugsnag/android/Client;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 67
    :goto_0
    invoke-virtual {v0, v2}, Lcom/bugsnag/android/Client;->setLoggingEnabled(Z)V

    const-string p0, "com.squareup"

    .line 74
    filled-new-array {p0}, [Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/bugsnag/android/Client;->setProjectPackages([Ljava/lang/String;)V

    return-object v0

    .line 52
    :cond_1
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string v0, "Did you forget to add a value for com.bugsnag.android.square.API_KEY in your Manifest?"

    invoke-direct {p0, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p0

    :catch_0
    move-exception p0

    .line 46
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Could not use application info to get Bugsnag API key"

    invoke-direct {v0, v1, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method private updateFileStoreDirectory(Lcom/bugsnag/android/FileStore;)V
    .locals 5

    .line 101
    iget-object v0, p1, Lcom/bugsnag/android/FileStore;->storeDirectory:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 102
    iget-object v0, p1, Lcom/bugsnag/android/FileStore;->storeDirectory:Ljava/lang/String;

    const-string v1, "/"

    .line 103
    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    const/4 v3, 0x1

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    .line 104
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    sub-int/2addr v4, v3

    invoke-virtual {v0, v2, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 106
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/bugsnag/android/HotfixClient;->directorySuffix:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 109
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 110
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    .line 111
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 113
    :try_start_0
    sget-object v1, Lcom/bugsnag/android/HotfixClient;->STORE_DIRECTORY_FIELD:Ljava/lang/reflect/Field;

    if-nez v1, :cond_1

    .line 114
    const-class v1, Lcom/bugsnag/android/FileStore;

    const-string v2, "storeDirectory"

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    sput-object v1, Lcom/bugsnag/android/HotfixClient;->STORE_DIRECTORY_FIELD:Ljava/lang/reflect/Field;

    .line 115
    sget-object v1, Lcom/bugsnag/android/HotfixClient;->STORE_DIRECTORY_FIELD:Ljava/lang/reflect/Field;

    invoke-virtual {v1, v3}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 117
    :cond_1
    sget-object v1, Lcom/bugsnag/android/HotfixClient;->STORE_DIRECTORY_FIELD:Ljava/lang/reflect/Field;

    invoke-virtual {v1, p1, v0}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_2
    return-void
.end method


# virtual methods
.method public enableExceptionHandler()V
    .locals 2

    .line 94
    iget-object v0, p0, Lcom/bugsnag/android/HotfixClient;->config:Lcom/bugsnag/android/Configuration;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/bugsnag/android/Configuration;->setEnableExceptionHandler(Z)V

    .line 96
    iget-object v0, p0, Lcom/bugsnag/android/HotfixClient;->sessionStore:Lcom/bugsnag/android/SessionStore;

    invoke-direct {p0, v0}, Lcom/bugsnag/android/HotfixClient;->updateFileStoreDirectory(Lcom/bugsnag/android/FileStore;)V

    .line 97
    iget-object v0, p0, Lcom/bugsnag/android/HotfixClient;->errorStore:Lcom/bugsnag/android/ErrorStore;

    invoke-direct {p0, v0}, Lcom/bugsnag/android/HotfixClient;->updateFileStoreDirectory(Lcom/bugsnag/android/FileStore;)V

    return-void
.end method
