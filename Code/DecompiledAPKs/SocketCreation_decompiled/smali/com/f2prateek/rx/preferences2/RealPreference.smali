.class final Lcom/f2prateek/rx/preferences2/RealPreference;
.super Ljava/lang/Object;
.source "RealPreference.java"

# interfaces
.implements Lcom/f2prateek/rx/preferences2/Preference;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/f2prateek/rx/preferences2/RealPreference$Adapter;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/f2prateek/rx/preferences2/Preference<",
        "TT;>;"
    }
.end annotation


# instance fields
.field private final adapter:Lcom/f2prateek/rx/preferences2/RealPreference$Adapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/f2prateek/rx/preferences2/RealPreference$Adapter<",
            "TT;>;"
        }
    .end annotation
.end field

.field private final defaultValue:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private final key:Ljava/lang/String;

.field private final preferences:Landroid/content/SharedPreferences;

.field private final values:Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/Observable<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/Object;Lcom/f2prateek/rx/preferences2/RealPreference$Adapter;Lio/reactivex/Observable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/SharedPreferences;",
            "Ljava/lang/String;",
            "TT;",
            "Lcom/f2prateek/rx/preferences2/RealPreference$Adapter<",
            "TT;>;",
            "Lio/reactivex/Observable<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/f2prateek/rx/preferences2/RealPreference;->preferences:Landroid/content/SharedPreferences;

    .line 37
    iput-object p2, p0, Lcom/f2prateek/rx/preferences2/RealPreference;->key:Ljava/lang/String;

    .line 38
    iput-object p3, p0, Lcom/f2prateek/rx/preferences2/RealPreference;->defaultValue:Ljava/lang/Object;

    .line 39
    iput-object p4, p0, Lcom/f2prateek/rx/preferences2/RealPreference;->adapter:Lcom/f2prateek/rx/preferences2/RealPreference$Adapter;

    .line 40
    new-instance p1, Lcom/f2prateek/rx/preferences2/RealPreference$2;

    invoke-direct {p1, p0, p2}, Lcom/f2prateek/rx/preferences2/RealPreference$2;-><init>(Lcom/f2prateek/rx/preferences2/RealPreference;Ljava/lang/String;)V

    .line 41
    invoke-virtual {p5, p1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object p1

    const-string p2, "<init>"

    .line 46
    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object p1

    new-instance p2, Lcom/f2prateek/rx/preferences2/RealPreference$1;

    invoke-direct {p2, p0}, Lcom/f2prateek/rx/preferences2/RealPreference$1;-><init>(Lcom/f2prateek/rx/preferences2/RealPreference;)V

    .line 47
    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    iput-object p1, p0, Lcom/f2prateek/rx/preferences2/RealPreference;->values:Lio/reactivex/Observable;

    return-void
.end method


# virtual methods
.method public asConsumer()Lio/reactivex/functions/Consumer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/functions/Consumer<",
            "-TT;>;"
        }
    .end annotation

    .line 89
    new-instance v0, Lcom/f2prateek/rx/preferences2/RealPreference$3;

    invoke-direct {v0, p0}, Lcom/f2prateek/rx/preferences2/RealPreference$3;-><init>(Lcom/f2prateek/rx/preferences2/RealPreference;)V

    return-object v0
.end method

.method public asObservable()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "TT;>;"
        }
    .end annotation

    .line 85
    iget-object v0, p0, Lcom/f2prateek/rx/preferences2/RealPreference;->values:Lio/reactivex/Observable;

    return-object v0
.end method

.method public defaultValue()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .line 59
    iget-object v0, p0, Lcom/f2prateek/rx/preferences2/RealPreference;->defaultValue:Ljava/lang/Object;

    return-object v0
.end method

.method public declared-synchronized delete()V
    .locals 2

    monitor-enter p0

    .line 81
    :try_start_0
    iget-object v0, p0, Lcom/f2prateek/rx/preferences2/RealPreference;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Lcom/f2prateek/rx/preferences2/RealPreference;->key:Ljava/lang/String;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 82
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized get()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    monitor-enter p0

    .line 63
    :try_start_0
    iget-object v0, p0, Lcom/f2prateek/rx/preferences2/RealPreference;->preferences:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/f2prateek/rx/preferences2/RealPreference;->key:Ljava/lang/String;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/f2prateek/rx/preferences2/RealPreference;->defaultValue:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 66
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/f2prateek/rx/preferences2/RealPreference;->adapter:Lcom/f2prateek/rx/preferences2/RealPreference$Adapter;

    iget-object v1, p0, Lcom/f2prateek/rx/preferences2/RealPreference;->key:Ljava/lang/String;

    iget-object v2, p0, Lcom/f2prateek/rx/preferences2/RealPreference;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v0, v1, v2}, Lcom/f2prateek/rx/preferences2/RealPreference$Adapter;->get(Ljava/lang/String;Landroid/content/SharedPreferences;)Ljava/lang/Object;

    move-result-object v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public isSet()Z
    .locals 2

    .line 77
    iget-object v0, p0, Lcom/f2prateek/rx/preferences2/RealPreference;->preferences:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/f2prateek/rx/preferences2/RealPreference;->key:Ljava/lang/String;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public key()Ljava/lang/String;
    .locals 1

    .line 55
    iget-object v0, p0, Lcom/f2prateek/rx/preferences2/RealPreference;->key:Ljava/lang/String;

    return-object v0
.end method

.method public set(Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    const-string v0, "value == null"

    .line 70
    invoke-static {p1, v0}, Lcom/f2prateek/rx/preferences2/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    iget-object v0, p0, Lcom/f2prateek/rx/preferences2/RealPreference;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 72
    iget-object v1, p0, Lcom/f2prateek/rx/preferences2/RealPreference;->adapter:Lcom/f2prateek/rx/preferences2/RealPreference$Adapter;

    iget-object v2, p0, Lcom/f2prateek/rx/preferences2/RealPreference;->key:Ljava/lang/String;

    invoke-interface {v1, v2, p1, v0}, Lcom/f2prateek/rx/preferences2/RealPreference$Adapter;->set(Ljava/lang/String;Ljava/lang/Object;Landroid/content/SharedPreferences$Editor;)V

    .line 73
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method
