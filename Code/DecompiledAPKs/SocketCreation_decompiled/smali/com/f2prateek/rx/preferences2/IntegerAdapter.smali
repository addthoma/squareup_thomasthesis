.class final Lcom/f2prateek/rx/preferences2/IntegerAdapter;
.super Ljava/lang/Object;
.source "IntegerAdapter.java"

# interfaces
.implements Lcom/f2prateek/rx/preferences2/RealPreference$Adapter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/f2prateek/rx/preferences2/RealPreference$Adapter<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# static fields
.field static final INSTANCE:Lcom/f2prateek/rx/preferences2/IntegerAdapter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 7
    new-instance v0, Lcom/f2prateek/rx/preferences2/IntegerAdapter;

    invoke-direct {v0}, Lcom/f2prateek/rx/preferences2/IntegerAdapter;-><init>()V

    sput-object v0, Lcom/f2prateek/rx/preferences2/IntegerAdapter;->INSTANCE:Lcom/f2prateek/rx/preferences2/IntegerAdapter;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public get(Ljava/lang/String;Landroid/content/SharedPreferences;)Ljava/lang/Integer;
    .locals 1

    const/4 v0, 0x0

    .line 10
    invoke-interface {p2, p1, v0}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic get(Ljava/lang/String;Landroid/content/SharedPreferences;)Ljava/lang/Object;
    .locals 0

    .line 6
    invoke-virtual {p0, p1, p2}, Lcom/f2prateek/rx/preferences2/IntegerAdapter;->get(Ljava/lang/String;Landroid/content/SharedPreferences;)Ljava/lang/Integer;

    move-result-object p1

    return-object p1
.end method

.method public set(Ljava/lang/String;Ljava/lang/Integer;Landroid/content/SharedPreferences$Editor;)V
    .locals 0

    .line 15
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result p2

    invoke-interface {p3, p1, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    return-void
.end method

.method public bridge synthetic set(Ljava/lang/String;Ljava/lang/Object;Landroid/content/SharedPreferences$Editor;)V
    .locals 0

    .line 6
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p0, p1, p2, p3}, Lcom/f2prateek/rx/preferences2/IntegerAdapter;->set(Ljava/lang/String;Ljava/lang/Integer;Landroid/content/SharedPreferences$Editor;)V

    return-void
.end method
