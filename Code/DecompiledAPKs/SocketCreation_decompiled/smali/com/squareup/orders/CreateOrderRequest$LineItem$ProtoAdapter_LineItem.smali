.class final Lcom/squareup/orders/CreateOrderRequest$LineItem$ProtoAdapter_LineItem;
.super Lcom/squareup/wire/ProtoAdapter;
.source "CreateOrderRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/CreateOrderRequest$LineItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_LineItem"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/orders/CreateOrderRequest$LineItem;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 769
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/orders/CreateOrderRequest$LineItem;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/orders/CreateOrderRequest$LineItem;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 802
    new-instance v0, Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;

    invoke-direct {v0}, Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;-><init>()V

    .line 803
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 804
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 816
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 814
    :pswitch_0
    iget-object v3, v0, Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;->discounts:Ljava/util/List;

    sget-object v4, Lcom/squareup/orders/CreateOrderRequest$Discount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 813
    :pswitch_1
    iget-object v3, v0, Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;->taxes:Ljava/util/List;

    sget-object v4, Lcom/squareup/orders/CreateOrderRequest$Tax;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 812
    :pswitch_2
    iget-object v3, v0, Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;->modifiers:Ljava/util/List;

    sget-object v4, Lcom/squareup/orders/CreateOrderRequest$Modifier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 811
    :pswitch_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;->catalog_object_id(Ljava/lang/String;)Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;

    goto :goto_0

    .line 810
    :pswitch_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;->note(Ljava/lang/String;)Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;

    goto :goto_0

    .line 809
    :pswitch_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;->variation_name(Ljava/lang/String;)Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;

    goto :goto_0

    .line 808
    :pswitch_6
    sget-object v3, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;->base_price_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;

    goto :goto_0

    .line 807
    :pswitch_7
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;->quantity(Ljava/lang/String;)Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;

    goto :goto_0

    .line 806
    :pswitch_8
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;->name(Ljava/lang/String;)Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;

    goto :goto_0

    .line 820
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 821
    invoke-virtual {v0}, Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;->build()Lcom/squareup/orders/CreateOrderRequest$LineItem;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 767
    invoke-virtual {p0, p1}, Lcom/squareup/orders/CreateOrderRequest$LineItem$ProtoAdapter_LineItem;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/orders/CreateOrderRequest$LineItem;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/orders/CreateOrderRequest$LineItem;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 788
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/CreateOrderRequest$LineItem;->name:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 789
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/CreateOrderRequest$LineItem;->quantity:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 790
    sget-object v0, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/CreateOrderRequest$LineItem;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 791
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/CreateOrderRequest$LineItem;->variation_name:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 792
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/CreateOrderRequest$LineItem;->note:Ljava/lang/String;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 793
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/CreateOrderRequest$LineItem;->catalog_object_id:Ljava/lang/String;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 794
    sget-object v0, Lcom/squareup/orders/CreateOrderRequest$Modifier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/orders/CreateOrderRequest$LineItem;->modifiers:Ljava/util/List;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 795
    sget-object v0, Lcom/squareup/orders/CreateOrderRequest$Tax;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/orders/CreateOrderRequest$LineItem;->taxes:Ljava/util/List;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 796
    sget-object v0, Lcom/squareup/orders/CreateOrderRequest$Discount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/orders/CreateOrderRequest$LineItem;->discounts:Ljava/util/List;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 797
    invoke-virtual {p2}, Lcom/squareup/orders/CreateOrderRequest$LineItem;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 767
    check-cast p2, Lcom/squareup/orders/CreateOrderRequest$LineItem;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/orders/CreateOrderRequest$LineItem$ProtoAdapter_LineItem;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/orders/CreateOrderRequest$LineItem;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/orders/CreateOrderRequest$LineItem;)I
    .locals 4

    .line 774
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/orders/CreateOrderRequest$LineItem;->name:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/CreateOrderRequest$LineItem;->quantity:Ljava/lang/String;

    const/4 v3, 0x2

    .line 775
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/CreateOrderRequest$LineItem;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    const/4 v3, 0x3

    .line 776
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/CreateOrderRequest$LineItem;->variation_name:Ljava/lang/String;

    const/4 v3, 0x4

    .line 777
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/CreateOrderRequest$LineItem;->note:Ljava/lang/String;

    const/4 v3, 0x5

    .line 778
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/CreateOrderRequest$LineItem;->catalog_object_id:Ljava/lang/String;

    const/4 v3, 0x6

    .line 779
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/orders/CreateOrderRequest$Modifier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 780
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/orders/CreateOrderRequest$LineItem;->modifiers:Ljava/util/List;

    const/4 v3, 0x7

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/orders/CreateOrderRequest$Tax;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 781
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/orders/CreateOrderRequest$LineItem;->taxes:Ljava/util/List;

    const/16 v3, 0x8

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/orders/CreateOrderRequest$Discount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 782
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/orders/CreateOrderRequest$LineItem;->discounts:Ljava/util/List;

    const/16 v3, 0x9

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 783
    invoke-virtual {p1}, Lcom/squareup/orders/CreateOrderRequest$LineItem;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 767
    check-cast p1, Lcom/squareup/orders/CreateOrderRequest$LineItem;

    invoke-virtual {p0, p1}, Lcom/squareup/orders/CreateOrderRequest$LineItem$ProtoAdapter_LineItem;->encodedSize(Lcom/squareup/orders/CreateOrderRequest$LineItem;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/orders/CreateOrderRequest$LineItem;)Lcom/squareup/orders/CreateOrderRequest$LineItem;
    .locals 2

    .line 826
    invoke-virtual {p1}, Lcom/squareup/orders/CreateOrderRequest$LineItem;->newBuilder()Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;

    move-result-object p1

    .line 827
    iget-object v0, p1, Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v0, p1, Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 828
    :cond_0
    iget-object v0, p1, Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;->modifiers:Ljava/util/List;

    sget-object v1, Lcom/squareup/orders/CreateOrderRequest$Modifier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 829
    iget-object v0, p1, Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;->taxes:Ljava/util/List;

    sget-object v1, Lcom/squareup/orders/CreateOrderRequest$Tax;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 830
    iget-object v0, p1, Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;->discounts:Ljava/util/List;

    sget-object v1, Lcom/squareup/orders/CreateOrderRequest$Discount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 831
    invoke-virtual {p1}, Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 832
    invoke-virtual {p1}, Lcom/squareup/orders/CreateOrderRequest$LineItem$Builder;->build()Lcom/squareup/orders/CreateOrderRequest$LineItem;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 767
    check-cast p1, Lcom/squareup/orders/CreateOrderRequest$LineItem;

    invoke-virtual {p0, p1}, Lcom/squareup/orders/CreateOrderRequest$LineItem$ProtoAdapter_LineItem;->redact(Lcom/squareup/orders/CreateOrderRequest$LineItem;)Lcom/squareup/orders/CreateOrderRequest$LineItem;

    move-result-object p1

    return-object p1
.end method
