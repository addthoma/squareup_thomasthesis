.class public final Lcom/squareup/orders/CreateOrderRequest$Discount$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CreateOrderRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/CreateOrderRequest$Discount;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/orders/CreateOrderRequest$Discount;",
        "Lcom/squareup/orders/CreateOrderRequest$Discount$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public amount_money:Lcom/squareup/protos/connect/v2/common/Money;

.field public catalog_object_id:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public percentage:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1234
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public amount_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/orders/CreateOrderRequest$Discount$Builder;
    .locals 0

    .line 1278
    iput-object p1, p0, Lcom/squareup/orders/CreateOrderRequest$Discount$Builder;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public build()Lcom/squareup/orders/CreateOrderRequest$Discount;
    .locals 7

    .line 1284
    new-instance v6, Lcom/squareup/orders/CreateOrderRequest$Discount;

    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$Discount$Builder;->catalog_object_id:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/orders/CreateOrderRequest$Discount$Builder;->name:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/orders/CreateOrderRequest$Discount$Builder;->percentage:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/orders/CreateOrderRequest$Discount$Builder;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/orders/CreateOrderRequest$Discount;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Money;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 1225
    invoke-virtual {p0}, Lcom/squareup/orders/CreateOrderRequest$Discount$Builder;->build()Lcom/squareup/orders/CreateOrderRequest$Discount;

    move-result-object v0

    return-object v0
.end method

.method public catalog_object_id(Ljava/lang/String;)Lcom/squareup/orders/CreateOrderRequest$Discount$Builder;
    .locals 0

    .line 1246
    iput-object p1, p0, Lcom/squareup/orders/CreateOrderRequest$Discount$Builder;->catalog_object_id:Ljava/lang/String;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/orders/CreateOrderRequest$Discount$Builder;
    .locals 0

    .line 1256
    iput-object p1, p0, Lcom/squareup/orders/CreateOrderRequest$Discount$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public percentage(Ljava/lang/String;)Lcom/squareup/orders/CreateOrderRequest$Discount$Builder;
    .locals 0

    .line 1268
    iput-object p1, p0, Lcom/squareup/orders/CreateOrderRequest$Discount$Builder;->percentage:Ljava/lang/String;

    return-object p0
.end method
