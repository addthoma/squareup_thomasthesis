.class public final Lcom/squareup/orders/model/Order$Fulfillment;
.super Lcom/squareup/wire/Message;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Fulfillment"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orders/model/Order$Fulfillment$ProtoAdapter_Fulfillment;,
        Lcom/squareup/orders/model/Order$Fulfillment$State;,
        Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry;,
        Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;,
        Lcom/squareup/orders/model/Order$Fulfillment$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/orders/model/Order$Fulfillment;",
        "Lcom/squareup/orders/model/Order$Fulfillment$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/orders/model/Order$Fulfillment;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_LINE_ITEM_APPLICATION:Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;

.field public static final DEFAULT_STATE:Lcom/squareup/orders/model/Order$Fulfillment$State;

.field public static final DEFAULT_TYPE:Lcom/squareup/orders/model/Order$FulfillmentType;

.field public static final DEFAULT_UID:Ljava/lang/String; = ""

.field public static final DEFAULT_WAS_STATUS:Lcom/squareup/orders/model/Order$Fulfillment$State;

.field private static final serialVersionUID:J


# instance fields
.field public final delivery_details:Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$FulfillmentDeliveryDetails#ADAPTER"
        tag = 0xd
    .end annotation
.end field

.field public final digital_details:Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$FulfillmentDigitalDetails#ADAPTER"
        tag = 0xc
    .end annotation
.end field

.field public final entries:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$Fulfillment$FulfillmentEntry#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x4
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry;",
            ">;"
        }
    .end annotation
.end field

.field public final ext_fulfillment_client_details:Lcom/squareup/protos/client/orders/FulfillmentClientDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.client.orders.FulfillmentClientDetails#ADAPTER"
        tag = 0x2710
    .end annotation
.end field

.field public final line_item_application:Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$Fulfillment$FulfillmentLineItemApplication#ADAPTER"
        tag = 0x8
    .end annotation
.end field

.field public final managed_delivery_details:Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$FulfillmentManagedDeliveryDetails#ADAPTER"
        tag = 0x7
    .end annotation
.end field

.field public final metadata:Ljava/util/Map;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        keyAdapter = "com.squareup.wire.ProtoAdapter#STRING"
        redacted = true
        tag = 0x5
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final pickup_details:Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$FulfillmentPickupDetails#ADAPTER"
        tag = 0x6
    .end annotation
.end field

.field public final shipment_details:Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$FulfillmentShipmentDetails#ADAPTER"
        tag = 0x9
    .end annotation
.end field

.field public final state:Lcom/squareup/orders/model/Order$Fulfillment$State;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$Fulfillment$State#ADAPTER"
        tag = 0x3
    .end annotation
.end field

.field public final type:Lcom/squareup/orders/model/Order$FulfillmentType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$FulfillmentType#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final uid:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final was_status:Lcom/squareup/orders/model/Order$Fulfillment$State;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$Fulfillment$State#ADAPTER"
        tag = 0x2328
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 6664
    new-instance v0, Lcom/squareup/orders/model/Order$Fulfillment$ProtoAdapter_Fulfillment;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$Fulfillment$ProtoAdapter_Fulfillment;-><init>()V

    sput-object v0, Lcom/squareup/orders/model/Order$Fulfillment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 6670
    sget-object v0, Lcom/squareup/orders/model/Order$FulfillmentType;->FULFILLMENT_TYPE_DO_NOT_USE:Lcom/squareup/orders/model/Order$FulfillmentType;

    sput-object v0, Lcom/squareup/orders/model/Order$Fulfillment;->DEFAULT_TYPE:Lcom/squareup/orders/model/Order$FulfillmentType;

    .line 6672
    sget-object v0, Lcom/squareup/orders/model/Order$Fulfillment$State;->FULFILLMENT_STATE_DO_NOT_USE:Lcom/squareup/orders/model/Order$Fulfillment$State;

    sput-object v0, Lcom/squareup/orders/model/Order$Fulfillment;->DEFAULT_STATE:Lcom/squareup/orders/model/Order$Fulfillment$State;

    .line 6674
    sget-object v0, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;->ALL:Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;

    sput-object v0, Lcom/squareup/orders/model/Order$Fulfillment;->DEFAULT_LINE_ITEM_APPLICATION:Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;

    .line 6676
    sget-object v0, Lcom/squareup/orders/model/Order$Fulfillment$State;->FULFILLMENT_STATE_DO_NOT_USE:Lcom/squareup/orders/model/Order$Fulfillment$State;

    sput-object v0, Lcom/squareup/orders/model/Order$Fulfillment;->DEFAULT_WAS_STATUS:Lcom/squareup/orders/model/Order$Fulfillment$State;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/orders/model/Order$FulfillmentType;Lcom/squareup/orders/model/Order$Fulfillment$State;Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;Ljava/util/List;Ljava/util/Map;Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;Lcom/squareup/orders/model/Order$Fulfillment$State;Lcom/squareup/protos/client/orders/FulfillmentClientDetails;)V
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$FulfillmentType;",
            "Lcom/squareup/orders/model/Order$Fulfillment$State;",
            "Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;",
            "Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;",
            "Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;",
            "Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;",
            "Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;",
            "Lcom/squareup/orders/model/Order$Fulfillment$State;",
            "Lcom/squareup/protos/client/orders/FulfillmentClientDetails;",
            ")V"
        }
    .end annotation

    .line 6841
    sget-object v14, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    invoke-direct/range {v0 .. v14}, Lcom/squareup/orders/model/Order$Fulfillment;-><init>(Ljava/lang/String;Lcom/squareup/orders/model/Order$FulfillmentType;Lcom/squareup/orders/model/Order$Fulfillment$State;Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;Ljava/util/List;Ljava/util/Map;Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;Lcom/squareup/orders/model/Order$Fulfillment$State;Lcom/squareup/protos/client/orders/FulfillmentClientDetails;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/orders/model/Order$FulfillmentType;Lcom/squareup/orders/model/Order$Fulfillment$State;Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;Ljava/util/List;Ljava/util/Map;Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;Lcom/squareup/orders/model/Order$Fulfillment$State;Lcom/squareup/protos/client/orders/FulfillmentClientDetails;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$FulfillmentType;",
            "Lcom/squareup/orders/model/Order$Fulfillment$State;",
            "Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;",
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentEntry;",
            ">;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;",
            "Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;",
            "Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;",
            "Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;",
            "Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;",
            "Lcom/squareup/orders/model/Order$Fulfillment$State;",
            "Lcom/squareup/protos/client/orders/FulfillmentClientDetails;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 6851
    sget-object v0, Lcom/squareup/orders/model/Order$Fulfillment;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p14}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 6852
    iput-object p1, p0, Lcom/squareup/orders/model/Order$Fulfillment;->uid:Ljava/lang/String;

    .line 6853
    iput-object p2, p0, Lcom/squareup/orders/model/Order$Fulfillment;->type:Lcom/squareup/orders/model/Order$FulfillmentType;

    .line 6854
    iput-object p3, p0, Lcom/squareup/orders/model/Order$Fulfillment;->state:Lcom/squareup/orders/model/Order$Fulfillment$State;

    .line 6855
    iput-object p4, p0, Lcom/squareup/orders/model/Order$Fulfillment;->line_item_application:Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;

    const-string p1, "entries"

    .line 6856
    invoke-static {p1, p5}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/orders/model/Order$Fulfillment;->entries:Ljava/util/List;

    const-string p1, "metadata"

    .line 6857
    invoke-static {p1, p6}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/Map;)Ljava/util/Map;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/orders/model/Order$Fulfillment;->metadata:Ljava/util/Map;

    .line 6858
    iput-object p7, p0, Lcom/squareup/orders/model/Order$Fulfillment;->pickup_details:Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;

    .line 6859
    iput-object p8, p0, Lcom/squareup/orders/model/Order$Fulfillment;->managed_delivery_details:Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;

    .line 6860
    iput-object p9, p0, Lcom/squareup/orders/model/Order$Fulfillment;->shipment_details:Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;

    .line 6861
    iput-object p10, p0, Lcom/squareup/orders/model/Order$Fulfillment;->digital_details:Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;

    .line 6862
    iput-object p11, p0, Lcom/squareup/orders/model/Order$Fulfillment;->delivery_details:Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;

    .line 6863
    iput-object p12, p0, Lcom/squareup/orders/model/Order$Fulfillment;->was_status:Lcom/squareup/orders/model/Order$Fulfillment$State;

    .line 6864
    iput-object p13, p0, Lcom/squareup/orders/model/Order$Fulfillment;->ext_fulfillment_client_details:Lcom/squareup/protos/client/orders/FulfillmentClientDetails;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 6890
    :cond_0
    instance-of v1, p1, Lcom/squareup/orders/model/Order$Fulfillment;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 6891
    :cond_1
    check-cast p1, Lcom/squareup/orders/model/Order$Fulfillment;

    .line 6892
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$Fulfillment;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$Fulfillment;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment;->uid:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$Fulfillment;->uid:Ljava/lang/String;

    .line 6893
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment;->type:Lcom/squareup/orders/model/Order$FulfillmentType;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$Fulfillment;->type:Lcom/squareup/orders/model/Order$FulfillmentType;

    .line 6894
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment;->state:Lcom/squareup/orders/model/Order$Fulfillment$State;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$Fulfillment;->state:Lcom/squareup/orders/model/Order$Fulfillment$State;

    .line 6895
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment;->line_item_application:Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$Fulfillment;->line_item_application:Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;

    .line 6896
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment;->entries:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$Fulfillment;->entries:Ljava/util/List;

    .line 6897
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment;->metadata:Ljava/util/Map;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$Fulfillment;->metadata:Ljava/util/Map;

    .line 6898
    invoke-interface {v1, v3}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment;->pickup_details:Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$Fulfillment;->pickup_details:Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;

    .line 6899
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment;->managed_delivery_details:Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$Fulfillment;->managed_delivery_details:Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;

    .line 6900
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment;->shipment_details:Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$Fulfillment;->shipment_details:Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;

    .line 6901
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment;->digital_details:Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$Fulfillment;->digital_details:Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;

    .line 6902
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment;->delivery_details:Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$Fulfillment;->delivery_details:Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;

    .line 6903
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment;->was_status:Lcom/squareup/orders/model/Order$Fulfillment$State;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$Fulfillment;->was_status:Lcom/squareup/orders/model/Order$Fulfillment$State;

    .line 6904
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment;->ext_fulfillment_client_details:Lcom/squareup/protos/client/orders/FulfillmentClientDetails;

    iget-object p1, p1, Lcom/squareup/orders/model/Order$Fulfillment;->ext_fulfillment_client_details:Lcom/squareup/protos/client/orders/FulfillmentClientDetails;

    .line 6905
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 6910
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_b

    .line 6912
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$Fulfillment;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 6913
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment;->uid:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6914
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment;->type:Lcom/squareup/orders/model/Order$FulfillmentType;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$FulfillmentType;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6915
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment;->state:Lcom/squareup/orders/model/Order$Fulfillment$State;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$Fulfillment$State;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6916
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment;->line_item_application:Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6917
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment;->entries:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6918
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment;->metadata:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6919
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment;->pickup_details:Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6920
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment;->managed_delivery_details:Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6921
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment;->shipment_details:Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6922
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment;->digital_details:Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6923
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment;->delivery_details:Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6924
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment;->was_status:Lcom/squareup/orders/model/Order$Fulfillment$State;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$Fulfillment$State;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 6925
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment;->ext_fulfillment_client_details:Lcom/squareup/protos/client/orders/FulfillmentClientDetails;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Lcom/squareup/protos/client/orders/FulfillmentClientDetails;->hashCode()I

    move-result v2

    :cond_a
    add-int/2addr v0, v2

    .line 6926
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_b
    return v0
.end method

.method public newBuilder()Lcom/squareup/orders/model/Order$Fulfillment$Builder;
    .locals 2

    .line 6869
    new-instance v0, Lcom/squareup/orders/model/Order$Fulfillment$Builder;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$Fulfillment$Builder;-><init>()V

    .line 6870
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment;->uid:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->uid:Ljava/lang/String;

    .line 6871
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment;->type:Lcom/squareup/orders/model/Order$FulfillmentType;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->type:Lcom/squareup/orders/model/Order$FulfillmentType;

    .line 6872
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment;->state:Lcom/squareup/orders/model/Order$Fulfillment$State;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->state:Lcom/squareup/orders/model/Order$Fulfillment$State;

    .line 6873
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment;->line_item_application:Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->line_item_application:Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;

    .line 6874
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment;->entries:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->entries:Ljava/util/List;

    .line 6875
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment;->metadata:Ljava/util/Map;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->metadata:Ljava/util/Map;

    .line 6876
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment;->pickup_details:Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->pickup_details:Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;

    .line 6877
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment;->managed_delivery_details:Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->managed_delivery_details:Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;

    .line 6878
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment;->shipment_details:Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->shipment_details:Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;

    .line 6879
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment;->digital_details:Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->digital_details:Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;

    .line 6880
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment;->delivery_details:Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->delivery_details:Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;

    .line 6881
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment;->was_status:Lcom/squareup/orders/model/Order$Fulfillment$State;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->was_status:Lcom/squareup/orders/model/Order$Fulfillment$State;

    .line 6882
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment;->ext_fulfillment_client_details:Lcom/squareup/protos/client/orders/FulfillmentClientDetails;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->ext_fulfillment_client_details:Lcom/squareup/protos/client/orders/FulfillmentClientDetails;

    .line 6883
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$Fulfillment;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/orders/model/Order$Fulfillment$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 6663
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$Fulfillment;->newBuilder()Lcom/squareup/orders/model/Order$Fulfillment$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 6933
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 6934
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment;->uid:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment;->uid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 6935
    :cond_0
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment;->type:Lcom/squareup/orders/model/Order$FulfillmentType;

    if-eqz v1, :cond_1

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment;->type:Lcom/squareup/orders/model/Order$FulfillmentType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6936
    :cond_1
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment;->state:Lcom/squareup/orders/model/Order$Fulfillment$State;

    if-eqz v1, :cond_2

    const-string v1, ", state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment;->state:Lcom/squareup/orders/model/Order$Fulfillment$State;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6937
    :cond_2
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment;->line_item_application:Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;

    if-eqz v1, :cond_3

    const-string v1, ", line_item_application="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment;->line_item_application:Lcom/squareup/orders/model/Order$Fulfillment$FulfillmentLineItemApplication;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6938
    :cond_3
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment;->entries:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, ", entries="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment;->entries:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6939
    :cond_4
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment;->metadata:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, ", metadata=\u2588\u2588"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 6940
    :cond_5
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment;->pickup_details:Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;

    if-eqz v1, :cond_6

    const-string v1, ", pickup_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment;->pickup_details:Lcom/squareup/orders/model/Order$FulfillmentPickupDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6941
    :cond_6
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment;->managed_delivery_details:Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;

    if-eqz v1, :cond_7

    const-string v1, ", managed_delivery_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment;->managed_delivery_details:Lcom/squareup/orders/model/Order$FulfillmentManagedDeliveryDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6942
    :cond_7
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment;->shipment_details:Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;

    if-eqz v1, :cond_8

    const-string v1, ", shipment_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment;->shipment_details:Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6943
    :cond_8
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment;->digital_details:Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;

    if-eqz v1, :cond_9

    const-string v1, ", digital_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment;->digital_details:Lcom/squareup/orders/model/Order$FulfillmentDigitalDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6944
    :cond_9
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment;->delivery_details:Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;

    if-eqz v1, :cond_a

    const-string v1, ", delivery_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment;->delivery_details:Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6945
    :cond_a
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment;->was_status:Lcom/squareup/orders/model/Order$Fulfillment$State;

    if-eqz v1, :cond_b

    const-string v1, ", was_status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment;->was_status:Lcom/squareup/orders/model/Order$Fulfillment$State;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 6946
    :cond_b
    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment;->ext_fulfillment_client_details:Lcom/squareup/protos/client/orders/FulfillmentClientDetails;

    if-eqz v1, :cond_c

    const-string v1, ", ext_fulfillment_client_details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$Fulfillment;->ext_fulfillment_client_details:Lcom/squareup/protos/client/orders/FulfillmentClientDetails;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_c
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Fulfillment{"

    .line 6947
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
