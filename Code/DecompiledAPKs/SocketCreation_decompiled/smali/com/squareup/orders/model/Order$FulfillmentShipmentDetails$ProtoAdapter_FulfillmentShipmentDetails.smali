.class final Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$ProtoAdapter_FulfillmentShipmentDetails;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_FulfillmentShipmentDetails"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 9693
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 9742
    new-instance v0, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;-><init>()V

    .line 9743
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 9744
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 9764
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 9762
    :pswitch_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->in_progress_at(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;

    goto :goto_0

    .line 9761
    :pswitch_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->expected_shipped_at(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;

    goto :goto_0

    .line 9760
    :pswitch_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->tracking_url(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;

    goto :goto_0

    .line 9759
    :pswitch_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->failure_reason(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;

    goto :goto_0

    .line 9758
    :pswitch_4
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->failed_at(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;

    goto :goto_0

    .line 9757
    :pswitch_5
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->cancel_reason(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;

    goto :goto_0

    .line 9756
    :pswitch_6
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->canceled_at(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;

    goto :goto_0

    .line 9755
    :pswitch_7
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->delivered_at(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;

    goto :goto_0

    .line 9754
    :pswitch_8
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->expected_delivered_at(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;

    goto :goto_0

    .line 9753
    :pswitch_9
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->shipped_at(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;

    goto/16 :goto_0

    .line 9752
    :pswitch_a
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->packaged_at(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;

    goto/16 :goto_0

    .line 9751
    :pswitch_b
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->placed_at(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;

    goto/16 :goto_0

    .line 9750
    :pswitch_c
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->tracking_number(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;

    goto/16 :goto_0

    .line 9749
    :pswitch_d
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->shipping_type(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;

    goto/16 :goto_0

    .line 9748
    :pswitch_e
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->shipping_note(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;

    goto/16 :goto_0

    .line 9747
    :pswitch_f
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->carrier(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;

    goto/16 :goto_0

    .line 9746
    :pswitch_10
    sget-object v3, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->recipient(Lcom/squareup/orders/model/Order$FulfillmentRecipient;)Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;

    goto/16 :goto_0

    .line 9768
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 9769
    invoke-virtual {v0}, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->build()Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 9691
    invoke-virtual {p0, p1}, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$ProtoAdapter_FulfillmentShipmentDetails;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 9720
    sget-object v0, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->recipient:Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 9721
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->carrier:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 9722
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->shipping_note:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 9723
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->shipping_type:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 9724
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->tracking_number:Ljava/lang/String;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 9725
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->tracking_url:Ljava/lang/String;

    const/16 v2, 0xf

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 9726
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->placed_at:Ljava/lang/String;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 9727
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->in_progress_at:Ljava/lang/String;

    const/16 v2, 0x11

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 9728
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->packaged_at:Ljava/lang/String;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 9729
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->expected_shipped_at:Ljava/lang/String;

    const/16 v2, 0x10

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 9730
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->shipped_at:Ljava/lang/String;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 9731
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->expected_delivered_at:Ljava/lang/String;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 9732
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->delivered_at:Ljava/lang/String;

    const/16 v2, 0xa

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 9733
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->canceled_at:Ljava/lang/String;

    const/16 v2, 0xb

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 9734
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->cancel_reason:Ljava/lang/String;

    const/16 v2, 0xc

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 9735
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->failed_at:Ljava/lang/String;

    const/16 v2, 0xd

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 9736
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->failure_reason:Ljava/lang/String;

    const/16 v2, 0xe

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 9737
    invoke-virtual {p2}, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 9691
    check-cast p2, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$ProtoAdapter_FulfillmentShipmentDetails;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;)I
    .locals 4

    .line 9698
    sget-object v0, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->recipient:Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->carrier:Ljava/lang/String;

    const/4 v3, 0x2

    .line 9699
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->shipping_note:Ljava/lang/String;

    const/4 v3, 0x3

    .line 9700
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->shipping_type:Ljava/lang/String;

    const/4 v3, 0x4

    .line 9701
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->tracking_number:Ljava/lang/String;

    const/4 v3, 0x5

    .line 9702
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->tracking_url:Ljava/lang/String;

    const/16 v3, 0xf

    .line 9703
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->placed_at:Ljava/lang/String;

    const/4 v3, 0x6

    .line 9704
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->in_progress_at:Ljava/lang/String;

    const/16 v3, 0x11

    .line 9705
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->packaged_at:Ljava/lang/String;

    const/4 v3, 0x7

    .line 9706
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->expected_shipped_at:Ljava/lang/String;

    const/16 v3, 0x10

    .line 9707
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->shipped_at:Ljava/lang/String;

    const/16 v3, 0x8

    .line 9708
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->expected_delivered_at:Ljava/lang/String;

    const/16 v3, 0x9

    .line 9709
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->delivered_at:Ljava/lang/String;

    const/16 v3, 0xa

    .line 9710
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->canceled_at:Ljava/lang/String;

    const/16 v3, 0xb

    .line 9711
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->cancel_reason:Ljava/lang/String;

    const/16 v3, 0xc

    .line 9712
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->failed_at:Ljava/lang/String;

    const/16 v3, 0xd

    .line 9713
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->failure_reason:Ljava/lang/String;

    const/16 v3, 0xe

    .line 9714
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9715
    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 9691
    check-cast p1, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$ProtoAdapter_FulfillmentShipmentDetails;->encodedSize(Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;)Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;
    .locals 2

    .line 9774
    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;->newBuilder()Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;

    move-result-object p1

    .line 9775
    iget-object v0, p1, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->recipient:Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/orders/model/Order$FulfillmentRecipient;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->recipient:Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    iput-object v0, p1, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->recipient:Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    .line 9776
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 9777
    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$Builder;->build()Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 9691
    check-cast p1, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails$ProtoAdapter_FulfillmentShipmentDetails;->redact(Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;)Lcom/squareup/orders/model/Order$FulfillmentShipmentDetails;

    move-result-object p1

    return-object p1
.end method
