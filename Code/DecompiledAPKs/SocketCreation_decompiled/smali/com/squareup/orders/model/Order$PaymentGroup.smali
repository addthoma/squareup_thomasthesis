.class public final Lcom/squareup/orders/model/Order$PaymentGroup;
.super Lcom/squareup/wire/Message;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PaymentGroup"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orders/model/Order$PaymentGroup$ProtoAdapter_PaymentGroup;,
        Lcom/squareup/orders/model/Order$PaymentGroup$State;,
        Lcom/squareup/orders/model/Order$PaymentGroup$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/orders/model/Order$PaymentGroup;",
        "Lcom/squareup/orders/model/Order$PaymentGroup$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/orders/model/Order$PaymentGroup;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_STATE:Lcom/squareup/orders/model/Order$PaymentGroup$State;

.field public static final DEFAULT_UID:Ljava/lang/String; = ""

.field public static final DEFAULT_WILL_COMPLETE_AT:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final payment_ids:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x3
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final state:Lcom/squareup/orders/model/Order$PaymentGroup$State;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$PaymentGroup$State#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final uid:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final will_complete_at:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 11501
    new-instance v0, Lcom/squareup/orders/model/Order$PaymentGroup$ProtoAdapter_PaymentGroup;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$PaymentGroup$ProtoAdapter_PaymentGroup;-><init>()V

    sput-object v0, Lcom/squareup/orders/model/Order$PaymentGroup;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 11507
    sget-object v0, Lcom/squareup/orders/model/Order$PaymentGroup$State;->DO_NOT_USE:Lcom/squareup/orders/model/Order$PaymentGroup$State;

    sput-object v0, Lcom/squareup/orders/model/Order$PaymentGroup;->DEFAULT_STATE:Lcom/squareup/orders/model/Order$PaymentGroup$State;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/orders/model/Order$PaymentGroup$State;Ljava/util/List;Ljava/lang/String;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$PaymentGroup$State;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 11552
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/orders/model/Order$PaymentGroup;-><init>(Ljava/lang/String;Lcom/squareup/orders/model/Order$PaymentGroup$State;Ljava/util/List;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/squareup/orders/model/Order$PaymentGroup$State;Ljava/util/List;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/orders/model/Order$PaymentGroup$State;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 11557
    sget-object v0, Lcom/squareup/orders/model/Order$PaymentGroup;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 11558
    iput-object p1, p0, Lcom/squareup/orders/model/Order$PaymentGroup;->uid:Ljava/lang/String;

    .line 11559
    iput-object p2, p0, Lcom/squareup/orders/model/Order$PaymentGroup;->state:Lcom/squareup/orders/model/Order$PaymentGroup$State;

    const-string p1, "payment_ids"

    .line 11560
    invoke-static {p1, p3}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/orders/model/Order$PaymentGroup;->payment_ids:Ljava/util/List;

    .line 11561
    iput-object p4, p0, Lcom/squareup/orders/model/Order$PaymentGroup;->will_complete_at:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 11578
    :cond_0
    instance-of v1, p1, Lcom/squareup/orders/model/Order$PaymentGroup;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 11579
    :cond_1
    check-cast p1, Lcom/squareup/orders/model/Order$PaymentGroup;

    .line 11580
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$PaymentGroup;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$PaymentGroup;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$PaymentGroup;->uid:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$PaymentGroup;->uid:Ljava/lang/String;

    .line 11581
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$PaymentGroup;->state:Lcom/squareup/orders/model/Order$PaymentGroup$State;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$PaymentGroup;->state:Lcom/squareup/orders/model/Order$PaymentGroup$State;

    .line 11582
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$PaymentGroup;->payment_ids:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$PaymentGroup;->payment_ids:Ljava/util/List;

    .line 11583
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$PaymentGroup;->will_complete_at:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/orders/model/Order$PaymentGroup;->will_complete_at:Ljava/lang/String;

    .line 11584
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 11589
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_3

    .line 11591
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$PaymentGroup;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 11592
    iget-object v1, p0, Lcom/squareup/orders/model/Order$PaymentGroup;->uid:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 11593
    iget-object v1, p0, Lcom/squareup/orders/model/Order$PaymentGroup;->state:Lcom/squareup/orders/model/Order$PaymentGroup$State;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$PaymentGroup$State;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 11594
    iget-object v1, p0, Lcom/squareup/orders/model/Order$PaymentGroup;->payment_ids:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 11595
    iget-object v1, p0, Lcom/squareup/orders/model/Order$PaymentGroup;->will_complete_at:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_2
    add-int/2addr v0, v2

    .line 11596
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_3
    return v0
.end method

.method public newBuilder()Lcom/squareup/orders/model/Order$PaymentGroup$Builder;
    .locals 2

    .line 11566
    new-instance v0, Lcom/squareup/orders/model/Order$PaymentGroup$Builder;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$PaymentGroup$Builder;-><init>()V

    .line 11567
    iget-object v1, p0, Lcom/squareup/orders/model/Order$PaymentGroup;->uid:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$PaymentGroup$Builder;->uid:Ljava/lang/String;

    .line 11568
    iget-object v1, p0, Lcom/squareup/orders/model/Order$PaymentGroup;->state:Lcom/squareup/orders/model/Order$PaymentGroup$State;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$PaymentGroup$Builder;->state:Lcom/squareup/orders/model/Order$PaymentGroup$State;

    .line 11569
    iget-object v1, p0, Lcom/squareup/orders/model/Order$PaymentGroup;->payment_ids:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/orders/model/Order$PaymentGroup$Builder;->payment_ids:Ljava/util/List;

    .line 11570
    iget-object v1, p0, Lcom/squareup/orders/model/Order$PaymentGroup;->will_complete_at:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$PaymentGroup$Builder;->will_complete_at:Ljava/lang/String;

    .line 11571
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$PaymentGroup;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/orders/model/Order$PaymentGroup$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 11500
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$PaymentGroup;->newBuilder()Lcom/squareup/orders/model/Order$PaymentGroup$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 11603
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 11604
    iget-object v1, p0, Lcom/squareup/orders/model/Order$PaymentGroup;->uid:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$PaymentGroup;->uid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11605
    :cond_0
    iget-object v1, p0, Lcom/squareup/orders/model/Order$PaymentGroup;->state:Lcom/squareup/orders/model/Order$PaymentGroup$State;

    if-eqz v1, :cond_1

    const-string v1, ", state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$PaymentGroup;->state:Lcom/squareup/orders/model/Order$PaymentGroup$State;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 11606
    :cond_1
    iget-object v1, p0, Lcom/squareup/orders/model/Order$PaymentGroup;->payment_ids:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, ", payment_ids="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$PaymentGroup;->payment_ids:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 11607
    :cond_2
    iget-object v1, p0, Lcom/squareup/orders/model/Order$PaymentGroup;->will_complete_at:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", will_complete_at="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$PaymentGroup;->will_complete_at:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "PaymentGroup{"

    .line 11608
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
