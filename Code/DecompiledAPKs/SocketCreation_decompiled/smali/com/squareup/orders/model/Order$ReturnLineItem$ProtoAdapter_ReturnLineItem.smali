.class final Lcom/squareup/orders/model/Order$ReturnLineItem$ProtoAdapter_ReturnLineItem;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order$ReturnLineItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_ReturnLineItem"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/orders/model/Order$ReturnLineItem;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 12917
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/orders/model/Order$ReturnLineItem;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/orders/model/Order$ReturnLineItem;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 12982
    new-instance v0, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;-><init>()V

    .line 12983
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 12984
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    packed-switch v3, :pswitch_data_0

    .line 13019
    :pswitch_0
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 13017
    :pswitch_1
    sget-object v3, Lcom/squareup/orders/model/Order$QuantityUnit;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/orders/model/Order$QuantityUnit;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->quantity_unit(Lcom/squareup/orders/model/Order$QuantityUnit;)Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;

    goto :goto_0

    .line 13016
    :pswitch_2
    sget-object v3, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->total_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;

    goto :goto_0

    .line 13015
    :pswitch_3
    sget-object v3, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->total_discount_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;

    goto :goto_0

    .line 13014
    :pswitch_4
    sget-object v3, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->total_tax_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;

    goto :goto_0

    .line 13013
    :pswitch_5
    sget-object v3, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->gross_return_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;

    goto :goto_0

    .line 13012
    :pswitch_6
    sget-object v3, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->variation_total_price_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;

    goto :goto_0

    .line 13011
    :pswitch_7
    sget-object v3, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->base_price_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;

    goto :goto_0

    .line 13010
    :pswitch_8
    iget-object v3, v0, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->applied_discounts:Ljava/util/List;

    sget-object v4, Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 13009
    :pswitch_9
    iget-object v3, v0, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->applied_taxes:Ljava/util/List;

    sget-object v4, Lcom/squareup/orders/model/Order$LineItem$AppliedTax;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 13008
    :pswitch_a
    iget-object v3, v0, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->return_discounts:Ljava/util/List;

    sget-object v4, Lcom/squareup/orders/model/Order$ReturnDiscount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 13007
    :pswitch_b
    iget-object v3, v0, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->return_taxes:Ljava/util/List;

    sget-object v4, Lcom/squareup/orders/model/Order$ReturnTax;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 13006
    :pswitch_c
    iget-object v3, v0, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->return_modifiers:Ljava/util/List;

    sget-object v4, Lcom/squareup/orders/model/Order$ReturnLineItemModifier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 13005
    :pswitch_d
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->category_name(Ljava/lang/String;)Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;

    goto/16 :goto_0

    .line 13004
    :pswitch_e
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->sku(Ljava/lang/String;)Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;

    goto/16 :goto_0

    .line 12998
    :pswitch_f
    :try_start_0
    sget-object v4, Lcom/squareup/orders/model/Order$LineItem$ItemType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/orders/model/Order$LineItem$ItemType;

    invoke-virtual {v0, v4}, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->item_type(Lcom/squareup/orders/model/Order$LineItem$ItemType;)Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v4

    .line 13000
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto/16 :goto_0

    .line 12995
    :pswitch_10
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->catalog_category_id(Ljava/lang/String;)Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;

    goto/16 :goto_0

    .line 12994
    :pswitch_11
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->catalog_item_id(Ljava/lang/String;)Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;

    goto/16 :goto_0

    .line 12993
    :pswitch_12
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->variation_name(Ljava/lang/String;)Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;

    goto/16 :goto_0

    .line 12992
    :pswitch_13
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->catalog_object_version(Ljava/lang/Long;)Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;

    goto/16 :goto_0

    .line 12991
    :pswitch_14
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->catalog_object_id(Ljava/lang/String;)Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;

    goto/16 :goto_0

    .line 12990
    :pswitch_15
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->note(Ljava/lang/String;)Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;

    goto/16 :goto_0

    .line 12989
    :pswitch_16
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->quantity(Ljava/lang/String;)Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;

    goto/16 :goto_0

    .line 12988
    :pswitch_17
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->name(Ljava/lang/String;)Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;

    goto/16 :goto_0

    .line 12987
    :pswitch_18
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->source_line_item_uid(Ljava/lang/String;)Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;

    goto/16 :goto_0

    .line 12986
    :pswitch_19
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->uid(Ljava/lang/String;)Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;

    goto/16 :goto_0

    .line 13023
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 13024
    invoke-virtual {v0}, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->build()Lcom/squareup/orders/model/Order$ReturnLineItem;

    move-result-object p1

    return-object p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_19
        :pswitch_18
        :pswitch_0
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_0
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 12915
    invoke-virtual {p0, p1}, Lcom/squareup/orders/model/Order$ReturnLineItem$ProtoAdapter_ReturnLineItem;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/orders/model/Order$ReturnLineItem;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/orders/model/Order$ReturnLineItem;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 12952
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$ReturnLineItem;->uid:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 12953
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$ReturnLineItem;->source_line_item_uid:Ljava/lang/String;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 12954
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$ReturnLineItem;->name:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 12955
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$ReturnLineItem;->quantity:Ljava/lang/String;

    const/4 v2, 0x5

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 12956
    sget-object v0, Lcom/squareup/orders/model/Order$QuantityUnit;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$ReturnLineItem;->quantity_unit:Lcom/squareup/orders/model/Order$QuantityUnit;

    const/16 v2, 0x1b

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 12957
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$ReturnLineItem;->note:Ljava/lang/String;

    const/4 v2, 0x6

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 12958
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$ReturnLineItem;->catalog_object_id:Ljava/lang/String;

    const/4 v2, 0x7

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 12959
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$ReturnLineItem;->catalog_object_version:Ljava/lang/Long;

    const/16 v2, 0x8

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 12960
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$ReturnLineItem;->variation_name:Ljava/lang/String;

    const/16 v2, 0x9

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 12961
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$ReturnLineItem;->catalog_item_id:Ljava/lang/String;

    const/16 v2, 0xa

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 12962
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$ReturnLineItem;->catalog_category_id:Ljava/lang/String;

    const/16 v2, 0xb

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 12963
    sget-object v0, Lcom/squareup/orders/model/Order$LineItem$ItemType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$ReturnLineItem;->item_type:Lcom/squareup/orders/model/Order$LineItem$ItemType;

    const/16 v2, 0xc

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 12964
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$ReturnLineItem;->sku:Ljava/lang/String;

    const/16 v2, 0xd

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 12965
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$ReturnLineItem;->category_name:Ljava/lang/String;

    const/16 v2, 0xe

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 12966
    sget-object v0, Lcom/squareup/orders/model/Order$ReturnLineItemModifier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/orders/model/Order$ReturnLineItem;->return_modifiers:Ljava/util/List;

    const/16 v2, 0x10

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 12967
    sget-object v0, Lcom/squareup/orders/model/Order$ReturnTax;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/orders/model/Order$ReturnLineItem;->return_taxes:Ljava/util/List;

    const/16 v2, 0x11

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 12968
    sget-object v0, Lcom/squareup/orders/model/Order$ReturnDiscount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/orders/model/Order$ReturnLineItem;->return_discounts:Ljava/util/List;

    const/16 v2, 0x12

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 12969
    sget-object v0, Lcom/squareup/orders/model/Order$LineItem$AppliedTax;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/orders/model/Order$ReturnLineItem;->applied_taxes:Ljava/util/List;

    const/16 v2, 0x13

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 12970
    sget-object v0, Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/orders/model/Order$ReturnLineItem;->applied_discounts:Ljava/util/List;

    const/16 v2, 0x14

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 12971
    sget-object v0, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$ReturnLineItem;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    const/16 v2, 0x15

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 12972
    sget-object v0, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$ReturnLineItem;->variation_total_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    const/16 v2, 0x16

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 12973
    sget-object v0, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$ReturnLineItem;->gross_return_money:Lcom/squareup/protos/connect/v2/common/Money;

    const/16 v2, 0x17

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 12974
    sget-object v0, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$ReturnLineItem;->total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    const/16 v2, 0x18

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 12975
    sget-object v0, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$ReturnLineItem;->total_discount_money:Lcom/squareup/protos/connect/v2/common/Money;

    const/16 v2, 0x19

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 12976
    sget-object v0, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$ReturnLineItem;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    const/16 v2, 0x1a

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 12977
    invoke-virtual {p2}, Lcom/squareup/orders/model/Order$ReturnLineItem;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 12915
    check-cast p2, Lcom/squareup/orders/model/Order$ReturnLineItem;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/orders/model/Order$ReturnLineItem$ProtoAdapter_ReturnLineItem;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/orders/model/Order$ReturnLineItem;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/orders/model/Order$ReturnLineItem;)I
    .locals 4

    .line 12922
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/orders/model/Order$ReturnLineItem;->uid:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$ReturnLineItem;->source_line_item_uid:Ljava/lang/String;

    const/4 v3, 0x2

    .line 12923
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$ReturnLineItem;->name:Ljava/lang/String;

    const/4 v3, 0x4

    .line 12924
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$ReturnLineItem;->quantity:Ljava/lang/String;

    const/4 v3, 0x5

    .line 12925
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/orders/model/Order$QuantityUnit;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$ReturnLineItem;->quantity_unit:Lcom/squareup/orders/model/Order$QuantityUnit;

    const/16 v3, 0x1b

    .line 12926
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$ReturnLineItem;->note:Ljava/lang/String;

    const/4 v3, 0x6

    .line 12927
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$ReturnLineItem;->catalog_object_id:Ljava/lang/String;

    const/4 v3, 0x7

    .line 12928
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$ReturnLineItem;->catalog_object_version:Ljava/lang/Long;

    const/16 v3, 0x8

    .line 12929
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$ReturnLineItem;->variation_name:Ljava/lang/String;

    const/16 v3, 0x9

    .line 12930
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$ReturnLineItem;->catalog_item_id:Ljava/lang/String;

    const/16 v3, 0xa

    .line 12931
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$ReturnLineItem;->catalog_category_id:Ljava/lang/String;

    const/16 v3, 0xb

    .line 12932
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/orders/model/Order$LineItem$ItemType;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$ReturnLineItem;->item_type:Lcom/squareup/orders/model/Order$LineItem$ItemType;

    const/16 v3, 0xc

    .line 12933
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$ReturnLineItem;->sku:Ljava/lang/String;

    const/16 v3, 0xd

    .line 12934
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$ReturnLineItem;->category_name:Ljava/lang/String;

    const/16 v3, 0xe

    .line 12935
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/orders/model/Order$ReturnLineItemModifier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 12936
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/orders/model/Order$ReturnLineItem;->return_modifiers:Ljava/util/List;

    const/16 v3, 0x10

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/orders/model/Order$ReturnTax;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 12937
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/orders/model/Order$ReturnLineItem;->return_taxes:Ljava/util/List;

    const/16 v3, 0x11

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/orders/model/Order$ReturnDiscount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 12938
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/orders/model/Order$ReturnLineItem;->return_discounts:Ljava/util/List;

    const/16 v3, 0x12

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/orders/model/Order$LineItem$AppliedTax;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 12939
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/orders/model/Order$ReturnLineItem;->applied_taxes:Ljava/util/List;

    const/16 v3, 0x13

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 12940
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/orders/model/Order$ReturnLineItem;->applied_discounts:Ljava/util/List;

    const/16 v3, 0x14

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$ReturnLineItem;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    const/16 v3, 0x15

    .line 12941
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$ReturnLineItem;->variation_total_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    const/16 v3, 0x16

    .line 12942
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$ReturnLineItem;->gross_return_money:Lcom/squareup/protos/connect/v2/common/Money;

    const/16 v3, 0x17

    .line 12943
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$ReturnLineItem;->total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    const/16 v3, 0x18

    .line 12944
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$ReturnLineItem;->total_discount_money:Lcom/squareup/protos/connect/v2/common/Money;

    const/16 v3, 0x19

    .line 12945
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$ReturnLineItem;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    const/16 v3, 0x1a

    .line 12946
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 12947
    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$ReturnLineItem;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 12915
    check-cast p1, Lcom/squareup/orders/model/Order$ReturnLineItem;

    invoke-virtual {p0, p1}, Lcom/squareup/orders/model/Order$ReturnLineItem$ProtoAdapter_ReturnLineItem;->encodedSize(Lcom/squareup/orders/model/Order$ReturnLineItem;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/orders/model/Order$ReturnLineItem;)Lcom/squareup/orders/model/Order$ReturnLineItem;
    .locals 2

    .line 13029
    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$ReturnLineItem;->newBuilder()Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;

    move-result-object p1

    .line 13030
    iget-object v0, p1, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->quantity_unit:Lcom/squareup/orders/model/Order$QuantityUnit;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/orders/model/Order$QuantityUnit;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->quantity_unit:Lcom/squareup/orders/model/Order$QuantityUnit;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/orders/model/Order$QuantityUnit;

    iput-object v0, p1, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->quantity_unit:Lcom/squareup/orders/model/Order$QuantityUnit;

    .line 13031
    :cond_0
    iget-object v0, p1, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->return_modifiers:Ljava/util/List;

    sget-object v1, Lcom/squareup/orders/model/Order$ReturnLineItemModifier;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 13032
    iget-object v0, p1, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->return_taxes:Ljava/util/List;

    sget-object v1, Lcom/squareup/orders/model/Order$ReturnTax;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 13033
    iget-object v0, p1, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->return_discounts:Ljava/util/List;

    sget-object v1, Lcom/squareup/orders/model/Order$ReturnDiscount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 13034
    iget-object v0, p1, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->applied_taxes:Ljava/util/List;

    sget-object v1, Lcom/squareup/orders/model/Order$LineItem$AppliedTax;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 13035
    iget-object v0, p1, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->applied_discounts:Ljava/util/List;

    sget-object v1, Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 13036
    iget-object v0, p1, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v0, p1, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 13037
    :cond_1
    iget-object v0, p1, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->variation_total_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->variation_total_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v0, p1, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->variation_total_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 13038
    :cond_2
    iget-object v0, p1, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->gross_return_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->gross_return_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v0, p1, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->gross_return_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 13039
    :cond_3
    iget-object v0, p1, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v0, :cond_4

    sget-object v0, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v0, p1, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 13040
    :cond_4
    iget-object v0, p1, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->total_discount_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v0, :cond_5

    sget-object v0, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->total_discount_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v0, p1, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->total_discount_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 13041
    :cond_5
    iget-object v0, p1, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v0, :cond_6

    sget-object v0, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v0, p1, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 13042
    :cond_6
    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 13043
    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->build()Lcom/squareup/orders/model/Order$ReturnLineItem;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 12915
    check-cast p1, Lcom/squareup/orders/model/Order$ReturnLineItem;

    invoke-virtual {p0, p1}, Lcom/squareup/orders/model/Order$ReturnLineItem$ProtoAdapter_ReturnLineItem;->redact(Lcom/squareup/orders/model/Order$ReturnLineItem;)Lcom/squareup/orders/model/Order$ReturnLineItem;

    move-result-object p1

    return-object p1
.end method
