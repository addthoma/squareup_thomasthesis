.class final Lcom/squareup/orders/model/Order$Tip$ProtoAdapter_Tip;
.super Lcom/squareup/wire/ProtoAdapter;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order$Tip;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_Tip"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/orders/model/Order$Tip;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 5778
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/orders/model/Order$Tip;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/orders/model/Order$Tip;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 5797
    new-instance v0, Lcom/squareup/orders/model/Order$Tip$Builder;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$Tip$Builder;-><init>()V

    .line 5798
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 5799
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x1

    if-eq v3, v4, :cond_1

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    .line 5804
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 5802
    :cond_0
    sget-object v3, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$Tip$Builder;->applied_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/orders/model/Order$Tip$Builder;

    goto :goto_0

    .line 5801
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/orders/model/Order$Tip$Builder;->uid(Ljava/lang/String;)Lcom/squareup/orders/model/Order$Tip$Builder;

    goto :goto_0

    .line 5808
    :cond_2
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/orders/model/Order$Tip$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 5809
    invoke-virtual {v0}, Lcom/squareup/orders/model/Order$Tip$Builder;->build()Lcom/squareup/orders/model/Order$Tip;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 5776
    invoke-virtual {p0, p1}, Lcom/squareup/orders/model/Order$Tip$ProtoAdapter_Tip;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/orders/model/Order$Tip;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/orders/model/Order$Tip;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 5790
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$Tip;->uid:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 5791
    sget-object v0, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/orders/model/Order$Tip;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 5792
    invoke-virtual {p2}, Lcom/squareup/orders/model/Order$Tip;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 5776
    check-cast p2, Lcom/squareup/orders/model/Order$Tip;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/orders/model/Order$Tip$ProtoAdapter_Tip;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/orders/model/Order$Tip;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/orders/model/Order$Tip;)I
    .locals 4

    .line 5783
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/orders/model/Order$Tip;->uid:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/orders/model/Order$Tip;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    const/4 v3, 0x2

    .line 5784
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5785
    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$Tip;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 5776
    check-cast p1, Lcom/squareup/orders/model/Order$Tip;

    invoke-virtual {p0, p1}, Lcom/squareup/orders/model/Order$Tip$ProtoAdapter_Tip;->encodedSize(Lcom/squareup/orders/model/Order$Tip;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/orders/model/Order$Tip;)Lcom/squareup/orders/model/Order$Tip;
    .locals 2

    .line 5814
    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$Tip;->newBuilder()Lcom/squareup/orders/model/Order$Tip$Builder;

    move-result-object p1

    .line 5815
    iget-object v0, p1, Lcom/squareup/orders/model/Order$Tip$Builder;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/protos/connect/v2/common/Money;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/orders/model/Order$Tip$Builder;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v0, p1, Lcom/squareup/orders/model/Order$Tip$Builder;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 5816
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$Tip$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 5817
    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$Tip$Builder;->build()Lcom/squareup/orders/model/Order$Tip;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 5776
    check-cast p1, Lcom/squareup/orders/model/Order$Tip;

    invoke-virtual {p0, p1}, Lcom/squareup/orders/model/Order$Tip$ProtoAdapter_Tip;->redact(Lcom/squareup/orders/model/Order$Tip;)Lcom/squareup/orders/model/Order$Tip;

    move-result-object p1

    return-object p1
.end method
