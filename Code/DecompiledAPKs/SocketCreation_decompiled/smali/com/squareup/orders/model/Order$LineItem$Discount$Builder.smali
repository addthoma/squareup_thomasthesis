.class public final Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order$LineItem$Discount;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/orders/model/Order$LineItem$Discount;",
        "Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public amount_money:Lcom/squareup/protos/connect/v2/common/Money;

.field public applied_money:Lcom/squareup/protos/connect/v2/common/Money;

.field public catalog_object_id:Ljava/lang/String;

.field public catalog_object_version:Ljava/lang/Long;

.field public discount_code_ids:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public metadata:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public name:Ljava/lang/String;

.field public percentage:Ljava/lang/String;

.field public pricing_rule_id:Ljava/lang/String;

.field public quantity:Ljava/lang/String;

.field public reward_ids:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public scope:Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;

.field public type:Lcom/squareup/orders/model/Order$LineItem$Discount$Type;

.field public uid:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 3611
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 3612
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableMap()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;->metadata:Ljava/util/Map;

    .line 3613
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;->discount_code_ids:Ljava/util/List;

    .line 3614
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;->reward_ids:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public amount_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;
    .locals 0

    .line 3686
    iput-object p1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public applied_money(Lcom/squareup/protos/connect/v2/common/Money;)Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;
    .locals 0

    .line 3702
    iput-object p1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-object p0
.end method

.method public build()Lcom/squareup/orders/model/Order$LineItem$Discount;
    .locals 18

    move-object/from16 v0, p0

    .line 3815
    new-instance v17, Lcom/squareup/orders/model/Order$LineItem$Discount;

    iget-object v2, v0, Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;->uid:Ljava/lang/String;

    iget-object v3, v0, Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;->catalog_object_id:Ljava/lang/String;

    iget-object v4, v0, Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;->catalog_object_version:Ljava/lang/Long;

    iget-object v5, v0, Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;->name:Ljava/lang/String;

    iget-object v6, v0, Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;->type:Lcom/squareup/orders/model/Order$LineItem$Discount$Type;

    iget-object v7, v0, Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;->percentage:Ljava/lang/String;

    iget-object v8, v0, Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;->amount_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v9, v0, Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;->applied_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v10, v0, Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;->metadata:Ljava/util/Map;

    iget-object v11, v0, Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;->scope:Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;

    iget-object v12, v0, Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;->discount_code_ids:Ljava/util/List;

    iget-object v13, v0, Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;->reward_ids:Ljava/util/List;

    iget-object v14, v0, Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;->pricing_rule_id:Ljava/lang/String;

    iget-object v15, v0, Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;->quantity:Ljava/lang/String;

    invoke-super/range {p0 .. p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v16

    move-object/from16 v1, v17

    invoke-direct/range {v1 .. v16}, Lcom/squareup/orders/model/Order$LineItem$Discount;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Lcom/squareup/orders/model/Order$LineItem$Discount$Type;Ljava/lang/String;Lcom/squareup/protos/connect/v2/common/Money;Lcom/squareup/protos/connect/v2/common/Money;Ljava/util/Map;Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v17
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 3582
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;->build()Lcom/squareup/orders/model/Order$LineItem$Discount;

    move-result-object v0

    return-object v0
.end method

.method public catalog_object_id(Ljava/lang/String;)Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;
    .locals 0

    .line 3633
    iput-object p1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;->catalog_object_id:Ljava/lang/String;

    return-object p0
.end method

.method public catalog_object_version(Ljava/lang/Long;)Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;
    .locals 0

    .line 3638
    iput-object p1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;->catalog_object_version:Ljava/lang/Long;

    return-object p0
.end method

.method public discount_code_ids(Ljava/util/List;)Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;"
        }
    .end annotation

    .line 3764
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 3765
    iput-object p1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;->discount_code_ids:Ljava/util/List;

    return-object p0
.end method

.method public metadata(Ljava/util/Map;)Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;"
        }
    .end annotation

    .line 3729
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/Map;)V

    .line 3730
    iput-object p1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;->metadata:Ljava/util/Map;

    return-object p0
.end method

.method public name(Ljava/lang/String;)Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;
    .locals 0

    .line 3648
    iput-object p1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;->name:Ljava/lang/String;

    return-object p0
.end method

.method public percentage(Ljava/lang/String;)Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;
    .locals 0

    .line 3674
    iput-object p1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;->percentage:Ljava/lang/String;

    return-object p0
.end method

.method public pricing_rule_id(Ljava/lang/String;)Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;
    .locals 0

    .line 3797
    iput-object p1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;->pricing_rule_id:Ljava/lang/String;

    return-object p0
.end method

.method public quantity(Ljava/lang/String;)Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;
    .locals 0

    .line 3809
    iput-object p1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;->quantity:Ljava/lang/String;

    return-object p0
.end method

.method public reward_ids(Ljava/util/List;)Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;"
        }
    .end annotation

    .line 3782
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 3783
    iput-object p1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;->reward_ids:Ljava/util/List;

    return-object p0
.end method

.method public scope(Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;)Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;
    .locals 0

    .line 3746
    iput-object p1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;->scope:Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;

    return-object p0
.end method

.method public type(Lcom/squareup/orders/model/Order$LineItem$Discount$Type;)Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;
    .locals 0

    .line 3661
    iput-object p1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;->type:Lcom/squareup/orders/model/Order$LineItem$Discount$Type;

    return-object p0
.end method

.method public uid(Ljava/lang/String;)Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;
    .locals 0

    .line 3623
    iput-object p1, p0, Lcom/squareup/orders/model/Order$LineItem$Discount$Builder;->uid:Ljava/lang/String;

    return-object p0
.end method
