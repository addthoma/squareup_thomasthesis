.class public final Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;",
        "Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public cancel_reason:Ljava/lang/String;

.field public canceled_at:Ljava/lang/String;

.field public completed_at:Ljava/lang/String;

.field public deliver_at:Ljava/lang/String;

.field public delivered_at:Ljava/lang/String;

.field public delivery_window_duration:Ljava/lang/String;

.field public in_progress_at:Ljava/lang/String;

.field public note:Ljava/lang/String;

.field public placed_at:Ljava/lang/String;

.field public prep_time_duration:Ljava/lang/String;

.field public ready_at:Ljava/lang/String;

.field public recipient:Lcom/squareup/orders/model/Order$FulfillmentRecipient;

.field public rejected_at:Ljava/lang/String;

.field public schedule_type:Lcom/squareup/orders/model/Order$FulfillmentPickupDetailsScheduleType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 10366
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;
    .locals 18

    move-object/from16 v0, p0

    .line 10525
    new-instance v17, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;

    iget-object v2, v0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;->recipient:Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    iget-object v3, v0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;->schedule_type:Lcom/squareup/orders/model/Order$FulfillmentPickupDetailsScheduleType;

    iget-object v4, v0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;->placed_at:Ljava/lang/String;

    iget-object v5, v0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;->deliver_at:Ljava/lang/String;

    iget-object v6, v0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;->prep_time_duration:Ljava/lang/String;

    iget-object v7, v0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;->delivery_window_duration:Ljava/lang/String;

    iget-object v8, v0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;->note:Ljava/lang/String;

    iget-object v9, v0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;->completed_at:Ljava/lang/String;

    iget-object v10, v0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;->in_progress_at:Ljava/lang/String;

    iget-object v11, v0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;->rejected_at:Ljava/lang/String;

    iget-object v12, v0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;->ready_at:Ljava/lang/String;

    iget-object v13, v0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;->delivered_at:Ljava/lang/String;

    iget-object v14, v0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;->canceled_at:Ljava/lang/String;

    iget-object v15, v0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;->cancel_reason:Ljava/lang/String;

    invoke-super/range {p0 .. p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v16

    move-object/from16 v1, v17

    invoke-direct/range {v1 .. v16}, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;-><init>(Lcom/squareup/orders/model/Order$FulfillmentRecipient;Lcom/squareup/orders/model/Order$FulfillmentPickupDetailsScheduleType;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v17
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 10337
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;->build()Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails;

    move-result-object v0

    return-object v0
.end method

.method public cancel_reason(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;
    .locals 0

    .line 10519
    iput-object p1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;->cancel_reason:Ljava/lang/String;

    return-object p0
.end method

.method public canceled_at(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;
    .locals 0

    .line 10511
    iput-object p1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;->canceled_at:Ljava/lang/String;

    return-object p0
.end method

.method public completed_at(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;
    .locals 0

    .line 10450
    iput-object p1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;->completed_at:Ljava/lang/String;

    return-object p0
.end method

.method public deliver_at(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;
    .locals 0

    .line 10406
    iput-object p1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;->deliver_at:Ljava/lang/String;

    return-object p0
.end method

.method public delivered_at(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;
    .locals 0

    .line 10499
    iput-object p1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;->delivered_at:Ljava/lang/String;

    return-object p0
.end method

.method public delivery_window_duration(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;
    .locals 0

    .line 10428
    iput-object p1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;->delivery_window_duration:Ljava/lang/String;

    return-object p0
.end method

.method public in_progress_at(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;
    .locals 0

    .line 10462
    iput-object p1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;->in_progress_at:Ljava/lang/String;

    return-object p0
.end method

.method public note(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;
    .locals 0

    .line 10439
    iput-object p1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;->note:Ljava/lang/String;

    return-object p0
.end method

.method public placed_at(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;
    .locals 0

    .line 10393
    iput-object p1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;->placed_at:Ljava/lang/String;

    return-object p0
.end method

.method public prep_time_duration(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;
    .locals 0

    .line 10417
    iput-object p1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;->prep_time_duration:Ljava/lang/String;

    return-object p0
.end method

.method public ready_at(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;
    .locals 0

    .line 10487
    iput-object p1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;->ready_at:Ljava/lang/String;

    return-object p0
.end method

.method public recipient(Lcom/squareup/orders/model/Order$FulfillmentRecipient;)Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;
    .locals 0

    .line 10373
    iput-object p1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;->recipient:Lcom/squareup/orders/model/Order$FulfillmentRecipient;

    return-object p0
.end method

.method public rejected_at(Ljava/lang/String;)Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;
    .locals 0

    .line 10474
    iput-object p1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;->rejected_at:Ljava/lang/String;

    return-object p0
.end method

.method public schedule_type(Lcom/squareup/orders/model/Order$FulfillmentPickupDetailsScheduleType;)Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;
    .locals 0

    .line 10381
    iput-object p1, p0, Lcom/squareup/orders/model/Order$FulfillmentDeliveryDetails$Builder;->schedule_type:Lcom/squareup/orders/model/Order$FulfillmentPickupDetailsScheduleType;

    return-object p0
.end method
