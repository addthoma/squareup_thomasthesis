.class public final Lcom/squareup/orders/model/Order$RefundGroup$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order$RefundGroup;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/orders/model/Order$RefundGroup;",
        "Lcom/squareup/orders/model/Order$RefundGroup$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public refund_ids:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public state:Lcom/squareup/orders/model/Order$RefundGroup$State;

.field public uid:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 11888
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 11889
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/orders/model/Order$RefundGroup$Builder;->refund_ids:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/orders/model/Order$RefundGroup;
    .locals 5

    .line 11920
    new-instance v0, Lcom/squareup/orders/model/Order$RefundGroup;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$RefundGroup$Builder;->uid:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/orders/model/Order$RefundGroup$Builder;->state:Lcom/squareup/orders/model/Order$RefundGroup$State;

    iget-object v3, p0, Lcom/squareup/orders/model/Order$RefundGroup$Builder;->refund_ids:Ljava/util/List;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/orders/model/Order$RefundGroup;-><init>(Ljava/lang/String;Lcom/squareup/orders/model/Order$RefundGroup$State;Ljava/util/List;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 11881
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$RefundGroup$Builder;->build()Lcom/squareup/orders/model/Order$RefundGroup;

    move-result-object v0

    return-object v0
.end method

.method public refund_ids(Ljava/util/List;)Lcom/squareup/orders/model/Order$RefundGroup$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/orders/model/Order$RefundGroup$Builder;"
        }
    .end annotation

    .line 11913
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 11914
    iput-object p1, p0, Lcom/squareup/orders/model/Order$RefundGroup$Builder;->refund_ids:Ljava/util/List;

    return-object p0
.end method

.method public state(Lcom/squareup/orders/model/Order$RefundGroup$State;)Lcom/squareup/orders/model/Order$RefundGroup$Builder;
    .locals 0

    .line 11904
    iput-object p1, p0, Lcom/squareup/orders/model/Order$RefundGroup$Builder;->state:Lcom/squareup/orders/model/Order$RefundGroup$State;

    return-object p0
.end method

.method public uid(Ljava/lang/String;)Lcom/squareup/orders/model/Order$RefundGroup$Builder;
    .locals 0

    .line 11896
    iput-object p1, p0, Lcom/squareup/orders/model/Order$RefundGroup$Builder;->uid:Ljava/lang/String;

    return-object p0
.end method
