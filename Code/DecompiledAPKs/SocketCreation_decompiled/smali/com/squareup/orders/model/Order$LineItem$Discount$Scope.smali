.class public final enum Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;
.super Ljava/lang/Enum;
.source "Order.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order$LineItem$Discount;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Scope"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orders/model/Order$LineItem$Discount$Scope$ProtoAdapter_Scope;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum LINE_ITEM:Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;

.field public static final enum ORDER:Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;

.field public static final enum OTHER_DISCOUNT_SCOPE:Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 3920
    new-instance v0, Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;

    const/4 v1, 0x0

    const-string v2, "OTHER_DISCOUNT_SCOPE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;->OTHER_DISCOUNT_SCOPE:Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;

    .line 3928
    new-instance v0, Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;

    const/4 v2, 0x1

    const-string v3, "LINE_ITEM"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;->LINE_ITEM:Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;

    .line 3935
    new-instance v0, Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;

    const/4 v3, 0x2

    const-string v4, "ORDER"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;->ORDER:Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;

    .line 3913
    sget-object v4, Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;->OTHER_DISCOUNT_SCOPE:Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;

    aput-object v4, v0, v1

    sget-object v1, Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;->LINE_ITEM:Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;->ORDER:Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;

    aput-object v1, v0, v3

    sput-object v0, Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;->$VALUES:[Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;

    .line 3937
    new-instance v0, Lcom/squareup/orders/model/Order$LineItem$Discount$Scope$ProtoAdapter_Scope;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$LineItem$Discount$Scope$ProtoAdapter_Scope;-><init>()V

    sput-object v0, Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 3941
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 3942
    iput p3, p0, Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;->value:I

    return-void
.end method

.method public static fromValue(I)Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;
    .locals 1

    if-eqz p0, :cond_2

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 3952
    :cond_0
    sget-object p0, Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;->ORDER:Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;

    return-object p0

    .line 3951
    :cond_1
    sget-object p0, Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;->LINE_ITEM:Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;

    return-object p0

    .line 3950
    :cond_2
    sget-object p0, Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;->OTHER_DISCOUNT_SCOPE:Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;
    .locals 1

    .line 3913
    const-class v0, Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;

    return-object p0
.end method

.method public static values()[Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;
    .locals 1

    .line 3913
    sget-object v0, Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;->$VALUES:[Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;

    invoke-virtual {v0}, [Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 3959
    iget v0, p0, Lcom/squareup/orders/model/Order$LineItem$Discount$Scope;->value:I

    return v0
.end method
