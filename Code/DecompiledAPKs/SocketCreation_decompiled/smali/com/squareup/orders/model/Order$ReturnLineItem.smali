.class public final Lcom/squareup/orders/model/Order$ReturnLineItem;
.super Lcom/squareup/wire/Message;
.source "Order.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/model/Order;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ReturnLineItem"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orders/model/Order$ReturnLineItem$ProtoAdapter_ReturnLineItem;,
        Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/orders/model/Order$ReturnLineItem;",
        "Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/orders/model/Order$ReturnLineItem;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CATALOG_CATEGORY_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_CATALOG_ITEM_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_CATALOG_OBJECT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_CATALOG_OBJECT_VERSION:Ljava/lang/Long;

.field public static final DEFAULT_CATEGORY_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_ITEM_TYPE:Lcom/squareup/orders/model/Order$LineItem$ItemType;

.field public static final DEFAULT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_NOTE:Ljava/lang/String; = ""

.field public static final DEFAULT_QUANTITY:Ljava/lang/String; = ""

.field public static final DEFAULT_SKU:Ljava/lang/String; = ""

.field public static final DEFAULT_SOURCE_LINE_ITEM_UID:Ljava/lang/String; = ""

.field public static final DEFAULT_UID:Ljava/lang/String; = ""

.field public static final DEFAULT_VARIATION_NAME:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final applied_discounts:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$LineItem$AppliedDiscount#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x14
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$LineItem$AppliedDiscount;",
            ">;"
        }
    .end annotation
.end field

.field public final applied_taxes:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$LineItem$AppliedTax#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x13
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$LineItem$AppliedTax;",
            ">;"
        }
    .end annotation
.end field

.field public final base_price_money:Lcom/squareup/protos/connect/v2/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Money#ADAPTER"
        tag = 0x15
    .end annotation
.end field

.field public final catalog_category_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xb
    .end annotation
.end field

.field public final catalog_item_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xa
    .end annotation
.end field

.field public final catalog_object_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x7
    .end annotation
.end field

.field public final catalog_object_version:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x8
    .end annotation
.end field

.field public final category_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xe
    .end annotation
.end field

.field public final gross_return_money:Lcom/squareup/protos/connect/v2/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Money#ADAPTER"
        tag = 0x17
    .end annotation
.end field

.field public final item_type:Lcom/squareup/orders/model/Order$LineItem$ItemType;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$LineItem$ItemType#ADAPTER"
        tag = 0xc
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final note:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x6
    .end annotation
.end field

.field public final quantity:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field

.field public final quantity_unit:Lcom/squareup/orders/model/Order$QuantityUnit;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$QuantityUnit#ADAPTER"
        tag = 0x1b
    .end annotation
.end field

.field public final return_discounts:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$ReturnDiscount#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x12
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$ReturnDiscount;",
            ">;"
        }
    .end annotation
.end field

.field public final return_modifiers:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$ReturnLineItemModifier#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x10
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$ReturnLineItemModifier;",
            ">;"
        }
    .end annotation
.end field

.field public final return_taxes:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$ReturnTax#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x11
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/orders/model/Order$ReturnTax;",
            ">;"
        }
    .end annotation
.end field

.field public final sku:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0xd
    .end annotation
.end field

.field public final source_line_item_uid:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final total_discount_money:Lcom/squareup/protos/connect/v2/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Money#ADAPTER"
        tag = 0x19
    .end annotation
.end field

.field public final total_money:Lcom/squareup/protos/connect/v2/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Money#ADAPTER"
        tag = 0x1a
    .end annotation
.end field

.field public final total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Money#ADAPTER"
        tag = 0x18
    .end annotation
.end field

.field public final uid:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final variation_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x9
    .end annotation
.end field

.field public final variation_total_price_money:Lcom/squareup/protos/connect/v2/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.connect.v2.common.Money#ADAPTER"
        tag = 0x16
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 12043
    new-instance v0, Lcom/squareup/orders/model/Order$ReturnLineItem$ProtoAdapter_ReturnLineItem;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$ReturnLineItem$ProtoAdapter_ReturnLineItem;-><init>()V

    sput-object v0, Lcom/squareup/orders/model/Order$ReturnLineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-wide/16 v0, 0x0

    .line 12059
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/squareup/orders/model/Order$ReturnLineItem;->DEFAULT_CATALOG_OBJECT_VERSION:Ljava/lang/Long;

    .line 12067
    sget-object v0, Lcom/squareup/orders/model/Order$LineItem$ItemType;->ITEM:Lcom/squareup/orders/model/Order$LineItem$ItemType;

    sput-object v0, Lcom/squareup/orders/model/Order$ReturnLineItem;->DEFAULT_ITEM_TYPE:Lcom/squareup/orders/model/Order$LineItem$ItemType;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;Lokio/ByteString;)V
    .locals 1

    .line 12395
    sget-object v0, Lcom/squareup/orders/model/Order$ReturnLineItem;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 12396
    iget-object p2, p1, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->uid:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->uid:Ljava/lang/String;

    .line 12397
    iget-object p2, p1, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->source_line_item_uid:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->source_line_item_uid:Ljava/lang/String;

    .line 12398
    iget-object p2, p1, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->name:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->name:Ljava/lang/String;

    .line 12399
    iget-object p2, p1, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->quantity:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->quantity:Ljava/lang/String;

    .line 12400
    iget-object p2, p1, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->quantity_unit:Lcom/squareup/orders/model/Order$QuantityUnit;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->quantity_unit:Lcom/squareup/orders/model/Order$QuantityUnit;

    .line 12401
    iget-object p2, p1, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->note:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->note:Ljava/lang/String;

    .line 12402
    iget-object p2, p1, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->catalog_object_id:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->catalog_object_id:Ljava/lang/String;

    .line 12403
    iget-object p2, p1, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->catalog_object_version:Ljava/lang/Long;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->catalog_object_version:Ljava/lang/Long;

    .line 12404
    iget-object p2, p1, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->variation_name:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->variation_name:Ljava/lang/String;

    .line 12405
    iget-object p2, p1, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->catalog_item_id:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->catalog_item_id:Ljava/lang/String;

    .line 12406
    iget-object p2, p1, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->catalog_category_id:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->catalog_category_id:Ljava/lang/String;

    .line 12407
    iget-object p2, p1, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->item_type:Lcom/squareup/orders/model/Order$LineItem$ItemType;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->item_type:Lcom/squareup/orders/model/Order$LineItem$ItemType;

    .line 12408
    iget-object p2, p1, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->sku:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->sku:Ljava/lang/String;

    .line 12409
    iget-object p2, p1, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->category_name:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->category_name:Ljava/lang/String;

    .line 12410
    iget-object p2, p1, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->return_modifiers:Ljava/util/List;

    const-string v0, "return_modifiers"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->return_modifiers:Ljava/util/List;

    .line 12411
    iget-object p2, p1, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->return_taxes:Ljava/util/List;

    const-string v0, "return_taxes"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->return_taxes:Ljava/util/List;

    .line 12412
    iget-object p2, p1, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->return_discounts:Ljava/util/List;

    const-string v0, "return_discounts"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->return_discounts:Ljava/util/List;

    .line 12413
    iget-object p2, p1, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->applied_taxes:Ljava/util/List;

    const-string v0, "applied_taxes"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->applied_taxes:Ljava/util/List;

    .line 12414
    iget-object p2, p1, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->applied_discounts:Ljava/util/List;

    const-string v0, "applied_discounts"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->applied_discounts:Ljava/util/List;

    .line 12415
    iget-object p2, p1, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 12416
    iget-object p2, p1, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->variation_total_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->variation_total_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 12417
    iget-object p2, p1, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->gross_return_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->gross_return_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 12418
    iget-object p2, p1, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 12419
    iget-object p2, p1, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->total_discount_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object p2, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->total_discount_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 12420
    iget-object p1, p1, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object p1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 12458
    :cond_0
    instance-of v1, p1, Lcom/squareup/orders/model/Order$ReturnLineItem;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 12459
    :cond_1
    check-cast p1, Lcom/squareup/orders/model/Order$ReturnLineItem;

    .line 12460
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$ReturnLineItem;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/orders/model/Order$ReturnLineItem;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->uid:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ReturnLineItem;->uid:Ljava/lang/String;

    .line 12461
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->source_line_item_uid:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ReturnLineItem;->source_line_item_uid:Ljava/lang/String;

    .line 12462
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ReturnLineItem;->name:Ljava/lang/String;

    .line 12463
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->quantity:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ReturnLineItem;->quantity:Ljava/lang/String;

    .line 12464
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->quantity_unit:Lcom/squareup/orders/model/Order$QuantityUnit;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ReturnLineItem;->quantity_unit:Lcom/squareup/orders/model/Order$QuantityUnit;

    .line 12465
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->note:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ReturnLineItem;->note:Ljava/lang/String;

    .line 12466
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->catalog_object_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ReturnLineItem;->catalog_object_id:Ljava/lang/String;

    .line 12467
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->catalog_object_version:Ljava/lang/Long;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ReturnLineItem;->catalog_object_version:Ljava/lang/Long;

    .line 12468
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->variation_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ReturnLineItem;->variation_name:Ljava/lang/String;

    .line 12469
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->catalog_item_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ReturnLineItem;->catalog_item_id:Ljava/lang/String;

    .line 12470
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->catalog_category_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ReturnLineItem;->catalog_category_id:Ljava/lang/String;

    .line 12471
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->item_type:Lcom/squareup/orders/model/Order$LineItem$ItemType;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ReturnLineItem;->item_type:Lcom/squareup/orders/model/Order$LineItem$ItemType;

    .line 12472
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->sku:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ReturnLineItem;->sku:Ljava/lang/String;

    .line 12473
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->category_name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ReturnLineItem;->category_name:Ljava/lang/String;

    .line 12474
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->return_modifiers:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ReturnLineItem;->return_modifiers:Ljava/util/List;

    .line 12475
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->return_taxes:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ReturnLineItem;->return_taxes:Ljava/util/List;

    .line 12476
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->return_discounts:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ReturnLineItem;->return_discounts:Ljava/util/List;

    .line 12477
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->applied_taxes:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ReturnLineItem;->applied_taxes:Ljava/util/List;

    .line 12478
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->applied_discounts:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ReturnLineItem;->applied_discounts:Ljava/util/List;

    .line 12479
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ReturnLineItem;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 12480
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->variation_total_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ReturnLineItem;->variation_total_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 12481
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->gross_return_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ReturnLineItem;->gross_return_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 12482
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ReturnLineItem;->total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 12483
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->total_discount_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object v3, p1, Lcom/squareup/orders/model/Order$ReturnLineItem;->total_discount_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 12484
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    iget-object p1, p1, Lcom/squareup/orders/model/Order$ReturnLineItem;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 12485
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 12490
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_14

    .line 12492
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$ReturnLineItem;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 12493
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->uid:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 12494
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->source_line_item_uid:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 12495
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->name:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 12496
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->quantity:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 12497
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->quantity_unit:Lcom/squareup/orders/model/Order$QuantityUnit;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$QuantityUnit;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 12498
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->note:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 12499
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->catalog_object_id:Ljava/lang/String;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 12500
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->catalog_object_version:Ljava/lang/Long;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 12501
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->variation_name:Ljava/lang/String;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 12502
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->catalog_item_id:Ljava/lang/String;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 12503
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->catalog_category_id:Ljava/lang/String;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 12504
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->item_type:Lcom/squareup/orders/model/Order$LineItem$ItemType;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$LineItem$ItemType;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 12505
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->sku:Ljava/lang/String;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_c

    :cond_c
    const/4 v1, 0x0

    :goto_c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 12506
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->category_name:Ljava/lang/String;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_d

    :cond_d
    const/4 v1, 0x0

    :goto_d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 12507
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->return_modifiers:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 12508
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->return_taxes:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 12509
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->return_discounts:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 12510
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->applied_taxes:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 12511
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->applied_discounts:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 12512
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_e

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Money;->hashCode()I

    move-result v1

    goto :goto_e

    :cond_e
    const/4 v1, 0x0

    :goto_e
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 12513
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->variation_total_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_f

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Money;->hashCode()I

    move-result v1

    goto :goto_f

    :cond_f
    const/4 v1, 0x0

    :goto_f
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 12514
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->gross_return_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_10

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Money;->hashCode()I

    move-result v1

    goto :goto_10

    :cond_10
    const/4 v1, 0x0

    :goto_10
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 12515
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_11

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Money;->hashCode()I

    move-result v1

    goto :goto_11

    :cond_11
    const/4 v1, 0x0

    :goto_11
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 12516
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->total_discount_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_12

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Money;->hashCode()I

    move-result v1

    goto :goto_12

    :cond_12
    const/4 v1, 0x0

    :goto_12
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 12517
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_13

    invoke-virtual {v1}, Lcom/squareup/protos/connect/v2/common/Money;->hashCode()I

    move-result v2

    :cond_13
    add-int/2addr v0, v2

    .line 12518
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_14
    return v0
.end method

.method public newBuilder()Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;
    .locals 2

    .line 12425
    new-instance v0, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;

    invoke-direct {v0}, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;-><init>()V

    .line 12426
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->uid:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->uid:Ljava/lang/String;

    .line 12427
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->source_line_item_uid:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->source_line_item_uid:Ljava/lang/String;

    .line 12428
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->name:Ljava/lang/String;

    .line 12429
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->quantity:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->quantity:Ljava/lang/String;

    .line 12430
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->quantity_unit:Lcom/squareup/orders/model/Order$QuantityUnit;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->quantity_unit:Lcom/squareup/orders/model/Order$QuantityUnit;

    .line 12431
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->note:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->note:Ljava/lang/String;

    .line 12432
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->catalog_object_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->catalog_object_id:Ljava/lang/String;

    .line 12433
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->catalog_object_version:Ljava/lang/Long;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->catalog_object_version:Ljava/lang/Long;

    .line 12434
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->variation_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->variation_name:Ljava/lang/String;

    .line 12435
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->catalog_item_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->catalog_item_id:Ljava/lang/String;

    .line 12436
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->catalog_category_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->catalog_category_id:Ljava/lang/String;

    .line 12437
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->item_type:Lcom/squareup/orders/model/Order$LineItem$ItemType;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->item_type:Lcom/squareup/orders/model/Order$LineItem$ItemType;

    .line 12438
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->sku:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->sku:Ljava/lang/String;

    .line 12439
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->category_name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->category_name:Ljava/lang/String;

    .line 12440
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->return_modifiers:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->return_modifiers:Ljava/util/List;

    .line 12441
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->return_taxes:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->return_taxes:Ljava/util/List;

    .line 12442
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->return_discounts:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->return_discounts:Ljava/util/List;

    .line 12443
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->applied_taxes:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->applied_taxes:Ljava/util/List;

    .line 12444
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->applied_discounts:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->applied_discounts:Ljava/util/List;

    .line 12445
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 12446
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->variation_total_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->variation_total_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 12447
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->gross_return_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->gross_return_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 12448
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 12449
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->total_discount_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->total_discount_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 12450
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    iput-object v1, v0, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    .line 12451
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$ReturnLineItem;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 12042
    invoke-virtual {p0}, Lcom/squareup/orders/model/Order$ReturnLineItem;->newBuilder()Lcom/squareup/orders/model/Order$ReturnLineItem$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 12525
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 12526
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->uid:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->uid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 12527
    :cond_0
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->source_line_item_uid:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", source_line_item_uid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->source_line_item_uid:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 12528
    :cond_1
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->name:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 12529
    :cond_2
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->quantity:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", quantity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->quantity:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 12530
    :cond_3
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->quantity_unit:Lcom/squareup/orders/model/Order$QuantityUnit;

    if-eqz v1, :cond_4

    const-string v1, ", quantity_unit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->quantity_unit:Lcom/squareup/orders/model/Order$QuantityUnit;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 12531
    :cond_4
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->note:Ljava/lang/String;

    if-eqz v1, :cond_5

    const-string v1, ", note="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->note:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 12532
    :cond_5
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->catalog_object_id:Ljava/lang/String;

    if-eqz v1, :cond_6

    const-string v1, ", catalog_object_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->catalog_object_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 12533
    :cond_6
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->catalog_object_version:Ljava/lang/Long;

    if-eqz v1, :cond_7

    const-string v1, ", catalog_object_version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->catalog_object_version:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 12534
    :cond_7
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->variation_name:Ljava/lang/String;

    if-eqz v1, :cond_8

    const-string v1, ", variation_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->variation_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 12535
    :cond_8
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->catalog_item_id:Ljava/lang/String;

    if-eqz v1, :cond_9

    const-string v1, ", catalog_item_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->catalog_item_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 12536
    :cond_9
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->catalog_category_id:Ljava/lang/String;

    if-eqz v1, :cond_a

    const-string v1, ", catalog_category_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->catalog_category_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 12537
    :cond_a
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->item_type:Lcom/squareup/orders/model/Order$LineItem$ItemType;

    if-eqz v1, :cond_b

    const-string v1, ", item_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->item_type:Lcom/squareup/orders/model/Order$LineItem$ItemType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 12538
    :cond_b
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->sku:Ljava/lang/String;

    if-eqz v1, :cond_c

    const-string v1, ", sku="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->sku:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 12539
    :cond_c
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->category_name:Ljava/lang/String;

    if-eqz v1, :cond_d

    const-string v1, ", category_name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->category_name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 12540
    :cond_d
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->return_modifiers:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_e

    const-string v1, ", return_modifiers="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->return_modifiers:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 12541
    :cond_e
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->return_taxes:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_f

    const-string v1, ", return_taxes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->return_taxes:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 12542
    :cond_f
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->return_discounts:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_10

    const-string v1, ", return_discounts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->return_discounts:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 12543
    :cond_10
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->applied_taxes:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_11

    const-string v1, ", applied_taxes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->applied_taxes:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 12544
    :cond_11
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->applied_discounts:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_12

    const-string v1, ", applied_discounts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->applied_discounts:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 12545
    :cond_12
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_13

    const-string v1, ", base_price_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->base_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 12546
    :cond_13
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->variation_total_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_14

    const-string v1, ", variation_total_price_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->variation_total_price_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 12547
    :cond_14
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->gross_return_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_15

    const-string v1, ", gross_return_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->gross_return_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 12548
    :cond_15
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_16

    const-string v1, ", total_tax_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->total_tax_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 12549
    :cond_16
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->total_discount_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_17

    const-string v1, ", total_discount_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->total_discount_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 12550
    :cond_17
    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    if-eqz v1, :cond_18

    const-string v1, ", total_money="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/model/Order$ReturnLineItem;->total_money:Lcom/squareup/protos/connect/v2/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_18
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ReturnLineItem{"

    .line 12551
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
