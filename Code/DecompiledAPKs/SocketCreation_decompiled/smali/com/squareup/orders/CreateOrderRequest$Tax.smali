.class public final Lcom/squareup/orders/CreateOrderRequest$Tax;
.super Lcom/squareup/wire/Message;
.source "CreateOrderRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/orders/CreateOrderRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Tax"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/orders/CreateOrderRequest$Tax$ProtoAdapter_Tax;,
        Lcom/squareup/orders/CreateOrderRequest$Tax$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lcom/squareup/orders/CreateOrderRequest$Tax;",
        "Lcom/squareup/orders/CreateOrderRequest$Tax$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/orders/CreateOrderRequest$Tax;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_CATALOG_OBJECT_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_PERCENTAGE:Ljava/lang/String; = ""

.field public static final DEFAULT_TYPE:Lcom/squareup/orders/model/Order$LineItem$Tax$Type;

.field private static final serialVersionUID:J


# instance fields
.field public final catalog_object_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final percentage:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final type:Lcom/squareup/orders/model/Order$LineItem$Tax$Type;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.orders.model.Order$LineItem$Tax$Type#ADAPTER"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 847
    new-instance v0, Lcom/squareup/orders/CreateOrderRequest$Tax$ProtoAdapter_Tax;

    invoke-direct {v0}, Lcom/squareup/orders/CreateOrderRequest$Tax$ProtoAdapter_Tax;-><init>()V

    sput-object v0, Lcom/squareup/orders/CreateOrderRequest$Tax;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 855
    sget-object v0, Lcom/squareup/orders/model/Order$LineItem$Tax$Type;->ADDITIVE:Lcom/squareup/orders/model/Order$LineItem$Tax$Type;

    sput-object v0, Lcom/squareup/orders/CreateOrderRequest$Tax;->DEFAULT_TYPE:Lcom/squareup/orders/model/Order$LineItem$Tax$Type;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/orders/model/Order$LineItem$Tax$Type;Ljava/lang/String;)V
    .locals 6

    .line 913
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/orders/CreateOrderRequest$Tax;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/orders/model/Order$LineItem$Tax$Type;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/squareup/orders/model/Order$LineItem$Tax$Type;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1

    .line 918
    sget-object v0, Lcom/squareup/orders/CreateOrderRequest$Tax;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 919
    iput-object p1, p0, Lcom/squareup/orders/CreateOrderRequest$Tax;->catalog_object_id:Ljava/lang/String;

    .line 920
    iput-object p2, p0, Lcom/squareup/orders/CreateOrderRequest$Tax;->name:Ljava/lang/String;

    .line 921
    iput-object p3, p0, Lcom/squareup/orders/CreateOrderRequest$Tax;->type:Lcom/squareup/orders/model/Order$LineItem$Tax$Type;

    .line 922
    iput-object p4, p0, Lcom/squareup/orders/CreateOrderRequest$Tax;->percentage:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 939
    :cond_0
    instance-of v1, p1, Lcom/squareup/orders/CreateOrderRequest$Tax;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 940
    :cond_1
    check-cast p1, Lcom/squareup/orders/CreateOrderRequest$Tax;

    .line 941
    invoke-virtual {p0}, Lcom/squareup/orders/CreateOrderRequest$Tax;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/orders/CreateOrderRequest$Tax;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$Tax;->catalog_object_id:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/CreateOrderRequest$Tax;->catalog_object_id:Ljava/lang/String;

    .line 942
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$Tax;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/orders/CreateOrderRequest$Tax;->name:Ljava/lang/String;

    .line 943
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$Tax;->type:Lcom/squareup/orders/model/Order$LineItem$Tax$Type;

    iget-object v3, p1, Lcom/squareup/orders/CreateOrderRequest$Tax;->type:Lcom/squareup/orders/model/Order$LineItem$Tax$Type;

    .line 944
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$Tax;->percentage:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/orders/CreateOrderRequest$Tax;->percentage:Ljava/lang/String;

    .line 945
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 950
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 952
    invoke-virtual {p0}, Lcom/squareup/orders/CreateOrderRequest$Tax;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 953
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$Tax;->catalog_object_id:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 954
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$Tax;->name:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 955
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$Tax;->type:Lcom/squareup/orders/model/Order$LineItem$Tax$Type;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/squareup/orders/model/Order$LineItem$Tax$Type;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 956
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$Tax;->percentage:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 957
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/orders/CreateOrderRequest$Tax$Builder;
    .locals 2

    .line 927
    new-instance v0, Lcom/squareup/orders/CreateOrderRequest$Tax$Builder;

    invoke-direct {v0}, Lcom/squareup/orders/CreateOrderRequest$Tax$Builder;-><init>()V

    .line 928
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$Tax;->catalog_object_id:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/CreateOrderRequest$Tax$Builder;->catalog_object_id:Ljava/lang/String;

    .line 929
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$Tax;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/CreateOrderRequest$Tax$Builder;->name:Ljava/lang/String;

    .line 930
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$Tax;->type:Lcom/squareup/orders/model/Order$LineItem$Tax$Type;

    iput-object v1, v0, Lcom/squareup/orders/CreateOrderRequest$Tax$Builder;->type:Lcom/squareup/orders/model/Order$LineItem$Tax$Type;

    .line 931
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$Tax;->percentage:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/orders/CreateOrderRequest$Tax$Builder;->percentage:Ljava/lang/String;

    .line 932
    invoke-virtual {p0}, Lcom/squareup/orders/CreateOrderRequest$Tax;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/orders/CreateOrderRequest$Tax$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 846
    invoke-virtual {p0}, Lcom/squareup/orders/CreateOrderRequest$Tax;->newBuilder()Lcom/squareup/orders/CreateOrderRequest$Tax$Builder;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 964
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 965
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$Tax;->catalog_object_id:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", catalog_object_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$Tax;->catalog_object_id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 966
    :cond_0
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$Tax;->name:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$Tax;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 967
    :cond_1
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$Tax;->type:Lcom/squareup/orders/model/Order$LineItem$Tax$Type;

    if-eqz v1, :cond_2

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$Tax;->type:Lcom/squareup/orders/model/Order$LineItem$Tax$Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 968
    :cond_2
    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$Tax;->percentage:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", percentage="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/orders/CreateOrderRequest$Tax;->percentage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Tax{"

    .line 969
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
