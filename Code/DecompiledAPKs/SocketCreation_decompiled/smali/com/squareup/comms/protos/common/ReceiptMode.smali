.class public final enum Lcom/squareup/comms/protos/common/ReceiptMode;
.super Ljava/lang/Enum;
.source "ReceiptMode.kt"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/comms/protos/common/ReceiptMode$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/comms/protos/common/ReceiptMode;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u000b\u0008\u0086\u0001\u0018\u0000 \u000e2\u0008\u0012\u0004\u0012\u00020\u00000\u00012\u00020\u0002:\u0001\u000eB\u000f\u0008\u0002\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005R\u0014\u0010\u0003\u001a\u00020\u0004X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007j\u0002\u0008\u0008j\u0002\u0008\tj\u0002\u0008\nj\u0002\u0008\u000bj\u0002\u0008\u000cj\u0002\u0008\r\u00a8\u0006\u000f"
    }
    d2 = {
        "Lcom/squareup/comms/protos/common/ReceiptMode;",
        "",
        "Lcom/squareup/wire/WireEnum;",
        "value",
        "",
        "(Ljava/lang/String;II)V",
        "getValue",
        "()I",
        "UNKNOWN",
        "EMAIL",
        "TEXT",
        "PRINT",
        "PRINT_AND_SIGN",
        "NONE",
        "Companion",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/comms/protos/common/ReceiptMode;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/comms/protos/common/ReceiptMode;",
            ">;"
        }
    .end annotation
.end field

.field public static final Companion:Lcom/squareup/comms/protos/common/ReceiptMode$Companion;

.field public static final enum EMAIL:Lcom/squareup/comms/protos/common/ReceiptMode;

.field public static final enum NONE:Lcom/squareup/comms/protos/common/ReceiptMode;

.field public static final enum PRINT:Lcom/squareup/comms/protos/common/ReceiptMode;

.field public static final enum PRINT_AND_SIGN:Lcom/squareup/comms/protos/common/ReceiptMode;

.field public static final enum TEXT:Lcom/squareup/comms/protos/common/ReceiptMode;

.field public static final enum UNKNOWN:Lcom/squareup/comms/protos/common/ReceiptMode;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/squareup/comms/protos/common/ReceiptMode;

    new-instance v1, Lcom/squareup/comms/protos/common/ReceiptMode;

    const/4 v2, 0x0

    const-string v3, "UNKNOWN"

    .line 15
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/common/ReceiptMode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/common/ReceiptMode;->UNKNOWN:Lcom/squareup/comms/protos/common/ReceiptMode;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/common/ReceiptMode;

    const/4 v2, 0x1

    const-string v3, "EMAIL"

    .line 17
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/common/ReceiptMode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/common/ReceiptMode;->EMAIL:Lcom/squareup/comms/protos/common/ReceiptMode;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/common/ReceiptMode;

    const/4 v2, 0x2

    const-string v3, "TEXT"

    .line 19
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/common/ReceiptMode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/common/ReceiptMode;->TEXT:Lcom/squareup/comms/protos/common/ReceiptMode;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/common/ReceiptMode;

    const/4 v2, 0x3

    const-string v3, "PRINT"

    .line 21
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/common/ReceiptMode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/common/ReceiptMode;->PRINT:Lcom/squareup/comms/protos/common/ReceiptMode;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/common/ReceiptMode;

    const/4 v2, 0x4

    const-string v3, "PRINT_AND_SIGN"

    .line 23
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/common/ReceiptMode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/common/ReceiptMode;->PRINT_AND_SIGN:Lcom/squareup/comms/protos/common/ReceiptMode;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/comms/protos/common/ReceiptMode;

    const/4 v2, 0x5

    const-string v3, "NONE"

    .line 25
    invoke-direct {v1, v3, v2, v2}, Lcom/squareup/comms/protos/common/ReceiptMode;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/squareup/comms/protos/common/ReceiptMode;->NONE:Lcom/squareup/comms/protos/common/ReceiptMode;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/comms/protos/common/ReceiptMode;->$VALUES:[Lcom/squareup/comms/protos/common/ReceiptMode;

    new-instance v0, Lcom/squareup/comms/protos/common/ReceiptMode$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/comms/protos/common/ReceiptMode$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/comms/protos/common/ReceiptMode;->Companion:Lcom/squareup/comms/protos/common/ReceiptMode$Companion;

    .line 29
    new-instance v0, Lcom/squareup/comms/protos/common/ReceiptMode$Companion$ADAPTER$1;

    .line 30
    const-class v1, Lcom/squareup/comms/protos/common/ReceiptMode;

    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->getOrCreateKotlinClass(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/comms/protos/common/ReceiptMode$Companion$ADAPTER$1;-><init>(Lkotlin/reflect/KClass;)V

    check-cast v0, Lcom/squareup/wire/ProtoAdapter;

    sput-object v0, Lcom/squareup/comms/protos/common/ReceiptMode;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 12
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/squareup/comms/protos/common/ReceiptMode;->value:I

    return-void
.end method

.method public static final fromValue(I)Lcom/squareup/comms/protos/common/ReceiptMode;
    .locals 1
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    sget-object v0, Lcom/squareup/comms/protos/common/ReceiptMode;->Companion:Lcom/squareup/comms/protos/common/ReceiptMode$Companion;

    invoke-virtual {v0, p0}, Lcom/squareup/comms/protos/common/ReceiptMode$Companion;->fromValue(I)Lcom/squareup/comms/protos/common/ReceiptMode;

    move-result-object p0

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/comms/protos/common/ReceiptMode;
    .locals 1

    const-class v0, Lcom/squareup/comms/protos/common/ReceiptMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/comms/protos/common/ReceiptMode;

    return-object p0
.end method

.method public static values()[Lcom/squareup/comms/protos/common/ReceiptMode;
    .locals 1

    sget-object v0, Lcom/squareup/comms/protos/common/ReceiptMode;->$VALUES:[Lcom/squareup/comms/protos/common/ReceiptMode;

    invoke-virtual {v0}, [Lcom/squareup/comms/protos/common/ReceiptMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/comms/protos/common/ReceiptMode;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 13
    iget v0, p0, Lcom/squareup/comms/protos/common/ReceiptMode;->value:I

    return v0
.end method
