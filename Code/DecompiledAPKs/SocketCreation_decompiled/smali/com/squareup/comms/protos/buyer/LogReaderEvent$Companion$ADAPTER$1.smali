.class public final Lcom/squareup/comms/protos/buyer/LogReaderEvent$Companion$ADAPTER$1;
.super Lcom/squareup/wire/ProtoAdapter;
.source "LogReaderEvent.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/buyer/LogReaderEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/comms/protos/buyer/LogReaderEvent;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nLogReaderEvent.kt\nKotlin\n*S Kotlin\n*F\n+ 1 LogReaderEvent.kt\ncom/squareup/comms/protos/buyer/LogReaderEvent$Companion$ADAPTER$1\n+ 2 ProtoReader.kt\ncom/squareup/wire/ProtoReader\n*L\n1#1,148:1\n415#2,7:149\n*E\n*S KotlinDebug\n*F\n+ 1 LogReaderEvent.kt\ncom/squareup/comms/protos/buyer/LogReaderEvent$Companion$ADAPTER$1\n*L\n123#1,7:149\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000-\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0004\u001a\u00020\u0005H\u0016J\u0018\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0002H\u0016J\u0010\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\n\u001a\u00020\u0002H\u0016J\u0010\u0010\r\u001a\u00020\u00022\u0006\u0010\n\u001a\u00020\u0002H\u0016\u00a8\u0006\u000e"
    }
    d2 = {
        "com/squareup/comms/protos/buyer/LogReaderEvent$Companion$ADAPTER$1",
        "Lcom/squareup/wire/ProtoAdapter;",
        "Lcom/squareup/comms/protos/buyer/LogReaderEvent;",
        "decode",
        "reader",
        "Lcom/squareup/wire/ProtoReader;",
        "encode",
        "",
        "writer",
        "Lcom/squareup/wire/ProtoWriter;",
        "value",
        "encodedSize",
        "",
        "redact",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method constructor <init>(Lcom/squareup/wire/FieldEncoding;Lkotlin/reflect/KClass;)V
    .locals 0

    .line 105
    invoke-direct {p0, p1, p2}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Lkotlin/reflect/KClass;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/comms/protos/buyer/LogReaderEvent;
    .locals 8

    const-string v0, "reader"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v0, 0x0

    .line 121
    move-object v1, v0

    check-cast v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;

    .line 122
    check-cast v0, Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;

    .line 149
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v2

    .line 151
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v4

    const/4 v5, -0x1

    const/4 v6, 0x2

    const/4 v7, 0x1

    if-ne v4, v5, :cond_2

    .line 155
    invoke-virtual {p1, v2, v3}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    .line 130
    new-instance v2, Lcom/squareup/comms/protos/buyer/LogReaderEvent;

    const/4 v3, 0x0

    if-eqz v1, :cond_1

    if-eqz v0, :cond_0

    invoke-direct {v2, v1, v0, p1}, Lcom/squareup/comms/protos/buyer/LogReaderEvent;-><init>(Lcom/squareup/comms/protos/buyer/BuyerEventName;Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;Lokio/ByteString;)V

    return-object v2

    :cond_0
    new-array p1, v6, [Ljava/lang/Object;

    aput-object v0, p1, v3

    const-string v0, "card_reader_info"

    aput-object v0, p1, v7

    .line 132
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object p1

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :cond_1
    new-array p1, v6, [Ljava/lang/Object;

    aput-object v1, p1, v3

    const-string v0, "event_name"

    aput-object v0, p1, v7

    .line 131
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object p1

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    :cond_2
    if-eq v4, v7, :cond_4

    if-eq v4, v6, :cond_3

    .line 127
    invoke-virtual {p1, v4}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 126
    :cond_3
    sget-object v0, Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;

    goto :goto_0

    .line 125
    :cond_4
    sget-object v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v1, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/comms/protos/buyer/BuyerEventName;

    goto :goto_0
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0

    .line 105
    invoke-virtual {p0, p1}, Lcom/squareup/comms/protos/buyer/LogReaderEvent$Companion$ADAPTER$1;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/comms/protos/buyer/LogReaderEvent;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/comms/protos/buyer/LogReaderEvent;)V
    .locals 3

    const-string v0, "writer"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "value"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 115
    sget-object v0, Lcom/squareup/comms/protos/buyer/BuyerEventName;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/comms/protos/buyer/LogReaderEvent;->event_name:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 116
    sget-object v0, Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/comms/protos/buyer/LogReaderEvent;->card_reader_info:Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 117
    invoke-virtual {p2}, Lcom/squareup/comms/protos/buyer/LogReaderEvent;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0

    .line 105
    check-cast p2, Lcom/squareup/comms/protos/buyer/LogReaderEvent;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/comms/protos/buyer/LogReaderEvent$Companion$ADAPTER$1;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/comms/protos/buyer/LogReaderEvent;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/comms/protos/buyer/LogReaderEvent;)I
    .locals 4

    const-string v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 110
    sget-object v0, Lcom/squareup/comms/protos/buyer/BuyerEventName;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/comms/protos/buyer/LogReaderEvent;->event_name:Lcom/squareup/comms/protos/buyer/BuyerEventName;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    .line 111
    sget-object v1, Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/comms/protos/buyer/LogReaderEvent;->card_reader_info:Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;

    const/4 v3, 0x2

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 112
    invoke-virtual {p1}, Lcom/squareup/comms/protos/buyer/LogReaderEvent;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 105
    check-cast p1, Lcom/squareup/comms/protos/buyer/LogReaderEvent;

    invoke-virtual {p0, p1}, Lcom/squareup/comms/protos/buyer/LogReaderEvent$Companion$ADAPTER$1;->encodedSize(Lcom/squareup/comms/protos/buyer/LogReaderEvent;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/comms/protos/buyer/LogReaderEvent;)Lcom/squareup/comms/protos/buyer/LogReaderEvent;
    .locals 7

    const-string v0, "value"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 139
    sget-object v0, Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/comms/protos/buyer/LogReaderEvent;->card_reader_info:Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->redact(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;

    .line 140
    sget-object v4, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    const/4 v2, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object v1, p1

    .line 138
    invoke-static/range {v1 .. v6}, Lcom/squareup/comms/protos/buyer/LogReaderEvent;->copy$default(Lcom/squareup/comms/protos/buyer/LogReaderEvent;Lcom/squareup/comms/protos/buyer/BuyerEventName;Lcom/squareup/comms/protos/buyer/BuyerCardReaderInfo;Lokio/ByteString;ILjava/lang/Object;)Lcom/squareup/comms/protos/buyer/LogReaderEvent;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 105
    check-cast p1, Lcom/squareup/comms/protos/buyer/LogReaderEvent;

    invoke-virtual {p0, p1}, Lcom/squareup/comms/protos/buyer/LogReaderEvent$Companion$ADAPTER$1;->redact(Lcom/squareup/comms/protos/buyer/LogReaderEvent;)Lcom/squareup/comms/protos/buyer/LogReaderEvent;

    move-result-object p1

    return-object p1
.end method
