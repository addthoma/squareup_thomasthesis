.class public final Lcom/squareup/comms/protos/buyer/CapturePaymentForSleep$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CapturePaymentForSleep.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/buyer/CapturePaymentForSleep;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/comms/protos/buyer/CapturePaymentForSleep;",
        "Lcom/squareup/comms/protos/buyer/CapturePaymentForSleep$Builder;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00000\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0008\u0010\u0006\u001a\u00020\u0002H\u0016J\u0010\u0010\u0004\u001a\u00020\u00002\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005R\u0014\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/comms/protos/buyer/CapturePaymentForSleep$Builder;",
        "Lcom/squareup/wire/Message$Builder;",
        "Lcom/squareup/comms/protos/buyer/CapturePaymentForSleep;",
        "()V",
        "capture_args",
        "Lcom/squareup/comms/protos/buyer/EmvCaptureArgs;",
        "build",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public capture_args:Lcom/squareup/comms/protos/buyer/EmvCaptureArgs;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 70
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/comms/protos/buyer/CapturePaymentForSleep;
    .locals 3

    .line 82
    new-instance v0, Lcom/squareup/comms/protos/buyer/CapturePaymentForSleep;

    .line 83
    iget-object v1, p0, Lcom/squareup/comms/protos/buyer/CapturePaymentForSleep$Builder;->capture_args:Lcom/squareup/comms/protos/buyer/EmvCaptureArgs;

    .line 84
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/CapturePaymentForSleep$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v2

    .line 82
    invoke-direct {v0, v1, v2}, Lcom/squareup/comms/protos/buyer/CapturePaymentForSleep;-><init>(Lcom/squareup/comms/protos/buyer/EmvCaptureArgs;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 70
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/CapturePaymentForSleep$Builder;->build()Lcom/squareup/comms/protos/buyer/CapturePaymentForSleep;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    return-object v0
.end method

.method public final capture_args(Lcom/squareup/comms/protos/buyer/EmvCaptureArgs;)Lcom/squareup/comms/protos/buyer/CapturePaymentForSleep$Builder;
    .locals 0

    .line 78
    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/CapturePaymentForSleep$Builder;->capture_args:Lcom/squareup/comms/protos/buyer/EmvCaptureArgs;

    return-object p0
.end method
