.class public final Lcom/squareup/comms/protos/buyer/BuyerDevice$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "BuyerDevice.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/buyer/BuyerDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/comms/protos/buyer/BuyerDevice;",
        "Lcom/squareup/comms/protos/buyer/BuyerDevice$Builder;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0002\u0008\u0005\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00000\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u000e\u0010\u0004\u001a\u00020\u00002\u0006\u0010\u0004\u001a\u00020\u0005J\u0010\u0010\u0007\u001a\u00020\u00002\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u0008J\u0010\u0010\t\u001a\u00020\u00002\u0008\u0010\t\u001a\u0004\u0018\u00010\u0008J\u0010\u0010\n\u001a\u00020\u00002\u0008\u0010\n\u001a\u0004\u0018\u00010\u0008J\u0008\u0010\u000c\u001a\u00020\u0002H\u0016J\u000e\u0010\u000b\u001a\u00020\u00002\u0006\u0010\u000b\u001a\u00020\u0005R\u0016\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0006R\u0014\u0010\u0007\u001a\u0004\u0018\u00010\u00088\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u0004\u0018\u00010\u00088\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\u0004\u0018\u00010\u00088\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000b\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0006\u00a8\u0006\r"
    }
    d2 = {
        "Lcom/squareup/comms/protos/buyer/BuyerDevice$Builder;",
        "Lcom/squareup/wire/Message$Builder;",
        "Lcom/squareup/comms/protos/buyer/BuyerDevice;",
        "()V",
        "bran_docked",
        "",
        "Ljava/lang/Boolean;",
        "bran_hwid",
        "",
        "bran_serial_number",
        "bran_squid_version",
        "compatible",
        "build",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public bran_docked:Ljava/lang/Boolean;

.field public bran_hwid:Ljava/lang/String;

.field public bran_serial_number:Ljava/lang/String;

.field public bran_squid_version:Ljava/lang/String;

.field public compatible:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 119
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public final bran_docked(Z)Lcom/squareup/comms/protos/buyer/BuyerDevice$Builder;
    .locals 0

    .line 159
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/BuyerDevice$Builder;->bran_docked:Ljava/lang/Boolean;

    return-object p0
.end method

.method public final bran_hwid(Ljava/lang/String;)Lcom/squareup/comms/protos/buyer/BuyerDevice$Builder;
    .locals 0

    .line 149
    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/BuyerDevice$Builder;->bran_hwid:Ljava/lang/String;

    return-object p0
.end method

.method public final bran_serial_number(Ljava/lang/String;)Lcom/squareup/comms/protos/buyer/BuyerDevice$Builder;
    .locals 0

    .line 144
    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/BuyerDevice$Builder;->bran_serial_number:Ljava/lang/String;

    return-object p0
.end method

.method public final bran_squid_version(Ljava/lang/String;)Lcom/squareup/comms/protos/buyer/BuyerDevice$Builder;
    .locals 0

    .line 154
    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/BuyerDevice$Builder;->bran_squid_version:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/comms/protos/buyer/BuyerDevice;
    .locals 11

    .line 163
    new-instance v7, Lcom/squareup/comms/protos/buyer/BuyerDevice;

    .line 164
    iget-object v0, p0, Lcom/squareup/comms/protos/buyer/BuyerDevice$Builder;->compatible:Ljava/lang/Boolean;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x2

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    .line 165
    iget-object v5, p0, Lcom/squareup/comms/protos/buyer/BuyerDevice$Builder;->bran_serial_number:Ljava/lang/String;

    .line 166
    iget-object v6, p0, Lcom/squareup/comms/protos/buyer/BuyerDevice$Builder;->bran_hwid:Ljava/lang/String;

    .line 167
    iget-object v8, p0, Lcom/squareup/comms/protos/buyer/BuyerDevice$Builder;->bran_squid_version:Ljava/lang/String;

    .line 168
    iget-object v0, p0, Lcom/squareup/comms/protos/buyer/BuyerDevice$Builder;->bran_docked:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    .line 169
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/BuyerDevice$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v10

    move-object v0, v7

    move v1, v4

    move-object v2, v5

    move-object v3, v6

    move-object v4, v8

    move v5, v9

    move-object v6, v10

    .line 163
    invoke-direct/range {v0 .. v6}, Lcom/squareup/comms/protos/buyer/BuyerDevice;-><init>(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLokio/ByteString;)V

    return-object v7

    :cond_0
    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v2

    const-string v0, "bran_docked"

    aput-object v0, v3, v1

    .line 168
    invoke-static {v3}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_1
    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v2

    const-string v0, "compatible"

    aput-object v0, v3, v1

    .line 164
    invoke-static {v3}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 119
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/BuyerDevice$Builder;->build()Lcom/squareup/comms/protos/buyer/BuyerDevice;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    return-object v0
.end method

.method public final compatible(Z)Lcom/squareup/comms/protos/buyer/BuyerDevice$Builder;
    .locals 0

    .line 136
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/BuyerDevice$Builder;->compatible:Ljava/lang/Boolean;

    return-object p0
.end method
