.class public final Lcom/squareup/comms/protos/buyer/OnTender$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "OnTender.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/comms/protos/buyer/OnTender;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/comms/protos/buyer/OnTender;",
        "Lcom/squareup/comms/protos/buyer/OnTender$Builder;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\t\n\u0002\u0008\u0005\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00000\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0008\u0010\u000f\u001a\u00020\u0002H\u0016J\u000e\u0010\u0004\u001a\u00020\u00002\u0006\u0010\u0004\u001a\u00020\u0005J\u0010\u0010\u0007\u001a\u00020\u00002\u0008\u0010\u0007\u001a\u0004\u0018\u00010\u0008J\u000e\u0010\t\u001a\u00020\u00002\u0006\u0010\t\u001a\u00020\u0005J\u000e\u0010\n\u001a\u00020\u00002\u0006\u0010\n\u001a\u00020\u000bJ\u000e\u0010\r\u001a\u00020\u00002\u0006\u0010\r\u001a\u00020\u0005J\u000e\u0010\u000e\u001a\u00020\u00002\u0006\u0010\u000e\u001a\u00020\u0005R\u0016\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0006R\u0014\u0010\u0007\u001a\u0004\u0018\u00010\u00088\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\t\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0006R\u0016\u0010\n\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u000cR\u0016\u0010\r\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0006R\u0016\u0010\u000e\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0006\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/comms/protos/buyer/OnTender$Builder;",
        "Lcom/squareup/wire/Message$Builder;",
        "Lcom/squareup/comms/protos/buyer/OnTender;",
        "()V",
        "is_card_present",
        "",
        "Ljava/lang/Boolean;",
        "magswipe",
        "Lcom/squareup/comms/protos/buyer/MagSwipe;",
        "prompt_above_credit_card_maximum",
        "prompt_amount",
        "",
        "Ljava/lang/Long;",
        "prompt_below_credit_card_minimum",
        "prompt_offline_mode",
        "build",
        "x2comms_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field public is_card_present:Ljava/lang/Boolean;

.field public magswipe:Lcom/squareup/comms/protos/buyer/MagSwipe;

.field public prompt_above_credit_card_maximum:Ljava/lang/Boolean;

.field public prompt_amount:Ljava/lang/Long;

.field public prompt_below_credit_card_minimum:Ljava/lang/Boolean;

.field public prompt_offline_mode:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 131
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/comms/protos/buyer/OnTender;
    .locals 13

    .line 180
    new-instance v9, Lcom/squareup/comms/protos/buyer/OnTender;

    .line 181
    iget-object v0, p0, Lcom/squareup/comms/protos/buyer/OnTender$Builder;->is_card_present:Ljava/lang/Boolean;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x2

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    .line 183
    iget-object v0, p0, Lcom/squareup/comms/protos/buyer/OnTender$Builder;->prompt_amount:Ljava/lang/Long;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    .line 184
    iget-object v0, p0, Lcom/squareup/comms/protos/buyer/OnTender$Builder;->prompt_offline_mode:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    .line 186
    iget-object v0, p0, Lcom/squareup/comms/protos/buyer/OnTender$Builder;->prompt_above_credit_card_maximum:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    .line 189
    iget-object v0, p0, Lcom/squareup/comms/protos/buyer/OnTender$Builder;->prompt_below_credit_card_minimum:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v10

    .line 192
    iget-object v11, p0, Lcom/squareup/comms/protos/buyer/OnTender$Builder;->magswipe:Lcom/squareup/comms/protos/buyer/MagSwipe;

    .line 193
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/OnTender$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v12

    move-object v0, v9

    move v1, v4

    move-wide v2, v5

    move v4, v7

    move v5, v8

    move v6, v10

    move-object v7, v11

    move-object v8, v12

    .line 180
    invoke-direct/range {v0 .. v8}, Lcom/squareup/comms/protos/buyer/OnTender;-><init>(ZJZZZLcom/squareup/comms/protos/buyer/MagSwipe;Lokio/ByteString;)V

    return-object v9

    :cond_0
    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v2

    const-string v0, "prompt_below_credit_card_minimum"

    aput-object v0, v3, v1

    .line 190
    invoke-static {v3}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_1
    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v2

    const-string v0, "prompt_above_credit_card_maximum"

    aput-object v0, v3, v1

    .line 187
    invoke-static {v3}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_2
    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v2

    const-string v0, "prompt_offline_mode"

    aput-object v0, v3, v1

    .line 184
    invoke-static {v3}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_3
    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v2

    const-string v0, "prompt_amount"

    aput-object v0, v3, v1

    .line 183
    invoke-static {v3}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_4
    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v2

    const-string v0, "is_card_present"

    aput-object v0, v3, v1

    .line 181
    invoke-static {v3}, Lcom/squareup/wire/internal/Internal;->missingRequiredFields([Ljava/lang/Object;)Ljava/lang/IllegalStateException;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 131
    invoke-virtual {p0}, Lcom/squareup/comms/protos/buyer/OnTender$Builder;->build()Lcom/squareup/comms/protos/buyer/OnTender;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;

    return-object v0
.end method

.method public final is_card_present(Z)Lcom/squareup/comms/protos/buyer/OnTender$Builder;
    .locals 0

    .line 151
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/OnTender$Builder;->is_card_present:Ljava/lang/Boolean;

    return-object p0
.end method

.method public final magswipe(Lcom/squareup/comms/protos/buyer/MagSwipe;)Lcom/squareup/comms/protos/buyer/OnTender$Builder;
    .locals 0

    .line 176
    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/OnTender$Builder;->magswipe:Lcom/squareup/comms/protos/buyer/MagSwipe;

    return-object p0
.end method

.method public final prompt_above_credit_card_maximum(Z)Lcom/squareup/comms/protos/buyer/OnTender$Builder;
    .locals 0

    .line 166
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/OnTender$Builder;->prompt_above_credit_card_maximum:Ljava/lang/Boolean;

    return-object p0
.end method

.method public final prompt_amount(J)Lcom/squareup/comms/protos/buyer/OnTender$Builder;
    .locals 0

    .line 156
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/OnTender$Builder;->prompt_amount:Ljava/lang/Long;

    return-object p0
.end method

.method public final prompt_below_credit_card_minimum(Z)Lcom/squareup/comms/protos/buyer/OnTender$Builder;
    .locals 0

    .line 171
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/OnTender$Builder;->prompt_below_credit_card_minimum:Ljava/lang/Boolean;

    return-object p0
.end method

.method public final prompt_offline_mode(Z)Lcom/squareup/comms/protos/buyer/OnTender$Builder;
    .locals 0

    .line 161
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/comms/protos/buyer/OnTender$Builder;->prompt_offline_mode:Ljava/lang/Boolean;

    return-object p0
.end method
