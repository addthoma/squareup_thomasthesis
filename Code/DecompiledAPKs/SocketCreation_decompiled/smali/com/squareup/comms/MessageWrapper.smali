.class public final Lcom/squareup/comms/MessageWrapper;
.super Ljava/lang/Object;
.source "MessageWrapper.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static unwrap(Lcom/squareup/comms/protos/wrapper/Envelope;)Lcom/squareup/wire/Message;
    .locals 3

    .line 36
    :try_start_0
    iget-object v0, p0, Lcom/squareup/comms/protos/wrapper/Envelope;->message_name:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 37
    const-class v1, Lcom/squareup/wire/Message;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v1, :cond_0

    .line 46
    :try_start_1
    invoke-static {v0}, Lcom/squareup/wire/ProtoAdapter;->get(Ljava/lang/Class;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/comms/protos/wrapper/Envelope;->message_body:Lokio/ByteString;

    invoke-virtual {v1}, Lokio/ByteString;->toByteArray()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/wire/ProtoAdapter;->decode([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/wire/Message;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    return-object v0

    .line 48
    :catch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid message body: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 38
    :cond_0
    :try_start_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Type not Message: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catch Ljava/lang/ClassNotFoundException; {:try_start_2 .. :try_end_2} :catch_1

    .line 42
    :catch_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Type not found: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static wrap(Lcom/squareup/wire/Message;)Lcom/squareup/comms/protos/wrapper/Envelope;
    .locals 1

    const-string v0, ""

    .line 20
    invoke-static {p0, v0}, Lcom/squareup/comms/MessageWrapper;->wrap(Lcom/squareup/wire/Message;Ljava/lang/String;)Lcom/squareup/comms/protos/wrapper/Envelope;

    move-result-object p0

    return-object p0
.end method

.method public static wrap(Lcom/squareup/wire/Message;Ljava/lang/String;)Lcom/squareup/comms/protos/wrapper/Envelope;
    .locals 2

    .line 24
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    .line 25
    invoke-static {p0}, Lcom/squareup/wire/ProtoAdapter;->get(Lcom/squareup/wire/Message;)Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/squareup/wire/ProtoAdapter;->encode(Ljava/lang/Object;)[B

    move-result-object p0

    .line 27
    new-instance v1, Lcom/squareup/comms/protos/wrapper/Envelope$Builder;

    invoke-direct {v1}, Lcom/squareup/comms/protos/wrapper/Envelope$Builder;-><init>()V

    invoke-virtual {v1, v0}, Lcom/squareup/comms/protos/wrapper/Envelope$Builder;->message_name(Ljava/lang/String;)Lcom/squareup/comms/protos/wrapper/Envelope$Builder;

    move-result-object v0

    .line 28
    invoke-static {p0}, Lokio/ByteString;->of([B)Lokio/ByteString;

    move-result-object p0

    invoke-virtual {v0, p0}, Lcom/squareup/comms/protos/wrapper/Envelope$Builder;->message_body(Lokio/ByteString;)Lcom/squareup/comms/protos/wrapper/Envelope$Builder;

    move-result-object p0

    .line 29
    invoke-virtual {p0, p1}, Lcom/squareup/comms/protos/wrapper/Envelope$Builder;->uuid(Ljava/lang/String;)Lcom/squareup/comms/protos/wrapper/Envelope$Builder;

    move-result-object p0

    .line 30
    invoke-virtual {p0}, Lcom/squareup/comms/protos/wrapper/Envelope$Builder;->build()Lcom/squareup/comms/protos/wrapper/Envelope;

    move-result-object p0

    return-object p0
.end method
