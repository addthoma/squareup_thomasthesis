.class Lcom/squareup/comms/RemoteBusImpl$ChannelListener$1;
.super Ljava/lang/Object;
.source "RemoteBusImpl.java"

# interfaces
.implements Lcom/squareup/comms/HealthChecker$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/comms/RemoteBusImpl$ChannelListener;->onConnected(Lcom/squareup/comms/net/Device;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/squareup/comms/RemoteBusImpl$ChannelListener;

.field final synthetic val$device:Lcom/squareup/comms/net/Device;


# direct methods
.method constructor <init>(Lcom/squareup/comms/RemoteBusImpl$ChannelListener;Lcom/squareup/comms/net/Device;)V
    .locals 0

    .line 90
    iput-object p1, p0, Lcom/squareup/comms/RemoteBusImpl$ChannelListener$1;->this$1:Lcom/squareup/comms/RemoteBusImpl$ChannelListener;

    iput-object p2, p0, Lcom/squareup/comms/RemoteBusImpl$ChannelListener$1;->val$device:Lcom/squareup/comms/net/Device;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onHealthy()V
    .locals 4

    .line 92
    iget-object v0, p0, Lcom/squareup/comms/RemoteBusImpl$ChannelListener$1;->this$1:Lcom/squareup/comms/RemoteBusImpl$ChannelListener;

    invoke-static {v0}, Lcom/squareup/comms/RemoteBusImpl$ChannelListener;->access$600(Lcom/squareup/comms/RemoteBusImpl$ChannelListener;)Lcom/squareup/comms/RemoteBusImpl$State;

    move-result-object v0

    sget-object v1, Lcom/squareup/comms/RemoteBusImpl$State;->HEALTHY:Lcom/squareup/comms/RemoteBusImpl$State;

    if-eq v0, v1, :cond_0

    .line 93
    iget-object v0, p0, Lcom/squareup/comms/RemoteBusImpl$ChannelListener$1;->this$1:Lcom/squareup/comms/RemoteBusImpl$ChannelListener;

    sget-object v1, Lcom/squareup/comms/RemoteBusImpl$State;->HEALTHY:Lcom/squareup/comms/RemoteBusImpl$State;

    const-string v2, "RemoteBusImpl.onHealthy"

    invoke-static {v0, v2, v1}, Lcom/squareup/comms/RemoteBusImpl$ChannelListener;->access$700(Lcom/squareup/comms/RemoteBusImpl$ChannelListener;Ljava/lang/String;Lcom/squareup/comms/RemoteBusImpl$State;)Lcom/squareup/comms/RemoteBusImpl$State;

    .line 94
    iget-object v0, p0, Lcom/squareup/comms/RemoteBusImpl$ChannelListener$1;->this$1:Lcom/squareup/comms/RemoteBusImpl$ChannelListener;

    iget-object v0, v0, Lcom/squareup/comms/RemoteBusImpl$ChannelListener;->this$0:Lcom/squareup/comms/RemoteBusImpl;

    new-instance v1, Lcom/squareup/comms/RemoteBusConnectionImpl;

    iget-object v2, p0, Lcom/squareup/comms/RemoteBusImpl$ChannelListener$1;->this$1:Lcom/squareup/comms/RemoteBusImpl$ChannelListener;

    iget-object v2, v2, Lcom/squareup/comms/RemoteBusImpl$ChannelListener;->this$0:Lcom/squareup/comms/RemoteBusImpl;

    iget-object v3, p0, Lcom/squareup/comms/RemoteBusImpl$ChannelListener$1;->this$1:Lcom/squareup/comms/RemoteBusImpl$ChannelListener;

    iget-object v3, v3, Lcom/squareup/comms/RemoteBusImpl$ChannelListener;->this$0:Lcom/squareup/comms/RemoteBusImpl;

    invoke-static {v3}, Lcom/squareup/comms/RemoteBusImpl;->access$900(Lcom/squareup/comms/RemoteBusImpl;)Ljava/util/concurrent/Executor;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/squareup/comms/RemoteBusConnectionImpl;-><init>(Lcom/squareup/comms/MessagePoster;Ljava/util/concurrent/Executor;)V

    invoke-static {v0, v1}, Lcom/squareup/comms/RemoteBusImpl;->access$802(Lcom/squareup/comms/RemoteBusImpl;Lcom/squareup/comms/RemoteBusConnectionImpl;)Lcom/squareup/comms/RemoteBusConnectionImpl;

    .line 95
    iget-object v0, p0, Lcom/squareup/comms/RemoteBusImpl$ChannelListener$1;->this$1:Lcom/squareup/comms/RemoteBusImpl$ChannelListener;

    iget-object v0, v0, Lcom/squareup/comms/RemoteBusImpl$ChannelListener;->this$0:Lcom/squareup/comms/RemoteBusImpl;

    invoke-static {v0}, Lcom/squareup/comms/RemoteBusImpl;->access$1000(Lcom/squareup/comms/RemoteBusImpl;)Lcom/squareup/comms/ConnectionListener;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/comms/RemoteBusImpl$ChannelListener$1;->this$1:Lcom/squareup/comms/RemoteBusImpl$ChannelListener;

    iget-object v1, v1, Lcom/squareup/comms/RemoteBusImpl$ChannelListener;->this$0:Lcom/squareup/comms/RemoteBusImpl;

    invoke-static {v1}, Lcom/squareup/comms/RemoteBusImpl;->access$800(Lcom/squareup/comms/RemoteBusImpl;)Lcom/squareup/comms/RemoteBusConnectionImpl;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/comms/RemoteBusImpl$ChannelListener$1;->val$device:Lcom/squareup/comms/net/Device;

    invoke-interface {v0, v1, v2}, Lcom/squareup/comms/ConnectionListener;->onConnected(Lcom/squareup/comms/RemoteBusConnection;Lcom/squareup/comms/net/Device;)V

    :cond_0
    return-void
.end method

.method public onUnhealthy()V
    .locals 3

    .line 100
    iget-object v0, p0, Lcom/squareup/comms/RemoteBusImpl$ChannelListener$1;->this$1:Lcom/squareup/comms/RemoteBusImpl$ChannelListener;

    invoke-static {v0}, Lcom/squareup/comms/RemoteBusImpl$ChannelListener;->access$600(Lcom/squareup/comms/RemoteBusImpl$ChannelListener;)Lcom/squareup/comms/RemoteBusImpl$State;

    move-result-object v0

    sget-object v1, Lcom/squareup/comms/RemoteBusImpl$State;->HEALTHY:Lcom/squareup/comms/RemoteBusImpl$State;

    if-ne v0, v1, :cond_0

    .line 101
    iget-object v0, p0, Lcom/squareup/comms/RemoteBusImpl$ChannelListener$1;->this$1:Lcom/squareup/comms/RemoteBusImpl$ChannelListener;

    sget-object v1, Lcom/squareup/comms/RemoteBusImpl$State;->UNHEALTHY:Lcom/squareup/comms/RemoteBusImpl$State;

    const-string v2, "RemoteBusImpl.onUnhealthy"

    invoke-static {v0, v2, v1}, Lcom/squareup/comms/RemoteBusImpl$ChannelListener;->access$700(Lcom/squareup/comms/RemoteBusImpl$ChannelListener;Ljava/lang/String;Lcom/squareup/comms/RemoteBusImpl$State;)Lcom/squareup/comms/RemoteBusImpl$State;

    .line 102
    iget-object v0, p0, Lcom/squareup/comms/RemoteBusImpl$ChannelListener$1;->this$1:Lcom/squareup/comms/RemoteBusImpl$ChannelListener;

    iget-object v0, v0, Lcom/squareup/comms/RemoteBusImpl$ChannelListener;->this$0:Lcom/squareup/comms/RemoteBusImpl;

    invoke-static {v0}, Lcom/squareup/comms/RemoteBusImpl;->access$800(Lcom/squareup/comms/RemoteBusImpl;)Lcom/squareup/comms/RemoteBusConnectionImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/comms/RemoteBusConnectionImpl;->onDisconnected()V

    .line 103
    iget-object v0, p0, Lcom/squareup/comms/RemoteBusImpl$ChannelListener$1;->this$1:Lcom/squareup/comms/RemoteBusImpl$ChannelListener;

    iget-object v0, v0, Lcom/squareup/comms/RemoteBusImpl$ChannelListener;->this$0:Lcom/squareup/comms/RemoteBusImpl;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/squareup/comms/RemoteBusImpl;->access$802(Lcom/squareup/comms/RemoteBusImpl;Lcom/squareup/comms/RemoteBusConnectionImpl;)Lcom/squareup/comms/RemoteBusConnectionImpl;

    .line 105
    :cond_0
    iget-object v0, p0, Lcom/squareup/comms/RemoteBusImpl$ChannelListener$1;->this$1:Lcom/squareup/comms/RemoteBusImpl$ChannelListener;

    iget-object v0, v0, Lcom/squareup/comms/RemoteBusImpl$ChannelListener;->this$0:Lcom/squareup/comms/RemoteBusImpl;

    invoke-static {v0}, Lcom/squareup/comms/RemoteBusImpl;->access$300(Lcom/squareup/comms/RemoteBusImpl;)Lcom/squareup/comms/net/Channel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/comms/net/Channel;->disconnect()V

    return-void
.end method
