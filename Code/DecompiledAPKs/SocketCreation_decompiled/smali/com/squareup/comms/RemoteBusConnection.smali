.class public interface abstract Lcom/squareup/comms/RemoteBusConnection;
.super Ljava/lang/Object;
.source "RemoteBusConnection.java"

# interfaces
.implements Lcom/squareup/comms/MessagePoster;


# virtual methods
.method public abstract observable()Lrx/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lrx/Observable<",
            "Lcom/squareup/wire/Message;",
            ">;"
        }
    .end annotation
.end method

.method public abstract post(Lcom/squareup/wire/Message;)V
.end method
