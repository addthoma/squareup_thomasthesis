.class Lcom/squareup/comms/common/IoThread$1;
.super Ljava/lang/Object;
.source "IoThread.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/comms/common/IoThread;->register(Ljava/nio/channels/SelectableChannel;ILcom/squareup/comms/common/IoCompletion;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/comms/common/IoThread;

.field final synthetic val$channel:Ljava/nio/channels/SelectableChannel;

.field final synthetic val$onComplete:Lcom/squareup/comms/common/IoCompletion;

.field final synthetic val$opSet:I


# direct methods
.method constructor <init>(Lcom/squareup/comms/common/IoThread;Lcom/squareup/comms/common/IoCompletion;Ljava/nio/channels/SelectableChannel;I)V
    .locals 0

    .line 111
    iput-object p1, p0, Lcom/squareup/comms/common/IoThread$1;->this$0:Lcom/squareup/comms/common/IoThread;

    iput-object p2, p0, Lcom/squareup/comms/common/IoThread$1;->val$onComplete:Lcom/squareup/comms/common/IoCompletion;

    iput-object p3, p0, Lcom/squareup/comms/common/IoThread$1;->val$channel:Ljava/nio/channels/SelectableChannel;

    iput p4, p0, Lcom/squareup/comms/common/IoThread$1;->val$opSet:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .line 113
    iget-object v0, p0, Lcom/squareup/comms/common/IoThread$1;->this$0:Lcom/squareup/comms/common/IoThread;

    invoke-static {v0}, Lcom/squareup/comms/common/IoThread;->access$100(Lcom/squareup/comms/common/IoThread;)Ljava/nio/channels/Selector;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/channels/Selector;->isOpen()Z

    move-result v0

    if-nez v0, :cond_0

    .line 114
    iget-object v0, p0, Lcom/squareup/comms/common/IoThread$1;->val$onComplete:Lcom/squareup/comms/common/IoCompletion;

    new-instance v1, Ljava/io/IOException;

    const-string v2, "IoThread: ignoring IO registration, selector is closed"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/squareup/comms/common/IoCompletion;->onError(Ljava/io/IOException;)V

    return-void

    .line 120
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/squareup/comms/common/IoThread$1;->val$channel:Ljava/nio/channels/SelectableChannel;

    iget-object v1, p0, Lcom/squareup/comms/common/IoThread$1;->this$0:Lcom/squareup/comms/common/IoThread;

    invoke-static {v1}, Lcom/squareup/comms/common/IoThread;->access$100(Lcom/squareup/comms/common/IoThread;)Ljava/nio/channels/Selector;

    move-result-object v1

    iget v2, p0, Lcom/squareup/comms/common/IoThread$1;->val$opSet:I

    new-instance v3, Lcom/squareup/comms/common/IoOperation;

    iget-object v4, p0, Lcom/squareup/comms/common/IoThread$1;->val$channel:Ljava/nio/channels/SelectableChannel;

    iget-object v5, p0, Lcom/squareup/comms/common/IoThread$1;->val$onComplete:Lcom/squareup/comms/common/IoCompletion;

    iget v6, p0, Lcom/squareup/comms/common/IoThread$1;->val$opSet:I

    invoke-direct {v3, v4, v5, v6}, Lcom/squareup/comms/common/IoOperation;-><init>(Ljava/nio/channels/SelectableChannel;Lcom/squareup/comms/common/IoCompletion;I)V

    invoke-virtual {v0, v1, v2, v3}, Ljava/nio/channels/SelectableChannel;->register(Ljava/nio/channels/Selector;ILjava/lang/Object;)Ljava/nio/channels/SelectionKey;
    :try_end_0
    .catch Ljava/nio/channels/ClosedChannelException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "IoThread: ignoring IO registration, channel is closed"

    .line 122
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void
.end method
