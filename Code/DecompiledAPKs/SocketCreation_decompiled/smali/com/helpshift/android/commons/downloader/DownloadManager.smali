.class public Lcom/helpshift/android/commons/downloader/DownloadManager;
.super Ljava/lang/Object;
.source "DownloadManager.java"


# static fields
.field private static final KEY_CACHED_DOWNLOADS:Ljava/lang/String; = "hs-cached-downloads"

.field private static final TAG:Ljava/lang/String; = "Helpshift_DownloadMngr"


# instance fields
.field activeDownloadFinishListeners:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/String;",
            "Ljava/util/concurrent/ConcurrentLinkedQueue<",
            "Lcom/helpshift/android/commons/downloader/contracts/OnDownloadFinishListener;",
            ">;>;"
        }
    .end annotation
.end field

.field activeProgressChangeListeners:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap<",
            "Ljava/lang/String;",
            "Ljava/util/concurrent/ConcurrentLinkedQueue<",
            "Lcom/helpshift/android/commons/downloader/contracts/OnProgressChangedListener;",
            ">;>;"
        }
    .end annotation
.end field

.field private context:Landroid/content/Context;

.field private downloadExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

.field private storage:Lcom/helpshift/android/commons/downloader/contracts/DownloaderKeyValueStorage;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/helpshift/android/commons/downloader/contracts/DownloaderKeyValueStorage;Ljava/util/concurrent/ThreadPoolExecutor;)V
    .locals 0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/helpshift/android/commons/downloader/DownloadManager;->context:Landroid/content/Context;

    .line 32
    iput-object p2, p0, Lcom/helpshift/android/commons/downloader/DownloadManager;->storage:Lcom/helpshift/android/commons/downloader/contracts/DownloaderKeyValueStorage;

    .line 33
    iput-object p3, p0, Lcom/helpshift/android/commons/downloader/DownloadManager;->downloadExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    .line 34
    new-instance p1, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {p1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object p1, p0, Lcom/helpshift/android/commons/downloader/DownloadManager;->activeDownloadFinishListeners:Ljava/util/concurrent/ConcurrentHashMap;

    .line 35
    new-instance p1, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {p1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object p1, p0, Lcom/helpshift/android/commons/downloader/DownloadManager;->activeProgressChangeListeners:Ljava/util/concurrent/ConcurrentHashMap;

    return-void
.end method

.method private getAvailableCacheFile(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .line 150
    iget-object v0, p0, Lcom/helpshift/android/commons/downloader/DownloadManager;->storage:Lcom/helpshift/android/commons/downloader/contracts/DownloaderKeyValueStorage;

    const-string v1, "hs-cached-downloads"

    invoke-interface {v0, v1}, Lcom/helpshift/android/commons/downloader/contracts/DownloaderKeyValueStorage;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    .line 152
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    .line 153
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 154
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 155
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Ljava/io/File;->canRead()Z

    move-result v0

    if-eqz v0, :cond_1

    return-object p1

    :cond_0
    move-object v1, p1

    :cond_1
    return-object v1
.end method


# virtual methods
.method addToCache(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 139
    iget-object v0, p0, Lcom/helpshift/android/commons/downloader/DownloadManager;->storage:Lcom/helpshift/android/commons/downloader/contracts/DownloaderKeyValueStorage;

    const-string v1, "hs-cached-downloads"

    invoke-interface {v0, v1}, Lcom/helpshift/android/commons/downloader/contracts/DownloaderKeyValueStorage;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    if-nez v0, :cond_0

    .line 141
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 143
    :cond_0
    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 144
    iget-object p1, p0, Lcom/helpshift/android/commons/downloader/DownloadManager;->storage:Lcom/helpshift/android/commons/downloader/contracts/DownloaderKeyValueStorage;

    invoke-interface {p1, v1, v0}, Lcom/helpshift/android/commons/downloader/contracts/DownloaderKeyValueStorage;->set(Ljava/lang/String;Ljava/io/Serializable;)Z

    return-void
.end method

.method public startDownload(Ljava/lang/String;Lcom/helpshift/android/commons/downloader/DownloadConfig;Lcom/helpshift/android/commons/downloader/contracts/OnDownloadFinishListener;Lcom/helpshift/android/commons/downloader/contracts/OnProgressChangedListener;)V
    .locals 7

    const/4 v2, 0x0

    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v5, p3

    move-object v6, p4

    .line 42
    invoke-virtual/range {v0 .. v6}, Lcom/helpshift/android/commons/downloader/DownloadManager;->startDownload(Ljava/lang/String;ZLcom/helpshift/android/commons/downloader/DownloadConfig;Lcom/helpshift/android/commons/downloader/contracts/NetworkAuthDataFetcher;Lcom/helpshift/android/commons/downloader/contracts/OnDownloadFinishListener;Lcom/helpshift/android/commons/downloader/contracts/OnProgressChangedListener;)V

    return-void
.end method

.method public startDownload(Ljava/lang/String;ZLcom/helpshift/android/commons/downloader/DownloadConfig;Lcom/helpshift/android/commons/downloader/contracts/NetworkAuthDataFetcher;Lcom/helpshift/android/commons/downloader/contracts/OnDownloadFinishListener;Lcom/helpshift/android/commons/downloader/contracts/OnProgressChangedListener;)V
    .locals 9

    .line 50
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Scheduling download in executor : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Helpshift_DownloadMngr"

    invoke-static {v1, v0}, Lcom/helpshift/util/HSLogger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    iget-boolean v0, p3, Lcom/helpshift/android/commons/downloader/DownloadConfig;->useCache:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/helpshift/android/commons/downloader/DownloadManager;->getAvailableCacheFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 p2, 0x1

    .line 54
    invoke-direct {p0, p1}, Lcom/helpshift/android/commons/downloader/DownloadManager;->getAvailableCacheFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    invoke-interface {p5, p2, p1, p3}, Lcom/helpshift/android/commons/downloader/contracts/OnDownloadFinishListener;->onDownloadFinish(ZLjava/lang/String;Ljava/lang/Object;)V

    return-void

    .line 60
    :cond_0
    iget-object v0, p0, Lcom/helpshift/android/commons/downloader/DownloadManager;->activeDownloadFinishListeners:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/helpshift/android/commons/downloader/DownloadManager;->activeProgressChangeListeners:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_3

    if-eqz p5, :cond_1

    .line 62
    iget-object p2, p0, Lcom/helpshift/android/commons/downloader/DownloadManager;->activeDownloadFinishListeners:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p2, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {p2, p5}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    :cond_1
    if-eqz p6, :cond_2

    .line 65
    iget-object p2, p0, Lcom/helpshift/android/commons/downloader/DownloadManager;->activeProgressChangeListeners:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p2, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {p1, p6}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    :cond_2
    return-void

    .line 72
    :cond_3
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    if-eqz p5, :cond_4

    .line 74
    invoke-virtual {v0, p5}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    .line 76
    :cond_4
    iget-object p5, p0, Lcom/helpshift/android/commons/downloader/DownloadManager;->activeDownloadFinishListeners:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p5, p1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    new-instance p5, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {p5}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    if-eqz p6, :cond_5

    .line 80
    invoke-virtual {p5, p6}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    .line 82
    :cond_5
    iget-object p6, p0, Lcom/helpshift/android/commons/downloader/DownloadManager;->activeProgressChangeListeners:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p6, p1, p5}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    new-instance v7, Lcom/helpshift/android/commons/downloader/DownloadManager$1;

    invoke-direct {v7, p0, p3}, Lcom/helpshift/android/commons/downloader/DownloadManager$1;-><init>(Lcom/helpshift/android/commons/downloader/DownloadManager;Lcom/helpshift/android/commons/downloader/DownloadConfig;)V

    .line 107
    new-instance v8, Lcom/helpshift/android/commons/downloader/DownloadManager$2;

    invoke-direct {v8, p0}, Lcom/helpshift/android/commons/downloader/DownloadManager$2;-><init>(Lcom/helpshift/android/commons/downloader/DownloadManager;)V

    .line 126
    new-instance p5, Lcom/helpshift/android/commons/downloader/DownloadRunnable;

    iget-object v1, p0, Lcom/helpshift/android/commons/downloader/DownloadManager;->context:Landroid/content/Context;

    iget-object v2, p0, Lcom/helpshift/android/commons/downloader/DownloadManager;->storage:Lcom/helpshift/android/commons/downloader/contracts/DownloaderKeyValueStorage;

    move-object v0, p5

    move-object v3, p1

    move v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v8}, Lcom/helpshift/android/commons/downloader/DownloadRunnable;-><init>(Landroid/content/Context;Lcom/helpshift/android/commons/downloader/contracts/DownloaderKeyValueStorage;Ljava/lang/String;ZLcom/helpshift/android/commons/downloader/DownloadConfig;Lcom/helpshift/android/commons/downloader/contracts/NetworkAuthDataFetcher;Lcom/helpshift/android/commons/downloader/contracts/OnDownloadFinishListener;Lcom/helpshift/android/commons/downloader/contracts/OnProgressChangedListener;)V

    .line 134
    iget-object p1, p0, Lcom/helpshift/android/commons/downloader/DownloadManager;->downloadExecutor:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {p1, p5}, Ljava/util/concurrent/ThreadPoolExecutor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method
