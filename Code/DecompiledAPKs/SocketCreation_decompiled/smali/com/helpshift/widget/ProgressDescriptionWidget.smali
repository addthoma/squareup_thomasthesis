.class public Lcom/helpshift/widget/ProgressDescriptionWidget;
.super Lcom/helpshift/widget/Widget;
.source "ProgressDescriptionWidget.java"


# instance fields
.field private isVisible:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 3
    invoke-direct {p0}, Lcom/helpshift/widget/Widget;-><init>()V

    const/4 v0, 0x0

    .line 5
    iput-boolean v0, p0, Lcom/helpshift/widget/ProgressDescriptionWidget;->isVisible:Z

    return-void
.end method


# virtual methods
.method public isVisible()Z
    .locals 1

    .line 8
    iget-boolean v0, p0, Lcom/helpshift/widget/ProgressDescriptionWidget;->isVisible:Z

    return v0
.end method

.method public setVisible(Z)V
    .locals 1

    .line 12
    iget-boolean v0, p0, Lcom/helpshift/widget/ProgressDescriptionWidget;->isVisible:Z

    if-ne v0, p1, :cond_0

    return-void

    .line 15
    :cond_0
    iput-boolean p1, p0, Lcom/helpshift/widget/ProgressDescriptionWidget;->isVisible:Z

    .line 16
    invoke-virtual {p0}, Lcom/helpshift/widget/ProgressDescriptionWidget;->notifyChanged()V

    return-void
.end method
