.class public final Lcom/helpshift/support/ContactUsFilter;
.super Ljava/lang/Object;
.source "ContactUsFilter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/helpshift/support/ContactUsFilter$LOCATION;
    }
.end annotation


# static fields
.field private static data:Lcom/helpshift/support/HSApiData;

.field private static enableContactUs:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static init(Landroid/content/Context;)V
    .locals 1

    .line 15
    sget-object v0, Lcom/helpshift/support/ContactUsFilter;->data:Lcom/helpshift/support/HSApiData;

    if-nez v0, :cond_0

    .line 16
    new-instance v0, Lcom/helpshift/support/HSApiData;

    invoke-direct {v0, p0}, Lcom/helpshift/support/HSApiData;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/helpshift/support/ContactUsFilter;->data:Lcom/helpshift/support/HSApiData;

    .line 17
    invoke-static {}, Lcom/helpshift/util/HelpshiftContext;->getCoreApi()Lcom/helpshift/CoreApi;

    move-result-object p0

    invoke-interface {p0}, Lcom/helpshift/CoreApi;->getSDKConfigurationDM()Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;

    move-result-object p0

    invoke-virtual {p0}, Lcom/helpshift/configuration/domainmodel/SDKConfigurationDM;->getEnableContactUs()Lcom/helpshift/configuration/dto/RootApiConfig$EnableContactUs;

    move-result-object p0

    invoke-virtual {p0}, Lcom/helpshift/configuration/dto/RootApiConfig$EnableContactUs;->getValue()I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    sput-object p0, Lcom/helpshift/support/ContactUsFilter;->enableContactUs:Ljava/lang/Integer;

    :cond_0
    return-void
.end method

.method protected static setConfig(Ljava/util/HashMap;)V
    .locals 3

    if-nez p0, :cond_0

    .line 23
    new-instance p0, Ljava/util/HashMap;

    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    :cond_0
    const-string v0, "enableContactUs"

    .line 26
    invoke-virtual {p0, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 28
    instance-of v2, v1, Ljava/lang/Integer;

    if-eqz v2, :cond_1

    .line 29
    invoke-virtual {p0, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Integer;

    sput-object p0, Lcom/helpshift/support/ContactUsFilter;->enableContactUs:Ljava/lang/Integer;

    goto :goto_0

    .line 31
    :cond_1
    instance-of p0, v1, Ljava/lang/Boolean;

    if-eqz p0, :cond_3

    .line 32
    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    if-eqz p0, :cond_2

    .line 33
    sget-object p0, Lcom/helpshift/support/SupportInternal$EnableContactUs;->ALWAYS:Ljava/lang/Integer;

    sput-object p0, Lcom/helpshift/support/ContactUsFilter;->enableContactUs:Ljava/lang/Integer;

    goto :goto_0

    .line 36
    :cond_2
    sget-object p0, Lcom/helpshift/support/SupportInternal$EnableContactUs;->NEVER:Ljava/lang/Integer;

    sput-object p0, Lcom/helpshift/support/ContactUsFilter;->enableContactUs:Ljava/lang/Integer;

    :cond_3
    :goto_0
    return-void
.end method

.method public static showContactUs(Lcom/helpshift/support/ContactUsFilter$LOCATION;)Z
    .locals 8

    .line 42
    sget-object v0, Lcom/helpshift/support/ContactUsFilter$1;->$SwitchMap$com$helpshift$support$ContactUsFilter$LOCATION:[I

    invoke-virtual {p0}, Lcom/helpshift/support/ContactUsFilter$LOCATION;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eq v0, v2, :cond_b

    .line 47
    sget-object v0, Lcom/helpshift/support/SupportInternal$EnableContactUs;->ALWAYS:Ljava/lang/Integer;

    sget-object v3, Lcom/helpshift/support/ContactUsFilter;->enableContactUs:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return v2

    .line 50
    :cond_0
    sget-object v0, Lcom/helpshift/support/SupportInternal$EnableContactUs;->NEVER:Ljava/lang/Integer;

    sget-object v3, Lcom/helpshift/support/ContactUsFilter;->enableContactUs:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    return v1

    .line 53
    :cond_1
    sget-object v0, Lcom/helpshift/support/SupportInternal$EnableContactUs;->AFTER_VIEWING_FAQS:Ljava/lang/Integer;

    sget-object v3, Lcom/helpshift/support/ContactUsFilter;->enableContactUs:Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v3, 0x5

    const/4 v4, 0x4

    const/4 v5, 0x3

    const/4 v6, 0x2

    if-eqz v0, :cond_5

    .line 54
    sget-object v0, Lcom/helpshift/support/ContactUsFilter$1;->$SwitchMap$com$helpshift$support$ContactUsFilter$LOCATION:[I

    invoke-virtual {p0}, Lcom/helpshift/support/ContactUsFilter$LOCATION;->ordinal()I

    move-result p0

    aget p0, v0, p0

    if-eq p0, v6, :cond_4

    if-eq p0, v5, :cond_4

    if-eq p0, v4, :cond_4

    if-eq p0, v3, :cond_2

    return v2

    .line 62
    :cond_2
    invoke-static {}, Lcom/helpshift/util/HelpshiftContext;->getCoreApi()Lcom/helpshift/CoreApi;

    move-result-object p0

    invoke-interface {p0}, Lcom/helpshift/CoreApi;->getActiveConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object p0

    if-eqz p0, :cond_3

    const/4 v1, 0x1

    :cond_3
    return v1

    :cond_4
    return v2

    .line 67
    :cond_5
    sget-object v0, Lcom/helpshift/support/SupportInternal$EnableContactUs;->AFTER_MARKING_ANSWER_UNHELPFUL:Ljava/lang/Integer;

    sget-object v7, Lcom/helpshift/support/ContactUsFilter;->enableContactUs:Ljava/lang/Integer;

    invoke-virtual {v0, v7}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 68
    sget-object v0, Lcom/helpshift/support/ContactUsFilter$1;->$SwitchMap$com$helpshift$support$ContactUsFilter$LOCATION:[I

    invoke-virtual {p0}, Lcom/helpshift/support/ContactUsFilter$LOCATION;->ordinal()I

    move-result p0

    aget p0, v0, p0

    if-eq p0, v6, :cond_9

    if-eq p0, v5, :cond_8

    if-eq p0, v4, :cond_6

    if-eq p0, v3, :cond_6

    return v2

    .line 75
    :cond_6
    invoke-static {}, Lcom/helpshift/util/HelpshiftContext;->getCoreApi()Lcom/helpshift/CoreApi;

    move-result-object p0

    invoke-interface {p0}, Lcom/helpshift/CoreApi;->getActiveConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object p0

    if-eqz p0, :cond_7

    const/4 v1, 0x1

    :cond_7
    return v1

    :cond_8
    return v2

    :cond_9
    return v1

    :cond_a
    return v2

    :cond_b
    return v1
.end method
