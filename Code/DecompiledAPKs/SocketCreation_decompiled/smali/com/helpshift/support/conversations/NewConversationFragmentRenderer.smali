.class public Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;
.super Ljava/lang/Object;
.source "NewConversationFragmentRenderer.java"

# interfaces
.implements Lcom/helpshift/conversation/viewmodel/NewConversationRenderer;


# instance fields
.field private final attachmentClearButton:Landroid/widget/ImageButton;

.field private final attachmentContainer:Landroidx/cardview/widget/CardView;

.field private final attachmentFileName:Landroid/widget/TextView;

.field private final attachmentFileSize:Landroid/widget/TextView;

.field private final attachmentImage:Landroid/widget/ImageView;

.field private final context:Landroid/content/Context;

.field private final descriptionField:Lcom/google/android/material/textfield/TextInputEditText;

.field private final descriptionFieldWrapper:Lcom/google/android/material/textfield/TextInputLayout;

.field private final emailField:Lcom/google/android/material/textfield/TextInputEditText;

.field private final emailFieldWrapper:Lcom/google/android/material/textfield/TextInputLayout;

.field private final menuItemRenderer:Lcom/helpshift/support/fragments/IToolbarMenuItemRenderer;

.field private final nameField:Lcom/google/android/material/textfield/TextInputEditText;

.field private final nameFieldWrapper:Lcom/google/android/material/textfield/TextInputLayout;

.field private final newConversationRouter:Lcom/helpshift/support/conversations/NewConversationRouter;

.field private final parentView:Landroid/view/View;

.field private final progressBar:Landroid/widget/ProgressBar;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/material/textfield/TextInputLayout;Lcom/google/android/material/textfield/TextInputEditText;Lcom/google/android/material/textfield/TextInputLayout;Lcom/google/android/material/textfield/TextInputEditText;Lcom/google/android/material/textfield/TextInputLayout;Lcom/google/android/material/textfield/TextInputEditText;Landroid/widget/ProgressBar;Landroid/widget/ImageView;Landroid/widget/TextView;Landroid/widget/TextView;Landroidx/cardview/widget/CardView;Landroid/widget/ImageButton;Landroid/view/View;Lcom/helpshift/support/conversations/NewConversationRouter;Lcom/helpshift/support/fragments/IToolbarMenuItemRenderer;)V
    .locals 2

    move-object v0, p0

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 67
    iput-object v1, v0, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->context:Landroid/content/Context;

    move-object v1, p2

    .line 68
    iput-object v1, v0, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->descriptionFieldWrapper:Lcom/google/android/material/textfield/TextInputLayout;

    move-object v1, p3

    .line 69
    iput-object v1, v0, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->descriptionField:Lcom/google/android/material/textfield/TextInputEditText;

    move-object v1, p4

    .line 70
    iput-object v1, v0, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->nameFieldWrapper:Lcom/google/android/material/textfield/TextInputLayout;

    move-object v1, p5

    .line 71
    iput-object v1, v0, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->nameField:Lcom/google/android/material/textfield/TextInputEditText;

    move-object v1, p6

    .line 72
    iput-object v1, v0, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->emailFieldWrapper:Lcom/google/android/material/textfield/TextInputLayout;

    move-object v1, p7

    .line 73
    iput-object v1, v0, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->emailField:Lcom/google/android/material/textfield/TextInputEditText;

    move-object v1, p8

    .line 74
    iput-object v1, v0, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->progressBar:Landroid/widget/ProgressBar;

    move-object v1, p9

    .line 75
    iput-object v1, v0, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->attachmentImage:Landroid/widget/ImageView;

    move-object v1, p10

    .line 76
    iput-object v1, v0, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->attachmentFileName:Landroid/widget/TextView;

    move-object v1, p11

    .line 77
    iput-object v1, v0, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->attachmentFileSize:Landroid/widget/TextView;

    move-object v1, p12

    .line 78
    iput-object v1, v0, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->attachmentContainer:Landroidx/cardview/widget/CardView;

    move-object v1, p13

    .line 79
    iput-object v1, v0, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->attachmentClearButton:Landroid/widget/ImageButton;

    move-object/from16 v1, p14

    .line 80
    iput-object v1, v0, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->parentView:Landroid/view/View;

    move-object/from16 v1, p15

    .line 81
    iput-object v1, v0, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->newConversationRouter:Lcom/helpshift/support/conversations/NewConversationRouter;

    move-object/from16 v1, p16

    .line 82
    iput-object v1, v0, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->menuItemRenderer:Lcom/helpshift/support/fragments/IToolbarMenuItemRenderer;

    return-void
.end method

.method private changeMenuItemVisibility(Lcom/helpshift/support/fragments/HSMenuItemType;Z)V
    .locals 1

    .line 283
    iget-object v0, p0, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->menuItemRenderer:Lcom/helpshift/support/fragments/IToolbarMenuItemRenderer;

    if-eqz v0, :cond_0

    .line 284
    invoke-interface {v0, p1, p2}, Lcom/helpshift/support/fragments/IToolbarMenuItemRenderer;->updateMenuItemVisibility(Lcom/helpshift/support/fragments/HSMenuItemType;Z)V

    :cond_0
    return-void
.end method

.method private getText(I)Ljava/lang/String;
    .locals 1

    .line 86
    iget-object v0, p0, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->context:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private setError(Lcom/google/android/material/textfield/TextInputLayout;Ljava/lang/CharSequence;)V
    .locals 1

    .line 278
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-virtual {p1, v0}, Lcom/google/android/material/textfield/TextInputLayout;->setErrorEnabled(Z)V

    .line 279
    invoke-virtual {p1, p2}, Lcom/google/android/material/textfield/TextInputLayout;->setError(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public clearDescriptionError()V
    .locals 2

    .line 106
    iget-object v0, p0, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->descriptionFieldWrapper:Lcom/google/android/material/textfield/TextInputLayout;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->setError(Lcom/google/android/material/textfield/TextInputLayout;Ljava/lang/CharSequence;)V

    return-void
.end method

.method public clearEmailError()V
    .locals 2

    .line 136
    iget-object v0, p0, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->emailFieldWrapper:Lcom/google/android/material/textfield/TextInputLayout;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->setError(Lcom/google/android/material/textfield/TextInputLayout;Ljava/lang/CharSequence;)V

    return-void
.end method

.method public clearNameError()V
    .locals 2

    .line 121
    iget-object v0, p0, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->nameFieldWrapper:Lcom/google/android/material/textfield/TextInputLayout;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->setError(Lcom/google/android/material/textfield/TextInputLayout;Ljava/lang/CharSequence;)V

    return-void
.end method

.method public disableImageAttachmentClickable()V
    .locals 0

    return-void
.end method

.method public enableImageAttachmentClickable()V
    .locals 0

    return-void
.end method

.method public exit()V
    .locals 1

    .line 227
    iget-object v0, p0, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->newConversationRouter:Lcom/helpshift/support/conversations/NewConversationRouter;

    invoke-interface {v0}, Lcom/helpshift/support/conversations/NewConversationRouter;->exitNewConversationView()V

    return-void
.end method

.method public gotoConversation(J)V
    .locals 0

    .line 222
    iget-object p1, p0, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->newConversationRouter:Lcom/helpshift/support/conversations/NewConversationRouter;

    invoke-interface {p1}, Lcom/helpshift/support/conversations/NewConversationRouter;->showConversationScreen()V

    return-void
.end method

.method public hideImageAttachmentButton()V
    .locals 2

    .line 141
    sget-object v0, Lcom/helpshift/support/fragments/HSMenuItemType;->SCREENSHOT_ATTACHMENT:Lcom/helpshift/support/fragments/HSMenuItemType;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->changeMenuItemVisibility(Lcom/helpshift/support/fragments/HSMenuItemType;Z)V

    return-void
.end method

.method public hideImageAttachmentContainer()V
    .locals 2

    .line 170
    iget-object v0, p0, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->attachmentContainer:Landroidx/cardview/widget/CardView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroidx/cardview/widget/CardView;->setVisibility(I)V

    .line 171
    iget-object v0, p0, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->attachmentImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 172
    iget-object v0, p0, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->attachmentClearButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    return-void
.end method

.method public hideProfileForm()V
    .locals 2

    .line 201
    iget-object v0, p0, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->nameField:Lcom/google/android/material/textfield/TextInputEditText;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/material/textfield/TextInputEditText;->setVisibility(I)V

    .line 202
    iget-object v0, p0, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->emailField:Lcom/google/android/material/textfield/TextInputEditText;

    invoke-virtual {v0, v1}, Lcom/google/android/material/textfield/TextInputEditText;->setVisibility(I)V

    return-void
.end method

.method public hideProgressBar()V
    .locals 2

    .line 242
    iget-object v0, p0, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->progressBar:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    return-void
.end method

.method public hideStartConversationButton()V
    .locals 2

    .line 217
    sget-object v0, Lcom/helpshift/support/fragments/HSMenuItemType;->START_NEW_CONVERSATION:Lcom/helpshift/support/fragments/HSMenuItemType;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->changeMenuItemVisibility(Lcom/helpshift/support/fragments/HSMenuItemType;Z)V

    return-void
.end method

.method public onAuthenticationFailure()V
    .locals 1

    .line 274
    iget-object v0, p0, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->newConversationRouter:Lcom/helpshift/support/conversations/NewConversationRouter;

    invoke-interface {v0}, Lcom/helpshift/support/conversations/NewConversationRouter;->onAuthenticationFailure()V

    return-void
.end method

.method public setDescription(Ljava/lang/String;)V
    .locals 1

    .line 177
    iget-object v0, p0, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->descriptionField:Lcom/google/android/material/textfield/TextInputEditText;

    invoke-virtual {v0, p1}, Lcom/google/android/material/textfield/TextInputEditText;->setText(Ljava/lang/CharSequence;)V

    .line 178
    iget-object p1, p0, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->descriptionField:Lcom/google/android/material/textfield/TextInputEditText;

    invoke-virtual {p1}, Lcom/google/android/material/textfield/TextInputEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/android/material/textfield/TextInputEditText;->setSelection(I)V

    return-void
.end method

.method public setEmail(Ljava/lang/String;)V
    .locals 1

    .line 189
    iget-object v0, p0, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->emailField:Lcom/google/android/material/textfield/TextInputEditText;

    invoke-virtual {v0, p1}, Lcom/google/android/material/textfield/TextInputEditText;->setText(Ljava/lang/CharSequence;)V

    .line 190
    iget-object p1, p0, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->emailField:Lcom/google/android/material/textfield/TextInputEditText;

    invoke-virtual {p1}, Lcom/google/android/material/textfield/TextInputEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/android/material/textfield/TextInputEditText;->setSelection(I)V

    return-void
.end method

.method public setEmailRequired()V
    .locals 2

    .line 207
    iget-object v0, p0, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->emailField:Lcom/google/android/material/textfield/TextInputEditText;

    sget v1, Lcom/helpshift/R$string;->hs__email_required_hint:I

    invoke-direct {p0, v1}, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->getText(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/material/textfield/TextInputEditText;->setHint(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 1

    .line 183
    iget-object v0, p0, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->nameField:Lcom/google/android/material/textfield/TextInputEditText;

    invoke-virtual {v0, p1}, Lcom/google/android/material/textfield/TextInputEditText;->setText(Ljava/lang/CharSequence;)V

    .line 184
    iget-object p1, p0, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->nameField:Lcom/google/android/material/textfield/TextInputEditText;

    invoke-virtual {p1}, Lcom/google/android/material/textfield/TextInputEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/android/material/textfield/TextInputEditText;->setSelection(I)V

    return-void
.end method

.method public showAttachmentPreviewScreenFromDraft(Lcom/helpshift/conversation/dto/ImagePickerFile;)V
    .locals 1

    .line 232
    iget-object v0, p0, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->newConversationRouter:Lcom/helpshift/support/conversations/NewConversationRouter;

    invoke-interface {v0, p1}, Lcom/helpshift/support/conversations/NewConversationRouter;->showAttachmentPreviewScreenFromDraft(Lcom/helpshift/conversation/dto/ImagePickerFile;)V

    return-void
.end method

.method public showConversationStartedMessage()V
    .locals 3

    .line 247
    iget-object v0, p0, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->context:Landroid/content/Context;

    sget v1, Lcom/helpshift/R$string;->hs__conversation_started_message:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/helpshift/views/HSToast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    const/16 v1, 0x10

    .line 250
    invoke-virtual {v0, v1, v2, v2}, Landroid/widget/Toast;->setGravity(III)V

    .line 251
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method public showDescriptionEmptyError()V
    .locals 2

    .line 91
    iget-object v0, p0, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->descriptionFieldWrapper:Lcom/google/android/material/textfield/TextInputLayout;

    sget v1, Lcom/helpshift/R$string;->hs__conversation_detail_error:I

    invoke-direct {p0, v1}, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->getText(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->setError(Lcom/google/android/material/textfield/TextInputLayout;Ljava/lang/CharSequence;)V

    return-void
.end method

.method public showDescriptionLessThanMinimumError()V
    .locals 2

    .line 96
    iget-object v0, p0, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->descriptionFieldWrapper:Lcom/google/android/material/textfield/TextInputLayout;

    sget v1, Lcom/helpshift/R$string;->hs__description_invalid_length_error:I

    invoke-direct {p0, v1}, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->getText(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->setError(Lcom/google/android/material/textfield/TextInputLayout;Ljava/lang/CharSequence;)V

    return-void
.end method

.method public showDescriptionOnlySpecialCharactersError()V
    .locals 2

    .line 101
    iget-object v0, p0, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->descriptionFieldWrapper:Lcom/google/android/material/textfield/TextInputLayout;

    sget v1, Lcom/helpshift/R$string;->hs__invalid_description_error:I

    invoke-direct {p0, v1}, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->getText(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->setError(Lcom/google/android/material/textfield/TextInputLayout;Ljava/lang/CharSequence;)V

    return-void
.end method

.method public showEmailEmptyError()V
    .locals 2

    .line 131
    iget-object v0, p0, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->emailFieldWrapper:Lcom/google/android/material/textfield/TextInputLayout;

    sget v1, Lcom/helpshift/R$string;->hs__invalid_email_error:I

    invoke-direct {p0, v1}, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->getText(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->setError(Lcom/google/android/material/textfield/TextInputLayout;Ljava/lang/CharSequence;)V

    return-void
.end method

.method public showEmailInvalidError()V
    .locals 2

    .line 126
    iget-object v0, p0, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->emailFieldWrapper:Lcom/google/android/material/textfield/TextInputLayout;

    sget v1, Lcom/helpshift/R$string;->hs__invalid_email_error:I

    invoke-direct {p0, v1}, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->getText(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->setError(Lcom/google/android/material/textfield/TextInputLayout;Ljava/lang/CharSequence;)V

    return-void
.end method

.method public showErrorView(Lcom/helpshift/common/exception/ExceptionType;)V
    .locals 1

    .line 269
    iget-object v0, p0, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->parentView:Landroid/view/View;

    invoke-static {p1, v0}, Lcom/helpshift/support/util/SnackbarUtil;->showSnackbar(Lcom/helpshift/common/exception/ExceptionType;Landroid/view/View;)V

    return-void
.end method

.method public showImageAttachmentButton()V
    .locals 2

    .line 146
    sget-object v0, Lcom/helpshift/support/fragments/HSMenuItemType;->SCREENSHOT_ATTACHMENT:Lcom/helpshift/support/fragments/HSMenuItemType;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->changeMenuItemVisibility(Lcom/helpshift/support/fragments/HSMenuItemType;Z)V

    return-void
.end method

.method public showImageAttachmentContainer(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V
    .locals 1

    const/4 v0, -0x1

    .line 153
    invoke-static {p1, v0}, Lcom/helpshift/support/util/AttachmentUtil;->getBitmap(Ljava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object p1

    if-eqz p1, :cond_2

    .line 155
    iget-object v0, p0, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->attachmentImage:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 156
    iget-object p1, p0, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->attachmentFileName:Landroid/widget/TextView;

    const-string v0, ""

    if-nez p2, :cond_0

    move-object p2, v0

    :cond_0
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    if-eqz p3, :cond_1

    .line 159
    new-instance p1, Lcom/helpshift/support/model/AttachmentFileSize;

    invoke-virtual {p3}, Ljava/lang/Long;->longValue()J

    move-result-wide p2

    long-to-double p2, p2

    invoke-direct {p1, p2, p3}, Lcom/helpshift/support/model/AttachmentFileSize;-><init>(D)V

    invoke-virtual {p1}, Lcom/helpshift/support/model/AttachmentFileSize;->getFormattedFileSize()Ljava/lang/String;

    move-result-object v0

    .line 161
    :cond_1
    iget-object p1, p0, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->attachmentFileSize:Landroid/widget/TextView;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 162
    iget-object p1, p0, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->attachmentImage:Landroid/widget/ImageView;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 163
    iget-object p1, p0, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->attachmentClearButton:Landroid/widget/ImageButton;

    invoke-virtual {p1, p2}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 164
    iget-object p1, p0, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->attachmentContainer:Landroidx/cardview/widget/CardView;

    invoke-virtual {p1, p2}, Landroidx/cardview/widget/CardView;->setVisibility(I)V

    :cond_2
    return-void
.end method

.method public showNameEmptyError()V
    .locals 2

    .line 111
    iget-object v0, p0, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->nameFieldWrapper:Lcom/google/android/material/textfield/TextInputLayout;

    sget v1, Lcom/helpshift/R$string;->hs__username_blank_error:I

    invoke-direct {p0, v1}, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->getText(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->setError(Lcom/google/android/material/textfield/TextInputLayout;Ljava/lang/CharSequence;)V

    return-void
.end method

.method public showNameOnlySpecialCharactersError()V
    .locals 2

    .line 116
    iget-object v0, p0, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->nameFieldWrapper:Lcom/google/android/material/textfield/TextInputLayout;

    sget v1, Lcom/helpshift/R$string;->hs__username_blank_error:I

    invoke-direct {p0, v1}, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->getText(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->setError(Lcom/google/android/material/textfield/TextInputLayout;Ljava/lang/CharSequence;)V

    return-void
.end method

.method public showProfileForm()V
    .locals 2

    .line 195
    iget-object v0, p0, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->nameField:Lcom/google/android/material/textfield/TextInputEditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/material/textfield/TextInputEditText;->setVisibility(I)V

    .line 196
    iget-object v0, p0, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->emailField:Lcom/google/android/material/textfield/TextInputEditText;

    invoke-virtual {v0, v1}, Lcom/google/android/material/textfield/TextInputEditText;->setVisibility(I)V

    return-void
.end method

.method public showProgressBar()V
    .locals 2

    .line 237
    iget-object v0, p0, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->progressBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    return-void
.end method

.method public showSearchResultFragment(Ljava/util/ArrayList;)V
    .locals 1

    .line 264
    iget-object v0, p0, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->newConversationRouter:Lcom/helpshift/support/conversations/NewConversationRouter;

    invoke-interface {v0, p1}, Lcom/helpshift/support/conversations/NewConversationRouter;->showSearchResultFragment(Ljava/util/ArrayList;)V

    return-void
.end method

.method public showStartConversationButton()V
    .locals 2

    .line 212
    sget-object v0, Lcom/helpshift/support/fragments/HSMenuItemType;->START_NEW_CONVERSATION:Lcom/helpshift/support/fragments/HSMenuItemType;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/helpshift/support/conversations/NewConversationFragmentRenderer;->changeMenuItemVisibility(Lcom/helpshift/support/fragments/HSMenuItemType;Z)V

    return-void
.end method
