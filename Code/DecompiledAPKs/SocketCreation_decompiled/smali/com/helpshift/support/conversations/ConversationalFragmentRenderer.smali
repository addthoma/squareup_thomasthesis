.class public Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;
.super Lcom/helpshift/support/conversations/ConversationFragmentRenderer;
.source "ConversationalFragmentRenderer.java"

# interfaces
.implements Lcom/helpshift/conversation/activeconversation/ConversationalRenderer;


# static fields
.field private static final OPTIONS_PICKER_PEEK_HEIGHT:I = 0x8e


# instance fields
.field bottomSheetBehavior:Lcom/google/android/material/bottomsheet/BottomSheetBehavior;

.field private final conversationalFragmentRouter:Lcom/helpshift/support/conversations/ConversationalFragmentRouter;

.field lastMessageItemDecor:Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;

.field networkErrorFooter:Landroid/widget/LinearLayout;

.field pickerAdapter:Lcom/helpshift/support/conversations/picker/PickerAdapter;

.field pickerBackView:Landroid/widget/ImageView;

.field pickerBottomSheet:Lcom/helpshift/views/bottomsheet/HSBottomSheet;

.field pickerClearView:Landroid/widget/ImageView;

.field pickerCollapseView:Landroid/widget/ImageView;

.field pickerCollapsedHeader:Landroid/view/View;

.field pickerCollapsedHeaderText:Landroid/widget/TextView;

.field pickerCollapsedShadow:Landroid/view/View;

.field pickerEmptySearchResultsView:Landroid/view/View;

.field pickerExpandView:Landroid/widget/ImageView;

.field pickerExpandedHeader:Landroid/view/View;

.field pickerExpandedHeaderText:Landroid/widget/TextView;

.field pickerExpandedShadow:Landroid/view/View;

.field pickerHeaderSearchView:Landroid/widget/EditText;

.field pickerOptionsRecycler:Landroidx/recyclerview/widget/RecyclerView;

.field pickerSearchView:Landroid/widget/ImageView;

.field replyValidationFailedView:Landroid/widget/TextView;

.field skipBubbleTextView:Landroid/widget/TextView;

.field skipOutterBubble:Landroid/widget/LinearLayout;


# direct methods
.method constructor <init>(Landroid/content/Context;Landroidx/recyclerview/widget/RecyclerView;Landroid/view/View;Landroid/view/View;Lcom/helpshift/support/conversations/ConversationFragmentRouter;Landroid/view/View;Landroid/view/View;Lcom/helpshift/support/fragments/IToolbarMenuItemRenderer;Lcom/helpshift/support/conversations/ConversationalFragmentRouter;)V
    .locals 0

    .line 97
    invoke-direct/range {p0 .. p8}, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;-><init>(Landroid/content/Context;Landroidx/recyclerview/widget/RecyclerView;Landroid/view/View;Landroid/view/View;Lcom/helpshift/support/conversations/ConversationFragmentRouter;Landroid/view/View;Landroid/view/View;Lcom/helpshift/support/fragments/IToolbarMenuItemRenderer;)V

    .line 99
    sget p1, Lcom/helpshift/R$id;->skipBubbleTextView:I

    invoke-virtual {p3, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->skipBubbleTextView:Landroid/widget/TextView;

    .line 100
    sget p1, Lcom/helpshift/R$id;->skipOuterBubble:I

    invoke-virtual {p3, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->skipOutterBubble:Landroid/widget/LinearLayout;

    .line 101
    sget p1, Lcom/helpshift/R$id;->errorReplyTextView:I

    invoke-virtual {p3, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->replyValidationFailedView:Landroid/widget/TextView;

    .line 102
    sget p1, Lcom/helpshift/R$id;->networkErrorFooter:I

    invoke-virtual {p3, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->networkErrorFooter:Landroid/widget/LinearLayout;

    .line 103
    iput-object p9, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->conversationalFragmentRouter:Lcom/helpshift/support/conversations/ConversationalFragmentRouter;

    return-void
.end method

.method static synthetic access$000(Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;)V
    .locals 0

    .line 61
    invoke-direct {p0}, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->onOptionPickerCollapsed()V

    return-void
.end method

.method static synthetic access$100(Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;)V
    .locals 0

    .line 61
    invoke-direct {p0}, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->onOptionPickerExpanded()V

    return-void
.end method

.method static synthetic access$200(Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;)Lcom/helpshift/support/conversations/ConversationalFragmentRouter;
    .locals 0

    .line 61
    iget-object p0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->conversationalFragmentRouter:Lcom/helpshift/support/conversations/ConversationalFragmentRouter;

    return-object p0
.end method

.method private calculatePickerBottomOffset(Z)I
    .locals 2

    const/16 v0, 0xe

    if-eqz p1, :cond_0

    .line 412
    iget-object p1, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->parentView:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    sget v1, Lcom/helpshift/R$dimen;->activity_horizontal_margin_large:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result p1

    float-to-int p1, p1

    add-int/2addr p1, v0

    add-int/lit8 p1, p1, 0x4

    .line 418
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->parentView:Landroid/view/View;

    sget v1, Lcom/helpshift/R$id;->hs__conversation_cardview_container:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/cardview/widget/CardView;

    int-to-float p1, p1

    .line 419
    invoke-virtual {v0}, Landroidx/cardview/widget/CardView;->getCardElevation()F

    move-result v0

    add-float/2addr p1, v0

    float-to-int v0, p1

    :cond_0
    return v0
.end method

.method private createRecyclerViewLastItemDecor()V
    .locals 1

    .line 735
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->lastMessageItemDecor:Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;

    if-eqz v0, :cond_0

    return-void

    .line 740
    :cond_0
    new-instance v0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer$14;

    invoke-direct {v0, p0}, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer$14;-><init>(Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;)V

    iput-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->lastMessageItemDecor:Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;

    return-void
.end method

.method private handleSkipButtonRenderingForPicker(ZLjava/lang/String;)V
    .locals 0

    if-nez p1, :cond_0

    .line 427
    invoke-static {p2}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_0

    .line 428
    invoke-direct {p0}, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->setPickerOptionsInputSkipListener()V

    .line 429
    iget-object p1, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->skipBubbleTextView:Landroid/widget/TextView;

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 430
    invoke-virtual {p0}, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->showSkipButton()V

    goto :goto_0

    .line 433
    :cond_0
    invoke-virtual {p0}, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->hideSkipButton()V

    :goto_0
    return-void
.end method

.method private initBottomSheetCallback()V
    .locals 2

    .line 438
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerBottomSheet:Lcom/helpshift/views/bottomsheet/HSBottomSheet;

    new-instance v1, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer$4;

    invoke-direct {v1, p0}, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer$4;-><init>(Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;)V

    invoke-virtual {v0, v1}, Lcom/helpshift/views/bottomsheet/HSBottomSheet;->addBottomSheetCallback(Lcom/google/android/material/bottomsheet/BottomSheetBehavior$BottomSheetCallback;)V

    return-void
.end method

.method private initPickerViews(Ljava/lang/String;)V
    .locals 6

    .line 626
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerBottomSheet:Lcom/helpshift/views/bottomsheet/HSBottomSheet;

    invoke-virtual {v0}, Lcom/helpshift/views/bottomsheet/HSBottomSheet;->getBottomSheetBehaviour()Lcom/google/android/material/bottomsheet/BottomSheetBehavior;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->bottomSheetBehavior:Lcom/google/android/material/bottomsheet/BottomSheetBehavior;

    .line 627
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerBottomSheet:Lcom/helpshift/views/bottomsheet/HSBottomSheet;

    invoke-virtual {v0}, Lcom/helpshift/views/bottomsheet/HSBottomSheet;->getBottomSheetContentView()Landroid/view/View;

    move-result-object v0

    .line 628
    sget v1, Lcom/helpshift/R$id;->hs__picker_collapsed_shadow:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerCollapsedShadow:Landroid/view/View;

    .line 629
    sget v1, Lcom/helpshift/R$id;->hs__picker_expanded_shadow:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerExpandedShadow:Landroid/view/View;

    .line 630
    sget v1, Lcom/helpshift/R$id;->hs__optionsList:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroidx/recyclerview/widget/RecyclerView;

    iput-object v1, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerOptionsRecycler:Landroidx/recyclerview/widget/RecyclerView;

    .line 631
    iget-object v1, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerOptionsRecycler:Landroidx/recyclerview/widget/RecyclerView;

    new-instance v2, Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 633
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-direct {v2, v3, v5, v4}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    .line 632
    invoke-virtual {v1, v2}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 634
    sget v1, Lcom/helpshift/R$id;->hs__picker_action_search:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerSearchView:Landroid/widget/ImageView;

    .line 635
    sget v1, Lcom/helpshift/R$id;->hs__picker_action_clear:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerClearView:Landroid/widget/ImageView;

    .line 636
    sget v1, Lcom/helpshift/R$id;->hs__picker_action_collapse:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerCollapseView:Landroid/widget/ImageView;

    .line 637
    sget v1, Lcom/helpshift/R$id;->hs__picker_action_back:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerBackView:Landroid/widget/ImageView;

    .line 638
    sget v1, Lcom/helpshift/R$id;->hs__picker_header_search:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerHeaderSearchView:Landroid/widget/EditText;

    .line 639
    sget v1, Lcom/helpshift/R$id;->hs__expanded_picker_header_text:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerExpandedHeaderText:Landroid/widget/TextView;

    .line 640
    sget v1, Lcom/helpshift/R$id;->hs__picker_expanded_header:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerExpandedHeader:Landroid/view/View;

    .line 641
    sget v1, Lcom/helpshift/R$id;->hs__picker_collapsed_header:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerCollapsedHeader:Landroid/view/View;

    .line 642
    sget v1, Lcom/helpshift/R$id;->hs__collapsed_picker_header_text:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerCollapsedHeaderText:Landroid/widget/TextView;

    .line 643
    sget v1, Lcom/helpshift/R$id;->hs__empty_picker_view:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerEmptySearchResultsView:Landroid/view/View;

    .line 644
    sget v1, Lcom/helpshift/R$id;->hs__picker_action_expand:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerExpandView:Landroid/widget/ImageView;

    .line 646
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerExpandedHeaderText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 647
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerCollapsedHeaderText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 650
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->parentView:Landroid/view/View;

    .line 651
    invoke-virtual {v0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/helpshift/R$string;->hs__picker_options_expand_header_voice_over:I

    new-array v2, v5, [Ljava/lang/Object;

    aput-object p1, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 653
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerCollapsedHeader:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 654
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerCollapsedHeaderText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 657
    iget-object p1, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->context:Landroid/content/Context;

    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerSearchView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sget v1, Lcom/helpshift/R$attr;->hs__expandedPickerIconColor:I

    invoke-static {p1, v0, v1}, Lcom/helpshift/util/Styles;->setColorFilter(Landroid/content/Context;Landroid/graphics/drawable/Drawable;I)V

    .line 658
    iget-object p1, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->context:Landroid/content/Context;

    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerBackView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sget v1, Lcom/helpshift/R$attr;->hs__expandedPickerIconColor:I

    invoke-static {p1, v0, v1}, Lcom/helpshift/util/Styles;->setColorFilter(Landroid/content/Context;Landroid/graphics/drawable/Drawable;I)V

    .line 659
    iget-object p1, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->context:Landroid/content/Context;

    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerCollapseView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sget v1, Lcom/helpshift/R$attr;->hs__expandedPickerIconColor:I

    invoke-static {p1, v0, v1}, Lcom/helpshift/util/Styles;->setColorFilter(Landroid/content/Context;Landroid/graphics/drawable/Drawable;I)V

    .line 660
    iget-object p1, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->context:Landroid/content/Context;

    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerClearView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sget v1, Lcom/helpshift/R$attr;->hs__expandedPickerIconColor:I

    invoke-static {p1, v0, v1}, Lcom/helpshift/util/Styles;->setColorFilter(Landroid/content/Context;Landroid/graphics/drawable/Drawable;I)V

    .line 661
    iget-object p1, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->context:Landroid/content/Context;

    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerExpandView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sget v1, Lcom/helpshift/R$attr;->hs__collapsedPickerIconColor:I

    invoke-static {p1, v0, v1}, Lcom/helpshift/util/Styles;->setColorFilter(Landroid/content/Context;Landroid/graphics/drawable/Drawable;I)V

    return-void
.end method

.method private onOptionPickerCollapsed()V
    .locals 4

    .line 512
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerCollapsedShadow:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 513
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerCollapsedShadow:Landroid/view/View;

    iget-object v2, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->context:Landroid/content/Context;

    sget v3, Lcom/helpshift/R$color;->hs__color_40000000:I

    .line 514
    invoke-static {v2, v3}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    sget-object v3, Landroid/graphics/drawable/GradientDrawable$Orientation;->BOTTOM_TOP:Landroid/graphics/drawable/GradientDrawable$Orientation;

    .line 513
    invoke-static {v0, v2, v1, v3}, Lcom/helpshift/util/Styles;->setGradientBackground(Landroid/view/View;IILandroid/graphics/drawable/GradientDrawable$Orientation;)V

    .line 520
    invoke-direct {p0}, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->showPickerContent()V

    .line 524
    invoke-virtual {p0}, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->resetPickerSearchViewToNormalHeader()V

    .line 526
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerCollapsedHeader:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 527
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerExpandedHeader:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 530
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerOptionsRecycler:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->scrollToPosition(I)V

    .line 532
    invoke-direct {p0}, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->resetAccessibilityForToolbar()V

    return-void
.end method

.method private onOptionPickerExpanded()V
    .locals 5

    .line 491
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerCollapsedShadow:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 492
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerExpandedShadow:Landroid/view/View;

    iget-object v2, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->context:Landroid/content/Context;

    sget v3, Lcom/helpshift/R$color;->hs__color_40000000:I

    .line 493
    invoke-static {v2, v3}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    sget-object v3, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    const/4 v4, 0x0

    .line 492
    invoke-static {v0, v2, v4, v3}, Lcom/helpshift/util/Styles;->setGradientBackground(Landroid/view/View;IILandroid/graphics/drawable/GradientDrawable$Orientation;)V

    .line 496
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerExpandedHeader:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 497
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerCollapsedHeader:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 502
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->parentView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 503
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->parentView:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setImportantForAccessibility(I)V

    .line 505
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->conversationalFragmentRouter:Lcom/helpshift/support/conversations/ConversationalFragmentRouter;

    .line 506
    invoke-interface {v0, v1}, Lcom/helpshift/support/conversations/ConversationalFragmentRouter;->setToolbarImportanceForAccessibility(I)V

    :cond_0
    return-void
.end method

.method private registerListeners()V
    .locals 2

    .line 547
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerHeaderSearchView:Landroid/widget/EditText;

    new-instance v1, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer$5;

    invoke-direct {v1, p0}, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer$5;-><init>(Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 556
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerHeaderSearchView:Landroid/widget/EditText;

    new-instance v1, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer$6;

    invoke-direct {v1, p0}, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer$6;-><init>(Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 567
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerSearchView:Landroid/widget/ImageView;

    new-instance v1, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer$7;

    invoke-direct {v1, p0}, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer$7;-><init>(Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 582
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerBackView:Landroid/widget/ImageView;

    new-instance v1, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer$8;

    invoke-direct {v1, p0}, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer$8;-><init>(Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 589
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerClearView:Landroid/widget/ImageView;

    new-instance v1, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer$9;

    invoke-direct {v1, p0}, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer$9;-><init>(Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 597
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerCollapseView:Landroid/widget/ImageView;

    new-instance v1, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer$10;

    invoke-direct {v1, p0}, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer$10;-><init>(Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 605
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerCollapsedHeader:Landroid/view/View;

    new-instance v1, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer$11;

    invoke-direct {v1, p0}, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer$11;-><init>(Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private renderForTextInput(Lcom/helpshift/conversation/activeconversation/message/input/TextInput;)V
    .locals 4

    .line 800
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->replyField:Landroid/widget/EditText;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFocusableInTouchMode(Z)V

    .line 801
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->replyField:Landroid/widget/EditText;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 804
    iget-object v0, p1, Lcom/helpshift/conversation/activeconversation/message/input/TextInput;->inputLabel:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const/4 v2, 0x0

    if-nez v0, :cond_0

    .line 805
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->parentView:Landroid/view/View;

    sget v3, Lcom/helpshift/R$id;->replyBoxLabelLayout:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 806
    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 807
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->replyBoxView:Landroid/view/View;

    sget v3, Lcom/helpshift/R$id;->replyFieldLabel:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 808
    iget-object v3, p1, Lcom/helpshift/conversation/activeconversation/message/input/TextInput;->inputLabel:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 812
    :cond_0
    iget-object v0, p1, Lcom/helpshift/conversation/activeconversation/message/input/TextInput;->placeholder:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, ""

    goto :goto_0

    :cond_1
    iget-object v0, p1, Lcom/helpshift/conversation/activeconversation/message/input/TextInput;->placeholder:Ljava/lang/String;

    .line 813
    :goto_0
    iget-object v3, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->replyField:Landroid/widget/EditText;

    invoke-virtual {v3, v0}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    const/high16 v0, 0x20000

    .line 817
    iget v3, p1, Lcom/helpshift/conversation/activeconversation/message/input/TextInput;->keyboard:I

    if-eq v3, v1, :cond_5

    const/4 v1, 0x2

    if-eq v3, v1, :cond_4

    const/4 v1, 0x3

    if-eq v3, v1, :cond_3

    const/4 v1, 0x4

    if-eq v3, v1, :cond_2

    .line 844
    invoke-direct {p0}, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->resetReplyFieldToNormalTextInput()V

    goto :goto_1

    .line 829
    :cond_2
    invoke-virtual {p0}, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->hideKeyboard()V

    .line 834
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->replyField:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setFocusableInTouchMode(Z)V

    .line 835
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->replyField:Landroid/widget/EditText;

    new-instance v1, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer$16;

    invoke-direct {v1, p0}, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer$16;-><init>(Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    const v0, 0x22002

    goto :goto_1

    :cond_4
    const v0, 0x20021

    goto :goto_1

    :cond_5
    const v0, 0x24001

    .line 848
    :goto_1
    iget-object v1, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->replyField:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setInputType(I)V

    .line 851
    iget-boolean v0, p1, Lcom/helpshift/conversation/activeconversation/message/input/TextInput;->required:Z

    if-nez v0, :cond_6

    iget-object v0, p1, Lcom/helpshift/conversation/activeconversation/message/input/TextInput;->skipLabel:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 853
    invoke-direct {p0}, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->setTextInputSkipListener()V

    .line 854
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->skipBubbleTextView:Landroid/widget/TextView;

    iget-object p1, p1, Lcom/helpshift/conversation/activeconversation/message/input/TextInput;->skipLabel:Ljava/lang/String;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 855
    invoke-virtual {p0}, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->showSkipButton()V

    goto :goto_2

    .line 858
    :cond_6
    invoke-virtual {p0}, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->hideSkipButton()V

    .line 861
    :goto_2
    iget-object p1, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->replyBoxView:Landroid/view/View;

    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private resetAccessibilityForToolbar()V
    .locals 2

    .line 539
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->parentView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 540
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->parentView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setImportantForAccessibility(I)V

    .line 542
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->conversationalFragmentRouter:Lcom/helpshift/support/conversations/ConversationalFragmentRouter;

    invoke-interface {v0}, Lcom/helpshift/support/conversations/ConversationalFragmentRouter;->resetToolbarImportanceForAccessibility()V

    :cond_0
    return-void
.end method

.method private resetReplyFieldToNormalTextInput()V
    .locals 2

    .line 668
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->replyField:Landroid/widget/EditText;

    const v1, 0x24001

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setInputType(I)V

    .line 671
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->replyField:Landroid/widget/EditText;

    sget v1, Lcom/helpshift/R$string;->hs__chat_hint:I

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(I)V

    return-void
.end method

.method private setBottomOffset(Landroid/view/View;I)V
    .locals 4

    .line 323
    invoke-virtual {p1}, Landroid/view/View;->getPaddingLeft()I

    move-result v0

    .line 324
    invoke-virtual {p1}, Landroid/view/View;->getPaddingRight()I

    move-result v1

    .line 325
    invoke-virtual {p1}, Landroid/view/View;->getPaddingTop()I

    move-result v2

    .line 327
    iget-object v3, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->context:Landroid/content/Context;

    int-to-float p2, p2

    invoke-static {v3, p2}, Lcom/helpshift/util/Styles;->dpToPx(Landroid/content/Context;F)F

    move-result p2

    float-to-int p2, p2

    .line 328
    invoke-virtual {p1, v0, v2, v1, p2}, Landroid/view/View;->setPadding(IIII)V

    return-void
.end method

.method private setPickerOptionsInputSkipListener()V
    .locals 2

    .line 724
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->skipBubbleTextView:Landroid/widget/TextView;

    new-instance v1, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer$13;

    invoke-direct {v1, p0}, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer$13;-><init>(Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private setTextInputSkipListener()V
    .locals 2

    .line 715
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->skipBubbleTextView:Landroid/widget/TextView;

    new-instance v1, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer$12;

    invoke-direct {v1, p0}, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer$12;-><init>(Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private showEmptyPickerView()V
    .locals 2

    .line 348
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerEmptySearchResultsView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v0

    if-nez v0, :cond_0

    .line 349
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerEmptySearchResultsView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 352
    :cond_0
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerOptionsRecycler:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->isShown()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 353
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerOptionsRecycler:Landroidx/recyclerview/widget/RecyclerView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setVisibility(I)V

    :cond_1
    return-void
.end method

.method private showPickerContent()V
    .locals 2

    .line 338
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerEmptySearchResultsView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 339
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerEmptySearchResultsView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 342
    :cond_0
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerOptionsRecycler:Landroidx/recyclerview/widget/RecyclerView;

    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->isShown()Z

    move-result v0

    if-nez v0, :cond_1

    .line 343
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerOptionsRecycler:Landroidx/recyclerview/widget/RecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setVisibility(I)V

    :cond_1
    return-void
.end method


# virtual methods
.method public bridge synthetic appendMessages(II)V
    .locals 0

    .line 61
    invoke-super {p0, p1, p2}, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->appendMessages(II)V

    return-void
.end method

.method createDatePickerForReplyField()Landroid/app/DatePickerDialog;
    .locals 7

    .line 760
    new-instance v2, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer$15;

    invoke-direct {v2, p0}, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer$15;-><init>(Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;)V

    .line 773
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 775
    :try_start_0
    iget-object v1, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->replyField:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 776
    invoke-static {v1}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 777
    invoke-static {}, Lcom/helpshift/util/HelpshiftContext;->getCoreApi()Lcom/helpshift/CoreApi;

    move-result-object v3

    invoke-interface {v3}, Lcom/helpshift/CoreApi;->getLocaleProviderDM()Lcom/helpshift/localeprovider/domainmodel/LocaleProviderDM;

    move-result-object v3

    invoke-virtual {v3}, Lcom/helpshift/localeprovider/domainmodel/LocaleProviderDM;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v3

    const-string v4, "EEEE, MMMM dd, yyyy"

    .line 779
    invoke-static {v4, v3}, Lcom/helpshift/common/util/HSDateFormatSpec;->getDateFormatter(Ljava/lang/String;Ljava/util/Locale;)Lcom/helpshift/common/util/HSSimpleDateFormat;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/helpshift/common/util/HSSimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    .line 780
    invoke-virtual {v0, v1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 788
    :catch_0
    :cond_0
    new-instance v6, Landroid/app/DatePickerDialog;

    iget-object v1, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->parentView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v3, 0x1

    .line 790
    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    const/4 v4, 0x2

    .line 791
    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    const/4 v5, 0x5

    .line 792
    invoke-virtual {v0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Landroid/app/DatePickerDialog;-><init>(Landroid/content/Context;Landroid/app/DatePickerDialog$OnDateSetListener;III)V

    return-object v6
.end method

.method public destroy()V
    .locals 1

    const/4 v0, 0x1

    .line 685
    invoke-virtual {p0, v0}, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->hideListPicker(Z)V

    .line 686
    invoke-super {p0}, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->destroy()V

    return-void
.end method

.method public bridge synthetic disableSendReplyButton()V
    .locals 0

    .line 61
    invoke-super {p0}, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->disableSendReplyButton()V

    return-void
.end method

.method public bridge synthetic enableSendReplyButton()V
    .locals 0

    .line 61
    invoke-super {p0}, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->enableSendReplyButton()V

    return-void
.end method

.method public bridge synthetic getReply()Ljava/lang/String;
    .locals 1

    .line 61
    invoke-super {p0}, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->getReply()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic hideAgentTypingIndicator()V
    .locals 0

    .line 61
    invoke-super {p0}, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->hideAgentTypingIndicator()V

    return-void
.end method

.method public bridge synthetic hideConversationResolutionQuestionUI()V
    .locals 0

    .line 61
    invoke-super {p0}, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->hideConversationResolutionQuestionUI()V

    return-void
.end method

.method public bridge synthetic hideImageAttachmentButton()V
    .locals 0

    .line 61
    invoke-super {p0}, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->hideImageAttachmentButton()V

    return-void
.end method

.method public bridge synthetic hideKeyboard()V
    .locals 0

    .line 61
    invoke-super {p0}, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->hideKeyboard()V

    return-void
.end method

.method public hideListPicker(Z)V
    .locals 2

    .line 282
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->bottomSheetBehavior:Lcom/google/android/material/bottomsheet/BottomSheetBehavior;

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerBottomSheet:Lcom/helpshift/views/bottomsheet/HSBottomSheet;

    if-nez v1, :cond_0

    goto :goto_1

    :cond_0
    if-eqz p1, :cond_1

    const/4 p1, 0x1

    .line 287
    invoke-virtual {v0, p1}, Lcom/google/android/material/bottomsheet/BottomSheetBehavior;->setHideable(Z)V

    .line 288
    iget-object p1, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerBottomSheet:Lcom/helpshift/views/bottomsheet/HSBottomSheet;

    invoke-virtual {p1}, Lcom/helpshift/views/bottomsheet/HSBottomSheet;->removeAllBottomSheetCallbacks()V

    .line 289
    iget-object p1, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerBottomSheet:Lcom/helpshift/views/bottomsheet/HSBottomSheet;

    new-instance v0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer$3;

    invoke-direct {v0, p0}, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer$3;-><init>(Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;)V

    invoke-virtual {p1, v0}, Lcom/helpshift/views/bottomsheet/HSBottomSheet;->addBottomSheetCallback(Lcom/google/android/material/bottomsheet/BottomSheetBehavior$BottomSheetCallback;)V

    .line 303
    iget-object p1, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->bottomSheetBehavior:Lcom/google/android/material/bottomsheet/BottomSheetBehavior;

    const/4 v0, 0x5

    invoke-virtual {p1, v0}, Lcom/google/android/material/bottomsheet/BottomSheetBehavior;->setState(I)V

    goto :goto_0

    .line 306
    :cond_1
    invoke-virtual {p0}, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->removePickerViewFromWindow()V

    .line 309
    :goto_0
    invoke-direct {p0}, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->resetAccessibilityForToolbar()V

    .line 311
    invoke-virtual {p0}, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->hideKeyboard()V

    .line 312
    iget-object p1, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->parentView:Landroid/view/View;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->setBottomOffset(Landroid/view/View;I)V

    .line 313
    invoke-virtual {p0}, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->hideSkipButton()V

    :cond_2
    :goto_1
    return-void
.end method

.method public hideNetworkErrorFooter()V
    .locals 2

    .line 268
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->networkErrorFooter:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    return-void
.end method

.method public hidePickerClearButton()V
    .locals 2

    .line 466
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerClearView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 467
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerClearView:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public hideReplyValidationFailedError()V
    .locals 2

    .line 217
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->replyValidationFailedView:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method public bridge synthetic hideScrollJumperView()V
    .locals 0

    .line 61
    invoke-super {p0}, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->hideScrollJumperView()V

    return-void
.end method

.method public hideSendReplyUI()V
    .locals 0

    .line 710
    invoke-super {p0}, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->hideSendReplyUI()V

    .line 711
    invoke-virtual {p0}, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->hideSkipButton()V

    return-void
.end method

.method public hideSkipButton()V
    .locals 2

    .line 149
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->skipOutterBubble:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 150
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->messagesRecyclerView:Landroidx/recyclerview/widget/RecyclerView;

    iget-object v1, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->lastMessageItemDecor:Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->removeItemDecoration(Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;)V

    return-void
.end method

.method public bridge synthetic initializeMessages(Ljava/util/List;)V
    .locals 0

    .line 61
    invoke-super {p0, p1}, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->initializeMessages(Ljava/util/List;)V

    return-void
.end method

.method public bridge synthetic isReplyBoxVisible()Z
    .locals 1

    .line 61
    invoke-super {p0}, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->isReplyBoxVisible()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic launchAttachment(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 61
    invoke-super {p0, p1, p2}, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->launchAttachment(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public bridge synthetic launchScreenshotAttachment(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 61
    invoke-super {p0, p1, p2}, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->launchScreenshotAttachment(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public bridge synthetic notifyRefreshList()V
    .locals 0

    .line 61
    invoke-super {p0}, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->notifyRefreshList()V

    return-void
.end method

.method public bridge synthetic onAuthenticationFailure()V
    .locals 0

    .line 61
    invoke-super {p0}, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->onAuthenticationFailure()V

    return-void
.end method

.method public onBackPressed()Z
    .locals 2

    .line 481
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerBottomSheet:Lcom/helpshift/views/bottomsheet/HSBottomSheet;

    if-eqz v0, :cond_0

    .line 482
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->bottomSheetBehavior:Lcom/google/android/material/bottomsheet/BottomSheetBehavior;

    invoke-virtual {v0}, Lcom/google/android/material/bottomsheet/BottomSheetBehavior;->getState()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 483
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->bottomSheetBehavior:Lcom/google/android/material/bottomsheet/BottomSheetBehavior;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/material/bottomsheet/BottomSheetBehavior;->setState(I)V

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public onFocusChanged(Z)V
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x1

    .line 678
    invoke-virtual {p0, p1}, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->hideListPicker(Z)V

    :cond_0
    return-void
.end method

.method public bridge synthetic openAppReviewStore(Ljava/lang/String;)V
    .locals 0

    .line 61
    invoke-super {p0, p1}, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->openAppReviewStore(Ljava/lang/String;)V

    return-void
.end method

.method public bridge synthetic openFreshConversationScreen()V
    .locals 0

    .line 61
    invoke-super {p0}, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->openFreshConversationScreen()V

    return-void
.end method

.method public bridge synthetic removeMessages(II)V
    .locals 0

    .line 61
    invoke-super {p0, p1, p2}, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->removeMessages(II)V

    return-void
.end method

.method removePickerViewFromWindow()V
    .locals 1

    .line 317
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerBottomSheet:Lcom/helpshift/views/bottomsheet/HSBottomSheet;

    invoke-virtual {v0}, Lcom/helpshift/views/bottomsheet/HSBottomSheet;->remove()V

    const/4 v0, 0x0

    .line 318
    iput-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerBottomSheet:Lcom/helpshift/views/bottomsheet/HSBottomSheet;

    return-void
.end method

.method public bridge synthetic requestReplyFieldFocus()V
    .locals 0

    .line 61
    invoke-super {p0}, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->requestReplyFieldFocus()V

    return-void
.end method

.method resetPickerSearchViewToNormalHeader()V
    .locals 4

    .line 614
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerHeaderSearchView:Landroid/widget/EditText;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setVisibility(I)V

    .line 615
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerExpandedHeaderText:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 616
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerHeaderSearchView:Landroid/widget/EditText;

    const-string v3, ""

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 617
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerBackView:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 618
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerCollapseView:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 619
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerClearView:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 620
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerSearchView:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 621
    invoke-virtual {p0}, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->hideKeyboard()V

    .line 622
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerBottomSheet:Lcom/helpshift/views/bottomsheet/HSBottomSheet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/helpshift/views/bottomsheet/HSBottomSheet;->setDraggable(Z)V

    return-void
.end method

.method public bridge synthetic scrollToBottom()V
    .locals 0

    .line 61
    invoke-super {p0}, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->scrollToBottom()V

    return-void
.end method

.method public bridge synthetic setReply(Ljava/lang/String;)V
    .locals 0

    .line 61
    invoke-super {p0, p1}, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->setReply(Ljava/lang/String;)V

    return-void
.end method

.method public bridge synthetic setReplyboxListeners()V
    .locals 0

    .line 61
    invoke-super {p0}, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->setReplyboxListeners()V

    return-void
.end method

.method public bridge synthetic showAgentTypingIndicator()V
    .locals 0

    .line 61
    invoke-super {p0}, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->showAgentTypingIndicator()V

    return-void
.end method

.method public bridge synthetic showCSATSubmittedView()V
    .locals 0

    .line 61
    invoke-super {p0}, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->showCSATSubmittedView()V

    return-void
.end method

.method public bridge synthetic showConversationResolutionQuestionUI()V
    .locals 0

    .line 61
    invoke-super {p0}, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->showConversationResolutionQuestionUI()V

    return-void
.end method

.method public showEmptyListPickerView()V
    .locals 0

    .line 334
    invoke-direct {p0}, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->showEmptyPickerView()V

    return-void
.end method

.method public bridge synthetic showErrorView(Lcom/helpshift/common/exception/ExceptionType;)V
    .locals 0

    .line 61
    invoke-super {p0, p1}, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->showErrorView(Lcom/helpshift/common/exception/ExceptionType;)V

    return-void
.end method

.method public bridge synthetic showImageAttachmentButton()V
    .locals 0

    .line 61
    invoke-super {p0}, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->showImageAttachmentButton()V

    return-void
.end method

.method public showInput(Lcom/helpshift/conversation/activeconversation/message/input/Input;)V
    .locals 1

    if-nez p1, :cond_0

    .line 111
    invoke-direct {p0}, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->resetReplyFieldToNormalTextInput()V

    return-void

    .line 116
    :cond_0
    instance-of v0, p1, Lcom/helpshift/conversation/activeconversation/message/input/TextInput;

    if-eqz v0, :cond_1

    .line 117
    check-cast p1, Lcom/helpshift/conversation/activeconversation/message/input/TextInput;

    invoke-direct {p0, p1}, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->renderForTextInput(Lcom/helpshift/conversation/activeconversation/message/input/TextInput;)V

    goto :goto_0

    .line 119
    :cond_1
    instance-of p1, p1, Lcom/helpshift/conversation/activeconversation/message/input/OptionInput;

    if-eqz p1, :cond_2

    .line 121
    invoke-virtual {p0}, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->hideSendReplyUI()V

    .line 122
    invoke-virtual {p0}, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->hideKeyboard()V

    .line 127
    :cond_2
    :goto_0
    invoke-virtual {p0}, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->setMessagesViewBottomPadding()V

    return-void
.end method

.method public bridge synthetic showKeyboard()V
    .locals 0

    .line 61
    invoke-super {p0}, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->showKeyboard()V

    return-void
.end method

.method public showListPicker(Ljava/util/List;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/helpshift/conversation/viewmodel/OptionUIModel;",
            ">;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 360
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerBottomSheet:Lcom/helpshift/views/bottomsheet/HSBottomSheet;

    if-eqz v0, :cond_0

    return-void

    .line 365
    :cond_0
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->parentView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/helpshift/support/util/Styles;->isTablet(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    const v1, 0x3f4ccccd    # 0.8f

    goto :goto_0

    :cond_1
    const/high16 v1, 0x3f800000    # 1.0f

    .line 369
    :goto_0
    new-instance v2, Lcom/helpshift/views/bottomsheet/HSBottomSheet$Builder;

    iget-object v3, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->context:Landroid/content/Context;

    check-cast v3, Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/helpshift/views/bottomsheet/HSBottomSheet$Builder;-><init>(Landroid/view/Window;)V

    sget v3, Lcom/helpshift/R$layout;->hs__picker_layout:I

    .line 370
    invoke-virtual {v2, v3}, Lcom/helpshift/views/bottomsheet/HSBottomSheet$Builder;->contentView(I)Lcom/helpshift/views/bottomsheet/HSBottomSheet$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->messagesRecyclerView:Landroidx/recyclerview/widget/RecyclerView;

    .line 371
    invoke-virtual {v2, v3}, Lcom/helpshift/views/bottomsheet/HSBottomSheet$Builder;->referenceView(Landroid/view/View;)Lcom/helpshift/views/bottomsheet/HSBottomSheet$Builder;

    move-result-object v2

    const/4 v3, 0x1

    .line 372
    invoke-virtual {v2, v3}, Lcom/helpshift/views/bottomsheet/HSBottomSheet$Builder;->enableDimAnimation(Z)Lcom/helpshift/views/bottomsheet/HSBottomSheet$Builder;

    move-result-object v2

    .line 373
    invoke-virtual {v2, v1}, Lcom/helpshift/views/bottomsheet/HSBottomSheet$Builder;->dimOpacity(F)Lcom/helpshift/views/bottomsheet/HSBottomSheet$Builder;

    move-result-object v1

    .line 374
    invoke-virtual {v1}, Lcom/helpshift/views/bottomsheet/HSBottomSheet$Builder;->inflateAndBuild()Lcom/helpshift/views/bottomsheet/HSBottomSheet;

    move-result-object v1

    iput-object v1, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerBottomSheet:Lcom/helpshift/views/bottomsheet/HSBottomSheet;

    .line 375
    invoke-direct {p0, p2}, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->initPickerViews(Ljava/lang/String;)V

    .line 376
    iget-object p2, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->bottomSheetBehavior:Lcom/google/android/material/bottomsheet/BottomSheetBehavior;

    iget-object v1, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->context:Landroid/content/Context;

    const/high16 v2, 0x430e0000    # 142.0f

    invoke-static {v1, v2}, Lcom/helpshift/util/Styles;->dpToPx(Landroid/content/Context;F)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p2, v1}, Lcom/google/android/material/bottomsheet/BottomSheetBehavior;->setPeekHeight(I)V

    .line 377
    new-instance p2, Lcom/helpshift/support/conversations/picker/PickerAdapter;

    iget-object v1, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->conversationalFragmentRouter:Lcom/helpshift/support/conversations/ConversationalFragmentRouter;

    invoke-direct {p2, p1, v1}, Lcom/helpshift/support/conversations/picker/PickerAdapter;-><init>(Ljava/util/List;Lcom/helpshift/support/conversations/ConversationalFragmentRouter;)V

    iput-object p2, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerAdapter:Lcom/helpshift/support/conversations/picker/PickerAdapter;

    .line 378
    iget-object p1, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerOptionsRecycler:Landroidx/recyclerview/widget/RecyclerView;

    iget-object p2, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerAdapter:Lcom/helpshift/support/conversations/picker/PickerAdapter;

    invoke-virtual {p1, p2}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 381
    iget-object p1, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerCollapsedShadow:Landroid/view/View;

    iget-object p2, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->context:Landroid/content/Context;

    sget v1, Lcom/helpshift/R$color;->hs__color_40000000:I

    .line 382
    invoke-static {p2, v1}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p2

    const/4 v1, 0x0

    sget-object v2, Landroid/graphics/drawable/GradientDrawable$Orientation;->BOTTOM_TOP:Landroid/graphics/drawable/GradientDrawable$Orientation;

    .line 381
    invoke-static {p1, p2, v1, v2}, Lcom/helpshift/util/Styles;->setGradientBackground(Landroid/view/View;IILandroid/graphics/drawable/GradientDrawable$Orientation;)V

    .line 385
    invoke-virtual {p0}, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->hideSendReplyUI()V

    .line 386
    invoke-direct {p0, p3, p4}, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->handleSkipButtonRenderingForPicker(ZLjava/lang/String;)V

    .line 389
    invoke-virtual {p0}, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->hideKeyboard()V

    .line 392
    invoke-direct {p0, v0}, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->calculatePickerBottomOffset(Z)I

    move-result p1

    .line 393
    iget-object p2, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->parentView:Landroid/view/View;

    rsub-int p1, p1, 0x8e

    invoke-direct {p0, p2, p1}, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->setBottomOffset(Landroid/view/View;I)V

    .line 395
    invoke-direct {p0}, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->registerListeners()V

    .line 396
    invoke-direct {p0}, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->initBottomSheetCallback()V

    .line 399
    invoke-direct {p0}, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->showPickerContent()V

    .line 400
    iget-object p1, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerBottomSheet:Lcom/helpshift/views/bottomsheet/HSBottomSheet;

    invoke-virtual {p1}, Lcom/helpshift/views/bottomsheet/HSBottomSheet;->show()V

    return-void
.end method

.method public showNetworkErrorFooter(I)V
    .locals 9

    .line 221
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->networkErrorFooter:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 222
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->networkErrorFooter:Landroid/widget/LinearLayout;

    sget v2, Lcom/helpshift/R$id;->networkErrorFooterText:I

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Landroid/widget/TextView;

    .line 223
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->networkErrorFooter:Landroid/widget/LinearLayout;

    sget v2, Lcom/helpshift/R$id;->networkErrorFooterTryAgainText:I

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 224
    iget-object v2, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->networkErrorFooter:Landroid/widget/LinearLayout;

    sget v3, Lcom/helpshift/R$id;->networkErrorProgressBar:I

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object v8, v2

    check-cast v8, Landroid/widget/ProgressBar;

    .line 225
    iget-object v2, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->networkErrorFooter:Landroid/widget/LinearLayout;

    sget v3, Lcom/helpshift/R$id;->networkErrorIcon:I

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object v7, v2

    check-cast v7, Landroid/widget/ImageView;

    .line 227
    invoke-virtual {v7, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 228
    iget-object v2, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->context:Landroid/content/Context;

    sget v3, Lcom/helpshift/R$drawable;->hs__network_error:I

    sget v5, Lcom/helpshift/R$attr;->hs__errorTextColor:I

    invoke-static {v2, v7, v3, v5}, Lcom/helpshift/util/Styles;->setDrawable(Landroid/content/Context;Landroid/view/View;II)V

    const/16 v2, 0x8

    .line 229
    invoke-virtual {v8, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 231
    iget-object v3, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->context:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    if-eqz p1, :cond_2

    const/4 v3, 0x1

    if-eq p1, v3, :cond_1

    const/4 v3, 0x2

    if-eq p1, v3, :cond_0

    goto :goto_0

    .line 256
    :cond_0
    sget p1, Lcom/helpshift/R$string;->hs__network_reconnecting_error:I

    invoke-virtual {v5, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v4, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 257
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 258
    invoke-virtual {v7, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 259
    invoke-virtual {v8, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    .line 251
    :cond_1
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 252
    sget p1, Lcom/helpshift/R$string;->hs__no_internet_error:I

    invoke-virtual {v5, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v4, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 234
    :cond_2
    sget p1, Lcom/helpshift/R$string;->hs__network_error_pre_issue_creation:I

    invoke-virtual {v5, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v4, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 235
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 236
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, " "

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget v1, Lcom/helpshift/R$string;->hs__tap_to_retry:I

    invoke-virtual {v5, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 237
    new-instance p1, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer$2;

    move-object v2, p1

    move-object v3, p0

    move-object v6, v0

    invoke-direct/range {v2 .. v8}, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer$2;-><init>(Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;Landroid/widget/TextView;Landroid/content/res/Resources;Landroid/widget/TextView;Landroid/widget/ImageView;Landroid/widget/ProgressBar;)V

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_0
    return-void
.end method

.method public showPickerClearButton()V
    .locals 2

    .line 473
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerClearView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->isShown()Z

    move-result v0

    if-nez v0, :cond_0

    .line 474
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerClearView:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public showReplyValidationFailedError(I)V
    .locals 5

    .line 155
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->parentView:Landroid/view/View;

    .line 156
    invoke-virtual {v0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x1

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 158
    :goto_0
    iget-object v4, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->context:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    if-eq p1, v3, :cond_7

    if-eq p1, v2, :cond_5

    const/4 v2, 0x3

    if-eq p1, v2, :cond_3

    const/4 v2, 0x4

    if-eq p1, v2, :cond_1

    const-string p1, ""

    goto :goto_1

    :cond_1
    if-eqz v0, :cond_2

    .line 180
    sget p1, Lcom/helpshift/R$string;->hs__landscape_date_input_validation_error:I

    invoke-virtual {v4, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    .line 183
    :cond_2
    sget p1, Lcom/helpshift/R$string;->hs__date_input_validation_error:I

    invoke-virtual {v4, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    :cond_3
    if-eqz v0, :cond_4

    .line 171
    sget p1, Lcom/helpshift/R$string;->hs__landscape_number_input_validation_error:I

    invoke-virtual {v4, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    .line 174
    :cond_4
    sget p1, Lcom/helpshift/R$string;->hs__number_input_validation_error:I

    invoke-virtual {v4, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    :cond_5
    if-eqz v0, :cond_6

    .line 162
    sget p1, Lcom/helpshift/R$string;->hs__landscape_email_input_validation_error:I

    invoke-virtual {v4, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    .line 165
    :cond_6
    sget p1, Lcom/helpshift/R$string;->hs__email_input_validation_error:I

    invoke-virtual {v4, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    .line 188
    :cond_7
    sget p1, Lcom/helpshift/R$string;->hs__conversation_detail_error:I

    invoke-virtual {v4, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    :goto_1
    if-eqz v0, :cond_8

    .line 194
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->parentView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 196
    sget v1, Lcom/helpshift/R$string;->hs__landscape_input_validation_dialog_title:I

    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 197
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 198
    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    const p1, 0x104000a

    .line 199
    new-instance v1, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer$1;

    invoke-direct {v1, p0}, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer$1;-><init>(Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;)V

    invoke-virtual {v0, p1, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 206
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object p1

    invoke-virtual {p1}, Landroid/app/AlertDialog;->show()V

    goto :goto_2

    .line 210
    :cond_8
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->replyValidationFailedView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 211
    iget-object p1, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->replyValidationFailedView:Landroid/widget/TextView;

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_2
    return-void
.end method

.method public bridge synthetic showScrollJumperView(Z)V
    .locals 0

    .line 61
    invoke-super {p0, p1}, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->showScrollJumperView(Z)V

    return-void
.end method

.method public showSendReplyUI()V
    .locals 2

    .line 695
    invoke-super {p0}, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->showSendReplyUI()V

    .line 698
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->parentView:Landroid/view/View;

    sget v1, Lcom/helpshift/R$id;->replyBoxLabelLayout:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    .line 699
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 702
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->replyField:Landroid/widget/EditText;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFocusableInTouchMode(Z)V

    .line 703
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->replyField:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 704
    invoke-direct {p0}, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->resetReplyFieldToNormalTextInput()V

    .line 705
    invoke-virtual {p0}, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->hideSkipButton()V

    return-void
.end method

.method public showSkipButton()V
    .locals 3

    .line 134
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->parentView:Landroid/view/View;

    .line 135
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->skipBubbleTextView:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sget v2, Lcom/helpshift/R$attr;->hs__selectableOptionColor:I

    invoke-static {v0, v1, v2}, Lcom/helpshift/util/Styles;->setColorFilter(Landroid/content/Context;Landroid/graphics/drawable/Drawable;I)V

    .line 136
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->parentView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->skipOutterBubble:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    const v2, 0x1010054

    invoke-static {v0, v1, v2}, Lcom/helpshift/util/Styles;->setColorFilter(Landroid/content/Context;Landroid/graphics/drawable/Drawable;I)V

    .line 137
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->skipOutterBubble:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 142
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->messagesRecyclerView:Landroidx/recyclerview/widget/RecyclerView;

    iget-object v1, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->lastMessageItemDecor:Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->removeItemDecoration(Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;)V

    .line 143
    invoke-direct {p0}, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->createRecyclerViewLastItemDecor()V

    .line 144
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->messagesRecyclerView:Landroidx/recyclerview/widget/RecyclerView;

    iget-object v1, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->lastMessageItemDecor:Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;

    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;)V

    return-void
.end method

.method public bridge synthetic unregisterFragmentRenderer()V
    .locals 0

    .line 61
    invoke-super {p0}, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->unregisterFragmentRenderer()V

    return-void
.end method

.method public bridge synthetic updateConversationFooterState(Lcom/helpshift/conversation/activeconversation/message/ConversationFooterState;)V
    .locals 0

    .line 61
    invoke-super {p0, p1}, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->updateConversationFooterState(Lcom/helpshift/conversation/activeconversation/message/ConversationFooterState;)V

    return-void
.end method

.method public bridge synthetic updateHistoryLoadingState(Lcom/helpshift/conversation/activeconversation/message/HistoryLoadingState;)V
    .locals 0

    .line 61
    invoke-super {p0, p1}, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->updateHistoryLoadingState(Lcom/helpshift/conversation/activeconversation/message/HistoryLoadingState;)V

    return-void
.end method

.method public updateListPickerOptions(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/helpshift/conversation/viewmodel/OptionUIModel;",
            ">;)V"
        }
    .end annotation

    .line 273
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerAdapter:Lcom/helpshift/support/conversations/picker/PickerAdapter;

    if-eqz v0, :cond_0

    .line 274
    invoke-direct {p0}, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->showPickerContent()V

    .line 275
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;->pickerAdapter:Lcom/helpshift/support/conversations/picker/PickerAdapter;

    invoke-virtual {v0, p1}, Lcom/helpshift/support/conversations/picker/PickerAdapter;->dispatchUpdates(Ljava/util/List;)V

    :cond_0
    return-void
.end method

.method public bridge synthetic updateMessages(II)V
    .locals 0

    .line 61
    invoke-super {p0, p1, p2}, Lcom/helpshift/support/conversations/ConversationFragmentRenderer;->updateMessages(II)V

    return-void
.end method
