.class public Lcom/helpshift/support/conversations/ConversationalFragment;
.super Lcom/helpshift/support/conversations/ConversationFragment;
.source "ConversationalFragment.java"

# interfaces
.implements Lcom/helpshift/network/HSNetworkConnectivityReceiver$HSNetworkConnectivityListener;
.implements Lcom/helpshift/support/conversations/ConversationalFragmentRouter;


# instance fields
.field private networkConnectivityReceiver:Lcom/helpshift/network/HSNetworkConnectivityReceiver;

.field private shouldShowConversationHistory:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 32
    invoke-direct {p0}, Lcom/helpshift/support/conversations/ConversationFragment;-><init>()V

    return-void
.end method

.method public static newInstance(Landroid/os/Bundle;)Lcom/helpshift/support/conversations/ConversationalFragment;
    .locals 1

    .line 39
    new-instance v0, Lcom/helpshift/support/conversations/ConversationalFragment;

    invoke-direct {v0}, Lcom/helpshift/support/conversations/ConversationalFragment;-><init>()V

    .line 40
    invoke-virtual {v0, p0}, Lcom/helpshift/support/conversations/ConversationalFragment;->setArguments(Landroid/os/Bundle;)V

    return-object v0
.end method


# virtual methods
.method public handleOptionSelected(Lcom/helpshift/conversation/activeconversation/message/OptionInputMessageDM;Lcom/helpshift/conversation/activeconversation/message/input/OptionInput$Option;Z)V
    .locals 1

    .line 119
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragment;->conversationVM:Lcom/helpshift/conversation/viewmodel/ConversationVM;

    check-cast v0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;

    invoke-virtual {v0, p1, p2, p3}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->handleOptionSelected(Lcom/helpshift/conversation/activeconversation/message/OptionInputMessageDM;Lcom/helpshift/conversation/activeconversation/message/input/OptionInput$Option;Z)V

    return-void
.end method

.method public handleOptionSelectedForPicker(Lcom/helpshift/conversation/viewmodel/OptionUIModel;Z)V
    .locals 1

    .line 169
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragment;->conversationVM:Lcom/helpshift/conversation/viewmodel/ConversationVM;

    check-cast v0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;

    invoke-virtual {v0, p1, p2}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->handleOptionSelectedForPicker(Lcom/helpshift/conversation/viewmodel/OptionUIModel;Z)V

    return-void
.end method

.method protected inflateReplyBoxView(Landroid/view/View;)V
    .locals 1

    .line 53
    sget v0, Lcom/helpshift/R$id;->replyBoxViewStub:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/view/ViewStub;

    .line 54
    sget v0, Lcom/helpshift/R$layout;->hs__conversational_labelledreplyboxview:I

    invoke-virtual {p1, v0}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 55
    invoke-virtual {p1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    return-void
.end method

.method protected initConversationVM()V
    .locals 5

    .line 60
    invoke-static {}, Lcom/helpshift/util/HelpshiftContext;->getCoreApi()Lcom/helpshift/CoreApi;

    move-result-object v0

    iget-boolean v1, p0, Lcom/helpshift/support/conversations/ConversationalFragment;->shouldShowConversationHistory:Z

    iget-object v2, p0, Lcom/helpshift/support/conversations/ConversationalFragment;->conversationId:Ljava/lang/Long;

    iget-object v3, p0, Lcom/helpshift/support/conversations/ConversationalFragment;->renderer:Lcom/helpshift/support/conversations/ConversationFragmentRenderer;

    check-cast v3, Lcom/helpshift/conversation/activeconversation/ConversationalRenderer;

    iget-boolean v4, p0, Lcom/helpshift/support/conversations/ConversationalFragment;->retainMessageBoxOnUI:Z

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/helpshift/CoreApi;->getConversationalViewModel(ZLjava/lang/Long;Lcom/helpshift/conversation/activeconversation/ConversationalRenderer;Z)Lcom/helpshift/conversation/viewmodel/ConversationalVM;

    move-result-object v0

    iput-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragment;->conversationVM:Lcom/helpshift/conversation/viewmodel/ConversationVM;

    return-void
.end method

.method protected initRenderer(Landroidx/recyclerview/widget/RecyclerView;Landroid/view/View;Landroid/view/View;Landroid/view/View;)V
    .locals 11

    .line 70
    new-instance v10, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;

    invoke-virtual {p0}, Lcom/helpshift/support/conversations/ConversationalFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 72
    invoke-virtual {p0}, Lcom/helpshift/support/conversations/ConversationalFragment;->getView()Landroid/view/View;

    move-result-object v3

    .line 76
    invoke-virtual {p0}, Lcom/helpshift/support/conversations/ConversationalFragment;->getSupportFragment()Lcom/helpshift/support/fragments/SupportFragment;

    move-result-object v8

    move-object v0, v10

    move-object v2, p1

    move-object v4, p2

    move-object v5, p0

    move-object v6, p3

    move-object v7, p4

    move-object v9, p0

    invoke-direct/range {v0 .. v9}, Lcom/helpshift/support/conversations/ConversationalFragmentRenderer;-><init>(Landroid/content/Context;Landroidx/recyclerview/widget/RecyclerView;Landroid/view/View;Landroid/view/View;Lcom/helpshift/support/conversations/ConversationFragmentRouter;Landroid/view/View;Landroid/view/View;Lcom/helpshift/support/fragments/IToolbarMenuItemRenderer;Lcom/helpshift/support/conversations/ConversationalFragmentRouter;)V

    iput-object v10, p0, Lcom/helpshift/support/conversations/ConversationalFragment;->renderer:Lcom/helpshift/support/conversations/ConversationFragmentRenderer;

    return-void
.end method

.method public networkAvailable()V
    .locals 1

    .line 149
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragment;->conversationVM:Lcom/helpshift/conversation/viewmodel/ConversationVM;

    check-cast v0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;

    invoke-virtual {v0}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->onNetworkAvailable()V

    return-void
.end method

.method public networkUnavailable()V
    .locals 1

    .line 154
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragment;->conversationVM:Lcom/helpshift/conversation/viewmodel/ConversationVM;

    check-cast v0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;

    invoke-virtual {v0}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->onNetworkUnAvailable()V

    return-void
.end method

.method public onAdminSuggestedQuestionSelected(Lcom/helpshift/conversation/activeconversation/message/MessageDM;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 125
    invoke-virtual {p0}, Lcom/helpshift/support/conversations/ConversationalFragment;->getSupportController()Lcom/helpshift/support/controllers/SupportController;

    move-result-object v0

    new-instance v1, Lcom/helpshift/support/conversations/ConversationalFragment$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/helpshift/support/conversations/ConversationalFragment$1;-><init>(Lcom/helpshift/support/conversations/ConversationalFragment;Lcom/helpshift/conversation/activeconversation/message/MessageDM;Ljava/lang/String;)V

    invoke-virtual {v0, p2, p3, v1}, Lcom/helpshift/support/controllers/SupportController;->onAdminSuggestedQuestionSelected(Ljava/lang/String;Ljava/lang/String;Lcom/helpshift/support/fragments/SingleQuestionFragment$QuestionReadListener;)V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .line 194
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragment;->renderer:Lcom/helpshift/support/conversations/ConversationFragmentRenderer;

    check-cast v0, Lcom/helpshift/conversation/activeconversation/ConversationalRenderer;

    invoke-interface {v0}, Lcom/helpshift/conversation/activeconversation/ConversationalRenderer;->onBackPressed()Z

    move-result v0

    return v0
.end method

.method public onFocusChanged(Z)V
    .locals 1

    .line 202
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragment;->renderer:Lcom/helpshift/support/conversations/ConversationFragmentRenderer;

    if-eqz v0, :cond_0

    .line 203
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragment;->renderer:Lcom/helpshift/support/conversations/ConversationFragmentRenderer;

    check-cast v0, Lcom/helpshift/conversation/activeconversation/ConversationalRenderer;

    invoke-interface {v0, p1}, Lcom/helpshift/conversation/activeconversation/ConversationalRenderer;->onFocusChanged(Z)V

    :cond_0
    return-void
.end method

.method public onListPickerSearchQueryChange(Ljava/lang/String;)V
    .locals 1

    .line 164
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragment;->conversationVM:Lcom/helpshift/conversation/viewmodel/ConversationVM;

    check-cast v0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;

    invoke-virtual {v0, p1}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->onListPickerSearchQueryChange(Ljava/lang/String;)V

    return-void
.end method

.method public onPause()V
    .locals 2

    .line 111
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragment;->networkConnectivityReceiver:Lcom/helpshift/network/HSNetworkConnectivityReceiver;

    invoke-virtual {v0, p0}, Lcom/helpshift/network/HSNetworkConnectivityReceiver;->removeListener(Lcom/helpshift/network/HSNetworkConnectivityReceiver$HSNetworkConnectivityListener;)V

    .line 112
    invoke-virtual {p0}, Lcom/helpshift/support/conversations/ConversationalFragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/helpshift/support/conversations/ConversationalFragment;->networkConnectivityReceiver:Lcom/helpshift/network/HSNetworkConnectivityReceiver;

    invoke-virtual {v0, v1}, Landroidx/fragment/app/FragmentActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 114
    invoke-super {p0}, Lcom/helpshift/support/conversations/ConversationFragment;->onPause()V

    return-void
.end method

.method public onResume()V
    .locals 4

    .line 82
    invoke-super {p0}, Lcom/helpshift/support/conversations/ConversationFragment;->onResume()V

    .line 85
    new-instance v0, Lcom/helpshift/network/HSNetworkConnectivityReceiver;

    invoke-virtual {p0}, Lcom/helpshift/support/conversations/ConversationalFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/helpshift/network/HSNetworkConnectivityReceiver;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragment;->networkConnectivityReceiver:Lcom/helpshift/network/HSNetworkConnectivityReceiver;

    .line 86
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragment;->networkConnectivityReceiver:Lcom/helpshift/network/HSNetworkConnectivityReceiver;

    invoke-virtual {v0, p0}, Lcom/helpshift/network/HSNetworkConnectivityReceiver;->addListener(Lcom/helpshift/network/HSNetworkConnectivityReceiver$HSNetworkConnectivityListener;)V

    .line 87
    invoke-virtual {p0}, Lcom/helpshift/support/conversations/ConversationalFragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/helpshift/support/conversations/ConversationalFragment;->networkConnectivityReceiver:Lcom/helpshift/network/HSNetworkConnectivityReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroidx/fragment/app/FragmentActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 89
    invoke-static {}, Lcom/helpshift/util/HelpshiftContext;->getCoreApi()Lcom/helpshift/CoreApi;

    move-result-object v0

    invoke-interface {v0}, Lcom/helpshift/CoreApi;->getAutoRetryFailedEventDM()Lcom/helpshift/common/AutoRetryFailedEventDM;

    move-result-object v0

    invoke-virtual {v0}, Lcom/helpshift/common/AutoRetryFailedEventDM;->resetBackoff()V

    .line 92
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragment;->conversationVM:Lcom/helpshift/conversation/viewmodel/ConversationVM;

    iget-object v0, v0, Lcom/helpshift/conversation/viewmodel/ConversationVM;->viewableConversation:Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getActiveConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v0

    .line 93
    iget-object v1, v0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->serverId:Ljava/lang/String;

    .line 94
    iget-object v0, v0, Lcom/helpshift/conversation/activeconversation/ConversationDM;->preConversationServerId:Ljava/lang/String;

    .line 95
    invoke-static {v1}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    .line 97
    invoke-static {v0}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 98
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    const-string v2, "preissue_id"

    .line 99
    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    :cond_0
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragment;->conversationVM:Lcom/helpshift/conversation/viewmodel/ConversationVM;

    sget-object v2, Lcom/helpshift/analytics/AnalyticsEventType;->REPORTED_ISSUE:Lcom/helpshift/analytics/AnalyticsEventType;

    invoke-virtual {v0, v2, v1}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->pushAnalyticsEvent(Lcom/helpshift/analytics/AnalyticsEventType;Ljava/util/Map;)V

    .line 105
    :cond_1
    invoke-static {}, Lcom/helpshift/util/HelpshiftContext;->getCoreApi()Lcom/helpshift/CoreApi;

    move-result-object v0

    invoke-interface {v0}, Lcom/helpshift/CoreApi;->getAutoRetryFailedEventDM()Lcom/helpshift/common/AutoRetryFailedEventDM;

    move-result-object v0

    sget-object v1, Lcom/helpshift/common/AutoRetryFailedEventDM$EventType;->CONVERSATION:Lcom/helpshift/common/AutoRetryFailedEventDM$EventType;

    invoke-virtual {v0, v1}, Lcom/helpshift/common/AutoRetryFailedEventDM;->sendEvents(Lcom/helpshift/common/AutoRetryFailedEventDM$EventType;)V

    return-void
.end method

.method public onStartNewConversationButtonClick()V
    .locals 1

    .line 159
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragment;->conversationVM:Lcom/helpshift/conversation/viewmodel/ConversationVM;

    invoke-virtual {v0}, Lcom/helpshift/conversation/viewmodel/ConversationVM;->onNewConversationButtonClicked()V

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1

    .line 138
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragment;->renderer:Lcom/helpshift/support/conversations/ConversationFragmentRenderer;

    check-cast v0, Lcom/helpshift/conversation/activeconversation/ConversationalRenderer;

    invoke-interface {v0}, Lcom/helpshift/conversation/activeconversation/ConversationalRenderer;->hideReplyValidationFailedError()V

    .line 139
    invoke-super {p0, p1, p2, p3, p4}, Lcom/helpshift/support/conversations/ConversationFragment;->onTextChanged(Ljava/lang/CharSequence;III)V

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .line 47
    invoke-virtual {p0}, Lcom/helpshift/support/conversations/ConversationalFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "show_conv_history"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/helpshift/support/conversations/ConversationalFragment;->shouldShowConversationHistory:Z

    .line 48
    invoke-super {p0, p1, p2}, Lcom/helpshift/support/conversations/ConversationFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    return-void
.end method

.method public resetToolbarImportanceForAccessibility()V
    .locals 1

    .line 182
    invoke-virtual {p0}, Lcom/helpshift/support/conversations/ConversationalFragment;->getSupportFragment()Lcom/helpshift/support/fragments/SupportFragment;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 184
    invoke-virtual {v0}, Lcom/helpshift/support/fragments/SupportFragment;->resetToolbarImportanceForAccessibility()V

    :cond_0
    return-void
.end method

.method public retryPreIssueCreation()V
    .locals 1

    .line 144
    iget-object v0, p0, Lcom/helpshift/support/conversations/ConversationalFragment;->conversationVM:Lcom/helpshift/conversation/viewmodel/ConversationVM;

    check-cast v0, Lcom/helpshift/conversation/viewmodel/ConversationalVM;

    invoke-virtual {v0}, Lcom/helpshift/conversation/viewmodel/ConversationalVM;->createPreIssue()V

    return-void
.end method

.method public setToolbarImportanceForAccessibility(I)V
    .locals 1

    .line 174
    invoke-virtual {p0}, Lcom/helpshift/support/conversations/ConversationalFragment;->getSupportFragment()Lcom/helpshift/support/fragments/SupportFragment;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 176
    invoke-virtual {v0, p1}, Lcom/helpshift/support/fragments/SupportFragment;->setToolbarImportanceForAccessibility(I)V

    :cond_0
    return-void
.end method
