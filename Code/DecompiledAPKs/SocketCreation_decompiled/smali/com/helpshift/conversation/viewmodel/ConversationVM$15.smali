.class Lcom/helpshift/conversation/viewmodel/ConversationVM$15;
.super Lcom/helpshift/common/domain/F;
.source "ConversationVM.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/helpshift/conversation/viewmodel/ConversationVM;->onCSATSurveySubmitted(ILjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/helpshift/conversation/viewmodel/ConversationVM;

.field final synthetic val$activeConversation:Lcom/helpshift/conversation/activeconversation/ConversationDM;

.field final synthetic val$feedback:Ljava/lang/String;

.field final synthetic val$rating:I


# direct methods
.method constructor <init>(Lcom/helpshift/conversation/viewmodel/ConversationVM;ILjava/lang/String;Lcom/helpshift/conversation/activeconversation/ConversationDM;)V
    .locals 0

    .line 715
    iput-object p1, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM$15;->this$0:Lcom/helpshift/conversation/viewmodel/ConversationVM;

    iput p2, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM$15;->val$rating:I

    iput-object p3, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM$15;->val$feedback:Ljava/lang/String;

    iput-object p4, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM$15;->val$activeConversation:Lcom/helpshift/conversation/activeconversation/ConversationDM;

    invoke-direct {p0}, Lcom/helpshift/common/domain/F;-><init>()V

    return-void
.end method


# virtual methods
.method public f()V
    .locals 3

    .line 718
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Sending CSAT rating : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM$15;->val$rating:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", feedback: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM$15;->val$feedback:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Helpshift_ConvVM"

    invoke-static {v1, v0}, Lcom/helpshift/util/HSLogger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 719
    iget-object v0, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM$15;->val$activeConversation:Lcom/helpshift/conversation/activeconversation/ConversationDM;

    iget v1, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM$15;->val$rating:I

    iget-object v2, p0, Lcom/helpshift/conversation/viewmodel/ConversationVM$15;->val$feedback:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->sendCSATSurvey(ILjava/lang/String;)V

    return-void
.end method
