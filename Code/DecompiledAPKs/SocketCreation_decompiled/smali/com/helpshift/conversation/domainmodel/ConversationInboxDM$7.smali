.class Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$7;
.super Lcom/helpshift/common/domain/F;
.source "ConversationInboxDM.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->resetPreIssueConversationsForUser(Lcom/helpshift/account/domainmodel/UserDM;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

.field final synthetic val$conversation:Lcom/helpshift/conversation/activeconversation/ConversationDM;

.field final synthetic val$userDM:Lcom/helpshift/account/domainmodel/UserDM;


# direct methods
.method constructor <init>(Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;Lcom/helpshift/account/domainmodel/UserDM;Lcom/helpshift/conversation/activeconversation/ConversationDM;)V
    .locals 0

    .line 1727
    iput-object p1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$7;->this$0:Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    iput-object p2, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$7;->val$userDM:Lcom/helpshift/account/domainmodel/UserDM;

    iput-object p3, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$7;->val$conversation:Lcom/helpshift/conversation/activeconversation/ConversationDM;

    invoke-direct {p0}, Lcom/helpshift/common/domain/F;-><init>()V

    return-void
.end method


# virtual methods
.method public f()V
    .locals 5

    .line 1732
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$7;->val$userDM:Lcom/helpshift/account/domainmodel/UserDM;

    invoke-static {v0}, Lcom/helpshift/common/domain/network/NetworkDataRequestUtil;->getUserRequestData(Lcom/helpshift/account/domainmodel/UserDM;)Ljava/util/HashMap;

    move-result-object v0

    const-string v1, "state"

    .line 1733
    sget-object v2, Lcom/helpshift/conversation/dto/IssueState;->REJECTED:Lcom/helpshift/conversation/dto/IssueState;

    invoke-virtual {v2}, Lcom/helpshift/conversation/dto/IssueState;->getValue()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1736
    new-instance v1, Lcom/helpshift/common/domain/network/PUTNetwork;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "/preissues/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$7;->val$conversation:Lcom/helpshift/conversation/activeconversation/ConversationDM;

    iget-object v3, v3, Lcom/helpshift/conversation/activeconversation/ConversationDM;->preConversationServerId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$7;->this$0:Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    iget-object v3, v3, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->domain:Lcom/helpshift/common/domain/Domain;

    iget-object v4, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$7;->this$0:Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    iget-object v4, v4, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-direct {v1, v2, v3, v4}, Lcom/helpshift/common/domain/network/PUTNetwork;-><init>(Ljava/lang/String;Lcom/helpshift/common/domain/Domain;Lcom/helpshift/common/platform/Platform;)V

    .line 1738
    new-instance v2, Lcom/helpshift/common/domain/network/TSCorrectedNetwork;

    iget-object v3, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$7;->this$0:Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    iget-object v3, v3, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->platform:Lcom/helpshift/common/platform/Platform;

    invoke-direct {v2, v1, v3}, Lcom/helpshift/common/domain/network/TSCorrectedNetwork;-><init>(Lcom/helpshift/common/domain/network/Network;Lcom/helpshift/common/platform/Platform;)V

    .line 1739
    new-instance v1, Lcom/helpshift/common/domain/network/GuardOKNetwork;

    invoke-direct {v1, v2}, Lcom/helpshift/common/domain/network/GuardOKNetwork;-><init>(Lcom/helpshift/common/domain/network/Network;)V

    .line 1742
    new-instance v2, Lcom/helpshift/common/platform/network/RequestData;

    invoke-direct {v2, v0}, Lcom/helpshift/common/platform/network/RequestData;-><init>(Ljava/util/Map;)V

    .line 1743
    invoke-interface {v1, v2}, Lcom/helpshift/common/domain/network/Network;->makeRequest(Lcom/helpshift/common/platform/network/RequestData;)Lcom/helpshift/common/platform/network/Response;

    .line 1746
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$7;->this$0:Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;

    iget-object v1, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$7;->val$conversation:Lcom/helpshift/conversation/activeconversation/ConversationDM;

    iget-object v1, v1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->localId:Ljava/lang/Long;

    invoke-static {v0, v1}, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;->access$000(Lcom/helpshift/conversation/domainmodel/ConversationInboxDM;Ljava/lang/Long;)Lcom/helpshift/conversation/activeconversation/ViewableConversation;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1750
    iget-object v0, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$7;->val$conversation:Lcom/helpshift/conversation/activeconversation/ConversationDM;

    goto :goto_0

    .line 1754
    :cond_0
    invoke-virtual {v0}, Lcom/helpshift/conversation/activeconversation/ViewableConversation;->getActiveConversation()Lcom/helpshift/conversation/activeconversation/ConversationDM;

    move-result-object v0

    .line 1756
    :goto_0
    sget-object v1, Lcom/helpshift/conversation/dto/IssueState;->REJECTED:Lcom/helpshift/conversation/dto/IssueState;

    invoke-virtual {v0, v1}, Lcom/helpshift/conversation/activeconversation/ConversationDM;->updateIssueStatus(Lcom/helpshift/conversation/dto/IssueState;)V
    :try_end_0
    .catch Lcom/helpshift/common/exception/RootAPIException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    .line 1759
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error resetting preissue : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/helpshift/conversation/domainmodel/ConversationInboxDM$7;->val$conversation:Lcom/helpshift/conversation/activeconversation/ConversationDM;

    iget-object v2, v2, Lcom/helpshift/conversation/activeconversation/ConversationDM;->preConversationServerId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Helpshift_ConvInboxDM"

    invoke-static {v2, v1, v0}, Lcom/helpshift/util/HSLogger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1760
    throw v0
.end method
