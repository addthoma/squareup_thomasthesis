.class Lcom/helpshift/conversation/activeconversation/ConversationDM$8;
.super Lcom/helpshift/common/domain/F;
.source "ConversationDM.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/helpshift/conversation/activeconversation/ConversationDM;->sendMessageWithAutoRetry(Lcom/helpshift/common/domain/F;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/helpshift/conversation/activeconversation/ConversationDM;

.field final synthetic val$f:Lcom/helpshift/common/domain/F;


# direct methods
.method constructor <init>(Lcom/helpshift/conversation/activeconversation/ConversationDM;Lcom/helpshift/common/domain/F;)V
    .locals 0

    .line 1367
    iput-object p1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM$8;->this$0:Lcom/helpshift/conversation/activeconversation/ConversationDM;

    iput-object p2, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM$8;->val$f:Lcom/helpshift/common/domain/F;

    invoke-direct {p0}, Lcom/helpshift/common/domain/F;-><init>()V

    return-void
.end method


# virtual methods
.method public f()V
    .locals 4

    .line 1371
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM$8;->val$f:Lcom/helpshift/common/domain/F;

    invoke-virtual {v0}, Lcom/helpshift/common/domain/F;->f()V
    :try_end_0
    .catch Lcom/helpshift/common/exception/RootAPIException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 1374
    iget-object v1, v0, Lcom/helpshift/common/exception/RootAPIException;->exceptionType:Lcom/helpshift/common/exception/ExceptionType;

    .line 1375
    sget-object v2, Lcom/helpshift/common/exception/NetworkException;->NON_RETRIABLE:Lcom/helpshift/common/exception/NetworkException;

    if-eq v1, v2, :cond_1

    sget-object v2, Lcom/helpshift/common/exception/NetworkException;->USER_PRE_CONDITION_FAILED:Lcom/helpshift/common/exception/NetworkException;

    if-ne v1, v2, :cond_0

    goto :goto_0

    .line 1376
    :cond_0
    iget-object v1, p0, Lcom/helpshift/conversation/activeconversation/ConversationDM$8;->this$0:Lcom/helpshift/conversation/activeconversation/ConversationDM;

    iget-object v1, v1, Lcom/helpshift/conversation/activeconversation/ConversationDM;->domain:Lcom/helpshift/common/domain/Domain;

    invoke-virtual {v1}, Lcom/helpshift/common/domain/Domain;->getAutoRetryFailedEventDM()Lcom/helpshift/common/AutoRetryFailedEventDM;

    move-result-object v1

    sget-object v2, Lcom/helpshift/common/AutoRetryFailedEventDM$EventType;->CONVERSATION:Lcom/helpshift/common/AutoRetryFailedEventDM$EventType;

    .line 1377
    invoke-virtual {v0}, Lcom/helpshift/common/exception/RootAPIException;->getServerStatusCode()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/helpshift/common/AutoRetryFailedEventDM;->scheduleRetryTaskForEventType(Lcom/helpshift/common/AutoRetryFailedEventDM$EventType;I)V

    .line 1378
    throw v0

    :cond_1
    :goto_0
    return-void
.end method
