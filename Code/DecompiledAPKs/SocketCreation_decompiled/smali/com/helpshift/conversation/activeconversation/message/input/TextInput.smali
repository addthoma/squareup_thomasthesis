.class public Lcom/helpshift/conversation/activeconversation/message/input/TextInput;
.super Lcom/helpshift/conversation/activeconversation/message/input/Input;
.source "TextInput.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/helpshift/conversation/activeconversation/message/input/TextInput$Keyboard;
    }
.end annotation


# instance fields
.field private domain:Lcom/helpshift/common/domain/Domain;

.field public final keyboard:I

.field public final placeholder:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0

    .line 19
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/helpshift/conversation/activeconversation/message/input/Input;-><init>(Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V

    .line 20
    iput-object p5, p0, Lcom/helpshift/conversation/activeconversation/message/input/TextInput;->placeholder:Ljava/lang/String;

    .line 21
    iput p6, p0, Lcom/helpshift/conversation/activeconversation/message/input/TextInput;->keyboard:I

    return-void
.end method


# virtual methods
.method public setDependencies(Lcom/helpshift/common/domain/Domain;)V
    .locals 0

    .line 25
    iput-object p1, p0, Lcom/helpshift/conversation/activeconversation/message/input/TextInput;->domain:Lcom/helpshift/common/domain/Domain;

    return-void
.end method

.method public validate(Ljava/lang/String;)Z
    .locals 3

    .line 36
    iget v0, p0, Lcom/helpshift/conversation/activeconversation/message/input/TextInput;->keyboard:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    const/4 v2, 0x1

    if-eq v0, v1, :cond_0

    return v2

    .line 46
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/helpshift/conversation/activeconversation/message/input/TextInput;->domain:Lcom/helpshift/common/domain/Domain;

    invoke-virtual {v0}, Lcom/helpshift/common/domain/Domain;->getLocaleProviderDM()Lcom/helpshift/localeprovider/domainmodel/LocaleProviderDM;

    move-result-object v0

    invoke-virtual {v0}, Lcom/helpshift/localeprovider/domainmodel/LocaleProviderDM;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v0

    const-string v1, "EEEE, MMMM dd, yyyy"

    .line 48
    invoke-static {v1, v0}, Lcom/helpshift/common/util/HSDateFormatSpec;->getDateFormatter(Ljava/lang/String;Ljava/util/Locale;)Lcom/helpshift/common/util/HSSimpleDateFormat;

    move-result-object v0

    .line 49
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/helpshift/common/util/HSSimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    return v2

    :catch_0
    const/4 p1, 0x0

    return p1

    .line 40
    :cond_1
    invoke-static {p1}, Lcom/helpshift/util/HSPattern;->isPositiveNumber(Ljava/lang/String;)Z

    move-result p1

    return p1

    .line 38
    :cond_2
    invoke-static {p1}, Lcom/helpshift/util/HSPattern;->isValidEmail(Ljava/lang/String;)Z

    move-result p1

    return p1
.end method
