.class public interface abstract Lcom/helpshift/conversation/activeconversation/ConversationRenderer;
.super Ljava/lang/Object;
.source "ConversationRenderer.java"


# virtual methods
.method public abstract appendMessages(II)V
.end method

.method public abstract destroy()V
.end method

.method public abstract disableSendReplyButton()V
.end method

.method public abstract enableSendReplyButton()V
.end method

.method public abstract getReply()Ljava/lang/String;
.end method

.method public abstract hideAgentTypingIndicator()V
.end method

.method public abstract hideConversationResolutionQuestionUI()V
.end method

.method public abstract hideImageAttachmentButton()V
.end method

.method public abstract hideKeyboard()V
.end method

.method public abstract hideScrollJumperView()V
.end method

.method public abstract hideSendReplyUI()V
.end method

.method public abstract initializeMessages(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/helpshift/conversation/activeconversation/message/MessageDM;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract isReplyBoxVisible()Z
.end method

.method public abstract launchAttachment(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract launchScreenshotAttachment(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract notifyRefreshList()V
.end method

.method public abstract onAuthenticationFailure()V
.end method

.method public abstract openAppReviewStore(Ljava/lang/String;)V
.end method

.method public abstract openFreshConversationScreen()V
.end method

.method public abstract removeMessages(II)V
.end method

.method public abstract requestReplyFieldFocus()V
.end method

.method public abstract scrollToBottom()V
.end method

.method public abstract setReply(Ljava/lang/String;)V
.end method

.method public abstract setReplyboxListeners()V
.end method

.method public abstract showAgentTypingIndicator()V
.end method

.method public abstract showCSATSubmittedView()V
.end method

.method public abstract showConversationResolutionQuestionUI()V
.end method

.method public abstract showErrorView(Lcom/helpshift/common/exception/ExceptionType;)V
.end method

.method public abstract showImageAttachmentButton()V
.end method

.method public abstract showKeyboard()V
.end method

.method public abstract showScrollJumperView(Z)V
.end method

.method public abstract showSendReplyUI()V
.end method

.method public abstract updateConversationFooterState(Lcom/helpshift/conversation/activeconversation/message/ConversationFooterState;)V
.end method

.method public abstract updateHistoryLoadingState(Lcom/helpshift/conversation/activeconversation/message/HistoryLoadingState;)V
.end method

.method public abstract updateMessages(II)V
.end method
