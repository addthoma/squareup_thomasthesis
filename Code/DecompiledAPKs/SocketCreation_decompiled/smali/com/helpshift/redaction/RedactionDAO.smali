.class public interface abstract Lcom/helpshift/redaction/RedactionDAO;
.super Ljava/lang/Object;
.source "RedactionDAO.java"


# virtual methods
.method public abstract deleteRedactionDetail(J)V
.end method

.method public abstract getRedactionDetail(J)Lcom/helpshift/redaction/RedactionDetail;
.end method

.method public abstract insertRedactionDetail(Lcom/helpshift/redaction/RedactionDetail;)V
.end method

.method public abstract updateRedactionRedail(Lcom/helpshift/redaction/RedactionDetail;)V
.end method

.method public abstract updateRedactionState(JLcom/helpshift/redaction/RedactionState;)V
.end method
