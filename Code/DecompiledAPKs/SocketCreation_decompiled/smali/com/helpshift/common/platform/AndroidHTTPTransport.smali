.class public Lcom/helpshift/common/platform/AndroidHTTPTransport;
.super Ljava/lang/Object;
.source "AndroidHTTPTransport.java"

# interfaces
.implements Lcom/helpshift/common/platform/network/HTTPTransport;


# static fields
.field private static final TAG:Ljava/lang/String; = "Helpshift_HTTPTrnsport"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private closeHelpshiftSSLSocketFactorySockets(Ljavax/net/ssl/HttpsURLConnection;)V
    .locals 2

    .line 213
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-gt v0, v1, :cond_0

    if-eqz p1, :cond_0

    .line 216
    invoke-virtual {p1}, Ljavax/net/ssl/HttpsURLConnection;->getSSLSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object p1

    .line 217
    instance-of v0, p1, Lcom/helpshift/android/commons/downloader/HelpshiftSSLSocketFactory;

    if-eqz v0, :cond_0

    .line 218
    check-cast p1, Lcom/helpshift/android/commons/downloader/HelpshiftSSLSocketFactory;

    .line 219
    invoke-virtual {p1}, Lcom/helpshift/android/commons/downloader/HelpshiftSSLSocketFactory;->closeSockets()V

    :cond_0
    return-void
.end method

.method private fixSSLSocketProtocols(Ljavax/net/ssl/HttpsURLConnection;)V
    .locals 4

    .line 194
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-gt v0, v1, :cond_0

    .line 198
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const-string v1, "TLSv1.2"

    .line 199
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 202
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const-string v2, "SSLv3"

    .line 203
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 205
    invoke-virtual {p1}, Ljavax/net/ssl/HttpsURLConnection;->getSSLSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v2

    .line 206
    new-instance v3, Lcom/helpshift/android/commons/downloader/HelpshiftSSLSocketFactory;

    invoke-direct {v3, v2, v0, v1}, Lcom/helpshift/android/commons/downloader/HelpshiftSSLSocketFactory;-><init>(Ljavax/net/ssl/SSLSocketFactory;Ljava/util/List;Ljava/util/List;)V

    .line 207
    invoke-virtual {p1, v3}, Ljavax/net/ssl/HttpsURLConnection;->setSSLSocketFactory(Ljavax/net/ssl/SSLSocketFactory;)V

    :cond_0
    return-void
.end method

.method private isInvalidKeyForHeader(Ljava/lang/String;)Z
    .locals 1

    const-string v0, "screenshot"

    .line 225
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "originalFileName"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method private makeNetworkRequest(Lcom/helpshift/common/platform/network/Request;)Lcom/helpshift/common/platform/network/Response;
    .locals 13

    const-string v0, "Network error"

    const/4 v1, 0x0

    :try_start_0
    const-string v2, "https://"

    .line 62
    sget-object v3, Lcom/helpshift/common/domain/network/NetworkConstants;->scheme:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 63
    new-instance v2, Ljava/net/URL;

    iget-object v3, p1, Lcom/helpshift/common/platform/network/Request;->url:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v2

    check-cast v2, Ljavax/net/ssl/HttpsURLConnection;
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_13
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_12
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_11
    .catch Ljavax/net/ssl/SSLPeerUnverifiedException; {:try_start_0 .. :try_end_0} :catch_10
    .catch Ljavax/net/ssl/SSLHandshakeException; {:try_start_0 .. :try_end_0} :catch_f
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_e
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 64
    :try_start_1
    move-object v3, v2

    check-cast v3, Ljavax/net/ssl/HttpsURLConnection;

    invoke-direct {p0, v3}, Lcom/helpshift/common/platform/AndroidHTTPTransport;->fixSSLSocketProtocols(Ljavax/net/ssl/HttpsURLConnection;)V
    :try_end_1
    .catch Ljava/net/UnknownHostException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/net/SocketException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljavax/net/ssl/SSLPeerUnverifiedException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljavax/net/ssl/SSLHandshakeException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v3

    move-object v4, v1

    goto/16 :goto_11

    :catch_0
    move-exception v3

    move-object v4, v1

    move-object v1, v2

    move-object v2, v4

    goto/16 :goto_b

    :catch_1
    move-exception v3

    move-object v4, v1

    move-object v1, v2

    move-object v2, v4

    goto/16 :goto_c

    :catch_2
    move-exception v3

    move-object v4, v1

    move-object v1, v2

    move-object v2, v4

    goto/16 :goto_d

    :catch_3
    move-exception v3

    goto :goto_0

    :catch_4
    move-exception v3

    :goto_0
    move-object v4, v1

    move-object v1, v2

    move-object v2, v4

    goto/16 :goto_f

    :catch_5
    move-exception v3

    move-object v4, v1

    move-object v1, v2

    move-object v2, v4

    goto/16 :goto_10

    .line 67
    :cond_0
    :try_start_2
    new-instance v2, Ljava/net/URL;

    iget-object v3, p1, Lcom/helpshift/common/platform/network/Request;->url:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v2

    check-cast v2, Ljava/net/HttpURLConnection;
    :try_end_2
    .catch Ljava/net/UnknownHostException; {:try_start_2 .. :try_end_2} :catch_13
    .catch Ljava/net/SocketException; {:try_start_2 .. :try_end_2} :catch_12
    .catch Ljava/lang/SecurityException; {:try_start_2 .. :try_end_2} :catch_11
    .catch Ljavax/net/ssl/SSLPeerUnverifiedException; {:try_start_2 .. :try_end_2} :catch_10
    .catch Ljavax/net/ssl/SSLHandshakeException; {:try_start_2 .. :try_end_2} :catch_f
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_e
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 70
    :goto_1
    :try_start_3
    iget-object v3, p1, Lcom/helpshift/common/platform/network/Request;->method:Lcom/helpshift/common/platform/network/Method;

    invoke-virtual {v3}, Lcom/helpshift/common/platform/network/Method;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 71
    iget v3, p1, Lcom/helpshift/common/platform/network/Request;->timeout:I

    invoke-virtual {v2, v3}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 72
    iget-object v3, p1, Lcom/helpshift/common/platform/network/Request;->headers:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/helpshift/common/platform/network/KeyValuePair;

    .line 73
    iget-object v5, v4, Lcom/helpshift/common/platform/network/KeyValuePair;->key:Ljava/lang/String;

    iget-object v4, v4, Lcom/helpshift/common/platform/network/KeyValuePair;->value:Ljava/lang/String;

    invoke-virtual {v2, v5, v4}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 76
    :cond_1
    iget-object v3, p1, Lcom/helpshift/common/platform/network/Request;->method:Lcom/helpshift/common/platform/network/Method;

    sget-object v4, Lcom/helpshift/common/platform/network/Method;->POST:Lcom/helpshift/common/platform/network/Method;

    if-eq v3, v4, :cond_3

    iget-object v3, p1, Lcom/helpshift/common/platform/network/Request;->method:Lcom/helpshift/common/platform/network/Method;

    sget-object v4, Lcom/helpshift/common/platform/network/Method;->PUT:Lcom/helpshift/common/platform/network/Method;

    if-ne v3, v4, :cond_2

    goto :goto_3

    :cond_2
    move-object v4, v1

    goto :goto_5

    .line 79
    :cond_3
    :goto_3
    iget-object v3, p1, Lcom/helpshift/common/platform/network/Request;->method:Lcom/helpshift/common/platform/network/Method;

    sget-object v4, Lcom/helpshift/common/platform/network/Method;->PUT:Lcom/helpshift/common/platform/network/Method;

    if-ne v3, v4, :cond_4

    .line 80
    const-class v3, Lcom/helpshift/common/platform/network/PUTRequest;

    invoke-virtual {v3, p1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/helpshift/common/platform/network/PUTRequest;

    iget-object v3, v3, Lcom/helpshift/common/platform/network/PUTRequest;->query:Ljava/lang/String;

    goto :goto_4

    .line 83
    :cond_4
    const-class v3, Lcom/helpshift/common/platform/network/POSTRequest;

    invoke-virtual {v3, p1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/helpshift/common/platform/network/POSTRequest;

    iget-object v3, v3, Lcom/helpshift/common/platform/network/POSTRequest;->query:Ljava/lang/String;

    :goto_4
    const/4 v4, 0x1

    .line 86
    invoke-virtual {v2, v4}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 87
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v4
    :try_end_3
    .catch Ljava/net/UnknownHostException; {:try_start_3 .. :try_end_3} :catch_5
    .catch Ljava/net/SocketException; {:try_start_3 .. :try_end_3} :catch_4
    .catch Ljava/lang/SecurityException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljavax/net/ssl/SSLPeerUnverifiedException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljavax/net/ssl/SSLHandshakeException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 88
    :try_start_4
    new-instance v5, Ljava/io/BufferedWriter;

    new-instance v6, Ljava/io/OutputStreamWriter;

    const-string v7, "UTF-8"

    invoke-direct {v6, v4, v7}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/lang/String;)V

    invoke-direct {v5, v6}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    .line 89
    invoke-virtual {v5, v3}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    .line 90
    invoke-virtual {v5}, Ljava/io/BufferedWriter;->flush()V

    .line 91
    invoke-virtual {v5}, Ljava/io/BufferedWriter;->close()V

    .line 92
    invoke-virtual {v4}, Ljava/io/OutputStream;->flush()V

    .line 95
    :goto_5
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v3

    .line 96
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 97
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getHeaderFields()Ljava/util/Map;

    move-result-object v6

    .line 98
    invoke-interface {v6}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_5
    :goto_6
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    const/4 v9, 0x0

    if-eqz v8, :cond_6

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 99
    invoke-static {v8}, Lcom/helpshift/common/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_5

    .line 100
    new-instance v10, Lcom/helpshift/common/platform/network/KeyValuePair;

    invoke-interface {v6, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/util/List;

    invoke-interface {v11, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-direct {v10, v8, v9}, Lcom/helpshift/common/platform/network/KeyValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_6

    :cond_6
    const/16 v7, 0xc8

    if-lt v3, v7, :cond_b

    const/16 v7, 0x12c

    if-ge v3, v7, :cond_b

    .line 105
    new-instance v7, Ljava/io/BufferedInputStream;

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    const-string v8, "Content-Encoding"

    .line 107
    invoke-interface {v6, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/List;

    if-eqz v6, :cond_7

    .line 108
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v8

    if-lez v8, :cond_7

    .line 109
    invoke-interface {v6, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    const-string v8, "gzip"

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 110
    new-instance v6, Ljava/util/zip/GZIPInputStream;

    invoke-direct {v6, v7}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V

    goto :goto_7

    :cond_7
    move-object v6, v7

    .line 112
    :goto_7
    invoke-direct {p0, v6}, Lcom/helpshift/common/platform/AndroidHTTPTransport;->readInputStream(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v7

    .line 113
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V

    .line 114
    new-instance v6, Lcom/helpshift/common/platform/network/Response;

    invoke-direct {v6, v3, v7, v5}, Lcom/helpshift/common/platform/network/Response;-><init>(ILjava/lang/String;Ljava/util/List;)V
    :try_end_4
    .catch Ljava/net/UnknownHostException; {:try_start_4 .. :try_end_4} :catch_d
    .catch Ljava/net/SocketException; {:try_start_4 .. :try_end_4} :catch_c
    .catch Ljava/lang/SecurityException; {:try_start_4 .. :try_end_4} :catch_b
    .catch Ljavax/net/ssl/SSLPeerUnverifiedException; {:try_start_4 .. :try_end_4} :catch_a
    .catch Ljavax/net/ssl/SSLHandshakeException; {:try_start_4 .. :try_end_4} :catch_9
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_8
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    if-eqz v4, :cond_8

    .line 158
    :try_start_5
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V

    .line 161
    :cond_8
    instance-of v1, v2, Ljavax/net/ssl/HttpsURLConnection;

    if-eqz v1, :cond_9

    .line 162
    move-object v1, v2

    check-cast v1, Ljavax/net/ssl/HttpsURLConnection;

    invoke-direct {p0, v1}, Lcom/helpshift/common/platform/AndroidHTTPTransport;->closeHelpshiftSSLSocketFactorySockets(Ljavax/net/ssl/HttpsURLConnection;)V

    :cond_9
    if-eqz v2, :cond_a

    .line 165
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_6

    :cond_a
    return-object v6

    :catch_6
    move-exception v1

    .line 169
    sget-object v2, Lcom/helpshift/common/exception/NetworkException;->GENERIC:Lcom/helpshift/common/exception/NetworkException;

    .line 170
    iget-object p1, p1, Lcom/helpshift/common/platform/network/Request;->url:Ljava/lang/String;

    iput-object p1, v2, Lcom/helpshift/common/exception/NetworkException;->route:Ljava/lang/String;

    .line 171
    invoke-static {v1, v2, v0}, Lcom/helpshift/common/exception/RootAPIException;->wrap(Ljava/lang/Exception;Lcom/helpshift/common/exception/ExceptionType;Ljava/lang/String;)Lcom/helpshift/common/exception/RootAPIException;

    move-result-object p1

    throw p1

    :cond_b
    :try_start_6
    const-string v6, "Helpshift_HTTPTrnsport"

    .line 117
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Api : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v8, p1, Lcom/helpshift/common/platform/network/Request;->url:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, " \t Status : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v8, "\t Thread : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 118
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 117
    invoke-static {v6, v7}, Lcom/helpshift/util/HSLogger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getErrorStream()Ljava/io/InputStream;

    move-result-object v1

    .line 121
    invoke-direct {p0, v1}, Lcom/helpshift/common/platform/AndroidHTTPTransport;->readInputStream(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v6

    .line 123
    new-instance v7, Lcom/helpshift/common/platform/network/Response;

    invoke-direct {v7, v3, v6, v5}, Lcom/helpshift/common/platform/network/Response;-><init>(ILjava/lang/String;Ljava/util/List;)V
    :try_end_6
    .catch Ljava/net/UnknownHostException; {:try_start_6 .. :try_end_6} :catch_d
    .catch Ljava/net/SocketException; {:try_start_6 .. :try_end_6} :catch_c
    .catch Ljava/lang/SecurityException; {:try_start_6 .. :try_end_6} :catch_b
    .catch Ljavax/net/ssl/SSLPeerUnverifiedException; {:try_start_6 .. :try_end_6} :catch_a
    .catch Ljavax/net/ssl/SSLHandshakeException; {:try_start_6 .. :try_end_6} :catch_9
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_8
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    if-eqz v1, :cond_c

    .line 154
    :try_start_7
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    goto :goto_8

    :catch_7
    move-exception v1

    goto :goto_9

    :cond_c
    :goto_8
    if-eqz v4, :cond_d

    .line 158
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V

    .line 161
    :cond_d
    instance-of v1, v2, Ljavax/net/ssl/HttpsURLConnection;

    if-eqz v1, :cond_e

    .line 162
    move-object v1, v2

    check-cast v1, Ljavax/net/ssl/HttpsURLConnection;

    invoke-direct {p0, v1}, Lcom/helpshift/common/platform/AndroidHTTPTransport;->closeHelpshiftSSLSocketFactorySockets(Ljavax/net/ssl/HttpsURLConnection;)V

    :cond_e
    if-eqz v2, :cond_f

    .line 165
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_7

    :cond_f
    return-object v7

    .line 169
    :goto_9
    sget-object v2, Lcom/helpshift/common/exception/NetworkException;->GENERIC:Lcom/helpshift/common/exception/NetworkException;

    .line 170
    iget-object p1, p1, Lcom/helpshift/common/platform/network/Request;->url:Ljava/lang/String;

    iput-object p1, v2, Lcom/helpshift/common/exception/NetworkException;->route:Ljava/lang/String;

    .line 171
    invoke-static {v1, v2, v0}, Lcom/helpshift/common/exception/RootAPIException;->wrap(Ljava/lang/Exception;Lcom/helpshift/common/exception/ExceptionType;Ljava/lang/String;)Lcom/helpshift/common/exception/RootAPIException;

    move-result-object p1

    throw p1

    :catchall_1
    move-exception v3

    goto/16 :goto_11

    :catch_8
    move-exception v3

    move-object v12, v2

    move-object v2, v1

    move-object v1, v12

    goto :goto_b

    :catch_9
    move-exception v3

    move-object v12, v2

    move-object v2, v1

    move-object v1, v12

    goto :goto_c

    :catch_a
    move-exception v3

    move-object v12, v2

    move-object v2, v1

    move-object v1, v12

    goto :goto_d

    :catch_b
    move-exception v3

    goto :goto_a

    :catch_c
    move-exception v3

    :goto_a
    move-object v12, v2

    move-object v2, v1

    move-object v1, v12

    goto :goto_f

    :catch_d
    move-exception v3

    move-object v12, v2

    move-object v2, v1

    move-object v1, v12

    goto :goto_10

    :catchall_2
    move-exception v3

    move-object v2, v1

    move-object v4, v2

    goto :goto_11

    :catch_e
    move-exception v3

    move-object v2, v1

    move-object v4, v2

    .line 147
    :goto_b
    :try_start_8
    sget-object v5, Lcom/helpshift/common/exception/NetworkException;->GENERIC:Lcom/helpshift/common/exception/NetworkException;

    .line 148
    iget-object v6, p1, Lcom/helpshift/common/platform/network/Request;->url:Ljava/lang/String;

    iput-object v6, v5, Lcom/helpshift/common/exception/NetworkException;->route:Ljava/lang/String;

    .line 149
    invoke-static {v3, v5, v0}, Lcom/helpshift/common/exception/RootAPIException;->wrap(Ljava/lang/Exception;Lcom/helpshift/common/exception/ExceptionType;Ljava/lang/String;)Lcom/helpshift/common/exception/RootAPIException;

    move-result-object v3

    throw v3

    :catch_f
    move-exception v3

    move-object v2, v1

    move-object v4, v2

    .line 142
    :goto_c
    sget-object v5, Lcom/helpshift/common/exception/NetworkException;->SSL_HANDSHAKE:Lcom/helpshift/common/exception/NetworkException;

    .line 143
    iget-object v6, p1, Lcom/helpshift/common/platform/network/Request;->url:Ljava/lang/String;

    iput-object v6, v5, Lcom/helpshift/common/exception/NetworkException;->route:Ljava/lang/String;

    .line 144
    invoke-static {v3, v5, v0}, Lcom/helpshift/common/exception/RootAPIException;->wrap(Ljava/lang/Exception;Lcom/helpshift/common/exception/ExceptionType;Ljava/lang/String;)Lcom/helpshift/common/exception/RootAPIException;

    move-result-object v3

    throw v3

    :catch_10
    move-exception v3

    move-object v2, v1

    move-object v4, v2

    .line 137
    :goto_d
    sget-object v5, Lcom/helpshift/common/exception/NetworkException;->SSL_PEER_UNVERIFIED:Lcom/helpshift/common/exception/NetworkException;

    .line 138
    iget-object v6, p1, Lcom/helpshift/common/platform/network/Request;->url:Ljava/lang/String;

    iput-object v6, v5, Lcom/helpshift/common/exception/NetworkException;->route:Ljava/lang/String;

    .line 139
    invoke-static {v3, v5, v0}, Lcom/helpshift/common/exception/RootAPIException;->wrap(Ljava/lang/Exception;Lcom/helpshift/common/exception/ExceptionType;Ljava/lang/String;)Lcom/helpshift/common/exception/RootAPIException;

    move-result-object v3

    throw v3

    :catch_11
    move-exception v3

    goto :goto_e

    :catch_12
    move-exception v3

    :goto_e
    move-object v2, v1

    move-object v4, v2

    .line 132
    :goto_f
    sget-object v5, Lcom/helpshift/common/exception/NetworkException;->NO_CONNECTION:Lcom/helpshift/common/exception/NetworkException;

    .line 133
    iget-object v6, p1, Lcom/helpshift/common/platform/network/Request;->url:Ljava/lang/String;

    iput-object v6, v5, Lcom/helpshift/common/exception/NetworkException;->route:Ljava/lang/String;

    .line 134
    invoke-static {v3, v5, v0}, Lcom/helpshift/common/exception/RootAPIException;->wrap(Ljava/lang/Exception;Lcom/helpshift/common/exception/ExceptionType;Ljava/lang/String;)Lcom/helpshift/common/exception/RootAPIException;

    move-result-object v3

    throw v3

    :catch_13
    move-exception v3

    move-object v2, v1

    move-object v4, v2

    .line 127
    :goto_10
    sget-object v5, Lcom/helpshift/common/exception/NetworkException;->UNKNOWN_HOST:Lcom/helpshift/common/exception/NetworkException;

    .line 128
    iget-object v6, p1, Lcom/helpshift/common/platform/network/Request;->url:Ljava/lang/String;

    iput-object v6, v5, Lcom/helpshift/common/exception/NetworkException;->route:Ljava/lang/String;

    .line 129
    invoke-static {v3, v5, v0}, Lcom/helpshift/common/exception/RootAPIException;->wrap(Ljava/lang/Exception;Lcom/helpshift/common/exception/ExceptionType;Ljava/lang/String;)Lcom/helpshift/common/exception/RootAPIException;

    move-result-object v3

    throw v3
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    :catchall_3
    move-exception v3

    move-object v12, v2

    move-object v2, v1

    move-object v1, v12

    :goto_11
    if-eqz v1, :cond_10

    .line 154
    :try_start_9
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    goto :goto_12

    :catch_14
    move-exception v1

    goto :goto_13

    :cond_10
    :goto_12
    if-eqz v4, :cond_11

    .line 158
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V

    .line 161
    :cond_11
    instance-of v1, v2, Ljavax/net/ssl/HttpsURLConnection;

    if-eqz v1, :cond_12

    .line 162
    move-object v1, v2

    check-cast v1, Ljavax/net/ssl/HttpsURLConnection;

    invoke-direct {p0, v1}, Lcom/helpshift/common/platform/AndroidHTTPTransport;->closeHelpshiftSSLSocketFactorySockets(Ljavax/net/ssl/HttpsURLConnection;)V

    :cond_12
    if-eqz v2, :cond_13

    .line 165
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_14

    .line 171
    :cond_13
    throw v3

    .line 169
    :goto_13
    sget-object v2, Lcom/helpshift/common/exception/NetworkException;->GENERIC:Lcom/helpshift/common/exception/NetworkException;

    .line 170
    iget-object p1, p1, Lcom/helpshift/common/platform/network/Request;->url:Ljava/lang/String;

    iput-object p1, v2, Lcom/helpshift/common/exception/NetworkException;->route:Ljava/lang/String;

    .line 171
    invoke-static {v1, v2, v0}, Lcom/helpshift/common/exception/RootAPIException;->wrap(Ljava/lang/Exception;Lcom/helpshift/common/exception/ExceptionType;Ljava/lang/String;)Lcom/helpshift/common/exception/RootAPIException;

    move-result-object p1

    throw p1
.end method

.method private readInputStream(Ljava/io/InputStream;)Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    .line 180
    :cond_0
    new-instance v0, Ljava/io/InputStreamReader;

    invoke-direct {v0, p1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    .line 181
    new-instance p1, Ljava/io/BufferedReader;

    invoke-direct {p1, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 182
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 185
    :goto_0
    invoke-virtual {p1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 186
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 188
    :cond_1
    invoke-virtual {v0}, Ljava/io/InputStreamReader;->close()V

    .line 189
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private upload(Lcom/helpshift/common/platform/network/UploadRequest;)Lcom/helpshift/common/platform/network/Response;
    .locals 18

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    const-string v3, "Network error"

    const-string v4, "Upload error"

    .line 234
    :try_start_0
    new-instance v6, Ljava/net/URL;

    iget-object v7, v2, Lcom/helpshift/common/platform/network/UploadRequest;->url:Ljava/lang/String;

    invoke-direct {v6, v7}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    const-string v7, "--"

    const-string v8, "*****"

    const-string v9, "\r\n"

    const-string v10, "https://"

    .line 239
    sget-object v11, Lcom/helpshift/common/domain/network/NetworkConstants;->scheme:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_3d
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_3c
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_3b
    .catch Ljavax/net/ssl/SSLPeerUnverifiedException; {:try_start_0 .. :try_end_0} :catch_3a
    .catch Ljavax/net/ssl/SSLHandshakeException; {:try_start_0 .. :try_end_0} :catch_39
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_38
    .catchall {:try_start_0 .. :try_end_0} :catchall_8

    if-eqz v10, :cond_0

    .line 240
    :try_start_1
    invoke-virtual {v6}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v6

    check-cast v6, Ljavax/net/ssl/HttpsURLConnection;
    :try_end_1
    .catch Ljava/net/UnknownHostException; {:try_start_1 .. :try_end_1} :catch_b
    .catch Ljava/net/SocketException; {:try_start_1 .. :try_end_1} :catch_a
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_9
    .catch Ljavax/net/ssl/SSLPeerUnverifiedException; {:try_start_1 .. :try_end_1} :catch_8
    .catch Ljavax/net/ssl/SSLHandshakeException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 241
    :try_start_2
    move-object v10, v6

    check-cast v10, Ljavax/net/ssl/HttpsURLConnection;

    invoke-direct {v1, v10}, Lcom/helpshift/common/platform/AndroidHTTPTransport;->fixSSLSocketProtocols(Ljavax/net/ssl/HttpsURLConnection;)V
    :try_end_2
    .catch Ljava/net/UnknownHostException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/net/SocketException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/lang/SecurityException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljavax/net/ssl/SSLPeerUnverifiedException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljavax/net/ssl/SSLHandshakeException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_e

    :catchall_0
    move-exception v0

    move-object v4, v0

    goto :goto_1

    :catch_0
    move-exception v0

    move-object v5, v0

    goto :goto_3

    :catch_1
    move-exception v0

    move-object v5, v0

    goto :goto_5

    :catch_2
    move-exception v0

    move-object v5, v0

    goto :goto_7

    :catch_3
    move-exception v0

    goto :goto_0

    :catch_4
    move-exception v0

    :goto_0
    move-object v5, v0

    goto :goto_a

    :catch_5
    move-exception v0

    move-object v5, v0

    goto :goto_c

    :catchall_1
    move-exception v0

    move-object v4, v0

    const/4 v6, 0x0

    :goto_1
    const/4 v8, 0x0

    const/4 v11, 0x0

    :goto_2
    const/4 v12, 0x0

    goto/16 :goto_2c

    :catch_6
    move-exception v0

    move-object v5, v0

    const/4 v6, 0x0

    :goto_3
    const/4 v8, 0x0

    const/4 v11, 0x0

    :goto_4
    const/4 v12, 0x0

    goto/16 :goto_21

    :catch_7
    move-exception v0

    move-object v5, v0

    const/4 v6, 0x0

    :goto_5
    const/4 v8, 0x0

    const/4 v11, 0x0

    :goto_6
    const/4 v12, 0x0

    goto/16 :goto_23

    :catch_8
    move-exception v0

    move-object v5, v0

    const/4 v6, 0x0

    :goto_7
    const/4 v8, 0x0

    const/4 v11, 0x0

    :goto_8
    const/4 v12, 0x0

    goto/16 :goto_25

    :catch_9
    move-exception v0

    goto :goto_9

    :catch_a
    move-exception v0

    :goto_9
    move-object v5, v0

    const/4 v6, 0x0

    :goto_a
    const/4 v8, 0x0

    const/4 v11, 0x0

    :goto_b
    const/4 v12, 0x0

    goto/16 :goto_28

    :catch_b
    move-exception v0

    move-object v5, v0

    const/4 v6, 0x0

    :goto_c
    const/4 v8, 0x0

    const/4 v11, 0x0

    :goto_d
    const/4 v12, 0x0

    goto/16 :goto_2a

    .line 244
    :cond_0
    :try_start_3
    invoke-virtual {v6}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v6

    check-cast v6, Ljava/net/HttpURLConnection;
    :try_end_3
    .catch Ljava/net/UnknownHostException; {:try_start_3 .. :try_end_3} :catch_3d
    .catch Ljava/net/SocketException; {:try_start_3 .. :try_end_3} :catch_3c
    .catch Ljava/lang/SecurityException; {:try_start_3 .. :try_end_3} :catch_3b
    .catch Ljavax/net/ssl/SSLPeerUnverifiedException; {:try_start_3 .. :try_end_3} :catch_3a
    .catch Ljavax/net/ssl/SSLHandshakeException; {:try_start_3 .. :try_end_3} :catch_39
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_38
    .catchall {:try_start_3 .. :try_end_3} :catchall_8

    :goto_e
    const/4 v10, 0x1

    .line 246
    :try_start_4
    invoke-virtual {v6, v10}, Ljava/net/HttpURLConnection;->setDoInput(Z)V

    .line 247
    invoke-virtual {v6, v10}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    const/4 v10, 0x0

    .line 248
    invoke-virtual {v6, v10}, Ljava/net/HttpURLConnection;->setUseCaches(Z)V

    .line 249
    iget-object v11, v2, Lcom/helpshift/common/platform/network/UploadRequest;->method:Lcom/helpshift/common/platform/network/Method;

    invoke-virtual {v11}, Lcom/helpshift/common/platform/network/Method;->name()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v6, v11}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 250
    iget v11, v2, Lcom/helpshift/common/platform/network/UploadRequest;->timeout:I

    invoke-virtual {v6, v11}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 251
    iget v11, v2, Lcom/helpshift/common/platform/network/UploadRequest;->timeout:I

    invoke-virtual {v6, v11}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 253
    iget-object v11, v2, Lcom/helpshift/common/platform/network/UploadRequest;->headers:Ljava/util/List;

    .line 254
    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_f
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12
    :try_end_4
    .catch Ljava/net/UnknownHostException; {:try_start_4 .. :try_end_4} :catch_37
    .catch Ljava/net/SocketException; {:try_start_4 .. :try_end_4} :catch_36
    .catch Ljava/lang/SecurityException; {:try_start_4 .. :try_end_4} :catch_35
    .catch Ljavax/net/ssl/SSLPeerUnverifiedException; {:try_start_4 .. :try_end_4} :catch_34
    .catch Ljavax/net/ssl/SSLHandshakeException; {:try_start_4 .. :try_end_4} :catch_33
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_32
    .catchall {:try_start_4 .. :try_end_4} :catchall_7

    if-eqz v12, :cond_1

    :try_start_5
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/helpshift/common/platform/network/KeyValuePair;

    .line 255
    iget-object v13, v12, Lcom/helpshift/common/platform/network/KeyValuePair;->key:Ljava/lang/String;

    iget-object v12, v12, Lcom/helpshift/common/platform/network/KeyValuePair;->value:Ljava/lang/String;

    invoke-virtual {v6, v13, v12}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/net/UnknownHostException; {:try_start_5 .. :try_end_5} :catch_5
    .catch Ljava/net/SocketException; {:try_start_5 .. :try_end_5} :catch_4
    .catch Ljava/lang/SecurityException; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljavax/net/ssl/SSLPeerUnverifiedException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljavax/net/ssl/SSLHandshakeException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_f

    .line 258
    :cond_1
    :try_start_6
    new-instance v11, Ljava/io/DataOutputStream;

    invoke-virtual {v6}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_6
    .catch Ljava/net/UnknownHostException; {:try_start_6 .. :try_end_6} :catch_37
    .catch Ljava/net/SocketException; {:try_start_6 .. :try_end_6} :catch_36
    .catch Ljava/lang/SecurityException; {:try_start_6 .. :try_end_6} :catch_35
    .catch Ljavax/net/ssl/SSLPeerUnverifiedException; {:try_start_6 .. :try_end_6} :catch_34
    .catch Ljavax/net/ssl/SSLHandshakeException; {:try_start_6 .. :try_end_6} :catch_33
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_32
    .catchall {:try_start_6 .. :try_end_6} :catchall_7

    .line 259
    :try_start_7
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v12, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    .line 260
    iget-object v12, v2, Lcom/helpshift/common/platform/network/UploadRequest;->data:Ljava/util/Map;

    .line 262
    invoke-interface {v12}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v13

    invoke-interface {v13}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_10
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v14
    :try_end_7
    .catch Ljava/net/UnknownHostException; {:try_start_7 .. :try_end_7} :catch_31
    .catch Ljava/net/SocketException; {:try_start_7 .. :try_end_7} :catch_30
    .catch Ljava/lang/SecurityException; {:try_start_7 .. :try_end_7} :catch_2f
    .catch Ljavax/net/ssl/SSLPeerUnverifiedException; {:try_start_7 .. :try_end_7} :catch_2e
    .catch Ljavax/net/ssl/SSLHandshakeException; {:try_start_7 .. :try_end_7} :catch_2d
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_2c
    .catchall {:try_start_7 .. :try_end_7} :catchall_6

    const-string v15, "Content-Length: "

    if-eqz v14, :cond_3

    :try_start_8
    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/util/Map$Entry;

    .line 263
    invoke-interface {v14}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v16

    move-object/from16 v5, v16

    check-cast v5, Ljava/lang/String;

    .line 265
    invoke-direct {v1, v5}, Lcom/helpshift/common/platform/AndroidHTTPTransport;->isInvalidKeyForHeader(Ljava/lang/String;)Z

    move-result v16

    if-nez v16, :cond_2

    .line 266
    invoke-interface {v14}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    .line 267
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v17, v13

    const-string v13, "Content-Disposition: form-data; name=\""

    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "\"; "

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v11, v5}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    .line 269
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Content-Type: text/plain;charset=UTF-8"

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v11, v5}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    .line 270
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v10

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v11, v5}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    .line 271
    invoke-virtual {v11, v9}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    .line 272
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v11, v5}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    .line 273
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v11, v5}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V
    :try_end_8
    .catch Ljava/net/UnknownHostException; {:try_start_8 .. :try_end_8} :catch_11
    .catch Ljava/net/SocketException; {:try_start_8 .. :try_end_8} :catch_10
    .catch Ljava/lang/SecurityException; {:try_start_8 .. :try_end_8} :catch_f
    .catch Ljavax/net/ssl/SSLPeerUnverifiedException; {:try_start_8 .. :try_end_8} :catch_e
    .catch Ljavax/net/ssl/SSLHandshakeException; {:try_start_8 .. :try_end_8} :catch_d
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_c
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    goto :goto_11

    :cond_2
    move-object/from16 v17, v13

    :goto_11
    move-object/from16 v13, v17

    const/4 v10, 0x0

    goto/16 :goto_10

    :catchall_2
    move-exception v0

    move-object v4, v0

    const/4 v8, 0x0

    goto/16 :goto_2

    :catch_c
    move-exception v0

    move-object v5, v0

    const/4 v8, 0x0

    goto/16 :goto_4

    :catch_d
    move-exception v0

    move-object v5, v0

    const/4 v8, 0x0

    goto/16 :goto_6

    :catch_e
    move-exception v0

    move-object v5, v0

    const/4 v8, 0x0

    goto/16 :goto_8

    :catch_f
    move-exception v0

    goto :goto_12

    :catch_10
    move-exception v0

    :goto_12
    move-object v5, v0

    const/4 v8, 0x0

    goto/16 :goto_b

    :catch_11
    move-exception v0

    move-object v5, v0

    const/4 v8, 0x0

    goto/16 :goto_d

    .line 277
    :cond_3
    :try_start_9
    new-instance v5, Ljava/io/File;

    const-string v10, "screenshot"

    invoke-interface {v12, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    invoke-direct {v5, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const-string v10, "originalFileName"

    .line 278
    invoke-interface {v12, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;
    :try_end_9
    .catch Ljava/net/UnknownHostException; {:try_start_9 .. :try_end_9} :catch_31
    .catch Ljava/net/SocketException; {:try_start_9 .. :try_end_9} :catch_30
    .catch Ljava/lang/SecurityException; {:try_start_9 .. :try_end_9} :catch_2f
    .catch Ljavax/net/ssl/SSLPeerUnverifiedException; {:try_start_9 .. :try_end_9} :catch_2e
    .catch Ljavax/net/ssl/SSLHandshakeException; {:try_start_9 .. :try_end_9} :catch_2d
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_2c
    .catchall {:try_start_9 .. :try_end_9} :catchall_6

    if-nez v10, :cond_4

    .line 281
    :try_start_a
    invoke-virtual {v5}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v10
    :try_end_a
    .catch Ljava/net/UnknownHostException; {:try_start_a .. :try_end_a} :catch_11
    .catch Ljava/net/SocketException; {:try_start_a .. :try_end_a} :catch_10
    .catch Ljava/lang/SecurityException; {:try_start_a .. :try_end_a} :catch_f
    .catch Ljavax/net/ssl/SSLPeerUnverifiedException; {:try_start_a .. :try_end_a} :catch_e
    .catch Ljavax/net/ssl/SSLHandshakeException; {:try_start_a .. :try_end_a} :catch_d
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_c
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    .line 284
    :cond_4
    :try_start_b
    new-instance v12, Ljava/io/FileInputStream;

    invoke-direct {v12, v5}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_b
    .catch Ljava/net/UnknownHostException; {:try_start_b .. :try_end_b} :catch_31
    .catch Ljava/net/SocketException; {:try_start_b .. :try_end_b} :catch_30
    .catch Ljava/lang/SecurityException; {:try_start_b .. :try_end_b} :catch_2f
    .catch Ljavax/net/ssl/SSLPeerUnverifiedException; {:try_start_b .. :try_end_b} :catch_2e
    .catch Ljavax/net/ssl/SSLHandshakeException; {:try_start_b .. :try_end_b} :catch_2d
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_2c
    .catchall {:try_start_b .. :try_end_b} :catchall_6

    .line 285
    :try_start_c
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v13, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v13}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    .line 286
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "Content-Disposition: form-data; name=\"screenshot\"; filename=\""

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v10, "\""

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v11, v10}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    .line 288
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Content-Type: "

    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v13, v2, Lcom/helpshift/common/platform/network/UploadRequest;->mimeType:Ljava/lang/String;

    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v11, v10}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    .line 289
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/io/File;->length()J

    move-result-wide v13

    invoke-virtual {v10, v13, v14}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v11, v5}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    .line 290
    invoke-virtual {v11, v9}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    const/high16 v5, 0x100000

    .line 296
    invoke-virtual {v12}, Ljava/io/FileInputStream;->available()I

    move-result v10

    .line 298
    invoke-static {v10, v5}, Ljava/lang/Math;->min(II)I

    move-result v10

    .line 299
    new-array v13, v10, [B

    const/4 v14, 0x0

    .line 302
    invoke-virtual {v12, v13, v14, v10}, Ljava/io/FileInputStream;->read([BII)I

    move-result v15
    :try_end_c
    .catch Ljava/net/UnknownHostException; {:try_start_c .. :try_end_c} :catch_2b
    .catch Ljava/net/SocketException; {:try_start_c .. :try_end_c} :catch_2a
    .catch Ljava/lang/SecurityException; {:try_start_c .. :try_end_c} :catch_29
    .catch Ljavax/net/ssl/SSLPeerUnverifiedException; {:try_start_c .. :try_end_c} :catch_28
    .catch Ljavax/net/ssl/SSLHandshakeException; {:try_start_c .. :try_end_c} :catch_27
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_26
    .catchall {:try_start_c .. :try_end_c} :catchall_5

    :goto_13
    if-lez v15, :cond_5

    .line 305
    :try_start_d
    invoke-virtual {v11, v13, v14, v10}, Ljava/io/DataOutputStream;->write([BII)V

    .line 306
    invoke-virtual {v12}, Ljava/io/FileInputStream;->available()I

    move-result v10

    .line 307
    invoke-static {v10, v5}, Ljava/lang/Math;->min(II)I

    move-result v10

    .line 308
    invoke-virtual {v12, v13, v14, v10}, Ljava/io/FileInputStream;->read([BII)I

    move-result v15
    :try_end_d
    .catch Ljava/net/UnknownHostException; {:try_start_d .. :try_end_d} :catch_17
    .catch Ljava/net/SocketException; {:try_start_d .. :try_end_d} :catch_16
    .catch Ljava/lang/SecurityException; {:try_start_d .. :try_end_d} :catch_15
    .catch Ljavax/net/ssl/SSLPeerUnverifiedException; {:try_start_d .. :try_end_d} :catch_14
    .catch Ljavax/net/ssl/SSLHandshakeException; {:try_start_d .. :try_end_d} :catch_13
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_12
    .catchall {:try_start_d .. :try_end_d} :catchall_3

    goto :goto_13

    :catchall_3
    move-exception v0

    move-object v4, v0

    const/4 v8, 0x0

    goto/16 :goto_2c

    :catch_12
    move-exception v0

    move-object v5, v0

    const/4 v8, 0x0

    goto/16 :goto_21

    :catch_13
    move-exception v0

    move-object v5, v0

    const/4 v8, 0x0

    goto/16 :goto_23

    :catch_14
    move-exception v0

    move-object v5, v0

    const/4 v8, 0x0

    goto/16 :goto_25

    :catch_15
    move-exception v0

    goto :goto_14

    :catch_16
    move-exception v0

    :goto_14
    move-object v5, v0

    const/4 v8, 0x0

    goto/16 :goto_28

    :catch_17
    move-exception v0

    move-object v5, v0

    const/4 v8, 0x0

    goto/16 :goto_2a

    .line 311
    :cond_5
    :try_start_e
    invoke-virtual {v11, v9}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    .line 312
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v11, v5}, Ljava/io/DataOutputStream;->writeBytes(Ljava/lang/String;)V

    .line 313
    invoke-virtual {v11}, Ljava/io/DataOutputStream;->flush()V

    .line 315
    invoke-virtual {v6}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v5
    :try_end_e
    .catch Ljava/net/UnknownHostException; {:try_start_e .. :try_end_e} :catch_2b
    .catch Ljava/net/SocketException; {:try_start_e .. :try_end_e} :catch_2a
    .catch Ljava/lang/SecurityException; {:try_start_e .. :try_end_e} :catch_29
    .catch Ljavax/net/ssl/SSLPeerUnverifiedException; {:try_start_e .. :try_end_e} :catch_28
    .catch Ljavax/net/ssl/SSLHandshakeException; {:try_start_e .. :try_end_e} :catch_27
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_26
    .catchall {:try_start_e .. :try_end_e} :catchall_5

    const/16 v7, 0xc8

    if-lt v5, v7, :cond_9

    const/16 v7, 0x12c

    if-ge v5, v7, :cond_9

    .line 318
    :try_start_f
    invoke-virtual {v6}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v7
    :try_end_f
    .catch Ljava/net/UnknownHostException; {:try_start_f .. :try_end_f} :catch_17
    .catch Ljava/net/SocketException; {:try_start_f .. :try_end_f} :catch_16
    .catch Ljava/lang/SecurityException; {:try_start_f .. :try_end_f} :catch_15
    .catch Ljavax/net/ssl/SSLPeerUnverifiedException; {:try_start_f .. :try_end_f} :catch_14
    .catch Ljavax/net/ssl/SSLHandshakeException; {:try_start_f .. :try_end_f} :catch_13
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_12
    .catchall {:try_start_f .. :try_end_f} :catchall_3

    if-eqz v7, :cond_6

    .line 320
    :try_start_10
    invoke-direct {v1, v7}, Lcom/helpshift/common/platform/AndroidHTTPTransport;->readInputStream(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v8

    goto :goto_15

    :cond_6
    const/4 v8, 0x0

    .line 322
    :goto_15
    new-instance v9, Lcom/helpshift/common/platform/network/Response;

    const/4 v10, 0x0

    invoke-direct {v9, v5, v8, v10}, Lcom/helpshift/common/platform/network/Response;-><init>(ILjava/lang/String;Ljava/util/List;)V
    :try_end_10
    .catch Ljava/net/UnknownHostException; {:try_start_10 .. :try_end_10} :catch_1e
    .catch Ljava/net/SocketException; {:try_start_10 .. :try_end_10} :catch_1d
    .catch Ljava/lang/SecurityException; {:try_start_10 .. :try_end_10} :catch_1c
    .catch Ljavax/net/ssl/SSLPeerUnverifiedException; {:try_start_10 .. :try_end_10} :catch_1b
    .catch Ljavax/net/ssl/SSLHandshakeException; {:try_start_10 .. :try_end_10} :catch_1a
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_19
    .catchall {:try_start_10 .. :try_end_10} :catchall_4

    .line 357
    :try_start_11
    invoke-virtual {v12}, Ljava/io/FileInputStream;->close()V

    .line 361
    invoke-virtual {v11}, Ljava/io/DataOutputStream;->close()V

    if-eqz v7, :cond_7

    .line 365
    invoke-virtual {v7}, Ljava/io/InputStream;->close()V

    :cond_7
    if-eqz v6, :cond_8

    .line 369
    invoke-virtual {v6}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_18

    :cond_8
    return-object v9

    :catch_18
    move-exception v0

    move-object v4, v0

    .line 373
    sget-object v5, Lcom/helpshift/common/exception/NetworkException;->GENERIC:Lcom/helpshift/common/exception/NetworkException;

    .line 374
    iget-object v2, v2, Lcom/helpshift/common/platform/network/UploadRequest;->url:Ljava/lang/String;

    iput-object v2, v5, Lcom/helpshift/common/exception/NetworkException;->route:Ljava/lang/String;

    .line 375
    invoke-static {v4, v5, v3}, Lcom/helpshift/common/exception/RootAPIException;->wrap(Ljava/lang/Exception;Lcom/helpshift/common/exception/ExceptionType;Ljava/lang/String;)Lcom/helpshift/common/exception/RootAPIException;

    move-result-object v2

    throw v2

    :catchall_4
    move-exception v0

    move-object v4, v0

    move-object v8, v7

    goto/16 :goto_2c

    :catch_19
    move-exception v0

    move-object v5, v0

    move-object v8, v7

    goto/16 :goto_21

    :catch_1a
    move-exception v0

    move-object v5, v0

    move-object v8, v7

    goto/16 :goto_23

    :catch_1b
    move-exception v0

    move-object v5, v0

    move-object v8, v7

    goto/16 :goto_25

    :catch_1c
    move-exception v0

    goto :goto_16

    :catch_1d
    move-exception v0

    :goto_16
    move-object v5, v0

    move-object v8, v7

    goto/16 :goto_28

    :catch_1e
    move-exception v0

    move-object v5, v0

    move-object v8, v7

    goto/16 :goto_2a

    .line 325
    :cond_9
    :try_start_12
    new-instance v7, Lcom/helpshift/common/platform/network/Response;
    :try_end_12
    .catch Ljava/net/UnknownHostException; {:try_start_12 .. :try_end_12} :catch_2b
    .catch Ljava/net/SocketException; {:try_start_12 .. :try_end_12} :catch_2a
    .catch Ljava/lang/SecurityException; {:try_start_12 .. :try_end_12} :catch_29
    .catch Ljavax/net/ssl/SSLPeerUnverifiedException; {:try_start_12 .. :try_end_12} :catch_28
    .catch Ljavax/net/ssl/SSLHandshakeException; {:try_start_12 .. :try_end_12} :catch_27
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_12} :catch_26
    .catchall {:try_start_12 .. :try_end_12} :catchall_5

    const/4 v8, 0x0

    :try_start_13
    invoke-direct {v7, v5, v8, v8}, Lcom/helpshift/common/platform/network/Response;-><init>(ILjava/lang/String;Ljava/util/List;)V
    :try_end_13
    .catch Ljava/net/UnknownHostException; {:try_start_13 .. :try_end_13} :catch_25
    .catch Ljava/net/SocketException; {:try_start_13 .. :try_end_13} :catch_24
    .catch Ljava/lang/SecurityException; {:try_start_13 .. :try_end_13} :catch_23
    .catch Ljavax/net/ssl/SSLPeerUnverifiedException; {:try_start_13 .. :try_end_13} :catch_22
    .catch Ljavax/net/ssl/SSLHandshakeException; {:try_start_13 .. :try_end_13} :catch_21
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_13} :catch_20
    .catchall {:try_start_13 .. :try_end_13} :catchall_9

    .line 357
    :try_start_14
    invoke-virtual {v12}, Ljava/io/FileInputStream;->close()V

    .line 361
    invoke-virtual {v11}, Ljava/io/DataOutputStream;->close()V

    if-eqz v6, :cond_a

    .line 369
    invoke-virtual {v6}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_14
    .catch Ljava/lang/Exception; {:try_start_14 .. :try_end_14} :catch_1f

    :cond_a
    return-object v7

    :catch_1f
    move-exception v0

    move-object v4, v0

    .line 373
    sget-object v5, Lcom/helpshift/common/exception/NetworkException;->GENERIC:Lcom/helpshift/common/exception/NetworkException;

    .line 374
    iget-object v2, v2, Lcom/helpshift/common/platform/network/UploadRequest;->url:Ljava/lang/String;

    iput-object v2, v5, Lcom/helpshift/common/exception/NetworkException;->route:Ljava/lang/String;

    .line 375
    invoke-static {v4, v5, v3}, Lcom/helpshift/common/exception/RootAPIException;->wrap(Ljava/lang/Exception;Lcom/helpshift/common/exception/ExceptionType;Ljava/lang/String;)Lcom/helpshift/common/exception/RootAPIException;

    move-result-object v2

    throw v2

    :catch_20
    move-exception v0

    goto :goto_17

    :catch_21
    move-exception v0

    goto :goto_18

    :catch_22
    move-exception v0

    goto :goto_19

    :catch_23
    move-exception v0

    goto :goto_1b

    :catch_24
    move-exception v0

    goto :goto_1b

    :catch_25
    move-exception v0

    goto :goto_1c

    :catchall_5
    move-exception v0

    const/4 v8, 0x0

    goto/16 :goto_2b

    :catch_26
    move-exception v0

    const/4 v8, 0x0

    :goto_17
    move-object v5, v0

    goto/16 :goto_21

    :catch_27
    move-exception v0

    const/4 v8, 0x0

    :goto_18
    move-object v5, v0

    goto/16 :goto_23

    :catch_28
    move-exception v0

    const/4 v8, 0x0

    :goto_19
    move-object v5, v0

    goto/16 :goto_25

    :catch_29
    move-exception v0

    goto :goto_1a

    :catch_2a
    move-exception v0

    :goto_1a
    const/4 v8, 0x0

    :goto_1b
    move-object v5, v0

    goto/16 :goto_28

    :catch_2b
    move-exception v0

    const/4 v8, 0x0

    :goto_1c
    move-object v5, v0

    goto/16 :goto_2a

    :catchall_6
    move-exception v0

    const/4 v8, 0x0

    move-object v4, v0

    move-object v12, v8

    goto/16 :goto_2c

    :catch_2c
    move-exception v0

    const/4 v8, 0x0

    move-object v5, v0

    move-object v12, v8

    goto/16 :goto_21

    :catch_2d
    move-exception v0

    const/4 v8, 0x0

    move-object v5, v0

    move-object v12, v8

    goto/16 :goto_23

    :catch_2e
    move-exception v0

    const/4 v8, 0x0

    move-object v5, v0

    move-object v12, v8

    goto/16 :goto_25

    :catch_2f
    move-exception v0

    goto :goto_1d

    :catch_30
    move-exception v0

    :goto_1d
    const/4 v8, 0x0

    move-object v5, v0

    move-object v12, v8

    goto/16 :goto_28

    :catch_31
    move-exception v0

    const/4 v8, 0x0

    move-object v5, v0

    move-object v12, v8

    goto/16 :goto_2a

    :catchall_7
    move-exception v0

    const/4 v8, 0x0

    move-object v4, v0

    move-object v11, v8

    goto :goto_1f

    :catch_32
    move-exception v0

    const/4 v8, 0x0

    move-object v5, v0

    move-object v11, v8

    goto :goto_20

    :catch_33
    move-exception v0

    const/4 v8, 0x0

    move-object v5, v0

    move-object v11, v8

    goto :goto_22

    :catch_34
    move-exception v0

    const/4 v8, 0x0

    move-object v5, v0

    move-object v11, v8

    goto :goto_24

    :catch_35
    move-exception v0

    goto :goto_1e

    :catch_36
    move-exception v0

    :goto_1e
    const/4 v8, 0x0

    move-object v5, v0

    move-object v11, v8

    goto :goto_27

    :catch_37
    move-exception v0

    const/4 v8, 0x0

    move-object v5, v0

    move-object v11, v8

    goto/16 :goto_29

    :catchall_8
    move-exception v0

    const/4 v8, 0x0

    move-object v4, v0

    move-object v6, v8

    move-object v11, v6

    :goto_1f
    move-object v12, v11

    goto/16 :goto_2c

    :catch_38
    move-exception v0

    const/4 v8, 0x0

    move-object v5, v0

    move-object v6, v8

    move-object v11, v6

    :goto_20
    move-object v12, v11

    .line 349
    :goto_21
    :try_start_15
    sget-object v7, Lcom/helpshift/common/exception/NetworkException;->GENERIC:Lcom/helpshift/common/exception/NetworkException;

    .line 350
    iget-object v9, v2, Lcom/helpshift/common/platform/network/UploadRequest;->url:Ljava/lang/String;

    iput-object v9, v7, Lcom/helpshift/common/exception/NetworkException;->route:Ljava/lang/String;

    .line 351
    invoke-static {v5, v7, v4}, Lcom/helpshift/common/exception/RootAPIException;->wrap(Ljava/lang/Exception;Lcom/helpshift/common/exception/ExceptionType;Ljava/lang/String;)Lcom/helpshift/common/exception/RootAPIException;

    move-result-object v4

    throw v4

    :catch_39
    move-exception v0

    const/4 v8, 0x0

    move-object v5, v0

    move-object v6, v8

    move-object v11, v6

    :goto_22
    move-object v12, v11

    .line 344
    :goto_23
    sget-object v7, Lcom/helpshift/common/exception/NetworkException;->SSL_HANDSHAKE:Lcom/helpshift/common/exception/NetworkException;

    .line 345
    iget-object v9, v2, Lcom/helpshift/common/platform/network/UploadRequest;->url:Ljava/lang/String;

    iput-object v9, v7, Lcom/helpshift/common/exception/NetworkException;->route:Ljava/lang/String;

    .line 346
    invoke-static {v5, v7, v4}, Lcom/helpshift/common/exception/RootAPIException;->wrap(Ljava/lang/Exception;Lcom/helpshift/common/exception/ExceptionType;Ljava/lang/String;)Lcom/helpshift/common/exception/RootAPIException;

    move-result-object v4

    throw v4

    :catch_3a
    move-exception v0

    const/4 v8, 0x0

    move-object v5, v0

    move-object v6, v8

    move-object v11, v6

    :goto_24
    move-object v12, v11

    .line 339
    :goto_25
    sget-object v7, Lcom/helpshift/common/exception/NetworkException;->SSL_PEER_UNVERIFIED:Lcom/helpshift/common/exception/NetworkException;

    .line 340
    iget-object v9, v2, Lcom/helpshift/common/platform/network/UploadRequest;->url:Ljava/lang/String;

    iput-object v9, v7, Lcom/helpshift/common/exception/NetworkException;->route:Ljava/lang/String;

    .line 341
    invoke-static {v5, v7, v4}, Lcom/helpshift/common/exception/RootAPIException;->wrap(Ljava/lang/Exception;Lcom/helpshift/common/exception/ExceptionType;Ljava/lang/String;)Lcom/helpshift/common/exception/RootAPIException;

    move-result-object v4

    throw v4

    :catch_3b
    move-exception v0

    goto :goto_26

    :catch_3c
    move-exception v0

    :goto_26
    const/4 v8, 0x0

    move-object v5, v0

    move-object v6, v8

    move-object v11, v6

    :goto_27
    move-object v12, v11

    .line 334
    :goto_28
    sget-object v7, Lcom/helpshift/common/exception/NetworkException;->NO_CONNECTION:Lcom/helpshift/common/exception/NetworkException;

    .line 335
    iget-object v9, v2, Lcom/helpshift/common/platform/network/UploadRequest;->url:Ljava/lang/String;

    iput-object v9, v7, Lcom/helpshift/common/exception/NetworkException;->route:Ljava/lang/String;

    .line 336
    invoke-static {v5, v7, v4}, Lcom/helpshift/common/exception/RootAPIException;->wrap(Ljava/lang/Exception;Lcom/helpshift/common/exception/ExceptionType;Ljava/lang/String;)Lcom/helpshift/common/exception/RootAPIException;

    move-result-object v4

    throw v4

    :catch_3d
    move-exception v0

    const/4 v8, 0x0

    move-object v5, v0

    move-object v6, v8

    move-object v11, v6

    :goto_29
    move-object v12, v11

    .line 329
    :goto_2a
    sget-object v7, Lcom/helpshift/common/exception/NetworkException;->UNKNOWN_HOST:Lcom/helpshift/common/exception/NetworkException;

    .line 330
    iget-object v9, v2, Lcom/helpshift/common/platform/network/UploadRequest;->url:Ljava/lang/String;

    iput-object v9, v7, Lcom/helpshift/common/exception/NetworkException;->route:Ljava/lang/String;

    .line 331
    invoke-static {v5, v7, v4}, Lcom/helpshift/common/exception/RootAPIException;->wrap(Ljava/lang/Exception;Lcom/helpshift/common/exception/ExceptionType;Ljava/lang/String;)Lcom/helpshift/common/exception/RootAPIException;

    move-result-object v4

    throw v4
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_9

    :catchall_9
    move-exception v0

    :goto_2b
    move-object v4, v0

    :goto_2c
    if-eqz v12, :cond_b

    .line 357
    :try_start_16
    invoke-virtual {v12}, Ljava/io/FileInputStream;->close()V

    goto :goto_2d

    :catch_3e
    move-exception v0

    move-object v4, v0

    goto :goto_2e

    :cond_b
    :goto_2d
    if-eqz v11, :cond_c

    .line 361
    invoke-virtual {v11}, Ljava/io/DataOutputStream;->close()V

    :cond_c
    if-eqz v8, :cond_d

    .line 365
    invoke-virtual {v8}, Ljava/io/InputStream;->close()V

    :cond_d
    if-eqz v6, :cond_e

    .line 369
    invoke-virtual {v6}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_16
    .catch Ljava/lang/Exception; {:try_start_16 .. :try_end_16} :catch_3e

    goto :goto_2f

    .line 373
    :goto_2e
    sget-object v5, Lcom/helpshift/common/exception/NetworkException;->GENERIC:Lcom/helpshift/common/exception/NetworkException;

    .line 374
    iget-object v2, v2, Lcom/helpshift/common/platform/network/UploadRequest;->url:Ljava/lang/String;

    iput-object v2, v5, Lcom/helpshift/common/exception/NetworkException;->route:Ljava/lang/String;

    .line 375
    invoke-static {v4, v5, v3}, Lcom/helpshift/common/exception/RootAPIException;->wrap(Ljava/lang/Exception;Lcom/helpshift/common/exception/ExceptionType;Ljava/lang/String;)Lcom/helpshift/common/exception/RootAPIException;

    move-result-object v2

    throw v2

    :cond_e
    :goto_2f
    throw v4
.end method


# virtual methods
.method public makeRequest(Lcom/helpshift/common/platform/network/Request;)Lcom/helpshift/common/platform/network/Response;
    .locals 1

    .line 49
    instance-of v0, p1, Lcom/helpshift/common/platform/network/UploadRequest;

    if-eqz v0, :cond_0

    .line 50
    const-class v0, Lcom/helpshift/common/platform/network/UploadRequest;

    invoke-virtual {v0, p1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/helpshift/common/platform/network/UploadRequest;

    invoke-direct {p0, p1}, Lcom/helpshift/common/platform/AndroidHTTPTransport;->upload(Lcom/helpshift/common/platform/network/UploadRequest;)Lcom/helpshift/common/platform/network/Response;

    move-result-object p1

    return-object p1

    .line 53
    :cond_0
    invoke-direct {p0, p1}, Lcom/helpshift/common/platform/AndroidHTTPTransport;->makeNetworkRequest(Lcom/helpshift/common/platform/network/Request;)Lcom/helpshift/common/platform/network/Response;

    move-result-object p1

    return-object p1
.end method
