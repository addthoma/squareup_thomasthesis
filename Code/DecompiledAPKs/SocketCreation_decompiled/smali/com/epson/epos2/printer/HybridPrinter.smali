.class public Lcom/epson/epos2/printer/HybridPrinter;
.super Lcom/epson/epos2/printer/CommonPrinter;
.source "HybridPrinter.java"


# static fields
.field public static final EVENT_INSERTION_WAIT_MICR:I = 0xe

.field public static final EVENT_INSERTION_WAIT_NONE:I = 0xf

.field public static final EVENT_INSERTION_WAIT_SLIP:I = 0xc

.field public static final EVENT_INSERTION_WAIT_VALIDATION:I = 0xd

.field public static final EVENT_REMOVAL_WAIT_NONE:I = 0x11

.field public static final EVENT_REMOVAL_WAIT_PAPER:I = 0x10

.field public static final EVENT_SLIP_PAPER_EMPTY:I = 0x13

.field public static final EVENT_SLIP_PAPER_OK:I = 0x12

.field public static final INSERTION_WAIT_MICR:I = 0x2

.field public static final INSERTION_WAIT_NONE:I = 0x3

.field public static final INSERTION_WAIT_SLIP:I = 0x0

.field public static final INSERTION_WAIT_VALIDATION:I = 0x1

.field public static final METHOD_CANCELINSERTION:I = 0x2

.field public static final METHOD_CLEANMICRREADER:I = 0x5

.field public static final METHOD_EJECTPAPER:I = 0x3

.field public static final METHOD_READMICRDATA:I = 0x4

.field public static final METHOD_SENDDATA:I = 0x1

.field public static final METHOD_WAITINSERTION:I = 0x0

.field public static final MICR_FONT_CMC7:I = 0x1

.field public static final MICR_FONT_E13B:I = 0x0

.field private static final MODE40CPL_DEFAULT:I = 0x1

.field private static final PAPER_TYPE_DEFAULT:I = 0x0

.field public static final PAPER_TYPE_ENDORSE:I = 0x2

.field public static final PAPER_TYPE_RECEIPT:I = 0x0

.field public static final PAPER_TYPE_SLIP:I = 0x1

.field public static final PAPER_TYPE_VALIDATION:I = 0x3

.field public static final REMOVAL_WAIT_NONE:I = 0x1

.field public static final REMOVAL_WAIT_PAPER:I = 0x0

.field public static final SLIP_PAPER_EMPTY:I = 0x1

.field public static final SLIP_PAPER_OK:I = 0x0

.field private static final WAITTIME_DEFAULT:I = 0x1f4


# instance fields
.field private mConnectionListener:Lcom/epson/epos2/ConnectionListener;

.field private mMode40Cpl:I

.field private mPaperType:I

.field private mReceiveListener:Lcom/epson/epos2/printer/HybdReceiveListener;

.field private mStatusChangeListener:Lcom/epson/epos2/printer/HybdStatusChangeListener;

.field private mWaitTime:I


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 135
    invoke-direct {p0}, Lcom/epson/epos2/printer/CommonPrinter;-><init>()V

    const/16 v0, 0x1f4

    .line 65
    iput v0, p0, Lcom/epson/epos2/printer/HybridPrinter;->mWaitTime:I

    const/4 v0, 0x1

    .line 66
    iput v0, p0, Lcom/epson/epos2/printer/HybridPrinter;->mMode40Cpl:I

    const/4 v0, 0x0

    .line 67
    iput v0, p0, Lcom/epson/epos2/printer/HybridPrinter;->mPaperType:I

    return-void
.end method

.method public constructor <init>(ILandroid/content/Context;)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 112
    invoke-direct {p0}, Lcom/epson/epos2/printer/HybridPrinter;-><init>()V

    .line 113
    invoke-virtual {p0, p2}, Lcom/epson/epos2/printer/HybridPrinter;->initializeOuputLogFunctions(Landroid/content/Context;)V

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/Object;

    .line 114
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x1

    aput-object p2, v1, v2

    const-string v4, "HybridPrinter"

    invoke-virtual {p0, v4, v1}, Lcom/epson/epos2/printer/HybridPrinter;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    :try_start_0
    const-string v1, "com.epson.epos2.NativeInitializer"

    .line 116
    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    const-string v5, "initializeNativeEnv"

    new-array v6, v2, [Ljava/lang/Class;

    .line 117
    const-class v7, Landroid/content/Context;

    aput-object v7, v6, v3

    invoke-virtual {v1, v5, v6}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v5

    .line 118
    invoke-virtual {v5, v2}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    new-array v6, v2, [Ljava/lang/Object;

    aput-object p2, v6, v3

    .line 119
    invoke-virtual {v5, v1, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 122
    invoke-virtual {p0, v4, v1}, Lcom/epson/epos2/printer/HybridPrinter;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 123
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 125
    :goto_0
    iput-object p2, p0, Lcom/epson/epos2/printer/HybridPrinter;->mContext:Landroid/content/Context;

    .line 127
    invoke-direct {p0, p1}, Lcom/epson/epos2/printer/HybridPrinter;->initializePrinterInstance(I)V

    new-array v0, v0, [Ljava/lang/Object;

    .line 128
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    aput-object p2, v0, v2

    invoke-virtual {p0, v4, v3, v0}, Lcom/epson/epos2/printer/HybridPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void
.end method

.method private initializePrinterInstance(I)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v0, v0, [J

    .line 157
    invoke-virtual {p0, p1, v0}, Lcom/epson/epos2/printer/HybridPrinter;->nativeEpos2CreateHandle(I[J)I

    move-result p1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    .line 163
    aget-wide v1, v0, p1

    iput-wide v1, p0, Lcom/epson/epos2/printer/HybridPrinter;->mPrinterHandle:J

    const-wide/16 v1, 0x0

    aput-wide v1, v0, p1

    return-void

    .line 160
    :cond_0
    new-instance v0, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v0, p1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v0
.end method

.method private onConnection(I)V
    .locals 6

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/Object;

    .line 989
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x1

    aput-object p0, v1, v2

    const-string v4, "onConnection"

    invoke-virtual {p0, v4, v1}, Lcom/epson/epos2/printer/HybridPrinter;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 990
    iget-object v1, p0, Lcom/epson/epos2/printer/HybridPrinter;->mConnectionListener:Lcom/epson/epos2/ConnectionListener;

    if-eqz v1, :cond_0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v5, "eventType->"

    aput-object v5, v1, v3

    .line 992
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v2

    .line 991
    invoke-virtual {p0, v4, v1}, Lcom/epson/epos2/printer/HybridPrinter;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 993
    iget-object v1, p0, Lcom/epson/epos2/printer/HybridPrinter;->mConnectionListener:Lcom/epson/epos2/ConnectionListener;

    invoke-interface {v1, p0, p1}, Lcom/epson/epos2/ConnectionListener;->onConnection(Ljava/lang/Object;I)V

    :cond_0
    new-array v0, v0, [Ljava/lang/Object;

    .line 995
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    aput-object p0, v0, v2

    invoke-virtual {p0, v4, v3, v0}, Lcom/epson/epos2/printer/HybridPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void
.end method

.method private onHybdReceive(IILjava/lang/String;Lcom/epson/epos2/printer/HybridPrinterStatusInfo;)V
    .locals 22

    move-object/from16 v6, p0

    .line 951
    invoke-virtual/range {p4 .. p4}, Lcom/epson/epos2/printer/HybridPrinterStatusInfo;->getConnection()I

    move-result v0

    .line 952
    invoke-virtual/range {p4 .. p4}, Lcom/epson/epos2/printer/HybridPrinterStatusInfo;->getOnline()I

    move-result v1

    .line 953
    invoke-virtual/range {p4 .. p4}, Lcom/epson/epos2/printer/HybridPrinterStatusInfo;->getCoverOpen()I

    move-result v2

    .line 954
    invoke-virtual/range {p4 .. p4}, Lcom/epson/epos2/printer/HybridPrinterStatusInfo;->getPaper()I

    move-result v3

    .line 955
    invoke-virtual/range {p4 .. p4}, Lcom/epson/epos2/printer/HybridPrinterStatusInfo;->getPaperFeed()I

    move-result v4

    .line 956
    invoke-virtual/range {p4 .. p4}, Lcom/epson/epos2/printer/HybridPrinterStatusInfo;->getPanelSwitch()I

    move-result v5

    .line 957
    invoke-virtual/range {p4 .. p4}, Lcom/epson/epos2/printer/HybridPrinterStatusInfo;->getOnline()I

    move-result v7

    .line 958
    invoke-virtual/range {p4 .. p4}, Lcom/epson/epos2/printer/HybridPrinterStatusInfo;->getDrawer()I

    move-result v8

    .line 959
    invoke-virtual/range {p4 .. p4}, Lcom/epson/epos2/printer/HybridPrinterStatusInfo;->getErrorStatus()I

    move-result v9

    .line 960
    invoke-virtual/range {p4 .. p4}, Lcom/epson/epos2/printer/HybridPrinterStatusInfo;->getAutoRecoverError()I

    move-result v10

    .line 961
    invoke-virtual/range {p4 .. p4}, Lcom/epson/epos2/printer/HybridPrinterStatusInfo;->getInsertionWaiting()I

    move-result v11

    .line 962
    invoke-virtual/range {p4 .. p4}, Lcom/epson/epos2/printer/HybridPrinterStatusInfo;->getRemovalWaiting()I

    move-result v12

    .line 963
    invoke-virtual/range {p4 .. p4}, Lcom/epson/epos2/printer/HybridPrinterStatusInfo;->getSlipPaper()I

    move-result v13

    const/4 v14, 0x4

    new-array v15, v14, [Ljava/lang/Object;

    .line 965
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    const/4 v14, 0x0

    aput-object v16, v15, v14

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    const/16 v18, 0x1

    aput-object v16, v15, v18

    const/16 v16, 0x2

    aput-object p4, v15, v16

    const/16 v19, 0x3

    aput-object v6, v15, v19

    const-string v14, "onHybdReceive"

    invoke-virtual {v6, v14, v15}, Lcom/epson/epos2/printer/HybridPrinter;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 966
    iget-object v15, v6, Lcom/epson/epos2/printer/HybridPrinter;->mReceiveListener:Lcom/epson/epos2/printer/HybdReceiveListener;

    if-eqz v15, :cond_0

    const/16 v15, 0x1e

    new-array v15, v15, [Ljava/lang/Object;

    const-string v21, "method->"

    const/16 v20, 0x0

    aput-object v21, v15, v20

    .line 968
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    aput-object v21, v15, v18

    const-string v21, "code->"

    aput-object v21, v15, v16

    .line 969
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    aput-object v21, v15, v19

    const-string v21, "connection->"

    const/16 v17, 0x4

    aput-object v21, v15, v17

    const/16 v21, 0x5

    .line 970
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v15, v21

    const/4 v0, 0x6

    const-string v21, "online->"

    aput-object v21, v15, v0

    const/4 v0, 0x7

    .line 971
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v15, v0

    const/16 v0, 0x8

    const-string v1, "coverOpen->"

    aput-object v1, v15, v0

    const/16 v0, 0x9

    .line 972
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v15, v0

    const/16 v0, 0xa

    const-string v1, "paper->"

    aput-object v1, v15, v0

    const/16 v0, 0xb

    .line 973
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v15, v0

    const/16 v0, 0xc

    const-string v1, "paperFeed->"

    aput-object v1, v15, v0

    const/16 v0, 0xd

    .line 974
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v15, v0

    const/16 v0, 0xe

    const-string v1, "panelSwitch->"

    aput-object v1, v15, v0

    const/16 v0, 0xf

    .line 975
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v15, v0

    const/16 v0, 0x10

    const-string v1, "waitOnline->"

    aput-object v1, v15, v0

    const/16 v0, 0x11

    .line 976
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v15, v0

    const/16 v0, 0x12

    const-string v1, "drawer->"

    aput-object v1, v15, v0

    const/16 v0, 0x13

    .line 977
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v15, v0

    const/16 v0, 0x14

    const-string v1, "errorStatus->"

    aput-object v1, v15, v0

    const/16 v0, 0x15

    .line 978
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v15, v0

    const/16 v0, 0x16

    const-string v1, "autoRecoverError->"

    aput-object v1, v15, v0

    const/16 v0, 0x17

    .line 979
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v15, v0

    const/16 v0, 0x18

    const-string v1, "insertionWaiting->"

    aput-object v1, v15, v0

    const/16 v0, 0x19

    .line 980
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v15, v0

    const/16 v0, 0x1a

    const-string v1, "removalWaiting->"

    aput-object v1, v15, v0

    const/16 v0, 0x1b

    .line 981
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v15, v0

    const/16 v0, 0x1c

    const-string v1, "slipPaper->"

    aput-object v1, v15, v0

    const/16 v0, 0x1d

    .line 982
    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v15, v0

    .line 967
    invoke-virtual {v6, v14, v15}, Lcom/epson/epos2/printer/HybridPrinter;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 983
    iget-object v0, v6, Lcom/epson/epos2/printer/HybridPrinter;->mReceiveListener:Lcom/epson/epos2/printer/HybdReceiveListener;

    move-object/from16 v1, p0

    move/from16 v2, p1

    move/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    invoke-interface/range {v0 .. v5}, Lcom/epson/epos2/printer/HybdReceiveListener;->onHybdReceive(Lcom/epson/epos2/printer/HybridPrinter;IILjava/lang/String;Lcom/epson/epos2/printer/HybridPrinterStatusInfo;)V

    :cond_0
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    .line 985
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v18

    aput-object p4, v0, v16

    aput-object v6, v0, v19

    invoke-virtual {v6, v14, v2, v0}, Lcom/epson/epos2/printer/HybridPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void
.end method

.method private onHybdStatusChange(I)V
    .locals 6

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/Object;

    .line 941
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x1

    aput-object p0, v1, v2

    const-string v4, "onHybdStatusChange"

    invoke-virtual {p0, v4, v1}, Lcom/epson/epos2/printer/HybridPrinter;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 942
    iget-object v1, p0, Lcom/epson/epos2/printer/HybridPrinter;->mStatusChangeListener:Lcom/epson/epos2/printer/HybdStatusChangeListener;

    if-eqz v1, :cond_0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v5, "eventType->"

    aput-object v5, v1, v3

    .line 944
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v2

    .line 943
    invoke-virtual {p0, v4, v1}, Lcom/epson/epos2/printer/HybridPrinter;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 945
    iget-object v1, p0, Lcom/epson/epos2/printer/HybridPrinter;->mStatusChangeListener:Lcom/epson/epos2/printer/HybdStatusChangeListener;

    invoke-interface {v1, p0, p1}, Lcom/epson/epos2/printer/HybdStatusChangeListener;->onHybdStatusChange(Lcom/epson/epos2/printer/HybridPrinter;I)V

    :cond_0
    new-array v0, v0, [Ljava/lang/Object;

    .line 947
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    aput-object p0, v0, v2

    invoke-virtual {p0, v4, v3, v0}, Lcom/epson/epos2/printer/HybridPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic addBarcode(Ljava/lang/String;IIIII)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 10
    invoke-super/range {p0 .. p6}, Lcom/epson/epos2/printer/CommonPrinter;->addBarcode(Ljava/lang/String;IIIII)V

    return-void
.end method

.method public bridge synthetic addCommand([B)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 10
    invoke-super {p0, p1}, Lcom/epson/epos2/printer/CommonPrinter;->addCommand([B)V

    return-void
.end method

.method public bridge synthetic addCut(I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 10
    invoke-super {p0, p1}, Lcom/epson/epos2/printer/CommonPrinter;->addCut(I)V

    return-void
.end method

.method public bridge synthetic addFeedLine(I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 10
    invoke-super {p0, p1}, Lcom/epson/epos2/printer/CommonPrinter;->addFeedLine(I)V

    return-void
.end method

.method public bridge synthetic addFeedUnit(I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 10
    invoke-super {p0, p1}, Lcom/epson/epos2/printer/CommonPrinter;->addFeedUnit(I)V

    return-void
.end method

.method public bridge synthetic addHPosition(I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 10
    invoke-super {p0, p1}, Lcom/epson/epos2/printer/CommonPrinter;->addHPosition(I)V

    return-void
.end method

.method public bridge synthetic addImage(Landroid/graphics/Bitmap;IIIIIIIDI)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 10
    invoke-super/range {p0 .. p11}, Lcom/epson/epos2/printer/CommonPrinter;->addImage(Landroid/graphics/Bitmap;IIIIIIIDI)V

    return-void
.end method

.method public bridge synthetic addLineSpace(I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 10
    invoke-super {p0, p1}, Lcom/epson/epos2/printer/CommonPrinter;->addLineSpace(I)V

    return-void
.end method

.method public bridge synthetic addLogo(II)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 10
    invoke-super {p0, p1, p2}, Lcom/epson/epos2/printer/CommonPrinter;->addLogo(II)V

    return-void
.end method

.method public bridge synthetic addPageArea(IIII)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 10
    invoke-super {p0, p1, p2, p3, p4}, Lcom/epson/epos2/printer/CommonPrinter;->addPageArea(IIII)V

    return-void
.end method

.method public bridge synthetic addPageBegin()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 10
    invoke-super {p0}, Lcom/epson/epos2/printer/CommonPrinter;->addPageBegin()V

    return-void
.end method

.method public bridge synthetic addPageDirection(I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 10
    invoke-super {p0, p1}, Lcom/epson/epos2/printer/CommonPrinter;->addPageDirection(I)V

    return-void
.end method

.method public bridge synthetic addPageEnd()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 10
    invoke-super {p0}, Lcom/epson/epos2/printer/CommonPrinter;->addPageEnd()V

    return-void
.end method

.method public bridge synthetic addPageLine(IIIII)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 10
    invoke-super/range {p0 .. p5}, Lcom/epson/epos2/printer/CommonPrinter;->addPageLine(IIIII)V

    return-void
.end method

.method public bridge synthetic addPagePosition(II)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 10
    invoke-super {p0, p1, p2}, Lcom/epson/epos2/printer/CommonPrinter;->addPagePosition(II)V

    return-void
.end method

.method public bridge synthetic addPageRectangle(IIIII)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 10
    invoke-super/range {p0 .. p5}, Lcom/epson/epos2/printer/CommonPrinter;->addPageRectangle(IIIII)V

    return-void
.end method

.method public bridge synthetic addPulse(II)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 10
    invoke-super {p0, p1, p2}, Lcom/epson/epos2/printer/CommonPrinter;->addPulse(II)V

    return-void
.end method

.method public bridge synthetic addSymbol(Ljava/lang/String;IIIII)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 10
    invoke-super/range {p0 .. p6}, Lcom/epson/epos2/printer/CommonPrinter;->addSymbol(Ljava/lang/String;IIIII)V

    return-void
.end method

.method public bridge synthetic addText(Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 10
    invoke-super {p0, p1}, Lcom/epson/epos2/printer/CommonPrinter;->addText(Ljava/lang/String;)V

    return-void
.end method

.method public bridge synthetic addTextAlign(I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 10
    invoke-super {p0, p1}, Lcom/epson/epos2/printer/CommonPrinter;->addTextAlign(I)V

    return-void
.end method

.method public bridge synthetic addTextFont(I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 10
    invoke-super {p0, p1}, Lcom/epson/epos2/printer/CommonPrinter;->addTextFont(I)V

    return-void
.end method

.method public bridge synthetic addTextLang(I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 10
    invoke-super {p0, p1}, Lcom/epson/epos2/printer/CommonPrinter;->addTextLang(I)V

    return-void
.end method

.method public bridge synthetic addTextRotate(I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 10
    invoke-super {p0, p1}, Lcom/epson/epos2/printer/CommonPrinter;->addTextRotate(I)V

    return-void
.end method

.method public bridge synthetic addTextSize(II)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 10
    invoke-super {p0, p1, p2}, Lcom/epson/epos2/printer/CommonPrinter;->addTextSize(II)V

    return-void
.end method

.method public bridge synthetic addTextSmooth(I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 10
    invoke-super {p0, p1}, Lcom/epson/epos2/printer/CommonPrinter;->addTextSmooth(I)V

    return-void
.end method

.method public bridge synthetic addTextStyle(IIII)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 10
    invoke-super {p0, p1, p2, p3, p4}, Lcom/epson/epos2/printer/CommonPrinter;->addTextStyle(IIII)V

    return-void
.end method

.method public bridge synthetic beginTransaction()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 10
    invoke-super {p0}, Lcom/epson/epos2/printer/CommonPrinter;->beginTransaction()V

    return-void
.end method

.method public cancelInsertion()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "cancelInsertion"

    .line 455
    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/HybridPrinter;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 459
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/printer/HybridPrinter;->checkHandle()V

    .line 461
    iget-wide v3, p0, Lcom/epson/epos2/printer/HybridPrinter;->mPrinterHandle:J

    invoke-virtual {p0, v3, v4}, Lcom/epson/epos2/printer/HybridPrinter;->nativeEpos2CancelInsertion(J)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v1, v0, [Ljava/lang/Object;

    .line 472
    invoke-virtual {p0, v2, v0, v1}, Lcom/epson/epos2/printer/HybridPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 463
    :cond_0
    :try_start_1
    new-instance v3, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v3, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v3
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 467
    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/HybridPrinter;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 468
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v3

    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {p0, v2, v3, v0}, Lcom/epson/epos2/printer/HybridPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 469
    throw v1
.end method

.method public cleanMicrReader(I)V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/Object;

    .line 576
    iget v2, p0, Lcom/epson/epos2/printer/HybridPrinter;->mWaitTime:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v4, 0x1

    aput-object v2, v1, v4

    const-string v2, "cleanMicrReader"

    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/HybridPrinter;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 580
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/printer/HybridPrinter;->checkHandle()V

    .line 582
    iget-wide v6, p0, Lcom/epson/epos2/printer/HybridPrinter;->mPrinterHandle:J

    iget v1, p0, Lcom/epson/epos2/printer/HybridPrinter;->mWaitTime:I

    int-to-long v8, v1

    int-to-long v10, p1

    move-object v5, p0

    invoke-virtual/range {v5 .. v11}, Lcom/epson/epos2/printer/HybridPrinter;->nativeEpos2CleanMicrReader(JJJ)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 593
    iget v1, p0, Lcom/epson/epos2/printer/HybridPrinter;->mWaitTime:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v4

    invoke-virtual {p0, v2, v3, v0}, Lcom/epson/epos2/printer/HybridPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 584
    :cond_0
    :try_start_1
    new-instance v5, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v5, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v5
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 588
    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/HybridPrinter;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 589
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v5

    new-array v0, v0, [Ljava/lang/Object;

    iget v6, p0, Lcom/epson/epos2/printer/HybridPrinter;->mWaitTime:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v0, v3

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v4

    invoke-virtual {p0, v2, v5, v0}, Lcom/epson/epos2/printer/HybridPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 590
    throw v1
.end method

.method public bridge synthetic clearCommandBuffer()V
    .locals 0

    .line 10
    invoke-super {p0}, Lcom/epson/epos2/printer/CommonPrinter;->clearCommandBuffer()V

    return-void
.end method

.method public connect(Ljava/lang/String;I)V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    .line 222
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v1, v4

    const-string v3, "connect"

    invoke-virtual {p0, v3, v1}, Lcom/epson/epos2/printer/HybridPrinter;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    if-eqz p1, :cond_1

    .line 231
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/printer/HybridPrinter;->checkHandle()V

    .line 233
    iget-wide v6, p0, Lcom/epson/epos2/printer/HybridPrinter;->mPrinterHandle:J

    int-to-long v9, p2

    iget-object v11, p0, Lcom/epson/epos2/printer/HybridPrinter;->mContext:Landroid/content/Context;

    move-object v5, p0

    move-object v8, p1

    invoke-virtual/range {v5 .. v11}, Lcom/epson/epos2/printer/HybridPrinter;->nativeEpos2Connect(JLjava/lang/String;JLjava/lang/Object;)I

    move-result v1

    if-nez v1, :cond_0

    .line 237
    iget-wide v5, p0, Lcom/epson/epos2/printer/HybridPrinter;->mPrinterHandle:J

    iget v1, p0, Lcom/epson/epos2/printer/HybridPrinter;->mInterval:I

    int-to-long v7, v1

    invoke-virtual {p0, v5, v6, v7, v8}, Lcom/epson/epos2/printer/HybridPrinter;->nativeEpos2SetInterval(JJ)I
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    .line 248
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v4

    invoke-virtual {p0, v3, v2, v0}, Lcom/epson/epos2/printer/HybridPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 235
    :cond_0
    :try_start_1
    new-instance v5, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v5, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v5

    :catch_0
    move-exception v1

    goto :goto_0

    .line 227
    :cond_1
    new-instance v1, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v1, v4}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v1
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 243
    :goto_0
    invoke-virtual {p0, v3, v1}, Lcom/epson/epos2/printer/HybridPrinter;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 244
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v5

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v4

    invoke-virtual {p0, v3, v5, v0}, Lcom/epson/epos2/printer/HybridPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 245
    throw v1
.end method

.method public disconnect()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "disconnect"

    .line 268
    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/HybridPrinter;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 272
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/printer/HybridPrinter;->checkHandle()V

    .line 274
    iget-wide v3, p0, Lcom/epson/epos2/printer/HybridPrinter;->mPrinterHandle:J

    invoke-virtual {p0, v3, v4}, Lcom/epson/epos2/printer/HybridPrinter;->nativeEpos2Disconnect(J)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v1, v0, [Ljava/lang/Object;

    .line 285
    invoke-virtual {p0, v2, v0, v1}, Lcom/epson/epos2/printer/HybridPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 276
    :cond_0
    :try_start_1
    new-instance v3, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v3, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v3
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 280
    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/HybridPrinter;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 281
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v3

    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {p0, v2, v3, v0}, Lcom/epson/epos2/printer/HybridPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 282
    throw v1
.end method

.method public ejectPaper()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "ejectPaper"

    .line 491
    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/HybridPrinter;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 495
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/printer/HybridPrinter;->checkHandle()V

    .line 497
    iget-wide v3, p0, Lcom/epson/epos2/printer/HybridPrinter;->mPrinterHandle:J

    invoke-virtual {p0, v3, v4}, Lcom/epson/epos2/printer/HybridPrinter;->nativeEpos2EjectPaper(J)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v1, v0, [Ljava/lang/Object;

    .line 508
    invoke-virtual {p0, v2, v0, v1}, Lcom/epson/epos2/printer/HybridPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 499
    :cond_0
    :try_start_1
    new-instance v3, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v3, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v3
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 503
    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/HybridPrinter;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 504
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v3

    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {p0, v2, v3, v0}, Lcom/epson/epos2/printer/HybridPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 505
    throw v1
.end method

.method public bridge synthetic endTransaction()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 10
    invoke-super {p0}, Lcom/epson/epos2/printer/CommonPrinter;->endTransaction()V

    return-void
.end method

.method protected finalize()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "finalize"

    .line 174
    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/HybridPrinter;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v1, 0x0

    .line 176
    iput-object v1, p0, Lcom/epson/epos2/printer/HybridPrinter;->mStatusChangeListener:Lcom/epson/epos2/printer/HybdStatusChangeListener;

    .line 177
    iput-object v1, p0, Lcom/epson/epos2/printer/HybridPrinter;->mReceiveListener:Lcom/epson/epos2/printer/HybdReceiveListener;

    .line 178
    iput-object v1, p0, Lcom/epson/epos2/printer/HybridPrinter;->mConnectionListener:Lcom/epson/epos2/ConnectionListener;

    .line 181
    :try_start_0
    iget-wide v3, p0, Lcom/epson/epos2/printer/HybridPrinter;->mPrinterHandle:J

    const-wide/16 v5, 0x0

    cmp-long v1, v3, v5

    if-eqz v1, :cond_0

    .line 182
    iget-wide v3, p0, Lcom/epson/epos2/printer/HybridPrinter;->mPrinterHandle:J

    invoke-virtual {p0, v3, v4}, Lcom/epson/epos2/printer/HybridPrinter;->nativeEpos2Disconnect(J)I

    .line 183
    iget-wide v3, p0, Lcom/epson/epos2/printer/HybridPrinter;->mPrinterHandle:J

    invoke-virtual {p0, v3, v4}, Lcom/epson/epos2/printer/HybridPrinter;->nativeEpos2ClearCommandBuffer(J)I

    .line 184
    iget-wide v3, p0, Lcom/epson/epos2/printer/HybridPrinter;->mPrinterHandle:J

    invoke-virtual {p0, v3, v4}, Lcom/epson/epos2/printer/HybridPrinter;->nativeEpos2DestroyHandle(J)I

    .line 185
    iput-wide v5, p0, Lcom/epson/epos2/printer/HybridPrinter;->mPrinterHandle:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 189
    :cond_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    new-array v1, v0, [Ljava/lang/Object;

    .line 192
    invoke-virtual {p0, v2, v0, v1}, Lcom/epson/epos2/printer/HybridPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    :catchall_0
    move-exception v0

    .line 189
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 190
    throw v0
.end method

.method public forceCommand([BI)V
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    .line 616
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x1

    aput-object v3, v1, v4

    const-string v3, "forceCommand"

    invoke-virtual {p0, v3, v1}, Lcom/epson/epos2/printer/HybridPrinter;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 620
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/printer/HybridPrinter;->checkHandle()V

    .line 622
    iget-wide v6, p0, Lcom/epson/epos2/printer/HybridPrinter;->mPrinterHandle:J

    iget v9, p0, Lcom/epson/epos2/printer/HybridPrinter;->mMode40Cpl:I

    move-object v5, p0

    move-object v8, p1

    move v10, p2

    invoke-virtual/range {v5 .. v10}, Lcom/epson/epos2/printer/HybridPrinter;->nativeEpos2ForceCommand(J[BII)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    .line 633
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v4

    invoke-virtual {p0, v3, v2, v0}, Lcom/epson/epos2/printer/HybridPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 624
    :cond_0
    :try_start_1
    new-instance v5, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v5, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v5
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 628
    invoke-virtual {p0, v3, v1}, Lcom/epson/epos2/printer/HybridPrinter;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 629
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v5

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v4

    invoke-virtual {p0, v3, v5, v0}, Lcom/epson/epos2/printer/HybridPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 630
    throw v1
.end method

.method public bridge synthetic forcePulse(III)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 10
    invoke-super {p0, p1, p2, p3}, Lcom/epson/epos2/printer/CommonPrinter;->forcePulse(III)V

    return-void
.end method

.method public bridge synthetic forceRecover(I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 10
    invoke-super {p0, p1}, Lcom/epson/epos2/printer/CommonPrinter;->forceRecover(I)V

    return-void
.end method

.method public bridge synthetic forceReset(I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 10
    invoke-super {p0, p1}, Lcom/epson/epos2/printer/CommonPrinter;->forceReset(I)V

    return-void
.end method

.method public getAdmin()Ljava/lang/String;
    .locals 8

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "getAdmin"

    .line 894
    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/HybridPrinter;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 896
    iget-wide v3, p0, Lcom/epson/epos2/printer/HybridPrinter;->mPrinterHandle:J

    const/16 v1, 0x100

    const-wide/16 v5, 0x0

    cmp-long v7, v3, v5

    if-nez v7, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 897
    invoke-virtual {p0, v2, v1, v0}, Lcom/epson/epos2/printer/HybridPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    const-string v0, ""

    return-object v0

    .line 901
    :cond_0
    iget-wide v3, p0, Lcom/epson/epos2/printer/HybridPrinter;->mPrinterHandle:J

    invoke-virtual {p0, v3, v4}, Lcom/epson/epos2/printer/HybridPrinter;->nativeEpos2GetAdmin(J)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v3, v1, v0

    .line 903
    invoke-virtual {p0, v2, v0, v1}, Lcom/epson/epos2/printer/HybridPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    new-array v0, v0, [Ljava/lang/Object;

    .line 906
    invoke-virtual {p0, v2, v1, v0}, Lcom/epson/epos2/printer/HybridPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    :goto_0
    return-object v3
.end method

.method public bridge synthetic getInterval()I
    .locals 1

    .line 10
    invoke-super {p0}, Lcom/epson/epos2/printer/CommonPrinter;->getInterval()I

    move-result v0

    return v0
.end method

.method public getLocation()Ljava/lang/String;
    .locals 8

    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Object;

    const-string v2, "getLocation"

    .line 921
    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/HybridPrinter;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 923
    iget-wide v3, p0, Lcom/epson/epos2/printer/HybridPrinter;->mPrinterHandle:J

    const/16 v1, 0x100

    const-wide/16 v5, 0x0

    cmp-long v7, v3, v5

    if-nez v7, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 924
    invoke-virtual {p0, v2, v1, v0}, Lcom/epson/epos2/printer/HybridPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    const-string v0, ""

    return-object v0

    .line 928
    :cond_0
    iget-wide v3, p0, Lcom/epson/epos2/printer/HybridPrinter;->mPrinterHandle:J

    invoke-virtual {p0, v3, v4}, Lcom/epson/epos2/printer/HybridPrinter;->nativeEpos2GetLocation(J)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v3, v1, v0

    .line 930
    invoke-virtual {p0, v2, v0, v1}, Lcom/epson/epos2/printer/HybridPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    new-array v0, v0, [Ljava/lang/Object;

    .line 933
    invoke-virtual {p0, v2, v1, v0}, Lcom/epson/epos2/printer/HybridPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    :goto_0
    return-object v3
.end method

.method public getMode40Cpl()I
    .locals 1

    .line 873
    iget v0, p0, Lcom/epson/epos2/printer/HybridPrinter;->mMode40Cpl:I

    return v0
.end method

.method public getPaperType()I
    .locals 1

    .line 882
    iget v0, p0, Lcom/epson/epos2/printer/HybridPrinter;->mPaperType:I

    return v0
.end method

.method public getStatus()Lcom/epson/epos2/printer/HybridPrinterStatusInfo;
    .locals 18

    move-object/from16 v0, p0

    const/4 v1, 0x0

    new-array v2, v1, [Ljava/lang/Object;

    const-string v3, "getStatus"

    .line 296
    invoke-virtual {v0, v3, v2}, Lcom/epson/epos2/printer/HybridPrinter;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 298
    iget-wide v4, v0, Lcom/epson/epos2/printer/HybridPrinter;->mPrinterHandle:J

    invoke-virtual {v0, v4, v5}, Lcom/epson/epos2/printer/HybridPrinter;->nativeEpos2GetStatus(J)Lcom/epson/epos2/printer/HybridPrinterStatusInfo;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 300
    invoke-virtual {v2}, Lcom/epson/epos2/printer/HybridPrinterStatusInfo;->getConnection()I

    move-result v4

    .line 301
    invoke-virtual {v2}, Lcom/epson/epos2/printer/HybridPrinterStatusInfo;->getOnline()I

    move-result v5

    .line 302
    invoke-virtual {v2}, Lcom/epson/epos2/printer/HybridPrinterStatusInfo;->getCoverOpen()I

    move-result v6

    .line 303
    invoke-virtual {v2}, Lcom/epson/epos2/printer/HybridPrinterStatusInfo;->getPaper()I

    move-result v7

    .line 304
    invoke-virtual {v2}, Lcom/epson/epos2/printer/HybridPrinterStatusInfo;->getPaperFeed()I

    move-result v8

    .line 305
    invoke-virtual {v2}, Lcom/epson/epos2/printer/HybridPrinterStatusInfo;->getPanelSwitch()I

    move-result v9

    .line 306
    invoke-virtual {v2}, Lcom/epson/epos2/printer/HybridPrinterStatusInfo;->getOnline()I

    move-result v10

    .line 307
    invoke-virtual {v2}, Lcom/epson/epos2/printer/HybridPrinterStatusInfo;->getDrawer()I

    move-result v11

    .line 308
    invoke-virtual {v2}, Lcom/epson/epos2/printer/HybridPrinterStatusInfo;->getErrorStatus()I

    move-result v12

    .line 309
    invoke-virtual {v2}, Lcom/epson/epos2/printer/HybridPrinterStatusInfo;->getAutoRecoverError()I

    move-result v13

    .line 310
    invoke-virtual {v2}, Lcom/epson/epos2/printer/HybridPrinterStatusInfo;->getInsertionWaiting()I

    move-result v14

    .line 311
    invoke-virtual {v2}, Lcom/epson/epos2/printer/HybridPrinterStatusInfo;->getRemovalWaiting()I

    move-result v15

    .line 312
    invoke-virtual {v2}, Lcom/epson/epos2/printer/HybridPrinterStatusInfo;->getSlipPaper()I

    move-result v1

    move-object/from16 v16, v2

    const/16 v2, 0xd

    new-array v2, v2, [Ljava/lang/Object;

    .line 314
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v17, v3

    const-string v3, "connection->"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x0

    aput-object v0, v2, v3

    const/4 v0, 0x1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "online->"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "coverOpen->"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "paper->"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x4

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "paperFeed->"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x5

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "panelSwitch->"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x6

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "waitOnline->"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x7

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "drawer->"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const/16 v0, 0x8

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "errorStatus->"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const/16 v0, 0x9

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "autoRecoverError->"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const/16 v0, 0xa

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "insertionWaiting->"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const/16 v0, 0xb

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "removalWaiting->"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const/16 v0, 0xc

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "slipPaper->"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v0

    const/4 v3, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v3, v2}, Lcom/epson/epos2/printer/HybridPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    move-object/from16 v16, v2

    move-object v1, v3

    const/4 v3, 0x0

    const/16 v2, 0x101

    new-array v3, v3, [Ljava/lang/Object;

    .line 319
    invoke-virtual {v0, v1, v2, v3}, Lcom/epson/epos2/printer/HybridPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    :goto_0
    return-object v16
.end method

.method public getWaitTime()I
    .locals 1

    .line 830
    iget v0, p0, Lcom/epson/epos2/printer/HybridPrinter;->mWaitTime:I

    return v0
.end method

.method protected native nativeEpos2CancelInsertion(J)I
.end method

.method protected native nativeEpos2CleanMicrReader(JJJ)I
.end method

.method protected native nativeEpos2Connect(JLjava/lang/String;JLjava/lang/Object;)I
.end method

.method protected native nativeEpos2CreateHandle(I[J)I
.end method

.method protected native nativeEpos2DestroyHandle(J)I
.end method

.method protected native nativeEpos2Disconnect(J)I
.end method

.method protected native nativeEpos2EjectPaper(J)I
.end method

.method protected native nativeEpos2ForceCommand(J[BII)I
.end method

.method protected native nativeEpos2GetAdmin(J)Ljava/lang/String;
.end method

.method protected native nativeEpos2GetLocation(J)Ljava/lang/String;
.end method

.method protected native nativeEpos2GetStatus(J)Lcom/epson/epos2/printer/HybridPrinterStatusInfo;
.end method

.method protected native nativeEpos2ReadMicrData(JIJJ)I
.end method

.method protected native nativeEpos2SelectPaperType(JI)I
.end method

.method protected native nativeEpos2SendData(JIJI)I
.end method

.method protected native nativeEpos2SetMode40Cpl(JI)I
.end method

.method protected native nativeEpos2SetWaitTime(JJ)I
.end method

.method protected native nativeEpos2WaitInsertion(JJJ)I
.end method

.method public readMicrData(II)V
    .locals 16
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    move-object/from16 v9, p0

    const/4 v10, 0x3

    new-array v0, v10, [Ljava/lang/Object;

    .line 534
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v11, 0x0

    aput-object v1, v0, v11

    iget v1, v9, Lcom/epson/epos2/printer/HybridPrinter;->mWaitTime:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v12, 0x1

    aput-object v1, v0, v12

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v13, 0x2

    aput-object v1, v0, v13

    const-string v14, "readMicrData"

    invoke-virtual {v9, v14, v0}, Lcom/epson/epos2/printer/HybridPrinter;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 538
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/epson/epos2/printer/HybridPrinter;->checkHandle()V

    .line 540
    iget-wide v2, v9, Lcom/epson/epos2/printer/HybridPrinter;->mPrinterHandle:J

    iget v0, v9, Lcom/epson/epos2/printer/HybridPrinter;->mWaitTime:I
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_1

    int-to-long v5, v0

    move/from16 v15, p2

    int-to-long v7, v15

    move-object/from16 v1, p0

    move/from16 v4, p1

    :try_start_1
    invoke-virtual/range {v1 .. v8}, Lcom/epson/epos2/printer/HybridPrinter;->nativeEpos2ReadMicrData(JIJJ)I

    move-result v0
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    if-nez v0, :cond_0

    new-array v0, v10, [Ljava/lang/Object;

    .line 551
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v11

    iget v1, v9, Lcom/epson/epos2/printer/HybridPrinter;->mWaitTime:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v12

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v13

    invoke-virtual {v9, v14, v11, v0}, Lcom/epson/epos2/printer/HybridPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 542
    :cond_0
    :try_start_2
    new-instance v1, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v1, v0}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v1
    :try_end_2
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    move/from16 v15, p2

    .line 546
    :goto_0
    invoke-virtual {v9, v14, v0}, Lcom/epson/epos2/printer/HybridPrinter;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 547
    invoke-virtual {v0}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v1

    new-array v2, v10, [Ljava/lang/Object;

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v11

    iget v3, v9, Lcom/epson/epos2/printer/HybridPrinter;->mWaitTime:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v12

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v13

    invoke-virtual {v9, v14, v1, v2}, Lcom/epson/epos2/printer/HybridPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 548
    throw v0
.end method

.method public selectPaperType(I)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 377
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "selectPaperType"

    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/HybridPrinter;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 381
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/printer/HybridPrinter;->checkHandle()V

    .line 383
    iget-wide v4, p0, Lcom/epson/epos2/printer/HybridPrinter;->mPrinterHandle:J

    invoke-virtual {p0, v4, v5, p1}, Lcom/epson/epos2/printer/HybridPrinter;->nativeEpos2SelectPaperType(JI)I

    move-result v1

    if-nez v1, :cond_0

    .line 388
    iput p1, p0, Lcom/epson/epos2/printer/HybridPrinter;->mPaperType:I
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 396
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {p0, v2, v3, v0}, Lcom/epson/epos2/printer/HybridPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 385
    :cond_0
    :try_start_1
    new-instance v4, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v4, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v4
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 391
    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/HybridPrinter;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 392
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v4

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {p0, v2, v4, v0}, Lcom/epson/epos2/printer/HybridPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 393
    throw v1
.end method

.method public sendData(I)V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/Object;

    .line 343
    iget v2, p0, Lcom/epson/epos2/printer/HybridPrinter;->mMode40Cpl:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v4, 0x1

    aput-object v2, v1, v4

    const-string v2, "sendData"

    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/HybridPrinter;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 347
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/printer/HybridPrinter;->checkHandle()V

    .line 349
    iget-wide v6, p0, Lcom/epson/epos2/printer/HybridPrinter;->mPrinterHandle:J

    iget v8, p0, Lcom/epson/epos2/printer/HybridPrinter;->mMode40Cpl:I

    int-to-long v9, p1

    const/4 v11, 0x0

    move-object v5, p0

    invoke-virtual/range {v5 .. v11}, Lcom/epson/epos2/printer/HybridPrinter;->nativeEpos2SendData(JIJI)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 360
    iget v1, p0, Lcom/epson/epos2/printer/HybridPrinter;->mMode40Cpl:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v4

    invoke-virtual {p0, v2, v3, v0}, Lcom/epson/epos2/printer/HybridPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 351
    :cond_0
    :try_start_1
    new-instance v5, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v5, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v5
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 355
    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/HybridPrinter;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 356
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v5

    new-array v0, v0, [Ljava/lang/Object;

    iget v6, p0, Lcom/epson/epos2/printer/HybridPrinter;->mMode40Cpl:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v0, v3

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v4

    invoke-virtual {p0, v2, v5, v0}, Lcom/epson/epos2/printer/HybridPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 357
    throw v1
.end method

.method public setConnectionEventListener(Lcom/epson/epos2/ConnectionListener;)V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "setConnectionEventListener"

    .line 776
    invoke-virtual {p0, v1, v0}, Lcom/epson/epos2/printer/HybridPrinter;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 778
    iget-wide v0, p0, Lcom/epson/epos2/printer/HybridPrinter;->mPrinterHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_1

    .line 783
    iput-object p1, p0, Lcom/epson/epos2/printer/HybridPrinter;->mConnectionListener:Lcom/epson/epos2/ConnectionListener;

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 786
    iput-object p1, p0, Lcom/epson/epos2/printer/HybridPrinter;->mConnectionListener:Lcom/epson/epos2/ConnectionListener;

    :goto_0
    return-void
.end method

.method public bridge synthetic setInterval(I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 10
    invoke-super {p0, p1}, Lcom/epson/epos2/printer/CommonPrinter;->setInterval(I)V

    return-void
.end method

.method public setMode40Cpl(I)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 845
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "setMode40Cpl"

    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/HybridPrinter;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 849
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/printer/HybridPrinter;->checkHandle()V

    .line 851
    iget-wide v4, p0, Lcom/epson/epos2/printer/HybridPrinter;->mPrinterHandle:J

    invoke-virtual {p0, v4, v5, p1}, Lcom/epson/epos2/printer/HybridPrinter;->nativeEpos2SetMode40Cpl(JI)I

    move-result v1

    if-nez v1, :cond_0

    .line 856
    iput p1, p0, Lcom/epson/epos2/printer/HybridPrinter;->mMode40Cpl:I
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 864
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {p0, v2, v3, v0}, Lcom/epson/epos2/printer/HybridPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 853
    :cond_0
    :try_start_1
    new-instance v4, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v4, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v4
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 859
    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/HybridPrinter;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 860
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v4

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {p0, v2, v4, v0}, Lcom/epson/epos2/printer/HybridPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 861
    throw v1
.end method

.method public setReceiveEventListener(Lcom/epson/epos2/printer/HybdReceiveListener;)V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "setReceiveEventListener"

    .line 755
    invoke-virtual {p0, v1, v0}, Lcom/epson/epos2/printer/HybridPrinter;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 757
    iget-wide v0, p0, Lcom/epson/epos2/printer/HybridPrinter;->mPrinterHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_1

    .line 762
    iput-object p1, p0, Lcom/epson/epos2/printer/HybridPrinter;->mReceiveListener:Lcom/epson/epos2/printer/HybdReceiveListener;

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 765
    iput-object p1, p0, Lcom/epson/epos2/printer/HybridPrinter;->mReceiveListener:Lcom/epson/epos2/printer/HybdReceiveListener;

    :goto_0
    return-void
.end method

.method public setStatusChangeEventListener(Lcom/epson/epos2/printer/HybdStatusChangeListener;)V
    .locals 5

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "setStatusChangeEventListener"

    .line 734
    invoke-virtual {p0, v1, v0}, Lcom/epson/epos2/printer/HybridPrinter;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 736
    iget-wide v0, p0, Lcom/epson/epos2/printer/HybridPrinter;->mPrinterHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_1

    .line 741
    iput-object p1, p0, Lcom/epson/epos2/printer/HybridPrinter;->mStatusChangeListener:Lcom/epson/epos2/printer/HybdStatusChangeListener;

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    .line 744
    iput-object p1, p0, Lcom/epson/epos2/printer/HybridPrinter;->mStatusChangeListener:Lcom/epson/epos2/printer/HybdStatusChangeListener;

    :goto_0
    return-void
.end method

.method public setWaitTime(I)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    .line 802
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-string v2, "setWaitTime"

    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/HybridPrinter;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 806
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/printer/HybridPrinter;->checkHandle()V

    .line 808
    iget-wide v4, p0, Lcom/epson/epos2/printer/HybridPrinter;->mPrinterHandle:J

    int-to-long v6, p1

    invoke-virtual {p0, v4, v5, v6, v7}, Lcom/epson/epos2/printer/HybridPrinter;->nativeEpos2SetWaitTime(JJ)I

    move-result v1

    if-nez v1, :cond_0

    .line 813
    iput p1, p0, Lcom/epson/epos2/printer/HybridPrinter;->mWaitTime:I
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 821
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {p0, v2, v3, v0}, Lcom/epson/epos2/printer/HybridPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 810
    :cond_0
    :try_start_1
    new-instance v4, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v4, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v4
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 816
    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/HybridPrinter;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 817
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v4

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v3

    invoke-virtual {p0, v2, v4, v0}, Lcom/epson/epos2/printer/HybridPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 818
    throw v1
.end method

.method public bridge synthetic startMonitor()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 10
    invoke-super {p0}, Lcom/epson/epos2/printer/CommonPrinter;->startMonitor()V

    return-void
.end method

.method public bridge synthetic stopMonitor()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    .line 10
    invoke-super {p0}, Lcom/epson/epos2/printer/CommonPrinter;->stopMonitor()V

    return-void
.end method

.method public waitInsertion(I)V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/epos2/Epos2Exception;
        }
    .end annotation

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/Object;

    .line 419
    iget v2, p0, Lcom/epson/epos2/printer/HybridPrinter;->mWaitTime:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v4, 0x1

    aput-object v2, v1, v4

    const-string v2, "waitInsertion"

    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/HybridPrinter;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 423
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/epos2/printer/HybridPrinter;->checkHandle()V

    .line 425
    iget-wide v6, p0, Lcom/epson/epos2/printer/HybridPrinter;->mPrinterHandle:J

    iget v1, p0, Lcom/epson/epos2/printer/HybridPrinter;->mWaitTime:I

    int-to-long v8, v1

    int-to-long v10, p1

    move-object v5, p0

    invoke-virtual/range {v5 .. v11}, Lcom/epson/epos2/printer/HybridPrinter;->nativeEpos2WaitInsertion(JJJ)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    .line 436
    iget v1, p0, Lcom/epson/epos2/printer/HybridPrinter;->mWaitTime:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v4

    invoke-virtual {p0, v2, v3, v0}, Lcom/epson/epos2/printer/HybridPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    return-void

    .line 427
    :cond_0
    :try_start_1
    new-instance v5, Lcom/epson/epos2/Epos2Exception;

    invoke-direct {v5, v1}, Lcom/epson/epos2/Epos2Exception;-><init>(I)V

    throw v5
    :try_end_1
    .catch Lcom/epson/epos2/Epos2Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v1

    .line 431
    invoke-virtual {p0, v2, v1}, Lcom/epson/epos2/printer/HybridPrinter;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 432
    invoke-virtual {v1}, Lcom/epson/epos2/Epos2Exception;->getErrorStatus()I

    move-result v5

    new-array v0, v0, [Ljava/lang/Object;

    iget v6, p0, Lcom/epson/epos2/printer/HybridPrinter;->mWaitTime:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v0, v3

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v0, v4

    invoke-virtual {p0, v2, v5, v0}, Lcom/epson/epos2/printer/HybridPrinter;->outputLogReturnFunction(Ljava/lang/String;I[Ljava/lang/Object;)V

    .line 433
    throw v1
.end method
