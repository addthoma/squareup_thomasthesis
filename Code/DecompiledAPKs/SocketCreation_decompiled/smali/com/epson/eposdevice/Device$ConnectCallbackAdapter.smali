.class Lcom/epson/eposdevice/Device$ConnectCallbackAdapter;
.super Ljava/lang/Object;
.source "Device.java"

# interfaces
.implements Lcom/epson/eposdevice/NativeDevice$NativeConnectCallbackAdapter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/eposdevice/Device;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ConnectCallbackAdapter"
.end annotation


# instance fields
.field private mListener:Lcom/epson/eposdevice/ConnectListener;

.field final synthetic this$0:Lcom/epson/eposdevice/Device;


# direct methods
.method public constructor <init>(Lcom/epson/eposdevice/Device;Lcom/epson/eposdevice/ConnectListener;)V
    .locals 0

    .line 385
    iput-object p1, p0, Lcom/epson/eposdevice/Device$ConnectCallbackAdapter;->this$0:Lcom/epson/eposdevice/Device;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p1, 0x0

    .line 383
    iput-object p1, p0, Lcom/epson/eposdevice/Device$ConnectCallbackAdapter;->mListener:Lcom/epson/eposdevice/ConnectListener;

    .line 386
    iput-object p2, p0, Lcom/epson/eposdevice/Device$ConnectCallbackAdapter;->mListener:Lcom/epson/eposdevice/ConnectListener;

    return-void
.end method


# virtual methods
.method public nativeOnConnect(Ljava/lang/String;I)V
    .locals 4

    .line 390
    iget-object v0, p0, Lcom/epson/eposdevice/Device$ConnectCallbackAdapter;->mListener:Lcom/epson/eposdevice/ConnectListener;

    if-eqz v0, :cond_0

    .line 391
    iget-object v0, p0, Lcom/epson/eposdevice/Device$ConnectCallbackAdapter;->this$0:Lcom/epson/eposdevice/Device;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const-string v2, "onConnect"

    invoke-virtual {v0, v2, v1}, Lcom/epson/eposdevice/Device;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 392
    iget-object v0, p0, Lcom/epson/eposdevice/Device$ConnectCallbackAdapter;->mListener:Lcom/epson/eposdevice/ConnectListener;

    invoke-interface {v0, p1, p2}, Lcom/epson/eposdevice/ConnectListener;->onConnect(Ljava/lang/String;I)V

    :cond_0
    return-void
.end method
