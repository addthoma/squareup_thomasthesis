.class public Lcom/epson/eposdevice/commbox/CommBox$CommBoxSendDataCallbackAdapter;
.super Ljava/lang/Object;
.source "CommBox.java"

# interfaces
.implements Lcom/epson/eposdevice/commbox/NativeCommBox$NativeCommBoxSendDataCallbackAdapter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/eposdevice/commbox/CommBox;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "CommBoxSendDataCallbackAdapter"
.end annotation


# instance fields
.field private mListener:Lcom/epson/eposdevice/commbox/SendDataListener;

.field final synthetic this$0:Lcom/epson/eposdevice/commbox/CommBox;


# direct methods
.method public constructor <init>(Lcom/epson/eposdevice/commbox/CommBox;Lcom/epson/eposdevice/commbox/SendDataListener;)V
    .locals 0

    .line 135
    iput-object p1, p0, Lcom/epson/eposdevice/commbox/CommBox$CommBoxSendDataCallbackAdapter;->this$0:Lcom/epson/eposdevice/commbox/CommBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p1, 0x0

    .line 134
    iput-object p1, p0, Lcom/epson/eposdevice/commbox/CommBox$CommBoxSendDataCallbackAdapter;->mListener:Lcom/epson/eposdevice/commbox/SendDataListener;

    .line 136
    iput-object p2, p0, Lcom/epson/eposdevice/commbox/CommBox$CommBoxSendDataCallbackAdapter;->mListener:Lcom/epson/eposdevice/commbox/SendDataListener;

    return-void
.end method


# virtual methods
.method public onCommBoxSendData(JLjava/lang/String;IJJ)V
    .locals 2

    .line 140
    iget-object p1, p0, Lcom/epson/eposdevice/commbox/CommBox$CommBoxSendDataCallbackAdapter;->mListener:Lcom/epson/eposdevice/commbox/SendDataListener;

    if-eqz p1, :cond_0

    .line 141
    iget-object p1, p0, Lcom/epson/eposdevice/commbox/CommBox$CommBoxSendDataCallbackAdapter;->this$0:Lcom/epson/eposdevice/commbox/CommBox;

    const/4 p2, 0x4

    new-array p2, p2, [Ljava/lang/Object;

    const/4 v0, 0x0

    aput-object p3, p2, v0

    const/4 v0, 0x1

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, p2, v0

    const/4 v0, 0x2

    invoke-static {p5, p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, p2, v0

    const/4 v0, 0x3

    invoke-static {p7, p8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, p2, v0

    const-string v0, "onCommBoxSendData"

    invoke-virtual {p1, v0, p2}, Lcom/epson/eposdevice/commbox/CommBox;->outputBoxLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 142
    iget-object p1, p0, Lcom/epson/eposdevice/commbox/CommBox$CommBoxSendDataCallbackAdapter;->mListener:Lcom/epson/eposdevice/commbox/SendDataListener;

    long-to-int p2, p5

    long-to-int p5, p7

    invoke-interface {p1, p3, p4, p2, p5}, Lcom/epson/eposdevice/commbox/SendDataListener;->onCommBoxSendData(Ljava/lang/String;III)V

    :cond_0
    return-void
.end method
