.class public Lcom/epson/eposdevice/commbox/CommBoxManager$CloseCommBoxCallbackAdapter;
.super Ljava/lang/Object;
.source "CommBoxManager.java"

# interfaces
.implements Lcom/epson/eposdevice/commbox/NativeCommBoxManager$NativeCloseCommBoxCallbackAdapter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/eposdevice/commbox/CommBoxManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "CloseCommBoxCallbackAdapter"
.end annotation


# instance fields
.field private mDeleteBox:Lcom/epson/eposdevice/commbox/CommBoxManager$CommBoxAccessor;

.field private mListener:Lcom/epson/eposdevice/commbox/CloseCommBoxListener;

.field final synthetic this$0:Lcom/epson/eposdevice/commbox/CommBoxManager;


# direct methods
.method public constructor <init>(Lcom/epson/eposdevice/commbox/CommBoxManager;Lcom/epson/eposdevice/commbox/CloseCommBoxListener;Lcom/epson/eposdevice/commbox/CommBoxManager$CommBoxAccessor;)V
    .locals 0

    .line 107
    iput-object p1, p0, Lcom/epson/eposdevice/commbox/CommBoxManager$CloseCommBoxCallbackAdapter;->this$0:Lcom/epson/eposdevice/commbox/CommBoxManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p1, 0x0

    .line 105
    iput-object p1, p0, Lcom/epson/eposdevice/commbox/CommBoxManager$CloseCommBoxCallbackAdapter;->mListener:Lcom/epson/eposdevice/commbox/CloseCommBoxListener;

    .line 106
    iput-object p1, p0, Lcom/epson/eposdevice/commbox/CommBoxManager$CloseCommBoxCallbackAdapter;->mDeleteBox:Lcom/epson/eposdevice/commbox/CommBoxManager$CommBoxAccessor;

    .line 108
    iput-object p2, p0, Lcom/epson/eposdevice/commbox/CommBoxManager$CloseCommBoxCallbackAdapter;->mListener:Lcom/epson/eposdevice/commbox/CloseCommBoxListener;

    .line 109
    iput-object p3, p0, Lcom/epson/eposdevice/commbox/CommBoxManager$CloseCommBoxCallbackAdapter;->mDeleteBox:Lcom/epson/eposdevice/commbox/CommBoxManager$CommBoxAccessor;

    return-void
.end method


# virtual methods
.method public nativeOnCloseCommBox(JJLjava/lang/String;IJ)V
    .locals 0

    .line 113
    iget-object p1, p0, Lcom/epson/eposdevice/commbox/CommBoxManager$CloseCommBoxCallbackAdapter;->mListener:Lcom/epson/eposdevice/commbox/CloseCommBoxListener;

    if-eqz p1, :cond_0

    .line 114
    iget-object p1, p0, Lcom/epson/eposdevice/commbox/CommBoxManager$CloseCommBoxCallbackAdapter;->this$0:Lcom/epson/eposdevice/commbox/CommBoxManager;

    const/4 p2, 0x3

    new-array p2, p2, [Ljava/lang/Object;

    const/4 p3, 0x0

    aput-object p5, p2, p3

    const/4 p3, 0x1

    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p4

    aput-object p4, p2, p3

    const/4 p3, 0x2

    invoke-static {p7, p8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p4

    aput-object p4, p2, p3

    const-string p3, "onCloseCommBox"

    invoke-virtual {p1, p3, p2}, Lcom/epson/eposdevice/commbox/CommBoxManager;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 115
    iget-object p1, p0, Lcom/epson/eposdevice/commbox/CommBoxManager$CloseCommBoxCallbackAdapter;->mListener:Lcom/epson/eposdevice/commbox/CloseCommBoxListener;

    long-to-int p2, p7

    invoke-interface {p1, p5, p6, p2}, Lcom/epson/eposdevice/commbox/CloseCommBoxListener;->onCloseCommBox(Ljava/lang/String;II)V

    .line 117
    :cond_0
    iget-object p1, p0, Lcom/epson/eposdevice/commbox/CommBoxManager$CloseCommBoxCallbackAdapter;->mDeleteBox:Lcom/epson/eposdevice/commbox/CommBoxManager$CommBoxAccessor;

    if-eqz p1, :cond_1

    .line 118
    iget-object p1, p0, Lcom/epson/eposdevice/commbox/CommBoxManager$CloseCommBoxCallbackAdapter;->this$0:Lcom/epson/eposdevice/commbox/CommBoxManager;

    invoke-static {p1}, Lcom/epson/eposdevice/commbox/CommBoxManager;->access$000(Lcom/epson/eposdevice/commbox/CommBoxManager;)Ljava/util/Vector;

    move-result-object p1

    iget-object p2, p0, Lcom/epson/eposdevice/commbox/CommBoxManager$CloseCommBoxCallbackAdapter;->mDeleteBox:Lcom/epson/eposdevice/commbox/CommBoxManager$CommBoxAccessor;

    invoke-virtual {p1, p2}, Ljava/util/Vector;->remove(Ljava/lang/Object;)Z

    .line 119
    iget-object p1, p0, Lcom/epson/eposdevice/commbox/CommBoxManager$CloseCommBoxCallbackAdapter;->mDeleteBox:Lcom/epson/eposdevice/commbox/CommBoxManager$CommBoxAccessor;

    invoke-virtual {p1}, Lcom/epson/eposdevice/commbox/CommBoxManager$CommBoxAccessor;->deleteInstance()V

    :cond_1
    return-void
.end method
