.class public Lcom/epson/eposdevice/DeviceInnerImplement$CommBoxManagerInner$CommBoxInner;
.super Lcom/epson/eposdevice/commbox/CommBoxManager$CommBoxAccessor;
.source "DeviceInnerImplement.java"

# interfaces
.implements Lcom/epson/eposdevice/DeviceInnerImplement$IHandleObject;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/epson/eposdevice/DeviceInnerImplement$CommBoxManagerInner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "CommBoxInner"
.end annotation


# instance fields
.field final synthetic this$1:Lcom/epson/eposdevice/DeviceInnerImplement$CommBoxManagerInner;


# direct methods
.method protected constructor <init>(Lcom/epson/eposdevice/DeviceInnerImplement$CommBoxManagerInner;J)V
    .locals 0

    .line 211
    iput-object p1, p0, Lcom/epson/eposdevice/DeviceInnerImplement$CommBoxManagerInner$CommBoxInner;->this$1:Lcom/epson/eposdevice/DeviceInnerImplement$CommBoxManagerInner;

    .line 212
    invoke-direct {p0, p1, p2, p3}, Lcom/epson/eposdevice/commbox/CommBoxManager$CommBoxAccessor;-><init>(Lcom/epson/eposdevice/commbox/CommBoxManager;J)V

    return-void
.end method


# virtual methods
.method public getDeviceHandle()J
    .locals 2

    .line 216
    invoke-virtual {p0}, Lcom/epson/eposdevice/DeviceInnerImplement$CommBoxManagerInner$CommBoxInner;->getHandle()J

    move-result-wide v0

    return-wide v0
.end method

.method protected outputBoxException(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 1

    .line 231
    iget-object v0, p0, Lcom/epson/eposdevice/DeviceInnerImplement$CommBoxManagerInner$CommBoxInner;->this$1:Lcom/epson/eposdevice/DeviceInnerImplement$CommBoxManagerInner;

    invoke-virtual {v0, p1, p2}, Lcom/epson/eposdevice/DeviceInnerImplement$CommBoxManagerInner;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method protected varargs outputBoxLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1

    .line 221
    iget-object v0, p0, Lcom/epson/eposdevice/DeviceInnerImplement$CommBoxManagerInner$CommBoxInner;->this$1:Lcom/epson/eposdevice/DeviceInnerImplement$CommBoxManagerInner;

    invoke-virtual {v0, p1, p2}, Lcom/epson/eposdevice/DeviceInnerImplement$CommBoxManagerInner;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method protected varargs outputBoxLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1

    .line 236
    iget-object v0, p0, Lcom/epson/eposdevice/DeviceInnerImplement$CommBoxManagerInner$CommBoxInner;->this$1:Lcom/epson/eposdevice/DeviceInnerImplement$CommBoxManagerInner;

    invoke-virtual {v0, p1, p2}, Lcom/epson/eposdevice/DeviceInnerImplement$CommBoxManagerInner;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method protected varargs outputBoxLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1

    .line 226
    iget-object v0, p0, Lcom/epson/eposdevice/DeviceInnerImplement$CommBoxManagerInner$CommBoxInner;->this$1:Lcom/epson/eposdevice/DeviceInnerImplement$CommBoxManagerInner;

    invoke-virtual {v0, p1, p2}, Lcom/epson/eposdevice/DeviceInnerImplement$CommBoxManagerInner;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method
