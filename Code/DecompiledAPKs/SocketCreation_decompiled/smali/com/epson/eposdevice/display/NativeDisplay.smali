.class abstract Lcom/epson/eposdevice/display/NativeDisplay;
.super Ljava/lang/Object;
.source "NativeDisplay.java"


# static fields
.field protected static final NATIVE_BRIGHTNESS_100:I = 0x3

.field protected static final NATIVE_BRIGHTNESS_20:I = 0x0

.field protected static final NATIVE_BRIGHTNESS_40:I = 0x1

.field protected static final NATIVE_BRIGHTNESS_60:I = 0x2

.field protected static final NATIVE_CURSOR_NONE:I = 0x0

.field protected static final NATIVE_CURSOR_UNDERLINE:I = 0x1

.field protected static final NATIVE_FALSE:I = 0x0

.field protected static final NATIVE_LANG_EN:I = 0x0

.field protected static final NATIVE_LANG_JA:I = 0x1

.field protected static final NATIVE_MARQUEE_PLACE:I = 0x1

.field protected static final NATIVE_MARQUEE_WALK:I = 0x0

.field protected static final NATIVE_MOVE_BOTTOM_LEFT:I = 0x2

.field protected static final NATIVE_MOVE_BOTTOM_RIGHT:I = 0x3

.field protected static final NATIVE_MOVE_TOP_LEFT:I = 0x0

.field protected static final NATIVE_MOVE_TOP_RIGHT:I = 0x1

.field protected static final NATIVE_SCROLL_HORIZONTAL:I = 0x2

.field protected static final NATIVE_SCROLL_OVERWRITE:I = 0x0

.field protected static final NATIVE_SCROLL_VERTICAL:I = 0x1

.field protected static final NATIVE_TRUE:I = 0x1


# direct methods
.method constructor <init>()V
    .locals 0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method protected native nativeDspAddBlink(JJ)I
.end method

.method protected native nativeDspAddBrightness(JI)I
.end method

.method protected native nativeDspAddClearWindow(J)I
.end method

.method protected native nativeDspAddCommand(J[B)I
.end method

.method protected native nativeDspAddCreateWindow(JJJJJJI)I
.end method

.method protected native nativeDspAddDestroyWindow(JJ)I
.end method

.method protected native nativeDspAddMarquee(JLjava/lang/String;IJJJI)I
.end method

.method protected native nativeDspAddMoveCursorPosition(JI)I
.end method

.method protected native nativeDspAddReset(J)I
.end method

.method protected native nativeDspAddReverseText(JLjava/lang/String;)I
.end method

.method protected native nativeDspAddReverseTextLang(JLjava/lang/String;I)I
.end method

.method protected native nativeDspAddReverseTextPosition(JLjava/lang/String;JJ)I
.end method

.method protected native nativeDspAddReverseTextPositionLang(JLjava/lang/String;JJI)I
.end method

.method protected native nativeDspAddSetCurrentWindow(JJ)I
.end method

.method protected native nativeDspAddSetCursorPosition(JJJ)I
.end method

.method protected native nativeDspAddSetCursorType(JI)I
.end method

.method protected native nativeDspAddShowClock(J)I
.end method

.method protected native nativeDspAddText(JLjava/lang/String;)I
.end method

.method protected native nativeDspAddTextLang(JLjava/lang/String;I)I
.end method

.method protected native nativeDspAddTextPosition(JLjava/lang/String;JJ)I
.end method

.method protected native nativeDspAddTextPositionLang(JLjava/lang/String;JJI)I
.end method

.method protected native nativeDspClearCommandBuffer(J)I
.end method

.method protected native nativeDspSendData(J)I
.end method

.method protected abstract nativeOnDspReceive(Ljava/lang/String;Ljava/lang/String;IIJJ)V
.end method

.method protected native nativeSetDspReceiveCallback(JLcom/epson/eposdevice/display/NativeDisplay;)I
.end method
