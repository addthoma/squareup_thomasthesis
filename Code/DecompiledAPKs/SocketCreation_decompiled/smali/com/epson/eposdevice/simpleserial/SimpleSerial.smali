.class public abstract Lcom/epson/eposdevice/simpleserial/SimpleSerial;
.super Lcom/epson/eposdevice/simpleserial/NativeSimpleSerial;
.source "SimpleSerial.java"


# instance fields
.field private mCommandReplayListener:Lcom/epson/eposdevice/simpleserial/CommandReplayListener;

.field private mSscHandle:J


# direct methods
.method protected constructor <init>(J)V
    .locals 2

    .line 18
    invoke-direct {p0}, Lcom/epson/eposdevice/simpleserial/NativeSimpleSerial;-><init>()V

    const/4 v0, 0x0

    .line 8
    iput-object v0, p0, Lcom/epson/eposdevice/simpleserial/SimpleSerial;->mCommandReplayListener:Lcom/epson/eposdevice/simpleserial/CommandReplayListener;

    const-wide/16 v0, 0x0

    .line 72
    iput-wide v0, p0, Lcom/epson/eposdevice/simpleserial/SimpleSerial;->mSscHandle:J

    .line 19
    iput-wide p1, p0, Lcom/epson/eposdevice/simpleserial/SimpleSerial;->mSscHandle:J

    return-void
.end method

.method private OnSimpleSerialCommandReply(Ljava/lang/String;Ljava/lang/String;[B)V
    .locals 2

    .line 12
    iget-object v0, p0, Lcom/epson/eposdevice/simpleserial/SimpleSerial;->mCommandReplayListener:Lcom/epson/eposdevice/simpleserial/CommandReplayListener;

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    aput-object p2, v0, v1

    const/4 v1, 0x2

    aput-object p3, v0, v1

    const-string v1, "onSimpleSerialCommandReplay"

    .line 13
    invoke-virtual {p0, v1, v0}, Lcom/epson/eposdevice/simpleserial/SimpleSerial;->outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 14
    iget-object v0, p0, Lcom/epson/eposdevice/simpleserial/SimpleSerial;->mCommandReplayListener:Lcom/epson/eposdevice/simpleserial/CommandReplayListener;

    invoke-interface {v0, p1, p2, p3}, Lcom/epson/eposdevice/simpleserial/CommandReplayListener;->onSimpleSerialCommandReplay(Ljava/lang/String;Ljava/lang/String;[B)V

    :cond_0
    return-void
.end method


# virtual methods
.method protected checkHandle()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    .line 30
    iget-wide v0, p0, Lcom/epson/eposdevice/simpleserial/SimpleSerial;->mSscHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-eqz v4, :cond_0

    return-void

    .line 31
    :cond_0
    new-instance v0, Lcom/epson/eposdevice/EposException;

    const/16 v1, 0xff

    invoke-direct {v0, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw v0
.end method

.method protected getInnerHandle()J
    .locals 2

    .line 22
    iget-wide v0, p0, Lcom/epson/eposdevice/simpleserial/SimpleSerial;->mSscHandle:J

    return-wide v0
.end method

.method protected innerDeleteInstance()V
    .locals 3

    .line 25
    iget-wide v0, p0, Lcom/epson/eposdevice/simpleserial/SimpleSerial;->mSscHandle:J

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/epson/eposdevice/simpleserial/SimpleSerial;->nativeSetSscCommandReplyEventCallback(JLcom/epson/eposdevice/simpleserial/NativeSimpleSerial;)I

    const-wide/16 v0, 0x0

    .line 26
    iput-wide v0, p0, Lcom/epson/eposdevice/simpleserial/SimpleSerial;->mSscHandle:J

    return-void
.end method

.method protected nativeOnSimpleSerialCommandReply(Ljava/lang/String;Ljava/lang/String;[B)V
    .locals 0

    .line 69
    invoke-direct {p0, p1, p2, p3}, Lcom/epson/eposdevice/simpleserial/SimpleSerial;->OnSimpleSerialCommandReply(Ljava/lang/String;Ljava/lang/String;[B)V

    return-void
.end method

.method protected abstract outputException(Ljava/lang/String;Ljava/lang/Exception;)V
.end method

.method protected varargs abstract outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V
.end method

.method protected varargs abstract outputLogEvent(Ljava/lang/String;[Ljava/lang/Object;)V
.end method

.method protected varargs abstract outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V
.end method

.method public sendCommand([B)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposdevice/EposException;
        }
    .end annotation

    const/4 v0, 0x1

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const-string v3, "sendCommand"

    .line 36
    invoke-virtual {p0, v3, v1}, Lcom/epson/eposdevice/simpleserial/SimpleSerial;->outputLogCallFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 38
    :try_start_0
    invoke-virtual {p0}, Lcom/epson/eposdevice/simpleserial/SimpleSerial;->checkHandle()V

    if-eqz p1, :cond_1

    .line 42
    iget-wide v4, p0, Lcom/epson/eposdevice/simpleserial/SimpleSerial;->mSscHandle:J

    invoke-virtual {p0, v4, v5, p1}, Lcom/epson/eposdevice/simpleserial/SimpleSerial;->nativeSscSendCommand(J[B)I

    move-result v1
    :try_end_0
    .catch Lcom/epson/eposdevice/EposException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_0

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v2

    .line 51
    invoke-virtual {p0, v3, v0}, Lcom/epson/eposdevice/simpleserial/SimpleSerial;->outputLogReturnFunction(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    .line 44
    :cond_0
    :try_start_1
    new-instance p1, Lcom/epson/eposdevice/EposException;

    invoke-direct {p1, v1}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw p1

    .line 40
    :cond_1
    new-instance p1, Lcom/epson/eposdevice/EposException;

    invoke-direct {p1, v0}, Lcom/epson/eposdevice/EposException;-><init>(I)V

    throw p1
    :try_end_1
    .catch Lcom/epson/eposdevice/EposException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception p1

    .line 48
    invoke-virtual {p0, v3, p1}, Lcom/epson/eposdevice/simpleserial/SimpleSerial;->outputException(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 49
    throw p1
.end method

.method public setCommandReplyEventCallback(Lcom/epson/eposdevice/simpleserial/CommandReplayListener;)V
    .locals 5

    .line 55
    iget-wide v0, p0, Lcom/epson/eposdevice/simpleserial/SimpleSerial;->mSscHandle:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-eqz v4, :cond_1

    if-eqz p1, :cond_0

    .line 57
    iput-object p1, p0, Lcom/epson/eposdevice/simpleserial/SimpleSerial;->mCommandReplayListener:Lcom/epson/eposdevice/simpleserial/CommandReplayListener;

    .line 58
    invoke-virtual {p0, v0, v1, p0}, Lcom/epson/eposdevice/simpleserial/SimpleSerial;->nativeSetSscCommandReplyEventCallback(JLcom/epson/eposdevice/simpleserial/NativeSimpleSerial;)I

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 61
    invoke-virtual {p0, v0, v1, p1}, Lcom/epson/eposdevice/simpleserial/SimpleSerial;->nativeSetSscCommandReplyEventCallback(JLcom/epson/eposdevice/simpleserial/NativeSimpleSerial;)I

    .line 62
    iput-object p1, p0, Lcom/epson/eposdevice/simpleserial/SimpleSerial;->mCommandReplayListener:Lcom/epson/eposdevice/simpleserial/CommandReplayListener;

    :cond_1
    :goto_0
    return-void
.end method
