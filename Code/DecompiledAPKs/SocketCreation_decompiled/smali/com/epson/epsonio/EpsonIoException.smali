.class public Lcom/epson/epsonio/EpsonIoException;
.super Ljava/lang/Exception;
.source "EpsonIoException.java"


# static fields
.field private static final serialVersionUID:J = -0x3eaac09ae8e9f5c2L


# instance fields
.field private mStatus:I


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .line 11
    invoke-direct {p0}, Ljava/lang/Exception;-><init>()V

    const/16 v0, 0xff

    .line 5
    iput v0, p0, Lcom/epson/epsonio/EpsonIoException;->mStatus:I

    .line 12
    iput p1, p0, Lcom/epson/epsonio/EpsonIoException;->mStatus:I

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0

    .line 8
    invoke-direct {p0, p1, p2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    const/16 p1, 0xff

    .line 5
    iput p1, p0, Lcom/epson/epsonio/EpsonIoException;->mStatus:I

    return-void
.end method


# virtual methods
.method public getStatus()I
    .locals 1

    .line 20
    iget v0, p0, Lcom/epson/epsonio/EpsonIoException;->mStatus:I

    return v0
.end method

.method public setStatus(I)V
    .locals 0

    .line 16
    iput p1, p0, Lcom/epson/epsonio/EpsonIoException;->mStatus:I

    return-void
.end method
