.class public Lcom/epson/eposprint/Builder;
.super Ljava/lang/Object;
.source "Builder.java"


# static fields
.field public static final ALIGN_CENTER:I = 0x1

.field public static final ALIGN_LEFT:I = 0x0

.field public static final ALIGN_RIGHT:I = 0x2

.field public static final BARCODE_CODABAR:I = 0x8

.field public static final BARCODE_CODE128:I = 0xa

.field public static final BARCODE_CODE39:I = 0x6

.field public static final BARCODE_CODE93:I = 0x9

.field public static final BARCODE_EAN13:I = 0x2

.field public static final BARCODE_EAN8:I = 0x4

.field public static final BARCODE_GS1_128:I = 0xb

.field public static final BARCODE_GS1_DATABAR_EXPANDED:I = 0xf

.field public static final BARCODE_GS1_DATABAR_LIMITED:I = 0xe

.field public static final BARCODE_GS1_DATABAR_OMNIDIRECTIONAL:I = 0xc

.field public static final BARCODE_GS1_DATABAR_TRUNCATED:I = 0xd

.field public static final BARCODE_ITF:I = 0x7

.field public static final BARCODE_JAN13:I = 0x3

.field public static final BARCODE_JAN8:I = 0x5

.field public static final BARCODE_UPC_A:I = 0x0

.field public static final BARCODE_UPC_E:I = 0x1

.field public static final COLOR_1:I = 0x1

.field public static final COLOR_2:I = 0x2

.field public static final COLOR_3:I = 0x3

.field public static final COLOR_4:I = 0x4

.field public static final COLOR_NONE:I = 0x0

.field public static final COMPRESS_DEFLATE:I = 0x1

.field public static final COMPRESS_NONE:I = 0x0

.field public static final CUT_FEED:I = 0x1

.field public static final CUT_NO_FEED:I = 0x0

.field public static final CUT_RESERVE:I = 0x2

.field public static final DIRECTION_BOTTOM_TO_TOP:I = 0x1

.field public static final DIRECTION_LEFT_TO_RIGHT:I = 0x0

.field public static final DIRECTION_RIGHT_TO_LEFT:I = 0x2

.field public static final DIRECTION_TOP_TO_BOTTOM:I = 0x3

.field public static final DRAWER_1:I = 0x0

.field public static final DRAWER_2:I = 0x1

.field public static final FALSE:I = 0x0

.field public static final FEED_CURRENT_TOF:I = 0x2

.field public static final FEED_CUTTING:I = 0x1

.field public static final FEED_NEXT_TOF:I = 0x3

.field public static final FEED_PEELING:I = 0x0

.field public static final FONT_A:I = 0x0

.field public static final FONT_B:I = 0x1

.field public static final FONT_C:I = 0x2

.field public static final FONT_D:I = 0x3

.field public static final FONT_E:I = 0x4

.field public static final HALFTONE_DITHER:I = 0x0

.field public static final HALFTONE_ERROR_DIFFUSION:I = 0x1

.field public static final HALFTONE_THRESHOLD:I = 0x2

.field public static final HRI_ABOVE:I = 0x1

.field public static final HRI_BELOW:I = 0x2

.field public static final HRI_BOTH:I = 0x3

.field public static final HRI_NONE:I = 0x0

.field public static final LANG_EN:I = 0x0

.field public static final LANG_JA:I = 0x1

.field public static final LANG_KO:I = 0x4

.field public static final LANG_TH:I = 0x5

.field public static final LANG_VI:I = 0x6

.field public static final LANG_ZH_CN:I = 0x2

.field public static final LANG_ZH_TW:I = 0x3

.field public static final LAYOUT_LABEL:I = 0x1

.field public static final LAYOUT_LABEL_BM:I = 0x2

.field public static final LAYOUT_RECEIPT:I = 0x0

.field public static final LAYOUT_RECEIPT_BM:I = 0x3

.field public static final LEVEL_0:I = 0x0

.field public static final LEVEL_1:I = 0x1

.field public static final LEVEL_2:I = 0x2

.field public static final LEVEL_3:I = 0x3

.field public static final LEVEL_4:I = 0x4

.field public static final LEVEL_5:I = 0x5

.field public static final LEVEL_6:I = 0x6

.field public static final LEVEL_7:I = 0x7

.field public static final LEVEL_8:I = 0x8

.field public static final LEVEL_DEFAULT:I = 0xd

.field public static final LEVEL_H:I = 0xc

.field public static final LEVEL_L:I = 0x9

.field public static final LEVEL_M:I = 0xa

.field public static final LEVEL_Q:I = 0xb

.field public static final LINE_MEDIUM:I = 0x1

.field public static final LINE_MEDIUM_DOUBLE:I = 0x4

.field public static final LINE_THICK:I = 0x2

.field public static final LINE_THICK_DOUBLE:I = 0x5

.field public static final LINE_THIN:I = 0x0

.field public static final LINE_THIN_DOUBLE:I = 0x3

.field private static final MIN_IMAGE_HEIGHT:I = 0x1

.field private static final MIN_IMAGE_WIDTH:I = 0x1

.field public static final MODEL_ANK:I = 0x0

.field public static final MODEL_CHINESE:I = 0x2

.field public static final MODEL_JAPANESE:I = 0x1

.field public static final MODEL_KOREAN:I = 0x4

.field public static final MODEL_SOUTHASIA:I = 0x6

.field public static final MODEL_TAIWAN:I = 0x3

.field public static final MODEL_THAI:I = 0x5

.field public static final MODE_GRAY16:I = 0x1

.field public static final MODE_MONO:I = 0x0

.field public static final PARAM_DEFAULT:I = -0x2

.field public static final PARAM_UNSPECIFIED:I = -0x1

.field public static final PATTERN_1:I = 0x8

.field public static final PATTERN_10:I = 0x11

.field public static final PATTERN_2:I = 0x9

.field public static final PATTERN_3:I = 0xa

.field public static final PATTERN_4:I = 0xb

.field public static final PATTERN_5:I = 0xc

.field public static final PATTERN_6:I = 0xd

.field public static final PATTERN_7:I = 0xe

.field public static final PATTERN_8:I = 0xf

.field public static final PATTERN_9:I = 0x10

.field public static final PATTERN_A:I = 0x1

.field public static final PATTERN_B:I = 0x2

.field public static final PATTERN_C:I = 0x3

.field public static final PATTERN_D:I = 0x4

.field public static final PATTERN_E:I = 0x5

.field public static final PATTERN_ERROR:I = 0x6

.field public static final PATTERN_PAPER_END:I = 0x7

.field public static final PULSE_100:I = 0x0

.field public static final PULSE_200:I = 0x1

.field public static final PULSE_300:I = 0x2

.field public static final PULSE_400:I = 0x3

.field public static final PULSE_500:I = 0x4

.field public static final SYMBOL_AZTECCODE_COMPACT:I = 0xd

.field public static final SYMBOL_AZTECCODE_FULLRANGE:I = 0xc

.field public static final SYMBOL_DATAMATRIX_RECTANGLE_12:I = 0x10

.field public static final SYMBOL_DATAMATRIX_RECTANGLE_16:I = 0x11

.field public static final SYMBOL_DATAMATRIX_RECTANGLE_8:I = 0xf

.field public static final SYMBOL_DATAMATRIX_SQUARE:I = 0xe

.field public static final SYMBOL_GS1_DATABAR_EXPANDED_STACKED:I = 0xb

.field public static final SYMBOL_GS1_DATABAR_STACKED:I = 0x9

.field public static final SYMBOL_GS1_DATABAR_STACKED_OMNIDIRECTIONAL:I = 0xa

.field public static final SYMBOL_MAXICODE_MODE_2:I = 0x4

.field public static final SYMBOL_MAXICODE_MODE_3:I = 0x5

.field public static final SYMBOL_MAXICODE_MODE_4:I = 0x6

.field public static final SYMBOL_MAXICODE_MODE_5:I = 0x7

.field public static final SYMBOL_MAXICODE_MODE_6:I = 0x8

.field public static final SYMBOL_PDF417_STANDARD:I = 0x0

.field public static final SYMBOL_PDF417_TRUNCATED:I = 0x1

.field public static final SYMBOL_QRCODE_MODEL_1:I = 0x2

.field public static final SYMBOL_QRCODE_MODEL_2:I = 0x3

.field public static final TRUE:I = 0x1


# instance fields
.field private mCommandHandle:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    :try_start_0
    const-string v0, "epos2"

    .line 18
    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposprint/EposException;
        }
    .end annotation

    .line 274
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    .line 27
    iput-wide v0, p0, Lcom/epson/eposprint/Builder;->mCommandHandle:J

    .line 275
    iput-wide v0, p0, Lcom/epson/eposprint/Builder;->mCommandHandle:J

    const/4 v2, 0x1

    if-eqz p1, :cond_1

    new-array v2, v2, [J

    const/4 v3, 0x0

    aput-wide v0, v2, v3

    .line 284
    invoke-direct {p0, v2, p1, p2}, Lcom/epson/eposprint/Builder;->eposCreateCommandBuffer([JLjava/lang/String;I)I

    move-result p1

    if-nez p1, :cond_0

    .line 289
    aget-wide p1, v2, v3

    iput-wide p1, p0, Lcom/epson/eposprint/Builder;->mCommandHandle:J

    return-void

    .line 287
    :cond_0
    new-instance p2, Lcom/epson/eposprint/EposException;

    invoke-direct {p2, p1}, Lcom/epson/eposprint/EposException;-><init>(I)V

    throw p2

    .line 278
    :cond_1
    new-instance p1, Lcom/epson/eposprint/EposException;

    invoke-direct {p1, v2}, Lcom/epson/eposprint/EposException;-><init>(I)V

    throw p1
.end method

.method public constructor <init>(Ljava/lang/String;ILandroid/content/Context;)V
    .locals 18
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposprint/EposException;
        }
    .end annotation

    move-object/from16 v9, p0

    move-object/from16 v10, p1

    move-object/from16 v0, p3

    .line 292
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v11, 0x0

    .line 27
    iput-wide v11, v9, Lcom/epson/eposprint/Builder;->mCommandHandle:J

    .line 293
    iput-wide v11, v9, Lcom/epson/eposprint/Builder;->mCommandHandle:J

    const/4 v13, 0x1

    if-eqz v10, :cond_2

    const-string v1, ""

    if-eqz v0, :cond_0

    const/4 v2, 0x0

    .line 304
    :try_start_0
    invoke-virtual {v0, v2}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, v0

    :catch_0
    :cond_0
    const-string v2, "ePOS-Print SDK for Android"

    const-string v3, "1.9.0"

    const-string v4, "Android"

    .line 314
    sget-object v5, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    .line 315
    sget-object v6, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 317
    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/eposprint/Builder;->getTotalMemorySize(Ljava/io/File;)J

    move-result-wide v7

    .line 319
    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/eposprint/Builder;->getFreeMemorySize(Ljava/io/File;)J

    move-result-wide v14

    .line 320
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, Lcom/epson/eposprint/Builder;->getFreeMemorySize(Ljava/io/File;)J

    move-result-wide v16

    .line 321
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/Object;

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    const/4 v14, 0x0

    aput-object v12, v11, v14

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v11, v13

    const-string v7, "%d/%d"

    invoke-static {v0, v7, v11}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 322
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    new-array v8, v13, [Ljava/lang/Object;

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v8, v14

    const-string v11, "%d\n"

    invoke-static {v0, v11, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p0

    .line 325
    :try_start_1
    invoke-direct/range {v0 .. v8}, Lcom/epson/eposprint/Builder;->eposReadLogSettings(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    nop

    :goto_0
    new-array v0, v13, [J

    const-wide/16 v1, 0x0

    aput-wide v1, v0, v14

    move/from16 v1, p2

    .line 335
    invoke-direct {v9, v0, v10, v1}, Lcom/epson/eposprint/Builder;->eposCreateCommandBuffer([JLjava/lang/String;I)I

    move-result v1

    if-nez v1, :cond_1

    .line 340
    aget-wide v1, v0, v14

    iput-wide v1, v9, Lcom/epson/eposprint/Builder;->mCommandHandle:J

    return-void

    .line 338
    :cond_1
    new-instance v0, Lcom/epson/eposprint/EposException;

    invoke-direct {v0, v1}, Lcom/epson/eposprint/EposException;-><init>(I)V

    throw v0

    .line 296
    :cond_2
    new-instance v0, Lcom/epson/eposprint/EposException;

    invoke-direct {v0, v13}, Lcom/epson/eposprint/EposException;-><init>(I)V

    throw v0
.end method

.method private native eposAddBarcode(JLjava/lang/String;IIIII)I
.end method

.method private native eposAddCommand(J[BI)I
.end method

.method private native eposAddCut(JI)I
.end method

.method private native eposAddFeedLine(JI)I
.end method

.method private native eposAddFeedPosition(JI)I
.end method

.method private native eposAddFeedUnit(JI)I
.end method

.method private native eposAddHLine(JIII)I
.end method

.method private native eposAddImage(J[BIIIIIIIIIDI)I
.end method

.method private native eposAddLayout(JIIIIIII)I
.end method

.method private native eposAddLogo(JII)I
.end method

.method private native eposAddPageArea(JIIII)I
.end method

.method private native eposAddPageBegin(J)I
.end method

.method private native eposAddPageDirection(JI)I
.end method

.method private native eposAddPageEnd(J)I
.end method

.method private native eposAddPageLine(JIIIII)I
.end method

.method private native eposAddPagePosition(JII)I
.end method

.method private native eposAddPageRectangle(JIIIII)I
.end method

.method private native eposAddPulse(JII)I
.end method

.method private native eposAddSound(JIII)I
.end method

.method private native eposAddSymbol(JLjava/lang/String;IIIII)I
.end method

.method private native eposAddText(JLjava/lang/String;)I
.end method

.method private native eposAddTextAlign(JI)I
.end method

.method private native eposAddTextDouble(JII)I
.end method

.method private native eposAddTextFont(JI)I
.end method

.method private native eposAddTextLang(JI)I
.end method

.method private native eposAddTextLineSpace(JI)I
.end method

.method private native eposAddTextPosition(JI)I
.end method

.method private native eposAddTextRotate(JI)I
.end method

.method private native eposAddTextSize(JII)I
.end method

.method private native eposAddTextSmooth(JI)I
.end method

.method private native eposAddTextStyle(JIIII)I
.end method

.method private native eposAddVLineBegin(JII)I
.end method

.method private native eposAddVLineEnd(JII)I
.end method

.method private native eposClearCommandBuffer(J)I
.end method

.method private native eposCreateCommandBuffer([JLjava/lang/String;I)I
.end method

.method private native eposDeleteCommandBuffer(J)I
.end method

.method private native eposReadLogSettings(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
.end method

.method private static getFreeMemorySize(Ljava/io/File;)J
    .locals 2

    .line 759
    :try_start_0
    invoke-virtual {p0}, Ljava/io/File;->getFreeSpace()J

    move-result-wide v0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const-wide/16 v0, -0x1

    :goto_0
    return-wide v0
.end method

.method private static getTotalMemorySize(Ljava/io/File;)J
    .locals 2

    .line 746
    :try_start_0
    invoke-virtual {p0}, Ljava/io/File;->getTotalSpace()J

    move-result-wide v0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const-wide/16 v0, -0x1

    :goto_0
    return-wide v0
.end method


# virtual methods
.method public addBarcode(Ljava/lang/String;IIIII)V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposprint/EposException;
        }
    .end annotation

    if-eqz p1, :cond_1

    .line 591
    iget-wide v1, p0, Lcom/epson/eposprint/Builder;->mCommandHandle:J

    move-object v0, p0

    move-object v3, p1

    move v4, p2

    move v5, p3

    move v6, p4

    move v7, p5

    move v8, p6

    invoke-direct/range {v0 .. v8}, Lcom/epson/eposprint/Builder;->eposAddBarcode(JLjava/lang/String;IIIII)I

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 594
    :cond_0
    new-instance p2, Lcom/epson/eposprint/EposException;

    invoke-direct {p2, p1}, Lcom/epson/eposprint/EposException;-><init>(I)V

    throw p2

    .line 588
    :cond_1
    new-instance p1, Lcom/epson/eposprint/EposException;

    const/4 p2, 0x1

    invoke-direct {p1, p2}, Lcom/epson/eposprint/EposException;-><init>(I)V

    throw p1
.end method

.method public addCommand([B)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposprint/EposException;
        }
    .end annotation

    if-eqz p1, :cond_1

    .line 719
    iget-wide v0, p0, Lcom/epson/eposprint/Builder;->mCommandHandle:J

    array-length v2, p1

    invoke-direct {p0, v0, v1, p1, v2}, Lcom/epson/eposprint/Builder;->eposAddCommand(J[BI)I

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 721
    :cond_0
    new-instance v0, Lcom/epson/eposprint/EposException;

    invoke-direct {v0, p1}, Lcom/epson/eposprint/EposException;-><init>(I)V

    throw v0

    .line 716
    :cond_1
    new-instance p1, Lcom/epson/eposprint/EposException;

    const/4 v0, 0x1

    invoke-direct {p1, v0}, Lcom/epson/eposprint/EposException;-><init>(I)V

    throw p1
.end method

.method public addCut(I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposprint/EposException;
        }
    .end annotation

    .line 688
    iget-wide v0, p0, Lcom/epson/eposprint/Builder;->mCommandHandle:J

    invoke-direct {p0, v0, v1, p1}, Lcom/epson/eposprint/Builder;->eposAddCut(JI)I

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 690
    :cond_0
    new-instance v0, Lcom/epson/eposprint/EposException;

    invoke-direct {v0, p1}, Lcom/epson/eposprint/EposException;-><init>(I)V

    throw v0
.end method

.method public addFeedLine(I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposprint/EposException;
        }
    .end annotation

    .line 448
    iget-wide v0, p0, Lcom/epson/eposprint/Builder;->mCommandHandle:J

    invoke-direct {p0, v0, v1, p1}, Lcom/epson/eposprint/Builder;->eposAddFeedLine(JI)I

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 450
    :cond_0
    new-instance v0, Lcom/epson/eposprint/EposException;

    invoke-direct {v0, p1}, Lcom/epson/eposprint/EposException;-><init>(I)V

    throw v0
.end method

.method public addFeedPosition(I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposprint/EposException;
        }
    .end annotation

    .line 726
    iget-wide v0, p0, Lcom/epson/eposprint/Builder;->mCommandHandle:J

    invoke-direct {p0, v0, v1, p1}, Lcom/epson/eposprint/Builder;->eposAddFeedPosition(JI)I

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 728
    :cond_0
    new-instance v0, Lcom/epson/eposprint/EposException;

    invoke-direct {v0, p1}, Lcom/epson/eposprint/EposException;-><init>(I)V

    throw v0
.end method

.method public addFeedUnit(I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposprint/EposException;
        }
    .end annotation

    .line 441
    iget-wide v0, p0, Lcom/epson/eposprint/Builder;->mCommandHandle:J

    invoke-direct {p0, v0, v1, p1}, Lcom/epson/eposprint/Builder;->eposAddFeedUnit(JI)I

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 443
    :cond_0
    new-instance v0, Lcom/epson/eposprint/EposException;

    invoke-direct {v0, p1}, Lcom/epson/eposprint/EposException;-><init>(I)V

    throw v0
.end method

.method public addHLine(III)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposprint/EposException;
        }
    .end annotation

    .line 612
    iget-wide v1, p0, Lcom/epson/eposprint/Builder;->mCommandHandle:J

    move-object v0, p0

    move v3, p1

    move v4, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/epson/eposprint/Builder;->eposAddHLine(JIII)I

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 614
    :cond_0
    new-instance p2, Lcom/epson/eposprint/EposException;

    invoke-direct {p2, p1}, Lcom/epson/eposprint/EposException;-><init>(I)V

    throw p2
.end method

.method public addImage(Landroid/graphics/Bitmap;IIIII)V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposprint/EposException;
        }
    .end annotation

    const/4 v7, -0x2

    const/4 v8, -0x2

    const-wide/high16 v9, -0x4000000000000000L    # -2.0

    const/4 v11, -0x2

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move/from16 v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    .line 456
    invoke-virtual/range {v0 .. v11}, Lcom/epson/eposprint/Builder;->addImage(Landroid/graphics/Bitmap;IIIIIIIDI)V

    return-void
.end method

.method public addImage(Landroid/graphics/Bitmap;IIIIIIID)V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposprint/EposException;
        }
    .end annotation

    const/4 v11, -0x2

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move/from16 v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move-wide/from16 v9, p9

    .line 462
    invoke-virtual/range {v0 .. v11}, Lcom/epson/eposprint/Builder;->addImage(Landroid/graphics/Bitmap;IIIIIIIDI)V

    return-void
.end method

.method public addImage(Landroid/graphics/Bitmap;IIIIIIIDI)V
    .locals 23
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposprint/EposException;
        }
    .end annotation

    move-object/from16 v0, p1

    const/4 v1, 0x1

    if-eqz v0, :cond_9

    .line 477
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    .line 478
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v11

    if-gt v1, v10, :cond_8

    if-gt v1, v11, :cond_8

    const/4 v1, 0x5

    .line 490
    :try_start_0
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v2

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v12, 0x0

    if-ne v2, v3, :cond_0

    goto :goto_0

    .line 494
    :cond_0
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v0, v2, v12}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 516
    :goto_0
    new-array v13, v10, [I

    const/16 v2, 0x100

    new-array v14, v2, [D

    const/4 v3, 0x0

    :goto_1
    const-wide v15, 0x406fe00000000000L    # 255.0

    if-ge v3, v2, :cond_1

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    int-to-double v6, v3

    div-double/2addr v6, v15

    sub-double/2addr v4, v6

    .line 521
    aput-wide v4, v14, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_1
    mul-int/lit8 v2, v10, 0x3

    mul-int v2, v2, v11

    .line 524
    new-array v9, v2, [B

    const/4 v8, 0x0

    const/16 v17, 0x0

    :goto_2
    if-ge v8, v11, :cond_5

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/16 v18, 0x1

    move-object v2, v0

    move-object v3, v13

    move v5, v10

    move v7, v8

    move/from16 v19, v8

    move v8, v10

    move-object/from16 v20, v9

    move/from16 v9, v18

    .line 527
    invoke-virtual/range {v2 .. v9}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_3
    if-ge v2, v10, :cond_4

    .line 531
    aget v4, v13, v3

    shr-int/lit8 v4, v4, 0x18

    const/16 v5, 0xff

    and-int/2addr v4, v5

    .line 532
    aget v6, v13, v3

    shr-int/lit8 v6, v6, 0x10

    and-int/2addr v6, v5

    .line 533
    aget v7, v13, v3

    shr-int/lit8 v7, v7, 0x8

    and-int/2addr v7, v5

    .line 534
    aget v8, v13, v3

    and-int/2addr v8, v5

    if-ne v5, v4, :cond_2

    int-to-byte v4, v8

    .line 537
    aput-byte v4, v20, v17

    add-int/lit8 v4, v17, 0x1

    int-to-byte v5, v7

    .line 538
    aput-byte v5, v20, v4

    add-int/lit8 v4, v17, 0x2

    int-to-byte v5, v6

    .line 539
    aput-byte v5, v20, v4

    :goto_4
    move-object v9, v13

    goto :goto_5

    :cond_2
    if-nez v4, :cond_3

    const/4 v4, -0x1

    .line 543
    aput-byte v4, v20, v17

    add-int/lit8 v5, v17, 0x1

    .line 544
    aput-byte v4, v20, v5

    add-int/lit8 v5, v17, 0x2

    .line 545
    aput-byte v4, v20, v5

    goto :goto_4

    :cond_3
    int-to-double v4, v4

    .line 549
    aget-wide v8, v14, v8

    mul-double v8, v8, v4

    sub-double v8, v15, v8

    double-to-int v8, v8

    int-to-byte v8, v8

    aput-byte v8, v20, v17

    add-int/lit8 v8, v17, 0x1

    .line 550
    aget-wide v21, v14, v7

    mul-double v21, v21, v4

    move-object v9, v13

    sub-double v12, v15, v21

    double-to-int v12, v12

    int-to-byte v12, v12

    aput-byte v12, v20, v8

    add-int/lit8 v8, v17, 0x2

    .line 551
    aget-wide v12, v14, v6

    mul-double v4, v4, v12

    sub-double v4, v15, v4

    double-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v20, v8
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    :goto_5
    add-int/lit8 v17, v17, 0x3

    add-int/lit8 v3, v3, 0x1

    add-int/lit8 v2, v2, 0x1

    move-object v13, v9

    const/4 v12, 0x0

    goto :goto_3

    :cond_4
    move-object v9, v13

    add-int/lit8 v8, v19, 0x1

    move-object/from16 v9, v20

    const/4 v12, 0x0

    goto :goto_2

    :cond_5
    move-object/from16 v0, p0

    move-object/from16 v20, v9

    .line 563
    :try_start_1
    iget-wide v3, v0, Lcom/epson/eposprint/Builder;->mCommandHandle:J

    move-object/from16 v2, p0

    move-object/from16 v5, v20

    move v6, v10

    move v7, v11

    move/from16 v8, p2

    move/from16 v9, p3

    move/from16 v10, p4

    move/from16 v11, p5

    move/from16 v12, p6

    move/from16 v13, p7

    move/from16 v14, p8

    move-wide/from16 v15, p9

    move/from16 v17, p11

    invoke-direct/range {v2 .. v17}, Lcom/epson/eposprint/Builder;->eposAddImage(J[BIIIIIIIIIDI)I

    move-result v2

    if-nez v2, :cond_6

    return-void

    .line 568
    :cond_6
    new-instance v3, Lcom/epson/eposprint/EposException;

    invoke-direct {v3, v2}, Lcom/epson/eposprint/EposException;-><init>(I)V

    throw v3

    :cond_7
    move-object/from16 v0, p0

    .line 497
    new-instance v2, Lcom/epson/eposprint/EposException;

    invoke-direct {v2, v1}, Lcom/epson/eposprint/EposException;-><init>(I)V

    throw v2
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_1

    :catch_0
    move-object/from16 v0, p0

    .line 573
    :catch_1
    new-instance v2, Lcom/epson/eposprint/EposException;

    invoke-direct {v2, v1}, Lcom/epson/eposprint/EposException;-><init>(I)V

    throw v2

    :cond_8
    move-object/from16 v0, p0

    .line 482
    new-instance v2, Lcom/epson/eposprint/EposException;

    invoke-direct {v2, v1}, Lcom/epson/eposprint/EposException;-><init>(I)V

    throw v2

    :cond_9
    move-object/from16 v0, p0

    .line 474
    new-instance v2, Lcom/epson/eposprint/EposException;

    invoke-direct {v2, v1}, Lcom/epson/eposprint/EposException;-><init>(I)V

    throw v2
.end method

.method public addLayout(IIIIIII)V
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposprint/EposException;
        }
    .end annotation

    move-object v10, p0

    .line 734
    iget-wide v1, v10, Lcom/epson/eposprint/Builder;->mCommandHandle:J

    move-object v0, p0

    move v3, p1

    move v4, p2

    move v5, p3

    move v6, p4

    move/from16 v7, p5

    move/from16 v8, p6

    move/from16 v9, p7

    invoke-direct/range {v0 .. v9}, Lcom/epson/eposprint/Builder;->eposAddLayout(JIIIIIII)I

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 737
    :cond_0
    new-instance v1, Lcom/epson/eposprint/EposException;

    invoke-direct {v1, v0}, Lcom/epson/eposprint/EposException;-><init>(I)V

    throw v1
.end method

.method public addLogo(II)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposprint/EposException;
        }
    .end annotation

    .line 579
    iget-wide v0, p0, Lcom/epson/eposprint/Builder;->mCommandHandle:J

    invoke-direct {p0, v0, v1, p1, p2}, Lcom/epson/eposprint/Builder;->eposAddLogo(JII)I

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 581
    :cond_0
    new-instance p2, Lcom/epson/eposprint/EposException;

    invoke-direct {p2, p1}, Lcom/epson/eposprint/EposException;-><init>(I)V

    throw p2
.end method

.method public addPageArea(IIII)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposprint/EposException;
        }
    .end annotation

    .line 648
    iget-wide v1, p0, Lcom/epson/eposprint/Builder;->mCommandHandle:J

    move-object v0, p0

    move v3, p1

    move v4, p2

    move v5, p3

    move v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/epson/eposprint/Builder;->eposAddPageArea(JIIII)I

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 651
    :cond_0
    new-instance p2, Lcom/epson/eposprint/EposException;

    invoke-direct {p2, p1}, Lcom/epson/eposprint/EposException;-><init>(I)V

    throw p2
.end method

.method public addPageBegin()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposprint/EposException;
        }
    .end annotation

    .line 633
    iget-wide v0, p0, Lcom/epson/eposprint/Builder;->mCommandHandle:J

    invoke-direct {p0, v0, v1}, Lcom/epson/eposprint/Builder;->eposAddPageBegin(J)I

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 635
    :cond_0
    new-instance v1, Lcom/epson/eposprint/EposException;

    invoke-direct {v1, v0}, Lcom/epson/eposprint/EposException;-><init>(I)V

    throw v1
.end method

.method public addPageDirection(I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposprint/EposException;
        }
    .end annotation

    .line 656
    iget-wide v0, p0, Lcom/epson/eposprint/Builder;->mCommandHandle:J

    invoke-direct {p0, v0, v1, p1}, Lcom/epson/eposprint/Builder;->eposAddPageDirection(JI)I

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 658
    :cond_0
    new-instance v0, Lcom/epson/eposprint/EposException;

    invoke-direct {v0, p1}, Lcom/epson/eposprint/EposException;-><init>(I)V

    throw v0
.end method

.method public addPageEnd()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposprint/EposException;
        }
    .end annotation

    .line 640
    iget-wide v0, p0, Lcom/epson/eposprint/Builder;->mCommandHandle:J

    invoke-direct {p0, v0, v1}, Lcom/epson/eposprint/Builder;->eposAddPageEnd(J)I

    move-result v0

    if-nez v0, :cond_0

    return-void

    .line 642
    :cond_0
    new-instance v1, Lcom/epson/eposprint/EposException;

    invoke-direct {v1, v0}, Lcom/epson/eposprint/EposException;-><init>(I)V

    throw v1
.end method

.method public addPageLine(IIIII)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposprint/EposException;
        }
    .end annotation

    .line 671
    iget-wide v1, p0, Lcom/epson/eposprint/Builder;->mCommandHandle:J

    move-object v0, p0

    move v3, p1

    move v4, p2

    move v5, p3

    move v6, p4

    move v7, p5

    invoke-direct/range {v0 .. v7}, Lcom/epson/eposprint/Builder;->eposAddPageLine(JIIIII)I

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 674
    :cond_0
    new-instance p2, Lcom/epson/eposprint/EposException;

    invoke-direct {p2, p1}, Lcom/epson/eposprint/EposException;-><init>(I)V

    throw p2
.end method

.method public addPagePosition(II)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposprint/EposException;
        }
    .end annotation

    .line 663
    iget-wide v0, p0, Lcom/epson/eposprint/Builder;->mCommandHandle:J

    invoke-direct {p0, v0, v1, p1, p2}, Lcom/epson/eposprint/Builder;->eposAddPagePosition(JII)I

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 665
    :cond_0
    new-instance p2, Lcom/epson/eposprint/EposException;

    invoke-direct {p2, p1}, Lcom/epson/eposprint/EposException;-><init>(I)V

    throw p2
.end method

.method public addPageRectangle(IIIII)V
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposprint/EposException;
        }
    .end annotation

    .line 680
    iget-wide v1, p0, Lcom/epson/eposprint/Builder;->mCommandHandle:J

    move-object v0, p0

    move v3, p1

    move v4, p2

    move v5, p3

    move v6, p4

    move v7, p5

    invoke-direct/range {v0 .. v7}, Lcom/epson/eposprint/Builder;->eposAddPageRectangle(JIIIII)I

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 683
    :cond_0
    new-instance p2, Lcom/epson/eposprint/EposException;

    invoke-direct {p2, p1}, Lcom/epson/eposprint/EposException;-><init>(I)V

    throw p2
.end method

.method public addPulse(II)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposprint/EposException;
        }
    .end annotation

    .line 695
    iget-wide v0, p0, Lcom/epson/eposprint/Builder;->mCommandHandle:J

    invoke-direct {p0, v0, v1, p1, p2}, Lcom/epson/eposprint/Builder;->eposAddPulse(JII)I

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 697
    :cond_0
    new-instance p2, Lcom/epson/eposprint/EposException;

    invoke-direct {p2, p1}, Lcom/epson/eposprint/EposException;-><init>(I)V

    throw p2
.end method

.method public addSound(II)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposprint/EposException;
        }
    .end annotation

    const/4 v0, -0x2

    .line 702
    invoke-virtual {p0, p1, p2, v0}, Lcom/epson/eposprint/Builder;->addSound(III)V

    return-void
.end method

.method public addSound(III)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposprint/EposException;
        }
    .end annotation

    .line 707
    iget-wide v1, p0, Lcom/epson/eposprint/Builder;->mCommandHandle:J

    move-object v0, p0

    move v3, p1

    move v4, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/epson/eposprint/Builder;->eposAddSound(JIII)I

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 710
    :cond_0
    new-instance p2, Lcom/epson/eposprint/EposException;

    invoke-direct {p2, p1}, Lcom/epson/eposprint/EposException;-><init>(I)V

    throw p2
.end method

.method public addSymbol(Ljava/lang/String;IIIII)V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposprint/EposException;
        }
    .end annotation

    if-eqz p1, :cond_1

    .line 604
    iget-wide v1, p0, Lcom/epson/eposprint/Builder;->mCommandHandle:J

    move-object v0, p0

    move-object v3, p1

    move v4, p2

    move v5, p3

    move v6, p4

    move v7, p5

    move v8, p6

    invoke-direct/range {v0 .. v8}, Lcom/epson/eposprint/Builder;->eposAddSymbol(JLjava/lang/String;IIIII)I

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 607
    :cond_0
    new-instance p2, Lcom/epson/eposprint/EposException;

    invoke-direct {p2, p1}, Lcom/epson/eposprint/EposException;-><init>(I)V

    throw p2

    .line 601
    :cond_1
    new-instance p1, Lcom/epson/eposprint/EposException;

    const/4 p2, 0x1

    invoke-direct {p1, p2}, Lcom/epson/eposprint/EposException;-><init>(I)V

    throw p1
.end method

.method public addText(Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposprint/EposException;
        }
    .end annotation

    if-eqz p1, :cond_1

    .line 383
    iget-wide v0, p0, Lcom/epson/eposprint/Builder;->mCommandHandle:J

    invoke-direct {p0, v0, v1, p1}, Lcom/epson/eposprint/Builder;->eposAddText(JLjava/lang/String;)I

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 385
    :cond_0
    new-instance v0, Lcom/epson/eposprint/EposException;

    invoke-direct {v0, p1}, Lcom/epson/eposprint/EposException;-><init>(I)V

    throw v0

    .line 380
    :cond_1
    new-instance p1, Lcom/epson/eposprint/EposException;

    const/4 v0, 0x1

    invoke-direct {p1, v0}, Lcom/epson/eposprint/EposException;-><init>(I)V

    throw p1
.end method

.method public addTextAlign(I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposprint/EposException;
        }
    .end annotation

    .line 358
    iget-wide v0, p0, Lcom/epson/eposprint/Builder;->mCommandHandle:J

    invoke-direct {p0, v0, v1, p1}, Lcom/epson/eposprint/Builder;->eposAddTextAlign(JI)I

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 360
    :cond_0
    new-instance v0, Lcom/epson/eposprint/EposException;

    invoke-direct {v0, p1}, Lcom/epson/eposprint/EposException;-><init>(I)V

    throw v0
.end method

.method public addTextDouble(II)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposprint/EposException;
        }
    .end annotation

    .line 411
    iget-wide v0, p0, Lcom/epson/eposprint/Builder;->mCommandHandle:J

    invoke-direct {p0, v0, v1, p1, p2}, Lcom/epson/eposprint/Builder;->eposAddTextDouble(JII)I

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 413
    :cond_0
    new-instance p2, Lcom/epson/eposprint/EposException;

    invoke-direct {p2, p1}, Lcom/epson/eposprint/EposException;-><init>(I)V

    throw p2
.end method

.method public addTextFont(I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposprint/EposException;
        }
    .end annotation

    .line 397
    iget-wide v0, p0, Lcom/epson/eposprint/Builder;->mCommandHandle:J

    invoke-direct {p0, v0, v1, p1}, Lcom/epson/eposprint/Builder;->eposAddTextFont(JI)I

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 399
    :cond_0
    new-instance v0, Lcom/epson/eposprint/EposException;

    invoke-direct {v0, p1}, Lcom/epson/eposprint/EposException;-><init>(I)V

    throw v0
.end method

.method public addTextLang(I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposprint/EposException;
        }
    .end annotation

    .line 390
    iget-wide v0, p0, Lcom/epson/eposprint/Builder;->mCommandHandle:J

    invoke-direct {p0, v0, v1, p1}, Lcom/epson/eposprint/Builder;->eposAddTextLang(JI)I

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 392
    :cond_0
    new-instance v0, Lcom/epson/eposprint/EposException;

    invoke-direct {v0, p1}, Lcom/epson/eposprint/EposException;-><init>(I)V

    throw v0
.end method

.method public addTextLineSpace(I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposprint/EposException;
        }
    .end annotation

    .line 365
    iget-wide v0, p0, Lcom/epson/eposprint/Builder;->mCommandHandle:J

    invoke-direct {p0, v0, v1, p1}, Lcom/epson/eposprint/Builder;->eposAddTextLineSpace(JI)I

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 367
    :cond_0
    new-instance v0, Lcom/epson/eposprint/EposException;

    invoke-direct {v0, p1}, Lcom/epson/eposprint/EposException;-><init>(I)V

    throw v0
.end method

.method public addTextPosition(I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposprint/EposException;
        }
    .end annotation

    .line 434
    iget-wide v0, p0, Lcom/epson/eposprint/Builder;->mCommandHandle:J

    invoke-direct {p0, v0, v1, p1}, Lcom/epson/eposprint/Builder;->eposAddTextPosition(JI)I

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 436
    :cond_0
    new-instance v0, Lcom/epson/eposprint/EposException;

    invoke-direct {v0, p1}, Lcom/epson/eposprint/EposException;-><init>(I)V

    throw v0
.end method

.method public addTextRotate(I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposprint/EposException;
        }
    .end annotation

    .line 372
    iget-wide v0, p0, Lcom/epson/eposprint/Builder;->mCommandHandle:J

    invoke-direct {p0, v0, v1, p1}, Lcom/epson/eposprint/Builder;->eposAddTextRotate(JI)I

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 374
    :cond_0
    new-instance v0, Lcom/epson/eposprint/EposException;

    invoke-direct {v0, p1}, Lcom/epson/eposprint/EposException;-><init>(I)V

    throw v0
.end method

.method public addTextSize(II)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposprint/EposException;
        }
    .end annotation

    .line 418
    iget-wide v0, p0, Lcom/epson/eposprint/Builder;->mCommandHandle:J

    invoke-direct {p0, v0, v1, p1, p2}, Lcom/epson/eposprint/Builder;->eposAddTextSize(JII)I

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 420
    :cond_0
    new-instance p2, Lcom/epson/eposprint/EposException;

    invoke-direct {p2, p1}, Lcom/epson/eposprint/EposException;-><init>(I)V

    throw p2
.end method

.method public addTextSmooth(I)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposprint/EposException;
        }
    .end annotation

    .line 404
    iget-wide v0, p0, Lcom/epson/eposprint/Builder;->mCommandHandle:J

    invoke-direct {p0, v0, v1, p1}, Lcom/epson/eposprint/Builder;->eposAddTextSmooth(JI)I

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 406
    :cond_0
    new-instance v0, Lcom/epson/eposprint/EposException;

    invoke-direct {v0, p1}, Lcom/epson/eposprint/EposException;-><init>(I)V

    throw v0
.end method

.method public addTextStyle(IIII)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposprint/EposException;
        }
    .end annotation

    .line 426
    iget-wide v1, p0, Lcom/epson/eposprint/Builder;->mCommandHandle:J

    move-object v0, p0

    move v3, p1

    move v4, p2

    move v5, p3

    move v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/epson/eposprint/Builder;->eposAddTextStyle(JIIII)I

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 429
    :cond_0
    new-instance p2, Lcom/epson/eposprint/EposException;

    invoke-direct {p2, p1}, Lcom/epson/eposprint/EposException;-><init>(I)V

    throw p2
.end method

.method public addVLineBegin(II)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposprint/EposException;
        }
    .end annotation

    .line 619
    iget-wide v0, p0, Lcom/epson/eposprint/Builder;->mCommandHandle:J

    invoke-direct {p0, v0, v1, p1, p2}, Lcom/epson/eposprint/Builder;->eposAddVLineBegin(JII)I

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 621
    :cond_0
    new-instance p2, Lcom/epson/eposprint/EposException;

    invoke-direct {p2, p1}, Lcom/epson/eposprint/EposException;-><init>(I)V

    throw p2
.end method

.method public addVLineEnd(II)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/epson/eposprint/EposException;
        }
    .end annotation

    .line 626
    iget-wide v0, p0, Lcom/epson/eposprint/Builder;->mCommandHandle:J

    invoke-direct {p0, v0, v1, p1, p2}, Lcom/epson/eposprint/Builder;->eposAddVLineEnd(JII)I

    move-result p1

    if-nez p1, :cond_0

    return-void

    .line 628
    :cond_0
    new-instance p2, Lcom/epson/eposprint/EposException;

    invoke-direct {p2, p1}, Lcom/epson/eposprint/EposException;-><init>(I)V

    throw p2
.end method

.method public clearCommandBuffer()V
    .locals 2

    .line 354
    iget-wide v0, p0, Lcom/epson/eposprint/Builder;->mCommandHandle:J

    invoke-direct {p0, v0, v1}, Lcom/epson/eposprint/Builder;->eposClearCommandBuffer(J)I

    return-void
.end method

.method protected finalize()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    const-wide/16 v0, 0x0

    .line 345
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 348
    iget-wide v2, p0, Lcom/epson/eposprint/Builder;->mCommandHandle:J

    invoke-direct {p0, v2, v3}, Lcom/epson/eposprint/Builder;->eposDeleteCommandBuffer(J)I

    .line 349
    iput-wide v0, p0, Lcom/epson/eposprint/Builder;->mCommandHandle:J

    return-void

    :catchall_0
    move-exception v2

    .line 348
    iget-wide v3, p0, Lcom/epson/eposprint/Builder;->mCommandHandle:J

    invoke-direct {p0, v3, v4}, Lcom/epson/eposprint/Builder;->eposDeleteCommandBuffer(J)I

    .line 349
    iput-wide v0, p0, Lcom/epson/eposprint/Builder;->mCommandHandle:J

    .line 350
    throw v2
.end method

.method protected getHandle()J
    .locals 2

    .line 30
    iget-wide v0, p0, Lcom/epson/eposprint/Builder;->mCommandHandle:J

    return-wide v0
.end method
