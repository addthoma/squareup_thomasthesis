.class public Lcom/felhr/deviceids/CP210xIds;
.super Ljava/lang/Object;
.source "CP210xIds.java"


# static fields
.field private static final cp210xDevices:[J


# direct methods
.method static constructor <clinit>()V
    .locals 11

    const/16 v0, 0x88

    new-array v0, v0, [J

    const/16 v1, 0x53

    const/16 v2, 0x45b

    .line 13
    invoke-static {v2, v1}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v2

    const/4 v4, 0x0

    aput-wide v2, v0, v4

    const/16 v2, 0x471

    const/16 v3, 0x66a

    .line 14
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v2

    const/4 v4, 0x1

    aput-wide v2, v0, v4

    const/16 v2, 0x489

    const v3, 0xe000

    .line 15
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v5

    const/4 v3, 0x2

    aput-wide v5, v0, v3

    const v3, 0xe003

    .line 16
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v2

    const/4 v5, 0x3

    aput-wide v2, v0, v5

    const/16 v2, 0x745

    const/16 v3, 0x1000

    .line 17
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v2

    const/4 v6, 0x4

    aput-wide v2, v0, v6

    const/16 v2, 0x1100

    const/16 v3, 0x846

    .line 18
    invoke-static {v3, v2}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/4 v3, 0x5

    aput-wide v7, v0, v3

    const/16 v7, 0x8e6

    const/16 v8, 0x5501

    .line 19
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/4 v9, 0x6

    aput-wide v7, v0, v9

    const/16 v7, 0xa

    const/16 v8, 0x8fd

    .line 20
    invoke-static {v8, v7}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v8

    const/4 v10, 0x7

    aput-wide v8, v0, v10

    const/16 v8, 0xbed

    .line 21
    invoke-static {v8, v2}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v9

    const/16 v2, 0x8

    aput-wide v9, v0, v2

    const/16 v2, 0x1101

    .line 22
    invoke-static {v8, v2}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v8

    const/16 v2, 0x9

    aput-wide v8, v0, v2

    const/16 v2, 0xfcf

    const/16 v8, 0x1003

    .line 23
    invoke-static {v2, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v8

    aput-wide v8, v0, v7

    const/16 v7, 0x1004

    .line 24
    invoke-static {v2, v7}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v9, 0xb

    aput-wide v7, v0, v9

    const/16 v7, 0x1006

    .line 25
    invoke-static {v2, v7}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v2, 0xc

    aput-wide v7, v0, v2

    const/16 v2, 0xfde

    const v7, 0xca05

    .line 26
    invoke-static {v2, v7}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v2, 0xd

    aput-wide v7, v0, v2

    const/16 v2, 0x10a6

    const v7, 0xaa26

    .line 27
    invoke-static {v2, v7}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v2, 0xe

    aput-wide v7, v0, v2

    const/16 v2, 0x10ab

    const/16 v7, 0x10c5

    .line 28
    invoke-static {v2, v7}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v2, 0xf

    aput-wide v7, v0, v2

    const/16 v2, 0x10b5

    const v7, 0xac70

    .line 29
    invoke-static {v2, v7}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v2, 0x10

    aput-wide v7, v0, v2

    const/16 v2, 0x10c4

    const/16 v7, 0xf91

    .line 30
    invoke-static {v2, v7}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v9, 0x11

    aput-wide v7, v0, v9

    const/16 v7, 0x1101

    .line 31
    invoke-static {v2, v7}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v9, 0x12

    aput-wide v7, v0, v9

    const/16 v7, 0x1601

    .line 32
    invoke-static {v2, v7}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v9, 0x13

    aput-wide v7, v0, v9

    const v7, 0x800a

    .line 33
    invoke-static {v2, v7}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v9, 0x14

    aput-wide v7, v0, v9

    const v7, 0x803b    # 4.6E-41f

    .line 34
    invoke-static {v2, v7}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v9, 0x15

    aput-wide v7, v0, v9

    const v7, 0x8044

    .line 35
    invoke-static {v2, v7}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v9, 0x16

    aput-wide v7, v0, v9

    const v7, 0x804e

    .line 36
    invoke-static {v2, v7}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v9, 0x17

    aput-wide v7, v0, v9

    const v7, 0x8053

    .line 37
    invoke-static {v2, v7}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v9, 0x18

    aput-wide v7, v0, v9

    const v7, 0x8054

    .line 38
    invoke-static {v2, v7}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v9, 0x19

    aput-wide v7, v0, v9

    const v7, 0x8066

    .line 39
    invoke-static {v2, v7}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v9, 0x1a

    aput-wide v7, v0, v9

    const v7, 0x806f

    .line 40
    invoke-static {v2, v7}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v9, 0x1b

    aput-wide v7, v0, v9

    const v7, 0x807a

    .line 41
    invoke-static {v2, v7}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v9, 0x1c

    aput-wide v7, v0, v9

    const v7, 0x80c4

    .line 42
    invoke-static {v2, v7}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v9, 0x1d

    aput-wide v7, v0, v9

    const v7, 0x80ca

    .line 43
    invoke-static {v2, v7}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v9, 0x1e

    aput-wide v7, v0, v9

    const v7, 0x80dd

    .line 44
    invoke-static {v2, v7}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v9, 0x1f

    aput-wide v7, v0, v9

    const v7, 0x80f6

    .line 45
    invoke-static {v2, v7}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v9, 0x20

    aput-wide v7, v0, v9

    const v7, 0x8115

    .line 46
    invoke-static {v2, v7}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v9, 0x21

    aput-wide v7, v0, v9

    const v7, 0x813d

    .line 47
    invoke-static {v2, v7}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v9, 0x22

    aput-wide v7, v0, v9

    const v7, 0x813f

    .line 48
    invoke-static {v2, v7}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v9, 0x23

    aput-wide v7, v0, v9

    const v7, 0x814a

    .line 49
    invoke-static {v2, v7}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v9, 0x24

    aput-wide v7, v0, v9

    const v7, 0x814b

    .line 50
    invoke-static {v2, v7}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v9, 0x25

    aput-wide v7, v0, v9

    const/16 v7, 0x2405

    .line 51
    invoke-static {v7, v5}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v5, 0x26

    aput-wide v7, v0, v5

    const v5, 0x8156

    .line 52
    invoke-static {v2, v5}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v5, 0x27

    aput-wide v7, v0, v5

    const v5, 0x815e

    .line 53
    invoke-static {v2, v5}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v5, 0x28

    aput-wide v7, v0, v5

    const v5, 0x815f

    .line 54
    invoke-static {v2, v5}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v5, 0x29

    aput-wide v7, v0, v5

    const v5, 0x818b

    .line 55
    invoke-static {v2, v5}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v5, 0x2a

    aput-wide v7, v0, v5

    const v5, 0x819f

    .line 56
    invoke-static {v2, v5}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v5, 0x2b

    aput-wide v7, v0, v5

    const v5, 0x81a6

    .line 57
    invoke-static {v2, v5}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v5, 0x2c

    aput-wide v7, v0, v5

    const v5, 0x81a9

    .line 58
    invoke-static {v2, v5}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v5, 0x2d

    aput-wide v7, v0, v5

    const v5, 0x81ac

    .line 59
    invoke-static {v2, v5}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v5, 0x2e

    aput-wide v7, v0, v5

    const v5, 0x81ad

    .line 60
    invoke-static {v2, v5}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v5, 0x2f

    aput-wide v7, v0, v5

    const v5, 0x81c8

    .line 61
    invoke-static {v2, v5}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v5, 0x30

    aput-wide v7, v0, v5

    const v5, 0x81e2

    .line 62
    invoke-static {v2, v5}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v5, 0x31

    aput-wide v7, v0, v5

    const v5, 0x81e7

    .line 63
    invoke-static {v2, v5}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v5, 0x32

    aput-wide v7, v0, v5

    const v5, 0x81e8

    .line 64
    invoke-static {v2, v5}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v5, 0x33

    aput-wide v7, v0, v5

    const v5, 0x81f2

    .line 65
    invoke-static {v2, v5}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v5, 0x34

    aput-wide v7, v0, v5

    const v5, 0x8218

    .line 66
    invoke-static {v2, v5}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v5, 0x35

    aput-wide v7, v0, v5

    const v5, 0x822b

    .line 67
    invoke-static {v2, v5}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v5, 0x36

    aput-wide v7, v0, v5

    const v5, 0x826b

    .line 68
    invoke-static {v2, v5}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v5, 0x37

    aput-wide v7, v0, v5

    const v5, 0x8281

    .line 69
    invoke-static {v2, v5}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v5, 0x38

    aput-wide v7, v0, v5

    const v5, 0x8293

    .line 70
    invoke-static {v2, v5}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v5, 0x39

    aput-wide v7, v0, v5

    const v5, 0x82f9

    .line 71
    invoke-static {v2, v5}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v5, 0x3a

    aput-wide v7, v0, v5

    const v5, 0x8341

    .line 72
    invoke-static {v2, v5}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v5, 0x3b

    aput-wide v7, v0, v5

    const v5, 0x8382

    .line 73
    invoke-static {v2, v5}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v5, 0x3c

    aput-wide v7, v0, v5

    const v5, 0x83a8

    .line 74
    invoke-static {v2, v5}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v5, 0x3d

    aput-wide v7, v0, v5

    const v5, 0x83d8

    .line 75
    invoke-static {v2, v5}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v5, 0x3e

    aput-wide v7, v0, v5

    const v5, 0x8411

    .line 76
    invoke-static {v2, v5}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v5, 0x3f

    aput-wide v7, v0, v5

    const v5, 0x8418

    .line 77
    invoke-static {v2, v5}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v5, 0x40

    aput-wide v7, v0, v5

    const v5, 0x846e

    .line 78
    invoke-static {v2, v5}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v5, 0x41

    aput-wide v7, v0, v5

    const v5, 0x8477

    .line 79
    invoke-static {v2, v5}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v5, 0x42

    aput-wide v7, v0, v5

    const v5, 0x85ea

    .line 80
    invoke-static {v2, v5}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v5, 0x43

    aput-wide v7, v0, v5

    const v5, 0x85eb

    .line 81
    invoke-static {v2, v5}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v5, 0x44

    aput-wide v7, v0, v5

    const v5, 0x85f8

    .line 82
    invoke-static {v2, v5}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v5, 0x45

    aput-wide v7, v0, v5

    const v5, 0x8664

    .line 83
    invoke-static {v2, v5}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v5, 0x46

    aput-wide v7, v0, v5

    const v5, 0x8665

    .line 84
    invoke-static {v2, v5}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v5, 0x47

    aput-wide v7, v0, v5

    const v5, 0x875c

    .line 85
    invoke-static {v2, v5}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v5, 0x48

    aput-wide v7, v0, v5

    const v5, 0x88a4

    .line 86
    invoke-static {v2, v5}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v5, 0x49

    aput-wide v7, v0, v5

    const v5, 0x88a5

    .line 87
    invoke-static {v2, v5}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v5, 0x4a

    aput-wide v7, v0, v5

    const v5, 0xea60

    .line 88
    invoke-static {v2, v5}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v5, 0x4b

    aput-wide v7, v0, v5

    const v5, 0xea61

    .line 89
    invoke-static {v2, v5}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v5, 0x4c

    aput-wide v7, v0, v5

    const v5, 0xea63

    .line 90
    invoke-static {v2, v5}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v5, 0x4d

    aput-wide v7, v0, v5

    const v5, 0xea70

    .line 91
    invoke-static {v2, v5}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v5, 0x4e

    aput-wide v7, v0, v5

    const v5, 0xea71

    .line 92
    invoke-static {v2, v5}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v5, 0x4f

    aput-wide v7, v0, v5

    const v5, 0xea7a

    .line 93
    invoke-static {v2, v5}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v5, 0x50

    aput-wide v7, v0, v5

    const v5, 0xea7b

    .line 94
    invoke-static {v2, v5}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v5, 0x51

    aput-wide v7, v0, v5

    const v5, 0xea80

    .line 95
    invoke-static {v2, v5}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v5, 0x52

    aput-wide v7, v0, v5

    const v5, 0xf001

    .line 96
    invoke-static {v2, v5}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    aput-wide v7, v0, v1

    const v1, 0xf002

    .line 97
    invoke-static {v2, v1}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v1, 0x54

    aput-wide v7, v0, v1

    const v1, 0xf003

    .line 98
    invoke-static {v2, v1}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v1, 0x55

    aput-wide v7, v0, v1

    const v1, 0xf004

    .line 99
    invoke-static {v2, v1}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v1

    const/16 v5, 0x56

    aput-wide v1, v0, v5

    const/16 v1, 0x10c5

    const v2, 0xea61

    .line 100
    invoke-static {v1, v2}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v1

    const/16 v5, 0x57

    aput-wide v1, v0, v5

    const/16 v1, 0x10ce

    const v2, 0xea6a

    .line 101
    invoke-static {v1, v2}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v1

    const/16 v5, 0x58

    aput-wide v1, v0, v5

    const/16 v1, 0x13ad

    const v2, 0x9999

    .line 102
    invoke-static {v1, v2}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v1

    const/16 v5, 0x59

    aput-wide v1, v0, v5

    const/16 v1, 0x1555

    .line 103
    invoke-static {v1, v6}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v1

    const/16 v5, 0x5a

    aput-wide v1, v0, v5

    const/16 v1, 0x166a

    const/16 v2, 0x201

    .line 104
    invoke-static {v1, v2}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v5

    const/16 v2, 0x5b

    aput-wide v5, v0, v2

    const/16 v2, 0x301

    .line 105
    invoke-static {v1, v2}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v5

    const/16 v2, 0x5c

    aput-wide v5, v0, v2

    const/16 v2, 0x303

    .line 106
    invoke-static {v1, v2}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v5

    const/16 v2, 0x5d

    aput-wide v5, v0, v2

    const/16 v2, 0x304

    .line 107
    invoke-static {v1, v2}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v5

    const/16 v2, 0x5e

    aput-wide v5, v0, v2

    const/16 v2, 0x305

    .line 108
    invoke-static {v1, v2}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v5

    const/16 v2, 0x5f

    aput-wide v5, v0, v2

    const/16 v2, 0x401

    .line 109
    invoke-static {v1, v2}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v5

    const/16 v2, 0x60

    aput-wide v5, v0, v2

    const/16 v2, 0x101

    .line 110
    invoke-static {v1, v2}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v1

    const/16 v5, 0x61

    aput-wide v1, v0, v5

    const/16 v1, 0x16d6

    .line 111
    invoke-static {v1, v4}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v1

    const/16 v5, 0x62

    aput-wide v1, v0, v5

    const/16 v1, 0x16dc

    const/16 v2, 0x10

    .line 112
    invoke-static {v1, v2}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v5

    const/16 v2, 0x63

    aput-wide v5, v0, v2

    const/16 v2, 0x11

    .line 113
    invoke-static {v1, v2}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v5

    const/16 v2, 0x64

    aput-wide v5, v0, v2

    const/16 v2, 0x12

    .line 114
    invoke-static {v1, v2}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v5

    const/16 v2, 0x65

    aput-wide v5, v0, v2

    const/16 v2, 0x15

    .line 115
    invoke-static {v1, v2}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v1

    const/16 v5, 0x66

    aput-wide v1, v0, v5

    const/16 v1, 0x17a8

    .line 116
    invoke-static {v1, v4}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v1

    const/16 v5, 0x67

    aput-wide v1, v0, v5

    const/16 v1, 0x17a8

    .line 117
    invoke-static {v1, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v1

    const/16 v3, 0x68

    aput-wide v1, v0, v3

    const/16 v1, 0x17f4

    const v2, 0xaaaa

    .line 118
    invoke-static {v1, v2}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v1

    const/16 v3, 0x69

    aput-wide v1, v0, v3

    const/16 v1, 0x1843

    const/16 v2, 0x200

    .line 119
    invoke-static {v1, v2}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v1

    const/16 v3, 0x6a

    aput-wide v1, v0, v3

    const/16 v1, 0x18ef

    const v2, 0xe00f

    .line 120
    invoke-static {v1, v2}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v1

    const/16 v3, 0x6b

    aput-wide v1, v0, v3

    const/16 v1, 0x1adb

    .line 121
    invoke-static {v1, v4}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v1

    const/16 v3, 0x6c

    aput-wide v1, v0, v3

    const/16 v1, 0x1be3    # 1.0004E-41f

    const/16 v2, 0x7a6

    .line 122
    invoke-static {v1, v2}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v1

    const/16 v3, 0x6d

    aput-wide v1, v0, v3

    const/16 v1, 0x1e29

    const/16 v2, 0x102

    .line 123
    invoke-static {v1, v2}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v1

    const/16 v3, 0x6e

    aput-wide v1, v0, v3

    const/16 v1, 0x1e29

    const/16 v2, 0x501

    .line 124
    invoke-static {v1, v2}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v1

    const/16 v3, 0x6f

    aput-wide v1, v0, v3

    const/16 v1, 0x1fb9

    const/16 v2, 0x100

    .line 125
    invoke-static {v1, v2}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v2

    const/16 v4, 0x70

    aput-wide v2, v0, v4

    const/16 v2, 0x200

    .line 126
    invoke-static {v1, v2}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v2

    const/16 v4, 0x71

    aput-wide v2, v0, v4

    const/16 v2, 0x201

    .line 127
    invoke-static {v1, v2}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v2

    const/16 v4, 0x72

    aput-wide v2, v0, v4

    const/16 v2, 0x202

    .line 128
    invoke-static {v1, v2}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v2

    const/16 v4, 0x73

    aput-wide v2, v0, v4

    const/16 v2, 0x203

    .line 129
    invoke-static {v1, v2}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v2

    const/16 v4, 0x74

    aput-wide v2, v0, v4

    const/16 v2, 0x300

    .line 130
    invoke-static {v1, v2}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v2

    const/16 v4, 0x75

    aput-wide v2, v0, v4

    const/16 v2, 0x301

    .line 131
    invoke-static {v1, v2}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v2

    const/16 v4, 0x76

    aput-wide v2, v0, v4

    const/16 v2, 0x302

    .line 132
    invoke-static {v1, v2}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v2

    const/16 v4, 0x77

    aput-wide v2, v0, v4

    const/16 v2, 0x303

    .line 133
    invoke-static {v1, v2}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v2

    const/16 v4, 0x78

    aput-wide v2, v0, v4

    const/16 v2, 0x400

    .line 134
    invoke-static {v1, v2}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v2

    const/16 v4, 0x79

    aput-wide v2, v0, v4

    const/16 v2, 0x401

    .line 135
    invoke-static {v1, v2}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v2

    const/16 v4, 0x7a

    aput-wide v2, v0, v4

    const/16 v2, 0x402

    .line 136
    invoke-static {v1, v2}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v2

    const/16 v4, 0x7b

    aput-wide v2, v0, v4

    const/16 v2, 0x403

    .line 137
    invoke-static {v1, v2}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v2

    const/16 v4, 0x7c

    aput-wide v2, v0, v4

    const/16 v2, 0x404

    .line 138
    invoke-static {v1, v2}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v2

    const/16 v4, 0x7d

    aput-wide v2, v0, v4

    const/16 v2, 0x600

    .line 139
    invoke-static {v1, v2}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v2

    const/16 v4, 0x7e

    aput-wide v2, v0, v4

    const/16 v2, 0x601

    .line 140
    invoke-static {v1, v2}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v2

    const/16 v4, 0x7f

    aput-wide v2, v0, v4

    const/16 v2, 0x602

    .line 141
    invoke-static {v1, v2}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v2

    const/16 v4, 0x80

    aput-wide v2, v0, v4

    const/16 v2, 0x700

    .line 142
    invoke-static {v1, v2}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v2

    const/16 v4, 0x81

    aput-wide v2, v0, v4

    const/16 v2, 0x701

    .line 143
    invoke-static {v1, v2}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v1

    const/16 v3, 0x82

    aput-wide v1, v0, v3

    const/16 v1, 0x3195

    const v2, 0xf190

    .line 144
    invoke-static {v1, v2}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v2

    const/16 v4, 0x83

    aput-wide v2, v0, v4

    const v2, 0xf280

    .line 145
    invoke-static {v1, v2}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v2

    const/16 v4, 0x84

    aput-wide v2, v0, v4

    const v2, 0xf281

    .line 146
    invoke-static {v1, v2}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v1

    const/16 v3, 0x85

    aput-wide v1, v0, v3

    const/16 v1, 0x413c

    const v2, 0x9500

    .line 147
    invoke-static {v1, v2}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v1

    const/16 v3, 0x86

    aput-wide v1, v0, v3

    const/16 v1, 0x1908

    const/16 v2, 0x2311

    .line 148
    invoke-static {v1, v2}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v1

    const/16 v3, 0x87

    aput-wide v1, v0, v3

    .line 12
    invoke-static {v0}, Lcom/felhr/deviceids/Helpers;->createTable([J)[J

    move-result-object v0

    sput-object v0, Lcom/felhr/deviceids/CP210xIds;->cp210xDevices:[J

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static isDeviceSupported(II)Z
    .locals 1

    .line 153
    sget-object v0, Lcom/felhr/deviceids/CP210xIds;->cp210xDevices:[J

    invoke-static {v0, p0, p1}, Lcom/felhr/deviceids/Helpers;->exists([JII)Z

    move-result p0

    return p0
.end method
