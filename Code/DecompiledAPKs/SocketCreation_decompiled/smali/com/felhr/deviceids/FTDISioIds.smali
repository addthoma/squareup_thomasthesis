.class public Lcom/felhr/deviceids/FTDISioIds;
.super Ljava/lang/Object;
.source "FTDISioIds.java"


# static fields
.field private static final ftdiDevices:[J


# direct methods
.method static constructor <clinit>()V
    .locals 12

    const/16 v0, 0x21d

    new-array v0, v0, [J

    const/16 v1, 0x6001

    const/16 v2, 0x403

    .line 21
    invoke-static {v2, v1}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/4 v5, 0x0

    aput-wide v3, v0, v5

    const/16 v3, 0x6006

    .line 22
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/4 v5, 0x1

    aput-wide v3, v0, v5

    const/16 v3, 0x6010

    .line 23
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/4 v6, 0x2

    aput-wide v3, v0, v6

    const/16 v3, 0x6011

    .line 24
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/4 v6, 0x3

    aput-wide v3, v0, v6

    const/16 v3, 0x6014

    .line 25
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/4 v6, 0x4

    aput-wide v3, v0, v6

    const/16 v3, 0x6015

    .line 26
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/4 v6, 0x5

    aput-wide v3, v0, v6

    const v3, 0x8372

    .line 27
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/4 v6, 0x6

    aput-wide v3, v0, v6

    const v3, 0xfbfa

    .line 28
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/4 v6, 0x7

    aput-wide v3, v0, v6

    const/16 v3, 0x6002

    .line 29
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x8

    aput-wide v3, v0, v6

    const v3, 0x9e90

    .line 30
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x9

    aput-wide v3, v0, v6

    const v3, 0x9f80

    .line 31
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xa

    aput-wide v3, v0, v6

    const v3, 0xa6d0

    .line 32
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xb

    aput-wide v3, v0, v6

    const v3, 0xabb8

    .line 33
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xc

    aput-wide v3, v0, v6

    const v3, 0xabb9

    .line 34
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xd

    aput-wide v3, v0, v6

    const v3, 0xb810

    .line 35
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xe

    aput-wide v3, v0, v6

    const v3, 0xb811

    .line 36
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xf

    aput-wide v3, v0, v6

    const v3, 0xb812

    .line 37
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x10

    aput-wide v3, v0, v6

    const v3, 0xbaf8

    .line 38
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x11

    aput-wide v3, v0, v6

    const v3, 0xbcd8

    .line 39
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x12

    aput-wide v3, v0, v6

    const v3, 0xbcd9

    .line 40
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x13

    aput-wide v3, v0, v6

    const v3, 0xbcda

    .line 41
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x14

    aput-wide v3, v0, v6

    const v3, 0xbdc8

    .line 42
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x15

    aput-wide v3, v0, v6

    const v3, 0xbfd8

    .line 43
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x16

    aput-wide v3, v0, v6

    const v3, 0xbfd9

    .line 44
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x17

    aput-wide v3, v0, v6

    const v3, 0xbfda

    .line 45
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x18

    aput-wide v3, v0, v6

    const v3, 0xbfdb

    .line 46
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x19

    aput-wide v3, v0, v6

    const v3, 0xbfdc

    .line 47
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x1a

    aput-wide v3, v0, v6

    const v3, 0xbfdd

    .line 48
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x1b

    aput-wide v3, v0, v6

    const v3, 0xc1e0

    .line 49
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x1c

    aput-wide v3, v0, v6

    const v3, 0xc7d0

    .line 50
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x1d

    aput-wide v3, v0, v6

    const v3, 0xc850

    .line 51
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x1e

    aput-wide v3, v0, v6

    const v3, 0xc991

    .line 52
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x1f

    aput-wide v3, v0, v6

    const v3, 0xcaa0

    .line 53
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x20

    aput-wide v3, v0, v6

    const v3, 0xcc48

    .line 54
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x21

    aput-wide v3, v0, v6

    const v3, 0xcc49

    .line 55
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x22

    aput-wide v3, v0, v6

    const v3, 0xcc4a

    .line 56
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x23

    aput-wide v3, v0, v6

    const v3, 0xcff8

    .line 57
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x24

    aput-wide v3, v0, v6

    const v3, 0xd010

    .line 58
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x25

    aput-wide v3, v0, v6

    const v3, 0xd011

    .line 59
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x26

    aput-wide v3, v0, v6

    const v3, 0xd012

    .line 60
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x27

    aput-wide v3, v0, v6

    const v3, 0xd013

    .line 61
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x28

    aput-wide v3, v0, v6

    const v3, 0xd014

    .line 62
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x29

    aput-wide v3, v0, v6

    const v3, 0xd015

    .line 63
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x2a

    aput-wide v3, v0, v6

    const v3, 0xd016

    .line 64
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x2b

    aput-wide v3, v0, v6

    const v3, 0xd017

    .line 65
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x2c

    aput-wide v3, v0, v6

    const v3, 0xd070

    .line 66
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x2d

    aput-wide v3, v0, v6

    const v3, 0xd071

    .line 67
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x2e

    aput-wide v3, v0, v6

    const v3, 0xd678

    .line 68
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x2f

    aput-wide v3, v0, v6

    const v3, 0xd738

    .line 69
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x30

    aput-wide v3, v0, v6

    const v3, 0xd739

    .line 70
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x31

    aput-wide v3, v0, v6

    const v3, 0xd780

    .line 71
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x32

    aput-wide v3, v0, v6

    const v3, 0xf070

    .line 72
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x33

    aput-wide v3, v0, v6

    const v3, 0xd388

    .line 73
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x34

    aput-wide v3, v0, v6

    const v3, 0xd389

    .line 74
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x35

    aput-wide v3, v0, v6

    const v3, 0xd38a

    .line 75
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x36

    aput-wide v3, v0, v6

    const v3, 0xd38b

    .line 76
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x37

    aput-wide v3, v0, v6

    const v3, 0xd38c

    .line 77
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x38

    aput-wide v3, v0, v6

    const v3, 0xd38d

    .line 78
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x39

    aput-wide v3, v0, v6

    const v3, 0xd38e

    .line 79
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x3a

    aput-wide v3, v0, v6

    const v3, 0xd38f

    .line 80
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x3b

    aput-wide v3, v0, v6

    const v3, 0xd491

    .line 81
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x3c

    aput-wide v3, v0, v6

    const v3, 0xda70

    .line 82
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x3d

    aput-wide v3, v0, v6

    const v3, 0xda71

    .line 83
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x3e

    aput-wide v3, v0, v6

    const v3, 0xda72

    .line 84
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x3f

    aput-wide v3, v0, v6

    const v3, 0xda73

    .line 85
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x40

    aput-wide v3, v0, v6

    const v3, 0xda74

    .line 86
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x41

    aput-wide v3, v0, v6

    const v3, 0xdaf8

    .line 87
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x42

    aput-wide v3, v0, v6

    const v3, 0xdaf9

    .line 88
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x43

    aput-wide v3, v0, v6

    const v3, 0xdafa

    .line 89
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x44

    aput-wide v3, v0, v6

    const v3, 0xdafb

    .line 90
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x45

    aput-wide v3, v0, v6

    const v3, 0xdafc

    .line 91
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x46

    aput-wide v3, v0, v6

    const v3, 0xdafd

    .line 92
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x47

    aput-wide v3, v0, v6

    const v3, 0xdafe

    .line 93
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x48

    aput-wide v3, v0, v6

    const v3, 0xdaff

    .line 94
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x49

    aput-wide v3, v0, v6

    const v3, 0xdc00

    .line 95
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x4a

    aput-wide v3, v0, v6

    const v3, 0xdc01

    .line 96
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x4b

    aput-wide v3, v0, v6

    const v3, 0xdd20

    .line 97
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x4c

    aput-wide v3, v0, v6

    const v3, 0xdf28

    .line 98
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x4d

    aput-wide v3, v0, v6

    const v3, 0xdf30

    .line 99
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x4e

    aput-wide v3, v0, v6

    const v3, 0xdf32

    .line 100
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x4f

    aput-wide v3, v0, v6

    const v3, 0xdf31

    .line 101
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x50

    aput-wide v3, v0, v6

    const v3, 0xdf33

    .line 102
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x51

    aput-wide v3, v0, v6

    const v3, 0xdf35

    .line 103
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x52

    aput-wide v3, v0, v6

    const v3, 0xe050

    .line 104
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x53

    aput-wide v3, v0, v6

    const v3, 0xc006

    .line 105
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x54

    aput-wide v3, v0, v6

    const v3, 0xe000

    .line 106
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x55

    aput-wide v3, v0, v6

    const v3, 0xe001

    .line 107
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x56

    aput-wide v3, v0, v6

    const v3, 0xe002

    .line 108
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x57

    aput-wide v3, v0, v6

    const v3, 0xe004

    .line 109
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x58

    aput-wide v3, v0, v6

    const v3, 0xe006

    .line 110
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x59

    aput-wide v3, v0, v6

    const v3, 0xe008

    .line 111
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x5a

    aput-wide v3, v0, v6

    const v3, 0xe009

    .line 112
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x5b

    aput-wide v3, v0, v6

    const v3, 0xe00a

    .line 113
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x5c

    aput-wide v3, v0, v6

    const v3, 0xe0e8

    .line 114
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x5d

    aput-wide v3, v0, v6

    const v3, 0xe0e9

    .line 115
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x5e

    aput-wide v3, v0, v6

    const v3, 0xe0ea

    .line 116
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x5f

    aput-wide v3, v0, v6

    const v3, 0xe0eb

    .line 117
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x60

    aput-wide v3, v0, v6

    const v3, 0xe0ec

    .line 118
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x61

    aput-wide v3, v0, v6

    const v3, 0xe0ee

    .line 119
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x62

    aput-wide v3, v0, v6

    const v3, 0xe0ef

    .line 120
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x63

    aput-wide v3, v0, v6

    const v3, 0xe0f0

    .line 121
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x64

    aput-wide v3, v0, v6

    const v3, 0xe0f1

    .line 122
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x65

    aput-wide v3, v0, v6

    const v3, 0xe0f2

    .line 123
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x66

    aput-wide v3, v0, v6

    const v3, 0xe0f3

    .line 124
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x67

    aput-wide v3, v0, v6

    const v3, 0xe0f4

    .line 125
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x68

    aput-wide v3, v0, v6

    const v3, 0xe0f5

    .line 126
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x69

    aput-wide v3, v0, v6

    const v3, 0xe0f6

    .line 127
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x6a

    aput-wide v3, v0, v6

    const v3, 0xe0f7

    .line 128
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x6b

    aput-wide v3, v0, v6

    const v3, 0xe40b

    .line 129
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x6c

    aput-wide v3, v0, v6

    const v3, 0xf068

    .line 130
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x6d

    aput-wide v3, v0, v6

    const v3, 0xf069

    .line 131
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x6e

    aput-wide v3, v0, v6

    const v3, 0xf06a

    .line 132
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x6f

    aput-wide v3, v0, v6

    const v3, 0xf06b

    .line 133
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x70

    aput-wide v3, v0, v6

    const v3, 0xf06c

    .line 134
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x71

    aput-wide v3, v0, v6

    const v3, 0xf06d

    .line 135
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x72

    aput-wide v3, v0, v6

    const v3, 0xf06e

    .line 136
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x73

    aput-wide v3, v0, v6

    const v3, 0xf06f

    .line 137
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x74

    aput-wide v3, v0, v6

    const v3, 0xfb58

    .line 138
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x75

    aput-wide v3, v0, v6

    const v3, 0xfb5a

    .line 139
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x76

    aput-wide v3, v0, v6

    const v3, 0xfb5b

    .line 140
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x77

    aput-wide v3, v0, v6

    const v3, 0xfb59

    .line 141
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x78

    aput-wide v3, v0, v6

    const v3, 0xfb5c

    .line 142
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x79

    aput-wide v3, v0, v6

    const v3, 0xfb5d

    .line 143
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x7a

    aput-wide v3, v0, v6

    const v3, 0xfb5e

    .line 144
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x7b

    aput-wide v3, v0, v6

    const v3, 0xfb5f

    .line 145
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x7c

    aput-wide v3, v0, v6

    const v3, 0xe520

    .line 146
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x7d

    aput-wide v3, v0, v6

    const v3, 0xe548

    .line 147
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x7e

    aput-wide v3, v0, v6

    const v3, 0xe6c8

    .line 148
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x7f

    aput-wide v3, v0, v6

    const v3, 0xe700

    .line 149
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x80

    aput-wide v3, v0, v6

    const v3, 0xe808

    .line 150
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x81

    aput-wide v3, v0, v6

    const v3, 0xe809

    .line 151
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x82

    aput-wide v3, v0, v6

    const v3, 0xe80a

    .line 152
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x83

    aput-wide v3, v0, v6

    const v3, 0xe80b

    .line 153
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x84

    aput-wide v3, v0, v6

    const v3, 0xe80c

    .line 154
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x85

    aput-wide v3, v0, v6

    const v3, 0xe80d

    .line 155
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x86

    aput-wide v3, v0, v6

    const v3, 0xe80e

    .line 156
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x87

    aput-wide v3, v0, v6

    const v3, 0xe80f

    .line 157
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x88

    aput-wide v3, v0, v6

    const v3, 0xe888

    .line 158
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x89

    aput-wide v3, v0, v6

    const v3, 0xe889

    .line 159
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x8a

    aput-wide v3, v0, v6

    const v3, 0xe88a

    .line 160
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x8b

    aput-wide v3, v0, v6

    const v3, 0xe88b

    .line 161
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x8c

    aput-wide v3, v0, v6

    const v3, 0xe88c

    .line 162
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x8d

    aput-wide v3, v0, v6

    const v3, 0xe88d

    .line 163
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x8e

    aput-wide v3, v0, v6

    const v3, 0xe88e

    .line 164
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x8f

    aput-wide v3, v0, v6

    const v3, 0xe88f

    .line 165
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x90

    aput-wide v3, v0, v6

    const v3, 0xea90

    .line 166
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x91

    aput-wide v3, v0, v6

    const v3, 0xebe0

    .line 167
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x92

    aput-wide v3, v0, v6

    const v3, 0xec88

    .line 168
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x93

    aput-wide v3, v0, v6

    const v3, 0xec89

    .line 169
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x94

    aput-wide v3, v0, v6

    const v3, 0xed22

    .line 170
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x95

    aput-wide v3, v0, v6

    const v3, 0xed74

    .line 171
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x96

    aput-wide v3, v0, v6

    const v3, 0xed73

    .line 172
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x97

    aput-wide v3, v0, v6

    const v3, 0xed72

    .line 173
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x98

    aput-wide v3, v0, v6

    const v3, 0xed71

    .line 174
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x99

    aput-wide v3, v0, v6

    const v3, 0xee18

    .line 175
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x9a

    aput-wide v3, v0, v6

    const v3, 0xeee8

    .line 176
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x9b

    aput-wide v3, v0, v6

    const v3, 0xeee9

    .line 177
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x9c

    aput-wide v3, v0, v6

    const v3, 0xeeea

    .line 178
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x9d

    aput-wide v3, v0, v6

    const v3, 0xeeeb

    .line 179
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x9e

    aput-wide v3, v0, v6

    const v3, 0xeeec

    .line 180
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0x9f

    aput-wide v3, v0, v6

    const v3, 0xeeed

    .line 181
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xa0

    aput-wide v3, v0, v6

    const v3, 0xeeee

    .line 182
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xa1

    aput-wide v3, v0, v6

    const v3, 0xeeef

    .line 183
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xa2

    aput-wide v3, v0, v6

    const v3, 0xef50

    .line 184
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xa3

    aput-wide v3, v0, v6

    const v3, 0xef51

    .line 185
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xa4

    aput-wide v3, v0, v6

    const v3, 0xf0c0

    .line 186
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xa5

    aput-wide v3, v0, v6

    const v3, 0xf0c8

    .line 187
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xa6

    aput-wide v3, v0, v6

    const v3, 0xf0e9

    .line 188
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xa7

    aput-wide v3, v0, v6

    const v3, 0xf0ee

    .line 189
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xa8

    aput-wide v3, v0, v6

    const v3, 0xf208

    .line 190
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xa9

    aput-wide v3, v0, v6

    const v3, 0xf2d0

    .line 191
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xaa

    aput-wide v3, v0, v6

    const v3, 0xf3c0

    .line 192
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xab

    aput-wide v3, v0, v6

    const v3, 0xf3c1

    .line 193
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xac

    aput-wide v3, v0, v6

    const v3, 0xf3c2

    .line 194
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xad

    aput-wide v3, v0, v6

    const v3, 0xf448

    .line 195
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xae

    aput-wide v3, v0, v6

    const v3, 0xf449

    .line 196
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xaf

    aput-wide v3, v0, v6

    const v3, 0xf44a

    .line 197
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xb0

    aput-wide v3, v0, v6

    const v3, 0xf44b

    .line 198
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xb1

    aput-wide v3, v0, v6

    const v3, 0xf44c

    .line 199
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xb2

    aput-wide v3, v0, v6

    const v3, 0xf460

    .line 200
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xb3

    aput-wide v3, v0, v6

    const v3, 0xf680

    .line 201
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xb4

    aput-wide v3, v0, v6

    const v3, 0xf850

    .line 202
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xb5

    aput-wide v3, v0, v6

    const v3, 0xf9d0

    .line 203
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xb6

    aput-wide v3, v0, v6

    const v3, 0xf9d1

    .line 204
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xb7

    aput-wide v3, v0, v6

    const v3, 0xf9d2

    .line 205
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xb8

    aput-wide v3, v0, v6

    const v3, 0xf9d3

    .line 206
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xb9

    aput-wide v3, v0, v6

    const v3, 0xf9d4

    .line 207
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xba

    aput-wide v3, v0, v6

    const v3, 0xf9d5

    .line 208
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xbb

    aput-wide v3, v0, v6

    const v3, 0xfa00

    .line 209
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xbc

    aput-wide v3, v0, v6

    const v3, 0xfa01

    .line 210
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xbd

    aput-wide v3, v0, v6

    const v3, 0xfa02

    .line 211
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xbe

    aput-wide v3, v0, v6

    const v3, 0xfa03

    .line 212
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xbf

    aput-wide v3, v0, v6

    const v3, 0xfa04

    .line 213
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xc0

    aput-wide v3, v0, v6

    const v3, 0xfa05

    .line 214
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xc1

    aput-wide v3, v0, v6

    const v3, 0xfa06

    .line 215
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xc2

    aput-wide v3, v0, v6

    const v3, 0xfa78

    .line 216
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xc3

    aput-wide v3, v0, v6

    const v3, 0xfad0

    .line 217
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xc4

    aput-wide v3, v0, v6

    const v3, 0xfaf0

    .line 218
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xc5

    aput-wide v3, v0, v6

    const v3, 0xfc70

    .line 219
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xc6

    aput-wide v3, v0, v6

    const v3, 0xfc71

    .line 220
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xc7

    aput-wide v3, v0, v6

    const v3, 0xfc72

    .line 221
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xc8

    aput-wide v3, v0, v6

    const v3, 0xfc73

    .line 222
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xc9

    aput-wide v3, v0, v6

    const v3, 0xfc82

    .line 223
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xca

    aput-wide v3, v0, v6

    const v3, 0xfc8a

    .line 224
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xcb

    aput-wide v3, v0, v6

    const v3, 0xfc8b

    .line 225
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xcc

    aput-wide v3, v0, v6

    const v3, 0xfc60

    .line 226
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xcd

    aput-wide v3, v0, v6

    const v3, 0xfd60

    .line 227
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xce

    aput-wide v3, v0, v6

    const v3, 0xff20

    .line 228
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xcf

    aput-wide v3, v0, v6

    const v3, 0xf857

    .line 229
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xd0

    aput-wide v3, v0, v6

    const v3, 0xfa10

    .line 230
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xd1

    aput-wide v3, v0, v6

    const v3, 0xfa88

    .line 231
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xd2

    aput-wide v3, v0, v6

    const v3, 0xfb99

    .line 232
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xd3

    aput-wide v3, v0, v6

    const v3, 0xfc08

    .line 233
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xd4

    aput-wide v3, v0, v6

    const v3, 0xfc09

    .line 234
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xd5

    aput-wide v3, v0, v6

    const v3, 0xfc0a

    .line 235
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xd6

    aput-wide v3, v0, v6

    const v3, 0xfc0b

    .line 236
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xd7

    aput-wide v3, v0, v6

    const v3, 0xfc0c

    .line 237
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xd8

    aput-wide v3, v0, v6

    const v3, 0xfc0d

    .line 238
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xd9

    aput-wide v3, v0, v6

    const v3, 0xfc0e

    .line 239
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xda

    aput-wide v3, v0, v6

    const v3, 0xfc0f

    .line 240
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xdb

    aput-wide v3, v0, v6

    const v3, 0xfe38

    .line 241
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xdc

    aput-wide v3, v0, v6

    const v3, 0xff00

    .line 242
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xdd

    aput-wide v3, v0, v6

    const v3, 0xff38

    .line 243
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xde

    aput-wide v3, v0, v6

    const v3, 0xff39

    .line 244
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xdf

    aput-wide v3, v0, v6

    const v3, 0xff3a

    .line 245
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xe0

    aput-wide v3, v0, v6

    const v3, 0xff3b

    .line 246
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xe1

    aput-wide v3, v0, v6

    const v3, 0xff3c

    .line 247
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xe2

    aput-wide v3, v0, v6

    const v3, 0xff3d

    .line 248
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xe3

    aput-wide v3, v0, v6

    const v3, 0xff3e

    .line 249
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xe4

    aput-wide v3, v0, v6

    const v3, 0xff3f

    .line 250
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xe5

    aput-wide v3, v0, v6

    const v3, 0xffa8

    .line 251
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xe6

    aput-wide v3, v0, v6

    const v3, 0xfa33

    .line 252
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xe7

    aput-wide v3, v0, v6

    const v3, 0x8a98

    .line 253
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xe8

    aput-wide v3, v0, v6

    const/16 v3, 0x3eb

    const/16 v4, 0x2109

    .line 254
    invoke-static {v3, v4}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xe9

    aput-wide v3, v0, v6

    const/16 v3, 0x456

    const v4, 0xf000

    .line 255
    invoke-static {v3, v4}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xea

    aput-wide v3, v0, v6

    const/16 v3, 0x456

    const v4, 0xf001

    .line 256
    invoke-static {v3, v4}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xeb

    aput-wide v3, v0, v6

    const/16 v3, 0x584

    const v4, 0xb020

    .line 257
    invoke-static {v3, v4}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v6, 0xec

    aput-wide v3, v0, v6

    const/16 v3, 0x100

    const/16 v4, 0x647

    .line 258
    invoke-static {v4, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v6

    const/16 v4, 0xed

    aput-wide v6, v0, v4

    const/16 v4, 0x6ce

    const v6, 0x8311

    .line 259
    invoke-static {v4, v6}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v6

    const/16 v4, 0xee

    aput-wide v6, v0, v4

    const/16 v4, 0x6d3

    const/16 v6, 0x284

    .line 260
    invoke-static {v4, v6}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v6

    const/16 v4, 0xef

    aput-wide v6, v0, v4

    const/16 v4, 0x856

    const v6, 0xac01

    .line 261
    invoke-static {v4, v6}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v6

    const/16 v8, 0xf0

    aput-wide v6, v0, v8

    const v6, 0xac02

    .line 262
    invoke-static {v4, v6}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v6

    const/16 v8, 0xf1

    aput-wide v6, v0, v8

    const v6, 0xac03

    .line 263
    invoke-static {v4, v6}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v6

    const/16 v8, 0xf2

    aput-wide v6, v0, v8

    const v6, 0xac11

    .line 264
    invoke-static {v4, v6}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v6

    const/16 v8, 0xf3

    aput-wide v6, v0, v8

    const v6, 0xac12

    .line 265
    invoke-static {v4, v6}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v6

    const/16 v8, 0xf4

    aput-wide v6, v0, v8

    const v6, 0xac16

    .line 266
    invoke-static {v4, v6}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v6

    const/16 v8, 0xf5

    aput-wide v6, v0, v8

    const v6, 0xac17

    .line 267
    invoke-static {v4, v6}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v6

    const/16 v8, 0xf6

    aput-wide v6, v0, v8

    const v6, 0xac18

    .line 268
    invoke-static {v4, v6}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v6

    const/16 v8, 0xf7

    aput-wide v6, v0, v8

    const v6, 0xac19

    .line 269
    invoke-static {v4, v6}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v6

    const/16 v8, 0xf8

    aput-wide v6, v0, v8

    const v6, 0xac25

    .line 270
    invoke-static {v4, v6}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v6

    const/16 v8, 0xf9

    aput-wide v6, v0, v8

    const v6, 0xac26

    .line 271
    invoke-static {v4, v6}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v6

    const/16 v8, 0xfa

    aput-wide v6, v0, v8

    const v6, 0xac27

    .line 272
    invoke-static {v4, v6}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v6

    const/16 v8, 0xfb

    aput-wide v6, v0, v8

    const v6, 0xac33

    .line 273
    invoke-static {v4, v6}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v6

    const/16 v8, 0xfc

    aput-wide v6, v0, v8

    const v6, 0xac34

    .line 274
    invoke-static {v4, v6}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v6

    const/16 v8, 0xfd

    aput-wide v6, v0, v8

    const v6, 0xac49

    .line 275
    invoke-static {v4, v6}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v6

    const/16 v8, 0xfe

    aput-wide v6, v0, v8

    const v6, 0xac50

    .line 276
    invoke-static {v4, v6}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v6

    const/16 v8, 0xff

    aput-wide v6, v0, v8

    const v6, 0xba02

    .line 277
    invoke-static {v4, v6}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v6

    aput-wide v6, v0, v3

    const/16 v4, 0x93c

    const/16 v6, 0x601

    .line 278
    invoke-static {v4, v6}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v6

    const/16 v4, 0x101

    aput-wide v6, v0, v4

    const/16 v4, 0x93c

    const/16 v6, 0x701

    .line 279
    invoke-static {v4, v6}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v6

    const/16 v4, 0x102

    aput-wide v6, v0, v4

    const/16 v6, 0x300

    const/16 v7, 0xacd

    .line 280
    invoke-static {v7, v6}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v9, 0x103

    aput-wide v7, v0, v9

    const/16 v7, 0xb39

    .line 281
    invoke-static {v7, v9}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v10, 0x104

    aput-wide v7, v0, v10

    const/16 v7, 0xb39

    const/16 v8, 0x421

    .line 282
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v10, 0x105

    aput-wide v7, v0, v10

    const/16 v7, 0xc26

    const/4 v8, 0x4

    .line 283
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x106

    aput-wide v10, v0, v8

    const/16 v8, 0x18

    .line 284
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x107

    aput-wide v10, v0, v8

    const/16 v8, 0x9

    .line 285
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x108

    aput-wide v10, v0, v8

    const/16 v8, 0xa

    .line 286
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x109

    aput-wide v10, v0, v8

    const/16 v8, 0xb

    .line 287
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x10a

    aput-wide v10, v0, v8

    const/16 v8, 0xc

    .line 288
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x10b

    aput-wide v10, v0, v8

    const/16 v8, 0xd

    .line 289
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x10c

    aput-wide v10, v0, v8

    const/16 v8, 0x10

    .line 290
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x10d

    aput-wide v10, v0, v8

    const/16 v8, 0x11

    .line 291
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x10e

    aput-wide v10, v0, v8

    const/16 v8, 0x12

    .line 292
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x10f

    aput-wide v10, v0, v8

    const/16 v8, 0x13

    .line 293
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v10, 0x110

    aput-wide v7, v0, v10

    const/16 v7, 0xc33

    const/16 v8, 0x10

    .line 294
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v10, 0x111

    aput-wide v7, v0, v10

    const/16 v7, 0xc52

    const/16 v8, 0x2101

    .line 295
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x112

    aput-wide v10, v0, v8

    const/16 v8, 0x2101

    .line 296
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x113

    aput-wide v10, v0, v8

    const/16 v8, 0x2102

    .line 297
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x114

    aput-wide v10, v0, v8

    const/16 v8, 0x2103

    .line 298
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x115

    aput-wide v10, v0, v8

    const/16 v8, 0x2104

    .line 299
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x116

    aput-wide v10, v0, v8

    const v8, 0x9020

    .line 300
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x117

    aput-wide v10, v0, v8

    const/16 v8, 0x2211

    .line 301
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x118

    aput-wide v10, v0, v8

    const/16 v8, 0x2221

    .line 302
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x119

    aput-wide v10, v0, v8

    const/16 v8, 0x2212

    .line 303
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x11a

    aput-wide v10, v0, v8

    const/16 v8, 0x2222

    .line 304
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x11b

    aput-wide v10, v0, v8

    const/16 v8, 0x2213

    .line 305
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x11c

    aput-wide v10, v0, v8

    const/16 v8, 0x2223

    .line 306
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x11d

    aput-wide v10, v0, v8

    const/16 v8, 0x2411

    .line 307
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x11e

    aput-wide v10, v0, v8

    const/16 v8, 0x2421

    .line 308
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x11f

    aput-wide v10, v0, v8

    const/16 v8, 0x2431

    .line 309
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x120

    aput-wide v10, v0, v8

    const/16 v8, 0x2441

    .line 310
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x121

    aput-wide v10, v0, v8

    const/16 v8, 0x2412

    .line 311
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x122

    aput-wide v10, v0, v8

    const/16 v8, 0x2422

    .line 312
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x123

    aput-wide v10, v0, v8

    const/16 v8, 0x2432

    .line 313
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x124

    aput-wide v10, v0, v8

    const/16 v8, 0x2442

    .line 314
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x125

    aput-wide v10, v0, v8

    const/16 v8, 0x2413

    .line 315
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x126

    aput-wide v10, v0, v8

    const/16 v8, 0x2423

    .line 316
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x127

    aput-wide v10, v0, v8

    const/16 v8, 0x2433

    .line 317
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x128

    aput-wide v10, v0, v8

    const/16 v8, 0x2443

    .line 318
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x129

    aput-wide v10, v0, v8

    const/16 v8, 0x2811

    .line 319
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x12a

    aput-wide v10, v0, v8

    const/16 v8, 0x2821

    .line 320
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x12b

    aput-wide v10, v0, v8

    const/16 v8, 0x2831

    .line 321
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x12c

    aput-wide v10, v0, v8

    const/16 v8, 0x2841

    .line 322
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x12d

    aput-wide v10, v0, v8

    const/16 v8, 0x2851

    .line 323
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x12e

    aput-wide v10, v0, v8

    const/16 v8, 0x2861

    .line 324
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x12f

    aput-wide v10, v0, v8

    const/16 v8, 0x2871

    .line 325
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x130

    aput-wide v10, v0, v8

    const/16 v8, 0x2881

    .line 326
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x131

    aput-wide v10, v0, v8

    const/16 v8, 0x2812

    .line 327
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x132

    aput-wide v10, v0, v8

    const/16 v8, 0x2822

    .line 328
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x133

    aput-wide v10, v0, v8

    const/16 v8, 0x2832

    .line 329
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x134

    aput-wide v10, v0, v8

    const/16 v8, 0x2842

    .line 330
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x135

    aput-wide v10, v0, v8

    const/16 v8, 0x2852

    .line 331
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x136

    aput-wide v10, v0, v8

    const/16 v8, 0x2862

    .line 332
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x137

    aput-wide v10, v0, v8

    const/16 v8, 0x2872

    .line 333
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x138

    aput-wide v10, v0, v8

    const/16 v8, 0x2882

    .line 334
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x139

    aput-wide v10, v0, v8

    const/16 v8, 0x2813

    .line 335
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x13a

    aput-wide v10, v0, v8

    const/16 v8, 0x2823

    .line 336
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x13b

    aput-wide v10, v0, v8

    const/16 v8, 0x2833

    .line 337
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x13c

    aput-wide v10, v0, v8

    const/16 v8, 0x2843

    .line 338
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x13d

    aput-wide v10, v0, v8

    const/16 v8, 0x2853

    .line 339
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x13e

    aput-wide v10, v0, v8

    const/16 v8, 0x2863

    .line 340
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x13f

    aput-wide v10, v0, v8

    const/16 v8, 0x2873

    .line 341
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x140

    aput-wide v10, v0, v8

    const/16 v8, 0x2883

    .line 342
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x141

    aput-wide v10, v0, v8

    const v8, 0xa02a

    .line 343
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x142

    aput-wide v10, v0, v8

    const v8, 0xa02b

    .line 344
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x143

    aput-wide v10, v0, v8

    const v8, 0xa02c

    .line 345
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x144

    aput-wide v10, v0, v8

    const v8, 0xa02d

    .line 346
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v10, 0x145

    aput-wide v7, v0, v10

    const/16 v7, 0xc6c

    const/16 v8, 0x4b2

    .line 347
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v10, 0x146

    aput-wide v7, v0, v10

    const/16 v7, 0xc7d

    const/4 v8, 0x5

    .line 348
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v10, 0x147

    aput-wide v7, v0, v10

    const/16 v7, 0xd3a

    .line 349
    invoke-static {v7, v6}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v10, 0x148

    aput-wide v7, v0, v10

    const/16 v7, 0xd46

    const/16 v8, 0x2020

    .line 350
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v10, 0x149

    aput-wide v7, v0, v10

    const/16 v7, 0xd46

    const/16 v8, 0x2021

    .line 351
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v10, 0x14a

    aput-wide v7, v0, v10

    const/16 v7, 0xdcd

    .line 352
    invoke-static {v7, v5}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v10, 0x14b

    aput-wide v7, v0, v10

    const/16 v7, 0xf94

    .line 353
    invoke-static {v7, v5}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v10, 0x14c

    aput-wide v7, v0, v10

    const/16 v7, 0xf94

    const/4 v8, 0x5

    .line 354
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v10, 0x14d

    aput-wide v7, v0, v10

    const/16 v7, 0xfd8

    .line 355
    invoke-static {v7, v5}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v10, 0x14e

    aput-wide v7, v0, v10

    const/16 v7, 0x103e

    const/16 v8, 0x3e8

    .line 356
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v10, 0x14f

    aput-wide v7, v0, v10

    const/16 v7, 0x104d

    const/16 v8, 0x3000

    .line 357
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v10, 0x150

    aput-wide v7, v0, v10

    const/16 v7, 0x104d

    const/16 v8, 0x3002

    .line 358
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v10, 0x151

    aput-wide v7, v0, v10

    const/16 v7, 0x104d

    const/16 v8, 0x3006

    .line 359
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v10, 0x152

    aput-wide v7, v0, v10

    const/16 v7, 0x1209

    const/16 v8, 0x1002

    .line 360
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v10, 0x153

    aput-wide v7, v0, v10

    const/16 v7, 0x1209

    const/16 v8, 0x1006

    .line 361
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v10, 0x154

    aput-wide v7, v0, v10

    const/16 v7, 0x128d

    .line 362
    invoke-static {v7, v5}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v10, 0x155

    aput-wide v7, v0, v10

    const/16 v7, 0x1342

    const/16 v8, 0x202

    .line 363
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v10, 0x156

    aput-wide v7, v0, v10

    const/16 v7, 0x1457

    const/16 v8, 0x5118

    .line 364
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v10, 0x157

    aput-wide v7, v0, v10

    const/16 v7, 0x15ba

    const/4 v8, 0x3

    .line 365
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v10, 0x158

    aput-wide v7, v0, v10

    const/16 v7, 0x15ba

    const/16 v8, 0x2b

    .line 366
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v10, 0x159

    aput-wide v7, v0, v10

    const/16 v7, 0x1781

    const/16 v8, 0xc30

    .line 367
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v10, 0x15a

    aput-wide v7, v0, v10

    const/16 v7, 0x2100

    const v8, 0x9001

    .line 368
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x15b

    aput-wide v10, v0, v8

    const v8, 0x9e50

    .line 369
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x15c

    aput-wide v10, v0, v8

    const v8, 0x9e51

    .line 370
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x15d

    aput-wide v10, v0, v8

    const v8, 0x9e52

    .line 371
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x15e

    aput-wide v10, v0, v8

    const v8, 0x9e53

    .line 372
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x15f

    aput-wide v10, v0, v8

    const v8, 0x9e54

    .line 373
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x160

    aput-wide v10, v0, v8

    const v8, 0x9e55

    .line 374
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x161

    aput-wide v10, v0, v8

    const v8, 0x9e56

    .line 375
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x162

    aput-wide v10, v0, v8

    const v8, 0x9e57

    .line 376
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x163

    aput-wide v10, v0, v8

    const v8, 0x9e58

    .line 377
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x164

    aput-wide v10, v0, v8

    const v8, 0x9e59

    .line 378
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x165

    aput-wide v10, v0, v8

    const v8, 0x9e5a

    .line 379
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x166

    aput-wide v10, v0, v8

    const v8, 0x9e5b

    .line 380
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x167

    aput-wide v10, v0, v8

    const v8, 0x9e5c

    .line 381
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x168

    aput-wide v10, v0, v8

    const v8, 0x9e5d

    .line 382
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x169

    aput-wide v10, v0, v8

    const v8, 0x9e5e

    .line 383
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x16a

    aput-wide v10, v0, v8

    const v8, 0x9e5f

    .line 384
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x16b

    aput-wide v10, v0, v8

    const v8, 0x9e60

    .line 385
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x16c

    aput-wide v10, v0, v8

    const v8, 0x9e61

    .line 386
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x16d

    aput-wide v10, v0, v8

    const v8, 0x9e62

    .line 387
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x16e

    aput-wide v10, v0, v8

    const v8, 0x9e63

    .line 388
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x16f

    aput-wide v10, v0, v8

    const v8, 0x9e64

    .line 389
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x170

    aput-wide v10, v0, v8

    const v8, 0x9e65

    .line 390
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x171

    aput-wide v10, v0, v8

    const v8, 0x9e65

    .line 391
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x172

    aput-wide v10, v0, v8

    const v8, 0x9e66

    .line 392
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x173

    aput-wide v10, v0, v8

    const v8, 0x9e67

    .line 393
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x174

    aput-wide v10, v0, v8

    const v8, 0x9e68

    .line 394
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x175

    aput-wide v10, v0, v8

    const v8, 0x9e69

    .line 395
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x176

    aput-wide v10, v0, v8

    const v8, 0x9e6a

    .line 396
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v10, 0x177

    aput-wide v7, v0, v10

    const v7, 0xe0a0

    .line 397
    invoke-static {v2, v7}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v10, 0x178

    aput-wide v7, v0, v10

    const v7, 0xe0a1

    .line 398
    invoke-static {v2, v7}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v10, 0x179

    aput-wide v7, v0, v10

    const/16 v7, 0x1a72

    const/16 v8, 0x1000

    .line 399
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x17a

    aput-wide v10, v0, v8

    const/16 v8, 0x1001

    .line 400
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x17b

    aput-wide v10, v0, v8

    const/16 v8, 0x1002

    .line 401
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x17c

    aput-wide v10, v0, v8

    const/16 v8, 0x1005

    .line 402
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x17d

    aput-wide v10, v0, v8

    const/16 v8, 0x1007

    .line 403
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x17e

    aput-wide v10, v0, v8

    const/16 v8, 0x1008

    .line 404
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x17f

    aput-wide v10, v0, v8

    const/16 v8, 0x1009

    .line 405
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x180

    aput-wide v10, v0, v8

    const/16 v8, 0x100d

    .line 406
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x181

    aput-wide v10, v0, v8

    const/16 v8, 0x100e

    .line 407
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x182

    aput-wide v10, v0, v8

    const/16 v8, 0x100f

    .line 408
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x183

    aput-wide v10, v0, v8

    const/16 v8, 0x1011

    .line 409
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x184

    aput-wide v10, v0, v8

    const/16 v8, 0x1012

    .line 410
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x185

    aput-wide v10, v0, v8

    const/16 v8, 0x1013

    .line 411
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x186

    aput-wide v10, v0, v8

    const/16 v8, 0x1014

    .line 412
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x187

    aput-wide v10, v0, v8

    const/16 v8, 0x1015

    .line 413
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x188

    aput-wide v10, v0, v8

    const/16 v8, 0x1016

    .line 414
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v10, 0x189

    aput-wide v7, v0, v10

    const/16 v7, 0x165c

    const/4 v8, 0x2

    .line 415
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v10, 0x18a

    aput-wide v7, v0, v10

    const/16 v7, 0x1a79

    .line 416
    invoke-static {v7, v1}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v10, 0x18b

    aput-wide v7, v0, v10

    const/16 v7, 0x1b3d

    .line 417
    invoke-static {v7, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x18c

    aput-wide v10, v0, v8

    const/16 v8, 0x101

    .line 418
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x18d

    aput-wide v10, v0, v8

    .line 419
    invoke-static {v7, v4}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x18e

    aput-wide v10, v0, v8

    .line 420
    invoke-static {v7, v9}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x18f

    aput-wide v10, v0, v8

    const/16 v8, 0x104

    .line 421
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x190

    aput-wide v10, v0, v8

    const/16 v8, 0x105

    .line 422
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x191

    aput-wide v10, v0, v8

    const/16 v8, 0x106

    .line 423
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x192

    aput-wide v10, v0, v8

    const/16 v8, 0x107

    .line 424
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x193

    aput-wide v10, v0, v8

    const/16 v8, 0x108

    .line 425
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x194

    aput-wide v10, v0, v8

    const/16 v8, 0x109

    .line 426
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x195

    aput-wide v10, v0, v8

    const/16 v8, 0x10a

    .line 427
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x196

    aput-wide v10, v0, v8

    const/16 v8, 0x10b

    .line 428
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x197

    aput-wide v10, v0, v8

    const/16 v8, 0x10c

    .line 429
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x198

    aput-wide v10, v0, v8

    const/16 v8, 0x10d

    .line 430
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x199

    aput-wide v10, v0, v8

    const/16 v8, 0x10e

    .line 431
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x19a

    aput-wide v10, v0, v8

    const/16 v8, 0x10f

    .line 432
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x19b

    aput-wide v10, v0, v8

    const/16 v8, 0x110

    .line 433
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x19c

    aput-wide v10, v0, v8

    const/16 v8, 0x111

    .line 434
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x19d

    aput-wide v10, v0, v8

    const/16 v8, 0x112

    .line 435
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x19e

    aput-wide v10, v0, v8

    const/16 v8, 0x113

    .line 436
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x19f

    aput-wide v10, v0, v8

    const/16 v8, 0x114

    .line 437
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x1a0

    aput-wide v10, v0, v8

    const/16 v8, 0x115

    .line 438
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x1a1

    aput-wide v10, v0, v8

    const/16 v8, 0x116

    .line 439
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x1a2

    aput-wide v10, v0, v8

    const/16 v8, 0x117

    .line 440
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x1a3

    aput-wide v10, v0, v8

    const/16 v8, 0x118

    .line 441
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x1a4

    aput-wide v10, v0, v8

    const/16 v8, 0x119

    .line 442
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x1a5

    aput-wide v10, v0, v8

    const/16 v8, 0x11a

    .line 443
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x1a6

    aput-wide v10, v0, v8

    const/16 v8, 0x11b

    .line 444
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x1a7

    aput-wide v10, v0, v8

    const/16 v8, 0x11c

    .line 445
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x1a8

    aput-wide v10, v0, v8

    const/16 v8, 0x11d

    .line 446
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x1a9

    aput-wide v10, v0, v8

    const/16 v8, 0x11e

    .line 447
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x1aa

    aput-wide v10, v0, v8

    const/16 v8, 0x11f

    .line 448
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x1ab

    aput-wide v10, v0, v8

    const/16 v8, 0x120

    .line 449
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x1ac

    aput-wide v10, v0, v8

    const/16 v8, 0x121

    .line 450
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x1ad

    aput-wide v10, v0, v8

    const/16 v8, 0x122

    .line 451
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x1ae

    aput-wide v10, v0, v8

    const/16 v8, 0x123

    .line 452
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x1af

    aput-wide v10, v0, v8

    const/16 v8, 0x124

    .line 453
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x1b0

    aput-wide v10, v0, v8

    const/16 v8, 0x125

    .line 454
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x1b1

    aput-wide v10, v0, v8

    const/16 v8, 0x126

    .line 455
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x1b2

    aput-wide v10, v0, v8

    const/16 v8, 0x127

    .line 456
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x1b3

    aput-wide v10, v0, v8

    const/16 v8, 0x128

    .line 457
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x1b4

    aput-wide v10, v0, v8

    const/16 v8, 0x129

    .line 458
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x1b5

    aput-wide v10, v0, v8

    const/16 v8, 0x12a

    .line 459
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x1b6

    aput-wide v10, v0, v8

    const/16 v8, 0x12b

    .line 460
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x1b7

    aput-wide v10, v0, v8

    const/16 v8, 0x12c

    .line 461
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x1b8

    aput-wide v10, v0, v8

    const/16 v8, 0x12e

    .line 462
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x1b9

    aput-wide v10, v0, v8

    const/16 v8, 0x12f

    .line 463
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v10

    const/16 v8, 0x1ba

    aput-wide v10, v0, v8

    const/16 v8, 0x130

    .line 464
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v10, 0x1bb

    aput-wide v7, v0, v10

    const/16 v7, 0x1b91

    const/16 v8, 0x64

    .line 466
    invoke-static {v7, v8}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v10, 0x1bc

    aput-wide v7, v0, v10

    const/16 v7, 0x1bc9

    .line 467
    invoke-static {v7, v1}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v10, 0x1bd

    aput-wide v7, v0, v10

    const/16 v7, 0x1c0c

    .line 468
    invoke-static {v7, v4}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v10, 0x1be

    aput-wide v7, v0, v10

    const/16 v7, 0x1cf1

    .line 469
    invoke-static {v7, v5}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v5, 0x1bf

    aput-wide v7, v0, v5

    const/16 v5, 0x1cf1

    const/16 v7, 0x41

    .line 470
    invoke-static {v5, v7}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v5, 0x1c0

    aput-wide v7, v0, v5

    const/16 v5, 0x483

    const/16 v7, 0x3746

    .line 471
    invoke-static {v5, v7}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v5, 0x1c1

    aput-wide v7, v0, v5

    const/16 v5, 0x483

    const/16 v7, 0x3747

    .line 472
    invoke-static {v5, v7}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v5, 0x1c2

    aput-wide v7, v0, v5

    const/16 v5, 0x5050

    .line 473
    invoke-static {v5, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v3, 0x1c3

    aput-wide v7, v0, v3

    const/16 v3, 0x101

    .line 474
    invoke-static {v5, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v7

    const/16 v3, 0x1c4

    aput-wide v7, v0, v3

    .line 475
    invoke-static {v5, v4}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v7, 0x1c5

    aput-wide v3, v0, v7

    .line 476
    invoke-static {v5, v9}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v7, 0x1c6

    aput-wide v3, v0, v7

    const/16 v3, 0x104

    .line 477
    invoke-static {v5, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v7, 0x1c7

    aput-wide v3, v0, v7

    const/16 v3, 0x105

    .line 478
    invoke-static {v5, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v7, 0x1c8

    aput-wide v3, v0, v7

    const/16 v3, 0x106

    .line 479
    invoke-static {v5, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v7, 0x1c9

    aput-wide v3, v0, v7

    const/16 v3, 0x107

    .line 480
    invoke-static {v5, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v7, 0x1ca

    aput-wide v3, v0, v7

    .line 481
    invoke-static {v5, v6}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v7, 0x1cb

    aput-wide v3, v0, v7

    const/16 v3, 0x301

    .line 482
    invoke-static {v5, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v7, 0x1cc

    aput-wide v3, v0, v7

    const/16 v3, 0x400

    .line 483
    invoke-static {v5, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v7, 0x1cd

    aput-wide v3, v0, v7

    const/16 v3, 0x500

    .line 484
    invoke-static {v5, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v7, 0x1ce

    aput-wide v3, v0, v7

    const/16 v3, 0x700

    .line 485
    invoke-static {v5, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v7, 0x1cf

    aput-wide v3, v0, v7

    const/16 v3, 0x800

    .line 486
    invoke-static {v5, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v7, 0x1d0

    aput-wide v3, v0, v7

    const/16 v3, 0x900

    .line 487
    invoke-static {v5, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v7, 0x1d1

    aput-wide v3, v0, v7

    const/16 v3, 0xa00

    .line 488
    invoke-static {v5, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v7, 0x1d2

    aput-wide v3, v0, v7

    const/16 v3, 0xb00

    .line 489
    invoke-static {v5, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v7, 0x1d3

    aput-wide v3, v0, v7

    const/16 v3, 0xc00

    .line 490
    invoke-static {v5, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v7, 0x1d4

    aput-wide v3, v0, v7

    const/16 v3, 0xd00

    .line 491
    invoke-static {v5, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v7, 0x1d5

    aput-wide v3, v0, v7

    const/16 v3, 0xe00

    .line 492
    invoke-static {v5, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v7, 0x1d6

    aput-wide v3, v0, v7

    const/16 v3, 0xf00

    .line 493
    invoke-static {v5, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v7, 0x1d7

    aput-wide v3, v0, v7

    const/16 v3, 0x1000

    .line 494
    invoke-static {v5, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v7, 0x1d8

    aput-wide v3, v0, v7

    const v3, 0x8000

    .line 495
    invoke-static {v5, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v7, 0x1d9

    aput-wide v3, v0, v7

    const v3, 0x8001

    .line 496
    invoke-static {v5, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v7, 0x1da

    aput-wide v3, v0, v7

    const v3, 0x8002

    .line 497
    invoke-static {v5, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v7, 0x1db

    aput-wide v3, v0, v7

    const v3, 0x8003

    .line 498
    invoke-static {v5, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v7, 0x1dc

    aput-wide v3, v0, v7

    const v3, 0x8004

    .line 499
    invoke-static {v5, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v7, 0x1dd

    aput-wide v3, v0, v7

    const v3, 0x8005

    .line 500
    invoke-static {v5, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v5, 0x1de

    aput-wide v3, v0, v5

    const v3, 0x9e88

    const v4, 0x9e8f

    .line 501
    invoke-static {v3, v4}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v5, 0x1df

    aput-wide v3, v0, v5

    const v3, 0xdeee

    .line 502
    invoke-static {v3, v6}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v5, 0x1e0

    aput-wide v3, v0, v5

    const v3, 0xdeee

    const/16 v4, 0x2ff

    .line 503
    invoke-static {v3, v4}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v5, 0x1e1

    aput-wide v3, v0, v5

    const v3, 0xdeee

    const/16 v4, 0x302

    .line 504
    invoke-static {v3, v4}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v5, 0x1e2

    aput-wide v3, v0, v5

    const v3, 0xdeee

    const/16 v4, 0x303

    .line 505
    invoke-static {v3, v4}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v5, 0x1e3

    aput-wide v3, v0, v5

    const v3, 0x9378

    .line 506
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v5, 0x1e4

    aput-wide v3, v0, v5

    const/16 v3, 0x379

    .line 507
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v5, 0x1e5

    aput-wide v3, v0, v5

    const v3, 0x937a

    .line 508
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v5, 0x1e6

    aput-wide v3, v0, v5

    const v3, 0x937c

    .line 509
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v5, 0x1e7

    aput-wide v3, v0, v5

    const v3, 0x9868

    .line 510
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v5, 0x1e8

    aput-wide v3, v0, v5

    const v3, 0xbca0

    .line 511
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v5, 0x1e9

    aput-wide v3, v0, v5

    const v3, 0xbca1

    .line 512
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v5, 0x1ea

    aput-wide v3, v0, v5

    const v3, 0xbca2

    .line 513
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v5, 0x1eb

    aput-wide v3, v0, v5

    const v3, 0xbca4

    .line 514
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v5, 0x1ec

    aput-wide v3, v0, v5

    const v3, 0xe729

    .line 515
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v5, 0x1ed

    aput-wide v3, v0, v5

    const v3, 0xd578

    .line 516
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v5, 0x1ee

    aput-wide v3, v0, v5

    const v3, 0xff18

    .line 517
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v5, 0x1ef

    aput-wide v3, v0, v5

    const v3, 0xff1c

    .line 518
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v5, 0x1f0

    aput-wide v3, v0, v5

    const v3, 0xff1d

    .line 519
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v5, 0x1f1

    aput-wide v3, v0, v5

    const/16 v3, 0x20b7

    .line 520
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v5, 0x1f2

    aput-wide v3, v0, v5

    const/16 v3, 0x713

    .line 521
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v5, 0x1f3

    aput-wide v3, v0, v5

    const v3, 0xf608

    .line 522
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v5, 0x1f4

    aput-wide v3, v0, v5

    const v3, 0xf60b

    .line 523
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v5, 0x1f5

    aput-wide v3, v0, v5

    const v3, 0xf7c0

    .line 524
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v5, 0x1f6

    aput-wide v3, v0, v5

    const v3, 0x8a28

    .line 525
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v5, 0x1f7

    aput-wide v3, v0, v5

    const v3, 0xa951

    .line 526
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v5, 0x1f8

    aput-wide v3, v0, v5

    const v3, 0x8e08

    .line 527
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v5, 0x1f9

    aput-wide v3, v0, v5

    const/16 v3, 0x11

    .line 528
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v5, 0x1fa

    aput-wide v3, v0, v5

    const v3, 0x87d0

    .line 529
    invoke-static {v2, v3}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v5, 0x1fb

    aput-wide v3, v0, v5

    const/16 v3, 0x5d1

    const/16 v4, 0x1001

    .line 530
    invoke-static {v3, v4}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v4

    const/16 v6, 0x1fc

    aput-wide v4, v0, v6

    const/16 v4, 0x1002

    .line 531
    invoke-static {v3, v4}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v4

    const/16 v6, 0x1fd

    aput-wide v4, v0, v6

    const/16 v4, 0x1003

    .line 532
    invoke-static {v3, v4}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v4

    const/16 v6, 0x1fe

    aput-wide v4, v0, v6

    const/16 v4, 0x1004

    .line 533
    invoke-static {v3, v4}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v4

    const/16 v6, 0x1ff

    aput-wide v4, v0, v6

    const/16 v4, 0x1011

    .line 534
    invoke-static {v3, v4}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v4

    const/16 v6, 0x200

    aput-wide v4, v0, v6

    const/16 v4, 0x1013

    .line 535
    invoke-static {v3, v4}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v4

    const/16 v6, 0x201

    aput-wide v4, v0, v6

    const/16 v4, 0x2001

    .line 536
    invoke-static {v3, v4}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v4

    const/16 v6, 0x202

    aput-wide v4, v0, v6

    const/16 v4, 0x2002

    .line 537
    invoke-static {v3, v4}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v4

    const/16 v6, 0x203

    aput-wide v4, v0, v6

    const/16 v4, 0x2003

    .line 538
    invoke-static {v3, v4}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v4

    const/16 v6, 0x204

    aput-wide v4, v0, v6

    const/16 v4, 0x2011

    .line 539
    invoke-static {v3, v4}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v4

    const/16 v6, 0x205

    aput-wide v4, v0, v6

    const/16 v4, 0x2012

    .line 540
    invoke-static {v3, v4}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v4

    const/16 v6, 0x206

    aput-wide v4, v0, v6

    const/16 v4, 0x2021

    .line 541
    invoke-static {v3, v4}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v4

    const/16 v6, 0x207

    aput-wide v4, v0, v6

    const/16 v4, 0x2022

    .line 542
    invoke-static {v3, v4}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v4

    const/16 v6, 0x208

    aput-wide v4, v0, v6

    const/16 v4, 0x2023

    .line 543
    invoke-static {v3, v4}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v4

    const/16 v6, 0x209

    aput-wide v4, v0, v6

    const/16 v4, 0x2024

    .line 544
    invoke-static {v3, v4}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v4

    const/16 v6, 0x20a

    aput-wide v4, v0, v6

    const/16 v4, 0x3011

    .line 545
    invoke-static {v3, v4}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v4

    const/16 v6, 0x20b

    aput-wide v4, v0, v6

    const/16 v4, 0x3012

    .line 546
    invoke-static {v3, v4}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v4

    const/16 v6, 0x20c

    aput-wide v4, v0, v6

    const/16 v4, 0x5001

    .line 547
    invoke-static {v3, v4}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v4

    const/16 v6, 0x20d

    aput-wide v4, v0, v6

    .line 548
    invoke-static {v3, v1}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v4

    const/16 v1, 0x20e

    aput-wide v4, v0, v1

    const/16 v1, 0x7001

    .line 549
    invoke-static {v3, v1}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v4

    const/16 v1, 0x20f

    aput-wide v4, v0, v1

    const v1, 0x8001

    .line 550
    invoke-static {v3, v1}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v4

    const/16 v1, 0x210

    aput-wide v4, v0, v1

    const v1, 0x8002

    .line 551
    invoke-static {v3, v1}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v4

    const/16 v1, 0x211

    aput-wide v4, v0, v1

    const v1, 0x8003

    .line 552
    invoke-static {v3, v1}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v4

    const/16 v1, 0x212

    aput-wide v4, v0, v1

    const v1, 0x8004

    .line 553
    invoke-static {v3, v1}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v4

    const/16 v1, 0x213

    aput-wide v4, v0, v1

    const v1, 0x9001

    .line 554
    invoke-static {v3, v1}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v4

    const/16 v1, 0x214

    aput-wide v4, v0, v1

    const v1, 0x9002

    .line 555
    invoke-static {v3, v1}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v4

    const/16 v1, 0x215

    aput-wide v4, v0, v1

    const v1, 0x9003

    .line 556
    invoke-static {v3, v1}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v4

    const/16 v1, 0x216

    aput-wide v4, v0, v1

    const v1, 0x9004

    .line 557
    invoke-static {v3, v1}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v4

    const/16 v1, 0x217

    aput-wide v4, v0, v1

    const v1, 0x9005

    .line 558
    invoke-static {v3, v1}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v4

    const/16 v1, 0x218

    aput-wide v4, v0, v1

    const v1, 0x9006

    .line 559
    invoke-static {v3, v1}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v4

    const/16 v1, 0x219

    aput-wide v4, v0, v1

    const v1, 0x9007

    .line 560
    invoke-static {v3, v1}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v4

    const/16 v1, 0x21a

    aput-wide v4, v0, v1

    const v1, 0x9008

    .line 561
    invoke-static {v3, v1}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v3

    const/16 v1, 0x21b

    aput-wide v3, v0, v1

    const/4 v1, 0x0

    .line 562
    invoke-static {v2, v1}, Lcom/felhr/deviceids/Helpers;->createDevice(II)J

    move-result-wide v1

    const/16 v3, 0x21c

    aput-wide v1, v0, v3

    .line 20
    invoke-static {v0}, Lcom/felhr/deviceids/Helpers;->createTable([J)[J

    move-result-object v0

    sput-object v0, Lcom/felhr/deviceids/FTDISioIds;->ftdiDevices:[J

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static isDeviceIdSupported(II)Z
    .locals 1

    .line 584
    sget-object v0, Lcom/felhr/deviceids/FTDISioIds;->ftdiDevices:[J

    invoke-static {v0, p0, p1}, Lcom/felhr/deviceids/Helpers;->exists([JII)Z

    move-result p0

    return p0
.end method

.method public static isDeviceSupported(Landroid/hardware/usb/UsbDevice;)Z
    .locals 3

    .line 578
    sget-object v0, Lcom/felhr/deviceids/FTDISioIds;->ftdiDevices:[J

    invoke-virtual {p0}, Landroid/hardware/usb/UsbDevice;->getVendorId()I

    move-result v1

    invoke-virtual {p0}, Landroid/hardware/usb/UsbDevice;->getProductId()I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/felhr/deviceids/Helpers;->exists([JII)Z

    move-result v0

    if-nez v0, :cond_1

    .line 579
    invoke-static {p0}, Lcom/felhr/deviceids/FTDISioIds;->isMicrochipFtdi(Landroid/hardware/usb/UsbDevice;)Z

    move-result p0

    if-eqz p0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method private static isMicrochipFtdi(Landroid/hardware/usb/UsbDevice;)Z
    .locals 5

    .line 566
    invoke-virtual {p0}, Landroid/hardware/usb/UsbDevice;->getVendorId()I

    move-result v0

    const/4 v1, 0x0

    const/16 v2, 0x4d8

    if-ne v0, v2, :cond_2

    invoke-virtual {p0}, Landroid/hardware/usb/UsbDevice;->getProductId()I

    move-result v0

    const/16 v2, 0xa

    if-eq v0, v2, :cond_0

    goto :goto_1

    :cond_0
    const/4 v0, 0x0

    .line 568
    :goto_0
    invoke-virtual {p0}, Landroid/hardware/usb/UsbDevice;->getInterfaceCount()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 569
    invoke-virtual {p0, v0}, Landroid/hardware/usb/UsbDevice;->getInterface(I)Landroid/hardware/usb/UsbInterface;

    move-result-object v2

    .line 570
    invoke-virtual {v2}, Landroid/hardware/usb/UsbInterface;->getInterfaceClass()I

    move-result v3

    const/16 v4, 0xff

    if-ne v3, v4, :cond_1

    invoke-virtual {v2}, Landroid/hardware/usb/UsbInterface;->getInterfaceSubclass()I

    move-result v3

    if-ne v3, v4, :cond_1

    .line 571
    invoke-virtual {v2}, Landroid/hardware/usb/UsbInterface;->getInterfaceProtocol()I

    move-result v2

    if-nez v2, :cond_1

    const/4 p0, 0x1

    return p0

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    :goto_1
    return v1
.end method
