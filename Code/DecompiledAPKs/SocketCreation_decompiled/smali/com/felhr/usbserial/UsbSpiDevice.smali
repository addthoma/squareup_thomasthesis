.class public abstract Lcom/felhr/usbserial/UsbSpiDevice;
.super Ljava/lang/Object;
.source "UsbSpiDevice.java"

# interfaces
.implements Lcom/felhr/usbserial/UsbSpiInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/felhr/usbserial/UsbSpiDevice$ReadThread;,
        Lcom/felhr/usbserial/UsbSpiDevice$WriteThread;
    }
.end annotation


# static fields
.field protected static final USB_TIMEOUT:I = 0x1388


# instance fields
.field protected final connection:Landroid/hardware/usb/UsbDeviceConnection;

.field protected final device:Landroid/hardware/usb/UsbDevice;

.field private inEndpoint:Landroid/hardware/usb/UsbEndpoint;

.field private outEndpoint:Landroid/hardware/usb/UsbEndpoint;

.field protected readThread:Lcom/felhr/usbserial/UsbSpiDevice$ReadThread;

.field protected serialBuffer:Lcom/felhr/usbserial/SerialBuffer;

.field protected writeThread:Lcom/felhr/usbserial/UsbSpiDevice$WriteThread;


# direct methods
.method public constructor <init>(Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbDeviceConnection;)V
    .locals 0

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/felhr/usbserial/UsbSpiDevice;->device:Landroid/hardware/usb/UsbDevice;

    .line 29
    iput-object p2, p0, Lcom/felhr/usbserial/UsbSpiDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    .line 30
    new-instance p1, Lcom/felhr/usbserial/SerialBuffer;

    const/4 p2, 0x0

    invoke-direct {p1, p2}, Lcom/felhr/usbserial/SerialBuffer;-><init>(Z)V

    iput-object p1, p0, Lcom/felhr/usbserial/UsbSpiDevice;->serialBuffer:Lcom/felhr/usbserial/SerialBuffer;

    return-void
.end method

.method public static createUsbSerialDevice(Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbDeviceConnection;)Lcom/felhr/usbserial/UsbSpiDevice;
    .locals 1

    const/4 v0, -0x1

    .line 35
    invoke-static {p0, p1, v0}, Lcom/felhr/usbserial/UsbSpiDevice;->createUsbSerialDevice(Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbDeviceConnection;I)Lcom/felhr/usbserial/UsbSpiDevice;

    move-result-object p0

    return-object p0
.end method

.method public static createUsbSerialDevice(Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbDeviceConnection;I)Lcom/felhr/usbserial/UsbSpiDevice;
    .locals 2

    .line 40
    invoke-virtual {p0}, Landroid/hardware/usb/UsbDevice;->getVendorId()I

    move-result v0

    .line 41
    invoke-virtual {p0}, Landroid/hardware/usb/UsbDevice;->getProductId()I

    move-result v1

    .line 43
    invoke-static {v0, v1}, Lcom/felhr/deviceids/CP2130Ids;->isDeviceSupported(II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 44
    new-instance v0, Lcom/felhr/usbserial/CP2130SpiDevice;

    invoke-direct {v0, p0, p1, p2}, Lcom/felhr/usbserial/CP2130SpiDevice;-><init>(Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbDeviceConnection;I)V

    return-object v0

    :cond_0
    const/4 p0, 0x0

    return-object p0
.end method


# virtual methods
.method public abstract closeSPI()V
.end method

.method public abstract connectSPI()Z
.end method

.method public abstract getClockDivider()I
.end method

.method public abstract getSelectedSlave()I
.end method

.method protected killWorkingThread()V
    .locals 1

    .line 156
    iget-object v0, p0, Lcom/felhr/usbserial/UsbSpiDevice;->readThread:Lcom/felhr/usbserial/UsbSpiDevice$ReadThread;

    if-eqz v0, :cond_0

    .line 158
    invoke-virtual {v0}, Lcom/felhr/usbserial/UsbSpiDevice$ReadThread;->stopThread()V

    const/4 v0, 0x0

    .line 159
    iput-object v0, p0, Lcom/felhr/usbserial/UsbSpiDevice;->readThread:Lcom/felhr/usbserial/UsbSpiDevice$ReadThread;

    :cond_0
    return-void
.end method

.method protected killWriteThread()V
    .locals 1

    .line 175
    iget-object v0, p0, Lcom/felhr/usbserial/UsbSpiDevice;->writeThread:Lcom/felhr/usbserial/UsbSpiDevice$WriteThread;

    if-eqz v0, :cond_0

    .line 177
    invoke-virtual {v0}, Lcom/felhr/usbserial/UsbSpiDevice$WriteThread;->stopThread()V

    const/4 v0, 0x0

    .line 178
    iput-object v0, p0, Lcom/felhr/usbserial/UsbSpiDevice;->writeThread:Lcom/felhr/usbserial/UsbSpiDevice$WriteThread;

    :cond_0
    return-void
.end method

.method public abstract readMISO(I)V
.end method

.method protected restartWorkingThread()V
    .locals 1

    .line 168
    new-instance v0, Lcom/felhr/usbserial/UsbSpiDevice$ReadThread;

    invoke-direct {v0, p0}, Lcom/felhr/usbserial/UsbSpiDevice$ReadThread;-><init>(Lcom/felhr/usbserial/UsbSpiDevice;)V

    iput-object v0, p0, Lcom/felhr/usbserial/UsbSpiDevice;->readThread:Lcom/felhr/usbserial/UsbSpiDevice$ReadThread;

    .line 169
    iget-object v0, p0, Lcom/felhr/usbserial/UsbSpiDevice;->readThread:Lcom/felhr/usbserial/UsbSpiDevice$ReadThread;

    invoke-virtual {v0}, Lcom/felhr/usbserial/UsbSpiDevice$ReadThread;->start()V

    .line 170
    :goto_0
    iget-object v0, p0, Lcom/felhr/usbserial/UsbSpiDevice;->readThread:Lcom/felhr/usbserial/UsbSpiDevice$ReadThread;

    invoke-virtual {v0}, Lcom/felhr/usbserial/UsbSpiDevice$ReadThread;->isAlive()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected restartWriteThread()V
    .locals 1

    .line 184
    iget-object v0, p0, Lcom/felhr/usbserial/UsbSpiDevice;->writeThread:Lcom/felhr/usbserial/UsbSpiDevice$WriteThread;

    if-nez v0, :cond_0

    .line 186
    new-instance v0, Lcom/felhr/usbserial/UsbSpiDevice$WriteThread;

    invoke-direct {v0, p0}, Lcom/felhr/usbserial/UsbSpiDevice$WriteThread;-><init>(Lcom/felhr/usbserial/UsbSpiDevice;)V

    iput-object v0, p0, Lcom/felhr/usbserial/UsbSpiDevice;->writeThread:Lcom/felhr/usbserial/UsbSpiDevice$WriteThread;

    .line 187
    iget-object v0, p0, Lcom/felhr/usbserial/UsbSpiDevice;->writeThread:Lcom/felhr/usbserial/UsbSpiDevice$WriteThread;

    invoke-virtual {v0}, Lcom/felhr/usbserial/UsbSpiDevice$WriteThread;->start()V

    .line 188
    :goto_0
    iget-object v0, p0, Lcom/felhr/usbserial/UsbSpiDevice;->writeThread:Lcom/felhr/usbserial/UsbSpiDevice$WriteThread;

    invoke-virtual {v0}, Lcom/felhr/usbserial/UsbSpiDevice$WriteThread;->isAlive()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    return-void
.end method

.method public abstract selectSlave(I)V
.end method

.method public abstract setClock(I)V
.end method

.method public setMISOCallback(Lcom/felhr/usbserial/UsbSpiInterface$UsbMISOCallback;)V
    .locals 1

    .line 71
    iget-object v0, p0, Lcom/felhr/usbserial/UsbSpiDevice;->readThread:Lcom/felhr/usbserial/UsbSpiDevice$ReadThread;

    invoke-virtual {v0, p1}, Lcom/felhr/usbserial/UsbSpiDevice$ReadThread;->setCallback(Lcom/felhr/usbserial/UsbSpiInterface$UsbMISOCallback;)V

    return-void
.end method

.method protected setThreadsParams(Landroid/hardware/usb/UsbEndpoint;Landroid/hardware/usb/UsbEndpoint;)V
    .locals 1

    .line 144
    iget-object v0, p0, Lcom/felhr/usbserial/UsbSpiDevice;->writeThread:Lcom/felhr/usbserial/UsbSpiDevice$WriteThread;

    if-eqz v0, :cond_0

    .line 145
    invoke-virtual {v0, p2}, Lcom/felhr/usbserial/UsbSpiDevice$WriteThread;->setUsbEndpoint(Landroid/hardware/usb/UsbEndpoint;)V

    .line 147
    :cond_0
    iget-object p2, p0, Lcom/felhr/usbserial/UsbSpiDevice;->readThread:Lcom/felhr/usbserial/UsbSpiDevice$ReadThread;

    if-eqz p2, :cond_1

    .line 148
    invoke-virtual {p2, p1}, Lcom/felhr/usbserial/UsbSpiDevice$ReadThread;->setUsbEndpoint(Landroid/hardware/usb/UsbEndpoint;)V

    :cond_1
    return-void
.end method

.method public abstract writeMOSI([B)V
.end method

.method public abstract writeRead([BI)V
.end method
