.class public Lcom/felhr/usbserial/CP2102SerialDevice;
.super Lcom/felhr/usbserial/UsbSerialDevice;
.source "CP2102SerialDevice.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/felhr/usbserial/CP2102SerialDevice$FlowControlThread;
    }
.end annotation


# static fields
.field private static final CLASS_ID:Ljava/lang/String;

.field private static final CP210X_SET_BREAK:I = 0x5

.field private static final CP210x_BREAK_OFF:I = 0x0

.field private static final CP210x_BREAK_ON:I = 0x1

.field private static final CP210x_GET_COMM_STATUS:I = 0x10

.field private static final CP210x_GET_LINE_CTL:I = 0x4

.field private static final CP210x_GET_MDMSTS:I = 0x8

.field private static final CP210x_IFC_ENABLE:I = 0x0

.field private static final CP210x_LINE_CTL_DEFAULT:I = 0x800

.field private static final CP210x_MHS_ALL:I = 0x11

.field private static final CP210x_MHS_DEFAULT:I = 0x0

.field private static final CP210x_MHS_DTR:I = 0x1

.field private static final CP210x_MHS_DTR_OFF:I = 0x100

.field private static final CP210x_MHS_DTR_ON:I = 0x101

.field private static final CP210x_MHS_RTS:I = 0x10

.field private static final CP210x_MHS_RTS_OFF:I = 0x200

.field private static final CP210x_MHS_RTS_ON:I = 0x202

.field private static final CP210x_PURGE:I = 0x12

.field private static final CP210x_PURGE_ALL:I = 0xf

.field private static final CP210x_REQTYPE_DEVICE2HOST:I = 0xc1

.field private static final CP210x_REQTYPE_HOST2DEVICE:I = 0x41

.field private static final CP210x_SET_BAUDDIV:I = 0x1

.field private static final CP210x_SET_BAUDRATE:I = 0x1e

.field private static final CP210x_SET_CHARS:I = 0x19

.field private static final CP210x_SET_FLOW:I = 0x13

.field private static final CP210x_SET_LINE_CTL:I = 0x3

.field private static final CP210x_SET_MHS:I = 0x7

.field private static final CP210x_SET_XOFF:I = 0xa

.field private static final CP210x_SET_XON:I = 0x9

.field private static final CP210x_UART_DISABLE:I = 0x0

.field private static final CP210x_UART_ENABLE:I = 0x1

.field private static final CP210x_XOFF:I = 0x0

.field private static final CP210x_XON:I = 0x0

.field private static final DEFAULT_BAUDRATE:I = 0x2580


# instance fields
.field private breakCallback:Lcom/felhr/usbserial/UsbSerialInterface$UsbBreakCallback;

.field private ctsCallback:Lcom/felhr/usbserial/UsbSerialInterface$UsbCTSCallback;

.field private ctsState:Z

.field private dsrCallback:Lcom/felhr/usbserial/UsbSerialInterface$UsbDSRCallback;

.field private dsrState:Z

.field private dtrDsrEnabled:Z

.field private flowControlThread:Lcom/felhr/usbserial/CP2102SerialDevice$FlowControlThread;

.field private frameCallback:Lcom/felhr/usbserial/UsbSerialInterface$UsbFrameCallback;

.field private inEndpoint:Landroid/hardware/usb/UsbEndpoint;

.field private final mInterface:Landroid/hardware/usb/UsbInterface;

.field private outEndpoint:Landroid/hardware/usb/UsbEndpoint;

.field private overrunCallback:Lcom/felhr/usbserial/UsbSerialInterface$UsbOverrunCallback;

.field private parityCallback:Lcom/felhr/usbserial/UsbSerialInterface$UsbParityCallback;

.field private rtsCtsEnabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 15
    const-class v0, Lcom/felhr/usbserial/CP2102SerialDevice;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/felhr/usbserial/CP2102SerialDevice;->CLASS_ID:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbDeviceConnection;)V
    .locals 1

    const/4 v0, -0x1

    .line 89
    invoke-direct {p0, p1, p2, v0}, Lcom/felhr/usbserial/CP2102SerialDevice;-><init>(Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbDeviceConnection;I)V

    return-void
.end method

.method public constructor <init>(Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbDeviceConnection;I)V
    .locals 1

    .line 94
    invoke-direct {p0, p1, p2}, Lcom/felhr/usbserial/UsbSerialDevice;-><init>(Landroid/hardware/usb/UsbDevice;Landroid/hardware/usb/UsbDeviceConnection;)V

    const/4 p2, 0x0

    .line 95
    iput-boolean p2, p0, Lcom/felhr/usbserial/CP2102SerialDevice;->rtsCtsEnabled:Z

    .line 96
    iput-boolean p2, p0, Lcom/felhr/usbserial/CP2102SerialDevice;->dtrDsrEnabled:Z

    const/4 v0, 0x1

    .line 97
    iput-boolean v0, p0, Lcom/felhr/usbserial/CP2102SerialDevice;->ctsState:Z

    .line 98
    iput-boolean v0, p0, Lcom/felhr/usbserial/CP2102SerialDevice;->dsrState:Z

    if-ltz p3, :cond_0

    move p2, p3

    .line 99
    :cond_0
    invoke-virtual {p1, p2}, Landroid/hardware/usb/UsbDevice;->getInterface(I)Landroid/hardware/usb/UsbInterface;

    move-result-object p1

    iput-object p1, p0, Lcom/felhr/usbserial/CP2102SerialDevice;->mInterface:Landroid/hardware/usb/UsbInterface;

    return-void
.end method

.method static synthetic access$000(Lcom/felhr/usbserial/CP2102SerialDevice;)[B
    .locals 0

    .line 13
    invoke-direct {p0}, Lcom/felhr/usbserial/CP2102SerialDevice;->getCommStatus()[B

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$100(Lcom/felhr/usbserial/CP2102SerialDevice;)Z
    .locals 0

    .line 13
    iget-boolean p0, p0, Lcom/felhr/usbserial/CP2102SerialDevice;->rtsCtsEnabled:Z

    return p0
.end method

.method static synthetic access$1000(Lcom/felhr/usbserial/CP2102SerialDevice;)Lcom/felhr/usbserial/UsbSerialInterface$UsbOverrunCallback;
    .locals 0

    .line 13
    iget-object p0, p0, Lcom/felhr/usbserial/CP2102SerialDevice;->overrunCallback:Lcom/felhr/usbserial/UsbSerialInterface$UsbOverrunCallback;

    return-object p0
.end method

.method static synthetic access$1100(Lcom/felhr/usbserial/CP2102SerialDevice;)[B
    .locals 0

    .line 13
    invoke-direct {p0}, Lcom/felhr/usbserial/CP2102SerialDevice;->getModemState()[B

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$200(Lcom/felhr/usbserial/CP2102SerialDevice;)Z
    .locals 0

    .line 13
    iget-boolean p0, p0, Lcom/felhr/usbserial/CP2102SerialDevice;->ctsState:Z

    return p0
.end method

.method static synthetic access$202(Lcom/felhr/usbserial/CP2102SerialDevice;Z)Z
    .locals 0

    .line 13
    iput-boolean p1, p0, Lcom/felhr/usbserial/CP2102SerialDevice;->ctsState:Z

    return p1
.end method

.method static synthetic access$300(Lcom/felhr/usbserial/CP2102SerialDevice;)Lcom/felhr/usbserial/UsbSerialInterface$UsbCTSCallback;
    .locals 0

    .line 13
    iget-object p0, p0, Lcom/felhr/usbserial/CP2102SerialDevice;->ctsCallback:Lcom/felhr/usbserial/UsbSerialInterface$UsbCTSCallback;

    return-object p0
.end method

.method static synthetic access$400(Lcom/felhr/usbserial/CP2102SerialDevice;)Z
    .locals 0

    .line 13
    iget-boolean p0, p0, Lcom/felhr/usbserial/CP2102SerialDevice;->dtrDsrEnabled:Z

    return p0
.end method

.method static synthetic access$500(Lcom/felhr/usbserial/CP2102SerialDevice;)Z
    .locals 0

    .line 13
    iget-boolean p0, p0, Lcom/felhr/usbserial/CP2102SerialDevice;->dsrState:Z

    return p0
.end method

.method static synthetic access$502(Lcom/felhr/usbserial/CP2102SerialDevice;Z)Z
    .locals 0

    .line 13
    iput-boolean p1, p0, Lcom/felhr/usbserial/CP2102SerialDevice;->dsrState:Z

    return p1
.end method

.method static synthetic access$600(Lcom/felhr/usbserial/CP2102SerialDevice;)Lcom/felhr/usbserial/UsbSerialInterface$UsbDSRCallback;
    .locals 0

    .line 13
    iget-object p0, p0, Lcom/felhr/usbserial/CP2102SerialDevice;->dsrCallback:Lcom/felhr/usbserial/UsbSerialInterface$UsbDSRCallback;

    return-object p0
.end method

.method static synthetic access$700(Lcom/felhr/usbserial/CP2102SerialDevice;)Lcom/felhr/usbserial/UsbSerialInterface$UsbParityCallback;
    .locals 0

    .line 13
    iget-object p0, p0, Lcom/felhr/usbserial/CP2102SerialDevice;->parityCallback:Lcom/felhr/usbserial/UsbSerialInterface$UsbParityCallback;

    return-object p0
.end method

.method static synthetic access$800(Lcom/felhr/usbserial/CP2102SerialDevice;)Lcom/felhr/usbserial/UsbSerialInterface$UsbFrameCallback;
    .locals 0

    .line 13
    iget-object p0, p0, Lcom/felhr/usbserial/CP2102SerialDevice;->frameCallback:Lcom/felhr/usbserial/UsbSerialInterface$UsbFrameCallback;

    return-object p0
.end method

.method static synthetic access$900(Lcom/felhr/usbserial/CP2102SerialDevice;)Lcom/felhr/usbserial/UsbSerialInterface$UsbBreakCallback;
    .locals 0

    .line 13
    iget-object p0, p0, Lcom/felhr/usbserial/CP2102SerialDevice;->breakCallback:Lcom/felhr/usbserial/UsbSerialInterface$UsbBreakCallback;

    return-object p0
.end method

.method private createFlowControlThread()V
    .locals 2

    .line 550
    new-instance v0, Lcom/felhr/usbserial/CP2102SerialDevice$FlowControlThread;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/felhr/usbserial/CP2102SerialDevice$FlowControlThread;-><init>(Lcom/felhr/usbserial/CP2102SerialDevice;Lcom/felhr/usbserial/CP2102SerialDevice$1;)V

    iput-object v0, p0, Lcom/felhr/usbserial/CP2102SerialDevice;->flowControlThread:Lcom/felhr/usbserial/CP2102SerialDevice$FlowControlThread;

    return-void
.end method

.method private getCTL()S
    .locals 9

    const/4 v0, 0x2

    new-array v0, v0, [B

    .line 598
    iget-object v1, p0, Lcom/felhr/usbserial/CP2102SerialDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v2, p0, Lcom/felhr/usbserial/CP2102SerialDevice;->mInterface:Landroid/hardware/usb/UsbInterface;

    invoke-virtual {v2}, Landroid/hardware/usb/UsbInterface;->getId()I

    move-result v5

    array-length v7, v0

    const/16 v2, 0xc1

    const/4 v3, 0x4

    const/4 v4, 0x0

    const/4 v8, 0x0

    move-object v6, v0

    invoke-virtual/range {v1 .. v8}, Landroid/hardware/usb/UsbDeviceConnection;->controlTransfer(IIII[BII)I

    move-result v1

    .line 599
    sget-object v2, Lcom/felhr/usbserial/CP2102SerialDevice;->CLASS_ID:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Control Transfer Response: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x1

    .line 600
    aget-byte v1, v0, v1

    shl-int/lit8 v1, v1, 0x8

    const/4 v2, 0x0

    aget-byte v0, v0, v2

    and-int/lit16 v0, v0, 0xff

    or-int/2addr v0, v1

    int-to-short v0, v0

    return v0
.end method

.method private getCommStatus()[B
    .locals 9

    const/16 v0, 0x13

    new-array v0, v0, [B

    .line 590
    iget-object v1, p0, Lcom/felhr/usbserial/CP2102SerialDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v2, p0, Lcom/felhr/usbserial/CP2102SerialDevice;->mInterface:Landroid/hardware/usb/UsbInterface;

    invoke-virtual {v2}, Landroid/hardware/usb/UsbInterface;->getId()I

    move-result v5

    const/16 v2, 0xc1

    const/16 v3, 0x10

    const/4 v4, 0x0

    const/16 v7, 0x13

    const/4 v8, 0x0

    move-object v6, v0

    invoke-virtual/range {v1 .. v8}, Landroid/hardware/usb/UsbDeviceConnection;->controlTransfer(IIII[BII)I

    move-result v1

    .line 591
    sget-object v2, Lcom/felhr/usbserial/CP2102SerialDevice;->CLASS_ID:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Control Transfer Response (Comm status): "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-object v0
.end method

.method private getModemState()[B
    .locals 9

    const/4 v0, 0x1

    new-array v0, v0, [B

    .line 583
    iget-object v1, p0, Lcom/felhr/usbserial/CP2102SerialDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v2, p0, Lcom/felhr/usbserial/CP2102SerialDevice;->mInterface:Landroid/hardware/usb/UsbInterface;

    invoke-virtual {v2}, Landroid/hardware/usb/UsbInterface;->getId()I

    move-result v5

    const/16 v2, 0xc1

    const/16 v3, 0x8

    const/4 v4, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    move-object v6, v0

    invoke-virtual/range {v1 .. v8}, Landroid/hardware/usb/UsbDeviceConnection;->controlTransfer(IIII[BII)I

    return-object v0
.end method

.method private openCP2102()Z
    .locals 7

    .line 510
    iget-object v0, p0, Lcom/felhr/usbserial/CP2102SerialDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v1, p0, Lcom/felhr/usbserial/CP2102SerialDevice;->mInterface:Landroid/hardware/usb/UsbInterface;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/hardware/usb/UsbDeviceConnection;->claimInterface(Landroid/hardware/usb/UsbInterface;Z)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_5

    .line 512
    sget-object v0, Lcom/felhr/usbserial/CP2102SerialDevice;->CLASS_ID:Ljava/lang/String;

    const-string v3, "Interface succesfully claimed"

    invoke-static {v0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 520
    iget-object v0, p0, Lcom/felhr/usbserial/CP2102SerialDevice;->mInterface:Landroid/hardware/usb/UsbInterface;

    invoke-virtual {v0}, Landroid/hardware/usb/UsbInterface;->getEndpointCount()I

    move-result v0

    const/4 v3, 0x0

    :goto_0
    add-int/lit8 v4, v0, -0x1

    if-gt v3, v4, :cond_1

    .line 523
    iget-object v4, p0, Lcom/felhr/usbserial/CP2102SerialDevice;->mInterface:Landroid/hardware/usb/UsbInterface;

    invoke-virtual {v4, v3}, Landroid/hardware/usb/UsbInterface;->getEndpoint(I)Landroid/hardware/usb/UsbEndpoint;

    move-result-object v4

    .line 524
    invoke-virtual {v4}, Landroid/hardware/usb/UsbEndpoint;->getType()I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_0

    .line 525
    invoke-virtual {v4}, Landroid/hardware/usb/UsbEndpoint;->getDirection()I

    move-result v5

    const/16 v6, 0x80

    if-ne v5, v6, :cond_0

    .line 527
    iput-object v4, p0, Lcom/felhr/usbserial/CP2102SerialDevice;->inEndpoint:Landroid/hardware/usb/UsbEndpoint;

    goto :goto_1

    .line 530
    :cond_0
    iput-object v4, p0, Lcom/felhr/usbserial/CP2102SerialDevice;->outEndpoint:Landroid/hardware/usb/UsbEndpoint;

    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    .line 536
    invoke-direct {p0, v1, v2, v0}, Lcom/felhr/usbserial/CP2102SerialDevice;->setControlCommand(II[B)I

    move-result v3

    if-gez v3, :cond_2

    return v1

    :cond_2
    const/16 v3, 0x2580

    .line 538
    invoke-virtual {p0, v3}, Lcom/felhr/usbserial/CP2102SerialDevice;->setBaudRate(I)V

    const/4 v3, 0x3

    const/16 v4, 0x800

    .line 539
    invoke-direct {p0, v3, v4, v0}, Lcom/felhr/usbserial/CP2102SerialDevice;->setControlCommand(II[B)I

    move-result v3

    if-gez v3, :cond_3

    return v1

    .line 541
    :cond_3
    invoke-virtual {p0, v1}, Lcom/felhr/usbserial/CP2102SerialDevice;->setFlowControl(I)V

    const/4 v3, 0x7

    .line 542
    invoke-direct {p0, v3, v1, v0}, Lcom/felhr/usbserial/CP2102SerialDevice;->setControlCommand(II[B)I

    move-result v0

    if-gez v0, :cond_4

    return v1

    :cond_4
    return v2

    .line 515
    :cond_5
    sget-object v0, Lcom/felhr/usbserial/CP2102SerialDevice;->CLASS_ID:Ljava/lang/String;

    const-string v2, "Interface could not be claimed"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return v1
.end method

.method private setControlCommand(II[B)I
    .locals 9

    if-eqz p3, :cond_0

    .line 573
    array-length v0, p3

    move v7, v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    const/4 v7, 0x0

    .line 575
    :goto_0
    iget-object v1, p0, Lcom/felhr/usbserial/CP2102SerialDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    const/16 v2, 0x41

    iget-object v0, p0, Lcom/felhr/usbserial/CP2102SerialDevice;->mInterface:Landroid/hardware/usb/UsbInterface;

    invoke-virtual {v0}, Landroid/hardware/usb/UsbInterface;->getId()I

    move-result v5

    const/4 v8, 0x0

    move v3, p1

    move v4, p2

    move-object v6, p3

    invoke-virtual/range {v1 .. v8}, Landroid/hardware/usb/UsbDeviceConnection;->controlTransfer(IIII[BII)I

    move-result p1

    .line 576
    sget-object p2, Lcom/felhr/usbserial/CP2102SerialDevice;->CLASS_ID:Ljava/lang/String;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Control Transfer Response: "

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-static {p2, p3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return p1
.end method

.method private startFlowControlThread()V
    .locals 1

    .line 555
    iget-object v0, p0, Lcom/felhr/usbserial/CP2102SerialDevice;->flowControlThread:Lcom/felhr/usbserial/CP2102SerialDevice$FlowControlThread;

    invoke-virtual {v0}, Lcom/felhr/usbserial/CP2102SerialDevice$FlowControlThread;->isAlive()Z

    move-result v0

    if-nez v0, :cond_0

    .line 556
    iget-object v0, p0, Lcom/felhr/usbserial/CP2102SerialDevice;->flowControlThread:Lcom/felhr/usbserial/CP2102SerialDevice$FlowControlThread;

    invoke-virtual {v0}, Lcom/felhr/usbserial/CP2102SerialDevice$FlowControlThread;->start()V

    :cond_0
    return-void
.end method

.method private stopFlowControlThread()V
    .locals 1

    .line 561
    iget-object v0, p0, Lcom/felhr/usbserial/CP2102SerialDevice;->flowControlThread:Lcom/felhr/usbserial/CP2102SerialDevice$FlowControlThread;

    if-eqz v0, :cond_0

    .line 563
    invoke-virtual {v0}, Lcom/felhr/usbserial/CP2102SerialDevice$FlowControlThread;->stopThread()V

    const/4 v0, 0x0

    .line 564
    iput-object v0, p0, Lcom/felhr/usbserial/CP2102SerialDevice;->flowControlThread:Lcom/felhr/usbserial/CP2102SerialDevice$FlowControlThread;

    :cond_0
    return-void
.end method


# virtual methods
.method public close()V
    .locals 3

    const/4 v0, 0x0

    const/16 v1, 0x12

    const/16 v2, 0xf

    .line 137
    invoke-direct {p0, v1, v2, v0}, Lcom/felhr/usbserial/CP2102SerialDevice;->setControlCommand(II[B)I

    const/4 v1, 0x0

    .line 138
    invoke-direct {p0, v1, v1, v0}, Lcom/felhr/usbserial/CP2102SerialDevice;->setControlCommand(II[B)I

    .line 139
    invoke-virtual {p0}, Lcom/felhr/usbserial/CP2102SerialDevice;->killWorkingThread()V

    .line 140
    invoke-virtual {p0}, Lcom/felhr/usbserial/CP2102SerialDevice;->killWriteThread()V

    .line 141
    invoke-direct {p0}, Lcom/felhr/usbserial/CP2102SerialDevice;->stopFlowControlThread()V

    .line 142
    iget-object v0, p0, Lcom/felhr/usbserial/CP2102SerialDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v2, p0, Lcom/felhr/usbserial/CP2102SerialDevice;->mInterface:Landroid/hardware/usb/UsbInterface;

    invoke-virtual {v0, v2}, Landroid/hardware/usb/UsbDeviceConnection;->releaseInterface(Landroid/hardware/usb/UsbInterface;)Z

    .line 143
    iput-boolean v1, p0, Lcom/felhr/usbserial/CP2102SerialDevice;->isOpen:Z

    return-void
.end method

.method public getBreak(Lcom/felhr/usbserial/UsbSerialInterface$UsbBreakCallback;)V
    .locals 0

    .line 382
    iput-object p1, p0, Lcom/felhr/usbserial/CP2102SerialDevice;->breakCallback:Lcom/felhr/usbserial/UsbSerialInterface$UsbBreakCallback;

    return-void
.end method

.method public getCTS(Lcom/felhr/usbserial/UsbSerialInterface$UsbCTSCallback;)V
    .locals 0

    .line 370
    iput-object p1, p0, Lcom/felhr/usbserial/CP2102SerialDevice;->ctsCallback:Lcom/felhr/usbserial/UsbSerialInterface$UsbCTSCallback;

    return-void
.end method

.method public getDSR(Lcom/felhr/usbserial/UsbSerialInterface$UsbDSRCallback;)V
    .locals 0

    .line 376
    iput-object p1, p0, Lcom/felhr/usbserial/CP2102SerialDevice;->dsrCallback:Lcom/felhr/usbserial/UsbSerialInterface$UsbDSRCallback;

    return-void
.end method

.method public getFrame(Lcom/felhr/usbserial/UsbSerialInterface$UsbFrameCallback;)V
    .locals 0

    .line 388
    iput-object p1, p0, Lcom/felhr/usbserial/CP2102SerialDevice;->frameCallback:Lcom/felhr/usbserial/UsbSerialInterface$UsbFrameCallback;

    return-void
.end method

.method public getOverrun(Lcom/felhr/usbserial/UsbSerialInterface$UsbOverrunCallback;)V
    .locals 0

    .line 394
    iput-object p1, p0, Lcom/felhr/usbserial/CP2102SerialDevice;->overrunCallback:Lcom/felhr/usbserial/UsbSerialInterface$UsbOverrunCallback;

    return-void
.end method

.method public getParity(Lcom/felhr/usbserial/UsbSerialInterface$UsbParityCallback;)V
    .locals 0

    .line 400
    iput-object p1, p0, Lcom/felhr/usbserial/CP2102SerialDevice;->parityCallback:Lcom/felhr/usbserial/UsbSerialInterface$UsbParityCallback;

    .line 401
    invoke-direct {p0}, Lcom/felhr/usbserial/CP2102SerialDevice;->startFlowControlThread()V

    return-void
.end method

.method public open()Z
    .locals 3

    .line 105
    invoke-direct {p0}, Lcom/felhr/usbserial/CP2102SerialDevice;->openCP2102()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 110
    new-instance v0, Lcom/felhr/utils/SafeUsbRequest;

    invoke-direct {v0}, Lcom/felhr/utils/SafeUsbRequest;-><init>()V

    .line 111
    iget-object v1, p0, Lcom/felhr/usbserial/CP2102SerialDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v2, p0, Lcom/felhr/usbserial/CP2102SerialDevice;->inEndpoint:Landroid/hardware/usb/UsbEndpoint;

    invoke-virtual {v0, v1, v2}, Landroid/hardware/usb/UsbRequest;->initialize(Landroid/hardware/usb/UsbDeviceConnection;Landroid/hardware/usb/UsbEndpoint;)Z

    .line 114
    invoke-virtual {p0}, Lcom/felhr/usbserial/CP2102SerialDevice;->restartWorkingThread()V

    .line 115
    invoke-virtual {p0}, Lcom/felhr/usbserial/CP2102SerialDevice;->restartWriteThread()V

    .line 118
    invoke-direct {p0}, Lcom/felhr/usbserial/CP2102SerialDevice;->createFlowControlThread()V

    .line 121
    iget-object v1, p0, Lcom/felhr/usbserial/CP2102SerialDevice;->outEndpoint:Landroid/hardware/usb/UsbEndpoint;

    invoke-virtual {p0, v0, v1}, Lcom/felhr/usbserial/CP2102SerialDevice;->setThreadsParams(Landroid/hardware/usb/UsbRequest;Landroid/hardware/usb/UsbEndpoint;)V

    const/4 v0, 0x1

    .line 123
    iput-boolean v0, p0, Lcom/felhr/usbserial/CP2102SerialDevice;->asyncMode:Z

    .line 124
    iput-boolean v0, p0, Lcom/felhr/usbserial/CP2102SerialDevice;->isOpen:Z

    return v0

    :cond_0
    const/4 v0, 0x0

    .line 129
    iput-boolean v0, p0, Lcom/felhr/usbserial/CP2102SerialDevice;->isOpen:Z

    return v0
.end method

.method public setBaudRate(I)V
    .locals 4

    const/4 v0, 0x4

    new-array v0, v0, [B

    and-int/lit16 v1, p1, 0xff

    int-to-byte v1, v1

    const/4 v2, 0x0

    aput-byte v1, v0, v2

    shr-int/lit8 v1, p1, 0x8

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    const/4 v3, 0x1

    aput-byte v1, v0, v3

    shr-int/lit8 v1, p1, 0x10

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    const/4 v3, 0x2

    aput-byte v1, v0, v3

    shr-int/lit8 p1, p1, 0x18

    and-int/lit16 p1, p1, 0xff

    int-to-byte p1, p1

    const/4 v1, 0x3

    aput-byte p1, v0, v1

    const/16 p1, 0x1e

    .line 189
    invoke-direct {p0, p1, v2, v0}, Lcom/felhr/usbserial/CP2102SerialDevice;->setControlCommand(II[B)I

    return-void
.end method

.method public setBreak(Z)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x5

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    .line 337
    invoke-direct {p0, v1, p1, v0}, Lcom/felhr/usbserial/CP2102SerialDevice;->setControlCommand(II[B)I

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    .line 339
    invoke-direct {p0, v1, p1, v0}, Lcom/felhr/usbserial/CP2102SerialDevice;->setControlCommand(II[B)I

    :goto_0
    return-void
.end method

.method public setDTR(Z)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x7

    if-eqz p1, :cond_0

    const/16 p1, 0x101

    .line 360
    invoke-direct {p0, v1, p1, v0}, Lcom/felhr/usbserial/CP2102SerialDevice;->setControlCommand(II[B)I

    goto :goto_0

    :cond_0
    const/16 p1, 0x100

    .line 363
    invoke-direct {p0, v1, p1, v0}, Lcom/felhr/usbserial/CP2102SerialDevice;->setControlCommand(II[B)I

    :goto_0
    return-void
.end method

.method public setDataBits(I)V
    .locals 2

    .line 195
    invoke-direct {p0}, Lcom/felhr/usbserial/CP2102SerialDevice;->getCTL()S

    move-result v0

    and-int/lit16 v0, v0, -0xf01

    int-to-short v0, v0

    const/4 v1, 0x5

    if-eq p1, v1, :cond_3

    const/4 v1, 0x6

    if-eq p1, v1, :cond_2

    const/4 v1, 0x7

    if-eq p1, v1, :cond_1

    const/16 v1, 0x8

    if-eq p1, v1, :cond_0

    return-void

    :cond_0
    or-int/lit16 p1, v0, 0x800

    goto :goto_0

    :cond_1
    or-int/lit16 p1, v0, 0x700

    goto :goto_0

    :cond_2
    or-int/lit16 p1, v0, 0x600

    goto :goto_0

    :cond_3
    or-int/lit16 p1, v0, 0x500

    :goto_0
    int-to-short p1, p1

    const/4 v0, 0x3

    const/4 v1, 0x0

    .line 214
    invoke-direct {p0, v0, p1, v1}, Lcom/felhr/usbserial/CP2102SerialDevice;->setControlCommand(II[B)I

    return-void
.end method

.method public setFlowControl(I)V
    .locals 8

    const/16 v0, 0x13

    const/16 v1, 0x10

    const/4 v2, 0x0

    if-eqz p1, :cond_5

    const/4 v3, 0x4

    const/4 v4, 0x0

    const/4 v5, 0x7

    const/4 v6, 0x1

    if-eq p1, v6, :cond_3

    const/4 v7, 0x2

    if-eq p1, v7, :cond_1

    const/4 v3, 0x3

    if-eq p1, v3, :cond_0

    return-void

    :cond_0
    new-array p1, v1, [B

    .line 314
    fill-array-data p1, :array_0

    const/4 v1, 0x6

    new-array v1, v1, [B

    .line 321
    fill-array-data v1, :array_1

    const/16 v3, 0x19

    .line 325
    invoke-direct {p0, v3, v2, v1}, Lcom/felhr/usbserial/CP2102SerialDevice;->setControlCommand(II[B)I

    .line 326
    invoke-direct {p0, v0, v2, p1}, Lcom/felhr/usbserial/CP2102SerialDevice;->setControlCommand(II[B)I

    goto :goto_0

    :cond_1
    new-array p1, v1, [B

    .line 299
    fill-array-data p1, :array_2

    .line 305
    iput-boolean v6, p0, Lcom/felhr/usbserial/CP2102SerialDevice;->dtrDsrEnabled:Z

    .line 306
    iput-boolean v2, p0, Lcom/felhr/usbserial/CP2102SerialDevice;->rtsCtsEnabled:Z

    .line 307
    invoke-direct {p0, v0, v2, p1}, Lcom/felhr/usbserial/CP2102SerialDevice;->setControlCommand(II[B)I

    const/16 p1, 0x101

    .line 308
    invoke-direct {p0, v5, p1, v4}, Lcom/felhr/usbserial/CP2102SerialDevice;->setControlCommand(II[B)I

    .line 309
    invoke-direct {p0}, Lcom/felhr/usbserial/CP2102SerialDevice;->getCommStatus()[B

    move-result-object p1

    .line 310
    aget-byte p1, p1, v3

    and-int/2addr p1, v7

    if-nez p1, :cond_2

    const/4 v2, 0x1

    :cond_2
    iput-boolean v2, p0, Lcom/felhr/usbserial/CP2102SerialDevice;->dsrState:Z

    .line 311
    invoke-direct {p0}, Lcom/felhr/usbserial/CP2102SerialDevice;->startFlowControlThread()V

    goto :goto_0

    :cond_3
    new-array p1, v1, [B

    .line 284
    fill-array-data p1, :array_3

    .line 290
    iput-boolean v6, p0, Lcom/felhr/usbserial/CP2102SerialDevice;->rtsCtsEnabled:Z

    .line 291
    iput-boolean v2, p0, Lcom/felhr/usbserial/CP2102SerialDevice;->dtrDsrEnabled:Z

    .line 292
    invoke-direct {p0, v0, v2, p1}, Lcom/felhr/usbserial/CP2102SerialDevice;->setControlCommand(II[B)I

    const/16 p1, 0x202

    .line 293
    invoke-direct {p0, v5, p1, v4}, Lcom/felhr/usbserial/CP2102SerialDevice;->setControlCommand(II[B)I

    .line 294
    invoke-direct {p0}, Lcom/felhr/usbserial/CP2102SerialDevice;->getCommStatus()[B

    move-result-object p1

    .line 295
    aget-byte p1, p1, v3

    and-int/2addr p1, v6

    if-nez p1, :cond_4

    const/4 v2, 0x1

    :cond_4
    iput-boolean v2, p0, Lcom/felhr/usbserial/CP2102SerialDevice;->ctsState:Z

    .line 296
    invoke-direct {p0}, Lcom/felhr/usbserial/CP2102SerialDevice;->startFlowControlThread()V

    goto :goto_0

    :cond_5
    new-array p1, v1, [B

    .line 273
    fill-array-data p1, :array_4

    .line 279
    iput-boolean v2, p0, Lcom/felhr/usbserial/CP2102SerialDevice;->rtsCtsEnabled:Z

    .line 280
    iput-boolean v2, p0, Lcom/felhr/usbserial/CP2102SerialDevice;->dtrDsrEnabled:Z

    .line 281
    invoke-direct {p0, v0, v2, p1}, Lcom/felhr/usbserial/CP2102SerialDevice;->setControlCommand(II[B)I

    :goto_0
    return-void

    nop

    :array_0
    .array-data 1
        0x1t
        0x0t
        0x0t
        0x0t
        0x43t
        0x0t
        0x0t
        0x0t
        0x0t
        -0x80t
        0x0t
        0x0t
        0x0t
        0x20t
        0x0t
        0x0t
    .end array-data

    :array_1
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x11t
        0x13t
    .end array-data

    nop

    :array_2
    .array-data 1
        0x11t
        0x0t
        0x0t
        0x0t
        0x40t
        0x0t
        0x0t
        0x0t
        0x0t
        -0x80t
        0x0t
        0x0t
        0x0t
        0x20t
        0x0t
        0x0t
    .end array-data

    :array_3
    .array-data 1
        0x9t
        0x0t
        0x0t
        0x0t
        0x40t
        0x0t
        0x0t
        0x0t
        0x0t
        -0x80t
        0x0t
        0x0t
        0x0t
        0x20t
        0x0t
        0x0t
    .end array-data

    :array_4
    .array-data 1
        0x1t
        0x0t
        0x0t
        0x0t
        0x40t
        0x0t
        0x0t
        0x0t
        0x0t
        -0x80t
        0x0t
        0x0t
        0x0t
        0x20t
        0x0t
        0x0t
    .end array-data
.end method

.method public setParity(I)V
    .locals 3

    .line 242
    invoke-direct {p0}, Lcom/felhr/usbserial/CP2102SerialDevice;->getCTL()S

    move-result v0

    and-int/lit16 v0, v0, -0xf1

    int-to-short v0, v0

    const/4 v1, 0x3

    if-eqz p1, :cond_4

    const/4 v2, 0x1

    if-eq p1, v2, :cond_3

    const/4 v2, 0x2

    if-eq p1, v2, :cond_2

    if-eq p1, v1, :cond_1

    const/4 v2, 0x4

    if-eq p1, v2, :cond_0

    return-void

    :cond_0
    or-int/lit8 p1, v0, 0x40

    goto :goto_0

    :cond_1
    or-int/lit8 p1, v0, 0x30

    goto :goto_0

    :cond_2
    or-int/lit8 p1, v0, 0x20

    goto :goto_0

    :cond_3
    or-int/lit8 p1, v0, 0x10

    goto :goto_0

    :cond_4
    or-int/lit8 p1, v0, 0x0

    :goto_0
    int-to-short p1, p1

    const/4 v0, 0x0

    .line 264
    invoke-direct {p0, v1, p1, v0}, Lcom/felhr/usbserial/CP2102SerialDevice;->setControlCommand(II[B)I

    return-void
.end method

.method public setRTS(Z)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x7

    if-eqz p1, :cond_0

    const/16 p1, 0x202

    .line 348
    invoke-direct {p0, v1, p1, v0}, Lcom/felhr/usbserial/CP2102SerialDevice;->setControlCommand(II[B)I

    goto :goto_0

    :cond_0
    const/16 p1, 0x200

    .line 351
    invoke-direct {p0, v1, p1, v0}, Lcom/felhr/usbserial/CP2102SerialDevice;->setControlCommand(II[B)I

    :goto_0
    return-void
.end method

.method public setStopBits(I)V
    .locals 4

    .line 220
    invoke-direct {p0}, Lcom/felhr/usbserial/CP2102SerialDevice;->getCTL()S

    move-result v0

    and-int/lit8 v0, v0, -0x4

    int-to-short v0, v0

    const/4 v1, 0x3

    const/4 v2, 0x1

    if-eq p1, v2, :cond_2

    const/4 v3, 0x2

    if-eq p1, v3, :cond_1

    if-eq p1, v1, :cond_0

    return-void

    :cond_0
    or-int/lit8 p1, v0, 0x1

    goto :goto_0

    :cond_1
    or-int/lit8 p1, v0, 0x2

    goto :goto_0

    :cond_2
    or-int/lit8 p1, v0, 0x0

    :goto_0
    int-to-short p1, p1

    const/4 v0, 0x0

    .line 236
    invoke-direct {p0, v1, p1, v0}, Lcom/felhr/usbserial/CP2102SerialDevice;->setControlCommand(II[B)I

    return-void
.end method

.method public syncClose()V
    .locals 3

    const/4 v0, 0x0

    const/16 v1, 0x12

    const/16 v2, 0xf

    .line 173
    invoke-direct {p0, v1, v2, v0}, Lcom/felhr/usbserial/CP2102SerialDevice;->setControlCommand(II[B)I

    const/4 v1, 0x0

    .line 174
    invoke-direct {p0, v1, v1, v0}, Lcom/felhr/usbserial/CP2102SerialDevice;->setControlCommand(II[B)I

    .line 175
    invoke-direct {p0}, Lcom/felhr/usbserial/CP2102SerialDevice;->stopFlowControlThread()V

    .line 176
    iget-object v0, p0, Lcom/felhr/usbserial/CP2102SerialDevice;->connection:Landroid/hardware/usb/UsbDeviceConnection;

    iget-object v2, p0, Lcom/felhr/usbserial/CP2102SerialDevice;->mInterface:Landroid/hardware/usb/UsbInterface;

    invoke-virtual {v0, v2}, Landroid/hardware/usb/UsbDeviceConnection;->releaseInterface(Landroid/hardware/usb/UsbInterface;)Z

    .line 177
    iput-boolean v1, p0, Lcom/felhr/usbserial/CP2102SerialDevice;->isOpen:Z

    return-void
.end method

.method public syncOpen()Z
    .locals 3

    .line 149
    invoke-direct {p0}, Lcom/felhr/usbserial/CP2102SerialDevice;->openCP2102()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 153
    invoke-direct {p0}, Lcom/felhr/usbserial/CP2102SerialDevice;->createFlowControlThread()V

    .line 154
    iget-object v0, p0, Lcom/felhr/usbserial/CP2102SerialDevice;->inEndpoint:Landroid/hardware/usb/UsbEndpoint;

    iget-object v2, p0, Lcom/felhr/usbserial/CP2102SerialDevice;->outEndpoint:Landroid/hardware/usb/UsbEndpoint;

    invoke-virtual {p0, v0, v2}, Lcom/felhr/usbserial/CP2102SerialDevice;->setSyncParams(Landroid/hardware/usb/UsbEndpoint;Landroid/hardware/usb/UsbEndpoint;)V

    .line 155
    iput-boolean v1, p0, Lcom/felhr/usbserial/CP2102SerialDevice;->asyncMode:Z

    const/4 v0, 0x1

    .line 156
    iput-boolean v0, p0, Lcom/felhr/usbserial/CP2102SerialDevice;->isOpen:Z

    .line 159
    new-instance v1, Lcom/felhr/usbserial/SerialInputStream;

    invoke-direct {v1, p0}, Lcom/felhr/usbserial/SerialInputStream;-><init>(Lcom/felhr/usbserial/UsbSerialInterface;)V

    iput-object v1, p0, Lcom/felhr/usbserial/CP2102SerialDevice;->inputStream:Lcom/felhr/usbserial/SerialInputStream;

    .line 160
    new-instance v1, Lcom/felhr/usbserial/SerialOutputStream;

    invoke-direct {v1, p0}, Lcom/felhr/usbserial/SerialOutputStream;-><init>(Lcom/felhr/usbserial/UsbSerialInterface;)V

    iput-object v1, p0, Lcom/felhr/usbserial/CP2102SerialDevice;->outputStream:Lcom/felhr/usbserial/SerialOutputStream;

    return v0

    .line 165
    :cond_0
    iput-boolean v1, p0, Lcom/felhr/usbserial/CP2102SerialDevice;->isOpen:Z

    return v1
.end method
