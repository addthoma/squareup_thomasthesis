.class Lcom/felhr/usbserial/SerialPortBuilder$InitSerialPortThread;
.super Ljava/lang/Thread;
.source "SerialPortBuilder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/felhr/usbserial/SerialPortBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InitSerialPortThread"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/felhr/usbserial/SerialPortBuilder;

.field private final usbSerialDevices:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/felhr/usbserial/UsbSerialDevice;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/felhr/usbserial/SerialPortBuilder;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/felhr/usbserial/UsbSerialDevice;",
            ">;)V"
        }
    .end annotation

    .line 240
    iput-object p1, p0, Lcom/felhr/usbserial/SerialPortBuilder$InitSerialPortThread;->this$0:Lcom/felhr/usbserial/SerialPortBuilder;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 241
    iput-object p2, p0, Lcom/felhr/usbserial/SerialPortBuilder$InitSerialPortThread;->usbSerialDevices:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .line 247
    iget-object v0, p0, Lcom/felhr/usbserial/SerialPortBuilder$InitSerialPortThread;->usbSerialDevices:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x1

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/felhr/usbserial/UsbSerialDevice;

    .line 248
    iget-boolean v3, v2, Lcom/felhr/usbserial/UsbSerialDevice;->isOpen:Z

    if-nez v3, :cond_0

    .line 249
    invoke-virtual {v2}, Lcom/felhr/usbserial/UsbSerialDevice;->syncOpen()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 250
    iget-object v3, p0, Lcom/felhr/usbserial/SerialPortBuilder$InitSerialPortThread;->this$0:Lcom/felhr/usbserial/SerialPortBuilder;

    invoke-static {v3}, Lcom/felhr/usbserial/SerialPortBuilder;->access$900(Lcom/felhr/usbserial/SerialPortBuilder;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/felhr/usbserial/UsbSerialDevice;->setBaudRate(I)V

    .line 251
    iget-object v3, p0, Lcom/felhr/usbserial/SerialPortBuilder$InitSerialPortThread;->this$0:Lcom/felhr/usbserial/SerialPortBuilder;

    invoke-static {v3}, Lcom/felhr/usbserial/SerialPortBuilder;->access$1000(Lcom/felhr/usbserial/SerialPortBuilder;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/felhr/usbserial/UsbSerialDevice;->setDataBits(I)V

    .line 252
    iget-object v3, p0, Lcom/felhr/usbserial/SerialPortBuilder$InitSerialPortThread;->this$0:Lcom/felhr/usbserial/SerialPortBuilder;

    invoke-static {v3}, Lcom/felhr/usbserial/SerialPortBuilder;->access$1100(Lcom/felhr/usbserial/SerialPortBuilder;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/felhr/usbserial/UsbSerialDevice;->setStopBits(I)V

    .line 253
    iget-object v3, p0, Lcom/felhr/usbserial/SerialPortBuilder$InitSerialPortThread;->this$0:Lcom/felhr/usbserial/SerialPortBuilder;

    invoke-static {v3}, Lcom/felhr/usbserial/SerialPortBuilder;->access$1200(Lcom/felhr/usbserial/SerialPortBuilder;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/felhr/usbserial/UsbSerialDevice;->setParity(I)V

    .line 254
    iget-object v3, p0, Lcom/felhr/usbserial/SerialPortBuilder$InitSerialPortThread;->this$0:Lcom/felhr/usbserial/SerialPortBuilder;

    invoke-static {v3}, Lcom/felhr/usbserial/SerialPortBuilder;->access$1300(Lcom/felhr/usbserial/SerialPortBuilder;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/felhr/usbserial/UsbSerialDevice;->setFlowControl(I)V

    .line 255
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "COM "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/felhr/usbserial/UsbSerialDevice;->setPortName(Ljava/lang/String;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 260
    :cond_1
    iget-object v0, p0, Lcom/felhr/usbserial/SerialPortBuilder$InitSerialPortThread;->this$0:Lcom/felhr/usbserial/SerialPortBuilder;

    invoke-static {v0}, Lcom/felhr/usbserial/SerialPortBuilder;->access$800(Lcom/felhr/usbserial/SerialPortBuilder;)Lcom/felhr/usbserial/SerialPortCallback;

    move-result-object v0

    iget-object v1, p0, Lcom/felhr/usbserial/SerialPortBuilder$InitSerialPortThread;->this$0:Lcom/felhr/usbserial/SerialPortBuilder;

    invoke-static {v1}, Lcom/felhr/usbserial/SerialPortBuilder;->access$700(Lcom/felhr/usbserial/SerialPortBuilder;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/felhr/usbserial/SerialPortCallback;->onSerialPortsDetected(Ljava/util/List;)V

    return-void
.end method
