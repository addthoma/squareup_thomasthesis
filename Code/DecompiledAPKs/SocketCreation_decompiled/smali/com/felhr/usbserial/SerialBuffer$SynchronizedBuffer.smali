.class Lcom/felhr/usbserial/SerialBuffer$SynchronizedBuffer;
.super Ljava/lang/Object;
.source "SerialBuffer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/felhr/usbserial/SerialBuffer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SynchronizedBuffer"
.end annotation


# instance fields
.field private final buffer:Lokio/Buffer;

.field final synthetic this$0:Lcom/felhr/usbserial/SerialBuffer;


# direct methods
.method constructor <init>(Lcom/felhr/usbserial/SerialBuffer;)V
    .locals 0

    .line 96
    iput-object p1, p0, Lcom/felhr/usbserial/SerialBuffer$SynchronizedBuffer;->this$0:Lcom/felhr/usbserial/SerialBuffer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    new-instance p1, Lokio/Buffer;

    invoke-direct {p1}, Lokio/Buffer;-><init>()V

    iput-object p1, p0, Lcom/felhr/usbserial/SerialBuffer$SynchronizedBuffer;->buffer:Lokio/Buffer;

    return-void
.end method


# virtual methods
.method declared-synchronized get()[B
    .locals 5

    monitor-enter p0

    .line 113
    :try_start_0
    iget-object v0, p0, Lcom/felhr/usbserial/SerialBuffer$SynchronizedBuffer;->buffer:Lokio/Buffer;

    invoke-virtual {v0}, Lokio/Buffer;->size()J

    move-result-wide v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-nez v4, :cond_0

    .line 117
    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    .line 120
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    .line 121
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 125
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/felhr/usbserial/SerialBuffer$SynchronizedBuffer;->buffer:Lokio/Buffer;

    invoke-virtual {v0}, Lokio/Buffer;->size()J

    move-result-wide v0

    const-wide/16 v2, 0x4000

    cmp-long v4, v0, v2

    if-gtz v4, :cond_1

    .line 126
    iget-object v0, p0, Lcom/felhr/usbserial/SerialBuffer$SynchronizedBuffer;->buffer:Lokio/Buffer;

    invoke-virtual {v0}, Lokio/Buffer;->readByteArray()[B

    move-result-object v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 129
    :cond_1
    :try_start_3
    iget-object v0, p0, Lcom/felhr/usbserial/SerialBuffer$SynchronizedBuffer;->buffer:Lokio/Buffer;

    invoke-virtual {v0, v2, v3}, Lokio/Buffer;->readByteArray(J)[B

    move-result-object v0
    :try_end_3
    .catch Ljava/io/EOFException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 136
    :goto_1
    :try_start_4
    iget-object v1, p0, Lcom/felhr/usbserial/SerialBuffer$SynchronizedBuffer;->this$0:Lcom/felhr/usbserial/SerialBuffer;

    invoke-static {v1}, Lcom/felhr/usbserial/SerialBuffer;->access$000(Lcom/felhr/usbserial/SerialBuffer;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    .line 137
    invoke-static {v0, v1}, Lcom/felhr/usbserial/UsbSerialDebugger;->printLogGet([BZ)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 139
    :cond_2
    monitor-exit p0

    return-object v0

    :catch_1
    move-exception v0

    .line 131
    :try_start_5
    invoke-virtual {v0}, Ljava/io/EOFException;->printStackTrace()V

    const/4 v0, 0x0

    new-array v0, v0, [B
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 132
    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized put([B)V
    .locals 1

    monitor-enter p0

    if-eqz p1, :cond_2

    .line 102
    :try_start_0
    array-length v0, p1

    if-nez v0, :cond_0

    goto :goto_0

    .line 104
    :cond_0
    iget-object v0, p0, Lcom/felhr/usbserial/SerialBuffer$SynchronizedBuffer;->this$0:Lcom/felhr/usbserial/SerialBuffer;

    invoke-static {v0}, Lcom/felhr/usbserial/SerialBuffer;->access$000(Lcom/felhr/usbserial/SerialBuffer;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 105
    invoke-static {p1, v0}, Lcom/felhr/usbserial/UsbSerialDebugger;->printLogPut([BZ)V

    .line 107
    :cond_1
    iget-object v0, p0, Lcom/felhr/usbserial/SerialBuffer$SynchronizedBuffer;->buffer:Lokio/Buffer;

    invoke-virtual {v0, p1}, Lokio/Buffer;->write([B)Lokio/Buffer;

    .line 108
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 109
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1

    .line 102
    :cond_2
    :goto_0
    monitor-exit p0

    return-void
.end method
