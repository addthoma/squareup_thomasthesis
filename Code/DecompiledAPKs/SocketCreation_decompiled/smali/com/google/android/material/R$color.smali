.class public final Lcom/google/android/material/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/material/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final abc_background_cache_hint_selector_material_dark:I = 0x7f060000

.field public static final abc_background_cache_hint_selector_material_light:I = 0x7f060001

.field public static final abc_btn_colored_borderless_text_material:I = 0x7f060002

.field public static final abc_btn_colored_text_material:I = 0x7f060003

.field public static final abc_color_highlight_material:I = 0x7f060004

.field public static final abc_hint_foreground_material_dark:I = 0x7f060005

.field public static final abc_hint_foreground_material_light:I = 0x7f060006

.field public static final abc_input_method_navigation_guard:I = 0x7f060007

.field public static final abc_primary_text_disable_only_material_dark:I = 0x7f060008

.field public static final abc_primary_text_disable_only_material_light:I = 0x7f060009

.field public static final abc_primary_text_material_dark:I = 0x7f06000a

.field public static final abc_primary_text_material_light:I = 0x7f06000b

.field public static final abc_search_url_text:I = 0x7f06000c

.field public static final abc_search_url_text_normal:I = 0x7f06000d

.field public static final abc_search_url_text_pressed:I = 0x7f06000e

.field public static final abc_search_url_text_selected:I = 0x7f06000f

.field public static final abc_secondary_text_material_dark:I = 0x7f060010

.field public static final abc_secondary_text_material_light:I = 0x7f060011

.field public static final abc_tint_btn_checkable:I = 0x7f060012

.field public static final abc_tint_default:I = 0x7f060013

.field public static final abc_tint_edittext:I = 0x7f060014

.field public static final abc_tint_seek_thumb:I = 0x7f060015

.field public static final abc_tint_spinner:I = 0x7f060016

.field public static final abc_tint_switch_track:I = 0x7f060017

.field public static final accent_material_dark:I = 0x7f060018

.field public static final accent_material_light:I = 0x7f060019

.field public static final background_floating_material_dark:I = 0x7f060022

.field public static final background_floating_material_light:I = 0x7f060023

.field public static final background_material_dark:I = 0x7f060024

.field public static final background_material_light:I = 0x7f060025

.field public static final bright_foreground_disabled_material_dark:I = 0x7f06002d

.field public static final bright_foreground_disabled_material_light:I = 0x7f06002e

.field public static final bright_foreground_inverse_material_dark:I = 0x7f06002f

.field public static final bright_foreground_inverse_material_light:I = 0x7f060030

.field public static final bright_foreground_material_dark:I = 0x7f060031

.field public static final bright_foreground_material_light:I = 0x7f060032

.field public static final button_material_dark:I = 0x7f060034

.field public static final button_material_light:I = 0x7f060035

.field public static final cardview_dark_background:I = 0x7f060057

.field public static final cardview_light_background:I = 0x7f060058

.field public static final cardview_shadow_end_color:I = 0x7f060059

.field public static final cardview_shadow_start_color:I = 0x7f06005a

.field public static final checkbox_themeable_attribute_color:I = 0x7f060062

.field public static final design_bottom_navigation_shadow_color:I = 0x7f060087

.field public static final design_box_stroke_color:I = 0x7f060088

.field public static final design_dark_default_color_background:I = 0x7f060089

.field public static final design_dark_default_color_error:I = 0x7f06008a

.field public static final design_dark_default_color_on_background:I = 0x7f06008b

.field public static final design_dark_default_color_on_error:I = 0x7f06008c

.field public static final design_dark_default_color_on_primary:I = 0x7f06008d

.field public static final design_dark_default_color_on_secondary:I = 0x7f06008e

.field public static final design_dark_default_color_on_surface:I = 0x7f06008f

.field public static final design_dark_default_color_primary:I = 0x7f060090

.field public static final design_dark_default_color_primary_dark:I = 0x7f060091

.field public static final design_dark_default_color_primary_variant:I = 0x7f060092

.field public static final design_dark_default_color_secondary:I = 0x7f060093

.field public static final design_dark_default_color_secondary_variant:I = 0x7f060094

.field public static final design_dark_default_color_surface:I = 0x7f060095

.field public static final design_default_color_background:I = 0x7f060096

.field public static final design_default_color_error:I = 0x7f060097

.field public static final design_default_color_on_background:I = 0x7f060098

.field public static final design_default_color_on_error:I = 0x7f060099

.field public static final design_default_color_on_primary:I = 0x7f06009a

.field public static final design_default_color_on_secondary:I = 0x7f06009b

.field public static final design_default_color_on_surface:I = 0x7f06009c

.field public static final design_default_color_primary:I = 0x7f06009d

.field public static final design_default_color_primary_dark:I = 0x7f06009e

.field public static final design_default_color_primary_variant:I = 0x7f06009f

.field public static final design_default_color_secondary:I = 0x7f0600a0

.field public static final design_default_color_secondary_variant:I = 0x7f0600a1

.field public static final design_default_color_surface:I = 0x7f0600a2

.field public static final design_error:I = 0x7f0600a3

.field public static final design_fab_shadow_end_color:I = 0x7f0600a4

.field public static final design_fab_shadow_mid_color:I = 0x7f0600a5

.field public static final design_fab_shadow_start_color:I = 0x7f0600a6

.field public static final design_fab_stroke_end_inner_color:I = 0x7f0600a7

.field public static final design_fab_stroke_end_outer_color:I = 0x7f0600a8

.field public static final design_fab_stroke_top_inner_color:I = 0x7f0600a9

.field public static final design_fab_stroke_top_outer_color:I = 0x7f0600aa

.field public static final design_icon_tint:I = 0x7f0600ab

.field public static final design_snackbar_background_color:I = 0x7f0600ac

.field public static final dim_foreground_disabled_material_dark:I = 0x7f0600ad

.field public static final dim_foreground_disabled_material_light:I = 0x7f0600ae

.field public static final dim_foreground_material_dark:I = 0x7f0600af

.field public static final dim_foreground_material_light:I = 0x7f0600b0

.field public static final error_color_material_dark:I = 0x7f0600d5

.field public static final error_color_material_light:I = 0x7f0600d6

.field public static final foreground_material_dark:I = 0x7f0600d9

.field public static final foreground_material_light:I = 0x7f0600da

.field public static final highlighted_text_material_dark:I = 0x7f0600df

.field public static final highlighted_text_material_light:I = 0x7f0600e0

.field public static final material_blue_grey_800:I = 0x7f060190

.field public static final material_blue_grey_900:I = 0x7f060191

.field public static final material_blue_grey_950:I = 0x7f060192

.field public static final material_deep_teal_200:I = 0x7f060193

.field public static final material_deep_teal_500:I = 0x7f060194

.field public static final material_grey_100:I = 0x7f060195

.field public static final material_grey_300:I = 0x7f060196

.field public static final material_grey_50:I = 0x7f060197

.field public static final material_grey_600:I = 0x7f060198

.field public static final material_grey_800:I = 0x7f060199

.field public static final material_grey_850:I = 0x7f06019a

.field public static final material_grey_900:I = 0x7f06019b

.field public static final material_on_background_disabled:I = 0x7f06019c

.field public static final material_on_background_emphasis_high_type:I = 0x7f06019d

.field public static final material_on_background_emphasis_medium:I = 0x7f06019e

.field public static final material_on_primary_disabled:I = 0x7f06019f

.field public static final material_on_primary_emphasis_high_type:I = 0x7f0601a0

.field public static final material_on_primary_emphasis_medium:I = 0x7f0601a1

.field public static final material_on_surface_disabled:I = 0x7f0601a2

.field public static final material_on_surface_emphasis_high_type:I = 0x7f0601a3

.field public static final material_on_surface_emphasis_medium:I = 0x7f0601a4

.field public static final mtrl_bottom_nav_colored_item_tint:I = 0x7f0601aa

.field public static final mtrl_bottom_nav_colored_ripple_color:I = 0x7f0601ab

.field public static final mtrl_bottom_nav_item_tint:I = 0x7f0601ac

.field public static final mtrl_bottom_nav_ripple_color:I = 0x7f0601ad

.field public static final mtrl_btn_bg_color_selector:I = 0x7f0601ae

.field public static final mtrl_btn_ripple_color:I = 0x7f0601af

.field public static final mtrl_btn_stroke_color_selector:I = 0x7f0601b0

.field public static final mtrl_btn_text_btn_bg_color_selector:I = 0x7f0601b1

.field public static final mtrl_btn_text_btn_ripple_color:I = 0x7f0601b2

.field public static final mtrl_btn_text_color_disabled:I = 0x7f0601b3

.field public static final mtrl_btn_text_color_selector:I = 0x7f0601b4

.field public static final mtrl_btn_transparent_bg_color:I = 0x7f0601b5

.field public static final mtrl_calendar_item_stroke_color:I = 0x7f0601b6

.field public static final mtrl_calendar_selected_range:I = 0x7f0601b7

.field public static final mtrl_card_view_foreground:I = 0x7f0601b8

.field public static final mtrl_card_view_ripple:I = 0x7f0601b9

.field public static final mtrl_chip_background_color:I = 0x7f0601ba

.field public static final mtrl_chip_close_icon_tint:I = 0x7f0601bb

.field public static final mtrl_chip_ripple_color:I = 0x7f0601bc

.field public static final mtrl_chip_surface_color:I = 0x7f0601bd

.field public static final mtrl_chip_text_color:I = 0x7f0601be

.field public static final mtrl_choice_chip_background_color:I = 0x7f0601bf

.field public static final mtrl_choice_chip_ripple_color:I = 0x7f0601c0

.field public static final mtrl_choice_chip_text_color:I = 0x7f0601c1

.field public static final mtrl_error:I = 0x7f0601c2

.field public static final mtrl_extended_fab_bg_color_selector:I = 0x7f0601c3

.field public static final mtrl_extended_fab_ripple_color:I = 0x7f0601c4

.field public static final mtrl_extended_fab_text_color_selector:I = 0x7f0601c5

.field public static final mtrl_fab_ripple_color:I = 0x7f0601c6

.field public static final mtrl_filled_background_color:I = 0x7f0601c7

.field public static final mtrl_filled_icon_tint:I = 0x7f0601c8

.field public static final mtrl_filled_stroke_color:I = 0x7f0601c9

.field public static final mtrl_indicator_text_color:I = 0x7f0601ca

.field public static final mtrl_navigation_item_background_color:I = 0x7f0601cb

.field public static final mtrl_navigation_item_icon_tint:I = 0x7f0601cc

.field public static final mtrl_navigation_item_text_color:I = 0x7f0601cd

.field public static final mtrl_on_primary_text_btn_text_color_selector:I = 0x7f0601ce

.field public static final mtrl_outlined_icon_tint:I = 0x7f0601cf

.field public static final mtrl_outlined_stroke_color:I = 0x7f0601d0

.field public static final mtrl_popupmenu_overlay_color:I = 0x7f0601d1

.field public static final mtrl_scrim_color:I = 0x7f0601d2

.field public static final mtrl_tabs_colored_ripple_color:I = 0x7f0601d3

.field public static final mtrl_tabs_icon_color_selector:I = 0x7f0601d4

.field public static final mtrl_tabs_icon_color_selector_colored:I = 0x7f0601d5

.field public static final mtrl_tabs_legacy_text_color_selector:I = 0x7f0601d6

.field public static final mtrl_tabs_ripple_color:I = 0x7f0601d7

.field public static final mtrl_text_btn_text_color_selector:I = 0x7f0601d8

.field public static final mtrl_textinput_default_box_stroke_color:I = 0x7f0601d9

.field public static final mtrl_textinput_disabled_color:I = 0x7f0601da

.field public static final mtrl_textinput_filled_box_default_background_color:I = 0x7f0601db

.field public static final mtrl_textinput_focused_box_stroke_color:I = 0x7f0601dc

.field public static final mtrl_textinput_hovered_box_stroke_color:I = 0x7f0601dd

.field public static final notification_action_color_filter:I = 0x7f060280

.field public static final notification_icon_bg_color:I = 0x7f060282

.field public static final primary_dark_material_dark:I = 0x7f06028f

.field public static final primary_dark_material_light:I = 0x7f060290

.field public static final primary_material_dark:I = 0x7f060291

.field public static final primary_material_light:I = 0x7f060292

.field public static final primary_text_default_material_dark:I = 0x7f060293

.field public static final primary_text_default_material_light:I = 0x7f060294

.field public static final primary_text_disabled_material_dark:I = 0x7f060295

.field public static final primary_text_disabled_material_light:I = 0x7f060296

.field public static final ripple_material_dark:I = 0x7f0602a8

.field public static final ripple_material_light:I = 0x7f0602a9

.field public static final secondary_text_default_material_dark:I = 0x7f0602c8

.field public static final secondary_text_default_material_light:I = 0x7f0602c9

.field public static final secondary_text_disabled_material_dark:I = 0x7f0602ca

.field public static final secondary_text_disabled_material_light:I = 0x7f0602cb

.field public static final switch_thumb_disabled_material_dark:I = 0x7f0602eb

.field public static final switch_thumb_disabled_material_light:I = 0x7f0602ec

.field public static final switch_thumb_material_dark:I = 0x7f0602ed

.field public static final switch_thumb_material_light:I = 0x7f0602ee

.field public static final switch_thumb_normal_material_dark:I = 0x7f0602ef

.field public static final switch_thumb_normal_material_light:I = 0x7f0602f0

.field public static final test_mtrl_calendar_day:I = 0x7f0602f2

.field public static final test_mtrl_calendar_day_selected:I = 0x7f0602f3

.field public static final tooltip_background_dark:I = 0x7f0602fe

.field public static final tooltip_background_light:I = 0x7f0602ff


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 693
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
