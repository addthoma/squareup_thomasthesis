.class public Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;
.super Landroid/widget/ScrollView;
.source "StickyScrollView.java"


# static fields
.field public static final FLAG_HASTRANSPARANCY:Ljava/lang/String; = "-hastransparancy"

.field public static final FLAG_NONCONSTANT:Ljava/lang/String; = "-nonconstant"

.field public static final STICKY_TAG:Ljava/lang/String; = "sticky"


# instance fields
.field private clipToPaddingHasBeenSet:Z

.field private clippingToPadding:Z

.field private currentlyStickingView:Landroid/view/View;

.field private hasNotDoneActionDown:Z

.field private final invalidateRunnable:Ljava/lang/Runnable;

.field private redirectTouchesToStickyView:Z

.field private stickyViewTopOffset:F

.field private stickyViews:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    .line 60
    invoke-direct {p0, p1, v0}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const v0, 0x1010080

    .line 64
    invoke-direct {p0, p1, p2, v0}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 68
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 44
    new-instance p1, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView$1;

    invoke-direct {p1, p0}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView$1;-><init>(Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;)V

    iput-object p1, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->invalidateRunnable:Ljava/lang/Runnable;

    const/4 p1, 0x1

    .line 199
    iput-boolean p1, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->hasNotDoneActionDown:Z

    .line 69
    invoke-virtual {p0}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->setup()V

    return-void
.end method

.method static synthetic access$000(Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;)Landroid/view/View;
    .locals 0

    .line 20
    iget-object p0, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->currentlyStickingView:Landroid/view/View;

    return-object p0
.end method

.method static synthetic access$100(Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;Landroid/view/View;)I
    .locals 0

    .line 20
    invoke-direct {p0, p1}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->getLeftForViewRelativeOnlyChild(Landroid/view/View;)I

    move-result p0

    return p0
.end method

.method static synthetic access$200(Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;Landroid/view/View;)I
    .locals 0

    .line 20
    invoke-direct {p0, p1}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->getBottomForViewRelativeOnlyChild(Landroid/view/View;)I

    move-result p0

    return p0
.end method

.method static synthetic access$300(Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;Landroid/view/View;)I
    .locals 0

    .line 20
    invoke-direct {p0, p1}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->getRightForViewRelativeOnlyChild(Landroid/view/View;)I

    move-result p0

    return p0
.end method

.method static synthetic access$400(Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;)F
    .locals 0

    .line 20
    iget p0, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->stickyViewTopOffset:F

    return p0
.end method

.method private doTheStickyThing()V
    .locals 8

    .line 234
    iget-object v0, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->stickyViews:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    move-object v2, v1

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    const/4 v4, 0x0

    if-eqz v3, :cond_7

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    .line 235
    invoke-direct {p0, v3}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->getTopForViewRelativeOnlyChild(Landroid/view/View;)I

    move-result v5

    invoke-virtual {p0}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->getScrollY()I

    move-result v6

    sub-int/2addr v5, v6

    iget-boolean v6, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->clippingToPadding:Z

    if-eqz v6, :cond_1

    const/4 v6, 0x0

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->getPaddingTop()I

    move-result v6

    :goto_1
    add-int/2addr v5, v6

    if-gtz v5, :cond_4

    if-eqz v1, :cond_3

    .line 237
    invoke-direct {p0, v1}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->getTopForViewRelativeOnlyChild(Landroid/view/View;)I

    move-result v6

    invoke-virtual {p0}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->getScrollY()I

    move-result v7

    sub-int/2addr v6, v7

    iget-boolean v7, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->clippingToPadding:Z

    if-eqz v7, :cond_2

    goto :goto_2

    :cond_2
    invoke-virtual {p0}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->getPaddingTop()I

    move-result v4

    :goto_2
    add-int/2addr v6, v4

    if-le v5, v6, :cond_0

    :cond_3
    move-object v1, v3

    goto :goto_0

    :cond_4
    if-eqz v2, :cond_6

    .line 241
    invoke-direct {p0, v2}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->getTopForViewRelativeOnlyChild(Landroid/view/View;)I

    move-result v6

    invoke-virtual {p0}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->getScrollY()I

    move-result v7

    sub-int/2addr v6, v7

    iget-boolean v7, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->clippingToPadding:Z

    if-eqz v7, :cond_5

    goto :goto_3

    :cond_5
    invoke-virtual {p0}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->getPaddingTop()I

    move-result v4

    :goto_3
    add-int/2addr v6, v4

    if-ge v5, v6, :cond_0

    :cond_6
    move-object v2, v3

    goto :goto_0

    :cond_7
    if-eqz v1, :cond_b

    if-nez v2, :cond_8

    const/4 v0, 0x0

    goto :goto_5

    .line 247
    :cond_8
    invoke-direct {p0, v2}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->getTopForViewRelativeOnlyChild(Landroid/view/View;)I

    move-result v0

    invoke-virtual {p0}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->getScrollY()I

    move-result v2

    sub-int/2addr v0, v2

    iget-boolean v2, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->clippingToPadding:Z

    if-eqz v2, :cond_9

    const/4 v2, 0x0

    goto :goto_4

    :cond_9
    invoke-virtual {p0}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->getPaddingTop()I

    move-result v2

    :goto_4
    add-int/2addr v0, v2

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v2

    sub-int/2addr v0, v2

    invoke-static {v4, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    int-to-float v0, v0

    :goto_5
    iput v0, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->stickyViewTopOffset:F

    .line 248
    iget-object v0, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->currentlyStickingView:Landroid/view/View;

    if-eq v1, v0, :cond_c

    if-eqz v0, :cond_a

    .line 250
    invoke-direct {p0}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->stopStickingCurrentlyStickingView()V

    .line 252
    :cond_a
    invoke-direct {p0, v1}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->startStickingView(Landroid/view/View;)V

    goto :goto_6

    .line 254
    :cond_b
    iget-object v0, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->currentlyStickingView:Landroid/view/View;

    if-eqz v0, :cond_c

    .line 255
    invoke-direct {p0}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->stopStickingCurrentlyStickingView()V

    :cond_c
    :goto_6
    return-void
.end method

.method private findStickyViews(Landroid/view/View;)V
    .locals 4

    .line 295
    instance-of v0, p1, Landroid/view/ViewGroup;

    const-string v1, "sticky"

    if-eqz v0, :cond_2

    .line 296
    check-cast p1, Landroid/view/ViewGroup;

    const/4 v0, 0x0

    .line 297
    :goto_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_3

    .line 298
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->getStringTagForView(Landroid/view/View;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 299
    invoke-virtual {v2, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 300
    iget-object v2, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->stickyViews:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 301
    :cond_0
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    instance-of v2, v2, Landroid/view/ViewGroup;

    if-eqz v2, :cond_1

    .line 302
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->findStickyViews(Landroid/view/View;)V

    :cond_1
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 306
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 307
    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 308
    iget-object v0, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->stickyViews:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    return-void
.end method

.method private getBottomForViewRelativeOnlyChild(Landroid/view/View;)I
    .locals 3

    .line 104
    invoke-virtual {p1}, Landroid/view/View;->getBottom()I

    move-result v0

    .line 105
    :goto_0
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    if-eq v1, v2, :cond_0

    .line 106
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    .line 107
    invoke-virtual {p1}, Landroid/view/View;->getBottom()I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_0

    :cond_0
    return v0
.end method

.method private getLeftForViewRelativeOnlyChild(Landroid/view/View;)I
    .locals 3

    .line 77
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v0

    .line 78
    :goto_0
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    if-eq v1, v2, :cond_0

    .line 79
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    .line 80
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_0

    :cond_0
    return v0
.end method

.method private getRightForViewRelativeOnlyChild(Landroid/view/View;)I
    .locals 3

    .line 95
    invoke-virtual {p1}, Landroid/view/View;->getRight()I

    move-result v0

    .line 96
    :goto_0
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    if-eq v1, v2, :cond_0

    .line 97
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    .line 98
    invoke-virtual {p1}, Landroid/view/View;->getRight()I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_0

    :cond_0
    return v0
.end method

.method private getStringTagForView(Landroid/view/View;)Ljava/lang/String;
    .locals 0

    .line 314
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object p1

    .line 315
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private getTopForViewRelativeOnlyChild(Landroid/view/View;)I
    .locals 3

    .line 86
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v0

    .line 87
    :goto_0
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    if-eq v1, v2, :cond_0

    .line 88
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    .line 89
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_0

    :cond_0
    return v0
.end method

.method private hideView(Landroid/view/View;)V
    .locals 3

    .line 319
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v1, 0x0

    const/16 v2, 0xb

    if-lt v0, v2, :cond_0

    .line 320
    invoke-virtual {p1, v1}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0

    .line 322
    :cond_0
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v2, v1}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    const-wide/16 v1, 0x0

    .line 323
    invoke-virtual {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    const/4 v1, 0x1

    .line 324
    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V

    .line 325
    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :goto_0
    return-void
.end method

.method private notifyHierarchyChanged()V
    .locals 1

    .line 285
    iget-object v0, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->currentlyStickingView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 286
    invoke-direct {p0}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->stopStickingCurrentlyStickingView()V

    .line 288
    :cond_0
    iget-object v0, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->stickyViews:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    const/4 v0, 0x0

    .line 289
    invoke-virtual {p0, v0}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->findStickyViews(Landroid/view/View;)V

    .line 290
    invoke-direct {p0}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->doTheStickyThing()V

    .line 291
    invoke-virtual {p0}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->invalidate()V

    return-void
.end method

.method private showView(Landroid/view/View;)V
    .locals 3

    .line 330
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/high16 v1, 0x3f800000    # 1.0f

    const/16 v2, 0xb

    if-lt v0, v2, :cond_0

    .line 331
    invoke-virtual {p1, v1}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0

    .line 333
    :cond_0
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/4 v2, 0x0

    invoke-direct {v0, v2, v1}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    const-wide/16 v1, 0x0

    .line 334
    invoke-virtual {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    const/4 v1, 0x1

    .line 335
    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V

    .line 336
    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :goto_0
    return-void
.end method

.method private startStickingView(Landroid/view/View;)V
    .locals 1

    .line 260
    iput-object p1, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->currentlyStickingView:Landroid/view/View;

    .line 261
    iget-object p1, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->currentlyStickingView:Landroid/view/View;

    invoke-direct {p0, p1}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->getStringTagForView(Landroid/view/View;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "-hastransparancy"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 262
    iget-object p1, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->currentlyStickingView:Landroid/view/View;

    invoke-direct {p0, p1}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->hideView(Landroid/view/View;)V

    .line 264
    :cond_0
    iget-object p1, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->currentlyStickingView:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    const-string v0, "-nonconstant"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 265
    iget-object p1, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->invalidateRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, p1}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->post(Ljava/lang/Runnable;)Z

    :cond_1
    return-void
.end method

.method private stopStickingCurrentlyStickingView()V
    .locals 2

    .line 270
    iget-object v0, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->currentlyStickingView:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->getStringTagForView(Landroid/view/View;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "-hastransparancy"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 271
    iget-object v0, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->currentlyStickingView:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->showView(Landroid/view/View;)V

    :cond_0
    const/4 v0, 0x0

    .line 273
    iput-object v0, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->currentlyStickingView:Landroid/view/View;

    .line 274
    iget-object v0, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->invalidateRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->removeCallbacks(Ljava/lang/Runnable;)Z

    return-void
.end method


# virtual methods
.method public addView(Landroid/view/View;)V
    .locals 0

    .line 130
    invoke-super {p0, p1}, Landroid/widget/ScrollView;->addView(Landroid/view/View;)V

    .line 131
    invoke-direct {p0, p1}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->findStickyViews(Landroid/view/View;)V

    return-void
.end method

.method public addView(Landroid/view/View;I)V
    .locals 0

    .line 136
    invoke-super {p0, p1, p2}, Landroid/widget/ScrollView;->addView(Landroid/view/View;I)V

    .line 137
    invoke-direct {p0, p1}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->findStickyViews(Landroid/view/View;)V

    return-void
.end method

.method public addView(Landroid/view/View;II)V
    .locals 0

    .line 148
    invoke-super {p0, p1, p2, p3}, Landroid/widget/ScrollView;->addView(Landroid/view/View;II)V

    .line 149
    invoke-direct {p0, p1}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->findStickyViews(Landroid/view/View;)V

    return-void
.end method

.method public addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .locals 0

    .line 142
    invoke-super {p0, p1, p2, p3}, Landroid/widget/ScrollView;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 143
    invoke-direct {p0, p1}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->findStickyViews(Landroid/view/View;)V

    return-void
.end method

.method public addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 0

    .line 154
    invoke-super {p0, p1, p2}, Landroid/widget/ScrollView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 155
    invoke-direct {p0, p1}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->findStickyViews(Landroid/view/View;)V

    return-void
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .line 160
    invoke-super {p0, p1}, Landroid/widget/ScrollView;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 161
    iget-object v0, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->currentlyStickingView:Landroid/view/View;

    if-eqz v0, :cond_3

    .line 162
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 163
    invoke-virtual {p0}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->getPaddingLeft()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->getScrollY()I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->stickyViewTopOffset:F

    add-float/2addr v1, v2

    iget-boolean v2, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->clippingToPadding:Z

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->getPaddingTop()I

    move-result v2

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    int-to-float v2, v2

    add-float/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 164
    iget-boolean v0, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->clippingToPadding:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->stickyViewTopOffset:F

    neg-float v0, v0

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p0}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->getWidth()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->currentlyStickingView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p1, v1, v0, v2, v3}, Landroid/graphics/Canvas;->clipRect(FFFF)Z

    .line 165
    iget-object v0, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->currentlyStickingView:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->getStringTagForView(Landroid/view/View;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "-hastransparancy"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 166
    iget-object v0, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->currentlyStickingView:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->showView(Landroid/view/View;)V

    .line 167
    iget-object v0, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->currentlyStickingView:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 168
    iget-object v0, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->currentlyStickingView:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->hideView(Landroid/view/View;)V

    goto :goto_2

    .line 170
    :cond_2
    iget-object v0, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->currentlyStickingView:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 172
    :goto_2
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    :cond_3
    return-void
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    .line 178
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_0

    .line 179
    iput-boolean v1, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->redirectTouchesToStickyView:Z

    .line 182
    :cond_0
    iget-boolean v0, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->redirectTouchesToStickyView:Z

    const/4 v2, 0x0

    if-eqz v0, :cond_3

    .line 183
    iget-object v0, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->currentlyStickingView:Landroid/view/View;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->redirectTouchesToStickyView:Z

    .line 184
    iget-boolean v0, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->redirectTouchesToStickyView:Z

    if-eqz v0, :cond_4

    .line 185
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iget-object v3, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->currentlyStickingView:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    int-to-float v3, v3

    iget v4, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->stickyViewTopOffset:F

    add-float/2addr v3, v4

    cmpg-float v0, v0, v3

    if-gtz v0, :cond_2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget-object v3, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->currentlyStickingView:Landroid/view/View;

    invoke-direct {p0, v3}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->getLeftForViewRelativeOnlyChild(Landroid/view/View;)I

    move-result v3

    int-to-float v3, v3

    cmpl-float v0, v0, v3

    if-ltz v0, :cond_2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget-object v3, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->currentlyStickingView:Landroid/view/View;

    invoke-direct {p0, v3}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->getRightForViewRelativeOnlyChild(Landroid/view/View;)I

    move-result v3

    int-to-float v3, v3

    cmpg-float v0, v0, v3

    if-gtz v0, :cond_2

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    :goto_1
    iput-boolean v1, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->redirectTouchesToStickyView:Z

    goto :goto_2

    .line 190
    :cond_3
    iget-object v0, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->currentlyStickingView:Landroid/view/View;

    if-nez v0, :cond_4

    .line 191
    iput-boolean v2, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->redirectTouchesToStickyView:Z

    .line 193
    :cond_4
    :goto_2
    iget-boolean v0, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->redirectTouchesToStickyView:Z

    if-eqz v0, :cond_5

    const/4 v0, 0x0

    const/high16 v1, -0x40800000    # -1.0f

    .line 194
    invoke-virtual {p0}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->getScrollY()I

    move-result v2

    int-to-float v2, v2

    iget v3, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->stickyViewTopOffset:F

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->currentlyStickingView:Landroid/view/View;

    invoke-direct {p0, v3}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->getTopForViewRelativeOnlyChild(Landroid/view/View;)I

    move-result v3

    int-to-float v3, v3

    sub-float/2addr v2, v3

    mul-float v2, v2, v1

    invoke-virtual {p1, v0, v2}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 196
    :cond_5
    invoke-super {p0, p1}, Landroid/widget/ScrollView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1
.end method

.method public notifyStickyAttributeChanged()V
    .locals 0

    .line 281
    invoke-direct {p0}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->notifyHierarchyChanged()V

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 0

    .line 114
    invoke-super/range {p0 .. p5}, Landroid/widget/ScrollView;->onLayout(ZIIII)V

    .line 115
    iget-boolean p1, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->clipToPaddingHasBeenSet:Z

    if-nez p1, :cond_0

    const/4 p1, 0x1

    .line 116
    iput-boolean p1, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->clippingToPadding:Z

    .line 118
    :cond_0
    invoke-direct {p0}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->notifyHierarchyChanged()V

    return-void
.end method

.method protected onScrollChanged(IIII)V
    .locals 0

    .line 227
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ScrollView;->onScrollChanged(IIII)V

    .line 228
    invoke-direct {p0}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->doTheStickyThing()V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .line 203
    iget-boolean v0, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->redirectTouchesToStickyView:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 204
    invoke-virtual {p0}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->getScrollY()I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->stickyViewTopOffset:F

    add-float/2addr v1, v2

    iget-object v2, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->currentlyStickingView:Landroid/view/View;

    invoke-direct {p0, v2}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->getTopForViewRelativeOnlyChild(Landroid/view/View;)I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 207
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_1

    .line 208
    iput-boolean v1, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->hasNotDoneActionDown:Z

    .line 211
    :cond_1
    iget-boolean v0, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->hasNotDoneActionDown:Z

    if-eqz v0, :cond_2

    .line 212
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    .line 213
    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->setAction(I)V

    .line 214
    invoke-super {p0, v0}, Landroid/widget/ScrollView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 215
    iput-boolean v1, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->hasNotDoneActionDown:Z

    .line 218
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v2, 0x3

    if-ne v0, v2, :cond_4

    .line 219
    :cond_3
    iput-boolean v1, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->hasNotDoneActionDown:Z

    .line 222
    :cond_4
    invoke-super {p0, p1}, Landroid/widget/ScrollView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result p1

    return p1
.end method

.method public setClipToPadding(Z)V
    .locals 0

    .line 123
    invoke-super {p0, p1}, Landroid/widget/ScrollView;->setClipToPadding(Z)V

    .line 124
    iput-boolean p1, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->clippingToPadding:Z

    const/4 p1, 0x1

    .line 125
    iput-boolean p1, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->clipToPaddingHasBeenSet:Z

    return-void
.end method

.method public setup()V
    .locals 1

    .line 73
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->stickyViews:Ljava/util/ArrayList;

    return-void
.end method
