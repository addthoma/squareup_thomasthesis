.class Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView$1;
.super Ljava/lang/Object;
.source "StickyScrollView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;


# direct methods
.method constructor <init>(Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;)V
    .locals 0

    .line 44
    iput-object p1, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView$1;->this$0:Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .line 48
    iget-object v0, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView$1;->this$0:Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;

    invoke-static {v0}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->access$000(Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 49
    iget-object v0, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView$1;->this$0:Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;

    invoke-static {v0}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->access$000(Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;)Landroid/view/View;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->access$100(Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;Landroid/view/View;)I

    move-result v0

    .line 50
    iget-object v1, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView$1;->this$0:Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;

    invoke-static {v1}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->access$000(Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;)Landroid/view/View;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->access$200(Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;Landroid/view/View;)I

    move-result v1

    .line 51
    iget-object v2, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView$1;->this$0:Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;

    invoke-static {v2}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->access$000(Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;)Landroid/view/View;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->access$300(Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;Landroid/view/View;)I

    move-result v2

    .line 52
    iget-object v3, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView$1;->this$0:Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;

    invoke-virtual {v3}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->getScrollY()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView$1;->this$0:Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;

    invoke-static {v4}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->access$000(Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v4

    int-to-float v4, v4

    iget-object v5, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView$1;->this$0:Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;

    invoke-static {v5}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->access$400(Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;)F

    move-result v5

    add-float/2addr v4, v5

    add-float/2addr v3, v4

    float-to-int v3, v3

    .line 53
    iget-object v4, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView$1;->this$0:Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;

    invoke-virtual {v4, v0, v1, v2, v3}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->invalidate(IIII)V

    .line 55
    :cond_0
    iget-object v0, p0, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView$1;->this$0:Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;

    const-wide/16 v1, 0x10

    invoke-virtual {v0, p0, v1, v2}, Lcom/emilsjolander/components/StickyScrollViewItems/StickyScrollView;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method
