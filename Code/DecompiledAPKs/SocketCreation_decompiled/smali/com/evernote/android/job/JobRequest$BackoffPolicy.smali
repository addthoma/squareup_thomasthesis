.class public final enum Lcom/evernote/android/job/JobRequest$BackoffPolicy;
.super Ljava/lang/Enum;
.source "JobRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/evernote/android/job/JobRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "BackoffPolicy"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/evernote/android/job/JobRequest$BackoffPolicy;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/evernote/android/job/JobRequest$BackoffPolicy;

.field public static final enum EXPONENTIAL:Lcom/evernote/android/job/JobRequest$BackoffPolicy;

.field public static final enum LINEAR:Lcom/evernote/android/job/JobRequest$BackoffPolicy;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 1189
    new-instance v0, Lcom/evernote/android/job/JobRequest$BackoffPolicy;

    const/4 v1, 0x0

    const-string v2, "LINEAR"

    invoke-direct {v0, v2, v1}, Lcom/evernote/android/job/JobRequest$BackoffPolicy;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/evernote/android/job/JobRequest$BackoffPolicy;->LINEAR:Lcom/evernote/android/job/JobRequest$BackoffPolicy;

    .line 1193
    new-instance v0, Lcom/evernote/android/job/JobRequest$BackoffPolicy;

    const/4 v2, 0x1

    const-string v3, "EXPONENTIAL"

    invoke-direct {v0, v3, v2}, Lcom/evernote/android/job/JobRequest$BackoffPolicy;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/evernote/android/job/JobRequest$BackoffPolicy;->EXPONENTIAL:Lcom/evernote/android/job/JobRequest$BackoffPolicy;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/evernote/android/job/JobRequest$BackoffPolicy;

    .line 1185
    sget-object v3, Lcom/evernote/android/job/JobRequest$BackoffPolicy;->LINEAR:Lcom/evernote/android/job/JobRequest$BackoffPolicy;

    aput-object v3, v0, v1

    sget-object v1, Lcom/evernote/android/job/JobRequest$BackoffPolicy;->EXPONENTIAL:Lcom/evernote/android/job/JobRequest$BackoffPolicy;

    aput-object v1, v0, v2

    sput-object v0, Lcom/evernote/android/job/JobRequest$BackoffPolicy;->$VALUES:[Lcom/evernote/android/job/JobRequest$BackoffPolicy;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1185
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/evernote/android/job/JobRequest$BackoffPolicy;
    .locals 1

    .line 1185
    const-class v0, Lcom/evernote/android/job/JobRequest$BackoffPolicy;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/evernote/android/job/JobRequest$BackoffPolicy;

    return-object p0
.end method

.method public static values()[Lcom/evernote/android/job/JobRequest$BackoffPolicy;
    .locals 1

    .line 1185
    sget-object v0, Lcom/evernote/android/job/JobRequest$BackoffPolicy;->$VALUES:[Lcom/evernote/android/job/JobRequest$BackoffPolicy;

    invoke-virtual {v0}, [Lcom/evernote/android/job/JobRequest$BackoffPolicy;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/evernote/android/job/JobRequest$BackoffPolicy;

    return-object v0
.end method
