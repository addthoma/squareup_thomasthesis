.class Lcom/mattprecious/telescope/TelescopeLayout$5;
.super Ljava/lang/Object;
.source "TelescopeLayout.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mattprecious/telescope/TelescopeLayout;->captureCanvasScreenshot()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mattprecious/telescope/TelescopeLayout;


# direct methods
.method constructor <init>(Lcom/mattprecious/telescope/TelescopeLayout;)V
    .locals 0

    .line 467
    iput-object p1, p0, Lcom/mattprecious/telescope/TelescopeLayout$5;->this$0:Lcom/mattprecious/telescope/TelescopeLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .line 469
    iget-object v0, p0, Lcom/mattprecious/telescope/TelescopeLayout$5;->this$0:Lcom/mattprecious/telescope/TelescopeLayout;

    invoke-static {v0}, Lcom/mattprecious/telescope/TelescopeLayout;->access$700(Lcom/mattprecious/telescope/TelescopeLayout;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x1

    .line 470
    invoke-virtual {v0, v1}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 471
    invoke-virtual {v0}, Landroid/view/View;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-static {v1}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v1

    const/4 v2, 0x0

    .line 472
    invoke-virtual {v0, v2}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 474
    iget-object v0, p0, Lcom/mattprecious/telescope/TelescopeLayout$5;->this$0:Lcom/mattprecious/telescope/TelescopeLayout;

    invoke-static {v0}, Lcom/mattprecious/telescope/TelescopeLayout;->access$800(Lcom/mattprecious/telescope/TelescopeLayout;)V

    .line 476
    iget-object v0, p0, Lcom/mattprecious/telescope/TelescopeLayout$5;->this$0:Lcom/mattprecious/telescope/TelescopeLayout;

    invoke-static {v0}, Lcom/mattprecious/telescope/TelescopeLayout;->access$900(Lcom/mattprecious/telescope/TelescopeLayout;)V

    .line 477
    iget-object v0, p0, Lcom/mattprecious/telescope/TelescopeLayout$5;->this$0:Lcom/mattprecious/telescope/TelescopeLayout;

    invoke-static {v0}, Lcom/mattprecious/telescope/TelescopeLayout;->access$1000(Lcom/mattprecious/telescope/TelescopeLayout;)Lcom/mattprecious/telescope/Lens;

    move-result-object v0

    new-instance v2, Lcom/mattprecious/telescope/TelescopeLayout$5$1;

    invoke-direct {v2, p0}, Lcom/mattprecious/telescope/TelescopeLayout$5$1;-><init>(Lcom/mattprecious/telescope/TelescopeLayout$5;)V

    invoke-virtual {v0, v1, v2}, Lcom/mattprecious/telescope/Lens;->onCapture(Landroid/graphics/Bitmap;Lcom/mattprecious/telescope/BitmapProcessorListener;)V

    return-void
.end method
