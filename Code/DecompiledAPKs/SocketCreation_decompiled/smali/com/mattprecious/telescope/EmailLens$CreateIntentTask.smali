.class final Lcom/mattprecious/telescope/EmailLens$CreateIntentTask;
.super Landroid/os/AsyncTask;
.source "EmailLens.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mattprecious/telescope/EmailLens;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "CreateIntentTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Landroid/content/Intent;",
        ">;"
    }
.end annotation


# instance fields
.field private final context:Landroid/content/Context;

.field private final screenshot:Ljava/io/File;

.field final synthetic this$0:Lcom/mattprecious/telescope/EmailLens;


# direct methods
.method constructor <init>(Lcom/mattprecious/telescope/EmailLens;Landroid/content/Context;Ljava/io/File;)V
    .locals 0

    .line 55
    iput-object p1, p0, Lcom/mattprecious/telescope/EmailLens$CreateIntentTask;->this$0:Lcom/mattprecious/telescope/EmailLens;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 56
    iput-object p2, p0, Lcom/mattprecious/telescope/EmailLens$CreateIntentTask;->context:Landroid/content/Context;

    .line 57
    iput-object p3, p0, Lcom/mattprecious/telescope/EmailLens$CreateIntentTask;->screenshot:Ljava/io/File;

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Landroid/content/Intent;
    .locals 3

    .line 61
    new-instance p1, Landroid/content/Intent;

    const-string v0, "android.intent.action.SEND_MULTIPLE"

    invoke-direct {p1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v0, "message/rfc822"

    .line 62
    invoke-virtual {p1, v0}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 64
    iget-object v0, p0, Lcom/mattprecious/telescope/EmailLens$CreateIntentTask;->this$0:Lcom/mattprecious/telescope/EmailLens;

    invoke-static {v0}, Lcom/mattprecious/telescope/EmailLens;->access$000(Lcom/mattprecious/telescope/EmailLens;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/mattprecious/telescope/EmailLens$CreateIntentTask;->this$0:Lcom/mattprecious/telescope/EmailLens;

    invoke-static {v0}, Lcom/mattprecious/telescope/EmailLens;->access$000(Lcom/mattprecious/telescope/EmailLens;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.extra.SUBJECT"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 68
    :cond_0
    iget-object v0, p0, Lcom/mattprecious/telescope/EmailLens$CreateIntentTask;->this$0:Lcom/mattprecious/telescope/EmailLens;

    invoke-static {v0}, Lcom/mattprecious/telescope/EmailLens;->access$100(Lcom/mattprecious/telescope/EmailLens;)[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 69
    iget-object v0, p0, Lcom/mattprecious/telescope/EmailLens$CreateIntentTask;->this$0:Lcom/mattprecious/telescope/EmailLens;

    invoke-static {v0}, Lcom/mattprecious/telescope/EmailLens;->access$100(Lcom/mattprecious/telescope/EmailLens;)[Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.extra.EMAIL"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 72
    :cond_1
    iget-object v0, p0, Lcom/mattprecious/telescope/EmailLens$CreateIntentTask;->this$0:Lcom/mattprecious/telescope/EmailLens;

    invoke-virtual {v0}, Lcom/mattprecious/telescope/EmailLens;->getBody()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    const-string v1, "android.intent.extra.TEXT"

    .line 74
    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 77
    :cond_2
    iget-object v0, p0, Lcom/mattprecious/telescope/EmailLens$CreateIntentTask;->this$0:Lcom/mattprecious/telescope/EmailLens;

    invoke-virtual {v0}, Lcom/mattprecious/telescope/EmailLens;->getAdditionalAttachments()Ljava/util/Set;

    move-result-object v0

    .line 78
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 79
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 80
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 82
    :cond_3
    iget-object v0, p0, Lcom/mattprecious/telescope/EmailLens$CreateIntentTask;->screenshot:Ljava/io/File;

    if-eqz v0, :cond_4

    .line 83
    iget-object v2, p0, Lcom/mattprecious/telescope/EmailLens$CreateIntentTask;->context:Landroid/content/Context;

    invoke-static {v2, v0}, Lcom/mattprecious/telescope/TelescopeFileProvider;->getUriForFile(Landroid/content/Context;Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 86
    :cond_4
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "android.intent.extra.STREAM"

    .line 87
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    :cond_5
    return-object p1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 51
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/mattprecious/telescope/EmailLens$CreateIntentTask;->doInBackground([Ljava/lang/Void;)Landroid/content/Intent;

    move-result-object p1

    return-object p1
.end method

.method protected onPostExecute(Landroid/content/Intent;)V
    .locals 1

    .line 94
    iget-object v0, p0, Lcom/mattprecious/telescope/EmailLens$CreateIntentTask;->context:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 51
    check-cast p1, Landroid/content/Intent;

    invoke-virtual {p0, p1}, Lcom/mattprecious/telescope/EmailLens$CreateIntentTask;->onPostExecute(Landroid/content/Intent;)V

    return-void
.end method
