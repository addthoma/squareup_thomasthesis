.class public final Lsquareup/items/merchant/Attribute$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Attribute.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/items/merchant/Attribute;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lsquareup/items/merchant/Attribute;",
        "Lsquareup/items/merchant/Attribute$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public definition_token:Ljava/lang/String;

.field public int_value:Ljava/lang/Long;

.field public string_value:Ljava/lang/String;

.field public unit_token:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 133
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 124
    invoke-virtual {p0}, Lsquareup/items/merchant/Attribute$Builder;->build()Lsquareup/items/merchant/Attribute;

    move-result-object v0

    return-object v0
.end method

.method public build()Lsquareup/items/merchant/Attribute;
    .locals 7

    .line 163
    new-instance v6, Lsquareup/items/merchant/Attribute;

    iget-object v1, p0, Lsquareup/items/merchant/Attribute$Builder;->definition_token:Ljava/lang/String;

    iget-object v2, p0, Lsquareup/items/merchant/Attribute$Builder;->unit_token:Ljava/lang/String;

    iget-object v3, p0, Lsquareup/items/merchant/Attribute$Builder;->int_value:Ljava/lang/Long;

    iget-object v4, p0, Lsquareup/items/merchant/Attribute$Builder;->string_value:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lsquareup/items/merchant/Attribute;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Lokio/ByteString;)V

    return-object v6
.end method

.method public definition_token(Ljava/lang/String;)Lsquareup/items/merchant/Attribute$Builder;
    .locals 0

    .line 140
    iput-object p1, p0, Lsquareup/items/merchant/Attribute$Builder;->definition_token:Ljava/lang/String;

    return-object p0
.end method

.method public int_value(Ljava/lang/Long;)Lsquareup/items/merchant/Attribute$Builder;
    .locals 0

    .line 150
    iput-object p1, p0, Lsquareup/items/merchant/Attribute$Builder;->int_value:Ljava/lang/Long;

    const/4 p1, 0x0

    .line 151
    iput-object p1, p0, Lsquareup/items/merchant/Attribute$Builder;->string_value:Ljava/lang/String;

    return-object p0
.end method

.method public string_value(Ljava/lang/String;)Lsquareup/items/merchant/Attribute$Builder;
    .locals 0

    .line 156
    iput-object p1, p0, Lsquareup/items/merchant/Attribute$Builder;->string_value:Ljava/lang/String;

    const/4 p1, 0x0

    .line 157
    iput-object p1, p0, Lsquareup/items/merchant/Attribute$Builder;->int_value:Ljava/lang/Long;

    return-object p0
.end method

.method public unit_token(Ljava/lang/String;)Lsquareup/items/merchant/Attribute$Builder;
    .locals 0

    .line 145
    iput-object p1, p0, Lsquareup/items/merchant/Attribute$Builder;->unit_token:Ljava/lang/String;

    return-object p0
.end method
