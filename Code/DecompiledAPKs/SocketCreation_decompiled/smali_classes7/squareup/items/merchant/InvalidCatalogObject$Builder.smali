.class public final Lsquareup/items/merchant/InvalidCatalogObject$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "InvalidCatalogObject.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/items/merchant/InvalidCatalogObject;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lsquareup/items/merchant/InvalidCatalogObject;",
        "Lsquareup/items/merchant/InvalidCatalogObject$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public catalog_object:Lsquareup/items/merchant/CatalogObject;

.field public data:Ljava/lang/String;

.field public message:Ljava/lang/String;

.field public reason:Lsquareup/items/merchant/InvalidCatalogObject$Reason;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 130
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 121
    invoke-virtual {p0}, Lsquareup/items/merchant/InvalidCatalogObject$Builder;->build()Lsquareup/items/merchant/InvalidCatalogObject;

    move-result-object v0

    return-object v0
.end method

.method public build()Lsquareup/items/merchant/InvalidCatalogObject;
    .locals 7

    .line 159
    new-instance v6, Lsquareup/items/merchant/InvalidCatalogObject;

    iget-object v1, p0, Lsquareup/items/merchant/InvalidCatalogObject$Builder;->catalog_object:Lsquareup/items/merchant/CatalogObject;

    iget-object v2, p0, Lsquareup/items/merchant/InvalidCatalogObject$Builder;->reason:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    iget-object v3, p0, Lsquareup/items/merchant/InvalidCatalogObject$Builder;->data:Ljava/lang/String;

    iget-object v4, p0, Lsquareup/items/merchant/InvalidCatalogObject$Builder;->message:Ljava/lang/String;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lsquareup/items/merchant/InvalidCatalogObject;-><init>(Lsquareup/items/merchant/CatalogObject;Lsquareup/items/merchant/InvalidCatalogObject$Reason;Ljava/lang/String;Ljava/lang/String;Lokio/ByteString;)V

    return-object v6
.end method

.method public catalog_object(Lsquareup/items/merchant/CatalogObject;)Lsquareup/items/merchant/InvalidCatalogObject$Builder;
    .locals 0

    .line 134
    iput-object p1, p0, Lsquareup/items/merchant/InvalidCatalogObject$Builder;->catalog_object:Lsquareup/items/merchant/CatalogObject;

    return-object p0
.end method

.method public data(Ljava/lang/String;)Lsquareup/items/merchant/InvalidCatalogObject$Builder;
    .locals 0

    .line 148
    iput-object p1, p0, Lsquareup/items/merchant/InvalidCatalogObject$Builder;->data:Ljava/lang/String;

    return-object p0
.end method

.method public message(Ljava/lang/String;)Lsquareup/items/merchant/InvalidCatalogObject$Builder;
    .locals 0

    .line 153
    iput-object p1, p0, Lsquareup/items/merchant/InvalidCatalogObject$Builder;->message:Ljava/lang/String;

    return-object p0
.end method

.method public reason(Lsquareup/items/merchant/InvalidCatalogObject$Reason;)Lsquareup/items/merchant/InvalidCatalogObject$Builder;
    .locals 0

    .line 139
    iput-object p1, p0, Lsquareup/items/merchant/InvalidCatalogObject$Builder;->reason:Lsquareup/items/merchant/InvalidCatalogObject$Reason;

    return-object p0
.end method
