.class public final Lsquareup/items/merchant/Response$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Response.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/items/merchant/Response;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lsquareup/items/merchant/Response;",
        "Lsquareup/items/merchant/Response$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public delete_response:Lsquareup/items/merchant/DeleteResponse;

.field public get_response:Lsquareup/items/merchant/GetResponse;

.field public put_response:Lsquareup/items/merchant/PutResponse;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 108
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 101
    invoke-virtual {p0}, Lsquareup/items/merchant/Response$Builder;->build()Lsquareup/items/merchant/Response;

    move-result-object v0

    return-object v0
.end method

.method public build()Lsquareup/items/merchant/Response;
    .locals 5

    .line 128
    new-instance v0, Lsquareup/items/merchant/Response;

    iget-object v1, p0, Lsquareup/items/merchant/Response$Builder;->get_response:Lsquareup/items/merchant/GetResponse;

    iget-object v2, p0, Lsquareup/items/merchant/Response$Builder;->put_response:Lsquareup/items/merchant/PutResponse;

    iget-object v3, p0, Lsquareup/items/merchant/Response$Builder;->delete_response:Lsquareup/items/merchant/DeleteResponse;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lsquareup/items/merchant/Response;-><init>(Lsquareup/items/merchant/GetResponse;Lsquareup/items/merchant/PutResponse;Lsquareup/items/merchant/DeleteResponse;Lokio/ByteString;)V

    return-object v0
.end method

.method public delete_response(Lsquareup/items/merchant/DeleteResponse;)Lsquareup/items/merchant/Response$Builder;
    .locals 0

    .line 122
    iput-object p1, p0, Lsquareup/items/merchant/Response$Builder;->delete_response:Lsquareup/items/merchant/DeleteResponse;

    return-object p0
.end method

.method public get_response(Lsquareup/items/merchant/GetResponse;)Lsquareup/items/merchant/Response$Builder;
    .locals 0

    .line 112
    iput-object p1, p0, Lsquareup/items/merchant/Response$Builder;->get_response:Lsquareup/items/merchant/GetResponse;

    return-object p0
.end method

.method public put_response(Lsquareup/items/merchant/PutResponse;)Lsquareup/items/merchant/Response$Builder;
    .locals 0

    .line 117
    iput-object p1, p0, Lsquareup/items/merchant/Response$Builder;->put_response:Lsquareup/items/merchant/PutResponse;

    return-object p0
.end method
