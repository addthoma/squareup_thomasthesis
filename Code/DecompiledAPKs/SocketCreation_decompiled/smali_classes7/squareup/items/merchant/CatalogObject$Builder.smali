.class public final Lsquareup/items/merchant/CatalogObject$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "CatalogObject.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/items/merchant/CatalogObject;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lsquareup/items/merchant/CatalogObject;",
        "Lsquareup/items/merchant/CatalogObject$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public attribute:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lsquareup/items/merchant/Attribute;",
            ">;"
        }
    .end annotation
.end field

.field public cogs_id:Ljava/lang/String;

.field public deleted:Ljava/lang/Boolean;

.field public token:Ljava/lang/String;

.field public type:Lsquareup/items/merchant/CatalogObjectType;

.field public version:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 171
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 172
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lsquareup/items/merchant/CatalogObject$Builder;->attribute:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public attribute(Ljava/util/List;)Lsquareup/items/merchant/CatalogObject$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lsquareup/items/merchant/Attribute;",
            ">;)",
            "Lsquareup/items/merchant/CatalogObject$Builder;"
        }
    .end annotation

    .line 186
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 187
    iput-object p1, p0, Lsquareup/items/merchant/CatalogObject$Builder;->attribute:Ljava/util/List;

    return-object p0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 158
    invoke-virtual {p0}, Lsquareup/items/merchant/CatalogObject$Builder;->build()Lsquareup/items/merchant/CatalogObject;

    move-result-object v0

    return-object v0
.end method

.method public build()Lsquareup/items/merchant/CatalogObject;
    .locals 9

    .line 221
    new-instance v8, Lsquareup/items/merchant/CatalogObject;

    iget-object v1, p0, Lsquareup/items/merchant/CatalogObject$Builder;->token:Ljava/lang/String;

    iget-object v2, p0, Lsquareup/items/merchant/CatalogObject$Builder;->type:Lsquareup/items/merchant/CatalogObjectType;

    iget-object v3, p0, Lsquareup/items/merchant/CatalogObject$Builder;->attribute:Ljava/util/List;

    iget-object v4, p0, Lsquareup/items/merchant/CatalogObject$Builder;->version:Ljava/lang/Long;

    iget-object v5, p0, Lsquareup/items/merchant/CatalogObject$Builder;->cogs_id:Ljava/lang/String;

    iget-object v6, p0, Lsquareup/items/merchant/CatalogObject$Builder;->deleted:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lsquareup/items/merchant/CatalogObject;-><init>(Ljava/lang/String;Lsquareup/items/merchant/CatalogObjectType;Ljava/util/List;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v8
.end method

.method public cogs_id(Ljava/lang/String;)Lsquareup/items/merchant/CatalogObject$Builder;
    .locals 0

    .line 206
    iput-object p1, p0, Lsquareup/items/merchant/CatalogObject$Builder;->cogs_id:Ljava/lang/String;

    return-object p0
.end method

.method public deleted(Ljava/lang/Boolean;)Lsquareup/items/merchant/CatalogObject$Builder;
    .locals 0

    .line 215
    iput-object p1, p0, Lsquareup/items/merchant/CatalogObject$Builder;->deleted:Ljava/lang/Boolean;

    return-object p0
.end method

.method public token(Ljava/lang/String;)Lsquareup/items/merchant/CatalogObject$Builder;
    .locals 0

    .line 176
    iput-object p1, p0, Lsquareup/items/merchant/CatalogObject$Builder;->token:Ljava/lang/String;

    return-object p0
.end method

.method public type(Lsquareup/items/merchant/CatalogObjectType;)Lsquareup/items/merchant/CatalogObject$Builder;
    .locals 0

    .line 181
    iput-object p1, p0, Lsquareup/items/merchant/CatalogObject$Builder;->type:Lsquareup/items/merchant/CatalogObjectType;

    return-object p0
.end method

.method public version(Ljava/lang/Long;)Lsquareup/items/merchant/CatalogObject$Builder;
    .locals 0

    .line 196
    iput-object p1, p0, Lsquareup/items/merchant/CatalogObject$Builder;->version:Ljava/lang/Long;

    return-object p0
.end method
