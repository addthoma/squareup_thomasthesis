.class public final Lsquareup/items/merchant/DeleteRequest;
.super Lcom/squareup/wire/Message;
.source "DeleteRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsquareup/items/merchant/DeleteRequest$ProtoAdapter_DeleteRequest;,
        Lsquareup/items/merchant/DeleteRequest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lsquareup/items/merchant/DeleteRequest;",
        "Lsquareup/items/merchant/DeleteRequest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lsquareup/items/merchant/DeleteRequest;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_LOCK_DURATION_MS:Ljava/lang/Long;

.field public static final DEFAULT_LOCK_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_MERCHANT_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_OPEN_TRANSACTION:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final catalog_object_token:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x2
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final lock_duration_ms:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x6
    .end annotation
.end field

.field public final lock_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field

.field public final merchant_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final open_transaction:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x4
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 23
    new-instance v0, Lsquareup/items/merchant/DeleteRequest$ProtoAdapter_DeleteRequest;

    invoke-direct {v0}, Lsquareup/items/merchant/DeleteRequest$ProtoAdapter_DeleteRequest;-><init>()V

    sput-object v0, Lsquareup/items/merchant/DeleteRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const/4 v0, 0x0

    .line 29
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lsquareup/items/merchant/DeleteRequest;->DEFAULT_OPEN_TRANSACTION:Ljava/lang/Boolean;

    const-wide/16 v0, 0x0

    .line 33
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lsquareup/items/merchant/DeleteRequest;->DEFAULT_LOCK_DURATION_MS:Ljava/lang/Long;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Long;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ")V"
        }
    .end annotation

    .line 88
    sget-object v6, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lsquareup/items/merchant/DeleteRequest;-><init>(Ljava/util/List;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Long;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Long;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 94
    sget-object v0, Lsquareup/items/merchant/DeleteRequest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p6}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    const-string p6, "catalog_object_token"

    .line 95
    invoke-static {p6, p1}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lsquareup/items/merchant/DeleteRequest;->catalog_object_token:Ljava/util/List;

    .line 96
    iput-object p2, p0, Lsquareup/items/merchant/DeleteRequest;->merchant_token:Ljava/lang/String;

    .line 97
    iput-object p3, p0, Lsquareup/items/merchant/DeleteRequest;->open_transaction:Ljava/lang/Boolean;

    .line 98
    iput-object p4, p0, Lsquareup/items/merchant/DeleteRequest;->lock_token:Ljava/lang/String;

    .line 99
    iput-object p5, p0, Lsquareup/items/merchant/DeleteRequest;->lock_duration_ms:Ljava/lang/Long;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 117
    :cond_0
    instance-of v1, p1, Lsquareup/items/merchant/DeleteRequest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 118
    :cond_1
    check-cast p1, Lsquareup/items/merchant/DeleteRequest;

    .line 119
    invoke-virtual {p0}, Lsquareup/items/merchant/DeleteRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lsquareup/items/merchant/DeleteRequest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/items/merchant/DeleteRequest;->catalog_object_token:Ljava/util/List;

    iget-object v3, p1, Lsquareup/items/merchant/DeleteRequest;->catalog_object_token:Ljava/util/List;

    .line 120
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/items/merchant/DeleteRequest;->merchant_token:Ljava/lang/String;

    iget-object v3, p1, Lsquareup/items/merchant/DeleteRequest;->merchant_token:Ljava/lang/String;

    .line 121
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/items/merchant/DeleteRequest;->open_transaction:Ljava/lang/Boolean;

    iget-object v3, p1, Lsquareup/items/merchant/DeleteRequest;->open_transaction:Ljava/lang/Boolean;

    .line 122
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/items/merchant/DeleteRequest;->lock_token:Ljava/lang/String;

    iget-object v3, p1, Lsquareup/items/merchant/DeleteRequest;->lock_token:Ljava/lang/String;

    .line 123
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/items/merchant/DeleteRequest;->lock_duration_ms:Ljava/lang/Long;

    iget-object p1, p1, Lsquareup/items/merchant/DeleteRequest;->lock_duration_ms:Ljava/lang/Long;

    .line 124
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 129
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_4

    .line 131
    invoke-virtual {p0}, Lsquareup/items/merchant/DeleteRequest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 132
    iget-object v1, p0, Lsquareup/items/merchant/DeleteRequest;->catalog_object_token:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 133
    iget-object v1, p0, Lsquareup/items/merchant/DeleteRequest;->merchant_token:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 134
    iget-object v1, p0, Lsquareup/items/merchant/DeleteRequest;->open_transaction:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 135
    iget-object v1, p0, Lsquareup/items/merchant/DeleteRequest;->lock_token:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 136
    iget-object v1, p0, Lsquareup/items/merchant/DeleteRequest;->lock_duration_ms:Ljava/lang/Long;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 137
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_4
    return v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 22
    invoke-virtual {p0}, Lsquareup/items/merchant/DeleteRequest;->newBuilder()Lsquareup/items/merchant/DeleteRequest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilder()Lsquareup/items/merchant/DeleteRequest$Builder;
    .locals 2

    .line 104
    new-instance v0, Lsquareup/items/merchant/DeleteRequest$Builder;

    invoke-direct {v0}, Lsquareup/items/merchant/DeleteRequest$Builder;-><init>()V

    .line 105
    iget-object v1, p0, Lsquareup/items/merchant/DeleteRequest;->catalog_object_token:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lsquareup/items/merchant/DeleteRequest$Builder;->catalog_object_token:Ljava/util/List;

    .line 106
    iget-object v1, p0, Lsquareup/items/merchant/DeleteRequest;->merchant_token:Ljava/lang/String;

    iput-object v1, v0, Lsquareup/items/merchant/DeleteRequest$Builder;->merchant_token:Ljava/lang/String;

    .line 107
    iget-object v1, p0, Lsquareup/items/merchant/DeleteRequest;->open_transaction:Ljava/lang/Boolean;

    iput-object v1, v0, Lsquareup/items/merchant/DeleteRequest$Builder;->open_transaction:Ljava/lang/Boolean;

    .line 108
    iget-object v1, p0, Lsquareup/items/merchant/DeleteRequest;->lock_token:Ljava/lang/String;

    iput-object v1, v0, Lsquareup/items/merchant/DeleteRequest$Builder;->lock_token:Ljava/lang/String;

    .line 109
    iget-object v1, p0, Lsquareup/items/merchant/DeleteRequest;->lock_duration_ms:Ljava/lang/Long;

    iput-object v1, v0, Lsquareup/items/merchant/DeleteRequest$Builder;->lock_duration_ms:Ljava/lang/Long;

    .line 110
    invoke-virtual {p0}, Lsquareup/items/merchant/DeleteRequest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lsquareup/items/merchant/DeleteRequest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 144
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 145
    iget-object v1, p0, Lsquareup/items/merchant/DeleteRequest;->catalog_object_token:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, ", catalog_object_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/items/merchant/DeleteRequest;->catalog_object_token:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 146
    :cond_0
    iget-object v1, p0, Lsquareup/items/merchant/DeleteRequest;->merchant_token:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", merchant_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/items/merchant/DeleteRequest;->merchant_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 147
    :cond_1
    iget-object v1, p0, Lsquareup/items/merchant/DeleteRequest;->open_transaction:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", open_transaction="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/items/merchant/DeleteRequest;->open_transaction:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 148
    :cond_2
    iget-object v1, p0, Lsquareup/items/merchant/DeleteRequest;->lock_token:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", lock_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/items/merchant/DeleteRequest;->lock_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 149
    :cond_3
    iget-object v1, p0, Lsquareup/items/merchant/DeleteRequest;->lock_duration_ms:Ljava/lang/Long;

    if-eqz v1, :cond_4

    const-string v1, ", lock_duration_ms="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/items/merchant/DeleteRequest;->lock_duration_ms:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_4
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "DeleteRequest{"

    .line 150
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
