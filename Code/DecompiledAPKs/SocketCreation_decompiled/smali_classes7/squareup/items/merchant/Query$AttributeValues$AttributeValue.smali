.class public final Lsquareup/items/merchant/Query$AttributeValues$AttributeValue;
.super Lcom/squareup/wire/Message;
.source "Query.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/items/merchant/Query$AttributeValues;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AttributeValue"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsquareup/items/merchant/Query$AttributeValues$AttributeValue$ProtoAdapter_AttributeValue;,
        Lsquareup/items/merchant/Query$AttributeValues$AttributeValue$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lsquareup/items/merchant/Query$AttributeValues$AttributeValue;",
        "Lsquareup/items/merchant/Query$AttributeValues$AttributeValue$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lsquareup/items/merchant/Query$AttributeValues$AttributeValue;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_INT_VALUE:Ljava/lang/Long;

.field public static final DEFAULT_STRING_VALUE:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final attribute_definition_tokens:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x1
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final int_value:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#INT64"
        tag = 0x2
    .end annotation
.end field

.field public final string_value:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 833
    new-instance v0, Lsquareup/items/merchant/Query$AttributeValues$AttributeValue$ProtoAdapter_AttributeValue;

    invoke-direct {v0}, Lsquareup/items/merchant/Query$AttributeValues$AttributeValue$ProtoAdapter_AttributeValue;-><init>()V

    sput-object v0, Lsquareup/items/merchant/Query$AttributeValues$AttributeValue;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    const-wide/16 v0, 0x0

    .line 837
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lsquareup/items/merchant/Query$AttributeValues$AttributeValue;->DEFAULT_INT_VALUE:Ljava/lang/Long;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/lang/Long;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 871
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lsquareup/items/merchant/Query$AttributeValues$AttributeValue;-><init>(Ljava/util/List;Ljava/lang/Long;Ljava/lang/String;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/lang/Long;Ljava/lang/String;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 876
    sget-object v0, Lsquareup/items/merchant/Query$AttributeValues$AttributeValue;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 877
    invoke-static {p2, p3}, Lcom/squareup/wire/internal/Internal;->countNonNull(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result p4

    const/4 v0, 0x1

    if-gt p4, v0, :cond_0

    const-string p4, "attribute_definition_tokens"

    .line 880
    invoke-static {p4, p1}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lsquareup/items/merchant/Query$AttributeValues$AttributeValue;->attribute_definition_tokens:Ljava/util/List;

    .line 881
    iput-object p2, p0, Lsquareup/items/merchant/Query$AttributeValues$AttributeValue;->int_value:Ljava/lang/Long;

    .line 882
    iput-object p3, p0, Lsquareup/items/merchant/Query$AttributeValues$AttributeValue;->string_value:Ljava/lang/String;

    return-void

    .line 878
    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "at most one of int_value, string_value may be non-null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 898
    :cond_0
    instance-of v1, p1, Lsquareup/items/merchant/Query$AttributeValues$AttributeValue;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 899
    :cond_1
    check-cast p1, Lsquareup/items/merchant/Query$AttributeValues$AttributeValue;

    .line 900
    invoke-virtual {p0}, Lsquareup/items/merchant/Query$AttributeValues$AttributeValue;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lsquareup/items/merchant/Query$AttributeValues$AttributeValue;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/items/merchant/Query$AttributeValues$AttributeValue;->attribute_definition_tokens:Ljava/util/List;

    iget-object v3, p1, Lsquareup/items/merchant/Query$AttributeValues$AttributeValue;->attribute_definition_tokens:Ljava/util/List;

    .line 901
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/items/merchant/Query$AttributeValues$AttributeValue;->int_value:Ljava/lang/Long;

    iget-object v3, p1, Lsquareup/items/merchant/Query$AttributeValues$AttributeValue;->int_value:Ljava/lang/Long;

    .line 902
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/items/merchant/Query$AttributeValues$AttributeValue;->string_value:Ljava/lang/String;

    iget-object p1, p1, Lsquareup/items/merchant/Query$AttributeValues$AttributeValue;->string_value:Ljava/lang/String;

    .line 903
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 908
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_2

    .line 910
    invoke-virtual {p0}, Lsquareup/items/merchant/Query$AttributeValues$AttributeValue;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 911
    iget-object v1, p0, Lsquareup/items/merchant/Query$AttributeValues$AttributeValue;->attribute_definition_tokens:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 912
    iget-object v1, p0, Lsquareup/items/merchant/Query$AttributeValues$AttributeValue;->int_value:Ljava/lang/Long;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 913
    iget-object v1, p0, Lsquareup/items/merchant/Query$AttributeValues$AttributeValue;->string_value:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 914
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_2
    return v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 832
    invoke-virtual {p0}, Lsquareup/items/merchant/Query$AttributeValues$AttributeValue;->newBuilder()Lsquareup/items/merchant/Query$AttributeValues$AttributeValue$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilder()Lsquareup/items/merchant/Query$AttributeValues$AttributeValue$Builder;
    .locals 2

    .line 887
    new-instance v0, Lsquareup/items/merchant/Query$AttributeValues$AttributeValue$Builder;

    invoke-direct {v0}, Lsquareup/items/merchant/Query$AttributeValues$AttributeValue$Builder;-><init>()V

    .line 888
    iget-object v1, p0, Lsquareup/items/merchant/Query$AttributeValues$AttributeValue;->attribute_definition_tokens:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lsquareup/items/merchant/Query$AttributeValues$AttributeValue$Builder;->attribute_definition_tokens:Ljava/util/List;

    .line 889
    iget-object v1, p0, Lsquareup/items/merchant/Query$AttributeValues$AttributeValue;->int_value:Ljava/lang/Long;

    iput-object v1, v0, Lsquareup/items/merchant/Query$AttributeValues$AttributeValue$Builder;->int_value:Ljava/lang/Long;

    .line 890
    iget-object v1, p0, Lsquareup/items/merchant/Query$AttributeValues$AttributeValue;->string_value:Ljava/lang/String;

    iput-object v1, v0, Lsquareup/items/merchant/Query$AttributeValues$AttributeValue$Builder;->string_value:Ljava/lang/String;

    .line 891
    invoke-virtual {p0}, Lsquareup/items/merchant/Query$AttributeValues$AttributeValue;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lsquareup/items/merchant/Query$AttributeValues$AttributeValue$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 921
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 922
    iget-object v1, p0, Lsquareup/items/merchant/Query$AttributeValues$AttributeValue;->attribute_definition_tokens:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, ", attribute_definition_tokens="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/items/merchant/Query$AttributeValues$AttributeValue;->attribute_definition_tokens:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 923
    :cond_0
    iget-object v1, p0, Lsquareup/items/merchant/Query$AttributeValues$AttributeValue;->int_value:Ljava/lang/Long;

    if-eqz v1, :cond_1

    const-string v1, ", int_value="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/items/merchant/Query$AttributeValues$AttributeValue;->int_value:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 924
    :cond_1
    iget-object v1, p0, Lsquareup/items/merchant/Query$AttributeValues$AttributeValue;->string_value:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", string_value="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/items/merchant/Query$AttributeValues$AttributeValue;->string_value:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "AttributeValue{"

    .line 925
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
