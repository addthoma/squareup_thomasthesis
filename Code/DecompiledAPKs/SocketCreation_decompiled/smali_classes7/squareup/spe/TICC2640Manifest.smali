.class public final Lsquareup/spe/TICC2640Manifest;
.super Lcom/squareup/wire/Message;
.source "TICC2640Manifest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsquareup/spe/TICC2640Manifest$ProtoAdapter_TICC2640Manifest;,
        Lsquareup/spe/TICC2640Manifest$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message<",
        "Lsquareup/spe/TICC2640Manifest;",
        "Lsquareup/spe/TICC2640Manifest$Builder;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lsquareup/spe/TICC2640Manifest;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final firmware:Lsquareup/spe/AssetManifest;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "squareup.spe.AssetManifest#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 20
    new-instance v0, Lsquareup/spe/TICC2640Manifest$ProtoAdapter_TICC2640Manifest;

    invoke-direct {v0}, Lsquareup/spe/TICC2640Manifest$ProtoAdapter_TICC2640Manifest;-><init>()V

    sput-object v0, Lsquareup/spe/TICC2640Manifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method public constructor <init>(Lsquareup/spe/AssetManifest;)V
    .locals 1

    .line 31
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, v0}, Lsquareup/spe/TICC2640Manifest;-><init>(Lsquareup/spe/AssetManifest;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lsquareup/spe/AssetManifest;Lokio/ByteString;)V
    .locals 1

    .line 35
    sget-object v0, Lsquareup/spe/TICC2640Manifest;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/Message;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 36
    iput-object p1, p0, Lsquareup/spe/TICC2640Manifest;->firmware:Lsquareup/spe/AssetManifest;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 50
    :cond_0
    instance-of v1, p1, Lsquareup/spe/TICC2640Manifest;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 51
    :cond_1
    check-cast p1, Lsquareup/spe/TICC2640Manifest;

    .line 52
    invoke-virtual {p0}, Lsquareup/spe/TICC2640Manifest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lsquareup/spe/TICC2640Manifest;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lsquareup/spe/TICC2640Manifest;->firmware:Lsquareup/spe/AssetManifest;

    iget-object p1, p1, Lsquareup/spe/TICC2640Manifest;->firmware:Lsquareup/spe/AssetManifest;

    .line 53
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 58
    iget v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    if-nez v0, :cond_1

    .line 60
    invoke-virtual {p0}, Lsquareup/spe/TICC2640Manifest;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 61
    iget-object v1, p0, Lsquareup/spe/TICC2640Manifest;->firmware:Lsquareup/spe/AssetManifest;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lsquareup/spe/AssetManifest;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 62
    iput v0, p0, Lcom/squareup/wire/Message;->hashCode:I

    :cond_1
    return v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 19
    invoke-virtual {p0}, Lsquareup/spe/TICC2640Manifest;->newBuilder()Lsquareup/spe/TICC2640Manifest$Builder;

    move-result-object v0

    return-object v0
.end method

.method public newBuilder()Lsquareup/spe/TICC2640Manifest$Builder;
    .locals 2

    .line 41
    new-instance v0, Lsquareup/spe/TICC2640Manifest$Builder;

    invoke-direct {v0}, Lsquareup/spe/TICC2640Manifest$Builder;-><init>()V

    .line 42
    iget-object v1, p0, Lsquareup/spe/TICC2640Manifest;->firmware:Lsquareup/spe/AssetManifest;

    iput-object v1, v0, Lsquareup/spe/TICC2640Manifest$Builder;->firmware:Lsquareup/spe/AssetManifest;

    .line 43
    invoke-virtual {p0}, Lsquareup/spe/TICC2640Manifest;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lsquareup/spe/TICC2640Manifest$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 69
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 70
    iget-object v1, p0, Lsquareup/spe/TICC2640Manifest;->firmware:Lsquareup/spe/AssetManifest;

    if-eqz v1, :cond_0

    const-string v1, ", firmware="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsquareup/spe/TICC2640Manifest;->firmware:Lsquareup/spe/AssetManifest;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "TICC2640Manifest{"

    .line 71
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
