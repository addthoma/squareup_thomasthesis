.class public final Lsquareup/spe/K21Manifest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "K21Manifest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/spe/K21Manifest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lsquareup/spe/K21Manifest;",
        "Lsquareup/spe/K21Manifest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public bootloader:Lsquareup/spe/AssetManifest;

.field public chipid:Lokio/ByteString;

.field public configuration:Lsquareup/spe/UnitConfiguration;

.field public fw_a:Lsquareup/spe/AssetManifest;

.field public fw_b:Lsquareup/spe/AssetManifest;

.field public running_slot:Lsquareup/spe/AssetTypeV2;

.field public signer_fingerprint:Lokio/ByteString;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 167
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public bootloader(Lsquareup/spe/AssetManifest;)Lsquareup/spe/K21Manifest$Builder;
    .locals 0

    .line 186
    iput-object p1, p0, Lsquareup/spe/K21Manifest$Builder;->bootloader:Lsquareup/spe/AssetManifest;

    return-object p0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 152
    invoke-virtual {p0}, Lsquareup/spe/K21Manifest$Builder;->build()Lsquareup/spe/K21Manifest;

    move-result-object v0

    return-object v0
.end method

.method public build()Lsquareup/spe/K21Manifest;
    .locals 10

    .line 207
    new-instance v9, Lsquareup/spe/K21Manifest;

    iget-object v1, p0, Lsquareup/spe/K21Manifest$Builder;->running_slot:Lsquareup/spe/AssetTypeV2;

    iget-object v2, p0, Lsquareup/spe/K21Manifest$Builder;->chipid:Lokio/ByteString;

    iget-object v3, p0, Lsquareup/spe/K21Manifest$Builder;->signer_fingerprint:Lokio/ByteString;

    iget-object v4, p0, Lsquareup/spe/K21Manifest$Builder;->bootloader:Lsquareup/spe/AssetManifest;

    iget-object v5, p0, Lsquareup/spe/K21Manifest$Builder;->fw_a:Lsquareup/spe/AssetManifest;

    iget-object v6, p0, Lsquareup/spe/K21Manifest$Builder;->fw_b:Lsquareup/spe/AssetManifest;

    iget-object v7, p0, Lsquareup/spe/K21Manifest$Builder;->configuration:Lsquareup/spe/UnitConfiguration;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v8

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lsquareup/spe/K21Manifest;-><init>(Lsquareup/spe/AssetTypeV2;Lokio/ByteString;Lokio/ByteString;Lsquareup/spe/AssetManifest;Lsquareup/spe/AssetManifest;Lsquareup/spe/AssetManifest;Lsquareup/spe/UnitConfiguration;Lokio/ByteString;)V

    return-object v9
.end method

.method public chipid(Lokio/ByteString;)Lsquareup/spe/K21Manifest$Builder;
    .locals 0

    .line 176
    iput-object p1, p0, Lsquareup/spe/K21Manifest$Builder;->chipid:Lokio/ByteString;

    return-object p0
.end method

.method public configuration(Lsquareup/spe/UnitConfiguration;)Lsquareup/spe/K21Manifest$Builder;
    .locals 0

    .line 201
    iput-object p1, p0, Lsquareup/spe/K21Manifest$Builder;->configuration:Lsquareup/spe/UnitConfiguration;

    return-object p0
.end method

.method public fw_a(Lsquareup/spe/AssetManifest;)Lsquareup/spe/K21Manifest$Builder;
    .locals 0

    .line 191
    iput-object p1, p0, Lsquareup/spe/K21Manifest$Builder;->fw_a:Lsquareup/spe/AssetManifest;

    return-object p0
.end method

.method public fw_b(Lsquareup/spe/AssetManifest;)Lsquareup/spe/K21Manifest$Builder;
    .locals 0

    .line 196
    iput-object p1, p0, Lsquareup/spe/K21Manifest$Builder;->fw_b:Lsquareup/spe/AssetManifest;

    return-object p0
.end method

.method public running_slot(Lsquareup/spe/AssetTypeV2;)Lsquareup/spe/K21Manifest$Builder;
    .locals 0

    .line 171
    iput-object p1, p0, Lsquareup/spe/K21Manifest$Builder;->running_slot:Lsquareup/spe/AssetTypeV2;

    return-object p0
.end method

.method public signer_fingerprint(Lokio/ByteString;)Lsquareup/spe/K21Manifest$Builder;
    .locals 0

    .line 181
    iput-object p1, p0, Lsquareup/spe/K21Manifest$Builder;->signer_fingerprint:Lokio/ByteString;

    return-object p0
.end method
