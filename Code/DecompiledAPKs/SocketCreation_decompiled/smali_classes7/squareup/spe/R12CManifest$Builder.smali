.class public final Lsquareup/spe/R12CManifest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "R12CManifest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/spe/R12CManifest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lsquareup/spe/R12CManifest;",
        "Lsquareup/spe/R12CManifest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public ble_manifest:Lsquareup/spe/TICC2640Manifest;

.field public comms_protocol_version:Lsquareup/spe/CommsProtocolVersion;

.field public crq_gt4_manifest:Lsquareup/spe/CRQGT4Manifest;

.field public k450_manifest:Lsquareup/spe/K450Manifest;

.field public max_compression_version:Ljava/lang/Integer;

.field public pts_version:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 151
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public ble_manifest(Lsquareup/spe/TICC2640Manifest;)Lsquareup/spe/R12CManifest$Builder;
    .locals 0

    .line 165
    iput-object p1, p0, Lsquareup/spe/R12CManifest$Builder;->ble_manifest:Lsquareup/spe/TICC2640Manifest;

    return-object p0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 138
    invoke-virtual {p0}, Lsquareup/spe/R12CManifest$Builder;->build()Lsquareup/spe/R12CManifest;

    move-result-object v0

    return-object v0
.end method

.method public build()Lsquareup/spe/R12CManifest;
    .locals 9

    .line 186
    new-instance v8, Lsquareup/spe/R12CManifest;

    iget-object v1, p0, Lsquareup/spe/R12CManifest$Builder;->comms_protocol_version:Lsquareup/spe/CommsProtocolVersion;

    iget-object v2, p0, Lsquareup/spe/R12CManifest$Builder;->k450_manifest:Lsquareup/spe/K450Manifest;

    iget-object v3, p0, Lsquareup/spe/R12CManifest$Builder;->ble_manifest:Lsquareup/spe/TICC2640Manifest;

    iget-object v4, p0, Lsquareup/spe/R12CManifest$Builder;->max_compression_version:Ljava/lang/Integer;

    iget-object v5, p0, Lsquareup/spe/R12CManifest$Builder;->pts_version:Ljava/lang/Integer;

    iget-object v6, p0, Lsquareup/spe/R12CManifest$Builder;->crq_gt4_manifest:Lsquareup/spe/CRQGT4Manifest;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v7

    move-object v0, v8

    invoke-direct/range {v0 .. v7}, Lsquareup/spe/R12CManifest;-><init>(Lsquareup/spe/CommsProtocolVersion;Lsquareup/spe/K450Manifest;Lsquareup/spe/TICC2640Manifest;Ljava/lang/Integer;Ljava/lang/Integer;Lsquareup/spe/CRQGT4Manifest;Lokio/ByteString;)V

    return-object v8
.end method

.method public comms_protocol_version(Lsquareup/spe/CommsProtocolVersion;)Lsquareup/spe/R12CManifest$Builder;
    .locals 0

    .line 155
    iput-object p1, p0, Lsquareup/spe/R12CManifest$Builder;->comms_protocol_version:Lsquareup/spe/CommsProtocolVersion;

    return-object p0
.end method

.method public crq_gt4_manifest(Lsquareup/spe/CRQGT4Manifest;)Lsquareup/spe/R12CManifest$Builder;
    .locals 0

    .line 180
    iput-object p1, p0, Lsquareup/spe/R12CManifest$Builder;->crq_gt4_manifest:Lsquareup/spe/CRQGT4Manifest;

    return-object p0
.end method

.method public k450_manifest(Lsquareup/spe/K450Manifest;)Lsquareup/spe/R12CManifest$Builder;
    .locals 0

    .line 160
    iput-object p1, p0, Lsquareup/spe/R12CManifest$Builder;->k450_manifest:Lsquareup/spe/K450Manifest;

    return-object p0
.end method

.method public max_compression_version(Ljava/lang/Integer;)Lsquareup/spe/R12CManifest$Builder;
    .locals 0

    .line 170
    iput-object p1, p0, Lsquareup/spe/R12CManifest$Builder;->max_compression_version:Ljava/lang/Integer;

    return-object p0
.end method

.method public pts_version(Ljava/lang/Integer;)Lsquareup/spe/R12CManifest$Builder;
    .locals 0

    .line 175
    iput-object p1, p0, Lsquareup/spe/R12CManifest$Builder;->pts_version:Ljava/lang/Integer;

    return-object p0
.end method
