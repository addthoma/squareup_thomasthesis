.class public final enum Lsquareup/spe/UnitConfiguration;
.super Ljava/lang/Enum;
.source "UnitConfiguration.java"

# interfaces
.implements Lcom/squareup/wire/WireEnum;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsquareup/spe/UnitConfiguration$ProtoAdapter_UnitConfiguration;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lsquareup/spe/UnitConfiguration;",
        ">;",
        "Lcom/squareup/wire/WireEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lsquareup/spe/UnitConfiguration;

.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lsquareup/spe/UnitConfiguration;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum CONFIG_DEBUG:Lsquareup/spe/UnitConfiguration;

.field public static final enum CONFIG_PRODUCTION:Lsquareup/spe/UnitConfiguration;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 11
    new-instance v0, Lsquareup/spe/UnitConfiguration;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const-string v3, "CONFIG_PRODUCTION"

    invoke-direct {v0, v3, v1, v2}, Lsquareup/spe/UnitConfiguration;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/spe/UnitConfiguration;->CONFIG_PRODUCTION:Lsquareup/spe/UnitConfiguration;

    .line 13
    new-instance v0, Lsquareup/spe/UnitConfiguration;

    const/4 v3, 0x2

    const-string v4, "CONFIG_DEBUG"

    invoke-direct {v0, v4, v2, v3}, Lsquareup/spe/UnitConfiguration;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lsquareup/spe/UnitConfiguration;->CONFIG_DEBUG:Lsquareup/spe/UnitConfiguration;

    new-array v0, v3, [Lsquareup/spe/UnitConfiguration;

    .line 10
    sget-object v3, Lsquareup/spe/UnitConfiguration;->CONFIG_PRODUCTION:Lsquareup/spe/UnitConfiguration;

    aput-object v3, v0, v1

    sget-object v1, Lsquareup/spe/UnitConfiguration;->CONFIG_DEBUG:Lsquareup/spe/UnitConfiguration;

    aput-object v1, v0, v2

    sput-object v0, Lsquareup/spe/UnitConfiguration;->$VALUES:[Lsquareup/spe/UnitConfiguration;

    .line 15
    new-instance v0, Lsquareup/spe/UnitConfiguration$ProtoAdapter_UnitConfiguration;

    invoke-direct {v0}, Lsquareup/spe/UnitConfiguration$ProtoAdapter_UnitConfiguration;-><init>()V

    sput-object v0, Lsquareup/spe/UnitConfiguration;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 20
    iput p3, p0, Lsquareup/spe/UnitConfiguration;->value:I

    return-void
.end method

.method public static fromValue(I)Lsquareup/spe/UnitConfiguration;
    .locals 1

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return-object p0

    .line 29
    :cond_0
    sget-object p0, Lsquareup/spe/UnitConfiguration;->CONFIG_DEBUG:Lsquareup/spe/UnitConfiguration;

    return-object p0

    .line 28
    :cond_1
    sget-object p0, Lsquareup/spe/UnitConfiguration;->CONFIG_PRODUCTION:Lsquareup/spe/UnitConfiguration;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lsquareup/spe/UnitConfiguration;
    .locals 1

    .line 10
    const-class v0, Lsquareup/spe/UnitConfiguration;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lsquareup/spe/UnitConfiguration;

    return-object p0
.end method

.method public static values()[Lsquareup/spe/UnitConfiguration;
    .locals 1

    .line 10
    sget-object v0, Lsquareup/spe/UnitConfiguration;->$VALUES:[Lsquareup/spe/UnitConfiguration;

    invoke-virtual {v0}, [Lsquareup/spe/UnitConfiguration;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lsquareup/spe/UnitConfiguration;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 36
    iget v0, p0, Lsquareup/spe/UnitConfiguration;->value:I

    return v0
.end method
