.class public final Lsquareup/spe/K400Manifest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "K400Manifest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsquareup/spe/K400Manifest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lsquareup/spe/K400Manifest;",
        "Lsquareup/spe/K400Manifest$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public configuration:Lsquareup/spe/UnitConfiguration;

.field public cpu0_bootloader:Lsquareup/spe/AssetManifest;

.field public cpu0_firmware:Lsquareup/spe/AssetManifest;

.field public cpu1_bootloader:Lsquareup/spe/AssetManifest;

.field public cpu1_firmware:Lsquareup/spe/AssetManifest;

.field public fpga:Lsquareup/spe/AssetManifest;

.field public jtag_fuses:Lokio/ByteString;

.field public signer_fingerprint:Lokio/ByteString;

.field public tms_capk:Lsquareup/spe/AssetManifest;

.field public tms_capk_struct_version:Ljava/lang/Integer;

.field public tms_capk_struct_version_supported_by_cpu0:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 226
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 203
    invoke-virtual {p0}, Lsquareup/spe/K400Manifest$Builder;->build()Lsquareup/spe/K400Manifest;

    move-result-object v0

    return-object v0
.end method

.method public build()Lsquareup/spe/K400Manifest;
    .locals 14

    .line 287
    new-instance v13, Lsquareup/spe/K400Manifest;

    iget-object v1, p0, Lsquareup/spe/K400Manifest$Builder;->cpu0_bootloader:Lsquareup/spe/AssetManifest;

    iget-object v2, p0, Lsquareup/spe/K400Manifest$Builder;->cpu1_bootloader:Lsquareup/spe/AssetManifest;

    iget-object v3, p0, Lsquareup/spe/K400Manifest$Builder;->cpu0_firmware:Lsquareup/spe/AssetManifest;

    iget-object v4, p0, Lsquareup/spe/K400Manifest$Builder;->cpu1_firmware:Lsquareup/spe/AssetManifest;

    iget-object v5, p0, Lsquareup/spe/K400Manifest$Builder;->tms_capk:Lsquareup/spe/AssetManifest;

    iget-object v6, p0, Lsquareup/spe/K400Manifest$Builder;->fpga:Lsquareup/spe/AssetManifest;

    iget-object v7, p0, Lsquareup/spe/K400Manifest$Builder;->signer_fingerprint:Lokio/ByteString;

    iget-object v8, p0, Lsquareup/spe/K400Manifest$Builder;->configuration:Lsquareup/spe/UnitConfiguration;

    iget-object v9, p0, Lsquareup/spe/K400Manifest$Builder;->jtag_fuses:Lokio/ByteString;

    iget-object v10, p0, Lsquareup/spe/K400Manifest$Builder;->tms_capk_struct_version:Ljava/lang/Integer;

    iget-object v11, p0, Lsquareup/spe/K400Manifest$Builder;->tms_capk_struct_version_supported_by_cpu0:Ljava/lang/Integer;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v12

    move-object v0, v13

    invoke-direct/range {v0 .. v12}, Lsquareup/spe/K400Manifest;-><init>(Lsquareup/spe/AssetManifest;Lsquareup/spe/AssetManifest;Lsquareup/spe/AssetManifest;Lsquareup/spe/AssetManifest;Lsquareup/spe/AssetManifest;Lsquareup/spe/AssetManifest;Lokio/ByteString;Lsquareup/spe/UnitConfiguration;Lokio/ByteString;Ljava/lang/Integer;Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v13
.end method

.method public configuration(Lsquareup/spe/UnitConfiguration;)Lsquareup/spe/K400Manifest$Builder;
    .locals 0

    .line 265
    iput-object p1, p0, Lsquareup/spe/K400Manifest$Builder;->configuration:Lsquareup/spe/UnitConfiguration;

    return-object p0
.end method

.method public cpu0_bootloader(Lsquareup/spe/AssetManifest;)Lsquareup/spe/K400Manifest$Builder;
    .locals 0

    .line 230
    iput-object p1, p0, Lsquareup/spe/K400Manifest$Builder;->cpu0_bootloader:Lsquareup/spe/AssetManifest;

    return-object p0
.end method

.method public cpu0_firmware(Lsquareup/spe/AssetManifest;)Lsquareup/spe/K400Manifest$Builder;
    .locals 0

    .line 240
    iput-object p1, p0, Lsquareup/spe/K400Manifest$Builder;->cpu0_firmware:Lsquareup/spe/AssetManifest;

    return-object p0
.end method

.method public cpu1_bootloader(Lsquareup/spe/AssetManifest;)Lsquareup/spe/K400Manifest$Builder;
    .locals 0

    .line 235
    iput-object p1, p0, Lsquareup/spe/K400Manifest$Builder;->cpu1_bootloader:Lsquareup/spe/AssetManifest;

    return-object p0
.end method

.method public cpu1_firmware(Lsquareup/spe/AssetManifest;)Lsquareup/spe/K400Manifest$Builder;
    .locals 0

    .line 245
    iput-object p1, p0, Lsquareup/spe/K400Manifest$Builder;->cpu1_firmware:Lsquareup/spe/AssetManifest;

    return-object p0
.end method

.method public fpga(Lsquareup/spe/AssetManifest;)Lsquareup/spe/K400Manifest$Builder;
    .locals 0

    .line 255
    iput-object p1, p0, Lsquareup/spe/K400Manifest$Builder;->fpga:Lsquareup/spe/AssetManifest;

    return-object p0
.end method

.method public jtag_fuses(Lokio/ByteString;)Lsquareup/spe/K400Manifest$Builder;
    .locals 0

    .line 270
    iput-object p1, p0, Lsquareup/spe/K400Manifest$Builder;->jtag_fuses:Lokio/ByteString;

    return-object p0
.end method

.method public signer_fingerprint(Lokio/ByteString;)Lsquareup/spe/K400Manifest$Builder;
    .locals 0

    .line 260
    iput-object p1, p0, Lsquareup/spe/K400Manifest$Builder;->signer_fingerprint:Lokio/ByteString;

    return-object p0
.end method

.method public tms_capk(Lsquareup/spe/AssetManifest;)Lsquareup/spe/K400Manifest$Builder;
    .locals 0

    .line 250
    iput-object p1, p0, Lsquareup/spe/K400Manifest$Builder;->tms_capk:Lsquareup/spe/AssetManifest;

    return-object p0
.end method

.method public tms_capk_struct_version(Ljava/lang/Integer;)Lsquareup/spe/K400Manifest$Builder;
    .locals 0

    .line 275
    iput-object p1, p0, Lsquareup/spe/K400Manifest$Builder;->tms_capk_struct_version:Ljava/lang/Integer;

    return-object p0
.end method

.method public tms_capk_struct_version_supported_by_cpu0(Ljava/lang/Integer;)Lsquareup/spe/K400Manifest$Builder;
    .locals 0

    .line 281
    iput-object p1, p0, Lsquareup/spe/K400Manifest$Builder;->tms_capk_struct_version_supported_by_cpu0:Ljava/lang/Integer;

    return-object p0
.end method
