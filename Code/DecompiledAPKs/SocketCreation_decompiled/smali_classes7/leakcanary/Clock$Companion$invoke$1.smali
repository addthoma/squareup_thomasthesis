.class public final Lleakcanary/Clock$Companion$invoke$1;
.super Ljava/lang/Object;
.source "Clock.kt"

# interfaces
.implements Lleakcanary/Clock;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lleakcanary/Clock$Companion;->invoke(Lkotlin/jvm/functions/Function0;)Lleakcanary/Clock;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nClock.kt\nKotlin\n*S Kotlin\n*F\n+ 1 Clock.kt\nleakcanary/Clock$Companion$invoke$1\n*L\n1#1,33:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0011\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H\u0016\u00a8\u0006\u0004"
    }
    d2 = {
        "leakcanary/Clock$Companion$invoke$1",
        "Lleakcanary/Clock;",
        "uptimeMillis",
        "",
        "leakcanary-object-watcher"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $block:Lkotlin/jvm/functions/Function0;


# direct methods
.method public constructor <init>(Lkotlin/jvm/functions/Function0;)V
    .locals 0

    .line 28
    iput-object p1, p0, Lleakcanary/Clock$Companion$invoke$1;->$block:Lkotlin/jvm/functions/Function0;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public uptimeMillis()J
    .locals 2

    .line 29
    iget-object v0, p0, Lleakcanary/Clock$Companion$invoke$1;->$block:Lkotlin/jvm/functions/Function0;

    invoke-interface {v0}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    move-result-wide v0

    return-wide v0
.end method
