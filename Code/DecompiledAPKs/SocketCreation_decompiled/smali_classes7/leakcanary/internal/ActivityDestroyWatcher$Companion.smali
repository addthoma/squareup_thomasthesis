.class public final Lleakcanary/internal/ActivityDestroyWatcher$Companion;
.super Ljava/lang/Object;
.source "ActivityDestroyWatcher.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lleakcanary/internal/ActivityDestroyWatcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J$\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00082\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n\u00a8\u0006\u000c"
    }
    d2 = {
        "Lleakcanary/internal/ActivityDestroyWatcher$Companion;",
        "",
        "()V",
        "install",
        "",
        "application",
        "Landroid/app/Application;",
        "objectWatcher",
        "Lleakcanary/ObjectWatcher;",
        "configProvider",
        "Lkotlin/Function0;",
        "Lleakcanary/AppWatcher$Config;",
        "leakcanary-object-watcher-android_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 40
    invoke-direct {p0}, Lleakcanary/internal/ActivityDestroyWatcher$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final install(Landroid/app/Application;Lleakcanary/ObjectWatcher;Lkotlin/jvm/functions/Function0;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Application;",
            "Lleakcanary/ObjectWatcher;",
            "Lkotlin/jvm/functions/Function0<",
            "Lleakcanary/AppWatcher$Config;",
            ">;)V"
        }
    .end annotation

    const-string v0, "application"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "objectWatcher"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "configProvider"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    new-instance v0, Lleakcanary/internal/ActivityDestroyWatcher;

    const/4 v1, 0x0

    invoke-direct {v0, p2, p3, v1}, Lleakcanary/internal/ActivityDestroyWatcher;-><init>(Lleakcanary/ObjectWatcher;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 48
    invoke-static {v0}, Lleakcanary/internal/ActivityDestroyWatcher;->access$getLifecycleCallbacks$p(Lleakcanary/internal/ActivityDestroyWatcher;)Lleakcanary/internal/ActivityDestroyWatcher$lifecycleCallbacks$1;

    move-result-object p2

    check-cast p2, Landroid/app/Application$ActivityLifecycleCallbacks;

    invoke-virtual {p1, p2}, Landroid/app/Application;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    return-void
.end method
