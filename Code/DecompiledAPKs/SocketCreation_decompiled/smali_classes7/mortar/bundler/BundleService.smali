.class public Lmortar/bundler/BundleService;
.super Ljava/lang/Object;
.source "BundleService.java"


# instance fields
.field final bundlers:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lmortar/bundler/Bundler;",
            ">;"
        }
    .end annotation
.end field

.field final runner:Lmortar/bundler/BundleServiceRunner;

.field final scope:Lmortar/MortarScope;

.field scopeBundle:Landroid/os/Bundle;

.field private toBeLoaded:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lmortar/bundler/Bundler;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lmortar/bundler/BundleServiceRunner;Lmortar/MortarScope;)V
    .locals 1

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lmortar/bundler/BundleService;->bundlers:Ljava/util/Set;

    .line 20
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lmortar/bundler/BundleService;->toBeLoaded:Ljava/util/List;

    .line 23
    iput-object p1, p0, Lmortar/bundler/BundleService;->runner:Lmortar/bundler/BundleServiceRunner;

    .line 24
    iput-object p2, p0, Lmortar/bundler/BundleService;->scope:Lmortar/MortarScope;

    .line 25
    iget-object p1, p1, Lmortar/bundler/BundleServiceRunner;->rootBundle:Landroid/os/Bundle;

    invoke-direct {p0, p1}, Lmortar/bundler/BundleService;->findScopeBundle(Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object p1

    iput-object p1, p0, Lmortar/bundler/BundleService;->scopeBundle:Landroid/os/Bundle;

    return-void
.end method

.method private findScopeBundle(Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 2

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    .line 116
    :cond_0
    iget-object v0, p0, Lmortar/bundler/BundleService;->runner:Lmortar/bundler/BundleServiceRunner;

    iget-object v1, p0, Lmortar/bundler/BundleService;->scope:Lmortar/MortarScope;

    invoke-virtual {v0, v1}, Lmortar/bundler/BundleServiceRunner;->bundleKey(Lmortar/MortarScope;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public static getBundleService(Landroid/content/Context;)Lmortar/bundler/BundleService;
    .locals 2

    .line 29
    invoke-static {p0}, Lmortar/bundler/BundleServiceRunner;->getBundleServiceRunner(Landroid/content/Context;)Lmortar/bundler/BundleServiceRunner;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 34
    invoke-static {p0}, Lmortar/MortarScope;->getScope(Landroid/content/Context;)Lmortar/MortarScope;

    move-result-object p0

    invoke-virtual {v0, p0}, Lmortar/bundler/BundleServiceRunner;->requireBundleService(Lmortar/MortarScope;)Lmortar/bundler/BundleService;

    move-result-object p0

    return-object p0

    .line 31
    :cond_0
    new-instance p0, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "You forgot to set up a "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-class v1, Lmortar/bundler/BundleServiceRunner;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " in your activity"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static getBundleService(Lmortar/MortarScope;)Lmortar/bundler/BundleService;
    .locals 2

    .line 38
    invoke-static {p0}, Lmortar/bundler/BundleServiceRunner;->getBundleServiceRunner(Lmortar/MortarScope;)Lmortar/bundler/BundleServiceRunner;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 43
    invoke-virtual {v0, p0}, Lmortar/bundler/BundleServiceRunner;->requireBundleService(Lmortar/MortarScope;)Lmortar/bundler/BundleService;

    move-result-object p0

    return-object p0

    .line 40
    :cond_0
    new-instance p0, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "You forgot to set up a "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-class v1, Lmortar/bundler/BundleServiceRunner;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " in your activity"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p0
.end method


# virtual methods
.method init()V
    .locals 2

    .line 81
    iget-object v0, p0, Lmortar/bundler/BundleService;->scope:Lmortar/MortarScope;

    new-instance v1, Lmortar/bundler/BundleService$1;

    invoke-direct {v1, p0}, Lmortar/bundler/BundleService$1;-><init>(Lmortar/bundler/BundleService;)V

    invoke-virtual {v0, v1}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    return-void
.end method

.method loadOne()V
    .locals 3

    .line 100
    iget-object v0, p0, Lmortar/bundler/BundleService;->toBeLoaded:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 102
    :cond_0
    iget-object v0, p0, Lmortar/bundler/BundleService;->toBeLoaded:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmortar/bundler/Bundler;

    .line 103
    iget-object v1, p0, Lmortar/bundler/BundleService;->scopeBundle:Landroid/os/Bundle;

    if-nez v1, :cond_1

    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    invoke-interface {v0}, Lmortar/bundler/Bundler;->getMortarBundleKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 105
    :goto_0
    invoke-interface {v0, v1}, Lmortar/bundler/Bundler;->onLoad(Landroid/os/Bundle;)V

    return-void
.end method

.method needsLoading()Z
    .locals 1

    .line 96
    iget-object v0, p0, Lmortar/bundler/BundleService;->toBeLoaded:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public register(Lmortar/bundler/Bundler;)V
    .locals 3

    if-eqz p1, :cond_6

    .line 52
    iget-object v0, p0, Lmortar/bundler/BundleService;->runner:Lmortar/bundler/BundleServiceRunner;

    iget-object v0, v0, Lmortar/bundler/BundleServiceRunner;->state:Lmortar/bundler/BundleServiceRunner$State;

    sget-object v1, Lmortar/bundler/BundleServiceRunner$State;->SAVING:Lmortar/bundler/BundleServiceRunner$State;

    if-eq v0, v1, :cond_5

    .line 56
    iget-object v0, p0, Lmortar/bundler/BundleService;->bundlers:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmortar/bundler/BundleService;->scope:Lmortar/MortarScope;

    invoke-interface {p1, v0}, Lmortar/bundler/Bundler;->onEnterScope(Lmortar/MortarScope;)V

    .line 57
    :cond_0
    invoke-interface {p1}, Lmortar/bundler/Bundler;->getMortarBundleKey()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    if-eqz v0, :cond_4

    .line 58
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 62
    sget-object v0, Lmortar/bundler/BundleService$2;->$SwitchMap$mortar$bundler$BundleServiceRunner$State:[I

    iget-object v2, p0, Lmortar/bundler/BundleService;->runner:Lmortar/bundler/BundleServiceRunner;

    iget-object v2, v2, Lmortar/bundler/BundleServiceRunner;->state:Lmortar/bundler/BundleServiceRunner$State;

    invoke-virtual {v2}, Lmortar/bundler/BundleServiceRunner$State;->ordinal()I

    move-result v2

    aget v0, v0, v2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 69
    iget-object v0, p0, Lmortar/bundler/BundleService;->toBeLoaded:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 70
    iget-object v0, p0, Lmortar/bundler/BundleService;->toBeLoaded:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 71
    iget-object p1, p0, Lmortar/bundler/BundleService;->runner:Lmortar/bundler/BundleServiceRunner;

    iget-object p1, p1, Lmortar/bundler/BundleServiceRunner;->servicesToBeLoaded:Ljava/util/NavigableSet;

    invoke-interface {p1, p0}, Ljava/util/NavigableSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 76
    :cond_1
    new-instance p1, Ljava/lang/AssertionError;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unexpected state "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lmortar/bundler/BundleService;->runner:Lmortar/bundler/BundleServiceRunner;

    iget-object v1, v1, Lmortar/bundler/BundleServiceRunner;->state:Lmortar/bundler/BundleServiceRunner$State;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw p1

    .line 64
    :cond_2
    iget-object v0, p0, Lmortar/bundler/BundleService;->toBeLoaded:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 65
    iget-object p1, p0, Lmortar/bundler/BundleService;->runner:Lmortar/bundler/BundleServiceRunner;

    iget-object p1, p1, Lmortar/bundler/BundleServiceRunner;->servicesToBeLoaded:Ljava/util/NavigableSet;

    invoke-interface {p1, p0}, Ljava/util/NavigableSet;->add(Ljava/lang/Object;)Z

    .line 66
    iget-object p1, p0, Lmortar/bundler/BundleService;->runner:Lmortar/bundler/BundleServiceRunner;

    invoke-virtual {p1}, Lmortar/bundler/BundleServiceRunner;->finishLoading()V

    :cond_3
    :goto_0
    return-void

    .line 59
    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const-string p1, "%s has null or empty bundle key"

    invoke-static {p1, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 53
    :cond_5
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Cannot register during onSave"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 50
    :cond_6
    new-instance p1, Ljava/lang/NullPointerException;

    const-string v0, "Cannot register null bundler."

    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method saveToRootBundle(Landroid/os/Bundle;)V
    .locals 4

    .line 120
    iget-object v0, p0, Lmortar/bundler/BundleService;->runner:Lmortar/bundler/BundleServiceRunner;

    iget-object v1, p0, Lmortar/bundler/BundleService;->scope:Lmortar/MortarScope;

    invoke-virtual {v0, v1}, Lmortar/bundler/BundleServiceRunner;->bundleKey(Lmortar/MortarScope;)Ljava/lang/String;

    move-result-object v0

    .line 121
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    iput-object v1, p0, Lmortar/bundler/BundleService;->scopeBundle:Landroid/os/Bundle;

    .line 123
    iget-object v1, p0, Lmortar/bundler/BundleService;->scopeBundle:Landroid/os/Bundle;

    if-nez v1, :cond_0

    .line 124
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    iput-object v1, p0, Lmortar/bundler/BundleService;->scopeBundle:Landroid/os/Bundle;

    .line 125
    iget-object v1, p0, Lmortar/bundler/BundleService;->scopeBundle:Landroid/os/Bundle;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 128
    :cond_0
    iget-object p1, p0, Lmortar/bundler/BundleService;->bundlers:Ljava/util/Set;

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lmortar/bundler/Bundler;

    .line 129
    iget-object v1, p0, Lmortar/bundler/BundleService;->scopeBundle:Landroid/os/Bundle;

    invoke-interface {v0}, Lmortar/bundler/Bundler;->getMortarBundleKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    if-nez v1, :cond_2

    .line 131
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 132
    iget-object v2, p0, Lmortar/bundler/BundleService;->scopeBundle:Landroid/os/Bundle;

    invoke-interface {v0}, Lmortar/bundler/Bundler;->getMortarBundleKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 135
    :cond_2
    invoke-interface {v0, v1}, Lmortar/bundler/Bundler;->onSave(Landroid/os/Bundle;)V

    .line 138
    iget-object v0, p0, Lmortar/bundler/BundleService;->scope:Lmortar/MortarScope;

    invoke-virtual {v0}, Lmortar/MortarScope;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_3
    return-void
.end method

.method updateScopedBundleOnCreate(Landroid/os/Bundle;)Z
    .locals 1

    .line 110
    invoke-direct {p0, p1}, Lmortar/bundler/BundleService;->findScopeBundle(Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object p1

    iput-object p1, p0, Lmortar/bundler/BundleService;->scopeBundle:Landroid/os/Bundle;

    .line 111
    iget-object p1, p0, Lmortar/bundler/BundleService;->toBeLoaded:Ljava/util/List;

    iget-object v0, p0, Lmortar/bundler/BundleService;->bundlers:Ljava/util/Set;

    invoke-interface {p1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 112
    iget-object p1, p0, Lmortar/bundler/BundleService;->toBeLoaded:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    return p1
.end method
