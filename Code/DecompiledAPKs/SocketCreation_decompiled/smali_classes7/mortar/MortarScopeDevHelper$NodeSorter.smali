.class Lmortar/MortarScopeDevHelper$NodeSorter;
.super Ljava/lang/Object;
.source "MortarScopeDevHelper.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lmortar/MortarScopeDevHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "NodeSorter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator<",
        "Lmortar/MortarScopeDevHelper$Node;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lmortar/MortarScopeDevHelper$1;)V
    .locals 0

    .line 114
    invoke-direct {p0}, Lmortar/MortarScopeDevHelper$NodeSorter;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 0

    .line 114
    check-cast p1, Lmortar/MortarScopeDevHelper$Node;

    check-cast p2, Lmortar/MortarScopeDevHelper$Node;

    invoke-virtual {p0, p1, p2}, Lmortar/MortarScopeDevHelper$NodeSorter;->compare(Lmortar/MortarScopeDevHelper$Node;Lmortar/MortarScopeDevHelper$Node;)I

    move-result p1

    return p1
.end method

.method public compare(Lmortar/MortarScopeDevHelper$Node;Lmortar/MortarScopeDevHelper$Node;)I
    .locals 0

    .line 116
    invoke-interface {p1}, Lmortar/MortarScopeDevHelper$Node;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-interface {p2}, Lmortar/MortarScopeDevHelper$Node;->getName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result p1

    return p1
.end method
