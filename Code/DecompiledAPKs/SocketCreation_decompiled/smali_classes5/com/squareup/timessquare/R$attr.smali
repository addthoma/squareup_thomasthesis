.class public final Lcom/squareup/timessquare/R$attr;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/timessquare/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "attr"
.end annotation


# static fields
.field public static final tsquare_dayBackground:I = 0x7f04047c

.field public static final tsquare_dayTextColor:I = 0x7f04047d

.field public static final tsquare_displayAlwaysDigitNumbers:I = 0x7f04047e

.field public static final tsquare_displayDayNamesHeaderRow:I = 0x7f04047f

.field public static final tsquare_displayHeader:I = 0x7f040480

.field public static final tsquare_dividerColor:I = 0x7f040481

.field public static final tsquare_headerTextColor:I = 0x7f040482

.field public static final tsquare_state_current_month:I = 0x7f040483

.field public static final tsquare_state_highlighted:I = 0x7f040484

.field public static final tsquare_state_range_first:I = 0x7f040485

.field public static final tsquare_state_range_last:I = 0x7f040486

.field public static final tsquare_state_range_middle:I = 0x7f040487

.field public static final tsquare_state_selectable:I = 0x7f040488

.field public static final tsquare_state_today:I = 0x7f040489

.field public static final tsquare_titleTextStyle:I = 0x7f04048a


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
