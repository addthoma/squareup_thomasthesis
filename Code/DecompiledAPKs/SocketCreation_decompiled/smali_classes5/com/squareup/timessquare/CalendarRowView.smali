.class public Lcom/squareup/timessquare/CalendarRowView;
.super Landroid/view/ViewGroup;
.source "CalendarRowView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private isHeaderRow:Z

.field private listener:Lcom/squareup/timessquare/MonthView$Listener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 22
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .locals 0

    .line 26
    invoke-virtual {p1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 27
    invoke-super {p0, p1, p2, p3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .line 76
    iget-object v0, p0, Lcom/squareup/timessquare/CalendarRowView;->listener:Lcom/squareup/timessquare/MonthView$Listener;

    if-eqz v0, :cond_0

    .line 77
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/timessquare/MonthCellDescriptor;

    invoke-interface {v0, p1}, Lcom/squareup/timessquare/MonthView$Listener;->handleClick(Lcom/squareup/timessquare/MonthCellDescriptor;)V

    :cond_0
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 5

    .line 58
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sub-int/2addr p5, p3

    sub-int/2addr p4, p2

    .line 61
    invoke-virtual {p0}, Lcom/squareup/timessquare/CalendarRowView;->getChildCount()I

    move-result p1

    const/4 p2, 0x0

    const/4 p3, 0x0

    :goto_0
    if-ge p3, p1, :cond_0

    .line 62
    invoke-virtual {p0, p3}, Lcom/squareup/timessquare/CalendarRowView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    mul-int v3, p3, p4

    .line 63
    div-int/lit8 v3, v3, 0x7

    add-int/lit8 p3, p3, 0x1

    mul-int v4, p3, p4

    .line 64
    div-int/lit8 v4, v4, 0x7

    .line 65
    invoke-virtual {v2, v3, p2, v4, p5}, Landroid/view/View;->layout(IIII)V

    goto :goto_0

    :cond_0
    const/4 p1, 0x1

    new-array p1, p1, [Ljava/lang/Object;

    .line 67
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide p3

    sub-long/2addr p3, v0

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p3

    aput-object p3, p1, p2

    const-string p2, "Row.onLayout %d ms"

    invoke-static {p2, p1}, Lcom/squareup/timessquare/Logr;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 9

    .line 31
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 32
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result p1

    const/high16 p2, -0x80000000

    .line 34
    invoke-static {p1, p2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    .line 35
    invoke-virtual {p0}, Lcom/squareup/timessquare/CalendarRowView;->getChildCount()I

    move-result v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    :cond_0
    :goto_0
    if-ge v4, v2, :cond_2

    .line 36
    invoke-virtual {p0, v4}, Lcom/squareup/timessquare/CalendarRowView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    mul-int v7, v4, p1

    .line 38
    div-int/lit8 v7, v7, 0x7

    add-int/lit8 v4, v4, 0x1

    mul-int v8, v4, p1

    .line 39
    div-int/lit8 v8, v8, 0x7

    sub-int/2addr v8, v7

    .line 41
    iget-boolean v7, p0, Lcom/squareup/timessquare/CalendarRowView;->isHeaderRow:Z

    if-nez v7, :cond_1

    .line 42
    invoke-virtual {v6, v8}, Landroid/view/View;->setMinimumHeight(I)V

    :cond_1
    const/high16 v7, 0x40000000    # 2.0f

    .line 44
    invoke-static {v8, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    .line 45
    invoke-virtual {v6, v7, p2}, Landroid/view/View;->measure(II)V

    .line 47
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    if-le v7, v5, :cond_0

    .line 48
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    goto :goto_0

    .line 51
    :cond_2
    invoke-virtual {p0}, Lcom/squareup/timessquare/CalendarRowView;->getPaddingLeft()I

    move-result p2

    add-int/2addr p1, p2

    invoke-virtual {p0}, Lcom/squareup/timessquare/CalendarRowView;->getPaddingRight()I

    move-result p2

    add-int/2addr p1, p2

    .line 52
    invoke-virtual {p0}, Lcom/squareup/timessquare/CalendarRowView;->getPaddingTop()I

    move-result p2

    add-int/2addr v5, p2

    invoke-virtual {p0}, Lcom/squareup/timessquare/CalendarRowView;->getPaddingBottom()I

    move-result p2

    add-int/2addr v5, p2

    .line 53
    invoke-virtual {p0, p1, v5}, Lcom/squareup/timessquare/CalendarRowView;->setMeasuredDimension(II)V

    const/4 p1, 0x1

    new-array p1, p1, [Ljava/lang/Object;

    .line 54
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    aput-object p2, p1, v3

    const-string p2, "Row.onMeasure %d ms"

    invoke-static {p2, p1}, Lcom/squareup/timessquare/Logr;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public setCellBackground(I)V
    .locals 2

    const/4 v0, 0x0

    .line 96
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/timessquare/CalendarRowView;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 97
    invoke-virtual {p0, v0}, Lcom/squareup/timessquare/CalendarRowView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/view/View;->setBackgroundResource(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public setCellTextColor(I)V
    .locals 2

    const/4 v0, 0x0

    .line 102
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/timessquare/CalendarRowView;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 103
    invoke-virtual {p0, v0}, Lcom/squareup/timessquare/CalendarRowView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    instance-of v1, v1, Lcom/squareup/timessquare/CalendarCellView;

    if-eqz v1, :cond_0

    .line 104
    invoke-virtual {p0, v0}, Lcom/squareup/timessquare/CalendarRowView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/timessquare/CalendarCellView;

    invoke-virtual {v1}, Lcom/squareup/timessquare/CalendarCellView;->getDayOfMonthTextView()Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_1

    .line 106
    :cond_0
    invoke-virtual {p0, v0}, Lcom/squareup/timessquare/CalendarRowView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setTextColor(I)V

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public setCellTextColor(Landroid/content/res/ColorStateList;)V
    .locals 2

    const/4 v0, 0x0

    .line 112
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/timessquare/CalendarRowView;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 113
    invoke-virtual {p0, v0}, Lcom/squareup/timessquare/CalendarRowView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    instance-of v1, v1, Lcom/squareup/timessquare/CalendarCellView;

    if-eqz v1, :cond_0

    .line 114
    invoke-virtual {p0, v0}, Lcom/squareup/timessquare/CalendarRowView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/timessquare/CalendarCellView;

    invoke-virtual {v1}, Lcom/squareup/timessquare/CalendarCellView;->getDayOfMonthTextView()Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    goto :goto_1

    .line 116
    :cond_0
    invoke-virtual {p0, v0}, Lcom/squareup/timessquare/CalendarRowView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public setDayViewAdapter(Lcom/squareup/timessquare/DayViewAdapter;)V
    .locals 2

    const/4 v0, 0x0

    .line 86
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/timessquare/CalendarRowView;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 87
    invoke-virtual {p0, v0}, Lcom/squareup/timessquare/CalendarRowView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    instance-of v1, v1, Lcom/squareup/timessquare/CalendarCellView;

    if-eqz v1, :cond_0

    .line 88
    invoke-virtual {p0, v0}, Lcom/squareup/timessquare/CalendarRowView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/timessquare/CalendarCellView;

    .line 89
    invoke-virtual {v1}, Lcom/squareup/timessquare/CalendarCellView;->removeAllViews()V

    .line 90
    invoke-interface {p1, v1}, Lcom/squareup/timessquare/DayViewAdapter;->makeCellView(Lcom/squareup/timessquare/CalendarCellView;)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public setIsHeaderRow(Z)V
    .locals 0

    .line 71
    iput-boolean p1, p0, Lcom/squareup/timessquare/CalendarRowView;->isHeaderRow:Z

    return-void
.end method

.method public setListener(Lcom/squareup/timessquare/MonthView$Listener;)V
    .locals 0

    .line 82
    iput-object p1, p0, Lcom/squareup/timessquare/CalendarRowView;->listener:Lcom/squareup/timessquare/MonthView$Listener;

    return-void
.end method

.method public setTypeface(Landroid/graphics/Typeface;)V
    .locals 2

    const/4 v0, 0x0

    .line 122
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/timessquare/CalendarRowView;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 123
    invoke-virtual {p0, v0}, Lcom/squareup/timessquare/CalendarRowView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    instance-of v1, v1, Lcom/squareup/timessquare/CalendarCellView;

    if-eqz v1, :cond_0

    .line 124
    invoke-virtual {p0, v0}, Lcom/squareup/timessquare/CalendarRowView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/squareup/timessquare/CalendarCellView;

    invoke-virtual {v1}, Lcom/squareup/timessquare/CalendarCellView;->getDayOfMonthTextView()Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    goto :goto_1

    .line 126
    :cond_0
    invoke-virtual {p0, v0}, Lcom/squareup/timessquare/CalendarRowView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method
