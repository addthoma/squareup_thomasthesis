.class public interface abstract Lcom/squareup/setupguide/SetupPaymentsDialog$Runner;
.super Ljava/lang/Object;
.source "SetupPaymentsDialog.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/setupguide/SetupPaymentsDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Runner"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H&J\u0010\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0005\u001a\u00020\u0001H&J\u0010\u0010\u0006\u001a\u00020\u00032\u0006\u0010\u0005\u001a\u00020\u0001H&J\u000e\u0010\u0007\u001a\u0008\u0012\u0004\u0012\u00020\t0\u0008H&\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/setupguide/SetupPaymentsDialog$Runner;",
        "",
        "goBackFromSetupPaymentsDialog",
        "",
        "onSetupPaymentsDialogPrimaryClicked",
        "key",
        "onSetupPaymentsDialogSecondaryClicked",
        "setupPaymentsDialogScreenData",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract goBackFromSetupPaymentsDialog()V
.end method

.method public abstract onSetupPaymentsDialogPrimaryClicked(Ljava/lang/Object;)V
.end method

.method public abstract onSetupPaymentsDialogSecondaryClicked(Ljava/lang/Object;)V
.end method

.method public abstract setupPaymentsDialogScreenData()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;",
            ">;"
        }
    .end annotation
.end method
