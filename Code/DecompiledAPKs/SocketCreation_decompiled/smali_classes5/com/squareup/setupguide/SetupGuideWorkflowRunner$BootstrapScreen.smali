.class public final Lcom/squareup/setupguide/SetupGuideWorkflowRunner$BootstrapScreen;
.super Lcom/squareup/container/BootstrapTreeKey;
.source "SetupGuideWorkflowRunner.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/setupguide/SetupGuideWorkflowRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BootstrapScreen"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\u0008\u0010\u0007\u001a\u00020\u0008H\u0016\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/setupguide/SetupGuideWorkflowRunner$BootstrapScreen;",
        "Lcom/squareup/container/BootstrapTreeKey;",
        "()V",
        "doRegister",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "getParentKey",
        "Lcom/squareup/setupguide/SetupGuideWorkflowRunner$Scope;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/setupguide/SetupGuideWorkflowRunner$BootstrapScreen;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 41
    new-instance v0, Lcom/squareup/setupguide/SetupGuideWorkflowRunner$BootstrapScreen;

    invoke-direct {v0}, Lcom/squareup/setupguide/SetupGuideWorkflowRunner$BootstrapScreen;-><init>()V

    sput-object v0, Lcom/squareup/setupguide/SetupGuideWorkflowRunner$BootstrapScreen;->INSTANCE:Lcom/squareup/setupguide/SetupGuideWorkflowRunner$BootstrapScreen;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 41
    invoke-direct {p0}, Lcom/squareup/container/BootstrapTreeKey;-><init>()V

    return-void
.end method


# virtual methods
.method public doRegister(Lmortar/MortarScope;)V
    .locals 1

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    sget-object v0, Lcom/squareup/setupguide/SetupGuideWorkflowRunner;->Companion:Lcom/squareup/setupguide/SetupGuideWorkflowRunner$Companion;

    invoke-virtual {v0, p1}, Lcom/squareup/setupguide/SetupGuideWorkflowRunner$Companion;->get(Lmortar/MortarScope;)Lcom/squareup/setupguide/SetupGuideWorkflowRunner;

    move-result-object p1

    .line 47
    invoke-interface {p1}, Lcom/squareup/setupguide/SetupGuideWorkflowRunner;->startWorkflow()V

    return-void
.end method

.method public getParentKey()Lcom/squareup/setupguide/SetupGuideWorkflowRunner$Scope;
    .locals 1

    .line 43
    sget-object v0, Lcom/squareup/setupguide/SetupGuideWorkflowRunner$Scope;->INSTANCE:Lcom/squareup/setupguide/SetupGuideWorkflowRunner$Scope;

    return-object v0
.end method

.method public bridge synthetic getParentKey()Ljava/lang/Object;
    .locals 1

    .line 41
    invoke-virtual {p0}, Lcom/squareup/setupguide/SetupGuideWorkflowRunner$BootstrapScreen;->getParentKey()Lcom/squareup/setupguide/SetupGuideWorkflowRunner$Scope;

    move-result-object v0

    return-object v0
.end method
