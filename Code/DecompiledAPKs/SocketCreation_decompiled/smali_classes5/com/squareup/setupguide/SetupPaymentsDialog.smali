.class public final Lcom/squareup/setupguide/SetupPaymentsDialog;
.super Lcom/squareup/ui/main/RegisterTreeKey;
.source "SetupPaymentsDialog.kt"

# interfaces
.implements Lcom/squareup/container/MaybePersistent;
.implements Lcom/squareup/coordinators/CoordinatorProvider;


# annotations
.annotation runtime Lcom/squareup/container/layer/DialogScreen;
    value = Lcom/squareup/setupguide/SetupPaymentsDialog$Factory;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/setupguide/SetupPaymentsDialog$ParentComponent;,
        Lcom/squareup/setupguide/SetupPaymentsDialog$ScreenData;,
        Lcom/squareup/setupguide/SetupPaymentsDialog$Runner;,
        Lcom/squareup/setupguide/SetupPaymentsDialog$Factory;
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSetupPaymentsDialog.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SetupPaymentsDialog.kt\ncom/squareup/setupguide/SetupPaymentsDialog\n+ 2 Components.kt\ncom/squareup/dagger/Components\n*L\n1#1,136:1\n43#2:137\n*E\n*S KotlinDebug\n*F\n+ 1 SetupPaymentsDialog.kt\ncom/squareup/setupguide/SetupPaymentsDialog\n*L\n114#1:137\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0005\u0008\u0007\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003:\u0004\r\u000e\u000f\u0010B\r\u0012\u0006\u0010\u0004\u001a\u00020\u0001\u00a2\u0006\u0002\u0010\u0005J\u0008\u0010\u0006\u001a\u00020\u0007H\u0016J\u0008\u0010\u0008\u001a\u00020\u0001H\u0016J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u000cH\u0016R\u000e\u0010\u0004\u001a\u00020\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/setupguide/SetupPaymentsDialog;",
        "Lcom/squareup/ui/main/RegisterTreeKey;",
        "Lcom/squareup/container/MaybePersistent;",
        "Lcom/squareup/coordinators/CoordinatorProvider;",
        "parentTreeKey",
        "(Lcom/squareup/ui/main/RegisterTreeKey;)V",
        "getAnalyticsName",
        "Lcom/squareup/analytics/RegisterViewName;",
        "getParentKey",
        "provideCoordinator",
        "Lcom/squareup/setupguide/SetupPaymentsDialogCoordinator;",
        "view",
        "Landroid/view/View;",
        "Factory",
        "ParentComponent",
        "Runner",
        "ScreenData",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final parentTreeKey:Lcom/squareup/ui/main/RegisterTreeKey;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/main/RegisterTreeKey;)V
    .locals 1

    const-string v0, "parentTreeKey"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    invoke-direct {p0}, Lcom/squareup/ui/main/RegisterTreeKey;-><init>()V

    iput-object p1, p0, Lcom/squareup/setupguide/SetupPaymentsDialog;->parentTreeKey:Lcom/squareup/ui/main/RegisterTreeKey;

    return-void
.end method


# virtual methods
.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 33
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->INVOICES_PROMPT_IDV:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getParentKey()Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 1

    .line 111
    iget-object v0, p0, Lcom/squareup/setupguide/SetupPaymentsDialog;->parentTreeKey:Lcom/squareup/ui/main/RegisterTreeKey;

    return-object v0
.end method

.method public bridge synthetic getParentKey()Ljava/lang/Object;
    .locals 1

    .line 28
    invoke-virtual {p0}, Lcom/squareup/setupguide/SetupPaymentsDialog;->getParentKey()Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object v0

    return-object v0
.end method

.method public isPersistent()Z
    .locals 1

    .line 28
    invoke-static {p0}, Lcom/squareup/container/MaybePersistent$DefaultImpls;->isPersistent(Lcom/squareup/container/MaybePersistent;)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 0

    .line 28
    invoke-virtual {p0, p1}, Lcom/squareup/setupguide/SetupPaymentsDialog;->provideCoordinator(Landroid/view/View;)Lcom/squareup/setupguide/SetupPaymentsDialogCoordinator;

    move-result-object p1

    check-cast p1, Lcom/squareup/coordinators/Coordinator;

    return-object p1
.end method

.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/setupguide/SetupPaymentsDialogCoordinator;
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 137
    const-class v0, Lcom/squareup/setupguide/SetupPaymentsDialog$ParentComponent;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/setupguide/SetupPaymentsDialog$ParentComponent;

    .line 114
    invoke-interface {p1}, Lcom/squareup/setupguide/SetupPaymentsDialog$ParentComponent;->setupPaymentsDialogCoordinator()Lcom/squareup/setupguide/SetupPaymentsDialogCoordinator;

    move-result-object p1

    return-object p1
.end method
