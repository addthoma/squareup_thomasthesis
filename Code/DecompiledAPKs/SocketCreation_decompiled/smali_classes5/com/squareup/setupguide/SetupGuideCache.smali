.class public interface abstract Lcom/squareup/setupguide/SetupGuideCache;
.super Ljava/lang/Object;
.source "SetupGuideCache.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/setupguide/SetupGuideCache$ActionItemStatus;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008f\u0018\u00002\u00020\u0001:\u0001\u0016J\u0016\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\n0\u00032\u0006\u0010\r\u001a\u00020\u0005H&J\u0016\u0010\u000e\u001a\u00020\u000f2\u000c\u0010\u0010\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0011H&J\u0010\u0010\u0012\u001a\u00020\u000f2\u0006\u0010\r\u001a\u00020\u0005H&J \u0010\u0013\u001a\u001a\u0012\u0016\u0012\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u00040\u00150\u0014H&R$\u0010\u0002\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u00040\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0007\u0010\u0008R\u0018\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\n0\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u000b\u0010\u0008\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/setupguide/SetupGuideCache;",
        "",
        "actionItemMap",
        "Lio/reactivex/Observable;",
        "",
        "Lcom/squareup/protos/checklist/common/ActionItemName;",
        "Lcom/squareup/setupguide/SetupGuideCache$ActionItemStatus;",
        "getActionItemMap",
        "()Lio/reactivex/Observable;",
        "hasIncompleteItem",
        "",
        "getHasIncompleteItem",
        "isCompleted",
        "actionItemName",
        "markAsCompleted",
        "",
        "actionItemNames",
        "",
        "markAsJustCompleted",
        "refresh",
        "Lio/reactivex/Single;",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "ActionItemStatus",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract getActionItemMap()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/util/Map<",
            "Lcom/squareup/protos/checklist/common/ActionItemName;",
            "Lcom/squareup/setupguide/SetupGuideCache$ActionItemStatus;",
            ">;>;"
        }
    .end annotation
.end method

.method public abstract getHasIncompleteItem()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end method

.method public abstract isCompleted(Lcom/squareup/protos/checklist/common/ActionItemName;)Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/checklist/common/ActionItemName;",
            ")",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end method

.method public abstract markAsCompleted(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/protos/checklist/common/ActionItemName;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract markAsJustCompleted(Lcom/squareup/protos/checklist/common/ActionItemName;)V
.end method

.method public abstract refresh()Lio/reactivex/Single;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Ljava/util/Map<",
            "Lcom/squareup/protos/checklist/common/ActionItemName;",
            "Lcom/squareup/setupguide/SetupGuideCache$ActionItemStatus;",
            ">;>;>;"
        }
    .end annotation
.end method
