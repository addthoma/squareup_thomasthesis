.class public final synthetic Lcom/squareup/tickets/-$$Lambda$RealTickets$gh-6UkwYfSPUX27IN59rEbPL7N8;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Lcom/squareup/tickets/RegisterTicketTask;


# instance fields
.field private final synthetic f$0:Lcom/squareup/tickets/RealTickets;

.field private final synthetic f$1:Ljava/util/List;

.field private final synthetic f$2:Ljava/lang/String;

.field private final synthetic f$3:Lcom/squareup/payment/Order;

.field private final synthetic f$4:Lcom/squareup/util/Res;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/tickets/RealTickets;Ljava/util/List;Ljava/lang/String;Lcom/squareup/payment/Order;Lcom/squareup/util/Res;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/tickets/-$$Lambda$RealTickets$gh-6UkwYfSPUX27IN59rEbPL7N8;->f$0:Lcom/squareup/tickets/RealTickets;

    iput-object p2, p0, Lcom/squareup/tickets/-$$Lambda$RealTickets$gh-6UkwYfSPUX27IN59rEbPL7N8;->f$1:Ljava/util/List;

    iput-object p3, p0, Lcom/squareup/tickets/-$$Lambda$RealTickets$gh-6UkwYfSPUX27IN59rEbPL7N8;->f$2:Ljava/lang/String;

    iput-object p4, p0, Lcom/squareup/tickets/-$$Lambda$RealTickets$gh-6UkwYfSPUX27IN59rEbPL7N8;->f$3:Lcom/squareup/payment/Order;

    iput-object p5, p0, Lcom/squareup/tickets/-$$Lambda$RealTickets$gh-6UkwYfSPUX27IN59rEbPL7N8;->f$4:Lcom/squareup/util/Res;

    return-void
.end method


# virtual methods
.method public bridge synthetic perform(Lcom/squareup/tickets/TicketDatabase;)Ljava/lang/Object;
    .locals 0

    invoke-static {p0, p1}, Lcom/squareup/tickets/RegisterTicketTask$-CC;->$default$perform(Lcom/squareup/tickets/RegisterTicketTask;Lcom/squareup/tickets/TicketDatabase;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final perform(Lcom/squareup/tickets/TicketStore;)Ljava/lang/Object;
    .locals 6

    iget-object v0, p0, Lcom/squareup/tickets/-$$Lambda$RealTickets$gh-6UkwYfSPUX27IN59rEbPL7N8;->f$0:Lcom/squareup/tickets/RealTickets;

    iget-object v1, p0, Lcom/squareup/tickets/-$$Lambda$RealTickets$gh-6UkwYfSPUX27IN59rEbPL7N8;->f$1:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/tickets/-$$Lambda$RealTickets$gh-6UkwYfSPUX27IN59rEbPL7N8;->f$2:Ljava/lang/String;

    iget-object v3, p0, Lcom/squareup/tickets/-$$Lambda$RealTickets$gh-6UkwYfSPUX27IN59rEbPL7N8;->f$3:Lcom/squareup/payment/Order;

    iget-object v4, p0, Lcom/squareup/tickets/-$$Lambda$RealTickets$gh-6UkwYfSPUX27IN59rEbPL7N8;->f$4:Lcom/squareup/util/Res;

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, Lcom/squareup/tickets/RealTickets;->lambda$mergeTicketsToExisting$15$RealTickets(Ljava/util/List;Ljava/lang/String;Lcom/squareup/payment/Order;Lcom/squareup/util/Res;Lcom/squareup/tickets/TicketStore;)Lcom/squareup/tickets/Tickets$MergeResults;

    move-result-object p1

    return-object p1
.end method
