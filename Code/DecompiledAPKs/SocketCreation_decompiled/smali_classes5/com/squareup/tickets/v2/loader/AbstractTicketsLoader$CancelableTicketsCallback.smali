.class abstract Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$CancelableTicketsCallback;
.super Ljava/lang/Object;
.source "AbstractTicketsLoader.java"

# interfaces
.implements Lcom/squareup/tickets/TicketsCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x40a
    name = "CancelableTicketsCallback"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/tickets/TicketsCallback<",
        "TT;>;"
    }
.end annotation


# instance fields
.field protected canceled:Z


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 217
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$1;)V
    .locals 0

    .line 217
    invoke-direct {p0}, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$CancelableTicketsCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public call(Lcom/squareup/tickets/TicketsResult;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tickets/TicketsResult<",
            "TT;>;)V"
        }
    .end annotation

    .line 222
    iget-boolean v0, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$CancelableTicketsCallback;->canceled:Z

    if-nez v0, :cond_0

    .line 223
    invoke-virtual {p0, p1}, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$CancelableTicketsCallback;->onCompleted(Lcom/squareup/tickets/TicketsResult;)V

    goto :goto_0

    .line 225
    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$CancelableTicketsCallback;->onAborted(Lcom/squareup/tickets/TicketsResult;)V

    :goto_0
    return-void
.end method

.method public final cancel()V
    .locals 1

    const/4 v0, 0x1

    .line 230
    iput-boolean v0, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$CancelableTicketsCallback;->canceled:Z

    return-void
.end method

.method protected abstract onAborted(Lcom/squareup/tickets/TicketsResult;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tickets/TicketsResult<",
            "TT;>;)V"
        }
    .end annotation
.end method

.method protected abstract onCompleted(Lcom/squareup/tickets/TicketsResult;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tickets/TicketsResult<",
            "TT;>;)V"
        }
    .end annotation
.end method
