.class Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketQueryCallback;
.super Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$CancelableTicketCursorCallback;
.source "AbstractTicketsLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TicketQueryCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;


# direct methods
.method private constructor <init>(Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;)V
    .locals 1

    .line 266
    iput-object p1, p0, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketQueryCallback;->this$0:Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$CancelableTicketCursorCallback;-><init>(Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$1;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$1;)V
    .locals 0

    .line 266
    invoke-direct {p0, p1}, Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader$TicketQueryCallback;-><init>(Lcom/squareup/tickets/v2/loader/AbstractTicketsLoader;)V

    return-void
.end method


# virtual methods
.method protected onAborted(Lcom/squareup/tickets/TicketsResult;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tickets/TicketsResult<",
            "Lcom/squareup/tickets/TicketRowCursorList;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method protected onCompleted(Lcom/squareup/tickets/TicketsResult;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/tickets/TicketsResult<",
            "Lcom/squareup/tickets/TicketRowCursorList;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    .line 270
    invoke-interface {p1}, Lcom/squareup/tickets/TicketsResult;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/tickets/TicketRowCursorList;

    invoke-virtual {p1}, Lcom/squareup/tickets/TicketRowCursorList;->size()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string p1, "[Tickets_Loader] Setting unfiltered ticket cursor with %d results."

    .line 269
    invoke-static {p1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method
