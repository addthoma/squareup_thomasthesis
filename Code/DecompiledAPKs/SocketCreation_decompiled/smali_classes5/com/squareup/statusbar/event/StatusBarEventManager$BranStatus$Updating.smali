.class public abstract Lcom/squareup/statusbar/event/StatusBarEventManager$BranStatus$Updating;
.super Lcom/squareup/statusbar/event/StatusBarEventManager$BranStatus;
.source "StatusBarEventManager.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/statusbar/event/StatusBarEventManager$BranStatus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Updating"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/statusbar/event/StatusBarEventManager$BranStatus$Updating$BfdSoftwareUpdateStarted;,
        Lcom/squareup/statusbar/event/StatusBarEventManager$BranStatus$Updating$BfdSoftwareUpdateCompleted;,
        Lcom/squareup/statusbar/event/StatusBarEventManager$BranStatus$Updating$BfdSoftwareUpdating;,
        Lcom/squareup/statusbar/event/StatusBarEventManager$BranStatus$Updating$BfdFirmwareUpdating;,
        Lcom/squareup/statusbar/event/StatusBarEventManager$BranStatus$Updating$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u0000 \r2\u00020\u0001:\u0005\t\n\u000b\u000c\rB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0012\u0010\u0003\u001a\u00020\u0004X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0005\u0010\u0006R\u0012\u0010\u0007\u001a\u00020\u0004X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0008\u0010\u0006\u0082\u0001\u0004\u000e\u000f\u0010\u0011\u00a8\u0006\u0012"
    }
    d2 = {
        "Lcom/squareup/statusbar/event/StatusBarEventManager$BranStatus$Updating;",
        "Lcom/squareup/statusbar/event/StatusBarEventManager$BranStatus;",
        "()V",
        "percentage",
        "",
        "getPercentage",
        "()I",
        "step",
        "getStep",
        "BfdFirmwareUpdating",
        "BfdSoftwareUpdateCompleted",
        "BfdSoftwareUpdateStarted",
        "BfdSoftwareUpdating",
        "Companion",
        "Lcom/squareup/statusbar/event/StatusBarEventManager$BranStatus$Updating$BfdSoftwareUpdateStarted;",
        "Lcom/squareup/statusbar/event/StatusBarEventManager$BranStatus$Updating$BfdSoftwareUpdateCompleted;",
        "Lcom/squareup/statusbar/event/StatusBarEventManager$BranStatus$Updating$BfdSoftwareUpdating;",
        "Lcom/squareup/statusbar/event/StatusBarEventManager$BranStatus$Updating$BfdFirmwareUpdating;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final BFD_FIRMWARE_UPDATE_STEP:I = 0x2

.field public static final BFD_SOFTWARE_UPDATE_COMPLETE_PERCENTAGE:I = 0x64

.field public static final BFD_SOFTWARE_UPDATE_START_PERCENTAGE:I = 0x0

.field public static final BFD_SOFTWARE_UPDATE_STEP:I = 0x1

.field public static final Companion:Lcom/squareup/statusbar/event/StatusBarEventManager$BranStatus$Updating$Companion;

.field public static final TOTAL_UPDATE_STEPS:I = 0x2


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/statusbar/event/StatusBarEventManager$BranStatus$Updating$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/statusbar/event/StatusBarEventManager$BranStatus$Updating$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/statusbar/event/StatusBarEventManager$BranStatus$Updating;->Companion:Lcom/squareup/statusbar/event/StatusBarEventManager$BranStatus$Updating$Companion;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 165
    invoke-direct {p0, v0}, Lcom/squareup/statusbar/event/StatusBarEventManager$BranStatus;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 165
    invoke-direct {p0}, Lcom/squareup/statusbar/event/StatusBarEventManager$BranStatus$Updating;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getPercentage()I
.end method

.method public abstract getStep()I
.end method
