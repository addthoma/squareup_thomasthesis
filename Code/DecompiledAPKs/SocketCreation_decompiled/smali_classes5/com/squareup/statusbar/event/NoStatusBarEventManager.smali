.class public final Lcom/squareup/statusbar/event/NoStatusBarEventManager;
.super Ljava/lang/Object;
.source "NoStatusBarEventManager.kt"

# interfaces
.implements Lcom/squareup/statusbar/event/StatusBarEventManager;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u000b\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0000\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\u0010\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0008\u001a\u00020\tH\u0016J\u0008\u0010\n\u001a\u00020\u0004H\u0016J\u0018\u0010\u000b\u001a\u00020\u00042\u0006\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\rH\u0016J\u0008\u0010\u000f\u001a\u00020\u0004H\u0016J\u0010\u0010\u0010\u001a\u00020\u00042\u0006\u0010\u0011\u001a\u00020\u0012H\u0016J\u0008\u0010\u0013\u001a\u00020\u0004H\u0016J\u0010\u0010\u0014\u001a\u00020\u00042\u0006\u0010\u0015\u001a\u00020\rH\u0016J\u000e\u0010\u0016\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u0017H\u0016\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/statusbar/event/NoStatusBarEventManager;",
        "Lcom/squareup/statusbar/event/StatusBarEventManager;",
        "()V",
        "acceptViewClickEvent",
        "",
        "action",
        "",
        "onEnterScope",
        "scope",
        "Lmortar/MortarScope;",
        "onExitScope",
        "onOfflineStateChanged",
        "isInOfflineMode",
        "",
        "isInternetStateConnected",
        "onSignedOut",
        "onSoftwareUpdateStateChanged",
        "softwareUpdateState",
        "Lcom/squareup/statusbar/event/StatusBarEventManager$SoftwareUpdateState;",
        "onStatusBarAttachChanged",
        "onStatusBarAvailable",
        "available",
        "onViewClicked",
        "Lio/reactivex/Observable;",
        "impl-noop_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/statusbar/event/NoStatusBarEventManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 7
    new-instance v0, Lcom/squareup/statusbar/event/NoStatusBarEventManager;

    invoke-direct {v0}, Lcom/squareup/statusbar/event/NoStatusBarEventManager;-><init>()V

    sput-object v0, Lcom/squareup/statusbar/event/NoStatusBarEventManager;->INSTANCE:Lcom/squareup/statusbar/event/NoStatusBarEventManager;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public acceptViewClickEvent(Ljava/lang/String;)V
    .locals 1

    const-string v0, "action"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public enterFullscreenMode(Lio/reactivex/Completable;)V
    .locals 1

    const-string/jumbo v0, "until"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    invoke-static {p0, p1}, Lcom/squareup/statusbar/event/StatusBarEventManager$DefaultImpls;->enterFullscreenMode(Lcom/squareup/statusbar/event/StatusBarEventManager;Lio/reactivex/Completable;)V

    return-void
.end method

.method public enterFullscreenMode(Ljava/lang/String;)V
    .locals 1

    const-string v0, "sessionKey"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    invoke-static {p0, p1}, Lcom/squareup/statusbar/event/StatusBarEventManager$DefaultImpls;->enterFullscreenMode(Lcom/squareup/statusbar/event/StatusBarEventManager;Ljava/lang/String;)V

    return-void
.end method

.method public exitFullscreenMode(Ljava/lang/String;)V
    .locals 1

    const-string v0, "sessionKey"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    invoke-static {p0, p1}, Lcom/squareup/statusbar/event/StatusBarEventManager$DefaultImpls;->exitFullscreenMode(Lcom/squareup/statusbar/event/StatusBarEventManager;Ljava/lang/String;)V

    return-void
.end method

.method public inFullscreenMode()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .line 7
    invoke-static {p0}, Lcom/squareup/statusbar/event/StatusBarEventManager$DefaultImpls;->inFullscreenMode(Lcom/squareup/statusbar/event/StatusBarEventManager;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public launchSignInOrRegister()V
    .locals 0

    .line 7
    invoke-static {p0}, Lcom/squareup/statusbar/event/StatusBarEventManager$DefaultImpls;->launchSignInOrRegister(Lcom/squareup/statusbar/event/StatusBarEventManager;)V

    return-void
.end method

.method public onBadgesChanged(Ljava/lang/String;I)V
    .locals 1

    const-string v0, "applet"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    invoke-static {p0, p1, p2}, Lcom/squareup/statusbar/event/StatusBarEventManager$DefaultImpls;->onBadgesChanged(Lcom/squareup/statusbar/event/StatusBarEventManager;Ljava/lang/String;I)V

    return-void
.end method

.method public onBranStatusChanged(Lcom/squareup/statusbar/event/StatusBarEventManager$BranStatus;)V
    .locals 1

    const-string v0, "branStatus"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    invoke-static {p0, p1}, Lcom/squareup/statusbar/event/StatusBarEventManager$DefaultImpls;->onBranStatusChanged(Lcom/squareup/statusbar/event/StatusBarEventManager;Lcom/squareup/statusbar/event/StatusBarEventManager$BranStatus;)V

    return-void
.end method

.method public onChargerTypeChanged(Lcom/squareup/statusbar/event/StatusBarEventManager$ChargerType;)V
    .locals 1

    const-string v0, "chargerType"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    invoke-static {p0, p1}, Lcom/squareup/statusbar/event/StatusBarEventManager$DefaultImpls;->onChargerTypeChanged(Lcom/squareup/statusbar/event/StatusBarEventManager;Lcom/squareup/statusbar/event/StatusBarEventManager$ChargerType;)V

    return-void
.end method

.method public onConfigureDeviceButtonStateChanged(Lcom/squareup/statusbar/event/StatusBarEventManager$ConfigureDeviceButtonState;)V
    .locals 1

    const-string v0, "configureDeviceButtonState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    invoke-static {p0, p1}, Lcom/squareup/statusbar/event/StatusBarEventManager$DefaultImpls;->onConfigureDeviceButtonStateChanged(Lcom/squareup/statusbar/event/StatusBarEventManager;Lcom/squareup/statusbar/event/StatusBarEventManager$ConfigureDeviceButtonState;)V

    return-void
.end method

.method public onCriticalSoftwareUpdateAvailable(Z)V
    .locals 0

    .line 7
    invoke-static {p0, p1}, Lcom/squareup/statusbar/event/StatusBarEventManager$DefaultImpls;->onCriticalSoftwareUpdateAvailable(Lcom/squareup/statusbar/event/StatusBarEventManager;Z)V

    return-void
.end method

.method public onEditModeChanged(Z)V
    .locals 0

    .line 7
    invoke-static {p0, p1}, Lcom/squareup/statusbar/event/StatusBarEventManager$DefaultImpls;->onEditModeChanged(Lcom/squareup/statusbar/event/StatusBarEventManager;Z)V

    return-void
.end method

.method public onEmployeeManagementButtonStateChanged(Lcom/squareup/statusbar/event/StatusBarEventManager$EmployeeManagementButtonState;)V
    .locals 1

    const-string v0, "employeeManagementButtonState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    invoke-static {p0, p1}, Lcom/squareup/statusbar/event/StatusBarEventManager$DefaultImpls;->onEmployeeManagementButtonStateChanged(Lcom/squareup/statusbar/event/StatusBarEventManager;Lcom/squareup/statusbar/event/StatusBarEventManager$EmployeeManagementButtonState;)V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 1

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public onInternalPrinterOutOfPaper(Z)V
    .locals 0

    .line 7
    invoke-static {p0, p1}, Lcom/squareup/statusbar/event/StatusBarEventManager$DefaultImpls;->onInternalPrinterOutOfPaper(Lcom/squareup/statusbar/event/StatusBarEventManager;Z)V

    return-void
.end method

.method public onInternalPrinterUnrecoverableError(Z)V
    .locals 0

    .line 7
    invoke-static {p0, p1}, Lcom/squareup/statusbar/event/StatusBarEventManager$DefaultImpls;->onInternalPrinterUnrecoverableError(Lcom/squareup/statusbar/event/StatusBarEventManager;Z)V

    return-void
.end method

.method public onLowBatteryStatusChanged(Lcom/squareup/statusbar/event/StatusBarEventManager$LowBatteryDialogStatus;)V
    .locals 1

    const-string v0, "lowBatteryDialogStatus"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    invoke-static {p0, p1}, Lcom/squareup/statusbar/event/StatusBarEventManager$DefaultImpls;->onLowBatteryStatusChanged(Lcom/squareup/statusbar/event/StatusBarEventManager;Lcom/squareup/statusbar/event/StatusBarEventManager$LowBatteryDialogStatus;)V

    return-void
.end method

.method public onNotificationButtonStateChanged(Lcom/squareup/statusbar/event/StatusBarEventManager$NotificationButtonState;)V
    .locals 1

    const-string v0, "notificationButtonState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    invoke-static {p0, p1}, Lcom/squareup/statusbar/event/StatusBarEventManager$DefaultImpls;->onNotificationButtonStateChanged(Lcom/squareup/statusbar/event/StatusBarEventManager;Lcom/squareup/statusbar/event/StatusBarEventManager$NotificationButtonState;)V

    return-void
.end method

.method public onNotificationCenterStateChanged(Lcom/squareup/statusbar/event/StatusBarEventManager$NotificationCenterState;)V
    .locals 1

    const-string v0, "notificationCenterState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    invoke-static {p0, p1}, Lcom/squareup/statusbar/event/StatusBarEventManager$DefaultImpls;->onNotificationCenterStateChanged(Lcom/squareup/statusbar/event/StatusBarEventManager;Lcom/squareup/statusbar/event/StatusBarEventManager$NotificationCenterState;)V

    return-void
.end method

.method public onOfflineStateChanged(ZZ)V
    .locals 0

    return-void
.end method

.method public onOobFinished()V
    .locals 0

    .line 7
    invoke-static {p0}, Lcom/squareup/statusbar/event/StatusBarEventManager$DefaultImpls;->onOobFinished(Lcom/squareup/statusbar/event/StatusBarEventManager;)V

    return-void
.end method

.method public onPrinterDoorStateChanged(Z)V
    .locals 0

    .line 7
    invoke-static {p0, p1}, Lcom/squareup/statusbar/event/StatusBarEventManager$DefaultImpls;->onPrinterDoorStateChanged(Lcom/squareup/statusbar/event/StatusBarEventManager;Z)V

    return-void
.end method

.method public onSignedOut()V
    .locals 0

    return-void
.end method

.method public onSoftwareUpdateStateChanged(Lcom/squareup/statusbar/event/StatusBarEventManager$SoftwareUpdateState;)V
    .locals 1

    const-string v0, "softwareUpdateState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public onStatusBarAttachChanged()V
    .locals 0

    return-void
.end method

.method public onStatusBarAvailable(Z)V
    .locals 0

    return-void
.end method

.method public onSwitcherStateChanged(Lcom/squareup/statusbar/event/StatusBarEventManager$SwitcherState;)V
    .locals 1

    const-string v0, "switcherState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 7
    invoke-static {p0, p1}, Lcom/squareup/statusbar/event/StatusBarEventManager$DefaultImpls;->onSwitcherStateChanged(Lcom/squareup/statusbar/event/StatusBarEventManager;Lcom/squareup/statusbar/event/StatusBarEventManager$SwitcherState;)V

    return-void
.end method

.method public onViewClicked()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 25
    invoke-static {}, Lio/reactivex/Observable;->empty()Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "Observable.empty()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public showBfdUpdateStatusDialog()V
    .locals 0

    .line 7
    invoke-static {p0}, Lcom/squareup/statusbar/event/StatusBarEventManager$DefaultImpls;->showBfdUpdateStatusDialog(Lcom/squareup/statusbar/event/StatusBarEventManager;)V

    return-void
.end method

.method public showNotificationListDialog()V
    .locals 0

    .line 7
    invoke-static {p0}, Lcom/squareup/statusbar/event/StatusBarEventManager$DefaultImpls;->showNotificationListDialog(Lcom/squareup/statusbar/event/StatusBarEventManager;)V

    return-void
.end method
