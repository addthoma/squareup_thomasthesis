.class public abstract Lcom/squareup/time/TimeReleaseModule;
.super Ljava/lang/Object;
.source "TimeReleaseModule.kt"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/time/TimeCommonModule;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000H\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\'\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\'J\u0010\u0010\u0007\u001a\u00020\u00082\u0006\u0010\t\u001a\u00020\nH\'J\u0010\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\u000eH\'J\u0010\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012H\'J\u0010\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0016H\'\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/time/TimeReleaseModule;",
        "",
        "()V",
        "bindCurrent24HourClockMode",
        "Lcom/squareup/time/Current24HourClockMode;",
        "realCurrent24HourClockMode",
        "Lcom/squareup/time/RealCurrent24HourClockMode;",
        "bindCurrentTime",
        "Lcom/squareup/time/CurrentTime;",
        "realCurrentTime",
        "Lcom/squareup/time/RealCurrentTime;",
        "bindCurrentTimeZone",
        "Lcom/squareup/time/CurrentTimeZone;",
        "realCurrentTimeZone",
        "Lcom/squareup/time/RealCurrentTimeZone;",
        "bindTimeZoneFormatter",
        "Lcom/squareup/time/TimeZoneFormatter;",
        "timeZoneFormatter",
        "Lcom/squareup/time/RealTimeZoneFormatter;",
        "bindTimeZoneSetter",
        "Lcom/squareup/time/TimeZoneSetter;",
        "timeZoneSetter",
        "Lcom/squareup/time/RealTimeZoneSetter;",
        "impl-wiring_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract bindCurrent24HourClockMode(Lcom/squareup/time/RealCurrent24HourClockMode;)Lcom/squareup/time/Current24HourClockMode;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindCurrentTime(Lcom/squareup/time/RealCurrentTime;)Lcom/squareup/time/CurrentTime;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindCurrentTimeZone(Lcom/squareup/time/RealCurrentTimeZone;)Lcom/squareup/time/CurrentTimeZone;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindTimeZoneFormatter(Lcom/squareup/time/RealTimeZoneFormatter;)Lcom/squareup/time/TimeZoneFormatter;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method public abstract bindTimeZoneSetter(Lcom/squareup/time/RealTimeZoneSetter;)Lcom/squareup/time/TimeZoneSetter;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
