.class public Lcom/squareup/sqwidgets/ConfirmDialog;
.super Ljava/lang/Object;
.source "ConfirmDialog.java"


# instance fields
.field private final confirmButton:Landroid/widget/TextView;

.field private final context:Landroid/content/Context;

.field private final dialog:Landroid/app/Dialog;

.field private final dialogIcon:Landroid/widget/ImageView;

.field private final dismissButton:Landroid/widget/TextView;

.field private final messageText:Landroid/widget/TextView;

.field private final titleText:Landroid/widget/TextView;

.field private type:Lcom/squareup/sqwidgets/DialogType;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    .line 32
    invoke-direct {p0, p1, v0}, Lcom/squareup/sqwidgets/ConfirmDialog;-><init>(Landroid/content/Context;Z)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 2

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/squareup/sqwidgets/ConfirmDialog;->context:Landroid/content/Context;

    .line 37
    new-instance v0, Landroid/app/Dialog;

    invoke-direct {v0, p1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/squareup/sqwidgets/ConfirmDialog;->dialog:Landroid/app/Dialog;

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    .line 39
    iget-object p2, p0, Lcom/squareup/sqwidgets/ConfirmDialog;->dialog:Landroid/app/Dialog;

    invoke-virtual {p2}, Landroid/app/Dialog;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object p2

    sget v1, Lcom/squareup/sqwidgets/R$layout;->warning_confirm_dialog_layout:I

    .line 40
    invoke-virtual {p2, v1, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    goto :goto_0

    :cond_0
    iget-object p2, p0, Lcom/squareup/sqwidgets/ConfirmDialog;->dialog:Landroid/app/Dialog;

    .line 41
    invoke-virtual {p2}, Landroid/app/Dialog;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object p2

    sget v1, Lcom/squareup/sqwidgets/R$layout;->confirm_dialog_layout:I

    invoke-virtual {p2, v1, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 42
    :goto_0
    iget-object v0, p0, Lcom/squareup/sqwidgets/ConfirmDialog;->dialog:Landroid/app/Dialog;

    invoke-virtual {v0, p2}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 43
    iget-object v0, p0, Lcom/squareup/sqwidgets/ConfirmDialog;->dialog:Landroid/app/Dialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 45
    sget v0, Lcom/squareup/sqwidgets/R$id;->dialog_icon:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/squareup/sqwidgets/ConfirmDialog;->dialogIcon:Landroid/widget/ImageView;

    .line 46
    sget v0, Lcom/squareup/sqwidgets/R$id;->title_text:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/sqwidgets/ConfirmDialog;->titleText:Landroid/widget/TextView;

    .line 47
    sget v0, Lcom/squareup/sqwidgets/R$id;->message_text:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/sqwidgets/ConfirmDialog;->messageText:Landroid/widget/TextView;

    .line 48
    sget v0, Lcom/squareup/sqwidgets/R$id;->dismiss_button:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/sqwidgets/ConfirmDialog;->dismissButton:Landroid/widget/TextView;

    .line 49
    sget v0, Lcom/squareup/sqwidgets/R$id;->confirm_button:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/TextView;

    iput-object p2, p0, Lcom/squareup/sqwidgets/ConfirmDialog;->confirmButton:Landroid/widget/TextView;

    .line 51
    iget-object p2, p0, Lcom/squareup/sqwidgets/ConfirmDialog;->dismissButton:Landroid/widget/TextView;

    new-instance v0, Lcom/squareup/sqwidgets/-$$Lambda$ConfirmDialog$V0ezigIFw9VoDXQIz7cLb-QJafE;

    invoke-direct {v0, p0}, Lcom/squareup/sqwidgets/-$$Lambda$ConfirmDialog$V0ezigIFw9VoDXQIz7cLb-QJafE;-><init>(Lcom/squareup/sqwidgets/ConfirmDialog;)V

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 53
    iget-object p2, p0, Lcom/squareup/sqwidgets/ConfirmDialog;->confirmButton:Landroid/widget/TextView;

    new-instance v0, Lcom/squareup/sqwidgets/-$$Lambda$ConfirmDialog$k0k_jUXFd8hBi7dReEE0dx-aFro;

    invoke-direct {v0, p0}, Lcom/squareup/sqwidgets/-$$Lambda$ConfirmDialog$k0k_jUXFd8hBi7dReEE0dx-aFro;-><init>(Lcom/squareup/sqwidgets/ConfirmDialog;)V

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 55
    invoke-static {p1}, Lcom/squareup/sqwidgets/DialogType;->forContext(Landroid/content/Context;)Lcom/squareup/sqwidgets/DialogType;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/sqwidgets/ConfirmDialog;->type:Lcom/squareup/sqwidgets/DialogType;

    return-void
.end method

.method private setConfirmBackgroundColor(I)V
    .locals 2

    .line 192
    iget-object v0, p0, Lcom/squareup/sqwidgets/ConfirmDialog;->confirmButton:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/squareup/sqwidgets/ConfirmDialog;->context:Landroid/content/Context;

    invoke-static {v1, p1}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setBackgroundColor(I)V

    return-void
.end method

.method private setConfirmTextColor(I)V
    .locals 2

    .line 196
    iget-object v0, p0, Lcom/squareup/sqwidgets/ConfirmDialog;->confirmButton:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/squareup/sqwidgets/ConfirmDialog;->context:Landroid/content/Context;

    invoke-static {v1, p1}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method

.method private setDismissBackground(I)V
    .locals 1

    .line 200
    iget-object v0, p0, Lcom/squareup/sqwidgets/ConfirmDialog;->dismissButton:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    return-void
.end method

.method private setImage(I)V
    .locals 1

    .line 204
    iget-object v0, p0, Lcom/squareup/sqwidgets/ConfirmDialog;->dialogIcon:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 205
    iget-object p1, p0, Lcom/squareup/sqwidgets/ConfirmDialog;->dialogIcon:Landroid/widget/ImageView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void
.end method

.method private setWindowTypeAndLayout()V
    .locals 3

    .line 184
    iget-object v0, p0, Lcom/squareup/sqwidgets/ConfirmDialog;->dialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 185
    iget-object v1, p0, Lcom/squareup/sqwidgets/ConfirmDialog;->type:Lcom/squareup/sqwidgets/DialogType;

    invoke-virtual {v1, v0}, Lcom/squareup/sqwidgets/DialogType;->applyTypeForWindow(Landroid/view/Window;)V

    .line 186
    iget-object v1, p0, Lcom/squareup/sqwidgets/ConfirmDialog;->context:Landroid/content/Context;

    .line 187
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/squareup/noho/R$dimen;->noho_dialog_width:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    const/4 v2, -0x2

    .line 186
    invoke-virtual {v0, v1, v2}, Landroid/view/Window;->setLayout(II)V

    return-void
.end method


# virtual methods
.method public confirm()V
    .locals 1

    .line 159
    iget-object v0, p0, Lcom/squareup/sqwidgets/ConfirmDialog;->confirmButton:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->performClick()Z

    return-void
.end method

.method public dismiss()V
    .locals 1

    .line 178
    iget-object v0, p0, Lcom/squareup/sqwidgets/ConfirmDialog;->dialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 179
    iget-object v0, p0, Lcom/squareup/sqwidgets/ConfirmDialog;->dialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    :cond_0
    return-void
.end method

.method public getDialog()Landroid/app/Dialog;
    .locals 1

    .line 167
    invoke-direct {p0}, Lcom/squareup/sqwidgets/ConfirmDialog;->setWindowTypeAndLayout()V

    .line 168
    iget-object v0, p0, Lcom/squareup/sqwidgets/ConfirmDialog;->dialog:Landroid/app/Dialog;

    return-object v0
.end method

.method public hideCancelButton()Lcom/squareup/sqwidgets/ConfirmDialog;
    .locals 2

    .line 112
    iget-object v0, p0, Lcom/squareup/sqwidgets/ConfirmDialog;->dismissButton:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-object p0
.end method

.method public isDismissed()Z
    .locals 1

    .line 163
    iget-object v0, p0, Lcom/squareup/sqwidgets/ConfirmDialog;->dialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public synthetic lambda$new$0$ConfirmDialog(Landroid/view/View;)V
    .locals 0

    .line 51
    iget-object p1, p0, Lcom/squareup/sqwidgets/ConfirmDialog;->dialog:Landroid/app/Dialog;

    invoke-virtual {p1}, Landroid/app/Dialog;->dismiss()V

    return-void
.end method

.method public synthetic lambda$new$1$ConfirmDialog(Landroid/view/View;)V
    .locals 0

    .line 53
    iget-object p1, p0, Lcom/squareup/sqwidgets/ConfirmDialog;->dialog:Landroid/app/Dialog;

    invoke-virtual {p1}, Landroid/app/Dialog;->dismiss()V

    return-void
.end method

.method public synthetic lambda$setOnConfirmed$2$ConfirmDialog(Ljava/lang/Runnable;Landroid/view/View;)V
    .locals 0

    .line 133
    iget-object p2, p0, Lcom/squareup/sqwidgets/ConfirmDialog;->dialog:Landroid/app/Dialog;

    invoke-virtual {p2}, Landroid/app/Dialog;->dismiss()V

    .line 134
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    return-void
.end method

.method public synthetic lambda$setOnConfirmedBeforeDialogDismiss$3$ConfirmDialog(Ljava/lang/Runnable;Landroid/view/View;)V
    .locals 0

    .line 142
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 143
    iget-object p1, p0, Lcom/squareup/sqwidgets/ConfirmDialog;->dialog:Landroid/app/Dialog;

    invoke-virtual {p1}, Landroid/app/Dialog;->dismiss()V

    return-void
.end method

.method public synthetic lambda$setOnDismissedBeforeDialogDismiss$4$ConfirmDialog(Ljava/lang/Runnable;Landroid/view/View;)V
    .locals 0

    .line 151
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 152
    iget-object p1, p0, Lcom/squareup/sqwidgets/ConfirmDialog;->dialog:Landroid/app/Dialog;

    invoke-virtual {p1}, Landroid/app/Dialog;->dismiss()V

    return-void
.end method

.method public setBlueConfirmButtonAndText(I)Lcom/squareup/sqwidgets/ConfirmDialog;
    .locals 0

    .line 89
    invoke-virtual {p0, p1}, Lcom/squareup/sqwidgets/ConfirmDialog;->setConfirmText(I)Lcom/squareup/sqwidgets/ConfirmDialog;

    .line 90
    sget p1, Lcom/squareup/marin/R$color;->marin_blue:I

    invoke-direct {p0, p1}, Lcom/squareup/sqwidgets/ConfirmDialog;->setConfirmBackgroundColor(I)V

    .line 91
    sget p1, Lcom/squareup/marin/R$color;->marin_white:I

    invoke-direct {p0, p1}, Lcom/squareup/sqwidgets/ConfirmDialog;->setConfirmTextColor(I)V

    return-object p0
.end method

.method public setCanceledOnTouchOutside(Z)Lcom/squareup/sqwidgets/ConfirmDialog;
    .locals 1

    .line 117
    iget-object v0, p0, Lcom/squareup/sqwidgets/ConfirmDialog;->dialog:Landroid/app/Dialog;

    invoke-virtual {v0, p1}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    return-object p0
.end method

.method public setConfirmText(I)Lcom/squareup/sqwidgets/ConfirmDialog;
    .locals 1

    .line 79
    iget-object v0, p0, Lcom/squareup/sqwidgets/ConfirmDialog;->confirmButton:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    return-object p0
.end method

.method public setConfirmText(Ljava/lang/String;)Lcom/squareup/sqwidgets/ConfirmDialog;
    .locals 1

    .line 84
    iget-object v0, p0, Lcom/squareup/sqwidgets/ConfirmDialog;->confirmButton:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-object p0
.end method

.method public setDismissText(I)Lcom/squareup/sqwidgets/ConfirmDialog;
    .locals 1

    .line 96
    iget-object v0, p0, Lcom/squareup/sqwidgets/ConfirmDialog;->dismissButton:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    return-object p0
.end method

.method public setMessage(I)Lcom/squareup/sqwidgets/ConfirmDialog;
    .locals 1

    .line 69
    iget-object v0, p0, Lcom/squareup/sqwidgets/ConfirmDialog;->messageText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    return-object p0
.end method

.method public setMessage(Ljava/lang/CharSequence;)Lcom/squareup/sqwidgets/ConfirmDialog;
    .locals 1

    .line 74
    iget-object v0, p0, Lcom/squareup/sqwidgets/ConfirmDialog;->messageText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-object p0
.end method

.method public setOnConfirmed(Ljava/lang/Runnable;)Lcom/squareup/sqwidgets/ConfirmDialog;
    .locals 2

    .line 132
    iget-object v0, p0, Lcom/squareup/sqwidgets/ConfirmDialog;->confirmButton:Landroid/widget/TextView;

    new-instance v1, Lcom/squareup/sqwidgets/-$$Lambda$ConfirmDialog$ztTj11Nnu1BT9aCY3_65RFQ_s-E;

    invoke-direct {v1, p0, p1}, Lcom/squareup/sqwidgets/-$$Lambda$ConfirmDialog$ztTj11Nnu1BT9aCY3_65RFQ_s-E;-><init>(Lcom/squareup/sqwidgets/ConfirmDialog;Ljava/lang/Runnable;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object p0
.end method

.method public setOnConfirmedBeforeDialogDismiss(Ljava/lang/Runnable;)Lcom/squareup/sqwidgets/ConfirmDialog;
    .locals 2

    .line 141
    iget-object v0, p0, Lcom/squareup/sqwidgets/ConfirmDialog;->confirmButton:Landroid/widget/TextView;

    new-instance v1, Lcom/squareup/sqwidgets/-$$Lambda$ConfirmDialog$MMBcDAqK_jkl2EQQlTMMfeWdyjI;

    invoke-direct {v1, p0, p1}, Lcom/squareup/sqwidgets/-$$Lambda$ConfirmDialog$MMBcDAqK_jkl2EQQlTMMfeWdyjI;-><init>(Lcom/squareup/sqwidgets/ConfirmDialog;Ljava/lang/Runnable;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object p0
.end method

.method public setOnDismissedBeforeDialogDismiss(Ljava/lang/Runnable;)Lcom/squareup/sqwidgets/ConfirmDialog;
    .locals 2

    .line 150
    iget-object v0, p0, Lcom/squareup/sqwidgets/ConfirmDialog;->dismissButton:Landroid/widget/TextView;

    new-instance v1, Lcom/squareup/sqwidgets/-$$Lambda$ConfirmDialog$FE99tsBZchA2Q4boIiXNtU3QR74;

    invoke-direct {v1, p0, p1}, Lcom/squareup/sqwidgets/-$$Lambda$ConfirmDialog$FE99tsBZchA2Q4boIiXNtU3QR74;-><init>(Lcom/squareup/sqwidgets/ConfirmDialog;Ljava/lang/Runnable;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object p0
.end method

.method public setTitle(I)Lcom/squareup/sqwidgets/ConfirmDialog;
    .locals 1

    .line 59
    iget-object v0, p0, Lcom/squareup/sqwidgets/ConfirmDialog;->titleText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    return-object p0
.end method

.method public setTitle(Ljava/lang/CharSequence;)Lcom/squareup/sqwidgets/ConfirmDialog;
    .locals 1

    .line 64
    iget-object v0, p0, Lcom/squareup/sqwidgets/ConfirmDialog;->titleText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-object p0
.end method

.method public setType(Lcom/squareup/sqwidgets/DialogType;)Lcom/squareup/sqwidgets/ConfirmDialog;
    .locals 0

    .line 122
    iput-object p1, p0, Lcom/squareup/sqwidgets/ConfirmDialog;->type:Lcom/squareup/sqwidgets/DialogType;

    return-object p0
.end method

.method public setWhiteDismissButton()Lcom/squareup/sqwidgets/ConfirmDialog;
    .locals 1

    .line 101
    sget v0, Lcom/squareup/marin/R$drawable;->marin_white_border_light_gray:I

    invoke-direct {p0, v0}, Lcom/squareup/sqwidgets/ConfirmDialog;->setDismissBackground(I)V

    return-object p0
.end method

.method public setWhiteDismissButtonAndText(I)Lcom/squareup/sqwidgets/ConfirmDialog;
    .locals 0

    .line 106
    invoke-virtual {p0, p1}, Lcom/squareup/sqwidgets/ConfirmDialog;->setDismissText(I)Lcom/squareup/sqwidgets/ConfirmDialog;

    .line 107
    sget p1, Lcom/squareup/marin/R$drawable;->marin_white_border_light_gray:I

    invoke-direct {p0, p1}, Lcom/squareup/sqwidgets/ConfirmDialog;->setDismissBackground(I)V

    return-object p0
.end method

.method public show()Lcom/squareup/sqwidgets/ConfirmDialog;
    .locals 1

    .line 172
    invoke-direct {p0}, Lcom/squareup/sqwidgets/ConfirmDialog;->setWindowTypeAndLayout()V

    .line 173
    iget-object v0, p0, Lcom/squareup/sqwidgets/ConfirmDialog;->dialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return-object p0
.end method

.method public showLogo()Lcom/squareup/sqwidgets/ConfirmDialog;
    .locals 1

    .line 127
    sget v0, Lcom/squareup/sqwidgets/R$drawable;->logo_dialog:I

    invoke-direct {p0, v0}, Lcom/squareup/sqwidgets/ConfirmDialog;->setImage(I)V

    return-object p0
.end method
