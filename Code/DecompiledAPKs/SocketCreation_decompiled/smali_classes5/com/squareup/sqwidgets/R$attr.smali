.class public final Lcom/squareup/sqwidgets/R$attr;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/sqwidgets/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "attr"
.end annotation


# static fields
.field public static final applet:I = 0x7f040035

.field public static final buttonBackground:I = 0x7f040077

.field public static final buttonHeight:I = 0x7f040083

.field public static final cellHeight:I = 0x7f040099

.field public static final cellStyle:I = 0x7f04009a

.field public static final cellWidth:I = 0x7f04009b

.field public static final confirmStageBackground:I = 0x7f0400ec

.field public static final confirmStageText:I = 0x7f0400ed

.field public static final confirmStageTextColor:I = 0x7f0400ee

.field public static final dialogButtonCancelStyle:I = 0x7f04011d

.field public static final dialogButtonConfirmStyle:I = 0x7f04011e

.field public static final dialogMessageStyle:I = 0x7f040120

.field public static final dialogTitleStyle:I = 0x7f040123

.field public static final editTextPasswordStyle:I = 0x7f04014f

.field public static final glyph:I = 0x7f0401a0

.field public static final highlightIfImportant:I = 0x7f0401bd

.field public static final importantColor:I = 0x7f04020a

.field public static final initialStageBackground:I = 0x7f040213

.field public static final initialStageText:I = 0x7f040214

.field public static final initialStageTextColor:I = 0x7f040215

.field public static final overlayBackground:I = 0x7f040305

.field public static final paddingSides:I = 0x7f040308

.field public static final statusBarFontSize:I = 0x7f0403ec

.field public static final statusBarGlyphFontSize:I = 0x7f0403ee

.field public static final statusBarHeight:I = 0x7f0403ef

.field public static final statusBarIconHeight:I = 0x7f0403f0

.field public static final statusBarIconPaddingHorizontal:I = 0x7f0403f1

.field public static final statusBarPaddingFull:I = 0x7f0403f2

.field public static final statusBarPaddingHalf:I = 0x7f0403f3

.field public static final statusBarPaddingTiny:I = 0x7f0403f4

.field public static final summaryMessagePlural:I = 0x7f040408

.field public static final summaryMessageSingular:I = 0x7f040409

.field public static final weight:I = 0x7f040497

.field public static final wifiRowBackground:I = 0x7f040498


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
