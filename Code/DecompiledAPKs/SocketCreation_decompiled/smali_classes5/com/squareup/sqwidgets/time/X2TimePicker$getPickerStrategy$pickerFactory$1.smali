.class final Lcom/squareup/sqwidgets/time/X2TimePicker$getPickerStrategy$pickerFactory$1;
.super Lkotlin/jvm/internal/Lambda;
.source "X2TimePicker.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function3;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/sqwidgets/time/X2TimePicker;->getPickerStrategy(Lcom/squareup/sqwidgets/time/TimeFormatChooser;)Lcom/squareup/sqwidgets/time/PickerStrategy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function3<",
        "Ljava/lang/Integer;",
        "Ljava/util/List<",
        "+",
        "Ljava/lang/String;",
        ">;",
        "Ljava/lang/Boolean;",
        "Lcom/squareup/sqwidgets/time/Picker;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00032\u000c\u0010\u0004\u001a\u0008\u0012\u0004\u0012\u00020\u00060\u00052\u0006\u0010\u0007\u001a\u00020\u0008H\n\u00a2\u0006\u0002\u0008\t"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/sqwidgets/time/Picker;",
        "id",
        "",
        "data",
        "",
        "",
        "isInfiniteScroll",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/sqwidgets/time/X2TimePicker;


# direct methods
.method constructor <init>(Lcom/squareup/sqwidgets/time/X2TimePicker;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/sqwidgets/time/X2TimePicker$getPickerStrategy$pickerFactory$1;->this$0:Lcom/squareup/sqwidgets/time/X2TimePicker;

    const/4 p1, 0x3

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(ILjava/util/List;Z)Lcom/squareup/sqwidgets/time/Picker;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;Z)",
            "Lcom/squareup/sqwidgets/time/Picker;"
        }
    .end annotation

    const-string v0, "data"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 91
    new-instance v0, Lcom/squareup/sqwidgets/time/Picker;

    .line 92
    iget-object v1, p0, Lcom/squareup/sqwidgets/time/X2TimePicker$getPickerStrategy$pickerFactory$1;->this$0:Lcom/squareup/sqwidgets/time/X2TimePicker;

    invoke-virtual {v1}, Lcom/squareup/sqwidgets/time/X2TimePicker;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 93
    new-instance v2, Lcom/squareup/sqwidgets/time/DataAdapter;

    .line 94
    iget-object v3, p0, Lcom/squareup/sqwidgets/time/X2TimePicker$getPickerStrategy$pickerFactory$1;->this$0:Lcom/squareup/sqwidgets/time/X2TimePicker;

    invoke-virtual {v3}, Lcom/squareup/sqwidgets/time/X2TimePicker;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 95
    check-cast p2, Ljava/util/Collection;

    if-eqz p3, :cond_0

    const/16 p3, 0x3e8

    goto :goto_0

    :cond_0
    const/4 p3, 0x1

    .line 97
    :goto_0
    iget-object v4, p0, Lcom/squareup/sqwidgets/time/X2TimePicker$getPickerStrategy$pickerFactory$1;->this$0:Lcom/squareup/sqwidgets/time/X2TimePicker;

    invoke-static {v4}, Lcom/squareup/sqwidgets/time/X2TimePicker;->access$getCellStyleId$p(Lcom/squareup/sqwidgets/time/X2TimePicker;)I

    move-result v4

    .line 93
    invoke-direct {v2, v3, p2, p3, v4}, Lcom/squareup/sqwidgets/time/DataAdapter;-><init>(Landroid/content/Context;Ljava/util/Collection;II)V

    .line 99
    iget-object p2, p0, Lcom/squareup/sqwidgets/time/X2TimePicker$getPickerStrategy$pickerFactory$1;->this$0:Lcom/squareup/sqwidgets/time/X2TimePicker;

    invoke-virtual {p2, p1}, Lcom/squareup/sqwidgets/time/X2TimePicker;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroidx/recyclerview/widget/RecyclerView;

    .line 91
    invoke-direct {v0, v1, v2, p1}, Lcom/squareup/sqwidgets/time/Picker;-><init>(Landroid/content/Context;Lcom/squareup/sqwidgets/time/DataAdapter;Landroidx/recyclerview/widget/RecyclerView;)V

    return-object v0
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 17
    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    check-cast p2, Ljava/util/List;

    check-cast p3, Ljava/lang/Boolean;

    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p3

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/sqwidgets/time/X2TimePicker$getPickerStrategy$pickerFactory$1;->invoke(ILjava/util/List;Z)Lcom/squareup/sqwidgets/time/Picker;

    move-result-object p1

    return-object p1
.end method
