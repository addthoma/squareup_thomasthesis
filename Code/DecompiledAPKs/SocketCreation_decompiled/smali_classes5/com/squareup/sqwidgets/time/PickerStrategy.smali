.class public abstract Lcom/squareup/sqwidgets/time/PickerStrategy;
.super Ljava/lang/Object;
.source "PickerStrategy.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000N\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0006\n\u0002\u0018\u0002\n\u0002\u0008\u0008\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0008 \u0018\u00002\u00020\u0001B\u0080\u0001\u0012U\u0010\u0002\u001aQ\u0012\u0013\u0012\u00110\u0004\u00a2\u0006\u000c\u0008\u0005\u0012\u0008\u0008\u0006\u0012\u0004\u0008\u0008(\u0007\u0012\u0019\u0012\u0017\u0012\u0004\u0012\u00020\t0\u0008\u00a2\u0006\u000c\u0008\u0005\u0012\u0008\u0008\u0006\u0012\u0004\u0008\u0008(\n\u0012\u0013\u0012\u00110\u000b\u00a2\u0006\u000c\u0008\u0005\u0012\u0008\u0008\u0006\u0012\u0004\u0008\u0008(\u000c\u0012\u0004\u0012\u00020\r0\u0003j\u0002`\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0004\u0012\u0006\u0010\u0010\u001a\u00020\u0004\u0012\u0008\u0008\u0001\u0010\u0011\u001a\u00020\u0004\u0012\u0008\u0008\u0001\u0010\u0012\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0013J\u0010\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020\u001eH&R\u0014\u0010\u0014\u001a\u00020\u0015X\u0084\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0016\u0010\u0017R\u0011\u0010\u0018\u001a\u00020\u00048F\u00a2\u0006\u0006\u001a\u0004\u0008\u0019\u0010\u001aR\u0011\u0010\u001b\u001a\u00020\u00048F\u00a2\u0006\u0006\u001a\u0004\u0008\u001c\u0010\u001aR\u0012\u0010\u001d\u001a\u00020\u001eX\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u001f\u0010 \u00a8\u0006$"
    }
    d2 = {
        "Lcom/squareup/sqwidgets/time/PickerStrategy;",
        "",
        "pickerFactory",
        "Lkotlin/Function3;",
        "",
        "Lkotlin/ParameterName;",
        "name",
        "id",
        "",
        "",
        "values",
        "",
        "isInfiniteScroll",
        "Lcom/squareup/sqwidgets/time/Picker;",
        "Lcom/squareup/sqwidgets/time/PickerFactory;",
        "hourPickerStart",
        "hourPickerEnd",
        "hourPickerId",
        "minutePickerId",
        "(Lkotlin/jvm/functions/Function3;IIII)V",
        "hourAndMinutePickers",
        "Lcom/squareup/sqwidgets/time/HourAndMinutePickers;",
        "getHourAndMinutePickers",
        "()Lcom/squareup/sqwidgets/time/HourAndMinutePickers;",
        "hourPickerHeight",
        "getHourPickerHeight",
        "()I",
        "hourPickerWidth",
        "getHourPickerWidth",
        "time",
        "Ljava/util/Calendar;",
        "getTime",
        "()Ljava/util/Calendar;",
        "setDisplayedTime",
        "",
        "currentTime",
        "x2widgets_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final hourAndMinutePickers:Lcom/squareup/sqwidgets/time/HourAndMinutePickers;


# direct methods
.method public constructor <init>(Lkotlin/jvm/functions/Function3;IIII)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function3<",
            "-",
            "Ljava/lang/Integer;",
            "-",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;-",
            "Ljava/lang/Boolean;",
            "Lcom/squareup/sqwidgets/time/Picker;",
            ">;IIII)V"
        }
    .end annotation

    const-string v0, "pickerFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Lcom/squareup/sqwidgets/time/HourAndMinutePickers;

    move-object v1, v0

    move-object v2, p1

    move v3, p4

    move v4, p5

    move v5, p2

    move v6, p3

    invoke-direct/range {v1 .. v6}, Lcom/squareup/sqwidgets/time/HourAndMinutePickers;-><init>(Lkotlin/jvm/functions/Function3;IIII)V

    iput-object v0, p0, Lcom/squareup/sqwidgets/time/PickerStrategy;->hourAndMinutePickers:Lcom/squareup/sqwidgets/time/HourAndMinutePickers;

    return-void
.end method


# virtual methods
.method protected final getHourAndMinutePickers()Lcom/squareup/sqwidgets/time/HourAndMinutePickers;
    .locals 1

    .line 22
    iget-object v0, p0, Lcom/squareup/sqwidgets/time/PickerStrategy;->hourAndMinutePickers:Lcom/squareup/sqwidgets/time/HourAndMinutePickers;

    return-object v0
.end method

.method public final getHourPickerHeight()I
    .locals 1

    .line 34
    iget-object v0, p0, Lcom/squareup/sqwidgets/time/PickerStrategy;->hourAndMinutePickers:Lcom/squareup/sqwidgets/time/HourAndMinutePickers;

    invoke-virtual {v0}, Lcom/squareup/sqwidgets/time/HourAndMinutePickers;->getHourPickerHeight()I

    move-result v0

    return v0
.end method

.method public final getHourPickerWidth()I
    .locals 1

    .line 31
    iget-object v0, p0, Lcom/squareup/sqwidgets/time/PickerStrategy;->hourAndMinutePickers:Lcom/squareup/sqwidgets/time/HourAndMinutePickers;

    invoke-virtual {v0}, Lcom/squareup/sqwidgets/time/HourAndMinutePickers;->getHourPickerWidth()I

    move-result v0

    return v0
.end method

.method public abstract getTime()Ljava/util/Calendar;
.end method

.method public abstract setDisplayedTime(Ljava/util/Calendar;)V
.end method
