.class public final Lcom/squareup/sqwidgets/R$style;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/sqwidgets/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "style"
.end annotation


# static fields
.field public static final SlideDownButtonRow:I = 0x7f1301b3

.field public static final SlideDownContent:I = 0x7f1301b4

.field public static final StatusBar:I = 0x7f1301bc

.field public static final StatusBarControl:I = 0x7f1301bd

.field public static final StatusBarControl_Icon:I = 0x7f1301be

.field public static final StatusBarControl_Icon_TextView:I = 0x7f1301bf

.field public static final StatusBarControl_Left:I = 0x7f1301c0

.field public static final StatusBarControl_Left_TextView:I = 0x7f1301c1

.field public static final StatusBarControl_Right:I = 0x7f1301c2

.field public static final StatusBarControl_Right_TextView:I = 0x7f1301c3

.field public static final StatusBarGlyph:I = 0x7f1301c4

.field public static final StatusBarView:I = 0x7f1301c5

.field public static final SwitcherButtonLayout:I = 0x7f1301c9

.field public static final SwitcherButtonTextView:I = 0x7f1301ca

.field public static final SwitcherButtonTextView_Notification:I = 0x7f1301cb

.field public static final SwitcherButtonTextView_Title:I = 0x7f1301cc

.field public static final SwitcherButtonVerticalStrip:I = 0x7f1301cd

.field public static final TextAppearance_Marin_Red:I = 0x7f13025e

.field public static final TextAppearance_Marin_TransparentBackground:I = 0x7f130270

.field public static final TextAppearance_Marin_TransparentBackground_Gray:I = 0x7f130271

.field public static final TextAppearance_Marin_TransparentBackground_Gray_Medium:I = 0x7f130272

.field public static final TextAppearance_Marin_TransparentBackground_White:I = 0x7f130273

.field public static final TextAppearance_Marin_TransparentBackground_White_Medium:I = 0x7f130274

.field public static final Theme_Marin_X2:I = 0x7f130305

.field public static final Theme_Marin_X2Dialog:I = 0x7f130307

.field public static final Theme_Marin_X2_Dark:I = 0x7f130306

.field public static final Theme_Noho_DatePicker_X2:I = 0x7f13033b

.field public static final Theme_SwitcherTheme:I = 0x7f130342

.field public static final Theme_SwitcherTheme_StockAndroid:I = 0x7f130343

.field public static final Widget_Marin_ConfirmButton:I = 0x7f130440

.field public static final Widget_Marin_Dialog_Message:I = 0x7f130442

.field public static final Widget_Marin_Dialog_Title:I = 0x7f130443

.field public static final Widget_Marin_EditText_X2:I = 0x7f130451

.field public static final Widget_Marin_EditText_X2_Dark:I = 0x7f130452

.field public static final Widget_Marin_EditText_X2_TextBox:I = 0x7f130453

.field public static final Widget_Marin_EditText_X2_TextBox_Dark:I = 0x7f130454

.field public static final Widget_Marin_ListView_NoPadding_X2:I = 0x7f1304ae

.field public static final Widget_Marin_ListView_NoPadding_X2_LightGray:I = 0x7f1304af

.field public static final Widget_Marin_ProgressBar_Dark_Medium:I = 0x7f1304b5

.field public static final Widget_Marin_SeekBar:I = 0x7f1304c1

.field public static final Widget_Marin_TimePicker:I = 0x7f1304dd

.field public static final Widget_Marin_TimePickerCell:I = 0x7f1304de

.field public static final Widget_Marin_WifiNameRow:I = 0x7f1304e0

.field public static final Widget_Marin_WifiNameRow_Dark:I = 0x7f1304e1

.field public static final Widget_Marin_X2LinearLayout_Vertical:I = 0x7f1304e2


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 168
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
