.class public final Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClickSequence$ClickSequenceReset;
.super Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClickSequence;
.source "ClickSequenceTrigger.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClickSequence;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ClickSequenceReset"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClickSequence$ClickSequenceReset;",
        "Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClickSequence;",
        "()V",
        "x2widgets_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClickSequence$ClickSequenceReset;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 138
    new-instance v0, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClickSequence$ClickSequenceReset;

    invoke-direct {v0}, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClickSequence$ClickSequenceReset;-><init>()V

    sput-object v0, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClickSequence$ClickSequenceReset;->INSTANCE:Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClickSequence$ClickSequenceReset;

    return-void
.end method

.method private constructor <init>()V
    .locals 7

    const/4 v1, 0x0

    const/4 v2, 0x0

    const-wide/16 v3, 0x0

    const/4 v5, 0x7

    const/4 v6, 0x0

    move-object v0, p0

    .line 138
    invoke-direct/range {v0 .. v6}, Lcom/squareup/sqwidgets/ui/ClickSequenceTrigger$ClickSequence;-><init>(IIJILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method
