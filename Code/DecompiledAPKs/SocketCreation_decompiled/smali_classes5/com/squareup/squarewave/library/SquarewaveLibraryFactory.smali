.class public interface abstract Lcom/squareup/squarewave/library/SquarewaveLibraryFactory;
.super Ljava/lang/Object;
.source "SquarewaveLibraryFactory.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J \u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\tH&\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/squarewave/library/SquarewaveLibraryFactory;",
        "",
        "createSquarewave",
        "Lcom/squareup/squarewave/library/SquarewaveLibrary;",
        "messenger",
        "Lcom/squareup/cardreader/SingleCardreaderMessenger;",
        "cardreaderConnectionId",
        "Lcom/squareup/cardreader/CardreaderConnectionId;",
        "readerTypeProvider",
        "Lcom/squareup/wavpool/swipe/ReaderTypeProvider;",
        "squarewave_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract createSquarewave(Lcom/squareup/cardreader/SingleCardreaderMessenger;Lcom/squareup/cardreader/CardreaderConnectionId;Lcom/squareup/wavpool/swipe/ReaderTypeProvider;)Lcom/squareup/squarewave/library/SquarewaveLibrary;
.end method
