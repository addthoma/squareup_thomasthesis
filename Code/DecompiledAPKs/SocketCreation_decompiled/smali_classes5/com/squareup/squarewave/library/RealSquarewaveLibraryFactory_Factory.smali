.class public final Lcom/squareup/squarewave/library/RealSquarewaveLibraryFactory_Factory;
.super Ljava/lang/Object;
.source "RealSquarewaveLibraryFactory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/squarewave/library/RealSquarewaveLibraryFactory;",
        ">;"
    }
.end annotation


# instance fields
.field private final parentComponentProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/library/SquarewaveLibraryComponent$ParentComponent;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/library/SquarewaveLibraryComponent$ParentComponent;",
            ">;)V"
        }
    .end annotation

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/squarewave/library/RealSquarewaveLibraryFactory_Factory;->parentComponentProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/squarewave/library/RealSquarewaveLibraryFactory_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/squarewave/library/SquarewaveLibraryComponent$ParentComponent;",
            ">;)",
            "Lcom/squareup/squarewave/library/RealSquarewaveLibraryFactory_Factory;"
        }
    .end annotation

    .line 30
    new-instance v0, Lcom/squareup/squarewave/library/RealSquarewaveLibraryFactory_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/squarewave/library/RealSquarewaveLibraryFactory_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/squarewave/library/SquarewaveLibraryComponent$ParentComponent;)Lcom/squareup/squarewave/library/RealSquarewaveLibraryFactory;
    .locals 1

    .line 35
    new-instance v0, Lcom/squareup/squarewave/library/RealSquarewaveLibraryFactory;

    invoke-direct {v0, p0}, Lcom/squareup/squarewave/library/RealSquarewaveLibraryFactory;-><init>(Lcom/squareup/squarewave/library/SquarewaveLibraryComponent$ParentComponent;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/squarewave/library/RealSquarewaveLibraryFactory;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/squarewave/library/RealSquarewaveLibraryFactory_Factory;->parentComponentProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/squarewave/library/SquarewaveLibraryComponent$ParentComponent;

    invoke-static {v0}, Lcom/squareup/squarewave/library/RealSquarewaveLibraryFactory_Factory;->newInstance(Lcom/squareup/squarewave/library/SquarewaveLibraryComponent$ParentComponent;)Lcom/squareup/squarewave/library/RealSquarewaveLibraryFactory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/squarewave/library/RealSquarewaveLibraryFactory_Factory;->get()Lcom/squareup/squarewave/library/RealSquarewaveLibraryFactory;

    move-result-object v0

    return-object v0
.end method
