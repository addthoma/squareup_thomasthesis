.class public Lcom/squareup/squarewave/ClassifyingDecoder;
.super Ljava/lang/Object;
.source "ClassifyingDecoder.java"

# interfaces
.implements Lcom/squareup/squarewave/SignalDecoder;


# instance fields
.field private final decoders:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;",
            "Lcom/squareup/squarewave/SignalDecoder;",
            ">;"
        }
    .end annotation
.end field

.field private expectedReaderType:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;


# direct methods
.method public constructor <init>(Lcom/squareup/squarewave/o1/O1SignalDecoder;Lcom/squareup/squarewave/SignalDecoder;Lcom/squareup/squarewave/SignalDecoder;Lcom/squareup/squarewave/gen2/Gen2SignalDecoder;)V
    .locals 2

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/squareup/squarewave/ClassifyingDecoder;->decoders:Ljava/util/Map;

    .line 49
    iget-object v0, p0, Lcom/squareup/squarewave/ClassifyingDecoder;->decoders:Ljava/util/Map;

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->O1:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    iget-object p1, p0, Lcom/squareup/squarewave/ClassifyingDecoder;->decoders:Ljava/util/Map;

    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->R4_FAST:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    invoke-interface {p1, v0, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    iget-object p1, p0, Lcom/squareup/squarewave/ClassifyingDecoder;->decoders:Ljava/util/Map;

    sget-object p2, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->R4_SLOW:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    invoke-interface {p1, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    iget-object p1, p0, Lcom/squareup/squarewave/ClassifyingDecoder;->decoders:Ljava/util/Map;

    sget-object p2, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->GEN2:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    invoke-interface {p1, p2, p4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 p1, 0x0

    .line 53
    iput-object p1, p0, Lcom/squareup/squarewave/ClassifyingDecoder;->expectedReaderType:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    return-void
.end method

.method private runDemods(Lcom/squareup/squarewave/Signal;)Lcom/squareup/squarewave/decode/DemodResult;
    .locals 2

    .line 94
    iget-object v0, p0, Lcom/squareup/squarewave/ClassifyingDecoder;->expectedReaderType:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;->GEN2_READER:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    if-ne v0, v1, :cond_0

    .line 95
    iget-object v0, p0, Lcom/squareup/squarewave/ClassifyingDecoder;->decoders:Ljava/util/Map;

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->GEN2:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/squarewave/SignalDecoder;

    invoke-interface {v0, p1}, Lcom/squareup/squarewave/SignalDecoder;->decode(Lcom/squareup/squarewave/Signal;)Lcom/squareup/squarewave/decode/DemodResult;

    move-result-object p1

    return-object p1

    .line 96
    :cond_0
    iget-object v0, p0, Lcom/squareup/squarewave/ClassifyingDecoder;->expectedReaderType:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;->O1_READER:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    if-ne v0, v1, :cond_1

    .line 97
    iget-object v0, p0, Lcom/squareup/squarewave/ClassifyingDecoder;->decoders:Ljava/util/Map;

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->O1:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/squarewave/SignalDecoder;

    invoke-interface {v0, p1}, Lcom/squareup/squarewave/SignalDecoder;->decode(Lcom/squareup/squarewave/Signal;)Lcom/squareup/squarewave/decode/DemodResult;

    move-result-object p1

    return-object p1

    .line 98
    :cond_1
    iget-object v0, p0, Lcom/squareup/squarewave/ClassifyingDecoder;->expectedReaderType:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;->R4_READER:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    if-ne v0, v1, :cond_2

    .line 99
    invoke-direct {p0, p1}, Lcom/squareup/squarewave/ClassifyingDecoder;->tryR4Decoders(Lcom/squareup/squarewave/Signal;)Lcom/squareup/squarewave/decode/DemodResult;

    move-result-object p1

    return-object p1

    .line 101
    :cond_2
    invoke-direct {p0, p1}, Lcom/squareup/squarewave/ClassifyingDecoder;->tryAllDecoders(Lcom/squareup/squarewave/Signal;)Lcom/squareup/squarewave/decode/DemodResult;

    move-result-object p1

    return-object p1
.end method

.method private tryAllDecoders(Lcom/squareup/squarewave/Signal;)Lcom/squareup/squarewave/decode/DemodResult;
    .locals 6

    .line 131
    iget-object v0, p1, Lcom/squareup/squarewave/Signal;->signalFoundBuilder:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;

    iget-object v0, v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->classified_link_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    const/4 v1, 0x0

    if-eqz v0, :cond_4

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    .line 137
    sget-object v2, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->O1:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    aput-object v2, v0, v1

    sget-object v2, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->R4_FAST:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    const/4 v3, 0x1

    aput-object v2, v0, v3

    const/4 v2, 0x2

    sget-object v3, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->R4_SLOW:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    aput-object v3, v0, v2

    const/4 v2, 0x3

    sget-object v3, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->GEN2:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    aput-object v3, v0, v2

    .line 140
    iget-object v2, p1, Lcom/squareup/squarewave/Signal;->signalFoundBuilder:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;

    iget-object v2, v2, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->classified_link_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    .line 144
    sget-object v3, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->NOISE:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    if-eq v2, v3, :cond_0

    .line 145
    iget-object v3, p0, Lcom/squareup/squarewave/ClassifyingDecoder;->decoders:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/squarewave/SignalDecoder;

    invoke-interface {v3, p1}, Lcom/squareup/squarewave/SignalDecoder;->decode(Lcom/squareup/squarewave/Signal;)Lcom/squareup/squarewave/decode/DemodResult;

    move-result-object v3

    .line 146
    invoke-virtual {v3}, Lcom/squareup/squarewave/decode/DemodResult;->isSuccessfulDemod()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 147
    invoke-static {v2}, Lcom/squareup/squarewave/gum/Mapping;->linkTypeToReaderType(Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;)Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/squarewave/ClassifyingDecoder;->expectedReaderType:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    return-object v3

    .line 153
    :cond_0
    array-length v3, v0

    :goto_0
    if-ge v1, v3, :cond_3

    aget-object v4, v0, v1

    if-ne v4, v2, :cond_1

    goto :goto_1

    .line 159
    :cond_1
    iget-object v5, p0, Lcom/squareup/squarewave/ClassifyingDecoder;->decoders:Ljava/util/Map;

    invoke-interface {v5, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/squarewave/SignalDecoder;

    invoke-interface {v4, p1}, Lcom/squareup/squarewave/SignalDecoder;->decode(Lcom/squareup/squarewave/Signal;)Lcom/squareup/squarewave/decode/DemodResult;

    move-result-object v4

    .line 160
    invoke-virtual {v4}, Lcom/squareup/squarewave/decode/DemodResult;->isSuccessfulDemod()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 161
    invoke-static {v2}, Lcom/squareup/squarewave/gum/Mapping;->linkTypeToReaderType(Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;)Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/squarewave/ClassifyingDecoder;->expectedReaderType:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    return-object v4

    :cond_2
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 165
    :cond_3
    invoke-static {}, Lcom/squareup/squarewave/decode/DemodResult;->failedDemod()Lcom/squareup/squarewave/decode/DemodResult;

    move-result-object p1

    return-object p1

    :cond_4
    new-array p1, v1, [Ljava/lang/Object;

    const-string v0, "No classified_link_type set"

    .line 132
    invoke-static {v0, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 133
    new-instance p1, Ljava/lang/RuntimeException;

    invoke-direct {p1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private tryR4Decoders(Lcom/squareup/squarewave/Signal;)Lcom/squareup/squarewave/decode/DemodResult;
    .locals 4

    .line 106
    iget-object v0, p1, Lcom/squareup/squarewave/Signal;->signalFoundBuilder:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;

    iget-object v0, v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->classified_link_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    .line 108
    iget-object v1, p0, Lcom/squareup/squarewave/ClassifyingDecoder;->decoders:Ljava/util/Map;

    sget-object v2, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->R4_FAST:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/squarewave/SignalDecoder;

    .line 109
    iget-object v2, p0, Lcom/squareup/squarewave/ClassifyingDecoder;->decoders:Ljava/util/Map;

    sget-object v3, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->R4_SLOW:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/squarewave/SignalDecoder;

    .line 111
    sget-object v3, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->R4_FAST:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    if-ne v0, v3, :cond_1

    .line 112
    invoke-interface {v1, p1}, Lcom/squareup/squarewave/SignalDecoder;->decode(Lcom/squareup/squarewave/Signal;)Lcom/squareup/squarewave/decode/DemodResult;

    move-result-object v0

    .line 113
    invoke-virtual {v0}, Lcom/squareup/squarewave/decode/DemodResult;->isSuccessfulDemod()Z

    move-result v1

    if-eqz v1, :cond_0

    return-object v0

    .line 116
    :cond_0
    invoke-interface {v2, p1}, Lcom/squareup/squarewave/SignalDecoder;->decode(Lcom/squareup/squarewave/Signal;)Lcom/squareup/squarewave/decode/DemodResult;

    move-result-object p1

    return-object p1

    .line 121
    :cond_1
    invoke-interface {v2, p1}, Lcom/squareup/squarewave/SignalDecoder;->decode(Lcom/squareup/squarewave/Signal;)Lcom/squareup/squarewave/decode/DemodResult;

    move-result-object v0

    .line 122
    invoke-virtual {v0}, Lcom/squareup/squarewave/decode/DemodResult;->isSuccessfulDemod()Z

    move-result v2

    if-eqz v2, :cond_2

    return-object v0

    .line 125
    :cond_2
    invoke-interface {v1, p1}, Lcom/squareup/squarewave/SignalDecoder;->decode(Lcom/squareup/squarewave/Signal;)Lcom/squareup/squarewave/decode/DemodResult;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public decode(Lcom/squareup/squarewave/Signal;)Lcom/squareup/squarewave/decode/DemodResult;
    .locals 2

    .line 62
    iget-object v0, p1, Lcom/squareup/squarewave/Signal;->signalFoundBuilder:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;

    iget-object v1, p0, Lcom/squareup/squarewave/ClassifyingDecoder;->expectedReaderType:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    iput-object v1, v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->expected_reader_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    .line 63
    invoke-direct {p0, p1}, Lcom/squareup/squarewave/ClassifyingDecoder;->runDemods(Lcom/squareup/squarewave/Signal;)Lcom/squareup/squarewave/decode/DemodResult;

    move-result-object v0

    .line 65
    invoke-virtual {v0}, Lcom/squareup/squarewave/decode/DemodResult;->isSuccessfulDemod()Z

    move-result v1

    if-eqz v1, :cond_0

    return-object v0

    .line 69
    :cond_0
    iget-object v0, p1, Lcom/squareup/squarewave/Signal;->eventBuilder:Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;

    iget-object v0, v0, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Builder;->carrier_detect_info:Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;

    iget-object v0, v0, Lcom/squareup/protos/logging/events/swipe_experience/CarrierDetectInfo;->is_early_packet:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 74
    invoke-static {}, Lcom/squareup/squarewave/decode/DemodResult;->probablyNoise()Lcom/squareup/squarewave/decode/DemodResult;

    move-result-object p1

    return-object p1

    .line 78
    :cond_1
    iget-object p1, p1, Lcom/squareup/squarewave/Signal;->signalFoundBuilder:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;

    iget-object p1, p1, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$Builder;->classified_link_type:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    .line 80
    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->NOISE:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    if-ne p1, v0, :cond_3

    .line 81
    iget-object p1, p0, Lcom/squareup/squarewave/ClassifyingDecoder;->expectedReaderType:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    sget-object v0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;->GEN2_READER:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    const/4 v1, 0x0

    if-ne p1, v0, :cond_2

    new-array p1, v1, [Ljava/lang/Object;

    const-string v0, "Classified as noise ignored for Gen2 reader"

    .line 83
    invoke-static {v0, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 84
    invoke-static {}, Lcom/squareup/squarewave/decode/DemodResult;->failedDemod()Lcom/squareup/squarewave/decode/DemodResult;

    move-result-object p1

    return-object p1

    :cond_2
    new-array p1, v1, [Ljava/lang/Object;

    const-string v0, "Signal classified as noise"

    .line 86
    invoke-static {v0, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 87
    invoke-static {}, Lcom/squareup/squarewave/decode/DemodResult;->probablyNoise()Lcom/squareup/squarewave/decode/DemodResult;

    move-result-object p1

    return-object p1

    .line 89
    :cond_3
    invoke-static {}, Lcom/squareup/squarewave/decode/DemodResult;->failedDemod()Lcom/squareup/squarewave/decode/DemodResult;

    move-result-object p1

    return-object p1
.end method

.method getExpectedReaderType()Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;
    .locals 1

    .line 169
    iget-object v0, p0, Lcom/squareup/squarewave/ClassifyingDecoder;->expectedReaderType:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    return-object v0
.end method

.method public reset()V
    .locals 1

    const/4 v0, 0x0

    .line 58
    iput-object v0, p0, Lcom/squareup/squarewave/ClassifyingDecoder;->expectedReaderType:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    return-void
.end method
