.class public Lcom/squareup/squarewave/gen2/Gen2BinaryFilter;
.super Ljava/lang/Object;
.source "Gen2BinaryFilter.java"

# interfaces
.implements Lcom/squareup/squarewave/gen2/Gen2SwipeFilter;


# static fields
.field private static final NUM_CLOCKING_BITS:I = 0x4


# instance fields
.field private final clockingTolerance:I

.field private final nextFilter:Lcom/squareup/squarewave/gen2/Gen2SwipeFilter;


# direct methods
.method public constructor <init>(Lcom/squareup/squarewave/gen2/Gen2SwipeFilter;I)V
    .locals 0

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/squareup/squarewave/gen2/Gen2BinaryFilter;->nextFilter:Lcom/squareup/squarewave/gen2/Gen2SwipeFilter;

    .line 29
    iput p2, p0, Lcom/squareup/squarewave/gen2/Gen2BinaryFilter;->clockingTolerance:I

    return-void
.end method

.method private findClockingStart([Lcom/squareup/squarewave/gen2/Peak;)I
    .locals 12

    .line 120
    array-length v0, p1

    const/4 v1, 0x2

    sub-int/2addr v0, v1

    .line 122
    array-length v2, p1

    const/4 v3, -0x1

    if-ge v2, v1, :cond_0

    return v3

    :cond_0
    const/4 v1, 0x0

    .line 124
    aget-object v2, p1, v1

    const/4 v4, 0x1

    .line 125
    aget-object v5, p1, v4

    move-object v6, v2

    const/4 v2, 0x0

    const/4 v7, 0x0

    const/4 v8, -0x1

    :goto_0
    if-ge v2, v0, :cond_4

    add-int/lit8 v9, v2, 0x2

    .line 128
    aget-object v9, p1, v9

    .line 129
    iget v10, v5, Lcom/squareup/squarewave/gen2/Peak;->sampleIndex:I

    iget v6, v6, Lcom/squareup/squarewave/gen2/Peak;->sampleIndex:I

    sub-int/2addr v10, v6

    invoke-static {v10}, Ljava/lang/Math;->abs(I)I

    move-result v6

    .line 130
    iget v10, v9, Lcom/squareup/squarewave/gen2/Peak;->sampleIndex:I

    iget v11, v5, Lcom/squareup/squarewave/gen2/Peak;->sampleIndex:I

    sub-int/2addr v10, v11

    invoke-static {v10}, Ljava/lang/Math;->abs(I)I

    move-result v10

    .line 133
    invoke-static {v6, v10}, Lcom/squareup/squarewave/MathUtil;->percentageChange(II)I

    move-result v6

    .line 134
    iget v10, p0, Lcom/squareup/squarewave/gen2/Gen2BinaryFilter;->clockingTolerance:I

    if-ge v6, v10, :cond_2

    add-int/2addr v7, v4

    if-ne v7, v4, :cond_1

    move v8, v2

    :cond_1
    const/4 v6, 0x4

    if-ne v7, v6, :cond_3

    return v8

    :cond_2
    const/4 v7, 0x0

    const/4 v8, -0x1

    :cond_3
    add-int/lit8 v2, v2, 0x1

    move-object v6, v5

    move-object v5, v9

    goto :goto_0

    :cond_4
    return v3
.end method


# virtual methods
.method public hear(Lcom/squareup/squarewave/gen2/Gen2Swipe;)Lcom/squareup/squarewave/gen2/Gen2DemodResult;
    .locals 18

    move-object/from16 v0, p0

    .line 34
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 36
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/squarewave/gen2/Gen2Swipe;->peaks()[Lcom/squareup/squarewave/gen2/Peak;

    move-result-object v2

    .line 37
    array-length v3, v2

    const/4 v4, 0x0

    const/4 v5, 0x2

    const/4 v6, 0x1

    if-lt v3, v5, :cond_1

    array-length v3, v2

    sub-int/2addr v3, v6

    aget-object v3, v2, v3

    iget v3, v3, Lcom/squareup/squarewave/gen2/Peak;->sampleIndex:I

    aget-object v7, v2, v4

    iget v7, v7, Lcom/squareup/squarewave/gen2/Peak;->sampleIndex:I

    if-le v3, v7, :cond_0

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v3, 0x1

    .line 47
    :goto_1
    invoke-direct {v0, v2}, Lcom/squareup/squarewave/gen2/Gen2BinaryFilter;->findClockingStart([Lcom/squareup/squarewave/gen2/Peak;)I

    move-result v7

    const/4 v8, -0x1

    if-le v7, v8, :cond_5

    add-int/lit8 v8, v7, 0x1

    .line 51
    aget-object v8, v2, v8

    iget v8, v8, Lcom/squareup/squarewave/gen2/Peak;->sampleIndex:I

    aget-object v9, v2, v7

    iget v9, v9, Lcom/squareup/squarewave/gen2/Peak;->sampleIndex:I

    sub-int/2addr v8, v9

    invoke-static {v8}, Ljava/lang/Math;->abs(I)I

    move-result v8

    .line 54
    new-instance v9, Lcom/squareup/squarewave/Averager;

    const/4 v10, 0x4

    invoke-direct {v9, v10}, Lcom/squareup/squarewave/Averager;-><init>(I)V

    .line 55
    new-instance v11, Lcom/squareup/squarewave/Averager;

    invoke-direct {v11, v10, v8}, Lcom/squareup/squarewave/Averager;-><init>(II)V

    .line 60
    array-length v10, v2

    :goto_2
    add-int/lit8 v12, v10, -0x2

    if-ge v7, v12, :cond_5

    .line 63
    aget-object v12, v2, v7

    iget v12, v12, Lcom/squareup/squarewave/gen2/Peak;->sampleIndex:I

    add-int/lit8 v13, v7, 0x1

    .line 64
    aget-object v14, v2, v13

    iget v14, v14, Lcom/squareup/squarewave/gen2/Peak;->sampleIndex:I

    add-int/lit8 v15, v7, 0x2

    .line 65
    aget-object v4, v2, v15

    iget v4, v4, Lcom/squareup/squarewave/gen2/Peak;->sampleIndex:I

    .line 69
    invoke-virtual {v9}, Lcom/squareup/squarewave/Averager;->average()I

    move-result v16

    if-eqz v3, :cond_2

    add-int v17, v12, v8

    add-int v17, v17, v16

    goto :goto_3

    :cond_2
    sub-int v17, v12, v8

    sub-int v17, v17, v16

    :goto_3
    sub-int v16, v17, v14

    .line 78
    invoke-static/range {v16 .. v16}, Ljava/lang/Math;->abs(I)I

    move-result v5

    sub-int v17, v17, v4

    .line 79
    invoke-static/range {v17 .. v17}, Ljava/lang/Math;->abs(I)I

    move-result v6

    if-gt v5, v6, :cond_3

    const/4 v5, 0x1

    goto :goto_4

    :cond_3
    const/4 v5, 0x0

    :goto_4
    if-eqz v5, :cond_4

    .line 84
    new-instance v4, Lcom/squareup/squarewave/gen2/Reading;

    aget-object v5, v2, v7

    aget-object v6, v2, v13

    invoke-direct {v4, v5, v6}, Lcom/squareup/squarewave/gen2/Reading;-><init>(Lcom/squareup/squarewave/gen2/Peak;Lcom/squareup/squarewave/gen2/Peak;)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sub-int/2addr v14, v12

    .line 87
    invoke-static {v14}, Ljava/lang/Math;->abs(I)I

    move-result v4

    goto :goto_5

    .line 90
    :cond_4
    new-instance v5, Lcom/squareup/squarewave/gen2/Reading;

    aget-object v6, v2, v7

    aget-object v7, v2, v13

    aget-object v14, v2, v15

    invoke-direct {v5, v6, v7, v14}, Lcom/squareup/squarewave/gen2/Reading;-><init>(Lcom/squareup/squarewave/gen2/Peak;Lcom/squareup/squarewave/gen2/Peak;Lcom/squareup/squarewave/gen2/Peak;)V

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sub-int/2addr v4, v12

    .line 94
    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    move v7, v13

    .line 98
    :goto_5
    invoke-virtual {v11, v4}, Lcom/squareup/squarewave/Averager;->add(I)Lcom/squareup/squarewave/Averager;

    .line 101
    invoke-virtual {v11}, Lcom/squareup/squarewave/Averager;->average()I

    move-result v4

    sub-int v5, v4, v8

    .line 102
    invoke-virtual {v9, v5}, Lcom/squareup/squarewave/Averager;->add(I)Lcom/squareup/squarewave/Averager;

    const/4 v5, 0x1

    add-int/2addr v7, v5

    move v8, v4

    const/4 v4, 0x0

    const/4 v5, 0x2

    const/4 v6, 0x1

    goto :goto_2

    .line 106
    :cond_5
    iget-object v2, v0, Lcom/squareup/squarewave/gen2/Gen2BinaryFilter;->nextFilter:Lcom/squareup/squarewave/gen2/Gen2SwipeFilter;

    invoke-virtual/range {p1 .. p1}, Lcom/squareup/squarewave/gen2/Gen2Swipe;->buildUpon()Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;

    move-result-object v3

    .line 107
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    new-array v4, v4, [Lcom/squareup/squarewave/gen2/Reading;

    invoke-interface {v1, v4}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/squareup/squarewave/gen2/Reading;

    .line 106
    invoke-virtual {v3, v1}, Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;->readings([Lcom/squareup/squarewave/gen2/Reading;)Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;

    move-result-object v1

    .line 107
    invoke-virtual {v1}, Lcom/squareup/squarewave/gen2/Gen2Swipe$Builder;->build()Lcom/squareup/squarewave/gen2/Gen2Swipe;

    move-result-object v1

    .line 106
    invoke-interface {v2, v1}, Lcom/squareup/squarewave/gen2/Gen2SwipeFilter;->hear(Lcom/squareup/squarewave/gen2/Gen2Swipe;)Lcom/squareup/squarewave/gen2/Gen2DemodResult;

    move-result-object v1

    return-object v1
.end method
