.class public Lcom/squareup/squarewave/gen2/DelayPeakFinder;
.super Ljava/lang/Object;
.source "DelayPeakFinder.java"

# interfaces
.implements Lcom/squareup/squarewave/gen2/Gen2SwipeFilter;


# instance fields
.field private final delay:I

.field private final nextFilter:Lcom/squareup/squarewave/gen2/Gen2SwipeFilter;


# direct methods
.method public constructor <init>(Lcom/squareup/squarewave/gen2/Gen2SwipeFilter;I)V
    .locals 0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/squareup/squarewave/gen2/DelayPeakFinder;->nextFilter:Lcom/squareup/squarewave/gen2/Gen2SwipeFilter;

    .line 27
    iput p2, p0, Lcom/squareup/squarewave/gen2/DelayPeakFinder;->delay:I

    return-void
.end method


# virtual methods
.method public getDelay()I
    .locals 1

    .line 31
    iget v0, p0, Lcom/squareup/squarewave/gen2/DelayPeakFinder;->delay:I

    return v0
.end method

.method public hear(Lcom/squareup/squarewave/gen2/Gen2Swipe;)Lcom/squareup/squarewave/gen2/Gen2DemodResult;
    .locals 18

    move-object/from16 v0, p0

    .line 37
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/squarewave/gen2/Gen2Swipe;->samples()[S

    move-result-object v1

    .line 38
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 50
    iget v3, v0, Lcom/squareup/squarewave/gen2/DelayPeakFinder;->delay:I

    const/16 v4, -0x8000

    const/16 v5, 0x7fff

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/16 v8, 0x7fff

    const/16 v9, -0x8000

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    .line 52
    :goto_0
    array-length v15, v1

    if-ge v3, v15, :cond_7

    .line 53
    aget-short v15, v1, v3

    add-int/lit8 v16, v7, 0x1

    .line 54
    aget-short v7, v1, v7

    if-ge v15, v8, :cond_0

    move v14, v3

    move v8, v15

    :cond_0
    if-le v15, v9, :cond_1

    move v13, v3

    move v9, v15

    :cond_1
    const/16 v17, 0x1

    if-eq v15, v7, :cond_3

    if-le v15, v7, :cond_2

    const/4 v7, 0x1

    goto :goto_1

    :cond_2
    const/4 v7, 0x0

    :goto_1
    move v12, v7

    :cond_3
    if-eq v12, v11, :cond_6

    if-eqz v10, :cond_5

    .line 73
    new-instance v7, Lcom/squareup/squarewave/gen2/Peak;

    if-eqz v12, :cond_4

    invoke-direct {v7, v14, v8}, Lcom/squareup/squarewave/gen2/Peak;-><init>(IS)V

    goto :goto_2

    :cond_4
    invoke-direct {v7, v13, v9}, Lcom/squareup/squarewave/gen2/Peak;-><init>(IS)V

    .line 77
    :goto_2
    invoke-interface {v2, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_5
    const/16 v8, 0x7fff

    const/16 v9, -0x8000

    const/4 v10, 0x1

    :cond_6
    add-int/lit8 v3, v3, 0x1

    move v11, v12

    move/from16 v7, v16

    goto :goto_0

    .line 89
    :cond_7
    iget-object v1, v0, Lcom/squareup/squarewave/gen2/DelayPeakFinder;->nextFilter:Lcom/squareup/squarewave/gen2/Gen2SwipeFilter;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [Lcom/squareup/squarewave/gen2/Peak;

    invoke-interface {v2, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lcom/squareup/squarewave/gen2/Peak;

    move-object/from16 v3, p1

    invoke-virtual {v3, v2}, Lcom/squareup/squarewave/gen2/Gen2Swipe;->withPeaks([Lcom/squareup/squarewave/gen2/Peak;)Lcom/squareup/squarewave/gen2/Gen2Swipe;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/squareup/squarewave/gen2/Gen2SwipeFilter;->hear(Lcom/squareup/squarewave/gen2/Gen2Swipe;)Lcom/squareup/squarewave/gen2/Gen2DemodResult;

    move-result-object v1

    return-object v1
.end method
