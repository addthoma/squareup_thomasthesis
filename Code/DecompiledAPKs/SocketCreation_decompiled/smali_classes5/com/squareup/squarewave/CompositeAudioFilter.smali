.class public Lcom/squareup/squarewave/CompositeAudioFilter;
.super Ljava/lang/Object;
.source "CompositeAudioFilter.java"

# interfaces
.implements Lcom/squareup/squarewave/AudioFilter;


# instance fields
.field private final audioFilters:[Lcom/squareup/squarewave/AudioFilter;


# direct methods
.method private constructor <init>([Lcom/squareup/squarewave/AudioFilter;)V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/squareup/squarewave/CompositeAudioFilter;->audioFilters:[Lcom/squareup/squarewave/AudioFilter;

    return-void
.end method

.method public static varargs from([Lcom/squareup/squarewave/AudioFilter;)Lcom/squareup/squarewave/AudioFilter;
    .locals 1

    .line 8
    new-instance v0, Lcom/squareup/squarewave/CompositeAudioFilter;

    invoke-direct {v0, p0}, Lcom/squareup/squarewave/CompositeAudioFilter;-><init>([Lcom/squareup/squarewave/AudioFilter;)V

    return-object v0
.end method


# virtual methods
.method public finish()V
    .locals 4

    .line 30
    iget-object v0, p0, Lcom/squareup/squarewave/CompositeAudioFilter;->audioFilters:[Lcom/squareup/squarewave/AudioFilter;

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, v0, v2

    .line 31
    invoke-interface {v3}, Lcom/squareup/squarewave/AudioFilter;->finish()V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public process(Ljava/nio/ByteBuffer;I)V
    .locals 4

    .line 24
    iget-object v0, p0, Lcom/squareup/squarewave/CompositeAudioFilter;->audioFilters:[Lcom/squareup/squarewave/AudioFilter;

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, v0, v2

    .line 25
    invoke-interface {v3, p1, p2}, Lcom/squareup/squarewave/AudioFilter;->process(Ljava/nio/ByteBuffer;I)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public start(I)V
    .locals 4

    .line 18
    iget-object v0, p0, Lcom/squareup/squarewave/CompositeAudioFilter;->audioFilters:[Lcom/squareup/squarewave/AudioFilter;

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, v0, v2

    .line 19
    invoke-interface {v3, p1}, Lcom/squareup/squarewave/AudioFilter;->start(I)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method
