.class public Lcom/squareup/squarewave/gum/Mapping;
.super Ljava/lang/Object;
.source "Mapping.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static demodResultToLogDemodResult(I)Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;
    .locals 1

    .line 54
    invoke-static {}, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;->values()[Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$DemodResult;

    move-result-object v0

    aget-object p0, v0, p0

    return-object p0
.end method

.method public static lcrEventToLogReaderCarrierDetectEvent(I)Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;
    .locals 1

    .line 67
    invoke-static {}, Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;->values()[Lcom/squareup/protos/logging/events/swipe_experience/ReaderCarrierDetectEvent$Event;

    move-result-object v0

    aget-object p0, v0, p0

    return-object p0
.end method

.method public static lcrLinkTypeToLogClassifyInfoLinkType(I)Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$LinkType;
    .locals 1

    .line 44
    invoke-static {}, Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$LinkType;->values()[Lcom/squareup/protos/logging/events/swipe_experience/ClassifyInfo$LinkType;

    move-result-object v0

    aget-object p0, v0, p0

    return-object p0
.end method

.method public static lcrLinkTypeToLogSignalFoundLinkType(I)Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;
    .locals 1

    .line 49
    invoke-static {}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->values()[Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;

    move-result-object v0

    aget-object p0, v0, p0

    return-object p0
.end method

.method public static linkTypeToLcrLinkType(Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;)I
    .locals 0

    .line 39
    invoke-virtual {p0}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->ordinal()I

    move-result p0

    return p0
.end method

.method public static linkTypeToReaderType(Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;)Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;
    .locals 2

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return-object v0

    .line 79
    :cond_0
    sget-object v1, Lcom/squareup/squarewave/gum/Mapping$1;->$SwitchMap$com$squareup$protos$logging$events$swipe_experience$SignalFound$LinkType:[I

    invoke-virtual {p0}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$LinkType;->ordinal()I

    move-result p0

    aget p0, v1, p0

    const/4 v1, 0x1

    if-eq p0, v1, :cond_3

    const/4 v1, 0x2

    if-eq p0, v1, :cond_2

    const/4 v1, 0x3

    if-eq p0, v1, :cond_1

    const/4 v1, 0x4

    if-eq p0, v1, :cond_1

    return-object v0

    .line 86
    :cond_1
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;->R4_READER:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    return-object p0

    .line 83
    :cond_2
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;->O1_READER:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    return-object p0

    .line 81
    :cond_3
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;->GEN2_READER:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;

    return-object p0
.end method

.method public static mapBrandToIssuer(Lcom/squareup/Card$Brand;)Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;
    .locals 3

    .line 16
    sget-object v0, Lcom/squareup/squarewave/gum/Mapping$1;->$SwitchMap$com$squareup$Card$Brand:[I

    invoke-virtual {p0}, Lcom/squareup/Card$Brand;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 33
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown brand "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 31
    :pswitch_0
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;->OTHER:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

    return-object p0

    .line 27
    :pswitch_1
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;->JCB:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

    return-object p0

    .line 25
    :pswitch_2
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;->AMERICAN_EXPRESS:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

    return-object p0

    .line 23
    :pswitch_3
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;->DISCOVER:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

    return-object p0

    .line 20
    :pswitch_4
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;->MASTERCARD:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

    return-object p0

    .line 18
    :pswitch_5
    sget-object p0, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;->VISA:Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$IssuerId;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static readerTypeToLcrReaderType(Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;)I
    .locals 1

    .line 94
    sget-object v0, Lcom/squareup/squarewave/gum/Mapping$1;->$SwitchMap$com$squareup$protos$logging$events$swipe_experience$SignalFound$ReaderType:[I

    invoke-virtual {p0}, Lcom/squareup/protos/logging/events/swipe_experience/SignalFound$ReaderType;->ordinal()I

    move-result p0

    aget p0, v0, p0

    const/4 v0, 0x1

    if-eq p0, v0, :cond_1

    const/4 v0, 0x2

    if-eq p0, v0, :cond_1

    const/4 v0, 0x3

    if-ne p0, v0, :cond_0

    return v0

    .line 102
    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string v0, "This only supports legacy readers"

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_1
    return v0
.end method

.method public static statusOrdinalToLast4Status(I)Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Last4Status;
    .locals 1

    .line 59
    invoke-static {}, Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Last4Status;->values()[Lcom/squareup/protos/logging/events/swipe_experience/SqLinkDemodInfo$Last4Status;

    move-result-object v0

    aget-object p0, v0, p0

    return-object p0
.end method
