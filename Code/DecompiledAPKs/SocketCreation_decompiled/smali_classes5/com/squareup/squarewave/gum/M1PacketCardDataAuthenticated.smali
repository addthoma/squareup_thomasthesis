.class public final Lcom/squareup/squarewave/gum/M1PacketCardDataAuthenticated;
.super Ljava/lang/Object;
.source "M1PacketCardDataAuthenticated.java"


# static fields
.field private static final M1_PACKET_LAST4_LENGTH:I = 0x4


# instance fields
.field public final issuer:Lcom/squareup/Card$Brand;

.field public final last4:Ljava/lang/String;

.field public final name:Ljava/lang/String;

.field public final t1Len:I

.field public final t1Result:B

.field public final t2Len:I

.field public final t2Result:B

.field public final t3Len:I

.field public final t3Result:B


# direct methods
.method constructor <init>(Ljava/nio/ByteBuffer;)V
    .locals 2

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->get()B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    iput v0, p0, Lcom/squareup/squarewave/gum/M1PacketCardDataAuthenticated;->t1Len:I

    .line 39
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->get()B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    iput v0, p0, Lcom/squareup/squarewave/gum/M1PacketCardDataAuthenticated;->t2Len:I

    .line 40
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->get()B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    iput v0, p0, Lcom/squareup/squarewave/gum/M1PacketCardDataAuthenticated;->t3Len:I

    .line 41
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->get()B

    move-result v0

    iput-byte v0, p0, Lcom/squareup/squarewave/gum/M1PacketCardDataAuthenticated;->t1Result:B

    .line 42
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->get()B

    move-result v0

    iput-byte v0, p0, Lcom/squareup/squarewave/gum/M1PacketCardDataAuthenticated;->t2Result:B

    .line 43
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->get()B

    move-result v0

    iput-byte v0, p0, Lcom/squareup/squarewave/gum/M1PacketCardDataAuthenticated;->t3Result:B

    .line 44
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->get()B

    move-result v0

    invoke-static {v0}, Lcom/squareup/Card$Brand;->fromSqLinkCode(B)Lcom/squareup/Card$Brand;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/squarewave/gum/M1PacketCardDataAuthenticated;->issuer:Lcom/squareup/Card$Brand;

    const/4 v0, 0x4

    new-array v0, v0, [B

    .line 47
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 48
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([B)V

    iput-object v1, p0, Lcom/squareup/squarewave/gum/M1PacketCardDataAuthenticated;->last4:Ljava/lang/String;

    .line 50
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->get()B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    .line 51
    new-array v0, v0, [B

    .line 52
    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 53
    new-instance p1, Ljava/lang/String;

    invoke-direct {p1, v0}, Ljava/lang/String;-><init>([B)V

    iput-object p1, p0, Lcom/squareup/squarewave/gum/M1PacketCardDataAuthenticated;->name:Ljava/lang/String;

    return-void
.end method

.method public static getErrorMessage(B)Ljava/lang/String;
    .locals 2

    if-nez p0, :cond_0

    const-string p0, ""

    return-object p0

    .line 103
    :cond_0
    invoke-static {p0}, Lcom/squareup/squarewave/gum/Util;->unsignedByteToInt(B)I

    move-result p0

    and-int/lit8 v0, p0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    const-string p0, "No track data"

    return-object p0

    :cond_1
    and-int/lit8 v0, p0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    const-string p0, "No zeros detected"

    return-object p0

    :cond_2
    and-int/lit8 v0, p0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_3

    const-string p0, "Card data too long"

    return-object p0

    :cond_3
    and-int/lit8 v0, p0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_4

    const-string p0, "Card data too short"

    return-object p0

    :cond_4
    and-int/lit8 v0, p0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_5

    const-string p0, "Missing start sentinel"

    return-object p0

    :cond_5
    and-int/lit8 v0, p0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_6

    const-string p0, "Missing end sentinel"

    return-object p0

    :cond_6
    and-int/lit8 v0, p0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_7

    const-string p0, "Parity Error"

    return-object p0

    :cond_7
    const/16 v0, 0x80

    and-int/2addr p0, v0

    if-ne p0, v0, :cond_8

    const-string p0, "LRC failure"

    return-object p0

    :cond_8
    const-string p0, "Unknown"

    return-object p0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 3

    .line 74
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "{"

    .line 75
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "t1Len: "

    .line 76
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/squarewave/gum/M1PacketCardDataAuthenticated;->t1Len:I

    .line 77
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", t2Len: "

    .line 78
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/squarewave/gum/M1PacketCardDataAuthenticated;->t2Len:I

    .line 79
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", t3Len: "

    .line 80
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/squarewave/gum/M1PacketCardDataAuthenticated;->t3Len:I

    .line 81
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", t1Result: "

    .line 82
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-byte v1, p0, Lcom/squareup/squarewave/gum/M1PacketCardDataAuthenticated;->t1Result:B

    .line 83
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", t2Result: "

    .line 84
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-byte v1, p0, Lcom/squareup/squarewave/gum/M1PacketCardDataAuthenticated;->t2Result:B

    .line 85
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", t3Result: "

    .line 86
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-byte v1, p0, Lcom/squareup/squarewave/gum/M1PacketCardDataAuthenticated;->t3Result:B

    .line 87
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", issuer: "

    .line 88
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/squarewave/gum/M1PacketCardDataAuthenticated;->issuer:Lcom/squareup/Card$Brand;

    .line 89
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", last4: \""

    .line 90
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/squarewave/gum/M1PacketCardDataAuthenticated;->last4:Ljava/lang/String;

    .line 91
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\""

    .line 92
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ", name: \""

    .line 93
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/squareup/squarewave/gum/M1PacketCardDataAuthenticated;->name:Ljava/lang/String;

    .line 94
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 95
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 96
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public track1SuccessfullyDecoded()Z
    .locals 1

    .line 57
    iget-byte v0, p0, Lcom/squareup/squarewave/gum/M1PacketCardDataAuthenticated;->t1Result:B

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public track2SuccessfullyDecoded()Z
    .locals 1

    .line 61
    iget-byte v0, p0, Lcom/squareup/squarewave/gum/M1PacketCardDataAuthenticated;->t2Result:B

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public trackResultsAsHexString()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x3

    new-array v0, v0, [B

    .line 65
    iget-byte v1, p0, Lcom/squareup/squarewave/gum/M1PacketCardDataAuthenticated;->t1Result:B

    const/4 v2, 0x0

    aput-byte v1, v0, v2

    iget-byte v1, p0, Lcom/squareup/squarewave/gum/M1PacketCardDataAuthenticated;->t2Result:B

    const/4 v2, 0x1

    aput-byte v1, v0, v2

    iget-byte v1, p0, Lcom/squareup/squarewave/gum/M1PacketCardDataAuthenticated;->t3Result:B

    const/4 v2, 0x2

    aput-byte v1, v0, v2

    .line 70
    invoke-static {v0}, Lcom/squareup/squarewave/gum/Util;->unsignedBytesToHexString([B)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
