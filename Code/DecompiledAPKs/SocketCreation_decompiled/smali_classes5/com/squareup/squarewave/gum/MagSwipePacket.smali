.class public Lcom/squareup/squarewave/gum/MagSwipePacket;
.super Ljava/lang/Object;
.source "MagSwipePacket.java"


# instance fields
.field public final cardDataAuthenticated:Lcom/squareup/squarewave/gum/M1PacketCardDataAuthenticated;

.field public final cardDataPlainText:Lcom/squareup/squarewave/gum/M1PacketCardDataPlainText;

.field public final fullPacket:Ljava/nio/ByteBuffer;


# direct methods
.method constructor <init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;)V
    .locals 2

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/squarewave/gum/MagSwipePacket;->fullPacket:Ljava/nio/ByteBuffer;

    const/4 p1, 0x0

    if-nez p2, :cond_0

    move-object v0, p1

    goto :goto_0

    .line 22
    :cond_0
    new-instance v0, Lcom/squareup/squarewave/gum/M1PacketCardDataPlainText;

    sget-object v1, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    .line 23
    invoke-virtual {p2, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object p2

    invoke-direct {v0, p2}, Lcom/squareup/squarewave/gum/M1PacketCardDataPlainText;-><init>(Ljava/nio/ByteBuffer;)V

    :goto_0
    iput-object v0, p0, Lcom/squareup/squarewave/gum/MagSwipePacket;->cardDataPlainText:Lcom/squareup/squarewave/gum/M1PacketCardDataPlainText;

    if-nez p3, :cond_1

    goto :goto_1

    .line 25
    :cond_1
    new-instance p1, Lcom/squareup/squarewave/gum/M1PacketCardDataAuthenticated;

    sget-object p2, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    .line 26
    invoke-virtual {p3, p2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/squareup/squarewave/gum/M1PacketCardDataAuthenticated;-><init>(Ljava/nio/ByteBuffer;)V

    :goto_1
    iput-object p1, p0, Lcom/squareup/squarewave/gum/MagSwipePacket;->cardDataAuthenticated:Lcom/squareup/squarewave/gum/M1PacketCardDataAuthenticated;

    return-void
.end method
