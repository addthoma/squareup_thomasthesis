.class public Lcom/squareup/squarewave/o1/O1BinaryFilter;
.super Ljava/lang/Object;
.source "O1BinaryFilter.java"

# interfaces
.implements Lcom/squareup/squarewave/o1/O1SwipeFilter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/squarewave/o1/O1BinaryFilter$Readings;,
        Lcom/squareup/squarewave/o1/O1BinaryFilter$Window;
    }
.end annotation


# static fields
.field private static final PREFERRED_STATUSES:[Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;

.field private static final THRESHOLDS_TO_TRY:[S


# instance fields
.field private final next:Lcom/squareup/squarewave/o1/O1SwipeFilter;

.field private toolThreshold:Lcom/squareup/squarewave/o1/Threshold;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x6

    new-array v0, v0, [S

    .line 38
    fill-array-data v0, :array_0

    sput-object v0, Lcom/squareup/squarewave/o1/O1BinaryFilter;->THRESHOLDS_TO_TRY:[S

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;

    .line 56
    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;->FAIL_INVALID_COUNTER:Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;->FAIL_DECODE:Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;->FAIL_LRC:Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;->FAIL_BAD_STREAM:Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/squarewave/o1/O1BinaryFilter;->PREFERRED_STATUSES:[Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;

    return-void

    :array_0
    .array-data 2
        0x2aaas
        0x1555s
        0xfffs
        0x3ffs
        0x1ffs
        0xffs
    .end array-data
.end method

.method public constructor <init>(Lcom/squareup/squarewave/o1/O1SwipeFilter;)V
    .locals 1

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    .line 48
    iput-object v0, p0, Lcom/squareup/squarewave/o1/O1BinaryFilter;->toolThreshold:Lcom/squareup/squarewave/o1/Threshold;

    .line 65
    iput-object p1, p0, Lcom/squareup/squarewave/o1/O1BinaryFilter;->next:Lcom/squareup/squarewave/o1/O1SwipeFilter;

    return-void
.end method

.method private adjust(Lcom/squareup/squarewave/o1/O1BinaryFilter$Window;[III)Lcom/squareup/squarewave/o1/O1BinaryFilter$Window;
    .locals 3

    .line 204
    array-length v0, p2

    if-nez v0, :cond_0

    return-object p1

    .line 207
    :cond_0
    array-length v0, p2

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-ne v0, v1, :cond_1

    .line 208
    aget p2, p2, v2

    invoke-virtual {p1, p2}, Lcom/squareup/squarewave/o1/O1BinaryFilter$Window;->centerOn(I)Lcom/squareup/squarewave/o1/O1BinaryFilter$Window;

    move-result-object p1

    return-object p1

    :cond_1
    shr-int/2addr p3, v1

    .line 215
    aget v0, p2, v2

    invoke-static {p1}, Lcom/squareup/squarewave/o1/O1BinaryFilter$Window;->access$100(Lcom/squareup/squarewave/o1/O1BinaryFilter$Window;)I

    move-result v1

    sub-int/2addr v0, v1

    .line 216
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v1

    if-ge v1, p3, :cond_2

    .line 217
    invoke-virtual {p1, v0}, Lcom/squareup/squarewave/o1/O1BinaryFilter$Window;->translate(I)Lcom/squareup/squarewave/o1/O1BinaryFilter$Window;

    move-result-object v0

    .line 218
    invoke-virtual {v0, v2, p4}, Lcom/squareup/squarewave/o1/O1BinaryFilter$Window;->within(II)Z

    move-result v1

    if-eqz v1, :cond_2

    return-object v0

    .line 223
    :cond_2
    aget p2, p2, v2

    invoke-static {p1}, Lcom/squareup/squarewave/o1/O1BinaryFilter$Window;->access$200(Lcom/squareup/squarewave/o1/O1BinaryFilter$Window;)I

    move-result v0

    sub-int/2addr p2, v0

    .line 224
    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result v0

    if-ge v0, p3, :cond_3

    .line 225
    invoke-virtual {p1, p2}, Lcom/squareup/squarewave/o1/O1BinaryFilter$Window;->translate(I)Lcom/squareup/squarewave/o1/O1BinaryFilter$Window;

    move-result-object p2

    .line 226
    invoke-virtual {p2, v2, p4}, Lcom/squareup/squarewave/o1/O1BinaryFilter$Window;->within(II)Z

    move-result p3

    if-eqz p3, :cond_3

    return-object p2

    :cond_3
    return-object p1
.end method

.method private attemptDecode(Lcom/squareup/squarewave/o1/O1Swipe;Lcom/squareup/squarewave/o1/Threshold;Z)Lcom/squareup/squarewave/o1/O1DemodResult;
    .locals 17

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    .line 123
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/squarewave/o1/O1Swipe;->getSignal()Lcom/squareup/squarewave/Signal;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/squarewave/Signal;->samples()[S

    move-result-object v2

    .line 125
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/squarewave/o1/O1Swipe;->samplesPerCycle()I

    move-result v3

    shr-int/lit8 v4, v3, 0x1

    shr-int/lit8 v5, v4, 0x3

    sub-int v6, v4, v5

    .line 136
    new-instance v7, Lcom/squareup/squarewave/o1/O1BinaryFilter$Readings;

    const/4 v8, 0x0

    invoke-direct {v7, v8}, Lcom/squareup/squarewave/o1/O1BinaryFilter$Readings;-><init>(Lcom/squareup/squarewave/o1/O1BinaryFilter$1;)V

    .line 137
    new-instance v8, Lcom/squareup/squarewave/o1/FlipDetector;

    move-object/from16 v9, p1

    invoke-direct {v8, v9, v1}, Lcom/squareup/squarewave/o1/FlipDetector;-><init>(Lcom/squareup/squarewave/o1/O1Swipe;Lcom/squareup/squarewave/o1/Threshold;)V

    const/4 v11, 0x0

    .line 140
    :goto_0
    array-length v12, v2

    if-ge v11, v12, :cond_5

    move v12, v11

    .line 146
    :goto_1
    array-length v13, v2

    if-ge v12, v13, :cond_0

    invoke-virtual {v8, v12}, Lcom/squareup/squarewave/o1/FlipDetector;->flipAt(I)Z

    move-result v13

    if-nez v13, :cond_0

    add-int/lit8 v12, v12, 0x1

    goto :goto_1

    :cond_0
    sub-int/2addr v12, v4

    .line 147
    invoke-static {v11, v12}, Ljava/lang/Math;->max(II)I

    move-result v11

    move v12, v11

    const/4 v11, 0x0

    const/4 v13, 0x0

    :goto_2
    const/16 v14, 0x9

    if-ge v11, v14, :cond_4

    .line 158
    new-instance v14, Lcom/squareup/squarewave/o1/O1BinaryFilter$Window;

    add-int v15, v12, v3

    invoke-direct {v14, v12, v15}, Lcom/squareup/squarewave/o1/O1BinaryFilter$Window;-><init>(II)V

    .line 161
    invoke-virtual {v14}, Lcom/squareup/squarewave/o1/O1BinaryFilter$Window;->getStart()I

    move-result v15

    invoke-virtual {v14}, Lcom/squareup/squarewave/o1/O1BinaryFilter$Window;->getEnd()I

    move-result v10

    invoke-virtual {v8, v15, v10}, Lcom/squareup/squarewave/o1/FlipDetector;->flipsBetween(II)[I

    move-result-object v10

    .line 162
    array-length v15, v10

    if-nez v15, :cond_1

    add-int/lit8 v13, v13, 0x1

    const/4 v15, 0x2

    if-le v13, v15, :cond_1

    goto :goto_4

    :cond_1
    if-eqz p3, :cond_2

    .line 166
    array-length v15, v2

    invoke-direct {v0, v14, v10, v4, v15}, Lcom/squareup/squarewave/o1/O1BinaryFilter;->adjust(Lcom/squareup/squarewave/o1/O1BinaryFilter$Window;[III)Lcom/squareup/squarewave/o1/O1BinaryFilter$Window;

    move-result-object v14

    .line 168
    :cond_2
    array-length v10, v2

    const/4 v15, 0x0

    invoke-virtual {v14, v15, v10}, Lcom/squareup/squarewave/o1/O1BinaryFilter$Window;->within(II)Z

    move-result v10

    if-nez v10, :cond_3

    goto :goto_3

    .line 172
    :cond_3
    invoke-virtual {v14}, Lcom/squareup/squarewave/o1/O1BinaryFilter$Window;->getStart()I

    move-result v10

    add-int/2addr v10, v5

    invoke-virtual {v14}, Lcom/squareup/squarewave/o1/O1BinaryFilter$Window;->getStart()I

    move-result v12

    add-int/2addr v12, v6

    invoke-direct {v0, v2, v10, v12}, Lcom/squareup/squarewave/o1/O1BinaryFilter;->average([SII)S

    move-result v10

    .line 174
    invoke-virtual {v14}, Lcom/squareup/squarewave/o1/O1BinaryFilter$Window;->getHalfway()I

    move-result v12

    add-int/2addr v12, v5

    invoke-virtual {v14}, Lcom/squareup/squarewave/o1/O1BinaryFilter$Window;->getHalfway()I

    move-result v16

    add-int v15, v16, v6

    invoke-direct {v0, v2, v12, v15}, Lcom/squareup/squarewave/o1/O1BinaryFilter;->average([SII)S

    move-result v12

    .line 178
    invoke-virtual {v14}, Lcom/squareup/squarewave/o1/O1BinaryFilter$Window;->getStart()I

    move-result v15

    move-object/from16 v16, v2

    invoke-virtual {v14}, Lcom/squareup/squarewave/o1/O1BinaryFilter$Window;->getHalfway()I

    move-result v2

    invoke-virtual {v7, v10, v15, v2}, Lcom/squareup/squarewave/o1/O1BinaryFilter$Readings;->addHalfCycleAverage(SII)V

    .line 179
    invoke-virtual {v14}, Lcom/squareup/squarewave/o1/O1BinaryFilter$Window;->getHalfway()I

    move-result v2

    invoke-virtual {v14}, Lcom/squareup/squarewave/o1/O1BinaryFilter$Window;->getEnd()I

    move-result v10

    invoke-virtual {v7, v12, v2, v10}, Lcom/squareup/squarewave/o1/O1BinaryFilter$Readings;->addHalfCycleAverage(SII)V

    .line 182
    invoke-virtual {v14}, Lcom/squareup/squarewave/o1/O1BinaryFilter$Window;->getEnd()I

    move-result v12

    add-int/lit8 v11, v11, 0x1

    move-object/from16 v2, v16

    goto :goto_2

    :cond_4
    :goto_3
    move-object/from16 v16, v2

    add-int/2addr v12, v6

    add-int/lit8 v11, v12, 0x1

    move-object/from16 v2, v16

    goto/16 :goto_0

    .line 191
    :cond_5
    :goto_4
    invoke-virtual/range {p1 .. p1}, Lcom/squareup/squarewave/o1/O1Swipe;->buildUpon()Lcom/squareup/squarewave/o1/O1Swipe$Builder;

    move-result-object v2

    invoke-virtual {v7}, Lcom/squareup/squarewave/o1/O1BinaryFilter$Readings;->getArray()[Lcom/squareup/squarewave/gen2/Reading;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/squarewave/o1/O1Swipe$Builder;->setReadings([Lcom/squareup/squarewave/gen2/Reading;)Lcom/squareup/squarewave/o1/O1Swipe$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/squareup/squarewave/o1/O1Swipe$Builder;->setThreshold(Lcom/squareup/squarewave/o1/Threshold;)Lcom/squareup/squarewave/o1/O1Swipe$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/squarewave/o1/O1Swipe$Builder;->build()Lcom/squareup/squarewave/o1/O1Swipe;

    move-result-object v1

    .line 193
    iget-object v2, v0, Lcom/squareup/squarewave/o1/O1BinaryFilter;->next:Lcom/squareup/squarewave/o1/O1SwipeFilter;

    invoke-interface {v2, v1}, Lcom/squareup/squarewave/o1/O1SwipeFilter;->filter(Lcom/squareup/squarewave/o1/O1Swipe;)Lcom/squareup/squarewave/o1/O1DemodResult;

    move-result-object v1

    .line 194
    iget-object v2, v1, Lcom/squareup/squarewave/o1/O1DemodResult;->globalResult:Lcom/squareup/squarewave/decode/DemodResult;

    invoke-virtual {v2}, Lcom/squareup/squarewave/decode/DemodResult;->isSuccessfulDemod()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 195
    invoke-virtual {v7}, Lcom/squareup/squarewave/o1/O1BinaryFilter$Readings;->clear()V

    :cond_6
    return-object v1
.end method

.method private average([SII)S
    .locals 3

    const/4 v0, 0x0

    move v0, p2

    const/4 v1, 0x0

    :goto_0
    if-ge v0, p3, :cond_0

    .line 240
    aget-short v2, p1, v0

    add-int/2addr v1, v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    sub-int/2addr p3, p2

    .line 242
    div-int/2addr v1, p3

    int-to-short p1, v1

    return p1
.end method

.method private static best(Lcom/squareup/squarewave/o1/O1DemodResult;Lcom/squareup/squarewave/o1/O1DemodResult;)Lcom/squareup/squarewave/o1/O1DemodResult;
    .locals 3

    if-eqz p0, :cond_4

    if-nez p1, :cond_0

    goto :goto_1

    :cond_0
    const/4 v0, 0x0

    .line 104
    :goto_0
    sget-object v1, Lcom/squareup/squarewave/o1/O1BinaryFilter;->PREFERRED_STATUSES:[Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;

    array-length v2, v1

    if-ge v0, v2, :cond_3

    .line 105
    aget-object v1, v1, v0

    .line 106
    iget-object v2, p0, Lcom/squareup/squarewave/o1/O1DemodResult;->detailedResult:Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;

    if-ne v2, v1, :cond_1

    return-object p0

    .line 107
    :cond_1
    iget-object v2, p1, Lcom/squareup/squarewave/o1/O1DemodResult;->detailedResult:Lcom/squareup/protos/logging/events/swipe_experience/O1DemodInfo$DemodResult;

    if-ne v2, v1, :cond_2

    return-object p1

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    return-object p0

    :cond_4
    :goto_1
    if-nez p0, :cond_5

    move-object p0, p1

    :cond_5
    return-object p0
.end method


# virtual methods
.method public filter(Lcom/squareup/squarewave/o1/O1Swipe;)Lcom/squareup/squarewave/o1/O1DemodResult;
    .locals 5

    .line 75
    iget-object v0, p0, Lcom/squareup/squarewave/o1/O1BinaryFilter;->toolThreshold:Lcom/squareup/squarewave/o1/Threshold;

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    .line 76
    invoke-direct {p0, p1, v0, v1}, Lcom/squareup/squarewave/o1/O1BinaryFilter;->attemptDecode(Lcom/squareup/squarewave/o1/O1Swipe;Lcom/squareup/squarewave/o1/Threshold;Z)Lcom/squareup/squarewave/o1/O1DemodResult;

    move-result-object p1

    return-object p1

    .line 80
    :cond_0
    new-instance v0, Lcom/squareup/squarewave/o1/VariableThreshold;

    invoke-direct {v0, p1}, Lcom/squareup/squarewave/o1/VariableThreshold;-><init>(Lcom/squareup/squarewave/o1/O1Swipe;)V

    invoke-virtual {p1, v0}, Lcom/squareup/squarewave/o1/O1Swipe;->setThreshold(Lcom/squareup/squarewave/o1/Threshold;)V

    .line 81
    invoke-virtual {p1}, Lcom/squareup/squarewave/o1/O1Swipe;->getThreshold()Lcom/squareup/squarewave/o1/Threshold;

    move-result-object v0

    invoke-direct {p0, p1, v0, v1}, Lcom/squareup/squarewave/o1/O1BinaryFilter;->attemptDecode(Lcom/squareup/squarewave/o1/O1Swipe;Lcom/squareup/squarewave/o1/Threshold;Z)Lcom/squareup/squarewave/o1/O1DemodResult;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 82
    iget-object v2, v0, Lcom/squareup/squarewave/o1/O1DemodResult;->globalResult:Lcom/squareup/squarewave/decode/DemodResult;

    invoke-virtual {v2}, Lcom/squareup/squarewave/decode/DemodResult;->isSuccessfulDemod()Z

    move-result v2

    if-eqz v2, :cond_1

    return-object v0

    :cond_1
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 88
    :goto_0
    sget-object v3, Lcom/squareup/squarewave/o1/O1BinaryFilter;->THRESHOLDS_TO_TRY:[S

    array-length v4, v3

    if-ge v2, v4, :cond_3

    .line 89
    new-instance v4, Lcom/squareup/squarewave/o1/ConstantThreshold;

    aget-short v3, v3, v2

    invoke-direct {v4, v3}, Lcom/squareup/squarewave/o1/ConstantThreshold;-><init>(S)V

    invoke-direct {p0, p1, v4, v1}, Lcom/squareup/squarewave/o1/O1BinaryFilter;->attemptDecode(Lcom/squareup/squarewave/o1/O1Swipe;Lcom/squareup/squarewave/o1/Threshold;Z)Lcom/squareup/squarewave/o1/O1DemodResult;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 90
    iget-object v4, v3, Lcom/squareup/squarewave/o1/O1DemodResult;->globalResult:Lcom/squareup/squarewave/decode/DemodResult;

    invoke-virtual {v4}, Lcom/squareup/squarewave/decode/DemodResult;->isSuccessfulDemod()Z

    move-result v4

    if-eqz v4, :cond_2

    return-object v3

    .line 93
    :cond_2
    invoke-static {v0, v3}, Lcom/squareup/squarewave/o1/O1BinaryFilter;->best(Lcom/squareup/squarewave/o1/O1DemodResult;Lcom/squareup/squarewave/o1/O1DemodResult;)Lcom/squareup/squarewave/o1/O1DemodResult;

    move-result-object v0

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    return-object v0
.end method

.method public setToolThreshold(Lcom/squareup/squarewave/o1/Threshold;)V
    .locals 0

    .line 70
    iput-object p1, p0, Lcom/squareup/squarewave/o1/O1BinaryFilter;->toolThreshold:Lcom/squareup/squarewave/o1/Threshold;

    return-void
.end method
