.class public final Lcom/squareup/transactionhistory/TimeInterval$Companion;
.super Ljava/lang/Object;
.source "Configuration.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/transactionhistory/TimeInterval;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0003\u001a\u00020\u0004J\u0016\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\u0008\u001a\u00020\u0007\u00a8\u0006\t"
    }
    d2 = {
        "Lcom/squareup/transactionhistory/TimeInterval$Companion;",
        "",
        "()V",
        "withNullStartAndEndTime",
        "Lcom/squareup/transactionhistory/TimeInterval;",
        "withStartAndEndTime",
        "startTimeInclusive",
        "Ljava/util/Date;",
        "endTimeExclusive",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 99
    invoke-direct {p0}, Lcom/squareup/transactionhistory/TimeInterval$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final withNullStartAndEndTime()Lcom/squareup/transactionhistory/TimeInterval;
    .locals 2

    .line 107
    new-instance v0, Lcom/squareup/transactionhistory/TimeInterval;

    const/4 v1, 0x0

    invoke-direct {v0, v1, v1, v1}, Lcom/squareup/transactionhistory/TimeInterval;-><init>(Ljava/util/Date;Ljava/util/Date;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method

.method public final withStartAndEndTime(Ljava/util/Date;Ljava/util/Date;)Lcom/squareup/transactionhistory/TimeInterval;
    .locals 2

    const-string v0, "startTimeInclusive"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "endTimeExclusive"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 120
    new-instance v0, Lcom/squareup/transactionhistory/TimeInterval;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p2, v1}, Lcom/squareup/transactionhistory/TimeInterval;-><init>(Ljava/util/Date;Ljava/util/Date;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method
