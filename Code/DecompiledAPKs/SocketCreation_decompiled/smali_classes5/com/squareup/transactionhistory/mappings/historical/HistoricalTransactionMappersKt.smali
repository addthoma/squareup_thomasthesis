.class public final Lcom/squareup/transactionhistory/mappings/historical/HistoricalTransactionMappersKt;
.super Ljava/lang/Object;
.source "HistoricalTransactionMappers.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nHistoricalTransactionMappers.kt\nKotlin\n*S Kotlin\n*F\n+ 1 HistoricalTransactionMappers.kt\ncom/squareup/transactionhistory/mappings/historical/HistoricalTransactionMappersKt\n+ 2 HistoricalTransaction.kt\ncom/squareup/transactionhistory/historical/HistoricalTransactionKt\n+ 3 PendingTransaction.kt\ncom/squareup/transactionhistory/pending/PendingTransactionKt\n+ 4 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n+ 5 ProcessedTransaction.kt\ncom/squareup/transactionhistory/processed/ProcessedTransactionKt\n*L\n1#1,175:1\n44#2:176\n55#3:177\n1360#4:178\n1429#4,3:179\n1360#4:183\n1429#4,3:184\n52#5:182\n*E\n*S KotlinDebug\n*F\n+ 1 HistoricalTransactionMappers.kt\ncom/squareup/transactionhistory/mappings/historical/HistoricalTransactionMappersKt\n*L\n44#1:176\n116#1:177\n135#1:178\n135#1,3:179\n167#1:183\n167#1,3:184\n143#1:182\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000X\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0002\u001a\u0018\u0010\u0000\u001a\u00020\u0001*\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0006\u0010\u0005\u001a\u00020\u0006\u001a\n\u0010\u0007\u001a\u00020\u0008*\u00020\u0004\u001a\n\u0010\u0007\u001a\u00020\u0008*\u00020\t\u001a\u001d\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000b*\u0008\u0012\u0004\u0012\u00020\r0\u000bH\u0007\u00a2\u0006\u0002\u0008\u000e\u001a\u001d\u0010\n\u001a\u0008\u0012\u0004\u0012\u00020\u000c0\u000b*\u0008\u0012\u0004\u0012\u00020\u000f0\u000bH\u0007\u00a2\u0006\u0002\u0008\u0010\u001a@\u0010\u0011\u001a\u00020\u000c*\u00020\u00122\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0013\u001a\u00020\u00142\u0008\u0008\u0002\u0010\u0015\u001a\u00020\u00162\u0008\u0008\u0002\u0010\u0017\u001a\u00020\u00182\u000e\u0008\u0002\u0010\u0019\u001a\u0008\u0012\u0004\u0012\u00020\u001a0\u000bH\u0007\u001a\u001a\u0010\u0011\u001a\u00020\u000c*\u00020\u00082\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0013\u001a\u00020\u0014\u001a\n\u0010\u0011\u001a\u00020\u000c*\u00020\r\u001a\n\u0010\u0011\u001a\u00020\u000c*\u00020\u000f\u00a8\u0006\u001b"
    }
    d2 = {
        "toFetchTransactionResult",
        "Lcom/squareup/transactionhistory/historical/FetchTransactionResult;",
        "Lcom/squareup/transactionhistory/processed/FetchTransactionResult;",
        "Lcom/squareup/util/Optional;",
        "Lcom/squareup/transactionhistory/pending/PendingTransaction;",
        "res",
        "Lcom/squareup/util/Res;",
        "toHistoricalTransaction",
        "Lcom/squareup/transactionhistory/historical/HistoricalTransaction;",
        "Lcom/squareup/transactionhistory/processed/ProcessedTransaction;",
        "toHistoricalTransactionSummaries",
        "",
        "Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;",
        "Lcom/squareup/transactionhistory/pending/PendingTransactionSummary;",
        "fromPendingToHistoricalTransactionSummaries",
        "Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;",
        "fromProcessedToHistoricalTransactionSummaries",
        "toHistoricalTransactionSummary",
        "Lcom/squareup/billhistory/model/BillHistory;",
        "perUnitFormatter",
        "Lcom/squareup/quantity/PerUnitFormatter;",
        "tenderId",
        "",
        "hasExpiringTip",
        "",
        "searchMatches",
        "Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final fromPendingToHistoricalTransactionSummaries(Ljava/util/List;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/transactionhistory/pending/PendingTransactionSummary;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$toHistoricalTransactionSummaries"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 135
    check-cast p0, Ljava/lang/Iterable;

    .line 178
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p0, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 179
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 180
    check-cast v1, Lcom/squareup/transactionhistory/pending/PendingTransactionSummary;

    .line 135
    invoke-static {v1}, Lcom/squareup/transactionhistory/mappings/historical/HistoricalTransactionMappersKt;->toHistoricalTransactionSummary(Lcom/squareup/transactionhistory/pending/PendingTransactionSummary;)Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 181
    :cond_0
    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public static final fromProcessedToHistoricalTransactionSummaries(Ljava/util/List;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;",
            ">;"
        }
    .end annotation

    const-string v0, "$this$toHistoricalTransactionSummaries"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 167
    check-cast p0, Ljava/lang/Iterable;

    .line 183
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-static {p0, v1}, Lkotlin/collections/CollectionsKt;->collectionSizeOrDefault(Ljava/lang/Iterable;I)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    check-cast v0, Ljava/util/Collection;

    .line 184
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 185
    check-cast v1, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;

    .line 167
    invoke-static {v1}, Lcom/squareup/transactionhistory/mappings/historical/HistoricalTransactionMappersKt;->toHistoricalTransactionSummary(Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;)Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 186
    :cond_0
    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public static final toFetchTransactionResult(Lcom/squareup/transactionhistory/processed/FetchTransactionResult;)Lcom/squareup/transactionhistory/historical/FetchTransactionResult;
    .locals 1

    const-string v0, "$this$toFetchTransactionResult"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 171
    instance-of v0, p0, Lcom/squareup/transactionhistory/processed/FetchTransactionResult$Success;

    if-eqz v0, :cond_0

    .line 172
    new-instance v0, Lcom/squareup/transactionhistory/historical/FetchTransactionResult$Success;

    check-cast p0, Lcom/squareup/transactionhistory/processed/FetchTransactionResult$Success;

    invoke-virtual {p0}, Lcom/squareup/transactionhistory/processed/FetchTransactionResult$Success;->getTransaction()Lcom/squareup/transactionhistory/processed/ProcessedTransaction;

    move-result-object p0

    invoke-static {p0}, Lcom/squareup/transactionhistory/mappings/historical/HistoricalTransactionMappersKt;->toHistoricalTransaction(Lcom/squareup/transactionhistory/processed/ProcessedTransaction;)Lcom/squareup/transactionhistory/historical/HistoricalTransaction;

    move-result-object p0

    invoke-direct {v0, p0}, Lcom/squareup/transactionhistory/historical/FetchTransactionResult$Success;-><init>(Lcom/squareup/transactionhistory/historical/HistoricalTransaction;)V

    check-cast v0, Lcom/squareup/transactionhistory/historical/FetchTransactionResult;

    goto :goto_0

    .line 173
    :cond_0
    instance-of v0, p0, Lcom/squareup/transactionhistory/processed/FetchTransactionResult$Failure;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/squareup/transactionhistory/historical/FetchTransactionResult$Failure;

    check-cast p0, Lcom/squareup/transactionhistory/processed/FetchTransactionResult$Failure;

    invoke-virtual {p0}, Lcom/squareup/transactionhistory/processed/FetchTransactionResult$Failure;->getFailureMessage()Lcom/squareup/receiving/FailureMessage;

    move-result-object p0

    invoke-direct {v0, p0}, Lcom/squareup/transactionhistory/historical/FetchTransactionResult$Failure;-><init>(Lcom/squareup/receiving/FailureMessage;)V

    check-cast v0, Lcom/squareup/transactionhistory/historical/FetchTransactionResult;

    :goto_0
    return-object v0

    :cond_1
    new-instance p0, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {p0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw p0
.end method

.method public static final toFetchTransactionResult(Lcom/squareup/util/Optional;Lcom/squareup/util/Res;)Lcom/squareup/transactionhistory/historical/FetchTransactionResult;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/transactionhistory/pending/PendingTransaction;",
            ">;",
            "Lcom/squareup/util/Res;",
            ")",
            "Lcom/squareup/transactionhistory/historical/FetchTransactionResult;"
        }
    .end annotation

    const-string v0, "$this$toFetchTransactionResult"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 138
    invoke-virtual {p0}, Lcom/squareup/util/Optional;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance p1, Lcom/squareup/transactionhistory/historical/FetchTransactionResult$Success;

    invoke-virtual {p0}, Lcom/squareup/util/Optional;->getValue()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/transactionhistory/pending/PendingTransaction;

    invoke-static {p0}, Lcom/squareup/transactionhistory/mappings/historical/HistoricalTransactionMappersKt;->toHistoricalTransaction(Lcom/squareup/transactionhistory/pending/PendingTransaction;)Lcom/squareup/transactionhistory/historical/HistoricalTransaction;

    move-result-object p0

    invoke-direct {p1, p0}, Lcom/squareup/transactionhistory/historical/FetchTransactionResult$Success;-><init>(Lcom/squareup/transactionhistory/historical/HistoricalTransaction;)V

    check-cast p1, Lcom/squareup/transactionhistory/historical/FetchTransactionResult;

    goto :goto_0

    .line 140
    :cond_0
    new-instance p0, Lcom/squareup/transactionhistory/historical/FetchTransactionResult$Failure;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/squareup/transactionhistory/utilities/FailureMessagesKt;->defaultFailureMessage(Lcom/squareup/util/Res;Z)Lcom/squareup/receiving/FailureMessage;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/squareup/transactionhistory/historical/FetchTransactionResult$Failure;-><init>(Lcom/squareup/receiving/FailureMessage;)V

    move-object p1, p0

    check-cast p1, Lcom/squareup/transactionhistory/historical/FetchTransactionResult;

    :goto_0
    return-object p1
.end method

.method public static final toHistoricalTransaction(Lcom/squareup/transactionhistory/pending/PendingTransaction;)Lcom/squareup/transactionhistory/historical/HistoricalTransaction;
    .locals 1

    const-string v0, "$this$toHistoricalTransaction"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 177
    invoke-virtual {p0}, Lcom/squareup/transactionhistory/pending/PendingTransaction;->getBillHistory()Ljava/lang/Object;

    move-result-object p0

    if-eqz p0, :cond_0

    check-cast p0, Lcom/squareup/billhistory/model/BillHistory;

    .line 116
    new-instance v0, Lcom/squareup/billhistory/model/BillHistory$Builder;

    invoke-direct {v0, p0}, Lcom/squareup/billhistory/model/BillHistory$Builder;-><init>(Lcom/squareup/billhistory/model/BillHistory;)V

    invoke-virtual {v0}, Lcom/squareup/billhistory/model/BillHistory$Builder;->build()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object p0

    const-string v0, "BillHistory.Builder(asBi\u2026y<BillHistory>()).build()"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/transactionhistory/historical/HistoricalTransaction;

    invoke-direct {v0, p0}, Lcom/squareup/transactionhistory/historical/HistoricalTransaction;-><init>(Ljava/lang/Object;)V

    return-object v0

    .line 177
    :cond_0
    new-instance p0, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.squareup.billhistory.model.BillHistory"

    invoke-direct {p0, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static final toHistoricalTransaction(Lcom/squareup/transactionhistory/processed/ProcessedTransaction;)Lcom/squareup/transactionhistory/historical/HistoricalTransaction;
    .locals 1

    const-string v0, "$this$toHistoricalTransaction"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 182
    invoke-virtual {p0}, Lcom/squareup/transactionhistory/processed/ProcessedTransaction;->getBillHistory()Ljava/lang/Object;

    move-result-object p0

    if-eqz p0, :cond_0

    check-cast p0, Lcom/squareup/billhistory/model/BillHistory;

    .line 143
    new-instance v0, Lcom/squareup/billhistory/model/BillHistory$Builder;

    invoke-direct {v0, p0}, Lcom/squareup/billhistory/model/BillHistory$Builder;-><init>(Lcom/squareup/billhistory/model/BillHistory;)V

    invoke-virtual {v0}, Lcom/squareup/billhistory/model/BillHistory$Builder;->build()Lcom/squareup/billhistory/model/BillHistory;

    move-result-object p0

    const-string v0, "BillHistory.Builder(asBi\u2026y<BillHistory>()).build()"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/transactionhistory/historical/HistoricalTransaction;

    invoke-direct {v0, p0}, Lcom/squareup/transactionhistory/historical/HistoricalTransaction;-><init>(Ljava/lang/Object;)V

    return-object v0

    .line 182
    :cond_0
    new-instance p0, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.squareup.billhistory.model.BillHistory"

    invoke-direct {p0, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static final toHistoricalTransactionSummary(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;)Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;
    .locals 8

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x1c

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-static/range {v0 .. v7}, Lcom/squareup/transactionhistory/mappings/historical/HistoricalTransactionMappersKt;->toHistoricalTransactionSummary$default(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;Ljava/lang/String;ZLjava/util/List;ILjava/lang/Object;)Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;

    move-result-object p0

    return-object p0
.end method

.method public static final toHistoricalTransactionSummary(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;Ljava/lang/String;)Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;
    .locals 8

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x18

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-static/range {v0 .. v7}, Lcom/squareup/transactionhistory/mappings/historical/HistoricalTransactionMappersKt;->toHistoricalTransactionSummary$default(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;Ljava/lang/String;ZLjava/util/List;ILjava/lang/Object;)Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;

    move-result-object p0

    return-object p0
.end method

.method public static final toHistoricalTransactionSummary(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;Ljava/lang/String;Z)Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;
    .locals 8

    const/4 v5, 0x0

    const/16 v6, 0x10

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    invoke-static/range {v0 .. v7}, Lcom/squareup/transactionhistory/mappings/historical/HistoricalTransactionMappersKt;->toHistoricalTransactionSummary$default(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;Ljava/lang/String;ZLjava/util/List;ILjava/lang/Object;)Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;

    move-result-object p0

    return-object p0
.end method

.method public static final toHistoricalTransactionSummary(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;Ljava/lang/String;ZLjava/util/List;)Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/billhistory/model/BillHistory;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/quantity/PerUnitFormatter;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/List<",
            "Lcom/squareup/transactionhistory/processed/TransactionSearchMatch;",
            ">;)",
            "Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    const-string v3, "$this$toHistoricalTransactionSummary"

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "res"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "perUnitFormatter"

    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "tenderId"

    move-object/from16 v11, p3

    invoke-static {v11, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v3, "searchMatches"

    move-object/from16 v15, p5

    invoke-static {v15, v3}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    iget-object v3, v0, Lcom/squareup/billhistory/model/BillHistory;->id:Lcom/squareup/billhistory/model/BillHistoryId;

    const-string v4, "id"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/squareup/billhistory/model/BillHistoryId;->getId()Lcom/squareup/protos/client/IdPair;

    move-result-object v3

    iget-object v6, v3, Lcom/squareup/protos/client/IdPair;->client_id:Ljava/lang/String;

    .line 76
    iget-object v3, v0, Lcom/squareup/billhistory/model/BillHistory;->id:Lcom/squareup/billhistory/model/BillHistoryId;

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/squareup/billhistory/model/BillHistoryId;->getId()Lcom/squareup/protos/client/IdPair;

    move-result-object v3

    iget-object v7, v3, Lcom/squareup/protos/client/IdPair;->server_id:Ljava/lang/String;

    .line 77
    iget-object v8, v0, Lcom/squareup/billhistory/model/BillHistory;->time:Ljava/util/Date;

    const-string/jumbo v3, "time"

    invoke-static {v8, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    iget-object v10, v0, Lcom/squareup/billhistory/model/BillHistory;->total:Lcom/squareup/protos/common/Money;

    const-string/jumbo v3, "total"

    invoke-static {v10, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 79
    iget-object v3, v0, Lcom/squareup/billhistory/model/BillHistory;->note:Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;

    if-eqz v3, :cond_0

    invoke-virtual {v3, v1, v2}, Lcom/squareup/billhistory/model/BillHistory$ItemizationsNote;->localizedNote(Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    .line 80
    :goto_0
    invoke-static/range {p0 .. p0}, Lcom/squareup/transactionhistory/mappings/TransactionHistoryMappersKt;->extractTenderInfo(Lcom/squareup/billhistory/model/BillHistory;)Ljava/util/List;

    move-result-object v12

    .line 81
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/billhistory/model/BillHistory;->isNoSale()Z

    move-result v2

    .line 82
    iget-boolean v3, v0, Lcom/squareup/billhistory/model/BillHistory;->isVoided:Z

    .line 83
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/billhistory/model/BillHistory;->hasError()Z

    move-result v4

    .line 84
    iget-boolean v5, v0, Lcom/squareup/billhistory/model/BillHistory;->pending:Z

    if-eqz v5, :cond_1

    sget-object v5, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;->Companion:Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary$Companion;

    .line 88
    iget-object v9, v0, Lcom/squareup/billhistory/model/BillHistory;->storeAndForward:Lcom/squareup/transactionhistory/pending/StoreAndForwardState;

    .line 92
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v13

    .line 93
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v14

    .line 94
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v15

    move-object v11, v1

    .line 84
    invoke-virtual/range {v5 .. v15}, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary$Companion;->newHistoricalSummaryForPendingTransaction(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Lcom/squareup/transactionhistory/pending/StoreAndForwardState;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;

    move-result-object v0

    goto :goto_1

    .line 96
    :cond_1
    sget-object v5, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;->Companion:Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary$Companion;

    .line 100
    invoke-static/range {p0 .. p0}, Lcom/squareup/transactionhistory/mappings/TransactionHistoryMappersKt;->transactionType(Lcom/squareup/billhistory/model/BillHistory;)Lcom/squareup/transactionhistory/TransactionType;

    move-result-object v9

    .line 105
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/billhistory/model/BillHistory;->isAwaitingMerchantTip()Z

    move-result v13

    invoke-static {v13}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v13

    .line 106
    invoke-static/range {p4 .. p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v14

    .line 107
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 108
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v16

    .line 109
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v17

    .line 110
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/billhistory/model/BillHistory;->getRelatedBillsExceptFirst()Ljava/util/List;

    move-result-object v0

    const-string v3, "relatedBillsExceptFirst"

    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v18

    move-object v4, v5

    move-object v5, v6

    move-object v6, v7

    move-object v7, v8

    move-object v8, v9

    move-object v9, v10

    move-object v10, v1

    move-object/from16 v11, p3

    move-object v15, v2

    move-object/from16 v19, p5

    .line 96
    invoke-virtual/range {v4 .. v19}, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary$Companion;->newHistoricalSummaryForProcessedTransaction(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Lcom/squareup/transactionhistory/TransactionType;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/util/List;)Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;

    move-result-object v0

    :goto_1
    return-object v0
.end method

.method public static final toHistoricalTransactionSummary(Lcom/squareup/transactionhistory/historical/HistoricalTransaction;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;)Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;
    .locals 8

    const-string v0, "$this$toHistoricalTransactionSummary"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "perUnitFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 176
    invoke-virtual {p0}, Lcom/squareup/transactionhistory/historical/HistoricalTransaction;->getBillHistory()Ljava/lang/Object;

    move-result-object p0

    if-eqz p0, :cond_0

    move-object v0, p0

    check-cast v0, Lcom/squareup/billhistory/model/BillHistory;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v6, 0x1c

    const/4 v7, 0x0

    move-object v1, p1

    move-object v2, p2

    .line 44
    invoke-static/range {v0 .. v7}, Lcom/squareup/transactionhistory/mappings/historical/HistoricalTransactionMappersKt;->toHistoricalTransactionSummary$default(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;Ljava/lang/String;ZLjava/util/List;ILjava/lang/Object;)Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;

    move-result-object p0

    return-object p0

    .line 176
    :cond_0
    new-instance p0, Lkotlin/TypeCastException;

    const-string p1, "null cannot be cast to non-null type com.squareup.billhistory.model.BillHistory"

    invoke-direct {p0, p1}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static final toHistoricalTransactionSummary(Lcom/squareup/transactionhistory/pending/PendingTransactionSummary;)Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;
    .locals 12

    const-string v0, "$this$toHistoricalTransactionSummary"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 119
    sget-object v1, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;->Companion:Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary$Companion;

    .line 120
    invoke-virtual {p0}, Lcom/squareup/transactionhistory/pending/PendingTransactionSummary;->getClientId()Ljava/lang/String;

    move-result-object v2

    .line 121
    invoke-virtual {p0}, Lcom/squareup/transactionhistory/pending/PendingTransactionSummary;->getServerId()Ljava/lang/String;

    move-result-object v3

    .line 122
    invoke-virtual {p0}, Lcom/squareup/transactionhistory/pending/PendingTransactionSummary;->getDate()Ljava/util/Date;

    move-result-object v4

    .line 123
    invoke-virtual {p0}, Lcom/squareup/transactionhistory/pending/PendingTransactionSummary;->getStoreAndForwardState()Lcom/squareup/transactionhistory/pending/StoreAndForwardState;

    move-result-object v5

    .line 124
    invoke-virtual {p0}, Lcom/squareup/transactionhistory/pending/PendingTransactionSummary;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v6

    .line 125
    invoke-virtual {p0}, Lcom/squareup/transactionhistory/pending/PendingTransactionSummary;->getDescription()Ljava/lang/String;

    move-result-object v7

    .line 126
    invoke-virtual {p0}, Lcom/squareup/transactionhistory/pending/PendingTransactionSummary;->getTenderInfo()Ljava/util/List;

    move-result-object v8

    .line 127
    invoke-virtual {p0}, Lcom/squareup/transactionhistory/pending/PendingTransactionSummary;->isNoSale()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    .line 128
    invoke-virtual {p0}, Lcom/squareup/transactionhistory/pending/PendingTransactionSummary;->isFullyVoided()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    .line 129
    invoke-virtual {p0}, Lcom/squareup/transactionhistory/pending/PendingTransactionSummary;->getHasError()Z

    move-result p0

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    .line 119
    invoke-virtual/range {v1 .. v11}, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary$Companion;->newHistoricalSummaryForPendingTransaction(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Lcom/squareup/transactionhistory/pending/StoreAndForwardState;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;

    move-result-object p0

    return-object p0
.end method

.method public static final toHistoricalTransactionSummary(Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;)Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;
    .locals 17

    const-string v0, "$this$toHistoricalTransactionSummary"

    move-object/from16 v1, p0

    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 146
    sget-object v0, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;->Companion:Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary$Companion;

    .line 147
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->getClientId()Ljava/lang/String;

    move-result-object v2

    .line 148
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->getServerId()Ljava/lang/String;

    move-result-object v3

    .line 149
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->getDate()Ljava/util/Date;

    move-result-object v4

    .line 150
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->getTransactionType()Lcom/squareup/transactionhistory/TransactionType;

    move-result-object v5

    .line 151
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->getAmount()Lcom/squareup/protos/common/Money;

    move-result-object v6

    .line 152
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->getDescription()Ljava/lang/String;

    move-result-object v7

    .line 153
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->getTenderId()Ljava/lang/String;

    move-result-object v8

    .line 154
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->getTenderInfo()Ljava/util/List;

    move-result-object v9

    .line 155
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->isAwaitingTip()Z

    move-result v10

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    .line 156
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->getHasExpiringTip()Z

    move-result v11

    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v11

    .line 157
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->isNoSale()Z

    move-result v12

    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v12

    .line 158
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->isFullyVoided()Z

    move-result v13

    invoke-static {v13}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v13

    .line 159
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->getHasError()Z

    move-result v14

    invoke-static {v14}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v14

    .line 160
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->getHasRelatedTransactions()Z

    move-result v15

    invoke-static {v15}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v15

    .line 161
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/transactionhistory/processed/ProcessedTransactionSummary;->getSearchMatches()Ljava/util/List;

    move-result-object v16

    move-object v1, v0

    .line 146
    invoke-virtual/range {v1 .. v16}, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary$Companion;->newHistoricalSummaryForProcessedTransaction(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Lcom/squareup/transactionhistory/TransactionType;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/util/List;)Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic toHistoricalTransactionSummary$default(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;Ljava/lang/String;ZLjava/util/List;ILjava/lang/Object;)Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;
    .locals 6

    and-int/lit8 p7, p6, 0x4

    if-eqz p7, :cond_0

    const-string p3, ""

    :cond_0
    move-object v3, p3

    and-int/lit8 p3, p6, 0x8

    if-eqz p3, :cond_1

    const/4 p4, 0x0

    const/4 v4, 0x0

    goto :goto_0

    :cond_1
    move v4, p4

    :goto_0
    and-int/lit8 p3, p6, 0x10

    if-eqz p3, :cond_2

    .line 73
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object p5

    :cond_2
    move-object v5, p5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-static/range {v0 .. v5}, Lcom/squareup/transactionhistory/mappings/historical/HistoricalTransactionMappersKt;->toHistoricalTransactionSummary(Lcom/squareup/billhistory/model/BillHistory;Lcom/squareup/util/Res;Lcom/squareup/quantity/PerUnitFormatter;Ljava/lang/String;ZLjava/util/List;)Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;

    move-result-object p0

    return-object p0
.end method
