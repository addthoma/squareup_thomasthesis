.class public final enum Lcom/squareup/transactionhistory/mappings/TransactionTypeMapping;
.super Ljava/lang/Enum;
.source "TransactionTypeMapping.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/transactionhistory/mappings/TransactionTypeMapping;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\t\u0008\u0080\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001B\u0017\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006R\u0014\u0010\u0002\u001a\u00020\u0003X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0007\u0010\u0008R\u0014\u0010\u0004\u001a\u00020\u0005X\u0080\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nj\u0002\u0008\u000bj\u0002\u0008\u000cj\u0002\u0008\r\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/transactionhistory/mappings/TransactionTypeMapping;",
        "",
        "protoTransactionType",
        "Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;",
        "transactionHistoryTransactionType",
        "Lcom/squareup/transactionhistory/TransactionType;",
        "(Ljava/lang/String;ILcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;Lcom/squareup/transactionhistory/TransactionType;)V",
        "getProtoTransactionType$public_release",
        "()Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;",
        "getTransactionHistoryTransactionType$public_release",
        "()Lcom/squareup/transactionhistory/TransactionType;",
        "SALE",
        "REFUND",
        "EXCHANGE",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/transactionhistory/mappings/TransactionTypeMapping;

.field public static final enum EXCHANGE:Lcom/squareup/transactionhistory/mappings/TransactionTypeMapping;

.field public static final enum REFUND:Lcom/squareup/transactionhistory/mappings/TransactionTypeMapping;

.field public static final enum SALE:Lcom/squareup/transactionhistory/mappings/TransactionTypeMapping;


# instance fields
.field private final protoTransactionType:Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;

.field private final transactionHistoryTransactionType:Lcom/squareup/transactionhistory/TransactionType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/squareup/transactionhistory/mappings/TransactionTypeMapping;

    new-instance v1, Lcom/squareup/transactionhistory/mappings/TransactionTypeMapping;

    .line 18
    sget-object v2, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;->SALE:Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;

    .line 19
    sget-object v3, Lcom/squareup/transactionhistory/TransactionType;->SALE:Lcom/squareup/transactionhistory/TransactionType;

    const/4 v4, 0x0

    const-string v5, "SALE"

    .line 17
    invoke-direct {v1, v5, v4, v2, v3}, Lcom/squareup/transactionhistory/mappings/TransactionTypeMapping;-><init>(Ljava/lang/String;ILcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;Lcom/squareup/transactionhistory/TransactionType;)V

    sput-object v1, Lcom/squareup/transactionhistory/mappings/TransactionTypeMapping;->SALE:Lcom/squareup/transactionhistory/mappings/TransactionTypeMapping;

    aput-object v1, v0, v4

    new-instance v1, Lcom/squareup/transactionhistory/mappings/TransactionTypeMapping;

    .line 22
    sget-object v2, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;->REFUND:Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;

    .line 23
    sget-object v3, Lcom/squareup/transactionhistory/TransactionType;->REFUND:Lcom/squareup/transactionhistory/TransactionType;

    const/4 v4, 0x1

    const-string v5, "REFUND"

    .line 21
    invoke-direct {v1, v5, v4, v2, v3}, Lcom/squareup/transactionhistory/mappings/TransactionTypeMapping;-><init>(Ljava/lang/String;ILcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;Lcom/squareup/transactionhistory/TransactionType;)V

    sput-object v1, Lcom/squareup/transactionhistory/mappings/TransactionTypeMapping;->REFUND:Lcom/squareup/transactionhistory/mappings/TransactionTypeMapping;

    aput-object v1, v0, v4

    new-instance v1, Lcom/squareup/transactionhistory/mappings/TransactionTypeMapping;

    .line 26
    sget-object v2, Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;->EXCHANGE:Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;

    .line 27
    sget-object v3, Lcom/squareup/transactionhistory/TransactionType;->EXCHANGE:Lcom/squareup/transactionhistory/TransactionType;

    const/4 v4, 0x2

    const-string v5, "EXCHANGE"

    .line 25
    invoke-direct {v1, v5, v4, v2, v3}, Lcom/squareup/transactionhistory/mappings/TransactionTypeMapping;-><init>(Ljava/lang/String;ILcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;Lcom/squareup/transactionhistory/TransactionType;)V

    sput-object v1, Lcom/squareup/transactionhistory/mappings/TransactionTypeMapping;->EXCHANGE:Lcom/squareup/transactionhistory/mappings/TransactionTypeMapping;

    aput-object v1, v0, v4

    sput-object v0, Lcom/squareup/transactionhistory/mappings/TransactionTypeMapping;->$VALUES:[Lcom/squareup/transactionhistory/mappings/TransactionTypeMapping;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;Lcom/squareup/transactionhistory/TransactionType;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;",
            "Lcom/squareup/transactionhistory/TransactionType;",
            ")V"
        }
    .end annotation

    .line 13
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/squareup/transactionhistory/mappings/TransactionTypeMapping;->protoTransactionType:Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;

    iput-object p4, p0, Lcom/squareup/transactionhistory/mappings/TransactionTypeMapping;->transactionHistoryTransactionType:Lcom/squareup/transactionhistory/TransactionType;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/transactionhistory/mappings/TransactionTypeMapping;
    .locals 1

    const-class v0, Lcom/squareup/transactionhistory/mappings/TransactionTypeMapping;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/transactionhistory/mappings/TransactionTypeMapping;

    return-object p0
.end method

.method public static values()[Lcom/squareup/transactionhistory/mappings/TransactionTypeMapping;
    .locals 1

    sget-object v0, Lcom/squareup/transactionhistory/mappings/TransactionTypeMapping;->$VALUES:[Lcom/squareup/transactionhistory/mappings/TransactionTypeMapping;

    invoke-virtual {v0}, [Lcom/squareup/transactionhistory/mappings/TransactionTypeMapping;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/transactionhistory/mappings/TransactionTypeMapping;

    return-object v0
.end method


# virtual methods
.method public final getProtoTransactionType$public_release()Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;
    .locals 1

    .line 14
    iget-object v0, p0, Lcom/squareup/transactionhistory/mappings/TransactionTypeMapping;->protoTransactionType:Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;

    return-object v0
.end method

.method public final getTransactionHistoryTransactionType$public_release()Lcom/squareup/transactionhistory/TransactionType;
    .locals 1

    .line 15
    iget-object v0, p0, Lcom/squareup/transactionhistory/mappings/TransactionTypeMapping;->transactionHistoryTransactionType:Lcom/squareup/transactionhistory/TransactionType;

    return-object v0
.end method
