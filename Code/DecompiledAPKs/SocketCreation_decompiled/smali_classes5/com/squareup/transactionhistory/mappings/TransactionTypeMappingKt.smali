.class public final Lcom/squareup/transactionhistory/mappings/TransactionTypeMappingKt;
.super Ljava/lang/Object;
.source "TransactionTypeMapping.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nTransactionTypeMapping.kt\nKotlin\n*S Kotlin\n*F\n+ 1 TransactionTypeMapping.kt\ncom/squareup/transactionhistory/mappings/TransactionTypeMappingKt\n*L\n1#1,37:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u001a\u000c\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\u0000\u00a8\u0006\u0003"
    }
    d2 = {
        "toTransactionType",
        "Lcom/squareup/transactionhistory/TransactionType;",
        "Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public static final toTransactionType(Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;)Lcom/squareup/transactionhistory/TransactionType;
    .locals 6

    const-string v0, "$this$toTransactionType"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    invoke-static {}, Lcom/squareup/transactionhistory/mappings/TransactionTypeMapping;->values()[Lcom/squareup/transactionhistory/mappings/TransactionTypeMapping;

    move-result-object v0

    .line 33
    array-length v1, v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_2

    aget-object v4, v0, v3

    invoke-virtual {v4}, Lcom/squareup/transactionhistory/mappings/TransactionTypeMapping;->getProtoTransactionType$public_release()Lcom/squareup/protos/transactionsfe/ListTransactionsResponse$Transaction$Type;

    move-result-object v5

    if-ne v5, p0, :cond_0

    const/4 v5, 0x1

    goto :goto_1

    :cond_0
    const/4 v5, 0x0

    :goto_1
    if-eqz v5, :cond_1

    goto :goto_2

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    const/4 v4, 0x0

    :goto_2
    if-eqz v4, :cond_3

    .line 34
    invoke-virtual {v4}, Lcom/squareup/transactionhistory/mappings/TransactionTypeMapping;->getTransactionHistoryTransactionType$public_release()Lcom/squareup/transactionhistory/TransactionType;

    move-result-object v0

    if-eqz v0, :cond_3

    return-object v0

    .line 35
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unrecognized transaction type: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast v0, Ljava/lang/Throwable;

    throw v0
.end method
