.class public final Lcom/squareup/transactionhistory/pending/PendingTransactionSummary$Companion;
.super Ljava/lang/Object;
.source "PendingTransactionSummary.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/transactionhistory/pending/PendingTransactionSummary;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0004\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u007f\u0010\u0003\u001a\u00020\u00042\n\u0008\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00062\n\u0008\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u00062\u0006\u0010\u0008\u001a\u00020\t2\u0008\u0010\n\u001a\u0004\u0018\u00010\u000b2\u0006\u0010\u000c\u001a\u00020\r2\n\u0008\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u00062\u0010\u0008\u0002\u0010\u000f\u001a\n\u0012\u0004\u0012\u00020\u0011\u0018\u00010\u00102\n\u0008\u0002\u0010\u0012\u001a\u0004\u0018\u00010\u00132\n\u0008\u0002\u0010\u0014\u001a\u0004\u0018\u00010\u00132\n\u0008\u0002\u0010\u0015\u001a\u0004\u0018\u00010\u0013\u00a2\u0006\u0002\u0010\u0016\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/transactionhistory/pending/PendingTransactionSummary$Companion;",
        "",
        "()V",
        "newPendingTransactionSummary",
        "Lcom/squareup/transactionhistory/pending/PendingTransactionSummary;",
        "clientId",
        "",
        "serverId",
        "date",
        "Ljava/util/Date;",
        "storeAndForwardState",
        "Lcom/squareup/transactionhistory/pending/StoreAndForwardState;",
        "amount",
        "Lcom/squareup/protos/common/Money;",
        "description",
        "tenderInfo",
        "",
        "Lcom/squareup/transactionhistory/TenderInfo;",
        "isNoSale",
        "",
        "isFullyVoided",
        "hasError",
        "(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Lcom/squareup/transactionhistory/pending/StoreAndForwardState;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)Lcom/squareup/transactionhistory/pending/PendingTransactionSummary;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 87
    invoke-direct {p0}, Lcom/squareup/transactionhistory/pending/PendingTransactionSummary$Companion;-><init>()V

    return-void
.end method

.method public static synthetic newPendingTransactionSummary$default(Lcom/squareup/transactionhistory/pending/PendingTransactionSummary$Companion;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Lcom/squareup/transactionhistory/pending/StoreAndForwardState;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;ILjava/lang/Object;)Lcom/squareup/transactionhistory/pending/PendingTransactionSummary;
    .locals 14

    move/from16 v0, p11

    and-int/lit8 v1, v0, 0x1

    const-string v2, ""

    if-eqz v1, :cond_0

    move-object v4, v2

    goto :goto_0

    :cond_0
    move-object v4, p1

    :goto_0
    and-int/lit8 v1, v0, 0x2

    if-eqz v1, :cond_1

    move-object v5, v2

    goto :goto_1

    :cond_1
    move-object/from16 v5, p2

    :goto_1
    and-int/lit8 v1, v0, 0x20

    if-eqz v1, :cond_2

    move-object v9, v2

    goto :goto_2

    :cond_2
    move-object/from16 v9, p6

    :goto_2
    and-int/lit8 v1, v0, 0x40

    if-eqz v1, :cond_3

    .line 102
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v1

    move-object v10, v1

    goto :goto_3

    :cond_3
    move-object/from16 v10, p7

    :goto_3
    and-int/lit16 v1, v0, 0x80

    const/4 v2, 0x0

    if-eqz v1, :cond_4

    .line 103
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    move-object v11, v1

    goto :goto_4

    :cond_4
    move-object/from16 v11, p8

    :goto_4
    and-int/lit16 v1, v0, 0x100

    if-eqz v1, :cond_5

    .line 104
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    move-object v12, v1

    goto :goto_5

    :cond_5
    move-object/from16 v12, p9

    :goto_5
    and-int/lit16 v0, v0, 0x200

    if-eqz v0, :cond_6

    .line 105
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    move-object v13, v0

    goto :goto_6

    :cond_6
    move-object/from16 v13, p10

    :goto_6
    move-object v3, p0

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    move-object/from16 v8, p5

    invoke-virtual/range {v3 .. v13}, Lcom/squareup/transactionhistory/pending/PendingTransactionSummary$Companion;->newPendingTransactionSummary(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Lcom/squareup/transactionhistory/pending/StoreAndForwardState;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)Lcom/squareup/transactionhistory/pending/PendingTransactionSummary;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final newPendingTransactionSummary(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Lcom/squareup/transactionhistory/pending/StoreAndForwardState;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)Lcom/squareup/transactionhistory/pending/PendingTransactionSummary;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Date;",
            "Lcom/squareup/transactionhistory/pending/StoreAndForwardState;",
            "Lcom/squareup/protos/common/Money;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/transactionhistory/TenderInfo;",
            ">;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            ")",
            "Lcom/squareup/transactionhistory/pending/PendingTransactionSummary;"
        }
    .end annotation

    const-string v0, "date"

    move-object/from16 v4, p3

    invoke-static {v4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "amount"

    move-object/from16 v6, p5

    invoke-static {v6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 107
    invoke-static {p1, p2}, Lcom/squareup/transactionhistory/TransactionHistoriesKt;->requireAtLeastOneNonEmpty(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    new-instance v0, Lcom/squareup/transactionhistory/pending/PendingTransactionSummary;

    const-string v1, ""

    if-eqz p1, :cond_0

    move-object v2, p1

    goto :goto_0

    :cond_0
    move-object v2, v1

    :goto_0
    if-eqz p2, :cond_1

    move-object v3, p2

    goto :goto_1

    :cond_1
    move-object v3, v1

    :goto_1
    if-eqz p4, :cond_2

    move-object/from16 v5, p4

    goto :goto_2

    .line 116
    :cond_2
    sget-object v5, Lcom/squareup/transactionhistory/pending/StoreAndForwardState;->UNTRACKED:Lcom/squareup/transactionhistory/pending/StoreAndForwardState;

    :goto_2
    if-eqz p6, :cond_3

    move-object/from16 v7, p6

    goto :goto_3

    :cond_3
    move-object v7, v1

    :goto_3
    if-eqz p7, :cond_4

    move-object/from16 v8, p7

    goto :goto_4

    .line 119
    :cond_4
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v1

    move-object v8, v1

    :goto_4
    const/4 v1, 0x0

    if-eqz p8, :cond_5

    .line 120
    invoke-virtual/range {p8 .. p8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    goto :goto_5

    :cond_5
    const/4 v9, 0x0

    :goto_5
    if-eqz p9, :cond_6

    .line 121
    invoke-virtual/range {p9 .. p9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v10

    goto :goto_6

    :cond_6
    const/4 v10, 0x0

    :goto_6
    if-eqz p10, :cond_7

    .line 122
    invoke-virtual/range {p10 .. p10}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    move v11, v1

    goto :goto_7

    :cond_7
    const/4 v11, 0x0

    :goto_7
    const/4 v12, 0x0

    move-object v1, v0

    move-object/from16 v4, p3

    move-object/from16 v6, p5

    .line 109
    invoke-direct/range {v1 .. v12}, Lcom/squareup/transactionhistory/pending/PendingTransactionSummary;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Lcom/squareup/transactionhistory/pending/StoreAndForwardState;Lcom/squareup/protos/common/Money;Ljava/lang/String;Ljava/util/List;ZZZLkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v0
.end method
