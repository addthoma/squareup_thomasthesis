.class final Lcom/squareup/transactionhistory/historical/TransactionSummariesList$addIfNotPresent$1;
.super Lkotlin/jvm/internal/Lambda;
.source "TransactionSummariesList.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->addIfNotPresent(Ljava/util/Collection;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $added:Lkotlin/jvm/internal/Ref$BooleanRef;

.field final synthetic $summaries:Ljava/util/Collection;

.field final synthetic this$0:Lcom/squareup/transactionhistory/historical/TransactionSummariesList;


# direct methods
.method constructor <init>(Lcom/squareup/transactionhistory/historical/TransactionSummariesList;Lkotlin/jvm/internal/Ref$BooleanRef;Ljava/util/Collection;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/transactionhistory/historical/TransactionSummariesList$addIfNotPresent$1;->this$0:Lcom/squareup/transactionhistory/historical/TransactionSummariesList;

    iput-object p2, p0, Lcom/squareup/transactionhistory/historical/TransactionSummariesList$addIfNotPresent$1;->$added:Lkotlin/jvm/internal/Ref$BooleanRef;

    iput-object p3, p0, Lcom/squareup/transactionhistory/historical/TransactionSummariesList$addIfNotPresent$1;->$summaries:Ljava/util/Collection;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 46
    invoke-virtual {p0}, Lcom/squareup/transactionhistory/historical/TransactionSummariesList$addIfNotPresent$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 4

    .line 381
    iget-object v0, p0, Lcom/squareup/transactionhistory/historical/TransactionSummariesList$addIfNotPresent$1;->$added:Lkotlin/jvm/internal/Ref$BooleanRef;

    iget-object v1, p0, Lcom/squareup/transactionhistory/historical/TransactionSummariesList$addIfNotPresent$1;->this$0:Lcom/squareup/transactionhistory/historical/TransactionSummariesList;

    iget-object v2, p0, Lcom/squareup/transactionhistory/historical/TransactionSummariesList$addIfNotPresent$1;->$summaries:Ljava/util/Collection;

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->access$addIfNotPresent(Lcom/squareup/transactionhistory/historical/TransactionSummariesList;Ljava/util/Collection;Z)Z

    move-result v1

    iput-boolean v1, v0, Lkotlin/jvm/internal/Ref$BooleanRef;->element:Z

    return-void
.end method
