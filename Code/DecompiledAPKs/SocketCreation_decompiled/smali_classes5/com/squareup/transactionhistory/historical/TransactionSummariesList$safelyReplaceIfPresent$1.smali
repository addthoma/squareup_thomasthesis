.class final Lcom/squareup/transactionhistory/historical/TransactionSummariesList$safelyReplaceIfPresent$1;
.super Lkotlin/jvm/internal/Lambda;
.source "TransactionSummariesList.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function0;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->safelyReplaceIfPresent(Ljava/util/List;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function0<",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nTransactionSummariesList.kt\nKotlin\n*S Kotlin\n*F\n+ 1 TransactionSummariesList.kt\ncom/squareup/transactionhistory/historical/TransactionSummariesList$safelyReplaceIfPresent$1\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,576:1\n1642#2,2:577\n*E\n*S KotlinDebug\n*F\n+ 1 TransactionSummariesList.kt\ncom/squareup/transactionhistory/historical/TransactionSummariesList$safelyReplaceIfPresent$1\n*L\n195#1,2:577\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0008\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n\u00a2\u0006\u0002\u0008\u0002"
    }
    d2 = {
        "<anonymous>",
        "",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $atLeastOneReplaced:Lkotlin/jvm/internal/Ref$BooleanRef;

.field final synthetic $summaries:Ljava/util/List;

.field final synthetic this$0:Lcom/squareup/transactionhistory/historical/TransactionSummariesList;


# direct methods
.method constructor <init>(Lcom/squareup/transactionhistory/historical/TransactionSummariesList;Ljava/util/List;Lkotlin/jvm/internal/Ref$BooleanRef;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/transactionhistory/historical/TransactionSummariesList$safelyReplaceIfPresent$1;->this$0:Lcom/squareup/transactionhistory/historical/TransactionSummariesList;

    iput-object p2, p0, Lcom/squareup/transactionhistory/historical/TransactionSummariesList$safelyReplaceIfPresent$1;->$summaries:Ljava/util/List;

    iput-object p3, p0, Lcom/squareup/transactionhistory/historical/TransactionSummariesList$safelyReplaceIfPresent$1;->$atLeastOneReplaced:Lkotlin/jvm/internal/Ref$BooleanRef;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke()Ljava/lang/Object;
    .locals 1

    .line 46
    invoke-virtual {p0}, Lcom/squareup/transactionhistory/historical/TransactionSummariesList$safelyReplaceIfPresent$1;->invoke()V

    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public final invoke()V
    .locals 4

    .line 195
    iget-object v0, p0, Lcom/squareup/transactionhistory/historical/TransactionSummariesList$safelyReplaceIfPresent$1;->$summaries:Ljava/util/List;

    check-cast v0, Ljava/lang/Iterable;

    .line 577
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;

    .line 196
    iget-object v2, p0, Lcom/squareup/transactionhistory/historical/TransactionSummariesList$safelyReplaceIfPresent$1;->this$0:Lcom/squareup/transactionhistory/historical/TransactionSummariesList;

    const/4 v3, 0x0

    invoke-static {v2, v1, v3}, Lcom/squareup/transactionhistory/historical/TransactionSummariesList;->access$safelyReplaceIfPresent(Lcom/squareup/transactionhistory/historical/TransactionSummariesList;Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 198
    iget-object v1, p0, Lcom/squareup/transactionhistory/historical/TransactionSummariesList$safelyReplaceIfPresent$1;->$atLeastOneReplaced:Lkotlin/jvm/internal/Ref$BooleanRef;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lkotlin/jvm/internal/Ref$BooleanRef;->element:Z

    goto :goto_0

    :cond_1
    return-void
.end method
