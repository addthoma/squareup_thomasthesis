.class public interface abstract Lcom/squareup/transactionhistory/historical/DualTransactionHistoryLoader;
.super Ljava/lang/Object;
.source "DualTransactionHistoryLoader.kt"

# interfaces
.implements Lcom/squareup/transactionhistory/TransactionsLoader;
.implements Lcom/squareup/transactionhistory/historical/HistoricalTransactionsUpdater;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/transactionhistory/TransactionsLoader<",
        "Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;",
        ">;",
        "Lcom/squareup/transactionhistory/historical/HistoricalTransactionsUpdater;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0005\u0008f\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u00020\u0003J\u0010\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H&J\u000e\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\tH&J\u0010\u0010\u000b\u001a\u00020\u000c2\u0006\u0010\r\u001a\u00020\nH&J\u0010\u0010\u000e\u001a\u00020\u00052\u0006\u0010\u000f\u001a\u00020\u0002H&J\u0010\u0010\u0010\u001a\u00020\u00052\u0006\u0010\u000f\u001a\u00020\u0002H&\u00a8\u0006\u0011"
    }
    d2 = {
        "Lcom/squareup/transactionhistory/historical/DualTransactionHistoryLoader;",
        "Lcom/squareup/transactionhistory/TransactionsLoader;",
        "Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;",
        "Lcom/squareup/transactionhistory/historical/HistoricalTransactionsUpdater;",
        "configureSearchResultsLoader",
        "",
        "configuration",
        "Lcom/squareup/transactionhistory/Configuration;",
        "currentLoader",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/transactionhistory/historical/SelectedLoader;",
        "setCurrentLoader",
        "",
        "loader",
        "update",
        "summary",
        "updateOrAdd",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract configureSearchResultsLoader(Lcom/squareup/transactionhistory/Configuration;)Z
.end method

.method public abstract currentLoader()Lio/reactivex/Observable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/transactionhistory/historical/SelectedLoader;",
            ">;"
        }
    .end annotation
.end method

.method public abstract setCurrentLoader(Lcom/squareup/transactionhistory/historical/SelectedLoader;)V
.end method

.method public abstract update(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;)Z
.end method

.method public abstract updateOrAdd(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;)Z
.end method
