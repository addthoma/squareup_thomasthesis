.class public final Lcom/squareup/transactionhistory/TransactionHistoriesKt;
.super Ljava/lang/Object;
.source "TransactionHistories.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nTransactionHistories.kt\nKotlin\n*S Kotlin\n*F\n+ 1 TransactionHistories.kt\ncom/squareup/transactionhistory/TransactionHistoriesKt\n*L\n1#1,54:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0010\u0002\n\u0002\u0008\u0003\u001a\u0016\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0006\u001a\u0016\u0010\u0008\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0006\u001a\u001a\u0010\t\u001a\u00020\n2\u0008\u0010\u000b\u001a\u0004\u0018\u00010\u00012\u0008\u0010\u000c\u001a\u0004\u0018\u00010\u0001\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"
    }
    d2 = {
        "EMPTY_DESCRIPTION",
        "",
        "EMPTY_ID",
        "allIdsEqual",
        "",
        "lhs",
        "Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;",
        "rhs",
        "atLeastOneIdEqualAndNonBlank",
        "requireAtLeastOneNonEmpty",
        "",
        "clientId",
        "serverId",
        "public_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final EMPTY_DESCRIPTION:Ljava/lang/String; = ""

.field public static final EMPTY_ID:Ljava/lang/String; = ""


# direct methods
.method public static final allIdsEqual(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;)Z
    .locals 2

    const-string v0, "lhs"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rhs"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    invoke-virtual {p0}, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;->getClientId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;->getClientId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;->getServerId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;->getServerId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;->getTenderId()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1}, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;->getTenderId()Ljava/lang/String;

    move-result-object p1

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static final atLeastOneIdEqualAndNonBlank(Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;)Z
    .locals 2

    const-string v0, "lhs"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rhs"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    invoke-virtual {p0}, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;->getClientId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;->getClientId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    .line 39
    invoke-virtual {p0}, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;->getClientId()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/2addr v0, v1

    if-nez v0, :cond_2

    :cond_0
    invoke-virtual {p0}, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;->getServerId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;->getServerId()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    .line 40
    invoke-virtual {p0}, Lcom/squareup/transactionhistory/historical/HistoricalTransactionSummary;->getServerId()Ljava/lang/String;

    move-result-object p0

    check-cast p0, Ljava/lang/CharSequence;

    invoke-static {p0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p0

    xor-int/2addr p0, v1

    if-eqz p0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :cond_2
    :goto_0
    return v1
.end method

.method public static final requireAtLeastOneNonEmpty(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    const-string v0, ""

    const/4 v1, 0x1

    if-eqz p0, :cond_0

    .line 25
    move-object v2, p0

    check-cast v2, Ljava/lang/CharSequence;

    .line 24
    invoke-static {v2}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v2

    xor-int/2addr v2, v1

    if-ne v2, v1, :cond_0

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    xor-int/2addr p0, v1

    if-nez p0, :cond_2

    :cond_0
    if-eqz p1, :cond_1

    move-object p0, p1

    check-cast p0, Ljava/lang/CharSequence;

    .line 25
    invoke-static {p0}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p0

    xor-int/2addr p0, v1

    if-ne p0, v1, :cond_1

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p0

    xor-int/2addr p0, v1

    if-eqz p0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :cond_2
    :goto_0
    if-eqz v1, :cond_3

    return-void

    .line 23
    :cond_3
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string p1, "At least one of clientId or serverId must be non-empty."

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p0, Ljava/lang/Throwable;

    throw p0
.end method
