.class public final Lcom/squareup/tenderworkflow/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tenderworkflow/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final buyer_cart_view:I = 0x7f0a0288

.field public static final buyer_language_button:I = 0x7f0a028b

.field public static final buyer_up_button:I = 0x7f0a0294

.field public static final card:I = 0x7f0a02b1

.field public static final card_slot:I = 0x7f0a02c9

.field public static final cart_recycler:I = 0x7f0a02e4

.field public static final cash_option_custom:I = 0x7f0a02ee

.field public static final change_glyph:I = 0x7f0a0303

.field public static final confirm_and_pay_button:I = 0x7f0a038a

.field public static final footer:I = 0x7f0a076b

.field public static final header:I = 0x7f0a07c8

.field public static final nfc_prompt:I = 0x7f0a0a24

.field public static final no_cards_on_file:I = 0x7f0a0a27

.field public static final payment_prompt_view:I = 0x7f0a0bf1

.field public static final payment_type_action_bar:I = 0x7f0a0bf4

.field public static final payment_type_icon:I = 0x7f0a0bf8

.field public static final payment_type_more_view:I = 0x7f0a0bfa

.field public static final payment_type_options_scroll_view:I = 0x7f0a0bfb

.field public static final payment_type_status_header:I = 0x7f0a0bfc

.field public static final payment_type_text:I = 0x7f0a0bfe

.field public static final payment_type_view:I = 0x7f0a0c00

.field public static final scrollView:I = 0x7f0a0e12

.field public static final select_method_transaction_total:I = 0x7f0a0e43

.field public static final select_payment_container:I = 0x7f0a0e4b

.field public static final select_payment_up_button:I = 0x7f0a0e4c

.field public static final seller_cash_received_continue:I = 0x7f0a0e5f

.field public static final seller_cash_received_subtitle:I = 0x7f0a0e60

.field public static final seller_cash_received_title:I = 0x7f0a0e61

.field public static final split_tender_amount:I = 0x7f0a0ecf

.field public static final split_tender_amount_recycler_view:I = 0x7f0a0ed0

.field public static final split_tender_amount_subtitle:I = 0x7f0a0ed1

.field public static final split_tender_button:I = 0x7f0a0ed2

.field public static final split_tender_completed_payments_cancel_button:I = 0x7f0a0ed5

.field public static final split_tender_completed_payments_line_row:I = 0x7f0a0ed6

.field public static final split_tender_completed_payments_title:I = 0x7f0a0ed7

.field public static final split_tender_confirm_button:I = 0x7f0a0ed8

.field public static final split_tender_custom_split:I = 0x7f0a0edb

.field public static final split_tender_even_split_custom:I = 0x7f0a0edc

.field public static final split_tender_even_split_four:I = 0x7f0a0edd

.field public static final split_tender_even_split_three:I = 0x7f0a0ede

.field public static final split_tender_even_split_two:I = 0x7f0a0edf

.field public static final split_tender_transaction_total:I = 0x7f0a0ee3

.field public static final swipe_insert_tap_prompt:I = 0x7f0a0f52

.field public static final tender_completed_row:I = 0x7f0a0f83

.field public static final transaction_total:I = 0x7f0a1070

.field public static final transaction_type_text:I = 0x7f0a1071


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
