.class public final Lcom/squareup/tenderworkflow/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/tenderworkflow/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final accept_cash:I = 0x7f120021

.field public static final back:I = 0x7f12011b

.field public static final buyer_cart_confirm_and_pay:I = 0x7f1201f4

.field public static final cancel_split_tender_transaction_message:I = 0x7f120298

.field public static final cancel_split_tender_transaction_title:I = 0x7f120299

.field public static final cancel_transaction:I = 0x7f12029b

.field public static final card_on_file_charge_confirmation_body:I = 0x7f120319

.field public static final charge:I = 0x7f1203f6

.field public static final check:I = 0x7f1203fe

.field public static final close:I = 0x7f12042d

.field public static final confirm_collect_cash_message:I = 0x7f12047e

.field public static final confirm_collect_cash_title:I = 0x7f12047f

.field public static final connect_reader_offline_instruction:I = 0x7f120489

.field public static final contactless_payment_methods_not_ready:I = 0x7f1204a8

.field public static final contactless_payment_methods_ready:I = 0x7f1204a9

.field public static final contactless_payment_methods_ready_show_contactless_row:I = 0x7f1204aa

.field public static final current_customer_cards_on_file:I = 0x7f120787

.field public static final current_customer_cards_on_file_offline_mode:I = 0x7f120788

.field public static final custom:I = 0x7f120797

.field public static final customer_card_on_file:I = 0x7f12079a

.field public static final customer_card_on_file_offline_mode:I = 0x7f12079b

.field public static final customer_no_cards_on_file_v2:I = 0x7f1207a6

.field public static final customer_number_of_cards_on_file_many:I = 0x7f1207a8

.field public static final customer_number_of_cards_on_file_one:I = 0x7f1207a9

.field public static final gift_cards_on_file:I = 0x7f120b2b

.field public static final invoice:I = 0x7f120c72

.field public static final invoice_offline_mode:I = 0x7f120d3e

.field public static final invoice_unavailable_for_split_tender:I = 0x7f120db6

.field public static final keep:I = 0x7f120e78

.field public static final manual_credit_card_entry:I = 0x7f120f8e

.field public static final manual_credit_card_entry_offline_mode:I = 0x7f120f8f

.field public static final manual_gift_card_entry:I = 0x7f120f93

.field public static final manual_gift_card_entry_offline_mode:I = 0x7f120f94

.field public static final more:I = 0x7f121024

.field public static final new_card:I = 0x7f121064

.field public static final number_0:I = 0x7f1210c3

.field public static final number_2:I = 0x7f1210c4

.field public static final number_3:I = 0x7f1210c5

.field public static final number_4:I = 0x7f1210c6

.field public static final ok:I = 0x7f1210e1

.field public static final other_gift_card:I = 0x7f1212f5

.field public static final other_tender:I = 0x7f1212f6

.field public static final pay_card_title:I = 0x7f1213a3

.field public static final pay_cash_custom_button:I = 0x7f1213a4

.field public static final payment_methods_prompt_nfc_enabled:I = 0x7f1213c3

.field public static final payment_methods_prompt_nfc_not_enabled:I = 0x7f1213c4

.field public static final payment_methods_prompt_offline_mode:I = 0x7f1213c5

.field public static final payment_methods_prompt_offline_mode_help_text:I = 0x7f1213c6

.field public static final payment_methods_prompt_swipe_or_dip:I = 0x7f1213c7

.field public static final payment_prompt_all_options:I = 0x7f1213cd

.field public static final payment_prompt_all_options_single_message:I = 0x7f1213ce

.field public static final payment_prompt_charge_text:I = 0x7f1213cf

.field public static final payment_prompt_choose_option:I = 0x7f1213d0

.field public static final payment_prompt_choose_option_x2:I = 0x7f1213d1

.field public static final payment_prompt_insert:I = 0x7f1213d2

.field public static final payment_prompt_insert_and_tap:I = 0x7f1213d3

.field public static final payment_prompt_insert_single_message:I = 0x7f1213d4

.field public static final payment_prompt_no_payment_options:I = 0x7f1213d5

.field public static final payment_prompt_swipe:I = 0x7f1213d6

.field public static final payment_prompt_swipe_and_insert:I = 0x7f1213d7

.field public static final payment_prompt_transaction_type_purchase:I = 0x7f1213d8

.field public static final payment_type_above_maximum_card:I = 0x7f1213fc

.field public static final payment_type_above_maximum_card_offline:I = 0x7f1213fd

.field public static final payment_type_below_minimum_card:I = 0x7f121400

.field public static final payment_type_contactless_row_phone:I = 0x7f121403

.field public static final record_card_payment:I = 0x7f1215df

.field public static final record_payment_amount:I = 0x7f1215e5

.field public static final secondary_payment_title_amount:I = 0x7f121796

.field public static final split_tender:I = 0x7f121845

.field public static final split_tender_again:I = 0x7f121846

.field public static final split_tender_amount_done:I = 0x7f121847

.field public static final split_tender_amount_label:I = 0x7f121848

.field public static final split_tender_amount_remaining_help_text:I = 0x7f12184a

.field public static final split_tender_completed_payments:I = 0x7f121856

.field public static final split_tender_completed_payments_cancel:I = 0x7f121857

.field public static final split_tender_edit_split:I = 0x7f121858

.field public static final split_tender_equal_label:I = 0x7f121859

.field public static final split_tender_equal_split_label:I = 0x7f12185a

.field public static final split_tender_equal_split_title:I = 0x7f12185b

.field public static final split_tender_payment_amount_label:I = 0x7f12185e

.field public static final split_tender_total_amount:I = 0x7f121860

.field public static final split_tender_total_subtitle:I = 0x7f121861

.field public static final split_tender_unavailable_message:I = 0x7f121862

.field public static final split_tender_unavailable_title:I = 0x7f121863

.field public static final tender:I = 0x7f12194f


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
