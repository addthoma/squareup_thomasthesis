.class public final enum Lcom/squareup/squid/SquareDeviceApps;
.super Ljava/lang/Enum;
.source "SquareDeviceApps.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/squid/SquareDeviceApps$SquareDeviceAppsHelper;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/squid/SquareDeviceApps;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000b\u0008\u0086\u0001\u0018\u00002\u0008\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u0012B\u000f\u0008\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008R\u0010\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000j\u0002\u0008\tj\u0002\u0008\nj\u0002\u0008\u000bj\u0002\u0008\u000cj\u0002\u0008\rj\u0002\u0008\u000ej\u0002\u0008\u000fj\u0002\u0008\u0010j\u0002\u0008\u0011\u00a8\u0006\u0013"
    }
    d2 = {
        "Lcom/squareup/squid/SquareDeviceApps;",
        "",
        "packageName",
        "",
        "(Ljava/lang/String;ILjava/lang/String;)V",
        "isInstalled",
        "",
        "context",
        "Landroid/content/Context;",
        "OOB",
        "APPOINTMENTS",
        "RESTAURANTS",
        "RETAIL",
        "SETTINGS",
        "SIGNIN",
        "SPOS",
        "STATUSBAR",
        "TERMINAL_API",
        "SquareDeviceAppsHelper",
        "square-device_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/squid/SquareDeviceApps;

.field public static final enum APPOINTMENTS:Lcom/squareup/squid/SquareDeviceApps;

.field public static final enum OOB:Lcom/squareup/squid/SquareDeviceApps;

.field public static final enum RESTAURANTS:Lcom/squareup/squid/SquareDeviceApps;

.field public static final enum RETAIL:Lcom/squareup/squid/SquareDeviceApps;

.field public static final enum SETTINGS:Lcom/squareup/squid/SquareDeviceApps;

.field public static final enum SIGNIN:Lcom/squareup/squid/SquareDeviceApps;

.field public static final enum SPOS:Lcom/squareup/squid/SquareDeviceApps;

.field public static final enum STATUSBAR:Lcom/squareup/squid/SquareDeviceApps;

.field public static final enum TERMINAL_API:Lcom/squareup/squid/SquareDeviceApps;


# instance fields
.field public final packageName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/16 v0, 0x9

    new-array v0, v0, [Lcom/squareup/squid/SquareDeviceApps;

    new-instance v1, Lcom/squareup/squid/SquareDeviceApps;

    const/4 v2, 0x0

    const-string v3, "OOB"

    const-string v4, "com.squareup.oob"

    .line 8
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/squid/SquareDeviceApps;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/squid/SquareDeviceApps;->OOB:Lcom/squareup/squid/SquareDeviceApps;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/squid/SquareDeviceApps;

    const/4 v2, 0x1

    const-string v3, "APPOINTMENTS"

    const-string/jumbo v4, "this.is.not.an.app.id"

    .line 9
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/squid/SquareDeviceApps;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/squid/SquareDeviceApps;->APPOINTMENTS:Lcom/squareup/squid/SquareDeviceApps;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/squid/SquareDeviceApps;

    const/4 v2, 0x2

    const-string v3, "RESTAURANTS"

    const-string v4, "com.squareup.restaurants"

    .line 10
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/squid/SquareDeviceApps;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/squid/SquareDeviceApps;->RESTAURANTS:Lcom/squareup/squid/SquareDeviceApps;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/squid/SquareDeviceApps;

    const/4 v2, 0x3

    const-string v3, "RETAIL"

    const-string v4, "com.squareup.retail"

    .line 11
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/squid/SquareDeviceApps;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/squid/SquareDeviceApps;->RETAIL:Lcom/squareup/squid/SquareDeviceApps;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/squid/SquareDeviceApps;

    const/4 v2, 0x4

    const-string v3, "SETTINGS"

    const-string v4, "com.squareup.x2settings"

    .line 12
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/squid/SquareDeviceApps;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/squid/SquareDeviceApps;->SETTINGS:Lcom/squareup/squid/SquareDeviceApps;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/squid/SquareDeviceApps;

    const/4 v2, 0x5

    const-string v3, "SIGNIN"

    const-string v4, "com.squareup.signin"

    .line 13
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/squid/SquareDeviceApps;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/squid/SquareDeviceApps;->SIGNIN:Lcom/squareup/squid/SquareDeviceApps;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/squid/SquareDeviceApps;

    const/4 v2, 0x6

    const-string v3, "SPOS"

    const-string v4, "com.squareup"

    .line 14
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/squid/SquareDeviceApps;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/squid/SquareDeviceApps;->SPOS:Lcom/squareup/squid/SquareDeviceApps;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/squid/SquareDeviceApps;

    const/4 v2, 0x7

    const-string v3, "STATUSBAR"

    const-string v4, "com.squareup.switcher"

    .line 15
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/squid/SquareDeviceApps;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/squid/SquareDeviceApps;->STATUSBAR:Lcom/squareup/squid/SquareDeviceApps;

    aput-object v1, v0, v2

    new-instance v1, Lcom/squareup/squid/SquareDeviceApps;

    const/16 v2, 0x8

    const-string v3, "TERMINAL_API"

    const-string v4, "com.squareup.terminalapi"

    .line 16
    invoke-direct {v1, v3, v2, v4}, Lcom/squareup/squid/SquareDeviceApps;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/squareup/squid/SquareDeviceApps;->TERMINAL_API:Lcom/squareup/squid/SquareDeviceApps;

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/squid/SquareDeviceApps;->$VALUES:[Lcom/squareup/squid/SquareDeviceApps;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 7
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/squareup/squid/SquareDeviceApps;->packageName:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/squid/SquareDeviceApps;
    .locals 1

    const-class v0, Lcom/squareup/squid/SquareDeviceApps;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/squid/SquareDeviceApps;

    return-object p0
.end method

.method public static values()[Lcom/squareup/squid/SquareDeviceApps;
    .locals 1

    sget-object v0, Lcom/squareup/squid/SquareDeviceApps;->$VALUES:[Lcom/squareup/squid/SquareDeviceApps;

    invoke-virtual {v0}, [Lcom/squareup/squid/SquareDeviceApps;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/squid/SquareDeviceApps;

    return-object v0
.end method


# virtual methods
.method public final isInstalled(Landroid/content/Context;)Z
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    iget-object v0, p0, Lcom/squareup/squid/SquareDeviceApps;->packageName:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/squareup/util/PackageManagerUtils;->isPackageInstalled(Landroid/content/Context;Ljava/lang/String;)Z

    move-result p1

    return p1
.end method
