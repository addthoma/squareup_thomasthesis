.class public final Lcom/squareup/server/account/protos/FeeTypesProto;
.super Lcom/squareup/wire/AndroidMessage;
.source "FeeTypesProto.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/FeeTypesProto$ProtoAdapter_FeeTypesProto;,
        Lcom/squareup/server/account/protos/FeeTypesProto$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/FeeTypesProto;",
        "Lcom/squareup/server/account/protos/FeeTypesProto$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/FeeTypesProto;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/FeeTypesProto;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/FeeTypesProto;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/FeeTypesProto;",
            ">;"
        }
    .end annotation
.end field

.field private static final serialVersionUID:J


# instance fields
.field public final types:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.FeeType#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x1
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/server/account/protos/FeeType;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 31
    new-instance v0, Lcom/squareup/server/account/protos/FeeTypesProto$ProtoAdapter_FeeTypesProto;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FeeTypesProto$ProtoAdapter_FeeTypesProto;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/FeeTypesProto;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 33
    sget-object v0, Lcom/squareup/server/account/protos/FeeTypesProto;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/FeeTypesProto;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/server/account/protos/FeeType;",
            ">;)V"
        }
    .end annotation

    .line 45
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, v0}, Lcom/squareup/server/account/protos/FeeTypesProto;-><init>(Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/server/account/protos/FeeType;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 49
    sget-object v0, Lcom/squareup/server/account/protos/FeeTypesProto;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    const-string/jumbo p2, "types"

    .line 50
    invoke-static {p2, p1}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/server/account/protos/FeeTypesProto;->types:Ljava/util/List;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/FeeTypesProto$Builder;)Lcom/squareup/server/account/protos/FeeTypesProto$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 106
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FeeTypesProto;->newBuilder()Lcom/squareup/server/account/protos/FeeTypesProto$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 64
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/FeeTypesProto;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 65
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/FeeTypesProto;

    .line 66
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FeeTypesProto;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/FeeTypesProto;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FeeTypesProto;->types:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/server/account/protos/FeeTypesProto;->types:Ljava/util/List;

    .line 67
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 72
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_0

    .line 74
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FeeTypesProto;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 75
    iget-object v1, p0, Lcom/squareup/server/account/protos/FeeTypesProto;->types:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 76
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_0
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/FeeTypesProto$Builder;
    .locals 2

    .line 55
    new-instance v0, Lcom/squareup/server/account/protos/FeeTypesProto$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FeeTypesProto$Builder;-><init>()V

    .line 56
    iget-object v1, p0, Lcom/squareup/server/account/protos/FeeTypesProto;->types:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/server/account/protos/FeeTypesProto$Builder;->types:Ljava/util/List;

    .line 57
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FeeTypesProto;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FeeTypesProto$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 30
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FeeTypesProto;->newBuilder()Lcom/squareup/server/account/protos/FeeTypesProto$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/FeeTypesProto;)Lcom/squareup/server/account/protos/FeeTypesProto;
    .locals 2

    .line 101
    iget-object v0, p1, Lcom/squareup/server/account/protos/FeeTypesProto;->types:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FeeTypesProto;->requireBuilder(Lcom/squareup/server/account/protos/FeeTypesProto$Builder;)Lcom/squareup/server/account/protos/FeeTypesProto$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/FeeTypesProto;->types:Ljava/util/List;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/FeeTypesProto$Builder;->types(Ljava/util/List;)Lcom/squareup/server/account/protos/FeeTypesProto$Builder;

    move-result-object v1

    :cond_0
    if-nez v1, :cond_1

    move-object p1, p0

    goto :goto_0

    .line 102
    :cond_1
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FeeTypesProto$Builder;->build()Lcom/squareup/server/account/protos/FeeTypesProto;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 30
    check-cast p1, Lcom/squareup/server/account/protos/FeeTypesProto;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/FeeTypesProto;->overlay(Lcom/squareup/server/account/protos/FeeTypesProto;)Lcom/squareup/server/account/protos/FeeTypesProto;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/FeeTypesProto;
    .locals 3

    .line 91
    iget-object v0, p0, Lcom/squareup/server/account/protos/FeeTypesProto;->types:Ljava/util/List;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 92
    invoke-static {v0}, Lcom/squareup/wired/Wired;->populateDefaults(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 93
    iget-object v2, p0, Lcom/squareup/server/account/protos/FeeTypesProto;->types:Ljava/util/List;

    if-eq v0, v2, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FeeTypesProto;->requireBuilder(Lcom/squareup/server/account/protos/FeeTypesProto$Builder;)Lcom/squareup/server/account/protos/FeeTypesProto$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/FeeTypesProto$Builder;->types(Ljava/util/List;)Lcom/squareup/server/account/protos/FeeTypesProto$Builder;

    move-result-object v1

    :cond_0
    if-nez v1, :cond_1

    move-object v0, p0

    goto :goto_0

    .line 95
    :cond_1
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FeeTypesProto$Builder;->build()Lcom/squareup/server/account/protos/FeeTypesProto;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 30
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FeeTypesProto;->populateDefaults()Lcom/squareup/server/account/protos/FeeTypesProto;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 83
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 84
    iget-object v1, p0, Lcom/squareup/server/account/protos/FeeTypesProto;->types:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, ", types="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FeeTypesProto;->types:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "FeeTypesProto{"

    .line 85
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
