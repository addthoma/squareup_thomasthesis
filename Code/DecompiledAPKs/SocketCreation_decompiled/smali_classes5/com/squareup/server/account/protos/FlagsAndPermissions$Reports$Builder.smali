.class public final Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FlagsAndPermissions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public filter_by_employee:Ljava/lang/Boolean;

.field public hide_tips:Ljava/lang/Boolean;

.field public see_measurement_unit:Ljava/lang/Boolean;

.field public use_alternate_sales_summary:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 7634
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;
    .locals 7

    .line 7671
    new-instance v6, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports$Builder;->use_alternate_sales_summary:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports$Builder;->filter_by_employee:Ljava/lang/Boolean;

    iget-object v3, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports$Builder;->hide_tips:Ljava/lang/Boolean;

    iget-object v4, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports$Builder;->see_measurement_unit:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 7625
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;

    move-result-object v0

    return-object v0
.end method

.method public filter_by_employee(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports$Builder;
    .locals 0

    .line 7649
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports$Builder;->filter_by_employee:Ljava/lang/Boolean;

    return-object p0
.end method

.method public hide_tips(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports$Builder;
    .locals 0

    .line 7657
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports$Builder;->hide_tips:Ljava/lang/Boolean;

    return-object p0
.end method

.method public see_measurement_unit(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports$Builder;
    .locals 0

    .line 7665
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports$Builder;->see_measurement_unit:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_alternate_sales_summary(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports$Builder;
    .locals 0

    .line 7641
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports$Builder;->use_alternate_sales_summary:Ljava/lang/Boolean;

    return-object p0
.end method
