.class public final Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FlagsAndPermissions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public allow_add_money:Ljava/lang/Boolean;

.field public bank_link_in_app:Ljava/lang/Boolean;

.field public bank_link_post_signup:Ljava/lang/Boolean;

.field public bank_linking_with_two_accounts:Ljava/lang/Boolean;

.field public bank_resend_email:Ljava/lang/Boolean;

.field public can_pause_nightly_deposit:Ljava/lang/Boolean;

.field public instant_deposit_requires_linked_card:Ljava/lang/Boolean;

.field public show_weekend_balance_toggle:Ljava/lang/Boolean;

.field public use_deposit_settings:Ljava/lang/Boolean;

.field public use_deposit_settings_card_linking:Ljava/lang/Boolean;

.field public use_deposit_settings_deposit_schedule:Ljava/lang/Boolean;

.field public use_deposits_report:Ljava/lang/Boolean;

.field public use_deposits_report_card_payments:Ljava/lang/Boolean;

.field public use_deposits_report_detail:Ljava/lang/Boolean;

.field public use_deposits_report_in_deposits_applet:Ljava/lang/Boolean;

.field public use_instant_deposit:Ljava/lang/Boolean;

.field public use_same_day_deposit:Ljava/lang/Boolean;

.field public view_bank_account:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 9573
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public allow_add_money(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;
    .locals 0

    .line 9719
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->allow_add_money:Ljava/lang/Boolean;

    return-object p0
.end method

.method public bank_link_in_app(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;
    .locals 0

    .line 9671
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->bank_link_in_app:Ljava/lang/Boolean;

    return-object p0
.end method

.method public bank_link_post_signup(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;
    .locals 0

    .line 9663
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->bank_link_post_signup:Ljava/lang/Boolean;

    return-object p0
.end method

.method public bank_linking_with_two_accounts(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;
    .locals 0

    .line 9687
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->bank_linking_with_two_accounts:Ljava/lang/Boolean;

    return-object p0
.end method

.method public bank_resend_email(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;
    .locals 0

    .line 9679
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->bank_resend_email:Ljava/lang/Boolean;

    return-object p0
.end method

.method public build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;
    .locals 2

    .line 9725
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;-><init>(Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 9536
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits;

    move-result-object v0

    return-object v0
.end method

.method public can_pause_nightly_deposit(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;
    .locals 0

    .line 9711
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->can_pause_nightly_deposit:Ljava/lang/Boolean;

    return-object p0
.end method

.method public instant_deposit_requires_linked_card(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;
    .locals 0

    .line 9621
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->instant_deposit_requires_linked_card:Ljava/lang/Boolean;

    return-object p0
.end method

.method public show_weekend_balance_toggle(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;
    .locals 0

    .line 9703
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->show_weekend_balance_toggle:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_deposit_settings(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;
    .locals 0

    .line 9695
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->use_deposit_settings:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_deposit_settings_card_linking(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;
    .locals 0

    .line 9629
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->use_deposit_settings_card_linking:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_deposit_settings_deposit_schedule(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;
    .locals 0

    .line 9638
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->use_deposit_settings_deposit_schedule:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_deposits_report(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;
    .locals 0

    .line 9580
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->use_deposits_report:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_deposits_report_card_payments(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;
    .locals 0

    .line 9596
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->use_deposits_report_card_payments:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_deposits_report_detail(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;
    .locals 0

    .line 9588
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->use_deposits_report_detail:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_deposits_report_in_deposits_applet(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;
    .locals 0

    .line 9655
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->use_deposits_report_in_deposits_applet:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_instant_deposit(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;
    .locals 0

    .line 9612
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->use_instant_deposit:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_same_day_deposit(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;
    .locals 0

    .line 9646
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->use_same_day_deposit:Ljava/lang/Boolean;

    return-object p0
.end method

.method public view_bank_account(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;
    .locals 0

    .line 9604
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Deposits$Builder;->view_bank_account:Ljava/lang/Boolean;

    return-object p0
.end method
