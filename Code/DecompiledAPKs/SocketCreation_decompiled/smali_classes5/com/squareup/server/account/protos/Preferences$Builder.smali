.class public final Lcom/squareup/server/account/protos/Preferences$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Preferences.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/Preferences;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/Preferences;",
        "Lcom/squareup/server/account/protos/Preferences$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public allow_instant_deposit:Ljava/lang/Boolean;

.field public amended_reopens_enabled:Ljava/lang/Boolean;

.field public behavior_after_adding_item:Ljava/lang/String;

.field public cash_management_default_starting_amount:Lcom/squareup/protos/common/Money;

.field public cash_management_email_report:Ljava/lang/Boolean;

.field public cash_management_enabled:Ljava/lang/Boolean;

.field public cash_management_print_report:Ljava/lang/Boolean;

.field public cash_management_report_recipient_email:Ljava/lang/String;

.field public custom_tender_options_proto:Ljava/lang/String;

.field public customer_management_collect_after_checkout:Ljava/lang/Boolean;

.field public customer_management_collect_before_checkout:Ljava/lang/Boolean;

.field public customer_management_show_email_collection_screen:Ljava/lang/Boolean;

.field public customer_management_show_save_card_button_after_checkout:Ljava/lang/Boolean;

.field public customer_management_use_card_on_file:Ljava/lang/Boolean;

.field public employee_management_enabled_for_account:Ljava/lang/Boolean;

.field public employee_management_enabled_for_device:Ljava/lang/Boolean;

.field public employee_management_inactivity_timeout:Ljava/lang/String;

.field public employee_management_requires_employee_logout_after_completed_transaction:Ljava/lang/Boolean;

.field public employee_management_tracking_level:Ljava/lang/String;

.field public error_message:Ljava/lang/String;

.field public error_title:Ljava/lang/String;

.field public for_paper_signature_always_print_customer_copy:Ljava/lang/Boolean;

.field public for_paper_signature_print_additional_auth_slip:Ljava/lang/Boolean;

.field public for_paper_signature_use_quick_tip_receipt:Ljava/lang/Boolean;

.field public message:Ljava/lang/String;

.field public open_tickets_enabled:Ljava/lang/Boolean;

.field public skip_receipt_screen:Ljava/lang/Boolean;

.field public skip_signature:Ljava/lang/Boolean;

.field public skip_signature_always:Ljava/lang/Boolean;

.field public store_and_forward_enabled:Ljava/lang/Boolean;

.field public store_and_forward_single_transaction_limit:Lcom/squareup/protos/common/Money;

.field public success:Ljava/lang/Boolean;

.field public terminal_connected_mode_enabled:Ljava/lang/Boolean;

.field public tipping_calculation_phase:Ljava/lang/String;

.field public tipping_custom_percentages:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation
.end field

.field public tipping_enabled:Ljava/lang/Boolean;

.field public tipping_use_custom_percentages:Ljava/lang/Boolean;

.field public tipping_use_manual_tip_entry:Ljava/lang/Boolean;

.field public title:Ljava/lang/String;

.field public use_allow_swipe_for_chip_cards:Ljava/lang/Boolean;

.field public use_curated_image_for_receipt:Ljava/lang/Boolean;

.field public use_open_tickets_menu_as_home_screen:Ljava/lang/Boolean;

.field public use_order_hub:Ljava/lang/Boolean;

.field public use_paper_signature:Ljava/lang/Boolean;

.field public use_predefined_tickets:Ljava/lang/Boolean;

.field public use_separate_tipping_screen:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 887
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 888
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/server/account/protos/Preferences$Builder;->tipping_custom_percentages:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public allow_instant_deposit(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;
    .locals 0

    .line 1111
    iput-object p1, p0, Lcom/squareup/server/account/protos/Preferences$Builder;->allow_instant_deposit:Ljava/lang/Boolean;

    return-object p0
.end method

.method public amended_reopens_enabled(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;
    .locals 0

    .line 1121
    iput-object p1, p0, Lcom/squareup/server/account/protos/Preferences$Builder;->amended_reopens_enabled:Ljava/lang/Boolean;

    return-object p0
.end method

.method public behavior_after_adding_item(Ljava/lang/String;)Lcom/squareup/server/account/protos/Preferences$Builder;
    .locals 0

    .line 1126
    iput-object p1, p0, Lcom/squareup/server/account/protos/Preferences$Builder;->behavior_after_adding_item:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/server/account/protos/Preferences;
    .locals 2

    .line 1148
    new-instance v0, Lcom/squareup/server/account/protos/Preferences;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/squareup/server/account/protos/Preferences;-><init>(Lcom/squareup/server/account/protos/Preferences$Builder;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 794
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/Preferences$Builder;->build()Lcom/squareup/server/account/protos/Preferences;

    move-result-object v0

    return-object v0
.end method

.method public cash_management_default_starting_amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/server/account/protos/Preferences$Builder;
    .locals 0

    .line 1070
    iput-object p1, p0, Lcom/squareup/server/account/protos/Preferences$Builder;->cash_management_default_starting_amount:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public cash_management_email_report(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;
    .locals 0

    .line 1059
    iput-object p1, p0, Lcom/squareup/server/account/protos/Preferences$Builder;->cash_management_email_report:Ljava/lang/Boolean;

    return-object p0
.end method

.method public cash_management_enabled(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;
    .locals 0

    .line 1054
    iput-object p1, p0, Lcom/squareup/server/account/protos/Preferences$Builder;->cash_management_enabled:Ljava/lang/Boolean;

    return-object p0
.end method

.method public cash_management_print_report(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;
    .locals 0

    .line 1064
    iput-object p1, p0, Lcom/squareup/server/account/protos/Preferences$Builder;->cash_management_print_report:Ljava/lang/Boolean;

    return-object p0
.end method

.method public cash_management_report_recipient_email(Ljava/lang/String;)Lcom/squareup/server/account/protos/Preferences$Builder;
    .locals 0

    .line 1076
    iput-object p1, p0, Lcom/squareup/server/account/protos/Preferences$Builder;->cash_management_report_recipient_email:Ljava/lang/String;

    return-object p0
.end method

.method public custom_tender_options_proto(Ljava/lang/String;)Lcom/squareup/server/account/protos/Preferences$Builder;
    .locals 0

    .line 1033
    iput-object p1, p0, Lcom/squareup/server/account/protos/Preferences$Builder;->custom_tender_options_proto:Ljava/lang/String;

    return-object p0
.end method

.method public customer_management_collect_after_checkout(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;
    .locals 0

    .line 1088
    iput-object p1, p0, Lcom/squareup/server/account/protos/Preferences$Builder;->customer_management_collect_after_checkout:Ljava/lang/Boolean;

    return-object p0
.end method

.method public customer_management_collect_before_checkout(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;
    .locals 0

    .line 1082
    iput-object p1, p0, Lcom/squareup/server/account/protos/Preferences$Builder;->customer_management_collect_before_checkout:Ljava/lang/Boolean;

    return-object p0
.end method

.method public customer_management_show_email_collection_screen(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;
    .locals 0

    .line 1106
    iput-object p1, p0, Lcom/squareup/server/account/protos/Preferences$Builder;->customer_management_show_email_collection_screen:Ljava/lang/Boolean;

    return-object p0
.end method

.method public customer_management_show_save_card_button_after_checkout(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;
    .locals 0

    .line 1100
    iput-object p1, p0, Lcom/squareup/server/account/protos/Preferences$Builder;->customer_management_show_save_card_button_after_checkout:Ljava/lang/Boolean;

    return-object p0
.end method

.method public customer_management_use_card_on_file(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;
    .locals 0

    .line 1094
    iput-object p1, p0, Lcom/squareup/server/account/protos/Preferences$Builder;->customer_management_use_card_on_file:Ljava/lang/Boolean;

    return-object p0
.end method

.method public employee_management_enabled_for_account(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;
    .locals 0

    .line 1001
    iput-object p1, p0, Lcom/squareup/server/account/protos/Preferences$Builder;->employee_management_enabled_for_account:Ljava/lang/Boolean;

    return-object p0
.end method

.method public employee_management_enabled_for_device(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;
    .locals 0

    .line 1007
    iput-object p1, p0, Lcom/squareup/server/account/protos/Preferences$Builder;->employee_management_enabled_for_device:Ljava/lang/Boolean;

    return-object p0
.end method

.method public employee_management_inactivity_timeout(Ljava/lang/String;)Lcom/squareup/server/account/protos/Preferences$Builder;
    .locals 0

    .line 1013
    iput-object p1, p0, Lcom/squareup/server/account/protos/Preferences$Builder;->employee_management_inactivity_timeout:Ljava/lang/String;

    return-object p0
.end method

.method public employee_management_requires_employee_logout_after_completed_transaction(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;
    .locals 0

    .line 1137
    iput-object p1, p0, Lcom/squareup/server/account/protos/Preferences$Builder;->employee_management_requires_employee_logout_after_completed_transaction:Ljava/lang/Boolean;

    return-object p0
.end method

.method public employee_management_tracking_level(Ljava/lang/String;)Lcom/squareup/server/account/protos/Preferences$Builder;
    .locals 0

    .line 1018
    iput-object p1, p0, Lcom/squareup/server/account/protos/Preferences$Builder;->employee_management_tracking_level:Ljava/lang/String;

    return-object p0
.end method

.method public error_message(Ljava/lang/String;)Lcom/squareup/server/account/protos/Preferences$Builder;
    .locals 0

    .line 920
    iput-object p1, p0, Lcom/squareup/server/account/protos/Preferences$Builder;->error_message:Ljava/lang/String;

    return-object p0
.end method

.method public error_title(Ljava/lang/String;)Lcom/squareup/server/account/protos/Preferences$Builder;
    .locals 0

    .line 910
    iput-object p1, p0, Lcom/squareup/server/account/protos/Preferences$Builder;->error_title:Ljava/lang/String;

    return-object p0
.end method

.method public for_paper_signature_always_print_customer_copy(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;
    .locals 0

    .line 983
    iput-object p1, p0, Lcom/squareup/server/account/protos/Preferences$Builder;->for_paper_signature_always_print_customer_copy:Ljava/lang/Boolean;

    return-object p0
.end method

.method public for_paper_signature_print_additional_auth_slip(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;
    .locals 0

    .line 995
    iput-object p1, p0, Lcom/squareup/server/account/protos/Preferences$Builder;->for_paper_signature_print_additional_auth_slip:Ljava/lang/Boolean;

    return-object p0
.end method

.method public for_paper_signature_use_quick_tip_receipt(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;
    .locals 0

    .line 989
    iput-object p1, p0, Lcom/squareup/server/account/protos/Preferences$Builder;->for_paper_signature_use_quick_tip_receipt:Ljava/lang/Boolean;

    return-object p0
.end method

.method public message(Ljava/lang/String;)Lcom/squareup/server/account/protos/Preferences$Builder;
    .locals 0

    .line 902
    iput-object p1, p0, Lcom/squareup/server/account/protos/Preferences$Builder;->message:Ljava/lang/String;

    return-object p0
.end method

.method public open_tickets_enabled(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;
    .locals 0

    .line 972
    iput-object p1, p0, Lcom/squareup/server/account/protos/Preferences$Builder;->open_tickets_enabled:Ljava/lang/Boolean;

    return-object p0
.end method

.method public skip_receipt_screen(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;
    .locals 0

    .line 1142
    iput-object p1, p0, Lcom/squareup/server/account/protos/Preferences$Builder;->skip_receipt_screen:Ljava/lang/Boolean;

    return-object p0
.end method

.method public skip_signature(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;
    .locals 0

    .line 946
    iput-object p1, p0, Lcom/squareup/server/account/protos/Preferences$Builder;->skip_signature:Ljava/lang/Boolean;

    return-object p0
.end method

.method public skip_signature_always(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;
    .locals 0

    .line 1038
    iput-object p1, p0, Lcom/squareup/server/account/protos/Preferences$Builder;->skip_signature_always:Ljava/lang/Boolean;

    return-object p0
.end method

.method public store_and_forward_enabled(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;
    .locals 0

    .line 951
    iput-object p1, p0, Lcom/squareup/server/account/protos/Preferences$Builder;->store_and_forward_enabled:Ljava/lang/Boolean;

    return-object p0
.end method

.method public store_and_forward_single_transaction_limit(Lcom/squareup/protos/common/Money;)Lcom/squareup/server/account/protos/Preferences$Builder;
    .locals 0

    .line 957
    iput-object p1, p0, Lcom/squareup/server/account/protos/Preferences$Builder;->store_and_forward_single_transaction_limit:Lcom/squareup/protos/common/Money;

    return-object p0
.end method

.method public success(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;
    .locals 0

    .line 892
    iput-object p1, p0, Lcom/squareup/server/account/protos/Preferences$Builder;->success:Ljava/lang/Boolean;

    return-object p0
.end method

.method public terminal_connected_mode_enabled(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;
    .locals 0

    .line 1116
    iput-object p1, p0, Lcom/squareup/server/account/protos/Preferences$Builder;->terminal_connected_mode_enabled:Ljava/lang/Boolean;

    return-object p0
.end method

.method public tipping_calculation_phase(Ljava/lang/String;)Lcom/squareup/server/account/protos/Preferences$Builder;
    .locals 0

    .line 1028
    iput-object p1, p0, Lcom/squareup/server/account/protos/Preferences$Builder;->tipping_calculation_phase:Ljava/lang/String;

    return-object p0
.end method

.method public tipping_custom_percentages(Ljava/util/List;)Lcom/squareup/server/account/protos/Preferences$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Double;",
            ">;)",
            "Lcom/squareup/server/account/protos/Preferences$Builder;"
        }
    .end annotation

    .line 940
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 941
    iput-object p1, p0, Lcom/squareup/server/account/protos/Preferences$Builder;->tipping_custom_percentages:Ljava/util/List;

    return-object p0
.end method

.method public tipping_enabled(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;
    .locals 0

    .line 925
    iput-object p1, p0, Lcom/squareup/server/account/protos/Preferences$Builder;->tipping_enabled:Ljava/lang/Boolean;

    return-object p0
.end method

.method public tipping_use_custom_percentages(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;
    .locals 0

    .line 930
    iput-object p1, p0, Lcom/squareup/server/account/protos/Preferences$Builder;->tipping_use_custom_percentages:Ljava/lang/Boolean;

    return-object p0
.end method

.method public tipping_use_manual_tip_entry(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;
    .locals 0

    .line 935
    iput-object p1, p0, Lcom/squareup/server/account/protos/Preferences$Builder;->tipping_use_manual_tip_entry:Ljava/lang/Boolean;

    return-object p0
.end method

.method public title(Ljava/lang/String;)Lcom/squareup/server/account/protos/Preferences$Builder;
    .locals 0

    .line 897
    iput-object p1, p0, Lcom/squareup/server/account/protos/Preferences$Builder;->title:Ljava/lang/String;

    return-object p0
.end method

.method public use_allow_swipe_for_chip_cards(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;
    .locals 0

    .line 1023
    iput-object p1, p0, Lcom/squareup/server/account/protos/Preferences$Builder;->use_allow_swipe_for_chip_cards:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_curated_image_for_receipt(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;
    .locals 0

    .line 962
    iput-object p1, p0, Lcom/squareup/server/account/protos/Preferences$Builder;->use_curated_image_for_receipt:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_open_tickets_menu_as_home_screen(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;
    .locals 0

    .line 1044
    iput-object p1, p0, Lcom/squareup/server/account/protos/Preferences$Builder;->use_open_tickets_menu_as_home_screen:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_order_hub(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;
    .locals 0

    .line 1131
    iput-object p1, p0, Lcom/squareup/server/account/protos/Preferences$Builder;->use_order_hub:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_paper_signature(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;
    .locals 0

    .line 977
    iput-object p1, p0, Lcom/squareup/server/account/protos/Preferences$Builder;->use_paper_signature:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_predefined_tickets(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;
    .locals 0

    .line 1049
    iput-object p1, p0, Lcom/squareup/server/account/protos/Preferences$Builder;->use_predefined_tickets:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_separate_tipping_screen(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;
    .locals 0

    .line 967
    iput-object p1, p0, Lcom/squareup/server/account/protos/Preferences$Builder;->use_separate_tipping_screen:Ljava/lang/Boolean;

    return-object p0
.end method
