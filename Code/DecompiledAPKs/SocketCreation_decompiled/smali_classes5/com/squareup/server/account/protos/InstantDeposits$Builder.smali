.class public final Lcom/squareup/server/account/protos/InstantDeposits$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "InstantDeposits.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/InstantDeposits;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/InstantDeposits;",
        "Lcom/squareup/server/account/protos/InstantDeposits$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public instant_deposit_fee:Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;

.field public linked_card:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;

.field public same_day_deposit_fee:Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 153
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/server/account/protos/InstantDeposits;
    .locals 5

    .line 179
    new-instance v0, Lcom/squareup/server/account/protos/InstantDeposits;

    iget-object v1, p0, Lcom/squareup/server/account/protos/InstantDeposits$Builder;->linked_card:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;

    iget-object v2, p0, Lcom/squareup/server/account/protos/InstantDeposits$Builder;->instant_deposit_fee:Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;

    iget-object v3, p0, Lcom/squareup/server/account/protos/InstantDeposits$Builder;->same_day_deposit_fee:Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/server/account/protos/InstantDeposits;-><init>(Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 146
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/InstantDeposits$Builder;->build()Lcom/squareup/server/account/protos/InstantDeposits;

    move-result-object v0

    return-object v0
.end method

.method public instant_deposit_fee(Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;)Lcom/squareup/server/account/protos/InstantDeposits$Builder;
    .locals 0

    .line 165
    iput-object p1, p0, Lcom/squareup/server/account/protos/InstantDeposits$Builder;->instant_deposit_fee:Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;

    return-object p0
.end method

.method public linked_card(Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;)Lcom/squareup/server/account/protos/InstantDeposits$Builder;
    .locals 0

    .line 157
    iput-object p1, p0, Lcom/squareup/server/account/protos/InstantDeposits$Builder;->linked_card:Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard;

    return-object p0
.end method

.method public same_day_deposit_fee(Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;)Lcom/squareup/server/account/protos/InstantDeposits$Builder;
    .locals 0

    .line 173
    iput-object p1, p0, Lcom/squareup/server/account/protos/InstantDeposits$Builder;->same_day_deposit_fee:Lcom/squareup/server/account/protos/InstantDeposits$FeeStructure;

    return-object p0
.end method
