.class final Lcom/squareup/server/account/protos/User$MerchantKey$ProtoAdapter_MerchantKey;
.super Lcom/squareup/wire/ProtoAdapter;
.source "User.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/User$MerchantKey;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_MerchantKey"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/server/account/protos/User$MerchantKey;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1392
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/server/account/protos/User$MerchantKey;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/server/account/protos/User$MerchantKey;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1413
    new-instance v0, Lcom/squareup/server/account/protos/User$MerchantKey$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/User$MerchantKey$Builder;-><init>()V

    .line 1414
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 1415
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 1421
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 1419
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/User$MerchantKey$Builder;->key(Ljava/lang/String;)Lcom/squareup/server/account/protos/User$MerchantKey$Builder;

    goto :goto_0

    .line 1418
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/User$MerchantKey$Builder;->timestamp(Ljava/lang/Long;)Lcom/squareup/server/account/protos/User$MerchantKey$Builder;

    goto :goto_0

    .line 1417
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/User$MerchantKey$Builder;->master_key_id(Ljava/lang/Integer;)Lcom/squareup/server/account/protos/User$MerchantKey$Builder;

    goto :goto_0

    .line 1425
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/User$MerchantKey$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 1426
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/User$MerchantKey$Builder;->build()Lcom/squareup/server/account/protos/User$MerchantKey;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1390
    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/User$MerchantKey$ProtoAdapter_MerchantKey;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/server/account/protos/User$MerchantKey;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/server/account/protos/User$MerchantKey;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1405
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/User$MerchantKey;->master_key_id:Ljava/lang/Integer;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1406
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/User$MerchantKey;->timestamp:Ljava/lang/Long;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1407
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/User$MerchantKey;->key:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1408
    invoke-virtual {p2}, Lcom/squareup/server/account/protos/User$MerchantKey;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1390
    check-cast p2, Lcom/squareup/server/account/protos/User$MerchantKey;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/server/account/protos/User$MerchantKey$ProtoAdapter_MerchantKey;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/server/account/protos/User$MerchantKey;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/server/account/protos/User$MerchantKey;)I
    .locals 4

    .line 1397
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->INT32:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/User$MerchantKey;->master_key_id:Ljava/lang/Integer;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->INT64:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/User$MerchantKey;->timestamp:Ljava/lang/Long;

    const/4 v3, 0x2

    .line 1398
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/User$MerchantKey;->key:Ljava/lang/String;

    const/4 v3, 0x3

    .line 1399
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1400
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/User$MerchantKey;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 1390
    check-cast p1, Lcom/squareup/server/account/protos/User$MerchantKey;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/User$MerchantKey$ProtoAdapter_MerchantKey;->encodedSize(Lcom/squareup/server/account/protos/User$MerchantKey;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/server/account/protos/User$MerchantKey;)Lcom/squareup/server/account/protos/User$MerchantKey;
    .locals 0

    .line 1431
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/User$MerchantKey;->newBuilder()Lcom/squareup/server/account/protos/User$MerchantKey$Builder;

    move-result-object p1

    .line 1432
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/User$MerchantKey$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 1433
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/User$MerchantKey$Builder;->build()Lcom/squareup/server/account/protos/User$MerchantKey;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1390
    check-cast p1, Lcom/squareup/server/account/protos/User$MerchantKey;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/User$MerchantKey$ProtoAdapter_MerchantKey;->redact(Lcom/squareup/server/account/protos/User$MerchantKey;)Lcom/squareup/server/account/protos/User$MerchantKey;

    move-result-object p1

    return-object p1
.end method
