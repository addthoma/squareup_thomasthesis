.class public final Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FlagsAndPermissions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public android_third_party_gc_ime:Ljava/lang/Boolean;

.field public android_third_party_gc_support:Ljava/lang/Boolean;

.field public can_use_cashout_reason:Ljava/lang/Boolean;

.field public configure_egift_on_pos:Ljava/lang/Boolean;

.field public refund_to_gift_card_android:Ljava/lang/Boolean;

.field public sell_egift_on_pos:Ljava/lang/Boolean;

.field public show_plastic_gift_cards:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 10039
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public android_third_party_gc_ime(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;
    .locals 0

    .line 10094
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;->android_third_party_gc_ime:Ljava/lang/Boolean;

    return-object p0
.end method

.method public android_third_party_gc_support(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;
    .locals 0

    .line 10046
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;->android_third_party_gc_support:Ljava/lang/Boolean;

    return-object p0
.end method

.method public build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;
    .locals 10

    .line 10100
    new-instance v9, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;->android_third_party_gc_support:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;->show_plastic_gift_cards:Ljava/lang/Boolean;

    iget-object v3, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;->can_use_cashout_reason:Ljava/lang/Boolean;

    iget-object v4, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;->refund_to_gift_card_android:Ljava/lang/Boolean;

    iget-object v5, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;->sell_egift_on_pos:Ljava/lang/Boolean;

    iget-object v6, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;->configure_egift_on_pos:Ljava/lang/Boolean;

    iget-object v7, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;->android_third_party_gc_ime:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v8

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v9
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 10024
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard;

    move-result-object v0

    return-object v0
.end method

.method public can_use_cashout_reason(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;
    .locals 0

    .line 10062
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;->can_use_cashout_reason:Ljava/lang/Boolean;

    return-object p0
.end method

.method public configure_egift_on_pos(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;
    .locals 0

    .line 10086
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;->configure_egift_on_pos:Ljava/lang/Boolean;

    return-object p0
.end method

.method public refund_to_gift_card_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;
    .locals 0

    .line 10070
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;->refund_to_gift_card_android:Ljava/lang/Boolean;

    return-object p0
.end method

.method public sell_egift_on_pos(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;
    .locals 0

    .line 10078
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;->sell_egift_on_pos:Ljava/lang/Boolean;

    return-object p0
.end method

.method public show_plastic_gift_cards(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;
    .locals 0

    .line 10054
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Giftcard$Builder;->show_plastic_gift_cards:Ljava/lang/Boolean;

    return-object p0
.end method
