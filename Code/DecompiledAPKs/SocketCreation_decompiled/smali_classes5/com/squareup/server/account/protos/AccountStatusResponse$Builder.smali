.class public final Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "AccountStatusResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/AccountStatusResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/AccountStatusResponse;",
        "Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public api_authorized_application_ids:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public api_url:Ljava/lang/String;

.field public api_url_list:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public appointment_settings:Lcom/squareup/server/account/protos/AppointmentSettings;

.field public bank_account_status:Ljava/lang/String;

.field public business_banking:Lcom/squareup/server/account/protos/BusinessBanking;

.field public device_credential:Lcom/squareup/server/account/protos/DeviceCredential;

.field public disputes:Lcom/squareup/server/account/protos/Disputes;

.field public employee:Lcom/squareup/server/account/protos/EmployeesEntity;

.field public error_message:Ljava/lang/String;

.field public error_title:Ljava/lang/String;

.field public features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

.field public fee_types:Lcom/squareup/server/account/protos/FeeTypesProto;

.field public fees:Lcom/squareup/server/account/protos/ProcessingFees;

.field public forced_offline_mode_server_urls:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public frozen:Ljava/lang/Boolean;

.field public gift_cards:Lcom/squareup/server/account/protos/GiftCards;

.field public installments_settings:Lcom/squareup/server/account/protos/InstallmentsSettings;

.field public instant_deposits:Lcom/squareup/server/account/protos/InstantDeposits;

.field public limits:Lcom/squareup/server/account/protos/Limits;

.field public loyalty_program:Lcom/squareup/server/account/protos/LoyaltyProgram;

.field public merchant_register_settings:Lcom/squareup/server/account/protos/MerchantRegisterSettings;

.field public merchant_units:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/server/account/protos/MerchantUnit;",
            ">;"
        }
    .end annotation
.end field

.field public message:Ljava/lang/String;

.field public notifications:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/server/account/protos/Notification;",
            ">;"
        }
    .end annotation
.end field

.field public open_tickets:Lcom/squareup/server/account/protos/OpenTickets;

.field public other_tenders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/server/account/protos/OtherTenderType;",
            ">;"
        }
    .end annotation
.end field

.field public preferences:Lcom/squareup/server/account/protos/Preferences;

.field public privacy:Lcom/squareup/server/account/protos/PrivacyFeatures;

.field public product_intent:Lcom/squareup/server/account/protos/ProductIntent;

.field public r12_getting_started_video_url:Ljava/lang/String;

.field public return_policy:Ljava/lang/String;

.field public server_time:Ljava/lang/String;

.field public store_and_forward_bill_key:Lcom/squareup/server/account/protos/StoreAndForwardKey;

.field public store_and_forward_key:Lcom/squareup/server/account/protos/StoreAndForwardKey;

.field public store_and_forward_payment_expiration_seconds:Ljava/lang/Integer;

.field public store_and_forward_user_credential:Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;

.field public subscriptions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/server/account/protos/Subscription;",
            ">;"
        }
    .end annotation
.end field

.field public success:Ljava/lang/Boolean;

.field public tax_ids:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/server/account/protos/TaxId;",
            ">;"
        }
    .end annotation
.end field

.field public third_party_card_tender:Lcom/squareup/server/account/protos/OtherTenderType;

.field public tipping:Lcom/squareup/server/account/protos/Tipping;

.field public title:Ljava/lang/String;

.field public tutorial:Lcom/squareup/server/account/protos/Tutorial;

.field public user:Lcom/squareup/server/account/protos/User;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 953
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 954
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->other_tenders:Ljava/util/List;

    .line 955
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->notifications:Ljava/util/List;

    .line 956
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->merchant_units:Ljava/util/List;

    .line 957
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->tax_ids:Ljava/util/List;

    .line 958
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->api_url_list:Ljava/util/List;

    .line 959
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->subscriptions:Ljava/util/List;

    .line 960
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->api_authorized_application_ids:Ljava/util/List;

    .line 961
    invoke-static {}, Lcom/squareup/wire/internal/Internal;->newMutableList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->forced_offline_mode_server_urls:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public api_authorized_application_ids(Ljava/util/List;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;"
        }
    .end annotation

    .line 1169
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 1170
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->api_authorized_application_ids:Ljava/util/List;

    return-object p0
.end method

.method public api_url(Ljava/lang/String;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;
    .locals 0

    .line 1091
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->api_url:Ljava/lang/String;

    return-object p0
.end method

.method public api_url_list(Ljava/util/List;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;"
        }
    .end annotation

    .line 1143
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 1144
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->api_url_list:Ljava/util/List;

    return-object p0
.end method

.method public appointment_settings(Lcom/squareup/server/account/protos/AppointmentSettings;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;
    .locals 0

    .line 1215
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->appointment_settings:Lcom/squareup/server/account/protos/AppointmentSettings;

    return-object p0
.end method

.method public bank_account_status(Ljava/lang/String;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;
    .locals 0

    .line 1049
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->bank_account_status:Ljava/lang/String;

    return-object p0
.end method

.method public build()Lcom/squareup/server/account/protos/AccountStatusResponse;
    .locals 2

    .line 1234
    new-instance v0, Lcom/squareup/server/account/protos/AccountStatusResponse;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/squareup/server/account/protos/AccountStatusResponse;-><init>(Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 862
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->build()Lcom/squareup/server/account/protos/AccountStatusResponse;

    move-result-object v0

    return-object v0
.end method

.method public business_banking(Lcom/squareup/server/account/protos/BusinessBanking;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;
    .locals 0

    .line 1154
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->business_banking:Lcom/squareup/server/account/protos/BusinessBanking;

    return-object p0
.end method

.method public device_credential(Lcom/squareup/server/account/protos/DeviceCredential;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;
    .locals 0

    .line 1183
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->device_credential:Lcom/squareup/server/account/protos/DeviceCredential;

    return-object p0
.end method

.method public disputes(Lcom/squareup/server/account/protos/Disputes;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;
    .locals 0

    .line 1197
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->disputes:Lcom/squareup/server/account/protos/Disputes;

    return-object p0
.end method

.method public employee(Lcom/squareup/server/account/protos/EmployeesEntity;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;
    .locals 0

    .line 1133
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->employee:Lcom/squareup/server/account/protos/EmployeesEntity;

    return-object p0
.end method

.method public error_message(Ljava/lang/String;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;
    .locals 0

    .line 996
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->error_message:Ljava/lang/String;

    return-object p0
.end method

.method public error_title(Ljava/lang/String;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;
    .locals 0

    .line 986
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->error_title:Ljava/lang/String;

    return-object p0
.end method

.method public features(Lcom/squareup/server/account/protos/FlagsAndPermissions;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;
    .locals 0

    .line 1016
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->features:Lcom/squareup/server/account/protos/FlagsAndPermissions;

    return-object p0
.end method

.method public fee_types(Lcom/squareup/server/account/protos/FeeTypesProto;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;
    .locals 0

    .line 1059
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->fee_types:Lcom/squareup/server/account/protos/FeeTypesProto;

    return-object p0
.end method

.method public fees(Lcom/squareup/server/account/protos/ProcessingFees;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;
    .locals 0

    .line 1054
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->fees:Lcom/squareup/server/account/protos/ProcessingFees;

    return-object p0
.end method

.method public forced_offline_mode_server_urls(Ljava/util/List;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;"
        }
    .end annotation

    .line 1191
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 1192
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->forced_offline_mode_server_urls:Ljava/util/List;

    return-object p0
.end method

.method public frozen(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;
    .locals 0

    .line 1210
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->frozen:Ljava/lang/Boolean;

    return-object p0
.end method

.method public gift_cards(Lcom/squareup/server/account/protos/GiftCards;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;
    .locals 0

    .line 1075
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->gift_cards:Lcom/squareup/server/account/protos/GiftCards;

    return-object p0
.end method

.method public installments_settings(Lcom/squareup/server/account/protos/InstallmentsSettings;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;
    .locals 0

    .line 1202
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->installments_settings:Lcom/squareup/server/account/protos/InstallmentsSettings;

    return-object p0
.end method

.method public instant_deposits(Lcom/squareup/server/account/protos/InstantDeposits;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;
    .locals 0

    .line 1080
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->instant_deposits:Lcom/squareup/server/account/protos/InstantDeposits;

    return-object p0
.end method

.method public limits(Lcom/squareup/server/account/protos/Limits;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;
    .locals 0

    .line 1011
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->limits:Lcom/squareup/server/account/protos/Limits;

    return-object p0
.end method

.method public loyalty_program(Lcom/squareup/server/account/protos/LoyaltyProgram;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;
    .locals 0

    .line 1149
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->loyalty_program:Lcom/squareup/server/account/protos/LoyaltyProgram;

    return-object p0
.end method

.method public merchant_register_settings(Lcom/squareup/server/account/protos/MerchantRegisterSettings;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;
    .locals 0

    .line 1064
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->merchant_register_settings:Lcom/squareup/server/account/protos/MerchantRegisterSettings;

    return-object p0
.end method

.method public merchant_units(Ljava/util/List;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/server/account/protos/MerchantUnit;",
            ">;)",
            "Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;"
        }
    .end annotation

    .line 1069
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 1070
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->merchant_units:Ljava/util/List;

    return-object p0
.end method

.method public message(Ljava/lang/String;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;
    .locals 0

    .line 978
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->message:Ljava/lang/String;

    return-object p0
.end method

.method public notifications(Ljava/util/List;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/server/account/protos/Notification;",
            ">;)",
            "Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;"
        }
    .end annotation

    .line 1038
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 1039
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->notifications:Ljava/util/List;

    return-object p0
.end method

.method public open_tickets(Lcom/squareup/server/account/protos/OpenTickets;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;
    .locals 0

    .line 1178
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->open_tickets:Lcom/squareup/server/account/protos/OpenTickets;

    return-object p0
.end method

.method public other_tenders(Ljava/util/List;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/server/account/protos/OtherTenderType;",
            ">;)",
            "Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;"
        }
    .end annotation

    .line 1024
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 1025
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->other_tenders:Ljava/util/List;

    return-object p0
.end method

.method public preferences(Lcom/squareup/server/account/protos/Preferences;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;
    .locals 0

    .line 1044
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->preferences:Lcom/squareup/server/account/protos/Preferences;

    return-object p0
.end method

.method public privacy(Lcom/squareup/server/account/protos/PrivacyFeatures;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;
    .locals 0

    .line 1228
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->privacy:Lcom/squareup/server/account/protos/PrivacyFeatures;

    return-object p0
.end method

.method public product_intent(Lcom/squareup/server/account/protos/ProductIntent;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;
    .locals 0

    .line 1223
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->product_intent:Lcom/squareup/server/account/protos/ProductIntent;

    return-object p0
.end method

.method public r12_getting_started_video_url(Ljava/lang/String;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;
    .locals 0

    .line 1138
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->r12_getting_started_video_url:Ljava/lang/String;

    return-object p0
.end method

.method public return_policy(Ljava/lang/String;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;
    .locals 0

    .line 1006
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->return_policy:Ljava/lang/String;

    return-object p0
.end method

.method public server_time(Ljava/lang/String;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;
    .locals 0

    .line 1118
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->server_time:Ljava/lang/String;

    return-object p0
.end method

.method public store_and_forward_bill_key(Lcom/squareup/server/account/protos/StoreAndForwardKey;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;
    .locals 0

    .line 1101
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->store_and_forward_bill_key:Lcom/squareup/server/account/protos/StoreAndForwardKey;

    return-object p0
.end method

.method public store_and_forward_key(Lcom/squareup/server/account/protos/StoreAndForwardKey;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;
    .locals 0

    .line 1096
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->store_and_forward_key:Lcom/squareup/server/account/protos/StoreAndForwardKey;

    return-object p0
.end method

.method public store_and_forward_payment_expiration_seconds(Ljava/lang/Integer;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;
    .locals 0

    .line 1107
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->store_and_forward_payment_expiration_seconds:Ljava/lang/Integer;

    return-object p0
.end method

.method public store_and_forward_user_credential(Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;
    .locals 0

    .line 1113
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->store_and_forward_user_credential:Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;

    return-object p0
.end method

.method public subscriptions(Ljava/util/List;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/server/account/protos/Subscription;",
            ">;)",
            "Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;"
        }
    .end annotation

    .line 1159
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 1160
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->subscriptions:Ljava/util/List;

    return-object p0
.end method

.method public success(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;
    .locals 0

    .line 968
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->success:Ljava/lang/Boolean;

    return-object p0
.end method

.method public tax_ids(Ljava/util/List;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/server/account/protos/TaxId;",
            ">;)",
            "Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;"
        }
    .end annotation

    .line 1085
    invoke-static {p1}, Lcom/squareup/wire/internal/Internal;->checkElementsNotNull(Ljava/util/List;)V

    .line 1086
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->tax_ids:Ljava/util/List;

    return-object p0
.end method

.method public third_party_card_tender(Lcom/squareup/server/account/protos/OtherTenderType;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;
    .locals 0

    .line 1033
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->third_party_card_tender:Lcom/squareup/server/account/protos/OtherTenderType;

    return-object p0
.end method

.method public tipping(Lcom/squareup/server/account/protos/Tipping;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;
    .locals 0

    .line 1123
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->tipping:Lcom/squareup/server/account/protos/Tipping;

    return-object p0
.end method

.method public title(Ljava/lang/String;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;
    .locals 0

    .line 973
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->title:Ljava/lang/String;

    return-object p0
.end method

.method public tutorial(Lcom/squareup/server/account/protos/Tutorial;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;
    .locals 0

    .line 1128
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->tutorial:Lcom/squareup/server/account/protos/Tutorial;

    return-object p0
.end method

.method public user(Lcom/squareup/server/account/protos/User;)Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;
    .locals 0

    .line 1001
    iput-object p1, p0, Lcom/squareup/server/account/protos/AccountStatusResponse$Builder;->user:Lcom/squareup/server/account/protos/User;

    return-object p0
.end method
