.class public final Lcom/squareup/server/account/protos/User$SquareLocale;
.super Lcom/squareup/wire/AndroidMessage;
.source "User.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/User;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SquareLocale"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/User$SquareLocale$ProtoAdapter_SquareLocale;,
        Lcom/squareup/server/account/protos/User$SquareLocale$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/User$SquareLocale;",
        "Lcom/squareup/server/account/protos/User$SquareLocale$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/User$SquareLocale;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/User$SquareLocale;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/User$SquareLocale;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/User$SquareLocale;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_COUNTRY_CODE:Ljava/lang/String; = ""

.field public static final DEFAULT_COUNTRY_CODE_GUESS:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final country_code:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final country_code_guess:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1
    .end annotation
.end field

.field public final currency_codes:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.CurrencyCode#ADAPTER"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x3
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 503
    new-instance v0, Lcom/squareup/server/account/protos/User$SquareLocale$ProtoAdapter_SquareLocale;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/User$SquareLocale$ProtoAdapter_SquareLocale;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/User$SquareLocale;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 505
    sget-object v0, Lcom/squareup/server/account/protos/User$SquareLocale;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/User$SquareLocale;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;)V"
        }
    .end annotation

    .line 539
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/squareup/server/account/protos/User$SquareLocale;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/protos/common/CurrencyCode;",
            ">;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 544
    sget-object v0, Lcom/squareup/server/account/protos/User$SquareLocale;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p4}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 545
    iput-object p1, p0, Lcom/squareup/server/account/protos/User$SquareLocale;->country_code_guess:Ljava/lang/String;

    .line 546
    iput-object p2, p0, Lcom/squareup/server/account/protos/User$SquareLocale;->country_code:Ljava/lang/String;

    const-string p1, "currency_codes"

    .line 547
    invoke-static {p1, p3}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/server/account/protos/User$SquareLocale;->currency_codes:Ljava/util/List;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/User$SquareLocale$Builder;)Lcom/squareup/server/account/protos/User$SquareLocale$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 609
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/User$SquareLocale;->newBuilder()Lcom/squareup/server/account/protos/User$SquareLocale$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 563
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/User$SquareLocale;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 564
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/User$SquareLocale;

    .line 565
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/User$SquareLocale;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/User$SquareLocale;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/User$SquareLocale;->country_code_guess:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/User$SquareLocale;->country_code_guess:Ljava/lang/String;

    .line 566
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/User$SquareLocale;->country_code:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/User$SquareLocale;->country_code:Ljava/lang/String;

    .line 567
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/User$SquareLocale;->currency_codes:Ljava/util/List;

    iget-object p1, p1, Lcom/squareup/server/account/protos/User$SquareLocale;->currency_codes:Ljava/util/List;

    .line 568
    invoke-interface {v1, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 573
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_2

    .line 575
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/User$SquareLocale;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 576
    iget-object v1, p0, Lcom/squareup/server/account/protos/User$SquareLocale;->country_code_guess:Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 577
    iget-object v1, p0, Lcom/squareup/server/account/protos/User$SquareLocale;->country_code:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x25

    .line 578
    iget-object v1, p0, Lcom/squareup/server/account/protos/User$SquareLocale;->currency_codes:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 579
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/User$SquareLocale$Builder;
    .locals 2

    .line 552
    new-instance v0, Lcom/squareup/server/account/protos/User$SquareLocale$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/User$SquareLocale$Builder;-><init>()V

    .line 553
    iget-object v1, p0, Lcom/squareup/server/account/protos/User$SquareLocale;->country_code_guess:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/User$SquareLocale$Builder;->country_code_guess:Ljava/lang/String;

    .line 554
    iget-object v1, p0, Lcom/squareup/server/account/protos/User$SquareLocale;->country_code:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/User$SquareLocale$Builder;->country_code:Ljava/lang/String;

    .line 555
    iget-object v1, p0, Lcom/squareup/server/account/protos/User$SquareLocale;->currency_codes:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/server/account/protos/User$SquareLocale$Builder;->currency_codes:Ljava/util/List;

    .line 556
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/User$SquareLocale;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/User$SquareLocale$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 502
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/User$SquareLocale;->newBuilder()Lcom/squareup/server/account/protos/User$SquareLocale$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/User$SquareLocale;)Lcom/squareup/server/account/protos/User$SquareLocale;
    .locals 2

    .line 602
    iget-object v0, p1, Lcom/squareup/server/account/protos/User$SquareLocale;->country_code_guess:Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/User$SquareLocale;->requireBuilder(Lcom/squareup/server/account/protos/User$SquareLocale$Builder;)Lcom/squareup/server/account/protos/User$SquareLocale$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/User$SquareLocale;->country_code_guess:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/User$SquareLocale$Builder;->country_code_guess(Ljava/lang/String;)Lcom/squareup/server/account/protos/User$SquareLocale$Builder;

    move-result-object v1

    .line 603
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/User$SquareLocale;->country_code:Ljava/lang/String;

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/User$SquareLocale;->requireBuilder(Lcom/squareup/server/account/protos/User$SquareLocale$Builder;)Lcom/squareup/server/account/protos/User$SquareLocale$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/User$SquareLocale;->country_code:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/User$SquareLocale$Builder;->country_code(Ljava/lang/String;)Lcom/squareup/server/account/protos/User$SquareLocale$Builder;

    move-result-object v1

    .line 604
    :cond_1
    iget-object v0, p1, Lcom/squareup/server/account/protos/User$SquareLocale;->currency_codes:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/User$SquareLocale;->requireBuilder(Lcom/squareup/server/account/protos/User$SquareLocale$Builder;)Lcom/squareup/server/account/protos/User$SquareLocale$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/User$SquareLocale;->currency_codes:Ljava/util/List;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/User$SquareLocale$Builder;->currency_codes(Ljava/util/List;)Lcom/squareup/server/account/protos/User$SquareLocale$Builder;

    move-result-object v1

    :cond_2
    if-nez v1, :cond_3

    move-object p1, p0

    goto :goto_0

    .line 605
    :cond_3
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/User$SquareLocale$Builder;->build()Lcom/squareup/server/account/protos/User$SquareLocale;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 502
    check-cast p1, Lcom/squareup/server/account/protos/User$SquareLocale;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/User$SquareLocale;->overlay(Lcom/squareup/server/account/protos/User$SquareLocale;)Lcom/squareup/server/account/protos/User$SquareLocale;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/User$SquareLocale;
    .locals 0

    return-object p0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 502
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/User$SquareLocale;->populateDefaults()Lcom/squareup/server/account/protos/User$SquareLocale;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 586
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 587
    iget-object v1, p0, Lcom/squareup/server/account/protos/User$SquareLocale;->country_code_guess:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, ", country_code_guess="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/User$SquareLocale;->country_code_guess:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 588
    :cond_0
    iget-object v1, p0, Lcom/squareup/server/account/protos/User$SquareLocale;->country_code:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", country_code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/User$SquareLocale;->country_code:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 589
    :cond_1
    iget-object v1, p0, Lcom/squareup/server/account/protos/User$SquareLocale;->currency_codes:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, ", currency_codes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/User$SquareLocale;->currency_codes:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "SquareLocale{"

    .line 590
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
