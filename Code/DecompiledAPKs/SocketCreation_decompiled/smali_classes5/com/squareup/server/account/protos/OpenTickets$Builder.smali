.class public final Lcom/squareup/server/account/protos/OpenTickets$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "OpenTickets.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/OpenTickets;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/OpenTickets;",
        "Lcom/squareup/server/account/protos/OpenTickets$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public open_tickets_max:Ljava/lang/Integer;

.field public ticket_groups_max:Ljava/lang/Integer;

.field public ticket_templates_per_group_max:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 149
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/server/account/protos/OpenTickets;
    .locals 5

    .line 169
    new-instance v0, Lcom/squareup/server/account/protos/OpenTickets;

    iget-object v1, p0, Lcom/squareup/server/account/protos/OpenTickets$Builder;->open_tickets_max:Ljava/lang/Integer;

    iget-object v2, p0, Lcom/squareup/server/account/protos/OpenTickets$Builder;->ticket_groups_max:Ljava/lang/Integer;

    iget-object v3, p0, Lcom/squareup/server/account/protos/OpenTickets$Builder;->ticket_templates_per_group_max:Ljava/lang/Integer;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/server/account/protos/OpenTickets;-><init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 142
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/OpenTickets$Builder;->build()Lcom/squareup/server/account/protos/OpenTickets;

    move-result-object v0

    return-object v0
.end method

.method public open_tickets_max(Ljava/lang/Integer;)Lcom/squareup/server/account/protos/OpenTickets$Builder;
    .locals 0

    .line 153
    iput-object p1, p0, Lcom/squareup/server/account/protos/OpenTickets$Builder;->open_tickets_max:Ljava/lang/Integer;

    return-object p0
.end method

.method public ticket_groups_max(Ljava/lang/Integer;)Lcom/squareup/server/account/protos/OpenTickets$Builder;
    .locals 0

    .line 158
    iput-object p1, p0, Lcom/squareup/server/account/protos/OpenTickets$Builder;->ticket_groups_max:Ljava/lang/Integer;

    return-object p0
.end method

.method public ticket_templates_per_group_max(Ljava/lang/Integer;)Lcom/squareup/server/account/protos/OpenTickets$Builder;
    .locals 0

    .line 163
    iput-object p1, p0, Lcom/squareup/server/account/protos/OpenTickets$Builder;->ticket_templates_per_group_max:Ljava/lang/Integer;

    return-object p0
.end method
