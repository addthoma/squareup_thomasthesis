.class public Lcom/squareup/server/account/FeeTypes;
.super Ljava/lang/Object;
.source "FeeTypes.java"


# instance fields
.field private final proto:Lcom/squareup/server/account/protos/FeeTypesProto;


# direct methods
.method private constructor <init>(Lcom/squareup/server/account/protos/FeeTypesProto;)V
    .locals 0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/server/account/FeeTypes;->proto:Lcom/squareup/server/account/protos/FeeTypesProto;

    return-void
.end method

.method public static fromProto(Lcom/squareup/server/account/protos/FeeTypesProto;)Lcom/squareup/server/account/FeeTypes;
    .locals 1

    .line 18
    new-instance v0, Lcom/squareup/server/account/FeeTypes;

    invoke-direct {v0, p0}, Lcom/squareup/server/account/FeeTypes;-><init>(Lcom/squareup/server/account/protos/FeeTypesProto;)V

    return-object v0
.end method


# virtual methods
.method public getDefault()Lcom/squareup/server/account/protos/FeeType;
    .locals 3

    .line 38
    iget-object v0, p0, Lcom/squareup/server/account/FeeTypes;->proto:Lcom/squareup/server/account/protos/FeeTypesProto;

    if-eqz v0, :cond_3

    iget-object v0, v0, Lcom/squareup/server/account/protos/FeeTypesProto;->types:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    .line 41
    :cond_0
    iget-object v0, p0, Lcom/squareup/server/account/FeeTypes;->proto:Lcom/squareup/server/account/protos/FeeTypesProto;

    iget-object v0, v0, Lcom/squareup/server/account/protos/FeeTypesProto;->types:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/server/account/protos/FeeType;

    .line 42
    iget-object v2, v1, Lcom/squareup/server/account/protos/FeeType;->is_default:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1

    return-object v1

    .line 46
    :cond_2
    iget-object v0, p0, Lcom/squareup/server/account/FeeTypes;->proto:Lcom/squareup/server/account/protos/FeeTypesProto;

    iget-object v0, v0, Lcom/squareup/server/account/protos/FeeTypesProto;->types:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/server/account/protos/FeeType;

    return-object v0

    :cond_3
    :goto_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getTypes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/server/account/protos/FeeType;",
            ">;"
        }
    .end annotation

    .line 28
    iget-object v0, p0, Lcom/squareup/server/account/FeeTypes;->proto:Lcom/squareup/server/account/protos/FeeTypesProto;

    if-nez v0, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    :cond_0
    iget-object v0, v0, Lcom/squareup/server/account/protos/FeeTypesProto;->types:Ljava/util/List;

    :goto_0
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .line 66
    iget-object v0, p0, Lcom/squareup/server/account/FeeTypes;->proto:Lcom/squareup/server/account/protos/FeeTypesProto;

    if-nez v0, :cond_0

    const-string v0, ""

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/FeeTypesProto;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public withId(Ljava/lang/String;)Lcom/squareup/server/account/protos/FeeType;
    .locals 4

    .line 54
    iget-object v0, p0, Lcom/squareup/server/account/FeeTypes;->proto:Lcom/squareup/server/account/protos/FeeTypesProto;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    .line 57
    :cond_0
    iget-object v0, v0, Lcom/squareup/server/account/protos/FeeTypesProto;->types:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/server/account/protos/FeeType;

    .line 58
    iget-object v3, v2, Lcom/squareup/server/account/protos/FeeType;->id:Ljava/lang/String;

    invoke-static {p1, v3}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    return-object v2

    :cond_2
    return-object v1
.end method
