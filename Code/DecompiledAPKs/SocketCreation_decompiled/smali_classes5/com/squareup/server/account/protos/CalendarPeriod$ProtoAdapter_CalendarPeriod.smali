.class final Lcom/squareup/server/account/protos/CalendarPeriod$ProtoAdapter_CalendarPeriod;
.super Lcom/squareup/wire/ProtoAdapter;
.source "CalendarPeriod.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/CalendarPeriod;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_CalendarPeriod"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/server/account/protos/CalendarPeriod;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 151
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/server/account/protos/CalendarPeriod;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/server/account/protos/CalendarPeriod;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 166
    new-instance v0, Lcom/squareup/server/account/protos/CalendarPeriod$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/CalendarPeriod$Builder;-><init>()V

    .line 167
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 168
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 171
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 175
    :cond_0
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/CalendarPeriod$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 176
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/CalendarPeriod$Builder;->build()Lcom/squareup/server/account/protos/CalendarPeriod;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 149
    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/CalendarPeriod$ProtoAdapter_CalendarPeriod;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/server/account/protos/CalendarPeriod;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/server/account/protos/CalendarPeriod;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 161
    invoke-virtual {p2}, Lcom/squareup/server/account/protos/CalendarPeriod;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 149
    check-cast p2, Lcom/squareup/server/account/protos/CalendarPeriod;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/server/account/protos/CalendarPeriod$ProtoAdapter_CalendarPeriod;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/server/account/protos/CalendarPeriod;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/server/account/protos/CalendarPeriod;)I
    .locals 0

    .line 156
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/CalendarPeriod;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    return p1
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 149
    check-cast p1, Lcom/squareup/server/account/protos/CalendarPeriod;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/CalendarPeriod$ProtoAdapter_CalendarPeriod;->encodedSize(Lcom/squareup/server/account/protos/CalendarPeriod;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/server/account/protos/CalendarPeriod;)Lcom/squareup/server/account/protos/CalendarPeriod;
    .locals 0

    .line 181
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/CalendarPeriod;->newBuilder()Lcom/squareup/server/account/protos/CalendarPeriod$Builder;

    move-result-object p1

    .line 182
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/CalendarPeriod$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 183
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/CalendarPeriod$Builder;->build()Lcom/squareup/server/account/protos/CalendarPeriod;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 149
    check-cast p1, Lcom/squareup/server/account/protos/CalendarPeriod;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/CalendarPeriod$ProtoAdapter_CalendarPeriod;->redact(Lcom/squareup/server/account/protos/CalendarPeriod;)Lcom/squareup/server/account/protos/CalendarPeriod;

    move-result-object p1

    return-object p1
.end method
