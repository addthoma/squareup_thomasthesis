.class public interface abstract Lcom/squareup/server/account/AuthenticationService;
.super Ljava/lang/Object;
.source "AuthenticationService.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/AuthenticationService$NoAuthenticationService;,
        Lcom/squareup/server/account/AuthenticationService$AuthenticationServiceResponse;,
        Lcom/squareup/server/account/AuthenticationService$SendVerificationCodeTwoFactorStandardResponse;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000N\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0008f\u0018\u00002\u00020\u0001:\u0003\u0016\u0017\u0018J\"\u0010\u0002\u001a\u0008\u0012\u0004\u0012\u00020\u00040\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u00062\u0008\u0008\u0001\u0010\u0007\u001a\u00020\u0008H\'J\u0018\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n2\u0008\u0008\u0001\u0010\u0007\u001a\u00020\u000cH\'J\"\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u00062\u0008\u0008\u0001\u0010\u0007\u001a\u00020\u000fH\'J\u001c\u0010\u0010\u001a\u00020\u00112\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u00062\u0008\u0008\u0001\u0010\u0007\u001a\u00020\u0012H\'J\"\u0010\u0013\u001a\u0008\u0012\u0004\u0012\u00020\u00140\u00032\u0008\u0008\u0001\u0010\u0005\u001a\u00020\u00062\u0008\u0008\u0001\u0010\u0007\u001a\u00020\u0015H\'\u00a8\u0006\u0019"
    }
    d2 = {
        "Lcom/squareup/server/account/AuthenticationService;",
        "",
        "enrollTwoFactor",
        "Lcom/squareup/server/account/AuthenticationService$AuthenticationServiceResponse;",
        "Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;",
        "sessionHeader",
        "",
        "request",
        "Lcom/squareup/protos/register/api/EnrollTwoFactorRequest;",
        "login",
        "Lcom/squareup/server/AcceptedResponse;",
        "Lcom/squareup/protos/register/api/LoginResponse;",
        "Lcom/squareup/protos/register/api/LoginRequest;",
        "selectUnit",
        "Lcom/squareup/protos/register/api/SelectUnitResponse;",
        "Lcom/squareup/protos/register/api/SelectUnitRequest;",
        "sendVerificationCodeTwoFactor",
        "Lcom/squareup/server/account/AuthenticationService$SendVerificationCodeTwoFactorStandardResponse;",
        "Lcom/squareup/protos/register/api/SendVerificationCodeTwoFactorRequest;",
        "upgradeSessionTwoFactor",
        "Lcom/squareup/protos/register/api/UpgradeSessionTwoFactorResponse;",
        "Lcom/squareup/protos/register/api/UpgradeSessionTwoFactorRequest;",
        "AuthenticationServiceResponse",
        "NoAuthenticationService",
        "SendVerificationCodeTwoFactorStandardResponse",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract enrollTwoFactor(Ljava/lang/String;Lcom/squareup/protos/register/api/EnrollTwoFactorRequest;)Lcom/squareup/server/account/AuthenticationService$AuthenticationServiceResponse;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Header;
            value = "Authorization"
        .end annotation
    .end param
    .param p2    # Lcom/squareup/protos/register/api/EnrollTwoFactorRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/register/api/EnrollTwoFactorRequest;",
            ")",
            "Lcom/squareup/server/account/AuthenticationService$AuthenticationServiceResponse<",
            "Lcom/squareup/protos/register/api/EnrollTwoFactorResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/services/squareup.register.api.AuthService/EnrollTwoFactor"
    .end annotation
.end method

.method public abstract login(Lcom/squareup/protos/register/api/LoginRequest;)Lcom/squareup/server/AcceptedResponse;
    .param p1    # Lcom/squareup/protos/register/api/LoginRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/register/api/LoginRequest;",
            ")",
            "Lcom/squareup/server/AcceptedResponse<",
            "Lcom/squareup/protos/register/api/LoginResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/services/squareup.register.api.AuthService/Login"
    .end annotation
.end method

.method public abstract selectUnit(Ljava/lang/String;Lcom/squareup/protos/register/api/SelectUnitRequest;)Lcom/squareup/server/account/AuthenticationService$AuthenticationServiceResponse;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Header;
            value = "Authorization"
        .end annotation
    .end param
    .param p2    # Lcom/squareup/protos/register/api/SelectUnitRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/register/api/SelectUnitRequest;",
            ")",
            "Lcom/squareup/server/account/AuthenticationService$AuthenticationServiceResponse<",
            "Lcom/squareup/protos/register/api/SelectUnitResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/services/squareup.register.api.AuthService/SelectUnit"
    .end annotation
.end method

.method public abstract sendVerificationCodeTwoFactor(Ljava/lang/String;Lcom/squareup/protos/register/api/SendVerificationCodeTwoFactorRequest;)Lcom/squareup/server/account/AuthenticationService$SendVerificationCodeTwoFactorStandardResponse;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Header;
            value = "Authorization"
        .end annotation
    .end param
    .param p2    # Lcom/squareup/protos/register/api/SendVerificationCodeTwoFactorRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation runtime Lretrofit2/http/POST;
        value = "/services/squareup.register.api.AuthService/SendVerificationCodeTwoFactor"
    .end annotation
.end method

.method public abstract upgradeSessionTwoFactor(Ljava/lang/String;Lcom/squareup/protos/register/api/UpgradeSessionTwoFactorRequest;)Lcom/squareup/server/account/AuthenticationService$AuthenticationServiceResponse;
    .param p1    # Ljava/lang/String;
        .annotation runtime Lretrofit2/http/Header;
            value = "Authorization"
        .end annotation
    .end param
    .param p2    # Lcom/squareup/protos/register/api/UpgradeSessionTwoFactorRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/register/api/UpgradeSessionTwoFactorRequest;",
            ")",
            "Lcom/squareup/server/account/AuthenticationService$AuthenticationServiceResponse<",
            "Lcom/squareup/protos/register/api/UpgradeSessionTwoFactorResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/services/squareup.register.api.AuthService/UpgradeSessionTwoFactor"
    .end annotation
.end method
