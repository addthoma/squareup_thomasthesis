.class public final Lcom/squareup/server/account/protos/ExpirationPolicy;
.super Lcom/squareup/wire/AndroidMessage;
.source "ExpirationPolicy.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/ExpirationPolicy$ProtoAdapter_ExpirationPolicy;,
        Lcom/squareup/server/account/protos/ExpirationPolicy$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/ExpirationPolicy;",
        "Lcom/squareup/server/account/protos/ExpirationPolicy$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/ExpirationPolicy;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/ExpirationPolicy;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/ExpirationPolicy;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/ExpirationPolicy;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_TYPE:Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Type;

.field private static final serialVersionUID:J


# instance fields
.field public final expiration_period:Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.ExpirationCalendarPeriod#ADAPTER"
        tag = 0x2
    .end annotation
.end field

.field public final type:Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Type;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.server.account.protos.LoyaltyPointsExpirationPolicy$Type#ADAPTER"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 30
    new-instance v0, Lcom/squareup/server/account/protos/ExpirationPolicy$ProtoAdapter_ExpirationPolicy;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/ExpirationPolicy$ProtoAdapter_ExpirationPolicy;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/ExpirationPolicy;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 32
    sget-object v0, Lcom/squareup/server/account/protos/ExpirationPolicy;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/ExpirationPolicy;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 36
    sget-object v0, Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Type;->UNKNOWN:Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Type;

    sput-object v0, Lcom/squareup/server/account/protos/ExpirationPolicy;->DEFAULT_TYPE:Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Type;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Type;Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;)V
    .locals 1

    .line 60
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/server/account/protos/ExpirationPolicy;-><init>(Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Type;Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Type;Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;Lokio/ByteString;)V
    .locals 1

    .line 65
    sget-object v0, Lcom/squareup/server/account/protos/ExpirationPolicy;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 66
    iput-object p1, p0, Lcom/squareup/server/account/protos/ExpirationPolicy;->type:Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Type;

    .line 67
    iput-object p2, p0, Lcom/squareup/server/account/protos/ExpirationPolicy;->expiration_period:Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/ExpirationPolicy$Builder;)Lcom/squareup/server/account/protos/ExpirationPolicy$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 129
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/ExpirationPolicy;->newBuilder()Lcom/squareup/server/account/protos/ExpirationPolicy$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 82
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/ExpirationPolicy;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 83
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/ExpirationPolicy;

    .line 84
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/ExpirationPolicy;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/ExpirationPolicy;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/ExpirationPolicy;->type:Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Type;

    iget-object v3, p1, Lcom/squareup/server/account/protos/ExpirationPolicy;->type:Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Type;

    .line 85
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/ExpirationPolicy;->expiration_period:Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;

    iget-object p1, p1, Lcom/squareup/server/account/protos/ExpirationPolicy;->expiration_period:Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;

    .line 86
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 91
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_2

    .line 93
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/ExpirationPolicy;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 94
    iget-object v1, p0, Lcom/squareup/server/account/protos/ExpirationPolicy;->type:Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Type;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Type;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 95
    iget-object v1, p0, Lcom/squareup/server/account/protos/ExpirationPolicy;->expiration_period:Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 96
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/ExpirationPolicy$Builder;
    .locals 2

    .line 72
    new-instance v0, Lcom/squareup/server/account/protos/ExpirationPolicy$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/ExpirationPolicy$Builder;-><init>()V

    .line 73
    iget-object v1, p0, Lcom/squareup/server/account/protos/ExpirationPolicy;->type:Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Type;

    iput-object v1, v0, Lcom/squareup/server/account/protos/ExpirationPolicy$Builder;->type:Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Type;

    .line 74
    iget-object v1, p0, Lcom/squareup/server/account/protos/ExpirationPolicy;->expiration_period:Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;

    iput-object v1, v0, Lcom/squareup/server/account/protos/ExpirationPolicy$Builder;->expiration_period:Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;

    .line 75
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/ExpirationPolicy;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/ExpirationPolicy$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 29
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/ExpirationPolicy;->newBuilder()Lcom/squareup/server/account/protos/ExpirationPolicy$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/ExpirationPolicy;)Lcom/squareup/server/account/protos/ExpirationPolicy;
    .locals 2

    .line 123
    iget-object v0, p1, Lcom/squareup/server/account/protos/ExpirationPolicy;->type:Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Type;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/ExpirationPolicy;->requireBuilder(Lcom/squareup/server/account/protos/ExpirationPolicy$Builder;)Lcom/squareup/server/account/protos/ExpirationPolicy$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/ExpirationPolicy;->type:Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Type;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/ExpirationPolicy$Builder;->type(Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Type;)Lcom/squareup/server/account/protos/ExpirationPolicy$Builder;

    move-result-object v1

    .line 124
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/ExpirationPolicy;->expiration_period:Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/ExpirationPolicy;->requireBuilder(Lcom/squareup/server/account/protos/ExpirationPolicy$Builder;)Lcom/squareup/server/account/protos/ExpirationPolicy$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/ExpirationPolicy;->expiration_period:Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/ExpirationPolicy$Builder;->expiration_period(Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;)Lcom/squareup/server/account/protos/ExpirationPolicy$Builder;

    move-result-object v1

    :cond_1
    if-nez v1, :cond_2

    move-object p1, p0

    goto :goto_0

    .line 125
    :cond_2
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/ExpirationPolicy$Builder;->build()Lcom/squareup/server/account/protos/ExpirationPolicy;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 29
    check-cast p1, Lcom/squareup/server/account/protos/ExpirationPolicy;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/ExpirationPolicy;->overlay(Lcom/squareup/server/account/protos/ExpirationPolicy;)Lcom/squareup/server/account/protos/ExpirationPolicy;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/ExpirationPolicy;
    .locals 3

    .line 112
    iget-object v0, p0, Lcom/squareup/server/account/protos/ExpirationPolicy;->type:Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Type;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/ExpirationPolicy;->requireBuilder(Lcom/squareup/server/account/protos/ExpirationPolicy$Builder;)Lcom/squareup/server/account/protos/ExpirationPolicy$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/ExpirationPolicy;->DEFAULT_TYPE:Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Type;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/ExpirationPolicy$Builder;->type(Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Type;)Lcom/squareup/server/account/protos/ExpirationPolicy$Builder;

    move-result-object v1

    .line 113
    :cond_0
    iget-object v0, p0, Lcom/squareup/server/account/protos/ExpirationPolicy;->expiration_period:Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;

    if-eqz v0, :cond_1

    .line 114
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;->populateDefaults()Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;

    move-result-object v0

    .line 115
    iget-object v2, p0, Lcom/squareup/server/account/protos/ExpirationPolicy;->expiration_period:Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;

    if-eq v0, v2, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/ExpirationPolicy;->requireBuilder(Lcom/squareup/server/account/protos/ExpirationPolicy$Builder;)Lcom/squareup/server/account/protos/ExpirationPolicy$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/server/account/protos/ExpirationPolicy$Builder;->expiration_period(Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;)Lcom/squareup/server/account/protos/ExpirationPolicy$Builder;

    move-result-object v1

    :cond_1
    if-nez v1, :cond_2

    move-object v0, p0

    goto :goto_0

    .line 117
    :cond_2
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/ExpirationPolicy$Builder;->build()Lcom/squareup/server/account/protos/ExpirationPolicy;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 29
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/ExpirationPolicy;->populateDefaults()Lcom/squareup/server/account/protos/ExpirationPolicy;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 103
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 104
    iget-object v1, p0, Lcom/squareup/server/account/protos/ExpirationPolicy;->type:Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Type;

    if-eqz v1, :cond_0

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/ExpirationPolicy;->type:Lcom/squareup/server/account/protos/LoyaltyPointsExpirationPolicy$Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 105
    :cond_0
    iget-object v1, p0, Lcom/squareup/server/account/protos/ExpirationPolicy;->expiration_period:Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;

    if-eqz v1, :cond_1

    const-string v1, ", expiration_period="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/ExpirationPolicy;->expiration_period:Lcom/squareup/server/account/protos/ExpirationCalendarPeriod;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "ExpirationPolicy{"

    .line 106
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
