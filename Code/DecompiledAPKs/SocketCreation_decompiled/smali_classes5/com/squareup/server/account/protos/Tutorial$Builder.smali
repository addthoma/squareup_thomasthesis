.class public final Lcom/squareup/server/account/protos/Tutorial$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "Tutorial.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/Tutorial;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/Tutorial;",
        "Lcom/squareup/server/account/protos/Tutorial$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public show_item_tutorial:Ljava/lang/Boolean;

.field public show_payment_tutorial:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 128
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/server/account/protos/Tutorial;
    .locals 4

    .line 143
    new-instance v0, Lcom/squareup/server/account/protos/Tutorial;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Tutorial$Builder;->show_payment_tutorial:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/squareup/server/account/protos/Tutorial$Builder;->show_item_tutorial:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/server/account/protos/Tutorial;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 123
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/Tutorial$Builder;->build()Lcom/squareup/server/account/protos/Tutorial;

    move-result-object v0

    return-object v0
.end method

.method public show_item_tutorial(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Tutorial$Builder;
    .locals 0

    .line 137
    iput-object p1, p0, Lcom/squareup/server/account/protos/Tutorial$Builder;->show_item_tutorial:Ljava/lang/Boolean;

    return-object p0
.end method

.method public show_payment_tutorial(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Tutorial$Builder;
    .locals 0

    .line 132
    iput-object p1, p0, Lcom/squareup/server/account/protos/Tutorial$Builder;->show_payment_tutorial:Ljava/lang/Boolean;

    return-object p0
.end method
