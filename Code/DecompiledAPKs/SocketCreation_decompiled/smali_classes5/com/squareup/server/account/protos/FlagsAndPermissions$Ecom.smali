.class public final Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;
.super Lcom/squareup/wire/AndroidMessage;
.source "FlagsAndPermissions.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Ecom"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$ProtoAdapter_Ecom;,
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ENABLE_BUY_LINKS_SILENT_AUTH:Ljava/lang/Boolean;

.field public static final DEFAULT_ENABLE_ONLINE_CHECKOUT_SETTINGS_DONATIONS:Ljava/lang/Boolean;

.field public static final DEFAULT_ENABLE_PAY_LINKS_ON_CHECKOUT:Ljava/lang/Boolean;

.field public static final DEFAULT_SHOW_BUY_BUTTON:Ljava/lang/Boolean;

.field public static final DEFAULT_SHOW_CHECKOUT_LINKS_SETTINGS:Ljava/lang/Boolean;

.field public static final DEFAULT_SHOW_ONLINE_CHECKOUT_SETTINGS_V2:Ljava/lang/Boolean;

.field public static final DEFAULT_SHOW_PAY_ONLINE_TENDER_OPTION:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final enable_buy_links_silent_auth:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x4
    .end annotation
.end field

.field public final enable_online_checkout_settings_donations:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x7
    .end annotation
.end field

.field public final enable_pay_links_on_checkout:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x5
    .end annotation
.end field

.field public final show_buy_button:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1
    .end annotation
.end field

.field public final show_checkout_links_settings:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2
    .end annotation
.end field

.field public final show_online_checkout_settings_v2:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x6
    .end annotation
.end field

.field public final show_pay_online_tender_option:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 18711
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$ProtoAdapter_Ecom;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$ProtoAdapter_Ecom;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 18713
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->CREATOR:Landroid/os/Parcelable$Creator;

    const/4 v0, 0x0

    .line 18717
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->DEFAULT_SHOW_BUY_BUTTON:Ljava/lang/Boolean;

    .line 18719
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->DEFAULT_SHOW_CHECKOUT_LINKS_SETTINGS:Ljava/lang/Boolean;

    .line 18721
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->DEFAULT_SHOW_PAY_ONLINE_TENDER_OPTION:Ljava/lang/Boolean;

    .line 18723
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->DEFAULT_ENABLE_BUY_LINKS_SILENT_AUTH:Ljava/lang/Boolean;

    .line 18725
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->DEFAULT_ENABLE_PAY_LINKS_ON_CHECKOUT:Ljava/lang/Boolean;

    .line 18727
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->DEFAULT_SHOW_ONLINE_CHECKOUT_SETTINGS_V2:Ljava/lang/Boolean;

    .line 18729
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->DEFAULT_ENABLE_ONLINE_CHECKOUT_SETTINGS_DONATIONS:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 9

    .line 18786
    sget-object v8, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1

    .line 18795
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p8}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 18796
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->show_buy_button:Ljava/lang/Boolean;

    .line 18797
    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->show_checkout_links_settings:Ljava/lang/Boolean;

    .line 18798
    iput-object p3, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->show_pay_online_tender_option:Ljava/lang/Boolean;

    .line 18799
    iput-object p4, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->enable_buy_links_silent_auth:Ljava/lang/Boolean;

    .line 18800
    iput-object p5, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->enable_pay_links_on_checkout:Ljava/lang/Boolean;

    .line 18801
    iput-object p6, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->show_online_checkout_settings_v2:Ljava/lang/Boolean;

    .line 18802
    iput-object p7, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->enable_online_checkout_settings_donations:Ljava/lang/Boolean;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 18891
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 18822
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 18823
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;

    .line 18824
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->show_buy_button:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->show_buy_button:Ljava/lang/Boolean;

    .line 18825
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->show_checkout_links_settings:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->show_checkout_links_settings:Ljava/lang/Boolean;

    .line 18826
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->show_pay_online_tender_option:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->show_pay_online_tender_option:Ljava/lang/Boolean;

    .line 18827
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->enable_buy_links_silent_auth:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->enable_buy_links_silent_auth:Ljava/lang/Boolean;

    .line 18828
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->enable_pay_links_on_checkout:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->enable_pay_links_on_checkout:Ljava/lang/Boolean;

    .line 18829
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->show_online_checkout_settings_v2:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->show_online_checkout_settings_v2:Ljava/lang/Boolean;

    .line 18830
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->enable_online_checkout_settings_donations:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->enable_online_checkout_settings_donations:Ljava/lang/Boolean;

    .line 18831
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 18836
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_7

    .line 18838
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 18839
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->show_buy_button:Ljava/lang/Boolean;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 18840
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->show_checkout_links_settings:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 18841
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->show_pay_online_tender_option:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 18842
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->enable_buy_links_silent_auth:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 18843
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->enable_pay_links_on_checkout:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 18844
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->show_online_checkout_settings_v2:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 18845
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->enable_online_checkout_settings_donations:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_6
    add-int/2addr v0, v2

    .line 18846
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_7
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;
    .locals 2

    .line 18807
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;-><init>()V

    .line 18808
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->show_buy_button:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;->show_buy_button:Ljava/lang/Boolean;

    .line 18809
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->show_checkout_links_settings:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;->show_checkout_links_settings:Ljava/lang/Boolean;

    .line 18810
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->show_pay_online_tender_option:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;->show_pay_online_tender_option:Ljava/lang/Boolean;

    .line 18811
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->enable_buy_links_silent_auth:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;->enable_buy_links_silent_auth:Ljava/lang/Boolean;

    .line 18812
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->enable_pay_links_on_checkout:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;->enable_pay_links_on_checkout:Ljava/lang/Boolean;

    .line 18813
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->show_online_checkout_settings_v2:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;->show_online_checkout_settings_v2:Ljava/lang/Boolean;

    .line 18814
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->enable_online_checkout_settings_donations:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;->enable_online_checkout_settings_donations:Ljava/lang/Boolean;

    .line 18815
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 18710
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;
    .locals 2

    .line 18880
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->show_buy_button:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->show_buy_button:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;->show_buy_button(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;

    move-result-object v1

    .line 18881
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->show_checkout_links_settings:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->show_checkout_links_settings:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;->show_checkout_links_settings(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;

    move-result-object v1

    .line 18882
    :cond_1
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->show_pay_online_tender_option:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->show_pay_online_tender_option:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;->show_pay_online_tender_option(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;

    move-result-object v1

    .line 18883
    :cond_2
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->enable_buy_links_silent_auth:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->enable_buy_links_silent_auth:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;->enable_buy_links_silent_auth(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;

    move-result-object v1

    .line 18884
    :cond_3
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->enable_pay_links_on_checkout:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->enable_pay_links_on_checkout:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;->enable_pay_links_on_checkout(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;

    move-result-object v1

    .line 18885
    :cond_4
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->show_online_checkout_settings_v2:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->show_online_checkout_settings_v2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;->show_online_checkout_settings_v2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;

    move-result-object v1

    .line 18886
    :cond_5
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->enable_online_checkout_settings_donations:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->enable_online_checkout_settings_donations:Ljava/lang/Boolean;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;->enable_online_checkout_settings_donations(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;

    move-result-object v1

    :cond_6
    if-nez v1, :cond_7

    move-object p1, p0

    goto :goto_0

    .line 18887
    :cond_7
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 18710
    check-cast p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->overlay(Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;
    .locals 2

    .line 18867
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->show_buy_button:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->DEFAULT_SHOW_BUY_BUTTON:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;->show_buy_button(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;

    move-result-object v1

    .line 18868
    :cond_0
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->show_checkout_links_settings:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->DEFAULT_SHOW_CHECKOUT_LINKS_SETTINGS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;->show_checkout_links_settings(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;

    move-result-object v1

    .line 18869
    :cond_1
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->show_pay_online_tender_option:Ljava/lang/Boolean;

    if-nez v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->DEFAULT_SHOW_PAY_ONLINE_TENDER_OPTION:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;->show_pay_online_tender_option(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;

    move-result-object v1

    .line 18870
    :cond_2
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->enable_buy_links_silent_auth:Ljava/lang/Boolean;

    if-nez v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->DEFAULT_ENABLE_BUY_LINKS_SILENT_AUTH:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;->enable_buy_links_silent_auth(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;

    move-result-object v1

    .line 18871
    :cond_3
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->enable_pay_links_on_checkout:Ljava/lang/Boolean;

    if-nez v0, :cond_4

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->DEFAULT_ENABLE_PAY_LINKS_ON_CHECKOUT:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;->enable_pay_links_on_checkout(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;

    move-result-object v1

    .line 18872
    :cond_4
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->show_online_checkout_settings_v2:Ljava/lang/Boolean;

    if-nez v0, :cond_5

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->DEFAULT_SHOW_ONLINE_CHECKOUT_SETTINGS_V2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;->show_online_checkout_settings_v2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;

    move-result-object v1

    .line 18873
    :cond_5
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->enable_online_checkout_settings_donations:Ljava/lang/Boolean;

    if-nez v0, :cond_6

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->DEFAULT_ENABLE_ONLINE_CHECKOUT_SETTINGS_DONATIONS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;->enable_online_checkout_settings_donations(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;

    move-result-object v1

    :cond_6
    if-nez v1, :cond_7

    move-object v0, p0

    goto :goto_0

    .line 18874
    :cond_7
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 18710
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 18853
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 18854
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->show_buy_button:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const-string v1, ", show_buy_button="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->show_buy_button:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 18855
    :cond_0
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->show_checkout_links_settings:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    const-string v1, ", show_checkout_links_settings="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->show_checkout_links_settings:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 18856
    :cond_1
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->show_pay_online_tender_option:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", show_pay_online_tender_option="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->show_pay_online_tender_option:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 18857
    :cond_2
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->enable_buy_links_silent_auth:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    const-string v1, ", enable_buy_links_silent_auth="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->enable_buy_links_silent_auth:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 18858
    :cond_3
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->enable_pay_links_on_checkout:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    const-string v1, ", enable_pay_links_on_checkout="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->enable_pay_links_on_checkout:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 18859
    :cond_4
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->show_online_checkout_settings_v2:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    const-string v1, ", show_online_checkout_settings_v2="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->show_online_checkout_settings_v2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 18860
    :cond_5
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->enable_online_checkout_settings_donations:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    const-string v1, ", enable_online_checkout_settings_donations="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Ecom;->enable_online_checkout_settings_donations:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_6
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Ecom{"

    .line 18861
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
