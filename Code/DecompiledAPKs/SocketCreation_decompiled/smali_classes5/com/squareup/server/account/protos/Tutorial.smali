.class public final Lcom/squareup/server/account/protos/Tutorial;
.super Lcom/squareup/wire/AndroidMessage;
.source "Tutorial.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/Tutorial$ProtoAdapter_Tutorial;,
        Lcom/squareup/server/account/protos/Tutorial$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/Tutorial;",
        "Lcom/squareup/server/account/protos/Tutorial$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/Tutorial;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/Tutorial;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/Tutorial;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/Tutorial;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_SHOW_ITEM_TUTORIAL:Ljava/lang/Boolean;

.field public static final DEFAULT_SHOW_PAYMENT_TUTORIAL:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final show_item_tutorial:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2
    .end annotation
.end field

.field public final show_payment_tutorial:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 29
    new-instance v0, Lcom/squareup/server/account/protos/Tutorial$ProtoAdapter_Tutorial;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/Tutorial$ProtoAdapter_Tutorial;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/Tutorial;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 31
    sget-object v0, Lcom/squareup/server/account/protos/Tutorial;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/Tutorial;->CREATOR:Landroid/os/Parcelable$Creator;

    const/4 v0, 0x0

    .line 35
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/Tutorial;->DEFAULT_SHOW_PAYMENT_TUTORIAL:Ljava/lang/Boolean;

    .line 37
    sput-object v0, Lcom/squareup/server/account/protos/Tutorial;->DEFAULT_SHOW_ITEM_TUTORIAL:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 1

    .line 54
    sget-object v0, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    invoke-direct {p0, p1, p2, v0}, Lcom/squareup/server/account/protos/Tutorial;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1

    .line 59
    sget-object v0, Lcom/squareup/server/account/protos/Tutorial;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p3}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 60
    iput-object p1, p0, Lcom/squareup/server/account/protos/Tutorial;->show_payment_tutorial:Ljava/lang/Boolean;

    .line 61
    iput-object p2, p0, Lcom/squareup/server/account/protos/Tutorial;->show_item_tutorial:Ljava/lang/Boolean;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/Tutorial$Builder;)Lcom/squareup/server/account/protos/Tutorial$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 120
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/Tutorial;->newBuilder()Lcom/squareup/server/account/protos/Tutorial$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 76
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/Tutorial;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 77
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/Tutorial;

    .line 78
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/Tutorial;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/Tutorial;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Tutorial;->show_payment_tutorial:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Tutorial;->show_payment_tutorial:Ljava/lang/Boolean;

    .line 79
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Tutorial;->show_item_tutorial:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/server/account/protos/Tutorial;->show_item_tutorial:Ljava/lang/Boolean;

    .line 80
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 85
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_2

    .line 87
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/Tutorial;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 88
    iget-object v1, p0, Lcom/squareup/server/account/protos/Tutorial;->show_payment_tutorial:Ljava/lang/Boolean;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 89
    iget-object v1, p0, Lcom/squareup/server/account/protos/Tutorial;->show_item_tutorial:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_1
    add-int/2addr v0, v2

    .line 90
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_2
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/Tutorial$Builder;
    .locals 2

    .line 66
    new-instance v0, Lcom/squareup/server/account/protos/Tutorial$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/Tutorial$Builder;-><init>()V

    .line 67
    iget-object v1, p0, Lcom/squareup/server/account/protos/Tutorial;->show_payment_tutorial:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Tutorial$Builder;->show_payment_tutorial:Ljava/lang/Boolean;

    .line 68
    iget-object v1, p0, Lcom/squareup/server/account/protos/Tutorial;->show_item_tutorial:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Tutorial$Builder;->show_item_tutorial:Ljava/lang/Boolean;

    .line 69
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/Tutorial;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Tutorial$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 28
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/Tutorial;->newBuilder()Lcom/squareup/server/account/protos/Tutorial$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/Tutorial;)Lcom/squareup/server/account/protos/Tutorial;
    .locals 2

    .line 114
    iget-object v0, p1, Lcom/squareup/server/account/protos/Tutorial;->show_payment_tutorial:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Tutorial;->requireBuilder(Lcom/squareup/server/account/protos/Tutorial$Builder;)Lcom/squareup/server/account/protos/Tutorial$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Tutorial;->show_payment_tutorial:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Tutorial$Builder;->show_payment_tutorial(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Tutorial$Builder;

    move-result-object v1

    .line 115
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/Tutorial;->show_item_tutorial:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Tutorial;->requireBuilder(Lcom/squareup/server/account/protos/Tutorial$Builder;)Lcom/squareup/server/account/protos/Tutorial$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/Tutorial;->show_item_tutorial:Ljava/lang/Boolean;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/Tutorial$Builder;->show_item_tutorial(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Tutorial$Builder;

    move-result-object v1

    :cond_1
    if-nez v1, :cond_2

    move-object p1, p0

    goto :goto_0

    .line 116
    :cond_2
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/Tutorial$Builder;->build()Lcom/squareup/server/account/protos/Tutorial;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 28
    check-cast p1, Lcom/squareup/server/account/protos/Tutorial;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/Tutorial;->overlay(Lcom/squareup/server/account/protos/Tutorial;)Lcom/squareup/server/account/protos/Tutorial;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/Tutorial;
    .locals 2

    .line 106
    iget-object v0, p0, Lcom/squareup/server/account/protos/Tutorial;->show_payment_tutorial:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Tutorial;->requireBuilder(Lcom/squareup/server/account/protos/Tutorial$Builder;)Lcom/squareup/server/account/protos/Tutorial$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/Tutorial;->DEFAULT_SHOW_PAYMENT_TUTORIAL:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Tutorial$Builder;->show_payment_tutorial(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Tutorial$Builder;

    move-result-object v1

    .line 107
    :cond_0
    iget-object v0, p0, Lcom/squareup/server/account/protos/Tutorial;->show_item_tutorial:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Tutorial;->requireBuilder(Lcom/squareup/server/account/protos/Tutorial$Builder;)Lcom/squareup/server/account/protos/Tutorial$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/Tutorial;->DEFAULT_SHOW_ITEM_TUTORIAL:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Tutorial$Builder;->show_item_tutorial(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Tutorial$Builder;

    move-result-object v1

    :cond_1
    if-nez v1, :cond_2

    move-object v0, p0

    goto :goto_0

    .line 108
    :cond_2
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/Tutorial$Builder;->build()Lcom/squareup/server/account/protos/Tutorial;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 28
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/Tutorial;->populateDefaults()Lcom/squareup/server/account/protos/Tutorial;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 97
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 98
    iget-object v1, p0, Lcom/squareup/server/account/protos/Tutorial;->show_payment_tutorial:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const-string v1, ", show_payment_tutorial="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Tutorial;->show_payment_tutorial:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 99
    :cond_0
    iget-object v1, p0, Lcom/squareup/server/account/protos/Tutorial;->show_item_tutorial:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    const-string v1, ", show_item_tutorial="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Tutorial;->show_item_tutorial:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Tutorial{"

    .line 100
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
