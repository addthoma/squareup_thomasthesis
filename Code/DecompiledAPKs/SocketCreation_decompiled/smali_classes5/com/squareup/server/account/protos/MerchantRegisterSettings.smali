.class public final Lcom/squareup/server/account/protos/MerchantRegisterSettings;
.super Lcom/squareup/wire/AndroidMessage;
.source "MerchantRegisterSettings.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/MerchantRegisterSettings$ProtoAdapter_MerchantRegisterSettings;,
        Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/MerchantRegisterSettings;",
        "Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/MerchantRegisterSettings;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/MerchantRegisterSettings;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/MerchantRegisterSettings;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/MerchantRegisterSettings;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_LEGACY_GUEST_MODE_EXPIRATION:Ljava/lang/String; = ""

.field public static final DEFAULT_SHOW_SHARE_TEAM_PASSCODE_WARNING:Ljava/lang/Boolean;

.field public static final DEFAULT_TEAM_PASSCODE:Ljava/lang/String; = ""

.field public static final DEFAULT_TEAM_ROLE_TOKEN:Ljava/lang/String; = ""

.field public static final DEFAULT_USE_TEAM_PERMISSIONS:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final default_register_permissions:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x1
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final legacy_guest_mode_expiration:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field

.field public final show_share_team_passcode_warning:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x6
    .end annotation
.end field

.field public final team_passcode:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final team_role_token:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final use_team_permissions:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 30
    new-instance v0, Lcom/squareup/server/account/protos/MerchantRegisterSettings$ProtoAdapter_MerchantRegisterSettings;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/MerchantRegisterSettings$ProtoAdapter_MerchantRegisterSettings;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 32
    sget-object v0, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->CREATOR:Landroid/os/Parcelable$Creator;

    const/4 v0, 0x0

    .line 36
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->DEFAULT_USE_TEAM_PERMISSIONS:Ljava/lang/Boolean;

    .line 44
    sput-object v0, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->DEFAULT_SHOW_SHARE_TEAM_PASSCODE_WARNING:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .line 95
    sget-object v7, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/server/account/protos/MerchantRegisterSettings;-><init>(Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            "Lokio/ByteString;",
            ")V"
        }
    .end annotation

    .line 102
    sget-object v0, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p7}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    const-string p7, "default_register_permissions"

    .line 103
    invoke-static {p7, p1}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->default_register_permissions:Ljava/util/List;

    .line 104
    iput-object p2, p0, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->use_team_permissions:Ljava/lang/Boolean;

    .line 105
    iput-object p3, p0, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->team_role_token:Ljava/lang/String;

    .line 106
    iput-object p4, p0, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->team_passcode:Ljava/lang/String;

    .line 107
    iput-object p5, p0, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->legacy_guest_mode_expiration:Ljava/lang/String;

    .line 108
    iput-object p6, p0, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->show_share_team_passcode_warning:Ljava/lang/Boolean;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;)Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 187
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->newBuilder()Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 127
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/MerchantRegisterSettings;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 128
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/MerchantRegisterSettings;

    .line 129
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->default_register_permissions:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->default_register_permissions:Ljava/util/List;

    .line 130
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->use_team_permissions:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->use_team_permissions:Ljava/lang/Boolean;

    .line 131
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->team_role_token:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->team_role_token:Ljava/lang/String;

    .line 132
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->team_passcode:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->team_passcode:Ljava/lang/String;

    .line 133
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->legacy_guest_mode_expiration:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->legacy_guest_mode_expiration:Ljava/lang/String;

    .line 134
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->show_share_team_passcode_warning:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->show_share_team_passcode_warning:Ljava/lang/Boolean;

    .line 135
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 140
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_5

    .line 142
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 143
    iget-object v1, p0, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->default_register_permissions:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 144
    iget-object v1, p0, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->use_team_permissions:Ljava/lang/Boolean;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 145
    iget-object v1, p0, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->team_role_token:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 146
    iget-object v1, p0, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->team_passcode:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 147
    iget-object v1, p0, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->legacy_guest_mode_expiration:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 148
    iget-object v1, p0, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->show_share_team_passcode_warning:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_4
    add-int/2addr v0, v2

    .line 149
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_5
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;
    .locals 2

    .line 113
    new-instance v0, Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;-><init>()V

    .line 114
    iget-object v1, p0, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->default_register_permissions:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;->default_register_permissions:Ljava/util/List;

    .line 115
    iget-object v1, p0, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->use_team_permissions:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;->use_team_permissions:Ljava/lang/Boolean;

    .line 116
    iget-object v1, p0, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->team_role_token:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;->team_role_token:Ljava/lang/String;

    .line 117
    iget-object v1, p0, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->team_passcode:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;->team_passcode:Ljava/lang/String;

    .line 118
    iget-object v1, p0, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->legacy_guest_mode_expiration:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;->legacy_guest_mode_expiration:Ljava/lang/String;

    .line 119
    iget-object v1, p0, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->show_share_team_passcode_warning:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;->show_share_team_passcode_warning:Ljava/lang/Boolean;

    .line 120
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 29
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->newBuilder()Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/MerchantRegisterSettings;)Lcom/squareup/server/account/protos/MerchantRegisterSettings;
    .locals 2

    .line 177
    iget-object v0, p1, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->default_register_permissions:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->requireBuilder(Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;)Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->default_register_permissions:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;->default_register_permissions(Ljava/util/List;)Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;

    move-result-object v1

    .line 178
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->use_team_permissions:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->requireBuilder(Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;)Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->use_team_permissions:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;->use_team_permissions(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;

    move-result-object v1

    .line 179
    :cond_1
    iget-object v0, p1, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->team_role_token:Ljava/lang/String;

    if-eqz v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->requireBuilder(Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;)Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->team_role_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;->team_role_token(Ljava/lang/String;)Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;

    move-result-object v1

    .line 180
    :cond_2
    iget-object v0, p1, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->team_passcode:Ljava/lang/String;

    if-eqz v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->requireBuilder(Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;)Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->team_passcode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;->team_passcode(Ljava/lang/String;)Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;

    move-result-object v1

    .line 181
    :cond_3
    iget-object v0, p1, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->legacy_guest_mode_expiration:Ljava/lang/String;

    if-eqz v0, :cond_4

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->requireBuilder(Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;)Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->legacy_guest_mode_expiration:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;->legacy_guest_mode_expiration(Ljava/lang/String;)Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;

    move-result-object v1

    .line 182
    :cond_4
    iget-object v0, p1, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->show_share_team_passcode_warning:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->requireBuilder(Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;)Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->show_share_team_passcode_warning:Ljava/lang/Boolean;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;->show_share_team_passcode_warning(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;

    move-result-object v1

    :cond_5
    if-nez v1, :cond_6

    move-object p1, p0

    goto :goto_0

    .line 183
    :cond_6
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;->build()Lcom/squareup/server/account/protos/MerchantRegisterSettings;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 29
    check-cast p1, Lcom/squareup/server/account/protos/MerchantRegisterSettings;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->overlay(Lcom/squareup/server/account/protos/MerchantRegisterSettings;)Lcom/squareup/server/account/protos/MerchantRegisterSettings;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/MerchantRegisterSettings;
    .locals 2

    .line 169
    iget-object v0, p0, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->use_team_permissions:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->requireBuilder(Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;)Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->DEFAULT_USE_TEAM_PERMISSIONS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;->use_team_permissions(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;

    move-result-object v1

    .line 170
    :cond_0
    iget-object v0, p0, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->show_share_team_passcode_warning:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->requireBuilder(Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;)Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->DEFAULT_SHOW_SHARE_TEAM_PASSCODE_WARNING:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;->show_share_team_passcode_warning(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;

    move-result-object v1

    :cond_1
    if-nez v1, :cond_2

    move-object v0, p0

    goto :goto_0

    .line 171
    :cond_2
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/MerchantRegisterSettings$Builder;->build()Lcom/squareup/server/account/protos/MerchantRegisterSettings;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 29
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->populateDefaults()Lcom/squareup/server/account/protos/MerchantRegisterSettings;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 156
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 157
    iget-object v1, p0, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->default_register_permissions:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, ", default_register_permissions="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->default_register_permissions:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 158
    :cond_0
    iget-object v1, p0, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->use_team_permissions:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    const-string v1, ", use_team_permissions="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->use_team_permissions:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 159
    :cond_1
    iget-object v1, p0, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->team_role_token:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", team_role_token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->team_role_token:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 160
    :cond_2
    iget-object v1, p0, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->team_passcode:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", team_passcode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->team_passcode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 161
    :cond_3
    iget-object v1, p0, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->legacy_guest_mode_expiration:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", legacy_guest_mode_expiration="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->legacy_guest_mode_expiration:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 162
    :cond_4
    iget-object v1, p0, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->show_share_team_passcode_warning:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    const-string v1, ", show_share_team_passcode_warning="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/MerchantRegisterSettings;->show_share_team_passcode_warning:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_5
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "MerchantRegisterSettings{"

    .line 163
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
