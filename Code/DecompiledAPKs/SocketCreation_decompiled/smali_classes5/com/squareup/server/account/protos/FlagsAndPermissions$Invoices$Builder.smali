.class public final Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FlagsAndPermissions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public allow_archive:Ljava/lang/Boolean;

.field public allow_edit_flow_v2_in_checkout:Ljava/lang/Boolean;

.field public allow_pdf_attachments:Ljava/lang/Boolean;

.field public automatic_payment_mobile:Ljava/lang/Boolean;

.field public automatic_reminders_mobile:Ljava/lang/Boolean;

.field public creation_flow_v2_widgets:Ljava/lang/Boolean;

.field public custom_send_reminder_message_mobile:Ljava/lang/Boolean;

.field public customer_invoice_linking_mobile:Ljava/lang/Boolean;

.field public default_message_android:Ljava/lang/Boolean;

.field public download_invoice:Ljava/lang/Boolean;

.field public edit_estimate_flow_v2:Ljava/lang/Boolean;

.field public edit_flow_v2:Ljava/lang/Boolean;

.field public estimate_defaults_mobile:Ljava/lang/Boolean;

.field public estimates_file_attachments:Ljava/lang/Boolean;

.field public estimates_mobile_alpha:Ljava/lang/Boolean;

.field public estimates_mobile_ga:Ljava/lang/Boolean;

.field public estimates_package_support_mobile_readonly:Ljava/lang/Boolean;

.field public file_attachment_android:Ljava/lang/Boolean;

.field public first_invoice_tutorial_android:Ljava/lang/Boolean;

.field public home_tab:Ljava/lang/Boolean;

.field public invoice_app_banner:Ljava/lang/Boolean;

.field public invoice_cof_mobile:Ljava/lang/Boolean;

.field public invoice_event_history_android:Ljava/lang/Boolean;

.field public invoice_preview_android:Ljava/lang/Boolean;

.field public invoice_share_link_android:Ljava/lang/Boolean;

.field public ipos_onboarding_merchant_customization:Ljava/lang/Boolean;

.field public ipos_rating_prompt:Ljava/lang/Boolean;

.field public mobile_analytics_android:Ljava/lang/Boolean;

.field public no_idv_experiment_rollout:Ljava/lang/Boolean;

.field public partially_pay_invoice:Ljava/lang/Boolean;

.field public payment_request_editing:Ljava/lang/Boolean;

.field public payment_schedule:Ljava/lang/Boolean;

.field public payment_schedule_reminders:Ljava/lang/Boolean;

.field public record_payment:Ljava/lang/Boolean;

.field public recurring_cancel_next:Ljava/lang/Boolean;

.field public recurring_mobile:Ljava/lang/Boolean;

.field public reduce_state_filters_mobile:Ljava/lang/Boolean;

.field public shipping_address_mobile:Ljava/lang/Boolean;

.field public timeline_cta_linking:Ljava/lang/Boolean;

.field public use_invoice_defaults_mobile:Ljava/lang/Boolean;

.field public visible_push:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 8640
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public allow_archive(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;
    .locals 0

    .line 8873
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->allow_archive:Ljava/lang/Boolean;

    return-object p0
.end method

.method public allow_edit_flow_v2_in_checkout(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;
    .locals 0

    .line 8955
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->allow_edit_flow_v2_in_checkout:Ljava/lang/Boolean;

    return-object p0
.end method

.method public allow_pdf_attachments(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;
    .locals 0

    .line 8939
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->allow_pdf_attachments:Ljava/lang/Boolean;

    return-object p0
.end method

.method public automatic_payment_mobile(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;
    .locals 0

    .line 8752
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->automatic_payment_mobile:Ljava/lang/Boolean;

    return-object p0
.end method

.method public automatic_reminders_mobile(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;
    .locals 0

    .line 8760
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->automatic_reminders_mobile:Ljava/lang/Boolean;

    return-object p0
.end method

.method public build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;
    .locals 2

    .line 8970
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;-><init>(Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 8557
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices;

    move-result-object v0

    return-object v0
.end method

.method public creation_flow_v2_widgets(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;
    .locals 0

    .line 8923
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->creation_flow_v2_widgets:Ljava/lang/Boolean;

    return-object p0
.end method

.method public custom_send_reminder_message_mobile(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;
    .locals 0

    .line 8728
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->custom_send_reminder_message_mobile:Ljava/lang/Boolean;

    return-object p0
.end method

.method public customer_invoice_linking_mobile(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;
    .locals 0

    .line 8719
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->customer_invoice_linking_mobile:Ljava/lang/Boolean;

    return-object p0
.end method

.method public default_message_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;
    .locals 0

    .line 8663
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->default_message_android:Ljava/lang/Boolean;

    return-object p0
.end method

.method public download_invoice(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;
    .locals 0

    .line 8881
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->download_invoice:Ljava/lang/Boolean;

    return-object p0
.end method

.method public edit_estimate_flow_v2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;
    .locals 0

    .line 8947
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->edit_estimate_flow_v2:Ljava/lang/Boolean;

    return-object p0
.end method

.method public edit_flow_v2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;
    .locals 0

    .line 8865
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->edit_flow_v2:Ljava/lang/Boolean;

    return-object p0
.end method

.method public estimate_defaults_mobile(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;
    .locals 0

    .line 8857
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->estimate_defaults_mobile:Ljava/lang/Boolean;

    return-object p0
.end method

.method public estimates_file_attachments(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;
    .locals 0

    .line 8825
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->estimates_file_attachments:Ljava/lang/Boolean;

    return-object p0
.end method

.method public estimates_mobile_alpha(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;
    .locals 0

    .line 8809
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->estimates_mobile_alpha:Ljava/lang/Boolean;

    return-object p0
.end method

.method public estimates_mobile_ga(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;
    .locals 0

    .line 8817
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->estimates_mobile_ga:Ljava/lang/Boolean;

    return-object p0
.end method

.method public estimates_package_support_mobile_readonly(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;
    .locals 0

    .line 8964
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->estimates_package_support_mobile_readonly:Ljava/lang/Boolean;

    return-object p0
.end method

.method public file_attachment_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;
    .locals 0

    .line 8687
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->file_attachment_android:Ljava/lang/Boolean;

    return-object p0
.end method

.method public first_invoice_tutorial_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;
    .locals 0

    .line 8679
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->first_invoice_tutorial_android:Ljava/lang/Boolean;

    return-object p0
.end method

.method public home_tab(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;
    .locals 0

    .line 8889
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->home_tab:Ljava/lang/Boolean;

    return-object p0
.end method

.method public invoice_app_banner(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;
    .locals 0

    .line 8849
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->invoice_app_banner:Ljava/lang/Boolean;

    return-object p0
.end method

.method public invoice_cof_mobile(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;
    .locals 0

    .line 8736
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->invoice_cof_mobile:Ljava/lang/Boolean;

    return-object p0
.end method

.method public invoice_event_history_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;
    .locals 0

    .line 8671
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->invoice_event_history_android:Ljava/lang/Boolean;

    return-object p0
.end method

.method public invoice_preview_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;
    .locals 0

    .line 8647
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->invoice_preview_android:Ljava/lang/Boolean;

    return-object p0
.end method

.method public invoice_share_link_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;
    .locals 0

    .line 8655
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->invoice_share_link_android:Ljava/lang/Boolean;

    return-object p0
.end method

.method public ipos_onboarding_merchant_customization(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;
    .locals 0

    .line 8777
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->ipos_onboarding_merchant_customization:Ljava/lang/Boolean;

    return-object p0
.end method

.method public ipos_rating_prompt(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;
    .locals 0

    .line 8905
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->ipos_rating_prompt:Ljava/lang/Boolean;

    return-object p0
.end method

.method public mobile_analytics_android(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;
    .locals 0

    .line 8695
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->mobile_analytics_android:Ljava/lang/Boolean;

    return-object p0
.end method

.method public no_idv_experiment_rollout(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;
    .locals 0

    .line 8918
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->no_idv_experiment_rollout:Ljava/lang/Boolean;

    return-object p0
.end method

.method public partially_pay_invoice(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;
    .locals 0

    .line 8913
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->partially_pay_invoice:Ljava/lang/Boolean;

    return-object p0
.end method

.method public payment_request_editing(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;
    .locals 0

    .line 8793
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->payment_request_editing:Ljava/lang/Boolean;

    return-object p0
.end method

.method public payment_schedule(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;
    .locals 0

    .line 8897
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->payment_schedule:Ljava/lang/Boolean;

    return-object p0
.end method

.method public payment_schedule_reminders(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;
    .locals 0

    .line 8931
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->payment_schedule_reminders:Ljava/lang/Boolean;

    return-object p0
.end method

.method public record_payment(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;
    .locals 0

    .line 8801
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->record_payment:Ljava/lang/Boolean;

    return-object p0
.end method

.method public recurring_cancel_next(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;
    .locals 0

    .line 8768
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->recurring_cancel_next:Ljava/lang/Boolean;

    return-object p0
.end method

.method public recurring_mobile(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;
    .locals 0

    .line 8703
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->recurring_mobile:Ljava/lang/Boolean;

    return-object p0
.end method

.method public reduce_state_filters_mobile(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;
    .locals 0

    .line 8744
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->reduce_state_filters_mobile:Ljava/lang/Boolean;

    return-object p0
.end method

.method public shipping_address_mobile(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;
    .locals 0

    .line 8711
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->shipping_address_mobile:Ljava/lang/Boolean;

    return-object p0
.end method

.method public timeline_cta_linking(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;
    .locals 0

    .line 8833
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->timeline_cta_linking:Ljava/lang/Boolean;

    return-object p0
.end method

.method public use_invoice_defaults_mobile(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;
    .locals 0

    .line 8785
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->use_invoice_defaults_mobile:Ljava/lang/Boolean;

    return-object p0
.end method

.method public visible_push(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;
    .locals 0

    .line 8841
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Invoices$Builder;->visible_push:Ljava/lang/Boolean;

    return-object p0
.end method
