.class final Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails$ProtoAdapter_CardDetails;
.super Lcom/squareup/wire/ProtoAdapter;
.source "InstantDeposits.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_CardDetails"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 448
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 469
    new-instance v0, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails$Builder;-><init>()V

    .line 470
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 471
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_2

    const/4 v4, 0x2

    if-eq v3, v4, :cond_1

    const/4 v4, 0x3

    if-eq v3, v4, :cond_0

    .line 491
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 489
    :cond_0
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails$Builder;->pan_suffix(Ljava/lang/String;)Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails$Builder;

    goto :goto_0

    .line 483
    :cond_1
    :try_start_0
    sget-object v4, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    invoke-virtual {v0, v4}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails$Builder;->brand(Lcom/squareup/protos/client/bills/CardTender$Card$Brand;)Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails$Builder;
    :try_end_0
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    .line 485
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 475
    :cond_2
    :try_start_1
    sget-object v4, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    invoke-virtual {v0, v4}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails$Builder;->entry_method(Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;)Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails$Builder;
    :try_end_1
    .catch Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v4

    .line 477
    sget-object v5, Lcom/squareup/wire/FieldEncoding;->VARINT:Lcom/squareup/wire/FieldEncoding;

    iget v4, v4, Lcom/squareup/wire/ProtoAdapter$EnumConstantNotFoundException;->value:I

    int-to-long v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v3, v5, v4}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails$Builder;->addUnknownField(ILcom/squareup/wire/FieldEncoding;Ljava/lang/Object;)Lcom/squareup/wire/Message$Builder;

    goto :goto_0

    .line 495
    :cond_3
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 496
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails$Builder;->build()Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 446
    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails$ProtoAdapter_CardDetails;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 461
    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 462
    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 463
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;->pan_suffix:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 464
    invoke-virtual {p2}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 446
    check-cast p2, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails$ProtoAdapter_CardDetails;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;)I
    .locals 4

    .line 453
    sget-object v0, Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;->entry_method:Lcom/squareup/protos/client/bills/CardTender$Card$EntryMethod;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/protos/client/bills/CardTender$Card$Brand;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;->brand:Lcom/squareup/protos/client/bills/CardTender$Card$Brand;

    const/4 v3, 0x2

    .line 454
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->STRING:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;->pan_suffix:Ljava/lang/String;

    const/4 v3, 0x3

    .line 455
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 456
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 446
    check-cast p1, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails$ProtoAdapter_CardDetails;->encodedSize(Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;)Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;
    .locals 0

    .line 501
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;->newBuilder()Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails$Builder;

    move-result-object p1

    .line 502
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 503
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails$Builder;->build()Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 446
    check-cast p1, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails$ProtoAdapter_CardDetails;->redact(Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;)Lcom/squareup/server/account/protos/InstantDeposits$LinkedCard$CardDetails;

    move-result-object p1

    return-object p1
.end method
