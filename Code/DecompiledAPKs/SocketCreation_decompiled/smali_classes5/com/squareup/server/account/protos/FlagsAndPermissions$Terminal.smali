.class public final Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;
.super Lcom/squareup/wire/AndroidMessage;
.source "FlagsAndPermissions.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Terminal"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$ProtoAdapter_Terminal;,
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_APP_CAN_ORDER_R12:Ljava/lang/Boolean;

.field public static final DEFAULT_APP_CAN_ORDER_R4:Ljava/lang/Boolean;

.field public static final DEFAULT_APP_CAN_ORDER_R41:Ljava/lang/Boolean;

.field public static final DEFAULT_APP_CAN_SHOW_ACCOUNT_FREEZE:Ljava/lang/Boolean;

.field public static final DEFAULT_APP_CAN_SHOW_ACCOUNT_FREEZE_BANNER:Ljava/lang/Boolean;

.field public static final DEFAULT_APP_CAN_SHOW_ACCOUNT_FREEZE_NOTIFICATIONS:Ljava/lang/Boolean;

.field public static final DEFAULT_APP_CAN_SHOW_CNP_FEES:Ljava/lang/Boolean;

.field public static final DEFAULT_APP_CAN_SHOW_CP_PRICING_CHANGE:Ljava/lang/Boolean;

.field public static final DEFAULT_APP_CAN_SHOW_DISPUTES:Ljava/lang/Boolean;

.field public static final DEFAULT_APP_CAN_SHOW_DISPUTES_CHALLENGE_FLOW:Ljava/lang/Boolean;

.field public static final DEFAULT_APP_CAN_SHOW_ORDER_READER_MENU:Ljava/lang/Boolean;

.field public static final DEFAULT_APP_COLLECT_CNP_POSTAL_CODE:Ljava/lang/Boolean;

.field public static final DEFAULT_APP_COLLECT_COF_POSTAL_CODE:Ljava/lang/Boolean;

.field public static final DEFAULT_APP_CONNECTED_TERMINAL:Ljava/lang/Boolean;

.field public static final DEFAULT_APP_DIP_COF_POST_PAYMENT:Ljava/lang/Boolean;

.field public static final DEFAULT_APP_FEE_TUTORIAL:Ljava/lang/Boolean;

.field public static final DEFAULT_APP_FIRST_PAYMENT_TUTORIAL_V2:Ljava/lang/Boolean;

.field public static final DEFAULT_APP_TERMINAL_API_SPM:Ljava/lang/Boolean;

.field public static final DEFAULT_APP_X2_DIP_CARD_FOR_CUSTOMER_COF:Ljava/lang/Boolean;

.field public static final DEFAULT_CAN_SHOW_FEES:Ljava/lang/Boolean;

.field public static final DEFAULT_DIP_CARD_FOR_CUSTOMER_COF:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final app_can_order_r12:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x7
    .end annotation
.end field

.field public final app_can_order_r4:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x8
    .end annotation
.end field

.field public final app_can_order_r41:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x9
    .end annotation
.end field

.field public final app_can_show_account_freeze:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x11
    .end annotation
.end field

.field public final app_can_show_account_freeze_banner:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x13
    .end annotation
.end field

.field public final app_can_show_account_freeze_notifications:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x12
    .end annotation
.end field

.field public final app_can_show_cnp_fees:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1
    .end annotation
.end field

.field public final app_can_show_cp_pricing_change:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x14
    .end annotation
.end field

.field public final app_can_show_disputes:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xc
    .end annotation
.end field

.field public final app_can_show_disputes_challenge_flow:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xd
    .end annotation
.end field

.field public final app_can_show_order_reader_menu:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x6
    .end annotation
.end field

.field public final app_collect_cnp_postal_code:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x10
    .end annotation
.end field

.field public final app_collect_cof_postal_code:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xf
    .end annotation
.end field

.field public final app_connected_terminal:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xe
    .end annotation
.end field

.field public final app_dip_cof_post_payment:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x5
    .end annotation
.end field

.field public final app_fee_tutorial:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2
    .end annotation
.end field

.field public final app_first_payment_tutorial_v2:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xb
    .end annotation
.end field

.field public final app_terminal_api_spm:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x15
    .end annotation
.end field

.field public final app_x2_dip_card_for_customer_cof:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xa
    .end annotation
.end field

.field public final can_show_fees:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3
    .end annotation
.end field

.field public final dip_card_for_customer_cof:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x4
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 12023
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$ProtoAdapter_Terminal;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$ProtoAdapter_Terminal;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 12025
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->CREATOR:Landroid/os/Parcelable$Creator;

    const/4 v0, 0x0

    .line 12029
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->DEFAULT_APP_CAN_SHOW_CNP_FEES:Ljava/lang/Boolean;

    .line 12031
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->DEFAULT_APP_FEE_TUTORIAL:Ljava/lang/Boolean;

    .line 12033
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->DEFAULT_CAN_SHOW_FEES:Ljava/lang/Boolean;

    .line 12035
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->DEFAULT_DIP_CARD_FOR_CUSTOMER_COF:Ljava/lang/Boolean;

    .line 12037
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->DEFAULT_APP_DIP_COF_POST_PAYMENT:Ljava/lang/Boolean;

    .line 12039
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->DEFAULT_APP_CAN_SHOW_ORDER_READER_MENU:Ljava/lang/Boolean;

    .line 12041
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->DEFAULT_APP_CAN_ORDER_R12:Ljava/lang/Boolean;

    .line 12043
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->DEFAULT_APP_CAN_ORDER_R4:Ljava/lang/Boolean;

    .line 12045
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->DEFAULT_APP_CAN_ORDER_R41:Ljava/lang/Boolean;

    .line 12047
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->DEFAULT_APP_X2_DIP_CARD_FOR_CUSTOMER_COF:Ljava/lang/Boolean;

    .line 12049
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->DEFAULT_APP_FIRST_PAYMENT_TUTORIAL_V2:Ljava/lang/Boolean;

    .line 12051
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->DEFAULT_APP_CAN_SHOW_DISPUTES:Ljava/lang/Boolean;

    .line 12053
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->DEFAULT_APP_CAN_SHOW_DISPUTES_CHALLENGE_FLOW:Ljava/lang/Boolean;

    .line 12055
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->DEFAULT_APP_CONNECTED_TERMINAL:Ljava/lang/Boolean;

    .line 12057
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->DEFAULT_APP_COLLECT_COF_POSTAL_CODE:Ljava/lang/Boolean;

    .line 12059
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->DEFAULT_APP_COLLECT_CNP_POSTAL_CODE:Ljava/lang/Boolean;

    .line 12061
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->DEFAULT_APP_CAN_SHOW_ACCOUNT_FREEZE:Ljava/lang/Boolean;

    .line 12063
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->DEFAULT_APP_CAN_SHOW_ACCOUNT_FREEZE_NOTIFICATIONS:Ljava/lang/Boolean;

    .line 12065
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->DEFAULT_APP_CAN_SHOW_ACCOUNT_FREEZE_BANNER:Ljava/lang/Boolean;

    .line 12067
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->DEFAULT_APP_CAN_SHOW_CP_PRICING_CHANGE:Ljava/lang/Boolean;

    .line 12069
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->DEFAULT_APP_TERMINAL_API_SPM:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;Lokio/ByteString;)V
    .locals 1

    .line 12253
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 12254
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_can_show_cnp_fees:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_cnp_fees:Ljava/lang/Boolean;

    .line 12255
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_fee_tutorial:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_fee_tutorial:Ljava/lang/Boolean;

    .line 12256
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->can_show_fees:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->can_show_fees:Ljava/lang/Boolean;

    .line 12257
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->dip_card_for_customer_cof:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->dip_card_for_customer_cof:Ljava/lang/Boolean;

    .line 12258
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_dip_cof_post_payment:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_dip_cof_post_payment:Ljava/lang/Boolean;

    .line 12259
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_can_show_order_reader_menu:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_order_reader_menu:Ljava/lang/Boolean;

    .line 12260
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_can_order_r12:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_order_r12:Ljava/lang/Boolean;

    .line 12261
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_can_order_r4:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_order_r4:Ljava/lang/Boolean;

    .line 12262
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_can_order_r41:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_order_r41:Ljava/lang/Boolean;

    .line 12263
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_x2_dip_card_for_customer_cof:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_x2_dip_card_for_customer_cof:Ljava/lang/Boolean;

    .line 12264
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_first_payment_tutorial_v2:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_first_payment_tutorial_v2:Ljava/lang/Boolean;

    .line 12265
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_can_show_disputes:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_disputes:Ljava/lang/Boolean;

    .line 12266
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_can_show_disputes_challenge_flow:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_disputes_challenge_flow:Ljava/lang/Boolean;

    .line 12267
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_connected_terminal:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_connected_terminal:Ljava/lang/Boolean;

    .line 12268
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_collect_cof_postal_code:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_collect_cof_postal_code:Ljava/lang/Boolean;

    .line 12269
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_collect_cnp_postal_code:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_collect_cnp_postal_code:Ljava/lang/Boolean;

    .line 12270
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_can_show_account_freeze:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_account_freeze:Ljava/lang/Boolean;

    .line 12271
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_can_show_account_freeze_notifications:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_account_freeze_notifications:Ljava/lang/Boolean;

    .line 12272
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_can_show_account_freeze_banner:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_account_freeze_banner:Ljava/lang/Boolean;

    .line 12273
    iget-object p2, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_can_show_cp_pricing_change:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_cp_pricing_change:Ljava/lang/Boolean;

    .line 12274
    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_terminal_api_spm:Ljava/lang/Boolean;

    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_terminal_api_spm:Ljava/lang/Boolean;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 12447
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 12308
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 12309
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;

    .line 12310
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_cnp_fees:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_cnp_fees:Ljava/lang/Boolean;

    .line 12311
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_fee_tutorial:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_fee_tutorial:Ljava/lang/Boolean;

    .line 12312
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->can_show_fees:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->can_show_fees:Ljava/lang/Boolean;

    .line 12313
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->dip_card_for_customer_cof:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->dip_card_for_customer_cof:Ljava/lang/Boolean;

    .line 12314
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_dip_cof_post_payment:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_dip_cof_post_payment:Ljava/lang/Boolean;

    .line 12315
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_order_reader_menu:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_order_reader_menu:Ljava/lang/Boolean;

    .line 12316
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_order_r12:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_order_r12:Ljava/lang/Boolean;

    .line 12317
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_order_r4:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_order_r4:Ljava/lang/Boolean;

    .line 12318
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_order_r41:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_order_r41:Ljava/lang/Boolean;

    .line 12319
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_x2_dip_card_for_customer_cof:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_x2_dip_card_for_customer_cof:Ljava/lang/Boolean;

    .line 12320
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_first_payment_tutorial_v2:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_first_payment_tutorial_v2:Ljava/lang/Boolean;

    .line 12321
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_disputes:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_disputes:Ljava/lang/Boolean;

    .line 12322
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_disputes_challenge_flow:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_disputes_challenge_flow:Ljava/lang/Boolean;

    .line 12323
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_connected_terminal:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_connected_terminal:Ljava/lang/Boolean;

    .line 12324
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_collect_cof_postal_code:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_collect_cof_postal_code:Ljava/lang/Boolean;

    .line 12325
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_collect_cnp_postal_code:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_collect_cnp_postal_code:Ljava/lang/Boolean;

    .line 12326
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_account_freeze:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_account_freeze:Ljava/lang/Boolean;

    .line 12327
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_account_freeze_notifications:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_account_freeze_notifications:Ljava/lang/Boolean;

    .line 12328
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_account_freeze_banner:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_account_freeze_banner:Ljava/lang/Boolean;

    .line 12329
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_cp_pricing_change:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_cp_pricing_change:Ljava/lang/Boolean;

    .line 12330
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_terminal_api_spm:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_terminal_api_spm:Ljava/lang/Boolean;

    .line 12331
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 12336
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_15

    .line 12338
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 12339
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_cnp_fees:Ljava/lang/Boolean;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 12340
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_fee_tutorial:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 12341
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->can_show_fees:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 12342
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->dip_card_for_customer_cof:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 12343
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_dip_cof_post_payment:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 12344
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_order_reader_menu:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 12345
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_order_r12:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 12346
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_order_r4:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 12347
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_order_r41:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 12348
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_x2_dip_card_for_customer_cof:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 12349
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_first_payment_tutorial_v2:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 12350
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_disputes:Ljava/lang/Boolean;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 12351
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_disputes_challenge_flow:Ljava/lang/Boolean;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_c

    :cond_c
    const/4 v1, 0x0

    :goto_c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 12352
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_connected_terminal:Ljava/lang/Boolean;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_d

    :cond_d
    const/4 v1, 0x0

    :goto_d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 12353
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_collect_cof_postal_code:Ljava/lang/Boolean;

    if-eqz v1, :cond_e

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_e

    :cond_e
    const/4 v1, 0x0

    :goto_e
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 12354
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_collect_cnp_postal_code:Ljava/lang/Boolean;

    if-eqz v1, :cond_f

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_f

    :cond_f
    const/4 v1, 0x0

    :goto_f
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 12355
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_account_freeze:Ljava/lang/Boolean;

    if-eqz v1, :cond_10

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_10

    :cond_10
    const/4 v1, 0x0

    :goto_10
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 12356
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_account_freeze_notifications:Ljava/lang/Boolean;

    if-eqz v1, :cond_11

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_11

    :cond_11
    const/4 v1, 0x0

    :goto_11
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 12357
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_account_freeze_banner:Ljava/lang/Boolean;

    if-eqz v1, :cond_12

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_12

    :cond_12
    const/4 v1, 0x0

    :goto_12
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 12358
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_cp_pricing_change:Ljava/lang/Boolean;

    if-eqz v1, :cond_13

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_13

    :cond_13
    const/4 v1, 0x0

    :goto_13
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 12359
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_terminal_api_spm:Ljava/lang/Boolean;

    if-eqz v1, :cond_14

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_14
    add-int/2addr v0, v2

    .line 12360
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_15
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;
    .locals 2

    .line 12279
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;-><init>()V

    .line 12280
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_cnp_fees:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_can_show_cnp_fees:Ljava/lang/Boolean;

    .line 12281
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_fee_tutorial:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_fee_tutorial:Ljava/lang/Boolean;

    .line 12282
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->can_show_fees:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->can_show_fees:Ljava/lang/Boolean;

    .line 12283
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->dip_card_for_customer_cof:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->dip_card_for_customer_cof:Ljava/lang/Boolean;

    .line 12284
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_dip_cof_post_payment:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_dip_cof_post_payment:Ljava/lang/Boolean;

    .line 12285
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_order_reader_menu:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_can_show_order_reader_menu:Ljava/lang/Boolean;

    .line 12286
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_order_r12:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_can_order_r12:Ljava/lang/Boolean;

    .line 12287
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_order_r4:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_can_order_r4:Ljava/lang/Boolean;

    .line 12288
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_order_r41:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_can_order_r41:Ljava/lang/Boolean;

    .line 12289
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_x2_dip_card_for_customer_cof:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_x2_dip_card_for_customer_cof:Ljava/lang/Boolean;

    .line 12290
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_first_payment_tutorial_v2:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_first_payment_tutorial_v2:Ljava/lang/Boolean;

    .line 12291
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_disputes:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_can_show_disputes:Ljava/lang/Boolean;

    .line 12292
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_disputes_challenge_flow:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_can_show_disputes_challenge_flow:Ljava/lang/Boolean;

    .line 12293
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_connected_terminal:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_connected_terminal:Ljava/lang/Boolean;

    .line 12294
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_collect_cof_postal_code:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_collect_cof_postal_code:Ljava/lang/Boolean;

    .line 12295
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_collect_cnp_postal_code:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_collect_cnp_postal_code:Ljava/lang/Boolean;

    .line 12296
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_account_freeze:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_can_show_account_freeze:Ljava/lang/Boolean;

    .line 12297
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_account_freeze_notifications:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_can_show_account_freeze_notifications:Ljava/lang/Boolean;

    .line 12298
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_account_freeze_banner:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_can_show_account_freeze_banner:Ljava/lang/Boolean;

    .line 12299
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_cp_pricing_change:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_can_show_cp_pricing_change:Ljava/lang/Boolean;

    .line 12300
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_terminal_api_spm:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_terminal_api_spm:Ljava/lang/Boolean;

    .line 12301
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 12022
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;
    .locals 2

    .line 12422
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_cnp_fees:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_cnp_fees:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_can_show_cnp_fees(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v1

    .line 12423
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_fee_tutorial:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_fee_tutorial:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_fee_tutorial(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v1

    .line 12424
    :cond_1
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->can_show_fees:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->can_show_fees:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->can_show_fees(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v1

    .line 12425
    :cond_2
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->dip_card_for_customer_cof:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->dip_card_for_customer_cof:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->dip_card_for_customer_cof(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v1

    .line 12426
    :cond_3
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_dip_cof_post_payment:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_dip_cof_post_payment:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_dip_cof_post_payment(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v1

    .line 12427
    :cond_4
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_order_reader_menu:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_order_reader_menu:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_can_show_order_reader_menu(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v1

    .line 12428
    :cond_5
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_order_r12:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_order_r12:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_can_order_r12(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v1

    .line 12429
    :cond_6
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_order_r4:Ljava/lang/Boolean;

    if-eqz v0, :cond_7

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_order_r4:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_can_order_r4(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v1

    .line 12430
    :cond_7
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_order_r41:Ljava/lang/Boolean;

    if-eqz v0, :cond_8

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_order_r41:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_can_order_r41(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v1

    .line 12431
    :cond_8
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_x2_dip_card_for_customer_cof:Ljava/lang/Boolean;

    if-eqz v0, :cond_9

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_x2_dip_card_for_customer_cof:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_x2_dip_card_for_customer_cof(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v1

    .line 12432
    :cond_9
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_first_payment_tutorial_v2:Ljava/lang/Boolean;

    if-eqz v0, :cond_a

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_first_payment_tutorial_v2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_first_payment_tutorial_v2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v1

    .line 12433
    :cond_a
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_disputes:Ljava/lang/Boolean;

    if-eqz v0, :cond_b

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_disputes:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_can_show_disputes(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v1

    .line 12434
    :cond_b
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_disputes_challenge_flow:Ljava/lang/Boolean;

    if-eqz v0, :cond_c

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_disputes_challenge_flow:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_can_show_disputes_challenge_flow(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v1

    .line 12435
    :cond_c
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_connected_terminal:Ljava/lang/Boolean;

    if-eqz v0, :cond_d

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_connected_terminal:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_connected_terminal(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v1

    .line 12436
    :cond_d
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_collect_cof_postal_code:Ljava/lang/Boolean;

    if-eqz v0, :cond_e

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_collect_cof_postal_code:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_collect_cof_postal_code(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v1

    .line 12437
    :cond_e
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_collect_cnp_postal_code:Ljava/lang/Boolean;

    if-eqz v0, :cond_f

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_collect_cnp_postal_code:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_collect_cnp_postal_code(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v1

    .line 12438
    :cond_f
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_account_freeze:Ljava/lang/Boolean;

    if-eqz v0, :cond_10

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_account_freeze:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_can_show_account_freeze(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v1

    .line 12439
    :cond_10
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_account_freeze_notifications:Ljava/lang/Boolean;

    if-eqz v0, :cond_11

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_account_freeze_notifications:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_can_show_account_freeze_notifications(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v1

    .line 12440
    :cond_11
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_account_freeze_banner:Ljava/lang/Boolean;

    if-eqz v0, :cond_12

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_account_freeze_banner:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_can_show_account_freeze_banner(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v1

    .line 12441
    :cond_12
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_cp_pricing_change:Ljava/lang/Boolean;

    if-eqz v0, :cond_13

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_cp_pricing_change:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_can_show_cp_pricing_change(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v1

    .line 12442
    :cond_13
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_terminal_api_spm:Ljava/lang/Boolean;

    if-eqz v0, :cond_14

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_terminal_api_spm:Ljava/lang/Boolean;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_terminal_api_spm(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v1

    :cond_14
    if-nez v1, :cond_15

    move-object p1, p0

    goto :goto_0

    .line 12443
    :cond_15
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 12022
    check-cast p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->overlay(Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;
    .locals 2

    .line 12395
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_cnp_fees:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->DEFAULT_APP_CAN_SHOW_CNP_FEES:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_can_show_cnp_fees(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v1

    .line 12396
    :cond_0
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_fee_tutorial:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->DEFAULT_APP_FEE_TUTORIAL:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_fee_tutorial(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v1

    .line 12397
    :cond_1
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->can_show_fees:Ljava/lang/Boolean;

    if-nez v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->DEFAULT_CAN_SHOW_FEES:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->can_show_fees(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v1

    .line 12398
    :cond_2
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->dip_card_for_customer_cof:Ljava/lang/Boolean;

    if-nez v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->DEFAULT_DIP_CARD_FOR_CUSTOMER_COF:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->dip_card_for_customer_cof(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v1

    .line 12399
    :cond_3
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_dip_cof_post_payment:Ljava/lang/Boolean;

    if-nez v0, :cond_4

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->DEFAULT_APP_DIP_COF_POST_PAYMENT:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_dip_cof_post_payment(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v1

    .line 12400
    :cond_4
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_order_reader_menu:Ljava/lang/Boolean;

    if-nez v0, :cond_5

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->DEFAULT_APP_CAN_SHOW_ORDER_READER_MENU:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_can_show_order_reader_menu(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v1

    .line 12401
    :cond_5
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_order_r12:Ljava/lang/Boolean;

    if-nez v0, :cond_6

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->DEFAULT_APP_CAN_ORDER_R12:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_can_order_r12(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v1

    .line 12402
    :cond_6
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_order_r4:Ljava/lang/Boolean;

    if-nez v0, :cond_7

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->DEFAULT_APP_CAN_ORDER_R4:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_can_order_r4(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v1

    .line 12403
    :cond_7
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_order_r41:Ljava/lang/Boolean;

    if-nez v0, :cond_8

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->DEFAULT_APP_CAN_ORDER_R41:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_can_order_r41(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v1

    .line 12404
    :cond_8
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_x2_dip_card_for_customer_cof:Ljava/lang/Boolean;

    if-nez v0, :cond_9

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->DEFAULT_APP_X2_DIP_CARD_FOR_CUSTOMER_COF:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_x2_dip_card_for_customer_cof(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v1

    .line 12405
    :cond_9
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_first_payment_tutorial_v2:Ljava/lang/Boolean;

    if-nez v0, :cond_a

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->DEFAULT_APP_FIRST_PAYMENT_TUTORIAL_V2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_first_payment_tutorial_v2(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v1

    .line 12406
    :cond_a
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_disputes:Ljava/lang/Boolean;

    if-nez v0, :cond_b

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->DEFAULT_APP_CAN_SHOW_DISPUTES:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_can_show_disputes(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v1

    .line 12407
    :cond_b
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_disputes_challenge_flow:Ljava/lang/Boolean;

    if-nez v0, :cond_c

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->DEFAULT_APP_CAN_SHOW_DISPUTES_CHALLENGE_FLOW:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_can_show_disputes_challenge_flow(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v1

    .line 12408
    :cond_c
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_connected_terminal:Ljava/lang/Boolean;

    if-nez v0, :cond_d

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->DEFAULT_APP_CONNECTED_TERMINAL:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_connected_terminal(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v1

    .line 12409
    :cond_d
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_collect_cof_postal_code:Ljava/lang/Boolean;

    if-nez v0, :cond_e

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->DEFAULT_APP_COLLECT_COF_POSTAL_CODE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_collect_cof_postal_code(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v1

    .line 12410
    :cond_e
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_collect_cnp_postal_code:Ljava/lang/Boolean;

    if-nez v0, :cond_f

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->DEFAULT_APP_COLLECT_CNP_POSTAL_CODE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_collect_cnp_postal_code(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v1

    .line 12411
    :cond_f
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_account_freeze:Ljava/lang/Boolean;

    if-nez v0, :cond_10

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->DEFAULT_APP_CAN_SHOW_ACCOUNT_FREEZE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_can_show_account_freeze(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v1

    .line 12412
    :cond_10
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_account_freeze_notifications:Ljava/lang/Boolean;

    if-nez v0, :cond_11

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->DEFAULT_APP_CAN_SHOW_ACCOUNT_FREEZE_NOTIFICATIONS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_can_show_account_freeze_notifications(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v1

    .line 12413
    :cond_11
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_account_freeze_banner:Ljava/lang/Boolean;

    if-nez v0, :cond_12

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->DEFAULT_APP_CAN_SHOW_ACCOUNT_FREEZE_BANNER:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_can_show_account_freeze_banner(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v1

    .line 12414
    :cond_12
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_cp_pricing_change:Ljava/lang/Boolean;

    if-nez v0, :cond_13

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->DEFAULT_APP_CAN_SHOW_CP_PRICING_CHANGE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_can_show_cp_pricing_change(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v1

    .line 12415
    :cond_13
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_terminal_api_spm:Ljava/lang/Boolean;

    if-nez v0, :cond_14

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->DEFAULT_APP_TERMINAL_API_SPM:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->app_terminal_api_spm(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;

    move-result-object v1

    :cond_14
    if-nez v1, :cond_15

    move-object v0, p0

    goto :goto_0

    .line 12416
    :cond_15
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 12022
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 12367
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 12368
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_cnp_fees:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const-string v1, ", app_can_show_cnp_fees="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_cnp_fees:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 12369
    :cond_0
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_fee_tutorial:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    const-string v1, ", app_fee_tutorial="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_fee_tutorial:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 12370
    :cond_1
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->can_show_fees:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", can_show_fees="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->can_show_fees:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 12371
    :cond_2
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->dip_card_for_customer_cof:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    const-string v1, ", dip_card_for_customer_cof="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->dip_card_for_customer_cof:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 12372
    :cond_3
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_dip_cof_post_payment:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    const-string v1, ", app_dip_cof_post_payment="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_dip_cof_post_payment:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 12373
    :cond_4
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_order_reader_menu:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    const-string v1, ", app_can_show_order_reader_menu="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_order_reader_menu:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 12374
    :cond_5
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_order_r12:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    const-string v1, ", app_can_order_r12="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_order_r12:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 12375
    :cond_6
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_order_r4:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    const-string v1, ", app_can_order_r4="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_order_r4:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 12376
    :cond_7
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_order_r41:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    const-string v1, ", app_can_order_r41="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_order_r41:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 12377
    :cond_8
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_x2_dip_card_for_customer_cof:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    const-string v1, ", app_x2_dip_card_for_customer_cof="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_x2_dip_card_for_customer_cof:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 12378
    :cond_9
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_first_payment_tutorial_v2:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    const-string v1, ", app_first_payment_tutorial_v2="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_first_payment_tutorial_v2:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 12379
    :cond_a
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_disputes:Ljava/lang/Boolean;

    if-eqz v1, :cond_b

    const-string v1, ", app_can_show_disputes="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_disputes:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 12380
    :cond_b
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_disputes_challenge_flow:Ljava/lang/Boolean;

    if-eqz v1, :cond_c

    const-string v1, ", app_can_show_disputes_challenge_flow="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_disputes_challenge_flow:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 12381
    :cond_c
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_connected_terminal:Ljava/lang/Boolean;

    if-eqz v1, :cond_d

    const-string v1, ", app_connected_terminal="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_connected_terminal:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 12382
    :cond_d
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_collect_cof_postal_code:Ljava/lang/Boolean;

    if-eqz v1, :cond_e

    const-string v1, ", app_collect_cof_postal_code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_collect_cof_postal_code:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 12383
    :cond_e
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_collect_cnp_postal_code:Ljava/lang/Boolean;

    if-eqz v1, :cond_f

    const-string v1, ", app_collect_cnp_postal_code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_collect_cnp_postal_code:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 12384
    :cond_f
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_account_freeze:Ljava/lang/Boolean;

    if-eqz v1, :cond_10

    const-string v1, ", app_can_show_account_freeze="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_account_freeze:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 12385
    :cond_10
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_account_freeze_notifications:Ljava/lang/Boolean;

    if-eqz v1, :cond_11

    const-string v1, ", app_can_show_account_freeze_notifications="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_account_freeze_notifications:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 12386
    :cond_11
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_account_freeze_banner:Ljava/lang/Boolean;

    if-eqz v1, :cond_12

    const-string v1, ", app_can_show_account_freeze_banner="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_account_freeze_banner:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 12387
    :cond_12
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_cp_pricing_change:Ljava/lang/Boolean;

    if-eqz v1, :cond_13

    const-string v1, ", app_can_show_cp_pricing_change="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_can_show_cp_pricing_change:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 12388
    :cond_13
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_terminal_api_spm:Ljava/lang/Boolean;

    if-eqz v1, :cond_14

    const-string v1, ", app_terminal_api_spm="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Terminal;->app_terminal_api_spm:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_14
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Terminal{"

    .line 12389
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
