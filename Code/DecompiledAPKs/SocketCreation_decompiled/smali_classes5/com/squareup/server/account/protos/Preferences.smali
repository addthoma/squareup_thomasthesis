.class public final Lcom/squareup/server/account/protos/Preferences;
.super Lcom/squareup/wire/AndroidMessage;
.source "Preferences.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/Preferences$ProtoAdapter_Preferences;,
        Lcom/squareup/server/account/protos/Preferences$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/Preferences;",
        "Lcom/squareup/server/account/protos/Preferences$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/Preferences;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/Preferences;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/Preferences;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/Preferences;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_ALLOW_INSTANT_DEPOSIT:Ljava/lang/Boolean;

.field public static final DEFAULT_AMENDED_REOPENS_ENABLED:Ljava/lang/Boolean;

.field public static final DEFAULT_BEHAVIOR_AFTER_ADDING_ITEM:Ljava/lang/String; = ""

.field public static final DEFAULT_CASH_MANAGEMENT_EMAIL_REPORT:Ljava/lang/Boolean;

.field public static final DEFAULT_CASH_MANAGEMENT_ENABLED:Ljava/lang/Boolean;

.field public static final DEFAULT_CASH_MANAGEMENT_PRINT_REPORT:Ljava/lang/Boolean;

.field public static final DEFAULT_CASH_MANAGEMENT_REPORT_RECIPIENT_EMAIL:Ljava/lang/String; = ""

.field public static final DEFAULT_CUSTOMER_MANAGEMENT_COLLECT_AFTER_CHECKOUT:Ljava/lang/Boolean;

.field public static final DEFAULT_CUSTOMER_MANAGEMENT_COLLECT_BEFORE_CHECKOUT:Ljava/lang/Boolean;

.field public static final DEFAULT_CUSTOMER_MANAGEMENT_SHOW_EMAIL_COLLECTION_SCREEN:Ljava/lang/Boolean;

.field public static final DEFAULT_CUSTOMER_MANAGEMENT_SHOW_SAVE_CARD_BUTTON_AFTER_CHECKOUT:Ljava/lang/Boolean;

.field public static final DEFAULT_CUSTOMER_MANAGEMENT_USE_CARD_ON_FILE:Ljava/lang/Boolean;

.field public static final DEFAULT_CUSTOM_TENDER_OPTIONS_PROTO:Ljava/lang/String; = ""

.field public static final DEFAULT_EMPLOYEE_MANAGEMENT_ENABLED_FOR_ACCOUNT:Ljava/lang/Boolean;

.field public static final DEFAULT_EMPLOYEE_MANAGEMENT_ENABLED_FOR_DEVICE:Ljava/lang/Boolean;

.field public static final DEFAULT_EMPLOYEE_MANAGEMENT_INACTIVITY_TIMEOUT:Ljava/lang/String; = ""

.field public static final DEFAULT_EMPLOYEE_MANAGEMENT_REQUIRES_EMPLOYEE_LOGOUT_AFTER_COMPLETED_TRANSACTION:Ljava/lang/Boolean;

.field public static final DEFAULT_EMPLOYEE_MANAGEMENT_TRACKING_LEVEL:Ljava/lang/String; = ""

.field public static final DEFAULT_ERROR_MESSAGE:Ljava/lang/String; = ""

.field public static final DEFAULT_ERROR_TITLE:Ljava/lang/String; = ""

.field public static final DEFAULT_FOR_PAPER_SIGNATURE_ALWAYS_PRINT_CUSTOMER_COPY:Ljava/lang/Boolean;

.field public static final DEFAULT_FOR_PAPER_SIGNATURE_PRINT_ADDITIONAL_AUTH_SLIP:Ljava/lang/Boolean;

.field public static final DEFAULT_FOR_PAPER_SIGNATURE_USE_QUICK_TIP_RECEIPT:Ljava/lang/Boolean;

.field public static final DEFAULT_MESSAGE:Ljava/lang/String; = ""

.field public static final DEFAULT_OPEN_TICKETS_ENABLED:Ljava/lang/Boolean;

.field public static final DEFAULT_SKIP_RECEIPT_SCREEN:Ljava/lang/Boolean;

.field public static final DEFAULT_SKIP_SIGNATURE:Ljava/lang/Boolean;

.field public static final DEFAULT_SKIP_SIGNATURE_ALWAYS:Ljava/lang/Boolean;

.field public static final DEFAULT_STORE_AND_FORWARD_ENABLED:Ljava/lang/Boolean;

.field public static final DEFAULT_SUCCESS:Ljava/lang/Boolean;

.field public static final DEFAULT_TERMINAL_CONNECTED_MODE_ENABLED:Ljava/lang/Boolean;

.field public static final DEFAULT_TIPPING_CALCULATION_PHASE:Ljava/lang/String; = ""

.field public static final DEFAULT_TIPPING_ENABLED:Ljava/lang/Boolean;

.field public static final DEFAULT_TIPPING_USE_CUSTOM_PERCENTAGES:Ljava/lang/Boolean;

.field public static final DEFAULT_TIPPING_USE_MANUAL_TIP_ENTRY:Ljava/lang/Boolean;

.field public static final DEFAULT_TITLE:Ljava/lang/String; = ""

.field public static final DEFAULT_USE_ALLOW_SWIPE_FOR_CHIP_CARDS:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_CURATED_IMAGE_FOR_RECEIPT:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_OPEN_TICKETS_MENU_AS_HOME_SCREEN:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_ORDER_HUB:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_PAPER_SIGNATURE:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_PREDEFINED_TICKETS:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_SEPARATE_TIPPING_SCREEN:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final allow_instant_deposit:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2a
    .end annotation
.end field

.field public final amended_reopens_enabled:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2c
    .end annotation
.end field

.field public final behavior_after_adding_item:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2d
    .end annotation
.end field

.field public final cash_management_default_starting_amount:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0x23
    .end annotation
.end field

.field public final cash_management_email_report:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x21
    .end annotation
.end field

.field public final cash_management_enabled:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x20
    .end annotation
.end field

.field public final cash_management_print_report:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x22
    .end annotation
.end field

.field public final cash_management_report_recipient_email:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x24
    .end annotation
.end field

.field public final custom_tender_options_proto:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1c
    .end annotation
.end field

.field public final customer_management_collect_after_checkout:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x26
    .end annotation
.end field

.field public final customer_management_collect_before_checkout:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x25
    .end annotation
.end field

.field public final customer_management_show_email_collection_screen:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x29
    .end annotation
.end field

.field public final customer_management_show_save_card_button_after_checkout:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x28
    .end annotation
.end field

.field public final customer_management_use_card_on_file:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x27
    .end annotation
.end field

.field public final employee_management_enabled_for_account:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x16
    .end annotation
.end field

.field public final employee_management_enabled_for_device:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x17
    .end annotation
.end field

.field public final employee_management_inactivity_timeout:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x18
    .end annotation
.end field

.field public final employee_management_requires_employee_logout_after_completed_transaction:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2f
    .end annotation
.end field

.field public final employee_management_tracking_level:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x19
    .end annotation
.end field

.field public final error_message:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x5
    .end annotation
.end field

.field public final error_title:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x4
    .end annotation
.end field

.field public final for_paper_signature_always_print_customer_copy:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x13
    .end annotation
.end field

.field public final for_paper_signature_print_additional_auth_slip:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x15
    .end annotation
.end field

.field public final for_paper_signature_use_quick_tip_receipt:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x14
    .end annotation
.end field

.field public final message:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x3
    .end annotation
.end field

.field public final open_tickets_enabled:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x11
    .end annotation
.end field

.field public final skip_receipt_screen:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x30
    .end annotation
.end field

.field public final skip_signature:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xa
    .end annotation
.end field

.field public final skip_signature_always:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1d
    .end annotation
.end field

.field public final store_and_forward_enabled:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xd
    .end annotation
.end field

.field public final store_and_forward_single_transaction_limit:Lcom/squareup/protos/common/Money;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.protos.common.Money#ADAPTER"
        tag = 0xe
    .end annotation
.end field

.field public final success:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1
    .end annotation
.end field

.field public final terminal_connected_mode_enabled:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2b
    .end annotation
.end field

.field public final tipping_calculation_phase:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x1b
    .end annotation
.end field

.field public final tipping_custom_percentages:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#DOUBLE"
        label = .enum Lcom/squareup/wire/WireField$Label;->REPEATED:Lcom/squareup/wire/WireField$Label;
        tag = 0x9
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation
.end field

.field public final tipping_enabled:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x6
    .end annotation
.end field

.field public final tipping_use_custom_percentages:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x7
    .end annotation
.end field

.field public final tipping_use_manual_tip_entry:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x8
    .end annotation
.end field

.field public final title:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#STRING"
        tag = 0x2
    .end annotation
.end field

.field public final use_allow_swipe_for_chip_cards:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1a
    .end annotation
.end field

.field public final use_curated_image_for_receipt:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0xf
    .end annotation
.end field

.field public final use_open_tickets_menu_as_home_screen:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1e
    .end annotation
.end field

.field public final use_order_hub:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2e
    .end annotation
.end field

.field public final use_paper_signature:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x12
    .end annotation
.end field

.field public final use_predefined_tickets:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1f
    .end annotation
.end field

.field public final use_separate_tipping_screen:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x10
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 34
    new-instance v0, Lcom/squareup/server/account/protos/Preferences$ProtoAdapter_Preferences;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/Preferences$ProtoAdapter_Preferences;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/Preferences;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 36
    sget-object v0, Lcom/squareup/server/account/protos/Preferences;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/Preferences;->CREATOR:Landroid/os/Parcelable$Creator;

    const/4 v0, 0x0

    .line 40
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/Preferences;->DEFAULT_SUCCESS:Ljava/lang/Boolean;

    .line 50
    sput-object v0, Lcom/squareup/server/account/protos/Preferences;->DEFAULT_TIPPING_ENABLED:Ljava/lang/Boolean;

    .line 52
    sput-object v0, Lcom/squareup/server/account/protos/Preferences;->DEFAULT_TIPPING_USE_CUSTOM_PERCENTAGES:Ljava/lang/Boolean;

    .line 54
    sput-object v0, Lcom/squareup/server/account/protos/Preferences;->DEFAULT_TIPPING_USE_MANUAL_TIP_ENTRY:Ljava/lang/Boolean;

    .line 56
    sput-object v0, Lcom/squareup/server/account/protos/Preferences;->DEFAULT_SKIP_SIGNATURE:Ljava/lang/Boolean;

    .line 58
    sput-object v0, Lcom/squareup/server/account/protos/Preferences;->DEFAULT_STORE_AND_FORWARD_ENABLED:Ljava/lang/Boolean;

    .line 60
    sput-object v0, Lcom/squareup/server/account/protos/Preferences;->DEFAULT_USE_CURATED_IMAGE_FOR_RECEIPT:Ljava/lang/Boolean;

    .line 62
    sput-object v0, Lcom/squareup/server/account/protos/Preferences;->DEFAULT_USE_SEPARATE_TIPPING_SCREEN:Ljava/lang/Boolean;

    .line 64
    sput-object v0, Lcom/squareup/server/account/protos/Preferences;->DEFAULT_OPEN_TICKETS_ENABLED:Ljava/lang/Boolean;

    .line 66
    sput-object v0, Lcom/squareup/server/account/protos/Preferences;->DEFAULT_USE_PAPER_SIGNATURE:Ljava/lang/Boolean;

    .line 68
    sput-object v0, Lcom/squareup/server/account/protos/Preferences;->DEFAULT_FOR_PAPER_SIGNATURE_ALWAYS_PRINT_CUSTOMER_COPY:Ljava/lang/Boolean;

    .line 70
    sput-object v0, Lcom/squareup/server/account/protos/Preferences;->DEFAULT_FOR_PAPER_SIGNATURE_USE_QUICK_TIP_RECEIPT:Ljava/lang/Boolean;

    .line 72
    sput-object v0, Lcom/squareup/server/account/protos/Preferences;->DEFAULT_FOR_PAPER_SIGNATURE_PRINT_ADDITIONAL_AUTH_SLIP:Ljava/lang/Boolean;

    .line 74
    sput-object v0, Lcom/squareup/server/account/protos/Preferences;->DEFAULT_EMPLOYEE_MANAGEMENT_ENABLED_FOR_ACCOUNT:Ljava/lang/Boolean;

    .line 76
    sput-object v0, Lcom/squareup/server/account/protos/Preferences;->DEFAULT_EMPLOYEE_MANAGEMENT_ENABLED_FOR_DEVICE:Ljava/lang/Boolean;

    .line 82
    sput-object v0, Lcom/squareup/server/account/protos/Preferences;->DEFAULT_USE_ALLOW_SWIPE_FOR_CHIP_CARDS:Ljava/lang/Boolean;

    .line 88
    sput-object v0, Lcom/squareup/server/account/protos/Preferences;->DEFAULT_SKIP_SIGNATURE_ALWAYS:Ljava/lang/Boolean;

    .line 90
    sput-object v0, Lcom/squareup/server/account/protos/Preferences;->DEFAULT_USE_OPEN_TICKETS_MENU_AS_HOME_SCREEN:Ljava/lang/Boolean;

    .line 92
    sput-object v0, Lcom/squareup/server/account/protos/Preferences;->DEFAULT_USE_PREDEFINED_TICKETS:Ljava/lang/Boolean;

    .line 94
    sput-object v0, Lcom/squareup/server/account/protos/Preferences;->DEFAULT_CASH_MANAGEMENT_ENABLED:Ljava/lang/Boolean;

    .line 96
    sput-object v0, Lcom/squareup/server/account/protos/Preferences;->DEFAULT_CASH_MANAGEMENT_EMAIL_REPORT:Ljava/lang/Boolean;

    .line 98
    sput-object v0, Lcom/squareup/server/account/protos/Preferences;->DEFAULT_CASH_MANAGEMENT_PRINT_REPORT:Ljava/lang/Boolean;

    .line 102
    sput-object v0, Lcom/squareup/server/account/protos/Preferences;->DEFAULT_CUSTOMER_MANAGEMENT_COLLECT_BEFORE_CHECKOUT:Ljava/lang/Boolean;

    .line 104
    sput-object v0, Lcom/squareup/server/account/protos/Preferences;->DEFAULT_CUSTOMER_MANAGEMENT_COLLECT_AFTER_CHECKOUT:Ljava/lang/Boolean;

    .line 106
    sput-object v0, Lcom/squareup/server/account/protos/Preferences;->DEFAULT_CUSTOMER_MANAGEMENT_USE_CARD_ON_FILE:Ljava/lang/Boolean;

    .line 108
    sput-object v0, Lcom/squareup/server/account/protos/Preferences;->DEFAULT_CUSTOMER_MANAGEMENT_SHOW_SAVE_CARD_BUTTON_AFTER_CHECKOUT:Ljava/lang/Boolean;

    .line 110
    sput-object v0, Lcom/squareup/server/account/protos/Preferences;->DEFAULT_CUSTOMER_MANAGEMENT_SHOW_EMAIL_COLLECTION_SCREEN:Ljava/lang/Boolean;

    .line 112
    sput-object v0, Lcom/squareup/server/account/protos/Preferences;->DEFAULT_ALLOW_INSTANT_DEPOSIT:Ljava/lang/Boolean;

    .line 114
    sput-object v0, Lcom/squareup/server/account/protos/Preferences;->DEFAULT_TERMINAL_CONNECTED_MODE_ENABLED:Ljava/lang/Boolean;

    .line 116
    sput-object v0, Lcom/squareup/server/account/protos/Preferences;->DEFAULT_AMENDED_REOPENS_ENABLED:Ljava/lang/Boolean;

    .line 120
    sput-object v0, Lcom/squareup/server/account/protos/Preferences;->DEFAULT_USE_ORDER_HUB:Ljava/lang/Boolean;

    .line 122
    sput-object v0, Lcom/squareup/server/account/protos/Preferences;->DEFAULT_EMPLOYEE_MANAGEMENT_REQUIRES_EMPLOYEE_LOGOUT_AFTER_COMPLETED_TRANSACTION:Ljava/lang/Boolean;

    .line 124
    sput-object v0, Lcom/squareup/server/account/protos/Preferences;->DEFAULT_SKIP_RECEIPT_SCREEN:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/server/account/protos/Preferences$Builder;Lokio/ByteString;)V
    .locals 1

    .line 457
    sget-object v0, Lcom/squareup/server/account/protos/Preferences;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p2}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 458
    iget-object p2, p1, Lcom/squareup/server/account/protos/Preferences$Builder;->success:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/Preferences;->success:Ljava/lang/Boolean;

    .line 459
    iget-object p2, p1, Lcom/squareup/server/account/protos/Preferences$Builder;->title:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/server/account/protos/Preferences;->title:Ljava/lang/String;

    .line 460
    iget-object p2, p1, Lcom/squareup/server/account/protos/Preferences$Builder;->message:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/server/account/protos/Preferences;->message:Ljava/lang/String;

    .line 461
    iget-object p2, p1, Lcom/squareup/server/account/protos/Preferences$Builder;->error_title:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/server/account/protos/Preferences;->error_title:Ljava/lang/String;

    .line 462
    iget-object p2, p1, Lcom/squareup/server/account/protos/Preferences$Builder;->error_message:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/server/account/protos/Preferences;->error_message:Ljava/lang/String;

    .line 463
    iget-object p2, p1, Lcom/squareup/server/account/protos/Preferences$Builder;->tipping_enabled:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/Preferences;->tipping_enabled:Ljava/lang/Boolean;

    .line 464
    iget-object p2, p1, Lcom/squareup/server/account/protos/Preferences$Builder;->tipping_use_custom_percentages:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/Preferences;->tipping_use_custom_percentages:Ljava/lang/Boolean;

    .line 465
    iget-object p2, p1, Lcom/squareup/server/account/protos/Preferences$Builder;->tipping_use_manual_tip_entry:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/Preferences;->tipping_use_manual_tip_entry:Ljava/lang/Boolean;

    .line 466
    iget-object p2, p1, Lcom/squareup/server/account/protos/Preferences$Builder;->tipping_custom_percentages:Ljava/util/List;

    const-string/jumbo v0, "tipping_custom_percentages"

    invoke-static {v0, p2}, Lcom/squareup/wire/internal/Internal;->immutableCopyOf(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    iput-object p2, p0, Lcom/squareup/server/account/protos/Preferences;->tipping_custom_percentages:Ljava/util/List;

    .line 467
    iget-object p2, p1, Lcom/squareup/server/account/protos/Preferences$Builder;->skip_signature:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/Preferences;->skip_signature:Ljava/lang/Boolean;

    .line 468
    iget-object p2, p1, Lcom/squareup/server/account/protos/Preferences$Builder;->store_and_forward_enabled:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/Preferences;->store_and_forward_enabled:Ljava/lang/Boolean;

    .line 469
    iget-object p2, p1, Lcom/squareup/server/account/protos/Preferences$Builder;->store_and_forward_single_transaction_limit:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/server/account/protos/Preferences;->store_and_forward_single_transaction_limit:Lcom/squareup/protos/common/Money;

    .line 470
    iget-object p2, p1, Lcom/squareup/server/account/protos/Preferences$Builder;->use_curated_image_for_receipt:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/Preferences;->use_curated_image_for_receipt:Ljava/lang/Boolean;

    .line 471
    iget-object p2, p1, Lcom/squareup/server/account/protos/Preferences$Builder;->use_separate_tipping_screen:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/Preferences;->use_separate_tipping_screen:Ljava/lang/Boolean;

    .line 472
    iget-object p2, p1, Lcom/squareup/server/account/protos/Preferences$Builder;->open_tickets_enabled:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/Preferences;->open_tickets_enabled:Ljava/lang/Boolean;

    .line 473
    iget-object p2, p1, Lcom/squareup/server/account/protos/Preferences$Builder;->use_paper_signature:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/Preferences;->use_paper_signature:Ljava/lang/Boolean;

    .line 474
    iget-object p2, p1, Lcom/squareup/server/account/protos/Preferences$Builder;->for_paper_signature_always_print_customer_copy:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/Preferences;->for_paper_signature_always_print_customer_copy:Ljava/lang/Boolean;

    .line 475
    iget-object p2, p1, Lcom/squareup/server/account/protos/Preferences$Builder;->for_paper_signature_use_quick_tip_receipt:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/Preferences;->for_paper_signature_use_quick_tip_receipt:Ljava/lang/Boolean;

    .line 476
    iget-object p2, p1, Lcom/squareup/server/account/protos/Preferences$Builder;->for_paper_signature_print_additional_auth_slip:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/Preferences;->for_paper_signature_print_additional_auth_slip:Ljava/lang/Boolean;

    .line 477
    iget-object p2, p1, Lcom/squareup/server/account/protos/Preferences$Builder;->employee_management_enabled_for_account:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/Preferences;->employee_management_enabled_for_account:Ljava/lang/Boolean;

    .line 478
    iget-object p2, p1, Lcom/squareup/server/account/protos/Preferences$Builder;->employee_management_enabled_for_device:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/Preferences;->employee_management_enabled_for_device:Ljava/lang/Boolean;

    .line 479
    iget-object p2, p1, Lcom/squareup/server/account/protos/Preferences$Builder;->employee_management_inactivity_timeout:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/server/account/protos/Preferences;->employee_management_inactivity_timeout:Ljava/lang/String;

    .line 480
    iget-object p2, p1, Lcom/squareup/server/account/protos/Preferences$Builder;->employee_management_tracking_level:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/server/account/protos/Preferences;->employee_management_tracking_level:Ljava/lang/String;

    .line 481
    iget-object p2, p1, Lcom/squareup/server/account/protos/Preferences$Builder;->use_allow_swipe_for_chip_cards:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/Preferences;->use_allow_swipe_for_chip_cards:Ljava/lang/Boolean;

    .line 482
    iget-object p2, p1, Lcom/squareup/server/account/protos/Preferences$Builder;->tipping_calculation_phase:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/server/account/protos/Preferences;->tipping_calculation_phase:Ljava/lang/String;

    .line 483
    iget-object p2, p1, Lcom/squareup/server/account/protos/Preferences$Builder;->custom_tender_options_proto:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/server/account/protos/Preferences;->custom_tender_options_proto:Ljava/lang/String;

    .line 484
    iget-object p2, p1, Lcom/squareup/server/account/protos/Preferences$Builder;->skip_signature_always:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/Preferences;->skip_signature_always:Ljava/lang/Boolean;

    .line 485
    iget-object p2, p1, Lcom/squareup/server/account/protos/Preferences$Builder;->use_open_tickets_menu_as_home_screen:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/Preferences;->use_open_tickets_menu_as_home_screen:Ljava/lang/Boolean;

    .line 486
    iget-object p2, p1, Lcom/squareup/server/account/protos/Preferences$Builder;->use_predefined_tickets:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/Preferences;->use_predefined_tickets:Ljava/lang/Boolean;

    .line 487
    iget-object p2, p1, Lcom/squareup/server/account/protos/Preferences$Builder;->cash_management_enabled:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/Preferences;->cash_management_enabled:Ljava/lang/Boolean;

    .line 488
    iget-object p2, p1, Lcom/squareup/server/account/protos/Preferences$Builder;->cash_management_email_report:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/Preferences;->cash_management_email_report:Ljava/lang/Boolean;

    .line 489
    iget-object p2, p1, Lcom/squareup/server/account/protos/Preferences$Builder;->cash_management_print_report:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/Preferences;->cash_management_print_report:Ljava/lang/Boolean;

    .line 490
    iget-object p2, p1, Lcom/squareup/server/account/protos/Preferences$Builder;->cash_management_default_starting_amount:Lcom/squareup/protos/common/Money;

    iput-object p2, p0, Lcom/squareup/server/account/protos/Preferences;->cash_management_default_starting_amount:Lcom/squareup/protos/common/Money;

    .line 491
    iget-object p2, p1, Lcom/squareup/server/account/protos/Preferences$Builder;->cash_management_report_recipient_email:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/server/account/protos/Preferences;->cash_management_report_recipient_email:Ljava/lang/String;

    .line 492
    iget-object p2, p1, Lcom/squareup/server/account/protos/Preferences$Builder;->customer_management_collect_before_checkout:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/Preferences;->customer_management_collect_before_checkout:Ljava/lang/Boolean;

    .line 493
    iget-object p2, p1, Lcom/squareup/server/account/protos/Preferences$Builder;->customer_management_collect_after_checkout:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/Preferences;->customer_management_collect_after_checkout:Ljava/lang/Boolean;

    .line 494
    iget-object p2, p1, Lcom/squareup/server/account/protos/Preferences$Builder;->customer_management_use_card_on_file:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/Preferences;->customer_management_use_card_on_file:Ljava/lang/Boolean;

    .line 495
    iget-object p2, p1, Lcom/squareup/server/account/protos/Preferences$Builder;->customer_management_show_save_card_button_after_checkout:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/Preferences;->customer_management_show_save_card_button_after_checkout:Ljava/lang/Boolean;

    .line 496
    iget-object p2, p1, Lcom/squareup/server/account/protos/Preferences$Builder;->customer_management_show_email_collection_screen:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/Preferences;->customer_management_show_email_collection_screen:Ljava/lang/Boolean;

    .line 497
    iget-object p2, p1, Lcom/squareup/server/account/protos/Preferences$Builder;->allow_instant_deposit:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/Preferences;->allow_instant_deposit:Ljava/lang/Boolean;

    .line 498
    iget-object p2, p1, Lcom/squareup/server/account/protos/Preferences$Builder;->terminal_connected_mode_enabled:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/Preferences;->terminal_connected_mode_enabled:Ljava/lang/Boolean;

    .line 499
    iget-object p2, p1, Lcom/squareup/server/account/protos/Preferences$Builder;->amended_reopens_enabled:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/Preferences;->amended_reopens_enabled:Ljava/lang/Boolean;

    .line 500
    iget-object p2, p1, Lcom/squareup/server/account/protos/Preferences$Builder;->behavior_after_adding_item:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/server/account/protos/Preferences;->behavior_after_adding_item:Ljava/lang/String;

    .line 501
    iget-object p2, p1, Lcom/squareup/server/account/protos/Preferences$Builder;->use_order_hub:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/Preferences;->use_order_hub:Ljava/lang/Boolean;

    .line 502
    iget-object p2, p1, Lcom/squareup/server/account/protos/Preferences$Builder;->employee_management_requires_employee_logout_after_completed_transaction:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/squareup/server/account/protos/Preferences;->employee_management_requires_employee_logout_after_completed_transaction:Ljava/lang/Boolean;

    .line 503
    iget-object p1, p1, Lcom/squareup/server/account/protos/Preferences$Builder;->skip_receipt_screen:Ljava/lang/Boolean;

    iput-object p1, p0, Lcom/squareup/server/account/protos/Preferences;->skip_receipt_screen:Ljava/lang/Boolean;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/Preferences$Builder;)Lcom/squareup/server/account/protos/Preferences$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 791
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/Preferences;->newBuilder()Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 562
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/Preferences;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 563
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/Preferences;

    .line 564
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/Preferences;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/Preferences;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->success:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Preferences;->success:Ljava/lang/Boolean;

    .line 565
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->title:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Preferences;->title:Ljava/lang/String;

    .line 566
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->message:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Preferences;->message:Ljava/lang/String;

    .line 567
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->error_title:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Preferences;->error_title:Ljava/lang/String;

    .line 568
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->error_message:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Preferences;->error_message:Ljava/lang/String;

    .line 569
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->tipping_enabled:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Preferences;->tipping_enabled:Ljava/lang/Boolean;

    .line 570
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->tipping_use_custom_percentages:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Preferences;->tipping_use_custom_percentages:Ljava/lang/Boolean;

    .line 571
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->tipping_use_manual_tip_entry:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Preferences;->tipping_use_manual_tip_entry:Ljava/lang/Boolean;

    .line 572
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->tipping_custom_percentages:Ljava/util/List;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Preferences;->tipping_custom_percentages:Ljava/util/List;

    .line 573
    invoke-interface {v1, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->skip_signature:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Preferences;->skip_signature:Ljava/lang/Boolean;

    .line 574
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->store_and_forward_enabled:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Preferences;->store_and_forward_enabled:Ljava/lang/Boolean;

    .line 575
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->store_and_forward_single_transaction_limit:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Preferences;->store_and_forward_single_transaction_limit:Lcom/squareup/protos/common/Money;

    .line 576
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->use_curated_image_for_receipt:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Preferences;->use_curated_image_for_receipt:Ljava/lang/Boolean;

    .line 577
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->use_separate_tipping_screen:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Preferences;->use_separate_tipping_screen:Ljava/lang/Boolean;

    .line 578
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->open_tickets_enabled:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Preferences;->open_tickets_enabled:Ljava/lang/Boolean;

    .line 579
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->use_paper_signature:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Preferences;->use_paper_signature:Ljava/lang/Boolean;

    .line 580
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->for_paper_signature_always_print_customer_copy:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Preferences;->for_paper_signature_always_print_customer_copy:Ljava/lang/Boolean;

    .line 581
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->for_paper_signature_use_quick_tip_receipt:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Preferences;->for_paper_signature_use_quick_tip_receipt:Ljava/lang/Boolean;

    .line 582
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->for_paper_signature_print_additional_auth_slip:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Preferences;->for_paper_signature_print_additional_auth_slip:Ljava/lang/Boolean;

    .line 583
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->employee_management_enabled_for_account:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Preferences;->employee_management_enabled_for_account:Ljava/lang/Boolean;

    .line 584
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->employee_management_enabled_for_device:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Preferences;->employee_management_enabled_for_device:Ljava/lang/Boolean;

    .line 585
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->employee_management_inactivity_timeout:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Preferences;->employee_management_inactivity_timeout:Ljava/lang/String;

    .line 586
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->employee_management_tracking_level:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Preferences;->employee_management_tracking_level:Ljava/lang/String;

    .line 587
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->use_allow_swipe_for_chip_cards:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Preferences;->use_allow_swipe_for_chip_cards:Ljava/lang/Boolean;

    .line 588
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->tipping_calculation_phase:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Preferences;->tipping_calculation_phase:Ljava/lang/String;

    .line 589
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->custom_tender_options_proto:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Preferences;->custom_tender_options_proto:Ljava/lang/String;

    .line 590
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->skip_signature_always:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Preferences;->skip_signature_always:Ljava/lang/Boolean;

    .line 591
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->use_open_tickets_menu_as_home_screen:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Preferences;->use_open_tickets_menu_as_home_screen:Ljava/lang/Boolean;

    .line 592
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->use_predefined_tickets:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Preferences;->use_predefined_tickets:Ljava/lang/Boolean;

    .line 593
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->cash_management_enabled:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Preferences;->cash_management_enabled:Ljava/lang/Boolean;

    .line 594
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->cash_management_email_report:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Preferences;->cash_management_email_report:Ljava/lang/Boolean;

    .line 595
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->cash_management_print_report:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Preferences;->cash_management_print_report:Ljava/lang/Boolean;

    .line 596
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->cash_management_default_starting_amount:Lcom/squareup/protos/common/Money;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Preferences;->cash_management_default_starting_amount:Lcom/squareup/protos/common/Money;

    .line 597
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->cash_management_report_recipient_email:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Preferences;->cash_management_report_recipient_email:Ljava/lang/String;

    .line 598
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->customer_management_collect_before_checkout:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Preferences;->customer_management_collect_before_checkout:Ljava/lang/Boolean;

    .line 599
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->customer_management_collect_after_checkout:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Preferences;->customer_management_collect_after_checkout:Ljava/lang/Boolean;

    .line 600
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->customer_management_use_card_on_file:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Preferences;->customer_management_use_card_on_file:Ljava/lang/Boolean;

    .line 601
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->customer_management_show_save_card_button_after_checkout:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Preferences;->customer_management_show_save_card_button_after_checkout:Ljava/lang/Boolean;

    .line 602
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->customer_management_show_email_collection_screen:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Preferences;->customer_management_show_email_collection_screen:Ljava/lang/Boolean;

    .line 603
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->allow_instant_deposit:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Preferences;->allow_instant_deposit:Ljava/lang/Boolean;

    .line 604
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->terminal_connected_mode_enabled:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Preferences;->terminal_connected_mode_enabled:Ljava/lang/Boolean;

    .line 605
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->amended_reopens_enabled:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Preferences;->amended_reopens_enabled:Ljava/lang/Boolean;

    .line 606
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->behavior_after_adding_item:Ljava/lang/String;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Preferences;->behavior_after_adding_item:Ljava/lang/String;

    .line 607
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->use_order_hub:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Preferences;->use_order_hub:Ljava/lang/Boolean;

    .line 608
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->employee_management_requires_employee_logout_after_completed_transaction:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/Preferences;->employee_management_requires_employee_logout_after_completed_transaction:Ljava/lang/Boolean;

    .line 609
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->skip_receipt_screen:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/server/account/protos/Preferences;->skip_receipt_screen:Ljava/lang/Boolean;

    .line 610
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 615
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_2d

    .line 617
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/Preferences;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 618
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->success:Ljava/lang/Boolean;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 619
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->title:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 620
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->message:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 621
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->error_title:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_3
    const/4 v1, 0x0

    :goto_3
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 622
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->error_message:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    :goto_4
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 623
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->tipping_enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    :goto_5
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 624
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->tipping_use_custom_percentages:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_6
    const/4 v1, 0x0

    :goto_6
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 625
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->tipping_use_manual_tip_entry:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_7
    const/4 v1, 0x0

    :goto_7
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 626
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->tipping_custom_percentages:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 627
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->skip_signature:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_8

    :cond_8
    const/4 v1, 0x0

    :goto_8
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 628
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->store_and_forward_enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_9

    :cond_9
    const/4 v1, 0x0

    :goto_9
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 629
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->store_and_forward_single_transaction_limit:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_a

    :cond_a
    const/4 v1, 0x0

    :goto_a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 630
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->use_curated_image_for_receipt:Ljava/lang/Boolean;

    if-eqz v1, :cond_b

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_b

    :cond_b
    const/4 v1, 0x0

    :goto_b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 631
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->use_separate_tipping_screen:Ljava/lang/Boolean;

    if-eqz v1, :cond_c

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_c

    :cond_c
    const/4 v1, 0x0

    :goto_c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 632
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->open_tickets_enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_d

    :cond_d
    const/4 v1, 0x0

    :goto_d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 633
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->use_paper_signature:Ljava/lang/Boolean;

    if-eqz v1, :cond_e

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_e

    :cond_e
    const/4 v1, 0x0

    :goto_e
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 634
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->for_paper_signature_always_print_customer_copy:Ljava/lang/Boolean;

    if-eqz v1, :cond_f

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_f

    :cond_f
    const/4 v1, 0x0

    :goto_f
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 635
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->for_paper_signature_use_quick_tip_receipt:Ljava/lang/Boolean;

    if-eqz v1, :cond_10

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_10

    :cond_10
    const/4 v1, 0x0

    :goto_10
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 636
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->for_paper_signature_print_additional_auth_slip:Ljava/lang/Boolean;

    if-eqz v1, :cond_11

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_11

    :cond_11
    const/4 v1, 0x0

    :goto_11
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 637
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->employee_management_enabled_for_account:Ljava/lang/Boolean;

    if-eqz v1, :cond_12

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_12

    :cond_12
    const/4 v1, 0x0

    :goto_12
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 638
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->employee_management_enabled_for_device:Ljava/lang/Boolean;

    if-eqz v1, :cond_13

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_13

    :cond_13
    const/4 v1, 0x0

    :goto_13
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 639
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->employee_management_inactivity_timeout:Ljava/lang/String;

    if-eqz v1, :cond_14

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_14

    :cond_14
    const/4 v1, 0x0

    :goto_14
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 640
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->employee_management_tracking_level:Ljava/lang/String;

    if-eqz v1, :cond_15

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_15

    :cond_15
    const/4 v1, 0x0

    :goto_15
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 641
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->use_allow_swipe_for_chip_cards:Ljava/lang/Boolean;

    if-eqz v1, :cond_16

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_16

    :cond_16
    const/4 v1, 0x0

    :goto_16
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 642
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->tipping_calculation_phase:Ljava/lang/String;

    if-eqz v1, :cond_17

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_17

    :cond_17
    const/4 v1, 0x0

    :goto_17
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 643
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->custom_tender_options_proto:Ljava/lang/String;

    if-eqz v1, :cond_18

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_18

    :cond_18
    const/4 v1, 0x0

    :goto_18
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 644
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->skip_signature_always:Ljava/lang/Boolean;

    if-eqz v1, :cond_19

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_19

    :cond_19
    const/4 v1, 0x0

    :goto_19
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 645
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->use_open_tickets_menu_as_home_screen:Ljava/lang/Boolean;

    if-eqz v1, :cond_1a

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1a

    :cond_1a
    const/4 v1, 0x0

    :goto_1a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 646
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->use_predefined_tickets:Ljava/lang/Boolean;

    if-eqz v1, :cond_1b

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1b

    :cond_1b
    const/4 v1, 0x0

    :goto_1b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 647
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->cash_management_enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_1c

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1c

    :cond_1c
    const/4 v1, 0x0

    :goto_1c
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 648
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->cash_management_email_report:Ljava/lang/Boolean;

    if-eqz v1, :cond_1d

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1d

    :cond_1d
    const/4 v1, 0x0

    :goto_1d
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 649
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->cash_management_print_report:Ljava/lang/Boolean;

    if-eqz v1, :cond_1e

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1e

    :cond_1e
    const/4 v1, 0x0

    :goto_1e
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 650
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->cash_management_default_starting_amount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_1f

    invoke-virtual {v1}, Lcom/squareup/protos/common/Money;->hashCode()I

    move-result v1

    goto :goto_1f

    :cond_1f
    const/4 v1, 0x0

    :goto_1f
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 651
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->cash_management_report_recipient_email:Ljava/lang/String;

    if-eqz v1, :cond_20

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_20

    :cond_20
    const/4 v1, 0x0

    :goto_20
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 652
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->customer_management_collect_before_checkout:Ljava/lang/Boolean;

    if-eqz v1, :cond_21

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_21

    :cond_21
    const/4 v1, 0x0

    :goto_21
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 653
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->customer_management_collect_after_checkout:Ljava/lang/Boolean;

    if-eqz v1, :cond_22

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_22

    :cond_22
    const/4 v1, 0x0

    :goto_22
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 654
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->customer_management_use_card_on_file:Ljava/lang/Boolean;

    if-eqz v1, :cond_23

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_23

    :cond_23
    const/4 v1, 0x0

    :goto_23
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 655
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->customer_management_show_save_card_button_after_checkout:Ljava/lang/Boolean;

    if-eqz v1, :cond_24

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_24

    :cond_24
    const/4 v1, 0x0

    :goto_24
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 656
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->customer_management_show_email_collection_screen:Ljava/lang/Boolean;

    if-eqz v1, :cond_25

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_25

    :cond_25
    const/4 v1, 0x0

    :goto_25
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 657
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->allow_instant_deposit:Ljava/lang/Boolean;

    if-eqz v1, :cond_26

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_26

    :cond_26
    const/4 v1, 0x0

    :goto_26
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 658
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->terminal_connected_mode_enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_27

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_27

    :cond_27
    const/4 v1, 0x0

    :goto_27
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 659
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->amended_reopens_enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_28

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_28

    :cond_28
    const/4 v1, 0x0

    :goto_28
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 660
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->behavior_after_adding_item:Ljava/lang/String;

    if-eqz v1, :cond_29

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_29

    :cond_29
    const/4 v1, 0x0

    :goto_29
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 661
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->use_order_hub:Ljava/lang/Boolean;

    if-eqz v1, :cond_2a

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2a

    :cond_2a
    const/4 v1, 0x0

    :goto_2a
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 662
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->employee_management_requires_employee_logout_after_completed_transaction:Ljava/lang/Boolean;

    if-eqz v1, :cond_2b

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2b

    :cond_2b
    const/4 v1, 0x0

    :goto_2b
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 663
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->skip_receipt_screen:Ljava/lang/Boolean;

    if-eqz v1, :cond_2c

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_2c
    add-int/2addr v0, v2

    .line 664
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_2d
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/Preferences$Builder;
    .locals 2

    .line 508
    new-instance v0, Lcom/squareup/server/account/protos/Preferences$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/Preferences$Builder;-><init>()V

    .line 509
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->success:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Preferences$Builder;->success:Ljava/lang/Boolean;

    .line 510
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->title:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Preferences$Builder;->title:Ljava/lang/String;

    .line 511
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->message:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Preferences$Builder;->message:Ljava/lang/String;

    .line 512
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->error_title:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Preferences$Builder;->error_title:Ljava/lang/String;

    .line 513
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->error_message:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Preferences$Builder;->error_message:Ljava/lang/String;

    .line 514
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->tipping_enabled:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Preferences$Builder;->tipping_enabled:Ljava/lang/Boolean;

    .line 515
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->tipping_use_custom_percentages:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Preferences$Builder;->tipping_use_custom_percentages:Ljava/lang/Boolean;

    .line 516
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->tipping_use_manual_tip_entry:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Preferences$Builder;->tipping_use_manual_tip_entry:Ljava/lang/Boolean;

    .line 517
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->tipping_custom_percentages:Ljava/util/List;

    invoke-static {v1}, Lcom/squareup/wire/internal/Internal;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/server/account/protos/Preferences$Builder;->tipping_custom_percentages:Ljava/util/List;

    .line 518
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->skip_signature:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Preferences$Builder;->skip_signature:Ljava/lang/Boolean;

    .line 519
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->store_and_forward_enabled:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Preferences$Builder;->store_and_forward_enabled:Ljava/lang/Boolean;

    .line 520
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->store_and_forward_single_transaction_limit:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Preferences$Builder;->store_and_forward_single_transaction_limit:Lcom/squareup/protos/common/Money;

    .line 521
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->use_curated_image_for_receipt:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Preferences$Builder;->use_curated_image_for_receipt:Ljava/lang/Boolean;

    .line 522
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->use_separate_tipping_screen:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Preferences$Builder;->use_separate_tipping_screen:Ljava/lang/Boolean;

    .line 523
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->open_tickets_enabled:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Preferences$Builder;->open_tickets_enabled:Ljava/lang/Boolean;

    .line 524
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->use_paper_signature:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Preferences$Builder;->use_paper_signature:Ljava/lang/Boolean;

    .line 525
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->for_paper_signature_always_print_customer_copy:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Preferences$Builder;->for_paper_signature_always_print_customer_copy:Ljava/lang/Boolean;

    .line 526
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->for_paper_signature_use_quick_tip_receipt:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Preferences$Builder;->for_paper_signature_use_quick_tip_receipt:Ljava/lang/Boolean;

    .line 527
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->for_paper_signature_print_additional_auth_slip:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Preferences$Builder;->for_paper_signature_print_additional_auth_slip:Ljava/lang/Boolean;

    .line 528
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->employee_management_enabled_for_account:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Preferences$Builder;->employee_management_enabled_for_account:Ljava/lang/Boolean;

    .line 529
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->employee_management_enabled_for_device:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Preferences$Builder;->employee_management_enabled_for_device:Ljava/lang/Boolean;

    .line 530
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->employee_management_inactivity_timeout:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Preferences$Builder;->employee_management_inactivity_timeout:Ljava/lang/String;

    .line 531
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->employee_management_tracking_level:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Preferences$Builder;->employee_management_tracking_level:Ljava/lang/String;

    .line 532
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->use_allow_swipe_for_chip_cards:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Preferences$Builder;->use_allow_swipe_for_chip_cards:Ljava/lang/Boolean;

    .line 533
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->tipping_calculation_phase:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Preferences$Builder;->tipping_calculation_phase:Ljava/lang/String;

    .line 534
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->custom_tender_options_proto:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Preferences$Builder;->custom_tender_options_proto:Ljava/lang/String;

    .line 535
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->skip_signature_always:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Preferences$Builder;->skip_signature_always:Ljava/lang/Boolean;

    .line 536
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->use_open_tickets_menu_as_home_screen:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Preferences$Builder;->use_open_tickets_menu_as_home_screen:Ljava/lang/Boolean;

    .line 537
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->use_predefined_tickets:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Preferences$Builder;->use_predefined_tickets:Ljava/lang/Boolean;

    .line 538
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->cash_management_enabled:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Preferences$Builder;->cash_management_enabled:Ljava/lang/Boolean;

    .line 539
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->cash_management_email_report:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Preferences$Builder;->cash_management_email_report:Ljava/lang/Boolean;

    .line 540
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->cash_management_print_report:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Preferences$Builder;->cash_management_print_report:Ljava/lang/Boolean;

    .line 541
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->cash_management_default_starting_amount:Lcom/squareup/protos/common/Money;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Preferences$Builder;->cash_management_default_starting_amount:Lcom/squareup/protos/common/Money;

    .line 542
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->cash_management_report_recipient_email:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Preferences$Builder;->cash_management_report_recipient_email:Ljava/lang/String;

    .line 543
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->customer_management_collect_before_checkout:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Preferences$Builder;->customer_management_collect_before_checkout:Ljava/lang/Boolean;

    .line 544
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->customer_management_collect_after_checkout:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Preferences$Builder;->customer_management_collect_after_checkout:Ljava/lang/Boolean;

    .line 545
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->customer_management_use_card_on_file:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Preferences$Builder;->customer_management_use_card_on_file:Ljava/lang/Boolean;

    .line 546
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->customer_management_show_save_card_button_after_checkout:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Preferences$Builder;->customer_management_show_save_card_button_after_checkout:Ljava/lang/Boolean;

    .line 547
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->customer_management_show_email_collection_screen:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Preferences$Builder;->customer_management_show_email_collection_screen:Ljava/lang/Boolean;

    .line 548
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->allow_instant_deposit:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Preferences$Builder;->allow_instant_deposit:Ljava/lang/Boolean;

    .line 549
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->terminal_connected_mode_enabled:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Preferences$Builder;->terminal_connected_mode_enabled:Ljava/lang/Boolean;

    .line 550
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->amended_reopens_enabled:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Preferences$Builder;->amended_reopens_enabled:Ljava/lang/Boolean;

    .line 551
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->behavior_after_adding_item:Ljava/lang/String;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Preferences$Builder;->behavior_after_adding_item:Ljava/lang/String;

    .line 552
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->use_order_hub:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Preferences$Builder;->use_order_hub:Ljava/lang/Boolean;

    .line 553
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->employee_management_requires_employee_logout_after_completed_transaction:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Preferences$Builder;->employee_management_requires_employee_logout_after_completed_transaction:Ljava/lang/Boolean;

    .line 554
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->skip_receipt_screen:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/Preferences$Builder;->skip_receipt_screen:Ljava/lang/Boolean;

    .line 555
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/Preferences;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 33
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/Preferences;->newBuilder()Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/Preferences;)Lcom/squareup/server/account/protos/Preferences;
    .locals 2

    .line 741
    iget-object v0, p1, Lcom/squareup/server/account/protos/Preferences;->success:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Preferences;->requireBuilder(Lcom/squareup/server/account/protos/Preferences$Builder;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Preferences;->success:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->success(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v1

    .line 742
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/Preferences;->title:Ljava/lang/String;

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Preferences;->requireBuilder(Lcom/squareup/server/account/protos/Preferences$Builder;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Preferences;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->title(Ljava/lang/String;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v1

    .line 743
    :cond_1
    iget-object v0, p1, Lcom/squareup/server/account/protos/Preferences;->message:Ljava/lang/String;

    if-eqz v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Preferences;->requireBuilder(Lcom/squareup/server/account/protos/Preferences$Builder;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Preferences;->message:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->message(Ljava/lang/String;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v1

    .line 744
    :cond_2
    iget-object v0, p1, Lcom/squareup/server/account/protos/Preferences;->error_title:Ljava/lang/String;

    if-eqz v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Preferences;->requireBuilder(Lcom/squareup/server/account/protos/Preferences$Builder;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Preferences;->error_title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->error_title(Ljava/lang/String;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v1

    .line 745
    :cond_3
    iget-object v0, p1, Lcom/squareup/server/account/protos/Preferences;->error_message:Ljava/lang/String;

    if-eqz v0, :cond_4

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Preferences;->requireBuilder(Lcom/squareup/server/account/protos/Preferences$Builder;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Preferences;->error_message:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->error_message(Ljava/lang/String;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v1

    .line 746
    :cond_4
    iget-object v0, p1, Lcom/squareup/server/account/protos/Preferences;->tipping_enabled:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Preferences;->requireBuilder(Lcom/squareup/server/account/protos/Preferences$Builder;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Preferences;->tipping_enabled:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->tipping_enabled(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v1

    .line 747
    :cond_5
    iget-object v0, p1, Lcom/squareup/server/account/protos/Preferences;->tipping_use_custom_percentages:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Preferences;->requireBuilder(Lcom/squareup/server/account/protos/Preferences$Builder;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Preferences;->tipping_use_custom_percentages:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->tipping_use_custom_percentages(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v1

    .line 748
    :cond_6
    iget-object v0, p1, Lcom/squareup/server/account/protos/Preferences;->tipping_use_manual_tip_entry:Ljava/lang/Boolean;

    if-eqz v0, :cond_7

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Preferences;->requireBuilder(Lcom/squareup/server/account/protos/Preferences$Builder;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Preferences;->tipping_use_manual_tip_entry:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->tipping_use_manual_tip_entry(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v1

    .line 749
    :cond_7
    iget-object v0, p1, Lcom/squareup/server/account/protos/Preferences;->tipping_custom_percentages:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Preferences;->requireBuilder(Lcom/squareup/server/account/protos/Preferences$Builder;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Preferences;->tipping_custom_percentages:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->tipping_custom_percentages(Ljava/util/List;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v1

    .line 750
    :cond_8
    iget-object v0, p1, Lcom/squareup/server/account/protos/Preferences;->skip_signature:Ljava/lang/Boolean;

    if-eqz v0, :cond_9

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Preferences;->requireBuilder(Lcom/squareup/server/account/protos/Preferences$Builder;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Preferences;->skip_signature:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->skip_signature(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v1

    .line 751
    :cond_9
    iget-object v0, p1, Lcom/squareup/server/account/protos/Preferences;->store_and_forward_enabled:Ljava/lang/Boolean;

    if-eqz v0, :cond_a

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Preferences;->requireBuilder(Lcom/squareup/server/account/protos/Preferences$Builder;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Preferences;->store_and_forward_enabled:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->store_and_forward_enabled(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v1

    .line 752
    :cond_a
    iget-object v0, p1, Lcom/squareup/server/account/protos/Preferences;->store_and_forward_single_transaction_limit:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_b

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Preferences;->requireBuilder(Lcom/squareup/server/account/protos/Preferences$Builder;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Preferences;->store_and_forward_single_transaction_limit:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->store_and_forward_single_transaction_limit(Lcom/squareup/protos/common/Money;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v1

    .line 753
    :cond_b
    iget-object v0, p1, Lcom/squareup/server/account/protos/Preferences;->use_curated_image_for_receipt:Ljava/lang/Boolean;

    if-eqz v0, :cond_c

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Preferences;->requireBuilder(Lcom/squareup/server/account/protos/Preferences$Builder;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Preferences;->use_curated_image_for_receipt:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->use_curated_image_for_receipt(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v1

    .line 754
    :cond_c
    iget-object v0, p1, Lcom/squareup/server/account/protos/Preferences;->use_separate_tipping_screen:Ljava/lang/Boolean;

    if-eqz v0, :cond_d

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Preferences;->requireBuilder(Lcom/squareup/server/account/protos/Preferences$Builder;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Preferences;->use_separate_tipping_screen:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->use_separate_tipping_screen(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v1

    .line 755
    :cond_d
    iget-object v0, p1, Lcom/squareup/server/account/protos/Preferences;->open_tickets_enabled:Ljava/lang/Boolean;

    if-eqz v0, :cond_e

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Preferences;->requireBuilder(Lcom/squareup/server/account/protos/Preferences$Builder;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Preferences;->open_tickets_enabled:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->open_tickets_enabled(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v1

    .line 756
    :cond_e
    iget-object v0, p1, Lcom/squareup/server/account/protos/Preferences;->use_paper_signature:Ljava/lang/Boolean;

    if-eqz v0, :cond_f

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Preferences;->requireBuilder(Lcom/squareup/server/account/protos/Preferences$Builder;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Preferences;->use_paper_signature:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->use_paper_signature(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v1

    .line 757
    :cond_f
    iget-object v0, p1, Lcom/squareup/server/account/protos/Preferences;->for_paper_signature_always_print_customer_copy:Ljava/lang/Boolean;

    if-eqz v0, :cond_10

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Preferences;->requireBuilder(Lcom/squareup/server/account/protos/Preferences$Builder;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Preferences;->for_paper_signature_always_print_customer_copy:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->for_paper_signature_always_print_customer_copy(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v1

    .line 758
    :cond_10
    iget-object v0, p1, Lcom/squareup/server/account/protos/Preferences;->for_paper_signature_use_quick_tip_receipt:Ljava/lang/Boolean;

    if-eqz v0, :cond_11

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Preferences;->requireBuilder(Lcom/squareup/server/account/protos/Preferences$Builder;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Preferences;->for_paper_signature_use_quick_tip_receipt:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->for_paper_signature_use_quick_tip_receipt(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v1

    .line 759
    :cond_11
    iget-object v0, p1, Lcom/squareup/server/account/protos/Preferences;->for_paper_signature_print_additional_auth_slip:Ljava/lang/Boolean;

    if-eqz v0, :cond_12

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Preferences;->requireBuilder(Lcom/squareup/server/account/protos/Preferences$Builder;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Preferences;->for_paper_signature_print_additional_auth_slip:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->for_paper_signature_print_additional_auth_slip(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v1

    .line 760
    :cond_12
    iget-object v0, p1, Lcom/squareup/server/account/protos/Preferences;->employee_management_enabled_for_account:Ljava/lang/Boolean;

    if-eqz v0, :cond_13

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Preferences;->requireBuilder(Lcom/squareup/server/account/protos/Preferences$Builder;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Preferences;->employee_management_enabled_for_account:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->employee_management_enabled_for_account(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v1

    .line 761
    :cond_13
    iget-object v0, p1, Lcom/squareup/server/account/protos/Preferences;->employee_management_enabled_for_device:Ljava/lang/Boolean;

    if-eqz v0, :cond_14

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Preferences;->requireBuilder(Lcom/squareup/server/account/protos/Preferences$Builder;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Preferences;->employee_management_enabled_for_device:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->employee_management_enabled_for_device(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v1

    .line 762
    :cond_14
    iget-object v0, p1, Lcom/squareup/server/account/protos/Preferences;->employee_management_inactivity_timeout:Ljava/lang/String;

    if-eqz v0, :cond_15

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Preferences;->requireBuilder(Lcom/squareup/server/account/protos/Preferences$Builder;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Preferences;->employee_management_inactivity_timeout:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->employee_management_inactivity_timeout(Ljava/lang/String;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v1

    .line 763
    :cond_15
    iget-object v0, p1, Lcom/squareup/server/account/protos/Preferences;->employee_management_tracking_level:Ljava/lang/String;

    if-eqz v0, :cond_16

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Preferences;->requireBuilder(Lcom/squareup/server/account/protos/Preferences$Builder;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Preferences;->employee_management_tracking_level:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->employee_management_tracking_level(Ljava/lang/String;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v1

    .line 764
    :cond_16
    iget-object v0, p1, Lcom/squareup/server/account/protos/Preferences;->use_allow_swipe_for_chip_cards:Ljava/lang/Boolean;

    if-eqz v0, :cond_17

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Preferences;->requireBuilder(Lcom/squareup/server/account/protos/Preferences$Builder;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Preferences;->use_allow_swipe_for_chip_cards:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->use_allow_swipe_for_chip_cards(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v1

    .line 765
    :cond_17
    iget-object v0, p1, Lcom/squareup/server/account/protos/Preferences;->tipping_calculation_phase:Ljava/lang/String;

    if-eqz v0, :cond_18

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Preferences;->requireBuilder(Lcom/squareup/server/account/protos/Preferences$Builder;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Preferences;->tipping_calculation_phase:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->tipping_calculation_phase(Ljava/lang/String;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v1

    .line 766
    :cond_18
    iget-object v0, p1, Lcom/squareup/server/account/protos/Preferences;->custom_tender_options_proto:Ljava/lang/String;

    if-eqz v0, :cond_19

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Preferences;->requireBuilder(Lcom/squareup/server/account/protos/Preferences$Builder;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Preferences;->custom_tender_options_proto:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->custom_tender_options_proto(Ljava/lang/String;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v1

    .line 767
    :cond_19
    iget-object v0, p1, Lcom/squareup/server/account/protos/Preferences;->skip_signature_always:Ljava/lang/Boolean;

    if-eqz v0, :cond_1a

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Preferences;->requireBuilder(Lcom/squareup/server/account/protos/Preferences$Builder;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Preferences;->skip_signature_always:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->skip_signature_always(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v1

    .line 768
    :cond_1a
    iget-object v0, p1, Lcom/squareup/server/account/protos/Preferences;->use_open_tickets_menu_as_home_screen:Ljava/lang/Boolean;

    if-eqz v0, :cond_1b

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Preferences;->requireBuilder(Lcom/squareup/server/account/protos/Preferences$Builder;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Preferences;->use_open_tickets_menu_as_home_screen:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->use_open_tickets_menu_as_home_screen(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v1

    .line 769
    :cond_1b
    iget-object v0, p1, Lcom/squareup/server/account/protos/Preferences;->use_predefined_tickets:Ljava/lang/Boolean;

    if-eqz v0, :cond_1c

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Preferences;->requireBuilder(Lcom/squareup/server/account/protos/Preferences$Builder;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Preferences;->use_predefined_tickets:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->use_predefined_tickets(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v1

    .line 770
    :cond_1c
    iget-object v0, p1, Lcom/squareup/server/account/protos/Preferences;->cash_management_enabled:Ljava/lang/Boolean;

    if-eqz v0, :cond_1d

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Preferences;->requireBuilder(Lcom/squareup/server/account/protos/Preferences$Builder;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Preferences;->cash_management_enabled:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->cash_management_enabled(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v1

    .line 771
    :cond_1d
    iget-object v0, p1, Lcom/squareup/server/account/protos/Preferences;->cash_management_email_report:Ljava/lang/Boolean;

    if-eqz v0, :cond_1e

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Preferences;->requireBuilder(Lcom/squareup/server/account/protos/Preferences$Builder;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Preferences;->cash_management_email_report:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->cash_management_email_report(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v1

    .line 772
    :cond_1e
    iget-object v0, p1, Lcom/squareup/server/account/protos/Preferences;->cash_management_print_report:Ljava/lang/Boolean;

    if-eqz v0, :cond_1f

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Preferences;->requireBuilder(Lcom/squareup/server/account/protos/Preferences$Builder;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Preferences;->cash_management_print_report:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->cash_management_print_report(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v1

    .line 773
    :cond_1f
    iget-object v0, p1, Lcom/squareup/server/account/protos/Preferences;->cash_management_default_starting_amount:Lcom/squareup/protos/common/Money;

    if-eqz v0, :cond_20

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Preferences;->requireBuilder(Lcom/squareup/server/account/protos/Preferences$Builder;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Preferences;->cash_management_default_starting_amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->cash_management_default_starting_amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v1

    .line 774
    :cond_20
    iget-object v0, p1, Lcom/squareup/server/account/protos/Preferences;->cash_management_report_recipient_email:Ljava/lang/String;

    if-eqz v0, :cond_21

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Preferences;->requireBuilder(Lcom/squareup/server/account/protos/Preferences$Builder;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Preferences;->cash_management_report_recipient_email:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->cash_management_report_recipient_email(Ljava/lang/String;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v1

    .line 775
    :cond_21
    iget-object v0, p1, Lcom/squareup/server/account/protos/Preferences;->customer_management_collect_before_checkout:Ljava/lang/Boolean;

    if-eqz v0, :cond_22

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Preferences;->requireBuilder(Lcom/squareup/server/account/protos/Preferences$Builder;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Preferences;->customer_management_collect_before_checkout:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->customer_management_collect_before_checkout(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v1

    .line 776
    :cond_22
    iget-object v0, p1, Lcom/squareup/server/account/protos/Preferences;->customer_management_collect_after_checkout:Ljava/lang/Boolean;

    if-eqz v0, :cond_23

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Preferences;->requireBuilder(Lcom/squareup/server/account/protos/Preferences$Builder;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Preferences;->customer_management_collect_after_checkout:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->customer_management_collect_after_checkout(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v1

    .line 777
    :cond_23
    iget-object v0, p1, Lcom/squareup/server/account/protos/Preferences;->customer_management_use_card_on_file:Ljava/lang/Boolean;

    if-eqz v0, :cond_24

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Preferences;->requireBuilder(Lcom/squareup/server/account/protos/Preferences$Builder;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Preferences;->customer_management_use_card_on_file:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->customer_management_use_card_on_file(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v1

    .line 778
    :cond_24
    iget-object v0, p1, Lcom/squareup/server/account/protos/Preferences;->customer_management_show_save_card_button_after_checkout:Ljava/lang/Boolean;

    if-eqz v0, :cond_25

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Preferences;->requireBuilder(Lcom/squareup/server/account/protos/Preferences$Builder;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Preferences;->customer_management_show_save_card_button_after_checkout:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->customer_management_show_save_card_button_after_checkout(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v1

    .line 779
    :cond_25
    iget-object v0, p1, Lcom/squareup/server/account/protos/Preferences;->customer_management_show_email_collection_screen:Ljava/lang/Boolean;

    if-eqz v0, :cond_26

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Preferences;->requireBuilder(Lcom/squareup/server/account/protos/Preferences$Builder;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Preferences;->customer_management_show_email_collection_screen:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->customer_management_show_email_collection_screen(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v1

    .line 780
    :cond_26
    iget-object v0, p1, Lcom/squareup/server/account/protos/Preferences;->allow_instant_deposit:Ljava/lang/Boolean;

    if-eqz v0, :cond_27

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Preferences;->requireBuilder(Lcom/squareup/server/account/protos/Preferences$Builder;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Preferences;->allow_instant_deposit:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->allow_instant_deposit(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v1

    .line 781
    :cond_27
    iget-object v0, p1, Lcom/squareup/server/account/protos/Preferences;->terminal_connected_mode_enabled:Ljava/lang/Boolean;

    if-eqz v0, :cond_28

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Preferences;->requireBuilder(Lcom/squareup/server/account/protos/Preferences$Builder;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Preferences;->terminal_connected_mode_enabled:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->terminal_connected_mode_enabled(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v1

    .line 782
    :cond_28
    iget-object v0, p1, Lcom/squareup/server/account/protos/Preferences;->amended_reopens_enabled:Ljava/lang/Boolean;

    if-eqz v0, :cond_29

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Preferences;->requireBuilder(Lcom/squareup/server/account/protos/Preferences$Builder;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Preferences;->amended_reopens_enabled:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->amended_reopens_enabled(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v1

    .line 783
    :cond_29
    iget-object v0, p1, Lcom/squareup/server/account/protos/Preferences;->behavior_after_adding_item:Ljava/lang/String;

    if-eqz v0, :cond_2a

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Preferences;->requireBuilder(Lcom/squareup/server/account/protos/Preferences$Builder;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Preferences;->behavior_after_adding_item:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->behavior_after_adding_item(Ljava/lang/String;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v1

    .line 784
    :cond_2a
    iget-object v0, p1, Lcom/squareup/server/account/protos/Preferences;->use_order_hub:Ljava/lang/Boolean;

    if-eqz v0, :cond_2b

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Preferences;->requireBuilder(Lcom/squareup/server/account/protos/Preferences$Builder;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Preferences;->use_order_hub:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->use_order_hub(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v1

    .line 785
    :cond_2b
    iget-object v0, p1, Lcom/squareup/server/account/protos/Preferences;->employee_management_requires_employee_logout_after_completed_transaction:Ljava/lang/Boolean;

    if-eqz v0, :cond_2c

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Preferences;->requireBuilder(Lcom/squareup/server/account/protos/Preferences$Builder;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/Preferences;->employee_management_requires_employee_logout_after_completed_transaction:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->employee_management_requires_employee_logout_after_completed_transaction(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v1

    .line 786
    :cond_2c
    iget-object v0, p1, Lcom/squareup/server/account/protos/Preferences;->skip_receipt_screen:Ljava/lang/Boolean;

    if-eqz v0, :cond_2d

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Preferences;->requireBuilder(Lcom/squareup/server/account/protos/Preferences$Builder;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/Preferences;->skip_receipt_screen:Ljava/lang/Boolean;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/Preferences$Builder;->skip_receipt_screen(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v1

    :cond_2d
    if-nez v1, :cond_2e

    move-object p1, p0

    goto :goto_0

    .line 787
    :cond_2e
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->build()Lcom/squareup/server/account/protos/Preferences;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 33
    check-cast p1, Lcom/squareup/server/account/protos/Preferences;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/Preferences;->overlay(Lcom/squareup/server/account/protos/Preferences;)Lcom/squareup/server/account/protos/Preferences;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/Preferences;
    .locals 2

    .line 724
    iget-object v0, p0, Lcom/squareup/server/account/protos/Preferences;->success:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Preferences;->requireBuilder(Lcom/squareup/server/account/protos/Preferences$Builder;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/Preferences;->DEFAULT_SUCCESS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->success(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v1

    .line 725
    :cond_0
    iget-object v0, p0, Lcom/squareup/server/account/protos/Preferences;->use_open_tickets_menu_as_home_screen:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Preferences;->requireBuilder(Lcom/squareup/server/account/protos/Preferences$Builder;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/Preferences;->DEFAULT_USE_OPEN_TICKETS_MENU_AS_HOME_SCREEN:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->use_open_tickets_menu_as_home_screen(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v1

    .line 726
    :cond_1
    iget-object v0, p0, Lcom/squareup/server/account/protos/Preferences;->use_predefined_tickets:Ljava/lang/Boolean;

    if-nez v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Preferences;->requireBuilder(Lcom/squareup/server/account/protos/Preferences$Builder;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/Preferences;->DEFAULT_USE_PREDEFINED_TICKETS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->use_predefined_tickets(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v1

    .line 727
    :cond_2
    iget-object v0, p0, Lcom/squareup/server/account/protos/Preferences;->cash_management_enabled:Ljava/lang/Boolean;

    if-nez v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Preferences;->requireBuilder(Lcom/squareup/server/account/protos/Preferences$Builder;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/Preferences;->DEFAULT_CASH_MANAGEMENT_ENABLED:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->cash_management_enabled(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v1

    .line 728
    :cond_3
    iget-object v0, p0, Lcom/squareup/server/account/protos/Preferences;->cash_management_email_report:Ljava/lang/Boolean;

    if-nez v0, :cond_4

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Preferences;->requireBuilder(Lcom/squareup/server/account/protos/Preferences$Builder;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/Preferences;->DEFAULT_CASH_MANAGEMENT_EMAIL_REPORT:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->cash_management_email_report(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v1

    .line 729
    :cond_4
    iget-object v0, p0, Lcom/squareup/server/account/protos/Preferences;->cash_management_print_report:Ljava/lang/Boolean;

    if-nez v0, :cond_5

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Preferences;->requireBuilder(Lcom/squareup/server/account/protos/Preferences$Builder;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/Preferences;->DEFAULT_CASH_MANAGEMENT_PRINT_REPORT:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->cash_management_print_report(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v1

    .line 730
    :cond_5
    iget-object v0, p0, Lcom/squareup/server/account/protos/Preferences;->customer_management_collect_before_checkout:Ljava/lang/Boolean;

    if-nez v0, :cond_6

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Preferences;->requireBuilder(Lcom/squareup/server/account/protos/Preferences$Builder;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/Preferences;->DEFAULT_CUSTOMER_MANAGEMENT_COLLECT_BEFORE_CHECKOUT:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->customer_management_collect_before_checkout(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v1

    .line 731
    :cond_6
    iget-object v0, p0, Lcom/squareup/server/account/protos/Preferences;->customer_management_collect_after_checkout:Ljava/lang/Boolean;

    if-nez v0, :cond_7

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Preferences;->requireBuilder(Lcom/squareup/server/account/protos/Preferences$Builder;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/Preferences;->DEFAULT_CUSTOMER_MANAGEMENT_COLLECT_AFTER_CHECKOUT:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->customer_management_collect_after_checkout(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v1

    .line 732
    :cond_7
    iget-object v0, p0, Lcom/squareup/server/account/protos/Preferences;->customer_management_use_card_on_file:Ljava/lang/Boolean;

    if-nez v0, :cond_8

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Preferences;->requireBuilder(Lcom/squareup/server/account/protos/Preferences$Builder;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/Preferences;->DEFAULT_CUSTOMER_MANAGEMENT_USE_CARD_ON_FILE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->customer_management_use_card_on_file(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v1

    .line 733
    :cond_8
    iget-object v0, p0, Lcom/squareup/server/account/protos/Preferences;->customer_management_show_save_card_button_after_checkout:Ljava/lang/Boolean;

    if-nez v0, :cond_9

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Preferences;->requireBuilder(Lcom/squareup/server/account/protos/Preferences$Builder;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/Preferences;->DEFAULT_CUSTOMER_MANAGEMENT_SHOW_SAVE_CARD_BUTTON_AFTER_CHECKOUT:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->customer_management_show_save_card_button_after_checkout(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v1

    .line 734
    :cond_9
    iget-object v0, p0, Lcom/squareup/server/account/protos/Preferences;->customer_management_show_email_collection_screen:Ljava/lang/Boolean;

    if-nez v0, :cond_a

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/Preferences;->requireBuilder(Lcom/squareup/server/account/protos/Preferences$Builder;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/Preferences;->DEFAULT_CUSTOMER_MANAGEMENT_SHOW_EMAIL_COLLECTION_SCREEN:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->customer_management_show_email_collection_screen(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v1

    :cond_a
    if-nez v1, :cond_b

    move-object v0, p0

    goto :goto_0

    .line 735
    :cond_b
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->build()Lcom/squareup/server/account/protos/Preferences;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 33
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/Preferences;->populateDefaults()Lcom/squareup/server/account/protos/Preferences;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 671
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 672
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->success:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const-string v1, ", success="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->success:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 673
    :cond_0
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->title:Ljava/lang/String;

    if-eqz v1, :cond_1

    const-string v1, ", title="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 674
    :cond_1
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->message:Ljava/lang/String;

    if-eqz v1, :cond_2

    const-string v1, ", message="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->message:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 675
    :cond_2
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->error_title:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string v1, ", error_title="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->error_title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 676
    :cond_3
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->error_message:Ljava/lang/String;

    if-eqz v1, :cond_4

    const-string v1, ", error_message="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->error_message:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 677
    :cond_4
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->tipping_enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    const-string v1, ", tipping_enabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->tipping_enabled:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 678
    :cond_5
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->tipping_use_custom_percentages:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    const-string v1, ", tipping_use_custom_percentages="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->tipping_use_custom_percentages:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 679
    :cond_6
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->tipping_use_manual_tip_entry:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    const-string v1, ", tipping_use_manual_tip_entry="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->tipping_use_manual_tip_entry:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 680
    :cond_7
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->tipping_custom_percentages:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_8

    const-string v1, ", tipping_custom_percentages="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->tipping_custom_percentages:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 681
    :cond_8
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->skip_signature:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    const-string v1, ", skip_signature="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->skip_signature:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 682
    :cond_9
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->store_and_forward_enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_a

    const-string v1, ", store_and_forward_enabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->store_and_forward_enabled:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 683
    :cond_a
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->store_and_forward_single_transaction_limit:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_b

    const-string v1, ", store_and_forward_single_transaction_limit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->store_and_forward_single_transaction_limit:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 684
    :cond_b
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->use_curated_image_for_receipt:Ljava/lang/Boolean;

    if-eqz v1, :cond_c

    const-string v1, ", use_curated_image_for_receipt="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->use_curated_image_for_receipt:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 685
    :cond_c
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->use_separate_tipping_screen:Ljava/lang/Boolean;

    if-eqz v1, :cond_d

    const-string v1, ", use_separate_tipping_screen="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->use_separate_tipping_screen:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 686
    :cond_d
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->open_tickets_enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_e

    const-string v1, ", open_tickets_enabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->open_tickets_enabled:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 687
    :cond_e
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->use_paper_signature:Ljava/lang/Boolean;

    if-eqz v1, :cond_f

    const-string v1, ", use_paper_signature="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->use_paper_signature:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 688
    :cond_f
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->for_paper_signature_always_print_customer_copy:Ljava/lang/Boolean;

    if-eqz v1, :cond_10

    const-string v1, ", for_paper_signature_always_print_customer_copy="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->for_paper_signature_always_print_customer_copy:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 689
    :cond_10
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->for_paper_signature_use_quick_tip_receipt:Ljava/lang/Boolean;

    if-eqz v1, :cond_11

    const-string v1, ", for_paper_signature_use_quick_tip_receipt="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->for_paper_signature_use_quick_tip_receipt:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 690
    :cond_11
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->for_paper_signature_print_additional_auth_slip:Ljava/lang/Boolean;

    if-eqz v1, :cond_12

    const-string v1, ", for_paper_signature_print_additional_auth_slip="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->for_paper_signature_print_additional_auth_slip:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 691
    :cond_12
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->employee_management_enabled_for_account:Ljava/lang/Boolean;

    if-eqz v1, :cond_13

    const-string v1, ", employee_management_enabled_for_account="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->employee_management_enabled_for_account:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 692
    :cond_13
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->employee_management_enabled_for_device:Ljava/lang/Boolean;

    if-eqz v1, :cond_14

    const-string v1, ", employee_management_enabled_for_device="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->employee_management_enabled_for_device:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 693
    :cond_14
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->employee_management_inactivity_timeout:Ljava/lang/String;

    if-eqz v1, :cond_15

    const-string v1, ", employee_management_inactivity_timeout="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->employee_management_inactivity_timeout:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 694
    :cond_15
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->employee_management_tracking_level:Ljava/lang/String;

    if-eqz v1, :cond_16

    const-string v1, ", employee_management_tracking_level="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->employee_management_tracking_level:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 695
    :cond_16
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->use_allow_swipe_for_chip_cards:Ljava/lang/Boolean;

    if-eqz v1, :cond_17

    const-string v1, ", use_allow_swipe_for_chip_cards="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->use_allow_swipe_for_chip_cards:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 696
    :cond_17
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->tipping_calculation_phase:Ljava/lang/String;

    if-eqz v1, :cond_18

    const-string v1, ", tipping_calculation_phase="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->tipping_calculation_phase:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 697
    :cond_18
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->custom_tender_options_proto:Ljava/lang/String;

    if-eqz v1, :cond_19

    const-string v1, ", custom_tender_options_proto="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->custom_tender_options_proto:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 698
    :cond_19
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->skip_signature_always:Ljava/lang/Boolean;

    if-eqz v1, :cond_1a

    const-string v1, ", skip_signature_always="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->skip_signature_always:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 699
    :cond_1a
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->use_open_tickets_menu_as_home_screen:Ljava/lang/Boolean;

    if-eqz v1, :cond_1b

    const-string v1, ", use_open_tickets_menu_as_home_screen="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->use_open_tickets_menu_as_home_screen:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 700
    :cond_1b
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->use_predefined_tickets:Ljava/lang/Boolean;

    if-eqz v1, :cond_1c

    const-string v1, ", use_predefined_tickets="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->use_predefined_tickets:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 701
    :cond_1c
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->cash_management_enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_1d

    const-string v1, ", cash_management_enabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->cash_management_enabled:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 702
    :cond_1d
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->cash_management_email_report:Ljava/lang/Boolean;

    if-eqz v1, :cond_1e

    const-string v1, ", cash_management_email_report="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->cash_management_email_report:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 703
    :cond_1e
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->cash_management_print_report:Ljava/lang/Boolean;

    if-eqz v1, :cond_1f

    const-string v1, ", cash_management_print_report="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->cash_management_print_report:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 704
    :cond_1f
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->cash_management_default_starting_amount:Lcom/squareup/protos/common/Money;

    if-eqz v1, :cond_20

    const-string v1, ", cash_management_default_starting_amount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->cash_management_default_starting_amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 705
    :cond_20
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->cash_management_report_recipient_email:Ljava/lang/String;

    if-eqz v1, :cond_21

    const-string v1, ", cash_management_report_recipient_email="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->cash_management_report_recipient_email:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 706
    :cond_21
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->customer_management_collect_before_checkout:Ljava/lang/Boolean;

    if-eqz v1, :cond_22

    const-string v1, ", customer_management_collect_before_checkout="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->customer_management_collect_before_checkout:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 707
    :cond_22
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->customer_management_collect_after_checkout:Ljava/lang/Boolean;

    if-eqz v1, :cond_23

    const-string v1, ", customer_management_collect_after_checkout="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->customer_management_collect_after_checkout:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 708
    :cond_23
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->customer_management_use_card_on_file:Ljava/lang/Boolean;

    if-eqz v1, :cond_24

    const-string v1, ", customer_management_use_card_on_file="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->customer_management_use_card_on_file:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 709
    :cond_24
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->customer_management_show_save_card_button_after_checkout:Ljava/lang/Boolean;

    if-eqz v1, :cond_25

    const-string v1, ", customer_management_show_save_card_button_after_checkout="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->customer_management_show_save_card_button_after_checkout:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 710
    :cond_25
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->customer_management_show_email_collection_screen:Ljava/lang/Boolean;

    if-eqz v1, :cond_26

    const-string v1, ", customer_management_show_email_collection_screen="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->customer_management_show_email_collection_screen:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 711
    :cond_26
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->allow_instant_deposit:Ljava/lang/Boolean;

    if-eqz v1, :cond_27

    const-string v1, ", allow_instant_deposit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->allow_instant_deposit:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 712
    :cond_27
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->terminal_connected_mode_enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_28

    const-string v1, ", terminal_connected_mode_enabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->terminal_connected_mode_enabled:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 713
    :cond_28
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->amended_reopens_enabled:Ljava/lang/Boolean;

    if-eqz v1, :cond_29

    const-string v1, ", amended_reopens_enabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->amended_reopens_enabled:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 714
    :cond_29
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->behavior_after_adding_item:Ljava/lang/String;

    if-eqz v1, :cond_2a

    const-string v1, ", behavior_after_adding_item="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->behavior_after_adding_item:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 715
    :cond_2a
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->use_order_hub:Ljava/lang/Boolean;

    if-eqz v1, :cond_2b

    const-string v1, ", use_order_hub="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->use_order_hub:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 716
    :cond_2b
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->employee_management_requires_employee_logout_after_completed_transaction:Ljava/lang/Boolean;

    if-eqz v1, :cond_2c

    const-string v1, ", employee_management_requires_employee_logout_after_completed_transaction="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->employee_management_requires_employee_logout_after_completed_transaction:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 717
    :cond_2c
    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->skip_receipt_screen:Ljava/lang/Boolean;

    if-eqz v1, :cond_2d

    const-string v1, ", skip_receipt_screen="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/Preferences;->skip_receipt_screen:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2d
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Preferences{"

    .line 718
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
