.class public final Lcom/squareup/server/account/AuthenticationService$SendVerificationCodeTwoFactorStandardResponse;
.super Lcom/squareup/server/StandardResponse;
.source "AuthenticationService.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/AuthenticationService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SendVerificationCodeTwoFactorStandardResponse"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/server/StandardResponse<",
        "Lcom/squareup/util/Optional<",
        "+",
        "Lcom/squareup/protos/register/api/SendVerificationCodeTwoFactorResponse;",
        ">;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nAuthenticationService.kt\nKotlin\n*S Kotlin\n*F\n+ 1 AuthenticationService.kt\ncom/squareup/server/account/AuthenticationService$SendVerificationCodeTwoFactorStandardResponse\n*L\n1#1,90:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\u0002\u0018\u00002\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00030\u00020\u0001B\u0019\u0012\u0012\u0010\u0004\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00020\u00030\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0016\u0010\u0007\u001a\u00020\u00082\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u00030\u0002H\u0014\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/server/account/AuthenticationService$SendVerificationCodeTwoFactorStandardResponse;",
        "Lcom/squareup/server/StandardResponse;",
        "Lcom/squareup/util/Optional;",
        "Lcom/squareup/protos/register/api/SendVerificationCodeTwoFactorResponse;",
        "factory",
        "Lcom/squareup/server/StandardResponse$Factory;",
        "(Lcom/squareup/server/StandardResponse$Factory;)V",
        "isSuccessful",
        "",
        "response",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/server/StandardResponse$Factory;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/server/StandardResponse$Factory<",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/protos/register/api/SendVerificationCodeTwoFactorResponse;",
            ">;>;)V"
        }
    .end annotation

    const-string v0, "factory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 85
    invoke-direct {p0, p1}, Lcom/squareup/server/StandardResponse;-><init>(Lcom/squareup/server/StandardResponse$Factory;)V

    return-void
.end method


# virtual methods
.method protected isSuccessful(Lcom/squareup/util/Optional;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/util/Optional<",
            "Lcom/squareup/protos/register/api/SendVerificationCodeTwoFactorResponse;",
            ">;)Z"
        }
    .end annotation

    const-string v0, "response"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 87
    invoke-virtual {p1}, Lcom/squareup/util/Optional;->getValueOrNull()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/protos/register/api/SendVerificationCodeTwoFactorResponse;

    if-eqz p1, :cond_0

    iget-object p1, p1, Lcom/squareup/protos/register/api/SendVerificationCodeTwoFactorResponse;->error_title:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    check-cast p1, Ljava/lang/CharSequence;

    if-eqz p1, :cond_2

    invoke-static {p1}, Lkotlin/text/StringsKt;->isBlank(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    goto :goto_2

    :cond_2
    :goto_1
    const/4 p1, 0x1

    :goto_2
    return p1
.end method

.method public bridge synthetic isSuccessful(Ljava/lang/Object;)Z
    .locals 0

    .line 83
    check-cast p1, Lcom/squareup/util/Optional;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/AuthenticationService$SendVerificationCodeTwoFactorStandardResponse;->isSuccessful(Lcom/squareup/util/Optional;)Z

    move-result p1

    return p1
.end method
