.class public final Lcom/squareup/server/account/PreferenceUtilsKt;
.super Ljava/lang/Object;
.source "PreferenceUtils.kt"


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nPreferenceUtils.kt\nKotlin\n*S Kotlin\n*F\n+ 1 PreferenceUtils.kt\ncom/squareup/server/account/PreferenceUtilsKt\n+ 2 _Collections.kt\nkotlin/collections/CollectionsKt___CollectionsKt\n*L\n1#1,106:1\n1412#2,9:107\n1642#2,2:116\n1421#2:118\n*E\n*S KotlinDebug\n*F\n+ 1 PreferenceUtils.kt\ncom/squareup/server/account/PreferenceUtilsKt\n*L\n43#1,9:107\n43#1,2:116\n43#1:118\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000&\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0010\u0006\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u001a\u001a\u0010\u0002\u001a\n\u0012\u0004\u0012\u00020\u0004\u0018\u00010\u00032\u0008\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0000\u001a\u0012\u0010\u0007\u001a\u00020\u0008*\u00020\u00082\u0006\u0010\t\u001a\u00020\n\u001a\u000c\u0010\u000b\u001a\u00020\u0008*\u00020\nH\u0002\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000c"
    }
    d2 = {
        "gson",
        "Lcom/google/gson/Gson;",
        "listFromJson",
        "",
        "",
        "customTippingPercentages",
        "",
        "overlayPreferences",
        "Lcom/squareup/server/account/protos/Preferences;",
        "overlay",
        "Lcom/squareup/server/account/protos/PreferencesRequest;",
        "toPreferences",
        "services-data_release"
    }
    k = 0x2
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field private static final gson:Lcom/google/gson/Gson;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 10
    new-instance v0, Lcom/google/gson/Gson;

    invoke-direct {v0}, Lcom/google/gson/Gson;-><init>()V

    sput-object v0, Lcom/squareup/server/account/PreferenceUtilsKt;->gson:Lcom/google/gson/Gson;

    return-void
.end method

.method public static final listFromJson(Ljava/lang/String;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    .line 34
    :try_start_0
    sget-object v1, Lcom/squareup/server/account/PreferenceUtilsKt;->gson:Lcom/google/gson/Gson;

    const-class v2, [Ljava/lang/String;

    invoke-virtual {v1, p0, v2}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, [Ljava/lang/String;
    :try_end_0
    .catch Lcom/google/gson/JsonParseException; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz p0, :cond_2

    .line 42
    invoke-static {p0}, Lkotlin/collections/ArraysKt;->filterNotNull([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p0

    check-cast p0, Ljava/lang/Iterable;

    .line 107
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    check-cast v1, Ljava/util/Collection;

    .line 116
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 115
    check-cast v2, Ljava/lang/String;

    .line 45
    :try_start_1
    sget-object v3, Lcom/squareup/util/Percentage;->Companion:Lcom/squareup/util/Percentage$Companion;

    invoke-virtual {v3, v2}, Lcom/squareup/util/Percentage$Companion;->fromString(Ljava/lang/String;)Lcom/squareup/util/Percentage;

    move-result-object v3

    .line 46
    invoke-virtual {v3}, Lcom/squareup/util/Percentage;->doubleValue()D

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    move-exception v3

    .line 48
    check-cast v3, Ljava/lang/Throwable;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unable to parse tip percentage "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;Ljava/lang/String;)V

    move-object v2, v0

    :goto_1
    if-eqz v2, :cond_0

    .line 115
    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 118
    :cond_1
    check-cast v1, Ljava/util/List;

    return-object v1

    :cond_2
    return-object v0

    :catch_1
    move-exception p0

    .line 36
    check-cast p0, Ljava/lang/Throwable;

    invoke-static {p0}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;)V

    return-object v0
.end method

.method public static final overlayPreferences(Lcom/squareup/server/account/protos/Preferences;Lcom/squareup/server/account/protos/PreferencesRequest;)Lcom/squareup/server/account/protos/Preferences;
    .locals 1

    const-string v0, "$this$overlayPreferences"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "overlay"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    invoke-static {p1}, Lcom/squareup/server/account/PreferenceUtilsKt;->toPreferences(Lcom/squareup/server/account/protos/PreferencesRequest;)Lcom/squareup/server/account/protos/Preferences;

    move-result-object p1

    .line 27
    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/Preferences;->overlay(Lcom/squareup/server/account/protos/Preferences;)Lcom/squareup/server/account/protos/Preferences;

    move-result-object p0

    const-string p1, "overlay(preferencesFromRequest)"

    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method

.method private static final toPreferences(Lcom/squareup/server/account/protos/PreferencesRequest;)Lcom/squareup/server/account/protos/Preferences;
    .locals 2

    .line 58
    new-instance v0, Lcom/squareup/server/account/protos/Preferences$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/Preferences$Builder;-><init>()V

    .line 59
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->tipping_enabled:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->tipping_enabled(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    .line 60
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->tipping_use_custom_percentages:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->tipping_use_custom_percentages(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    .line 61
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->tipping_use_manual_tip_entry:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->tipping_use_manual_tip_entry(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    .line 62
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->tipping_custom_percentages:Ljava/lang/String;

    invoke-static {v1}, Lcom/squareup/server/account/PreferenceUtilsKt;->listFromJson(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v1

    :goto_0
    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->tipping_custom_percentages(Ljava/util/List;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    .line 63
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->skip_signature:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->skip_signature(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    .line 64
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->store_and_forward_enabled:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->store_and_forward_enabled(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    .line 65
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->store_and_forward_single_transaction_limit:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->store_and_forward_single_transaction_limit(Lcom/squareup/protos/common/Money;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    .line 66
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->use_curated_image_for_receipt:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->use_curated_image_for_receipt(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    .line 67
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->use_separate_tipping_screen:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->use_separate_tipping_screen(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    .line 68
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->open_tickets_enabled:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->open_tickets_enabled(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    .line 69
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->use_paper_signature:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->use_paper_signature(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    .line 71
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->for_paper_signature_always_print_customer_copy:Ljava/lang/Boolean;

    .line 70
    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->for_paper_signature_always_print_customer_copy(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    .line 73
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->for_paper_signature_use_quick_tip_receipt:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->for_paper_signature_use_quick_tip_receipt(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    .line 75
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->for_paper_signature_print_additional_auth_slip:Ljava/lang/Boolean;

    .line 74
    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->for_paper_signature_print_additional_auth_slip(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    .line 77
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->employee_management_enabled_for_account:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->employee_management_enabled_for_account(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    .line 78
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->employee_management_enabled_for_device:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->employee_management_enabled_for_device(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    .line 79
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->employee_management_inactivity_timeout:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->employee_management_inactivity_timeout(Ljava/lang/String;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    .line 80
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->employee_management_tracking_level:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->employee_management_tracking_level(Ljava/lang/String;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    .line 82
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->requires_employee_logout_after_completed_transaction:Ljava/lang/Boolean;

    .line 81
    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->employee_management_requires_employee_logout_after_completed_transaction(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    .line 83
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->use_allow_swipe_for_chip_cards:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->use_allow_swipe_for_chip_cards(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    .line 84
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->tipping_calculation_phase:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->tipping_calculation_phase(Ljava/lang/String;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    .line 85
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->custom_tender_options_proto:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->custom_tender_options_proto(Ljava/lang/String;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    .line 86
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->skip_signature_always:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->skip_signature_always(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    .line 87
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->use_open_tickets_menu_as_home_screen:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->use_open_tickets_menu_as_home_screen(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    .line 88
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->use_predefined_tickets:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->use_predefined_tickets(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    .line 89
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->cash_management_enabled:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->cash_management_enabled(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    .line 90
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->cash_management_email_report:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->cash_management_email_report(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    .line 91
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->cash_management_print_report:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->cash_management_print_report(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    .line 92
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->cash_management_default_starting_amount:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->cash_management_default_starting_amount(Lcom/squareup/protos/common/Money;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    .line 93
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->cash_management_report_recipient_email:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->cash_management_report_recipient_email(Ljava/lang/String;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    .line 94
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->customer_management_collect_before_checkout:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->customer_management_collect_before_checkout(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    .line 95
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->customer_management_collect_after_checkout:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->customer_management_collect_after_checkout(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    .line 96
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->customer_management_use_card_on_file:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->customer_management_use_card_on_file(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    .line 98
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->customer_management_show_save_card_button_after_checkout:Ljava/lang/Boolean;

    .line 97
    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->customer_management_show_save_card_button_after_checkout(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    .line 101
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->customer_management_show_email_collection_screen:Ljava/lang/Boolean;

    .line 100
    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->customer_management_show_email_collection_screen(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    .line 103
    iget-object v1, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->allow_instant_deposit:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/Preferences$Builder;->allow_instant_deposit(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object v0

    .line 104
    iget-object p0, p0, Lcom/squareup/server/account/protos/PreferencesRequest;->terminal_connected_mode_enabled:Ljava/lang/Boolean;

    invoke-virtual {v0, p0}, Lcom/squareup/server/account/protos/Preferences$Builder;->terminal_connected_mode_enabled(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/Preferences$Builder;

    move-result-object p0

    .line 105
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/Preferences$Builder;->build()Lcom/squareup/server/account/protos/Preferences;

    move-result-object p0

    const-string v0, "Preferences.Builder()\n  \u2026e_enabled)\n      .build()"

    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p0
.end method
