.class final Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$ProtoAdapter_AccrualRulesOptions;
.super Lcom/squareup/wire/ProtoAdapter;
.source "AccrualRules.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ProtoAdapter_AccrualRulesOptions"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/ProtoAdapter<",
        "Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1415
    sget-object v0, Lcom/squareup/wire/FieldEncoding;->LENGTH_DELIMITED:Lcom/squareup/wire/FieldEncoding;

    const-class v1, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;

    invoke-direct {p0, v0, v1}, Lcom/squareup/wire/ProtoAdapter;-><init>(Lcom/squareup/wire/FieldEncoding;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1438
    new-instance v0, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;-><init>()V

    .line 1439
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->beginMessage()J

    move-result-wide v1

    .line 1440
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/wire/ProtoReader;->nextTag()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_4

    const/4 v4, 0x1

    if-eq v3, v4, :cond_3

    const/4 v4, 0x2

    if-eq v3, v4, :cond_2

    const/4 v4, 0x3

    if-eq v3, v4, :cond_1

    const/4 v4, 0x4

    if-eq v3, v4, :cond_0

    .line 1447
    invoke-virtual {p1, v3}, Lcom/squareup/wire/ProtoReader;->readUnknownField(I)V

    goto :goto_0

    .line 1445
    :cond_0
    iget-object v3, v0, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;->excluded_catalog_objects:Ljava/util/List;

    sget-object v4, Lcom/squareup/server/account/protos/AccrualRules$CatalogObject;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v4, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1444
    :cond_1
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;->exclude_comp_items(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;

    goto :goto_0

    .line 1443
    :cond_2
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;->exclude_discounted_items(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;

    goto :goto_0

    .line 1442
    :cond_3
    sget-object v3, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v3, p1}, Lcom/squareup/wire/ProtoAdapter;->decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v0, v3}, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;->exclude_reward_items(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;

    goto :goto_0

    .line 1451
    :cond_4
    invoke-virtual {p1, v1, v2}, Lcom/squareup/wire/ProtoReader;->endMessageAndGetUnknownFields(J)Lokio/ByteString;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    .line 1452
    invoke-virtual {v0}, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;->build()Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic decode(Lcom/squareup/wire/ProtoReader;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1413
    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$ProtoAdapter_AccrualRulesOptions;->decode(Lcom/squareup/wire/ProtoReader;)Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;

    move-result-object p1

    return-object p1
.end method

.method public encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1429
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->exclude_reward_items:Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1430
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->exclude_discounted_items:Ljava/lang/Boolean;

    const/4 v2, 0x2

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1431
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p2, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->exclude_comp_items:Ljava/lang/Boolean;

    const/4 v2, 0x3

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1432
    sget-object v0, Lcom/squareup/server/account/protos/AccrualRules$CatalogObject;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-virtual {v0}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v0

    iget-object v1, p2, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->excluded_catalog_objects:Ljava/util/List;

    const/4 v2, 0x4

    invoke-virtual {v0, p1, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodeWithTag(Lcom/squareup/wire/ProtoWriter;ILjava/lang/Object;)V

    .line 1433
    invoke-virtual {p2}, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->unknownFields()Lokio/ByteString;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/wire/ProtoWriter;->writeBytes(Lokio/ByteString;)V

    return-void
.end method

.method public bridge synthetic encode(Lcom/squareup/wire/ProtoWriter;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .line 1413
    check-cast p2, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$ProtoAdapter_AccrualRulesOptions;->encode(Lcom/squareup/wire/ProtoWriter;Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;)V

    return-void
.end method

.method public encodedSize(Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;)I
    .locals 4

    .line 1420
    sget-object v0, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v1, p1, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->exclude_reward_items:Ljava/lang/Boolean;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v0

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->exclude_discounted_items:Ljava/lang/Boolean;

    const/4 v3, 0x2

    .line 1421
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/wire/ProtoAdapter;->BOOL:Lcom/squareup/wire/ProtoAdapter;

    iget-object v2, p1, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->exclude_comp_items:Ljava/lang/Boolean;

    const/4 v3, 0x3

    .line 1422
    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    sget-object v1, Lcom/squareup/server/account/protos/AccrualRules$CatalogObject;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 1423
    invoke-virtual {v1}, Lcom/squareup/wire/ProtoAdapter;->asRepeated()Lcom/squareup/wire/ProtoAdapter;

    move-result-object v1

    iget-object v2, p1, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->excluded_catalog_objects:Ljava/util/List;

    const/4 v3, 0x4

    invoke-virtual {v1, v3, v2}, Lcom/squareup/wire/ProtoAdapter;->encodedSizeWithTag(ILjava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1424
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->unknownFields()Lokio/ByteString;

    move-result-object p1

    invoke-virtual {p1}, Lokio/ByteString;->size()I

    move-result p1

    add-int/2addr v0, p1

    return v0
.end method

.method public bridge synthetic encodedSize(Ljava/lang/Object;)I
    .locals 0

    .line 1413
    check-cast p1, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$ProtoAdapter_AccrualRulesOptions;->encodedSize(Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;)I

    move-result p1

    return p1
.end method

.method public redact(Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;)Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;
    .locals 2

    .line 1457
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;->newBuilder()Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;

    move-result-object p1

    .line 1458
    iget-object v0, p1, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;->excluded_catalog_objects:Ljava/util/List;

    sget-object v1, Lcom/squareup/server/account/protos/AccrualRules$CatalogObject;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0, v1}, Lcom/squareup/wire/internal/Internal;->redactElements(Ljava/util/List;Lcom/squareup/wire/ProtoAdapter;)V

    .line 1459
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;->clearUnknownFields()Lcom/squareup/wire/Message$Builder;

    .line 1460
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$Builder;->build()Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic redact(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1413
    check-cast p1, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions$ProtoAdapter_AccrualRulesOptions;->redact(Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;)Lcom/squareup/server/account/protos/AccrualRules$AccrualRulesOptions;

    move-result-object p1

    return-object p1
.end method
