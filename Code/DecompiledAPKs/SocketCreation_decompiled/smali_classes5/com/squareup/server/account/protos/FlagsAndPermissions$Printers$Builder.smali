.class public final Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FlagsAndPermissions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public can_print_compact_tickets:Ljava/lang/Boolean;

.field public enable_epson_printers:Ljava/lang/Boolean;

.field public epson_debugging:Ljava/lang/Boolean;

.field public retain_printer_connection:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 19152
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;
    .locals 7

    .line 19177
    new-instance v6, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers$Builder;->enable_epson_printers:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers$Builder;->epson_debugging:Ljava/lang/Boolean;

    iget-object v3, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers$Builder;->can_print_compact_tickets:Ljava/lang/Boolean;

    iget-object v4, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers$Builder;->retain_printer_connection:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v6
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 19143
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers;

    move-result-object v0

    return-object v0
.end method

.method public can_print_compact_tickets(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers$Builder;
    .locals 0

    .line 19166
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers$Builder;->can_print_compact_tickets:Ljava/lang/Boolean;

    return-object p0
.end method

.method public enable_epson_printers(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers$Builder;
    .locals 0

    .line 19156
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers$Builder;->enable_epson_printers:Ljava/lang/Boolean;

    return-object p0
.end method

.method public epson_debugging(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers$Builder;
    .locals 0

    .line 19161
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers$Builder;->epson_debugging:Ljava/lang/Boolean;

    return-object p0
.end method

.method public retain_printer_connection(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers$Builder;
    .locals 0

    .line 19171
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Printers$Builder;->retain_printer_connection:Ljava/lang/Boolean;

    return-object p0
.end method
