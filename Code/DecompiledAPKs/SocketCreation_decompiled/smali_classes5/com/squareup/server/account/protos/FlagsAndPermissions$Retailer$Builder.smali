.class public final Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FlagsAndPermissions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public adjust_inventory_after_order_cancellation:Ljava/lang/Boolean;

.field public can_small_red_search:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 17806
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    return-void
.end method


# virtual methods
.method public adjust_inventory_after_order_cancellation(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer$Builder;
    .locals 0

    .line 17811
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer$Builder;->adjust_inventory_after_order_cancellation:Ljava/lang/Boolean;

    return-object p0
.end method

.method public build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;
    .locals 4

    .line 17822
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer$Builder;->adjust_inventory_after_order_cancellation:Ljava/lang/Boolean;

    iget-object v2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer$Builder;->can_small_red_search:Ljava/lang/Boolean;

    invoke-super {p0}, Lcom/squareup/wire/Message$Builder;->buildUnknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .line 17801
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer;

    move-result-object v0

    return-object v0
.end method

.method public can_small_red_search(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer$Builder;
    .locals 0

    .line 17816
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Retailer$Builder;->can_small_red_search:Ljava/lang/Boolean;

    return-object p0
.end method
