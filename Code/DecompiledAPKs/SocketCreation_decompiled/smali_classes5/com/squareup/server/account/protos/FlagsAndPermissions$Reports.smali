.class public final Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;
.super Lcom/squareup/wire/AndroidMessage;
.source "FlagsAndPermissions.java"

# interfaces
.implements Lcom/squareup/wired/PopulatesDefaults;
.implements Lcom/squareup/wired/OverlaysMessage;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/account/protos/FlagsAndPermissions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Reports"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports$ProtoAdapter_Reports;,
        Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports$Builder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/AndroidMessage<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports$Builder;",
        ">;",
        "Lcom/squareup/wired/PopulatesDefaults<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;",
        ">;",
        "Lcom/squareup/wired/OverlaysMessage<",
        "Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;",
        ">;"
    }
.end annotation


# static fields
.field public static final ADAPTER:Lcom/squareup/wire/ProtoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/wire/ProtoAdapter<",
            "Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;",
            ">;"
        }
    .end annotation
.end field

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_FILTER_BY_EMPLOYEE:Ljava/lang/Boolean;

.field public static final DEFAULT_HIDE_TIPS:Ljava/lang/Boolean;

.field public static final DEFAULT_SEE_MEASUREMENT_UNIT:Ljava/lang/Boolean;

.field public static final DEFAULT_USE_ALTERNATE_SALES_SUMMARY:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final filter_by_employee:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x2
    .end annotation
.end field

.field public final hide_tips:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x3
    .end annotation
.end field

.field public final see_measurement_unit:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x4
    .end annotation
.end field

.field public final use_alternate_sales_summary:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/WireField;
        adapter = "com.squareup.wire.ProtoAdapter#BOOL"
        tag = 0x1
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 7484
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports$ProtoAdapter_Reports;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports$ProtoAdapter_Reports;-><init>()V

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    .line 7486
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-static {v0}, Lcom/squareup/wire/AndroidMessage;->newCreator(Lcom/squareup/wire/ProtoAdapter;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->CREATOR:Landroid/os/Parcelable$Creator;

    const/4 v0, 0x0

    .line 7490
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->DEFAULT_USE_ALTERNATE_SALES_SUMMARY:Ljava/lang/Boolean;

    .line 7492
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->DEFAULT_FILTER_BY_EMPLOYEE:Ljava/lang/Boolean;

    .line 7494
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->DEFAULT_HIDE_TIPS:Ljava/lang/Boolean;

    .line 7496
    sput-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->DEFAULT_SEE_MEASUREMENT_UNIT:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 6

    .line 7541
    sget-object v5, Lokio/ByteString;->EMPTY:Lokio/ByteString;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Lokio/ByteString;)V
    .locals 1

    .line 7547
    sget-object v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->ADAPTER:Lcom/squareup/wire/ProtoAdapter;

    invoke-direct {p0, v0, p5}, Lcom/squareup/wire/AndroidMessage;-><init>(Lcom/squareup/wire/ProtoAdapter;Lokio/ByteString;)V

    .line 7548
    iput-object p1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->use_alternate_sales_summary:Ljava/lang/Boolean;

    .line 7549
    iput-object p2, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->filter_by_employee:Ljava/lang/Boolean;

    .line 7550
    iput-object p3, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->hide_tips:Ljava/lang/Boolean;

    .line 7551
    iput-object p4, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->see_measurement_unit:Ljava/lang/Boolean;

    return-void
.end method

.method private requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports$Builder;
    .locals 0

    if-nez p1, :cond_0

    .line 7622
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports$Builder;

    move-result-object p1

    :cond_0
    return-object p1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    .line 7568
    :cond_0
    instance-of v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    .line 7569
    :cond_1
    check-cast p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;

    .line 7570
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->unknownFields()Lokio/ByteString;

    move-result-object v3

    invoke-virtual {v1, v3}, Lokio/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->use_alternate_sales_summary:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->use_alternate_sales_summary:Ljava/lang/Boolean;

    .line 7571
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->filter_by_employee:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->filter_by_employee:Ljava/lang/Boolean;

    .line 7572
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->hide_tips:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->hide_tips:Ljava/lang/Boolean;

    .line 7573
    invoke-static {v1, v3}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->see_measurement_unit:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->see_measurement_unit:Ljava/lang/Boolean;

    .line 7574
    invoke-static {v1, p1}, Lcom/squareup/wire/internal/Internal;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 7579
    iget v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    if-nez v0, :cond_4

    .line 7581
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->unknownFields()Lokio/ByteString;

    move-result-object v0

    invoke-virtual {v0}, Lokio/ByteString;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x25

    .line 7582
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->use_alternate_sales_summary:Ljava/lang/Boolean;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 7583
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->filter_by_employee:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 7584
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->hide_tips:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x25

    .line 7585
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->see_measurement_unit:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :cond_3
    add-int/2addr v0, v2

    .line 7586
    iput v0, p0, Lcom/squareup/wire/AndroidMessage;->hashCode:I

    :cond_4
    return v0
.end method

.method public newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports$Builder;
    .locals 2

    .line 7556
    new-instance v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports$Builder;

    invoke-direct {v0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports$Builder;-><init>()V

    .line 7557
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->use_alternate_sales_summary:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports$Builder;->use_alternate_sales_summary:Ljava/lang/Boolean;

    .line 7558
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->filter_by_employee:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports$Builder;->filter_by_employee:Ljava/lang/Boolean;

    .line 7559
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->hide_tips:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports$Builder;->hide_tips:Ljava/lang/Boolean;

    .line 7560
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->see_measurement_unit:Ljava/lang/Boolean;

    iput-object v1, v0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports$Builder;->see_measurement_unit:Ljava/lang/Boolean;

    .line 7561
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->unknownFields()Lokio/ByteString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports$Builder;->addUnknownFields(Lokio/ByteString;)Lcom/squareup/wire/Message$Builder;

    return-object v0
.end method

.method public bridge synthetic newBuilder()Lcom/squareup/wire/Message$Builder;
    .locals 1

    .line 7483
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->newBuilder()Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports$Builder;

    move-result-object v0

    return-object v0
.end method

.method public overlay(Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;
    .locals 2

    .line 7614
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->use_alternate_sales_summary:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->use_alternate_sales_summary:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports$Builder;->use_alternate_sales_summary(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports$Builder;

    move-result-object v1

    .line 7615
    :cond_0
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->filter_by_employee:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->filter_by_employee:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports$Builder;->filter_by_employee(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports$Builder;

    move-result-object v1

    .line 7616
    :cond_1
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->hide_tips:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports$Builder;

    move-result-object v0

    iget-object v1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->hide_tips:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports$Builder;->hide_tips(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports$Builder;

    move-result-object v1

    .line 7617
    :cond_2
    iget-object v0, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->see_measurement_unit:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports$Builder;

    move-result-object v0

    iget-object p1, p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->see_measurement_unit:Ljava/lang/Boolean;

    invoke-virtual {v0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports$Builder;->see_measurement_unit(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports$Builder;

    move-result-object v1

    :cond_3
    if-nez v1, :cond_4

    move-object p1, p0

    goto :goto_0

    .line 7618
    :cond_4
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public bridge synthetic overlay(Lcom/squareup/wired/OverlaysMessage;)Lcom/squareup/wired/OverlaysMessage;
    .locals 0

    .line 7483
    check-cast p1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;

    invoke-virtual {p0, p1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->overlay(Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;

    move-result-object p1

    return-object p1
.end method

.method public populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;
    .locals 2

    .line 7604
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->use_alternate_sales_summary:Ljava/lang/Boolean;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->DEFAULT_USE_ALTERNATE_SALES_SUMMARY:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports$Builder;->use_alternate_sales_summary(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports$Builder;

    move-result-object v1

    .line 7605
    :cond_0
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->filter_by_employee:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->DEFAULT_FILTER_BY_EMPLOYEE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports$Builder;->filter_by_employee(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports$Builder;

    move-result-object v1

    .line 7606
    :cond_1
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->hide_tips:Ljava/lang/Boolean;

    if-nez v0, :cond_2

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->DEFAULT_HIDE_TIPS:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports$Builder;->hide_tips(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports$Builder;

    move-result-object v1

    .line 7607
    :cond_2
    iget-object v0, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->see_measurement_unit:Ljava/lang/Boolean;

    if-nez v0, :cond_3

    invoke-direct {p0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->requireBuilder(Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports$Builder;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports$Builder;

    move-result-object v0

    sget-object v1, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->DEFAULT_SEE_MEASUREMENT_UNIT:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports$Builder;->see_measurement_unit(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports$Builder;

    move-result-object v1

    :cond_3
    if-nez v1, :cond_4

    move-object v0, p0

    goto :goto_0

    .line 7608
    :cond_4
    invoke-virtual {v1}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports$Builder;->build()Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public bridge synthetic populateDefaults()Lcom/squareup/wired/PopulatesDefaults;
    .locals 1

    .line 7483
    invoke-virtual {p0}, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->populateDefaults()Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 7593
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 7594
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->use_alternate_sales_summary:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    const-string v1, ", use_alternate_sales_summary="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->use_alternate_sales_summary:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 7595
    :cond_0
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->filter_by_employee:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    const-string v1, ", filter_by_employee="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->filter_by_employee:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 7596
    :cond_1
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->hide_tips:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    const-string v1, ", hide_tips="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->hide_tips:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 7597
    :cond_2
    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->see_measurement_unit:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    const-string v1, ", see_measurement_unit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/server/account/protos/FlagsAndPermissions$Reports;->see_measurement_unit:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const/4 v1, 0x0

    const/4 v2, 0x2

    const-string v3, "Reports{"

    .line 7598
    invoke-virtual {v0, v1, v2, v3}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
