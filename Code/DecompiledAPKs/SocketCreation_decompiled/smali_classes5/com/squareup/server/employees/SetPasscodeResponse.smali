.class public Lcom/squareup/server/employees/SetPasscodeResponse;
.super Lcom/squareup/server/SimpleResponse;
.source "SetPasscodeResponse.java"


# static fields
.field private static final UNIQUE_ERROR_CODE:Ljava/lang/String; = "unique"


# instance fields
.field private final error_code:Ljava/lang/String;

.field private final passcode:Ljava/lang/String;


# direct methods
.method public constructor <init>(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    .line 13
    invoke-direct {p0, p1, v0, p2}, Lcom/squareup/server/SimpleResponse;-><init>(ZLjava/lang/String;Ljava/lang/String;)V

    .line 14
    iput-object p3, p0, Lcom/squareup/server/employees/SetPasscodeResponse;->error_code:Ljava/lang/String;

    .line 15
    iput-object p4, p0, Lcom/squareup/server/employees/SetPasscodeResponse;->passcode:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public duplicatePasscode()Z
    .locals 2

    .line 19
    iget-object v0, p0, Lcom/squareup/server/employees/SetPasscodeResponse;->error_code:Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string/jumbo v1, "unique"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public passcode()Ljava/lang/String;
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/squareup/server/employees/SetPasscodeResponse;->passcode:Ljava/lang/String;

    return-object v0
.end method
