.class public final Lcom/squareup/server/seller/PassTender;
.super Ljava/lang/Object;
.source "PassTender.java"


# instance fields
.field public final scanned:Ljava/lang/Boolean;

.field public final token:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/Boolean;)V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/squareup/server/seller/PassTender;->token:Ljava/lang/String;

    .line 17
    iput-object p2, p0, Lcom/squareup/server/seller/PassTender;->scanned:Ljava/lang/Boolean;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    .line 21
    instance-of v1, p1, Lcom/squareup/server/seller/PassTender;

    if-nez v1, :cond_0

    goto :goto_0

    .line 22
    :cond_0
    check-cast p1, Lcom/squareup/server/seller/PassTender;

    .line 23
    iget-object v1, p0, Lcom/squareup/server/seller/PassTender;->token:Ljava/lang/String;

    iget-object v2, p1, Lcom/squareup/server/seller/PassTender;->token:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/squareup/server/seller/PassTender;->scanned:Ljava/lang/Boolean;

    iget-object p1, p1, Lcom/squareup/server/seller/PassTender;->scanned:Ljava/lang/Boolean;

    invoke-static {v1, p1}, Lcom/squareup/util/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :cond_1
    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    .line 27
    iget-object v1, p0, Lcom/squareup/server/seller/PassTender;->token:Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget-object v1, p0, Lcom/squareup/server/seller/PassTender;->scanned:Ljava/lang/Boolean;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/squareup/util/Objects;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
