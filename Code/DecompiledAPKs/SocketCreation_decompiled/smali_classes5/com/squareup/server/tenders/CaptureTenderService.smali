.class public interface abstract Lcom/squareup/server/tenders/CaptureTenderService;
.super Ljava/lang/Object;
.source "CaptureTenderService.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008f\u0018\u00002\u00020\u0001J\u0012\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0001\u0010\u0004\u001a\u00020\u0005H\'\u00a8\u0006\u0006"
    }
    d2 = {
        "Lcom/squareup/server/tenders/CaptureTenderService;",
        "",
        "captureTenders",
        "Lcom/squareup/protos/client/bills/CaptureTendersResponse;",
        "request",
        "Lcom/squareup/protos/client/bills/CaptureTendersRequest;",
        "checkout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# virtual methods
.method public abstract captureTenders(Lcom/squareup/protos/client/bills/CaptureTendersRequest;)Lcom/squareup/protos/client/bills/CaptureTendersResponse;
    .param p1    # Lcom/squareup/protos/client/bills/CaptureTendersRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation runtime Lretrofit/http/Headers;
        value = {
            "Accept: application/x-protobuf"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/1.0/bills/capture-tenders"
    .end annotation
.end method
