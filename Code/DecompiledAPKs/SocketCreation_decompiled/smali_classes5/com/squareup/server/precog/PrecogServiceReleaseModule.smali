.class public abstract Lcom/squareup/server/precog/PrecogServiceReleaseModule;
.super Ljava/lang/Object;
.source "PrecogServiceReleaseModule.java"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/server/precog/PrecogServiceCommonModule;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract bindPrecogService(Lcom/squareup/server/precog/PrecogService;)Lcom/squareup/server/precog/PrecogService;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
