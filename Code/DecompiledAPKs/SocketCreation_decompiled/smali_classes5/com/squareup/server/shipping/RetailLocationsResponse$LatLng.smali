.class public Lcom/squareup/server/shipping/RetailLocationsResponse$LatLng;
.super Ljava/lang/Object;
.source "RetailLocationsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/shipping/RetailLocationsResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LatLng"
.end annotation


# instance fields
.field public final lat:Ljava/lang/String;

.field public final lng:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/squareup/server/shipping/RetailLocationsResponse$LatLng;->lat:Ljava/lang/String;

    .line 45
    iput-object p2, p0, Lcom/squareup/server/shipping/RetailLocationsResponse$LatLng;->lng:Ljava/lang/String;

    return-void
.end method
