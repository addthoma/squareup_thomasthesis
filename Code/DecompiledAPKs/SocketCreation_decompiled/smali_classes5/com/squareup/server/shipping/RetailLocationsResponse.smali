.class public Lcom/squareup/server/shipping/RetailLocationsResponse;
.super Ljava/lang/Object;
.source "RetailLocationsResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/server/shipping/RetailLocationsResponse$LatLng;,
        Lcom/squareup/server/shipping/RetailLocationsResponse$Merchant;
    }
.end annotation


# instance fields
.field public final merchants:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/server/shipping/RetailLocationsResponse$Merchant;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/server/shipping/RetailLocationsResponse$Merchant;",
            ">;)V"
        }
    .end annotation

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    iput-object p1, p0, Lcom/squareup/server/shipping/RetailLocationsResponse;->merchants:Ljava/util/List;

    return-void
.end method
