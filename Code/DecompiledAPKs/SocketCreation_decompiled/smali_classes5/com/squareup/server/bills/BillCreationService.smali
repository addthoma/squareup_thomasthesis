.class public interface abstract Lcom/squareup/server/bills/BillCreationService;
.super Ljava/lang/Object;
.source "BillCreationService.java"


# static fields
.field public static final REGISTER_API_HEADER:Ljava/lang/String; = "X-Connect-App-ID"


# virtual methods
.method public abstract addTenders(Lcom/squareup/protos/client/bills/AddTendersRequest;Ljava/lang/String;)Lcom/squareup/protos/client/bills/AddTendersResponse;
    .param p1    # Lcom/squareup/protos/client/bills/AddTendersRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Header;
            value = "X-Connect-App-ID"
        .end annotation
    .end param
    .annotation runtime Lretrofit/http/Headers;
        value = {
            "X-Square-Gzip: true"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/1.0/bills/add-tenders"
    .end annotation
.end method

.method public abstract cancelBill(Lcom/squareup/protos/client/bills/CancelBillRequest;)Lcom/squareup/protos/client/bills/CancelBillResponse;
    .param p1    # Lcom/squareup/protos/client/bills/CancelBillRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation runtime Lretrofit/http/POST;
        value = "/1.0/bills/cancel-bill"
    .end annotation
.end method

.method public abstract checkBalance(Lcom/squareup/protos/client/bills/CheckBalanceRequest;)Lrx/Observable;
    .param p1    # Lcom/squareup/protos/client/bills/CheckBalanceRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/bills/CheckBalanceRequest;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/bills/CheckBalanceResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/1.0/bills/check-balance"
    .end annotation
.end method

.method public abstract completeBill(Lcom/squareup/protos/client/bills/CompleteBillRequest;Ljava/lang/String;)Lcom/squareup/protos/client/bills/CompleteBillResponse;
    .param p1    # Lcom/squareup/protos/client/bills/CompleteBillRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lretrofit/http/Header;
            value = "X-Connect-App-ID"
        .end annotation
    .end param
    .annotation runtime Lretrofit/http/Headers;
        value = {
            "X-Square-Gzip: true"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/1.0/bills/complete-bill"
    .end annotation
.end method
