.class public Lcom/squareup/server/bills/BillListServiceHelper;
.super Ljava/lang/Object;
.source "BillListServiceHelper.java"


# instance fields
.field private final service:Lcom/squareup/server/bills/BillListService;


# direct methods
.method public constructor <init>(Lcom/squareup/server/bills/BillListService;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/server/bills/BillListServiceHelper;->service:Lcom/squareup/server/bills/BillListService;

    return-void
.end method


# virtual methods
.method public getBill(Ljava/lang/String;)Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/bills/GetBillByTenderServerIdResponse;",
            ">;>;"
        }
    .end annotation

    .line 25
    new-instance v0, Lcom/squareup/protos/client/bills/GetBillByTenderServerIdRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/GetBillByTenderServerIdRequest$Builder;-><init>()V

    const-string/jumbo v1, "tenderServerId"

    .line 26
    invoke-static {p1, v1}, Lcom/squareup/util/Preconditions;->nonBlank(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/GetBillByTenderServerIdRequest$Builder;->tender_server_id(Ljava/lang/String;)Lcom/squareup/protos/client/bills/GetBillByTenderServerIdRequest$Builder;

    move-result-object p1

    .line 27
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/GetBillByTenderServerIdRequest$Builder;->build()Lcom/squareup/protos/client/bills/GetBillByTenderServerIdRequest;

    move-result-object p1

    .line 29
    iget-object v0, p0, Lcom/squareup/server/bills/BillListServiceHelper;->service:Lcom/squareup/server/bills/BillListService;

    invoke-interface {v0, p1}, Lcom/squareup/server/bills/BillListService;->getBill(Lcom/squareup/protos/client/bills/GetBillByTenderServerIdRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public getBillFamilies(Ljava/lang/String;Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;ILjava/lang/String;Z)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;",
            "I",
            "Ljava/lang/String;",
            "Z)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/bills/GetBillFamiliesResponse;",
            ">;>;"
        }
    .end annotation

    .line 34
    new-instance v0, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Builder;-><init>()V

    .line 35
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Builder;->merchant_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Builder;

    move-result-object p1

    .line 36
    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Builder;->query(Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Query;)Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Builder;

    move-result-object p1

    .line 37
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Builder;->limit(Ljava/lang/Integer;)Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Builder;

    move-result-object p1

    .line 38
    invoke-virtual {p1, p4}, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Builder;->pagination_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Builder;

    move-result-object p1

    .line 39
    invoke-static {p5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Builder;->exclude_related_bills(Ljava/lang/Boolean;)Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Builder;

    move-result-object p1

    .line 40
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/GetBillFamiliesRequest$Builder;->build()Lcom/squareup/protos/client/bills/GetBillFamiliesRequest;

    move-result-object p1

    .line 42
    iget-object p2, p0, Lcom/squareup/server/bills/BillListServiceHelper;->service:Lcom/squareup/server/bills/BillListService;

    invoke-interface {p2, p1}, Lcom/squareup/server/bills/BillListService;->getBillFamilies(Lcom/squareup/protos/client/bills/GetBillFamiliesRequest;)Lcom/squareup/server/StatusResponse;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/server/StatusResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public getResidualBill(Ljava/lang/String;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "Lcom/squareup/protos/client/bills/GetResidualBillResponse;",
            ">;>;"
        }
    .end annotation

    .line 46
    new-instance v0, Lcom/squareup/protos/client/bills/GetResidualBillRequest$Builder;

    invoke-direct {v0}, Lcom/squareup/protos/client/bills/GetResidualBillRequest$Builder;-><init>()V

    .line 47
    invoke-virtual {v0, p1}, Lcom/squareup/protos/client/bills/GetResidualBillRequest$Builder;->bill_server_token(Ljava/lang/String;)Lcom/squareup/protos/client/bills/GetResidualBillRequest$Builder;

    move-result-object p1

    .line 48
    invoke-virtual {p1}, Lcom/squareup/protos/client/bills/GetResidualBillRequest$Builder;->build()Lcom/squareup/protos/client/bills/GetResidualBillRequest;

    move-result-object p1

    .line 50
    iget-object v0, p0, Lcom/squareup/server/bills/BillListServiceHelper;->service:Lcom/squareup/server/bills/BillListService;

    invoke-interface {v0, p1}, Lcom/squareup/server/bills/BillListService;->getResidualBill(Lcom/squareup/protos/client/bills/GetResidualBillRequest;)Lcom/squareup/server/AcceptedResponse;

    move-result-object p1

    invoke-virtual {p1}, Lcom/squareup/server/AcceptedResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
