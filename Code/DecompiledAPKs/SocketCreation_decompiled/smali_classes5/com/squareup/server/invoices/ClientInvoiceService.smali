.class public interface abstract Lcom/squareup/server/invoices/ClientInvoiceService;
.super Ljava/lang/Object;
.source "ClientInvoiceService.java"


# virtual methods
.method public abstract archiveInvoice(Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest;)Lrx/Observable;
    .param p1    # Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/invoice/ArchiveInvoiceRequest;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/invoice/ArchiveInvoiceResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit/http/Headers;
        value = {
            "X-Square-Gzip: true"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/1.0/invoices/archive-invoice"
    .end annotation
.end method

.method public abstract cancel(Lcom/squareup/protos/client/invoice/CancelInvoiceRequest;)Lrx/Observable;
    .param p1    # Lcom/squareup/protos/client/invoice/CancelInvoiceRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/invoice/CancelInvoiceRequest;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/invoice/CancelInvoiceResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit/http/Headers;
        value = {
            "X-Square-Gzip: true"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/1.0/invoices/cancel"
    .end annotation
.end method

.method public abstract clone(Lcom/squareup/protos/client/invoice/CloneInvoiceRequest;)Lrx/Observable;
    .param p1    # Lcom/squareup/protos/client/invoice/CloneInvoiceRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/invoice/CloneInvoiceRequest;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/invoice/CloneInvoiceResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit/http/Headers;
        value = {
            "X-Square-Gzip: true"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/1.0/invoices/clone-invoice"
    .end annotation
.end method

.method public abstract deleteDraft(Lcom/squareup/protos/client/invoice/DeleteDraftRequest;)Lrx/Observable;
    .param p1    # Lcom/squareup/protos/client/invoice/DeleteDraftRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/invoice/DeleteDraftRequest;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/invoice/DeleteDraftResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit/http/Headers;
        value = {
            "X-Square-Gzip: true"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/1.0/invoices/delete"
    .end annotation
.end method

.method public abstract deleteDraftSeries(Lcom/squareup/protos/client/invoice/DeleteDraftRecurringSeriesRequest;)Lrx/Observable;
    .param p1    # Lcom/squareup/protos/client/invoice/DeleteDraftRecurringSeriesRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/invoice/DeleteDraftRecurringSeriesRequest;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/invoice/DeleteDraftRecurringSeriesResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit/http/Headers;
        value = {
            "X-Square-Gzip: true"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/1.0/invoices/delete-draft-recurring"
    .end annotation
.end method

.method public abstract endRecurring(Lcom/squareup/protos/client/invoice/EndRecurringSeriesRequest;)Lrx/Observable;
    .param p1    # Lcom/squareup/protos/client/invoice/EndRecurringSeriesRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/invoice/EndRecurringSeriesRequest;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/invoice/EndRecurringSeriesResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit/http/Headers;
        value = {
            "X-Square-Gzip: true"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/1.0/invoices/end-recurring"
    .end annotation
.end method

.method public abstract get(Lcom/squareup/protos/client/invoice/GetInvoiceRequest;)Lrx/Observable;
    .param p1    # Lcom/squareup/protos/client/invoice/GetInvoiceRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/invoice/GetInvoiceRequest;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/invoice/GetInvoiceResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit/http/Headers;
        value = {
            "X-Square-Gzip: true"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/1.0/invoices/get"
    .end annotation
.end method

.method public abstract getAllNotificationSettings(Lcom/squareup/protos/invoice/v2/notifications/GetAllNotificationSettingsRequest;)Lrx/Observable;
    .param p1    # Lcom/squareup/protos/invoice/v2/notifications/GetAllNotificationSettingsRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/invoice/v2/notifications/GetAllNotificationSettingsRequest;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/protos/invoice/v2/notifications/GetAllNotificationSettingsResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit/http/Headers;
        value = {
            "X-Square-Gzip: true"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/1.0/invoices/get-all-notification-settings"
    .end annotation
.end method

.method public abstract getMetrics(Lcom/squareup/protos/client/invoice/GetMetricsRequest;)Lrx/Observable;
    .param p1    # Lcom/squareup/protos/client/invoice/GetMetricsRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/invoice/GetMetricsRequest;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/invoice/GetMetricsResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit/http/Headers;
        value = {
            "X-Square-Gzip: true"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/1.0/invoices/get-metrics"
    .end annotation
.end method

.method public abstract getRecurring(Lcom/squareup/protos/client/invoice/GetRecurringSeriesRequest;)Lrx/Observable;
    .param p1    # Lcom/squareup/protos/client/invoice/GetRecurringSeriesRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/invoice/GetRecurringSeriesRequest;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/invoice/GetRecurringSeriesResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit/http/Headers;
        value = {
            "X-Square-Gzip: true"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/1.0/invoices/get-recurring"
    .end annotation
.end method

.method public abstract getUnitMetadata(Lcom/squareup/protos/client/invoice/GetUnitMetadataRequest;)Lrx/Observable;
    .param p1    # Lcom/squareup/protos/client/invoice/GetUnitMetadataRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/invoice/GetUnitMetadataRequest;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/invoice/GetUnitMetadataResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit/http/Headers;
        value = {
            "X-Square-Gzip: true"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/1.0/invoices/get-unit-metadata"
    .end annotation
.end method

.method public abstract getUnitSettings(Lcom/squareup/protos/client/invoice/GetUnitSettingsRequest;)Lrx/Observable;
    .param p1    # Lcom/squareup/protos/client/invoice/GetUnitSettingsRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/invoice/GetUnitSettingsRequest;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/invoice/GetUnitSettingsResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit/http/Headers;
        value = {
            "X-Square-Gzip: true"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/1.0/invoices/get-unit-settings"
    .end annotation
.end method

.method public abstract list(Lcom/squareup/protos/client/invoice/ListInvoicesRequest;)Lrx/Observable;
    .param p1    # Lcom/squareup/protos/client/invoice/ListInvoicesRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/invoice/ListInvoicesRequest;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/invoice/ListInvoicesResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit/http/Headers;
        value = {
            "X-Square-Gzip: true"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/1.0/invoices/list"
    .end annotation
.end method

.method public abstract listRecurring(Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;)Lrx/Observable;
    .param p1    # Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/invoice/ListRecurringSeriesRequest;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/invoice/ListRecurringSeriesResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit/http/Headers;
        value = {
            "X-Square-Gzip: true"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/1.0/invoices/list-recurring"
    .end annotation
.end method

.method public abstract recordPayment(Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest;)Lrx/Observable;
    .param p1    # Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandRequest;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/invoice/PayInvoiceOutOfBandResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit/http/Headers;
        value = {
            "X-Square-Gzip: true"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/1.0/invoices/pay-invoice-out-of-band"
    .end annotation
.end method

.method public abstract saveDraftInvoice(Lcom/squareup/protos/client/invoice/SaveDraftInvoiceRequest;)Lrx/Observable;
    .param p1    # Lcom/squareup/protos/client/invoice/SaveDraftInvoiceRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/invoice/SaveDraftInvoiceRequest;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/invoice/SaveDraftInvoiceResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit/http/Headers;
        value = {
            "X-Square-Gzip: true"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/1.0/invoices/save-draft-invoice"
    .end annotation
.end method

.method public abstract saveRecurringDraft(Lcom/squareup/protos/client/invoice/SaveDraftRecurringSeriesRequest;)Lrx/Observable;
    .param p1    # Lcom/squareup/protos/client/invoice/SaveDraftRecurringSeriesRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/invoice/SaveDraftRecurringSeriesRequest;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/invoice/SaveDraftRecurringSeriesResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit/http/Headers;
        value = {
            "X-Square-Gzip: true"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/1.0/invoices/save-draft-recurring"
    .end annotation
.end method

.method public abstract scheduleRecurring(Lcom/squareup/protos/client/invoice/ScheduleRecurringSeriesRequest;)Lrx/Observable;
    .param p1    # Lcom/squareup/protos/client/invoice/ScheduleRecurringSeriesRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/invoice/ScheduleRecurringSeriesRequest;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/invoice/ScheduleRecurringSeriesResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit/http/Headers;
        value = {
            "X-Square-Gzip: true"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/1.0/invoices/schedule-recurring"
    .end annotation
.end method

.method public abstract sendOrSchedule(Lcom/squareup/protos/client/invoice/SendOrScheduleInvoiceRequest;)Lrx/Observable;
    .param p1    # Lcom/squareup/protos/client/invoice/SendOrScheduleInvoiceRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/invoice/SendOrScheduleInvoiceRequest;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/invoice/SendOrScheduleInvoiceResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit/http/Headers;
        value = {
            "X-Square-Gzip: true"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/1.0/invoices/send-or-schedule"
    .end annotation
.end method

.method public abstract sendReminder(Lcom/squareup/protos/client/invoice/SendInvoiceReminderRequest;)Lrx/Observable;
    .param p1    # Lcom/squareup/protos/client/invoice/SendInvoiceReminderRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/invoice/SendInvoiceReminderRequest;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/invoice/SendInvoiceReminderResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit/http/Headers;
        value = {
            "X-Square-Gzip: true"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/1.0/invoices/send-reminder"
    .end annotation
.end method

.method public abstract submitFeedback(Lcom/squareup/protos/client/feedback/CreateFeedbackRequest;)Lrx/Observable;
    .param p1    # Lcom/squareup/protos/client/feedback/CreateFeedbackRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/feedback/CreateFeedbackRequest;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/feedback/CreateFeedbackResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit/http/Headers;
        value = {
            "X-Square-Gzip: true"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/1.0/invoices/create-merchant-feedback"
    .end annotation
.end method

.method public abstract unarchiveInvoice(Lcom/squareup/protos/client/invoice/UnarchiveInvoiceRequest;)Lrx/Observable;
    .param p1    # Lcom/squareup/protos/client/invoice/UnarchiveInvoiceRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/invoice/UnarchiveInvoiceRequest;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/invoice/UnarchiveInvoiceResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit/http/Headers;
        value = {
            "X-Square-Gzip: true"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/1.0/invoices/unarchive-invoice"
    .end annotation
.end method

.method public abstract updateNotificationSettings(Lcom/squareup/protos/invoice/v2/notifications/UpdateNotificationSettingsRequest;)Lrx/Observable;
    .param p1    # Lcom/squareup/protos/invoice/v2/notifications/UpdateNotificationSettingsRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/invoice/v2/notifications/UpdateNotificationSettingsRequest;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/protos/invoice/v2/notifications/UpdateNotificationSettingsResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit/http/Headers;
        value = {
            "X-Square-Gzip: true"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/1.0/invoices/update-notification-settings"
    .end annotation
.end method

.method public abstract updateUnitMetadata(Lcom/squareup/protos/client/invoice/UpdateUnitMetadataRequest;)Lrx/Observable;
    .param p1    # Lcom/squareup/protos/client/invoice/UpdateUnitMetadataRequest;
        .annotation runtime Lretrofit/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/invoice/UpdateUnitMetadataRequest;",
            ")",
            "Lrx/Observable<",
            "Lcom/squareup/protos/client/invoice/UpdateUnitMetadataResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit/http/Headers;
        value = {
            "X-Square-Gzip: true"
        }
    .end annotation

    .annotation runtime Lretrofit/http/POST;
        value = "/1.0/invoices/update-unit-metadata"
    .end annotation
.end method
