.class public abstract Lcom/squareup/server/address/AddressServiceReleaseModule;
.super Ljava/lang/Object;
.source "AddressServiceReleaseModule.java"


# annotations
.annotation runtime Ldagger/Module;
    includes = {
        Lcom/squareup/server/address/AddressServiceCommonModule;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract bindAddressService(Lcom/squareup/server/address/AddressService;)Lcom/squareup/server/address/AddressService;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
