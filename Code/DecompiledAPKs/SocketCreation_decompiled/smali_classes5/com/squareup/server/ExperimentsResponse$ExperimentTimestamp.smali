.class public Lcom/squareup/server/ExperimentsResponse$ExperimentTimestamp;
.super Ljava/lang/Object;
.source "ExperimentsResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/ExperimentsResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ExperimentTimestamp"
.end annotation


# instance fields
.field public final instant_usec:J


# direct methods
.method public constructor <init>(J)V
    .locals 0

    .line 245
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 246
    iput-wide p1, p0, Lcom/squareup/server/ExperimentsResponse$ExperimentTimestamp;->instant_usec:J

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_3

    .line 251
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_0

    .line 253
    :cond_1
    check-cast p1, Lcom/squareup/server/ExperimentsResponse$ExperimentTimestamp;

    .line 255
    iget-wide v2, p0, Lcom/squareup/server/ExperimentsResponse$ExperimentTimestamp;->instant_usec:J

    iget-wide v4, p1, Lcom/squareup/server/ExperimentsResponse$ExperimentTimestamp;->instant_usec:J

    cmp-long p1, v2, v4

    if-eqz p1, :cond_2

    return v1

    :cond_2
    return v0

    :cond_3
    :goto_0
    return v1
.end method

.method public hashCode()I
    .locals 4

    .line 261
    iget-wide v0, p0, Lcom/squareup/server/ExperimentsResponse$ExperimentTimestamp;->instant_usec:J

    const/16 v2, 0x20

    ushr-long v2, v0, v2

    xor-long/2addr v0, v2

    long-to-int v1, v0

    return v1
.end method
