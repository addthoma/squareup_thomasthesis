.class final Lcom/squareup/server/payment/RelatedBillHistory$Companion$getCreatedByDescendingComparator$1;
.super Ljava/lang/Object;
.source "RelatedBillHistory.kt"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/server/payment/RelatedBillHistory$Companion;->getCreatedByDescendingComparator()Ljava/util/Comparator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/Comparator<",
        "Lcom/squareup/server/payment/RelatedBillHistory;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u00032\u000e\u0010\u0005\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "",
        "refund1",
        "Lcom/squareup/server/payment/RelatedBillHistory;",
        "kotlin.jvm.PlatformType",
        "refund2",
        "compare"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/server/payment/RelatedBillHistory$Companion$getCreatedByDescendingComparator$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/server/payment/RelatedBillHistory$Companion$getCreatedByDescendingComparator$1;

    invoke-direct {v0}, Lcom/squareup/server/payment/RelatedBillHistory$Companion$getCreatedByDescendingComparator$1;-><init>()V

    sput-object v0, Lcom/squareup/server/payment/RelatedBillHistory$Companion$getCreatedByDescendingComparator$1;->INSTANCE:Lcom/squareup/server/payment/RelatedBillHistory$Companion$getCreatedByDescendingComparator$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final compare(Lcom/squareup/server/payment/RelatedBillHistory;Lcom/squareup/server/payment/RelatedBillHistory;)I
    .locals 5

    .line 124
    sget-object v0, Lcom/squareup/server/payment/RelatedBillHistory;->Companion:Lcom/squareup/server/payment/RelatedBillHistory$Companion;

    invoke-static {v0, p1}, Lcom/squareup/server/payment/RelatedBillHistory$Companion;->access$timestamp(Lcom/squareup/server/payment/RelatedBillHistory$Companion;Lcom/squareup/server/payment/RelatedBillHistory;)J

    move-result-wide v0

    .line 125
    sget-object v2, Lcom/squareup/server/payment/RelatedBillHistory;->Companion:Lcom/squareup/server/payment/RelatedBillHistory$Companion;

    invoke-static {v2, p2}, Lcom/squareup/server/payment/RelatedBillHistory$Companion;->access$timestamp(Lcom/squareup/server/payment/RelatedBillHistory$Companion;Lcom/squareup/server/payment/RelatedBillHistory;)J

    move-result-wide v2

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    const/4 p1, -0x1

    goto :goto_2

    :cond_0
    if-gez v4, :cond_1

    const/4 p1, 0x1

    goto :goto_2

    .line 131
    :cond_1
    invoke-virtual {p1}, Lcom/squareup/server/payment/RelatedBillHistory;->getRefundId()Ljava/lang/String;

    move-result-object p1

    const-string v0, ""

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    move-object p1, v0

    .line 132
    :goto_0
    invoke-virtual {p2}, Lcom/squareup/server/payment/RelatedBillHistory;->getRefundId()Ljava/lang/String;

    move-result-object p2

    if-eqz p2, :cond_3

    goto :goto_1

    :cond_3
    move-object p2, v0

    .line 133
    :goto_1
    invoke-virtual {p1, p2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result p1

    :goto_2
    return p1
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 0

    .line 115
    check-cast p1, Lcom/squareup/server/payment/RelatedBillHistory;

    check-cast p2, Lcom/squareup/server/payment/RelatedBillHistory;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/server/payment/RelatedBillHistory$Companion$getCreatedByDescendingComparator$1;->compare(Lcom/squareup/server/payment/RelatedBillHistory;Lcom/squareup/server/payment/RelatedBillHistory;)I

    move-result p1

    return p1
.end method
