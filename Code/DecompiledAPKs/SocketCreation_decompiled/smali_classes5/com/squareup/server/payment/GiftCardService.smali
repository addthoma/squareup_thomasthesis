.class public interface abstract Lcom/squareup/server/payment/GiftCardService;
.super Ljava/lang/Object;
.source "GiftCardService.java"


# virtual methods
.method public abstract checkBalanceByServerToken(Lcom/squareup/protos/client/giftcards/GetGiftCardByServerTokenRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/giftcards/GetGiftCardByServerTokenRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/giftcards/GetGiftCardByServerTokenRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/giftcards/GetGiftCardByServerTokenResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/giftcards/get-gift-card"
    .end annotation
.end method

.method public abstract clearBalance(Lcom/squareup/protos/client/giftcards/ClearBalanceRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/giftcards/ClearBalanceRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/giftcards/ClearBalanceRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/giftcards/ClearBalanceResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/giftcards/clear-balance"
    .end annotation
.end method

.method public abstract getEGiftCardOrderConfiguration(Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationRequest;)Lcom/squareup/server/AcceptedResponse;
    .param p1    # Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationRequest;",
            ")",
            "Lcom/squareup/server/AcceptedResponse<",
            "Lcom/squareup/protos/client/giftcards/GetEGiftOrderConfigurationResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/giftcards/get-egift-order-configuration"
    .end annotation
.end method

.method public abstract getGiftCardHistory(Lcom/squareup/protos/client/giftcards/ListHistoryEventsRequest;)Lcom/squareup/server/AcceptedResponse;
    .param p1    # Lcom/squareup/protos/client/giftcards/ListHistoryEventsRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/giftcards/ListHistoryEventsRequest;",
            ")",
            "Lcom/squareup/server/AcceptedResponse<",
            "Lcom/squareup/protos/client/giftcards/ListHistoryEventsResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/giftcards/list-history-events"
    .end annotation
.end method

.method public abstract registerEGiftCard(Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/giftcards/RegisterEGiftCardRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/giftcards/RegisterEGiftCardResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/giftcards/register-electronic-gift-card"
    .end annotation
.end method

.method public abstract registerGiftCard(Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest;)Lcom/squareup/server/StatusResponse;
    .param p1    # Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/giftcards/RegisterGiftCardRequest;",
            ")",
            "Lcom/squareup/server/StatusResponse<",
            "Lcom/squareup/protos/client/giftcards/RegisterGiftCardResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/giftcards/register-gift-card"
    .end annotation
.end method

.method public abstract setEGiftCardOrderConfiguration(Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;)Lcom/squareup/server/AcceptedResponse;
    .param p1    # Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;
        .annotation runtime Lretrofit2/http/Body;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationRequest;",
            ")",
            "Lcom/squareup/server/AcceptedResponse<",
            "Lcom/squareup/protos/client/giftcards/SetEGiftOrderConfigurationResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/giftcards/set-egift-order-configuration"
    .end annotation
.end method

.method public abstract uploadEGiftCardTheme(Lokhttp3/MultipartBody$Part;)Lcom/squareup/server/SimpleStandardResponse;
    .param p1    # Lokhttp3/MultipartBody$Part;
        .annotation runtime Lretrofit2/http/Part;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lokhttp3/MultipartBody$Part;",
            ")",
            "Lcom/squareup/server/SimpleStandardResponse<",
            "Lcom/squareup/server/payment/GiftCardImageUploadResponse;",
            ">;"
        }
    .end annotation

    .annotation runtime Lretrofit2/http/Multipart;
    .end annotation

    .annotation runtime Lretrofit2/http/POST;
        value = "/1.0/outreach/api/images"
    .end annotation
.end method
