.class public Lcom/squareup/server/payment/OtherTenderMessage;
.super Lcom/squareup/server/payment/CreatePaymentMessage;
.source "OtherTenderMessage.java"


# instance fields
.field public final other_tender:Lcom/squareup/server/payment/OtherTender;

.field public final tip_amount_cents:J


# direct methods
.method public constructor <init>(JJLjava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/squareup/server/account/protos/OtherTenderType;Ljava/lang/String;Ljava/util/List;J)V
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JJ",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/AdjustmentMessage;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemizationMessage;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z",
            "Lcom/squareup/server/account/protos/OtherTenderType;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/server/payment/ItemModel;",
            ">;J)V"
        }
    .end annotation

    move-object v13, p0

    move-object/from16 v14, p12

    move-object v0, p0

    move-wide/from16 v1, p1

    move-wide/from16 v3, p3

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move/from16 v11, p11

    move-object/from16 v12, p14

    .line 23
    invoke-direct/range {v0 .. v12}, Lcom/squareup/server/payment/CreatePaymentMessage;-><init>(JJLjava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/util/List;)V

    move-wide/from16 v0, p15

    .line 25
    iput-wide v0, v13, Lcom/squareup/server/payment/OtherTenderMessage;->tip_amount_cents:J

    .line 26
    new-instance v0, Lcom/squareup/server/payment/OtherTender;

    iget-object v1, v14, Lcom/squareup/server/account/protos/OtherTenderType;->tender_type:Ljava/lang/Integer;

    .line 27
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v2, v14, Lcom/squareup/server/account/protos/OtherTenderType;->tender_name:Ljava/lang/String;

    move-object/from16 v3, p13

    invoke-direct {v0, v1, v2, v3}, Lcom/squareup/server/payment/OtherTender;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    iput-object v0, v13, Lcom/squareup/server/payment/OtherTenderMessage;->other_tender:Lcom/squareup/server/payment/OtherTender;

    return-void
.end method
