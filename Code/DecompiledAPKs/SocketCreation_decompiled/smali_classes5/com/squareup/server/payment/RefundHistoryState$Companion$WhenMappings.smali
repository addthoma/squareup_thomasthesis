.class public final synthetic Lcom/squareup/server/payment/RefundHistoryState$Companion$WhenMappings;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I

.field public static final synthetic $EnumSwitchMapping$1:[I


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 8

    invoke-static {}, Lcom/squareup/protos/client/bills/RefundV1$State;->values()[Lcom/squareup/protos/client/bills/RefundV1$State;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/server/payment/RefundHistoryState$Companion$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v0, Lcom/squareup/server/payment/RefundHistoryState$Companion$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/bills/RefundV1$State;->UNKNOWN:Lcom/squareup/protos/client/bills/RefundV1$State;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/RefundV1$State;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/server/payment/RefundHistoryState$Companion$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/bills/RefundV1$State;->REQUESTED:Lcom/squareup/protos/client/bills/RefundV1$State;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/RefundV1$State;->ordinal()I

    move-result v1

    const/4 v3, 0x2

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/server/payment/RefundHistoryState$Companion$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/bills/RefundV1$State;->APPROVED:Lcom/squareup/protos/client/bills/RefundV1$State;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/RefundV1$State;->ordinal()I

    move-result v1

    const/4 v4, 0x3

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/server/payment/RefundHistoryState$Companion$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/bills/RefundV1$State;->COMPLETED:Lcom/squareup/protos/client/bills/RefundV1$State;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/RefundV1$State;->ordinal()I

    move-result v1

    const/4 v5, 0x4

    aput v5, v0, v1

    sget-object v0, Lcom/squareup/server/payment/RefundHistoryState$Companion$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/bills/RefundV1$State;->REJECTED:Lcom/squareup/protos/client/bills/RefundV1$State;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/RefundV1$State;->ordinal()I

    move-result v1

    const/4 v6, 0x5

    aput v6, v0, v1

    sget-object v0, Lcom/squareup/server/payment/RefundHistoryState$Companion$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/client/bills/RefundV1$State;->FAILED:Lcom/squareup/protos/client/bills/RefundV1$State;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/RefundV1$State;->ordinal()I

    move-result v1

    const/4 v7, 0x6

    aput v7, v0, v1

    invoke-static {}, Lcom/squareup/protos/client/bills/Refund$State;->values()[Lcom/squareup/protos/client/bills/Refund$State;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/server/payment/RefundHistoryState$Companion$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v0, Lcom/squareup/server/payment/RefundHistoryState$Companion$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/bills/Refund$State;->UNKNOWN_STATE_DO_NOT_USE:Lcom/squareup/protos/client/bills/Refund$State;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Refund$State;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/server/payment/RefundHistoryState$Companion$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/bills/Refund$State;->NEW:Lcom/squareup/protos/client/bills/Refund$State;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Refund$State;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/server/payment/RefundHistoryState$Companion$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/bills/Refund$State;->PENDING:Lcom/squareup/protos/client/bills/Refund$State;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Refund$State;->ordinal()I

    move-result v1

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/server/payment/RefundHistoryState$Companion$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/bills/Refund$State;->SUCCESS:Lcom/squareup/protos/client/bills/Refund$State;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Refund$State;->ordinal()I

    move-result v1

    aput v5, v0, v1

    sget-object v0, Lcom/squareup/server/payment/RefundHistoryState$Companion$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/client/bills/Refund$State;->FAILED:Lcom/squareup/protos/client/bills/Refund$State;

    invoke-virtual {v1}, Lcom/squareup/protos/client/bills/Refund$State;->ordinal()I

    move-result v1

    aput v6, v0, v1

    return-void
.end method
