.class final Lcom/squareup/server/DefaultFactory;
.super Ljava/lang/Object;
.source "StandardResponse.kt"

# interfaces
.implements Lcom/squareup/server/StandardResponse$Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/squareup/server/StandardResponse$Factory<",
        "TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\\\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\u0010\u001b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0002\u0018\u0000*\u0008\u0008\u0000\u0010\u0001*\u00020\u00022\u0008\u0012\u0004\u0012\u0002H\u00010\u0003BW\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0012\u0010\u0008\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u000c\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000f\u0012\u000e\u0010\u0011\u001a\n\u0012\u0006\u0012\u0004\u0018\u00018\u00000\u0012\u00a2\u0006\u0002\u0010\u0013J\"\u0010\u0015\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u00162\u0012\u0010\u0017\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u00190\u0018H\u0016J\"\u0010\u001a\u001a\u0008\u0012\u0004\u0012\u00028\u00000\u001b2\u0012\u0010\u0017\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u00190\u0018H\u0016J(\u0010\u001c\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00028\u00000\u00160\u001d2\u0012\u0010\u0017\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u00190\u0018H\u0016J(\u0010\u0017\u001a\u000e\u0012\n\u0012\u0008\u0012\u0004\u0012\u00028\u00000\u001b0\u001d2\u0012\u0010\u0017\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u00190\u0018H\u0016R\u0016\u0010\u000e\u001a\u0008\u0012\u0004\u0012\u00020\u00100\u000fX\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010\u0014R\u0016\u0010\u0011\u001a\n\u0012\u0006\u0012\u0004\u0018\u00018\u00000\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0008\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001e"
    }
    d2 = {
        "Lcom/squareup/server/DefaultFactory;",
        "R",
        "",
        "Lcom/squareup/server/StandardResponse$Factory;",
        "standardReceiver",
        "Lcom/squareup/receiving/StandardReceiver;",
        "mainScheduler",
        "Lio/reactivex/Scheduler;",
        "rxCallAdapter",
        "Lretrofit2/CallAdapter;",
        "networkResponseType",
        "Ljava/lang/reflect/Type;",
        "retrofit",
        "Lretrofit2/Retrofit;",
        "annotations",
        "",
        "",
        "call",
        "Lretrofit2/Call;",
        "(Lcom/squareup/receiving/StandardReceiver;Lio/reactivex/Scheduler;Lretrofit2/CallAdapter;Ljava/lang/reflect/Type;Lretrofit2/Retrofit;[Ljava/lang/annotation/Annotation;Lretrofit2/Call;)V",
        "[Ljava/lang/annotation/Annotation;",
        "blocking",
        "Lcom/squareup/receiving/ReceivedResponse;",
        "successOrFailure",
        "Lkotlin/Function1;",
        "",
        "blockingSuccessOrFailure",
        "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;",
        "receivedResponse",
        "Lio/reactivex/Single;",
        "public_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final annotations:[Ljava/lang/annotation/Annotation;

.field private final call:Lretrofit2/Call;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lretrofit2/Call<",
            "TR;>;"
        }
    .end annotation
.end field

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final networkResponseType:Ljava/lang/reflect/Type;

.field private final retrofit:Lretrofit2/Retrofit;

.field private final rxCallAdapter:Lretrofit2/CallAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lretrofit2/CallAdapter<",
            "TR;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final standardReceiver:Lcom/squareup/receiving/StandardReceiver;


# direct methods
.method public constructor <init>(Lcom/squareup/receiving/StandardReceiver;Lio/reactivex/Scheduler;Lretrofit2/CallAdapter;Ljava/lang/reflect/Type;Lretrofit2/Retrofit;[Ljava/lang/annotation/Annotation;Lretrofit2/Call;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/receiving/StandardReceiver;",
            "Lio/reactivex/Scheduler;",
            "Lretrofit2/CallAdapter<",
            "TR;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/reflect/Type;",
            "Lretrofit2/Retrofit;",
            "[",
            "Ljava/lang/annotation/Annotation;",
            "Lretrofit2/Call<",
            "TR;>;)V"
        }
    .end annotation

    const-string v0, "standardReceiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainScheduler"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "rxCallAdapter"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "networkResponseType"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "retrofit"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "annotations"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "call"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/server/DefaultFactory;->standardReceiver:Lcom/squareup/receiving/StandardReceiver;

    iput-object p2, p0, Lcom/squareup/server/DefaultFactory;->mainScheduler:Lio/reactivex/Scheduler;

    iput-object p3, p0, Lcom/squareup/server/DefaultFactory;->rxCallAdapter:Lretrofit2/CallAdapter;

    iput-object p4, p0, Lcom/squareup/server/DefaultFactory;->networkResponseType:Ljava/lang/reflect/Type;

    iput-object p5, p0, Lcom/squareup/server/DefaultFactory;->retrofit:Lretrofit2/Retrofit;

    iput-object p6, p0, Lcom/squareup/server/DefaultFactory;->annotations:[Ljava/lang/annotation/Annotation;

    iput-object p7, p0, Lcom/squareup/server/DefaultFactory;->call:Lretrofit2/Call;

    return-void
.end method

.method public static final synthetic access$getStandardReceiver$p(Lcom/squareup/server/DefaultFactory;)Lcom/squareup/receiving/StandardReceiver;
    .locals 0

    .line 122
    iget-object p0, p0, Lcom/squareup/server/DefaultFactory;->standardReceiver:Lcom/squareup/receiving/StandardReceiver;

    return-object p0
.end method


# virtual methods
.method public blocking(Lkotlin/jvm/functions/Function1;)Lcom/squareup/receiving/ReceivedResponse;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-TR;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lcom/squareup/receiving/ReceivedResponse<",
            "TR;>;"
        }
    .end annotation

    const-string v0, "successOrFailure"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 158
    sget-object v1, Lcom/squareup/receiving/ReceivedResponse;->Companion:Lcom/squareup/receiving/ReceivedResponse$Companion;

    .line 159
    iget-object v2, p0, Lcom/squareup/server/DefaultFactory;->retrofit:Lretrofit2/Retrofit;

    iget-object v3, p0, Lcom/squareup/server/DefaultFactory;->networkResponseType:Ljava/lang/reflect/Type;

    iget-object v4, p0, Lcom/squareup/server/DefaultFactory;->annotations:[Ljava/lang/annotation/Annotation;

    iget-object v6, p0, Lcom/squareup/server/DefaultFactory;->call:Lretrofit2/Call;

    move-object v5, p1

    .line 158
    invoke-virtual/range {v1 .. v6}, Lcom/squareup/receiving/ReceivedResponse$Companion;->receiveFromRetrofitBlocking$public_release(Lretrofit2/Retrofit;Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;Lkotlin/jvm/functions/Function1;Lretrofit2/Call;)Lcom/squareup/receiving/ReceivedResponse;

    move-result-object p1

    return-object p1
.end method

.method public blockingSuccessOrFailure(Lkotlin/jvm/functions/Function1;)Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-TR;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "TR;>;"
        }
    .end annotation

    const-string v0, "successOrFailure"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 164
    invoke-virtual {p0, p1}, Lcom/squareup/server/DefaultFactory;->blocking(Lkotlin/jvm/functions/Function1;)Lcom/squareup/receiving/ReceivedResponse;

    move-result-object p1

    .line 165
    instance-of v0, p1, Lcom/squareup/receiving/ReceivedResponse$Error$SessionExpired;

    if-eqz v0, :cond_0

    .line 168
    iget-object v0, p0, Lcom/squareup/server/DefaultFactory;->mainScheduler:Lio/reactivex/Scheduler;

    new-instance v1, Lcom/squareup/server/DefaultFactory$blockingSuccessOrFailure$1;

    invoke-direct {v1, p0}, Lcom/squareup/server/DefaultFactory$blockingSuccessOrFailure$1;-><init>(Lcom/squareup/server/DefaultFactory;)V

    check-cast v1, Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lio/reactivex/Scheduler;->scheduleDirect(Ljava/lang/Runnable;)Lio/reactivex/disposables/Disposable;

    .line 171
    :cond_0
    invoke-static {p1}, Lcom/squareup/receiving/StandardReceiverKt;->toSuccessOrFailure(Lcom/squareup/receiving/ReceivedResponse;)Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure;

    move-result-object p1

    return-object p1
.end method

.method public receivedResponse(Lkotlin/jvm/functions/Function1;)Lio/reactivex/Single;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-TR;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/ReceivedResponse<",
            "TR;>;>;"
        }
    .end annotation

    const-string v0, "successOrFailure"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 148
    iget-object v0, p0, Lcom/squareup/server/DefaultFactory;->rxCallAdapter:Lretrofit2/CallAdapter;

    iget-object v1, p0, Lcom/squareup/server/DefaultFactory;->call:Lretrofit2/Call;

    invoke-interface {v0, v1}, Lretrofit2/CallAdapter;->adapt(Lretrofit2/Call;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Lio/reactivex/Single;

    .line 149
    iget-object v1, p0, Lcom/squareup/server/DefaultFactory;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v0

    .line 151
    sget-object v1, Lcom/squareup/receiving/ReceivedResponse;->Companion:Lcom/squareup/receiving/ReceivedResponse$Companion;

    .line 152
    iget-object v2, p0, Lcom/squareup/server/DefaultFactory;->retrofit:Lretrofit2/Retrofit;

    iget-object v3, p0, Lcom/squareup/server/DefaultFactory;->networkResponseType:Ljava/lang/reflect/Type;

    iget-object v4, p0, Lcom/squareup/server/DefaultFactory;->annotations:[Ljava/lang/annotation/Annotation;

    .line 151
    invoke-virtual {v1, v2, v3, v4, p1}, Lcom/squareup/receiving/ReceivedResponse$Companion;->receiveFromRetrofit(Lretrofit2/Retrofit;Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;Lkotlin/jvm/functions/Function1;)Lio/reactivex/SingleTransformer;

    move-result-object p1

    .line 150
    invoke-virtual {v0, p1}, Lio/reactivex/Single;->compose(Lio/reactivex/SingleTransformer;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "(rxCallAdapter.adapt(cal\u2026e\n            )\n        )"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    .line 148
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type io.reactivex.Single<R>"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public successOrFailure(Lkotlin/jvm/functions/Function1;)Lio/reactivex/Single;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-TR;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lio/reactivex/Single<",
            "Lcom/squareup/receiving/StandardReceiver$SuccessOrFailure<",
            "TR;>;>;"
        }
    .end annotation

    const-string v0, "successOrFailure"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 136
    iget-object v0, p0, Lcom/squareup/server/DefaultFactory;->rxCallAdapter:Lretrofit2/CallAdapter;

    iget-object v1, p0, Lcom/squareup/server/DefaultFactory;->call:Lretrofit2/Call;

    invoke-interface {v0, v1}, Lretrofit2/CallAdapter;->adapt(Lretrofit2/Call;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Lio/reactivex/Single;

    .line 137
    iget-object v1, p0, Lcom/squareup/server/DefaultFactory;->mainScheduler:Lio/reactivex/Scheduler;

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v0

    .line 139
    iget-object v1, p0, Lcom/squareup/server/DefaultFactory;->standardReceiver:Lcom/squareup/receiving/StandardReceiver;

    .line 140
    iget-object v2, p0, Lcom/squareup/server/DefaultFactory;->retrofit:Lretrofit2/Retrofit;

    iget-object v3, p0, Lcom/squareup/server/DefaultFactory;->networkResponseType:Ljava/lang/reflect/Type;

    iget-object v4, p0, Lcom/squareup/server/DefaultFactory;->annotations:[Ljava/lang/annotation/Annotation;

    .line 139
    invoke-virtual {v1, v2, v3, v4, p1}, Lcom/squareup/receiving/StandardReceiver;->succeedOrFailSingle$public_release(Lretrofit2/Retrofit;Ljava/lang/reflect/Type;[Ljava/lang/annotation/Annotation;Lkotlin/jvm/functions/Function1;)Lio/reactivex/SingleTransformer;

    move-result-object p1

    .line 138
    invoke-virtual {v0, p1}, Lio/reactivex/Single;->compose(Lio/reactivex/SingleTransformer;)Lio/reactivex/Single;

    move-result-object p1

    const-string v0, "(rxCallAdapter.adapt(cal\u2026e\n            )\n        )"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1

    .line 136
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type io.reactivex.Single<R>"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method
