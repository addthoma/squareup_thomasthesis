.class public final Lcom/squareup/server/RestAdapterReleaseModule_ProvideGsonServiceCreatorFactory;
.super Ljava/lang/Object;
.source "RestAdapterReleaseModule_ProvideGsonServiceCreatorFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/api/ServiceCreator;",
        ">;"
    }
.end annotation


# instance fields
.field private final restAdapterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lretrofit/RestAdapter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lretrofit/RestAdapter;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/server/RestAdapterReleaseModule_ProvideGsonServiceCreatorFactory;->restAdapterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/server/RestAdapterReleaseModule_ProvideGsonServiceCreatorFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lretrofit/RestAdapter;",
            ">;)",
            "Lcom/squareup/server/RestAdapterReleaseModule_ProvideGsonServiceCreatorFactory;"
        }
    .end annotation

    .line 33
    new-instance v0, Lcom/squareup/server/RestAdapterReleaseModule_ProvideGsonServiceCreatorFactory;

    invoke-direct {v0, p0}, Lcom/squareup/server/RestAdapterReleaseModule_ProvideGsonServiceCreatorFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideGsonServiceCreator(Lretrofit/RestAdapter;)Lcom/squareup/api/ServiceCreator;
    .locals 1

    .line 37
    invoke-static {p0}, Lcom/squareup/server/RestAdapterReleaseModule;->provideGsonServiceCreator(Lretrofit/RestAdapter;)Lcom/squareup/api/ServiceCreator;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/api/ServiceCreator;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/api/ServiceCreator;
    .locals 1

    .line 28
    iget-object v0, p0, Lcom/squareup/server/RestAdapterReleaseModule_ProvideGsonServiceCreatorFactory;->restAdapterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lretrofit/RestAdapter;

    invoke-static {v0}, Lcom/squareup/server/RestAdapterReleaseModule_ProvideGsonServiceCreatorFactory;->provideGsonServiceCreator(Lretrofit/RestAdapter;)Lcom/squareup/api/ServiceCreator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/server/RestAdapterReleaseModule_ProvideGsonServiceCreatorFactory;->get()Lcom/squareup/api/ServiceCreator;

    move-result-object v0

    return-object v0
.end method
