.class public Lcom/squareup/server/ItemImageUploadResponse;
.super Lcom/squareup/server/SimpleResponse;
.source "ItemImageUploadResponse.java"


# instance fields
.field public final image_url:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 11
    invoke-direct {p0, v0}, Lcom/squareup/server/ItemImageUploadResponse;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x1

    .line 15
    invoke-direct {p0, v0}, Lcom/squareup/server/SimpleResponse;-><init>(Z)V

    .line 16
    iput-object p1, p0, Lcom/squareup/server/ItemImageUploadResponse;->image_url:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final isSuccessful()Z
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/squareup/server/ItemImageUploadResponse;->image_url:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method
