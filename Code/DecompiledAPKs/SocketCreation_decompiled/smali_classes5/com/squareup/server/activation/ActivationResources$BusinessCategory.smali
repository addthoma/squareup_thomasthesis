.class public final Lcom/squareup/server/activation/ActivationResources$BusinessCategory;
.super Ljava/lang/Object;
.source "ActivationResources.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/server/activation/ActivationResources;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BusinessCategory"
.end annotation


# static fields
.field public static final SHOW_BUSINESS_INFO:Z = false

.field public static final SKIP_BUSINESS_INFO:Z = true


# instance fields
.field public final description:Ljava/lang/String;

.field public final key:Ljava/lang/String;

.field public final skip_business_info:Z

.field private final sub_categories:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/server/activation/ActivationResources$BusinessSubcategory;",
            ">;"
        }
    .end annotation
.end field

.field public final sub_categories_title:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lcom/squareup/server/activation/ActivationResources$BusinessSubcategory;",
            ">;)V"
        }
    .end annotation

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object p1, p0, Lcom/squareup/server/activation/ActivationResources$BusinessCategory;->key:Ljava/lang/String;

    .line 55
    iput-object p2, p0, Lcom/squareup/server/activation/ActivationResources$BusinessCategory;->description:Ljava/lang/String;

    .line 56
    iput-boolean p3, p0, Lcom/squareup/server/activation/ActivationResources$BusinessCategory;->skip_business_info:Z

    .line 57
    iput-object p4, p0, Lcom/squareup/server/activation/ActivationResources$BusinessCategory;->sub_categories_title:Ljava/lang/String;

    .line 58
    iput-object p5, p0, Lcom/squareup/server/activation/ActivationResources$BusinessCategory;->sub_categories:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public getSubCategories()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/server/activation/ActivationResources$BusinessSubcategory;",
            ">;"
        }
    .end annotation

    .line 62
    iget-object v0, p0, Lcom/squareup/server/activation/ActivationResources$BusinessCategory;->sub_categories:Ljava/util/List;

    if-nez v0, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 63
    :cond_0
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .line 67
    iget-object v0, p0, Lcom/squareup/server/activation/ActivationResources$BusinessCategory;->description:Ljava/lang/String;

    return-object v0
.end method
