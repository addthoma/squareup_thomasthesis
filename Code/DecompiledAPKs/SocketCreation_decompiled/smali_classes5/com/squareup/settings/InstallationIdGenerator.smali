.class public final Lcom/squareup/settings/InstallationIdGenerator;
.super Ljava/lang/Object;
.source "InstallationIdGenerator.java"


# instance fields
.field private volatile installationId:Ljava/lang/String;

.field private final unique:Lcom/squareup/util/Unique;


# direct methods
.method constructor <init>(Lcom/squareup/util/Unique;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/squareup/settings/InstallationIdGenerator;->unique:Lcom/squareup/util/Unique;

    return-void
.end method


# virtual methods
.method public getInstallationId(Lcom/squareup/settings/LocalSetting;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/settings/LocalSetting<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 23
    iget-object v0, p0, Lcom/squareup/settings/InstallationIdGenerator;->installationId:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 24
    invoke-interface {p1}, Lcom/squareup/settings/LocalSetting;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/squareup/settings/InstallationIdGenerator;->installationId:Ljava/lang/String;

    .line 27
    :cond_0
    iget-object v0, p0, Lcom/squareup/settings/InstallationIdGenerator;->installationId:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 28
    iget-object v0, p0, Lcom/squareup/settings/InstallationIdGenerator;->unique:Lcom/squareup/util/Unique;

    invoke-interface {v0}, Lcom/squareup/util/Unique;->generate()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/settings/InstallationIdGenerator;->installationId:Ljava/lang/String;

    .line 29
    iget-object v0, p0, Lcom/squareup/settings/InstallationIdGenerator;->installationId:Ljava/lang/String;

    invoke-interface {p1, v0}, Lcom/squareup/settings/LocalSetting;->set(Ljava/lang/Object;)V

    .line 31
    :cond_1
    iget-object p1, p0, Lcom/squareup/settings/InstallationIdGenerator;->installationId:Ljava/lang/String;

    if-eqz p1, :cond_2

    .line 32
    iget-object p1, p0, Lcom/squareup/settings/InstallationIdGenerator;->installationId:Ljava/lang/String;

    return-object p1

    .line 31
    :cond_2
    new-instance p1, Ljava/lang/AssertionError;

    const-string v0, "null installationId"

    invoke-direct {p1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw p1
.end method
