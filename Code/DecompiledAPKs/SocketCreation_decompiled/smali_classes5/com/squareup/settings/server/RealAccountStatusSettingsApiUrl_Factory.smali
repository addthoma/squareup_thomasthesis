.class public final Lcom/squareup/settings/server/RealAccountStatusSettingsApiUrl_Factory;
.super Ljava/lang/Object;
.source "RealAccountStatusSettingsApiUrl_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/settings/server/RealAccountStatusSettingsApiUrl;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/account/protos/AccountStatusResponse;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/account/protos/AccountStatusResponse;",
            ">;)V"
        }
    .end annotation

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/squareup/settings/server/RealAccountStatusSettingsApiUrl_Factory;->arg0Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/settings/server/RealAccountStatusSettingsApiUrl_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/account/protos/AccountStatusResponse;",
            ">;)",
            "Lcom/squareup/settings/server/RealAccountStatusSettingsApiUrl_Factory;"
        }
    .end annotation

    .line 26
    new-instance v0, Lcom/squareup/settings/server/RealAccountStatusSettingsApiUrl_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/settings/server/RealAccountStatusSettingsApiUrl_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Ljavax/inject/Provider;)Lcom/squareup/settings/server/RealAccountStatusSettingsApiUrl;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/server/account/protos/AccountStatusResponse;",
            ">;)",
            "Lcom/squareup/settings/server/RealAccountStatusSettingsApiUrl;"
        }
    .end annotation

    .line 30
    new-instance v0, Lcom/squareup/settings/server/RealAccountStatusSettingsApiUrl;

    invoke-direct {v0, p0}, Lcom/squareup/settings/server/RealAccountStatusSettingsApiUrl;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/settings/server/RealAccountStatusSettingsApiUrl;
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/squareup/settings/server/RealAccountStatusSettingsApiUrl_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-static {v0}, Lcom/squareup/settings/server/RealAccountStatusSettingsApiUrl_Factory;->newInstance(Ljavax/inject/Provider;)Lcom/squareup/settings/server/RealAccountStatusSettingsApiUrl;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/settings/server/RealAccountStatusSettingsApiUrl_Factory;->get()Lcom/squareup/settings/server/RealAccountStatusSettingsApiUrl;

    move-result-object v0

    return-object v0
.end method
