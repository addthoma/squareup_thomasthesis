.class public Lcom/squareup/settings/server/StoreAndForwardSettings;
.super Ljava/lang/Object;
.source "StoreAndForwardSettings.java"


# static fields
.field private static final MILLIS_PER_HOUR:J = 0x36ee80L


# instance fields
.field private final accountStatusProvider:Lcom/squareup/accountstatus/AccountStatusProvider;

.field private final paymentExpirationMillis:J

.field private final singleTransactionLimit:Lcom/squareup/protos/common/Money;

.field private final storeAndForwardBillKey:Lcom/squareup/server/account/protos/StoreAndForwardKey;

.field private final storeAndForwardEnabled:Z

.field private final storeAndForwardKey:Lcom/squareup/server/account/protos/StoreAndForwardKey;

.field private final userCredential:Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;


# direct methods
.method public constructor <init>(ZLcom/squareup/protos/common/Money;Ljava/lang/Integer;Lcom/squareup/accountstatus/AccountStatusProvider;Lcom/squareup/server/account/protos/StoreAndForwardKey;Lcom/squareup/server/account/protos/StoreAndForwardKey;Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;)V
    .locals 0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p4, p0, Lcom/squareup/settings/server/StoreAndForwardSettings;->accountStatusProvider:Lcom/squareup/accountstatus/AccountStatusProvider;

    .line 27
    iput-boolean p1, p0, Lcom/squareup/settings/server/StoreAndForwardSettings;->storeAndForwardEnabled:Z

    .line 28
    iput-object p5, p0, Lcom/squareup/settings/server/StoreAndForwardSettings;->storeAndForwardKey:Lcom/squareup/server/account/protos/StoreAndForwardKey;

    .line 29
    iput-object p6, p0, Lcom/squareup/settings/server/StoreAndForwardSettings;->storeAndForwardBillKey:Lcom/squareup/server/account/protos/StoreAndForwardKey;

    .line 30
    iput-object p7, p0, Lcom/squareup/settings/server/StoreAndForwardSettings;->userCredential:Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;

    const-wide/16 p4, 0x0

    if-eqz p2, :cond_0

    .line 34
    iget-object p1, p2, Lcom/squareup/protos/common/Money;->amount:Ljava/lang/Long;

    .line 35
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide p6

    cmp-long p1, p6, p4

    if-gtz p1, :cond_1

    :cond_0
    const/4 p2, 0x0

    :cond_1
    iput-object p2, p0, Lcom/squareup/settings/server/StoreAndForwardSettings;->singleTransactionLimit:Lcom/squareup/protos/common/Money;

    if-nez p3, :cond_2

    goto :goto_0

    .line 42
    :cond_2
    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result p1

    int-to-long p1, p1

    const-wide/16 p3, 0x3e8

    mul-long p4, p1, p3

    :goto_0
    iput-wide p4, p0, Lcom/squareup/settings/server/StoreAndForwardSettings;->paymentExpirationMillis:J

    return-void
.end method

.method private doSet(Ljava/lang/Boolean;Lcom/squareup/protos/common/Money;)V
    .locals 2

    .line 87
    iget-object v0, p0, Lcom/squareup/settings/server/StoreAndForwardSettings;->accountStatusProvider:Lcom/squareup/accountstatus/AccountStatusProvider;

    new-instance v1, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    invoke-direct {v1}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;-><init>()V

    .line 88
    invoke-virtual {v1, p1}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->store_and_forward_enabled(Ljava/lang/Boolean;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object p1

    .line 89
    invoke-virtual {p1, p2}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->store_and_forward_single_transaction_limit(Lcom/squareup/protos/common/Money;)Lcom/squareup/server/account/protos/PreferencesRequest$Builder;

    move-result-object p1

    .line 90
    invoke-virtual {p1}, Lcom/squareup/server/account/protos/PreferencesRequest$Builder;->build()Lcom/squareup/server/account/protos/PreferencesRequest;

    move-result-object p1

    .line 87
    invoke-interface {v0, p1}, Lcom/squareup/accountstatus/AccountStatusProvider;->maybeUpdatePreferences(Lcom/squareup/server/account/protos/PreferencesRequest;)V

    return-void
.end method


# virtual methods
.method public getPaymentExpirationHours()I
    .locals 4

    .line 58
    invoke-virtual {p0}, Lcom/squareup/settings/server/StoreAndForwardSettings;->getPaymentExpirationMillis()J

    move-result-wide v0

    const-wide/32 v2, 0x36ee80

    div-long/2addr v0, v2

    long-to-int v1, v0

    return v1
.end method

.method public getPaymentExpirationMillis()J
    .locals 2

    .line 54
    iget-wide v0, p0, Lcom/squareup/settings/server/StoreAndForwardSettings;->paymentExpirationMillis:J

    return-wide v0
.end method

.method public getSingleTransactionLimit()Lcom/squareup/protos/common/Money;
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/squareup/settings/server/StoreAndForwardSettings;->singleTransactionLimit:Lcom/squareup/protos/common/Money;

    return-object v0
.end method

.method public getStoreAndForwardBillKey()Lcom/squareup/server/account/protos/StoreAndForwardKey;
    .locals 1

    .line 66
    iget-object v0, p0, Lcom/squareup/settings/server/StoreAndForwardSettings;->storeAndForwardBillKey:Lcom/squareup/server/account/protos/StoreAndForwardKey;

    return-object v0
.end method

.method public getStoreAndForwardKey()Lcom/squareup/server/account/protos/StoreAndForwardKey;
    .locals 1

    .line 62
    iget-object v0, p0, Lcom/squareup/settings/server/StoreAndForwardSettings;->storeAndForwardKey:Lcom/squareup/server/account/protos/StoreAndForwardKey;

    return-object v0
.end method

.method public getUserCredential()Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;
    .locals 1

    .line 94
    iget-object v0, p0, Lcom/squareup/settings/server/StoreAndForwardSettings;->userCredential:Lcom/squareup/server/account/protos/StoreAndForwardUserCredential;

    return-object v0
.end method

.method public isStoreAndForwardEnabled()Z
    .locals 1

    .line 46
    iget-boolean v0, p0, Lcom/squareup/settings/server/StoreAndForwardSettings;->storeAndForwardEnabled:Z

    return v0
.end method

.method public setEnabled(Ljava/lang/Boolean;)V
    .locals 1

    const/4 v0, 0x0

    .line 74
    invoke-direct {p0, p1, v0}, Lcom/squareup/settings/server/StoreAndForwardSettings;->doSet(Ljava/lang/Boolean;Lcom/squareup/protos/common/Money;)V

    return-void
.end method

.method public setTransactionLimit(Lcom/squareup/protos/common/Money;)V
    .locals 1

    const/4 v0, 0x0

    .line 83
    invoke-direct {p0, v0, p1}, Lcom/squareup/settings/server/StoreAndForwardSettings;->doSet(Ljava/lang/Boolean;Lcom/squareup/protos/common/Money;)V

    return-void
.end method
