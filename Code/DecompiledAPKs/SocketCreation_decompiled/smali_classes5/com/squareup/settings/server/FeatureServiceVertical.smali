.class public abstract Lcom/squareup/settings/server/FeatureServiceVertical;
.super Ljava/lang/Object;
.source "FeatureServiceVertical.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/settings/server/FeatureServiceVertical$APos;,
        Lcom/squareup/settings/server/FeatureServiceVertical$IPos;,
        Lcom/squareup/settings/server/FeatureServiceVertical$ReaderSdk;,
        Lcom/squareup/settings/server/FeatureServiceVertical$SPos;,
        Lcom/squareup/settings/server/FeatureServiceVertical$T2Pos;,
        Lcom/squareup/settings/server/FeatureServiceVertical$X2Pos;,
        Lcom/squareup/settings/server/FeatureServiceVertical$DevelopmentApp;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0002\u0008\n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0007\u0008\t\n\u000b\u000c\r\u000eB\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0011\u0010\u0003\u001a\u00020\u00048F\u00a2\u0006\u0006\u001a\u0004\u0008\u0003\u0010\u0005R\u0011\u0010\u0006\u001a\u00020\u00048F\u00a2\u0006\u0006\u001a\u0004\u0008\u0006\u0010\u0005R\u0011\u0010\u0007\u001a\u00020\u00048F\u00a2\u0006\u0006\u001a\u0004\u0008\u0007\u0010\u0005\u0082\u0001\u0007\u000f\u0010\u0011\u0012\u0013\u0014\u0015\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/settings/server/FeatureServiceVertical;",
        "",
        "()V",
        "isT2",
        "",
        "()Z",
        "isX2",
        "isX2Rst",
        "APos",
        "DevelopmentApp",
        "IPos",
        "ReaderSdk",
        "SPos",
        "T2Pos",
        "X2Pos",
        "Lcom/squareup/settings/server/FeatureServiceVertical$APos;",
        "Lcom/squareup/settings/server/FeatureServiceVertical$IPos;",
        "Lcom/squareup/settings/server/FeatureServiceVertical$ReaderSdk;",
        "Lcom/squareup/settings/server/FeatureServiceVertical$SPos;",
        "Lcom/squareup/settings/server/FeatureServiceVertical$T2Pos;",
        "Lcom/squareup/settings/server/FeatureServiceVertical$X2Pos;",
        "Lcom/squareup/settings/server/FeatureServiceVertical$DevelopmentApp;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 11
    invoke-direct {p0}, Lcom/squareup/settings/server/FeatureServiceVertical;-><init>()V

    return-void
.end method


# virtual methods
.method public final isT2()Z
    .locals 1

    .line 31
    instance-of v0, p0, Lcom/squareup/settings/server/FeatureServiceVertical$T2Pos;

    return v0
.end method

.method public final isX2()Z
    .locals 1

    .line 30
    instance-of v0, p0, Lcom/squareup/settings/server/FeatureServiceVertical$X2Pos;

    return v0
.end method

.method public final isX2Rst()Z
    .locals 1

    .line 32
    instance-of v0, p0, Lcom/squareup/settings/server/FeatureServiceVertical$X2Pos$X2Rst;

    return v0
.end method
