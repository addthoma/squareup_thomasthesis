.class Lcom/squareup/settings/server/RealFeesEditor$1;
.super Ljava/lang/Object;
.source "RealFeesEditor.java"

# interfaces
.implements Lcom/squareup/shared/catalog/CatalogCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/settings/server/RealFeesEditor;->read(Ljava/lang/Runnable;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/shared/catalog/CatalogCallback<",
        "Lcom/squareup/settings/server/RealFeesEditor$Fees;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/settings/server/RealFeesEditor;

.field final synthetic val$callback:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(Lcom/squareup/settings/server/RealFeesEditor;Ljava/lang/Runnable;)V
    .locals 0

    .line 153
    iput-object p1, p0, Lcom/squareup/settings/server/RealFeesEditor$1;->this$0:Lcom/squareup/settings/server/RealFeesEditor;

    iput-object p2, p0, Lcom/squareup/settings/server/RealFeesEditor$1;->val$callback:Ljava/lang/Runnable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public call(Lcom/squareup/shared/catalog/CatalogResult;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/CatalogResult<",
            "Lcom/squareup/settings/server/RealFeesEditor$Fees;",
            ">;)V"
        }
    .end annotation

    .line 155
    iget-object v0, p0, Lcom/squareup/settings/server/RealFeesEditor$1;->this$0:Lcom/squareup/settings/server/RealFeesEditor;

    iget-object v1, p0, Lcom/squareup/settings/server/RealFeesEditor$1;->val$callback:Ljava/lang/Runnable;

    invoke-static {v0, p1, v1}, Lcom/squareup/settings/server/RealFeesEditor;->access$000(Lcom/squareup/settings/server/RealFeesEditor;Lcom/squareup/shared/catalog/CatalogResult;Ljava/lang/Runnable;)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 159
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " => "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/settings/server/RealFeesEditor$1;->val$callback:Ljava/lang/Runnable;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
