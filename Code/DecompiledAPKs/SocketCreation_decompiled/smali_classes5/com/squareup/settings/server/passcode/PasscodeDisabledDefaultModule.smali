.class public abstract Lcom/squareup/settings/server/passcode/PasscodeDisabledDefaultModule;
.super Ljava/lang/Object;
.source "PasscodeDisabledDefaultModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method abstract provideGuestModeDefault(Lcom/squareup/settings/server/passcode/GuestModeDefault$GuestModeDisabled;)Lcom/squareup/settings/server/passcode/GuestModeDefault;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract providePasscodeEmployeeManagementDefault(Lcom/squareup/settings/server/passcode/PasscodeEmployeeManagementDefault$PasscodeEmployeeManagementDisabled;)Lcom/squareup/settings/server/passcode/PasscodeEmployeeManagementDefault;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
