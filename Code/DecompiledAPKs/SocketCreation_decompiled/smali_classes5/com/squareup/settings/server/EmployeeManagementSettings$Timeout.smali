.class public final enum Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;
.super Ljava/lang/Enum;
.source "EmployeeManagementSettings.java"

# interfaces
.implements Lcom/squareup/settings/server/PasscodeTimeout;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/settings/server/EmployeeManagementSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Timeout"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;",
        ">;",
        "Lcom/squareup/settings/server/PasscodeTimeout;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;

.field public static final enum NEVER:Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;

.field public static final enum TIME_1M:Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;

.field public static final enum TIME_30S:Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;

.field public static final enum TIME_5M:Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;


# instance fields
.field private final serverName:Ljava/lang/String;

.field private final shouldLockOnAppBackgrounded:Z

.field private final timeoutMs:I


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .line 92
    new-instance v6, Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;

    const-string v1, "NEVER"

    const/4 v2, 0x0

    const-string v3, "employee_management_inactivity_timeout_none"

    const/4 v4, -0x1

    const/4 v5, 0x0

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;-><init>(Ljava/lang/String;ILjava/lang/String;IZ)V

    sput-object v6, Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;->NEVER:Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;

    .line 93
    new-instance v0, Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;

    const-string v8, "TIME_30S"

    const/4 v9, 0x1

    const-string v10, "employee_management_inactivity_timeout_thirty_seconds"

    const/16 v11, 0x7530

    const/4 v12, 0x1

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;-><init>(Ljava/lang/String;ILjava/lang/String;IZ)V

    sput-object v0, Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;->TIME_30S:Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;

    .line 94
    new-instance v0, Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;

    const-string v2, "TIME_1M"

    const/4 v3, 0x2

    const-string v4, "employee_management_inactivity_timeout_one_minute"

    const v5, 0xea60

    const/4 v6, 0x1

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;-><init>(Ljava/lang/String;ILjava/lang/String;IZ)V

    sput-object v0, Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;->TIME_1M:Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;

    .line 95
    new-instance v0, Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;

    const-string v8, "TIME_5M"

    const/4 v9, 0x3

    const-string v10, "employee_management_inactivity_timeout_five_minutes"

    const v11, 0x493e0

    move-object v7, v0

    invoke-direct/range {v7 .. v12}, Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;-><init>(Ljava/lang/String;ILjava/lang/String;IZ)V

    sput-object v0, Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;->TIME_5M:Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;

    .line 91
    sget-object v1, Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;->NEVER:Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;->TIME_30S:Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;

    const/4 v2, 0x1

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;->TIME_1M:Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;

    const/4 v2, 0x2

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;->TIME_5M:Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;

    const/4 v2, 0x3

    aput-object v1, v0, v2

    sput-object v0, Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;->$VALUES:[Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;IZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "IZ)V"
        }
    .end annotation

    .line 101
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 102
    iput-object p3, p0, Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;->serverName:Ljava/lang/String;

    .line 103
    iput p4, p0, Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;->timeoutMs:I

    .line 104
    iput-boolean p5, p0, Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;->shouldLockOnAppBackgrounded:Z

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;
    .locals 5

    .line 123
    invoke-static {}, Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;->values()[Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    .line 124
    invoke-virtual {v3}, Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;->serverName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    return-object v3

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;
    .locals 1

    .line 91
    const-class v0, Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;

    return-object p0
.end method

.method public static values()[Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;
    .locals 1

    .line 91
    sget-object v0, Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;->$VALUES:[Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;

    invoke-virtual {v0}, [Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;

    return-object v0
.end method


# virtual methods
.method public serverName()Ljava/lang/String;
    .locals 1

    .line 114
    iget-object v0, p0, Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;->serverName:Ljava/lang/String;

    return-object v0
.end method

.method public shouldLockOnAppBackgrounded()Z
    .locals 1

    .line 109
    iget-boolean v0, p0, Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;->shouldLockOnAppBackgrounded:Z

    return v0
.end method

.method public timeoutInMs()I
    .locals 1

    .line 119
    iget v0, p0, Lcom/squareup/settings/server/EmployeeManagementSettings$Timeout;->timeoutMs:I

    return v0
.end method
