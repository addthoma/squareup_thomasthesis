.class public final Lcom/squareup/settings/RealLocalSettingFactory_Factory;
.super Ljava/lang/Object;
.source "RealLocalSettingFactory_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/settings/RealLocalSettingFactory;",
        ">;"
    }
.end annotation


# instance fields
.field private final arg0Provider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/content/SharedPreferences;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/content/SharedPreferences;",
            ">;)V"
        }
    .end annotation

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/squareup/settings/RealLocalSettingFactory_Factory;->arg0Provider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/settings/RealLocalSettingFactory_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/content/SharedPreferences;",
            ">;)",
            "Lcom/squareup/settings/RealLocalSettingFactory_Factory;"
        }
    .end annotation

    .line 25
    new-instance v0, Lcom/squareup/settings/RealLocalSettingFactory_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/settings/RealLocalSettingFactory_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Landroid/content/SharedPreferences;)Lcom/squareup/settings/RealLocalSettingFactory;
    .locals 1

    .line 29
    new-instance v0, Lcom/squareup/settings/RealLocalSettingFactory;

    invoke-direct {v0, p0}, Lcom/squareup/settings/RealLocalSettingFactory;-><init>(Landroid/content/SharedPreferences;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/settings/RealLocalSettingFactory;
    .locals 1

    .line 21
    iget-object v0, p0, Lcom/squareup/settings/RealLocalSettingFactory_Factory;->arg0Provider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    invoke-static {v0}, Lcom/squareup/settings/RealLocalSettingFactory_Factory;->newInstance(Landroid/content/SharedPreferences;)Lcom/squareup/settings/RealLocalSettingFactory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/settings/RealLocalSettingFactory_Factory;->get()Lcom/squareup/settings/RealLocalSettingFactory;

    move-result-object v0

    return-object v0
.end method
