.class public Lcom/squareup/settings/GsonLocalSetting;
.super Lcom/squareup/settings/AbstractLocalSetting;
.source "GsonLocalSetting.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/squareup/settings/AbstractLocalSetting<",
        "TT;>;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private cache:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private final gson:Lcom/google/gson/Gson;

.field private final type:Ljava/lang/reflect/Type;


# direct methods
.method private constructor <init>(Landroid/content/SharedPreferences;Ljava/lang/String;Lcom/google/gson/Gson;Ljava/lang/reflect/Type;)V
    .locals 0

    .line 31
    invoke-direct {p0, p1, p2}, Lcom/squareup/settings/AbstractLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    .line 32
    iput-object p3, p0, Lcom/squareup/settings/GsonLocalSetting;->gson:Lcom/google/gson/Gson;

    .line 33
    iput-object p4, p0, Lcom/squareup/settings/GsonLocalSetting;->type:Ljava/lang/reflect/Type;

    return-void
.end method

.method public static forClass(Landroid/content/SharedPreferences;Ljava/lang/String;Lcom/google/gson/Gson;Ljava/lang/Class;)Lcom/squareup/settings/GsonLocalSetting;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/content/SharedPreferences;",
            "Ljava/lang/String;",
            "Lcom/google/gson/Gson;",
            "Ljava/lang/Class<",
            "TT;>;)",
            "Lcom/squareup/settings/GsonLocalSetting<",
            "TT;>;"
        }
    .end annotation

    .line 22
    invoke-static {p0, p1, p2, p3}, Lcom/squareup/settings/GsonLocalSetting;->forType(Landroid/content/SharedPreferences;Ljava/lang/String;Lcom/google/gson/Gson;Ljava/lang/reflect/Type;)Lcom/squareup/settings/GsonLocalSetting;

    move-result-object p0

    return-object p0
.end method

.method public static forType(Landroid/content/SharedPreferences;Ljava/lang/String;Lcom/google/gson/Gson;Ljava/lang/reflect/Type;)Lcom/squareup/settings/GsonLocalSetting;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/content/SharedPreferences;",
            "Ljava/lang/String;",
            "Lcom/google/gson/Gson;",
            "Ljava/lang/reflect/Type;",
            ")",
            "Lcom/squareup/settings/GsonLocalSetting<",
            "TT;>;"
        }
    .end annotation

    .line 27
    new-instance v0, Lcom/squareup/settings/GsonLocalSetting;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/settings/GsonLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;Lcom/google/gson/Gson;Ljava/lang/reflect/Type;)V

    return-object v0
.end method


# virtual methods
.method public clearCache()V
    .locals 1

    const/4 v0, 0x0

    .line 59
    iput-object v0, p0, Lcom/squareup/settings/GsonLocalSetting;->cache:Ljava/lang/Object;

    return-void
.end method

.method protected doGet()Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .line 43
    iget-object v0, p0, Lcom/squareup/settings/GsonLocalSetting;->cache:Ljava/lang/Object;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/settings/GsonLocalSetting;->gson:Lcom/google/gson/Gson;

    iget-object v1, p0, Lcom/squareup/settings/GsonLocalSetting;->preferences:Landroid/content/SharedPreferences;

    iget-object v2, p0, Lcom/squareup/settings/GsonLocalSetting;->key:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/settings/GsonLocalSetting;->type:Ljava/lang/reflect/Type;

    invoke-virtual {v0, v1, v2}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/settings/GsonLocalSetting;->cache:Ljava/lang/Object;

    .line 44
    :cond_0
    iget-object v0, p0, Lcom/squareup/settings/GsonLocalSetting;->cache:Ljava/lang/Object;

    return-object v0
.end method

.method public get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)TT;"
        }
    .end annotation

    .line 37
    iget-object v0, p0, Lcom/squareup/settings/GsonLocalSetting;->cache:Ljava/lang/Object;

    if-eqz v0, :cond_0

    return-object v0

    .line 38
    :cond_0
    invoke-super {p0, p1}, Lcom/squareup/settings/AbstractLocalSetting;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public remove()V
    .locals 1

    const/4 v0, 0x0

    .line 53
    iput-object v0, p0, Lcom/squareup/settings/GsonLocalSetting;->cache:Ljava/lang/Object;

    .line 54
    invoke-super {p0}, Lcom/squareup/settings/AbstractLocalSetting;->remove()V

    return-void
.end method

.method public set(Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .line 48
    iput-object p1, p0, Lcom/squareup/settings/GsonLocalSetting;->cache:Ljava/lang/Object;

    .line 49
    iget-object v0, p0, Lcom/squareup/settings/GsonLocalSetting;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/settings/GsonLocalSetting;->key:Ljava/lang/String;

    iget-object v2, p0, Lcom/squareup/settings/GsonLocalSetting;->gson:Lcom/google/gson/Gson;

    invoke-virtual {v2, p1}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/settings/GsonLocalSetting;->apply(Landroid/content/SharedPreferences$Editor;)V

    return-void
.end method
