.class public Lcom/squareup/settings/InstallationIdModule;
.super Ljava/lang/Object;
.source "InstallationIdModule.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation


# static fields
.field private static final INSTALLATION_ID:Ljava/lang/String; = "installation-id"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideInstallationId(Landroid/content/SharedPreferences;Lcom/squareup/settings/InstallationIdGenerator;)Ljava/lang/String;
    .locals 2
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 15
    new-instance v0, Lcom/squareup/settings/StringLocalSetting;

    const-string v1, "installation-id"

    invoke-direct {v0, p0, v1}, Lcom/squareup/settings/StringLocalSetting;-><init>(Landroid/content/SharedPreferences;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/squareup/settings/InstallationIdGenerator;->getInstallationId(Lcom/squareup/settings/LocalSetting;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method
