.class public Lcom/squareup/settings/MapPreferenceAdapter;
.super Ljava/lang/Object;
.source "MapPreferenceAdapter.java"

# interfaces
.implements Lcom/f2prateek/rx/preferences2/Preference$Converter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/f2prateek/rx/preferences2/Preference$Converter<",
        "Ljava/util/Map<",
        "TK;TV;>;>;"
    }
.end annotation


# instance fields
.field private final gson:Lcom/google/gson/Gson;


# direct methods
.method public constructor <init>(Lcom/google/gson/Gson;)V
    .locals 0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/squareup/settings/MapPreferenceAdapter;->gson:Lcom/google/gson/Gson;

    return-void
.end method


# virtual methods
.method public bridge synthetic deserialize(Ljava/lang/String;)Ljava/lang/Object;
    .locals 0

    .line 11
    invoke-virtual {p0, p1}, Lcom/squareup/settings/MapPreferenceAdapter;->deserialize(Ljava/lang/String;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public deserialize(Ljava/lang/String;)Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map<",
            "TK;TV;>;"
        }
    .end annotation

    .line 20
    new-instance v0, Lcom/squareup/settings/MapPreferenceAdapter$1;

    invoke-direct {v0, p0}, Lcom/squareup/settings/MapPreferenceAdapter$1;-><init>(Lcom/squareup/settings/MapPreferenceAdapter;)V

    invoke-virtual {v0}, Lcom/squareup/settings/MapPreferenceAdapter$1;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    .line 21
    iget-object v1, p0, Lcom/squareup/settings/MapPreferenceAdapter;->gson:Lcom/google/gson/Gson;

    invoke-virtual {v1, p1, v0}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    if-nez p1, :cond_0

    .line 23
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object p1

    :cond_0
    return-object p1
.end method

.method public bridge synthetic serialize(Ljava/lang/Object;)Ljava/lang/String;
    .locals 0

    .line 11
    check-cast p1, Ljava/util/Map;

    invoke-virtual {p0, p1}, Lcom/squareup/settings/MapPreferenceAdapter;->serialize(Ljava/util/Map;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public serialize(Ljava/util/Map;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "TK;TV;>;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .line 29
    iget-object v0, p0, Lcom/squareup/settings/MapPreferenceAdapter;->gson:Lcom/google/gson/Gson;

    new-instance v1, Lcom/squareup/settings/MapPreferenceAdapter$2;

    invoke-direct {v1, p0}, Lcom/squareup/settings/MapPreferenceAdapter$2;-><init>(Lcom/squareup/settings/MapPreferenceAdapter;)V

    invoke-virtual {v1}, Lcom/squareup/settings/MapPreferenceAdapter$2;->getType()Ljava/lang/reflect/Type;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;Ljava/lang/reflect/Type;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method
