.class public final Lcom/squareup/settings/LoggedInSettingsModule_ProvideCashReportEmailRecipientFactory;
.super Ljava/lang/Object;
.source "LoggedInSettingsModule_ProvideCashReportEmailRecipientFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/settings/StringLocalSetting;",
        ">;"
    }
.end annotation


# instance fields
.field private final preferencesProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/content/SharedPreferences;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/content/SharedPreferences;",
            ">;)V"
        }
    .end annotation

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/squareup/settings/LoggedInSettingsModule_ProvideCashReportEmailRecipientFactory;->preferencesProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/settings/LoggedInSettingsModule_ProvideCashReportEmailRecipientFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Landroid/content/SharedPreferences;",
            ">;)",
            "Lcom/squareup/settings/LoggedInSettingsModule_ProvideCashReportEmailRecipientFactory;"
        }
    .end annotation

    .line 32
    new-instance v0, Lcom/squareup/settings/LoggedInSettingsModule_ProvideCashReportEmailRecipientFactory;

    invoke-direct {v0, p0}, Lcom/squareup/settings/LoggedInSettingsModule_ProvideCashReportEmailRecipientFactory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static provideCashReportEmailRecipient(Landroid/content/SharedPreferences;)Lcom/squareup/settings/StringLocalSetting;
    .locals 1

    .line 36
    invoke-static {p0}, Lcom/squareup/settings/LoggedInSettingsModule;->provideCashReportEmailRecipient(Landroid/content/SharedPreferences;)Lcom/squareup/settings/StringLocalSetting;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/settings/StringLocalSetting;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/settings/StringLocalSetting;
    .locals 1

    .line 27
    iget-object v0, p0, Lcom/squareup/settings/LoggedInSettingsModule_ProvideCashReportEmailRecipientFactory;->preferencesProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    invoke-static {v0}, Lcom/squareup/settings/LoggedInSettingsModule_ProvideCashReportEmailRecipientFactory;->provideCashReportEmailRecipient(Landroid/content/SharedPreferences;)Lcom/squareup/settings/StringLocalSetting;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/settings/LoggedInSettingsModule_ProvideCashReportEmailRecipientFactory;->get()Lcom/squareup/settings/StringLocalSetting;

    move-result-object v0

    return-object v0
.end method
