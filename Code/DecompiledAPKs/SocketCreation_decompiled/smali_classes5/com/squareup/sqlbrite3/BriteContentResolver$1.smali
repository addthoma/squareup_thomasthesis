.class Lcom/squareup/sqlbrite3/BriteContentResolver$1;
.super Lcom/squareup/sqlbrite3/SqlBrite$Query;
.source "BriteContentResolver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/sqlbrite3/BriteContentResolver;->createQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Z)Lcom/squareup/sqlbrite3/QueryObservable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/sqlbrite3/BriteContentResolver;

.field final synthetic val$notifyForDescendents:Z

.field final synthetic val$projection:[Ljava/lang/String;

.field final synthetic val$selection:Ljava/lang/String;

.field final synthetic val$selectionArgs:[Ljava/lang/String;

.field final synthetic val$sortOrder:Ljava/lang/String;

.field final synthetic val$uri:Landroid/net/Uri;


# direct methods
.method constructor <init>(Lcom/squareup/sqlbrite3/BriteContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    .line 95
    iput-object p1, p0, Lcom/squareup/sqlbrite3/BriteContentResolver$1;->this$0:Lcom/squareup/sqlbrite3/BriteContentResolver;

    iput-object p2, p0, Lcom/squareup/sqlbrite3/BriteContentResolver$1;->val$uri:Landroid/net/Uri;

    iput-object p3, p0, Lcom/squareup/sqlbrite3/BriteContentResolver$1;->val$projection:[Ljava/lang/String;

    iput-object p4, p0, Lcom/squareup/sqlbrite3/BriteContentResolver$1;->val$selection:Ljava/lang/String;

    iput-object p5, p0, Lcom/squareup/sqlbrite3/BriteContentResolver$1;->val$selectionArgs:[Ljava/lang/String;

    iput-object p6, p0, Lcom/squareup/sqlbrite3/BriteContentResolver$1;->val$sortOrder:Ljava/lang/String;

    iput-boolean p7, p0, Lcom/squareup/sqlbrite3/BriteContentResolver$1;->val$notifyForDescendents:Z

    invoke-direct {p0}, Lcom/squareup/sqlbrite3/SqlBrite$Query;-><init>()V

    return-void
.end method


# virtual methods
.method public run()Landroid/database/Cursor;
    .locals 9

    .line 97
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    .line 98
    iget-object v2, p0, Lcom/squareup/sqlbrite3/BriteContentResolver$1;->this$0:Lcom/squareup/sqlbrite3/BriteContentResolver;

    iget-object v3, v2, Lcom/squareup/sqlbrite3/BriteContentResolver;->contentResolver:Landroid/content/ContentResolver;

    iget-object v4, p0, Lcom/squareup/sqlbrite3/BriteContentResolver$1;->val$uri:Landroid/net/Uri;

    iget-object v5, p0, Lcom/squareup/sqlbrite3/BriteContentResolver$1;->val$projection:[Ljava/lang/String;

    iget-object v6, p0, Lcom/squareup/sqlbrite3/BriteContentResolver$1;->val$selection:Ljava/lang/String;

    iget-object v7, p0, Lcom/squareup/sqlbrite3/BriteContentResolver$1;->val$selectionArgs:[Ljava/lang/String;

    iget-object v8, p0, Lcom/squareup/sqlbrite3/BriteContentResolver$1;->val$sortOrder:Ljava/lang/String;

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 100
    iget-object v3, p0, Lcom/squareup/sqlbrite3/BriteContentResolver$1;->this$0:Lcom/squareup/sqlbrite3/BriteContentResolver;

    iget-boolean v3, v3, Lcom/squareup/sqlbrite3/BriteContentResolver;->logging:Z

    if-eqz v3, :cond_0

    .line 101
    sget-object v3, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v4

    sub-long/2addr v4, v0

    invoke-virtual {v3, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    .line 102
    iget-object v3, p0, Lcom/squareup/sqlbrite3/BriteContentResolver$1;->this$0:Lcom/squareup/sqlbrite3/BriteContentResolver;

    const/4 v4, 0x7

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 103
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v4, v5

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/squareup/sqlbrite3/BriteContentResolver$1;->val$uri:Landroid/net/Uri;

    aput-object v1, v4, v0

    const/4 v0, 0x2

    iget-object v1, p0, Lcom/squareup/sqlbrite3/BriteContentResolver$1;->val$projection:[Ljava/lang/String;

    .line 104
    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x3

    iget-object v1, p0, Lcom/squareup/sqlbrite3/BriteContentResolver$1;->val$selection:Ljava/lang/String;

    aput-object v1, v4, v0

    const/4 v0, 0x4

    iget-object v1, p0, Lcom/squareup/sqlbrite3/BriteContentResolver$1;->val$selectionArgs:[Ljava/lang/String;

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x5

    iget-object v1, p0, Lcom/squareup/sqlbrite3/BriteContentResolver$1;->val$sortOrder:Ljava/lang/String;

    aput-object v1, v4, v0

    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/squareup/sqlbrite3/BriteContentResolver$1;->val$notifyForDescendents:Z

    .line 105
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v4, v0

    const-string v0, "QUERY (%sms)\n  uri: %s\n  projection: %s\n  selection: %s\n  selectionArgs: %s\n  sortOrder: %s\n  notifyForDescendents: %s"

    .line 102
    invoke-virtual {v3, v0, v4}, Lcom/squareup/sqlbrite3/BriteContentResolver;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    return-object v2
.end method
