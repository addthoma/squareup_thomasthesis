.class public final Lcom/squareup/sqlbrite3/BriteDatabase;
.super Ljava/lang/Object;
.source "BriteDatabase.java"

# interfaces
.implements Ljava/io/Closeable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/sqlbrite3/BriteDatabase$DatabaseQuery;,
        Lcom/squareup/sqlbrite3/BriteDatabase$SqliteTransaction;,
        Lcom/squareup/sqlbrite3/BriteDatabase$Transaction;
    }
.end annotation


# instance fields
.field private final ensureNotInTransaction:Lio/reactivex/functions/Consumer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/functions/Consumer<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final helper:Landroidx/sqlite/db/SupportSQLiteOpenHelper;

.field private final logger:Lcom/squareup/sqlbrite3/SqlBrite$Logger;

.field volatile logging:Z

.field private final queryTransformer:Lio/reactivex/ObservableTransformer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/ObservableTransformer<",
            "Lcom/squareup/sqlbrite3/SqlBrite$Query;",
            "Lcom/squareup/sqlbrite3/SqlBrite$Query;",
            ">;"
        }
    .end annotation
.end field

.field private final scheduler:Lio/reactivex/Scheduler;

.field private final transaction:Lcom/squareup/sqlbrite3/BriteDatabase$Transaction;

.field final transactions:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal<",
            "Lcom/squareup/sqlbrite3/BriteDatabase$SqliteTransaction;",
            ">;"
        }
    .end annotation
.end field

.field private final triggers:Lio/reactivex/subjects/Subject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/subjects/Subject<",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroidx/sqlite/db/SupportSQLiteOpenHelper;Lcom/squareup/sqlbrite3/SqlBrite$Logger;Lio/reactivex/Scheduler;Lio/reactivex/ObservableTransformer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/sqlite/db/SupportSQLiteOpenHelper;",
            "Lcom/squareup/sqlbrite3/SqlBrite$Logger;",
            "Lio/reactivex/Scheduler;",
            "Lio/reactivex/ObservableTransformer<",
            "Lcom/squareup/sqlbrite3/SqlBrite$Query;",
            "Lcom/squareup/sqlbrite3/SqlBrite$Query;",
            ">;)V"
        }
    .end annotation

    .line 120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    iput-object v0, p0, Lcom/squareup/sqlbrite3/BriteDatabase;->transactions:Ljava/lang/ThreadLocal;

    .line 71
    invoke-static {}, Lio/reactivex/subjects/PublishSubject;->create()Lio/reactivex/subjects/PublishSubject;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/sqlbrite3/BriteDatabase;->triggers:Lio/reactivex/subjects/Subject;

    .line 73
    new-instance v0, Lcom/squareup/sqlbrite3/BriteDatabase$1;

    invoke-direct {v0, p0}, Lcom/squareup/sqlbrite3/BriteDatabase$1;-><init>(Lcom/squareup/sqlbrite3/BriteDatabase;)V

    iput-object v0, p0, Lcom/squareup/sqlbrite3/BriteDatabase;->transaction:Lcom/squareup/sqlbrite3/BriteDatabase$Transaction;

    .line 106
    new-instance v0, Lcom/squareup/sqlbrite3/BriteDatabase$2;

    invoke-direct {v0, p0}, Lcom/squareup/sqlbrite3/BriteDatabase$2;-><init>(Lcom/squareup/sqlbrite3/BriteDatabase;)V

    iput-object v0, p0, Lcom/squareup/sqlbrite3/BriteDatabase;->ensureNotInTransaction:Lio/reactivex/functions/Consumer;

    .line 121
    iput-object p1, p0, Lcom/squareup/sqlbrite3/BriteDatabase;->helper:Landroidx/sqlite/db/SupportSQLiteOpenHelper;

    .line 122
    iput-object p2, p0, Lcom/squareup/sqlbrite3/BriteDatabase;->logger:Lcom/squareup/sqlbrite3/SqlBrite$Logger;

    .line 123
    iput-object p3, p0, Lcom/squareup/sqlbrite3/BriteDatabase;->scheduler:Lio/reactivex/Scheduler;

    .line 124
    iput-object p4, p0, Lcom/squareup/sqlbrite3/BriteDatabase;->queryTransformer:Lio/reactivex/ObservableTransformer;

    return-void
.end method

.method private static conflictString(I)Ljava/lang/String;
    .locals 2

    if-eqz p0, :cond_5

    const/4 v0, 0x1

    if-eq p0, v0, :cond_4

    const/4 v0, 0x2

    if-eq p0, v0, :cond_3

    const/4 v0, 0x3

    if-eq p0, v0, :cond_2

    const/4 v0, 0x4

    if-eq p0, v0, :cond_1

    const/4 v0, 0x5

    if-eq p0, v0, :cond_0

    .line 746
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "unknown ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 p0, 0x29

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_0
    const-string p0, "replace"

    return-object p0

    :cond_1
    const-string p0, "ignore"

    return-object p0

    :cond_2
    const-string p0, "fail"

    return-object p0

    :cond_3
    const-string p0, "abort"

    return-object p0

    :cond_4
    const-string p0, "rollback"

    return-object p0

    :cond_5
    const-string p0, "none"

    return-object p0
.end method

.method private createQuery(Lcom/squareup/sqlbrite3/BriteDatabase$DatabaseQuery;)Lcom/squareup/sqlbrite3/QueryObservable;
    .locals 1

    .line 378
    iget-object v0, p0, Lcom/squareup/sqlbrite3/BriteDatabase;->transactions:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 383
    iget-object v0, p0, Lcom/squareup/sqlbrite3/BriteDatabase;->triggers:Lio/reactivex/subjects/Subject;

    .line 384
    invoke-virtual {v0, p1}, Lio/reactivex/subjects/Subject;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    .line 385
    invoke-virtual {v0, p1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 386
    invoke-virtual {v0, p1}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/sqlbrite3/BriteDatabase;->scheduler:Lio/reactivex/Scheduler;

    .line 387
    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/sqlbrite3/BriteDatabase;->queryTransformer:Lio/reactivex/ObservableTransformer;

    .line 388
    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/sqlbrite3/BriteDatabase;->ensureNotInTransaction:Lio/reactivex/functions/Consumer;

    .line 389
    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->doOnSubscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object p1

    sget-object v0, Lcom/squareup/sqlbrite3/QueryObservable;->QUERY_OBSERVABLE:Lio/reactivex/functions/Function;

    .line 390
    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->to(Lio/reactivex/functions/Function;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/sqlbrite3/QueryObservable;

    return-object p1

    .line 379
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Cannot create observable query in transaction. Use query() for a query inside a transaction."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method static indentSql(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    const-string v0, "\n"

    const-string v1, "\n       "

    .line 723
    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public close()V
    .locals 1

    .line 291
    iget-object v0, p0, Lcom/squareup/sqlbrite3/BriteDatabase;->helper:Landroidx/sqlite/db/SupportSQLiteOpenHelper;

    invoke-interface {v0}, Landroidx/sqlite/db/SupportSQLiteOpenHelper;->close()V

    return-void
.end method

.method public createQuery(Ljava/lang/Iterable;Landroidx/sqlite/db/SupportSQLiteQuery;)Lcom/squareup/sqlbrite3/QueryObservable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable<",
            "Ljava/lang/String;",
            ">;",
            "Landroidx/sqlite/db/SupportSQLiteQuery;",
            ")",
            "Lcom/squareup/sqlbrite3/QueryObservable;"
        }
    .end annotation

    .line 373
    new-instance v0, Lcom/squareup/sqlbrite3/BriteDatabase$DatabaseQuery;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/sqlbrite3/BriteDatabase$DatabaseQuery;-><init>(Lcom/squareup/sqlbrite3/BriteDatabase;Ljava/lang/Iterable;Landroidx/sqlite/db/SupportSQLiteQuery;)V

    invoke-direct {p0, v0}, Lcom/squareup/sqlbrite3/BriteDatabase;->createQuery(Lcom/squareup/sqlbrite3/BriteDatabase$DatabaseQuery;)Lcom/squareup/sqlbrite3/QueryObservable;

    move-result-object p1

    return-object p1
.end method

.method public varargs createQuery(Ljava/lang/Iterable;Ljava/lang/String;[Ljava/lang/Object;)Lcom/squareup/sqlbrite3/QueryObservable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/Object;",
            ")",
            "Lcom/squareup/sqlbrite3/QueryObservable;"
        }
    .end annotation

    .line 332
    new-instance v0, Lcom/squareup/sqlbrite3/BriteDatabase$DatabaseQuery;

    new-instance v1, Landroidx/sqlite/db/SimpleSQLiteQuery;

    invoke-direct {v1, p2, p3}, Landroidx/sqlite/db/SimpleSQLiteQuery;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-direct {v0, p0, p1, v1}, Lcom/squareup/sqlbrite3/BriteDatabase$DatabaseQuery;-><init>(Lcom/squareup/sqlbrite3/BriteDatabase;Ljava/lang/Iterable;Landroidx/sqlite/db/SupportSQLiteQuery;)V

    invoke-direct {p0, v0}, Lcom/squareup/sqlbrite3/BriteDatabase;->createQuery(Lcom/squareup/sqlbrite3/BriteDatabase$DatabaseQuery;)Lcom/squareup/sqlbrite3/QueryObservable;

    move-result-object p1

    return-object p1
.end method

.method public createQuery(Ljava/lang/String;Landroidx/sqlite/db/SupportSQLiteQuery;)Lcom/squareup/sqlbrite3/QueryObservable;
    .locals 1

    .line 361
    new-instance v0, Lcom/squareup/sqlbrite3/BriteDatabase$DatabaseQuery;

    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/sqlbrite3/BriteDatabase$DatabaseQuery;-><init>(Lcom/squareup/sqlbrite3/BriteDatabase;Ljava/lang/Iterable;Landroidx/sqlite/db/SupportSQLiteQuery;)V

    invoke-direct {p0, v0}, Lcom/squareup/sqlbrite3/BriteDatabase;->createQuery(Lcom/squareup/sqlbrite3/BriteDatabase$DatabaseQuery;)Lcom/squareup/sqlbrite3/QueryObservable;

    move-result-object p1

    return-object p1
.end method

.method public varargs createQuery(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)Lcom/squareup/sqlbrite3/QueryObservable;
    .locals 2

    .line 320
    new-instance v0, Lcom/squareup/sqlbrite3/BriteDatabase$DatabaseQuery;

    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    new-instance v1, Landroidx/sqlite/db/SimpleSQLiteQuery;

    invoke-direct {v1, p2, p3}, Landroidx/sqlite/db/SimpleSQLiteQuery;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-direct {v0, p0, p1, v1}, Lcom/squareup/sqlbrite3/BriteDatabase$DatabaseQuery;-><init>(Lcom/squareup/sqlbrite3/BriteDatabase;Ljava/lang/Iterable;Landroidx/sqlite/db/SupportSQLiteQuery;)V

    invoke-direct {p0, v0}, Lcom/squareup/sqlbrite3/BriteDatabase;->createQuery(Lcom/squareup/sqlbrite3/BriteDatabase$DatabaseQuery;)Lcom/squareup/sqlbrite3/QueryObservable;

    move-result-object p1

    return-object p1
.end method

.method public varargs delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 6

    .line 457
    invoke-virtual {p0}, Lcom/squareup/sqlbrite3/BriteDatabase;->getWritableDatabase()Landroidx/sqlite/db/SupportSQLiteDatabase;

    move-result-object v0

    .line 459
    iget-boolean v1, p0, Lcom/squareup/sqlbrite3/BriteDatabase;->logging:Z

    const/4 v2, 0x2

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-eqz v1, :cond_0

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v3

    aput-object p2, v1, v4

    .line 461
    invoke-static {p3}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v2

    const-string v5, "DELETE\n  table: %s\n  whereClause: %s\n  whereArgs: %s"

    .line 460
    invoke-virtual {p0, v5, v1}, Lcom/squareup/sqlbrite3/BriteDatabase;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 463
    :cond_0
    invoke-interface {v0, p1, p2, p3}, Landroidx/sqlite/db/SupportSQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    move-result p2

    .line 465
    iget-boolean p3, p0, Lcom/squareup/sqlbrite3/BriteDatabase;->logging:Z

    if-eqz p3, :cond_2

    new-array p3, v2, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, p3, v3

    if-eq p2, v4, :cond_1

    const-string v0, "rows"

    goto :goto_0

    :cond_1
    const-string v0, "row"

    :goto_0
    aput-object v0, p3, v4

    const-string v0, "DELETE affected %s %s"

    invoke-virtual {p0, v0, p3}, Lcom/squareup/sqlbrite3/BriteDatabase;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_2
    if-lez p2, :cond_3

    .line 469
    invoke-static {p1}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/sqlbrite3/BriteDatabase;->sendTableTrigger(Ljava/util/Set;)V

    :cond_3
    return p2
.end method

.method public execute(Ljava/lang/String;)V
    .locals 3

    .line 512
    iget-boolean v0, p0, Lcom/squareup/sqlbrite3/BriteDatabase;->logging:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {p1}, Lcom/squareup/sqlbrite3/BriteDatabase;->indentSql(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const-string v1, "EXECUTE\n  sql: %s"

    invoke-virtual {p0, v1, v0}, Lcom/squareup/sqlbrite3/BriteDatabase;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 514
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/sqlbrite3/BriteDatabase;->getWritableDatabase()Landroidx/sqlite/db/SupportSQLiteDatabase;

    move-result-object v0

    invoke-interface {v0, p1}, Landroidx/sqlite/db/SupportSQLiteDatabase;->execSQL(Ljava/lang/String;)V

    return-void
.end method

.method public varargs execute(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 3

    .line 528
    iget-boolean v0, p0, Lcom/squareup/sqlbrite3/BriteDatabase;->logging:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {p1}, Lcom/squareup/sqlbrite3/BriteDatabase;->indentSql(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-static {p2}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const-string v1, "EXECUTE\n  sql: %s\n  args: %s"

    invoke-virtual {p0, v1, v0}, Lcom/squareup/sqlbrite3/BriteDatabase;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 530
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/sqlbrite3/BriteDatabase;->getWritableDatabase()Landroidx/sqlite/db/SupportSQLiteDatabase;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Landroidx/sqlite/db/SupportSQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public executeAndTrigger(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 544
    invoke-static {p1}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object p1

    invoke-virtual {p0, p1, p2}, Lcom/squareup/sqlbrite3/BriteDatabase;->executeAndTrigger(Ljava/util/Set;Ljava/lang/String;)V

    return-void
.end method

.method public varargs executeAndTrigger(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 0

    .line 570
    invoke-static {p1}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object p1

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/sqlbrite3/BriteDatabase;->executeAndTrigger(Ljava/util/Set;Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public executeAndTrigger(Ljava/util/Set;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 554
    invoke-virtual {p0, p2}, Lcom/squareup/sqlbrite3/BriteDatabase;->execute(Ljava/lang/String;)V

    .line 556
    invoke-virtual {p0, p1}, Lcom/squareup/sqlbrite3/BriteDatabase;->sendTableTrigger(Ljava/util/Set;)V

    return-void
.end method

.method public varargs executeAndTrigger(Ljava/util/Set;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .line 580
    invoke-virtual {p0, p2, p3}, Lcom/squareup/sqlbrite3/BriteDatabase;->execute(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 582
    invoke-virtual {p0, p1}, Lcom/squareup/sqlbrite3/BriteDatabase;->sendTableTrigger(Ljava/util/Set;)V

    return-void
.end method

.method public executeInsert(Ljava/lang/String;Landroidx/sqlite/db/SupportSQLiteStatement;)J
    .locals 0

    .line 629
    invoke-static {p1}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object p1

    invoke-virtual {p0, p1, p2}, Lcom/squareup/sqlbrite3/BriteDatabase;->executeInsert(Ljava/util/Set;Landroidx/sqlite/db/SupportSQLiteStatement;)J

    move-result-wide p1

    return-wide p1
.end method

.method public executeInsert(Ljava/util/Set;Landroidx/sqlite/db/SupportSQLiteStatement;)J
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;",
            "Landroidx/sqlite/db/SupportSQLiteStatement;",
            ")J"
        }
    .end annotation

    .line 640
    iget-boolean v0, p0, Lcom/squareup/sqlbrite3/BriteDatabase;->logging:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    const-string v1, "EXECUTE\n %s"

    invoke-virtual {p0, v1, v0}, Lcom/squareup/sqlbrite3/BriteDatabase;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 642
    :cond_0
    invoke-interface {p2}, Landroidx/sqlite/db/SupportSQLiteStatement;->executeInsert()J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long p2, v0, v2

    if-eqz p2, :cond_1

    .line 645
    invoke-virtual {p0, p1}, Lcom/squareup/sqlbrite3/BriteDatabase;->sendTableTrigger(Ljava/util/Set;)V

    :cond_1
    return-wide v0
.end method

.method public executeUpdateDelete(Ljava/lang/String;Landroidx/sqlite/db/SupportSQLiteStatement;)I
    .locals 0

    .line 596
    invoke-static {p1}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object p1

    invoke-virtual {p0, p1, p2}, Lcom/squareup/sqlbrite3/BriteDatabase;->executeUpdateDelete(Ljava/util/Set;Landroidx/sqlite/db/SupportSQLiteStatement;)I

    move-result p1

    return p1
.end method

.method public executeUpdateDelete(Ljava/util/Set;Landroidx/sqlite/db/SupportSQLiteStatement;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;",
            "Landroidx/sqlite/db/SupportSQLiteStatement;",
            ")I"
        }
    .end annotation

    .line 607
    iget-boolean v0, p0, Lcom/squareup/sqlbrite3/BriteDatabase;->logging:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    const-string v1, "EXECUTE\n %s"

    invoke-virtual {p0, v1, v0}, Lcom/squareup/sqlbrite3/BriteDatabase;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 609
    :cond_0
    invoke-interface {p2}, Landroidx/sqlite/db/SupportSQLiteStatement;->executeUpdateDelete()I

    move-result p2

    if-lez p2, :cond_1

    .line 612
    invoke-virtual {p0, p1}, Lcom/squareup/sqlbrite3/BriteDatabase;->sendTableTrigger(Ljava/util/Set;)V

    :cond_1
    return p2
.end method

.method public getReadableDatabase()Landroidx/sqlite/db/SupportSQLiteDatabase;
    .locals 1

    .line 154
    iget-object v0, p0, Lcom/squareup/sqlbrite3/BriteDatabase;->helper:Landroidx/sqlite/db/SupportSQLiteOpenHelper;

    invoke-interface {v0}, Landroidx/sqlite/db/SupportSQLiteOpenHelper;->getReadableDatabase()Landroidx/sqlite/db/SupportSQLiteDatabase;

    move-result-object v0

    return-object v0
.end method

.method public getWritableDatabase()Landroidx/sqlite/db/SupportSQLiteDatabase;
    .locals 1

    .line 178
    iget-object v0, p0, Lcom/squareup/sqlbrite3/BriteDatabase;->helper:Landroidx/sqlite/db/SupportSQLiteOpenHelper;

    invoke-interface {v0}, Landroidx/sqlite/db/SupportSQLiteOpenHelper;->getWritableDatabase()Landroidx/sqlite/db/SupportSQLiteDatabase;

    move-result-object v0

    return-object v0
.end method

.method public insert(Ljava/lang/String;ILandroid/content/ContentValues;)J
    .locals 6

    .line 431
    invoke-virtual {p0}, Lcom/squareup/sqlbrite3/BriteDatabase;->getWritableDatabase()Landroidx/sqlite/db/SupportSQLiteDatabase;

    move-result-object v0

    .line 433
    iget-boolean v1, p0, Lcom/squareup/sqlbrite3/BriteDatabase;->logging:Z

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v3

    aput-object p3, v1, v2

    const/4 v4, 0x2

    .line 435
    invoke-static {p2}, Lcom/squareup/sqlbrite3/BriteDatabase;->conflictString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v4

    const-string v4, "INSERT\n  table: %s\n  values: %s\n  conflictAlgorithm: %s"

    .line 434
    invoke-virtual {p0, v4, v1}, Lcom/squareup/sqlbrite3/BriteDatabase;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 437
    :cond_0
    invoke-interface {v0, p1, p2, p3}, Landroidx/sqlite/db/SupportSQLiteDatabase;->insert(Ljava/lang/String;ILandroid/content/ContentValues;)J

    move-result-wide p2

    .line 439
    iget-boolean v0, p0, Lcom/squareup/sqlbrite3/BriteDatabase;->logging:Z

    if-eqz v0, :cond_1

    new-array v0, v2, [Ljava/lang/Object;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v0, v3

    const-string v1, "INSERT id: %s"

    invoke-virtual {p0, v1, v0}, Lcom/squareup/sqlbrite3/BriteDatabase;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_1
    const-wide/16 v0, -0x1

    cmp-long v2, p2, v0

    if-eqz v2, :cond_2

    .line 443
    invoke-static {p1}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/sqlbrite3/BriteDatabase;->sendTableTrigger(Ljava/util/Set;)V

    :cond_2
    return-wide p2
.end method

.method varargs log(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1

    .line 727
    array-length v0, p2

    if-lez v0, :cond_0

    invoke-static {p1, p2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 728
    :cond_0
    iget-object p2, p0, Lcom/squareup/sqlbrite3/BriteDatabase;->logger:Lcom/squareup/sqlbrite3/SqlBrite$Logger;

    invoke-interface {p2, p1}, Lcom/squareup/sqlbrite3/SqlBrite$Logger;->log(Ljava/lang/String;)V

    return-void
.end method

.method public newNonExclusiveTransaction()Lcom/squareup/sqlbrite3/BriteDatabase$Transaction;
    .locals 3

    .line 277
    new-instance v0, Lcom/squareup/sqlbrite3/BriteDatabase$SqliteTransaction;

    iget-object v1, p0, Lcom/squareup/sqlbrite3/BriteDatabase;->transactions:Ljava/lang/ThreadLocal;

    invoke-virtual {v1}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/sqlbrite3/BriteDatabase$SqliteTransaction;

    invoke-direct {v0, v1}, Lcom/squareup/sqlbrite3/BriteDatabase$SqliteTransaction;-><init>(Lcom/squareup/sqlbrite3/BriteDatabase$SqliteTransaction;)V

    .line 278
    iget-object v1, p0, Lcom/squareup/sqlbrite3/BriteDatabase;->transactions:Ljava/lang/ThreadLocal;

    invoke-virtual {v1, v0}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 279
    iget-boolean v1, p0, Lcom/squareup/sqlbrite3/BriteDatabase;->logging:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    const-string v2, "TXN BEGIN %s"

    invoke-virtual {p0, v2, v1}, Lcom/squareup/sqlbrite3/BriteDatabase;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 280
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/sqlbrite3/BriteDatabase;->getWritableDatabase()Landroidx/sqlite/db/SupportSQLiteDatabase;

    move-result-object v1

    invoke-interface {v1, v0}, Landroidx/sqlite/db/SupportSQLiteDatabase;->beginTransactionWithListenerNonExclusive(Landroid/database/sqlite/SQLiteTransactionListener;)V

    .line 282
    iget-object v0, p0, Lcom/squareup/sqlbrite3/BriteDatabase;->transaction:Lcom/squareup/sqlbrite3/BriteDatabase$Transaction;

    return-object v0
.end method

.method public newTransaction()Lcom/squareup/sqlbrite3/BriteDatabase$Transaction;
    .locals 3

    .line 230
    new-instance v0, Lcom/squareup/sqlbrite3/BriteDatabase$SqliteTransaction;

    iget-object v1, p0, Lcom/squareup/sqlbrite3/BriteDatabase;->transactions:Ljava/lang/ThreadLocal;

    invoke-virtual {v1}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/sqlbrite3/BriteDatabase$SqliteTransaction;

    invoke-direct {v0, v1}, Lcom/squareup/sqlbrite3/BriteDatabase$SqliteTransaction;-><init>(Lcom/squareup/sqlbrite3/BriteDatabase$SqliteTransaction;)V

    .line 231
    iget-object v1, p0, Lcom/squareup/sqlbrite3/BriteDatabase;->transactions:Ljava/lang/ThreadLocal;

    invoke-virtual {v1, v0}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 232
    iget-boolean v1, p0, Lcom/squareup/sqlbrite3/BriteDatabase;->logging:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    const-string v2, "TXN BEGIN %s"

    invoke-virtual {p0, v2, v1}, Lcom/squareup/sqlbrite3/BriteDatabase;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 233
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/sqlbrite3/BriteDatabase;->getWritableDatabase()Landroidx/sqlite/db/SupportSQLiteDatabase;

    move-result-object v1

    invoke-interface {v1, v0}, Landroidx/sqlite/db/SupportSQLiteDatabase;->beginTransactionWithListener(Landroid/database/sqlite/SQLiteTransactionListener;)V

    .line 235
    iget-object v0, p0, Lcom/squareup/sqlbrite3/BriteDatabase;->transaction:Lcom/squareup/sqlbrite3/BriteDatabase$Transaction;

    return-object v0
.end method

.method public query(Landroidx/sqlite/db/SupportSQLiteQuery;)Landroid/database/Cursor;
    .locals 3

    .line 415
    invoke-virtual {p0}, Lcom/squareup/sqlbrite3/BriteDatabase;->getReadableDatabase()Landroidx/sqlite/db/SupportSQLiteDatabase;

    move-result-object v0

    invoke-interface {v0, p1}, Landroidx/sqlite/db/SupportSQLiteDatabase;->query(Landroidx/sqlite/db/SupportSQLiteQuery;)Landroid/database/Cursor;

    move-result-object v0

    .line 416
    iget-boolean v1, p0, Lcom/squareup/sqlbrite3/BriteDatabase;->logging:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 417
    invoke-interface {p1}, Landroidx/sqlite/db/SupportSQLiteQuery;->getSql()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/squareup/sqlbrite3/BriteDatabase;->indentSql(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v1, v2

    const-string p1, "QUERY\n  sql: %s"

    invoke-virtual {p0, p1, v1}, Lcom/squareup/sqlbrite3/BriteDatabase;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    return-object v0
.end method

.method public varargs query(Ljava/lang/String;[Ljava/lang/Object;)Landroid/database/Cursor;
    .locals 3

    .line 400
    invoke-virtual {p0}, Lcom/squareup/sqlbrite3/BriteDatabase;->getReadableDatabase()Landroidx/sqlite/db/SupportSQLiteDatabase;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Landroidx/sqlite/db/SupportSQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/Object;)Landroid/database/Cursor;

    move-result-object v0

    .line 401
    iget-boolean v1, p0, Lcom/squareup/sqlbrite3/BriteDatabase;->logging:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 402
    invoke-static {p1}, Lcom/squareup/sqlbrite3/BriteDatabase;->indentSql(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    aput-object p1, v1, v2

    const/4 p1, 0x1

    invoke-static {p2}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    aput-object p2, v1, p1

    const-string p1, "QUERY\n  sql: %s\n  args: %s"

    invoke-virtual {p0, p1, v1}, Lcom/squareup/sqlbrite3/BriteDatabase;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    return-object v0
.end method

.method sendTableTrigger(Ljava/util/Set;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 182
    iget-object v0, p0, Lcom/squareup/sqlbrite3/BriteDatabase;->transactions:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/sqlbrite3/BriteDatabase$SqliteTransaction;

    if-eqz v0, :cond_0

    .line 184
    invoke-virtual {v0, p1}, Lcom/squareup/sqlbrite3/BriteDatabase$SqliteTransaction;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 186
    :cond_0
    iget-boolean v0, p0, Lcom/squareup/sqlbrite3/BriteDatabase;->logging:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const-string v1, "TRIGGER %s"

    invoke-virtual {p0, v1, v0}, Lcom/squareup/sqlbrite3/BriteDatabase;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 187
    :cond_1
    iget-object v0, p0, Lcom/squareup/sqlbrite3/BriteDatabase;->triggers:Lio/reactivex/subjects/Subject;

    invoke-virtual {v0, p1}, Lio/reactivex/subjects/Subject;->onNext(Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method public setLoggingEnabled(Z)V
    .locals 0

    .line 131
    iput-boolean p1, p0, Lcom/squareup/sqlbrite3/BriteDatabase;->logging:Z

    return-void
.end method

.method public varargs update(Ljava/lang/String;ILandroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 9

    .line 483
    invoke-virtual {p0}, Lcom/squareup/sqlbrite3/BriteDatabase;->getWritableDatabase()Landroidx/sqlite/db/SupportSQLiteDatabase;

    move-result-object v0

    .line 485
    iget-boolean v1, p0, Lcom/squareup/sqlbrite3/BriteDatabase;->logging:Z

    const/4 v6, 0x2

    const/4 v7, 0x0

    const/4 v8, 0x1

    if-eqz v1, :cond_0

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v7

    aput-object p3, v1, v8

    aput-object p4, v1, v6

    const/4 v2, 0x3

    .line 487
    invoke-static {p5}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    .line 488
    invoke-static {p2}, Lcom/squareup/sqlbrite3/BriteDatabase;->conflictString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const-string v2, "UPDATE\n  table: %s\n  values: %s\n  whereClause: %s\n  whereArgs: %s\n  conflictAlgorithm: %s"

    .line 486
    invoke-virtual {p0, v2, v1}, Lcom/squareup/sqlbrite3/BriteDatabase;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 490
    invoke-interface/range {v0 .. v5}, Landroidx/sqlite/db/SupportSQLiteDatabase;->update(Ljava/lang/String;ILandroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/Object;)I

    move-result p2

    .line 492
    iget-boolean p3, p0, Lcom/squareup/sqlbrite3/BriteDatabase;->logging:Z

    if-eqz p3, :cond_2

    new-array p3, v6, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p4

    aput-object p4, p3, v7

    if-eq p2, v8, :cond_1

    const-string p4, "rows"

    goto :goto_0

    :cond_1
    const-string p4, "row"

    :goto_0
    aput-object p4, p3, v8

    const-string p4, "UPDATE affected %s %s"

    invoke-virtual {p0, p4, p3}, Lcom/squareup/sqlbrite3/BriteDatabase;->log(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_2
    if-lez p2, :cond_3

    .line 496
    invoke-static {p1}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/squareup/sqlbrite3/BriteDatabase;->sendTableTrigger(Ljava/util/Set;)V

    :cond_3
    return p2
.end method
