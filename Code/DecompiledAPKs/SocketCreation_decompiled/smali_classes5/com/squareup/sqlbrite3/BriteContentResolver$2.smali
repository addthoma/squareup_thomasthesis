.class Lcom/squareup/sqlbrite3/BriteContentResolver$2;
.super Ljava/lang/Object;
.source "BriteContentResolver.java"

# interfaces
.implements Lio/reactivex/ObservableOnSubscribe;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/sqlbrite3/BriteContentResolver;->createQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Z)Lcom/squareup/sqlbrite3/QueryObservable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lio/reactivex/ObservableOnSubscribe<",
        "Lcom/squareup/sqlbrite3/SqlBrite$Query;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/sqlbrite3/BriteContentResolver;

.field final synthetic val$notifyForDescendents:Z

.field final synthetic val$query:Lcom/squareup/sqlbrite3/SqlBrite$Query;

.field final synthetic val$uri:Landroid/net/Uri;


# direct methods
.method constructor <init>(Lcom/squareup/sqlbrite3/BriteContentResolver;Lcom/squareup/sqlbrite3/SqlBrite$Query;Landroid/net/Uri;Z)V
    .locals 0

    .line 111
    iput-object p1, p0, Lcom/squareup/sqlbrite3/BriteContentResolver$2;->this$0:Lcom/squareup/sqlbrite3/BriteContentResolver;

    iput-object p2, p0, Lcom/squareup/sqlbrite3/BriteContentResolver$2;->val$query:Lcom/squareup/sqlbrite3/SqlBrite$Query;

    iput-object p3, p0, Lcom/squareup/sqlbrite3/BriteContentResolver$2;->val$uri:Landroid/net/Uri;

    iput-boolean p4, p0, Lcom/squareup/sqlbrite3/BriteContentResolver$2;->val$notifyForDescendents:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public subscribe(Lio/reactivex/ObservableEmitter;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/ObservableEmitter<",
            "Lcom/squareup/sqlbrite3/SqlBrite$Query;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 113
    new-instance v0, Lcom/squareup/sqlbrite3/BriteContentResolver$2$1;

    iget-object v1, p0, Lcom/squareup/sqlbrite3/BriteContentResolver$2;->this$0:Lcom/squareup/sqlbrite3/BriteContentResolver;

    iget-object v1, v1, Lcom/squareup/sqlbrite3/BriteContentResolver;->contentObserverHandler:Landroid/os/Handler;

    invoke-direct {v0, p0, v1, p1}, Lcom/squareup/sqlbrite3/BriteContentResolver$2$1;-><init>(Lcom/squareup/sqlbrite3/BriteContentResolver$2;Landroid/os/Handler;Lio/reactivex/ObservableEmitter;)V

    .line 120
    iget-object v1, p0, Lcom/squareup/sqlbrite3/BriteContentResolver$2;->this$0:Lcom/squareup/sqlbrite3/BriteContentResolver;

    iget-object v1, v1, Lcom/squareup/sqlbrite3/BriteContentResolver;->contentResolver:Landroid/content/ContentResolver;

    iget-object v2, p0, Lcom/squareup/sqlbrite3/BriteContentResolver$2;->val$uri:Landroid/net/Uri;

    iget-boolean v3, p0, Lcom/squareup/sqlbrite3/BriteContentResolver$2;->val$notifyForDescendents:Z

    invoke-virtual {v1, v2, v3, v0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 121
    new-instance v1, Lcom/squareup/sqlbrite3/BriteContentResolver$2$2;

    invoke-direct {v1, p0, v0}, Lcom/squareup/sqlbrite3/BriteContentResolver$2$2;-><init>(Lcom/squareup/sqlbrite3/BriteContentResolver$2;Landroid/database/ContentObserver;)V

    invoke-interface {p1, v1}, Lio/reactivex/ObservableEmitter;->setCancellable(Lio/reactivex/functions/Cancellable;)V

    .line 127
    invoke-interface {p1}, Lio/reactivex/ObservableEmitter;->isDisposed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 128
    iget-object v0, p0, Lcom/squareup/sqlbrite3/BriteContentResolver$2;->val$query:Lcom/squareup/sqlbrite3/SqlBrite$Query;

    invoke-interface {p1, v0}, Lio/reactivex/ObservableEmitter;->onNext(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method
