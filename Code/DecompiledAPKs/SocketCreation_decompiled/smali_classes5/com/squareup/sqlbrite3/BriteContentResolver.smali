.class public final Lcom/squareup/sqlbrite3/BriteContentResolver;
.super Ljava/lang/Object;
.source "BriteContentResolver.java"


# instance fields
.field final contentObserverHandler:Landroid/os/Handler;

.field final contentResolver:Landroid/content/ContentResolver;

.field private final logger:Lcom/squareup/sqlbrite3/SqlBrite$Logger;

.field volatile logging:Z

.field private final queryTransformer:Lio/reactivex/ObservableTransformer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/ObservableTransformer<",
            "Lcom/squareup/sqlbrite3/SqlBrite$Query;",
            "Lcom/squareup/sqlbrite3/SqlBrite$Query;",
            ">;"
        }
    .end annotation
.end field

.field private final scheduler:Lio/reactivex/Scheduler;


# direct methods
.method constructor <init>(Landroid/content/ContentResolver;Lcom/squareup/sqlbrite3/SqlBrite$Logger;Lio/reactivex/Scheduler;Lio/reactivex/ObservableTransformer;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            "Lcom/squareup/sqlbrite3/SqlBrite$Logger;",
            "Lio/reactivex/Scheduler;",
            "Lio/reactivex/ObservableTransformer<",
            "Lcom/squareup/sqlbrite3/SqlBrite$Query;",
            "Lcom/squareup/sqlbrite3/SqlBrite$Query;",
            ">;)V"
        }
    .end annotation

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/squareup/sqlbrite3/BriteContentResolver;->contentObserverHandler:Landroid/os/Handler;

    .line 57
    iput-object p1, p0, Lcom/squareup/sqlbrite3/BriteContentResolver;->contentResolver:Landroid/content/ContentResolver;

    .line 58
    iput-object p2, p0, Lcom/squareup/sqlbrite3/BriteContentResolver;->logger:Lcom/squareup/sqlbrite3/SqlBrite$Logger;

    .line 59
    iput-object p3, p0, Lcom/squareup/sqlbrite3/BriteContentResolver;->scheduler:Lio/reactivex/Scheduler;

    .line 60
    iput-object p4, p0, Lcom/squareup/sqlbrite3/BriteContentResolver;->queryTransformer:Lio/reactivex/ObservableTransformer;

    return-void
.end method


# virtual methods
.method public createQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Z)Lcom/squareup/sqlbrite3/QueryObservable;
    .locals 9

    .line 95
    new-instance v8, Lcom/squareup/sqlbrite3/BriteContentResolver$1;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/sqlbrite3/BriteContentResolver$1;-><init>(Lcom/squareup/sqlbrite3/BriteContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Z)V

    .line 111
    new-instance p2, Lcom/squareup/sqlbrite3/BriteContentResolver$2;

    invoke-direct {p2, p0, v8, p1, p6}, Lcom/squareup/sqlbrite3/BriteContentResolver$2;-><init>(Lcom/squareup/sqlbrite3/BriteContentResolver;Lcom/squareup/sqlbrite3/SqlBrite$Query;Landroid/net/Uri;Z)V

    invoke-static {p2}, Lio/reactivex/Observable;->create(Lio/reactivex/ObservableOnSubscribe;)Lio/reactivex/Observable;

    move-result-object p1

    .line 132
    iget-object p2, p0, Lcom/squareup/sqlbrite3/BriteContentResolver;->scheduler:Lio/reactivex/Scheduler;

    .line 133
    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object p1

    iget-object p2, p0, Lcom/squareup/sqlbrite3/BriteContentResolver;->queryTransformer:Lio/reactivex/ObservableTransformer;

    .line 134
    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->compose(Lio/reactivex/ObservableTransformer;)Lio/reactivex/Observable;

    move-result-object p1

    sget-object p2, Lcom/squareup/sqlbrite3/QueryObservable;->QUERY_OBSERVABLE:Lio/reactivex/functions/Function;

    .line 135
    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->to(Lio/reactivex/functions/Function;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/sqlbrite3/QueryObservable;

    return-object p1
.end method

.method varargs log(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1

    .line 139
    array-length v0, p2

    if-lez v0, :cond_0

    invoke-static {p1, p2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 140
    :cond_0
    iget-object p2, p0, Lcom/squareup/sqlbrite3/BriteContentResolver;->logger:Lcom/squareup/sqlbrite3/SqlBrite$Logger;

    invoke-interface {p2, p1}, Lcom/squareup/sqlbrite3/SqlBrite$Logger;->log(Ljava/lang/String;)V

    return-void
.end method

.method public setLoggingEnabled(Z)V
    .locals 0

    .line 65
    iput-boolean p1, p0, Lcom/squareup/sqlbrite3/BriteContentResolver;->logging:Z

    return-void
.end method
