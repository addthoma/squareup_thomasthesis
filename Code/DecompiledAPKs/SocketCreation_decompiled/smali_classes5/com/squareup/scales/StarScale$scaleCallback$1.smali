.class public final Lcom/squareup/scales/StarScale$scaleCallback$1;
.super Lcom/starmicronics/starmgsio/ScaleCallback;
.source "StarScale.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/scales/StarScale;-><init>(Landroid/app/Application;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000%\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0005\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u001a\u0010\u0002\u001a\u00020\u00032\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\u0016J\u001a\u0010\u0008\u001a\u00020\u00032\u0008\u0010\t\u001a\u0004\u0018\u00010\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\u0016J\u001a\u0010\n\u001a\u00020\u00032\u0008\u0010\u000b\u001a\u0004\u0018\u00010\u00052\u0006\u0010\u000c\u001a\u00020\rH\u0016\u00a8\u0006\u000e"
    }
    d2 = {
        "com/squareup/scales/StarScale$scaleCallback$1",
        "Lcom/starmicronics/starmgsio/ScaleCallback;",
        "onConnect",
        "",
        "scaleToConnect",
        "Lcom/starmicronics/starmgsio/Scale;",
        "status",
        "",
        "onDisconnect",
        "scaleToDisconnect",
        "onReadScaleData",
        "scaleToRead",
        "scaleData",
        "Lcom/starmicronics/starmgsio/ScaleData;",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/scales/StarScale;


# direct methods
.method constructor <init>(Lcom/squareup/scales/StarScale;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 61
    iput-object p1, p0, Lcom/squareup/scales/StarScale$scaleCallback$1;->this$0:Lcom/squareup/scales/StarScale;

    invoke-direct {p0}, Lcom/starmicronics/starmgsio/ScaleCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onConnect(Lcom/starmicronics/starmgsio/Scale;I)V
    .locals 1

    const/4 p1, 0x0

    if-eqz p2, :cond_3

    const/16 v0, 0xc8

    if-eq p2, v0, :cond_2

    const/16 v0, 0x64

    if-eq p2, v0, :cond_1

    const/16 v0, 0x65

    if-eq p2, v0, :cond_0

    packed-switch p2, :pswitch_data_0

    new-array p2, p1, [Ljava/lang/Object;

    const-string v0, "Failed to connect. (Unknown error)"

    .line 80
    invoke-static {v0, p2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_0
    new-array p2, p1, [Ljava/lang/Object;

    const-string v0, "Failed to connect. (Not granted permission)"

    .line 78
    invoke-static {v0, p2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_1
    new-array p2, p1, [Ljava/lang/Object;

    const-string v0, "Failed to connect. (Not supported device)"

    .line 76
    invoke-static {v0, p2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_2
    new-array p2, p1, [Ljava/lang/Object;

    const-string v0, "Failed to connect. (Timeout)"

    .line 75
    invoke-static {v0, p2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    new-array p2, p1, [Ljava/lang/Object;

    const-string v0, "Failed to connect. (Already connected)"

    .line 74
    invoke-static {v0, p2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    new-array p2, p1, [Ljava/lang/Object;

    const-string v0, "Failed to connect. (Not available)"

    .line 73
    invoke-static {v0, p2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    new-array p2, p1, [Ljava/lang/Object;

    const-string v0, "Failed to connect. (Unexpected error)"

    .line 79
    invoke-static {v0, p2}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_3
    const/4 p2, 0x1

    new-array p1, p1, [Ljava/lang/Object;

    const-string v0, "Connect success."

    .line 71
    invoke-static {v0, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 p1, 0x1

    :goto_0
    if-nez p1, :cond_4

    .line 84
    iget-object p1, p0, Lcom/squareup/scales/StarScale$scaleCallback$1;->this$0:Lcom/squareup/scales/StarScale;

    const/4 p2, 0x0

    check-cast p2, Lcom/starmicronics/starmgsio/Scale;

    invoke-static {p1, p2}, Lcom/squareup/scales/StarScale;->access$setScale$p(Lcom/squareup/scales/StarScale;Lcom/starmicronics/starmgsio/Scale;)V

    :cond_4
    return-void

    :pswitch_data_0
    .packed-switch 0x69
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onDisconnect(Lcom/starmicronics/starmgsio/Scale;I)V
    .locals 1

    .line 92
    iget-object p1, p0, Lcom/squareup/scales/StarScale$scaleCallback$1;->this$0:Lcom/squareup/scales/StarScale;

    const/4 v0, 0x0

    check-cast v0, Lcom/starmicronics/starmgsio/Scale;

    invoke-static {p1, v0}, Lcom/squareup/scales/StarScale;->access$setScale$p(Lcom/squareup/scales/StarScale;Lcom/starmicronics/starmgsio/Scale;)V

    const/4 p1, 0x0

    if-eqz p2, :cond_4

    const/16 v0, 0x66

    if-eq p2, v0, :cond_3

    const/16 v0, 0xc8

    if-eq p2, v0, :cond_2

    const/16 v0, 0x68

    if-eq p2, v0, :cond_1

    const/16 v0, 0x69

    if-eq p2, v0, :cond_0

    new-array p1, p1, [Ljava/lang/Object;

    const-string p2, "Unknown disconnection."

    .line 100
    invoke-static {p2, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    new-array p1, p1, [Ljava/lang/Object;

    const-string p2, "Failed to disconnect - timeout"

    .line 97
    invoke-static {p2, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    new-array p1, p1, [Ljava/lang/Object;

    const-string p2, "Unexpected disconnection."

    .line 99
    invoke-static {p2, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    new-array p1, p1, [Ljava/lang/Object;

    const-string p2, "Failed to disconnect - Unexpected error"

    .line 98
    invoke-static {p2, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_3
    new-array p1, p1, [Ljava/lang/Object;

    const-string p2, "Failed to disconnect - not connected"

    .line 96
    invoke-static {p2, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_4
    new-array p1, p1, [Ljava/lang/Object;

    const-string p2, "Disconnect success."

    .line 95
    invoke-static {p2, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method public onReadScaleData(Lcom/starmicronics/starmgsio/Scale;Lcom/starmicronics/starmgsio/ScaleData;)V
    .locals 7

    const-string p1, "scaleData"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 108
    invoke-interface {p2}, Lcom/starmicronics/starmgsio/ScaleData;->getUnit()Lcom/starmicronics/starmgsio/ScaleData$Unit;

    move-result-object p1

    const/4 v0, 0x0

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    sget-object v1, Lcom/squareup/scales/StarScale$WhenMappings;->$EnumSwitchMapping$1:[I

    invoke-virtual {p1}, Lcom/starmicronics/starmgsio/ScaleData$Unit;->ordinal()I

    move-result p1

    aget p1, v1, p1

    const/4 v1, 0x1

    if-eq p1, v1, :cond_4

    const/4 v1, 0x2

    if-eq p1, v1, :cond_3

    const/4 v1, 0x3

    if-eq p1, v1, :cond_2

    const/4 v1, 0x4

    if-eq p1, v1, :cond_1

    :goto_0
    move-object v3, v0

    goto :goto_2

    .line 112
    :cond_1
    sget-object p1, Lcom/squareup/scales/UnitOfMeasurement;->G:Lcom/squareup/scales/UnitOfMeasurement;

    goto :goto_1

    .line 111
    :cond_2
    sget-object p1, Lcom/squareup/scales/UnitOfMeasurement;->MG:Lcom/squareup/scales/UnitOfMeasurement;

    goto :goto_1

    .line 110
    :cond_3
    sget-object p1, Lcom/squareup/scales/UnitOfMeasurement;->LB:Lcom/squareup/scales/UnitOfMeasurement;

    goto :goto_1

    .line 109
    :cond_4
    sget-object p1, Lcom/squareup/scales/UnitOfMeasurement;->OZ:Lcom/squareup/scales/UnitOfMeasurement;

    :goto_1
    move-object v3, p1

    :goto_2
    if-eqz v3, :cond_5

    .line 116
    iget-object p1, p0, Lcom/squareup/scales/StarScale$scaleCallback$1;->this$0:Lcom/squareup/scales/StarScale;

    invoke-static {p1}, Lcom/squareup/scales/StarScale;->access$getUnitOfMeasurementRelay$p(Lcom/squareup/scales/StarScale;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p1

    invoke-virtual {p1, v3}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    :cond_5
    if-nez v3, :cond_6

    .line 119
    new-instance p1, Lcom/squareup/scales/ScaleEvent$NoReadingEvent;

    sget-object p2, Lcom/squareup/scales/Status;->DECODING_ERROR:Lcom/squareup/scales/Status;

    invoke-direct {p1, v0, p2}, Lcom/squareup/scales/ScaleEvent$NoReadingEvent;-><init>(Lcom/squareup/scales/ScaleTracker$HardwareScale;Lcom/squareup/scales/Status;)V

    check-cast p1, Lcom/squareup/scales/ScaleEvent;

    goto :goto_3

    .line 120
    :cond_6
    invoke-interface {p2}, Lcom/starmicronics/starmgsio/ScaleData;->getStatus()Lcom/starmicronics/starmgsio/ScaleData$Status;

    move-result-object p1

    sget-object v1, Lcom/starmicronics/starmgsio/ScaleData$Status;->STABLE:Lcom/starmicronics/starmgsio/ScaleData$Status;

    if-ne p1, v1, :cond_7

    new-instance p1, Lcom/squareup/scales/ScaleEvent$StableReadingEvent;

    const/4 v2, 0x0

    .line 123
    invoke-interface {p2}, Lcom/starmicronics/starmgsio/ScaleData;->getWeight()D

    move-result-wide v4

    .line 124
    invoke-interface {p2}, Lcom/starmicronics/starmgsio/ScaleData;->getNumberOfDecimalPlaces()I

    move-result v6

    move-object v1, p1

    .line 120
    invoke-direct/range {v1 .. v6}, Lcom/squareup/scales/ScaleEvent$StableReadingEvent;-><init>(Lcom/squareup/scales/ScaleTracker$HardwareScale;Lcom/squareup/scales/UnitOfMeasurement;DI)V

    check-cast p1, Lcom/squareup/scales/ScaleEvent;

    goto :goto_3

    .line 126
    :cond_7
    invoke-interface {p2}, Lcom/starmicronics/starmgsio/ScaleData;->getStatus()Lcom/starmicronics/starmgsio/ScaleData$Status;

    move-result-object p1

    sget-object p2, Lcom/starmicronics/starmgsio/ScaleData$Status;->UNSTABLE:Lcom/starmicronics/starmgsio/ScaleData$Status;

    if-ne p1, p2, :cond_8

    new-instance p1, Lcom/squareup/scales/ScaleEvent$UnstableReadingEvent;

    invoke-direct {p1, v0}, Lcom/squareup/scales/ScaleEvent$UnstableReadingEvent;-><init>(Lcom/squareup/scales/ScaleTracker$HardwareScale;)V

    check-cast p1, Lcom/squareup/scales/ScaleEvent;

    goto :goto_3

    .line 127
    :cond_8
    new-instance p1, Lcom/squareup/scales/ScaleEvent$NoReadingEvent;

    sget-object p2, Lcom/squareup/scales/Status;->ERROR:Lcom/squareup/scales/Status;

    invoke-direct {p1, v0, p2}, Lcom/squareup/scales/ScaleEvent$NoReadingEvent;-><init>(Lcom/squareup/scales/ScaleTracker$HardwareScale;Lcom/squareup/scales/Status;)V

    check-cast p1, Lcom/squareup/scales/ScaleEvent;

    .line 129
    :goto_3
    iget-object p2, p0, Lcom/squareup/scales/StarScale$scaleCallback$1;->this$0:Lcom/squareup/scales/StarScale;

    invoke-static {p2}, Lcom/squareup/scales/StarScale;->access$getScaleEventRelay$p(Lcom/squareup/scales/StarScale;)Lcom/jakewharton/rxrelay2/BehaviorRelay;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/jakewharton/rxrelay2/BehaviorRelay;->accept(Ljava/lang/Object;)V

    return-void
.end method
