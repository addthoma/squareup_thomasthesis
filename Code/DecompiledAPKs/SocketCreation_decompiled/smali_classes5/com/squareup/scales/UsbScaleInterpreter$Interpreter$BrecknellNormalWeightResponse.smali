.class public final Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellNormalWeightResponse;
.super Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellWeightResponse;
.source "UsbScaleInterpreter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/scales/UsbScaleInterpreter$Interpreter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BrecknellNormalWeightResponse"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0006\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u000c\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0008\u0086\u0008\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0007H\u00c6\u0003J\'\u0010\u0012\u001a\u00020\u00002\u0008\u0008\u0002\u0010\u0002\u001a\u00020\u00032\u0008\u0008\u0002\u0010\u0004\u001a\u00020\u00052\u0008\u0008\u0002\u0010\u0006\u001a\u00020\u0007H\u00c6\u0001J\u0013\u0010\u0013\u001a\u00020\u00142\u0008\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u00d6\u0003J\t\u0010\u0017\u001a\u00020\u0007H\u00d6\u0001J\t\u0010\u0018\u001a\u00020\u0019H\u00d6\u0001R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\t\u0010\nR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u000b\u0010\u000cR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\r\u0010\u000e\u00a8\u0006\u001a"
    }
    d2 = {
        "Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellNormalWeightResponse;",
        "Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellWeightResponse;",
        "weight",
        "",
        "unit",
        "Lcom/squareup/scales/UnitOfMeasurement;",
        "numberOfDecimalPlaces",
        "",
        "(DLcom/squareup/scales/UnitOfMeasurement;I)V",
        "getNumberOfDecimalPlaces",
        "()I",
        "getUnit",
        "()Lcom/squareup/scales/UnitOfMeasurement;",
        "getWeight",
        "()D",
        "component1",
        "component2",
        "component3",
        "copy",
        "equals",
        "",
        "other",
        "",
        "hashCode",
        "toString",
        "",
        "impl_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final numberOfDecimalPlaces:I

.field private final unit:Lcom/squareup/scales/UnitOfMeasurement;

.field private final weight:D


# direct methods
.method public constructor <init>(DLcom/squareup/scales/UnitOfMeasurement;I)V
    .locals 1

    const-string/jumbo v0, "unit"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    invoke-direct {p0}, Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellWeightResponse;-><init>()V

    iput-wide p1, p0, Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellNormalWeightResponse;->weight:D

    iput-object p3, p0, Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellNormalWeightResponse;->unit:Lcom/squareup/scales/UnitOfMeasurement;

    iput p4, p0, Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellNormalWeightResponse;->numberOfDecimalPlaces:I

    return-void
.end method

.method public static synthetic copy$default(Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellNormalWeightResponse;DLcom/squareup/scales/UnitOfMeasurement;IILjava/lang/Object;)Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellNormalWeightResponse;
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    iget-wide p1, p0, Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellNormalWeightResponse;->weight:D

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    iget-object p3, p0, Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellNormalWeightResponse;->unit:Lcom/squareup/scales/UnitOfMeasurement;

    :cond_1
    and-int/lit8 p5, p5, 0x4

    if-eqz p5, :cond_2

    iget p4, p0, Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellNormalWeightResponse;->numberOfDecimalPlaces:I

    :cond_2
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellNormalWeightResponse;->copy(DLcom/squareup/scales/UnitOfMeasurement;I)Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellNormalWeightResponse;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final component1()D
    .locals 2

    iget-wide v0, p0, Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellNormalWeightResponse;->weight:D

    return-wide v0
.end method

.method public final component2()Lcom/squareup/scales/UnitOfMeasurement;
    .locals 1

    iget-object v0, p0, Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellNormalWeightResponse;->unit:Lcom/squareup/scales/UnitOfMeasurement;

    return-object v0
.end method

.method public final component3()I
    .locals 1

    iget v0, p0, Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellNormalWeightResponse;->numberOfDecimalPlaces:I

    return v0
.end method

.method public final copy(DLcom/squareup/scales/UnitOfMeasurement;I)Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellNormalWeightResponse;
    .locals 1

    const-string/jumbo v0, "unit"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellNormalWeightResponse;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellNormalWeightResponse;-><init>(DLcom/squareup/scales/UnitOfMeasurement;I)V

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    if-eq p0, p1, :cond_1

    instance-of v0, p1, Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellNormalWeightResponse;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellNormalWeightResponse;

    iget-wide v0, p0, Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellNormalWeightResponse;->weight:D

    iget-wide v2, p1, Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellNormalWeightResponse;->weight:D

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Double;->compare(DD)I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellNormalWeightResponse;->unit:Lcom/squareup/scales/UnitOfMeasurement;

    iget-object v1, p1, Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellNormalWeightResponse;->unit:Lcom/squareup/scales/UnitOfMeasurement;

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellNormalWeightResponse;->numberOfDecimalPlaces:I

    iget p1, p1, Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellNormalWeightResponse;->numberOfDecimalPlaces:I

    if-ne v0, p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return p1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    return p1
.end method

.method public final getNumberOfDecimalPlaces()I
    .locals 1

    .line 35
    iget v0, p0, Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellNormalWeightResponse;->numberOfDecimalPlaces:I

    return v0
.end method

.method public final getUnit()Lcom/squareup/scales/UnitOfMeasurement;
    .locals 1

    .line 34
    iget-object v0, p0, Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellNormalWeightResponse;->unit:Lcom/squareup/scales/UnitOfMeasurement;

    return-object v0
.end method

.method public final getWeight()D
    .locals 2

    .line 33
    iget-wide v0, p0, Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellNormalWeightResponse;->weight:D

    return-wide v0
.end method

.method public hashCode()I
    .locals 2

    iget-wide v0, p0, Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellNormalWeightResponse;->weight:D

    invoke-static {v0, v1}, L$r8$java8methods$utility$Double$hashCode$ID;->hashCode(D)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellNormalWeightResponse;->unit:Lcom/squareup/scales/UnitOfMeasurement;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellNormalWeightResponse;->numberOfDecimalPlaces:I

    invoke-static {v1}, L$r8$java8methods$utility$Integer$hashCode$II;->hashCode(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "BrecknellNormalWeightResponse(weight="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellNormalWeightResponse;->weight:D

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string v1, ", unit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellNormalWeightResponse;->unit:Lcom/squareup/scales/UnitOfMeasurement;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", numberOfDecimalPlaces="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/squareup/scales/UsbScaleInterpreter$Interpreter$BrecknellNormalWeightResponse;->numberOfDecimalPlaces:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
