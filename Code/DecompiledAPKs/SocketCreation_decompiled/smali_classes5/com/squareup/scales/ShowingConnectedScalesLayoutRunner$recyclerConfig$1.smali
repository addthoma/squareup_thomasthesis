.class final Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$recyclerConfig$1;
.super Lkotlin/jvm/internal/Lambda;
.source "ShowingConnectedScalesLayoutRunner.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner;-><init>(Landroid/view/View;Lcom/squareup/scales/ScalesActionBarConfig;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/cycler/Recycler$Config<",
        "Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$Row;",
        ">;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nShowingConnectedScalesLayoutRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 ShowingConnectedScalesLayoutRunner.kt\ncom/squareup/scales/ShowingConnectedScalesLayoutRunner$recyclerConfig$1\n+ 2 MosaicRowSpec.kt\ncom/squareup/cycler/mosaic/MosaicRowSpecKt\n+ 3 MosaicRowSpec.kt\ncom/squareup/cycler/mosaic/MosaicRowSpec\n+ 4 RecyclerEdges.kt\ncom/squareup/noho/dsl/RecyclerEdgesKt\n*L\n1#1,222:1\n33#2:223\n88#2:224\n34#2:225\n35#2:228\n89#2:229\n36#2:230\n88#2,2:231\n33#2:233\n88#2:234\n34#2:235\n35#2:238\n89#2:239\n36#2:240\n135#3,2:226\n135#3,2:236\n43#4,2:241\n*E\n*S KotlinDebug\n*F\n+ 1 ShowingConnectedScalesLayoutRunner.kt\ncom/squareup/scales/ShowingConnectedScalesLayoutRunner$recyclerConfig$1\n*L\n147#1:223\n147#1:224\n147#1:225\n147#1:228\n147#1:229\n147#1:230\n158#1,2:231\n163#1:233\n163#1:234\n163#1:235\n163#1:238\n163#1:239\n163#1:240\n147#1,2:226\n163#1,2:236\n174#1,2:241\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001*\u0008\u0012\u0004\u0012\u00020\u00030\u0002H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "Lcom/squareup/cycler/Recycler$Config;",
        "Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$Row;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner;


# direct methods
.method constructor <init>(Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$recyclerConfig$1;->this$0:Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 57
    check-cast p1, Lcom/squareup/cycler/Recycler$Config;

    invoke-virtual {p0, p1}, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$recyclerConfig$1;->invoke(Lcom/squareup/cycler/Recycler$Config;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/cycler/Recycler$Config;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/cycler/Recycler$Config<",
            "Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$Row;",
            ">;)V"
        }
    .end annotation

    const-string v0, "$receiver"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 147
    new-instance v0, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$recyclerConfig$1$1;

    invoke-direct {v0, p0}, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$recyclerConfig$1$1;-><init>(Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$recyclerConfig$1;)V

    check-cast v0, Lkotlin/jvm/functions/Function2;

    .line 224
    new-instance v1, Lcom/squareup/cycler/mosaic/MosaicRowSpec;

    sget-object v2, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$recyclerConfig$1$$special$$inlined$mosaicRow$1;->INSTANCE:Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$recyclerConfig$1$$special$$inlined$mosaicRow$1;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-direct {v1, v2}, Lcom/squareup/cycler/mosaic/MosaicRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 226
    new-instance v2, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$recyclerConfig$1$$special$$inlined$mosaicRow$2;

    invoke-direct {v2, v0}, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$recyclerConfig$1$$special$$inlined$mosaicRow$2;-><init>(Lkotlin/jvm/functions/Function2;)V

    check-cast v2, Lkotlin/jvm/functions/Function3;

    invoke-virtual {v1, v2}, Lcom/squareup/cycler/mosaic/MosaicRowSpec;->model(Lkotlin/jvm/functions/Function3;)V

    .line 228
    check-cast v1, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 224
    invoke-virtual {p1, v1}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 231
    new-instance v0, Lcom/squareup/cycler/mosaic/MosaicRowSpec;

    sget-object v1, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$recyclerConfig$1$$special$$inlined$mosaicRow$3;->INSTANCE:Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$recyclerConfig$1$$special$$inlined$mosaicRow$3;

    check-cast v1, Lkotlin/jvm/functions/Function1;

    invoke-direct {v0, v1}, Lcom/squareup/cycler/mosaic/MosaicRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 159
    move-object v1, v0

    check-cast v1, Lcom/squareup/cycler/Recycler$RowSpec;

    const/16 v2, 0xa

    invoke-static {v1, v2}, Lcom/squareup/noho/dsl/RecyclerEdgesKt;->setEdges(Lcom/squareup/cycler/Recycler$RowSpec;I)V

    .line 160
    iget-object v2, p0, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$recyclerConfig$1;->this$0:Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner;

    invoke-static {v2, v0}, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner;->access$scaleRowModel(Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner;Lcom/squareup/cycler/mosaic/MosaicRowSpec;)V

    .line 231
    invoke-virtual {p1, v1}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 163
    new-instance v0, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$recyclerConfig$1$3;

    invoke-direct {v0, p0}, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$recyclerConfig$1$3;-><init>(Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$recyclerConfig$1;)V

    check-cast v0, Lkotlin/jvm/functions/Function2;

    .line 234
    new-instance v1, Lcom/squareup/cycler/mosaic/MosaicRowSpec;

    sget-object v2, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$recyclerConfig$1$$special$$inlined$mosaicRow$4;->INSTANCE:Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$recyclerConfig$1$$special$$inlined$mosaicRow$4;

    check-cast v2, Lkotlin/jvm/functions/Function1;

    invoke-direct {v1, v2}, Lcom/squareup/cycler/mosaic/MosaicRowSpec;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 236
    new-instance v2, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$recyclerConfig$1$$special$$inlined$mosaicRow$5;

    invoke-direct {v2, v0}, Lcom/squareup/scales/ShowingConnectedScalesLayoutRunner$recyclerConfig$1$$special$$inlined$mosaicRow$5;-><init>(Lkotlin/jvm/functions/Function2;)V

    check-cast v2, Lkotlin/jvm/functions/Function3;

    invoke-virtual {v1, v2}, Lcom/squareup/cycler/mosaic/MosaicRowSpec;->model(Lkotlin/jvm/functions/Function3;)V

    .line 238
    check-cast v1, Lcom/squareup/cycler/Recycler$RowSpec;

    .line 234
    invoke-virtual {p1, v1}, Lcom/squareup/cycler/Recycler$Config;->row(Lcom/squareup/cycler/Recycler$RowSpec;)V

    .line 241
    new-instance v0, Lcom/squareup/noho/dsl/EdgesExtensionSpec;

    invoke-direct {v0}, Lcom/squareup/noho/dsl/EdgesExtensionSpec;-><init>()V

    const/4 v1, 0x0

    .line 175
    invoke-virtual {v0, v1}, Lcom/squareup/noho/dsl/EdgesExtensionSpec;->setDefault(I)V

    .line 176
    check-cast v0, Lcom/squareup/cycler/ExtensionSpec;

    .line 241
    invoke-virtual {p1, v0}, Lcom/squareup/cycler/Recycler$Config;->extension(Lcom/squareup/cycler/ExtensionSpec;)V

    return-void
.end method
