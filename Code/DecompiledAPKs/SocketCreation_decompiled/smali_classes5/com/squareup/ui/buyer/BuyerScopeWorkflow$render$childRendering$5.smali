.class final Lcom/squareup/ui/buyer/BuyerScopeWorkflow$render$childRendering$5;
.super Lkotlin/jvm/internal/Lambda;
.source "BuyerScopeWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/buyer/BuyerScopeWorkflow;->render(Lcom/squareup/ui/buyer/BuyerScopeInput;Lkotlin/Unit;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsResult;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lkotlin/Unit;",
        "+",
        "Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult;",
        ">;>;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nBuyerScopeWorkflow.kt\nKotlin\n*S Kotlin\n*F\n+ 1 BuyerScopeWorkflow.kt\ncom/squareup/ui/buyer/BuyerScopeWorkflow$render$childRendering$5\n+ 2 WorkflowAction.kt\ncom/squareup/workflow/WorkflowActionKt\n*L\n1#1,152:1\n179#2,3:153\n199#2,4:156\n*E\n*S KotlinDebug\n*F\n+ 1 BuyerScopeWorkflow.kt\ncom/squareup/ui/buyer/BuyerScopeWorkflow$render$childRendering$5\n*L\n145#1,3:153\n145#1,4:156\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "",
        "Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult;",
        "it",
        "Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsResult;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/buyer/BuyerScopeWorkflow$render$childRendering$5;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/ui/buyer/BuyerScopeWorkflow$render$childRendering$5;

    invoke-direct {v0}, Lcom/squareup/ui/buyer/BuyerScopeWorkflow$render$childRendering$5;-><init>()V

    sput-object v0, Lcom/squareup/ui/buyer/BuyerScopeWorkflow$render$childRendering$5;->INSTANCE:Lcom/squareup/ui/buyer/BuyerScopeWorkflow$render$childRendering$5;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsResult;)Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsResult;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lkotlin/Unit;",
            "Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 156
    new-instance p1, Lcom/squareup/ui/buyer/BuyerScopeWorkflow$render$childRendering$5$$special$$inlined$action$1;

    const-string v0, ""

    invoke-direct {p1, v0}, Lcom/squareup/ui/buyer/BuyerScopeWorkflow$render$childRendering$5$$special$$inlined$action$1;-><init>(Ljava/lang/String;)V

    check-cast p1, Lcom/squareup/workflow/WorkflowAction;

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 51
    check-cast p1, Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsResult;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyer/BuyerScopeWorkflow$render$childRendering$5;->invoke(Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsResult;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
