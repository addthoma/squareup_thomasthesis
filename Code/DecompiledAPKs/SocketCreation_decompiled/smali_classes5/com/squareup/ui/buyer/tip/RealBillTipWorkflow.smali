.class public final Lcom/squareup/ui/buyer/tip/RealBillTipWorkflow;
.super Lcom/squareup/workflow/StatelessWorkflow;
.source "RealBillTipWorkflow.kt"

# interfaces
.implements Lcom/squareup/ui/buyer/tip/BillTipWorkflow;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatelessWorkflow<",
        "Lkotlin/Unit;",
        "Lcom/squareup/ui/buyer/tip/BillTipResult;",
        "Lcom/squareup/workflow/legacy/Screen;",
        ">;",
        "Lcom/squareup/ui/buyer/tip/BillTipWorkflow;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0001\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0002*\u0001\u0012\u0008\u0000\u0018\u00002\u00020\u00012&\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0016\u0012\u0014\u0012\u0002\u0008\u0003\u0012\u0004\u0012\u00020\u00060\u0005j\u0006\u0012\u0002\u0008\u0003`\u00070\u0002B)\u0008\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0008\u0008\u0001\u0010\u000e\u001a\u00020\u000f\u00a2\u0006\u0002\u0010\u0010JX\u0010\u0014\u001aR\u0012\u001a\u0012\u0018\u0012\u0004\u0012\u00020\u0003\u0012\u000e\u0012\u000c\u0012\u0002\u0008\u0003\u0012\u0004\u0012\u00020\u00060\u00050\u0016\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0015j&\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012\u0016\u0012\u0014\u0012\u0002\u0008\u0003\u0012\u0004\u0012\u00020\u00060\u0005j\u0006\u0012\u0002\u0008\u0003`\u0007`\u0017H\u0016J\u0008\u0010\u0018\u001a\u00020\u0019H\u0002J;\u0010\u001a\u001a\u0014\u0012\u0002\u0008\u0003\u0012\u0004\u0012\u00020\u00060\u0005j\u0006\u0012\u0002\u0008\u0003`\u00072\u0006\u0010\u001b\u001a\u00020\u00032\u0012\u0010\u001c\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00040\u001dH\u0016\u00a2\u0006\u0002\u0010\u001eR\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010\u0013R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001f"
    }
    d2 = {
        "Lcom/squareup/ui/buyer/tip/RealBillTipWorkflow;",
        "Lcom/squareup/ui/buyer/tip/BillTipWorkflow;",
        "Lcom/squareup/workflow/StatelessWorkflow;",
        "",
        "Lcom/squareup/ui/buyer/tip/BillTipResult;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "",
        "Lcom/squareup/workflow/legacy/V2ScreenWrapper;",
        "transaction",
        "Lcom/squareup/payment/Transaction;",
        "tipReaderHandler",
        "Lcom/squareup/ui/buyer/tip/TipReaderHandler;",
        "tipWorkflow",
        "Lcom/squareup/checkoutflow/core/tip/TipWorkflow;",
        "mainDispatcher",
        "Lkotlinx/coroutines/CoroutineDispatcher;",
        "(Lcom/squareup/payment/Transaction;Lcom/squareup/ui/buyer/tip/TipReaderHandler;Lcom/squareup/checkoutflow/core/tip/TipWorkflow;Lkotlinx/coroutines/CoroutineDispatcher;)V",
        "tipReaderRegistrar",
        "com/squareup/ui/buyer/tip/RealBillTipWorkflow$tipReaderRegistrar$1",
        "Lcom/squareup/ui/buyer/tip/RealBillTipWorkflow$tipReaderRegistrar$1;",
        "asLegacyLauncher",
        "Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;",
        "Lcom/squareup/workflow/legacyintegration/LegacyState;",
        "Lcom/squareup/workflow/legacyintegration/LegacyLauncher;",
        "createTipProps",
        "Lcom/squareup/checkoutflow/core/tip/TipProps;",
        "render",
        "props",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "(Lkotlin/Unit;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/legacy/Screen;",
        "buyer-flow_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final mainDispatcher:Lkotlinx/coroutines/CoroutineDispatcher;

.field private final tipReaderHandler:Lcom/squareup/ui/buyer/tip/TipReaderHandler;

.field private final tipReaderRegistrar:Lcom/squareup/ui/buyer/tip/RealBillTipWorkflow$tipReaderRegistrar$1;

.field private final tipWorkflow:Lcom/squareup/checkoutflow/core/tip/TipWorkflow;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method public constructor <init>(Lcom/squareup/payment/Transaction;Lcom/squareup/ui/buyer/tip/TipReaderHandler;Lcom/squareup/checkoutflow/core/tip/TipWorkflow;Lkotlinx/coroutines/CoroutineDispatcher;)V
    .locals 1
    .param p4    # Lkotlinx/coroutines/CoroutineDispatcher;
        .annotation runtime Lcom/squareup/thread/Main$Immediate;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string/jumbo v0, "transaction"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "tipReaderHandler"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "tipWorkflow"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "mainDispatcher"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    invoke-direct {p0}, Lcom/squareup/workflow/StatelessWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/buyer/tip/RealBillTipWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    iput-object p2, p0, Lcom/squareup/ui/buyer/tip/RealBillTipWorkflow;->tipReaderHandler:Lcom/squareup/ui/buyer/tip/TipReaderHandler;

    iput-object p3, p0, Lcom/squareup/ui/buyer/tip/RealBillTipWorkflow;->tipWorkflow:Lcom/squareup/checkoutflow/core/tip/TipWorkflow;

    iput-object p4, p0, Lcom/squareup/ui/buyer/tip/RealBillTipWorkflow;->mainDispatcher:Lkotlinx/coroutines/CoroutineDispatcher;

    .line 31
    new-instance p1, Lcom/squareup/ui/buyer/tip/RealBillTipWorkflow$tipReaderRegistrar$1;

    invoke-direct {p1, p0}, Lcom/squareup/ui/buyer/tip/RealBillTipWorkflow$tipReaderRegistrar$1;-><init>(Lcom/squareup/ui/buyer/tip/RealBillTipWorkflow;)V

    iput-object p1, p0, Lcom/squareup/ui/buyer/tip/RealBillTipWorkflow;->tipReaderRegistrar:Lcom/squareup/ui/buyer/tip/RealBillTipWorkflow$tipReaderRegistrar$1;

    return-void
.end method

.method public static final synthetic access$getTipReaderHandler$p(Lcom/squareup/ui/buyer/tip/RealBillTipWorkflow;)Lcom/squareup/ui/buyer/tip/TipReaderHandler;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/squareup/ui/buyer/tip/RealBillTipWorkflow;->tipReaderHandler:Lcom/squareup/ui/buyer/tip/TipReaderHandler;

    return-object p0
.end method

.method public static final synthetic access$getTransaction$p(Lcom/squareup/ui/buyer/tip/RealBillTipWorkflow;)Lcom/squareup/payment/Transaction;
    .locals 0

    .line 22
    iget-object p0, p0, Lcom/squareup/ui/buyer/tip/RealBillTipWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    return-object p0
.end method

.method private final createTipProps()Lcom/squareup/checkoutflow/core/tip/TipProps;
    .locals 8

    .line 62
    iget-object v0, p0, Lcom/squareup/ui/buyer/tip/RealBillTipWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasPayment()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/squareup/ui/buyer/tip/RealBillTipWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requirePayment()Lcom/squareup/payment/Payment;

    move-result-object v0

    const-string/jumbo v1, "transaction.requirePayment()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/payment/Payment;->getTenderAmount()Lcom/squareup/protos/common/Money;

    move-result-object v0

    goto :goto_0

    .line 66
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/tip/RealBillTipWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v0

    :goto_0
    move-object v2, v0

    .line 68
    iget-object v0, p0, Lcom/squareup/ui/buyer/tip/RealBillTipWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requireTippingPayment()Lcom/squareup/payment/AcceptsTips;

    move-result-object v0

    .line 69
    new-instance v7, Lcom/squareup/checkoutflow/core/tip/TipProps;

    const-string v1, "amountDue"

    .line 70
    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    iget-object v1, p0, Lcom/squareup/ui/buyer/tip/RealBillTipWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->hasAutoGratuity()Z

    move-result v3

    .line 72
    iget-object v1, p0, Lcom/squareup/ui/buyer/tip/RealBillTipWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->getTipSettings()Lcom/squareup/settings/server/TipSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/settings/server/TipSettings;->isUsingCustomAmounts()Z

    move-result v4

    const-string v1, "payment"

    .line 73
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v0}, Lcom/squareup/payment/AcceptsTips;->getTipOptions()Ljava/util/List;

    move-result-object v5

    const-string v1, "payment.tipOptions"

    invoke-static {v5, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    invoke-interface {v0}, Lcom/squareup/payment/AcceptsTips;->getCustomTipMaxMoney()Lcom/squareup/protos/common/Money;

    move-result-object v6

    const-string v0, "payment.customTipMaxMoney"

    invoke-static {v6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v1, v7

    .line 69
    invoke-direct/range {v1 .. v6}, Lcom/squareup/checkoutflow/core/tip/TipProps;-><init>(Lcom/squareup/protos/common/Money;ZZLjava/util/List;Lcom/squareup/protos/common/Money;)V

    return-object v7
.end method


# virtual methods
.method public asLegacyLauncher()Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/squareup/workflow/legacy/WorkflowPool$Launcher<",
            "Lcom/squareup/workflow/legacyintegration/LegacyState<",
            "Lkotlin/Unit;",
            "Lcom/squareup/workflow/legacy/Screen;",
            ">;",
            "Lkotlin/Unit;",
            "Lcom/squareup/ui/buyer/tip/BillTipResult;",
            ">;"
        }
    .end annotation

    .line 79
    move-object v0, p0

    check-cast v0, Lcom/squareup/workflow/Workflow;

    iget-object v1, p0, Lcom/squareup/ui/buyer/tip/RealBillTipWorkflow;->mainDispatcher:Lkotlinx/coroutines/CoroutineDispatcher;

    invoke-static {v0, v1}, Lcom/squareup/workflow/legacyintegration/LegacyLauncherKt;->createLegacyLauncher(Lcom/squareup/workflow/Workflow;Lkotlinx/coroutines/CoroutineDispatcher;)Lcom/squareup/workflow/legacy/WorkflowPool$Launcher;

    move-result-object v0

    return-object v0
.end method

.method public render(Lkotlin/Unit;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/legacy/Screen;
    .locals 7

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "context"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    iget-object p1, p0, Lcom/squareup/ui/buyer/tip/RealBillTipWorkflow;->tipReaderRegistrar:Lcom/squareup/ui/buyer/tip/RealBillTipWorkflow$tipReaderRegistrar$1;

    check-cast p1, Lcom/squareup/workflow/Worker;

    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-static {p2, p1, v0, v1, v0}, Lcom/squareup/workflow/RenderContextKt;->runningWorker$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Worker;Ljava/lang/String;ILjava/lang/Object;)V

    .line 48
    iget-object p1, p0, Lcom/squareup/ui/buyer/tip/RealBillTipWorkflow;->tipWorkflow:Lcom/squareup/checkoutflow/core/tip/TipWorkflow;

    move-object v1, p1

    check-cast v1, Lcom/squareup/workflow/Workflow;

    invoke-direct {p0}, Lcom/squareup/ui/buyer/tip/RealBillTipWorkflow;->createTipProps()Lcom/squareup/checkoutflow/core/tip/TipProps;

    move-result-object v2

    new-instance p1, Lcom/squareup/ui/buyer/tip/RealBillTipWorkflow$render$1;

    invoke-direct {p1, p0}, Lcom/squareup/ui/buyer/tip/RealBillTipWorkflow$render$1;-><init>(Lcom/squareup/ui/buyer/tip/RealBillTipWorkflow;)V

    move-object v4, p1

    check-cast v4, Lkotlin/jvm/functions/Function1;

    const/4 v3, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v0, p2

    invoke-static/range {v0 .. v6}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/workflow/legacy/Screen;

    return-object p1
.end method

.method public bridge synthetic render(Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 22
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/buyer/tip/RealBillTipWorkflow;->render(Lkotlin/Unit;Lcom/squareup/workflow/RenderContext;)Lcom/squareup/workflow/legacy/Screen;

    move-result-object p1

    return-object p1
.end method
