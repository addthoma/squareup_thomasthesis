.class Lcom/squareup/ui/buyer/BuyerWorkflow;
.super Ljava/lang/Object;
.source "BuyerWorkflow.java"


# instance fields
.field private final activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

.field private final buyerFlowReceiptManager:Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;

.field private final countryCodeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;"
        }
    .end annotation
.end field

.field private final device:Lcom/squareup/util/Device;

.field private final displayNameProvider:Lcom/squareup/ui/buyer/DisplayNameProvider;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final hudToaster:Lcom/squareup/hudtoaster/HudToaster;

.field private final loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

.field private final moneyFormatter:Lcom/squareup/text/Formatter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;"
        }
    .end annotation
.end field

.field private final paymentCapturer:Lcom/squareup/payment/PaymentCapturer;

.field private final postReceiptOperations:Lcom/squareup/payment/PostReceiptOperations;

.field private final printerStations:Lcom/squareup/print/PrinterStations;

.field private final receiptAutoCloseProvider:Lcom/squareup/checkoutflow/datamodels/receipt/ReceiptAutoCloseProvider;

.field private final res:Lcom/squareup/util/Res;

.field private final settings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final skipReceiptScreenSettings:Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;

.field private final tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

.field private final tenderInEdit:Lcom/squareup/payment/TenderInEdit;

.field private final tipDeterminerFactory:Lcom/squareup/payment/TipDeterminerFactory;

.field private final transaction:Lcom/squareup/payment/Transaction;


# direct methods
.method constructor <init>(Lcom/squareup/print/PrinterStations;Lcom/squareup/payment/Transaction;Lcom/squareup/payment/TipDeterminerFactory;Lcom/squareup/settings/server/Features;Lcom/squareup/cardreader/dipper/ActiveCardReader;Lcom/squareup/payment/TenderInEdit;Lcom/squareup/payment/PaymentCapturer;Lcom/squareup/util/Res;Lcom/squareup/util/Device;Lcom/squareup/payment/PostReceiptOperations;Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/payment/tender/TenderFactory;Lcom/squareup/text/Formatter;Lcom/squareup/checkoutflow/datamodels/receipt/ReceiptAutoCloseProvider;Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;Ljavax/inject/Provider;Lcom/squareup/ui/buyer/DisplayNameProvider;Lcom/squareup/loyalty/LoyaltySettings;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/print/PrinterStations;",
            "Lcom/squareup/payment/Transaction;",
            "Lcom/squareup/payment/TipDeterminerFactory;",
            "Lcom/squareup/settings/server/Features;",
            "Lcom/squareup/cardreader/dipper/ActiveCardReader;",
            "Lcom/squareup/payment/TenderInEdit;",
            "Lcom/squareup/payment/PaymentCapturer;",
            "Lcom/squareup/util/Res;",
            "Lcom/squareup/util/Device;",
            "Lcom/squareup/payment/PostReceiptOperations;",
            "Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            "Lcom/squareup/hudtoaster/HudToaster;",
            "Lcom/squareup/payment/tender/TenderFactory;",
            "Lcom/squareup/text/Formatter<",
            "Lcom/squareup/protos/common/Money;",
            ">;",
            "Lcom/squareup/checkoutflow/datamodels/receipt/ReceiptAutoCloseProvider;",
            "Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/CountryCode;",
            ">;",
            "Lcom/squareup/ui/buyer/DisplayNameProvider;",
            "Lcom/squareup/loyalty/LoyaltySettings;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v0, p0

    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 96
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerWorkflow;->printerStations:Lcom/squareup/print/PrinterStations;

    move-object v1, p2

    .line 97
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    move-object v1, p3

    .line 98
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerWorkflow;->tipDeterminerFactory:Lcom/squareup/payment/TipDeterminerFactory;

    move-object v1, p4

    .line 99
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerWorkflow;->features:Lcom/squareup/settings/server/Features;

    move-object/from16 v1, p16

    .line 100
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerWorkflow;->receiptAutoCloseProvider:Lcom/squareup/checkoutflow/datamodels/receipt/ReceiptAutoCloseProvider;

    move-object v1, p5

    .line 101
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerWorkflow;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    move-object v1, p6

    .line 102
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerWorkflow;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    move-object v1, p7

    .line 103
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerWorkflow;->paymentCapturer:Lcom/squareup/payment/PaymentCapturer;

    move-object v1, p8

    .line 104
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerWorkflow;->res:Lcom/squareup/util/Res;

    move-object v1, p9

    .line 105
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerWorkflow;->device:Lcom/squareup/util/Device;

    move-object v1, p10

    .line 106
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerWorkflow;->postReceiptOperations:Lcom/squareup/payment/PostReceiptOperations;

    move-object v1, p11

    .line 107
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerWorkflow;->skipReceiptScreenSettings:Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;

    move-object v1, p12

    .line 108
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerWorkflow;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    move-object v1, p13

    .line 109
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerWorkflow;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    move-object/from16 v1, p14

    .line 110
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerWorkflow;->tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

    move-object/from16 v1, p15

    .line 111
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerWorkflow;->moneyFormatter:Lcom/squareup/text/Formatter;

    move-object/from16 v1, p17

    .line 112
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerWorkflow;->buyerFlowReceiptManager:Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;

    move-object/from16 v1, p18

    .line 113
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerWorkflow;->countryCodeProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p19

    .line 114
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerWorkflow;->displayNameProvider:Lcom/squareup/ui/buyer/DisplayNameProvider;

    move-object/from16 v1, p20

    .line 115
    iput-object v1, v0, Lcom/squareup/ui/buyer/BuyerWorkflow;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    return-void
.end method

.method private getAuthSpinnerScreen(Lcom/squareup/ui/buyer/BuyerScope;)Lcom/squareup/container/ContainerTreeKey;
    .locals 1

    .line 406
    new-instance v0, Lcom/squareup/ui/buyer/auth/AuthSpinnerScreen;

    invoke-direct {v0, p1}, Lcom/squareup/ui/buyer/auth/AuthSpinnerScreen;-><init>(Lcom/squareup/ui/buyer/BuyerScope;)V

    return-object v0
.end method

.method private getPostAuthCardScreen(Lcom/squareup/ui/buyer/BuyerScope;)Lcom/squareup/container/ContainerTreeKey;
    .locals 1

    .line 249
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/BillPayment;->hasPartialAuthCardTenderInFlight()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 250
    new-instance v0, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningScreen;

    invoke-direct {v0, p1}, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningScreen;-><init>(Lcom/squareup/ui/buyer/BuyerScope;)V

    return-object v0

    .line 253
    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyer/BuyerWorkflow;->getPostPartialAuthWarningScreen(Lcom/squareup/ui/buyer/BuyerScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    return-object p1
.end method

.method private getReceiptScreen(Lcom/squareup/ui/buyer/BuyerScope;)Lcom/squareup/container/ContainerTreeKey;
    .locals 17

    move-object/from16 v0, p0

    move-object/from16 v2, p1

    .line 303
    iget-object v1, v0, Lcom/squareup/ui/buyer/BuyerWorkflow;->receiptAutoCloseProvider:Lcom/squareup/checkoutflow/datamodels/receipt/ReceiptAutoCloseProvider;

    invoke-interface {v1}, Lcom/squareup/checkoutflow/datamodels/receipt/ReceiptAutoCloseProvider;->hasReceiptAutoCloseOverride()Z

    move-result v1

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-nez v1, :cond_1

    iget-object v1, v0, Lcom/squareup/ui/buyer/BuyerWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    .line 304
    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->requirePayment()Lcom/squareup/payment/Payment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/payment/Payment;->isLocalPayment()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    .line 305
    :goto_1
    iget-object v5, v0, Lcom/squareup/ui/buyer/BuyerWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    .line 306
    invoke-virtual {v5}, Lcom/squareup/payment/Transaction;->requireBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v5

    invoke-virtual {v5}, Lcom/squareup/payment/BillPayment;->hasSmartCardTenderInFlight()Z

    move-result v5

    if-nez v1, :cond_5

    .line 307
    iget-object v6, v0, Lcom/squareup/ui/buyer/BuyerWorkflow;->device:Lcom/squareup/util/Device;

    .line 308
    invoke-interface {v6}, Lcom/squareup/util/Device;->isTablet()Z

    move-result v6

    if-eqz v6, :cond_5

    if-nez v5, :cond_5

    .line 313
    iget-object v1, v0, Lcom/squareup/ui/buyer/BuyerWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->requirePayment()Lcom/squareup/payment/Payment;

    move-result-object v1

    instance-of v1, v1, Lcom/squareup/payment/AcceptsTips;

    if-eqz v1, :cond_2

    .line 314
    iget-object v1, v0, Lcom/squareup/ui/buyer/BuyerWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->requireTippingPayment()Lcom/squareup/payment/AcceptsTips;

    move-result-object v1

    invoke-interface {v1}, Lcom/squareup/payment/AcceptsTips;->askForTip()Z

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    .line 317
    :goto_2
    iget-object v6, v0, Lcom/squareup/ui/buyer/BuyerWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v6}, Lcom/squareup/payment/Transaction;->requirePayment()Lcom/squareup/payment/Payment;

    move-result-object v6

    instance-of v6, v6, Lcom/squareup/payment/AcceptsSignature;

    if-eqz v6, :cond_3

    .line 318
    iget-object v6, v0, Lcom/squareup/ui/buyer/BuyerWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v6}, Lcom/squareup/payment/Transaction;->requireSignedPayment()Lcom/squareup/payment/AcceptsSignature;

    move-result-object v6

    invoke-interface {v6}, Lcom/squareup/payment/AcceptsSignature;->askForSignature()Z

    move-result v6

    goto :goto_3

    :cond_3
    const/4 v6, 0x0

    :goto_3
    if-nez v1, :cond_4

    if-nez v6, :cond_4

    const/4 v1, 0x1

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    .line 324
    :cond_5
    :goto_4
    iget-object v6, v0, Lcom/squareup/ui/buyer/BuyerWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v6}, Lcom/squareup/payment/Transaction;->getUniqueKey()Ljava/lang/String;

    move-result-object v6

    .line 325
    iget-object v7, v0, Lcom/squareup/ui/buyer/BuyerWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v7}, Lcom/squareup/payment/Transaction;->endCurrentTenderAndCreateReceipt()V

    const/4 v7, 0x0

    if-eqz v5, :cond_6

    .line 328
    iget-object v5, v0, Lcom/squareup/ui/buyer/BuyerWorkflow;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    .line 329
    invoke-virtual {v5}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->getMostRecentActiveCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v5

    move-object v9, v5

    goto :goto_5

    :cond_6
    move-object v9, v7

    .line 330
    :goto_5
    iget-object v5, v0, Lcom/squareup/ui/buyer/BuyerWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v5}, Lcom/squareup/payment/Transaction;->isTakingPaymentFromInvoice()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 331
    iget-object v1, v0, Lcom/squareup/ui/buyer/BuyerWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->asBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v1

    .line 332
    invoke-virtual {v1}, Lcom/squareup/payment/BillPayment;->getRemainingAmountOnTotalBill()Lcom/squareup/protos/common/Money;

    move-result-object v1

    .line 333
    new-instance v3, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreen;

    invoke-direct {v3, v2, v6, v1, v9}, Lcom/squareup/ui/buyer/invoice/InvoicePaidScreen;-><init>(Lcom/squareup/ui/buyer/BuyerScope;Ljava/lang/String;Lcom/squareup/protos/common/Money;Lcom/squareup/cardreader/CardReaderId;)V

    goto/16 :goto_a

    .line 335
    :cond_7
    iget-object v5, v0, Lcom/squareup/ui/buyer/BuyerWorkflow;->features:Lcom/squareup/settings/server/Features;

    sget-object v8, Lcom/squareup/settings/server/Features$Feature;->SHOW_NOHO_RECEIPT:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v5, v8}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v5

    if-eqz v5, :cond_c

    .line 336
    iget-object v1, v0, Lcom/squareup/ui/buyer/BuyerWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->requireReceiptForLastPayment()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v5

    .line 339
    new-instance v8, Lcom/squareup/ui/buyer/-$$Lambda$BuyerWorkflow$opdUlcem7J8N7ZGyYiDIPLkZZpk;

    invoke-direct {v8, v5}, Lcom/squareup/ui/buyer/-$$Lambda$BuyerWorkflow$opdUlcem7J8N7ZGyYiDIPLkZZpk;-><init>(Lcom/squareup/payment/PaymentReceipt;)V

    .line 341
    iget-object v1, v0, Lcom/squareup/ui/buyer/BuyerWorkflow;->displayNameProvider:Lcom/squareup/ui/buyer/DisplayNameProvider;

    invoke-virtual {v1}, Lcom/squareup/ui/buyer/DisplayNameProvider;->getShouldShowDisplayName()Z

    move-result v1

    if-eqz v1, :cond_8

    iget-object v1, v0, Lcom/squareup/ui/buyer/BuyerWorkflow;->displayNameProvider:Lcom/squareup/ui/buyer/DisplayNameProvider;

    .line 342
    invoke-virtual {v1}, Lcom/squareup/ui/buyer/DisplayNameProvider;->getDisplayNameText()Ljava/lang/String;

    move-result-object v1

    move-object v10, v1

    goto :goto_6

    :cond_8
    move-object v10, v7

    .line 343
    :goto_6
    iget-object v1, v0, Lcom/squareup/ui/buyer/BuyerWorkflow;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/settings/server/UserSettings;->getSubunitName()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_9

    const-string v1, ""

    goto :goto_7

    :cond_9
    iget-object v1, v0, Lcom/squareup/ui/buyer/BuyerWorkflow;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 344
    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/settings/server/UserSettings;->getSubunitName()Ljava/lang/String;

    move-result-object v1

    :goto_7
    move-object v13, v1

    .line 345
    iget-object v1, v0, Lcom/squareup/ui/buyer/BuyerWorkflow;->receiptAutoCloseProvider:Lcom/squareup/checkoutflow/datamodels/receipt/ReceiptAutoCloseProvider;

    invoke-interface {v1}, Lcom/squareup/checkoutflow/datamodels/receipt/ReceiptAutoCloseProvider;->getReceiptAutoCloseOverride()Lcom/squareup/checkoutflow/datamodels/receipt/ReceiptAutoCloseOverride;

    move-result-object v1

    .line 346
    instance-of v11, v1, Lcom/squareup/checkoutflow/datamodels/receipt/ReceiptAutoCloseOverride$HasReceiptAutoCloseOverride;

    if-eqz v11, :cond_a

    check-cast v1, Lcom/squareup/checkoutflow/datamodels/receipt/ReceiptAutoCloseOverride$HasReceiptAutoCloseOverride;

    .line 347
    invoke-virtual {v1}, Lcom/squareup/checkoutflow/datamodels/receipt/ReceiptAutoCloseOverride$HasReceiptAutoCloseOverride;->getDurationMs()J

    move-result-wide v11

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    move-object v11, v1

    goto :goto_8

    :cond_a
    move-object v11, v7

    .line 348
    :goto_8
    iget-object v1, v0, Lcom/squareup/ui/buyer/BuyerWorkflow;->features:Lcom/squareup/settings/server/Features;

    sget-object v7, Lcom/squareup/settings/server/Features$Feature;->CAN_SHOW_SMS_MARKETING:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v7}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    if-eqz v1, :cond_b

    iget-object v1, v0, Lcom/squareup/ui/buyer/BuyerWorkflow;->loyaltySettings:Lcom/squareup/loyalty/LoyaltySettings;

    .line 349
    invoke-interface {v1}, Lcom/squareup/loyalty/LoyaltySettings;->isLoyaltyProgramActive()Z

    move-result v1

    if-nez v1, :cond_b

    const/4 v12, 0x1

    goto :goto_9

    :cond_b
    const/4 v12, 0x0

    .line 351
    :goto_9
    new-instance v14, Lcom/squareup/ui/buyer/receipt/ReceiptBootstrapScreen;

    iget-object v1, v0, Lcom/squareup/ui/buyer/BuyerWorkflow;->countryCodeProvider:Ljavax/inject/Provider;

    .line 352
    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/squareup/CountryCode;

    iget-object v1, v0, Lcom/squareup/ui/buyer/BuyerWorkflow;->features:Lcom/squareup/settings/server/Features;

    sget-object v3, Lcom/squareup/settings/server/Features$Feature;->CAN_USE_BUYER_LANGUAGE_SELECTION:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v3}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v15

    iget-object v1, v0, Lcom/squareup/ui/buyer/BuyerWorkflow;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 353
    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/settings/server/PaymentSettings;->supportsSms()Z

    move-result v16

    move-object v1, v14

    move-object/from16 v2, p1

    move-object v3, v6

    move-object v4, v5

    move-object v5, v8

    move-object v6, v7

    move v7, v15

    move-object v8, v10

    move/from16 v10, v16

    invoke-direct/range {v1 .. v13}, Lcom/squareup/ui/buyer/receipt/ReceiptBootstrapScreen;-><init>(Lcom/squareup/ui/buyer/BuyerScope;Ljava/lang/String;Lcom/squareup/payment/PaymentReceipt;Lkotlin/jvm/functions/Function1;Lcom/squareup/CountryCode;ZLjava/lang/String;Lcom/squareup/cardreader/CardReaderId;ZLjava/lang/Long;ZLjava/lang/String;)V

    move-object v3, v14

    goto :goto_a

    .line 356
    :cond_c
    new-instance v3, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptScreenLegacy;

    invoke-direct {v3, v2, v1, v6, v9}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptScreenLegacy;-><init>(Lcom/squareup/ui/buyer/BuyerScope;ZLjava/lang/String;Lcom/squareup/cardreader/CardReaderId;)V

    .line 360
    :goto_a
    iget-object v1, v0, Lcom/squareup/ui/buyer/BuyerWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v1}, Lcom/squareup/payment/Transaction;->requirePayment()Lcom/squareup/payment/Payment;

    move-result-object v1

    .line 361
    invoke-virtual {v1}, Lcom/squareup/payment/Payment;->isComplete()Z

    move-result v1

    if-eqz v1, :cond_d

    .line 362
    iget-object v1, v0, Lcom/squareup/ui/buyer/BuyerWorkflow;->buyerFlowReceiptManager:Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;

    invoke-interface {v1}, Lcom/squareup/ui/buyer/receiptlegacy/BuyerFlowReceiptManager;->printOrderStubAndTicket()V

    .line 364
    :cond_d
    iget-object v1, v0, Lcom/squareup/ui/buyer/BuyerWorkflow;->postReceiptOperations:Lcom/squareup/payment/PostReceiptOperations;

    invoke-virtual {v1}, Lcom/squareup/payment/PostReceiptOperations;->resetTransactionBeforeReceiptScreen()V

    return-object v3
.end method

.method static synthetic lambda$getReceiptScreen$0(Lcom/squareup/payment/PaymentReceipt;Lcom/squareup/util/Res;)Ljava/lang/String;
    .locals 0

    .line 340
    invoke-virtual {p0, p1}, Lcom/squareup/payment/PaymentReceipt;->getRemainingBalanceText(Lcom/squareup/util/Res;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method doCaptureAndGetReceiptScreen(Lcom/squareup/ui/buyer/BuyerScope;Z)Lcom/squareup/container/ContainerTreeKey;
    .locals 1

    .line 241
    :try_start_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerWorkflow;->paymentCapturer:Lcom/squareup/payment/PaymentCapturer;

    invoke-virtual {v0, p2}, Lcom/squareup/payment/PaymentCapturer;->capture(Z)V

    .line 242
    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/BuyerWorkflow;->getReceiptScreen(Lcom/squareup/ui/buyer/BuyerScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1
    :try_end_0
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    .line 244
    :catch_0
    iget-object p2, p0, Lcom/squareup/ui/buyer/BuyerWorkflow;->res:Lcom/squareup/util/Res;

    invoke-static {p1, p2}, Lcom/squareup/ui/buyer/error/PaymentErrorScreen;->offlineError(Lcom/squareup/ui/buyer/BuyerScope;Lcom/squareup/util/Res;)Lcom/squareup/ui/buyer/error/PaymentErrorScreen;

    move-result-object p1

    return-object p1
.end method

.method doClaimCoupon(Lcom/squareup/ui/buyer/BuyerScope;Lcom/squareup/protos/client/coupons/Coupon;Z)Lcom/squareup/container/ContainerTreeKey;
    .locals 5

    .line 371
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requirePayment()Lcom/squareup/payment/Payment;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/buyer/BuyerWorkflow;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->DISCOUNT_APPLY_MULTIPLE_COUPONS:Lcom/squareup/settings/server/Features$Feature;

    .line 372
    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    .line 371
    invoke-virtual {v0, p2, v1}, Lcom/squareup/payment/Payment;->applyCoupon(Lcom/squareup/protos/client/coupons/Coupon;Z)Lcom/squareup/payment/Order;

    move-result-object p2

    if-eqz p3, :cond_0

    .line 375
    iget-object p3, p0, Lcom/squareup/ui/buyer/BuyerWorkflow;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_REWARDS:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    iget-object v1, p0, Lcom/squareup/ui/buyer/BuyerWorkflow;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/ui/buyerflow/R$string;->coupon_applied:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/ui/buyer/BuyerWorkflow;->res:Lcom/squareup/util/Res;

    sget v3, Lcom/squareup/ui/buyerflow/R$string;->amount_charged:I

    .line 376
    invoke-interface {v2, v3}, Lcom/squareup/util/Res;->phrase(I)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/ui/buyer/BuyerWorkflow;->moneyFormatter:Lcom/squareup/text/Formatter;

    .line 377
    invoke-virtual {p2}, Lcom/squareup/payment/Order;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v3

    const-string v4, "amount"

    invoke-virtual {v2, v4, v3}, Lcom/squareup/phrase/Phrase;->put(Ljava/lang/String;Ljava/lang/CharSequence;)Lcom/squareup/phrase/Phrase;

    move-result-object v2

    .line 378
    invoke-virtual {v2}, Lcom/squareup/phrase/Phrase;->format()Ljava/lang/CharSequence;

    move-result-object v2

    .line 375
    invoke-interface {p3, v0, v1, v2}, Lcom/squareup/hudtoaster/HudToaster;->toastLongHud(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 381
    :cond_0
    invoke-virtual {p2}, Lcom/squareup/payment/Order;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object p3

    invoke-static {p3}, Lcom/squareup/money/MoneyMath;->isPositive(Lcom/squareup/protos/common/Money;)Z

    move-result p3

    if-nez p3, :cond_2

    .line 383
    iget-object p3, p0, Lcom/squareup/ui/buyer/BuyerWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p3}, Lcom/squareup/payment/Transaction;->requirePayment()Lcom/squareup/payment/Payment;

    move-result-object p3

    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Lcom/squareup/payment/Payment;->setAvailableCoupon(Lcom/squareup/protos/client/coupons/Coupon;)V

    .line 386
    iget-object p3, p0, Lcom/squareup/ui/buyer/BuyerWorkflow;->settings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {p3}, Lcom/squareup/settings/server/AccountStatusSettings;->getPaymentSettings()Lcom/squareup/settings/server/PaymentSettings;

    move-result-object p3

    invoke-virtual {p3}, Lcom/squareup/settings/server/PaymentSettings;->canUseZeroAmountTender()Z

    move-result p3

    if-eqz p3, :cond_1

    .line 387
    iget-object p2, p0, Lcom/squareup/ui/buyer/BuyerWorkflow;->tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

    invoke-virtual {p2}, Lcom/squareup/payment/tender/TenderFactory;->createZeroToReplaceCard()Lcom/squareup/payment/tender/ZeroTender$Builder;

    move-result-object p2

    goto :goto_0

    .line 389
    :cond_1
    invoke-virtual {p2}, Lcom/squareup/payment/Order;->getAmountDue()Lcom/squareup/protos/common/Money;

    move-result-object p2

    .line 390
    iget-object p3, p0, Lcom/squareup/ui/buyer/BuyerWorkflow;->tenderFactory:Lcom/squareup/payment/tender/TenderFactory;

    invoke-virtual {p3, p2, p2}, Lcom/squareup/payment/tender/TenderFactory;->createCash(Lcom/squareup/protos/common/Money;Lcom/squareup/protos/common/Money;)Lcom/squareup/payment/tender/CashTender$Builder;

    move-result-object p2

    .line 392
    :goto_0
    iget-object p3, p0, Lcom/squareup/ui/buyer/BuyerWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {p3}, Lcom/squareup/payment/Transaction;->asBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object p3

    invoke-virtual {p3, p2}, Lcom/squareup/payment/BillPayment;->replaceCardTenderWithZeroTender(Lcom/squareup/payment/tender/BaseLocalTender$Builder;)V

    .line 393
    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/BuyerWorkflow;->getAuthSpinnerScreen(Lcom/squareup/ui/buyer/BuyerScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    return-object p1

    .line 395
    :cond_2
    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyer/BuyerWorkflow;->doFinishCouponScreen(Lcom/squareup/ui/buyer/BuyerScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    return-object p1
.end method

.method doFinishCouponScreen(Lcom/squareup/ui/buyer/BuyerScope;)Lcom/squareup/container/ContainerTreeKey;
    .locals 2

    .line 401
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requirePayment()Lcom/squareup/payment/Payment;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/payment/Payment;->setAvailableCoupon(Lcom/squareup/protos/client/coupons/Coupon;)V

    .line 402
    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/BuyerWorkflow;->getPostAuthCardScreen(Lcom/squareup/ui/buyer/BuyerScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    return-object p1
.end method

.method getFirstPostAuthMagStripeScreen(Lcom/squareup/ui/buyer/BuyerScope;)Lcom/squareup/container/ContainerTreeKey;
    .locals 2

    .line 155
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerWorkflow;->printerStations:Lcom/squareup/print/PrinterStations;

    invoke-interface {v0}, Lcom/squareup/print/PrinterStations;->hasEnabledKitchenPrintingStations()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    .line 156
    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->canStillNameCart()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    .line 157
    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->hasTicket()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerWorkflow;->printerStations:Lcom/squareup/print/PrinterStations;

    .line 158
    invoke-interface {v0}, Lcom/squareup/print/PrinterStations;->isTicketAutoNumberingEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 159
    iget-object v0, p1, Lcom/squareup/ui/buyer/BuyerScope;->pinRequest:Lcom/squareup/ui/buyer/ContactlessPinRequest;

    iget-boolean v0, v0, Lcom/squareup/ui/buyer/ContactlessPinRequest;->pinRequested:Z

    xor-int/lit8 v0, v0, 0x1

    const-string v1, "Should not have tapped a card before getting to BuyerOrderTicketNameScreen"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 161
    new-instance v0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameScreen;

    invoke-direct {v0, p1}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameScreen;-><init>(Lcom/squareup/ui/buyer/BuyerScope;)V

    return-object v0

    .line 164
    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyer/BuyerWorkflow;->getFirstPostAuthScreen(Lcom/squareup/ui/buyer/BuyerScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    return-object p1
.end method

.method getFirstPostAuthScreen(Lcom/squareup/ui/buyer/BuyerScope;)Lcom/squareup/container/ContainerTreeKey;
    .locals 1

    .line 168
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requirePayment()Lcom/squareup/payment/Payment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Payment;->isLocalPayment()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 169
    invoke-virtual {p0, p1, v0}, Lcom/squareup/ui/buyer/BuyerWorkflow;->doCaptureAndGetReceiptScreen(Lcom/squareup/ui/buyer/BuyerScope;Z)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    return-object p1

    .line 172
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->asInvoicePayment()Lcom/squareup/payment/InvoicePayment;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 173
    new-instance v0, Lcom/squareup/ui/invoices/InvoiceSentSavedScreen;

    invoke-direct {v0, p1}, Lcom/squareup/ui/invoices/InvoiceSentSavedScreen;-><init>(Lcom/squareup/ui/buyer/BuyerScope;)V

    return-object v0

    .line 177
    :cond_1
    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/BuyerWorkflow;->getPostAuthCardScreen(Lcom/squareup/ui/buyer/BuyerScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    return-object p1
.end method

.method public getFirstScreen(Lcom/squareup/ui/buyer/BuyerScope;Z)Lcom/squareup/container/ContainerTreeKey;
    .locals 1

    if-eqz p2, :cond_0

    .line 133
    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/BuyerWorkflow;->getAuthSpinnerScreen(Lcom/squareup/ui/buyer/BuyerScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    return-object p1

    .line 139
    :cond_0
    iget-object p2, p0, Lcom/squareup/ui/buyer/BuyerWorkflow;->printerStations:Lcom/squareup/print/PrinterStations;

    invoke-interface {p2}, Lcom/squareup/print/PrinterStations;->hasEnabledKitchenPrintingStations()Z

    move-result p2

    if-eqz p2, :cond_1

    iget-object p2, p0, Lcom/squareup/ui/buyer/BuyerWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    .line 140
    invoke-virtual {p2}, Lcom/squareup/payment/Transaction;->canStillNameCart()Z

    move-result p2

    if-eqz p2, :cond_1

    iget-object p2, p0, Lcom/squareup/ui/buyer/BuyerWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    .line 141
    invoke-virtual {p2}, Lcom/squareup/payment/Transaction;->hasTicket()Z

    move-result p2

    if-nez p2, :cond_1

    iget-object p2, p0, Lcom/squareup/ui/buyer/BuyerWorkflow;->printerStations:Lcom/squareup/print/PrinterStations;

    .line 142
    invoke-interface {p2}, Lcom/squareup/print/PrinterStations;->isTicketAutoNumberingEnabled()Z

    move-result p2

    if-nez p2, :cond_1

    .line 143
    iget-object p2, p1, Lcom/squareup/ui/buyer/BuyerScope;->pinRequest:Lcom/squareup/ui/buyer/ContactlessPinRequest;

    iget-boolean p2, p2, Lcom/squareup/ui/buyer/ContactlessPinRequest;->pinRequested:Z

    xor-int/lit8 p2, p2, 0x1

    const-string v0, "Should not have tapped a card before getting to BuyerOrderTicketNameScreen"

    invoke-static {p2, v0}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 145
    new-instance p2, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameScreen;

    invoke-direct {p2, p1}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameScreen;-><init>(Lcom/squareup/ui/buyer/BuyerScope;)V

    return-object p2

    .line 148
    :cond_1
    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyer/BuyerWorkflow;->getFirstScreenAfterTicketName(Lcom/squareup/ui/buyer/BuyerScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    return-object p1
.end method

.method getFirstScreenAfterAuthApproved(Lcom/squareup/ui/buyer/BuyerScope;)Lcom/squareup/container/ContainerTreeKey;
    .locals 3

    .line 269
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    .line 270
    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requireSignedPayment()Lcom/squareup/payment/AcceptsSignature;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/payment/AcceptsSignature;->signOnPrintedReceipt()Z

    move-result v0

    const/4 v1, 0x1

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    .line 271
    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requireTippingPayment()Lcom/squareup/payment/AcceptsTips;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/payment/AcceptsTips;->tipOnPrintedReceipt()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_2

    .line 272
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->isTakingPaymentFromInvoice()Z

    move-result v0

    if-nez v0, :cond_2

    .line 273
    invoke-virtual {p0, p1, v1}, Lcom/squareup/ui/buyer/BuyerWorkflow;->doCaptureAndGetReceiptScreen(Lcom/squareup/ui/buyer/BuyerScope;Z)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    return-object p1

    .line 276
    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerWorkflow;->tipDeterminerFactory:Lcom/squareup/payment/TipDeterminerFactory;

    invoke-virtual {v0}, Lcom/squareup/payment/TipDeterminerFactory;->createForCurrentTender()Lcom/squareup/payment/TipDeterminer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/TipDeterminer;->showPostAuthTipScreen()Z

    move-result v0

    .line 277
    iget-object v2, p0, Lcom/squareup/ui/buyer/BuyerWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v2}, Lcom/squareup/payment/Transaction;->requireSignedPayment()Lcom/squareup/payment/AcceptsSignature;

    move-result-object v2

    invoke-interface {v2}, Lcom/squareup/payment/AcceptsSignature;->askForSignature()Z

    move-result v2

    if-eqz v0, :cond_4

    if-eqz v2, :cond_4

    .line 280
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requireTippingPayment()Lcom/squareup/payment/AcceptsTips;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/payment/AcceptsTips;->useSeparateTippingScreen()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 281
    new-instance v0, Lcom/squareup/ui/buyer/tip/TipBootstrapScreen;

    invoke-direct {v0, p1}, Lcom/squareup/ui/buyer/tip/TipBootstrapScreen;-><init>(Lcom/squareup/ui/buyer/BuyerScope;)V

    return-object v0

    .line 283
    :cond_3
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerWorkflow;->features:Lcom/squareup/settings/server/Features;

    invoke-static {p1, v0}, Lcom/squareup/ui/buyer/signature/SignScreen;->getSignScreen(Lcom/squareup/ui/buyer/BuyerScope;Lcom/squareup/settings/server/Features;)Lcom/squareup/ui/buyer/signature/SignScreen;

    move-result-object p1

    return-object p1

    :cond_4
    if-eqz v0, :cond_5

    if-nez v2, :cond_5

    .line 287
    new-instance v0, Lcom/squareup/ui/buyer/tip/TipBootstrapScreen;

    invoke-direct {v0, p1}, Lcom/squareup/ui/buyer/tip/TipBootstrapScreen;-><init>(Lcom/squareup/ui/buyer/BuyerScope;)V

    return-object v0

    :cond_5
    if-nez v0, :cond_6

    if-eqz v2, :cond_6

    .line 291
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerWorkflow;->features:Lcom/squareup/settings/server/Features;

    invoke-static {p1, v0}, Lcom/squareup/ui/buyer/signature/SignScreen;->getSignScreen(Lcom/squareup/ui/buyer/BuyerScope;Lcom/squareup/settings/server/Features;)Lcom/squareup/ui/buyer/signature/SignScreen;

    move-result-object p1

    return-object p1

    .line 294
    :cond_6
    invoke-virtual {p0, p1, v1}, Lcom/squareup/ui/buyer/BuyerWorkflow;->doCaptureAndGetReceiptScreen(Lcom/squareup/ui/buyer/BuyerScope;Z)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    return-object p1
.end method

.method getFirstScreenAfterTicketName(Lcom/squareup/ui/buyer/BuyerScope;)Lcom/squareup/container/ContainerTreeKey;
    .locals 10

    .line 195
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerWorkflow;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {v0}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->getActiveCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v0

    .line 198
    iget-object v1, p0, Lcom/squareup/ui/buyer/BuyerWorkflow;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v1}, Lcom/squareup/payment/TenderInEdit;->isSmartCardTender()Z

    move-result v1

    if-nez v1, :cond_1

    .line 199
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerWorkflow;->tipDeterminerFactory:Lcom/squareup/payment/TipDeterminerFactory;

    invoke-virtual {v0}, Lcom/squareup/payment/TipDeterminerFactory;->createForCurrentTender()Lcom/squareup/payment/TipDeterminer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/TipDeterminer;->showPreAuthTipScreen()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 200
    new-instance v0, Lcom/squareup/ui/buyer/tip/TipBootstrapScreen;

    invoke-direct {v0, p1}, Lcom/squareup/ui/buyer/tip/TipBootstrapScreen;-><init>(Lcom/squareup/ui/buyer/BuyerScope;)V

    return-object v0

    .line 202
    :cond_0
    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyer/BuyerWorkflow;->getFirstPostAuthScreen(Lcom/squareup/ui/buyer/BuyerScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    return-object p1

    .line 206
    :cond_1
    iget-object v1, p0, Lcom/squareup/ui/buyer/BuyerWorkflow;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v1}, Lcom/squareup/payment/TenderInEdit;->requireSmartCardTender()Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->isContactless()Z

    move-result v1

    const/4 v2, 0x1

    if-eqz v1, :cond_3

    .line 207
    iget-object v3, p1, Lcom/squareup/ui/buyer/BuyerScope;->pinRequest:Lcom/squareup/ui/buyer/ContactlessPinRequest;

    iget-boolean v3, v3, Lcom/squareup/ui/buyer/ContactlessPinRequest;->pinRequested:Z

    if-eqz v3, :cond_3

    .line 208
    iget-object v3, p1, Lcom/squareup/ui/buyer/BuyerScope;->pinRequest:Lcom/squareup/ui/buyer/ContactlessPinRequest;

    .line 209
    iget-boolean v4, v3, Lcom/squareup/ui/buyer/ContactlessPinRequest;->pinRequested:Z

    if-eqz v4, :cond_3

    .line 213
    iget-object v4, p0, Lcom/squareup/ui/buyer/BuyerWorkflow;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v4}, Lcom/squareup/payment/TenderInEdit;->requireSmartCardTender()Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->hasAuthData()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 214
    new-instance v3, Lcom/squareup/ui/buyer/emv/EmvScope;

    invoke-direct {v3, p1, v0, v1}, Lcom/squareup/ui/buyer/emv/EmvScope;-><init>(Lcom/squareup/ui/buyer/BuyerScope;Lcom/squareup/cardreader/CardReaderId;Z)V

    .line 215
    new-instance p1, Lcom/squareup/ui/buyer/emv/authorizing/EmvAuthorizingScreen;

    invoke-direct {p1, v3, v2}, Lcom/squareup/ui/buyer/emv/authorizing/EmvAuthorizingScreen;-><init>(Lcom/squareup/ui/buyer/emv/EmvScope;Z)V

    return-object p1

    .line 217
    :cond_2
    new-instance v5, Lcom/squareup/ui/buyer/emv/EmvScope;

    invoke-direct {v5, p1, v0, v1}, Lcom/squareup/ui/buyer/emv/EmvScope;-><init>(Lcom/squareup/ui/buyer/BuyerScope;Lcom/squareup/cardreader/CardReaderId;Z)V

    .line 218
    new-instance p1, Lcom/squareup/ui/buyer/emv/pinpad/PinPadDialogScreen;

    iget-boolean v6, v3, Lcom/squareup/ui/buyer/ContactlessPinRequest;->canSkip:Z

    iget-object v7, v3, Lcom/squareup/ui/buyer/ContactlessPinRequest;->cardInfo:Lcom/squareup/cardreader/CardInfo;

    iget-boolean v8, v3, Lcom/squareup/ui/buyer/ContactlessPinRequest;->isFinal:Z

    iget-boolean v9, v3, Lcom/squareup/ui/buyer/ContactlessPinRequest;->isFinal:Z

    move-object v4, p1

    invoke-direct/range {v4 .. v9}, Lcom/squareup/ui/buyer/emv/pinpad/PinPadDialogScreen;-><init>(Lcom/squareup/ui/buyer/emv/EmvScope;ZLcom/squareup/cardreader/CardInfo;ZZ)V

    return-object p1

    .line 223
    :cond_3
    iget-object v3, p0, Lcom/squareup/ui/buyer/BuyerWorkflow;->tipDeterminerFactory:Lcom/squareup/payment/TipDeterminerFactory;

    invoke-virtual {v3}, Lcom/squareup/payment/TipDeterminerFactory;->createForCurrentTender()Lcom/squareup/payment/TipDeterminer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/payment/TipDeterminer;->showPreAuthTipScreen()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 224
    new-instance v0, Lcom/squareup/ui/buyer/tip/TipBootstrapScreen;

    invoke-direct {v0, p1}, Lcom/squareup/ui/buyer/tip/TipBootstrapScreen;-><init>(Lcom/squareup/ui/buyer/BuyerScope;)V

    return-object v0

    :cond_4
    if-eqz v1, :cond_5

    if-nez v0, :cond_5

    .line 229
    new-instance v0, Lcom/squareup/ui/buyer/emv/contactless/PayContactlessScreen;

    invoke-direct {v0, p1}, Lcom/squareup/ui/buyer/emv/contactless/PayContactlessScreen;-><init>(Lcom/squareup/ui/buyer/BuyerScope;)V

    return-object v0

    .line 232
    :cond_5
    new-instance v3, Lcom/squareup/ui/buyer/emv/EmvScope;

    invoke-direct {v3, p1, v0, v1}, Lcom/squareup/ui/buyer/emv/EmvScope;-><init>(Lcom/squareup/ui/buyer/BuyerScope;Lcom/squareup/cardreader/CardReaderId;Z)V

    if-eqz v1, :cond_6

    .line 234
    new-instance p1, Lcom/squareup/ui/buyer/emv/authorizing/EmvAuthorizingScreen;

    invoke-direct {p1, v3, v2}, Lcom/squareup/ui/buyer/emv/authorizing/EmvAuthorizingScreen;-><init>(Lcom/squareup/ui/buyer/emv/EmvScope;Z)V

    return-object p1

    .line 236
    :cond_6
    new-instance p1, Lcom/squareup/ui/buyer/emv/preparing/PreparingPaymentScreen;

    invoke-direct {p1, v3}, Lcom/squareup/ui/buyer/emv/preparing/PreparingPaymentScreen;-><init>(Lcom/squareup/ui/buyer/emv/EmvScope;)V

    return-object p1
.end method

.method getFirstScreenToStartEmvAuth(Lcom/squareup/ui/buyer/BuyerScope;)Lcom/squareup/container/ContainerTreeKey;
    .locals 3

    .line 181
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerWorkflow;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->isSmartCardTender()Z

    move-result v0

    const-string v1, "BuyerWorkflow::getFirstScreenToStartEmvAuth tenderInEdit is not a smart card tender"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 183
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerWorkflow;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->requireSmartCardTender()Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->isContactless()Z

    move-result v0

    .line 184
    new-instance v1, Lcom/squareup/ui/buyer/emv/EmvScope;

    iget-object v2, p0, Lcom/squareup/ui/buyer/BuyerWorkflow;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    invoke-virtual {v2}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->getActiveCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v2

    invoke-direct {v1, p1, v2, v0}, Lcom/squareup/ui/buyer/emv/EmvScope;-><init>(Lcom/squareup/ui/buyer/BuyerScope;Lcom/squareup/cardreader/CardReaderId;Z)V

    if-eqz v0, :cond_0

    .line 188
    new-instance p1, Lcom/squareup/ui/buyer/emv/authorizing/EmvAuthorizingScreen;

    const/4 v0, 0x1

    invoke-direct {p1, v1, v0}, Lcom/squareup/ui/buyer/emv/authorizing/EmvAuthorizingScreen;-><init>(Lcom/squareup/ui/buyer/emv/EmvScope;Z)V

    return-object p1

    .line 190
    :cond_0
    new-instance p1, Lcom/squareup/ui/buyer/emv/preparing/PreparingPaymentScreen;

    invoke-direct {p1, v1}, Lcom/squareup/ui/buyer/emv/preparing/PreparingPaymentScreen;-><init>(Lcom/squareup/ui/buyer/emv/EmvScope;)V

    return-object p1
.end method

.method public getPinPadDialog(Lcom/squareup/ui/buyer/BuyerScope;)Lcom/squareup/container/ContainerTreeKey;
    .locals 7

    .line 119
    iget-object v0, p1, Lcom/squareup/ui/buyer/BuyerScope;->pinRequest:Lcom/squareup/ui/buyer/ContactlessPinRequest;

    iget-boolean v0, v0, Lcom/squareup/ui/buyer/ContactlessPinRequest;->pinRequested:Z

    const-string v1, "Cannot make PIN dialog without a PIN request"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 120
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerWorkflow;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->requireSmartCardTender()Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->hasAuthData()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    const-string v1, "BuyerWorkflow::getPinPadDialog tenderInEdit smart card tender has auth data"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 123
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerWorkflow;->tenderInEdit:Lcom/squareup/payment/TenderInEdit;

    invoke-interface {v0}, Lcom/squareup/payment/TenderInEdit;->requireSmartCardTender()Lcom/squareup/payment/tender/SmartCardTenderBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/tender/SmartCardTenderBuilder;->isContactless()Z

    move-result v0

    .line 124
    new-instance v2, Lcom/squareup/ui/buyer/emv/EmvScope;

    iget-object v1, p0, Lcom/squareup/ui/buyer/BuyerWorkflow;->activeCardReader:Lcom/squareup/cardreader/dipper/ActiveCardReader;

    .line 125
    invoke-virtual {v1}, Lcom/squareup/cardreader/dipper/ActiveCardReader;->getActiveCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v1

    invoke-direct {v2, p1, v1, v0}, Lcom/squareup/ui/buyer/emv/EmvScope;-><init>(Lcom/squareup/ui/buyer/BuyerScope;Lcom/squareup/cardreader/CardReaderId;Z)V

    .line 127
    new-instance v0, Lcom/squareup/ui/buyer/emv/pinpad/PinPadDialogScreen;

    iget-object v1, p1, Lcom/squareup/ui/buyer/BuyerScope;->pinRequest:Lcom/squareup/ui/buyer/ContactlessPinRequest;

    iget-boolean v3, v1, Lcom/squareup/ui/buyer/ContactlessPinRequest;->canSkip:Z

    iget-object v1, p1, Lcom/squareup/ui/buyer/BuyerScope;->pinRequest:Lcom/squareup/ui/buyer/ContactlessPinRequest;

    iget-object v4, v1, Lcom/squareup/ui/buyer/ContactlessPinRequest;->cardInfo:Lcom/squareup/cardreader/CardInfo;

    iget-object v1, p1, Lcom/squareup/ui/buyer/BuyerScope;->pinRequest:Lcom/squareup/ui/buyer/ContactlessPinRequest;

    iget-boolean v5, v1, Lcom/squareup/ui/buyer/ContactlessPinRequest;->isFinal:Z

    iget-object p1, p1, Lcom/squareup/ui/buyer/BuyerScope;->pinRequest:Lcom/squareup/ui/buyer/ContactlessPinRequest;

    iget-boolean v6, p1, Lcom/squareup/ui/buyer/ContactlessPinRequest;->isFinal:Z

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/squareup/ui/buyer/emv/pinpad/PinPadDialogScreen;-><init>(Lcom/squareup/ui/buyer/emv/EmvScope;ZLcom/squareup/cardreader/CardInfo;ZZ)V

    return-object v0
.end method

.method getPostPartialAuthWarningScreen(Lcom/squareup/ui/buyer/BuyerScope;)Lcom/squareup/container/ContainerTreeKey;
    .locals 2

    .line 257
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requirePayment()Lcom/squareup/payment/Payment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Payment;->getAvailableCoupon()Lcom/squareup/protos/client/coupons/Coupon;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 258
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerWorkflow;->skipReceiptScreenSettings:Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;

    invoke-interface {v0}, Lcom/squareup/ui/settings/signatureAndReceipt/SkipReceiptScreenSettings;->skipReceiptScreenForFastCheckout()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 259
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerWorkflow;->transaction:Lcom/squareup/payment/Transaction;

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requirePayment()Lcom/squareup/payment/Payment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Payment;->getAvailableCoupon()Lcom/squareup/protos/client/coupons/Coupon;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, p1, v0, v1}, Lcom/squareup/ui/buyer/BuyerWorkflow;->doClaimCoupon(Lcom/squareup/ui/buyer/BuyerScope;Lcom/squareup/protos/client/coupons/Coupon;Z)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    return-object p1

    .line 261
    :cond_0
    new-instance v0, Lcom/squareup/ui/buyer/coupon/PostAuthCouponScreen;

    invoke-direct {v0, p1}, Lcom/squareup/ui/buyer/coupon/PostAuthCouponScreen;-><init>(Lcom/squareup/ui/buyer/BuyerScope;)V

    return-object v0

    .line 265
    :cond_1
    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyer/BuyerWorkflow;->getFirstScreenAfterAuthApproved(Lcom/squareup/ui/buyer/BuyerScope;)Lcom/squareup/container/ContainerTreeKey;

    move-result-object p1

    return-object p1
.end method
