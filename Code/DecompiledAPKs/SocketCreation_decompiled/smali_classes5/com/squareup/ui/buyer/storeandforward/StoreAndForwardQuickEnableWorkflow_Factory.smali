.class public final Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow_Factory;
.super Ljava/lang/Object;
.source "StoreAndForwardQuickEnableWorkflow_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/StoreAndForwardAnalytics;",
            ">;"
        }
    .end annotation
.end field

.field private final offlineModeMonitorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final permissionGatekeeperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/StoreAndForwardAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;)V"
        }
    .end annotation

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow_Factory;->analyticsProvider:Ljavax/inject/Provider;

    .line 34
    iput-object p2, p0, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow_Factory;->settingsProvider:Ljavax/inject/Provider;

    .line 35
    iput-object p3, p0, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow_Factory;->offlineModeMonitorProvider:Ljavax/inject/Provider;

    .line 36
    iput-object p4, p0, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/StoreAndForwardAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/PermissionGatekeeper;",
            ">;)",
            "Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow_Factory;"
        }
    .end annotation

    .line 49
    new-instance v0, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow_Factory;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/analytics/StoreAndForwardAnalytics;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/permissions/PermissionGatekeeper;)Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow;
    .locals 1

    .line 55
    new-instance v0, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow;-><init>(Lcom/squareup/analytics/StoreAndForwardAnalytics;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/permissions/PermissionGatekeeper;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow;
    .locals 4

    .line 41
    iget-object v0, p0, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/analytics/StoreAndForwardAnalytics;

    iget-object v1, p0, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v2, p0, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow_Factory;->offlineModeMonitorProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/payment/OfflineModeMonitor;

    iget-object v3, p0, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow_Factory;->permissionGatekeeperProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/permissions/PermissionGatekeeper;

    invoke-static {v0, v1, v2, v3}, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow_Factory;->newInstance(Lcom/squareup/analytics/StoreAndForwardAnalytics;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/permissions/PermissionGatekeeper;)Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 11
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow_Factory;->get()Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow;

    move-result-object v0

    return-object v0
.end method
