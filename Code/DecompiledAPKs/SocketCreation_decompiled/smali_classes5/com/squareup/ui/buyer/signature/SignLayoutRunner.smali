.class public final Lcom/squareup/ui/buyer/signature/SignLayoutRunner;
.super Ljava/lang/Object;
.source "SignLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/workflow/ui/LayoutRunner;
.implements Lcom/squareup/signature/SignatureRenderer$BitmapProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/buyer/signature/SignLayoutRunner$Factory;,
        Lcom/squareup/ui/buyer/signature/SignLayoutRunner$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/squareup/workflow/ui/LayoutRunner<",
        "Lcom/squareup/ui/buyer/signature/SignScreenData;",
        ">;",
        "Lcom/squareup/signature/SignatureRenderer$BitmapProvider;"
    }
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nSignLayoutRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 SignLayoutRunner.kt\ncom/squareup/ui/buyer/signature/SignLayoutRunner\n*L\n1#1,468:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u00b8\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u0000 @2\u0008\u0012\u0004\u0012\u00020\u00020\u00012\u00020\u0003:\u0002@AB5\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u00a2\u0006\u0002\u0010\u0010J\u0018\u0010\'\u001a\u00020(2\u0006\u0010)\u001a\u00020*2\u0006\u0010+\u001a\u00020*H\u0016J\u0008\u0010,\u001a\u00020-H\u0002J\u0008\u0010.\u001a\u00020-H\u0002J\u0010\u0010/\u001a\u00020-2\u0006\u00100\u001a\u000201H\u0002J\u0018\u00102\u001a\u00020-2\u0006\u00100\u001a\u0002032\u0006\u00104\u001a\u000205H\u0002J\u0018\u00106\u001a\u00020-2\u0006\u00100\u001a\u0002072\u0006\u00104\u001a\u000205H\u0002J\u0018\u00108\u001a\u00020-2\u0006\u00109\u001a\u00020:2\u0006\u00104\u001a\u000205H\u0002J\u0008\u0010;\u001a\u00020-H\u0002J\u0018\u0010<\u001a\u00020-2\u0006\u00100\u001a\u00020\u00022\u0006\u0010=\u001a\u00020>H\u0016J\u0008\u0010?\u001a\u00020-H\u0002R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u001aX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u0016X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u001dX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001e\u001a\u00020\u001fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010 \u001a\u00020!X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\"\u001a\u00020#X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010$\u001a\u00020\u001aX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010%\u001a\u00020&X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006B"
    }
    d2 = {
        "Lcom/squareup/ui/buyer/signature/SignLayoutRunner;",
        "Lcom/squareup/workflow/ui/LayoutRunner;",
        "Lcom/squareup/ui/buyer/signature/SignScreenData;",
        "Lcom/squareup/signature/SignatureRenderer$BitmapProvider;",
        "view",
        "Landroid/view/View;",
        "picassoCache",
        "Lcom/squareup/picasso/Cache;",
        "ohSnapLogger",
        "Lcom/squareup/log/OhSnapLogger;",
        "agreementBuilder",
        "Lcom/squareup/ui/buyer/signature/AgreementBuilder;",
        "moneyLocaleHelper",
        "Lcom/squareup/money/MoneyLocaleHelper;",
        "currencyCode",
        "Lcom/squareup/protos/common/CurrencyCode;",
        "(Landroid/view/View;Lcom/squareup/picasso/Cache;Lcom/squareup/log/OhSnapLogger;Lcom/squareup/ui/buyer/signature/AgreementBuilder;Lcom/squareup/money/MoneyLocaleHelper;Lcom/squareup/protos/common/CurrencyCode;)V",
        "agreement",
        "Lcom/squareup/widgets/MessageView;",
        "buyerActionBar",
        "Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;",
        "clearButton",
        "Lcom/squareup/marketfont/MarketButton;",
        "customTip",
        "Lcom/squareup/ui/XableEditText;",
        "customTipIsClearable",
        "",
        "doneButton",
        "signHereView",
        "Landroid/widget/TextView;",
        "signatureView",
        "Lcom/squareup/signature/SignatureView;",
        "tipAnimator",
        "Lcom/squareup/widgets/PersistentViewAnimator;",
        "tipBar",
        "Lcom/squareup/widgets/CheckableGroup;",
        "tipInflated",
        "xButton",
        "Lcom/squareup/glyph/SquareGlyphView;",
        "createSignatureBitmap",
        "Landroid/graphics/Bitmap;",
        "width",
        "",
        "height",
        "fadeInButtons",
        "",
        "fadeOutButtons",
        "renderActionBar",
        "rendering",
        "Lcom/squareup/ui/buyer/signature/SignScreenData$HeaderInformation;",
        "renderAgreement",
        "Lcom/squareup/ui/buyer/signature/SignScreenData$ReturnPolicy;",
        "res",
        "Lcom/squareup/util/Res;",
        "renderSignatureView",
        "Lcom/squareup/ui/buyer/signature/SignScreenData$SignatureConfig;",
        "renderTipBar",
        "tipConfig",
        "Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;",
        "showCustomTip",
        "showRendering",
        "containerHints",
        "Lcom/squareup/workflow/ui/ContainerHints;",
        "showTipBar",
        "Companion",
        "Factory",
        "buyer-flow_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ui/buyer/signature/SignLayoutRunner$Companion;

.field private static final FADE_DURATION_MS:I = 0xc8

.field private static final TIP_OPTION_ID:I = 0x19a


# instance fields
.field private final agreement:Lcom/squareup/widgets/MessageView;

.field private final agreementBuilder:Lcom/squareup/ui/buyer/signature/AgreementBuilder;

.field private final buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

.field private final clearButton:Lcom/squareup/marketfont/MarketButton;

.field private final currencyCode:Lcom/squareup/protos/common/CurrencyCode;

.field private final customTip:Lcom/squareup/ui/XableEditText;

.field private customTipIsClearable:Z

.field private final doneButton:Lcom/squareup/marketfont/MarketButton;

.field private final moneyLocaleHelper:Lcom/squareup/money/MoneyLocaleHelper;

.field private final ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

.field private final picassoCache:Lcom/squareup/picasso/Cache;

.field private final signHereView:Landroid/widget/TextView;

.field private final signatureView:Lcom/squareup/signature/SignatureView;

.field private final tipAnimator:Lcom/squareup/widgets/PersistentViewAnimator;

.field private final tipBar:Lcom/squareup/widgets/CheckableGroup;

.field private tipInflated:Z

.field private final view:Landroid/view/View;

.field private final xButton:Lcom/squareup/glyph/SquareGlyphView;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->Companion:Lcom/squareup/ui/buyer/signature/SignLayoutRunner$Companion;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;Lcom/squareup/picasso/Cache;Lcom/squareup/log/OhSnapLogger;Lcom/squareup/ui/buyer/signature/AgreementBuilder;Lcom/squareup/money/MoneyLocaleHelper;Lcom/squareup/protos/common/CurrencyCode;)V
    .locals 1

    const-string/jumbo v0, "view"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "picassoCache"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "ohSnapLogger"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "agreementBuilder"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "moneyLocaleHelper"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "currencyCode"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->view:Landroid/view/View;

    iput-object p2, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->picassoCache:Lcom/squareup/picasso/Cache;

    iput-object p3, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    iput-object p4, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->agreementBuilder:Lcom/squareup/ui/buyer/signature/AgreementBuilder;

    iput-object p5, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->moneyLocaleHelper:Lcom/squareup/money/MoneyLocaleHelper;

    iput-object p6, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    .line 80
    iget-object p1, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/ui/buyerflow/R$id;->please_sign_here:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->signHereView:Landroid/widget/TextView;

    .line 81
    iget-object p1, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/ui/buyerflow/R$id;->tip_bar:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/CheckableGroup;

    iput-object p1, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->tipBar:Lcom/squareup/widgets/CheckableGroup;

    .line 82
    iget-object p1, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/ui/buyerflow/R$id;->tip_animator:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/PersistentViewAnimator;

    iput-object p1, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->tipAnimator:Lcom/squareup/widgets/PersistentViewAnimator;

    .line 83
    iget-object p1, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/ui/buyerflow/R$id;->custom_tip:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/XableEditText;

    iput-object p1, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->customTip:Lcom/squareup/ui/XableEditText;

    .line 84
    iget-object p1, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/widgets/pos/R$id;->x_button:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/glyph/SquareGlyphView;

    iput-object p1, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->xButton:Lcom/squareup/glyph/SquareGlyphView;

    .line 85
    iget-object p1, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/ui/buyerflow/R$id;->signature_canvas:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/signature/SignatureView;

    iput-object p1, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->signatureView:Lcom/squareup/signature/SignatureView;

    .line 86
    iget-object p1, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/ui/buyerflow/R$id;->sign_clear_button:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marketfont/MarketButton;

    iput-object p1, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->clearButton:Lcom/squareup/marketfont/MarketButton;

    .line 87
    iget-object p1, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/ui/buyerflow/R$id;->sign_done_button:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/marketfont/MarketButton;

    iput-object p1, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->doneButton:Lcom/squareup/marketfont/MarketButton;

    .line 88
    iget-object p1, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/ui/buyerflow/R$id;->buyer_action_bar:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    iput-object p1, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    .line 89
    iget-object p1, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->view:Landroid/view/View;

    sget p2, Lcom/squareup/ui/buyerflow/R$id;->agreement_label:I

    invoke-static {p1, p2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/MessageView;

    iput-object p1, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->agreement:Lcom/squareup/widgets/MessageView;

    return-void
.end method

.method public static final synthetic access$fadeInButtons(Lcom/squareup/ui/buyer/signature/SignLayoutRunner;)V
    .locals 0

    .line 51
    invoke-direct {p0}, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->fadeInButtons()V

    return-void
.end method

.method public static final synthetic access$fadeOutButtons(Lcom/squareup/ui/buyer/signature/SignLayoutRunner;)V
    .locals 0

    .line 51
    invoke-direct {p0}, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->fadeOutButtons()V

    return-void
.end method

.method public static final synthetic access$getCustomTip$p(Lcom/squareup/ui/buyer/signature/SignLayoutRunner;)Lcom/squareup/ui/XableEditText;
    .locals 0

    .line 51
    iget-object p0, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->customTip:Lcom/squareup/ui/XableEditText;

    return-object p0
.end method

.method public static final synthetic access$getCustomTipIsClearable$p(Lcom/squareup/ui/buyer/signature/SignLayoutRunner;)Z
    .locals 0

    .line 51
    iget-boolean p0, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->customTipIsClearable:Z

    return p0
.end method

.method public static final synthetic access$getSignHereView$p(Lcom/squareup/ui/buyer/signature/SignLayoutRunner;)Landroid/widget/TextView;
    .locals 0

    .line 51
    iget-object p0, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->signHereView:Landroid/widget/TextView;

    return-object p0
.end method

.method public static final synthetic access$getSignatureView$p(Lcom/squareup/ui/buyer/signature/SignLayoutRunner;)Lcom/squareup/signature/SignatureView;
    .locals 0

    .line 51
    iget-object p0, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->signatureView:Lcom/squareup/signature/SignatureView;

    return-object p0
.end method

.method public static final synthetic access$getTipAnimator$p(Lcom/squareup/ui/buyer/signature/SignLayoutRunner;)Lcom/squareup/widgets/PersistentViewAnimator;
    .locals 0

    .line 51
    iget-object p0, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->tipAnimator:Lcom/squareup/widgets/PersistentViewAnimator;

    return-object p0
.end method

.method public static final synthetic access$getXButton$p(Lcom/squareup/ui/buyer/signature/SignLayoutRunner;)Lcom/squareup/glyph/SquareGlyphView;
    .locals 0

    .line 51
    iget-object p0, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->xButton:Lcom/squareup/glyph/SquareGlyphView;

    return-object p0
.end method

.method public static final synthetic access$setCustomTipIsClearable$p(Lcom/squareup/ui/buyer/signature/SignLayoutRunner;Z)V
    .locals 0

    .line 51
    iput-boolean p1, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->customTipIsClearable:Z

    return-void
.end method

.method public static final synthetic access$showCustomTip(Lcom/squareup/ui/buyer/signature/SignLayoutRunner;)V
    .locals 0

    .line 51
    invoke-direct {p0}, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->showCustomTip()V

    return-void
.end method

.method public static final synthetic access$showTipBar(Lcom/squareup/ui/buyer/signature/SignLayoutRunner;)V
    .locals 0

    .line 51
    invoke-direct {p0}, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->showTipBar()V

    return-void
.end method

.method private final fadeInButtons()V
    .locals 5

    .line 377
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    .line 378
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-string v2, "buyerActionBar.animate()\n        .alpha(1f)"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v2, 0xc8

    int-to-long v2, v2

    .line 379
    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 380
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->tipBar:Lcom/squareup/widgets/CheckableGroup;

    invoke-virtual {v0}, Lcom/squareup/widgets/CheckableGroup;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 381
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-string/jumbo v4, "tipBar.animate()\n        .alpha(1f)"

    invoke-static {v0, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 382
    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 383
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->agreement:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0}, Lcom/squareup/widgets/MessageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 384
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-string v1, "agreement.animate()\n        .alpha(1f)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 385
    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    return-void
.end method

.method private final fadeOutButtons()V
    .locals 5

    .line 365
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f000000    # 0.5f

    .line 366
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-string v2, "buyerActionBar.animate()\n        .alpha(.5f)"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const/16 v2, 0xc8

    int-to-long v2, v2

    .line 367
    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 368
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->tipBar:Lcom/squareup/widgets/CheckableGroup;

    invoke-virtual {v0}, Lcom/squareup/widgets/CheckableGroup;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 369
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-string/jumbo v4, "tipBar.animate()\n        .alpha(.5f)"

    invoke-static {v0, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 370
    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 371
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->agreement:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v0}, Lcom/squareup/widgets/MessageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 372
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-string v1, "agreement.animate()\n        .alpha(.5f)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 373
    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    return-void
.end method

.method private final renderActionBar(Lcom/squareup/ui/buyer/signature/SignScreenData$HeaderInformation;)V
    .locals 5

    .line 111
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->view:Landroid/view/View;

    new-instance v1, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderActionBar$1;

    invoke-direct {v1, p1}, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderActionBar$1;-><init>(Lcom/squareup/ui/buyer/signature/SignScreenData$HeaderInformation;)V

    check-cast v1, Lkotlin/jvm/functions/Function0;

    invoke-static {v0, v1}, Lcom/squareup/workflow/ui/HandlesBackKt;->setBackHandler(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    .line 115
    invoke-virtual {p1}, Lcom/squareup/ui/buyer/signature/SignScreenData$HeaderInformation;->isRetreatable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->BACK_ARROW:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    new-instance v2, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderActionBar$2;

    invoke-direct {v2, p1}, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderActionBar$2;-><init>(Lcom/squareup/ui/buyer/signature/SignScreenData$HeaderInformation;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setupUpGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;Lkotlin/jvm/functions/Function0;)V

    goto :goto_0

    .line 120
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    new-instance v2, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderActionBar$3;

    invoke-direct {v2, p1}, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderActionBar$3;-><init>(Lcom/squareup/ui/buyer/signature/SignScreenData$HeaderInformation;)V

    check-cast v2, Lkotlin/jvm/functions/Function0;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setupUpGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;Lkotlin/jvm/functions/Function0;)V

    .line 125
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/ui/buyer/signature/SignScreenData$HeaderInformation;->getTitle()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-lez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    :goto_1
    if-eqz v0, :cond_2

    .line 126
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    new-instance v3, Lcom/squareup/util/ViewString$TextString;

    invoke-virtual {p1}, Lcom/squareup/ui/buyer/signature/SignScreenData$HeaderInformation;->getTitle()Ljava/lang/String;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    invoke-direct {v3, v4}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    check-cast v3, Lcom/squareup/util/ViewString;

    invoke-virtual {v0, v3}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setTitle(Lcom/squareup/util/ViewString;)V

    .line 129
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/ui/buyer/signature/SignScreenData$HeaderInformation;->getSubtitle()Ljava/lang/String;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_3

    goto :goto_2

    :cond_3
    const/4 v1, 0x0

    :goto_2
    if-eqz v1, :cond_4

    .line 130
    new-instance v0, Lcom/squareup/util/ViewString$TextString;

    invoke-virtual {p1}, Lcom/squareup/ui/buyer/signature/SignScreenData$HeaderInformation;->getSubtitle()Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    invoke-direct {v0, p1}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    goto :goto_3

    :cond_4
    const/4 v0, 0x0

    .line 134
    :goto_3
    iget-object p1, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    check-cast v0, Lcom/squareup/util/ViewString;

    invoke-virtual {p1, v0}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setSubtitle(Lcom/squareup/util/ViewString;)V

    return-void
.end method

.method private final renderAgreement(Lcom/squareup/ui/buyer/signature/SignScreenData$ReturnPolicy;Lcom/squareup/util/Res;)V
    .locals 4

    .line 200
    invoke-virtual {p1}, Lcom/squareup/ui/buyer/signature/SignScreenData$ReturnPolicy;->getShowReturnPolicy()Lkotlin/jvm/functions/Function0;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 201
    new-instance v1, Lcom/squareup/ui/LinkSpan$Builder;

    iget-object v2, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->agreement:Lcom/squareup/widgets/MessageView;

    invoke-virtual {v2}, Lcom/squareup/widgets/MessageView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    .line 202
    sget v2, Lcom/squareup/ui/buyerflow/R$string;->buyer_view_refund_policy:I

    invoke-virtual {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    .line 203
    new-instance v2, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderAgreement$$inlined$let$lambda$1;

    sget v3, Lcom/squareup/ui/LinkSpan;->DEFAULT_COLOR_ID:I

    invoke-interface {p2, v3}, Lcom/squareup/util/Res;->getColor(I)I

    move-result v3

    invoke-direct {v2, v0, v3, p0, p2}, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderAgreement$$inlined$let$lambda$1;-><init>(Lkotlin/jvm/functions/Function0;ILcom/squareup/ui/buyer/signature/SignLayoutRunner;Lcom/squareup/util/Res;)V

    check-cast v2, Lcom/squareup/ui/LinkSpan;

    invoke-virtual {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->clickableSpan(Lcom/squareup/ui/LinkSpan;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v0

    .line 208
    invoke-virtual {v0}, Lcom/squareup/ui/LinkSpan$Builder;->asSpannable()Landroid/text/Spannable;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 210
    :goto_0
    invoke-virtual {p1}, Lcom/squareup/ui/buyer/signature/SignScreenData$ReturnPolicy;->getCardPayment()Lcom/squareup/payment/RequiresCardAgreement;

    move-result-object p1

    .line 213
    iget-object v1, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->agreementBuilder:Lcom/squareup/ui/buyer/signature/AgreementBuilder;

    .line 214
    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, p1, v0, p2}, Lcom/squareup/ui/buyer/signature/AgreementBuilder;->buildCardPaymentAgreementForDevice(Lcom/squareup/payment/RequiresCardAgreement;Ljava/lang/CharSequence;Lcom/squareup/util/Res;)Ljava/lang/CharSequence;

    move-result-object p1

    .line 216
    iget-object p2, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->agreement:Lcom/squareup/widgets/MessageView;

    invoke-virtual {p2, p1}, Lcom/squareup/widgets/MessageView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private final renderSignatureView(Lcom/squareup/ui/buyer/signature/SignScreenData$SignatureConfig;Lcom/squareup/util/Res;)V
    .locals 4

    .line 141
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->clearButton:Lcom/squareup/marketfont/MarketButton;

    invoke-virtual {p1}, Lcom/squareup/ui/buyer/signature/SignScreenData$SignatureConfig;->getClearSignature()Lkotlin/jvm/functions/Function0;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketButton;->setEnabled(Z)V

    .line 142
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->clearButton:Lcom/squareup/marketfont/MarketButton;

    sget v1, Lcom/squareup/ui/buyerflow/R$string;->buyer_clear_signature:I

    invoke-interface {p2, v1}, Lcom/squareup/util/Res;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketButton;->setText(Ljava/lang/CharSequence;)V

    .line 143
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->clearButton:Lcom/squareup/marketfont/MarketButton;

    new-instance v1, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderSignatureView$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderSignatureView$1;-><init>(Lcom/squareup/ui/buyer/signature/SignLayoutRunner;)V

    check-cast v1, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 149
    invoke-virtual {p1}, Lcom/squareup/ui/buyer/signature/SignScreenData$SignatureConfig;->getSubmitSignature()Lkotlin/jvm/functions/Function0;

    move-result-object v0

    .line 150
    iget-object v1, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->doneButton:Lcom/squareup/marketfont/MarketButton;

    if-eqz v0, :cond_1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    invoke-virtual {v1, v2}, Lcom/squareup/marketfont/MarketButton;->setEnabled(Z)V

    .line 151
    iget-object v1, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->doneButton:Lcom/squareup/marketfont/MarketButton;

    sget v2, Lcom/squareup/ui/buyerflow/R$string;->buyer_done_label:I

    invoke-interface {p2, v2}, Lcom/squareup/util/Res;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/marketfont/MarketButton;->setText(Ljava/lang/CharSequence;)V

    if-eqz v0, :cond_2

    .line 153
    iget-object v1, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->doneButton:Lcom/squareup/marketfont/MarketButton;

    new-instance v2, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderSignatureView$2;

    invoke-direct {v2, p0, v0}, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderSignatureView$2;-><init>(Lcom/squareup/ui/buyer/signature/SignLayoutRunner;Lkotlin/jvm/functions/Function0;)V

    check-cast v2, Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Lcom/squareup/marketfont/MarketButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 161
    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->signHereView:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/squareup/ui/buyer/signature/SignScreenData$SignatureConfig;->getUseManualSignText()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 162
    sget v1, Lcom/squareup/ui/buyerflow/R$string;->buyer_please_sign_here_manual:I

    invoke-interface {p2, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    goto :goto_2

    .line 164
    :cond_3
    sget v1, Lcom/squareup/ui/buyerflow/R$string;->buyer_please_sign_here:I

    invoke-interface {p2, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p2

    check-cast p2, Ljava/lang/CharSequence;

    .line 161
    :goto_2
    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 167
    iget-object p2, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->signatureView:Lcom/squareup/signature/SignatureView;

    new-instance v0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderSignatureView$3;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderSignatureView$3;-><init>(Lcom/squareup/ui/buyer/signature/SignLayoutRunner;Lcom/squareup/ui/buyer/signature/SignScreenData$SignatureConfig;)V

    check-cast v0, Lcom/squareup/signature/SignatureView$SignatureStateListener;

    invoke-virtual {p2, v0}, Lcom/squareup/signature/SignatureView;->setListener(Lcom/squareup/signature/SignatureView$SignatureStateListener;)V

    .line 192
    iget-object p1, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->signatureView:Lcom/squareup/signature/SignatureView;

    move-object p2, p0

    check-cast p2, Lcom/squareup/signature/SignatureRenderer$BitmapProvider;

    invoke-virtual {p1, p2}, Lcom/squareup/signature/SignatureView;->setBitmapProvider(Lcom/squareup/signature/SignatureRenderer$BitmapProvider;)V

    .line 193
    iget-object p1, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->signatureView:Lcom/squareup/signature/SignatureView;

    const/high16 p2, -0x1000000

    invoke-virtual {p1, p2}, Lcom/squareup/signature/SignatureView;->setSignatureColor(I)V

    return-void
.end method

.method private final renderTipBar(Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;Lcom/squareup/util/Res;)V
    .locals 9

    .line 230
    iget-boolean v0, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->tipInflated:Z

    if-nez v0, :cond_9

    .line 232
    invoke-virtual {p1}, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->getTipInfo()Ljava/util/List;

    const/4 v0, 0x1

    .line 233
    iput-boolean v0, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->tipInflated:Z

    .line 235
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->tipBar:Lcom/squareup/widgets/CheckableGroup;

    new-instance v1, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderTipBar$$inlined$let$lambda$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderTipBar$$inlined$let$lambda$1;-><init>(Lcom/squareup/ui/buyer/signature/SignLayoutRunner;Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;Lcom/squareup/util/Res;)V

    check-cast v1, Lcom/squareup/widgets/CheckableGroup$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/CheckableGroup;->setOnCheckedChangeListener(Lcom/squareup/widgets/CheckableGroup$OnCheckedChangeListener;)V

    .line 256
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->tipBar:Lcom/squareup/widgets/CheckableGroup;

    invoke-virtual {v0}, Lcom/squareup/widgets/CheckableGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 258
    sget v1, Lcom/squareup/ui/buyerflow/R$layout;->signature_scaling_tip_option_item:I

    iget-object v2, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->tipBar:Lcom/squareup/widgets/CheckableGroup;

    check-cast v2, Landroid/view/ViewGroup;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const-string v2, "null cannot be cast to non-null type android.widget.RadioButton"

    if-eqz v1, :cond_8

    check-cast v1, Landroid/widget/RadioButton;

    .line 259
    iget-object v4, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->tipAnimator:Lcom/squareup/widgets/PersistentViewAnimator;

    invoke-virtual {v4, v3}, Lcom/squareup/widgets/PersistentViewAnimator;->setVisibility(I)V

    .line 260
    invoke-virtual {p1}, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->getHasAutoGratuity()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 261
    sget v4, Lcom/squareup/checkout/R$string;->buyer_tip_no_additional_tip:I

    .line 260
    invoke-interface {p2, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    goto :goto_0

    .line 262
    :cond_0
    sget v4, Lcom/squareup/checkout/R$string;->buyer_tip_no_tip:I

    invoke-interface {p2, v4}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    .line 260
    :goto_0
    invoke-virtual {v1, v4}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 263
    sget v4, Lcom/squareup/ui/buyerflow/R$id;->no_tip_button:I

    invoke-virtual {v1, v4}, Landroid/widget/RadioButton;->setId(I)V

    .line 264
    iget-object v4, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->tipBar:Lcom/squareup/widgets/CheckableGroup;

    move-object v5, v1

    check-cast v5, Landroid/view/View;

    invoke-virtual {v4, v5, v3}, Lcom/squareup/widgets/CheckableGroup;->addView(Landroid/view/View;I)V

    .line 265
    iget-object v4, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->tipBar:Lcom/squareup/widgets/CheckableGroup;

    sget v5, Lcom/squareup/ui/buyerflow/R$id;->no_tip_button:I

    invoke-virtual {v4, v5}, Lcom/squareup/widgets/CheckableGroup;->check(I)V

    .line 267
    invoke-virtual {p1}, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->getTipInfo()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/squareup/ui/buyer/signature/SignScreenData$TipOptionInfo;

    .line 269
    sget v6, Lcom/squareup/ui/buyerflow/R$layout;->signature_tip_option_item:I

    iget-object v7, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->tipBar:Lcom/squareup/widgets/CheckableGroup;

    check-cast v7, Landroid/view/ViewGroup;

    invoke-virtual {v0, v6, v7, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    if-eqz v6, :cond_1

    check-cast v6, Landroid/widget/RadioButton;

    .line 270
    invoke-virtual {v5}, Lcom/squareup/ui/buyer/signature/SignScreenData$TipOptionInfo;->getLabelText()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 271
    invoke-virtual {v5}, Lcom/squareup/ui/buyer/signature/SignScreenData$TipOptionInfo;->getOrdinal()I

    move-result v5

    add-int/lit16 v5, v5, 0x19a

    .line 272
    invoke-virtual {v6, v5}, Landroid/widget/RadioButton;->setId(I)V

    .line 274
    iget-object v5, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->tipBar:Lcom/squareup/widgets/CheckableGroup;

    check-cast v6, Landroid/view/View;

    invoke-virtual {v5, v6}, Lcom/squareup/widgets/CheckableGroup;->addView(Landroid/view/View;)V

    goto :goto_1

    .line 269
    :cond_1
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 277
    :cond_2
    invoke-virtual {p1}, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->getUseCustomTip()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 278
    iget-object v4, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->customTip:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v4}, Lcom/squareup/ui/XableEditText;->getEditText()Landroid/widget/EditText;

    move-result-object v4

    check-cast v4, Lcom/squareup/widgets/SelectableEditText;

    .line 280
    iget-object v5, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->moneyLocaleHelper:Lcom/squareup/money/MoneyLocaleHelper;

    const-string v6, "editText"

    invoke-static {v4, v6}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v4, Lcom/squareup/text/HasSelectableText;

    const/4 v6, 0x2

    const/4 v7, 0x0

    invoke-static {v5, v4, v7, v6, v7}, Lcom/squareup/money/MoneyLocaleHelper;->configure$default(Lcom/squareup/money/MoneyLocaleHelper;Lcom/squareup/text/HasSelectableText;Lcom/squareup/text/SelectableTextScrubber;ILjava/lang/Object;)Lcom/squareup/text/ScrubbingTextWatcher;

    move-result-object v4

    .line 284
    new-instance v5, Lcom/squareup/money/MaxMoneyScrubber;

    .line 285
    iget-object v6, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->moneyLocaleHelper:Lcom/squareup/money/MoneyLocaleHelper;

    check-cast v6, Lcom/squareup/money/MoneyExtractor;

    invoke-virtual {p1}, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->getMoneyFormatter()Lcom/squareup/text/Formatter;

    move-result-object v7

    if-nez v7, :cond_3

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_3
    invoke-virtual {p1}, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->getCustomTipMaxMoney()Lcom/squareup/protos/common/Money;

    move-result-object v8

    if-nez v8, :cond_4

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    .line 284
    :cond_4
    invoke-direct {v5, v6, v7, v8}, Lcom/squareup/money/MaxMoneyScrubber;-><init>(Lcom/squareup/money/MoneyExtractor;Lcom/squareup/text/Formatter;Lcom/squareup/protos/common/Money;)V

    .line 287
    check-cast v5, Lcom/squareup/text/Scrubber;

    invoke-virtual {v4, v5}, Lcom/squareup/text/ScrubbingTextWatcher;->addScrubber(Lcom/squareup/text/Scrubber;)V

    .line 289
    invoke-virtual {p1}, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->getMoneyFormatter()Lcom/squareup/text/Formatter;

    move-result-object v4

    if-nez v4, :cond_5

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_5
    const-wide/16 v5, 0x0

    iget-object v7, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->currencyCode:Lcom/squareup/protos/common/CurrencyCode;

    invoke-static {v5, v6, v7}, Lcom/squareup/money/MoneyBuilder;->of(JLcom/squareup/protos/common/CurrencyCode;)Lcom/squareup/protos/common/Money;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/squareup/text/Formatter;->format(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v4

    .line 293
    sget v5, Lcom/squareup/ui/buyerflow/R$layout;->signature_scaling_tip_option_item:I

    iget-object v6, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->tipBar:Lcom/squareup/widgets/CheckableGroup;

    check-cast v6, Landroid/view/ViewGroup;

    .line 292
    invoke-virtual {v0, v5, v6, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_7

    check-cast v0, Landroid/widget/RadioButton;

    .line 295
    new-instance v2, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderTipBar$$inlined$let$lambda$2;

    invoke-direct {v2, p0, p1, p2}, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderTipBar$$inlined$let$lambda$2;-><init>(Lcom/squareup/ui/buyer/signature/SignLayoutRunner;Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;Lcom/squareup/util/Res;)V

    check-cast v2, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 302
    sget v2, Lcom/squareup/ui/buyerflow/R$id;->custom_tip_button:I

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setId(I)V

    .line 303
    iget-object v2, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->tipBar:Lcom/squareup/widgets/CheckableGroup;

    move-object v3, v0

    check-cast v3, Landroid/view/View;

    invoke-virtual {v2, v3}, Lcom/squareup/widgets/CheckableGroup;->addView(Landroid/view/View;)V

    .line 306
    iget-object v2, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->customTip:Lcom/squareup/ui/XableEditText;

    new-instance v3, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderTipBar$$inlined$let$lambda$3;

    invoke-direct {v3, v1, p0, p1, p2}, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderTipBar$$inlined$let$lambda$3;-><init>(Landroid/widget/RadioButton;Lcom/squareup/ui/buyer/signature/SignLayoutRunner;Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;Lcom/squareup/util/Res;)V

    check-cast v3, Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Lcom/squareup/ui/XableEditText;->setOnButtonClickListener(Landroid/view/View$OnClickListener;)V

    .line 315
    iget-object v1, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->customTip:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v1}, Lcom/squareup/ui/XableEditText;->getEditText()Landroid/widget/EditText;

    move-result-object v1

    check-cast v1, Lcom/squareup/widgets/SelectableEditText;

    const-string v2, "customTipEditText"

    .line 316
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Lcom/squareup/widgets/SelectableEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 317
    sget-object v2, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderTipBar$1$4;->INSTANCE:Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderTipBar$1$4;

    check-cast v2, Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v1, v2}, Lcom/squareup/widgets/SelectableEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 324
    iget-object v1, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->customTip:Lcom/squareup/ui/XableEditText;

    new-instance v2, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderTipBar$$inlined$let$lambda$4;

    invoke-direct {v2, p0, p1, p2}, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderTipBar$$inlined$let$lambda$4;-><init>(Lcom/squareup/ui/buyer/signature/SignLayoutRunner;Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;Lcom/squareup/util/Res;)V

    check-cast v2, Landroid/text/TextWatcher;

    invoke-virtual {v1, v2}, Lcom/squareup/ui/XableEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 338
    invoke-virtual {p1}, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->getHasAutoGratuity()Z

    move-result p1

    if-eqz p1, :cond_6

    .line 339
    sget p1, Lcom/squareup/checkout/R$string;->buyer_tip_additional_tip:I

    invoke-interface {p2, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    goto :goto_2

    .line 341
    :cond_6
    sget p1, Lcom/squareup/checkout/R$string;->buyer_tip_custom:I

    invoke-interface {p2, p1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    check-cast p1, Ljava/lang/CharSequence;

    .line 338
    :goto_2
    invoke-virtual {v0, p1}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 292
    :cond_7
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 258
    :cond_8
    new-instance p1, Lkotlin/TypeCastException;

    invoke-direct {p1, v2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_9
    :goto_3
    return-void
.end method

.method private final showCustomTip()V
    .locals 2

    .line 357
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->tipAnimator:Lcom/squareup/widgets/PersistentViewAnimator;

    invoke-virtual {v0}, Lcom/squareup/widgets/PersistentViewAnimator;->getDisplayedChildId()I

    move-result v0

    iget-object v1, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->tipBar:Lcom/squareup/widgets/CheckableGroup;

    invoke-virtual {v1}, Lcom/squareup/widgets/CheckableGroup;->getId()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 358
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->tipAnimator:Lcom/squareup/widgets/PersistentViewAnimator;

    iget-object v1, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->customTip:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v1}, Lcom/squareup/ui/XableEditText;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/widgets/PersistentViewAnimator;->setDisplayedChildById(I)V

    .line 359
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->customTip:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0}, Lcom/squareup/ui/XableEditText;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/SelectableEditText;

    invoke-virtual {v0}, Lcom/squareup/widgets/SelectableEditText;->requestFocus()Z

    .line 360
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->customTip:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0}, Lcom/squareup/ui/XableEditText;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/SelectableEditText;

    const-string v1, "customTip.editText"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/view/View;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/squareup/util/Views;->showSoftKeyboard(Landroid/view/View;Z)V

    :cond_0
    return-void
.end method

.method private final showTipBar()V
    .locals 2

    .line 349
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->tipBar:Lcom/squareup/widgets/CheckableGroup;

    invoke-virtual {v0}, Lcom/squareup/widgets/CheckableGroup;->getId()I

    move-result v0

    .line 350
    iget-object v1, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->tipAnimator:Lcom/squareup/widgets/PersistentViewAnimator;

    invoke-virtual {v1}, Lcom/squareup/widgets/PersistentViewAnimator;->getDisplayedChildId()I

    move-result v1

    if-eq v1, v0, :cond_0

    .line 351
    iget-object v1, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->tipAnimator:Lcom/squareup/widgets/PersistentViewAnimator;

    invoke-virtual {v1, v0}, Lcom/squareup/widgets/PersistentViewAnimator;->setDisplayedChildById(I)V

    .line 352
    iget-object v0, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->customTip:Lcom/squareup/ui/XableEditText;

    invoke-virtual {v0}, Lcom/squareup/ui/XableEditText;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/SelectableEditText;

    const-string v1, "customTip.editText"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public createSignatureBitmap(II)Landroid/graphics/Bitmap;
    .locals 8

    const-string/jumbo v0, "x"

    const/4 v1, 0x1

    .line 399
    invoke-static {v1, p1}, Ljava/lang/Math;->max(II)I

    move-result p1

    .line 400
    invoke-static {v1, p2}, Ljava/lang/Math;->max(II)I

    move-result p2

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 415
    :goto_0
    :try_start_0
    sget-object v3, Landroid/graphics/Bitmap$Config;->ALPHA_8:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, p2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    const-string v4, "Bitmap.createBitmap(width, height, ALPHA_8)"

    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    return-object v3

    :catch_0
    move-exception v3

    if-nez v2, :cond_0

    .line 428
    iget-object v4, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->picassoCache:Lcom/squareup/picasso/Cache;

    invoke-interface {v4}, Lcom/squareup/picasso/Cache;->clear()V

    .line 430
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v4

    .line 431
    invoke-virtual {v4}, Ljava/lang/Runtime;->gc()V

    .line 433
    :cond_0
    iget-object v4, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->ohSnapLogger:Lcom/squareup/log/OhSnapLogger;

    .line 434
    sget-object v5, Lcom/squareup/log/OhSnapLogger$EventType;->WARNING:Lcom/squareup/log/OhSnapLogger$EventType;

    .line 435
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Could not create "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v7, " signature bitmap, attempt "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 433
    invoke-interface {v4, v5, v6}, Lcom/squareup/log/OhSnapLogger;->log(Lcom/squareup/log/OhSnapLogger$EventType;Ljava/lang/String;)V

    add-int/lit8 v2, v2, 0x1

    const/4 v4, 0x5

    if-eq v2, v4, :cond_1

    const-wide/16 v3, 0xc8

    .line 462
    invoke-static {v3, v4}, Landroid/os/SystemClock;->sleep(J)V

    goto :goto_0

    .line 459
    :cond_1
    check-cast v3, Ljava/lang/Throwable;

    throw v3

    :catch_1
    move-exception v2

    .line 418
    invoke-virtual {v2}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 419
    move-object v4, v3

    check-cast v4, Ljava/lang/CharSequence;

    const-string v5, "bitmap size exceeds"

    check-cast v5, Ljava/lang/CharSequence;

    const/4 v6, 0x2

    const/4 v7, 0x0

    invoke-static {v4, v5, v1, v6, v7}, Lkotlin/text/StringsKt;->contains$default(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZILjava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 420
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ": "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    check-cast v2, Ljava/lang/Throwable;

    invoke-direct {v1, p1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    check-cast v1, Ljava/lang/Throwable;

    throw v1

    .line 422
    :cond_2
    check-cast v2, Ljava/lang/Throwable;

    throw v2
.end method

.method public showRendering(Lcom/squareup/ui/buyer/signature/SignScreenData;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 1

    const-string v0, "rendering"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "containerHints"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 101
    invoke-virtual {p1}, Lcom/squareup/ui/buyer/signature/SignScreenData;->getSignatureConfig()Lcom/squareup/ui/buyer/signature/SignScreenData$SignatureConfig;

    move-result-object p2

    invoke-virtual {p1}, Lcom/squareup/ui/buyer/signature/SignScreenData;->getRes()Lcom/squareup/util/Res;

    move-result-object v0

    invoke-direct {p0, p2, v0}, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->renderSignatureView(Lcom/squareup/ui/buyer/signature/SignScreenData$SignatureConfig;Lcom/squareup/util/Res;)V

    .line 102
    invoke-virtual {p1}, Lcom/squareup/ui/buyer/signature/SignScreenData;->getHeaderInformation()Lcom/squareup/ui/buyer/signature/SignScreenData$HeaderInformation;

    move-result-object p2

    invoke-direct {p0, p2}, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->renderActionBar(Lcom/squareup/ui/buyer/signature/SignScreenData$HeaderInformation;)V

    .line 103
    invoke-virtual {p1}, Lcom/squareup/ui/buyer/signature/SignScreenData;->getReturnPolicy()Lcom/squareup/ui/buyer/signature/SignScreenData$ReturnPolicy;

    move-result-object p2

    invoke-virtual {p1}, Lcom/squareup/ui/buyer/signature/SignScreenData;->getRes()Lcom/squareup/util/Res;

    move-result-object v0

    invoke-direct {p0, p2, v0}, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->renderAgreement(Lcom/squareup/ui/buyer/signature/SignScreenData$ReturnPolicy;Lcom/squareup/util/Res;)V

    .line 105
    invoke-virtual {p1}, Lcom/squareup/ui/buyer/signature/SignScreenData;->getTipConfig()Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig;

    move-result-object p2

    instance-of p2, p2, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;

    if-eqz p2, :cond_0

    .line 106
    invoke-virtual {p1}, Lcom/squareup/ui/buyer/signature/SignScreenData;->getTipConfig()Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig;

    move-result-object p2

    check-cast p2, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;

    invoke-virtual {p1}, Lcom/squareup/ui/buyer/signature/SignScreenData;->getRes()Lcom/squareup/util/Res;

    move-result-object p1

    invoke-direct {p0, p2, p1}, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->renderTipBar(Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;Lcom/squareup/util/Res;)V

    :cond_0
    return-void
.end method

.method public bridge synthetic showRendering(Ljava/lang/Object;Lcom/squareup/workflow/ui/ContainerHints;)V
    .locals 0

    .line 51
    check-cast p1, Lcom/squareup/ui/buyer/signature/SignScreenData;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->showRendering(Lcom/squareup/ui/buyer/signature/SignScreenData;Lcom/squareup/workflow/ui/ContainerHints;)V

    return-void
.end method
