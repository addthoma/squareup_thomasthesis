.class public abstract Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult$EmoneyPaymentProcessingResult;
.super Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult;
.source "BuyerWorkflowResult.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "EmoneyPaymentProcessingResult"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult$EmoneyPaymentProcessingResult$EmoneySuccess;,
        Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult$EmoneyPaymentProcessingResult$EmoneyAbort;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u00086\u0018\u00002\u00020\u0001:\u0002\u0003\u0004B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u0002\u0005\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult$EmoneyPaymentProcessingResult;",
        "Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult;",
        "()V",
        "EmoneyAbort",
        "EmoneySuccess",
        "Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult$EmoneyPaymentProcessingResult$EmoneySuccess;",
        "Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult$EmoneyPaymentProcessingResult$EmoneyAbort;",
        "buyer-flow_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    .line 21
    invoke-direct {p0, v0}, Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 21
    invoke-direct {p0}, Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult$EmoneyPaymentProcessingResult;-><init>()V

    return-void
.end method
