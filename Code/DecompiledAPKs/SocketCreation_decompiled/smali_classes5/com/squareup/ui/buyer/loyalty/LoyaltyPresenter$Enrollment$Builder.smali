.class Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;
.super Ljava/lang/Object;
.source "LoyaltyPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Builder"
.end annotation


# instance fields
.field private claimHelpText:Ljava/lang/String;

.field private claimText:Ljava/lang/String;

.field private defaultPhoneNumber:Ljava/lang/String;

.field private enrollmentLoyaltyStatus:Ljava/lang/String;

.field private phoneEditHint:Ljava/lang/String;

.field private phoneNumber:Ljava/lang/String;

.field private programName:Ljava/lang/String;

.field private receiptPhoneNumber:Ljava/lang/String;

.field private rewardRequirementText:Ljava/lang/String;

.field private rewardTiers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/server/account/protos/RewardTier;",
            ">;"
        }
    .end annotation
.end field

.field private rewardTiersLoyaltyStatus:Ljava/lang/String;


# direct methods
.method constructor <init>()V
    .locals 0

    .line 224
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;)Ljava/lang/String;
    .locals 0

    .line 211
    iget-object p0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;->defaultPhoneNumber:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;)Ljava/lang/String;
    .locals 0

    .line 211
    iget-object p0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;->receiptPhoneNumber:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$1000(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;)Ljava/util/List;
    .locals 0

    .line 211
    iget-object p0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;->rewardTiers:Ljava/util/List;

    return-object p0
.end method

.method static synthetic access$200(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;)Ljava/lang/String;
    .locals 0

    .line 211
    iget-object p0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;->phoneNumber:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$300(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;)Ljava/lang/String;
    .locals 0

    .line 211
    iget-object p0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;->programName:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$400(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;)Ljava/lang/String;
    .locals 0

    .line 211
    iget-object p0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;->rewardTiersLoyaltyStatus:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$500(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;)Ljava/lang/String;
    .locals 0

    .line 211
    iget-object p0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;->enrollmentLoyaltyStatus:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$600(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;)Ljava/lang/String;
    .locals 0

    .line 211
    iget-object p0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;->rewardRequirementText:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$700(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;)Ljava/lang/String;
    .locals 0

    .line 211
    iget-object p0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;->phoneEditHint:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$800(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;)Ljava/lang/String;
    .locals 0

    .line 211
    iget-object p0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;->claimText:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$900(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;)Ljava/lang/String;
    .locals 0

    .line 211
    iget-object p0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;->claimHelpText:Ljava/lang/String;

    return-object p0
.end method


# virtual methods
.method public build()Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment;
    .locals 2

    .line 283
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;->rewardRequirementText:Ljava/lang/String;

    const-string v1, "rewardRequirementText"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 284
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;->phoneEditHint:Ljava/lang/String;

    const-string v1, "phoneEditHint"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 285
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;->claimText:Ljava/lang/String;

    const-string v1, "claimText"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 286
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;->claimHelpText:Ljava/lang/String;

    const-string v1, "claimHelpText"

    invoke-static {v0, v1}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 288
    new-instance v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment;

    invoke-direct {v0, p0}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment;-><init>(Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;)V

    return-object v0
.end method

.method claimHelpText(Ljava/lang/String;)Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;
    .locals 0

    .line 273
    iput-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;->claimHelpText:Ljava/lang/String;

    return-object p0
.end method

.method claimText(Ljava/lang/String;)Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;
    .locals 0

    .line 268
    iput-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;->claimText:Ljava/lang/String;

    return-object p0
.end method

.method defaultPhoneNumber(Ljava/lang/String;)Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;
    .locals 0

    .line 228
    iput-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;->defaultPhoneNumber:Ljava/lang/String;

    return-object p0
.end method

.method enrollmentLoyaltyStatus(Ljava/lang/String;)Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;
    .locals 0

    .line 253
    iput-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;->enrollmentLoyaltyStatus:Ljava/lang/String;

    return-object p0
.end method

.method phoneEditHint(Ljava/lang/String;)Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;
    .locals 0

    .line 263
    iput-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;->phoneEditHint:Ljava/lang/String;

    return-object p0
.end method

.method public phoneNumber(Ljava/lang/String;)Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;
    .locals 0

    .line 238
    iput-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;->phoneNumber:Ljava/lang/String;

    return-object p0
.end method

.method public programName(Ljava/lang/String;)Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;
    .locals 0

    .line 243
    iput-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;->programName:Ljava/lang/String;

    return-object p0
.end method

.method receiptPhoneNumber(Ljava/lang/String;)Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;
    .locals 0

    .line 233
    iput-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;->receiptPhoneNumber:Ljava/lang/String;

    return-object p0
.end method

.method rewardRequirementText(Ljava/lang/String;)Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;
    .locals 0

    .line 258
    iput-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;->rewardRequirementText:Ljava/lang/String;

    return-object p0
.end method

.method rewardTiers(Ljava/util/List;)Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/server/account/protos/RewardTier;",
            ">;)",
            "Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;"
        }
    .end annotation

    .line 278
    iput-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;->rewardTiers:Ljava/util/List;

    return-object p0
.end method

.method rewardTiersLoyaltyStatus(Ljava/lang/String;)Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;
    .locals 0

    .line 248
    iput-object p1, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter$Enrollment$Builder;->rewardTiersLoyaltyStatus:Ljava/lang/String;

    return-object p0
.end method
