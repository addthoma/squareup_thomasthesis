.class public final Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningPresenter_Factory;
.super Ljava/lang/Object;
.source "PartialAuthWarningPresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final apiTransactionStateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiTransactionState;",
            ">;"
        }
    .end annotation
.end field

.field private final buyerLocaleOverrideProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
            ">;"
        }
    .end annotation
.end field

.field private final buyerScopeRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerScopeRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final lastAddedTenderProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/tender/BaseCardTender;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/tender/BaseCardTender;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiTransactionState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)V"
        }
    .end annotation

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningPresenter_Factory;->buyerScopeRunnerProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p2, p0, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningPresenter_Factory;->lastAddedTenderProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p3, p0, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningPresenter_Factory;->apiTransactionStateProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p4, p0, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningPresenter_Factory;->buyerLocaleOverrideProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p5, p0, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningPresenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningPresenter_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/tender/BaseCardTender;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/api/ApiTransactionState;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/buyer/language/BuyerLocaleOverride;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;)",
            "Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningPresenter_Factory;"
        }
    .end annotation

    .line 54
    new-instance v6, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningPresenter_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningPresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Lcom/squareup/ui/buyer/BuyerScopeRunner;Lcom/squareup/payment/tender/BaseCardTender;Lcom/squareup/api/ApiTransactionState;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/settings/server/Features;)Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningPresenter;
    .locals 7

    .line 60
    new-instance v6, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningPresenter;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningPresenter;-><init>(Lcom/squareup/ui/buyer/BuyerScopeRunner;Lcom/squareup/payment/tender/BaseCardTender;Lcom/squareup/api/ApiTransactionState;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/settings/server/Features;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningPresenter;
    .locals 5

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningPresenter_Factory;->buyerScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/BuyerScopeRunner;

    iget-object v1, p0, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningPresenter_Factory;->lastAddedTenderProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/payment/tender/BaseCardTender;

    iget-object v2, p0, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningPresenter_Factory;->apiTransactionStateProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/api/ApiTransactionState;

    iget-object v3, p0, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningPresenter_Factory;->buyerLocaleOverrideProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/buyer/language/BuyerLocaleOverride;

    iget-object v4, p0, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningPresenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v4}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/squareup/settings/server/Features;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningPresenter_Factory;->newInstance(Lcom/squareup/ui/buyer/BuyerScopeRunner;Lcom/squareup/payment/tender/BaseCardTender;Lcom/squareup/api/ApiTransactionState;Lcom/squareup/buyer/language/BuyerLocaleOverride;Lcom/squareup/settings/server/Features;)Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningPresenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningPresenter_Factory;->get()Lcom/squareup/ui/buyer/partialauth/PartialAuthWarningPresenter;

    move-result-object v0

    return-object v0
.end method
