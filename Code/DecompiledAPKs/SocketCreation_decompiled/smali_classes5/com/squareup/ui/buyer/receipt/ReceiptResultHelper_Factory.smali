.class public final Lcom/squareup/ui/buyer/receipt/ReceiptResultHelper_Factory;
.super Ljava/lang/Object;
.source "ReceiptResultHelper_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/buyer/receipt/ReceiptResultHelper;",
        ">;"
    }
.end annotation


# instance fields
.field private final checkoutInformationEventLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/CheckoutInformationEventLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final customerManagementSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final emailAndLoyaltyRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final employeeManagementProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;"
        }
    .end annotation
.end field

.field private final loyaltySettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltySettings;",
            ">;"
        }
    .end annotation
.end field

.field private final offlineModeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionMetricsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/TransactionMetrics;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltySettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/TransactionMetrics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/CheckoutInformationEventLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            ">;)V"
        }
    .end annotation

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lcom/squareup/ui/buyer/receipt/ReceiptResultHelper_Factory;->offlineModeProvider:Ljavax/inject/Provider;

    .line 48
    iput-object p2, p0, Lcom/squareup/ui/buyer/receipt/ReceiptResultHelper_Factory;->loyaltySettingsProvider:Ljavax/inject/Provider;

    .line 49
    iput-object p3, p0, Lcom/squareup/ui/buyer/receipt/ReceiptResultHelper_Factory;->employeeManagementProvider:Ljavax/inject/Provider;

    .line 50
    iput-object p4, p0, Lcom/squareup/ui/buyer/receipt/ReceiptResultHelper_Factory;->emailAndLoyaltyRunnerProvider:Ljavax/inject/Provider;

    .line 51
    iput-object p5, p0, Lcom/squareup/ui/buyer/receipt/ReceiptResultHelper_Factory;->resProvider:Ljavax/inject/Provider;

    .line 52
    iput-object p6, p0, Lcom/squareup/ui/buyer/receipt/ReceiptResultHelper_Factory;->transactionMetricsProvider:Ljavax/inject/Provider;

    .line 53
    iput-object p7, p0, Lcom/squareup/ui/buyer/receipt/ReceiptResultHelper_Factory;->checkoutInformationEventLoggerProvider:Ljavax/inject/Provider;

    .line 54
    iput-object p8, p0, Lcom/squareup/ui/buyer/receipt/ReceiptResultHelper_Factory;->customerManagementSettingsProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/buyer/receipt/ReceiptResultHelper_Factory;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltySettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/TransactionMetrics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/CheckoutInformationEventLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            ">;)",
            "Lcom/squareup/ui/buyer/receipt/ReceiptResultHelper_Factory;"
        }
    .end annotation

    .line 69
    new-instance v9, Lcom/squareup/ui/buyer/receipt/ReceiptResultHelper_Factory;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/ui/buyer/receipt/ReceiptResultHelper_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v9
.end method

.method public static newInstance(Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;Lcom/squareup/util/Res;Lcom/squareup/ui/main/TransactionMetrics;Lcom/squareup/log/CheckoutInformationEventLogger;Lcom/squareup/crm/CustomerManagementSettings;)Lcom/squareup/ui/buyer/receipt/ReceiptResultHelper;
    .locals 10

    .line 78
    new-instance v9, Lcom/squareup/ui/buyer/receipt/ReceiptResultHelper;

    move-object v0, v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/squareup/ui/buyer/receipt/ReceiptResultHelper;-><init>(Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;Lcom/squareup/util/Res;Lcom/squareup/ui/main/TransactionMetrics;Lcom/squareup/log/CheckoutInformationEventLogger;Lcom/squareup/crm/CustomerManagementSettings;)V

    return-object v9
.end method


# virtual methods
.method public get()Lcom/squareup/ui/buyer/receipt/ReceiptResultHelper;
    .locals 9

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/buyer/receipt/ReceiptResultHelper_Factory;->offlineModeProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/payment/OfflineModeMonitor;

    iget-object v0, p0, Lcom/squareup/ui/buyer/receipt/ReceiptResultHelper_Factory;->loyaltySettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/loyalty/LoyaltySettings;

    iget-object v0, p0, Lcom/squareup/ui/buyer/receipt/ReceiptResultHelper_Factory;->employeeManagementProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/permissions/EmployeeManagement;

    iget-object v0, p0, Lcom/squareup/ui/buyer/receipt/ReceiptResultHelper_Factory;->emailAndLoyaltyRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;

    iget-object v0, p0, Lcom/squareup/ui/buyer/receipt/ReceiptResultHelper_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/buyer/receipt/ReceiptResultHelper_Factory;->transactionMetricsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/ui/main/TransactionMetrics;

    iget-object v0, p0, Lcom/squareup/ui/buyer/receipt/ReceiptResultHelper_Factory;->checkoutInformationEventLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/squareup/log/CheckoutInformationEventLogger;

    iget-object v0, p0, Lcom/squareup/ui/buyer/receipt/ReceiptResultHelper_Factory;->customerManagementSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/squareup/crm/CustomerManagementSettings;

    invoke-static/range {v1 .. v8}, Lcom/squareup/ui/buyer/receipt/ReceiptResultHelper_Factory;->newInstance(Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;Lcom/squareup/util/Res;Lcom/squareup/ui/main/TransactionMetrics;Lcom/squareup/log/CheckoutInformationEventLogger;Lcom/squareup/crm/CustomerManagementSettings;)Lcom/squareup/ui/buyer/receipt/ReceiptResultHelper;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 15
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/receipt/ReceiptResultHelper_Factory;->get()Lcom/squareup/ui/buyer/receipt/ReceiptResultHelper;

    move-result-object v0

    return-object v0
.end method
