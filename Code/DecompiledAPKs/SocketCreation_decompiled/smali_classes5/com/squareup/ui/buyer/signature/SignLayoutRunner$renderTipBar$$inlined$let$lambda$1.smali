.class final Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderTipBar$$inlined$let$lambda$1;
.super Ljava/lang/Object;
.source "SignLayoutRunner.kt"

# interfaces
.implements Lcom/squareup/widgets/CheckableGroup$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->renderTipBar(Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;Lcom/squareup/util/Res;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0008\u0003\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u00032\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0006H\n\u00a2\u0006\u0002\u0008\u0008\u00a8\u0006\t"
    }
    d2 = {
        "<anonymous>",
        "",
        "<anonymous parameter 0>",
        "Lcom/squareup/widgets/CheckableGroup;",
        "kotlin.jvm.PlatformType",
        "checkedId",
        "",
        "<anonymous parameter 2>",
        "onCheckedChanged",
        "com/squareup/ui/buyer/signature/SignLayoutRunner$renderTipBar$1$1"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $res$inlined:Lcom/squareup/util/Res;

.field final synthetic $tipConfig$inlined:Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;

.field final synthetic this$0:Lcom/squareup/ui/buyer/signature/SignLayoutRunner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/buyer/signature/SignLayoutRunner;Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;Lcom/squareup/util/Res;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderTipBar$$inlined$let$lambda$1;->this$0:Lcom/squareup/ui/buyer/signature/SignLayoutRunner;

    iput-object p2, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderTipBar$$inlined$let$lambda$1;->$tipConfig$inlined:Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;

    iput-object p3, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderTipBar$$inlined$let$lambda$1;->$res$inlined:Lcom/squareup/util/Res;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCheckedChanged(Lcom/squareup/widgets/CheckableGroup;II)V
    .locals 0

    .line 241
    sget p1, Lcom/squareup/ui/buyerflow/R$id;->no_tip_button:I

    const/4 p3, 0x1

    if-ne p2, p1, :cond_0

    .line 242
    iget-object p1, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderTipBar$$inlined$let$lambda$1;->$tipConfig$inlined:Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;

    invoke-virtual {p1}, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->getClearTip()Lkotlin/jvm/functions/Function0;

    move-result-object p1

    invoke-interface {p1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    .line 243
    iget-object p1, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderTipBar$$inlined$let$lambda$1;->this$0:Lcom/squareup/ui/buyer/signature/SignLayoutRunner;

    invoke-static {p1, p3}, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->access$setCustomTipIsClearable$p(Lcom/squareup/ui/buyer/signature/SignLayoutRunner;Z)V

    goto :goto_0

    .line 244
    :cond_0
    sget p1, Lcom/squareup/ui/buyerflow/R$id;->custom_tip_button:I

    if-ne p2, p1, :cond_1

    .line 245
    iget-object p1, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderTipBar$$inlined$let$lambda$1;->this$0:Lcom/squareup/ui/buyer/signature/SignLayoutRunner;

    invoke-static {p1}, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->access$showCustomTip(Lcom/squareup/ui/buyer/signature/SignLayoutRunner;)V

    .line 246
    iget-object p1, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderTipBar$$inlined$let$lambda$1;->this$0:Lcom/squareup/ui/buyer/signature/SignLayoutRunner;

    invoke-static {p1}, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->access$getCustomTipIsClearable$p(Lcom/squareup/ui/buyer/signature/SignLayoutRunner;)Z

    move-result p1

    if-eqz p1, :cond_2

    .line 247
    iget-object p1, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderTipBar$$inlined$let$lambda$1;->$tipConfig$inlined:Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;

    invoke-virtual {p1}, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->getClearTip()Lkotlin/jvm/functions/Function0;

    move-result-object p1

    invoke-interface {p1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    .line 248
    iget-object p1, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderTipBar$$inlined$let$lambda$1;->this$0:Lcom/squareup/ui/buyer/signature/SignLayoutRunner;

    const/4 p2, 0x0

    invoke-static {p1, p2}, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->access$setCustomTipIsClearable$p(Lcom/squareup/ui/buyer/signature/SignLayoutRunner;Z)V

    goto :goto_0

    .line 251
    :cond_1
    iget-object p1, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderTipBar$$inlined$let$lambda$1;->$tipConfig$inlined:Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;

    invoke-virtual {p1}, Lcom/squareup/ui/buyer/signature/SignScreenData$TipConfig$HasTip;->getSetTip()Lkotlin/jvm/functions/Function1;

    move-result-object p1

    add-int/lit16 p2, p2, -0x19a

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-interface {p1, p2}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 252
    iget-object p1, p0, Lcom/squareup/ui/buyer/signature/SignLayoutRunner$renderTipBar$$inlined$let$lambda$1;->this$0:Lcom/squareup/ui/buyer/signature/SignLayoutRunner;

    invoke-static {p1, p3}, Lcom/squareup/ui/buyer/signature/SignLayoutRunner;->access$setCustomTipIsClearable$p(Lcom/squareup/ui/buyer/signature/SignLayoutRunner;Z)V

    :cond_2
    :goto_0
    return-void
.end method
