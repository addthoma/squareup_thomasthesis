.class public final Lcom/squareup/ui/buyer/emv/EmvScope_Module_ProvidePinListenerFactory;
.super Ljava/lang/Object;
.source "EmvScope_Module_ProvidePinListenerFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$PinListener;",
        ">;"
    }
.end annotation


# instance fields
.field private final module:Lcom/squareup/ui/buyer/emv/EmvScope$Module;

.field private final runnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/EmvScope$Runner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/ui/buyer/emv/EmvScope$Module;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/buyer/emv/EmvScope$Module;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/EmvScope$Runner;",
            ">;)V"
        }
    .end annotation

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/squareup/ui/buyer/emv/EmvScope_Module_ProvidePinListenerFactory;->module:Lcom/squareup/ui/buyer/emv/EmvScope$Module;

    .line 25
    iput-object p2, p0, Lcom/squareup/ui/buyer/emv/EmvScope_Module_ProvidePinListenerFactory;->runnerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Lcom/squareup/ui/buyer/emv/EmvScope$Module;Ljavax/inject/Provider;)Lcom/squareup/ui/buyer/emv/EmvScope_Module_ProvidePinListenerFactory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/buyer/emv/EmvScope$Module;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/emv/EmvScope$Runner;",
            ">;)",
            "Lcom/squareup/ui/buyer/emv/EmvScope_Module_ProvidePinListenerFactory;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/ui/buyer/emv/EmvScope_Module_ProvidePinListenerFactory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/buyer/emv/EmvScope_Module_ProvidePinListenerFactory;-><init>(Lcom/squareup/ui/buyer/emv/EmvScope$Module;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static providePinListener(Lcom/squareup/ui/buyer/emv/EmvScope$Module;Lcom/squareup/ui/buyer/emv/EmvScope$Runner;)Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$PinListener;
    .locals 0

    .line 40
    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyer/emv/EmvScope$Module;->providePinListener(Lcom/squareup/ui/buyer/emv/EmvScope$Runner;)Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$PinListener;

    move-result-object p0

    const-string p1, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, p1}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$PinListener;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$PinListener;
    .locals 2

    .line 30
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/EmvScope_Module_ProvidePinListenerFactory;->module:Lcom/squareup/ui/buyer/emv/EmvScope$Module;

    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/EmvScope_Module_ProvidePinListenerFactory;->runnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    invoke-static {v0, v1}, Lcom/squareup/ui/buyer/emv/EmvScope_Module_ProvidePinListenerFactory;->providePinListener(Lcom/squareup/ui/buyer/emv/EmvScope$Module;Lcom/squareup/ui/buyer/emv/EmvScope$Runner;)Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$PinListener;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/emv/EmvScope_Module_ProvidePinListenerFactory;->get()Lcom/squareup/ui/buyer/emv/pinpad/PinPresenter$PinListener;

    move-result-object v0

    return-object v0
.end method
