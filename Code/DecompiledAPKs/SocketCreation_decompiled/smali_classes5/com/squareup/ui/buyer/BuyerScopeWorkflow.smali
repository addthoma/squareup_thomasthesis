.class public final Lcom/squareup/ui/buyer/BuyerScopeWorkflow;
.super Lcom/squareup/workflow/StatefulWorkflow;
.source "BuyerScopeWorkflow.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/workflow/StatefulWorkflow<",
        "Lcom/squareup/ui/buyer/BuyerScopeInput;",
        "Lkotlin/Unit;",
        "Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult;",
        "Ljava/util/Map<",
        "+",
        "Lcom/squareup/container/ContainerLayering;",
        "+",
        "Lcom/squareup/workflow/legacy/Screen<",
        "**>;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002@\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0004\u0012*\u0012(\u0012\u0006\u0008\u0001\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\n\u0012\u0006\u0008\u0001\u0012\u00020\u0006`\t0\u0001B5\u0008\u0007\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u000c\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u00140\u0013\u00a2\u0006\u0002\u0010\u0015J\u001a\u0010\u0016\u001a\u00020\u00032\u0006\u0010\u0017\u001a\u00020\u00022\u0008\u0010\u0018\u001a\u0004\u0018\u00010\u0019H\u0016JW\u0010\u001a\u001a(\u0012\u0006\u0008\u0001\u0012\u00020\u0006\u0012\u0010\u0012\u000e\u0012\u0002\u0008\u0003\u0012\u0002\u0008\u00030\u0007j\u0002`\u00080\u0005j\n\u0012\u0006\u0008\u0001\u0012\u00020\u0006`\t2\u0006\u0010\u0017\u001a\u00020\u00022\u0006\u0010\u001b\u001a\u00020\u00032\u0012\u0010\u001c\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u001dH\u0016\u00a2\u0006\u0002\u0010\u001eJ\u0015\u0010\u001f\u001a\u00020\u00192\u0006\u0010\u001b\u001a\u00020\u0003H\u0016\u00a2\u0006\u0002\u0010 R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0012\u001a\u0008\u0012\u0004\u0012\u00020\u00140\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006!"
    }
    d2 = {
        "Lcom/squareup/ui/buyer/BuyerScopeWorkflow;",
        "Lcom/squareup/workflow/StatefulWorkflow;",
        "Lcom/squareup/ui/buyer/BuyerScopeInput;",
        "",
        "Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult;",
        "",
        "Lcom/squareup/container/ContainerLayering;",
        "Lcom/squareup/workflow/legacy/Screen;",
        "Lcom/squareup/workflow/legacy/AnyScreen;",
        "Lcom/squareup/workflow/LayeredScreen;",
        "storeAndForwardQuickEnableWorkflow",
        "Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow;",
        "billReceiptWorkflow",
        "Lcom/squareup/ui/buyer/receipt/BillReceiptWorkflow;",
        "billTipWorkflow",
        "Lcom/squareup/ui/buyer/tip/BillTipWorkflow;",
        "emoneyWorkflow",
        "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingWorkflow;",
        "separatedPrintoutsWorkflow",
        "Ldagger/Lazy;",
        "Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsWorkflow;",
        "(Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow;Lcom/squareup/ui/buyer/receipt/BillReceiptWorkflow;Lcom/squareup/ui/buyer/tip/BillTipWorkflow;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingWorkflow;Ldagger/Lazy;)V",
        "initialState",
        "props",
        "snapshot",
        "Lcom/squareup/workflow/Snapshot;",
        "render",
        "state",
        "context",
        "Lcom/squareup/workflow/RenderContext;",
        "(Lcom/squareup/ui/buyer/BuyerScopeInput;Lkotlin/Unit;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;",
        "snapshotState",
        "(Lkotlin/Unit;)Lcom/squareup/workflow/Snapshot;",
        "buyer-flow_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final billReceiptWorkflow:Lcom/squareup/ui/buyer/receipt/BillReceiptWorkflow;

.field private final billTipWorkflow:Lcom/squareup/ui/buyer/tip/BillTipWorkflow;

.field private final emoneyWorkflow:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingWorkflow;

.field private final separatedPrintoutsWorkflow:Ldagger/Lazy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldagger/Lazy<",
            "Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final storeAndForwardQuickEnableWorkflow:Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow;Lcom/squareup/ui/buyer/receipt/BillReceiptWorkflow;Lcom/squareup/ui/buyer/tip/BillTipWorkflow;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingWorkflow;Ldagger/Lazy;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow;",
            "Lcom/squareup/ui/buyer/receipt/BillReceiptWorkflow;",
            "Lcom/squareup/ui/buyer/tip/BillTipWorkflow;",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingWorkflow;",
            "Ldagger/Lazy<",
            "Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsWorkflow;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "storeAndForwardQuickEnableWorkflow"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "billReceiptWorkflow"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "billTipWorkflow"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "emoneyWorkflow"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "separatedPrintoutsWorkflow"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 57
    invoke-direct {p0}, Lcom/squareup/workflow/StatefulWorkflow;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/buyer/BuyerScopeWorkflow;->storeAndForwardQuickEnableWorkflow:Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow;

    iput-object p2, p0, Lcom/squareup/ui/buyer/BuyerScopeWorkflow;->billReceiptWorkflow:Lcom/squareup/ui/buyer/receipt/BillReceiptWorkflow;

    iput-object p3, p0, Lcom/squareup/ui/buyer/BuyerScopeWorkflow;->billTipWorkflow:Lcom/squareup/ui/buyer/tip/BillTipWorkflow;

    iput-object p4, p0, Lcom/squareup/ui/buyer/BuyerScopeWorkflow;->emoneyWorkflow:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingWorkflow;

    iput-object p5, p0, Lcom/squareup/ui/buyer/BuyerScopeWorkflow;->separatedPrintoutsWorkflow:Ldagger/Lazy;

    return-void
.end method


# virtual methods
.method public bridge synthetic initialState(Ljava/lang/Object;Lcom/squareup/workflow/Snapshot;)Ljava/lang/Object;
    .locals 0

    .line 51
    check-cast p1, Lcom/squareup/ui/buyer/BuyerScopeInput;

    invoke-virtual {p0, p1, p2}, Lcom/squareup/ui/buyer/BuyerScopeWorkflow;->initialState(Lcom/squareup/ui/buyer/BuyerScopeInput;Lcom/squareup/workflow/Snapshot;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public initialState(Lcom/squareup/ui/buyer/BuyerScopeInput;Lcom/squareup/workflow/Snapshot;)V
    .locals 0

    const-string p2, "props"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-void
.end method

.method public bridge synthetic render(Ljava/lang/Object;Ljava/lang/Object;Lcom/squareup/workflow/RenderContext;)Ljava/lang/Object;
    .locals 0

    .line 51
    check-cast p1, Lcom/squareup/ui/buyer/BuyerScopeInput;

    check-cast p2, Lkotlin/Unit;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/buyer/BuyerScopeWorkflow;->render(Lcom/squareup/ui/buyer/BuyerScopeInput;Lkotlin/Unit;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public render(Lcom/squareup/ui/buyer/BuyerScopeInput;Lkotlin/Unit;Lcom/squareup/workflow/RenderContext;)Ljava/util/Map;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/buyer/BuyerScopeInput;",
            "Lkotlin/Unit;",
            "Lcom/squareup/workflow/RenderContext<",
            "Lkotlin/Unit;",
            "-",
            "Lcom/squareup/ui/buyer/workflow/BuyerWorkflowResult;",
            ">;)",
            "Ljava/util/Map<",
            "+",
            "Lcom/squareup/container/ContainerLayering;",
            "Lcom/squareup/workflow/legacy/Screen<",
            "**>;>;"
        }
    .end annotation

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    const-string v2, "props"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "state"

    move-object/from16 v3, p2

    invoke-static {v3, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "context"

    move-object/from16 v3, p3

    invoke-static {v3, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 76
    instance-of v2, v1, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningStoreAndForwardQuickEnable;

    if-eqz v2, :cond_1

    .line 77
    iget-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeWorkflow;->storeAndForwardQuickEnableWorkflow:Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow;

    move-object v4, v1

    check-cast v4, Lcom/squareup/workflow/Workflow;

    const/4 v5, 0x0

    sget-object v1, Lcom/squareup/ui/buyer/BuyerScopeWorkflow$render$childRendering$1;->INSTANCE:Lcom/squareup/ui/buyer/BuyerScopeWorkflow$render$childRendering$1;

    move-object v6, v1

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v7, 0x2

    const/4 v8, 0x0

    move-object/from16 v3, p3

    invoke-static/range {v3 .. v8}, Lcom/squareup/workflow/RenderContextKt;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/workflow/legacy/Screen;

    if-eqz v1, :cond_0

    .line 79
    sget-object v2, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {v1, v2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object v1

    goto/16 :goto_0

    :cond_0
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 81
    :cond_1
    instance-of v2, v1, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningTip;

    if-eqz v2, :cond_2

    iget-object v1, v0, Lcom/squareup/ui/buyer/BuyerScopeWorkflow;->billTipWorkflow:Lcom/squareup/ui/buyer/tip/BillTipWorkflow;

    move-object v4, v1

    check-cast v4, Lcom/squareup/workflow/Workflow;

    const/4 v5, 0x0

    sget-object v1, Lcom/squareup/ui/buyer/BuyerScopeWorkflow$render$childRendering$2;->INSTANCE:Lcom/squareup/ui/buyer/BuyerScopeWorkflow$render$childRendering$2;

    move-object v6, v1

    check-cast v6, Lkotlin/jvm/functions/Function1;

    const/4 v7, 0x2

    const/4 v8, 0x0

    move-object/from16 v3, p3

    invoke-static/range {v3 .. v8}, Lcom/squareup/workflow/RenderContextKt;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/workflow/legacy/Screen;

    .line 91
    sget-object v2, Lcom/squareup/container/PosLayering;->BODY:Lcom/squareup/container/PosLayering;

    invoke-static {v1, v2}, Lcom/squareup/container/PosLayeringKt;->toPosLayer(Lcom/squareup/workflow/legacy/Screen;Lcom/squareup/container/PosLayering;)Ljava/util/Map;

    move-result-object v1

    goto/16 :goto_0

    .line 93
    :cond_2
    instance-of v2, v1, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningEmoneyPaymentProcessing;

    if-eqz v2, :cond_3

    .line 95
    iget-object v2, v0, Lcom/squareup/ui/buyer/BuyerScopeWorkflow;->emoneyWorkflow:Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingWorkflow;

    move-object v4, v2

    check-cast v4, Lcom/squareup/workflow/Workflow;

    .line 96
    new-instance v5, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;

    .line 97
    check-cast v1, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningEmoneyPaymentProcessing;

    invoke-virtual {v1}, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningEmoneyPaymentProcessing;->getBillPayment()Lcom/squareup/payment/BillPayment;

    move-result-object v2

    invoke-virtual {v1}, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningEmoneyPaymentProcessing;->getMoney()Lcom/squareup/protos/common/Money;

    move-result-object v6

    invoke-virtual {v1}, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningEmoneyPaymentProcessing;->getBrand()Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;

    move-result-object v1

    .line 96
    invoke-direct {v5, v2, v6, v1}, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingInput;-><init>(Lcom/squareup/payment/BillPayment;Lcom/squareup/protos/common/Money;Lcom/squareup/checkoutflow/selecttender/tenderoption/EmoneyBrand;)V

    const/4 v6, 0x0

    .line 99
    sget-object v1, Lcom/squareup/ui/buyer/BuyerScopeWorkflow$render$childRendering$3;->INSTANCE:Lcom/squareup/ui/buyer/BuyerScopeWorkflow$render$childRendering$3;

    move-object v7, v1

    check-cast v7, Lkotlin/jvm/functions/Function1;

    const/4 v8, 0x4

    const/4 v9, 0x0

    move-object/from16 v3, p3

    .line 94
    invoke-static/range {v3 .. v9}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    goto/16 :goto_0

    .line 111
    :cond_3
    instance-of v2, v1, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;

    if-eqz v2, :cond_4

    .line 113
    iget-object v2, v0, Lcom/squareup/ui/buyer/BuyerScopeWorkflow;->billReceiptWorkflow:Lcom/squareup/ui/buyer/receipt/BillReceiptWorkflow;

    move-object v4, v2

    check-cast v4, Lcom/squareup/workflow/Workflow;

    .line 114
    new-instance v2, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;

    .line 115
    move-object v5, v1

    check-cast v5, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;

    invoke-virtual {v5}, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->getReceipt()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v6

    invoke-virtual {v5}, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->getGetRemainingBalanceText()Lkotlin/jvm/functions/Function1;

    move-result-object v7

    invoke-virtual {v5}, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->getCountryCode()Lcom/squareup/CountryCode;

    move-result-object v8

    .line 116
    invoke-virtual {v5}, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->getShowLanguageSelection()Z

    move-result v9

    invoke-virtual {v5}, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->getDisplayName()Ljava/lang/String;

    move-result-object v10

    .line 117
    invoke-virtual {v5}, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->getMostRecentActiveCardReaderId()Lcom/squareup/cardreader/CardReaderId;

    move-result-object v11

    invoke-virtual {v5}, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->getSupportsSms()Z

    move-result v12

    .line 118
    invoke-virtual {v5}, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->getAutoReceiptCompleteTimeout()Ljava/lang/Long;

    move-result-object v13

    invoke-virtual {v5}, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->getShowSmsMarketing()Z

    move-result v14

    invoke-virtual {v5}, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningBillReceipt;->getBusinessName()Ljava/lang/String;

    move-result-object v15

    move-object v5, v2

    .line 114
    invoke-direct/range {v5 .. v15}, Lcom/squareup/ui/buyer/receipt/BillReceiptInput;-><init>(Lcom/squareup/payment/PaymentReceipt;Lkotlin/jvm/functions/Function1;Lcom/squareup/CountryCode;ZLjava/lang/String;Lcom/squareup/cardreader/CardReaderId;ZLjava/lang/Long;ZLjava/lang/String;)V

    const/4 v6, 0x0

    .line 120
    new-instance v5, Lcom/squareup/ui/buyer/BuyerScopeWorkflow$render$childRendering$4;

    invoke-direct {v5, v1}, Lcom/squareup/ui/buyer/BuyerScopeWorkflow$render$childRendering$4;-><init>(Lcom/squareup/ui/buyer/BuyerScopeInput;)V

    move-object v7, v5

    check-cast v7, Lkotlin/jvm/functions/Function1;

    const/4 v8, 0x4

    const/4 v9, 0x0

    move-object/from16 v3, p3

    move-object v5, v2

    .line 112
    invoke-static/range {v3 .. v9}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    goto :goto_0

    .line 143
    :cond_4
    instance-of v2, v1, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningSeparatedPrintouts;

    if-eqz v2, :cond_6

    .line 144
    iget-object v2, v0, Lcom/squareup/ui/buyer/BuyerScopeWorkflow;->separatedPrintoutsWorkflow:Ldagger/Lazy;

    invoke-interface {v2}, Ldagger/Lazy;->get()Ljava/lang/Object;

    move-result-object v2

    const-string v4, "separatedPrintoutsWorkflow.get()"

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v4, v2

    check-cast v4, Lcom/squareup/workflow/Workflow;

    check-cast v1, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningSeparatedPrintouts;

    invoke-virtual {v1}, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningSeparatedPrintouts;->getArgs()Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsInput;

    move-result-object v5

    const/4 v6, 0x0

    sget-object v1, Lcom/squareup/ui/buyer/BuyerScopeWorkflow$render$childRendering$5;->INSTANCE:Lcom/squareup/ui/buyer/BuyerScopeWorkflow$render$childRendering$5;

    move-object v7, v1

    check-cast v7, Lkotlin/jvm/functions/Function1;

    const/4 v8, 0x4

    const/4 v9, 0x0

    move-object/from16 v3, p3

    invoke-static/range {v3 .. v9}, Lcom/squareup/workflow/RenderContext$DefaultImpls;->renderChild$default(Lcom/squareup/workflow/RenderContext;Lcom/squareup/workflow/Workflow;Ljava/lang/Object;Ljava/lang/String;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    :goto_0
    if-eqz v1, :cond_5

    goto :goto_1

    .line 147
    :cond_5
    invoke-static {}, Lkotlin/collections/MapsKt;->emptyMap()Ljava/util/Map;

    move-result-object v1

    :goto_1
    const/4 v2, 0x0

    .line 149
    invoke-static {v1, v2}, Lcom/squareup/workflow/LayeredScreenKt;->withPersistence(Ljava/util/Map;Z)Ljava/util/Map;

    move-result-object v1

    return-object v1

    .line 144
    :cond_6
    new-instance v1, Lkotlin/NoWhenBranchMatchedException;

    invoke-direct {v1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    throw v1
.end method

.method public bridge synthetic snapshotState(Ljava/lang/Object;)Lcom/squareup/workflow/Snapshot;
    .locals 0

    .line 51
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyer/BuyerScopeWorkflow;->snapshotState(Lkotlin/Unit;)Lcom/squareup/workflow/Snapshot;

    move-result-object p1

    return-object p1
.end method

.method public snapshotState(Lkotlin/Unit;)Lcom/squareup/workflow/Snapshot;
    .locals 1

    const-string v0, "state"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 68
    sget-object p1, Lcom/squareup/workflow/Snapshot;->EMPTY:Lcom/squareup/workflow/Snapshot;

    return-object p1
.end method
