.class final Lcom/squareup/ui/buyer/signature/RefundPolicyDialogCard$1;
.super Lcom/squareup/container/ContainerTreeKey$PathCreator;
.source "RefundPolicyDialogCard.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyer/signature/RefundPolicyDialogCard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/container/ContainerTreeKey$PathCreator<",
        "Lcom/squareup/ui/buyer/signature/RefundPolicyDialogCard;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 69
    invoke-direct {p0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doCreateFromParcel(Landroid/os/Parcel;)Lcom/squareup/container/ContainerTreeKey;
    .locals 0

    .line 69
    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyer/signature/RefundPolicyDialogCard$1;->doCreateFromParcel(Landroid/os/Parcel;)Lcom/squareup/ui/buyer/signature/RefundPolicyDialogCard;

    move-result-object p1

    return-object p1
.end method

.method protected doCreateFromParcel(Landroid/os/Parcel;)Lcom/squareup/ui/buyer/signature/RefundPolicyDialogCard;
    .locals 1

    .line 71
    const-class v0, Lcom/squareup/ui/buyer/signature/RefundPolicy;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/buyer/signature/RefundPolicy;

    .line 72
    new-instance v0, Lcom/squareup/ui/buyer/signature/RefundPolicyDialogCard;

    invoke-direct {v0, p1}, Lcom/squareup/ui/buyer/signature/RefundPolicyDialogCard;-><init>(Lcom/squareup/ui/buyer/signature/RefundPolicy;)V

    return-object v0
.end method

.method public newArray(I)[Lcom/squareup/ui/buyer/signature/RefundPolicyDialogCard;
    .locals 0

    .line 76
    new-array p1, p1, [Lcom/squareup/ui/buyer/signature/RefundPolicyDialogCard;

    return-object p1
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 0

    .line 69
    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyer/signature/RefundPolicyDialogCard$1;->newArray(I)[Lcom/squareup/ui/buyer/signature/RefundPolicyDialogCard;

    move-result-object p1

    return-object p1
.end method
