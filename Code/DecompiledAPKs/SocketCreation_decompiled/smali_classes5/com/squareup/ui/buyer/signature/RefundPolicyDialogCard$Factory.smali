.class public Lcom/squareup/ui/buyer/signature/RefundPolicyDialogCard$Factory;
.super Ljava/lang/Object;
.source "RefundPolicyDialogCard.java"

# interfaces
.implements Lcom/squareup/workflow/DialogFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyer/signature/RefundPolicyDialogCard;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Factory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private createContentView(Landroid/content/Context;)Lcom/squareup/ui/DialogCardView;
    .locals 2

    .line 64
    sget v0, Lcom/squareup/ui/buyerflow/R$layout;->refund_policy_popup_view:I

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/DialogCardView;

    return-object p1
.end method


# virtual methods
.method public create(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    .line 36
    invoke-static {p1}, Lflow/path/Path;->get(Landroid/content/Context;)Lflow/path/Path;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/signature/RefundPolicyDialogCard;

    .line 37
    invoke-direct {p0, p1}, Lcom/squareup/ui/buyer/signature/RefundPolicyDialogCard$Factory;->createContentView(Landroid/content/Context;)Lcom/squareup/ui/DialogCardView;

    move-result-object v1

    .line 39
    sget v2, Lcom/squareup/ui/buyerflow/R$id;->refund_policy_popup_title:I

    invoke-static {v1, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 40
    iget-object v3, v0, Lcom/squareup/ui/buyer/signature/RefundPolicyDialogCard;->refundPolicy:Lcom/squareup/ui/buyer/signature/RefundPolicy;

    iget-object v3, v3, Lcom/squareup/ui/buyer/signature/RefundPolicy;->title:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 42
    sget v2, Lcom/squareup/ui/buyerflow/R$id;->refund_policy:I

    invoke-static {v1, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 43
    iget-object v0, v0, Lcom/squareup/ui/buyer/signature/RefundPolicyDialogCard;->refundPolicy:Lcom/squareup/ui/buyer/signature/RefundPolicy;

    iget-object v0, v0, Lcom/squareup/ui/buyer/signature/RefundPolicy;->policy:Ljava/lang/String;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 45
    new-instance v0, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    invoke-direct {v0, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 p1, 0x1

    .line 46
    invoke-virtual {v0, p1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setCancelable(Z)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    .line 47
    invoke-virtual {v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->clearWindowBackground()Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    .line 48
    invoke-virtual {v0, v1}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->setView(Landroid/view/View;)Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;

    move-result-object v0

    .line 49
    invoke-virtual {v0}, Lcom/squareup/widgets/dialog/ThemedAlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 51
    sget v2, Lcom/squareup/ui/buyerflow/R$id;->refund_policy_x_button:I

    invoke-static {v1, v2}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    .line 52
    new-instance v3, Lcom/squareup/ui/buyer/signature/RefundPolicyDialogCard$Factory$1;

    invoke-direct {v3, p0, v0}, Lcom/squareup/ui/buyer/signature/RefundPolicyDialogCard$Factory$1;-><init>(Lcom/squareup/ui/buyer/signature/RefundPolicyDialogCard$Factory;Landroid/app/Dialog;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 58
    invoke-virtual {v1, v0}, Lcom/squareup/ui/DialogCardView;->dialogUseFullScreenWidth(Landroid/app/Dialog;)V

    .line 59
    invoke-virtual {v0, p1}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 60
    invoke-static {v0}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
