.class public Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessageView;
.super Landroid/widget/FrameLayout;
.source "StarGroupMessageView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessageView$Component;
    }
.end annotation


# static fields
.field private static final ANIMATION_DELAY_MS:I = 0x3e8


# instance fields
.field private final delayAnimationRunnable:Ljava/lang/Runnable;

.field private fadeInAnimation:Landroid/view/animation/Animation;

.field private fadeOutAnimation:Landroid/view/animation/Animation;

.field private messageView:Landroid/widget/TextView;

.field presenter:Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessagePresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private starGroup:Lcom/squareup/ui/StarGroup;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 33
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    new-instance p2, Lcom/squareup/ui/buyer/emv/pinpad/-$$Lambda$StarGroupMessageView$EccmXw06rBKT99FUhhnZbk3jrqQ;

    invoke-direct {p2, p0}, Lcom/squareup/ui/buyer/emv/pinpad/-$$Lambda$StarGroupMessageView$EccmXw06rBKT99FUhhnZbk3jrqQ;-><init>(Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessageView;)V

    iput-object p2, p0, Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessageView;->delayAnimationRunnable:Ljava/lang/Runnable;

    .line 34
    const-class p2, Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessageView$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessageView$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessageView$Component;->inject(Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessageView;)V

    return-void
.end method

.method private animateShowMessage()V
    .locals 2

    .line 75
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessageView;->starGroup:Lcom/squareup/ui/StarGroup;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/squareup/ui/StarGroup;->setVisibility(I)V

    .line 76
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessageView;->messageView:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 77
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessageView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 78
    sget v1, Lcom/squareup/pinpad/R$anim;->fade_in_short:I

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessageView;->fadeInAnimation:Landroid/view/animation/Animation;

    .line 79
    sget v1, Lcom/squareup/pinpad/R$anim;->fade_out_short:I

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessageView;->fadeOutAnimation:Landroid/view/animation/Animation;

    .line 80
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessageView;->starGroup:Lcom/squareup/ui/StarGroup;

    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessageView;->fadeOutAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Lcom/squareup/ui/StarGroup;->startAnimation(Landroid/view/animation/Animation;)V

    .line 81
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessageView;->messageView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessageView;->fadeInAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method private cancelShowMessage()V
    .locals 2

    .line 85
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessageView;->getHandler()Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 87
    iget-object v1, p0, Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessageView;->delayAnimationRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 89
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessageView;->fadeInAnimation:Landroid/view/animation/Animation;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/view/animation/Animation;->cancel()V

    .line 90
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessageView;->fadeOutAnimation:Landroid/view/animation/Animation;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/view/animation/Animation;->cancel()V

    :cond_2
    return-void
.end method

.method public static synthetic lambda$EccmXw06rBKT99FUhhnZbk3jrqQ(Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessageView;)V
    .locals 0

    invoke-direct {p0}, Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessageView;->animateShowMessage()V

    return-void
.end method


# virtual methods
.method protected onDetachedFromWindow()V
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessageView;->presenter:Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessagePresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessagePresenter;->dropView(Ljava/lang/Object;)V

    .line 46
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 38
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 39
    sget v0, Lcom/squareup/pinpad/R$id;->star_group:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/StarGroup;

    iput-object v0, p0, Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessageView;->starGroup:Lcom/squareup/ui/StarGroup;

    .line 40
    sget v0, Lcom/squareup/pinpad/R$id;->pin_message:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessageView;->messageView:Landroid/widget/TextView;

    .line 41
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessageView;->presenter:Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessagePresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessagePresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public setStarCount(I)V
    .locals 1

    .line 50
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessageView;->starGroup:Lcom/squareup/ui/StarGroup;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/StarGroup;->setStarCount(I)V

    return-void
.end method

.method public setStarGroupCorrect(Z)V
    .locals 1

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessageView;->starGroup:Lcom/squareup/ui/StarGroup;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/StarGroup;->setCorrect(Z)V

    return-void
.end method

.method public showMessage(Z)V
    .locals 3

    .line 64
    invoke-direct {p0}, Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessageView;->cancelShowMessage()V

    .line 65
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessageView;->starGroup:Lcom/squareup/ui/StarGroup;

    invoke-virtual {v0}, Lcom/squareup/ui/StarGroup;->getVisibility()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_1

    .line 67
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessageView;->starGroup:Lcom/squareup/ui/StarGroup;

    invoke-virtual {p1, v1}, Lcom/squareup/ui/StarGroup;->setVisibility(I)V

    .line 68
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessageView;->messageView:Landroid/widget/TextView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 70
    :cond_1
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessageView;->getHandler()Landroid/os/Handler;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessageView;->delayAnimationRunnable:Ljava/lang/Runnable;

    const-wide/16 v1, 0x3e8

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :goto_0
    return-void
.end method

.method public showStarGroup()V
    .locals 2

    .line 58
    invoke-direct {p0}, Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessageView;->cancelShowMessage()V

    .line 59
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessageView;->starGroup:Lcom/squareup/ui/StarGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/ui/StarGroup;->setVisibility(I)V

    .line 60
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessageView;->messageView:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method
