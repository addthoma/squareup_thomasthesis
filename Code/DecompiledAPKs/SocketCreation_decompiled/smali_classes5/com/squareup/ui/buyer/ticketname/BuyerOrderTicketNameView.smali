.class public Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;
.super Landroid/widget/ScrollView;
.source "BuyerOrderTicketNameView.java"

# interfaces
.implements Lcom/squareup/container/spot/HasSpot;
.implements Lcom/squareup/workflow/ui/HandlesBack;


# instance fields
.field private buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

.field private cardholderNameStatus:Lcom/squareup/marketfont/MarketTextView;

.field private orderTicketName:Landroid/widget/EditText;

.field protected presenter:Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private progressView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 49
    invoke-direct {p0, p1, p2}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 50
    const-class p2, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameScreen$Component;->inject(Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;)V

    return-void
.end method

.method private bindViews()V
    .locals 1

    .line 163
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->buyer_action_bar:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    iput-object v0, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    .line 164
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->order_name_edit_text:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;->orderTicketName:Landroid/widget/EditText;

    .line 165
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->order_name_cardholder_name_status:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/marketfont/MarketTextView;

    iput-object v0, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;->cardholderNameStatus:Lcom/squareup/marketfont/MarketTextView;

    .line 166
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->order_name_fetch_progress:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;->progressView:Landroid/view/View;

    return-void
.end method


# virtual methods
.method getOrderTicketName()Ljava/lang/String;
    .locals 1

    .line 118
    iget-object v0, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;->orderTicketName:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 101
    sget-object p1, Lcom/squareup/ui/buyer/BuyerSpots;->BUYER_RIGHT:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method hideKeyboard()V
    .locals 1

    .line 135
    iget-object v0, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;->orderTicketName:Landroid/widget/EditText;

    invoke-static {v0}, Lcom/squareup/util/Views;->hideSoftKeyboard(Landroid/view/View;)V

    return-void
.end method

.method public synthetic lambda$onFinishInflate$0$BuyerOrderTicketNameView()Lkotlin/Unit;
    .locals 1

    .line 57
    iget-object v0, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;->presenter:Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->onBackPressed()Z

    .line 58
    sget-object v0, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object v0
.end method

.method public synthetic lambda$requestInitialFocus$1$BuyerOrderTicketNameView()V
    .locals 1

    .line 140
    iget-object v0, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;->orderTicketName:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 141
    iget-object v0, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;->orderTicketName:Landroid/widget/EditText;

    invoke-static {v0}, Lcom/squareup/util/Views;->showSoftKeyboard(Landroid/view/View;)V

    return-void
.end method

.method public onBackPressed()Z
    .locals 1

    .line 105
    iget-object v0, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;->presenter:Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;

    invoke-virtual {v0}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->onBackPressed()Z

    move-result v0

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 96
    iget-object v0, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;->presenter:Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->dropView(Ljava/lang/Object;)V

    .line 97
    invoke-super {p0}, Landroid/widget/ScrollView;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    .line 54
    invoke-super {p0}, Landroid/widget/ScrollView;->onFinishInflate()V

    .line 55
    invoke-direct {p0}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;->bindViews()V

    .line 56
    iget-object v0, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->X:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    new-instance v2, Lcom/squareup/ui/buyer/ticketname/-$$Lambda$BuyerOrderTicketNameView$PnGaTTrfDjU8JXIWc_AyRlquTys;

    invoke-direct {v2, p0}, Lcom/squareup/ui/buyer/ticketname/-$$Lambda$BuyerOrderTicketNameView$PnGaTTrfDjU8JXIWc_AyRlquTys;-><init>(Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;)V

    invoke-virtual {v0, v1, v2}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setupUpGlyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;Lkotlin/jvm/functions/Function0;)V

    .line 60
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->order_name_cardholder_name_status:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView$1;-><init>(Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;)V

    .line 61
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 66
    sget v0, Lcom/squareup/ui/buyerflow/R$id;->order_name_next_button:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView$2;

    invoke-direct {v1, p0}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView$2;-><init>(Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;)V

    .line 67
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 73
    iget-object v0, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;->orderTicketName:Landroid/widget/EditText;

    .line 74
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/squareup/common/strings/R$string;->save:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x6

    .line 73
    invoke-virtual {v0, v1, v2}, Landroid/widget/EditText;->setImeActionLabel(Ljava/lang/CharSequence;I)V

    .line 75
    iget-object v0, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;->orderTicketName:Landroid/widget/EditText;

    new-instance v1, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView$3;

    invoke-direct {v1, p0}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView$3;-><init>(Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 86
    new-instance v0, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    invoke-virtual {p0}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;-><init>(Landroid/content/res/Resources;)V

    sget-object v1, Lcom/squareup/glyph/GlyphTypeface$Glyph;->RECEIPT:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 87
    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->glyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    move-result-object v0

    sget v1, Lcom/squareup/marin/R$color;->marin_dark_gray:I

    .line 88
    invoke-virtual {v0, v1}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->colorId(I)Lcom/squareup/glyph/SquareGlyphDrawable$Builder;

    move-result-object v0

    .line 89
    invoke-virtual {v0}, Lcom/squareup/glyph/SquareGlyphDrawable$Builder;->build()Lcom/squareup/glyph/SquareGlyphDrawable;

    move-result-object v0

    .line 90
    iget-object v1, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;->orderTicketName:Landroid/widget/EditText;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2, v2, v2}, Landroid/widget/EditText;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 92
    iget-object v0, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;->presenter:Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNamePresenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method requestInitialFocus()V
    .locals 2

    .line 139
    iget-object v0, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;->orderTicketName:Landroid/widget/EditText;

    new-instance v1, Lcom/squareup/ui/buyer/ticketname/-$$Lambda$BuyerOrderTicketNameView$e0NwuLZqiDKjaR0obkT1UbphHEs;

    invoke-direct {v1, p0}, Lcom/squareup/ui/buyer/ticketname/-$$Lambda$BuyerOrderTicketNameView$e0NwuLZqiDKjaR0obkT1UbphHEs;-><init>(Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method selectAllText()V
    .locals 1

    .line 127
    iget-object v0, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;->orderTicketName:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->selectAll()V

    return-void
.end method

.method setCardholderClickable(Z)V
    .locals 3

    if-eqz p1, :cond_0

    .line 147
    iget-object v0, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;->cardholderNameStatus:Lcom/squareup/marketfont/MarketTextView;

    sget-object v1, Lcom/squareup/marketfont/MarketFont$Weight;->MEDIUM:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    .line 148
    iget-object v0, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;->cardholderNameStatus:Lcom/squareup/marketfont/MarketTextView;

    .line 149
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/squareup/marin/R$color;->marin_blue:I

    invoke-static {v1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    .line 148
    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setTextColor(I)V

    goto :goto_0

    .line 151
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;->cardholderNameStatus:Lcom/squareup/marketfont/MarketTextView;

    sget-object v1, Lcom/squareup/marketfont/MarketFont$Weight;->DEFAULT:Lcom/squareup/marketfont/MarketFont$Weight;

    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setWeight(Lcom/squareup/marketfont/MarketFont$Weight;)V

    .line 152
    iget-object v0, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;->cardholderNameStatus:Lcom/squareup/marketfont/MarketTextView;

    .line 153
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/squareup/marin/R$color;->marin_medium_gray:I

    invoke-static {v1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    .line 152
    invoke-virtual {v0, v1}, Lcom/squareup/marketfont/MarketTextView;->setTextColor(I)V

    .line 155
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;->cardholderNameStatus:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setClickable(Z)V

    return-void
.end method

.method setCardholderNameStatus(Ljava/lang/String;)V
    .locals 1

    .line 131
    iget-object v0, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;->cardholderNameStatus:Lcom/squareup/marketfont/MarketTextView;

    invoke-virtual {v0, p1}, Lcom/squareup/marketfont/MarketTextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method setOrderTicketName(Ljava/lang/String;)V
    .locals 1

    .line 122
    iget-object v0, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;->orderTicketName:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 123
    iget-object v0, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;->orderTicketName:Landroid/widget/EditText;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setSelection(I)V

    return-void
.end method

.method setProgressVisible(Z)V
    .locals 1

    .line 159
    iget-object v0, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;->progressView:Landroid/view/View;

    invoke-static {v0, p1}, Lcom/squareup/util/Views;->setVisibleOrGone(Landroid/view/View;Z)V

    return-void
.end method

.method public setSubtitle(Ljava/lang/CharSequence;)V
    .locals 1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    .line 113
    :cond_0
    new-instance v0, Lcom/squareup/util/ViewString$TextString;

    invoke-direct {v0, p1}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    move-object p1, v0

    .line 114
    :goto_0
    iget-object v0, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setSubtitle(Lcom/squareup/util/ViewString;)V

    return-void
.end method

.method public setTotal(Ljava/lang/CharSequence;)V
    .locals 2

    .line 109
    iget-object v0, p0, Lcom/squareup/ui/buyer/ticketname/BuyerOrderTicketNameView;->buyerActionBar:Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;

    new-instance v1, Lcom/squareup/util/ViewString$TextString;

    invoke-direct {v1, p1}, Lcom/squareup/util/ViewString$TextString;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, Lcom/squareup/ui/buyer/actionbar/BuyerNohoActionBar;->setTitle(Lcom/squareup/util/ViewString;)V

    return-void
.end method
