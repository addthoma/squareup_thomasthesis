.class public final Lcom/squareup/ui/buyer/emv/pinpad/PinPadDialogScreen_PinScreenModule_StarGroupPresenterFactory;
.super Ljava/lang/Object;
.source "PinPadDialogScreen_PinScreenModule_StarGroupPresenterFactory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessagePresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final module:Lcom/squareup/ui/buyer/emv/pinpad/PinPadDialogScreen$PinScreenModule;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/buyer/emv/pinpad/PinPadDialogScreen$PinScreenModule;)V
    .locals 0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPadDialogScreen_PinScreenModule_StarGroupPresenterFactory;->module:Lcom/squareup/ui/buyer/emv/pinpad/PinPadDialogScreen$PinScreenModule;

    return-void
.end method

.method public static create(Lcom/squareup/ui/buyer/emv/pinpad/PinPadDialogScreen$PinScreenModule;)Lcom/squareup/ui/buyer/emv/pinpad/PinPadDialogScreen_PinScreenModule_StarGroupPresenterFactory;
    .locals 1

    .line 30
    new-instance v0, Lcom/squareup/ui/buyer/emv/pinpad/PinPadDialogScreen_PinScreenModule_StarGroupPresenterFactory;

    invoke-direct {v0, p0}, Lcom/squareup/ui/buyer/emv/pinpad/PinPadDialogScreen_PinScreenModule_StarGroupPresenterFactory;-><init>(Lcom/squareup/ui/buyer/emv/pinpad/PinPadDialogScreen$PinScreenModule;)V

    return-object v0
.end method

.method public static starGroupPresenter(Lcom/squareup/ui/buyer/emv/pinpad/PinPadDialogScreen$PinScreenModule;)Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessagePresenter;
    .locals 1

    .line 35
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/emv/pinpad/PinPadDialogScreen$PinScreenModule;->starGroupPresenter()Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessagePresenter;

    move-result-object p0

    const-string v0, "Cannot return null from a non-@Nullable @Provides method"

    invoke-static {p0, v0}, Ldagger/internal/Preconditions;->checkNotNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessagePresenter;

    return-object p0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessagePresenter;
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/pinpad/PinPadDialogScreen_PinScreenModule_StarGroupPresenterFactory;->module:Lcom/squareup/ui/buyer/emv/pinpad/PinPadDialogScreen$PinScreenModule;

    invoke-static {v0}, Lcom/squareup/ui/buyer/emv/pinpad/PinPadDialogScreen_PinScreenModule_StarGroupPresenterFactory;->starGroupPresenter(Lcom/squareup/ui/buyer/emv/pinpad/PinPadDialogScreen$PinScreenModule;)Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessagePresenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 7
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/emv/pinpad/PinPadDialogScreen_PinScreenModule_StarGroupPresenterFactory;->get()Lcom/squareup/ui/buyer/emv/pinpad/StarGroupMessagePresenter;

    move-result-object v0

    return-object v0
.end method
