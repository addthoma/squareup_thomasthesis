.class final Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$filteredLoyaltyEvent$1;
.super Ljava/lang/Object;
.source "ReceiptEmailAndLoyaltyHelper.kt"

# interfaces
.implements Lrx/functions/Func1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->filteredLoyaltyEvent()Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func1<",
        "Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "event",
        "Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;


# direct methods
.method constructor <init>(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$filteredLoyaltyEvent$1;->this$0:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 64
    check-cast p1, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$filteredLoyaltyEvent$1;->call(Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;)Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1
.end method

.method public final call(Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;)Z
    .locals 2

    .line 362
    iget-object v0, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$filteredLoyaltyEvent$1;->this$0:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;

    invoke-static {v0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->access$getTransaction$p(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;)Lcom/squareup/payment/Transaction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->requireReceiptForLastPayment()Lcom/squareup/payment/PaymentReceipt;

    move-result-object v0

    const-string/jumbo v1, "transaction.requireReceiptForLastPayment()"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/squareup/payment/PaymentReceipt;->getTenderIdPair()Lcom/squareup/protos/client/IdPair;

    move-result-object v0

    .line 361
    invoke-virtual {p1, v0}, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;->matchesOtherTenderIdPair(Lcom/squareup/protos/client/IdPair;)Z

    move-result p1

    return p1
.end method
