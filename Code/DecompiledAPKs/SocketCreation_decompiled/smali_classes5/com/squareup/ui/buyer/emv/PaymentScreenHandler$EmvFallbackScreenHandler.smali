.class public abstract Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$EmvFallbackScreenHandler;
.super Lcom/squareup/ui/buyer/emv/PaymentScreenHandler;
.source "PaymentScreenHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyer/emv/PaymentScreenHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "EmvFallbackScreenHandler"
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final badBus:Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;

.field private final hudToaster:Lcom/squareup/hudtoaster/HudToaster;

.field private final messageId:I


# direct methods
.method public constructor <init>(Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;Lcom/squareup/hudtoaster/HudToaster;Lcom/squareup/ui/buyer/emv/EmvScope$Runner;ILcom/squareup/permissions/PermissionGatekeeper;)V
    .locals 0

    .line 81
    invoke-direct {p0, p3, p5}, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler;-><init>(Lcom/squareup/ui/buyer/emv/EmvScope$Runner;Lcom/squareup/permissions/PermissionGatekeeper;)V

    .line 82
    iput-object p1, p0, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$EmvFallbackScreenHandler;->badBus:Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;

    .line 83
    iput-object p2, p0, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$EmvFallbackScreenHandler;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    .line 84
    iput p4, p0, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$EmvFallbackScreenHandler;->messageId:I

    return-void
.end method

.method private onFailedSwipe()V
    .locals 2

    .line 105
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$EmvFallbackScreenHandler;->hudToaster:Lcom/squareup/hudtoaster/HudToaster;

    sget-object v1, Lcom/squareup/ui/cardreader/HudToasts;->SWIPE_FAILED_SWIPE_STRAIGHT:Lcom/squareup/ui/cardreader/HudToasts;

    invoke-interface {v0, v1}, Lcom/squareup/hudtoaster/HudToaster;->toastShortHud(Lcom/squareup/hudtoaster/HudToaster$ToastBundle;)Z

    return-void
.end method


# virtual methods
.method public from(Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;
    .locals 1

    .line 92
    new-instance p1, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    invoke-direct {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;-><init>()V

    sget-object v0, Lcom/squareup/glyph/GlyphTypeface$Glyph;->HUD_SWIPE:Lcom/squareup/glyph/GlyphTypeface$Glyph;

    .line 93
    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->glyph(Lcom/squareup/glyph/GlyphTypeface$Glyph;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/common/strings/R$string;->emv_fallback_title:I

    .line 94
    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->titleId(I)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    iget v0, p0, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$EmvFallbackScreenHandler;->messageId:I

    .line 95
    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->messageId(I)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 96
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$EmvFallbackScreenHandler;->cancelPaymentButton()Lcom/squareup/cardreader/ui/api/ButtonDescriptor;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->defaultButton(Lcom/squareup/cardreader/ui/api/ButtonDescriptor;)Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;

    move-result-object p1

    .line 97
    invoke-virtual {p1}, Lcom/squareup/cardreader/ui/api/ReaderWarningParameters$Builder;->build()Lcom/squareup/cardreader/ui/api/ReaderWarningParameters;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onEnterScope$0$PaymentScreenHandler$EmvFallbackScreenHandler(Lcom/squareup/wavpool/swipe/SwipeEvents$FailedSwipe;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 88
    invoke-direct {p0}, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$EmvFallbackScreenHandler;->onFailedSwipe()V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 88
    iget-object v0, p0, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$EmvFallbackScreenHandler;->badBus:Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;

    invoke-virtual {v0}, Lcom/squareup/wavpool/swipe/SwipeBusWhenVisible;->failedSwipes()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/buyer/emv/-$$Lambda$PaymentScreenHandler$EmvFallbackScreenHandler$hwQKpZRhYQd22qPEkos3_xJdu8I;

    invoke-direct {v1, p0}, Lcom/squareup/ui/buyer/emv/-$$Lambda$PaymentScreenHandler$EmvFallbackScreenHandler$hwQKpZRhYQd22qPEkos3_xJdu8I;-><init>(Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$EmvFallbackScreenHandler;)V

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method public processEmvCardInserted(Lcom/squareup/cardreader/CardReaderInfo;)V
    .locals 0

    .line 101
    iget-object p1, p0, Lcom/squareup/ui/buyer/emv/PaymentScreenHandler$EmvFallbackScreenHandler;->emvRunner:Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    invoke-virtual {p1}, Lcom/squareup/ui/buyer/emv/EmvScope$Runner;->goToPreparingPaymentScreen()V

    return-void
.end method
