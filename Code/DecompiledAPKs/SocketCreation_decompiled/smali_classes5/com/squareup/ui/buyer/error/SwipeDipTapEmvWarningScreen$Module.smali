.class public Lcom/squareup/ui/buyer/error/SwipeDipTapEmvWarningScreen$Module;
.super Ljava/lang/Object;
.source "SwipeDipTapEmvWarningScreen.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyer/error/SwipeDipTapEmvWarningScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Module"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/buyer/error/SwipeDipTapEmvWarningScreen;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/buyer/error/SwipeDipTapEmvWarningScreen;)V
    .locals 0

    .line 78
    iput-object p1, p0, Lcom/squareup/ui/buyer/error/SwipeDipTapEmvWarningScreen$Module;->this$0:Lcom/squareup/ui/buyer/error/SwipeDipTapEmvWarningScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method provideInitialViewData(Lcom/squareup/util/Res;)Lcom/squareup/ui/main/errors/WarningScreenData;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 80
    iget-object v0, p0, Lcom/squareup/ui/buyer/error/SwipeDipTapEmvWarningScreen$Module;->this$0:Lcom/squareup/ui/buyer/error/SwipeDipTapEmvWarningScreen;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/buyer/error/SwipeDipTapEmvWarningScreen;->getInitialScreenData(Lcom/squareup/util/Res;)Lcom/squareup/ui/main/errors/WarningScreenData;

    move-result-object p1

    return-object p1
.end method

.method providePaymentTakingWorkflow(Lcom/squareup/ui/main/errors/ButtonFlowStarter;Lcom/squareup/ui/main/errors/WarningScreenData;Lcom/squareup/ui/buyer/emv/EmvPaymentInputHandler;Lcom/squareup/ui/buyer/emv/EmvScope$Runner;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/main/SmartPaymentFlowStarter;)Lcom/squareup/ui/main/errors/PaymentTakingWarningWorkflow;
    .locals 9
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 89
    new-instance v8, Lcom/squareup/ui/buyer/error/EmvPaymentTakingWarningWorkflow;

    move-object v0, v8

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p5

    move-object v5, p4

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/buyer/error/EmvPaymentTakingWarningWorkflow;-><init>(Lcom/squareup/ui/main/errors/ButtonFlowStarter;Lcom/squareup/ui/main/errors/WarningScreenData;Lcom/squareup/ui/buyer/emv/EmvPaymentInputHandler;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/ui/buyer/emv/EmvScope$Runner;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/main/SmartPaymentFlowStarter;)V

    return-object v8
.end method
