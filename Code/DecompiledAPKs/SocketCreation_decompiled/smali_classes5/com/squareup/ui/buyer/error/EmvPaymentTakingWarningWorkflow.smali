.class public Lcom/squareup/ui/buyer/error/EmvPaymentTakingWarningWorkflow;
.super Lcom/squareup/ui/main/errors/PaymentTakingWarningWorkflow;
.source "EmvPaymentTakingWarningWorkflow.java"


# instance fields
.field private emvScopeRunner:Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

.field private final permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;


# direct methods
.method constructor <init>(Lcom/squareup/ui/main/errors/ButtonFlowStarter;Lcom/squareup/ui/main/errors/WarningScreenData;Lcom/squareup/ui/buyer/emv/EmvPaymentInputHandler;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/ui/buyer/emv/EmvScope$Runner;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/main/SmartPaymentFlowStarter;)V
    .locals 6

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p7

    .line 23
    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/main/errors/PaymentTakingWarningWorkflow;-><init>(Lcom/squareup/ui/main/errors/ButtonFlowStarter;Lcom/squareup/ui/main/errors/WarningScreenData;Lcom/squareup/ui/main/errors/PaymentInputHandler;Lcom/squareup/ui/tender/TenderStarter;Lcom/squareup/ui/main/SmartPaymentFlowStarter;)V

    .line 25
    iput-object p5, p0, Lcom/squareup/ui/buyer/error/EmvPaymentTakingWarningWorkflow;->emvScopeRunner:Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    .line 26
    iput-object p6, p0, Lcom/squareup/ui/buyer/error/EmvPaymentTakingWarningWorkflow;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/buyer/error/EmvPaymentTakingWarningWorkflow;)Lcom/squareup/ui/buyer/emv/EmvScope$Runner;
    .locals 0

    .line 15
    iget-object p0, p0, Lcom/squareup/ui/buyer/error/EmvPaymentTakingWarningWorkflow;->emvScopeRunner:Lcom/squareup/ui/buyer/emv/EmvScope$Runner;

    return-object p0
.end method


# virtual methods
.method protected handleButtonPressed(Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;)V
    .locals 2

    .line 30
    sget-object v0, Lcom/squareup/ui/buyer/error/EmvPaymentTakingWarningWorkflow$2;->$SwitchMap$com$squareup$ui$main$errors$WarningScreenButtonConfig$ButtonBehaviorType:[I

    invoke-virtual {p1}, Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 40
    invoke-super {p0, p1}, Lcom/squareup/ui/main/errors/PaymentTakingWarningWorkflow;->handleButtonPressed(Lcom/squareup/ui/main/errors/WarningScreenButtonConfig$ButtonBehaviorType;)V

    return-void

    .line 32
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/buyer/error/EmvPaymentTakingWarningWorkflow;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    sget-object v0, Lcom/squareup/permissions/Permission;->CANCEL_BUYER_FLOW:Lcom/squareup/permissions/Permission;

    new-instance v1, Lcom/squareup/ui/buyer/error/EmvPaymentTakingWarningWorkflow$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/buyer/error/EmvPaymentTakingWarningWorkflow$1;-><init>(Lcom/squareup/ui/buyer/error/EmvPaymentTakingWarningWorkflow;)V

    invoke-virtual {p1, v0, v1}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessExplicitlyGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    return-void
.end method
