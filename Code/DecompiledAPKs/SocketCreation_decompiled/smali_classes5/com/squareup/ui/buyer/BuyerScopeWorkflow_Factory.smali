.class public final Lcom/squareup/ui/buyer/BuyerScopeWorkflow_Factory;
.super Ljava/lang/Object;
.source "BuyerScopeWorkflow_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/buyer/BuyerScopeWorkflow;",
        ">;"
    }
.end annotation


# instance fields
.field private final billReceiptWorkflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/receipt/BillReceiptWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final billTipWorkflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/tip/BillTipWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final emoneyWorkflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final separatedPrintoutsWorkflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsWorkflow;",
            ">;"
        }
    .end annotation
.end field

.field private final storeAndForwardQuickEnableWorkflowProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/receipt/BillReceiptWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/tip/BillTipWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsWorkflow;",
            ">;)V"
        }
    .end annotation

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/squareup/ui/buyer/BuyerScopeWorkflow_Factory;->storeAndForwardQuickEnableWorkflowProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p2, p0, Lcom/squareup/ui/buyer/BuyerScopeWorkflow_Factory;->billReceiptWorkflowProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p3, p0, Lcom/squareup/ui/buyer/BuyerScopeWorkflow_Factory;->billTipWorkflowProvider:Ljavax/inject/Provider;

    .line 42
    iput-object p4, p0, Lcom/squareup/ui/buyer/BuyerScopeWorkflow_Factory;->emoneyWorkflowProvider:Ljavax/inject/Provider;

    .line 43
    iput-object p5, p0, Lcom/squareup/ui/buyer/BuyerScopeWorkflow_Factory;->separatedPrintoutsWorkflowProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/buyer/BuyerScopeWorkflow_Factory;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/receipt/BillReceiptWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/tip/BillTipWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingWorkflow;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsWorkflow;",
            ">;)",
            "Lcom/squareup/ui/buyer/BuyerScopeWorkflow_Factory;"
        }
    .end annotation

    .line 57
    new-instance v6, Lcom/squareup/ui/buyer/BuyerScopeWorkflow_Factory;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/buyer/BuyerScopeWorkflow_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v6
.end method

.method public static newInstance(Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow;Lcom/squareup/ui/buyer/receipt/BillReceiptWorkflow;Lcom/squareup/ui/buyer/tip/BillTipWorkflow;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingWorkflow;Ldagger/Lazy;)Lcom/squareup/ui/buyer/BuyerScopeWorkflow;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow;",
            "Lcom/squareup/ui/buyer/receipt/BillReceiptWorkflow;",
            "Lcom/squareup/ui/buyer/tip/BillTipWorkflow;",
            "Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingWorkflow;",
            "Ldagger/Lazy<",
            "Lcom/squareup/separatedprintouts/api/SeparatedPrintoutsWorkflow;",
            ">;)",
            "Lcom/squareup/ui/buyer/BuyerScopeWorkflow;"
        }
    .end annotation

    .line 65
    new-instance v6, Lcom/squareup/ui/buyer/BuyerScopeWorkflow;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/squareup/ui/buyer/BuyerScopeWorkflow;-><init>(Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow;Lcom/squareup/ui/buyer/receipt/BillReceiptWorkflow;Lcom/squareup/ui/buyer/tip/BillTipWorkflow;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingWorkflow;Ldagger/Lazy;)V

    return-object v6
.end method


# virtual methods
.method public get()Lcom/squareup/ui/buyer/BuyerScopeWorkflow;
    .locals 5

    .line 48
    iget-object v0, p0, Lcom/squareup/ui/buyer/BuyerScopeWorkflow_Factory;->storeAndForwardQuickEnableWorkflowProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow;

    iget-object v1, p0, Lcom/squareup/ui/buyer/BuyerScopeWorkflow_Factory;->billReceiptWorkflowProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/buyer/receipt/BillReceiptWorkflow;

    iget-object v2, p0, Lcom/squareup/ui/buyer/BuyerScopeWorkflow_Factory;->billTipWorkflowProvider:Ljavax/inject/Provider;

    invoke-interface {v2}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/squareup/ui/buyer/tip/BillTipWorkflow;

    iget-object v3, p0, Lcom/squareup/ui/buyer/BuyerScopeWorkflow_Factory;->emoneyWorkflowProvider:Ljavax/inject/Provider;

    invoke-interface {v3}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingWorkflow;

    iget-object v4, p0, Lcom/squareup/ui/buyer/BuyerScopeWorkflow_Factory;->separatedPrintoutsWorkflowProvider:Ljavax/inject/Provider;

    invoke-static {v4}, Ldagger/internal/DoubleCheck;->lazy(Ljavax/inject/Provider;)Ldagger/Lazy;

    move-result-object v4

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/ui/buyer/BuyerScopeWorkflow_Factory;->newInstance(Lcom/squareup/ui/buyer/storeandforward/StoreAndForwardQuickEnableWorkflow;Lcom/squareup/ui/buyer/receipt/BillReceiptWorkflow;Lcom/squareup/ui/buyer/tip/BillTipWorkflow;Lcom/squareup/checkoutflow/emoney/paymentprocessing/EmoneyPaymentProcessingWorkflow;Ldagger/Lazy;)Lcom/squareup/ui/buyer/BuyerScopeWorkflow;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/BuyerScopeWorkflow_Factory;->get()Lcom/squareup/ui/buyer/BuyerScopeWorkflow;

    move-result-object v0

    return-object v0
.end method
