.class public final Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter_Factory;
.super Ljava/lang/Object;
.source "LoyaltyPresenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final analyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;"
        }
    .end annotation
.end field

.field private final buyerScopeRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerScopeRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final checkoutInformationEventLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/CheckoutInformationEventLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final clockProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;"
        }
    .end annotation
.end field

.field private final curatedImageProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/merchantimages/CuratedImage;",
            ">;"
        }
    .end annotation
.end field

.field private final customerManagementSettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final employeeManagementProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;"
        }
    .end annotation
.end field

.field private final featuresProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;"
        }
    .end annotation
.end field

.field private final localeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation
.end field

.field private final loyaltyAnalyticsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltyAnalytics;",
            ">;"
        }
    .end annotation
.end field

.field private final loyaltyProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltyServiceHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final loyaltyRulesFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltyRulesFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final loyaltySettingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltySettings;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final offlineModeProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;"
        }
    .end annotation
.end field

.field private final phoneNumbersProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/PhoneNumberHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final picassoProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/picasso/Picasso;",
            ">;"
        }
    .end annotation
.end field

.field private final pointsFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/PointsTermsFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final retrofitQueueProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;"
        }
    .end annotation
.end field

.field private final settingsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final threadEnforcerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;"
        }
    .end annotation
.end field

.field private final topScreenCheckerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/TopScreenChecker;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/CheckoutInformationEventLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltyServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/PhoneNumberHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltyAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/TopScreenChecker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltySettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/merchantimages/CuratedImage;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/picasso/Picasso;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/PointsTermsFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltyRulesFormatter;",
            ">;)V"
        }
    .end annotation

    move-object v0, p0

    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 105
    iput-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter_Factory;->buyerScopeRunnerProvider:Ljavax/inject/Provider;

    move-object v1, p2

    .line 106
    iput-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter_Factory;->settingsProvider:Ljavax/inject/Provider;

    move-object v1, p3

    .line 107
    iput-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter_Factory;->checkoutInformationEventLoggerProvider:Ljavax/inject/Provider;

    move-object v1, p4

    .line 108
    iput-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter_Factory;->loyaltyProvider:Ljavax/inject/Provider;

    move-object v1, p5

    .line 109
    iput-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter_Factory;->phoneNumbersProvider:Ljavax/inject/Provider;

    move-object v1, p6

    .line 110
    iput-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    move-object v1, p7

    .line 111
    iput-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter_Factory;->loyaltyAnalyticsProvider:Ljavax/inject/Provider;

    move-object v1, p8

    .line 112
    iput-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    move-object v1, p9

    .line 113
    iput-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter_Factory;->clockProvider:Ljavax/inject/Provider;

    move-object v1, p10

    .line 114
    iput-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter_Factory;->customerManagementSettingsProvider:Ljavax/inject/Provider;

    move-object v1, p11

    .line 115
    iput-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    move-object v1, p12

    .line 116
    iput-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter_Factory;->retrofitQueueProvider:Ljavax/inject/Provider;

    move-object v1, p13

    .line 117
    iput-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter_Factory;->employeeManagementProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p14

    .line 118
    iput-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter_Factory;->transactionProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p15

    .line 119
    iput-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter_Factory;->topScreenCheckerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p16

    .line 120
    iput-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p17

    .line 121
    iput-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter_Factory;->threadEnforcerProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p18

    .line 122
    iput-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter_Factory;->localeProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p19

    .line 123
    iput-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter_Factory;->loyaltySettingsProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p20

    .line 124
    iput-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter_Factory;->offlineModeProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p21

    .line 125
    iput-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter_Factory;->curatedImageProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p22

    .line 126
    iput-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter_Factory;->picassoProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p23

    .line 127
    iput-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter_Factory;->pointsFormatterProvider:Ljavax/inject/Provider;

    move-object/from16 v1, p24

    .line 128
    iput-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter_Factory;->loyaltyRulesFormatterProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter_Factory;
    .locals 26
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/buyer/BuyerScopeRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/AccountStatusSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/CheckoutInformationEventLogger;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltyServiceHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/text/PhoneNumberHelper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltyAnalytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/analytics/Analytics;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Clock;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/crm/CustomerManagementSettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/settings/server/Features;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/queue/retrofit/RetrofitQueue;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/permissions/EmployeeManagement;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/Transaction;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/main/TopScreenChecker;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/thread/enforcer/ThreadEnforcer;",
            ">;",
            "Ljavax/inject/Provider<",
            "Ljava/util/Locale;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltySettings;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/payment/OfflineModeMonitor;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/merchantimages/CuratedImage;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/picasso/Picasso;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/PointsTermsFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/loyalty/LoyaltyRulesFormatter;",
            ">;)",
            "Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter_Factory;"
        }
    .end annotation

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    move-object/from16 v23, p22

    move-object/from16 v24, p23

    .line 153
    new-instance v25, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter_Factory;

    move-object/from16 v0, v25

    invoke-direct/range {v0 .. v24}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v25
.end method

.method public static newInstance(Lcom/squareup/ui/buyer/BuyerScopeRunner;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/log/CheckoutInformationEventLogger;Lcom/squareup/loyalty/LoyaltyServiceHelper;Lcom/squareup/text/PhoneNumberHelper;Lcom/squareup/util/Res;Lcom/squareup/loyalty/LoyaltyAnalytics;Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Clock;Lcom/squareup/crm/CustomerManagementSettings;Lcom/squareup/settings/server/Features;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/main/TopScreenChecker;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Ljava/util/Locale;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/merchantimages/CuratedImage;Lcom/squareup/picasso/Picasso;Lcom/squareup/loyalty/PointsTermsFormatter;Lcom/squareup/loyalty/LoyaltyRulesFormatter;)Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;
    .locals 26

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    move-object/from16 v17, p16

    move-object/from16 v18, p17

    move-object/from16 v19, p18

    move-object/from16 v20, p19

    move-object/from16 v21, p20

    move-object/from16 v22, p21

    move-object/from16 v23, p22

    move-object/from16 v24, p23

    .line 166
    new-instance v25, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;

    move-object/from16 v0, v25

    invoke-direct/range {v0 .. v24}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;-><init>(Lcom/squareup/ui/buyer/BuyerScopeRunner;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/log/CheckoutInformationEventLogger;Lcom/squareup/loyalty/LoyaltyServiceHelper;Lcom/squareup/text/PhoneNumberHelper;Lcom/squareup/util/Res;Lcom/squareup/loyalty/LoyaltyAnalytics;Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Clock;Lcom/squareup/crm/CustomerManagementSettings;Lcom/squareup/settings/server/Features;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/main/TopScreenChecker;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Ljava/util/Locale;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/merchantimages/CuratedImage;Lcom/squareup/picasso/Picasso;Lcom/squareup/loyalty/PointsTermsFormatter;Lcom/squareup/loyalty/LoyaltyRulesFormatter;)V

    return-object v25
.end method


# virtual methods
.method public get()Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;
    .locals 26

    move-object/from16 v0, p0

    .line 133
    iget-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter_Factory;->buyerScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/squareup/ui/buyer/BuyerScopeRunner;

    iget-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter_Factory;->settingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/squareup/settings/server/AccountStatusSettings;

    iget-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter_Factory;->checkoutInformationEventLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/squareup/log/CheckoutInformationEventLogger;

    iget-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter_Factory;->loyaltyProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v5, v1

    check-cast v5, Lcom/squareup/loyalty/LoyaltyServiceHelper;

    iget-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter_Factory;->phoneNumbersProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/squareup/text/PhoneNumberHelper;

    iget-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/squareup/util/Res;

    iget-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter_Factory;->loyaltyAnalyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/squareup/loyalty/LoyaltyAnalytics;

    iget-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter_Factory;->analyticsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/squareup/analytics/Analytics;

    iget-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter_Factory;->clockProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/squareup/util/Clock;

    iget-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter_Factory;->customerManagementSettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/squareup/crm/CustomerManagementSettings;

    iget-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter_Factory;->featuresProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v12, v1

    check-cast v12, Lcom/squareup/settings/server/Features;

    iget-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter_Factory;->retrofitQueueProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v13, v1

    check-cast v13, Lcom/squareup/queue/retrofit/RetrofitQueue;

    iget-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter_Factory;->employeeManagementProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Lcom/squareup/permissions/EmployeeManagement;

    iget-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter_Factory;->transactionProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, Lcom/squareup/payment/Transaction;

    iget-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter_Factory;->topScreenCheckerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Lcom/squareup/ui/main/TopScreenChecker;

    iget-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v17, v1

    check-cast v17, Lio/reactivex/Scheduler;

    iget-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter_Factory;->threadEnforcerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v18, v1

    check-cast v18, Lcom/squareup/thread/enforcer/ThreadEnforcer;

    iget-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter_Factory;->localeProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v19, v1

    check-cast v19, Ljava/util/Locale;

    iget-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter_Factory;->loyaltySettingsProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v20, v1

    check-cast v20, Lcom/squareup/loyalty/LoyaltySettings;

    iget-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter_Factory;->offlineModeProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v21, v1

    check-cast v21, Lcom/squareup/payment/OfflineModeMonitor;

    iget-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter_Factory;->curatedImageProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v22, v1

    check-cast v22, Lcom/squareup/merchantimages/CuratedImage;

    iget-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter_Factory;->picassoProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v23, v1

    check-cast v23, Lcom/squareup/picasso/Picasso;

    iget-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter_Factory;->pointsFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v24, v1

    check-cast v24, Lcom/squareup/loyalty/PointsTermsFormatter;

    iget-object v1, v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter_Factory;->loyaltyRulesFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v25, v1

    check-cast v25, Lcom/squareup/loyalty/LoyaltyRulesFormatter;

    invoke-static/range {v2 .. v25}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter_Factory;->newInstance(Lcom/squareup/ui/buyer/BuyerScopeRunner;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/log/CheckoutInformationEventLogger;Lcom/squareup/loyalty/LoyaltyServiceHelper;Lcom/squareup/text/PhoneNumberHelper;Lcom/squareup/util/Res;Lcom/squareup/loyalty/LoyaltyAnalytics;Lcom/squareup/analytics/Analytics;Lcom/squareup/util/Clock;Lcom/squareup/crm/CustomerManagementSettings;Lcom/squareup/settings/server/Features;Lcom/squareup/queue/retrofit/RetrofitQueue;Lcom/squareup/permissions/EmployeeManagement;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/main/TopScreenChecker;Lio/reactivex/Scheduler;Lcom/squareup/thread/enforcer/ThreadEnforcer;Ljava/util/Locale;Lcom/squareup/loyalty/LoyaltySettings;Lcom/squareup/payment/OfflineModeMonitor;Lcom/squareup/merchantimages/CuratedImage;Lcom/squareup/picasso/Picasso;Lcom/squareup/loyalty/PointsTermsFormatter;Lcom/squareup/loyalty/LoyaltyRulesFormatter;)Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;

    move-result-object v1

    return-object v1
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 31
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter_Factory;->get()Lcom/squareup/ui/buyer/loyalty/LoyaltyPresenter;

    move-result-object v0

    return-object v0
.end method
