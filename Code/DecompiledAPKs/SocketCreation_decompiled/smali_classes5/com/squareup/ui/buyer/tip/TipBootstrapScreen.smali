.class public final Lcom/squareup/ui/buyer/tip/TipBootstrapScreen;
.super Lcom/squareup/container/BootstrapTreeKey;
.source "TipBootstrapScreen.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H\u0016J\u0008\u0010\t\u001a\u00020\u0003H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/ui/buyer/tip/TipBootstrapScreen;",
        "Lcom/squareup/container/BootstrapTreeKey;",
        "buyerScope",
        "Lcom/squareup/ui/buyer/BuyerScope;",
        "(Lcom/squareup/ui/buyer/BuyerScope;)V",
        "doRegister",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "getParentKey",
        "buyer-flow_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final buyerScope:Lcom/squareup/ui/buyer/BuyerScope;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/buyer/BuyerScope;)V
    .locals 1

    const-string v0, "buyerScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    invoke-direct {p0}, Lcom/squareup/container/BootstrapTreeKey;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/buyer/tip/TipBootstrapScreen;->buyerScope:Lcom/squareup/ui/buyer/BuyerScope;

    return-void
.end method


# virtual methods
.method public doRegister(Lmortar/MortarScope;)V
    .locals 1

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    sget-object v0, Lcom/squareup/ui/buyer/workflow/BuyerFlowWorkflowRunner;->Companion:Lcom/squareup/ui/buyer/workflow/BuyerFlowWorkflowRunner$Companion;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/buyer/workflow/BuyerFlowWorkflowRunner$Companion;->get(Lmortar/MortarScope;)Lcom/squareup/ui/buyer/workflow/BuyerFlowWorkflowRunner;

    move-result-object p1

    .line 15
    sget-object v0, Lcom/squareup/ui/buyer/BuyerScopeInput$RunningTip;->INSTANCE:Lcom/squareup/ui/buyer/BuyerScopeInput$RunningTip;

    check-cast v0, Lcom/squareup/ui/buyer/BuyerScopeInput;

    invoke-interface {p1, v0}, Lcom/squareup/ui/buyer/workflow/BuyerFlowWorkflowRunner;->start(Lcom/squareup/ui/buyer/BuyerScopeInput;)V

    return-void
.end method

.method public getParentKey()Lcom/squareup/ui/buyer/BuyerScope;
    .locals 1

    .line 11
    iget-object v0, p0, Lcom/squareup/ui/buyer/tip/TipBootstrapScreen;->buyerScope:Lcom/squareup/ui/buyer/BuyerScope;

    return-object v0
.end method

.method public bridge synthetic getParentKey()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/ui/buyer/tip/TipBootstrapScreen;->getParentKey()Lcom/squareup/ui/buyer/BuyerScope;

    move-result-object v0

    return-object v0
.end method
