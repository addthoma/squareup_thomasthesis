.class public final Lcom/squareup/ui/buyer/loyalty/LoyaltyScreen;
.super Lcom/squareup/ui/main/InBuyerScope;
.source "LoyaltyScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/container/spot/HasSpot;
.implements Lcom/squareup/ui/buyer/PaymentExempt;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/buyer/loyalty/LoyaltyScreen$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/buyer/loyalty/LoyaltyScreen$Component;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/buyer/loyalty/LoyaltyScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final loyaltyEvent:Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 56
    sget-object v0, Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyScreen$cK8Ya3zIXlkywYDMVe6th0zQHv4;->INSTANCE:Lcom/squareup/ui/buyer/loyalty/-$$Lambda$LoyaltyScreen$cK8Ya3zIXlkywYDMVe6th0zQHv4;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/buyer/BuyerScope;Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;)V
    .locals 0

    .line 29
    invoke-direct {p0, p1}, Lcom/squareup/ui/main/InBuyerScope;-><init>(Lcom/squareup/ui/buyer/BuyerScope;)V

    .line 30
    iput-object p2, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyScreen;->loyaltyEvent:Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/buyer/loyalty/LoyaltyScreen;
    .locals 2

    .line 57
    const-class v0, Lcom/squareup/ui/buyer/BuyerScope;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/buyer/BuyerScope;

    .line 58
    new-instance v0, Lcom/squareup/ui/buyer/loyalty/LoyaltyScreen;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/ui/buyer/loyalty/LoyaltyScreen;-><init>(Lcom/squareup/ui/buyer/BuyerScope;Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .line 52
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/main/InBuyerScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 53
    iget-object v0, p0, Lcom/squareup/ui/buyer/loyalty/LoyaltyScreen;->buyerPath:Lcom/squareup/ui/buyer/BuyerScope;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method

.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 34
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->LOYALTY:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getSpot(Landroid/content/Context;)Lcom/squareup/container/spot/Spot;
    .locals 0

    .line 42
    sget-object p1, Lcom/squareup/container/spot/Spots;->HERE_CROSS_FADE:Lcom/squareup/container/spot/Spot;

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 38
    sget v0, Lcom/squareup/ui/buyerflow/R$layout;->loyalty_view:I

    return v0
.end method
