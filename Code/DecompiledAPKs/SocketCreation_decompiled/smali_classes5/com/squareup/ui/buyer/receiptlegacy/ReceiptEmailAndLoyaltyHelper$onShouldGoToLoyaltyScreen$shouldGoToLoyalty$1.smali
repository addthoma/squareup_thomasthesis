.class final Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$onShouldGoToLoyaltyScreen$shouldGoToLoyalty$1;
.super Ljava/lang/Object;
.source "ReceiptEmailAndLoyaltyHelper.kt"

# interfaces
.implements Lrx/functions/Func3;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper;->onShouldGoToLoyaltyScreen()Lrx/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T1:",
        "Ljava/lang/Object;",
        "T2:",
        "Ljava/lang/Object;",
        "T3:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Func3<",
        "TT1;TT2;TT3;TR;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0008\u0003\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u000e\u0010\u0003\u001a\n \u0002*\u0004\u0018\u00010\u00040\u00042\u000e\u0010\u0005\u001a\n \u0002*\u0004\u0018\u00010\u00060\u00062\u000e\u0010\u0007\u001a\n \u0002*\u0004\u0018\u00010\u00060\u0006H\n\u00a2\u0006\u0004\u0008\u0008\u0010\t"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/loyalty/MaybeLoyaltyEvent;",
        "kotlin.jvm.PlatformType",
        "loyaltyEvent",
        "Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;",
        "isUnobscured",
        "",
        "isFinishing",
        "call",
        "(Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;Ljava/lang/Boolean;Ljava/lang/Boolean;)Lcom/squareup/loyalty/MaybeLoyaltyEvent;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$onShouldGoToLoyaltyScreen$shouldGoToLoyalty$1;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$onShouldGoToLoyaltyScreen$shouldGoToLoyalty$1;

    invoke-direct {v0}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$onShouldGoToLoyaltyScreen$shouldGoToLoyalty$1;-><init>()V

    sput-object v0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$onShouldGoToLoyaltyScreen$shouldGoToLoyalty$1;->INSTANCE:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$onShouldGoToLoyaltyScreen$shouldGoToLoyalty$1;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;Ljava/lang/Boolean;Ljava/lang/Boolean;)Lcom/squareup/loyalty/MaybeLoyaltyEvent;
    .locals 1

    const-string v0, "isUnobscured"

    .line 212
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    if-nez p2, :cond_0

    .line 213
    check-cast p1, Lcom/squareup/loyalty/MaybeLoyaltyEvent;

    goto :goto_0

    .line 215
    :cond_0
    new-instance p1, Lcom/squareup/loyalty/MaybeLoyaltyEvent$NonLoyaltyEvent;

    const/4 p2, 0x3

    const/4 p3, 0x0

    invoke-direct {p1, p3, p3, p2, p3}, Lcom/squareup/loyalty/MaybeLoyaltyEvent$NonLoyaltyEvent;-><init>(Lcom/squareup/protos/client/IdPair;Lcom/squareup/protos/client/IdPair;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    check-cast p1, Lcom/squareup/loyalty/MaybeLoyaltyEvent;

    :goto_0
    return-object p1
.end method

.method public bridge synthetic call(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 64
    check-cast p1, Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;

    check-cast p2, Ljava/lang/Boolean;

    check-cast p3, Ljava/lang/Boolean;

    invoke-virtual {p0, p1, p2, p3}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptEmailAndLoyaltyHelper$onShouldGoToLoyaltyScreen$shouldGoToLoyalty$1;->call(Lcom/squareup/loyalty/MaybeLoyaltyEvent$LoyaltyEvent;Ljava/lang/Boolean;Ljava/lang/Boolean;)Lcom/squareup/loyalty/MaybeLoyaltyEvent;

    move-result-object p1

    return-object p1
.end method
