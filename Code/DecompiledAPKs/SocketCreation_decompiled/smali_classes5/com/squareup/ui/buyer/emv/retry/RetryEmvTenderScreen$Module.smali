.class public abstract Lcom/squareup/ui/buyer/emv/retry/RetryEmvTenderScreen$Module;
.super Ljava/lang/Object;
.source "RetryEmvTenderScreen.kt"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/buyer/emv/retry/RetryEmvTenderScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401
    name = "Module"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u00a7\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\'\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/ui/buyer/emv/retry/RetryEmvTenderScreen$Module;",
        "",
        "(Lcom/squareup/ui/buyer/emv/retry/RetryEmvTenderScreen;)V",
        "bindRetryTenderStrategy",
        "Lcom/squareup/ui/buyer/retry/RetryTenderPresenter$RetryTenderStrategy;",
        "strategy",
        "Lcom/squareup/ui/buyer/emv/retry/RetryEmvTenderStrategy;",
        "buyer-flow_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/buyer/emv/retry/RetryEmvTenderScreen;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/buyer/emv/retry/RetryEmvTenderScreen;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 30
    iput-object p1, p0, Lcom/squareup/ui/buyer/emv/retry/RetryEmvTenderScreen$Module;->this$0:Lcom/squareup/ui/buyer/emv/retry/RetryEmvTenderScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract bindRetryTenderStrategy(Lcom/squareup/ui/buyer/emv/retry/RetryEmvTenderStrategy;)Lcom/squareup/ui/buyer/retry/RetryTenderPresenter$RetryTenderStrategy;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
