.class Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView$15;
.super Lcom/squareup/debounce/DebouncedOnEditorActionListener;
.source "ReceiptPhoneView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->resourcesUpdated(Lcom/squareup/locale/LocaleOverrideFactory;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;

.field final synthetic val$buyerLocaleOverride:Lcom/squareup/locale/LocaleOverrideFactory;


# direct methods
.method constructor <init>(Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;Lcom/squareup/locale/LocaleOverrideFactory;)V
    .locals 0

    .line 588
    iput-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView$15;->this$0:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;

    iput-object p2, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView$15;->val$buyerLocaleOverride:Lcom/squareup/locale/LocaleOverrideFactory;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnEditorActionListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doOnEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 0

    const/4 p1, 0x4

    if-ne p2, p1, :cond_0

    .line 591
    iget-object p1, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView$15;->this$0:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;

    iget-object p1, p1, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView;->presenter:Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;

    iget-object p2, p0, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhoneView$15;->val$buyerLocaleOverride:Lcom/squareup/locale/LocaleOverrideFactory;

    invoke-virtual {p1, p2}, Lcom/squareup/ui/buyer/receiptlegacy/ReceiptPhonePresenter;->sendEmailReceiptClicked(Lcom/squareup/locale/LocaleOverrideFactory;)V

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method
