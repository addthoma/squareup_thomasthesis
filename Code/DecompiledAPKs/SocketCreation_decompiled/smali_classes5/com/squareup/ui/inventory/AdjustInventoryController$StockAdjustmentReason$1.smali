.class final Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason$1;
.super Ljava/lang/Object;
.source "AdjustInventoryController.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator<",
        "Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 540
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;
    .locals 3

    .line 542
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 544
    invoke-static {}, Lcom/squareup/protos/client/InventoryAdjustmentReason;->values()[Lcom/squareup/protos/client/InventoryAdjustmentReason;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    aget-object v1, v1, v2

    .line 545
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result p1

    const/4 v2, 0x1

    if-ne p1, v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    .line 546
    :goto_0
    new-instance p1, Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;

    invoke-direct {p1, v0, v1, v2}, Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;-><init>(ILcom/squareup/protos/client/InventoryAdjustmentReason;Z)V

    return-object p1
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 0

    .line 540
    invoke-virtual {p0, p1}, Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason$1;->createFromParcel(Landroid/os/Parcel;)Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;

    move-result-object p1

    return-object p1
.end method

.method public newArray(I)[Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;
    .locals 0

    .line 550
    new-array p1, p1, [Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;

    return-object p1
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 0

    .line 540
    invoke-virtual {p0, p1}, Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason$1;->newArray(I)[Lcom/squareup/ui/inventory/AdjustInventoryController$StockAdjustmentReason;

    move-result-object p1

    return-object p1
.end method
