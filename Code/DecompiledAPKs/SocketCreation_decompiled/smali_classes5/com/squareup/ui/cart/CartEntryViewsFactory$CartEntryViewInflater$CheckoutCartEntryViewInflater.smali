.class public final Lcom/squareup/ui/cart/CartEntryViewsFactory$CartEntryViewInflater$CheckoutCartEntryViewInflater;
.super Ljava/lang/Object;
.source "CartEntryViewsFactory.kt"

# interfaces
.implements Lcom/squareup/ui/cart/CartEntryViewsFactory$CartEntryViewInflater;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/cart/CartEntryViewsFactory$CartEntryViewInflater;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CheckoutCartEntryViewInflater"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0007\u0008\u0001\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/ui/cart/CartEntryViewsFactory$CartEntryViewInflater$CheckoutCartEntryViewInflater;",
        "Lcom/squareup/ui/cart/CartEntryViewsFactory$CartEntryViewInflater;",
        "()V",
        "inflate",
        "Lcom/squareup/ui/cart/CartEntryView;",
        "context",
        "Landroid/content/Context;",
        "checkout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public inflate(Landroid/content/Context;)Lcom/squareup/ui/cart/CartEntryView;
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    sget v0, Lcom/squareup/checkout/R$layout;->cart_entry_row:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->inflate(Landroid/content/Context;I)Landroid/view/View;

    move-result-object p1

    if-eqz p1, :cond_0

    check-cast p1, Lcom/squareup/ui/cart/CartEntryView;

    return-object p1

    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.squareup.ui.cart.CartEntryView"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public mapToEntryViews(Landroid/content/Context;Lcom/squareup/ui/cart/CartEntryViewModel;)Lcom/squareup/ui/cart/CartEntryView;
    .locals 1

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "model"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    invoke-static {p0, p1, p2}, Lcom/squareup/ui/cart/CartEntryViewsFactory$CartEntryViewInflater$DefaultImpls;->mapToEntryViews(Lcom/squareup/ui/cart/CartEntryViewsFactory$CartEntryViewInflater;Landroid/content/Context;Lcom/squareup/ui/cart/CartEntryViewModel;)Lcom/squareup/ui/cart/CartEntryView;

    move-result-object p1

    return-object p1
.end method
