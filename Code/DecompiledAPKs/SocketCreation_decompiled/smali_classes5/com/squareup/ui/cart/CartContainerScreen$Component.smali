.class public interface abstract Lcom/squareup/ui/cart/CartContainerScreen$Component;
.super Ljava/lang/Object;
.source "CartContainerScreen.java"

# interfaces
.implements Lcom/squareup/marin/widgets/MarinActionBarView$Component;
.implements Lcom/squareup/ui/cart/CartComponent;


# annotations
.annotation runtime Ldagger/Subcomponent;
    modules = {
        Lcom/squareup/marin/widgets/MarinActionBarModule;,
        Lcom/squareup/ui/cart/CartContainerScreen$Module;
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/cart/CartContainerScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Component"
.end annotation


# virtual methods
.method public abstract inject(Lcom/squareup/ui/cart/CartContainerView;)V
.end method
