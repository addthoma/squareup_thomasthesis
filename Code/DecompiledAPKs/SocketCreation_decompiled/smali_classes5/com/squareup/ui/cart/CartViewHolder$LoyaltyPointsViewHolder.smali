.class Lcom/squareup/ui/cart/CartViewHolder$LoyaltyPointsViewHolder;
.super Lcom/squareup/ui/cart/CartViewHolder;
.source "CartViewHolder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/cart/CartViewHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "LoyaltyPointsViewHolder"
.end annotation


# instance fields
.field private final cartPresenter:Lcom/squareup/ui/cart/CartRecyclerViewPresenter;

.field private final entryView:Lcom/squareup/ui/cart/CartEntryView;


# direct methods
.method constructor <init>(Landroid/view/View;Lcom/squareup/ui/cart/CartRecyclerViewPresenter;Lcom/squareup/ui/cart/CartEntryViewModelFactory;)V
    .locals 0

    .line 260
    invoke-direct {p0, p1, p3}, Lcom/squareup/ui/cart/CartViewHolder;-><init>(Landroid/view/View;Lcom/squareup/ui/cart/CartEntryViewModelFactory;)V

    .line 261
    iput-object p2, p0, Lcom/squareup/ui/cart/CartViewHolder$LoyaltyPointsViewHolder;->cartPresenter:Lcom/squareup/ui/cart/CartRecyclerViewPresenter;

    .line 262
    check-cast p1, Lcom/squareup/ui/cart/CartEntryView;

    iput-object p1, p0, Lcom/squareup/ui/cart/CartViewHolder$LoyaltyPointsViewHolder;->entryView:Lcom/squareup/ui/cart/CartEntryView;

    return-void
.end method

.method static synthetic access$300(Lcom/squareup/ui/cart/CartViewHolder$LoyaltyPointsViewHolder;)Lcom/squareup/ui/cart/CartRecyclerViewPresenter;
    .locals 0

    .line 254
    iget-object p0, p0, Lcom/squareup/ui/cart/CartViewHolder$LoyaltyPointsViewHolder;->cartPresenter:Lcom/squareup/ui/cart/CartRecyclerViewPresenter;

    return-object p0
.end method


# virtual methods
.method bind(Lcom/squareup/ui/cart/CartAdapterItem;)V
    .locals 3

    .line 266
    check-cast p1, Lcom/squareup/ui/cart/CartAdapterItem$LoyaltyPointsRow;

    .line 267
    iget-object v0, p0, Lcom/squareup/ui/cart/CartViewHolder$LoyaltyPointsViewHolder;->cartEntryViewModelFactory:Lcom/squareup/ui/cart/CartEntryViewModelFactory;

    sget v1, Lcom/squareup/orderentry/R$string;->loyalty_cart_row_label:I

    iget-object v2, p1, Lcom/squareup/ui/cart/CartAdapterItem$LoyaltyPointsRow;->subLabel:Ljava/lang/Integer;

    iget-object p1, p1, Lcom/squareup/ui/cart/CartAdapterItem$LoyaltyPointsRow;->formattedValue:Ljava/lang/String;

    .line 268
    invoke-interface {v0, v1, v2, p1}, Lcom/squareup/ui/cart/CartEntryViewModelFactory;->loyaltyPoints(ILjava/lang/Integer;Ljava/lang/String;)Lcom/squareup/ui/cart/CartEntryViewModel;

    move-result-object p1

    .line 271
    iget-object v0, p0, Lcom/squareup/ui/cart/CartViewHolder$LoyaltyPointsViewHolder;->entryView:Lcom/squareup/ui/cart/CartEntryView;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/cart/CartEntryView;->present(Lcom/squareup/ui/cart/CartEntryViewModel;)V

    .line 272
    iget-object p1, p0, Lcom/squareup/ui/cart/CartViewHolder$LoyaltyPointsViewHolder;->entryView:Lcom/squareup/ui/cart/CartEntryView;

    new-instance v0, Lcom/squareup/ui/cart/CartViewHolder$LoyaltyPointsViewHolder$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/cart/CartViewHolder$LoyaltyPointsViewHolder$1;-><init>(Lcom/squareup/ui/cart/CartViewHolder$LoyaltyPointsViewHolder;)V

    invoke-virtual {p1, v0}, Lcom/squareup/ui/cart/CartEntryView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method
