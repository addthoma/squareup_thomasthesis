.class public Lcom/squareup/ui/cart/CartTaxesView;
.super Lcom/squareup/ui/cart/AbstractCartFeesView;
.source "CartTaxesView.java"

# interfaces
.implements Lcom/squareup/container/VisualTransitionListener;


# instance fields
.field presenter:Lcom/squareup/ui/cart/CartTaxesScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 15
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/cart/AbstractCartFeesView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 16
    const-class p2, Lcom/squareup/ui/cart/CartTaxesScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/cart/CartTaxesScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/cart/CartTaxesScreen$Component;->inject(Lcom/squareup/ui/cart/CartTaxesView;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic getPresenter()Lcom/squareup/ui/cart/AbstractCartFeesPresenter;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/ui/cart/CartTaxesView;->getPresenter()Lcom/squareup/ui/cart/CartTaxesScreen$Presenter;

    move-result-object v0

    return-object v0
.end method

.method protected getPresenter()Lcom/squareup/ui/cart/CartTaxesScreen$Presenter;
    .locals 1

    .line 30
    iget-object v0, p0, Lcom/squareup/ui/cart/CartTaxesView;->presenter:Lcom/squareup/ui/cart/CartTaxesScreen$Presenter;

    return-object v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 25
    iget-object v0, p0, Lcom/squareup/ui/cart/CartTaxesView;->presenter:Lcom/squareup/ui/cart/CartTaxesScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/cart/CartTaxesScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 26
    invoke-super {p0}, Lcom/squareup/ui/cart/AbstractCartFeesView;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    .line 20
    invoke-super {p0}, Lcom/squareup/ui/cart/AbstractCartFeesView;->onFinishInflate()V

    .line 21
    iget-object v0, p0, Lcom/squareup/ui/cart/CartTaxesView;->presenter:Lcom/squareup/ui/cart/CartTaxesScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/cart/CartTaxesScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method

.method public onStartVisualTransition()V
    .locals 1

    .line 34
    iget-object v0, p0, Lcom/squareup/ui/cart/CartTaxesView;->presenter:Lcom/squareup/ui/cart/CartTaxesScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/cart/CartTaxesScreen$Presenter;->onStartVisualTransition()V

    return-void
.end method
