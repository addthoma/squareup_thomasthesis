.class public final Lcom/squareup/ui/cart/CartEntryViewModels$Companion;
.super Ljava/lang/Object;
.source "CartEntryViewModels.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/cart/CartEntryViewModels;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0008\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002J\u0008\u0010\u0003\u001a\u00020\u0004H\u0007\u00a8\u0006\u0005"
    }
    d2 = {
        "Lcom/squareup/ui/cart/CartEntryViewModels$Companion;",
        "",
        "()V",
        "empty",
        "Lcom/squareup/ui/cart/CartEntryViewModels;",
        "checkout_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 13
    invoke-direct {p0}, Lcom/squareup/ui/cart/CartEntryViewModels$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final empty()Lcom/squareup/ui/cart/CartEntryViewModels;
    .locals 9
    .annotation runtime Lkotlin/jvm/JvmStatic;
    .end annotation

    .line 14
    new-instance v8, Lcom/squareup/ui/cart/CartEntryViewModels;

    .line 15
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v1

    .line 17
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v3

    .line 18
    invoke-static {}, Lkotlin/collections/CollectionsKt;->emptyList()Ljava/util/List;

    move-result-object v4

    const/4 v2, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, v8

    .line 14
    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/cart/CartEntryViewModels;-><init>(Ljava/util/List;Lcom/squareup/ui/cart/CartEntryViewModel;Ljava/util/List;Ljava/util/List;Lcom/squareup/ui/cart/CartEntryViewModel;Lcom/squareup/ui/cart/CartEntryViewModel;Lcom/squareup/ui/cart/CartEntryViewModel;)V

    return-object v8
.end method
