.class Lcom/squareup/ui/cart/CartTaxesScreen$Presenter;
.super Lcom/squareup/ui/cart/AbstractCartFeesPresenter;
.source "CartTaxesScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/cart/CartTaxesScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Presenter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/cart/AbstractCartFeesPresenter<",
        "Lcom/squareup/ui/cart/CartTaxesView;",
        ">;"
    }
.end annotation


# instance fields
.field private final accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private final cartEditor:Lcom/squareup/ui/cart/CartFeesModel$Session;

.field private final cartScreenRunner:Lcom/squareup/ui/cart/CartScreenRunner;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

.field private final transaction:Lcom/squareup/payment/Transaction;

.field private final transactionInteractionsLogger:Lcom/squareup/log/cart/TransactionInteractionsLogger;


# direct methods
.method constructor <init>(Lcom/squareup/marin/widgets/MarinActionBar;Lcom/squareup/ui/cart/CartFeesModel$Session;Lcom/squareup/ui/cart/CartScreenRunner;Lcom/squareup/util/Res;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/payment/Transaction;Lcom/squareup/ui/cart/TaxFormatter;Lcom/squareup/settings/server/Features;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/log/cart/TransactionInteractionsLogger;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 76
    invoke-direct {p0, p1, p2, p4, p7}, Lcom/squareup/ui/cart/AbstractCartFeesPresenter;-><init>(Lcom/squareup/marin/widgets/MarinActionBar;Lcom/squareup/ui/cart/CartFeesModel$Session;Lcom/squareup/util/Res;Lcom/squareup/text/Formatter;)V

    .line 77
    iput-object p1, p0, Lcom/squareup/ui/cart/CartTaxesScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 78
    iput-object p2, p0, Lcom/squareup/ui/cart/CartTaxesScreen$Presenter;->cartEditor:Lcom/squareup/ui/cart/CartFeesModel$Session;

    .line 79
    iput-object p3, p0, Lcom/squareup/ui/cart/CartTaxesScreen$Presenter;->cartScreenRunner:Lcom/squareup/ui/cart/CartScreenRunner;

    .line 80
    iput-object p5, p0, Lcom/squareup/ui/cart/CartTaxesScreen$Presenter;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 81
    iput-object p6, p0, Lcom/squareup/ui/cart/CartTaxesScreen$Presenter;->transaction:Lcom/squareup/payment/Transaction;

    .line 82
    iput-object p8, p0, Lcom/squareup/ui/cart/CartTaxesScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    .line 83
    iput-object p9, p0, Lcom/squareup/ui/cart/CartTaxesScreen$Presenter;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    .line 84
    iput-object p10, p0, Lcom/squareup/ui/cart/CartTaxesScreen$Presenter;->transactionInteractionsLogger:Lcom/squareup/log/cart/TransactionInteractionsLogger;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/cart/CartTaxesScreen$Presenter;)Lcom/squareup/ui/cart/CartFeesModel$Session;
    .locals 0

    .line 61
    iget-object p0, p0, Lcom/squareup/ui/cart/CartTaxesScreen$Presenter;->cartEditor:Lcom/squareup/ui/cart/CartFeesModel$Session;

    return-object p0
.end method

.method private updateActionBar()V
    .locals 3

    .line 158
    iget-object v0, p0, Lcom/squareup/ui/cart/CartTaxesScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    iget-object v1, p0, Lcom/squareup/ui/cart/CartTaxesScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/orderentry/R$string;->cart_tax_reset:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setSecondaryButtonTextNoGlyph(Ljava/lang/CharSequence;)V

    .line 159
    iget-object v0, p0, Lcom/squareup/ui/cart/CartTaxesScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    new-instance v1, Lcom/squareup/ui/cart/-$$Lambda$CartTaxesScreen$Presenter$DJsek9vYDMxFCwlcyazbIrW8fRI;

    invoke-direct {v1, p0}, Lcom/squareup/ui/cart/-$$Lambda$CartTaxesScreen$Presenter$DJsek9vYDMxFCwlcyazbIrW8fRI;-><init>(Lcom/squareup/ui/cart/CartTaxesScreen$Presenter;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->showSecondaryButton(Ljava/lang/Runnable;)V

    .line 174
    iget-object v0, p0, Lcom/squareup/ui/cart/CartTaxesScreen$Presenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    iget-object v1, p0, Lcom/squareup/ui/cart/CartTaxesScreen$Presenter;->cartEditor:Lcom/squareup/ui/cart/CartFeesModel$Session;

    invoke-virtual {v1}, Lcom/squareup/ui/cart/CartFeesModel$Session;->hasCustomizedTaxes()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setSecondaryButtonEnabled(Z)V

    return-void
.end method


# virtual methods
.method protected deleteIfPermitted(Lcom/squareup/ui/cart/CartFeeRowView;)V
    .locals 3

    .line 137
    iget-object v0, p0, Lcom/squareup/ui/cart/CartTaxesScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->PROTECT_EDIT_TAX:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 138
    iget-object v0, p0, Lcom/squareup/ui/cart/CartTaxesScreen$Presenter;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    sget-object v1, Lcom/squareup/permissions/Permission;->EDIT_TAXES_IN_SALE:Lcom/squareup/permissions/Permission;

    new-instance v2, Lcom/squareup/ui/cart/CartTaxesScreen$Presenter$1;

    invoke-direct {v2, p0, p1}, Lcom/squareup/ui/cart/CartTaxesScreen$Presenter$1;-><init>(Lcom/squareup/ui/cart/CartTaxesScreen$Presenter;Lcom/squareup/ui/cart/CartFeeRowView;)V

    invoke-virtual {v0, v1, v2}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    goto :goto_0

    .line 144
    :cond_0
    invoke-virtual {p1}, Lcom/squareup/ui/cart/CartFeeRowView;->doDelete()V

    :goto_0
    return-void
.end method

.method protected executeDeletes()V
    .locals 1

    .line 122
    iget-object v0, p0, Lcom/squareup/ui/cart/CartTaxesScreen$Presenter;->cartEditor:Lcom/squareup/ui/cart/CartFeesModel$Session;

    invoke-virtual {v0}, Lcom/squareup/ui/cart/CartFeesModel$Session;->executeTaxDeletes()V

    return-void
.end method

.method protected getHelpText()Ljava/lang/CharSequence;
    .locals 4

    .line 111
    invoke-virtual {p0}, Lcom/squareup/ui/cart/CartTaxesScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/cart/CartTaxesView;

    invoke-virtual {v0}, Lcom/squareup/ui/cart/CartTaxesView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 112
    invoke-virtual {p0}, Lcom/squareup/ui/cart/CartTaxesScreen$Presenter;->getView()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/cart/CartTaxesView;

    invoke-virtual {v1}, Lcom/squareup/ui/cart/CartTaxesView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 113
    new-instance v2, Lcom/squareup/ui/LinkSpan$Builder;

    invoke-direct {v2, v0}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    sget v0, Lcom/squareup/configure/item/R$string;->conditional_taxes_help_text_for_cart:I

    const-string v3, "dashboard"

    .line 114
    invoke-virtual {v2, v0, v3}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v0

    sget v2, Lcom/squareup/configure/item/R$string;->dashboard_taxes_url:I

    .line 116
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/LinkSpan$Builder;->url(Ljava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v0

    sget v1, Lcom/squareup/configure/item/R$string;->dashboard:I

    .line 117
    invoke-virtual {v0, v1}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v0

    .line 118
    invoke-virtual {v0}, Lcom/squareup/ui/LinkSpan$Builder;->asCharSequence()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method goBack()V
    .locals 1

    .line 93
    iget-object v0, p0, Lcom/squareup/ui/cart/CartTaxesScreen$Presenter;->cartScreenRunner:Lcom/squareup/ui/cart/CartScreenRunner;

    invoke-virtual {v0}, Lcom/squareup/ui/cart/CartScreenRunner;->finishCartTaxes()V

    return-void
.end method

.method public synthetic lambda$updateActionBar$0$CartTaxesScreen$Presenter()V
    .locals 3

    .line 160
    new-instance v0, Lcom/squareup/ui/cart/CartTaxesScreen$Presenter$2;

    invoke-direct {v0, p0}, Lcom/squareup/ui/cart/CartTaxesScreen$Presenter$2;-><init>(Lcom/squareup/ui/cart/CartTaxesScreen$Presenter;)V

    .line 168
    iget-object v1, p0, Lcom/squareup/ui/cart/CartTaxesScreen$Presenter;->features:Lcom/squareup/settings/server/Features;

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->PROTECT_EDIT_TAX:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 169
    iget-object v1, p0, Lcom/squareup/ui/cart/CartTaxesScreen$Presenter;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    sget-object v2, Lcom/squareup/permissions/Permission;->EDIT_TAXES_IN_SALE:Lcom/squareup/permissions/Permission;

    invoke-virtual {v1, v2, v0}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    goto :goto_0

    .line 171
    :cond_0
    invoke-virtual {v0}, Lcom/squareup/permissions/PermissionGatekeeper$When;->success()V

    :goto_0
    return-void
.end method

.method public onBackPressed()Z
    .locals 3

    .line 126
    iget-object v0, p0, Lcom/squareup/ui/cart/CartTaxesScreen$Presenter;->transactionInteractionsLogger:Lcom/squareup/log/cart/TransactionInteractionsLogger;

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_CART_TAXES:Lcom/squareup/analytics/RegisterViewName;

    sget-object v2, Lcom/squareup/analytics/RegisterTapName;->SELLER_FLOW_CART_TAXES_BACK_BUTTON:Lcom/squareup/analytics/RegisterTapName;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/log/cart/TransactionInteractionsLogger;->start(Lcom/squareup/analytics/RegisterViewName;Lcom/squareup/analytics/RegisterTapName;)V

    .line 128
    invoke-super {p0}, Lcom/squareup/ui/cart/AbstractCartFeesPresenter;->onBackPressed()Z

    move-result v0

    return v0
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 0

    .line 88
    invoke-super {p0, p1}, Lcom/squareup/ui/cart/AbstractCartFeesPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 89
    invoke-direct {p0}, Lcom/squareup/ui/cart/CartTaxesScreen$Presenter;->updateActionBar()V

    return-void
.end method

.method onStartVisualTransition()V
    .locals 2

    .line 154
    iget-object v0, p0, Lcom/squareup/ui/cart/CartTaxesScreen$Presenter;->transactionInteractionsLogger:Lcom/squareup/log/cart/TransactionInteractionsLogger;

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_CART_TAXES:Lcom/squareup/analytics/RegisterViewName;

    invoke-virtual {v0, v1}, Lcom/squareup/log/cart/TransactionInteractionsLogger;->logAllTaxesEvent(Lcom/squareup/analytics/RegisterViewName;)V

    return-void
.end method

.method protected onUpPressed()V
    .locals 3

    .line 149
    iget-object v0, p0, Lcom/squareup/ui/cart/CartTaxesScreen$Presenter;->transactionInteractionsLogger:Lcom/squareup/log/cart/TransactionInteractionsLogger;

    sget-object v1, Lcom/squareup/analytics/RegisterViewName;->SELLER_FLOW_CART_TAXES:Lcom/squareup/analytics/RegisterViewName;

    sget-object v2, Lcom/squareup/analytics/RegisterTapName;->SELLER_FLOW_CART_TAXES_UP_BUTTON:Lcom/squareup/analytics/RegisterTapName;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/log/cart/TransactionInteractionsLogger;->start(Lcom/squareup/analytics/RegisterViewName;Lcom/squareup/analytics/RegisterTapName;)V

    .line 150
    invoke-super {p0}, Lcom/squareup/ui/cart/AbstractCartFeesPresenter;->onUpPressed()V

    return-void
.end method

.method protected showHelpText()Z
    .locals 1

    .line 106
    iget-object v0, p0, Lcom/squareup/ui/cart/CartTaxesScreen$Presenter;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v0}, Lcom/squareup/settings/server/AccountStatusSettings;->shouldUseConditionalTaxes()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/cart/CartTaxesScreen$Presenter;->transaction:Lcom/squareup/payment/Transaction;

    .line 107
    invoke-virtual {v0}, Lcom/squareup/payment/Transaction;->getAvailableTaxRules()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method title()Ljava/lang/CharSequence;
    .locals 2

    .line 97
    iget-object v0, p0, Lcom/squareup/ui/cart/CartTaxesScreen$Presenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/transaction/R$string;->cart_taxes_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected update()V
    .locals 0

    .line 101
    invoke-super {p0}, Lcom/squareup/ui/cart/AbstractCartFeesPresenter;->update()V

    .line 102
    invoke-direct {p0}, Lcom/squareup/ui/cart/CartTaxesScreen$Presenter;->updateActionBar()V

    return-void
.end method
