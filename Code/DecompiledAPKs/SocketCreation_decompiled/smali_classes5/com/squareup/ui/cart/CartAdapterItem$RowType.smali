.class public final enum Lcom/squareup/ui/cart/CartAdapterItem$RowType;
.super Ljava/lang/Enum;
.source "CartAdapterItem.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/cart/CartAdapterItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "RowType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/squareup/ui/cart/CartAdapterItem$RowType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/squareup/ui/cart/CartAdapterItem$RowType;

.field public static final enum COMP:Lcom/squareup/ui/cart/CartAdapterItem$RowType;

.field public static final enum DISCOUNTS:Lcom/squareup/ui/cart/CartAdapterItem$RowType;

.field public static final enum EMPTY_CUSTOM_AMOUNT:Lcom/squareup/ui/cart/CartAdapterItem$RowType;

.field public static final enum LOYALTY_POINTS:Lcom/squareup/ui/cart/CartAdapterItem$RowType;

.field public static final enum STANDARD:Lcom/squareup/ui/cart/CartAdapterItem$RowType;

.field public static final enum SURCHARGE:Lcom/squareup/ui/cart/CartAdapterItem$RowType;

.field public static final enum TAX:Lcom/squareup/ui/cart/CartAdapterItem$RowType;

.field public static final enum TICKET_LINE:Lcom/squareup/ui/cart/CartAdapterItem$RowType;

.field public static final enum TICKET_NOTE:Lcom/squareup/ui/cart/CartAdapterItem$RowType;

.field public static final enum TOTAL:Lcom/squareup/ui/cart/CartAdapterItem$RowType;

.field public static final enum UNAPPLIED_COUPON_ROW:Lcom/squareup/ui/cart/CartAdapterItem$RowType;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .line 25
    new-instance v0, Lcom/squareup/ui/cart/CartAdapterItem$RowType;

    const/4 v1, 0x0

    const-string v2, "TICKET_NOTE"

    invoke-direct {v0, v2, v1, v1}, Lcom/squareup/ui/cart/CartAdapterItem$RowType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/ui/cart/CartAdapterItem$RowType;->TICKET_NOTE:Lcom/squareup/ui/cart/CartAdapterItem$RowType;

    .line 26
    new-instance v0, Lcom/squareup/ui/cart/CartAdapterItem$RowType;

    const/4 v2, 0x1

    const-string v3, "STANDARD"

    invoke-direct {v0, v3, v2, v2}, Lcom/squareup/ui/cart/CartAdapterItem$RowType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/ui/cart/CartAdapterItem$RowType;->STANDARD:Lcom/squareup/ui/cart/CartAdapterItem$RowType;

    .line 27
    new-instance v0, Lcom/squareup/ui/cart/CartAdapterItem$RowType;

    const/4 v3, 0x2

    const-string v4, "EMPTY_CUSTOM_AMOUNT"

    invoke-direct {v0, v4, v3, v3}, Lcom/squareup/ui/cart/CartAdapterItem$RowType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/ui/cart/CartAdapterItem$RowType;->EMPTY_CUSTOM_AMOUNT:Lcom/squareup/ui/cart/CartAdapterItem$RowType;

    .line 28
    new-instance v0, Lcom/squareup/ui/cart/CartAdapterItem$RowType;

    const/4 v4, 0x3

    const-string v5, "DISCOUNTS"

    invoke-direct {v0, v5, v4, v4}, Lcom/squareup/ui/cart/CartAdapterItem$RowType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/ui/cart/CartAdapterItem$RowType;->DISCOUNTS:Lcom/squareup/ui/cart/CartAdapterItem$RowType;

    .line 29
    new-instance v0, Lcom/squareup/ui/cart/CartAdapterItem$RowType;

    const/4 v5, 0x4

    const-string v6, "COMP"

    invoke-direct {v0, v6, v5, v5}, Lcom/squareup/ui/cart/CartAdapterItem$RowType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/ui/cart/CartAdapterItem$RowType;->COMP:Lcom/squareup/ui/cart/CartAdapterItem$RowType;

    .line 30
    new-instance v0, Lcom/squareup/ui/cart/CartAdapterItem$RowType;

    const/4 v6, 0x5

    const-string v7, "TAX"

    invoke-direct {v0, v7, v6, v6}, Lcom/squareup/ui/cart/CartAdapterItem$RowType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/ui/cart/CartAdapterItem$RowType;->TAX:Lcom/squareup/ui/cart/CartAdapterItem$RowType;

    .line 31
    new-instance v0, Lcom/squareup/ui/cart/CartAdapterItem$RowType;

    const/4 v7, 0x6

    const-string v8, "SURCHARGE"

    invoke-direct {v0, v8, v7, v7}, Lcom/squareup/ui/cart/CartAdapterItem$RowType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/ui/cart/CartAdapterItem$RowType;->SURCHARGE:Lcom/squareup/ui/cart/CartAdapterItem$RowType;

    .line 32
    new-instance v0, Lcom/squareup/ui/cart/CartAdapterItem$RowType;

    const/4 v8, 0x7

    const-string v9, "TOTAL"

    invoke-direct {v0, v9, v8, v8}, Lcom/squareup/ui/cart/CartAdapterItem$RowType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/ui/cart/CartAdapterItem$RowType;->TOTAL:Lcom/squareup/ui/cart/CartAdapterItem$RowType;

    .line 33
    new-instance v0, Lcom/squareup/ui/cart/CartAdapterItem$RowType;

    const/16 v9, 0x8

    const-string v10, "TICKET_LINE"

    invoke-direct {v0, v10, v9, v9}, Lcom/squareup/ui/cart/CartAdapterItem$RowType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/ui/cart/CartAdapterItem$RowType;->TICKET_LINE:Lcom/squareup/ui/cart/CartAdapterItem$RowType;

    .line 34
    new-instance v0, Lcom/squareup/ui/cart/CartAdapterItem$RowType;

    const/16 v10, 0x9

    const-string v11, "LOYALTY_POINTS"

    invoke-direct {v0, v11, v10, v10}, Lcom/squareup/ui/cart/CartAdapterItem$RowType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/ui/cart/CartAdapterItem$RowType;->LOYALTY_POINTS:Lcom/squareup/ui/cart/CartAdapterItem$RowType;

    .line 35
    new-instance v0, Lcom/squareup/ui/cart/CartAdapterItem$RowType;

    const/16 v11, 0xa

    const-string v12, "UNAPPLIED_COUPON_ROW"

    invoke-direct {v0, v12, v11, v11}, Lcom/squareup/ui/cart/CartAdapterItem$RowType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/squareup/ui/cart/CartAdapterItem$RowType;->UNAPPLIED_COUPON_ROW:Lcom/squareup/ui/cart/CartAdapterItem$RowType;

    const/16 v0, 0xb

    new-array v0, v0, [Lcom/squareup/ui/cart/CartAdapterItem$RowType;

    .line 24
    sget-object v12, Lcom/squareup/ui/cart/CartAdapterItem$RowType;->TICKET_NOTE:Lcom/squareup/ui/cart/CartAdapterItem$RowType;

    aput-object v12, v0, v1

    sget-object v1, Lcom/squareup/ui/cart/CartAdapterItem$RowType;->STANDARD:Lcom/squareup/ui/cart/CartAdapterItem$RowType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/squareup/ui/cart/CartAdapterItem$RowType;->EMPTY_CUSTOM_AMOUNT:Lcom/squareup/ui/cart/CartAdapterItem$RowType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/squareup/ui/cart/CartAdapterItem$RowType;->DISCOUNTS:Lcom/squareup/ui/cart/CartAdapterItem$RowType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/ui/cart/CartAdapterItem$RowType;->COMP:Lcom/squareup/ui/cart/CartAdapterItem$RowType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/squareup/ui/cart/CartAdapterItem$RowType;->TAX:Lcom/squareup/ui/cart/CartAdapterItem$RowType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/squareup/ui/cart/CartAdapterItem$RowType;->SURCHARGE:Lcom/squareup/ui/cart/CartAdapterItem$RowType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/squareup/ui/cart/CartAdapterItem$RowType;->TOTAL:Lcom/squareup/ui/cart/CartAdapterItem$RowType;

    aput-object v1, v0, v8

    sget-object v1, Lcom/squareup/ui/cart/CartAdapterItem$RowType;->TICKET_LINE:Lcom/squareup/ui/cart/CartAdapterItem$RowType;

    aput-object v1, v0, v9

    sget-object v1, Lcom/squareup/ui/cart/CartAdapterItem$RowType;->LOYALTY_POINTS:Lcom/squareup/ui/cart/CartAdapterItem$RowType;

    aput-object v1, v0, v10

    sget-object v1, Lcom/squareup/ui/cart/CartAdapterItem$RowType;->UNAPPLIED_COUPON_ROW:Lcom/squareup/ui/cart/CartAdapterItem$RowType;

    aput-object v1, v0, v11

    sput-object v0, Lcom/squareup/ui/cart/CartAdapterItem$RowType;->$VALUES:[Lcom/squareup/ui/cart/CartAdapterItem$RowType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 40
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 41
    iput p3, p0, Lcom/squareup/ui/cart/CartAdapterItem$RowType;->value:I

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/cart/CartAdapterItem$RowType;)I
    .locals 0

    .line 24
    iget p0, p0, Lcom/squareup/ui/cart/CartAdapterItem$RowType;->value:I

    return p0
.end method

.method public static fromValue(I)Lcom/squareup/ui/cart/CartAdapterItem$RowType;
    .locals 1

    packed-switch p0, :pswitch_data_0

    .line 76
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string v0, "Invalid RowType value."

    invoke-direct {p0, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p0

    .line 74
    :pswitch_0
    sget-object p0, Lcom/squareup/ui/cart/CartAdapterItem$RowType;->UNAPPLIED_COUPON_ROW:Lcom/squareup/ui/cart/CartAdapterItem$RowType;

    return-object p0

    .line 72
    :pswitch_1
    sget-object p0, Lcom/squareup/ui/cart/CartAdapterItem$RowType;->LOYALTY_POINTS:Lcom/squareup/ui/cart/CartAdapterItem$RowType;

    return-object p0

    .line 70
    :pswitch_2
    sget-object p0, Lcom/squareup/ui/cart/CartAdapterItem$RowType;->TICKET_LINE:Lcom/squareup/ui/cart/CartAdapterItem$RowType;

    return-object p0

    .line 68
    :pswitch_3
    sget-object p0, Lcom/squareup/ui/cart/CartAdapterItem$RowType;->TOTAL:Lcom/squareup/ui/cart/CartAdapterItem$RowType;

    return-object p0

    .line 66
    :pswitch_4
    sget-object p0, Lcom/squareup/ui/cart/CartAdapterItem$RowType;->SURCHARGE:Lcom/squareup/ui/cart/CartAdapterItem$RowType;

    return-object p0

    .line 64
    :pswitch_5
    sget-object p0, Lcom/squareup/ui/cart/CartAdapterItem$RowType;->TAX:Lcom/squareup/ui/cart/CartAdapterItem$RowType;

    return-object p0

    .line 62
    :pswitch_6
    sget-object p0, Lcom/squareup/ui/cart/CartAdapterItem$RowType;->COMP:Lcom/squareup/ui/cart/CartAdapterItem$RowType;

    return-object p0

    .line 60
    :pswitch_7
    sget-object p0, Lcom/squareup/ui/cart/CartAdapterItem$RowType;->DISCOUNTS:Lcom/squareup/ui/cart/CartAdapterItem$RowType;

    return-object p0

    .line 58
    :pswitch_8
    sget-object p0, Lcom/squareup/ui/cart/CartAdapterItem$RowType;->EMPTY_CUSTOM_AMOUNT:Lcom/squareup/ui/cart/CartAdapterItem$RowType;

    return-object p0

    .line 56
    :pswitch_9
    sget-object p0, Lcom/squareup/ui/cart/CartAdapterItem$RowType;->STANDARD:Lcom/squareup/ui/cart/CartAdapterItem$RowType;

    return-object p0

    .line 54
    :pswitch_a
    sget-object p0, Lcom/squareup/ui/cart/CartAdapterItem$RowType;->TICKET_NOTE:Lcom/squareup/ui/cart/CartAdapterItem$RowType;

    return-object p0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/ui/cart/CartAdapterItem$RowType;
    .locals 1

    .line 24
    const-class v0, Lcom/squareup/ui/cart/CartAdapterItem$RowType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/squareup/ui/cart/CartAdapterItem$RowType;

    return-object p0
.end method

.method public static values()[Lcom/squareup/ui/cart/CartAdapterItem$RowType;
    .locals 1

    .line 24
    sget-object v0, Lcom/squareup/ui/cart/CartAdapterItem$RowType;->$VALUES:[Lcom/squareup/ui/cart/CartAdapterItem$RowType;

    invoke-virtual {v0}, [Lcom/squareup/ui/cart/CartAdapterItem$RowType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/ui/cart/CartAdapterItem$RowType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 45
    iget v0, p0, Lcom/squareup/ui/cart/CartAdapterItem$RowType;->value:I

    return v0
.end method
