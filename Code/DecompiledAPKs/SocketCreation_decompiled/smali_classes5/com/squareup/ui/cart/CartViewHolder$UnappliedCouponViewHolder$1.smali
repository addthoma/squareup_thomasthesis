.class Lcom/squareup/ui/cart/CartViewHolder$UnappliedCouponViewHolder$1;
.super Lcom/squareup/debounce/DebouncedOnClickListener;
.source "CartViewHolder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/cart/CartViewHolder$UnappliedCouponViewHolder;->bind(Lcom/squareup/ui/cart/CartAdapterItem;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/cart/CartViewHolder$UnappliedCouponViewHolder;

.field final synthetic val$row:Lcom/squareup/ui/cart/CartAdapterItem$UnappliedCouponRow;


# direct methods
.method constructor <init>(Lcom/squareup/ui/cart/CartViewHolder$UnappliedCouponViewHolder;Lcom/squareup/ui/cart/CartAdapterItem$UnappliedCouponRow;)V
    .locals 0

    .line 299
    iput-object p1, p0, Lcom/squareup/ui/cart/CartViewHolder$UnappliedCouponViewHolder$1;->this$0:Lcom/squareup/ui/cart/CartViewHolder$UnappliedCouponViewHolder;

    iput-object p2, p0, Lcom/squareup/ui/cart/CartViewHolder$UnappliedCouponViewHolder$1;->val$row:Lcom/squareup/ui/cart/CartAdapterItem$UnappliedCouponRow;

    invoke-direct {p0}, Lcom/squareup/debounce/DebouncedOnClickListener;-><init>()V

    return-void
.end method


# virtual methods
.method public doClick(Landroid/view/View;)V
    .locals 2

    .line 301
    iget-object p1, p0, Lcom/squareup/ui/cart/CartViewHolder$UnappliedCouponViewHolder$1;->this$0:Lcom/squareup/ui/cart/CartViewHolder$UnappliedCouponViewHolder;

    invoke-static {p1}, Lcom/squareup/ui/cart/CartViewHolder$UnappliedCouponViewHolder;->access$400(Lcom/squareup/ui/cart/CartViewHolder$UnappliedCouponViewHolder;)Lcom/squareup/ui/cart/CartRecyclerViewPresenter;

    move-result-object p1

    iget-object v0, p0, Lcom/squareup/ui/cart/CartViewHolder$UnappliedCouponViewHolder$1;->this$0:Lcom/squareup/ui/cart/CartViewHolder$UnappliedCouponViewHolder;

    invoke-virtual {v0}, Lcom/squareup/ui/cart/CartViewHolder$UnappliedCouponViewHolder;->getAdapterPosition()I

    move-result v0

    iget-object v1, p0, Lcom/squareup/ui/cart/CartViewHolder$UnappliedCouponViewHolder$1;->val$row:Lcom/squareup/ui/cart/CartAdapterItem$UnappliedCouponRow;

    invoke-virtual {p1, v0, v1}, Lcom/squareup/ui/cart/CartRecyclerViewPresenter;->onUnappliedCouponRowClicked(ILcom/squareup/ui/cart/CartAdapterItem$UnappliedCouponRow;)V

    return-void
.end method
