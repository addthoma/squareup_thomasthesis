.class public Lcom/squareup/ui/cart/CartAdapterItem$LoyaltyPointsRow;
.super Ljava/lang/Object;
.source "CartAdapterItem.java"

# interfaces
.implements Lcom/squareup/ui/cart/CartAdapterItem;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/cart/CartAdapterItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LoyaltyPointsRow"
.end annotation


# instance fields
.field public final formattedValue:Ljava/lang/String;

.field public final subLabel:Ljava/lang/Integer;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/Integer;)V
    .locals 0

    .line 297
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 298
    iput-object p1, p0, Lcom/squareup/ui/cart/CartAdapterItem$LoyaltyPointsRow;->formattedValue:Ljava/lang/String;

    .line 299
    iput-object p2, p0, Lcom/squareup/ui/cart/CartAdapterItem$LoyaltyPointsRow;->subLabel:Ljava/lang/Integer;

    return-void
.end method


# virtual methods
.method public getId()I
    .locals 1

    .line 307
    sget-object v0, Lcom/squareup/ui/cart/CartAdapterItem$RowType;->LOYALTY_POINTS:Lcom/squareup/ui/cart/CartAdapterItem$RowType;

    invoke-static {v0}, Lcom/squareup/ui/cart/CartAdapterItem$RowType;->access$000(Lcom/squareup/ui/cart/CartAdapterItem$RowType;)I

    move-result v0

    return v0
.end method

.method public getType()Lcom/squareup/ui/cart/CartAdapterItem$RowType;
    .locals 1

    .line 303
    sget-object v0, Lcom/squareup/ui/cart/CartAdapterItem$RowType;->LOYALTY_POINTS:Lcom/squareup/ui/cart/CartAdapterItem$RowType;

    return-object v0
.end method
