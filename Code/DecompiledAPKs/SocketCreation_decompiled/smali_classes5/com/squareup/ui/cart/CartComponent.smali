.class public interface abstract Lcom/squareup/ui/cart/CartComponent;
.super Ljava/lang/Object;
.source "CartComponent.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/cart/CartComponent$SharedScope;
    }
.end annotation


# virtual methods
.method public abstract inject(Lcom/squareup/ui/cart/CartRecyclerView;)V
.end method

.method public abstract inject(Lcom/squareup/ui/cart/DiningOptionViewPager;)V
.end method

.method public abstract inject(Lcom/squareup/ui/cart/menu/CartMenuDropDownContainer;)V
.end method

.method public abstract inject(Lcom/squareup/ui/cart/menu/CartMenuView;)V
.end method
