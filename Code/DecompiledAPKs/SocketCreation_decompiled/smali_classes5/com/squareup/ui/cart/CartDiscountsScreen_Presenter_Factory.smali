.class public final Lcom/squareup/ui/cart/CartDiscountsScreen_Presenter_Factory;
.super Ljava/lang/Object;
.source "CartDiscountsScreen_Presenter_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/cart/CartDiscountsScreen$Presenter;",
        ">;"
    }
.end annotation


# instance fields
.field private final actionBarProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/marin/widgets/MarinActionBar;",
            ">;"
        }
    .end annotation
.end field

.field private final cartEditorProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/cart/CartFeesModel$Session;",
            ">;"
        }
    .end annotation
.end field

.field private final cartScreenRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/cart/CartScreenRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final discountFormatterProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/cart/DiscountFormatter;",
            ">;"
        }
    .end annotation
.end field

.field private final resProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;"
        }
    .end annotation
.end field

.field private final transactionInteractionsLoggerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/cart/TransactionInteractionsLogger;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/marin/widgets/MarinActionBar;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/cart/CartFeesModel$Session;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/cart/CartScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/cart/DiscountFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/cart/TransactionInteractionsLogger;",
            ">;)V"
        }
    .end annotation

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/squareup/ui/cart/CartDiscountsScreen_Presenter_Factory;->actionBarProvider:Ljavax/inject/Provider;

    .line 37
    iput-object p2, p0, Lcom/squareup/ui/cart/CartDiscountsScreen_Presenter_Factory;->cartEditorProvider:Ljavax/inject/Provider;

    .line 38
    iput-object p3, p0, Lcom/squareup/ui/cart/CartDiscountsScreen_Presenter_Factory;->cartScreenRunnerProvider:Ljavax/inject/Provider;

    .line 39
    iput-object p4, p0, Lcom/squareup/ui/cart/CartDiscountsScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    .line 40
    iput-object p5, p0, Lcom/squareup/ui/cart/CartDiscountsScreen_Presenter_Factory;->discountFormatterProvider:Ljavax/inject/Provider;

    .line 41
    iput-object p6, p0, Lcom/squareup/ui/cart/CartDiscountsScreen_Presenter_Factory;->transactionInteractionsLoggerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/cart/CartDiscountsScreen_Presenter_Factory;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/marin/widgets/MarinActionBar;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/cart/CartFeesModel$Session;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/cart/CartScreenRunner;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/util/Res;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/cart/DiscountFormatter;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/log/cart/TransactionInteractionsLogger;",
            ">;)",
            "Lcom/squareup/ui/cart/CartDiscountsScreen_Presenter_Factory;"
        }
    .end annotation

    .line 55
    new-instance v7, Lcom/squareup/ui/cart/CartDiscountsScreen_Presenter_Factory;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/cart/CartDiscountsScreen_Presenter_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v7
.end method

.method public static newInstance(Lcom/squareup/marin/widgets/MarinActionBar;Lcom/squareup/ui/cart/CartFeesModel$Session;Lcom/squareup/ui/cart/CartScreenRunner;Lcom/squareup/util/Res;Lcom/squareup/ui/cart/DiscountFormatter;Lcom/squareup/log/cart/TransactionInteractionsLogger;)Lcom/squareup/ui/cart/CartDiscountsScreen$Presenter;
    .locals 8

    .line 62
    new-instance v7, Lcom/squareup/ui/cart/CartDiscountsScreen$Presenter;

    move-object v0, v7

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/cart/CartDiscountsScreen$Presenter;-><init>(Lcom/squareup/marin/widgets/MarinActionBar;Lcom/squareup/ui/cart/CartFeesModel$Session;Lcom/squareup/ui/cart/CartScreenRunner;Lcom/squareup/util/Res;Lcom/squareup/ui/cart/DiscountFormatter;Lcom/squareup/log/cart/TransactionInteractionsLogger;)V

    return-object v7
.end method


# virtual methods
.method public get()Lcom/squareup/ui/cart/CartDiscountsScreen$Presenter;
    .locals 7

    .line 46
    iget-object v0, p0, Lcom/squareup/ui/cart/CartDiscountsScreen_Presenter_Factory;->actionBarProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/marin/widgets/MarinActionBar;

    iget-object v0, p0, Lcom/squareup/ui/cart/CartDiscountsScreen_Presenter_Factory;->cartEditorProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/squareup/ui/cart/CartFeesModel$Session;

    iget-object v0, p0, Lcom/squareup/ui/cart/CartDiscountsScreen_Presenter_Factory;->cartScreenRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/ui/cart/CartScreenRunner;

    iget-object v0, p0, Lcom/squareup/ui/cart/CartDiscountsScreen_Presenter_Factory;->resProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/squareup/util/Res;

    iget-object v0, p0, Lcom/squareup/ui/cart/CartDiscountsScreen_Presenter_Factory;->discountFormatterProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/ui/cart/DiscountFormatter;

    iget-object v0, p0, Lcom/squareup/ui/cart/CartDiscountsScreen_Presenter_Factory;->transactionInteractionsLoggerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/log/cart/TransactionInteractionsLogger;

    invoke-static/range {v1 .. v6}, Lcom/squareup/ui/cart/CartDiscountsScreen_Presenter_Factory;->newInstance(Lcom/squareup/marin/widgets/MarinActionBar;Lcom/squareup/ui/cart/CartFeesModel$Session;Lcom/squareup/ui/cart/CartScreenRunner;Lcom/squareup/util/Res;Lcom/squareup/ui/cart/DiscountFormatter;Lcom/squareup/log/cart/TransactionInteractionsLogger;)Lcom/squareup/ui/cart/CartDiscountsScreen$Presenter;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 10
    invoke-virtual {p0}, Lcom/squareup/ui/cart/CartDiscountsScreen_Presenter_Factory;->get()Lcom/squareup/ui/cart/CartDiscountsScreen$Presenter;

    move-result-object v0

    return-object v0
.end method
