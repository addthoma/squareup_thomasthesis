.class public final Lcom/squareup/ui/help/about/AndroidLibraries;
.super Ljava/lang/Object;
.source "AndroidLibraries.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0003\u0008\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0002\u00a2\u0006\u0002\u0010\u0002R\u0017\u0010\u0003\u001a\u0008\u0012\u0004\u0012\u00020\u00050\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0006\u0010\u0007\u00a8\u0006\u0008"
    }
    d2 = {
        "Lcom/squareup/ui/help/about/AndroidLibraries;",
        "",
        "()V",
        "librariesUsed",
        "",
        "Lcom/squareup/ui/help/about/AndroidLibrary;",
        "getLibrariesUsed",
        "()Ljava/util/List;",
        "help_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final INSTANCE:Lcom/squareup/ui/help/about/AndroidLibraries;

.field private static final librariesUsed:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/ui/help/about/AndroidLibrary;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .line 12
    new-instance v0, Lcom/squareup/ui/help/about/AndroidLibraries;

    invoke-direct {v0}, Lcom/squareup/ui/help/about/AndroidLibraries;-><init>()V

    sput-object v0, Lcom/squareup/ui/help/about/AndroidLibraries;->INSTANCE:Lcom/squareup/ui/help/about/AndroidLibraries;

    const/16 v0, 0x1e

    new-array v0, v0, [Lcom/squareup/ui/help/about/AndroidLibrary;

    .line 15
    new-instance v1, Lcom/squareup/ui/help/about/AndroidLibrary;

    .line 19
    sget-object v2, Lcom/squareup/ui/help/about/License;->APACHE_2_0:Lcom/squareup/ui/help/about/License;

    const-string v3, "Android-Job"

    const-string v4, "Evernote"

    const-string v5, "https://github.com/evernote/android-job/"

    .line 15
    invoke-direct {v1, v3, v4, v5, v2}, Lcom/squareup/ui/help/about/AndroidLibrary;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/help/about/License;)V

    const/4 v2, 0x0

    aput-object v1, v0, v2

    .line 21
    new-instance v1, Lcom/squareup/ui/help/about/AndroidLibrary;

    .line 25
    sget-object v2, Lcom/squareup/ui/help/about/License;->APACHE_2_0:Lcom/squareup/ui/help/about/License;

    const-string v3, "Square"

    const-string v4, "Cycler"

    const-string v5, "https://github.com/square/cycler"

    .line 21
    invoke-direct {v1, v4, v3, v5, v2}, Lcom/squareup/ui/help/about/AndroidLibrary;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/help/about/License;)V

    const/4 v2, 0x1

    aput-object v1, v0, v2

    .line 27
    new-instance v1, Lcom/squareup/ui/help/about/AndroidLibrary;

    .line 31
    sget-object v2, Lcom/squareup/ui/help/about/License;->APACHE_2_0:Lcom/squareup/ui/help/about/License;

    const-string v4, "Google"

    const-string v5, "Dagger 2"

    const-string v6, "https://google.github.io/dagger/"

    .line 27
    invoke-direct {v1, v5, v4, v6, v2}, Lcom/squareup/ui/help/about/AndroidLibrary;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/help/about/License;)V

    const/4 v2, 0x2

    aput-object v1, v0, v2

    .line 33
    new-instance v1, Lcom/squareup/ui/help/about/AndroidLibrary;

    .line 37
    sget-object v2, Lcom/squareup/ui/help/about/License;->BSD_3_CLAUSE:Lcom/squareup/ui/help/about/License;

    const-string v5, "Device Year Class"

    const-string v6, "Facebook"

    const-string v7, "https://github.com/facebook/device-year-class"

    .line 33
    invoke-direct {v1, v5, v6, v7, v2}, Lcom/squareup/ui/help/about/AndroidLibrary;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/help/about/License;)V

    const/4 v2, 0x3

    aput-object v1, v0, v2

    .line 39
    new-instance v1, Lcom/squareup/ui/help/about/AndroidLibrary;

    .line 43
    sget-object v2, Lcom/squareup/ui/help/about/License;->APACHE_2_0:Lcom/squareup/ui/help/about/License;

    const-string v5, "DragSortListView"

    const-string v6, "Carl A. Bauer"

    const-string v7, "https://github.com/bauerca/drag-sort-listview"

    .line 39
    invoke-direct {v1, v5, v6, v7, v2}, Lcom/squareup/ui/help/about/AndroidLibrary;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/help/about/License;)V

    const/4 v2, 0x4

    aput-object v1, v0, v2

    .line 45
    new-instance v1, Lcom/squareup/ui/help/about/AndroidLibrary;

    .line 49
    sget-object v2, Lcom/squareup/ui/help/about/License;->CC_BY_1_0:Lcom/squareup/ui/help/about/License;

    const-string v5, "fft.c"

    const-string v6, "Douglas L. Jones"

    const-string v7, "https://cnx.org/content/m12016/latest/"

    .line 45
    invoke-direct {v1, v5, v6, v7, v2}, Lcom/squareup/ui/help/about/AndroidLibrary;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/help/about/License;)V

    const/4 v2, 0x5

    aput-object v1, v0, v2

    .line 51
    new-instance v1, Lcom/squareup/ui/help/about/AndroidLibrary;

    .line 55
    sget-object v2, Lcom/squareup/ui/help/about/License;->APACHE_2_0:Lcom/squareup/ui/help/about/License;

    const-string v5, "Flow"

    const-string v6, "https://github.com/square/flow"

    .line 51
    invoke-direct {v1, v5, v3, v6, v2}, Lcom/squareup/ui/help/about/AndroidLibrary;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/help/about/License;)V

    const/4 v2, 0x6

    aput-object v1, v0, v2

    .line 57
    new-instance v1, Lcom/squareup/ui/help/about/AndroidLibrary;

    .line 61
    sget-object v2, Lcom/squareup/ui/help/about/License;->APACHE_2_0:Lcom/squareup/ui/help/about/License;

    const-string v5, "Gson"

    const-string v6, "https://github.com/google/gson"

    .line 57
    invoke-direct {v1, v5, v4, v6, v2}, Lcom/squareup/ui/help/about/AndroidLibrary;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/help/about/License;)V

    const/4 v2, 0x7

    aput-object v1, v0, v2

    .line 63
    new-instance v1, Lcom/squareup/ui/help/about/AndroidLibrary;

    .line 67
    sget-object v2, Lcom/squareup/ui/help/about/License;->APACHE_2_0:Lcom/squareup/ui/help/about/License;

    const-string v5, "LeakCanary"

    const-string v6, "https://github.com/square/leakcanary"

    .line 63
    invoke-direct {v1, v5, v3, v6, v2}, Lcom/squareup/ui/help/about/AndroidLibrary;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/help/about/License;)V

    const/16 v2, 0x8

    aput-object v1, v0, v2

    .line 69
    new-instance v1, Lcom/squareup/ui/help/about/AndroidLibrary;

    .line 73
    sget-object v2, Lcom/squareup/ui/help/about/License;->APACHE_2_0:Lcom/squareup/ui/help/about/License;

    const-string v5, "libphonenumber"

    const-string v6, "https://github.com/googlei18n/libphonenumber"

    .line 69
    invoke-direct {v1, v5, v4, v6, v2}, Lcom/squareup/ui/help/about/AndroidLibrary;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/help/about/License;)V

    const/16 v2, 0x9

    aput-object v1, v0, v2

    .line 75
    new-instance v1, Lcom/squareup/ui/help/about/AndroidLibrary;

    .line 79
    sget-object v2, Lcom/squareup/ui/help/about/License;->APACHE_2_0:Lcom/squareup/ui/help/about/License;

    const-string v5, "Mortar"

    const-string v6, "https://github.com/square/mortar"

    .line 75
    invoke-direct {v1, v5, v3, v6, v2}, Lcom/squareup/ui/help/about/AndroidLibrary;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/help/about/License;)V

    const/16 v2, 0xa

    aput-object v1, v0, v2

    .line 81
    new-instance v1, Lcom/squareup/ui/help/about/AndroidLibrary;

    .line 85
    sget-object v2, Lcom/squareup/ui/help/about/License;->APACHE_2_0:Lcom/squareup/ui/help/about/License;

    const-string v5, "MPAndroidChart"

    const-string v6, "Philipp Jahoda"

    const-string v7, "https://github.com/PhilJay/MPAndroidChart"

    .line 81
    invoke-direct {v1, v5, v6, v7, v2}, Lcom/squareup/ui/help/about/AndroidLibrary;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/help/about/License;)V

    const/16 v2, 0xb

    aput-object v1, v0, v2

    .line 87
    new-instance v1, Lcom/squareup/ui/help/about/AndroidLibrary;

    .line 91
    sget-object v2, Lcom/squareup/ui/help/about/License;->APACHE_2_0:Lcom/squareup/ui/help/about/License;

    const-string v5, "OkHttp"

    const-string v6, "https://square.github.io/okhttp/"

    .line 87
    invoke-direct {v1, v5, v3, v6, v2}, Lcom/squareup/ui/help/about/AndroidLibrary;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/help/about/License;)V

    const/16 v2, 0xc

    aput-object v1, v0, v2

    .line 93
    new-instance v1, Lcom/squareup/ui/help/about/AndroidLibrary;

    .line 97
    sget-object v2, Lcom/squareup/ui/help/about/License;->APACHE_2_0:Lcom/squareup/ui/help/about/License;

    const-string v5, "Otto"

    const-string v6, "https://square.github.io/otto/"

    .line 93
    invoke-direct {v1, v5, v3, v6, v2}, Lcom/squareup/ui/help/about/AndroidLibrary;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/help/about/License;)V

    const/16 v2, 0xd

    aput-object v1, v0, v2

    .line 99
    new-instance v1, Lcom/squareup/ui/help/about/AndroidLibrary;

    .line 102
    new-instance v2, Ljava/net/URL;

    const-string v5, "https"

    const-string v6, "chromium.googlesource.com"

    const-string v7, "chromium/blink/+/master/Source/wtf/PartitionAlloc.h"

    invoke-direct {v2, v5, v6, v7}, Ljava/net/URL;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    invoke-virtual {v2}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v5, "URL(\n                \"ht\u2026\n            ).toString()"

    invoke-static {v2, v5}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 107
    sget-object v5, Lcom/squareup/ui/help/about/License;->BSD_3_CLAUSE:Lcom/squareup/ui/help/about/License;

    const-string v6, "PartitionAlloc"

    .line 99
    invoke-direct {v1, v6, v4, v2, v5}, Lcom/squareup/ui/help/about/AndroidLibrary;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/help/about/License;)V

    const/16 v2, 0xe

    aput-object v1, v0, v2

    .line 109
    new-instance v1, Lcom/squareup/ui/help/about/AndroidLibrary;

    .line 113
    sget-object v2, Lcom/squareup/ui/help/about/License;->APACHE_2_0:Lcom/squareup/ui/help/about/License;

    const-string v4, "PhotoView"

    const-string v5, "Chris Banes"

    const-string v6, "https://github.com/chrisbanes/PhotoView"

    .line 109
    invoke-direct {v1, v4, v5, v6, v2}, Lcom/squareup/ui/help/about/AndroidLibrary;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/help/about/License;)V

    const/16 v2, 0xf

    aput-object v1, v0, v2

    .line 115
    new-instance v1, Lcom/squareup/ui/help/about/AndroidLibrary;

    .line 119
    sget-object v2, Lcom/squareup/ui/help/about/License;->APACHE_2_0:Lcom/squareup/ui/help/about/License;

    const-string v4, "Phrase"

    const-string v5, "https://github.com/square/phrase"

    .line 115
    invoke-direct {v1, v4, v3, v5, v2}, Lcom/squareup/ui/help/about/AndroidLibrary;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/help/about/License;)V

    const/16 v2, 0x10

    aput-object v1, v0, v2

    .line 121
    new-instance v1, Lcom/squareup/ui/help/about/AndroidLibrary;

    .line 125
    sget-object v2, Lcom/squareup/ui/help/about/License;->APACHE_2_0:Lcom/squareup/ui/help/about/License;

    const-string v4, "Picasso"

    const-string v5, "https://square.github.io/picasso/"

    .line 121
    invoke-direct {v1, v4, v3, v5, v2}, Lcom/squareup/ui/help/about/AndroidLibrary;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/help/about/License;)V

    const/16 v2, 0x11

    aput-object v1, v0, v2

    .line 127
    new-instance v1, Lcom/squareup/ui/help/about/AndroidLibrary;

    .line 131
    sget-object v2, Lcom/squareup/ui/help/about/License;->APACHE_2_0:Lcom/squareup/ui/help/about/License;

    const-string v4, "Pollexor"

    const-string v5, "https://square.github.io/pollexor/"

    .line 127
    invoke-direct {v1, v4, v3, v5, v2}, Lcom/squareup/ui/help/about/AndroidLibrary;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/help/about/License;)V

    const/16 v2, 0x12

    aput-object v1, v0, v2

    .line 133
    new-instance v1, Lcom/squareup/ui/help/about/AndroidLibrary;

    .line 137
    sget-object v2, Lcom/squareup/ui/help/about/License;->APACHE_2_0:Lcom/squareup/ui/help/about/License;

    const-string v4, "Retrofit"

    const-string v5, "https://square.github.io/retrofit/"

    .line 133
    invoke-direct {v1, v4, v3, v5, v2}, Lcom/squareup/ui/help/about/AndroidLibrary;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/help/about/License;)V

    const/16 v2, 0x13

    aput-object v1, v0, v2

    .line 139
    new-instance v1, Lcom/squareup/ui/help/about/AndroidLibrary;

    .line 143
    sget-object v2, Lcom/squareup/ui/help/about/License;->APACHE_2_0:Lcom/squareup/ui/help/about/License;

    const-string v4, "ReactiveX"

    const-string v5, "RxAndroid"

    const-string v6, "https://github.com/ReactiveX/RxAndroid"

    .line 139
    invoke-direct {v1, v5, v4, v6, v2}, Lcom/squareup/ui/help/about/AndroidLibrary;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/help/about/License;)V

    const/16 v2, 0x14

    aput-object v1, v0, v2

    .line 145
    new-instance v1, Lcom/squareup/ui/help/about/AndroidLibrary;

    .line 149
    sget-object v2, Lcom/squareup/ui/help/about/License;->APACHE_2_0:Lcom/squareup/ui/help/about/License;

    const-string v5, "RxJava"

    const-string v6, "https://github.com/ReactiveX/RxJava"

    .line 145
    invoke-direct {v1, v5, v4, v6, v2}, Lcom/squareup/ui/help/about/AndroidLibrary;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/help/about/License;)V

    const/16 v2, 0x15

    aput-object v1, v0, v2

    .line 151
    new-instance v1, Lcom/squareup/ui/help/about/AndroidLibrary;

    .line 155
    sget-object v2, Lcom/squareup/ui/help/about/License;->MIT:Lcom/squareup/ui/help/about/License;

    const-string v4, "Spongy Castle"

    const-string v5, "Roberto Tyley"

    const-string v6, "https://rtyley.github.io/spongycastle/"

    .line 151
    invoke-direct {v1, v4, v5, v6, v2}, Lcom/squareup/ui/help/about/AndroidLibrary;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/help/about/License;)V

    const/16 v2, 0x16

    aput-object v1, v0, v2

    .line 157
    new-instance v1, Lcom/squareup/ui/help/about/AndroidLibrary;

    .line 161
    sget-object v2, Lcom/squareup/ui/help/about/License;->APACHE_2_0:Lcom/squareup/ui/help/about/License;

    const-string v4, "Emil Sj\u00f6lander"

    const-string v5, "Sticky List Headers"

    const-string v6, "https://github.com/emilsjolander/StickyListHeaders"

    .line 157
    invoke-direct {v1, v5, v4, v6, v2}, Lcom/squareup/ui/help/about/AndroidLibrary;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/help/about/License;)V

    const/16 v2, 0x17

    aput-object v1, v0, v2

    .line 163
    new-instance v1, Lcom/squareup/ui/help/about/AndroidLibrary;

    .line 167
    sget-object v2, Lcom/squareup/ui/help/about/License;->APACHE_2_0:Lcom/squareup/ui/help/about/License;

    const-string v5, "Sticky Scroll View Items"

    const-string v6, "https://github.com/emilsjolander/StickyScrollViewItems"

    .line 163
    invoke-direct {v1, v5, v4, v6, v2}, Lcom/squareup/ui/help/about/AndroidLibrary;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/help/about/License;)V

    const/16 v2, 0x18

    aput-object v1, v0, v2

    .line 169
    new-instance v1, Lcom/squareup/ui/help/about/AndroidLibrary;

    .line 173
    sget-object v2, Lcom/squareup/ui/help/about/License;->APACHE_2_0:Lcom/squareup/ui/help/about/License;

    const-string v4, "Tape"

    const-string v5, "https://square.github.io/tape/"

    .line 169
    invoke-direct {v1, v4, v3, v5, v2}, Lcom/squareup/ui/help/about/AndroidLibrary;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/help/about/License;)V

    const/16 v2, 0x19

    aput-object v1, v0, v2

    .line 175
    new-instance v1, Lcom/squareup/ui/help/about/AndroidLibrary;

    .line 179
    sget-object v2, Lcom/squareup/ui/help/about/License;->APACHE_2_0:Lcom/squareup/ui/help/about/License;

    const-string v4, "Jake Wharton"

    const-string v5, "Timber"

    const-string v6, "https://github.com/JakeWharton/timber"

    .line 175
    invoke-direct {v1, v5, v4, v6, v2}, Lcom/squareup/ui/help/about/AndroidLibrary;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/help/about/License;)V

    const/16 v2, 0x1a

    aput-object v1, v0, v2

    .line 181
    new-instance v1, Lcom/squareup/ui/help/about/AndroidLibrary;

    .line 185
    sget-object v2, Lcom/squareup/ui/help/about/License;->APACHE_2_0:Lcom/squareup/ui/help/about/License;

    const-string v5, "TimesSquare for Android"

    const-string v6, "https://github.com/square/android-times-square"

    .line 181
    invoke-direct {v1, v5, v3, v6, v2}, Lcom/squareup/ui/help/about/AndroidLibrary;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/help/about/License;)V

    const/16 v2, 0x1b

    aput-object v1, v0, v2

    .line 187
    new-instance v1, Lcom/squareup/ui/help/about/AndroidLibrary;

    .line 191
    sget-object v2, Lcom/squareup/ui/help/about/License;->APACHE_2_0:Lcom/squareup/ui/help/about/License;

    const-string v5, "ViewPagerIndicator"

    const-string v6, "https://github.com/JakeWharton/ViewPagerIndicator"

    .line 187
    invoke-direct {v1, v5, v4, v6, v2}, Lcom/squareup/ui/help/about/AndroidLibrary;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/help/about/License;)V

    const/16 v2, 0x1c

    aput-object v1, v0, v2

    .line 193
    new-instance v1, Lcom/squareup/ui/help/about/AndroidLibrary;

    .line 197
    sget-object v2, Lcom/squareup/ui/help/about/License;->APACHE_2_0:Lcom/squareup/ui/help/about/License;

    const-string v4, "Wire"

    const-string v5, "https://github.com/square/wire"

    .line 193
    invoke-direct {v1, v4, v3, v5, v2}, Lcom/squareup/ui/help/about/AndroidLibrary;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/squareup/ui/help/about/License;)V

    const/16 v2, 0x1d

    aput-object v1, v0, v2

    .line 14
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/help/about/AndroidLibraries;->librariesUsed:Ljava/util/List;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getLibrariesUsed()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/ui/help/about/AndroidLibrary;",
            ">;"
        }
    .end annotation

    .line 14
    sget-object v0, Lcom/squareup/ui/help/about/AndroidLibraries;->librariesUsed:Ljava/util/List;

    return-object v0
.end method
