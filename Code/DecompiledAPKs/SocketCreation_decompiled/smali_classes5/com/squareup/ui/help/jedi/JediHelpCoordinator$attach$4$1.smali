.class final Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$4$1;
.super Ljava/lang/Object;
.source "JediHelpCoordinator.kt"

# interfaces
.implements Lrx/functions/Action1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$4;->invoke()Lrx/Subscription;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lrx/functions/Action1<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "header",
        "",
        "kotlin.jvm.PlatformType",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$4;


# direct methods
.method constructor <init>(Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$4;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$4$1;->this$0:Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$4;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic call(Ljava/lang/Object;)V
    .locals 0

    .line 32
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$4$1;->call(Ljava/lang/String;)V

    return-void
.end method

.method public final call(Ljava/lang/String;)V
    .locals 3

    .line 104
    iget-object v0, p0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$4$1;->this$0:Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$4;

    iget-object v0, v0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$4;->this$0:Lcom/squareup/ui/help/jedi/JediHelpCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/help/jedi/JediHelpCoordinator;->access$getActionBar$p(Lcom/squareup/ui/help/jedi/JediHelpCoordinator;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$4$1;->this$0:Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$4;

    iget-object v1, v1, Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$4;->this$0:Lcom/squareup/ui/help/jedi/JediHelpCoordinator;

    const-string v2, "header"

    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$4$1;->this$0:Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$4;

    iget-object v2, v2, Lcom/squareup/ui/help/jedi/JediHelpCoordinator$attach$4;->$screen:Lcom/squareup/ui/help/jedi/JediHelpScreen;

    invoke-virtual {v2}, Lcom/squareup/ui/help/jedi/JediHelpScreen;->getFirstScreen()Z

    move-result v2

    invoke-static {v1, p1, v2}, Lcom/squareup/ui/help/jedi/JediHelpCoordinator;->access$getActionBarConfig(Lcom/squareup/ui/help/jedi/JediHelpCoordinator;Ljava/lang/String;Z)Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    return-void
.end method
