.class public final Lcom/squareup/ui/help/referrals/ReferralsSection;
.super Lcom/squareup/ui/help/AbstractHelpSection;
.source "ReferralsSection.kt"


# annotations
.annotation runtime Lcom/squareup/dagger/SingleInMainActivity;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/help/referrals/ReferralsSection$ListEntry;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0008\u0007\u0018\u00002\u00020\u0001:\u0001\tB\u000f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0008\u0010\u0005\u001a\u00020\u0006H\u0016J\u0008\u0010\u0007\u001a\u00020\u0008H\u0016\u00a8\u0006\n"
    }
    d2 = {
        "Lcom/squareup/ui/help/referrals/ReferralsSection;",
        "Lcom/squareup/ui/help/AbstractHelpSection;",
        "referralsAccess",
        "Lcom/squareup/ui/help/referrals/ReferralsAccess;",
        "(Lcom/squareup/ui/help/referrals/ReferralsAccess;)V",
        "getInitialScreen",
        "Lcom/squareup/ui/main/RegisterTreeKey;",
        "tapName",
        "Lcom/squareup/analytics/RegisterTapName;",
        "ListEntry",
        "help_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/squareup/ui/help/referrals/ReferralsAccess;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "referralsAccess"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    check-cast p1, Lcom/squareup/applet/SectionAccess;

    invoke-direct {p0, p1}, Lcom/squareup/ui/help/AbstractHelpSection;-><init>(Lcom/squareup/applet/SectionAccess;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic getInitialScreen()Lcom/squareup/container/ContainerTreeKey;
    .locals 1

    .line 14
    invoke-virtual {p0}, Lcom/squareup/ui/help/referrals/ReferralsSection;->getInitialScreen()Lcom/squareup/ui/main/RegisterTreeKey;

    move-result-object v0

    check-cast v0, Lcom/squareup/container/ContainerTreeKey;

    return-object v0
.end method

.method public getInitialScreen()Lcom/squareup/ui/main/RegisterTreeKey;
    .locals 2

    .line 17
    sget-object v0, Lcom/squareup/ui/help/referrals/ReferralMasterDetailScreen;->INSTANCE:Lcom/squareup/ui/help/referrals/ReferralMasterDetailScreen;

    const-string v1, "ReferralMasterDetailScreen.INSTANCE"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    check-cast v0, Lcom/squareup/ui/main/RegisterTreeKey;

    return-object v0
.end method

.method public tapName()Lcom/squareup/analytics/RegisterTapName;
    .locals 1

    .line 19
    sget-object v0, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_REFERRAL:Lcom/squareup/analytics/RegisterTapName;

    return-object v0
.end method
