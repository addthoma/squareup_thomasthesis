.class public final Lcom/squareup/ui/help/help/HelpSection$ListEntry;
.super Lcom/squareup/ui/help/HelpAppletSectionsListEntry;
.source "HelpSection.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/help/help/HelpSection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ListEntry"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0008\n\u0000\n\u0002\u0010\u000e\n\u0000\u0018\u00002\u00020\u0001B\u001f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u000e\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\nH\u0016J\u000e\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\r0\nH\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000e"
    }
    d2 = {
        "Lcom/squareup/ui/help/help/HelpSection$ListEntry;",
        "Lcom/squareup/ui/help/HelpAppletSectionsListEntry;",
        "section",
        "Lcom/squareup/ui/help/help/HelpSection;",
        "res",
        "Lcom/squareup/util/Res;",
        "messagesVisibility",
        "Lcom/squareup/ui/help/messages/MessagesVisibility;",
        "(Lcom/squareup/ui/help/help/HelpSection;Lcom/squareup/util/Res;Lcom/squareup/ui/help/messages/MessagesVisibility;)V",
        "badgeCount",
        "Lio/reactivex/Observable;",
        "",
        "getValueTextObservable",
        "",
        "help_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final messagesVisibility:Lcom/squareup/ui/help/messages/MessagesVisibility;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/help/help/HelpSection;Lcom/squareup/util/Res;Lcom/squareup/ui/help/messages/MessagesVisibility;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "section"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "messagesVisibility"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 47
    check-cast p1, Lcom/squareup/applet/AppletSection;

    sget v0, Lcom/squareup/applet/help/R$string;->help:I

    invoke-direct {p0, p1, v0, p2}, Lcom/squareup/ui/help/HelpAppletSectionsListEntry;-><init>(Lcom/squareup/applet/AppletSection;ILcom/squareup/util/Res;)V

    iput-object p3, p0, Lcom/squareup/ui/help/help/HelpSection$ListEntry;->messagesVisibility:Lcom/squareup/ui/help/messages/MessagesVisibility;

    return-void
.end method

.method public static final synthetic access$getRes$p(Lcom/squareup/ui/help/help/HelpSection$ListEntry;)Lcom/squareup/util/Res;
    .locals 0

    .line 43
    iget-object p0, p0, Lcom/squareup/ui/help/help/HelpSection$ListEntry;->res:Lcom/squareup/util/Res;

    return-object p0
.end method


# virtual methods
.method public badgeCount()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 56
    iget-object v0, p0, Lcom/squareup/ui/help/help/HelpSection$ListEntry;->messagesVisibility:Lcom/squareup/ui/help/messages/MessagesVisibility;

    invoke-virtual {v0}, Lcom/squareup/ui/help/messages/MessagesVisibility;->badgeCount()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public getValueTextObservable()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 51
    invoke-virtual {p0}, Lcom/squareup/ui/help/help/HelpSection$ListEntry;->badgeCount()Lio/reactivex/Observable;

    move-result-object v0

    .line 52
    new-instance v1, Lcom/squareup/ui/help/help/HelpSection$ListEntry$getValueTextObservable$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/help/help/HelpSection$ListEntry$getValueTextObservable$1;-><init>(Lcom/squareup/ui/help/help/HelpSection$ListEntry;)V

    check-cast v1, Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "badgeCount()\n          .\u2026tring.new_word) else \"\" }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method
