.class final Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$searchResponseForString$1$1;
.super Ljava/lang/Object;
.source "JediHelpScopeRunner.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$searchResponseForString$1;->apply(Lio/reactivex/Observable;)Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/ObservableSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0014\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000e\n\u0000\u0010\u0000\u001a&\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002 \u0003*\u0012\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u0002\u0018\u00010\u00010\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/jedi/JediHelpScreenData;",
        "kotlin.jvm.PlatformType",
        "it",
        "",
        "apply"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$searchResponseForString$1;


# direct methods
.method constructor <init>(Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$searchResponseForString$1;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$searchResponseForString$1$1;->this$0:Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$searchResponseForString$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/String;)Lio/reactivex/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/jedi/JediHelpScreenData;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 228
    iget-object v0, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$searchResponseForString$1$1;->this$0:Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$searchResponseForString$1;

    iget-object v0, v0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$searchResponseForString$1;->$screen:Lcom/squareup/ui/help/jedi/JediHelpScreen;

    invoke-virtual {v0}, Lcom/squareup/ui/help/jedi/JediHelpScreen;->getScreenData()Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 229
    new-instance v1, Lcom/squareup/protos/jedi/service/SearchRequest$Builder;

    invoke-direct {v1}, Lcom/squareup/protos/jedi/service/SearchRequest$Builder;-><init>()V

    .line 230
    invoke-virtual {v0}, Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;->getSessionToken()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/protos/jedi/service/SearchRequest$Builder;->session_token(Ljava/lang/String;)Lcom/squareup/protos/jedi/service/SearchRequest$Builder;

    move-result-object v1

    .line 231
    invoke-virtual {v1, p1}, Lcom/squareup/protos/jedi/service/SearchRequest$Builder;->search_text(Ljava/lang/String;)Lcom/squareup/protos/jedi/service/SearchRequest$Builder;

    move-result-object v1

    .line 232
    invoke-virtual {v1}, Lcom/squareup/protos/jedi/service/SearchRequest$Builder;->build()Lcom/squareup/protos/jedi/service/SearchRequest;

    move-result-object v1

    .line 234
    iget-object v2, p0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$searchResponseForString$1$1;->this$0:Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$searchResponseForString$1;

    iget-object v2, v2, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$searchResponseForString$1;->this$0:Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;

    invoke-static {v2}, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;->access$getJediService$p(Lcom/squareup/ui/help/jedi/JediHelpScopeRunner;)Lcom/squareup/server/help/JediService;

    move-result-object v2

    const-string v3, "request"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {v2, v1}, Lcom/squareup/server/help/JediService;->searchText(Lcom/squareup/protos/jedi/service/SearchRequest;)Lcom/squareup/server/help/JediService$JediServiceStandardResponse;

    move-result-object v1

    .line 235
    invoke-virtual {v1}, Lcom/squareup/server/help/JediService$JediServiceStandardResponse;->successOrFailure()Lio/reactivex/Single;

    move-result-object v1

    .line 236
    new-instance v2, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$searchResponseForString$1$1$1;

    invoke-direct {v2, p0, v0, p1}, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$searchResponseForString$1$1$1;-><init>(Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$searchResponseForString$1$1;Lcom/squareup/jedi/JediHelpScreenData$JediHelpPanelScreenData;Ljava/lang/String;)V

    check-cast v2, Lio/reactivex/functions/Function;

    invoke-virtual {v1, v2}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    .line 243
    invoke-virtual {p1}, Lio/reactivex/Single;->toObservable()Lio/reactivex/Observable;

    move-result-object p1

    .line 244
    sget-object v0, Lcom/squareup/jedi/JediHelpScreenData$JediHelpLoadingScreenData;->INSTANCE:Lcom/squareup/jedi/JediHelpScreenData$JediHelpLoadingScreenData;

    invoke-virtual {p1, v0}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1

    .line 228
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string v0, "null cannot be cast to non-null type com.squareup.jedi.JediHelpScreenData.JediHelpPanelScreenData"

    invoke-direct {p1, v0}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 76
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$searchResponseForString$1$1;->apply(Ljava/lang/String;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method
