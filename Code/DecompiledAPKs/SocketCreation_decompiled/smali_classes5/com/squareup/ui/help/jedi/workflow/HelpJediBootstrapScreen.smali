.class public final Lcom/squareup/ui/help/jedi/workflow/HelpJediBootstrapScreen;
.super Lcom/squareup/container/BootstrapTreeKey;
.source "HelpJediBootstrapScreen.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0007\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0008H\u0016J\u0008\u0010\t\u001a\u00020\nH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/ui/help/jedi/workflow/HelpJediBootstrapScreen;",
        "Lcom/squareup/container/BootstrapTreeKey;",
        "props",
        "Lcom/squareup/jedi/JediWorkflowProps;",
        "(Lcom/squareup/jedi/JediWorkflowProps;)V",
        "doRegister",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "getParentKey",
        "Lcom/squareup/ui/help/jedi/workflow/HelpJediWorkflowScope;",
        "help_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final props:Lcom/squareup/jedi/JediWorkflowProps;


# direct methods
.method public constructor <init>(Lcom/squareup/jedi/JediWorkflowProps;)V
    .locals 1

    const-string v0, "props"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    invoke-direct {p0}, Lcom/squareup/container/BootstrapTreeKey;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/help/jedi/workflow/HelpJediBootstrapScreen;->props:Lcom/squareup/jedi/JediWorkflowProps;

    return-void
.end method


# virtual methods
.method public doRegister(Lmortar/MortarScope;)V
    .locals 2

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    sget-object v0, Lcom/squareup/ui/help/jedi/workflow/JediWorkflowRunner;->Companion:Lcom/squareup/ui/help/jedi/workflow/JediWorkflowRunner$Companion;

    iget-object v1, p0, Lcom/squareup/ui/help/jedi/workflow/HelpJediBootstrapScreen;->props:Lcom/squareup/jedi/JediWorkflowProps;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/ui/help/jedi/workflow/JediWorkflowRunner$Companion;->startNewWorkflow(Lmortar/MortarScope;Lcom/squareup/jedi/JediWorkflowProps;)V

    return-void
.end method

.method public getParentKey()Lcom/squareup/ui/help/jedi/workflow/HelpJediWorkflowScope;
    .locals 1

    .line 17
    sget-object v0, Lcom/squareup/ui/help/jedi/workflow/HelpJediWorkflowScope;->INSTANCE:Lcom/squareup/ui/help/jedi/workflow/HelpJediWorkflowScope;

    return-object v0
.end method

.method public bridge synthetic getParentKey()Ljava/lang/Object;
    .locals 1

    .line 9
    invoke-virtual {p0}, Lcom/squareup/ui/help/jedi/workflow/HelpJediBootstrapScreen;->getParentKey()Lcom/squareup/ui/help/jedi/workflow/HelpJediWorkflowScope;

    move-result-object v0

    return-object v0
.end method
