.class public final Lcom/squareup/ui/help/tutorials/content/WhatsNewTutorial;
.super Lcom/squareup/ui/help/HelpAppletContent;
.source "WhatsNewTutorial.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0017\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0008\u0010\u0007\u001a\u00020\u0008H\u0016J\u0008\u0010\t\u001a\u00020\nH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"
    }
    d2 = {
        "Lcom/squareup/ui/help/tutorials/content/WhatsNewTutorial;",
        "Lcom/squareup/ui/help/HelpAppletContent;",
        "whatsNewSettings",
        "Lcom/squareup/tour/WhatsNewSettings;",
        "appNameFormatter",
        "Lcom/squareup/util/AppNameFormatter;",
        "(Lcom/squareup/tour/WhatsNewSettings;Lcom/squareup/util/AppNameFormatter;)V",
        "shouldDisplay",
        "",
        "updateBadge",
        "",
        "help_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final whatsNewSettings:Lcom/squareup/tour/WhatsNewSettings;


# direct methods
.method public constructor <init>(Lcom/squareup/tour/WhatsNewSettings;Lcom/squareup/util/AppNameFormatter;)V
    .locals 9
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string/jumbo v0, "whatsNewSettings"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "appNameFormatter"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    sget v2, Lcom/squareup/applet/help/R$string;->feature_tour:I

    .line 15
    sget v0, Lcom/squareup/applet/help/R$string;->whats_new_subtext:I

    invoke-interface {p2, v0}, Lcom/squareup/util/AppNameFormatter;->getStringWithAppName(I)Ljava/lang/CharSequence;

    move-result-object v3

    .line 16
    sget-object v5, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_WHATS_NEW:Lcom/squareup/analytics/RegisterTapName;

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/16 v7, 0x14

    const/4 v8, 0x0

    move-object v1, p0

    .line 13
    invoke-direct/range {v1 .. v8}, Lcom/squareup/ui/help/HelpAppletContent;-><init>(ILjava/lang/CharSequence;Ljava/lang/Integer;Lcom/squareup/analytics/RegisterTapName;Ljava/lang/Integer;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/ui/help/tutorials/content/WhatsNewTutorial;->whatsNewSettings:Lcom/squareup/tour/WhatsNewSettings;

    return-void
.end method


# virtual methods
.method public shouldDisplay()Z
    .locals 1

    .line 20
    iget-object v0, p0, Lcom/squareup/ui/help/tutorials/content/WhatsNewTutorial;->whatsNewSettings:Lcom/squareup/tour/WhatsNewSettings;

    invoke-virtual {v0}, Lcom/squareup/tour/WhatsNewSettings;->hasPages()Z

    move-result v0

    return v0
.end method

.method public updateBadge()V
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/ui/help/tutorials/content/WhatsNewTutorial;->whatsNewSettings:Lcom/squareup/tour/WhatsNewSettings;

    invoke-virtual {v0}, Lcom/squareup/tour/WhatsNewSettings;->hasUnseenPages()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 25
    sget v0, Lcom/squareup/applet/help/R$string;->new_word:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/help/HelpAppletContent;->badgeStringId:Ljava/lang/Integer;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 27
    check-cast v0, Ljava/lang/Integer;

    iput-object v0, p0, Lcom/squareup/ui/help/HelpAppletContent;->badgeStringId:Ljava/lang/Integer;

    :goto_0
    return-void
.end method
