.class public final Lcom/squareup/ui/help/tutorials/content/AcceptCreditCards;
.super Lcom/squareup/ui/help/HelpAppletContent;
.source "AcceptCreditCards.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"
    }
    d2 = {
        "Lcom/squareup/ui/help/tutorials/content/AcceptCreditCards;",
        "Lcom/squareup/ui/help/HelpAppletContent;",
        "()V",
        "help_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 8
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 9
    sget v1, Lcom/squareup/onboarding/common/R$string;->activation_account_title:I

    .line 10
    sget-object v4, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_ONBOARDING:Lcom/squareup/analytics/RegisterTapName;

    .line 11
    sget v0, Lcom/squareup/applet/help/R$string;->new_word:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v6, 0x6

    const/4 v7, 0x0

    move-object v0, p0

    .line 8
    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/help/HelpAppletContent;-><init>(ILjava/lang/CharSequence;Ljava/lang/Integer;Lcom/squareup/analytics/RegisterTapName;Ljava/lang/Integer;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method
