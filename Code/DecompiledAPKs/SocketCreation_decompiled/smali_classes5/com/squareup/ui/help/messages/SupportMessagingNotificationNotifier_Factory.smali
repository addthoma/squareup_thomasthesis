.class public final Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier_Factory;
.super Ljava/lang/Object;
.source "SupportMessagingNotificationNotifier_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier;",
        ">;"
    }
.end annotation


# instance fields
.field private final contextProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;"
        }
    .end annotation
.end field

.field private final delegateProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pushmessages/PushMessageDelegate;",
            ">;"
        }
    .end annotation
.end field

.field private final foregroundedActivityListenerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/messages/ForegroundedActivityListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mainSchedulerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;"
        }
    .end annotation
.end field

.field private final messagesVisibilityProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/messages/MessagesVisibility;",
            ">;"
        }
    .end annotation
.end field

.field private final notificationManagerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Landroid/app/NotificationManager;",
            ">;"
        }
    .end annotation
.end field

.field private final notificationWrapperProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notification/NotificationWrapper;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pushmessages/PushMessageDelegate;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/app/NotificationManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notification/NotificationWrapper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/messages/MessagesVisibility;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/messages/ForegroundedActivityListener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;)V"
        }
    .end annotation

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier_Factory;->delegateProvider:Ljavax/inject/Provider;

    .line 44
    iput-object p2, p0, Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier_Factory;->notificationManagerProvider:Ljavax/inject/Provider;

    .line 45
    iput-object p3, p0, Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier_Factory;->notificationWrapperProvider:Ljavax/inject/Provider;

    .line 46
    iput-object p4, p0, Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier_Factory;->contextProvider:Ljavax/inject/Provider;

    .line 47
    iput-object p5, p0, Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier_Factory;->messagesVisibilityProvider:Ljavax/inject/Provider;

    .line 48
    iput-object p6, p0, Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier_Factory;->foregroundedActivityListenerProvider:Ljavax/inject/Provider;

    .line 49
    iput-object p7, p0, Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier_Factory;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/pushmessages/PushMessageDelegate;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/app/NotificationManager;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/notification/NotificationWrapper;",
            ">;",
            "Ljavax/inject/Provider<",
            "Landroid/app/Application;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/messages/MessagesVisibility;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/messages/ForegroundedActivityListener;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lio/reactivex/Scheduler;",
            ">;)",
            "Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier_Factory;"
        }
    .end annotation

    .line 65
    new-instance v8, Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier_Factory;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v8
.end method

.method public static newInstance(Lcom/squareup/pushmessages/PushMessageDelegate;Landroid/app/NotificationManager;Lcom/squareup/notification/NotificationWrapper;Landroid/app/Application;Lcom/squareup/ui/help/messages/MessagesVisibility;Lcom/squareup/ui/help/messages/ForegroundedActivityListener;Lio/reactivex/Scheduler;)Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier;
    .locals 9

    .line 72
    new-instance v8, Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier;

    move-object v0, v8

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier;-><init>(Lcom/squareup/pushmessages/PushMessageDelegate;Landroid/app/NotificationManager;Lcom/squareup/notification/NotificationWrapper;Landroid/app/Application;Lcom/squareup/ui/help/messages/MessagesVisibility;Lcom/squareup/ui/help/messages/ForegroundedActivityListener;Lio/reactivex/Scheduler;)V

    return-object v8
.end method


# virtual methods
.method public get()Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier;
    .locals 8

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier_Factory;->delegateProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/squareup/pushmessages/PushMessageDelegate;

    iget-object v0, p0, Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier_Factory;->notificationManagerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Landroid/app/NotificationManager;

    iget-object v0, p0, Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier_Factory;->notificationWrapperProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/squareup/notification/NotificationWrapper;

    iget-object v0, p0, Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier_Factory;->contextProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Landroid/app/Application;

    iget-object v0, p0, Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier_Factory;->messagesVisibilityProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/squareup/ui/help/messages/MessagesVisibility;

    iget-object v0, p0, Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier_Factory;->foregroundedActivityListenerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/squareup/ui/help/messages/ForegroundedActivityListener;

    iget-object v0, p0, Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier_Factory;->mainSchedulerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lio/reactivex/Scheduler;

    invoke-static/range {v1 .. v7}, Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier_Factory;->newInstance(Lcom/squareup/pushmessages/PushMessageDelegate;Landroid/app/NotificationManager;Lcom/squareup/notification/NotificationWrapper;Landroid/app/Application;Lcom/squareup/ui/help/messages/MessagesVisibility;Lcom/squareup/ui/help/messages/ForegroundedActivityListener;Lio/reactivex/Scheduler;)Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 12
    invoke-virtual {p0}, Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier_Factory;->get()Lcom/squareup/ui/help/messages/SupportMessagingNotificationNotifier;

    move-result-object v0

    return-object v0
.end method
