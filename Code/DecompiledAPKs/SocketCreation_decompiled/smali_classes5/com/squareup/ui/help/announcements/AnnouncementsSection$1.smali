.class public final Lcom/squareup/ui/help/announcements/AnnouncementsSection$1;
.super Lcom/squareup/applet/SectionAccess;
.source "AnnouncementsSection.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/help/announcements/AnnouncementsSection;-><init>(Lcom/squareup/settings/server/Features;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0011\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H\u0016\u00a8\u0006\u0004"
    }
    d2 = {
        "com/squareup/ui/help/announcements/AnnouncementsSection$1",
        "Lcom/squareup/applet/SectionAccess;",
        "determineVisibility",
        "",
        "help_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $features:Lcom/squareup/settings/server/Features;


# direct methods
.method constructor <init>(Lcom/squareup/settings/server/Features;)V
    .locals 0

    .line 21
    iput-object p1, p0, Lcom/squareup/ui/help/announcements/AnnouncementsSection$1;->$features:Lcom/squareup/settings/server/Features;

    invoke-direct {p0}, Lcom/squareup/applet/SectionAccess;-><init>()V

    return-void
.end method


# virtual methods
.method public determineVisibility()Z
    .locals 2

    .line 24
    iget-object v0, p0, Lcom/squareup/ui/help/announcements/AnnouncementsSection$1;->$features:Lcom/squareup/settings/server/Features;

    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->NOTIFICATION_CENTER:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/help/announcements/AnnouncementsSection$1;->$features:Lcom/squareup/settings/server/Features;

    .line 25
    sget-object v1, Lcom/squareup/settings/server/Features$Feature;->HIDE_ANNOUNCEMENTS_SECTION:Lcom/squareup/settings/server/Features$Feature;

    .line 24
    invoke-interface {v0, v1}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method
