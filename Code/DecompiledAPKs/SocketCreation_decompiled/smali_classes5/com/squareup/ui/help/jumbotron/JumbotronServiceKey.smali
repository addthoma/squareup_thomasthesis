.class public interface abstract Lcom/squareup/ui/help/jumbotron/JumbotronServiceKey;
.super Ljava/lang/Object;
.source "JumbotronServiceKey.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/help/jumbotron/JumbotronServiceKey$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\u0008f\u0018\u0000 \u00042\u00020\u0001:\u0001\u0004J\u0008\u0010\u0002\u001a\u00020\u0003H&\u00a8\u0006\u0005"
    }
    d2 = {
        "Lcom/squareup/ui/help/jumbotron/JumbotronServiceKey;",
        "",
        "inAppMessageKey",
        "",
        "Companion",
        "help_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ui/help/jumbotron/JumbotronServiceKey$Companion;

.field public static final POS_SERVICE_KEY:Ljava/lang/String; = "Register"

.field public static final T2_SERVICE_KEY:Ljava/lang/String; = "T2"

.field public static final X2_SERVICE_KEY:Ljava/lang/String; = "X2"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget-object v0, Lcom/squareup/ui/help/jumbotron/JumbotronServiceKey$Companion;->$$INSTANCE:Lcom/squareup/ui/help/jumbotron/JumbotronServiceKey$Companion;

    sput-object v0, Lcom/squareup/ui/help/jumbotron/JumbotronServiceKey;->Companion:Lcom/squareup/ui/help/jumbotron/JumbotronServiceKey$Companion;

    return-void
.end method


# virtual methods
.method public abstract inAppMessageKey()Ljava/lang/String;
.end method
