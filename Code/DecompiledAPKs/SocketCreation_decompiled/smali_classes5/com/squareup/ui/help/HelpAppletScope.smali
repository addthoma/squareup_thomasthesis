.class public final Lcom/squareup/ui/help/HelpAppletScope;
.super Lcom/squareup/ui/main/InMainActivityScope;
.source "HelpAppletScope.java"

# interfaces
.implements Lcom/squareup/container/RegistersInScope;


# annotations
.annotation runtime Lcom/squareup/ui/WithComponent;
    value = Lcom/squareup/ui/help/HelpAppletScope$Component;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/help/HelpAppletScope$ReferralModule;,
        Lcom/squareup/ui/help/HelpAppletScope$HelpAppletModule;,
        Lcom/squareup/ui/help/HelpAppletScope$Component;,
        Lcom/squareup/ui/help/HelpAppletScope$SupportComponents;,
        Lcom/squareup/ui/help/HelpAppletScope$ParentComponent;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/help/HelpAppletScope;",
            ">;"
        }
    .end annotation
.end field

.field public static final INSTANCE:Lcom/squareup/ui/help/HelpAppletScope;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 38
    new-instance v0, Lcom/squareup/ui/help/HelpAppletScope;

    invoke-direct {v0}, Lcom/squareup/ui/help/HelpAppletScope;-><init>()V

    sput-object v0, Lcom/squareup/ui/help/HelpAppletScope;->INSTANCE:Lcom/squareup/ui/help/HelpAppletScope;

    .line 118
    sget-object v0, Lcom/squareup/ui/help/HelpAppletScope;->INSTANCE:Lcom/squareup/ui/help/HelpAppletScope;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->forSingleton(Lcom/squareup/container/ContainerTreeKey;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/help/HelpAppletScope;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .line 40
    invoke-direct {p0}, Lcom/squareup/ui/main/InMainActivityScope;-><init>()V

    return-void
.end method


# virtual methods
.method public register(Lmortar/MortarScope;)V
    .locals 2

    .line 44
    const-class v0, Lcom/squareup/ui/help/HelpAppletScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/help/HelpAppletScope$Component;

    .line 45
    invoke-interface {v0}, Lcom/squareup/ui/help/HelpAppletScope$Component;->helpAppletScopeRunner()Lcom/squareup/ui/help/HelpAppletScopeRunner;

    move-result-object v1

    invoke-virtual {p1, v1}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    .line 46
    invoke-interface {v0}, Lcom/squareup/ui/help/HelpAppletScope$Component;->troubleshootingScopeRunner()Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;

    move-result-object v0

    invoke-virtual {p1, v0}, Lmortar/MortarScope;->register(Lmortar/Scoped;)V

    return-void
.end method
