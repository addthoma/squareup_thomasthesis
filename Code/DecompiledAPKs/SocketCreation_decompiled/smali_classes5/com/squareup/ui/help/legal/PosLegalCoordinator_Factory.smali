.class public final Lcom/squareup/ui/help/legal/PosLegalCoordinator_Factory;
.super Ljava/lang/Object;
.source "PosLegalCoordinator_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/help/legal/PosLegalCoordinator;",
        ">;"
    }
.end annotation


# instance fields
.field private final helpAppletScopeRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/HelpAppletScopeRunner;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/HelpAppletScopeRunner;",
            ">;)V"
        }
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/ui/help/legal/PosLegalCoordinator_Factory;->helpAppletScopeRunnerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;)Lcom/squareup/ui/help/legal/PosLegalCoordinator_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/HelpAppletScopeRunner;",
            ">;)",
            "Lcom/squareup/ui/help/legal/PosLegalCoordinator_Factory;"
        }
    .end annotation

    .line 31
    new-instance v0, Lcom/squareup/ui/help/legal/PosLegalCoordinator_Factory;

    invoke-direct {v0, p0}, Lcom/squareup/ui/help/legal/PosLegalCoordinator_Factory;-><init>(Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/ui/help/HelpAppletScopeRunner;)Lcom/squareup/ui/help/legal/PosLegalCoordinator;
    .locals 1

    .line 35
    new-instance v0, Lcom/squareup/ui/help/legal/PosLegalCoordinator;

    invoke-direct {v0, p0}, Lcom/squareup/ui/help/legal/PosLegalCoordinator;-><init>(Lcom/squareup/ui/help/HelpAppletScopeRunner;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/help/legal/PosLegalCoordinator;
    .locals 1

    .line 26
    iget-object v0, p0, Lcom/squareup/ui/help/legal/PosLegalCoordinator_Factory;->helpAppletScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/help/HelpAppletScopeRunner;

    invoke-static {v0}, Lcom/squareup/ui/help/legal/PosLegalCoordinator_Factory;->newInstance(Lcom/squareup/ui/help/HelpAppletScopeRunner;)Lcom/squareup/ui/help/legal/PosLegalCoordinator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/ui/help/legal/PosLegalCoordinator_Factory;->get()Lcom/squareup/ui/help/legal/PosLegalCoordinator;

    move-result-object v0

    return-object v0
.end method
