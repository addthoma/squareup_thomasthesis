.class Lcom/squareup/ui/help/announcements/AnnouncementsCoordinator$AnnouncementsAdapter;
.super Landroid/widget/BaseAdapter;
.source "AnnouncementsCoordinator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/help/announcements/AnnouncementsCoordinator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AnnouncementsAdapter"
.end annotation


# instance fields
.field private announcements:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/server/messages/Message;",
            ">;"
        }
    .end annotation
.end field

.field private final helpAppletScopeRunner:Lcom/squareup/ui/help/HelpAppletScopeRunner;


# direct methods
.method private constructor <init>(Lcom/squareup/ui/help/HelpAppletScopeRunner;)V
    .locals 1

    .line 86
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 84
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/help/announcements/AnnouncementsCoordinator$AnnouncementsAdapter;->announcements:Ljava/util/List;

    .line 87
    iput-object p1, p0, Lcom/squareup/ui/help/announcements/AnnouncementsCoordinator$AnnouncementsAdapter;->helpAppletScopeRunner:Lcom/squareup/ui/help/HelpAppletScopeRunner;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/ui/help/HelpAppletScopeRunner;Lcom/squareup/ui/help/announcements/AnnouncementsCoordinator$1;)V
    .locals 0

    .line 80
    invoke-direct {p0, p1}, Lcom/squareup/ui/help/announcements/AnnouncementsCoordinator$AnnouncementsAdapter;-><init>(Lcom/squareup/ui/help/HelpAppletScopeRunner;)V

    return-void
.end method

.method private showNewBadge(Lcom/squareup/ui/account/view/SmartLineRow;ZLandroid/content/res/Resources;)V
    .locals 0

    .line 113
    invoke-virtual {p1, p2}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueVisible(Z)V

    if-eqz p2, :cond_0

    .line 114
    sget p2, Lcom/squareup/applet/help/R$string;->new_word:I

    invoke-virtual {p3, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    :cond_0
    const-string p2, ""

    :goto_0
    invoke-virtual {p1, p2}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueText(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .line 118
    iget-object v0, p0, Lcom/squareup/ui/help/announcements/AnnouncementsCoordinator$AnnouncementsAdapter;->announcements:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Lcom/squareup/server/messages/Message;
    .locals 1

    .line 122
    iget-object v0, p0, Lcom/squareup/ui/help/announcements/AnnouncementsCoordinator$AnnouncementsAdapter;->announcements:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/server/messages/Message;

    return-object p1
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 0

    .line 80
    invoke-virtual {p0, p1}, Lcom/squareup/ui/help/announcements/AnnouncementsCoordinator$AnnouncementsAdapter;->getItem(I)Lcom/squareup/server/messages/Message;

    move-result-object p1

    return-object p1
.end method

.method public getItemId(I)J
    .locals 2

    .line 126
    iget-object v0, p0, Lcom/squareup/ui/help/announcements/AnnouncementsCoordinator$AnnouncementsAdapter;->announcements:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/server/messages/Message;

    iget-object p1, p1, Lcom/squareup/server/messages/Message;->tracker_token:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result p1

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    if-nez p2, :cond_0

    .line 97
    sget p2, Lcom/squareup/applet/help/R$layout;->announcements_row:I

    invoke-static {p2, p3}, Lcom/squareup/util/Views;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 100
    :cond_0
    sget p3, Lcom/squareup/applet/help/R$id;->message:I

    invoke-static {p2, p3}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p3

    check-cast p3, Lcom/squareup/ui/account/view/SmartLineRow;

    .line 102
    invoke-virtual {p0, p1}, Lcom/squareup/ui/help/announcements/AnnouncementsCoordinator$AnnouncementsAdapter;->getItem(I)Lcom/squareup/server/messages/Message;

    move-result-object p1

    .line 104
    new-instance v0, Lcom/squareup/ui/help/announcements/-$$Lambda$AnnouncementsCoordinator$AnnouncementsAdapter$Nme0qleQH3I6IGD3ePpvGfGG2So;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/help/announcements/-$$Lambda$AnnouncementsCoordinator$AnnouncementsAdapter$Nme0qleQH3I6IGD3ePpvGfGG2So;-><init>(Lcom/squareup/ui/help/announcements/AnnouncementsCoordinator$AnnouncementsAdapter;Lcom/squareup/server/messages/Message;)V

    .line 105
    invoke-static {v0}, Lcom/squareup/debounce/Debouncers;->debounce(Landroid/view/View$OnClickListener;)Lcom/squareup/debounce/DebouncedOnClickListener;

    move-result-object v0

    .line 104
    invoke-virtual {p3, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 106
    iget-object v0, p1, Lcom/squareup/server/messages/Message;->title:Ljava/lang/String;

    invoke-virtual {p3, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setTitleText(Ljava/lang/CharSequence;)V

    .line 107
    sget v0, Lcom/squareup/marin/R$color;->marin_blue:I

    invoke-virtual {p3, v0}, Lcom/squareup/ui/account/view/SmartLineRow;->setValueColor(I)V

    .line 108
    iget-boolean p1, p1, Lcom/squareup/server/messages/Message;->read:Z

    xor-int/lit8 p1, p1, 0x1

    invoke-virtual {p2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-direct {p0, p3, p1, v0}, Lcom/squareup/ui/help/announcements/AnnouncementsCoordinator$AnnouncementsAdapter;->showNewBadge(Lcom/squareup/ui/account/view/SmartLineRow;ZLandroid/content/res/Resources;)V

    return-object p2
.end method

.method public synthetic lambda$getView$0$AnnouncementsCoordinator$AnnouncementsAdapter(Lcom/squareup/server/messages/Message;Landroid/view/View;)V
    .locals 0

    .line 105
    iget-object p2, p0, Lcom/squareup/ui/help/announcements/AnnouncementsCoordinator$AnnouncementsAdapter;->helpAppletScopeRunner:Lcom/squareup/ui/help/HelpAppletScopeRunner;

    invoke-virtual {p2, p1}, Lcom/squareup/ui/help/HelpAppletScopeRunner;->announcementSelected(Lcom/squareup/server/messages/Message;)V

    return-void
.end method

.method public setAnnouncements(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/squareup/server/messages/Message;",
            ">;)V"
        }
    .end annotation

    .line 91
    iput-object p1, p0, Lcom/squareup/ui/help/announcements/AnnouncementsCoordinator$AnnouncementsAdapter;->announcements:Ljava/util/List;

    .line 92
    invoke-virtual {p0}, Lcom/squareup/ui/help/announcements/AnnouncementsCoordinator$AnnouncementsAdapter;->notifyDataSetChanged()V

    return-void
.end method
