.class final Lcom/squareup/ui/help/about/LibrariesCoordinator$displayLibraries$1;
.super Lkotlin/jvm/internal/Lambda;
.source "LibrariesCoordinator.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/help/about/LibrariesCoordinator;->displayLibraries(Lcom/squareup/ui/help/about/LibrariesScreenData;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/ui/help/about/License;",
        "Lkotlin/Unit;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n\u00a2\u0006\u0002\u0008\u0004"
    }
    d2 = {
        "<anonymous>",
        "",
        "license",
        "Lcom/squareup/ui/help/about/License;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $screenData:Lcom/squareup/ui/help/about/LibrariesScreenData;

.field final synthetic this$0:Lcom/squareup/ui/help/about/LibrariesCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/help/about/LibrariesCoordinator;Lcom/squareup/ui/help/about/LibrariesScreenData;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/help/about/LibrariesCoordinator$displayLibraries$1;->this$0:Lcom/squareup/ui/help/about/LibrariesCoordinator;

    iput-object p2, p0, Lcom/squareup/ui/help/about/LibrariesCoordinator$displayLibraries$1;->$screenData:Lcom/squareup/ui/help/about/LibrariesScreenData;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 16
    check-cast p1, Lcom/squareup/ui/help/about/License;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/help/about/LibrariesCoordinator$displayLibraries$1;->invoke(Lcom/squareup/ui/help/about/License;)V

    sget-object p1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    return-object p1
.end method

.method public final invoke(Lcom/squareup/ui/help/about/License;)V
    .locals 1

    const-string v0, "license"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    iget-object v0, p0, Lcom/squareup/ui/help/about/LibrariesCoordinator$displayLibraries$1;->$screenData:Lcom/squareup/ui/help/about/LibrariesScreenData;

    iget-boolean v0, v0, Lcom/squareup/ui/help/about/LibrariesScreenData;->canFollowLinks:Z

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/help/about/LibrariesCoordinator$displayLibraries$1;->this$0:Lcom/squareup/ui/help/about/LibrariesCoordinator;

    invoke-static {v0}, Lcom/squareup/ui/help/about/LibrariesCoordinator;->access$getHelpAppletScopeRunner$p(Lcom/squareup/ui/help/about/LibrariesCoordinator;)Lcom/squareup/ui/help/HelpAppletScopeRunner;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/ui/help/about/License;->getUrl()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/help/HelpAppletScopeRunner;->licenseClicked(Ljava/lang/String;)V

    :cond_0
    return-void
.end method
