.class public final Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;
.super Ljava/lang/Object;
.source "HelpTroubleshootingRunner.java"

# interfaces
.implements Lmortar/Scoped;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadType;,
        Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;
    }
.end annotation


# instance fields
.field private final accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final diagnosticCrasher:Lcom/squareup/ui/DiagnosticCrasher;

.field private final diagnosticsReporter:Lcom/squareup/payment/ledger/DiagnosticsReporter;

.field private final mainScheduler:Lio/reactivex/Scheduler;

.field private final onSendDiagnosticsClicked:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final onUploadLedgerClicked:Lcom/jakewharton/rxrelay2/PublishRelay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jakewharton/rxrelay2/PublishRelay<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private final res:Lcom/squareup/util/Res;

.field private final rpcScheduler:Lio/reactivex/Scheduler;

.field private final toastFactory:Lcom/squareup/util/ToastFactory;

.field private final transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;

.field private final troubleshootingVisibility:Lcom/squareup/ui/help/troubleshooting/TroubleshootingVisibility;

.field private final uploadDiagnosticsScreenData:Lio/reactivex/observables/ConnectableObservable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/observables/ConnectableObservable<",
            "Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/squareup/ui/DiagnosticCrasher;Lcom/squareup/payment/ledger/DiagnosticsReporter;Lio/reactivex/Scheduler;Lio/reactivex/Scheduler;Lcom/squareup/util/Res;Lcom/squareup/ui/help/troubleshooting/TroubleshootingVisibility;Lcom/squareup/util/ToastFactory;Lcom/squareup/payment/ledger/TransactionLedgerManager;Lcom/squareup/settings/server/AccountStatusSettings;Lcom/squareup/analytics/Analytics;)V
    .locals 1
    .param p3    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Main;
        .end annotation
    .end param
    .param p4    # Lio/reactivex/Scheduler;
        .annotation runtime Lcom/squareup/thread/Rpc;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;->onSendDiagnosticsClicked:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 42
    invoke-static {}, Lcom/jakewharton/rxrelay2/PublishRelay;->create()Lcom/jakewharton/rxrelay2/PublishRelay;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;->onUploadLedgerClicked:Lcom/jakewharton/rxrelay2/PublishRelay;

    .line 62
    iput-object p1, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;->diagnosticCrasher:Lcom/squareup/ui/DiagnosticCrasher;

    .line 63
    iput-object p2, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;->diagnosticsReporter:Lcom/squareup/payment/ledger/DiagnosticsReporter;

    .line 64
    iput-object p5, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;->res:Lcom/squareup/util/Res;

    .line 65
    iput-object p6, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;->troubleshootingVisibility:Lcom/squareup/ui/help/troubleshooting/TroubleshootingVisibility;

    .line 66
    iput-object p3, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;->mainScheduler:Lio/reactivex/Scheduler;

    .line 67
    iput-object p4, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;->rpcScheduler:Lio/reactivex/Scheduler;

    .line 68
    iput-object p7, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;->toastFactory:Lcom/squareup/util/ToastFactory;

    .line 69
    iput-object p8, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;->transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;

    .line 70
    iput-object p9, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    .line 71
    iput-object p10, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;->analytics:Lcom/squareup/analytics/Analytics;

    .line 73
    iget-object p1, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;->onSendDiagnosticsClicked:Lcom/jakewharton/rxrelay2/PublishRelay;

    new-instance p2, Lcom/squareup/ui/help/troubleshooting/-$$Lambda$HelpTroubleshootingRunner$HB8LblQ64f8NaCd64UBozHUr5Ns;

    invoke-direct {p2, p0}, Lcom/squareup/ui/help/troubleshooting/-$$Lambda$HelpTroubleshootingRunner$HB8LblQ64f8NaCd64UBozHUr5Ns;-><init>(Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;)V

    .line 74
    invoke-virtual {p1, p2}, Lcom/jakewharton/rxrelay2/PublishRelay;->switchMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p1

    .line 76
    iget-object p2, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;->onUploadLedgerClicked:Lcom/jakewharton/rxrelay2/PublishRelay;

    new-instance p3, Lcom/squareup/ui/help/troubleshooting/-$$Lambda$HelpTroubleshootingRunner$zQ9CVvAz6FttHmCJudzsLw2-jsk;

    invoke-direct {p3, p0}, Lcom/squareup/ui/help/troubleshooting/-$$Lambda$HelpTroubleshootingRunner$zQ9CVvAz6FttHmCJudzsLw2-jsk;-><init>(Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;)V

    .line 77
    invoke-virtual {p2, p3}, Lcom/jakewharton/rxrelay2/PublishRelay;->switchMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object p2

    .line 79
    invoke-static {p1, p2}, Lio/reactivex/Observable;->merge(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;)Lio/reactivex/Observable;

    move-result-object p1

    .line 80
    invoke-static {}, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;->access$000()Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;

    move-result-object p2

    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object p1

    const/4 p2, 0x1

    .line 81
    invoke-virtual {p1, p2}, Lio/reactivex/Observable;->replay(I)Lio/reactivex/observables/ConnectableObservable;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;->uploadDiagnosticsScreenData:Lio/reactivex/observables/ConnectableObservable;

    return-void
.end method

.method static synthetic lambda$onEnterScope$3(Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 97
    iget-object p0, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;->sending:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    xor-int/lit8 p0, p0, 0x1

    return p0
.end method

.method static synthetic lambda$onEnterScope$4(Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;)Z
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 98
    iget-object p0, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;->response:Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method static synthetic lambda$sendDiagnostics$7(Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;)Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 137
    invoke-static {p0}, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;->access$500(Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;)Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;

    move-result-object p0

    return-object p0
.end method

.method static synthetic lambda$troubleshootingScreenData$5(Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;)Ljava/lang/Boolean;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 110
    iget-object p0, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;->sending:Ljava/lang/Boolean;

    return-object p0
.end method

.method static synthetic lambda$uploadLedger$9(Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;)Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 145
    invoke-static {p0}, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;->access$300(Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;)Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;

    move-result-object p0

    return-object p0
.end method

.method private sendDiagnostics()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;",
            ">;"
        }
    .end annotation

    .line 135
    iget-object v0, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;->diagnosticsReporter:Lcom/squareup/payment/ledger/DiagnosticsReporter;

    invoke-interface {v0}, Lcom/squareup/payment/ledger/DiagnosticsReporter;->sendDiagnosticsReport()Lio/reactivex/Single;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/Single;->toObservable()Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;->rpcScheduler:Lio/reactivex/Scheduler;

    .line 136
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/help/troubleshooting/-$$Lambda$HelpTroubleshootingRunner$BjgSbn-mPMV2dQw2SUl7G-Vg9QY;->INSTANCE:Lcom/squareup/ui/help/troubleshooting/-$$Lambda$HelpTroubleshootingRunner$BjgSbn-mPMV2dQw2SUl7G-Vg9QY;

    .line 137
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/help/troubleshooting/-$$Lambda$HelpTroubleshootingRunner$A1raPXvriPAY1IxLEeOhmrnyfpY;

    invoke-direct {v1, p0}, Lcom/squareup/ui/help/troubleshooting/-$$Lambda$HelpTroubleshootingRunner$A1raPXvriPAY1IxLEeOhmrnyfpY;-><init>(Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;)V

    .line 138
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->onErrorReturn(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 139
    invoke-static {}, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;->access$100()Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method private toastOnResponse()Lio/reactivex/functions/Consumer;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/functions/Consumer<",
            "Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;",
            ">;"
        }
    .end annotation

    .line 151
    new-instance v0, Lcom/squareup/ui/help/troubleshooting/-$$Lambda$HelpTroubleshootingRunner$OLQF29bosktrGXPT2FOKilA7QLI;

    invoke-direct {v0, p0}, Lcom/squareup/ui/help/troubleshooting/-$$Lambda$HelpTroubleshootingRunner$OLQF29bosktrGXPT2FOKilA7QLI;-><init>(Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;)V

    return-object v0
.end method

.method private uploadLedger()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;",
            ">;"
        }
    .end annotation

    .line 143
    iget-object v0, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;->transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;

    invoke-interface {v0}, Lcom/squareup/payment/ledger/TransactionLedgerManager;->uploadLedger()Lio/reactivex/Single;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/Single;->toObservable()Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;->rpcScheduler:Lio/reactivex/Scheduler;

    .line 144
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/help/troubleshooting/-$$Lambda$HelpTroubleshootingRunner$IJRwVetsTtQvhZCYBVxOWTCLbQ8;->INSTANCE:Lcom/squareup/ui/help/troubleshooting/-$$Lambda$HelpTroubleshootingRunner$IJRwVetsTtQvhZCYBVxOWTCLbQ8;

    .line 145
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/help/troubleshooting/-$$Lambda$HelpTroubleshootingRunner$cloe7jjjGeHZiS4IwB3jCRnngH4;

    invoke-direct {v1, p0}, Lcom/squareup/ui/help/troubleshooting/-$$Lambda$HelpTroubleshootingRunner$cloe7jjjGeHZiS4IwB3jCRnngH4;-><init>(Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;)V

    .line 146
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->onErrorReturn(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 147
    invoke-static {}, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;->access$100()Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->startWith(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public emailSupportLedger()V
    .locals 2

    .line 125
    iget-object v0, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;->transactionLedgerManager:Lcom/squareup/payment/ledger/TransactionLedgerManager;

    iget-object v1, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;->accountStatusSettings:Lcom/squareup/settings/server/AccountStatusSettings;

    invoke-virtual {v1}, Lcom/squareup/settings/server/AccountStatusSettings;->getUserSettings()Lcom/squareup/settings/server/UserSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/settings/server/UserSettings;->getToken()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/payment/ledger/TransactionLedgerManager;->emailLedger(Ljava/lang/String;)V

    .line 126
    iget-object v0, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_EMAIL_TRANSACTION_LEDGER:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    return-void
.end method

.method public synthetic lambda$new$0$HelpTroubleshootingRunner(Lkotlin/Unit;)Lio/reactivex/ObservableSource;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 74
    invoke-direct {p0}, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;->sendDiagnostics()Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$new$1$HelpTroubleshootingRunner(Lkotlin/Unit;)Lio/reactivex/ObservableSource;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 77
    invoke-direct {p0}, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;->uploadLedger()Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$onEnterScope$2$HelpTroubleshootingRunner(Lkotlin/Unit;)Lrx/Completable;
    .locals 0

    .line 91
    iget-object p1, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;->diagnosticCrasher:Lcom/squareup/ui/DiagnosticCrasher;

    invoke-virtual {p1}, Lcom/squareup/ui/DiagnosticCrasher;->logSupportDiagnostics()Lrx/Completable;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$sendDiagnostics$8$HelpTroubleshootingRunner(Ljava/lang/Throwable;)Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 138
    iget-object p1, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;->res:Lcom/squareup/util/Res;

    invoke-static {p1}, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;->access$400(Lcom/squareup/util/Res;)Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$toastOnResponse$11$HelpTroubleshootingRunner(Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;)V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 152
    iget-object v0, p1, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;->response:Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;

    .line 153
    iget-object v1, p1, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;->uploadType:Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadType;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    .line 155
    iget-object v3, v1, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadType;->uploadName:Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    iget-boolean v3, v0, Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;->success:Z

    if-eqz v3, :cond_0

    const-string v3, "Success"

    goto :goto_0

    :cond_0
    const-string v3, "Failure"

    :goto_0
    const/4 v5, 0x1

    aput-object v3, v2, v5

    const/4 v3, 0x2

    iget-object v6, v0, Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;->responseString:Ljava/lang/String;

    aput-object v6, v2, v3

    const-string v3, "%s %s: %s"

    .line 156
    invoke-static {v3, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    .line 158
    invoke-static {v2, v3}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 160
    iget-boolean v3, v0, Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;->success:Z

    if-eqz v3, :cond_1

    .line 161
    iget-object p1, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;->toastFactory:Lcom/squareup/util/ToastFactory;

    iget-object v0, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;->res:Lcom/squareup/util/Res;

    iget v1, v1, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadType;->successToast:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0, v5}, Lcom/squareup/util/ToastFactory;->showText(Ljava/lang/CharSequence;I)V

    goto :goto_1

    .line 163
    :cond_1
    iget-object v3, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;->toastFactory:Lcom/squareup/util/ToastFactory;

    iget-object v4, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;->res:Lcom/squareup/util/Res;

    iget v1, v1, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadType;->failureToast:I

    invoke-interface {v4, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v3, v1, v5}, Lcom/squareup/util/ToastFactory;->showText(Ljava/lang/CharSequence;I)V

    .line 165
    new-instance v1, Lcom/squareup/ui/help/troubleshooting/TroubleshootingUploadFailure;

    iget-object v0, v0, Lcom/squareup/payment/ledger/TransactionLedgerManager$DiagnosticsResponse;->responseString:Ljava/lang/String;

    iget-object p1, p1, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;->throwable:Ljava/lang/Throwable;

    invoke-direct {v1, v0, p1}, Lcom/squareup/ui/help/troubleshooting/TroubleshootingUploadFailure;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-static {v1, v2}, Lcom/squareup/logging/RemoteLog;->w(Ljava/lang/Throwable;Ljava/lang/String;)V

    :goto_1
    return-void
.end method

.method public synthetic lambda$troubleshootingScreenData$6$HelpTroubleshootingRunner(Ljava/lang/Boolean;)Lcom/squareup/ui/help/troubleshooting/TroubleshootingScreenData;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 111
    new-instance v0, Lcom/squareup/ui/help/troubleshooting/TroubleshootingScreenData$Builder;

    invoke-direct {v0}, Lcom/squareup/ui/help/troubleshooting/TroubleshootingScreenData$Builder;-><init>()V

    iget-object v1, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;->troubleshootingVisibility:Lcom/squareup/ui/help/troubleshooting/TroubleshootingVisibility;

    .line 112
    invoke-virtual {v1}, Lcom/squareup/ui/help/troubleshooting/TroubleshootingVisibility;->showUploadLedger()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/help/troubleshooting/TroubleshootingScreenData$Builder;->showUploadSupportLedger(Z)Lcom/squareup/ui/help/troubleshooting/TroubleshootingScreenData$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;->troubleshootingVisibility:Lcom/squareup/ui/help/troubleshooting/TroubleshootingVisibility;

    .line 113
    invoke-virtual {v1}, Lcom/squareup/ui/help/troubleshooting/TroubleshootingVisibility;->showEmailLedger()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/help/troubleshooting/TroubleshootingScreenData$Builder;->showEmailSupportLedger(Z)Lcom/squareup/ui/help/troubleshooting/TroubleshootingScreenData$Builder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;->troubleshootingVisibility:Lcom/squareup/ui/help/troubleshooting/TroubleshootingVisibility;

    .line 114
    invoke-virtual {v1}, Lcom/squareup/ui/help/troubleshooting/TroubleshootingVisibility;->showSendDiagnostics()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/ui/help/troubleshooting/TroubleshootingScreenData$Builder;->showSendDiagnosticReport(Z)Lcom/squareup/ui/help/troubleshooting/TroubleshootingScreenData$Builder;

    move-result-object v0

    .line 115
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-virtual {v0, p1}, Lcom/squareup/ui/help/troubleshooting/TroubleshootingScreenData$Builder;->isSending(Z)Lcom/squareup/ui/help/troubleshooting/TroubleshootingScreenData$Builder;

    move-result-object p1

    .line 116
    invoke-virtual {p1}, Lcom/squareup/ui/help/troubleshooting/TroubleshootingScreenData$Builder;->build()Lcom/squareup/ui/help/troubleshooting/TroubleshootingScreenData;

    move-result-object p1

    return-object p1
.end method

.method public synthetic lambda$uploadLedger$10$HelpTroubleshootingRunner(Ljava/lang/Throwable;)Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .line 146
    iget-object v0, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;->res:Lcom/squareup/util/Res;

    invoke-static {v0, p1}, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;->access$200(Lcom/squareup/util/Res;Ljava/lang/Throwable;)Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner$UploadDiagnosticsScreenData;

    move-result-object p1

    return-object p1
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 2

    .line 85
    iget-object v0, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;->uploadDiagnosticsScreenData:Lio/reactivex/observables/ConnectableObservable;

    invoke-virtual {v0}, Lio/reactivex/observables/ConnectableObservable;->connect()Lio/reactivex/disposables/Disposable;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    .line 88
    iget-object v0, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;->onSendDiagnosticsClicked:Lcom/jakewharton/rxrelay2/PublishRelay;

    iget-object v1, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;->mainScheduler:Lio/reactivex/Scheduler;

    .line 89
    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lio/reactivex/BackpressureStrategy;->LATEST:Lio/reactivex/BackpressureStrategy;

    invoke-static {v0, v1}, Lhu/akarnokd/rxjava/interop/RxJavaInterop;->toV1Observable(Lio/reactivex/ObservableSource;Lio/reactivex/BackpressureStrategy;)Lrx/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/help/troubleshooting/-$$Lambda$HelpTroubleshootingRunner$KMUdJJzKlN137JVNjKUFxqF68fc;

    invoke-direct {v1, p0}, Lcom/squareup/ui/help/troubleshooting/-$$Lambda$HelpTroubleshootingRunner$KMUdJJzKlN137JVNjKUFxqF68fc;-><init>(Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;)V

    .line 91
    invoke-virtual {v0, v1}, Lrx/Observable;->flatMapCompletable(Lrx/functions/Func1;)Lrx/Observable;

    move-result-object v0

    .line 92
    invoke-virtual {v0}, Lrx/Observable;->subscribe()Lrx/Subscription;

    move-result-object v0

    .line 88
    invoke-static {p1, v0}, Lcom/squareup/util/MortarScopesRx1;->unsubscribeOnExit(Lmortar/MortarScope;Lrx/Subscription;)V

    .line 95
    iget-object v0, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;->uploadDiagnosticsScreenData:Lio/reactivex/observables/ConnectableObservable;

    iget-object v1, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;->mainScheduler:Lio/reactivex/Scheduler;

    .line 96
    invoke-virtual {v0, v1}, Lio/reactivex/observables/ConnectableObservable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/help/troubleshooting/-$$Lambda$HelpTroubleshootingRunner$eal8XPi19C1UnIEuzJ0hc_YZ8iQ;->INSTANCE:Lcom/squareup/ui/help/troubleshooting/-$$Lambda$HelpTroubleshootingRunner$eal8XPi19C1UnIEuzJ0hc_YZ8iQ;

    .line 97
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lcom/squareup/ui/help/troubleshooting/-$$Lambda$HelpTroubleshootingRunner$pTm9bGdygGgEHP1LlJJVI9ZnlcU;->INSTANCE:Lcom/squareup/ui/help/troubleshooting/-$$Lambda$HelpTroubleshootingRunner$pTm9bGdygGgEHP1LlJJVI9ZnlcU;

    .line 98
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    .line 99
    invoke-direct {p0}, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;->toastOnResponse()Lio/reactivex/functions/Consumer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 95
    invoke-static {p1, v0}, Lcom/squareup/mortar/MortarScopes;->disposeOnExit(Lmortar/MortarScope;Lio/reactivex/disposables/Disposable;)V

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method public sendDiagnosticReport()V
    .locals 2

    .line 130
    iget-object v0, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;->onSendDiagnosticsClicked:Lcom/jakewharton/rxrelay2/PublishRelay;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    .line 131
    iget-object v0, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_UPLOAD_DIAGNOSTICS_DATA:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    return-void
.end method

.method public troubleshootingScreenData()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/help/troubleshooting/TroubleshootingScreenData;",
            ">;"
        }
    .end annotation

    .line 109
    iget-object v0, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;->uploadDiagnosticsScreenData:Lio/reactivex/observables/ConnectableObservable;

    sget-object v1, Lcom/squareup/ui/help/troubleshooting/-$$Lambda$HelpTroubleshootingRunner$trNtsvMBiRhTwl-pBxWwulDRQmE;->INSTANCE:Lcom/squareup/ui/help/troubleshooting/-$$Lambda$HelpTroubleshootingRunner$trNtsvMBiRhTwl-pBxWwulDRQmE;

    .line 110
    invoke-virtual {v0, v1}, Lio/reactivex/observables/ConnectableObservable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/help/troubleshooting/-$$Lambda$HelpTroubleshootingRunner$CfRWQ88wwXYw0Fahlioer_c5nMc;

    invoke-direct {v1, p0}, Lcom/squareup/ui/help/troubleshooting/-$$Lambda$HelpTroubleshootingRunner$CfRWQ88wwXYw0Fahlioer_c5nMc;-><init>(Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;)V

    .line 111
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method public uploadSupportLedger()V
    .locals 2

    .line 120
    iget-object v0, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;->onUploadLedgerClicked:Lcom/jakewharton/rxrelay2/PublishRelay;

    sget-object v1, Lkotlin/Unit;->INSTANCE:Lkotlin/Unit;

    invoke-virtual {v0, v1}, Lcom/jakewharton/rxrelay2/PublishRelay;->accept(Ljava/lang/Object;)V

    .line 121
    iget-object v0, p0, Lcom/squareup/ui/help/troubleshooting/HelpTroubleshootingRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->SUPPORT_UPLOAD_TRANSACTION_LEDGER:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    return-void
.end method
