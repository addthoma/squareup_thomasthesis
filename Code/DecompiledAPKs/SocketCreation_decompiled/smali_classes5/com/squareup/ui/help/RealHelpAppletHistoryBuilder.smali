.class public Lcom/squareup/ui/help/RealHelpAppletHistoryBuilder;
.super Ljava/lang/Object;
.source "RealHelpAppletHistoryBuilder.java"

# interfaces
.implements Lcom/squareup/ui/help/HelpAppletHistoryBuilder;


# instance fields
.field private final helpApplet:Lcom/squareup/ui/help/HelpApplet;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/help/HelpApplet;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object p1, p0, Lcom/squareup/ui/help/RealHelpAppletHistoryBuilder;->helpApplet:Lcom/squareup/ui/help/HelpApplet;

    return-void
.end method


# virtual methods
.method public varargs recreateHistory([Lcom/squareup/container/ContainerTreeKey;)Lflow/History;
    .locals 4

    .line 17
    invoke-static {}, Lflow/History;->emptyBuilder()Lflow/History$Builder;

    move-result-object v0

    .line 19
    sget-object v1, Lcom/squareup/ui/help/HelpAppletMasterScreen;->INSTANCE:Lcom/squareup/ui/help/HelpAppletMasterScreen;

    invoke-virtual {v0, v1}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    .line 20
    iget-object v1, p0, Lcom/squareup/ui/help/RealHelpAppletHistoryBuilder;->helpApplet:Lcom/squareup/ui/help/HelpApplet;

    invoke-virtual {v1}, Lcom/squareup/ui/help/HelpApplet;->getSections()Lcom/squareup/applet/AppletSectionsList;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/applet/AppletSectionsList;->getLastSelectedSection()Lcom/squareup/applet/AppletSection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/applet/AppletSection;->getInitialScreen()Lcom/squareup/container/ContainerTreeKey;

    move-result-object v1

    invoke-virtual {v0, v1}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    .line 22
    array-length v1, p1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, p1, v2

    .line 23
    invoke-virtual {v0, v3}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 26
    :cond_0
    invoke-virtual {v0}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object p1

    return-object p1
.end method

.method public varargs recreateHistoryOverMaster([Lcom/squareup/container/ContainerTreeKey;)Lflow/History;
    .locals 4

    .line 30
    invoke-static {}, Lflow/History;->emptyBuilder()Lflow/History$Builder;

    move-result-object v0

    .line 32
    sget-object v1, Lcom/squareup/ui/help/HelpAppletMasterScreen;->INSTANCE:Lcom/squareup/ui/help/HelpAppletMasterScreen;

    invoke-virtual {v0, v1}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    .line 34
    array-length v1, p1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, p1, v2

    .line 35
    invoke-virtual {v0, v3}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 38
    :cond_0
    invoke-virtual {v0}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object p1

    return-object p1
.end method
