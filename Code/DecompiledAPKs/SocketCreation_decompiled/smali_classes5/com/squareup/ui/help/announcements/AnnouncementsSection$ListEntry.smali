.class public final Lcom/squareup/ui/help/announcements/AnnouncementsSection$ListEntry;
.super Lcom/squareup/ui/help/HelpAppletSectionsListEntry;
.source "AnnouncementsSection.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/help/announcements/AnnouncementsSection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ListEntry"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0018\u00002\u00020\u0001B\u001f\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\u0008J\u000e\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\n0\rH\u0016J\n\u0010\u000e\u001a\u0004\u0018\u00010\u000fH\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\t\u001a\u0004\u0018\u00010\nX\u0082\u000e\u00a2\u0006\u0004\n\u0002\u0010\u000b\u00a8\u0006\u0010"
    }
    d2 = {
        "Lcom/squareup/ui/help/announcements/AnnouncementsSection$ListEntry;",
        "Lcom/squareup/ui/help/HelpAppletSectionsListEntry;",
        "section",
        "Lcom/squareup/ui/help/announcements/AnnouncementsSection;",
        "res",
        "Lcom/squareup/util/Res;",
        "announcements",
        "Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;",
        "(Lcom/squareup/ui/help/announcements/AnnouncementsSection;Lcom/squareup/util/Res;Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;)V",
        "newCount",
        "",
        "Ljava/lang/Integer;",
        "badgeCount",
        "Lio/reactivex/Observable;",
        "getValueText",
        "",
        "help_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final announcements:Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;

.field private newCount:Ljava/lang/Integer;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/help/announcements/AnnouncementsSection;Lcom/squareup/util/Res;Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "section"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "res"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "announcements"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    check-cast p1, Lcom/squareup/applet/AppletSection;

    sget v0, Lcom/squareup/applet/help/R$string;->announcements:I

    invoke-direct {p0, p1, v0, p2}, Lcom/squareup/ui/help/HelpAppletSectionsListEntry;-><init>(Lcom/squareup/applet/AppletSection;ILcom/squareup/util/Res;)V

    iput-object p3, p0, Lcom/squareup/ui/help/announcements/AnnouncementsSection$ListEntry;->announcements:Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;

    const/4 p1, 0x0

    .line 44
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iput-object p1, p0, Lcom/squareup/ui/help/announcements/AnnouncementsSection$ListEntry;->newCount:Ljava/lang/Integer;

    return-void
.end method

.method public static final synthetic access$getNewCount$p(Lcom/squareup/ui/help/announcements/AnnouncementsSection$ListEntry;)Ljava/lang/Integer;
    .locals 0

    .line 38
    iget-object p0, p0, Lcom/squareup/ui/help/announcements/AnnouncementsSection$ListEntry;->newCount:Ljava/lang/Integer;

    return-object p0
.end method

.method public static final synthetic access$setNewCount$p(Lcom/squareup/ui/help/announcements/AnnouncementsSection$ListEntry;Ljava/lang/Integer;)V
    .locals 0

    .line 38
    iput-object p1, p0, Lcom/squareup/ui/help/announcements/AnnouncementsSection$ListEntry;->newCount:Ljava/lang/Integer;

    return-void
.end method


# virtual methods
.method public badgeCount()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 51
    iget-object v0, p0, Lcom/squareup/ui/help/announcements/AnnouncementsSection$ListEntry;->announcements:Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;

    invoke-virtual {v0}, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;->messages()Lio/reactivex/Observable;

    move-result-object v0

    .line 52
    sget-object v1, Lcom/squareup/ui/help/jumbotron/JumbotronMessageProducer;->toUnreadMessageCount:Lio/reactivex/functions/Function;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 53
    invoke-virtual {v0}, Lio/reactivex/Observable;->distinctUntilChanged()Lio/reactivex/Observable;

    move-result-object v0

    .line 54
    new-instance v1, Lcom/squareup/ui/help/announcements/AnnouncementsSection$ListEntry$badgeCount$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/help/announcements/AnnouncementsSection$ListEntry$badgeCount$1;-><init>(Lcom/squareup/ui/help/announcements/AnnouncementsSection$ListEntry;)V

    check-cast v1, Lio/reactivex/functions/Consumer;

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v0

    const-string v1, "announcements.messages()\u2026unt -> newCount = count }"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public getValueText()Ljava/lang/String;
    .locals 2

    .line 47
    iget-object v0, p0, Lcom/squareup/ui/help/announcements/AnnouncementsSection$ListEntry;->res:Lcom/squareup/util/Res;

    iget-object v1, p0, Lcom/squareup/ui/help/announcements/AnnouncementsSection$ListEntry;->newCount:Ljava/lang/Integer;

    if-nez v1, :cond_0

    invoke-static {}, Lkotlin/jvm/internal/Intrinsics;->throwNpe()V

    :cond_0
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Lcom/squareup/applet/AppletsBadgeCounter;->getBadgeRowValueText(Lcom/squareup/util/Res;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
