.class public Lcom/squareup/ui/help/announcements/AnnouncementDetailsScreen;
.super Lcom/squareup/ui/help/InHelpAppletScope;
.source "AnnouncementDetailsScreen.java"

# interfaces
.implements Lcom/squareup/container/LayoutScreen;
.implements Lcom/squareup/coordinators/CoordinatorProvider;
.implements Lcom/squareup/container/layer/InSection;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/help/announcements/AnnouncementDetailsScreen;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final announcementId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 53
    sget-object v0, Lcom/squareup/ui/help/announcements/-$$Lambda$AnnouncementDetailsScreen$9Rgj4-WpX3n87RhD689kVHlSSMg;->INSTANCE:Lcom/squareup/ui/help/announcements/-$$Lambda$AnnouncementDetailsScreen$9Rgj4-WpX3n87RhD689kVHlSSMg;

    invoke-static {v0}, Lcom/squareup/container/ContainerTreeKey$PathCreator;->fromParcel(Lcom/squareup/container/ContainerTreeKey$PathCreator$ParcelFunc;)Lcom/squareup/container/ContainerTreeKey$PathCreator;

    move-result-object v0

    sput-object v0, Lcom/squareup/ui/help/announcements/AnnouncementDetailsScreen;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 24
    invoke-direct {p0}, Lcom/squareup/ui/help/InHelpAppletScope;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/squareup/ui/help/announcements/AnnouncementDetailsScreen;->announcementId:Ljava/lang/String;

    return-void
.end method

.method static synthetic lambda$static$0(Landroid/os/Parcel;)Lcom/squareup/ui/help/announcements/AnnouncementDetailsScreen;
    .locals 1

    .line 54
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object p0

    .line 55
    new-instance v0, Lcom/squareup/ui/help/announcements/AnnouncementDetailsScreen;

    invoke-direct {v0, p0}, Lcom/squareup/ui/help/announcements/AnnouncementDetailsScreen;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method protected doWriteToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 45
    invoke-super {p0, p1, p2}, Lcom/squareup/ui/help/InHelpAppletScope;->doWriteToParcel(Landroid/os/Parcel;I)V

    .line 46
    iget-object p2, p0, Lcom/squareup/ui/help/announcements/AnnouncementDetailsScreen;->announcementId:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method

.method public getAnalyticsName()Lcom/squareup/analytics/RegisterViewName;
    .locals 1

    .line 41
    sget-object v0, Lcom/squareup/analytics/RegisterViewName;->SUPPORT_ANNOUNCEMENT_DETAILS:Lcom/squareup/analytics/RegisterViewName;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .line 29
    iget-object v0, p0, Lcom/squareup/ui/help/announcements/AnnouncementDetailsScreen;->announcementId:Ljava/lang/String;

    return-object v0
.end method

.method public getSection()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 33
    const-class v0, Lcom/squareup/ui/help/announcements/AnnouncementsSection;

    return-object v0
.end method

.method public provideCoordinator(Landroid/view/View;)Lcom/squareup/coordinators/Coordinator;
    .locals 1

    .line 50
    const-class v0, Lcom/squareup/ui/help/HelpAppletScope$Component;

    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Landroid/view/View;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/help/HelpAppletScope$Component;

    invoke-interface {p1}, Lcom/squareup/ui/help/HelpAppletScope$Component;->announcementDetailCoordinator()Lcom/squareup/ui/help/announcements/AnnouncementDetailsCoordinator;

    move-result-object p1

    return-object p1
.end method

.method public screenLayout()I
    .locals 1

    .line 37
    sget v0, Lcom/squareup/applet/help/R$layout;->announcement_details:I

    return v0
.end method
