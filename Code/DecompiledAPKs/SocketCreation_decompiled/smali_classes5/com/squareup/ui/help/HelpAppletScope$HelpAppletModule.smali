.class public abstract Lcom/squareup/ui/help/HelpAppletScope$HelpAppletModule;
.super Ljava/lang/Object;
.source "HelpAppletScope.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/help/HelpAppletScope;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "HelpAppletModule"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static provideOrderMagstripeSection()Ljava/lang/Class;
    .locals 1
    .annotation runtime Ldagger/Provides;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .line 108
    const-class v0, Lcom/squareup/ui/help/orders/OrdersSection;

    return-object v0
.end method


# virtual methods
.method abstract provideHelpAppletMasterScreenRunner(Lcom/squareup/ui/help/HelpAppletScopeRunner;)Lcom/squareup/ui/help/HelpAppletMasterScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method

.method abstract provideHelpScreenRunner(Lcom/squareup/ui/help/HelpAppletScopeRunner;)Lcom/squareup/ui/help/help/HelpScreen$Runner;
    .annotation runtime Ldagger/Binds;
    .end annotation
.end method
