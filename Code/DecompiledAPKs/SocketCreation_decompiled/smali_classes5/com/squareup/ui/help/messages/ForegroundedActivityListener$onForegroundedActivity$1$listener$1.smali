.class public final Lcom/squareup/ui/help/messages/ForegroundedActivityListener$onForegroundedActivity$1$listener$1;
.super Ljava/lang/Object;
.source "ForegroundedActivityListener.kt"

# interfaces
.implements Lcom/squareup/ActivityListener$ResumedPausedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/help/messages/ForegroundedActivityListener$onForegroundedActivity$1;->subscribe(Lio/reactivex/ObservableEmitter;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0013\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0008\u0002*\u0001\u0000\u0008\n\u0018\u00002\u00020\u0001J\u0008\u0010\u0002\u001a\u00020\u0003H\u0016J\u0008\u0010\u0004\u001a\u00020\u0003H\u0016\u00a8\u0006\u0005"
    }
    d2 = {
        "com/squareup/ui/help/messages/ForegroundedActivityListener$onForegroundedActivity$1$listener$1",
        "Lcom/squareup/ActivityListener$ResumedPausedListener;",
        "onPause",
        "",
        "onResume",
        "help_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $emitter:Lio/reactivex/ObservableEmitter;

.field final synthetic this$0:Lcom/squareup/ui/help/messages/ForegroundedActivityListener$onForegroundedActivity$1;


# direct methods
.method constructor <init>(Lcom/squareup/ui/help/messages/ForegroundedActivityListener$onForegroundedActivity$1;Lio/reactivex/ObservableEmitter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/ObservableEmitter;",
            ")V"
        }
    .end annotation

    .line 30
    iput-object p1, p0, Lcom/squareup/ui/help/messages/ForegroundedActivityListener$onForegroundedActivity$1$listener$1;->this$0:Lcom/squareup/ui/help/messages/ForegroundedActivityListener$onForegroundedActivity$1;

    iput-object p2, p0, Lcom/squareup/ui/help/messages/ForegroundedActivityListener$onForegroundedActivity$1$listener$1;->$emitter:Lio/reactivex/ObservableEmitter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPause()V
    .locals 2

    .line 39
    iget-object v0, p0, Lcom/squareup/ui/help/messages/ForegroundedActivityListener$onForegroundedActivity$1$listener$1;->$emitter:Lio/reactivex/ObservableEmitter;

    sget-object v1, Lcom/squareup/ui/help/messages/ForegroundedActivity$Background;->INSTANCE:Lcom/squareup/ui/help/messages/ForegroundedActivity$Background;

    invoke-interface {v0, v1}, Lio/reactivex/ObservableEmitter;->onNext(Ljava/lang/Object;)V

    return-void
.end method

.method public onResume()V
    .locals 2

    .line 33
    iget-object v0, p0, Lcom/squareup/ui/help/messages/ForegroundedActivityListener$onForegroundedActivity$1$listener$1;->this$0:Lcom/squareup/ui/help/messages/ForegroundedActivityListener$onForegroundedActivity$1;

    iget-object v0, v0, Lcom/squareup/ui/help/messages/ForegroundedActivityListener$onForegroundedActivity$1;->this$0:Lcom/squareup/ui/help/messages/ForegroundedActivityListener;

    invoke-static {v0}, Lcom/squareup/ui/help/messages/ForegroundedActivityListener;->access$resumedActivityIsMain(Lcom/squareup/ui/help/messages/ForegroundedActivityListener;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/help/messages/ForegroundedActivityListener$onForegroundedActivity$1$listener$1;->$emitter:Lio/reactivex/ObservableEmitter;

    sget-object v1, Lcom/squareup/ui/help/messages/ForegroundedActivity$ForegroundIsMainActivity;->INSTANCE:Lcom/squareup/ui/help/messages/ForegroundedActivity$ForegroundIsMainActivity;

    invoke-interface {v0, v1}, Lio/reactivex/ObservableEmitter;->onNext(Ljava/lang/Object;)V

    goto :goto_0

    .line 34
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/help/messages/ForegroundedActivityListener$onForegroundedActivity$1$listener$1;->this$0:Lcom/squareup/ui/help/messages/ForegroundedActivityListener$onForegroundedActivity$1;

    iget-object v0, v0, Lcom/squareup/ui/help/messages/ForegroundedActivityListener$onForegroundedActivity$1;->this$0:Lcom/squareup/ui/help/messages/ForegroundedActivityListener;

    invoke-static {v0}, Lcom/squareup/ui/help/messages/ForegroundedActivityListener;->access$resumedActivityIsHelpshift(Lcom/squareup/ui/help/messages/ForegroundedActivityListener;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/help/messages/ForegroundedActivityListener$onForegroundedActivity$1$listener$1;->$emitter:Lio/reactivex/ObservableEmitter;

    sget-object v1, Lcom/squareup/ui/help/messages/ForegroundedActivity$ForegroundIsHelpshiftActivity;->INSTANCE:Lcom/squareup/ui/help/messages/ForegroundedActivity$ForegroundIsHelpshiftActivity;

    invoke-interface {v0, v1}, Lio/reactivex/ObservableEmitter;->onNext(Ljava/lang/Object;)V

    :cond_1
    :goto_0
    return-void
.end method
