.class public final Lcom/squareup/ui/help/about/NtepCoordinator;
.super Lcom/squareup/coordinators/Coordinator;
.source "NtepCoordinator.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/help/about/NtepCoordinator$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0018\u0000 \u00162\u00020\u0001:\u0001\u0016B\u000f\u0008\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u000c\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0016J\u0010\u0010\u0010\u001a\u00020\r2\u0006\u0010\u0011\u001a\u00020\u000fH\u0002J\u0010\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0015H\u0002R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0008X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0008X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0008X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"
    }
    d2 = {
        "Lcom/squareup/ui/help/about/NtepCoordinator;",
        "Lcom/squareup/coordinators/Coordinator;",
        "helpAppletScopeRunner",
        "Lcom/squareup/ui/help/HelpAppletScopeRunner;",
        "(Lcom/squareup/ui/help/HelpAppletScopeRunner;)V",
        "actionBar",
        "Lcom/squareup/marin/widgets/MarinActionBar;",
        "ntepCertificationNumberRow",
        "Lcom/squareup/widgets/list/NameValueRow;",
        "ntepCompanyRow",
        "ntepModelRow",
        "ntepVersionRow",
        "attach",
        "",
        "rootView",
        "Landroid/view/View;",
        "bindViews",
        "view",
        "getActionBarConfig",
        "Lcom/squareup/marin/widgets/MarinActionBar$Config;",
        "res",
        "Landroid/content/res/Resources;",
        "Companion",
        "help_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final Companion:Lcom/squareup/ui/help/about/NtepCoordinator$Companion;

.field public static final NTEP_CERTIFICATION_NUMBER:Ljava/lang/String; = "19-065"

.field public static final NTEP_CERTIFICATION_NUMBER_FULL:Ljava/lang/String; = "CC 19-065"

.field public static final NTEP_COMPANY:Ljava/lang/String; = "Square, Inc."

.field public static final NTEP_MODEL:Ljava/lang/String; = "Square Point of Sale Application"


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field private final helpAppletScopeRunner:Lcom/squareup/ui/help/HelpAppletScopeRunner;

.field private ntepCertificationNumberRow:Lcom/squareup/widgets/list/NameValueRow;

.field private ntepCompanyRow:Lcom/squareup/widgets/list/NameValueRow;

.field private ntepModelRow:Lcom/squareup/widgets/list/NameValueRow;

.field private ntepVersionRow:Lcom/squareup/widgets/list/NameValueRow;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/ui/help/about/NtepCoordinator$Companion;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/squareup/ui/help/about/NtepCoordinator$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/squareup/ui/help/about/NtepCoordinator;->Companion:Lcom/squareup/ui/help/about/NtepCoordinator$Companion;

    return-void
.end method

.method public constructor <init>(Lcom/squareup/ui/help/HelpAppletScopeRunner;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "helpAppletScopeRunner"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    invoke-direct {p0}, Lcom/squareup/coordinators/Coordinator;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/help/about/NtepCoordinator;->helpAppletScopeRunner:Lcom/squareup/ui/help/HelpAppletScopeRunner;

    return-void
.end method

.method public static final synthetic access$getHelpAppletScopeRunner$p(Lcom/squareup/ui/help/about/NtepCoordinator;)Lcom/squareup/ui/help/HelpAppletScopeRunner;
    .locals 0

    .line 23
    iget-object p0, p0, Lcom/squareup/ui/help/about/NtepCoordinator;->helpAppletScopeRunner:Lcom/squareup/ui/help/HelpAppletScopeRunner;

    return-object p0
.end method

.method public static final synthetic access$getNtepCertificationNumberRow$p(Lcom/squareup/ui/help/about/NtepCoordinator;)Lcom/squareup/widgets/list/NameValueRow;
    .locals 1

    .line 23
    iget-object p0, p0, Lcom/squareup/ui/help/about/NtepCoordinator;->ntepCertificationNumberRow:Lcom/squareup/widgets/list/NameValueRow;

    if-nez p0, :cond_0

    const-string v0, "ntepCertificationNumberRow"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getNtepCompanyRow$p(Lcom/squareup/ui/help/about/NtepCoordinator;)Lcom/squareup/widgets/list/NameValueRow;
    .locals 1

    .line 23
    iget-object p0, p0, Lcom/squareup/ui/help/about/NtepCoordinator;->ntepCompanyRow:Lcom/squareup/widgets/list/NameValueRow;

    if-nez p0, :cond_0

    const-string v0, "ntepCompanyRow"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getNtepModelRow$p(Lcom/squareup/ui/help/about/NtepCoordinator;)Lcom/squareup/widgets/list/NameValueRow;
    .locals 1

    .line 23
    iget-object p0, p0, Lcom/squareup/ui/help/about/NtepCoordinator;->ntepModelRow:Lcom/squareup/widgets/list/NameValueRow;

    if-nez p0, :cond_0

    const-string v0, "ntepModelRow"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$getNtepVersionRow$p(Lcom/squareup/ui/help/about/NtepCoordinator;)Lcom/squareup/widgets/list/NameValueRow;
    .locals 1

    .line 23
    iget-object p0, p0, Lcom/squareup/ui/help/about/NtepCoordinator;->ntepVersionRow:Lcom/squareup/widgets/list/NameValueRow;

    if-nez p0, :cond_0

    const-string v0, "ntepVersionRow"

    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    return-object p0
.end method

.method public static final synthetic access$setNtepCertificationNumberRow$p(Lcom/squareup/ui/help/about/NtepCoordinator;Lcom/squareup/widgets/list/NameValueRow;)V
    .locals 0

    .line 23
    iput-object p1, p0, Lcom/squareup/ui/help/about/NtepCoordinator;->ntepCertificationNumberRow:Lcom/squareup/widgets/list/NameValueRow;

    return-void
.end method

.method public static final synthetic access$setNtepCompanyRow$p(Lcom/squareup/ui/help/about/NtepCoordinator;Lcom/squareup/widgets/list/NameValueRow;)V
    .locals 0

    .line 23
    iput-object p1, p0, Lcom/squareup/ui/help/about/NtepCoordinator;->ntepCompanyRow:Lcom/squareup/widgets/list/NameValueRow;

    return-void
.end method

.method public static final synthetic access$setNtepModelRow$p(Lcom/squareup/ui/help/about/NtepCoordinator;Lcom/squareup/widgets/list/NameValueRow;)V
    .locals 0

    .line 23
    iput-object p1, p0, Lcom/squareup/ui/help/about/NtepCoordinator;->ntepModelRow:Lcom/squareup/widgets/list/NameValueRow;

    return-void
.end method

.method public static final synthetic access$setNtepVersionRow$p(Lcom/squareup/ui/help/about/NtepCoordinator;Lcom/squareup/widgets/list/NameValueRow;)V
    .locals 0

    .line 23
    iput-object p1, p0, Lcom/squareup/ui/help/about/NtepCoordinator;->ntepVersionRow:Lcom/squareup/widgets/list/NameValueRow;

    return-void
.end method

.method private final bindViews(Landroid/view/View;)V
    .locals 2

    .line 56
    invoke-static {p1}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    const-string v1, "ActionBarView.findIn(view)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/ui/help/about/NtepCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 57
    sget v0, Lcom/squareup/applet/help/R$id;->ntep_company:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/NameValueRow;

    iput-object v0, p0, Lcom/squareup/ui/help/about/NtepCoordinator;->ntepCompanyRow:Lcom/squareup/widgets/list/NameValueRow;

    .line 58
    sget v0, Lcom/squareup/applet/help/R$id;->ntep_version:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/NameValueRow;

    iput-object v0, p0, Lcom/squareup/ui/help/about/NtepCoordinator;->ntepVersionRow:Lcom/squareup/widgets/list/NameValueRow;

    .line 59
    sget v0, Lcom/squareup/applet/help/R$id;->ntep_model:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/squareup/widgets/list/NameValueRow;

    iput-object v0, p0, Lcom/squareup/ui/help/about/NtepCoordinator;->ntepModelRow:Lcom/squareup/widgets/list/NameValueRow;

    .line 60
    sget v0, Lcom/squareup/applet/help/R$id;->ntep_certification_number:I

    invoke-static {p1, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/squareup/widgets/list/NameValueRow;

    iput-object p1, p0, Lcom/squareup/ui/help/about/NtepCoordinator;->ntepCertificationNumberRow:Lcom/squareup/widgets/list/NameValueRow;

    return-void
.end method

.method private final getActionBarConfig(Landroid/content/res/Resources;)Lcom/squareup/marin/widgets/MarinActionBar$Config;
    .locals 2

    .line 52
    iget-object v0, p0, Lcom/squareup/ui/help/about/NtepCoordinator;->helpAppletScopeRunner:Lcom/squareup/ui/help/HelpAppletScopeRunner;

    sget v1, Lcom/squareup/applet/help/R$string;->ntep_title:I

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/squareup/ui/help/HelpAppletScopeRunner;->getActionBarConfig(Ljava/lang/String;Z)Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object p1

    const-string v0, "helpAppletScopeRunner.ge\u2026string.ntep_title), true)"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method


# virtual methods
.method public attach(Landroid/view/View;)V
    .locals 3

    const-string v0, "rootView"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    invoke-direct {p0, p1}, Lcom/squareup/ui/help/about/NtepCoordinator;->bindViews(Landroid/view/View;)V

    .line 36
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 38
    iget-object v1, p0, Lcom/squareup/ui/help/about/NtepCoordinator;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    if-nez v1, :cond_0

    const-string v2, "actionBar"

    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->throwUninitializedPropertyAccessException(Ljava/lang/String;)V

    :cond_0
    const-string v2, "res"

    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/squareup/ui/help/about/NtepCoordinator;->getActionBarConfig(Landroid/content/res/Resources;)Lcom/squareup/marin/widgets/MarinActionBar$Config;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/marin/widgets/MarinActionBar;->setConfig(Lcom/squareup/marin/widgets/MarinActionBar$Config;)V

    .line 40
    new-instance v0, Lcom/squareup/ui/help/about/NtepCoordinator$attach$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/help/about/NtepCoordinator$attach$1;-><init>(Lcom/squareup/ui/help/about/NtepCoordinator;)V

    check-cast v0, Lkotlin/jvm/functions/Function0;

    invoke-static {p1, v0}, Lcom/squareup/util/rx2/Rx2Views;->disposeOnDetach(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V

    return-void
.end method
