.class public final Lcom/squareup/ui/help/tutorials/TutorialsCoordinator_Factory;
.super Ljava/lang/Object;
.source "TutorialsCoordinator_Factory.java"

# interfaces
.implements Ldagger/internal/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ldagger/internal/Factory<",
        "Lcom/squareup/ui/help/tutorials/TutorialsCoordinator;",
        ">;"
    }
.end annotation


# instance fields
.field private final helpAppletScopeRunnerProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/HelpAppletScopeRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final tutorialsSectionsProvider:Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/tutorials/TutorialsSections;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/tutorials/TutorialsSections;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/HelpAppletScopeRunner;",
            ">;)V"
        }
    .end annotation

    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/squareup/ui/help/tutorials/TutorialsCoordinator_Factory;->tutorialsSectionsProvider:Ljavax/inject/Provider;

    .line 24
    iput-object p2, p0, Lcom/squareup/ui/help/tutorials/TutorialsCoordinator_Factory;->helpAppletScopeRunnerProvider:Ljavax/inject/Provider;

    return-void
.end method

.method public static create(Ljavax/inject/Provider;Ljavax/inject/Provider;)Lcom/squareup/ui/help/tutorials/TutorialsCoordinator_Factory;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/tutorials/TutorialsSections;",
            ">;",
            "Ljavax/inject/Provider<",
            "Lcom/squareup/ui/help/HelpAppletScopeRunner;",
            ">;)",
            "Lcom/squareup/ui/help/tutorials/TutorialsCoordinator_Factory;"
        }
    .end annotation

    .line 35
    new-instance v0, Lcom/squareup/ui/help/tutorials/TutorialsCoordinator_Factory;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/help/tutorials/TutorialsCoordinator_Factory;-><init>(Ljavax/inject/Provider;Ljavax/inject/Provider;)V

    return-object v0
.end method

.method public static newInstance(Lcom/squareup/ui/help/tutorials/TutorialsSections;Lcom/squareup/ui/help/HelpAppletScopeRunner;)Lcom/squareup/ui/help/tutorials/TutorialsCoordinator;
    .locals 1

    .line 40
    new-instance v0, Lcom/squareup/ui/help/tutorials/TutorialsCoordinator;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/help/tutorials/TutorialsCoordinator;-><init>(Lcom/squareup/ui/help/tutorials/TutorialsSections;Lcom/squareup/ui/help/HelpAppletScopeRunner;)V

    return-object v0
.end method


# virtual methods
.method public get()Lcom/squareup/ui/help/tutorials/TutorialsCoordinator;
    .locals 2

    .line 29
    iget-object v0, p0, Lcom/squareup/ui/help/tutorials/TutorialsCoordinator_Factory;->tutorialsSectionsProvider:Ljavax/inject/Provider;

    invoke-interface {v0}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/help/tutorials/TutorialsSections;

    iget-object v1, p0, Lcom/squareup/ui/help/tutorials/TutorialsCoordinator_Factory;->helpAppletScopeRunnerProvider:Ljavax/inject/Provider;

    invoke-interface {v1}, Ljavax/inject/Provider;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/help/HelpAppletScopeRunner;

    invoke-static {v0, v1}, Lcom/squareup/ui/help/tutorials/TutorialsCoordinator_Factory;->newInstance(Lcom/squareup/ui/help/tutorials/TutorialsSections;Lcom/squareup/ui/help/HelpAppletScopeRunner;)Lcom/squareup/ui/help/tutorials/TutorialsCoordinator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get()Ljava/lang/Object;
    .locals 1

    .line 8
    invoke-virtual {p0}, Lcom/squareup/ui/help/tutorials/TutorialsCoordinator_Factory;->get()Lcom/squareup/ui/help/tutorials/TutorialsCoordinator;

    move-result-object v0

    return-object v0
.end method
