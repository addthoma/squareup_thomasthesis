.class public final Lcom/squareup/ui/help/about/LibrariesListAdapter$Item$LicenseHeader;
.super Lcom/squareup/ui/help/about/LibrariesListAdapter$Item;
.source "LibrariesListAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/help/about/LibrariesListAdapter$Item;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LicenseHeader"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0005\u0010\u0006\u00a8\u0006\u0007"
    }
    d2 = {
        "Lcom/squareup/ui/help/about/LibrariesListAdapter$Item$LicenseHeader;",
        "Lcom/squareup/ui/help/about/LibrariesListAdapter$Item;",
        "license",
        "Lcom/squareup/ui/help/about/License;",
        "(Lcom/squareup/ui/help/about/License;)V",
        "getLicense",
        "()Lcom/squareup/ui/help/about/License;",
        "help_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final license:Lcom/squareup/ui/help/about/License;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/help/about/License;)V
    .locals 2

    const-string v0, "license"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    sget-object v0, Lcom/squareup/ui/help/about/LibrariesListAdapter$ItemType;->LicenseHeader:Lcom/squareup/ui/help/about/LibrariesListAdapter$ItemType;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/squareup/ui/help/about/LibrariesListAdapter$Item;-><init>(Lcom/squareup/ui/help/about/LibrariesListAdapter$ItemType;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    iput-object p1, p0, Lcom/squareup/ui/help/about/LibrariesListAdapter$Item$LicenseHeader;->license:Lcom/squareup/ui/help/about/License;

    return-void
.end method


# virtual methods
.method public final getLicense()Lcom/squareup/ui/help/about/License;
    .locals 1

    .line 54
    iget-object v0, p0, Lcom/squareup/ui/help/about/LibrariesListAdapter$Item$LicenseHeader;->license:Lcom/squareup/ui/help/about/License;

    return-object v0
.end method
