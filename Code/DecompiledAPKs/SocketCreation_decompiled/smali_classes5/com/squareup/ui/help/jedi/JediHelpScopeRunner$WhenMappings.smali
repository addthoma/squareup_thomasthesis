.class public final synthetic Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$WhenMappings;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# static fields
.field public static final synthetic $EnumSwitchMapping$0:[I

.field public static final synthetic $EnumSwitchMapping$1:[I


# direct methods
.method static synthetic constructor <clinit>()V
    .locals 5

    invoke-static {}, Lcom/squareup/protos/jedi/service/ComponentKind;->values()[Lcom/squareup/protos/jedi/service/ComponentKind;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/jedi/service/ComponentKind;->BANNER:Lcom/squareup/protos/jedi/service/ComponentKind;

    invoke-virtual {v1}, Lcom/squareup/protos/jedi/service/ComponentKind;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/jedi/service/ComponentKind;->SEARCH_BAR:Lcom/squareup/protos/jedi/service/ComponentKind;

    invoke-virtual {v1}, Lcom/squareup/protos/jedi/service/ComponentKind;->ordinal()I

    move-result v1

    const/4 v3, 0x2

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$WhenMappings;->$EnumSwitchMapping$0:[I

    sget-object v1, Lcom/squareup/protos/jedi/service/ComponentKind;->HEADLINE:Lcom/squareup/protos/jedi/service/ComponentKind;

    invoke-virtual {v1}, Lcom/squareup/protos/jedi/service/ComponentKind;->ordinal()I

    move-result v1

    const/4 v4, 0x3

    aput v4, v0, v1

    invoke-static {}, Lcom/squareup/protos/jedi/service/ComponentKind;->values()[Lcom/squareup/protos/jedi/service/ComponentKind;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/jedi/service/ComponentKind;->BUTTON_BASE:Lcom/squareup/protos/jedi/service/ComponentKind;

    invoke-virtual {v1}, Lcom/squareup/protos/jedi/service/ComponentKind;->ordinal()I

    move-result v1

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/jedi/service/ComponentKind;->HEADLINE:Lcom/squareup/protos/jedi/service/ComponentKind;

    invoke-virtual {v1}, Lcom/squareup/protos/jedi/service/ComponentKind;->ordinal()I

    move-result v1

    aput v3, v0, v1

    sget-object v0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/jedi/service/ComponentKind;->ICON:Lcom/squareup/protos/jedi/service/ComponentKind;

    invoke-virtual {v1}, Lcom/squareup/protos/jedi/service/ComponentKind;->ordinal()I

    move-result v1

    aput v4, v0, v1

    sget-object v0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/jedi/service/ComponentKind;->INPUT_CONFIRMATION:Lcom/squareup/protos/jedi/service/ComponentKind;

    invoke-virtual {v1}, Lcom/squareup/protos/jedi/service/ComponentKind;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/jedi/service/ComponentKind;->INSTANT_ANSWER:Lcom/squareup/protos/jedi/service/ComponentKind;

    invoke-virtual {v1}, Lcom/squareup/protos/jedi/service/ComponentKind;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/jedi/service/ComponentKind;->PARAGRAPH:Lcom/squareup/protos/jedi/service/ComponentKind;

    invoke-virtual {v1}, Lcom/squareup/protos/jedi/service/ComponentKind;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/jedi/service/ComponentKind;->ROW:Lcom/squareup/protos/jedi/service/ComponentKind;

    invoke-virtual {v1}, Lcom/squareup/protos/jedi/service/ComponentKind;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/jedi/service/ComponentKind;->SECTION_HEADER:Lcom/squareup/protos/jedi/service/ComponentKind;

    invoke-virtual {v1}, Lcom/squareup/protos/jedi/service/ComponentKind;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1

    sget-object v0, Lcom/squareup/ui/help/jedi/JediHelpScopeRunner$WhenMappings;->$EnumSwitchMapping$1:[I

    sget-object v1, Lcom/squareup/protos/jedi/service/ComponentKind;->TEXT_FIELD:Lcom/squareup/protos/jedi/service/ComponentKind;

    invoke-virtual {v1}, Lcom/squareup/protos/jedi/service/ComponentKind;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1

    return-void
.end method
