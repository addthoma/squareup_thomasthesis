.class public final synthetic Lcom/squareup/ui/items/-$$Lambda$StockCountRowAction$TAxJrNme4jxvpYxZynPG4dQ8vrA;
.super Ljava/lang/Object;
.source "lambda"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final synthetic f$0:Lcom/squareup/ui/items/StockCountRowAction;

.field private final synthetic f$1:Z

.field private final synthetic f$2:Ljava/math/BigDecimal;

.field private final synthetic f$3:Lcom/squareup/protos/common/Money;


# direct methods
.method public synthetic constructor <init>(Lcom/squareup/ui/items/StockCountRowAction;ZLjava/math/BigDecimal;Lcom/squareup/protos/common/Money;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/items/-$$Lambda$StockCountRowAction$TAxJrNme4jxvpYxZynPG4dQ8vrA;->f$0:Lcom/squareup/ui/items/StockCountRowAction;

    iput-boolean p2, p0, Lcom/squareup/ui/items/-$$Lambda$StockCountRowAction$TAxJrNme4jxvpYxZynPG4dQ8vrA;->f$1:Z

    iput-object p3, p0, Lcom/squareup/ui/items/-$$Lambda$StockCountRowAction$TAxJrNme4jxvpYxZynPG4dQ8vrA;->f$2:Ljava/math/BigDecimal;

    iput-object p4, p0, Lcom/squareup/ui/items/-$$Lambda$StockCountRowAction$TAxJrNme4jxvpYxZynPG4dQ8vrA;->f$3:Lcom/squareup/protos/common/Money;

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 4

    iget-object v0, p0, Lcom/squareup/ui/items/-$$Lambda$StockCountRowAction$TAxJrNme4jxvpYxZynPG4dQ8vrA;->f$0:Lcom/squareup/ui/items/StockCountRowAction;

    iget-boolean v1, p0, Lcom/squareup/ui/items/-$$Lambda$StockCountRowAction$TAxJrNme4jxvpYxZynPG4dQ8vrA;->f$1:Z

    iget-object v2, p0, Lcom/squareup/ui/items/-$$Lambda$StockCountRowAction$TAxJrNme4jxvpYxZynPG4dQ8vrA;->f$2:Ljava/math/BigDecimal;

    iget-object v3, p0, Lcom/squareup/ui/items/-$$Lambda$StockCountRowAction$TAxJrNme4jxvpYxZynPG4dQ8vrA;->f$3:Lcom/squareup/protos/common/Money;

    invoke-virtual {v0, v1, v2, v3, p1}, Lcom/squareup/ui/items/StockCountRowAction;->lambda$showNumberInStockOrReceiveStock$0$StockCountRowAction(ZLjava/math/BigDecimal;Lcom/squareup/protos/common/Money;Landroid/view/View;)V

    return-void
.end method
