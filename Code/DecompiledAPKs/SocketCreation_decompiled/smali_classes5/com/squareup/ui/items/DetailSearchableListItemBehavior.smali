.class public final Lcom/squareup/ui/items/DetailSearchableListItemBehavior;
.super Ljava/lang/Object;
.source "DetailSearchableListItemBehavior.kt"

# interfaces
.implements Lcom/squareup/ui/items/DetailSearchableListBehavior;


# annotations
.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDetailSearchableListItemBehavior.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DetailSearchableListItemBehavior.kt\ncom/squareup/ui/items/DetailSearchableListItemBehavior\n*L\n1#1,55:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000j\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0018\u00002\u00020\u0001BE\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u000c\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\t\u0012\u0006\u0010\u000b\u001a\u00020\u000c\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u00a2\u0006\u0002\u0010\u0011J$\u0010\u001b\u001a\u000e\u0012\u0004\u0012\u00020\u001d\u0012\u0004\u0012\u00020\u001e0\u001c2\u0006\u0010\u001f\u001a\u00020 2\u0006\u0010!\u001a\u00020\"H\u0016J$\u0010#\u001a\u000e\u0012\u0004\u0012\u00020\u001d\u0012\u0004\u0012\u00020\u001e0\u001c2\u0006\u0010\u001f\u001a\u00020 2\u0006\u0010!\u001a\u00020$H\u0016J\u0016\u0010%\u001a\u0008\u0012\u0004\u0012\u00020\'0&2\u0006\u0010(\u001a\u00020)H\u0016R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u000cX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0008\u001a\u0008\u0012\u0004\u0012\u00020\n0\tX\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0012\u0010\u0013R\u0018\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0096\u0004\u00a2\u0006\n\n\u0002\u0010\u0016\u001a\u0004\u0008\u0014\u0010\u0015R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\u0018R\u0014\u0010\u0006\u001a\u00020\u0007X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0019\u0010\u001a\u00a8\u0006*"
    }
    d2 = {
        "Lcom/squareup/ui/items/DetailSearchableListItemBehavior;",
        "Lcom/squareup/ui/items/DetailSearchableListBehavior;",
        "shouldShowCreateFromSearchButton",
        "",
        "objectNumberLimit",
        "",
        "textBag",
        "Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;",
        "itemTypes",
        "",
        "Lcom/squareup/api/items/Item$Type;",
        "cogs",
        "Lcom/squareup/cogs/Cogs;",
        "badBus",
        "Lcom/squareup/badbus/BadBus;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "(ZLjava/lang/Integer;Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;Ljava/util/List;Lcom/squareup/cogs/Cogs;Lcom/squareup/badbus/BadBus;Lcom/squareup/analytics/Analytics;)V",
        "getItemTypes",
        "()Ljava/util/List;",
        "getObjectNumberLimit",
        "()Ljava/lang/Integer;",
        "Ljava/lang/Integer;",
        "getShouldShowCreateFromSearchButton",
        "()Z",
        "getTextBag",
        "()Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;",
        "handlePressingCreateButton",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/ui/items/DetailSearchableListState;",
        "Lcom/squareup/ui/items/DetailSearchableListResult;",
        "currentState",
        "Lcom/squareup/ui/items/DetailSearchableListState$ShowList;",
        "event",
        "Lcom/squareup/ui/items/DetailSearchableListScreen$Event$CreateButtonPressed;",
        "handleSelectingObjectFromList",
        "Lcom/squareup/ui/items/DetailSearchableListScreen$Event$ObjectSelected;",
        "list",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/ui/items/DetailSearchableListData;",
        "searchTerm",
        "",
        "items-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final badBus:Lcom/squareup/badbus/BadBus;

.field private final cogs:Lcom/squareup/cogs/Cogs;

.field private final itemTypes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Item$Type;",
            ">;"
        }
    .end annotation
.end field

.field private final objectNumberLimit:Ljava/lang/Integer;

.field private final shouldShowCreateFromSearchButton:Z

.field private final textBag:Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;


# direct methods
.method public constructor <init>(ZLjava/lang/Integer;Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;Ljava/util/List;Lcom/squareup/cogs/Cogs;Lcom/squareup/badbus/BadBus;Lcom/squareup/analytics/Analytics;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/lang/Integer;",
            "Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;",
            "Ljava/util/List<",
            "+",
            "Lcom/squareup/api/items/Item$Type;",
            ">;",
            "Lcom/squareup/cogs/Cogs;",
            "Lcom/squareup/badbus/BadBus;",
            "Lcom/squareup/analytics/Analytics;",
            ")V"
        }
    .end annotation

    const-string/jumbo v0, "textBag"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "itemTypes"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cogs"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "badBus"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/squareup/ui/items/DetailSearchableListItemBehavior;->shouldShowCreateFromSearchButton:Z

    iput-object p2, p0, Lcom/squareup/ui/items/DetailSearchableListItemBehavior;->objectNumberLimit:Ljava/lang/Integer;

    iput-object p3, p0, Lcom/squareup/ui/items/DetailSearchableListItemBehavior;->textBag:Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;

    iput-object p4, p0, Lcom/squareup/ui/items/DetailSearchableListItemBehavior;->itemTypes:Ljava/util/List;

    iput-object p5, p0, Lcom/squareup/ui/items/DetailSearchableListItemBehavior;->cogs:Lcom/squareup/cogs/Cogs;

    iput-object p6, p0, Lcom/squareup/ui/items/DetailSearchableListItemBehavior;->badBus:Lcom/squareup/badbus/BadBus;

    iput-object p7, p0, Lcom/squareup/ui/items/DetailSearchableListItemBehavior;->analytics:Lcom/squareup/analytics/Analytics;

    .line 30
    invoke-virtual {p0}, Lcom/squareup/ui/items/DetailSearchableListItemBehavior;->getItemTypes()Ljava/util/List;

    move-result-object p1

    sget-object p2, Lcom/squareup/api/items/Item$Type;->APPOINTMENTS_SERVICE:Lcom/squareup/api/items/Item$Type;

    invoke-interface {p1, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    if-eqz p1, :cond_0

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Check failed."

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method


# virtual methods
.method public getItemTypes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Item$Type;",
            ">;"
        }
    .end annotation

    .line 24
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListItemBehavior;->itemTypes:Ljava/util/List;

    return-object v0
.end method

.method public getObjectNumberLimit()Ljava/lang/Integer;
    .locals 1

    .line 22
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListItemBehavior;->objectNumberLimit:Ljava/lang/Integer;

    return-object v0
.end method

.method public getShouldShowCreateFromSearchButton()Z
    .locals 1

    .line 21
    iget-boolean v0, p0, Lcom/squareup/ui/items/DetailSearchableListItemBehavior;->shouldShowCreateFromSearchButton:Z

    return v0
.end method

.method public getTextBag()Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListItemBehavior;->textBag:Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;

    return-object v0
.end method

.method public handlePressingCreateButton(Lcom/squareup/ui/items/DetailSearchableListState$ShowList;Lcom/squareup/ui/items/DetailSearchableListScreen$Event$CreateButtonPressed;)Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/items/DetailSearchableListState$ShowList;",
            "Lcom/squareup/ui/items/DetailSearchableListScreen$Event$CreateButtonPressed;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/items/DetailSearchableListState;",
            "Lcom/squareup/ui/items/DetailSearchableListResult;",
            ">;"
        }
    .end annotation

    const-string v0, "currentState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "event"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 36
    sget-object p2, Lcom/squareup/ui/items/DetailSearchableListResult$GoToCreateItem;->INSTANCE:Lcom/squareup/ui/items/DetailSearchableListResult$GoToCreateItem;

    .line 35
    invoke-virtual {p1, p2}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method public handleSelectingObjectFromList(Lcom/squareup/ui/items/DetailSearchableListState$ShowList;Lcom/squareup/ui/items/DetailSearchableListScreen$Event$ObjectSelected;)Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/items/DetailSearchableListState$ShowList;",
            "Lcom/squareup/ui/items/DetailSearchableListScreen$Event$ObjectSelected;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/items/DetailSearchableListState;",
            "Lcom/squareup/ui/items/DetailSearchableListResult;",
            ">;"
        }
    .end annotation

    const-string v0, "currentState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "event"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    new-instance v0, Lcom/squareup/ui/items/DetailSearchableListResult$GoToEditItem;

    invoke-virtual {p2}, Lcom/squareup/ui/items/DetailSearchableListScreen$Event$ObjectSelected;->getSelectedObject()Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/squareup/ui/items/DetailSearchableListScreen$Event$ObjectSelected;->getSelectedObject()Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;

    move-result-object p2

    if-eqz p2, :cond_0

    check-cast p2, Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Item;

    invoke-virtual {p2}, Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Item;->getItem()Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getImageUrl()Ljava/lang/String;

    move-result-object p2

    invoke-direct {v0, v1, p2}, Lcom/squareup/ui/items/DetailSearchableListResult$GoToEditItem;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1

    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type com.squareup.ui.items.DetailSearchableListState.DetailSearchableObject.Item"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public list(Ljava/lang/String;)Lio/reactivex/Observable;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/items/DetailSearchableListData;",
            ">;"
        }
    .end annotation

    const-string v0, "searchTerm"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    sget-object v1, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper;->Companion:Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion;

    .line 47
    iget-object v2, p0, Lcom/squareup/ui/items/DetailSearchableListItemBehavior;->cogs:Lcom/squareup/cogs/Cogs;

    .line 48
    iget-object v3, p0, Lcom/squareup/ui/items/DetailSearchableListItemBehavior;->badBus:Lcom/squareup/badbus/BadBus;

    .line 49
    sget-object v4, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 51
    iget-object v6, p0, Lcom/squareup/ui/items/DetailSearchableListItemBehavior;->analytics:Lcom/squareup/analytics/Analytics;

    .line 52
    invoke-virtual {p0}, Lcom/squareup/ui/items/DetailSearchableListItemBehavior;->getItemTypes()Ljava/util/List;

    move-result-object v7

    const/4 v8, 0x0

    const/16 v9, 0x40

    const/4 v10, 0x0

    move-object v5, p1

    .line 46
    invoke-static/range {v1 .. v10}, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion;->listCogsObjects$default(Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion;Lcom/squareup/cogs/Cogs;Lcom/squareup/badbus/BadBus;Lcom/squareup/shared/catalog/models/CatalogObjectType;Ljava/lang/String;Lcom/squareup/analytics/Analytics;Ljava/util/List;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method
