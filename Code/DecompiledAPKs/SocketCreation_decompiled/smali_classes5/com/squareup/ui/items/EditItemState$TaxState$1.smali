.class final Lcom/squareup/ui/items/EditItemState$TaxState$1;
.super Ljava/lang/Object;
.source "EditItemState.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/EditItemState$TaxState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator<",
        "Lcom/squareup/ui/items/EditItemState$TaxState;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .line 1020
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/squareup/ui/items/EditItemState$TaxState;
    .locals 11

    .line 1022
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 1023
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 1024
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 1025
    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v4

    .line 1026
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    const/4 v5, 0x1

    const/4 v6, 0x0

    if-eqz v0, :cond_0

    const/4 v7, 0x1

    goto :goto_0

    :cond_0
    const/4 v7, 0x0

    .line 1027
    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_1

    const/4 v8, 0x1

    goto :goto_1

    :cond_1
    const/4 v8, 0x0

    .line 1028
    :goto_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result p1

    if-eqz p1, :cond_2

    const/4 p1, 0x1

    goto :goto_2

    :cond_2
    const/4 p1, 0x0

    .line 1029
    :goto_2
    new-instance v9, Lcom/squareup/ui/items/EditItemState$TaxState;

    const/4 v10, 0x0

    move-object v0, v9

    move v5, v7

    move v6, v8

    move v7, p1

    move-object v8, v10

    invoke-direct/range {v0 .. v8}, Lcom/squareup/ui/items/EditItemState$TaxState;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[BZZZLcom/squareup/ui/items/EditItemState$1;)V

    return-object v9
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 0

    .line 1020
    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/EditItemState$TaxState$1;->createFromParcel(Landroid/os/Parcel;)Lcom/squareup/ui/items/EditItemState$TaxState;

    move-result-object p1

    return-object p1
.end method

.method public newArray(I)[Lcom/squareup/ui/items/EditItemState$TaxState;
    .locals 0

    .line 1033
    new-array p1, p1, [Lcom/squareup/ui/items/EditItemState$TaxState;

    return-object p1
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 0

    .line 1020
    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/EditItemState$TaxState$1;->newArray(I)[Lcom/squareup/ui/items/EditItemState$TaxState;

    move-result-object p1

    return-object p1
.end method
