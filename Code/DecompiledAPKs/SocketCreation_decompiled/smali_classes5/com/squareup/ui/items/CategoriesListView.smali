.class public Lcom/squareup/ui/items/CategoriesListView;
.super Lcom/squareup/ui/items/DetailSearchableListView;
.source "CategoriesListView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/CategoriesListView$CategoriesListAdapter;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/ui/items/DetailSearchableListView<",
        "Lcom/squareup/ui/items/CategoriesListView;",
        ">;"
    }
.end annotation


# instance fields
.field presenter:Lcom/squareup/ui/items/CategoriesListScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 23
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/items/DetailSearchableListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method protected buildAdapter(Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;)Lcom/squareup/ui/items/DetailSearchableListView$DetailSearchableListAdapter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;",
            ")",
            "Lcom/squareup/ui/items/DetailSearchableListView<",
            "Lcom/squareup/ui/items/CategoriesListView;",
            ">.DetailSearchable",
            "ListAdapter;"
        }
    .end annotation

    .line 49
    new-instance v0, Lcom/squareup/ui/items/CategoriesListView$CategoriesListAdapter;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/items/CategoriesListView$CategoriesListAdapter;-><init>(Lcom/squareup/ui/items/CategoriesListView;Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;)V

    return-object v0
.end method

.method getPresenter()Lcom/squareup/ui/items/CategoriesListScreen$Presenter;
    .locals 1

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/items/CategoriesListView;->presenter:Lcom/squareup/ui/items/CategoriesListScreen$Presenter;

    return-object v0
.end method

.method bridge synthetic getPresenter()Lcom/squareup/ui/items/DetailSearchableListPresenter;
    .locals 1

    .line 18
    invoke-virtual {p0}, Lcom/squareup/ui/items/CategoriesListView;->getPresenter()Lcom/squareup/ui/items/CategoriesListScreen$Presenter;

    move-result-object v0

    return-object v0
.end method

.method protected inject()V
    .locals 2

    .line 27
    invoke-virtual {p0}, Lcom/squareup/ui/items/CategoriesListView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lcom/squareup/ui/items/CategoriesListScreen$Component;

    invoke-static {v0, v1}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/CategoriesListScreen$Component;

    invoke-interface {v0, p0}, Lcom/squareup/ui/items/CategoriesListScreen$Component;->inject(Lcom/squareup/ui/items/CategoriesListView;)V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 4

    .line 31
    invoke-super {p0}, Lcom/squareup/ui/items/DetailSearchableListView;->onFinishInflate()V

    .line 32
    invoke-virtual {p0}, Lcom/squareup/ui/items/CategoriesListView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 33
    iget-object v1, p0, Lcom/squareup/ui/items/CategoriesListView;->searchBar:Lcom/squareup/ui/XableEditText;

    sget v2, Lcom/squareup/itemsapplet/R$string;->items_applet_search_categories:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/ui/XableEditText;->setHint(Ljava/lang/String;)V

    .line 34
    new-instance v1, Lcom/squareup/ui/LinkSpan$Builder;

    invoke-virtual {p0}, Lcom/squareup/ui/items/CategoriesListView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/squareup/itemsapplet/R$string;->items_applet_categories_null_message:I

    const-string v3, "learn_more"

    .line 35
    invoke-virtual {v1, v2, v3}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    sget v2, Lcom/squareup/registerlib/R$string;->categories_hint_url:I

    .line 36
    invoke-virtual {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->url(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    sget v2, Lcom/squareup/common/strings/R$string;->learn_more_lowercase_more:I

    .line 37
    invoke-virtual {v1, v2}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object v1

    .line 38
    invoke-virtual {v1}, Lcom/squareup/ui/LinkSpan$Builder;->asCharSequence()Ljava/lang/CharSequence;

    move-result-object v1

    .line 40
    sget v2, Lcom/squareup/itemsapplet/R$string;->items_applet_categories_null_title:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, v1}, Lcom/squareup/ui/items/CategoriesListView;->setNullState(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 41
    sget v0, Lcom/squareup/noho/R$drawable;->noho_divider_gutter_half:I

    invoke-virtual {p0, v0}, Lcom/squareup/ui/items/CategoriesListView;->setDetailListDivider(I)V

    return-void
.end method
