.class final Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion$getLatestUnitsListData$3;
.super Ljava/lang/Object;
.source "DetailSearchableListUnitBehavior.kt"

# interfaces
.implements Lio/reactivex/functions/Function;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion;->getLatestUnitsListData(Lcom/squareup/cogs/Cogs;Lcom/squareup/badbus/BadBus;Ljava/util/Locale;Lcom/squareup/util/Res;Ljava/lang/String;)Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Function<",
        "TT;",
        "Lio/reactivex/SingleSource<",
        "+TR;>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u0010\u0012\u000c\u0012\n \u0003*\u0004\u0018\u00010\u00020\u00020\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0004\u0008\u0006\u0010\u0007"
    }
    d2 = {
        "<anonymous>",
        "Lio/reactivex/Single;",
        "Lcom/squareup/ui/items/DetailSearchableListData;",
        "kotlin.jvm.PlatformType",
        "it",
        "",
        "apply",
        "(Lkotlin/Unit;)Lio/reactivex/Single;"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $cogs:Lcom/squareup/cogs/Cogs;

.field final synthetic $locale:Ljava/util/Locale;

.field final synthetic $res:Lcom/squareup/util/Res;

.field final synthetic $searchTerm:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/squareup/cogs/Cogs;Lcom/squareup/util/Res;Ljava/util/Locale;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion$getLatestUnitsListData$3;->$cogs:Lcom/squareup/cogs/Cogs;

    iput-object p2, p0, Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion$getLatestUnitsListData$3;->$res:Lcom/squareup/util/Res;

    iput-object p3, p0, Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion$getLatestUnitsListData$3;->$locale:Ljava/util/Locale;

    iput-object p4, p0, Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion$getLatestUnitsListData$3;->$searchTerm:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Lkotlin/Unit;)Lio/reactivex/Single;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/Unit;",
            ")",
            "Lio/reactivex/Single<",
            "Lcom/squareup/ui/items/DetailSearchableListData;",
            ">;"
        }
    .end annotation

    const-string v0, "it"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    sget-object p1, Lcom/squareup/ui/items/DetailSearchableListUnitBehavior;->Companion:Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion;

    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion$getLatestUnitsListData$3;->$cogs:Lcom/squareup/cogs/Cogs;

    invoke-static {p1, v0}, Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion;->access$readUnitsList(Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion;Lcom/squareup/cogs/Cogs;)Lio/reactivex/Single;

    move-result-object p1

    new-instance v0, Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion$getLatestUnitsListData$3$1;

    invoke-direct {v0, p0}, Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion$getLatestUnitsListData$3$1;-><init>(Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion$getLatestUnitsListData$3;)V

    check-cast v0, Lio/reactivex/functions/Function;

    invoke-virtual {p1, v0}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 51
    check-cast p1, Lkotlin/Unit;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/DetailSearchableListUnitBehavior$Companion$getLatestUnitsListData$3;->apply(Lkotlin/Unit;)Lio/reactivex/Single;

    move-result-object p1

    return-object p1
.end method
