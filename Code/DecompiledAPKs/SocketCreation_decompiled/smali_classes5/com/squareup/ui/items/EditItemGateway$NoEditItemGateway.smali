.class public final Lcom/squareup/ui/items/EditItemGateway$NoEditItemGateway;
.super Ljava/lang/Object;
.source "EditItemGateway.kt"

# interfaces
.implements Lcom/squareup/ui/items/EditItemGateway;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/EditItemGateway;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "NoEditItemGateway"
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nEditItemGateway.kt\nKotlin\n*S Kotlin\n*F\n+ 1 EditItemGateway.kt\ncom/squareup/ui/items/EditItemGateway$NoEditItemGateway\n+ 2 Objects.kt\ncom/squareup/util/Objects\n*L\n1#1,49:1\n152#2:50\n*E\n*S KotlinDebug\n*F\n+ 1 EditItemGateway.kt\ncom/squareup/ui/items/EditItemGateway$NoEditItemGateway\n*L\n47#1:50\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0003\n\u0002\u0018\u0002\n\u0002\u0008\t\n\u0002\u0010\t\n\u0002\u0008\u0004\u0018\u00002\u00020\u0001B\u0007\u0008\u0007\u00a2\u0006\u0002\u0010\u0002J\u0011\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0096\u0001J#\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0008\u001a\u00020\u00062\u0006\u0010\t\u001a\u00020\n2\u0008\u0010\u000b\u001a\u0004\u0018\u00010\u0006H\u0096\u0001J\u0011\u0010\u000c\u001a\u00020\u00042\u0006\u0010\t\u001a\u00020\nH\u0096\u0001J%\u0010\r\u001a\u00020\u00042\u0006\u0010\t\u001a\u00020\n2\u0008\u0010\u000e\u001a\u0004\u0018\u00010\u00062\u0008\u0010\u000f\u001a\u0004\u0018\u00010\u0006H\u0096\u0001J!\u0010\u0010\u001a\u00020\u00042\u0006\u0010\u0011\u001a\u00020\u00062\u0006\u0010\u0012\u001a\u00020\u00062\u0006\u0010\u0013\u001a\u00020\u0014H\u0096\u0001J\u0011\u0010\u0015\u001a\u00020\u00042\u0006\u0010\u0016\u001a\u00020\u0006H\u0096\u0001J\t\u0010\u0017\u001a\u00020\u0004H\u0096\u0001\u00a8\u0006\u0018"
    }
    d2 = {
        "Lcom/squareup/ui/items/EditItemGateway$NoEditItemGateway;",
        "Lcom/squareup/ui/items/EditItemGateway;",
        "()V",
        "startEditDiscount",
        "",
        "id",
        "",
        "startEditItemFlow",
        "itemId",
        "type",
        "Lcom/squareup/api/items/Item$Type;",
        "imageUrl",
        "startEditNewItemFlow",
        "startEditNewItemFlowInCategory",
        "categoryId",
        "categoryName",
        "startEditNewItemFlowWithNameAndAmount",
        "itemAbbreviation",
        "itemName",
        "itemAmount",
        "",
        "startEditNewItemFlowWithSku",
        "sku",
        "startNewDiscount",
        "edit-item"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final synthetic $$delegate_0:Lcom/squareup/ui/items/EditItemGateway;


# direct methods
.method public constructor <init>()V
    .locals 5
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    const-class v0, Lcom/squareup/ui/items/EditItemGateway;

    const-string v1, "Has no access to the Edit Item Flow"

    const/4 v2, 0x0

    const/4 v3, 0x4

    const/4 v4, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Lcom/squareup/util/Objects;->allMethodsThrowUnsupportedOperation$default(Ljava/lang/Class;Ljava/lang/String;ZILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/EditItemGateway;

    iput-object v0, p0, Lcom/squareup/ui/items/EditItemGateway$NoEditItemGateway;->$$delegate_0:Lcom/squareup/ui/items/EditItemGateway;

    return-void
.end method


# virtual methods
.method public startEditDiscount(Ljava/lang/String;)V
    .locals 1

    const-string v0, "id"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/ui/items/EditItemGateway$NoEditItemGateway;->$$delegate_0:Lcom/squareup/ui/items/EditItemGateway;

    invoke-interface {v0, p1}, Lcom/squareup/ui/items/EditItemGateway;->startEditDiscount(Ljava/lang/String;)V

    return-void
.end method

.method public startEditItemFlow(Ljava/lang/String;Lcom/squareup/api/items/Item$Type;Ljava/lang/String;)V
    .locals 1

    const-string v0, "itemId"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "type"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/ui/items/EditItemGateway$NoEditItemGateway;->$$delegate_0:Lcom/squareup/ui/items/EditItemGateway;

    invoke-interface {v0, p1, p2, p3}, Lcom/squareup/ui/items/EditItemGateway;->startEditItemFlow(Ljava/lang/String;Lcom/squareup/api/items/Item$Type;Ljava/lang/String;)V

    return-void
.end method

.method public startEditNewItemFlow(Lcom/squareup/api/items/Item$Type;)V
    .locals 1

    const-string/jumbo v0, "type"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/ui/items/EditItemGateway$NoEditItemGateway;->$$delegate_0:Lcom/squareup/ui/items/EditItemGateway;

    invoke-interface {v0, p1}, Lcom/squareup/ui/items/EditItemGateway;->startEditNewItemFlow(Lcom/squareup/api/items/Item$Type;)V

    return-void
.end method

.method public startEditNewItemFlowInCategory(Lcom/squareup/api/items/Item$Type;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const-string/jumbo v0, "type"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/ui/items/EditItemGateway$NoEditItemGateway;->$$delegate_0:Lcom/squareup/ui/items/EditItemGateway;

    invoke-interface {v0, p1, p2, p3}, Lcom/squareup/ui/items/EditItemGateway;->startEditNewItemFlowInCategory(Lcom/squareup/api/items/Item$Type;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public startEditNewItemFlowWithNameAndAmount(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 1

    const-string v0, "itemAbbreviation"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "itemName"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/ui/items/EditItemGateway$NoEditItemGateway;->$$delegate_0:Lcom/squareup/ui/items/EditItemGateway;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/squareup/ui/items/EditItemGateway;->startEditNewItemFlowWithNameAndAmount(Ljava/lang/String;Ljava/lang/String;J)V

    return-void
.end method

.method public startEditNewItemFlowWithSku(Ljava/lang/String;)V
    .locals 1

    const-string v0, "sku"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/squareup/ui/items/EditItemGateway$NoEditItemGateway;->$$delegate_0:Lcom/squareup/ui/items/EditItemGateway;

    invoke-interface {v0, p1}, Lcom/squareup/ui/items/EditItemGateway;->startEditNewItemFlowWithSku(Ljava/lang/String;)V

    return-void
.end method

.method public startNewDiscount()V
    .locals 1

    iget-object v0, p0, Lcom/squareup/ui/items/EditItemGateway$NoEditItemGateway;->$$delegate_0:Lcom/squareup/ui/items/EditItemGateway;

    invoke-interface {v0}, Lcom/squareup/ui/items/EditItemGateway;->startNewDiscount()V

    return-void
.end method
