.class public Lcom/squareup/ui/items/AllServicesDetailScreen$AllServicesDetailPresenter;
.super Lcom/squareup/ui/items/AllItemsOrServicesDetailPresenter;
.source "AllServicesDetailScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/AllServicesDetailScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AllServicesDetailPresenter"
.end annotation


# instance fields
.field private final catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

.field private final convertItemsUrlHelper:Lcom/squareup/ui/items/ConvertItemsUrlHelper;

.field private final launcher:Lcom/squareup/util/BrowserLauncher;


# direct methods
.method constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/badbus/BadBus;Lcom/squareup/cogs/Cogs;Lcom/squareup/util/Device;Lflow/Flow;Lcom/squareup/analytics/Analytics;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/settings/server/Features;Lcom/squareup/barcodescanners/BarcodeScannerTracker;Lcom/squareup/ui/main/TopScreenChecker;Lcom/squareup/ui/items/ItemsAppletScopeRunner;Lcom/squareup/ui/items/EditItemGateway;Lcom/squareup/orderentry/OrderEntryAppletGateway;Lcom/squareup/util/BrowserLauncher;Lcom/squareup/ui/items/ConvertItemsUrlHelper;Lcom/squareup/catalogapi/CatalogIntegrationController;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    move-object v0, p0

    .line 65
    invoke-direct/range {p0 .. p13}, Lcom/squareup/ui/items/AllItemsOrServicesDetailPresenter;-><init>(Lcom/squareup/util/Res;Lcom/squareup/badbus/BadBus;Lcom/squareup/cogs/Cogs;Lcom/squareup/util/Device;Lflow/Flow;Lcom/squareup/analytics/Analytics;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/settings/server/Features;Lcom/squareup/barcodescanners/BarcodeScannerTracker;Lcom/squareup/ui/main/TopScreenChecker;Lcom/squareup/ui/items/ItemsAppletScopeRunner;Lcom/squareup/ui/items/EditItemGateway;Lcom/squareup/orderentry/OrderEntryAppletGateway;)V

    move-object/from16 v1, p14

    .line 68
    iput-object v1, v0, Lcom/squareup/ui/items/AllServicesDetailScreen$AllServicesDetailPresenter;->launcher:Lcom/squareup/util/BrowserLauncher;

    move-object/from16 v1, p15

    .line 69
    iput-object v1, v0, Lcom/squareup/ui/items/AllServicesDetailScreen$AllServicesDetailPresenter;->convertItemsUrlHelper:Lcom/squareup/ui/items/ConvertItemsUrlHelper;

    move-object/from16 v1, p16

    .line 70
    iput-object v1, v0, Lcom/squareup/ui/items/AllServicesDetailScreen$AllServicesDetailPresenter;->catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

    return-void
.end method


# virtual methods
.method protected getActionBarTitle()Ljava/lang/CharSequence;
    .locals 2

    .line 98
    iget-object v0, p0, Lcom/squareup/ui/items/AllServicesDetailScreen$AllServicesDetailPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/itemsapplet/R$string;->items_applet_all_services_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getAnalyticsTapName()Lcom/squareup/analytics/RegisterTapName;
    .locals 1

    .line 94
    sget-object v0, Lcom/squareup/analytics/RegisterTapName;->ITEMS_APPLET_TAP_SERVICE:Lcom/squareup/analytics/RegisterTapName;

    return-object v0
.end method

.method getButtonCount()I
    .locals 1

    .line 102
    iget-object v0, p0, Lcom/squareup/ui/items/AllServicesDetailScreen$AllServicesDetailPresenter;->catalogIntegrationController:Lcom/squareup/catalogapi/CatalogIntegrationController;

    invoke-interface {v0}, Lcom/squareup/catalogapi/CatalogIntegrationController;->shouldShowNewFeature()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0
.end method

.method getButtonText(I)Ljava/lang/String;
    .locals 1

    if-nez p1, :cond_0

    .line 106
    iget-object p1, p0, Lcom/squareup/ui/items/AllServicesDetailScreen$AllServicesDetailPresenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/registerlib/R$string;->create_service:I

    .line 107
    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/items/AllServicesDetailScreen$AllServicesDetailPresenter;->res:Lcom/squareup/util/Res;

    sget v0, Lcom/squareup/itemsapplet/R$string;->convert_items:I

    .line 108
    invoke-interface {p1, v0}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method getItemTypes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Item$Type;",
            ">;"
        }
    .end annotation

    .line 122
    sget-object v0, Lcom/squareup/api/items/Item$Type;->APPOINTMENTS_SERVICE:Lcom/squareup/api/items/Item$Type;

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method getNullHint(Landroid/content/Context;)Ljava/lang/CharSequence;
    .locals 2

    .line 82
    new-instance v0, Lcom/squareup/ui/LinkSpan$Builder;

    invoke-direct {v0, p1}, Lcom/squareup/ui/LinkSpan$Builder;-><init>(Landroid/content/Context;)V

    sget p1, Lcom/squareup/itemsapplet/R$string;->items_applet_services_null_message:I

    const-string v1, "learn_more"

    .line 83
    invoke-virtual {v0, p1, v1}, Lcom/squareup/ui/LinkSpan$Builder;->pattern(ILjava/lang/String;)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/registerlib/R$string;->items_hint_url:I

    .line 84
    invoke-virtual {p1, v0}, Lcom/squareup/ui/LinkSpan$Builder;->url(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p1

    sget v0, Lcom/squareup/common/strings/R$string;->learn_more_lowercase_more:I

    .line 85
    invoke-virtual {p1, v0}, Lcom/squareup/ui/LinkSpan$Builder;->clickableText(I)Lcom/squareup/ui/LinkSpan$Builder;

    move-result-object p1

    .line 86
    invoke-virtual {p1}, Lcom/squareup/ui/LinkSpan$Builder;->asCharSequence()Ljava/lang/CharSequence;

    move-result-object p1

    return-object p1
.end method

.method getNullTitle()Ljava/lang/String;
    .locals 2

    .line 78
    iget-object v0, p0, Lcom/squareup/ui/items/AllServicesDetailScreen$AllServicesDetailPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/itemsapplet/R$string;->items_applet_services_null_title:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method getScreenInstance()Lcom/squareup/container/ContainerTreeKey;
    .locals 1

    .line 90
    sget-object v0, Lcom/squareup/ui/items/AllServicesDetailScreen;->INSTANCE:Lcom/squareup/ui/items/AllServicesDetailScreen;

    return-object v0
.end method

.method getSearchHint()Ljava/lang/String;
    .locals 2

    .line 74
    iget-object v0, p0, Lcom/squareup/ui/items/AllServicesDetailScreen$AllServicesDetailPresenter;->res:Lcom/squareup/util/Res;

    sget v1, Lcom/squareup/registerlib/R$string;->search_library_hint_all_services:I

    invoke-interface {v0, v1}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method onButtonClicked(I)V
    .locals 1

    if-nez p1, :cond_0

    .line 113
    iget-object p1, p0, Lcom/squareup/ui/items/AllServicesDetailScreen$AllServicesDetailPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v0, Lcom/squareup/analytics/RegisterTapName;->ITEMS_APPLET_CREATE_SERVICE:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 114
    iget-object p1, p0, Lcom/squareup/ui/items/AllServicesDetailScreen$AllServicesDetailPresenter;->editItemGateway:Lcom/squareup/ui/items/EditItemGateway;

    sget-object v0, Lcom/squareup/api/items/Item$Type;->APPOINTMENTS_SERVICE:Lcom/squareup/api/items/Item$Type;

    invoke-interface {p1, v0}, Lcom/squareup/ui/items/EditItemGateway;->startEditNewItemFlow(Lcom/squareup/api/items/Item$Type;)V

    goto :goto_0

    .line 116
    :cond_0
    iget-object p1, p0, Lcom/squareup/ui/items/AllServicesDetailScreen$AllServicesDetailPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v0, Lcom/squareup/analytics/RegisterTapName;->SERVICES_CONVERT_ITEMS:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 117
    iget-object p1, p0, Lcom/squareup/ui/items/AllServicesDetailScreen$AllServicesDetailPresenter;->launcher:Lcom/squareup/util/BrowserLauncher;

    iget-object v0, p0, Lcom/squareup/ui/items/AllServicesDetailScreen$AllServicesDetailPresenter;->convertItemsUrlHelper:Lcom/squareup/ui/items/ConvertItemsUrlHelper;

    invoke-virtual {v0}, Lcom/squareup/ui/items/ConvertItemsUrlHelper;->getConvertItemsURL()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/squareup/util/BrowserLauncher;->launchBrowser(Ljava/lang/String;)V

    :goto_0
    return-void
.end method
