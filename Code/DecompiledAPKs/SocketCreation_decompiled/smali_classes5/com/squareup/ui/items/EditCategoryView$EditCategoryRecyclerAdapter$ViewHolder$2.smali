.class Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder$2;
.super Lcom/squareup/text/EmptyTextWatcher;
.source "EditCategoryView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder;->bindStaticTopRowContent()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder;

.field final synthetic val$categoryImageTile:Lcom/squareup/register/widgets/EditCatalogObjectLabel;

.field final synthetic val$categoryTextTile:Lcom/squareup/orderentry/TextTile;


# direct methods
.method constructor <init>(Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder;Lcom/squareup/orderentry/TextTile;Lcom/squareup/register/widgets/EditCatalogObjectLabel;)V
    .locals 0

    .line 167
    iput-object p1, p0, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder$2;->this$2:Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder;

    iput-object p2, p0, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder$2;->val$categoryTextTile:Lcom/squareup/orderentry/TextTile;

    iput-object p3, p0, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder$2;->val$categoryImageTile:Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    invoke-direct {p0}, Lcom/squareup/text/EmptyTextWatcher;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 3

    .line 169
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    .line 170
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder$2;->this$2:Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder;

    iget-object v0, v0, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder;->this$1:Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;

    iget-object v0, v0, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;->this$0:Lcom/squareup/ui/items/EditCategoryView;

    iget-object v0, v0, Lcom/squareup/ui/items/EditCategoryView;->presenter:Lcom/squareup/ui/items/EditCategoryScreen$Presenter;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/items/EditCategoryScreen$Presenter;->updateCategoryNameAndAbbreviation(Ljava/lang/String;)V

    .line 171
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder$2;->this$2:Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder;

    iget-object v0, v0, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder;->this$1:Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;

    iget-object v1, p0, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder$2;->val$categoryTextTile:Lcom/squareup/orderentry/TextTile;

    iget-object v2, p0, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder$2;->val$categoryImageTile:Lcom/squareup/register/widgets/EditCatalogObjectLabel;

    invoke-static {v0, v1, v2}, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;->access$300(Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;Lcom/squareup/orderentry/TextTile;Lcom/squareup/register/widgets/EditCatalogObjectLabel;)V

    .line 173
    iget-object v0, p0, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder$2;->this$2:Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder;

    iget-object v0, v0, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter$ViewHolder;->this$1:Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;

    invoke-static {v0}, Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;->access$000(Lcom/squareup/ui/items/EditCategoryView$EditCategoryRecyclerAdapter;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/ui/items/EditCategoryView$CategoryNameChangeListener;

    .line 174
    invoke-interface {v1, p1}, Lcom/squareup/ui/items/EditCategoryView$CategoryNameChangeListener;->onCurrentCategoryNameChanged(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    return-void
.end method
