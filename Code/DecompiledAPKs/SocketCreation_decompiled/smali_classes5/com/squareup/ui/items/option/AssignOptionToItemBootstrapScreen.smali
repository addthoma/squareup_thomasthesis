.class public final Lcom/squareup/ui/items/option/AssignOptionToItemBootstrapScreen;
.super Lcom/squareup/container/BootstrapTreeKey;
.source "AssignOptionToItemBootstrapScreen.kt"


# annotations
.annotation runtime Lcom/squareup/container/layer/FullSheet;
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0008\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0008\u0001\u0018\u00002\u00020\u0001BQ\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0008\u0010\u0006\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0008\u0012\u000c\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\n\u0012\u000c\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u00050\n\u0012\u000c\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\n\u00a2\u0006\u0002\u0010\u000fJ\u0010\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0016J\u0008\u0010\u0014\u001a\u00020\u0015H\u0016R\u000e\u0010\u0007\u001a\u00020\u0008X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\u0008\u0012\u0004\u0012\u00020\u000e0\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u0008\u0012\u0004\u0012\u00020\u000b0\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u0004\u0018\u00010\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000c\u001a\u0008\u0012\u0004\u0012\u00020\u00050\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"
    }
    d2 = {
        "Lcom/squareup/ui/items/option/AssignOptionToItemBootstrapScreen;",
        "Lcom/squareup/container/BootstrapTreeKey;",
        "editItemScope",
        "Lcom/squareup/ui/items/EditItemScope;",
        "itemName",
        "",
        "itemToken",
        "canSkipFetchingVariations",
        "",
        "existingVariations",
        "",
        "Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
        "locallyDeletedVariationTokens",
        "existingItemOptions",
        "Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;",
        "(Lcom/squareup/ui/items/EditItemScope;Ljava/lang/String;Ljava/lang/String;ZLjava/util/List;Ljava/util/List;Ljava/util/List;)V",
        "doRegister",
        "",
        "scope",
        "Lmortar/MortarScope;",
        "getParentKey",
        "Lcom/squareup/ui/items/option/AssignOptionToItemScope;",
        "edit-item_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final canSkipFetchingVariations:Z

.field private final editItemScope:Lcom/squareup/ui/items/EditItemScope;

.field private final existingItemOptions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;",
            ">;"
        }
    .end annotation
.end field

.field private final existingVariations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
            ">;"
        }
    .end annotation
.end field

.field private final itemName:Ljava/lang/String;

.field private final itemToken:Ljava/lang/String;

.field private final locallyDeletedVariationTokens:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/squareup/ui/items/EditItemScope;Ljava/lang/String;Ljava/lang/String;ZLjava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/items/EditItemScope;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/models/CatalogItemVariation;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Lcom/squareup/shared/catalog/connectv2/models/CatalogItemOption;",
            ">;)V"
        }
    .end annotation

    const-string v0, "editItemScope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "itemName"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "existingVariations"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "locallyDeletedVariationTokens"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "existingItemOptions"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0}, Lcom/squareup/container/BootstrapTreeKey;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/items/option/AssignOptionToItemBootstrapScreen;->editItemScope:Lcom/squareup/ui/items/EditItemScope;

    iput-object p2, p0, Lcom/squareup/ui/items/option/AssignOptionToItemBootstrapScreen;->itemName:Ljava/lang/String;

    iput-object p3, p0, Lcom/squareup/ui/items/option/AssignOptionToItemBootstrapScreen;->itemToken:Ljava/lang/String;

    iput-boolean p4, p0, Lcom/squareup/ui/items/option/AssignOptionToItemBootstrapScreen;->canSkipFetchingVariations:Z

    iput-object p5, p0, Lcom/squareup/ui/items/option/AssignOptionToItemBootstrapScreen;->existingVariations:Ljava/util/List;

    iput-object p6, p0, Lcom/squareup/ui/items/option/AssignOptionToItemBootstrapScreen;->locallyDeletedVariationTokens:Ljava/util/List;

    iput-object p7, p0, Lcom/squareup/ui/items/option/AssignOptionToItemBootstrapScreen;->existingItemOptions:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public doRegister(Lmortar/MortarScope;)V
    .locals 8

    const-string v0, "scope"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    sget-object v0, Lcom/squareup/items/assignitemoptions/AssignOptionToItemWorkflowRunner;->Companion:Lcom/squareup/items/assignitemoptions/AssignOptionToItemWorkflowRunner$Companion;

    invoke-virtual {v0, p1}, Lcom/squareup/items/assignitemoptions/AssignOptionToItemWorkflowRunner$Companion;->getRunner(Lmortar/MortarScope;)Lcom/squareup/items/assignitemoptions/AssignOptionToItemWorkflowRunner;

    move-result-object v1

    .line 24
    iget-object v2, p0, Lcom/squareup/ui/items/option/AssignOptionToItemBootstrapScreen;->itemName:Ljava/lang/String;

    .line 25
    iget-object v3, p0, Lcom/squareup/ui/items/option/AssignOptionToItemBootstrapScreen;->itemToken:Ljava/lang/String;

    .line 26
    iget-boolean v4, p0, Lcom/squareup/ui/items/option/AssignOptionToItemBootstrapScreen;->canSkipFetchingVariations:Z

    .line 27
    iget-object v5, p0, Lcom/squareup/ui/items/option/AssignOptionToItemBootstrapScreen;->existingVariations:Ljava/util/List;

    .line 28
    iget-object v6, p0, Lcom/squareup/ui/items/option/AssignOptionToItemBootstrapScreen;->locallyDeletedVariationTokens:Ljava/util/List;

    .line 29
    iget-object v7, p0, Lcom/squareup/ui/items/option/AssignOptionToItemBootstrapScreen;->existingItemOptions:Ljava/util/List;

    .line 23
    invoke-interface/range {v1 .. v7}, Lcom/squareup/items/assignitemoptions/AssignOptionToItemWorkflowRunner;->start(Ljava/lang/String;Ljava/lang/String;ZLjava/util/List;Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method public getParentKey()Lcom/squareup/ui/items/option/AssignOptionToItemScope;
    .locals 2

    .line 33
    new-instance v0, Lcom/squareup/ui/items/option/AssignOptionToItemScope;

    .line 34
    iget-object v1, p0, Lcom/squareup/ui/items/option/AssignOptionToItemBootstrapScreen;->editItemScope:Lcom/squareup/ui/items/EditItemScope;

    .line 33
    invoke-direct {v0, v1}, Lcom/squareup/ui/items/option/AssignOptionToItemScope;-><init>(Lcom/squareup/ui/items/EditItemScope;)V

    return-object v0
.end method

.method public bridge synthetic getParentKey()Ljava/lang/Object;
    .locals 1

    .line 13
    invoke-virtual {p0}, Lcom/squareup/ui/items/option/AssignOptionToItemBootstrapScreen;->getParentKey()Lcom/squareup/ui/items/option/AssignOptionToItemScope;

    move-result-object v0

    return-object v0
.end method
