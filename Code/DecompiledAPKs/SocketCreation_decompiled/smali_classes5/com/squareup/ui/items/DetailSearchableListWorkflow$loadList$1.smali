.class final Lcom/squareup/ui/items/DetailSearchableListWorkflow$loadList$1;
.super Lkotlin/jvm/internal/Lambda;
.source "DetailSearchableListWorkflow.kt"

# interfaces
.implements Lkotlin/jvm/functions/Function1;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/DetailSearchableListWorkflow;->loadList(Lcom/squareup/workflow/RenderContext;Lcom/squareup/ui/items/DetailSearchableListState;Lio/reactivex/Observable;Ljava/lang/Integer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkotlin/jvm/internal/Lambda;",
        "Lkotlin/jvm/functions/Function1<",
        "Lcom/squareup/ui/items/DetailSearchableListData;",
        "Lcom/squareup/workflow/WorkflowAction<",
        "Lcom/squareup/ui/items/DetailSearchableListState;",
        "+",
        "Lcom/squareup/ui/items/DetailSearchableListResult;",
        ">;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0016\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u00012\u0006\u0010\u0004\u001a\u00020\u0005H\n\u00a2\u0006\u0002\u0008\u0006"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/ui/items/DetailSearchableListState;",
        "Lcom/squareup/ui/items/DetailSearchableListResult;",
        "listData",
        "Lcom/squareup/ui/items/DetailSearchableListData;",
        "invoke"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic $destinationState:Lcom/squareup/ui/items/DetailSearchableListState;

.field final synthetic $objectNumberLimit:Ljava/lang/Integer;


# direct methods
.method constructor <init>(Lcom/squareup/ui/items/DetailSearchableListState;Ljava/lang/Integer;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflow$loadList$1;->$destinationState:Lcom/squareup/ui/items/DetailSearchableListState;

    iput-object p2, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflow$loadList$1;->$objectNumberLimit:Ljava/lang/Integer;

    const/4 p1, 0x1

    invoke-direct {p0, p1}, Lkotlin/jvm/internal/Lambda;-><init>(I)V

    return-void
.end method


# virtual methods
.method public final invoke(Lcom/squareup/ui/items/DetailSearchableListData;)Lcom/squareup/workflow/WorkflowAction;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/items/DetailSearchableListData;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/items/DetailSearchableListState;",
            "Lcom/squareup/ui/items/DetailSearchableListResult;",
            ">;"
        }
    .end annotation

    const-string v0, "listData"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 237
    sget-object v0, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 238
    sget-object v1, Lcom/squareup/ui/items/DetailSearchableListState;->Companion:Lcom/squareup/ui/items/DetailSearchableListState$Companion;

    .line 239
    iget-object v2, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflow$loadList$1;->$destinationState:Lcom/squareup/ui/items/DetailSearchableListState;

    .line 240
    invoke-virtual {p1}, Lcom/squareup/ui/items/DetailSearchableListData;->getFilteredListData()Lcom/squareup/cycler/DataSource;

    move-result-object v3

    .line 241
    invoke-virtual {p1}, Lcom/squareup/ui/items/DetailSearchableListData;->getUnfilteredListSize()I

    move-result p1

    .line 242
    iget-object v4, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflow$loadList$1;->$objectNumberLimit:Ljava/lang/Integer;

    .line 238
    invoke-virtual {v1, v2, v3, p1, v4}, Lcom/squareup/ui/items/DetailSearchableListState$Companion;->copyStateWithNewDatasource(Lcom/squareup/ui/items/DetailSearchableListState;Lcom/squareup/cycler/DataSource;ILjava/lang/Integer;)Lcom/squareup/ui/items/DetailSearchableListState;

    move-result-object p1

    const/4 v1, 0x0

    const/4 v2, 0x2

    .line 237
    invoke-static {v0, p1, v1, v2, v1}, Lcom/squareup/workflow/WorkflowAction$Companion;->enterState$default(Lcom/squareup/workflow/WorkflowAction$Companion;Ljava/lang/Object;Ljava/lang/Object;ILjava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic invoke(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 115
    check-cast p1, Lcom/squareup/ui/items/DetailSearchableListData;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/DetailSearchableListWorkflow$loadList$1;->invoke(Lcom/squareup/ui/items/DetailSearchableListData;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method
