.class public abstract Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;
.source "LegacyReorderableRecyclerViewAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<VH:",
        "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;",
        "T:",
        "Ljava/lang/Object;",
        ">",
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "TVH;>;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field protected draggingRowId:J

.field protected draggingRowView:Landroid/view/View;

.field protected list:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>()V
    .locals 1

    .line 20
    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    const/4 v0, 0x1

    .line 21
    invoke-virtual {p0, v0}, Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;->setHasStableIds(Z)V

    return-void
.end method


# virtual methods
.method public getDraggingRowView()Landroid/view/View;
    .locals 1

    .line 95
    iget-object v0, p0, Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;->draggingRowView:Landroid/view/View;

    return-object v0
.end method

.method public getIndexForId(I)I
    .locals 6

    const/4 v0, 0x0

    .line 62
    :goto_0
    iget-object v1, p0, Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;->list:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 63
    invoke-virtual {p0, v0}, Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;->getStableIdForIndex(I)J

    move-result-wide v1

    int-to-long v3, p1

    cmp-long v5, v1, v3

    if-nez v5, :cond_0

    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, -0x1

    return p1
.end method

.method public getItemAtPosition(I)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    .line 37
    iget-object v0, p0, Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;->list:Ljava/util/List;

    invoke-virtual {p0}, Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;->getStaticTopRowsCount()I

    move-result v1

    sub-int/2addr p1, v1

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public getItemCount()I
    .locals 2

    .line 76
    iget-object v0, p0, Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;->list:Ljava/util/List;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 77
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;->getStaticTopRowsCount()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;->getStaticBottomRowsCount()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public getItemId(I)J
    .locals 2

    .line 81
    invoke-virtual {p0}, Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;->getStaticTopRowsCount()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 82
    invoke-virtual {p0}, Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;->getItemCount()I

    move-result v0

    invoke-virtual {p0}, Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;->getStaticBottomRowsCount()I

    move-result v1

    sub-int/2addr v0, v1

    if-lt p1, v0, :cond_0

    goto :goto_0

    .line 85
    :cond_0
    invoke-virtual {p0}, Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;->getStaticTopRowsCount()I

    move-result v0

    sub-int/2addr p1, v0

    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;->getStableIdForIndex(I)J

    move-result-wide v0

    return-wide v0

    :cond_1
    :goto_0
    neg-int p1, p1

    int-to-long v0, p1

    return-wide v0
.end method

.method public abstract getStableIdForIndex(I)J
.end method

.method public abstract getStaticBottomRowsCount()I
.end method

.method public abstract getStaticTopRowsCount()I
.end method

.method public notifyIndexChanged(I)V
    .locals 1

    .line 72
    invoke-virtual {p0}, Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;->getStaticTopRowsCount()I

    move-result v0

    add-int/2addr p1, v0

    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;->notifyItemChanged(I)V

    return-void
.end method

.method public reorderItems(II)V
    .locals 2

    if-ltz p1, :cond_1

    if-ltz p2, :cond_1

    .line 46
    invoke-virtual {p0}, Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;->getItemCount()I

    move-result v0

    if-ge p1, v0, :cond_1

    invoke-virtual {p0}, Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;->getItemCount()I

    move-result v0

    if-lt p2, v0, :cond_0

    goto :goto_0

    .line 50
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;->list:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    .line 51
    iget-object v1, p0, Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;->list:Ljava/util/List;

    invoke-interface {v1, p2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 55
    invoke-virtual {p0}, Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;->getStaticTopRowsCount()I

    move-result v0

    add-int/2addr v0, p1

    invoke-virtual {p0}, Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;->getStaticTopRowsCount()I

    move-result v1

    add-int/2addr v1, p2

    invoke-virtual {p0, v0, v1}, Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;->notifyItemMoved(II)V

    .line 57
    invoke-virtual {p0}, Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;->getStaticTopRowsCount()I

    move-result v0

    add-int/2addr p1, v0

    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;->notifyItemChanged(I)V

    .line 58
    invoke-virtual {p0}, Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;->getStaticTopRowsCount()I

    move-result p1

    add-int/2addr p2, p1

    invoke-virtual {p0, p2}, Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;->notifyItemChanged(I)V

    :cond_1
    :goto_0
    return-void
.end method

.method public setContentsFromSource(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "TT;>;)V"
        }
    .end annotation

    .line 32
    iput-object p1, p0, Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;->list:Ljava/util/List;

    .line 33
    invoke-virtual {p0}, Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public setDraggingRowId(J)V
    .locals 0

    .line 90
    iput-wide p1, p0, Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;->draggingRowId:J

    const/4 p1, 0x0

    .line 91
    iput-object p1, p0, Lcom/squareup/ui/items/LegacyReorderableRecyclerViewAdapter;->draggingRowView:Landroid/view/View;

    return-void
.end method
