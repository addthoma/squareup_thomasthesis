.class Lcom/squareup/ui/items/EditDiscountView$SavedState;
.super Landroid/view/View$BaseSavedState;
.source "EditDiscountView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/EditDiscountView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SavedState"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/squareup/ui/items/EditDiscountView$SavedState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private percentageDiscount:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 370
    new-instance v0, Lcom/squareup/ui/items/EditDiscountView$SavedState$1;

    invoke-direct {v0}, Lcom/squareup/ui/items/EditDiscountView$SavedState$1;-><init>()V

    sput-object v0, Lcom/squareup/ui/items/EditDiscountView$SavedState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .line 388
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcel;)V

    .line 389
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result p1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lcom/squareup/ui/items/EditDiscountView$SavedState;->percentageDiscount:Z

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/squareup/ui/items/EditDiscountView$1;)V
    .locals 0

    .line 369
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/EditDiscountView$SavedState;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcelable;Z)V
    .locals 0

    .line 383
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcelable;)V

    .line 384
    iput-boolean p2, p0, Lcom/squareup/ui/items/EditDiscountView$SavedState;->percentageDiscount:Z

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcelable;ZLcom/squareup/ui/items/EditDiscountView$1;)V
    .locals 0

    .line 369
    invoke-direct {p0, p1, p2}, Lcom/squareup/ui/items/EditDiscountView$SavedState;-><init>(Landroid/os/Parcelable;Z)V

    return-void
.end method

.method static synthetic access$500(Lcom/squareup/ui/items/EditDiscountView$SavedState;)Z
    .locals 0

    .line 369
    iget-boolean p0, p0, Lcom/squareup/ui/items/EditDiscountView$SavedState;->percentageDiscount:Z

    return p0
.end method


# virtual methods
.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .line 393
    invoke-super {p0, p1, p2}, Landroid/view/View$BaseSavedState;->writeToParcel(Landroid/os/Parcel;I)V

    .line 394
    iget-boolean p2, p0, Lcom/squareup/ui/items/EditDiscountView$SavedState;->percentageDiscount:Z

    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
