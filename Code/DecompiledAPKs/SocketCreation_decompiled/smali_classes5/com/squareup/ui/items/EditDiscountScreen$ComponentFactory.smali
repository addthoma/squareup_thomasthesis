.class public Lcom/squareup/ui/items/EditDiscountScreen$ComponentFactory;
.super Ljava/lang/Object;
.source "EditDiscountScreen.java"

# interfaces
.implements Lcom/squareup/ui/WithComponent$Factory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/EditDiscountScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ComponentFactory"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 507
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public create(Lmortar/MortarScope;Lcom/squareup/container/ContainerTreeKey;)Ljava/lang/Object;
    .locals 1

    .line 509
    const-class v0, Lcom/squareup/ui/items/EditDiscountScope$Component;

    .line 510
    invoke-static {p1, v0}, Lcom/squareup/dagger/Components;->component(Lmortar/MortarScope;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/items/EditDiscountScope$Component;

    .line 511
    check-cast p2, Lcom/squareup/ui/items/EditDiscountScreen;

    .line 512
    new-instance v0, Lcom/squareup/ui/items/EditDiscountScreen$Module;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {v0, p2}, Lcom/squareup/ui/items/EditDiscountScreen$Module;-><init>(Lcom/squareup/ui/items/EditDiscountScreen;)V

    .line 513
    invoke-interface {p1, v0}, Lcom/squareup/ui/items/EditDiscountScope$Component;->editDiscountComponent(Lcom/squareup/ui/items/EditDiscountScreen$Module;)Lcom/squareup/ui/items/EditDiscountScreen$Component;

    move-result-object p1

    return-object p1
.end method
