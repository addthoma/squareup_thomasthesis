.class final Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$onEnterScope$2;
.super Ljava/lang/Object;
.source "DetailSearchableListWorkflowRunner.kt"

# interfaces
.implements Lio/reactivex/functions/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;->onEnterScope(Lmortar/MortarScope;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/reactivex/functions/Consumer<",
        "Lcom/squareup/ui/items/DetailSearchableListResult;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "",
        "it",
        "Lcom/squareup/ui/items/DetailSearchableListResult;",
        "kotlin.jvm.PlatformType",
        "accept"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$onEnterScope$2;->this$0:Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final accept(Lcom/squareup/ui/items/DetailSearchableListResult;)V
    .locals 4

    .line 80
    sget-object v0, Lcom/squareup/ui/items/DetailSearchableListResult$ExitList;->INSTANCE:Lcom/squareup/ui/items/DetailSearchableListResult$ExitList;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$onEnterScope$2;->this$0:Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;

    invoke-static {p1}, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;->access$getContainer$p(Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;)Lcom/squareup/ui/main/PosContainer;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/ui/main/PosContainer;->onBackPressed()V

    goto/16 :goto_0

    .line 81
    :cond_0
    sget-object v0, Lcom/squareup/ui/items/DetailSearchableListResult$GoToEditFavGrid;->INSTANCE:Lcom/squareup/ui/items/DetailSearchableListResult$GoToEditFavGrid;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$onEnterScope$2;->this$0:Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;

    invoke-static {p1}, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;->access$getPermissionGatekeeper$p(Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;)Lcom/squareup/permissions/PermissionGatekeeper;

    move-result-object p1

    sget-object v0, Lcom/squareup/permissions/Permission;->EDIT_ITEMS:Lcom/squareup/permissions/Permission;

    new-instance v1, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$onEnterScope$2$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$onEnterScope$2$1;-><init>(Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$onEnterScope$2;)V

    check-cast v1, Lcom/squareup/permissions/PermissionGatekeeper$When;

    invoke-virtual {p1, v0, v1}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    goto/16 :goto_0

    .line 86
    :cond_1
    sget-object v0, Lcom/squareup/ui/items/DetailSearchableListResult$GoToConvertItemToServiceOnDashboard;->INSTANCE:Lcom/squareup/ui/items/DetailSearchableListResult$GoToConvertItemToServiceOnDashboard;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$onEnterScope$2;->this$0:Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;

    invoke-static {p1}, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;->access$getBrowserLauncher$p(Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;)Lcom/squareup/util/BrowserLauncher;

    move-result-object p1

    .line 87
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$onEnterScope$2;->this$0:Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;

    invoke-static {v0}, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;->access$getConvertItemsUrlHelper$p(Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;)Lcom/squareup/ui/items/ConvertItemsUrlHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/ui/items/ConvertItemsUrlHelper;->getConvertItemsURL()Ljava/lang/String;

    move-result-object v0

    .line 86
    invoke-interface {p1, v0}, Lcom/squareup/util/BrowserLauncher;->launchBrowser(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 89
    :cond_2
    sget-object v0, Lcom/squareup/ui/items/DetailSearchableListResult$GoToCreateDiscount;->INSTANCE:Lcom/squareup/ui/items/DetailSearchableListResult$GoToCreateDiscount;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 90
    iget-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$onEnterScope$2;->this$0:Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;

    invoke-static {p1}, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;->access$getAnalytics$p(Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;)Lcom/squareup/analytics/Analytics;

    move-result-object p1

    sget-object v0, Lcom/squareup/analytics/RegisterTapName;->ITEMS_APPLET_CREATE_DISCOUNT:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 91
    iget-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$onEnterScope$2;->this$0:Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;

    invoke-static {p1}, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;->access$getEditItemGateway$p(Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;)Lcom/squareup/ui/items/EditItemGateway;

    move-result-object p1

    invoke-interface {p1}, Lcom/squareup/ui/items/EditItemGateway;->startNewDiscount()V

    goto/16 :goto_0

    .line 93
    :cond_3
    sget-object v0, Lcom/squareup/ui/items/DetailSearchableListResult$GoToCreateItem;->INSTANCE:Lcom/squareup/ui/items/DetailSearchableListResult$GoToCreateItem;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 94
    iget-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$onEnterScope$2;->this$0:Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;

    invoke-static {p1}, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;->access$getAnalytics$p(Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;)Lcom/squareup/analytics/Analytics;

    move-result-object p1

    sget-object v0, Lcom/squareup/analytics/RegisterTapName;->ITEMS_APPLET_CREATE_ITEM:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 95
    iget-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$onEnterScope$2;->this$0:Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;

    invoke-static {p1}, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;->access$getEditItemGateway$p(Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;)Lcom/squareup/ui/items/EditItemGateway;

    move-result-object p1

    sget-object v0, Lcom/squareup/api/items/Item$Type;->REGULAR:Lcom/squareup/api/items/Item$Type;

    invoke-interface {p1, v0}, Lcom/squareup/ui/items/EditItemGateway;->startEditNewItemFlow(Lcom/squareup/api/items/Item$Type;)V

    goto/16 :goto_0

    .line 97
    :cond_4
    sget-object v0, Lcom/squareup/ui/items/DetailSearchableListResult$GoToCreateService;->INSTANCE:Lcom/squareup/ui/items/DetailSearchableListResult$GoToCreateService;

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 98
    iget-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$onEnterScope$2;->this$0:Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;

    invoke-static {p1}, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;->access$getAnalytics$p(Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;)Lcom/squareup/analytics/Analytics;

    move-result-object p1

    sget-object v0, Lcom/squareup/analytics/RegisterTapName;->ITEMS_APPLET_CREATE_SERVICE:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {p1, v0}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 99
    iget-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$onEnterScope$2;->this$0:Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;

    invoke-static {p1}, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;->access$getEditItemGateway$p(Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;)Lcom/squareup/ui/items/EditItemGateway;

    move-result-object p1

    sget-object v0, Lcom/squareup/api/items/Item$Type;->APPOINTMENTS_SERVICE:Lcom/squareup/api/items/Item$Type;

    invoke-interface {p1, v0}, Lcom/squareup/ui/items/EditItemGateway;->startEditNewItemFlow(Lcom/squareup/api/items/Item$Type;)V

    goto/16 :goto_0

    .line 101
    :cond_5
    instance-of v0, p1, Lcom/squareup/ui/items/DetailSearchableListResult$GoToEditDiscount;

    if-eqz v0, :cond_6

    .line 102
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$onEnterScope$2;->this$0:Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;

    invoke-static {v0}, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;->access$getAnalytics$p(Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;)Lcom/squareup/analytics/Analytics;

    move-result-object v0

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ITEMS_APPLET_EDIT_DISCOUNT:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 103
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$onEnterScope$2;->this$0:Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;

    invoke-static {v0}, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;->access$getEditItemGateway$p(Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;)Lcom/squareup/ui/items/EditItemGateway;

    move-result-object v0

    check-cast p1, Lcom/squareup/ui/items/DetailSearchableListResult$GoToEditDiscount;

    invoke-virtual {p1}, Lcom/squareup/ui/items/DetailSearchableListResult$GoToEditDiscount;->getDiscountId()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/ui/items/EditItemGateway;->startEditDiscount(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 105
    :cond_6
    instance-of v0, p1, Lcom/squareup/ui/items/DetailSearchableListResult$GoToEditItem;

    if-eqz v0, :cond_7

    .line 106
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$onEnterScope$2;->this$0:Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;

    invoke-static {v0}, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;->access$getAnalytics$p(Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;)Lcom/squareup/analytics/Analytics;

    move-result-object v0

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ITEMS_APPLET_EDIT_ITEM:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 107
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$onEnterScope$2;->this$0:Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;

    invoke-static {v0}, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;->access$getEditItemGateway$p(Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;)Lcom/squareup/ui/items/EditItemGateway;

    move-result-object v0

    check-cast p1, Lcom/squareup/ui/items/DetailSearchableListResult$GoToEditItem;

    invoke-virtual {p1}, Lcom/squareup/ui/items/DetailSearchableListResult$GoToEditItem;->getItemId()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/squareup/api/items/Item$Type;->REGULAR:Lcom/squareup/api/items/Item$Type;

    invoke-virtual {p1}, Lcom/squareup/ui/items/DetailSearchableListResult$GoToEditItem;->getImageUrl()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, v1, v2, p1}, Lcom/squareup/ui/items/EditItemGateway;->startEditItemFlow(Ljava/lang/String;Lcom/squareup/api/items/Item$Type;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 109
    :cond_7
    instance-of v0, p1, Lcom/squareup/ui/items/DetailSearchableListResult$GoToEditService;

    if-eqz v0, :cond_8

    .line 110
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$onEnterScope$2;->this$0:Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;

    invoke-static {v0}, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;->access$getAnalytics$p(Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;)Lcom/squareup/analytics/Analytics;

    move-result-object v0

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ITEMS_APPLET_EDIT_SERVICE:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 111
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$onEnterScope$2;->this$0:Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;

    invoke-static {v0}, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;->access$getEditItemGateway$p(Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;)Lcom/squareup/ui/items/EditItemGateway;

    move-result-object v0

    check-cast p1, Lcom/squareup/ui/items/DetailSearchableListResult$GoToEditService;

    invoke-virtual {p1}, Lcom/squareup/ui/items/DetailSearchableListResult$GoToEditService;->getServiceId()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/squareup/api/items/Item$Type;->APPOINTMENTS_SERVICE:Lcom/squareup/api/items/Item$Type;

    invoke-virtual {p1}, Lcom/squareup/ui/items/DetailSearchableListResult$GoToEditService;->getImageUrl()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, v1, v2, p1}, Lcom/squareup/ui/items/EditItemGateway;->startEditItemFlow(Ljava/lang/String;Lcom/squareup/api/items/Item$Type;Ljava/lang/String;)V

    goto :goto_0

    .line 113
    :cond_8
    instance-of v0, p1, Lcom/squareup/ui/items/DetailSearchableListResult$GoToHandleScannedBarcode;

    if-eqz v0, :cond_a

    .line 114
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$onEnterScope$2;->this$0:Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;

    invoke-static {v0}, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;->access$isDetailSearchableListScreenInForeground(Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;)Z

    move-result v0

    .line 115
    iget-object v1, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$onEnterScope$2;->this$0:Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;

    invoke-static {v1}, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;->access$getFeatures$p(Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;)Lcom/squareup/settings/server/Features;

    move-result-object v1

    sget-object v2, Lcom/squareup/settings/server/Features$Feature;->BARCODE_SCANNERS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v1, v2}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v1

    if-eqz v1, :cond_9

    if-eqz v0, :cond_9

    .line 116
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$onEnterScope$2;->this$0:Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;

    invoke-static {v0}, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;->access$getCogs$p(Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;)Lcom/squareup/cogs/Cogs;

    move-result-object v0

    new-instance v1, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$onEnterScope$2$2;

    invoke-direct {v1, p1}, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$onEnterScope$2$2;-><init>(Lcom/squareup/ui/items/DetailSearchableListResult;)V

    check-cast v1, Lcom/squareup/shared/catalog/CatalogTask;

    .line 121
    new-instance v2, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$onEnterScope$2$3;

    invoke-direct {v2, p0, p1}, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$onEnterScope$2$3;-><init>(Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$onEnterScope$2;Lcom/squareup/ui/items/DetailSearchableListResult;)V

    check-cast v2, Lcom/squareup/shared/catalog/CatalogCallback;

    .line 116
    invoke-interface {v0, v1, v2}, Lcom/squareup/cogs/Cogs;->execute(Lcom/squareup/shared/catalog/CatalogTask;Lcom/squareup/shared/catalog/CatalogCallback;)V

    goto :goto_0

    :cond_9
    const/4 p1, 0x2

    new-array p1, p1, [Ljava/lang/Object;

    const/4 v1, 0x0

    .line 128
    iget-object v2, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$onEnterScope$2;->this$0:Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;

    invoke-static {v2}, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;->access$getFeatures$p(Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;)Lcom/squareup/settings/server/Features;

    move-result-object v2

    sget-object v3, Lcom/squareup/settings/server/Features$Feature;->BARCODE_SCANNERS:Lcom/squareup/settings/server/Features$Feature;

    invoke-interface {v2, v3}, Lcom/squareup/settings/server/Features;->isEnabled(Lcom/squareup/settings/server/Features$Feature;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, p1, v1

    const/4 v1, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, p1, v1

    const-string v0, "Ignoring barcode scanned. Feature enabled: %b, screen in foreground: %b"

    .line 126
    invoke-static {v0, p1}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_a
    :goto_0
    return-void
.end method

.method public bridge synthetic accept(Ljava/lang/Object;)V
    .locals 0

    .line 52
    check-cast p1, Lcom/squareup/ui/items/DetailSearchableListResult;

    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$onEnterScope$2;->accept(Lcom/squareup/ui/items/DetailSearchableListResult;)V

    return-void
.end method
