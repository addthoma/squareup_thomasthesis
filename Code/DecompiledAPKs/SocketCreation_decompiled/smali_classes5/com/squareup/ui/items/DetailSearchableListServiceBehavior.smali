.class public final Lcom/squareup/ui/items/DetailSearchableListServiceBehavior;
.super Ljava/lang/Object;
.source "DetailSearchableListServiceBehavior.kt"

# interfaces
.implements Lcom/squareup/ui/items/DetailSearchableListBehavior;


# annotations
.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000l\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0008\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0008\n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\u0018\u00002\u00020\u0001B7\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0008\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u00a2\u0006\u0002\u0010\u000eJ$\u0010\u001b\u001a\u000e\u0012\u0004\u0012\u00020\u001d\u0012\u0004\u0012\u00020\u001e0\u001c2\u0006\u0010\u001f\u001a\u00020 2\u0006\u0010!\u001a\u00020\"H\u0016J$\u0010#\u001a\u000e\u0012\u0004\u0012\u00020\u001d\u0012\u0004\u0012\u00020\u001e0\u001c2\u0006\u0010\u001f\u001a\u00020 2\u0006\u0010!\u001a\u00020$H\u0016J\u0016\u0010%\u001a\u0008\u0012\u0004\u0012\u00020\'0&2\u0006\u0010(\u001a\u00020)H\u0016R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u000f\u001a\u0008\u0012\u0004\u0012\u00020\u00110\u00108VX\u0096\u0004\u00a2\u0006\u0006\u001a\u0004\u0008\u0012\u0010\u0013R\u0018\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0096\u0004\u00a2\u0006\n\n\u0002\u0010\u0016\u001a\u0004\u0008\u0014\u0010\u0015R\u0014\u0010\u0002\u001a\u00020\u0003X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0017\u0010\u0018R\u0014\u0010\u0006\u001a\u00020\u0007X\u0096\u0004\u00a2\u0006\u0008\n\u0000\u001a\u0004\u0008\u0019\u0010\u001a\u00a8\u0006*"
    }
    d2 = {
        "Lcom/squareup/ui/items/DetailSearchableListServiceBehavior;",
        "Lcom/squareup/ui/items/DetailSearchableListBehavior;",
        "shouldShowCreateFromSearchButton",
        "",
        "objectNumberLimit",
        "",
        "textBag",
        "Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;",
        "cogs",
        "Lcom/squareup/cogs/Cogs;",
        "badBus",
        "Lcom/squareup/badbus/BadBus;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "(ZLjava/lang/Integer;Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;Lcom/squareup/cogs/Cogs;Lcom/squareup/badbus/BadBus;Lcom/squareup/analytics/Analytics;)V",
        "itemTypes",
        "",
        "Lcom/squareup/api/items/Item$Type;",
        "getItemTypes",
        "()Ljava/util/List;",
        "getObjectNumberLimit",
        "()Ljava/lang/Integer;",
        "Ljava/lang/Integer;",
        "getShouldShowCreateFromSearchButton",
        "()Z",
        "getTextBag",
        "()Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;",
        "handlePressingCreateButton",
        "Lcom/squareup/workflow/WorkflowAction;",
        "Lcom/squareup/ui/items/DetailSearchableListState;",
        "Lcom/squareup/ui/items/DetailSearchableListResult;",
        "currentState",
        "Lcom/squareup/ui/items/DetailSearchableListState$ShowList;",
        "event",
        "Lcom/squareup/ui/items/DetailSearchableListScreen$Event$CreateButtonPressed;",
        "handleSelectingObjectFromList",
        "Lcom/squareup/ui/items/DetailSearchableListScreen$Event$ObjectSelected;",
        "list",
        "Lio/reactivex/Observable;",
        "Lcom/squareup/ui/items/DetailSearchableListData;",
        "searchTerm",
        "",
        "items-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final badBus:Lcom/squareup/badbus/BadBus;

.field private final cogs:Lcom/squareup/cogs/Cogs;

.field private final objectNumberLimit:Ljava/lang/Integer;

.field private final shouldShowCreateFromSearchButton:Z

.field private final textBag:Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;


# direct methods
.method public constructor <init>(ZLjava/lang/Integer;Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;Lcom/squareup/cogs/Cogs;Lcom/squareup/badbus/BadBus;Lcom/squareup/analytics/Analytics;)V
    .locals 1

    const-string/jumbo v0, "textBag"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cogs"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "badBus"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/squareup/ui/items/DetailSearchableListServiceBehavior;->shouldShowCreateFromSearchButton:Z

    iput-object p2, p0, Lcom/squareup/ui/items/DetailSearchableListServiceBehavior;->objectNumberLimit:Ljava/lang/Integer;

    iput-object p3, p0, Lcom/squareup/ui/items/DetailSearchableListServiceBehavior;->textBag:Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;

    iput-object p4, p0, Lcom/squareup/ui/items/DetailSearchableListServiceBehavior;->cogs:Lcom/squareup/cogs/Cogs;

    iput-object p5, p0, Lcom/squareup/ui/items/DetailSearchableListServiceBehavior;->badBus:Lcom/squareup/badbus/BadBus;

    iput-object p6, p0, Lcom/squareup/ui/items/DetailSearchableListServiceBehavior;->analytics:Lcom/squareup/analytics/Analytics;

    return-void
.end method


# virtual methods
.method public getItemTypes()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Item$Type;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/squareup/api/items/Item$Type;

    .line 31
    sget-object v1, Lcom/squareup/api/items/Item$Type;->APPOINTMENTS_SERVICE:Lcom/squareup/api/items/Item$Type;

    const/4 v2, 0x0

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    const-string v1, "asList(APPOINTMENTS_SERVICE)"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkExpressionValueIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public getObjectNumberLimit()Ljava/lang/Integer;
    .locals 1

    .line 23
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListServiceBehavior;->objectNumberLimit:Ljava/lang/Integer;

    return-object v0
.end method

.method public getShouldShowCreateFromSearchButton()Z
    .locals 1

    .line 22
    iget-boolean v0, p0, Lcom/squareup/ui/items/DetailSearchableListServiceBehavior;->shouldShowCreateFromSearchButton:Z

    return v0
.end method

.method public getTextBag()Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;
    .locals 1

    .line 24
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListServiceBehavior;->textBag:Lcom/squareup/ui/items/DetailSearchableListScreenTextBag;

    return-object v0
.end method

.method public handlePressingCreateButton(Lcom/squareup/ui/items/DetailSearchableListState$ShowList;Lcom/squareup/ui/items/DetailSearchableListScreen$Event$CreateButtonPressed;)Lcom/squareup/workflow/WorkflowAction;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/items/DetailSearchableListState$ShowList;",
            "Lcom/squareup/ui/items/DetailSearchableListScreen$Event$CreateButtonPressed;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/items/DetailSearchableListState;",
            "Lcom/squareup/ui/items/DetailSearchableListResult;",
            ">;"
        }
    .end annotation

    const-string v0, "currentState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "event"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 37
    sget-object p2, Lcom/squareup/ui/items/DetailSearchableListResult$GoToCreateService;->INSTANCE:Lcom/squareup/ui/items/DetailSearchableListResult$GoToCreateService;

    .line 36
    invoke-virtual {p1, p2}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1
.end method

.method public handleSelectingObjectFromList(Lcom/squareup/ui/items/DetailSearchableListState$ShowList;Lcom/squareup/ui/items/DetailSearchableListScreen$Event$ObjectSelected;)Lcom/squareup/workflow/WorkflowAction;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/items/DetailSearchableListState$ShowList;",
            "Lcom/squareup/ui/items/DetailSearchableListScreen$Event$ObjectSelected;",
            ")",
            "Lcom/squareup/workflow/WorkflowAction<",
            "Lcom/squareup/ui/items/DetailSearchableListState;",
            "Lcom/squareup/ui/items/DetailSearchableListResult;",
            ">;"
        }
    .end annotation

    const-string v0, "currentState"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "event"

    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    sget-object p1, Lcom/squareup/workflow/WorkflowAction;->Companion:Lcom/squareup/workflow/WorkflowAction$Companion;

    .line 45
    new-instance v0, Lcom/squareup/ui/items/DetailSearchableListResult$GoToEditService;

    .line 46
    invoke-virtual {p2}, Lcom/squareup/ui/items/DetailSearchableListScreen$Event$ObjectSelected;->getSelectedObject()Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;->getId()Ljava/lang/String;

    move-result-object v1

    .line 47
    invoke-virtual {p2}, Lcom/squareup/ui/items/DetailSearchableListScreen$Event$ObjectSelected;->getSelectedObject()Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject;

    move-result-object p2

    if-eqz p2, :cond_0

    check-cast p2, Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Service;

    invoke-virtual {p2}, Lcom/squareup/ui/items/DetailSearchableListState$DetailSearchableObject$Service;->getService()Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;

    move-result-object p2

    invoke-virtual {p2}, Lcom/squareup/shared/catalog/synthetictables/LibraryEntry;->getImageUrl()Ljava/lang/String;

    move-result-object p2

    .line 45
    invoke-direct {v0, v1, p2}, Lcom/squareup/ui/items/DetailSearchableListResult$GoToEditService;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    invoke-virtual {p1, v0}, Lcom/squareup/workflow/WorkflowAction$Companion;->emitOutput(Ljava/lang/Object;)Lcom/squareup/workflow/WorkflowAction;

    move-result-object p1

    return-object p1

    .line 47
    :cond_0
    new-instance p1, Lkotlin/TypeCastException;

    const-string p2, "null cannot be cast to non-null type com.squareup.ui.items.DetailSearchableListState.DetailSearchableObject.Service"

    invoke-direct {p1, p2}, Lkotlin/TypeCastException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public list(Ljava/lang/String;)Lio/reactivex/Observable;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Observable<",
            "Lcom/squareup/ui/items/DetailSearchableListData;",
            ">;"
        }
    .end annotation

    const-string v0, "searchTerm"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    sget-object v1, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper;->Companion:Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion;

    .line 53
    iget-object v2, p0, Lcom/squareup/ui/items/DetailSearchableListServiceBehavior;->cogs:Lcom/squareup/cogs/Cogs;

    .line 54
    iget-object v3, p0, Lcom/squareup/ui/items/DetailSearchableListServiceBehavior;->badBus:Lcom/squareup/badbus/BadBus;

    .line 55
    sget-object v4, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ITEM:Lcom/squareup/shared/catalog/models/CatalogObjectType;

    .line 57
    iget-object v6, p0, Lcom/squareup/ui/items/DetailSearchableListServiceBehavior;->analytics:Lcom/squareup/analytics/Analytics;

    .line 58
    invoke-virtual {p0}, Lcom/squareup/ui/items/DetailSearchableListServiceBehavior;->getItemTypes()Ljava/util/List;

    move-result-object v7

    const/4 v8, 0x0

    const/16 v9, 0x40

    const/4 v10, 0x0

    move-object v5, p1

    .line 52
    invoke-static/range {v1 .. v10}, Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion;->listCogsObjects$default(Lcom/squareup/ui/items/DetailSearchableListCogsObjectListHelper$Companion;Lcom/squareup/cogs/Cogs;Lcom/squareup/badbus/BadBus;Lcom/squareup/shared/catalog/models/CatalogObjectType;Ljava/lang/String;Lcom/squareup/analytics/Analytics;Ljava/util/List;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lio/reactivex/Observable;

    move-result-object p1

    return-object p1
.end method
