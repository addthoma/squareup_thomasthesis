.class final Lcom/squareup/ui/items/EditServiceAssignedEmployeesCoordinator$populateStaffView$2;
.super Ljava/lang/Object;
.source "EditServiceAssignedEmployeesCoordinator.kt"

# interfaces
.implements Lcom/squareup/noho/NohoCheckableGroup$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/EditServiceAssignedEmployeesCoordinator;->populateStaffView(Landroid/content/Context;Lcom/squareup/ui/items/EditItemScopeRunner$StaffItemState;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0018\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0010\u0008\n\u0002\u0008\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u00032\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0006H\n\u00a2\u0006\u0002\u0008\u0008"
    }
    d2 = {
        "<anonymous>",
        "",
        "<anonymous parameter 0>",
        "Lcom/squareup/noho/NohoCheckableGroup;",
        "kotlin.jvm.PlatformType",
        "<anonymous parameter 1>",
        "",
        "<anonymous parameter 2>",
        "onCheckedChanged"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/items/EditServiceAssignedEmployeesCoordinator;


# direct methods
.method constructor <init>(Lcom/squareup/ui/items/EditServiceAssignedEmployeesCoordinator;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/items/EditServiceAssignedEmployeesCoordinator$populateStaffView$2;->this$0:Lcom/squareup/ui/items/EditServiceAssignedEmployeesCoordinator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCheckedChanged(Lcom/squareup/noho/NohoCheckableGroup;II)V
    .locals 0

    .line 56
    iget-object p1, p0, Lcom/squareup/ui/items/EditServiceAssignedEmployeesCoordinator$populateStaffView$2;->this$0:Lcom/squareup/ui/items/EditServiceAssignedEmployeesCoordinator;

    invoke-static {p1}, Lcom/squareup/ui/items/EditServiceAssignedEmployeesCoordinator;->access$saveAssignedEmployees(Lcom/squareup/ui/items/EditServiceAssignedEmployeesCoordinator;)V

    return-void
.end method
