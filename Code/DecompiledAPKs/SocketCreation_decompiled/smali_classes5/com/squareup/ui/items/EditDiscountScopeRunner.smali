.class public Lcom/squareup/ui/items/EditDiscountScopeRunner;
.super Ljava/lang/Object;
.source "EditDiscountScopeRunner.java"

# interfaces
.implements Lmortar/Scoped;


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final flow:Lflow/Flow;


# direct methods
.method public constructor <init>(Lcom/squareup/analytics/Analytics;Lflow/Flow;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/squareup/ui/items/EditDiscountScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    .line 22
    iput-object p2, p0, Lcom/squareup/ui/items/EditDiscountScopeRunner;->flow:Lflow/Flow;

    return-void
.end method


# virtual methods
.method discardDiscountUnsavedChanges()V
    .locals 4

    .line 32
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ITEMS_APPLET_CONFIRM_DISCARD_CHANGES:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    .line 33
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountScopeRunner;->flow:Lflow/Flow;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Class;

    const-class v2, Lcom/squareup/ui/items/ConfirmDiscardDiscountChangesDialogScreen;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-class v2, Lcom/squareup/ui/items/EditDiscountScreen;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/squareup/container/Flows;->goBackPast(Lflow/Flow;[Ljava/lang/Class;)V

    return-void
.end method

.method public onEnterScope(Lmortar/MortarScope;)V
    .locals 0

    return-void
.end method

.method public onExitScope()V
    .locals 0

    return-void
.end method

.method resumeEditing()V
    .locals 2

    .line 38
    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountScopeRunner;->analytics:Lcom/squareup/analytics/Analytics;

    sget-object v1, Lcom/squareup/analytics/RegisterTapName;->ITEMS_APPLET_CANCEL_DISCARD_CHANGES:Lcom/squareup/analytics/RegisterTapName;

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logTap(Lcom/squareup/analytics/RegisterTapName;)V

    return-void
.end method
