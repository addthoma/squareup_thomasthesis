.class public Lcom/squareup/ui/items/NewCategoryNameView;
.super Landroid/widget/LinearLayout;
.source "NewCategoryNameView.java"

# interfaces
.implements Lcom/squareup/workflow/ui/HandlesBack;


# instance fields
.field private categoryEditText:Landroid/widget/EditText;

.field presenter:Lcom/squareup/ui/items/NewCategoryNameScreen$Presenter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .line 22
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    invoke-virtual {p0}, Lcom/squareup/ui/items/NewCategoryNameView;->getContext()Landroid/content/Context;

    move-result-object p1

    const-class p2, Lcom/squareup/ui/items/NewCategoryNameScreen$Component;

    invoke-static {p1, p2}, Lcom/squareup/dagger/Components;->component(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/items/NewCategoryNameScreen$Component;

    invoke-interface {p1, p0}, Lcom/squareup/ui/items/NewCategoryNameScreen$Component;->inject(Lcom/squareup/ui/items/NewCategoryNameView;)V

    return-void
.end method

.method private bindViews()V
    .locals 1

    .line 50
    sget v0, Lcom/squareup/edititem/R$id;->category_name:I

    invoke-static {p0, v0}, Lcom/squareup/util/Views;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/squareup/ui/items/NewCategoryNameView;->categoryEditText:Landroid/widget/EditText;

    return-void
.end method


# virtual methods
.method public onBackPressed()Z
    .locals 1

    .line 44
    iget-object v0, p0, Lcom/squareup/ui/items/NewCategoryNameView;->presenter:Lcom/squareup/ui/items/NewCategoryNameScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/items/NewCategoryNameScreen$Presenter;->saveCategory()V

    .line 45
    iget-object v0, p0, Lcom/squareup/ui/items/NewCategoryNameView;->presenter:Lcom/squareup/ui/items/NewCategoryNameScreen$Presenter;

    invoke-virtual {v0}, Lcom/squareup/ui/items/NewCategoryNameScreen$Presenter;->finish()V

    const/4 v0, 0x1

    return v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .line 39
    iget-object v0, p0, Lcom/squareup/ui/items/NewCategoryNameView;->presenter:Lcom/squareup/ui/items/NewCategoryNameScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/items/NewCategoryNameScreen$Presenter;->dropView(Ljava/lang/Object;)V

    .line 40
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .line 27
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 28
    invoke-direct {p0}, Lcom/squareup/ui/items/NewCategoryNameView;->bindViews()V

    .line 29
    iget-object v0, p0, Lcom/squareup/ui/items/NewCategoryNameView;->categoryEditText:Landroid/widget/EditText;

    new-instance v1, Lcom/squareup/ui/items/NewCategoryNameView$1;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/NewCategoryNameView$1;-><init>(Lcom/squareup/ui/items/NewCategoryNameView;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 35
    iget-object v0, p0, Lcom/squareup/ui/items/NewCategoryNameView;->presenter:Lcom/squareup/ui/items/NewCategoryNameScreen$Presenter;

    invoke-virtual {v0, p0}, Lcom/squareup/ui/items/NewCategoryNameScreen$Presenter;->takeView(Ljava/lang/Object;)V

    return-void
.end method
