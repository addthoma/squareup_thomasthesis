.class final Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$showHomeScreenInEditModeFromItemsApplet$1;
.super Ljava/lang/Object;
.source "DetailSearchableListWorkflowRunner.kt"

# interfaces
.implements Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;->showHomeScreenInEditModeFromItemsApplet()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = null
.end annotation

.annotation system Ldalvik/annotation/SourceDebugExtension;
    value = "SMAP\nDetailSearchableListWorkflowRunner.kt\nKotlin\n*S Kotlin\n*F\n+ 1 DetailSearchableListWorkflowRunner.kt\ncom/squareup/ui/items/DetailSearchableListWorkflowRunner$showHomeScreenInEditModeFromItemsApplet$1\n*L\n1#1,267:1\n*E\n"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n\u00a2\u0006\u0002\u0008\u0005"
    }
    d2 = {
        "<anonymous>",
        "Lcom/squareup/container/Command;",
        "kotlin.jvm.PlatformType",
        "history",
        "Lflow/History;",
        "call"
    }
    k = 0x3
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;


# direct methods
.method constructor <init>(Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$showHomeScreenInEditModeFromItemsApplet$1;->this$0:Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call(Lflow/History;)Lcom/squareup/container/Command;
    .locals 5

    const-string v0, "history"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 148
    invoke-virtual {p1}, Lflow/History;->buildUpon()Lflow/History$Builder;

    move-result-object p1

    .line 150
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    .line 151
    invoke-virtual {p1}, Lflow/History$Builder;->peek()Ljava/lang/Object;

    move-result-object v1

    .line 152
    :goto_0
    invoke-static {v1}, Lcom/squareup/ui/items/RealItemsAppletGatewayKt;->isInItemsAppletScope(Ljava/lang/Object;)Z

    move-result v2

    const/4 v3, 0x1

    if-eqz v2, :cond_2

    .line 153
    invoke-virtual {p1}, Lflow/History$Builder;->pop()Ljava/lang/Object;

    move-result-object v2

    .line 154
    instance-of v4, v2, Lcom/squareup/ui/items/ItemsAppletMasterScreen;

    if-eqz v4, :cond_1

    .line 155
    invoke-virtual {p1}, Lflow/History$Builder;->peek()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/ui/items/RealItemsAppletGatewayKt;->isInItemsAppletScope(Ljava/lang/Object;)Z

    move-result v1

    xor-int/2addr v1, v3

    if-eqz v1, :cond_0

    goto :goto_1

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "There must be nothing in ItemsAppletPath below the ItemsAppletMasterScreen."

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 161
    :cond_1
    invoke-virtual {v0, v2}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    const/4 v3, 0x0

    .line 164
    :goto_1
    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    if-eqz v3, :cond_4

    .line 174
    :goto_2
    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 175
    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p1, v1}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    goto :goto_2

    .line 179
    :cond_3
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$showHomeScreenInEditModeFromItemsApplet$1;->this$0:Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;

    invoke-static {v0}, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;->access$getOrderEntryAppletGateway$p(Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;)Lcom/squareup/orderentry/OrderEntryAppletGateway;

    move-result-object v0

    invoke-virtual {p1}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/orderentry/OrderEntryAppletGateway;->historyForFavoritesEditor(Lflow/History;)Lflow/History;

    move-result-object p1

    .line 181
    sget-object v0, Lflow/Direction;->REPLACE:Lflow/Direction;

    invoke-static {p1, v0}, Lcom/squareup/container/Command;->setHistory(Lflow/History;Lflow/Direction;)Lcom/squareup/container/Command;

    move-result-object p1

    return-object p1

    .line 169
    :cond_4
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "We expect the ItemsAppletMasterScreen below all keys in ItemsAppletPath."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1

    .line 165
    :cond_5
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "The current screen must be in ItemsAppletPath."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    check-cast p1, Ljava/lang/Throwable;

    throw p1
.end method
