.class public final Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$Factory;
.super Ljava/lang/Object;
.source "DetailSearchableListWorkflowRunner.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Factory"
.end annotation

.annotation runtime Lkotlin/Metadata;
    bv = {
        0x1,
        0x0,
        0x3
    }
    d1 = {
        "\u0000f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0008\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001Bo\u0008\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\u0008\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\u000c\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0013\u0012\u0006\u0010\u0014\u001a\u00020\u0015\u0012\u0006\u0010\u0016\u001a\u00020\u0017\u0012\u0006\u0010\u0018\u001a\u00020\u0019\u0012\u0006\u0010\u001a\u001a\u00020\u001b\u00a2\u0006\u0002\u0010\u001cJ\u000e\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020 R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0008\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000c\u001a\u00020\rX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006!"
    }
    d2 = {
        "Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$Factory;",
        "",
        "factoryOfViewFactory",
        "Lcom/squareup/ui/items/DetailSearchableListViewFactory$Factory;",
        "container",
        "Lcom/squareup/ui/main/PosContainer;",
        "workflow",
        "Lcom/squareup/ui/items/DetailSearchableListWorkflow;",
        "flow",
        "Lflow/Flow;",
        "orderEntryAppletGateway",
        "Lcom/squareup/orderentry/OrderEntryAppletGateway;",
        "permissionGatekeeper",
        "Lcom/squareup/permissions/PermissionGatekeeper;",
        "editItemGateway",
        "Lcom/squareup/ui/items/EditItemGateway;",
        "analytics",
        "Lcom/squareup/analytics/Analytics;",
        "features",
        "Lcom/squareup/settings/server/Features;",
        "itemsAppletScopeRunner",
        "Lcom/squareup/ui/items/ItemsAppletScopeRunner;",
        "cogs",
        "Lcom/squareup/cogs/Cogs;",
        "convertItemsUrlHelper",
        "Lcom/squareup/ui/items/ConvertItemsUrlHelper;",
        "browserLauncher",
        "Lcom/squareup/util/BrowserLauncher;",
        "(Lcom/squareup/ui/items/DetailSearchableListViewFactory$Factory;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/ui/items/DetailSearchableListWorkflow;Lflow/Flow;Lcom/squareup/orderentry/OrderEntryAppletGateway;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/items/EditItemGateway;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/items/ItemsAppletScopeRunner;Lcom/squareup/cogs/Cogs;Lcom/squareup/ui/items/ConvertItemsUrlHelper;Lcom/squareup/util/BrowserLauncher;)V",
        "build",
        "Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;",
        "dataType",
        "Lcom/squareup/ui/items/DetailSearchableListDataType;",
        "items-applet_release"
    }
    k = 0x1
    mv = {
        0x1,
        0x1,
        0x10
    }
.end annotation


# instance fields
.field private final analytics:Lcom/squareup/analytics/Analytics;

.field private final browserLauncher:Lcom/squareup/util/BrowserLauncher;

.field private final cogs:Lcom/squareup/cogs/Cogs;

.field private final container:Lcom/squareup/ui/main/PosContainer;

.field private final convertItemsUrlHelper:Lcom/squareup/ui/items/ConvertItemsUrlHelper;

.field private final editItemGateway:Lcom/squareup/ui/items/EditItemGateway;

.field private final factoryOfViewFactory:Lcom/squareup/ui/items/DetailSearchableListViewFactory$Factory;

.field private final features:Lcom/squareup/settings/server/Features;

.field private final flow:Lflow/Flow;

.field private final itemsAppletScopeRunner:Lcom/squareup/ui/items/ItemsAppletScopeRunner;

.field private final orderEntryAppletGateway:Lcom/squareup/orderentry/OrderEntryAppletGateway;

.field private final permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

.field private final workflow:Lcom/squareup/ui/items/DetailSearchableListWorkflow;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/items/DetailSearchableListViewFactory$Factory;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/ui/items/DetailSearchableListWorkflow;Lflow/Flow;Lcom/squareup/orderentry/OrderEntryAppletGateway;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/items/EditItemGateway;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/items/ItemsAppletScopeRunner;Lcom/squareup/cogs/Cogs;Lcom/squareup/ui/items/ConvertItemsUrlHelper;Lcom/squareup/util/BrowserLauncher;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    const-string v0, "factoryOfViewFactory"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "container"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string/jumbo v0, "workflow"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "flow"

    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "orderEntryAppletGateway"

    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "permissionGatekeeper"

    invoke-static {p6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "editItemGateway"

    invoke-static {p7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "analytics"

    invoke-static {p8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "features"

    invoke-static {p9, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "itemsAppletScopeRunner"

    invoke-static {p10, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "cogs"

    invoke-static {p11, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "convertItemsUrlHelper"

    invoke-static {p12, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "browserLauncher"

    invoke-static {p13, v0}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 245
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$Factory;->factoryOfViewFactory:Lcom/squareup/ui/items/DetailSearchableListViewFactory$Factory;

    iput-object p2, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$Factory;->container:Lcom/squareup/ui/main/PosContainer;

    iput-object p3, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$Factory;->workflow:Lcom/squareup/ui/items/DetailSearchableListWorkflow;

    iput-object p4, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$Factory;->flow:Lflow/Flow;

    iput-object p5, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$Factory;->orderEntryAppletGateway:Lcom/squareup/orderentry/OrderEntryAppletGateway;

    iput-object p6, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$Factory;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    iput-object p7, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$Factory;->editItemGateway:Lcom/squareup/ui/items/EditItemGateway;

    iput-object p8, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$Factory;->analytics:Lcom/squareup/analytics/Analytics;

    iput-object p9, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$Factory;->features:Lcom/squareup/settings/server/Features;

    iput-object p10, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$Factory;->itemsAppletScopeRunner:Lcom/squareup/ui/items/ItemsAppletScopeRunner;

    iput-object p11, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$Factory;->cogs:Lcom/squareup/cogs/Cogs;

    iput-object p12, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$Factory;->convertItemsUrlHelper:Lcom/squareup/ui/items/ConvertItemsUrlHelper;

    iput-object p13, p0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$Factory;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    return-void
.end method


# virtual methods
.method public final build(Lcom/squareup/ui/items/DetailSearchableListDataType;)Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;
    .locals 18

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    const-string v2, "dataType"

    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkParameterIsNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 260
    new-instance v2, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;

    .line 261
    iget-object v3, v0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$Factory;->factoryOfViewFactory:Lcom/squareup/ui/items/DetailSearchableListViewFactory$Factory;

    invoke-virtual {v3, v1}, Lcom/squareup/ui/items/DetailSearchableListViewFactory$Factory;->build(Lcom/squareup/ui/items/DetailSearchableListDataType;)Lcom/squareup/ui/items/DetailSearchableListViewFactory;

    move-result-object v4

    iget-object v5, v0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$Factory;->container:Lcom/squareup/ui/main/PosContainer;

    iget-object v6, v0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$Factory;->workflow:Lcom/squareup/ui/items/DetailSearchableListWorkflow;

    iget-object v7, v0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$Factory;->flow:Lflow/Flow;

    iget-object v8, v0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$Factory;->orderEntryAppletGateway:Lcom/squareup/orderentry/OrderEntryAppletGateway;

    .line 262
    iget-object v9, v0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$Factory;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    iget-object v10, v0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$Factory;->editItemGateway:Lcom/squareup/ui/items/EditItemGateway;

    iget-object v11, v0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$Factory;->analytics:Lcom/squareup/analytics/Analytics;

    iget-object v12, v0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$Factory;->features:Lcom/squareup/settings/server/Features;

    iget-object v13, v0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$Factory;->itemsAppletScopeRunner:Lcom/squareup/ui/items/ItemsAppletScopeRunner;

    iget-object v14, v0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$Factory;->cogs:Lcom/squareup/cogs/Cogs;

    .line 263
    iget-object v15, v0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$Factory;->convertItemsUrlHelper:Lcom/squareup/ui/items/ConvertItemsUrlHelper;

    iget-object v1, v0, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner$Factory;->browserLauncher:Lcom/squareup/util/BrowserLauncher;

    const/16 v17, 0x0

    move-object v3, v2

    move-object/from16 v16, v1

    .line 260
    invoke-direct/range {v3 .. v17}, Lcom/squareup/ui/items/DetailSearchableListWorkflowRunner;-><init>(Lcom/squareup/ui/items/DetailSearchableListViewFactory;Lcom/squareup/ui/main/PosContainer;Lcom/squareup/ui/items/DetailSearchableListWorkflow;Lflow/Flow;Lcom/squareup/orderentry/OrderEntryAppletGateway;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/items/EditItemGateway;Lcom/squareup/analytics/Analytics;Lcom/squareup/settings/server/Features;Lcom/squareup/ui/items/ItemsAppletScopeRunner;Lcom/squareup/cogs/Cogs;Lcom/squareup/ui/items/ConvertItemsUrlHelper;Lcom/squareup/util/BrowserLauncher;Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-object v2
.end method
