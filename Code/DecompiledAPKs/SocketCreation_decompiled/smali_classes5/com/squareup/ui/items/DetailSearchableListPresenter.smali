.class public abstract Lcom/squareup/ui/items/DetailSearchableListPresenter;
.super Lmortar/ViewPresenter;
.source "DetailSearchableListPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/squareup/ui/items/DetailSearchableListPresenter$ItemsQueryCallback;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/squareup/ui/items/DetailSearchableListView;",
        ">",
        "Lmortar/ViewPresenter<",
        "TT;>;"
    }
.end annotation


# instance fields
.field private actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

.field protected final analytics:Lcom/squareup/analytics/Analytics;

.field protected final cogs:Lcom/squareup/cogs/Cogs;

.field private final device:Lcom/squareup/util/Device;

.field protected final editItemGateway:Lcom/squareup/ui/items/EditItemGateway;

.field protected final flow:Lflow/Flow;

.field protected itemCursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

.field protected final itemsAppletScopeRunner:Lcom/squareup/ui/items/ItemsAppletScopeRunner;

.field protected final orderEntryAppletGateway:Lcom/squareup/orderentry/OrderEntryAppletGateway;

.field private final permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

.field private queryCallback:Lcom/squareup/ui/items/DetailSearchableListPresenter$ItemsQueryCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/squareup/ui/items/DetailSearchableListPresenter<",
            "TT;>.ItemsQueryCallback;"
        }
    .end annotation
.end field

.field protected final res:Lcom/squareup/util/Res;


# direct methods
.method constructor <init>(Lcom/squareup/util/Res;Lcom/squareup/cogs/Cogs;Lcom/squareup/util/Device;Lflow/Flow;Lcom/squareup/analytics/Analytics;Lcom/squareup/permissions/PermissionGatekeeper;Lcom/squareup/ui/items/ItemsAppletScopeRunner;Lcom/squareup/ui/items/EditItemGateway;Lcom/squareup/orderentry/OrderEntryAppletGateway;)V
    .locals 0

    .line 88
    invoke-direct {p0}, Lmortar/ViewPresenter;-><init>()V

    .line 89
    iput-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListPresenter;->res:Lcom/squareup/util/Res;

    .line 90
    iput-object p2, p0, Lcom/squareup/ui/items/DetailSearchableListPresenter;->cogs:Lcom/squareup/cogs/Cogs;

    .line 91
    iput-object p3, p0, Lcom/squareup/ui/items/DetailSearchableListPresenter;->device:Lcom/squareup/util/Device;

    .line 92
    iput-object p4, p0, Lcom/squareup/ui/items/DetailSearchableListPresenter;->flow:Lflow/Flow;

    .line 93
    iput-object p5, p0, Lcom/squareup/ui/items/DetailSearchableListPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    .line 94
    iput-object p6, p0, Lcom/squareup/ui/items/DetailSearchableListPresenter;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    .line 95
    iput-object p7, p0, Lcom/squareup/ui/items/DetailSearchableListPresenter;->itemsAppletScopeRunner:Lcom/squareup/ui/items/ItemsAppletScopeRunner;

    .line 96
    iput-object p8, p0, Lcom/squareup/ui/items/DetailSearchableListPresenter;->editItemGateway:Lcom/squareup/ui/items/EditItemGateway;

    .line 97
    iput-object p9, p0, Lcom/squareup/ui/items/DetailSearchableListPresenter;->orderEntryAppletGateway:Lcom/squareup/orderentry/OrderEntryAppletGateway;

    return-void
.end method

.method static synthetic access$000(Lcom/squareup/ui/items/DetailSearchableListPresenter;)Ljava/lang/Object;
    .locals 0

    .line 38
    invoke-virtual {p0}, Lcom/squareup/ui/items/DetailSearchableListPresenter;->getView()Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method static synthetic access$100(Lcom/squareup/ui/items/DetailSearchableListPresenter;Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;)V
    .locals 0

    .line 38
    invoke-direct {p0, p1}, Lcom/squareup/ui/items/DetailSearchableListPresenter;->setItemCursor(Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;)V

    return-void
.end method

.method static synthetic access$202(Lcom/squareup/ui/items/DetailSearchableListPresenter;Lcom/squareup/ui/items/DetailSearchableListPresenter$ItemsQueryCallback;)Lcom/squareup/ui/items/DetailSearchableListPresenter$ItemsQueryCallback;
    .locals 0

    .line 38
    iput-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListPresenter;->queryCallback:Lcom/squareup/ui/items/DetailSearchableListPresenter$ItemsQueryCallback;

    return-object p1
.end method

.method static synthetic access$300(Lcom/squareup/ui/items/DetailSearchableListPresenter;)V
    .locals 0

    .line 38
    invoke-direct {p0}, Lcom/squareup/ui/items/DetailSearchableListPresenter;->showHomeScreenInEditModeFromItemsApplet()V

    return-void
.end method

.method private cancelQueryCallback()V
    .locals 2

    .line 319
    invoke-direct {p0}, Lcom/squareup/ui/items/DetailSearchableListPresenter;->hasPendingQuery()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "[Items_Detail] Query callback canceled."

    .line 320
    invoke-static {v1, v0}, Ltimber/log/Timber;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 321
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListPresenter;->queryCallback:Lcom/squareup/ui/items/DetailSearchableListPresenter$ItemsQueryCallback;

    invoke-virtual {v0}, Lcom/squareup/ui/items/DetailSearchableListPresenter$ItemsQueryCallback;->cancel()V

    const/4 v0, 0x0

    .line 322
    iput-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListPresenter;->queryCallback:Lcom/squareup/ui/items/DetailSearchableListPresenter$ItemsQueryCallback;

    :cond_0
    return-void
.end method

.method private endCursor()V
    .locals 1

    .line 335
    invoke-direct {p0}, Lcom/squareup/ui/items/DetailSearchableListPresenter;->hasCursor()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListPresenter;->itemCursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 336
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListPresenter;->itemCursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->close()V

    const/4 v0, 0x0

    .line 337
    iput-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListPresenter;->itemCursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    :cond_0
    return-void
.end method

.method private hasCursor()Z
    .locals 1

    .line 331
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListPresenter;->itemCursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private hasPendingQuery()Z
    .locals 1

    .line 327
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListPresenter;->queryCallback:Lcom/squareup/ui/items/DetailSearchableListPresenter$ItemsQueryCallback;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private setItemCursor(Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;)V
    .locals 1

    .line 267
    iput-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListPresenter;->itemCursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    .line 268
    invoke-virtual {p0}, Lcom/squareup/ui/items/DetailSearchableListPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/DetailSearchableListView;

    invoke-virtual {v0, p1}, Lcom/squareup/ui/items/DetailSearchableListView;->setCursor(Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;)V

    .line 269
    invoke-virtual {p0}, Lcom/squareup/ui/items/DetailSearchableListPresenter;->getView()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/squareup/ui/items/DetailSearchableListView;

    invoke-virtual {p1}, Lcom/squareup/ui/items/DetailSearchableListView;->hideSpinner()V

    return-void
.end method

.method private showHomeScreenInEditModeFromItemsApplet()V
    .locals 4

    .line 223
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListPresenter;->flow:Lflow/Flow;

    new-instance v1, Lcom/squareup/container/CalculatedKey;

    new-instance v2, Lcom/squareup/ui/items/-$$Lambda$DetailSearchableListPresenter$88CqY-Nh_ceR89cFceyFVW_ofk4;

    invoke-direct {v2, p0}, Lcom/squareup/ui/items/-$$Lambda$DetailSearchableListPresenter$88CqY-Nh_ceR89cFceyFVW_ofk4;-><init>(Lcom/squareup/ui/items/DetailSearchableListPresenter;)V

    const-string v3, "showHomeScreenInEditModeFromItemsApplet"

    invoke-direct {v1, v3, v2}, Lcom/squareup/container/CalculatedKey;-><init>(Ljava/lang/String;Lcom/squareup/container/CalculatedKey$HistoryToFlowCommand;)V

    invoke-virtual {v0, v1}, Lflow/Flow;->set(Ljava/lang/Object;)V

    return-void
.end method

.method private updateCursorFromCogs(Lcom/squareup/ui/items/DetailSearchableListPresenter$ItemsQueryCallback;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/ui/items/DetailSearchableListPresenter<",
            "TT;>.ItemsQueryCallback;)V"
        }
    .end annotation

    .line 189
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListPresenter;->cogs:Lcom/squareup/cogs/Cogs;

    new-instance v1, Lcom/squareup/ui/items/-$$Lambda$DetailSearchableListPresenter$TYvrYLOYQ3MKk6yL4_GrIyAQ5Dw;

    invoke-direct {v1, p0, p1}, Lcom/squareup/ui/items/-$$Lambda$DetailSearchableListPresenter$TYvrYLOYQ3MKk6yL4_GrIyAQ5Dw;-><init>(Lcom/squareup/ui/items/DetailSearchableListPresenter;Lcom/squareup/ui/items/DetailSearchableListPresenter$ItemsQueryCallback;)V

    invoke-interface {v0, v1}, Lcom/squareup/cogs/Cogs;->asSingle(Lcom/squareup/shared/catalog/CatalogTask;)Lrx/Single;

    move-result-object v0

    .line 192
    invoke-virtual {v0, p1}, Lrx/Single;->subscribe(Lrx/functions/Action1;)Lrx/Subscription;

    return-void
.end method


# virtual methods
.method protected buildLibraryCursorInBackground(Lcom/squareup/shared/catalog/Catalog$Local;Lcom/squareup/ui/items/DetailSearchableListPresenter$ItemsQueryCallback;)Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/squareup/shared/catalog/Catalog$Local;",
            "Lcom/squareup/ui/items/DetailSearchableListPresenter<",
            "TT;>.ItemsQueryCallback;)",
            "Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;"
        }
    .end annotation

    .line 198
    const-class v0, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;

    invoke-interface {p1, v0}, Lcom/squareup/shared/catalog/Catalog$Local;->getSyntheticTableReader(Ljava/lang/Class;)Lcom/squareup/shared/catalog/synthetictables/SyntheticTableReader;

    move-result-object p1

    check-cast p1, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;

    .line 199
    iget-object v0, p2, Lcom/squareup/ui/items/DetailSearchableListPresenter$ItemsQueryCallback;->searchText:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 201
    invoke-virtual {p0}, Lcom/squareup/ui/items/DetailSearchableListPresenter;->getCatalogObjectType()Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object p2

    .line 202
    invoke-virtual {p0}, Lcom/squareup/ui/items/DetailSearchableListPresenter;->getItemTypes()Ljava/util/List;

    move-result-object v0

    .line 201
    invoke-virtual {p1, p2, v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->readAllObjectsOfTypeFromLibraryTable(Lcom/squareup/shared/catalog/models/CatalogObjectType;Ljava/util/List;)Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    move-result-object p1

    goto :goto_0

    .line 204
    :cond_0
    iget-object v0, p2, Lcom/squareup/ui/items/DetailSearchableListPresenter$ItemsQueryCallback;->searchText:Ljava/lang/String;

    .line 205
    invoke-virtual {p0}, Lcom/squareup/ui/items/DetailSearchableListPresenter;->getCatalogObjectType()Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object v1

    invoke-virtual {p0}, Lcom/squareup/ui/items/DetailSearchableListPresenter;->getItemTypes()Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x0

    .line 204
    invoke-virtual {p1, v0, v1, v2, v3}, Lcom/squareup/shared/catalog/synthetictables/LibraryTableReader;->wordPrefixSearchByNameAndTypes(Ljava/lang/String;Lcom/squareup/shared/catalog/models/CatalogObjectType;Ljava/util/List;Z)Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    move-result-object p1

    .line 207
    sget-object v0, Lcom/squareup/ui/items/DetailSearchableListPresenter$2;->$SwitchMap$com$squareup$shared$catalog$models$CatalogObjectType:[I

    invoke-virtual {p0}, Lcom/squareup/ui/items/DetailSearchableListPresenter;->getCatalogObjectType()Lcom/squareup/shared/catalog/models/CatalogObjectType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/shared/catalog/models/CatalogObjectType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    goto :goto_0

    .line 215
    :cond_1
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/ui/items/log/DiscountSearchEvent;

    iget-object p2, p2, Lcom/squareup/ui/items/DetailSearchableListPresenter$ItemsQueryCallback;->searchText:Ljava/lang/String;

    invoke-direct {v1, p2}, Lcom/squareup/ui/items/log/DiscountSearchEvent;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    goto :goto_0

    .line 212
    :cond_2
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/ui/items/log/ModifierSearchEvent;

    iget-object p2, p2, Lcom/squareup/ui/items/DetailSearchableListPresenter$ItemsQueryCallback;->searchText:Ljava/lang/String;

    invoke-direct {v1, p2}, Lcom/squareup/ui/items/log/ModifierSearchEvent;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    goto :goto_0

    .line 209
    :cond_3
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListPresenter;->analytics:Lcom/squareup/analytics/Analytics;

    new-instance v1, Lcom/squareup/ui/items/log/CategorySearchEvent;

    iget-object p2, p2, Lcom/squareup/ui/items/DetailSearchableListPresenter$ItemsQueryCallback;->searchText:Ljava/lang/String;

    invoke-direct {v1, p2}, Lcom/squareup/ui/items/log/CategorySearchEvent;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/squareup/analytics/Analytics;->logEvent(Lcom/squareup/eventstream/v1/EventStreamEvent;)V

    :goto_0
    return-object p1
.end method

.method decorateCursor(Lcom/squareup/shared/catalog/Catalog$Local;Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;)Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;
    .locals 0

    return-object p2
.end method

.method protected forceRefresh()V
    .locals 2

    .line 298
    invoke-direct {p0}, Lcom/squareup/ui/items/DetailSearchableListPresenter;->hasPendingQuery()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 303
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/items/DetailSearchableListPresenter;->hasCursor()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 304
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListPresenter;->itemCursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getSearchFilter()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const-string v0, ""

    .line 306
    :goto_0
    new-instance v1, Lcom/squareup/ui/items/DetailSearchableListPresenter$ItemsQueryCallback;

    invoke-direct {v1, p0, v0}, Lcom/squareup/ui/items/DetailSearchableListPresenter$ItemsQueryCallback;-><init>(Lcom/squareup/ui/items/DetailSearchableListPresenter;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/squareup/ui/items/DetailSearchableListPresenter;->queryCallback:Lcom/squareup/ui/items/DetailSearchableListPresenter$ItemsQueryCallback;

    .line 307
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListPresenter;->queryCallback:Lcom/squareup/ui/items/DetailSearchableListPresenter$ItemsQueryCallback;

    invoke-direct {p0, v0}, Lcom/squareup/ui/items/DetailSearchableListPresenter;->updateCursorFromCogs(Lcom/squareup/ui/items/DetailSearchableListPresenter$ItemsQueryCallback;)V

    return-void
.end method

.method protected abstract getActionBarTitle()Ljava/lang/CharSequence;
.end method

.method abstract getButtonCount()I
.end method

.method abstract getButtonText(I)Ljava/lang/String;
.end method

.method abstract getCatalogObjectType()Lcom/squareup/shared/catalog/models/CatalogObjectType;
.end method

.method abstract getItemTypes()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/squareup/api/items/Item$Type;",
            ">;"
        }
    .end annotation
.end method

.method protected isNested()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public synthetic lambda$onLoad$0$DetailSearchableListPresenter()V
    .locals 3

    .line 114
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListPresenter;->permissionGatekeeper:Lcom/squareup/permissions/PermissionGatekeeper;

    sget-object v1, Lcom/squareup/permissions/Permission;->EDIT_ITEMS:Lcom/squareup/permissions/Permission;

    new-instance v2, Lcom/squareup/ui/items/DetailSearchableListPresenter$1;

    invoke-direct {v2, p0}, Lcom/squareup/ui/items/DetailSearchableListPresenter$1;-><init>(Lcom/squareup/ui/items/DetailSearchableListPresenter;)V

    invoke-virtual {v0, v1, v2}, Lcom/squareup/permissions/PermissionGatekeeper;->runWhenAccessGranted(Lcom/squareup/permissions/Permission;Lcom/squareup/permissions/PermissionGatekeeper$When;)V

    return-void
.end method

.method public synthetic lambda$showHomeScreenInEditModeFromItemsApplet$2$DetailSearchableListPresenter(Lflow/History;)Lcom/squareup/container/Command;
    .locals 3

    .line 226
    invoke-virtual {p1}, Lflow/History;->buildUpon()Lflow/History$Builder;

    move-result-object p1

    .line 228
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    .line 229
    :goto_0
    invoke-virtual {p1}, Lflow/History$Builder;->peek()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/ui/items/RealItemsAppletGatewayKt;->isInItemsAppletScope(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 230
    invoke-virtual {p1}, Lflow/History$Builder;->pop()Ljava/lang/Object;

    move-result-object v1

    .line 231
    instance-of v2, v1, Lcom/squareup/ui/items/ItemsAppletMasterScreen;

    if-eqz v2, :cond_1

    .line 232
    invoke-virtual {p1}, Lflow/History$Builder;->peek()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/ui/items/RealItemsAppletGatewayKt;->isInItemsAppletScope(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    goto :goto_1

    .line 233
    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "There must be nothing in ItemsAppletPath below the ItemsAppletMasterScreen."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 239
    :cond_1
    invoke-virtual {v0, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    .line 242
    :goto_1
    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_5

    if-eqz v1, :cond_4

    .line 251
    :goto_2
    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 252
    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p1, v1}, Lflow/History$Builder;->push(Ljava/lang/Object;)Lflow/History$Builder;

    goto :goto_2

    .line 255
    :cond_3
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListPresenter;->orderEntryAppletGateway:Lcom/squareup/orderentry/OrderEntryAppletGateway;

    .line 256
    invoke-virtual {p1}, Lflow/History$Builder;->build()Lflow/History;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/squareup/orderentry/OrderEntryAppletGateway;->historyForFavoritesEditor(Lflow/History;)Lflow/History;

    move-result-object p1

    .line 258
    sget-object v0, Lflow/Direction;->REPLACE:Lflow/Direction;

    invoke-static {p1, v0}, Lcom/squareup/container/Command;->setHistory(Lflow/History;Lflow/Direction;)Lcom/squareup/container/Command;

    move-result-object p1

    return-object p1

    .line 247
    :cond_4
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "We expect the ItemsAppletMasterScreen below all keys in ItemsAppletPath."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    .line 243
    :cond_5
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "The current screen must be in ItemsAppletPath."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public synthetic lambda$updateCursorFromCogs$1$DetailSearchableListPresenter(Lcom/squareup/ui/items/DetailSearchableListPresenter$ItemsQueryCallback;Lcom/squareup/shared/catalog/Catalog$Local;)Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;
    .locals 0

    .line 190
    invoke-virtual {p0, p2, p1}, Lcom/squareup/ui/items/DetailSearchableListPresenter;->buildLibraryCursorInBackground(Lcom/squareup/shared/catalog/Catalog$Local;Lcom/squareup/ui/items/DetailSearchableListPresenter$ItemsQueryCallback;)Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    move-result-object p1

    .line 191
    invoke-virtual {p0, p2, p1}, Lcom/squareup/ui/items/DetailSearchableListPresenter;->decorateCursor(Lcom/squareup/shared/catalog/Catalog$Local;Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;)Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    move-result-object p1

    return-object p1
.end method

.method abstract onButtonClicked(I)V
.end method

.method protected onExitScope()V
    .locals 0

    .line 151
    invoke-direct {p0}, Lcom/squareup/ui/items/DetailSearchableListPresenter;->cancelQueryCallback()V

    .line 152
    invoke-direct {p0}, Lcom/squareup/ui/items/DetailSearchableListPresenter;->endCursor()V

    return-void
.end method

.method protected onLoad(Landroid/os/Bundle;)V
    .locals 3

    .line 101
    invoke-super {p0, p1}, Lmortar/ViewPresenter;->onLoad(Landroid/os/Bundle;)V

    .line 103
    invoke-virtual {p0}, Lcom/squareup/ui/items/DetailSearchableListPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Lcom/squareup/marin/widgets/ActionBarView;->findIn(Landroid/view/View;)Lcom/squareup/marin/widgets/MarinActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListPresenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    .line 104
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListPresenter;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isPhoneOrPortraitLessThan10Inches()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/ui/items/DetailSearchableListPresenter;->isNested()Z

    move-result v0

    if-nez v0, :cond_0

    .line 105
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListPresenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    invoke-virtual {v0}, Lcom/squareup/marin/widgets/MarinActionBar;->hideUpButton()V

    goto :goto_0

    .line 107
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListPresenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    iget-object v1, p0, Lcom/squareup/ui/items/DetailSearchableListPresenter;->flow:Lflow/Flow;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    new-instance v2, Lcom/squareup/ui/items/-$$Lambda$sZX4UsMUUPBhpIptxMDlKtBJT0w;

    invoke-direct {v2, v1}, Lcom/squareup/ui/items/-$$Lambda$sZX4UsMUUPBhpIptxMDlKtBJT0w;-><init>(Lflow/Flow;)V

    invoke-virtual {v0, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->showUpButton(Ljava/lang/Runnable;)V

    .line 109
    :goto_0
    invoke-virtual {p0}, Lcom/squareup/ui/items/DetailSearchableListPresenter;->updateActionBarTitle()V

    .line 110
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListPresenter;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isTablet()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListPresenter;->orderEntryAppletGateway:Lcom/squareup/orderentry/OrderEntryAppletGateway;

    invoke-interface {v0}, Lcom/squareup/orderentry/OrderEntryAppletGateway;->hasFavoritesEditor()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 111
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListPresenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setSecondaryButtonEnabled(Z)V

    .line 112
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListPresenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    iget-object v1, p0, Lcom/squareup/ui/items/DetailSearchableListPresenter;->res:Lcom/squareup/util/Res;

    sget v2, Lcom/squareup/itemsapplet/R$string;->items_applet_setup_item_grid:I

    invoke-interface {v1, v2}, Lcom/squareup/util/Res;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setSecondaryButtonTextNoGlyph(Ljava/lang/CharSequence;)V

    .line 113
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListPresenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    new-instance v1, Lcom/squareup/ui/items/-$$Lambda$DetailSearchableListPresenter$AD_LQzqaUtpUJqRV-pSm6joDD2M;

    invoke-direct {v1, p0}, Lcom/squareup/ui/items/-$$Lambda$DetailSearchableListPresenter$AD_LQzqaUtpUJqRV-pSm6joDD2M;-><init>(Lcom/squareup/ui/items/DetailSearchableListPresenter;)V

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->showSecondaryButton(Ljava/lang/Runnable;)V

    .line 121
    :cond_1
    invoke-direct {p0}, Lcom/squareup/ui/items/DetailSearchableListPresenter;->hasCursor()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 123
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListPresenter;->itemCursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-direct {p0, v0}, Lcom/squareup/ui/items/DetailSearchableListPresenter;->setItemCursor(Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;)V

    :cond_2
    if-nez p1, :cond_3

    const-string p1, ""

    .line 130
    invoke-virtual {p0, p1}, Lcom/squareup/ui/items/DetailSearchableListPresenter;->onTextSearched(Ljava/lang/String;)V

    :cond_3
    return-void
.end method

.method onProgressHidden()V
    .locals 1

    .line 315
    invoke-virtual {p0}, Lcom/squareup/ui/items/DetailSearchableListPresenter;->getView()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/ui/items/DetailSearchableListView;

    invoke-virtual {v0}, Lcom/squareup/ui/items/DetailSearchableListView;->showList()V

    return-void
.end method

.method abstract onRowClicked(I)V
.end method

.method onTextSearched(Ljava/lang/String;)V
    .locals 1

    const-string v0, "filter"

    .line 277
    invoke-static {p1, v0}, Lcom/squareup/util/Preconditions;->nonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 278
    invoke-direct {p0}, Lcom/squareup/ui/items/DetailSearchableListPresenter;->hasPendingQuery()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListPresenter;->queryCallback:Lcom/squareup/ui/items/DetailSearchableListPresenter$ItemsQueryCallback;

    iget-object v0, v0, Lcom/squareup/ui/items/DetailSearchableListPresenter$ItemsQueryCallback;->searchText:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    .line 281
    :cond_0
    invoke-direct {p0}, Lcom/squareup/ui/items/DetailSearchableListPresenter;->hasCursor()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListPresenter;->itemCursor:Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;

    invoke-virtual {v0}, Lcom/squareup/shared/catalog/synthetictables/LibraryCursor;->getSearchFilter()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    .line 286
    :cond_1
    invoke-direct {p0}, Lcom/squareup/ui/items/DetailSearchableListPresenter;->cancelQueryCallback()V

    .line 287
    new-instance v0, Lcom/squareup/ui/items/DetailSearchableListPresenter$ItemsQueryCallback;

    invoke-direct {v0, p0, p1}, Lcom/squareup/ui/items/DetailSearchableListPresenter$ItemsQueryCallback;-><init>(Lcom/squareup/ui/items/DetailSearchableListPresenter;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListPresenter;->queryCallback:Lcom/squareup/ui/items/DetailSearchableListPresenter$ItemsQueryCallback;

    .line 288
    iget-object p1, p0, Lcom/squareup/ui/items/DetailSearchableListPresenter;->queryCallback:Lcom/squareup/ui/items/DetailSearchableListPresenter$ItemsQueryCallback;

    invoke-direct {p0, p1}, Lcom/squareup/ui/items/DetailSearchableListPresenter;->updateCursorFromCogs(Lcom/squareup/ui/items/DetailSearchableListPresenter$ItemsQueryCallback;)V

    return-void
.end method

.method rowsHaveThumbnails()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected updateActionBarTitle()V
    .locals 3

    .line 135
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListPresenter;->device:Lcom/squareup/util/Device;

    invoke-interface {v0}, Lcom/squareup/util/Device;->isPhoneOrPortraitLessThan10Inches()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/ui/items/DetailSearchableListPresenter;->isNested()Z

    move-result v0

    if-nez v0, :cond_0

    .line 136
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListPresenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/squareup/ui/items/DetailSearchableListPresenter;->getActionBarTitle()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonGlyphAndText(Lcom/squareup/glyph/GlyphTypeface$Glyph;Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 138
    :cond_0
    iget-object v0, p0, Lcom/squareup/ui/items/DetailSearchableListPresenter;->actionBar:Lcom/squareup/marin/widgets/MarinActionBar;

    invoke-virtual {p0}, Lcom/squareup/ui/items/DetailSearchableListPresenter;->getActionBarTitle()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/marin/widgets/MarinActionBar;->setUpButtonTextBackArrow(Ljava/lang/CharSequence;)V

    :goto_0
    return-void
.end method

.method public usesDraggableListView()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
