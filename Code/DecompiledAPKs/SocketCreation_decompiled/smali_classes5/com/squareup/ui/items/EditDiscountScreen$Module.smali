.class public Lcom/squareup/ui/items/EditDiscountScreen$Module;
.super Ljava/lang/Object;
.source "EditDiscountScreen.java"


# annotations
.annotation runtime Ldagger/Module;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/squareup/ui/items/EditDiscountScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Module"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/squareup/ui/items/EditDiscountScreen;


# direct methods
.method public constructor <init>(Lcom/squareup/ui/items/EditDiscountScreen;)V
    .locals 0

    .line 518
    iput-object p1, p0, Lcom/squareup/ui/items/EditDiscountScreen$Module;->this$0:Lcom/squareup/ui/items/EditDiscountScreen;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method provideAppliedLocationsBannerPresenter(Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;Lcom/squareup/util/Res;)Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;
    .locals 8
    .annotation runtime Ldagger/Provides;
    .end annotation

    .line 522
    new-instance v7, Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;

    sget-object v3, Lcom/squareup/ui/items/ItemEditingStringIds;->DISCOUNT:Lcom/squareup/ui/items/ItemEditingStringIds;

    iget-object v0, p0, Lcom/squareup/ui/items/EditDiscountScreen$Module;->this$0:Lcom/squareup/ui/items/EditDiscountScreen;

    iget-object v0, v0, Lcom/squareup/ui/items/EditDiscountScreen;->id:Ljava/lang/String;

    .line 523
    invoke-static {v0}, Lcom/squareup/util/Strings;->isBlank(Ljava/lang/CharSequence;)Z

    move-result v4

    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object v0, v7

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v6}, Lcom/squareup/ui/items/AppliedLocationsBannerPresenter;-><init>(Lcom/squareup/catalog/online/CatalogAppliedLocationCountFetcher;Lcom/squareup/util/Res;Lcom/squareup/ui/items/ItemEditingStringIds;ZZZ)V

    return-object v7
.end method
